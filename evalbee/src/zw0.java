import android.content.SharedPreferences;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.more.TeachersActivity;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.more.SettingsActivity;
import com.ekodroid.omrevaluator.more.ProductAndServicesActivity;
import com.ekodroid.omrevaluator.more.HelpAndContactUsActivity;
import android.view.View$OnClickListener;
import android.content.Intent;
import com.ekodroid.omrevaluator.activities.LoginActivity;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.view.View;
import android.content.Context;
import androidx.fragment.app.Fragment;

// 
// Decompiled by Procyon v0.6.0
// 

public class zw0 extends Fragment
{
    public Context a;
    public View b;
    public LinearLayout c;
    public LinearLayout d;
    public LinearLayout e;
    public LinearLayout f;
    public LinearLayout g;
    public LinearLayout h;
    public TextView i;
    public TextView j;
    public TextView k;
    public TextView l;
    public TextView m;
    public ImageButton n;
    
    public String j(String upperCase) {
        if (upperCase != null && !upperCase.isEmpty()) {
            final String[] split = upperCase.split(" ");
            final StringBuilder sb = new StringBuilder();
            for (final String s : split) {
                if (!s.isEmpty()) {
                    sb.append(s.charAt(0));
                }
            }
            upperCase = sb.toString().toUpperCase();
            return upperCase.substring(0, Math.min(upperCase.length(), 2));
        }
        return "";
    }
    
    public final void k() {
        this.startActivity(new Intent(this.a, (Class)LoginActivity.class));
    }
    
    public final void l() {
        ((View)this.n).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                new bw(this.a.a, true, new y01(this) {
                    public final zw0$a a;
                    
                    @Override
                    public void a(final Object o) {
                    }
                }).show();
            }
        });
    }
    
    public final void m() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                this.a.startActivity(new Intent(this.a.a, (Class)HelpAndContactUsActivity.class));
            }
        });
    }
    
    public final void n() {
        ((View)this.c).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                this.a.startActivity(new Intent(this.a.a, (Class)ProductAndServicesActivity.class));
            }
        });
    }
    
    public final void o() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                this.a.startActivity(new Intent(this.a.a, (Class)SettingsActivity.class));
            }
        });
    }
    
    @Override
    public void onAttach(final Context a) {
        super.onAttach(a);
        this.a = a;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131493007, viewGroup, false);
        this.b = inflate;
        this.i = (TextView)inflate.findViewById(2131297318);
        this.j = (TextView)this.b.findViewById(2131297316);
        this.k = (TextView)this.b.findViewById(2131297315);
        this.l = (TextView)this.b.findViewById(2131297319);
        this.m = (TextView)this.b.findViewById(2131297317);
        this.n = (ImageButton)this.b.findViewById(2131296663);
        this.e = (LinearLayout)this.b.findViewById(2131296760);
        this.d = (LinearLayout)this.b.findViewById(2131296801);
        this.h = (LinearLayout)this.b.findViewById(2131296802);
        this.f = (LinearLayout)this.b.findViewById(2131296809);
        this.c = (LinearLayout)this.b.findViewById(2131296780);
        this.g = (LinearLayout)this.b.findViewById(2131296804);
        this.l();
        this.n();
        this.o();
        this.m();
        this.p();
        this.q();
        this.r();
        return this.b;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final OrgProfile instance = OrgProfile.getInstance(this.a);
        if (instance != null) {
            final String displayName = instance.getDisplayName();
            this.i.setText((CharSequence)displayName);
            this.j.setText((CharSequence)FirebaseAuth.getInstance().e().getEmail());
            this.k.setText((CharSequence)instance.getOrganization());
            final TextView l = this.l;
            String text;
            if (instance.getRole() == UserRole.OWNER) {
                text = "Admin";
            }
            else {
                text = "Teacher";
            }
            l.setText((CharSequence)text);
            if (displayName != null && displayName.length() > 0) {
                this.m.setText((CharSequence)this.j(displayName));
            }
        }
    }
    
    public final void p() {
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                this.a.s();
            }
        });
    }
    
    public final void q() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                xs.c(this.a.a, new y01(this) {
                    public final zw0$c a;
                    
                    @Override
                    public void a(final Object o) {
                        this.a.a.t();
                    }
                }, 2131886832, 2131886572, 2131886835, 2131886163, 0, 2131230970);
            }
        });
    }
    
    public final void r() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zw0 a;
            
            public void onClick(final View view) {
                this.a.startActivity(new Intent(this.a.a, (Class)TeachersActivity.class));
            }
        });
    }
    
    public final void s() {
        try {
            final Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.SUBJECT", "Evalbee OMR scanning application");
            intent.putExtra("android.intent.extra.TEXT", "Please download Evalbee application for OMR/Bubble sheet scanning\n\nAndroid app\nhttps://play.google.com/store/apps/details?id=com.ekodroid.omrevaluator\n\nIOS app\nhttps://apps.apple.com/us/app/evalbee/id1537594035\n\nWeb app\nhttps://evalbee.com\n\nvideo link : \nhttps://www.youtube.com/watch?v=pW9heQVDksI");
            this.startActivity(Intent.createChooser(intent, (CharSequence)"Share via"));
            FirebaseAnalytics.getInstance(this.a).a("ShareApp", null);
        }
        catch (final Exception ex) {}
    }
    
    public final void t() {
        if (FirebaseAuth.getInstance().e() == null) {
            this.k();
            return;
        }
        GoogleSignIn.getClient(this.a, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(this.getString(2131886229)).requestEmail().build()).signOut().addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final zw0 a;
            
            public void onComplete(final Task task) {
                FirebaseAuth.getInstance().n();
                final SharedPreferences sharedPreferences = this.a.a.getSharedPreferences("MyPref", 0);
                sharedPreferences.edit().putString("user_country", (String)null).commit();
                sharedPreferences.edit().putString("user_organization", (String)null).commit();
                sharedPreferences.edit().putString("org_profile", (String)null).commit();
                a91.G(this.a.a, 2131886836, 2131230927, 2131231085);
                this.a.k();
            }
        });
    }
}

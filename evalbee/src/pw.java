import androidx.emoji2.text.c;
import android.text.method.PasswordTransformationMethod;
import android.util.SparseArray;
import android.text.method.TransformationMethod;
import android.text.InputFilter;
import android.widget.TextView;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pw
{
    public final b a;
    
    public pw(final TextView textView, final boolean b) {
        l71.h(textView, "textView cannot be null");
        b a;
        if (!b) {
            a = new c(textView);
        }
        else {
            a = new a(textView);
        }
        this.a = a;
    }
    
    public InputFilter[] a(final InputFilter[] array) {
        return this.a.a(array);
    }
    
    public boolean b() {
        return this.a.b();
    }
    
    public void c(final boolean b) {
        this.a.c(b);
    }
    
    public void d(final boolean b) {
        this.a.d(b);
    }
    
    public TransformationMethod e(final TransformationMethod transformationMethod) {
        return this.a.e(transformationMethod);
    }
    
    public static class a extends b
    {
        public final TextView a;
        public final lw b;
        public boolean c;
        
        public a(final TextView a) {
            this.a = a;
            this.c = true;
            this.b = new lw(a);
        }
        
        @Override
        public InputFilter[] a(final InputFilter[] array) {
            if (!this.c) {
                return this.h(array);
            }
            return this.f(array);
        }
        
        @Override
        public boolean b() {
            return this.c;
        }
        
        @Override
        public void c(final boolean b) {
            if (b) {
                this.l();
            }
        }
        
        @Override
        public void d(final boolean c) {
            this.c = c;
            this.l();
            this.k();
        }
        
        @Override
        public TransformationMethod e(final TransformationMethod transformationMethod) {
            if (this.c) {
                return this.m(transformationMethod);
            }
            return this.j(transformationMethod);
        }
        
        public final InputFilter[] f(final InputFilter[] array) {
            final int length = array.length;
            for (int i = 0; i < length; ++i) {
                if (array[i] == this.b) {
                    return array;
                }
            }
            final InputFilter[] array2 = new InputFilter[array.length + 1];
            System.arraycopy(array, 0, array2, 0, length);
            array2[length] = (InputFilter)this.b;
            return array2;
        }
        
        public final SparseArray g(final InputFilter[] array) {
            final SparseArray sparseArray = new SparseArray(1);
            for (int i = 0; i < array.length; ++i) {
                final InputFilter inputFilter = array[i];
                if (inputFilter instanceof lw) {
                    sparseArray.put(i, (Object)inputFilter);
                }
            }
            return sparseArray;
        }
        
        public final InputFilter[] h(final InputFilter[] array) {
            final SparseArray g = this.g(array);
            if (g.size() == 0) {
                return array;
            }
            final int length = array.length;
            final InputFilter[] array2 = new InputFilter[array.length - g.size()];
            int i = 0;
            int n = 0;
            while (i < length) {
                int n2 = n;
                if (g.indexOfKey(i) < 0) {
                    array2[n] = array[i];
                    n2 = n + 1;
                }
                ++i;
                n = n2;
            }
            return array2;
        }
        
        public void i(final boolean c) {
            this.c = c;
        }
        
        public final TransformationMethod j(final TransformationMethod transformationMethod) {
            TransformationMethod a = transformationMethod;
            if (transformationMethod instanceof rw) {
                a = ((rw)transformationMethod).a();
            }
            return a;
        }
        
        public final void k() {
            this.a.setFilters(this.a(this.a.getFilters()));
        }
        
        public void l() {
            this.a.setTransformationMethod(this.e(this.a.getTransformationMethod()));
        }
        
        public final TransformationMethod m(final TransformationMethod transformationMethod) {
            if (transformationMethod instanceof rw) {
                return transformationMethod;
            }
            if (transformationMethod instanceof PasswordTransformationMethod) {
                return transformationMethod;
            }
            return (TransformationMethod)new rw(transformationMethod);
        }
    }
    
    public abstract static class b
    {
        public abstract InputFilter[] a(final InputFilter[] p0);
        
        public abstract boolean b();
        
        public abstract void c(final boolean p0);
        
        public abstract void d(final boolean p0);
        
        public abstract TransformationMethod e(final TransformationMethod p0);
    }
    
    public static class c extends b
    {
        public final a a;
        
        public c(final TextView textView) {
            this.a = new a(textView);
        }
        
        @Override
        public InputFilter[] a(final InputFilter[] array) {
            if (this.f()) {
                return array;
            }
            return this.a.a(array);
        }
        
        @Override
        public boolean b() {
            return this.a.b();
        }
        
        @Override
        public void c(final boolean b) {
            if (this.f()) {
                return;
            }
            this.a.c(b);
        }
        
        @Override
        public void d(final boolean b) {
            if (this.f()) {
                this.a.i(b);
            }
            else {
                this.a.d(b);
            }
        }
        
        @Override
        public TransformationMethod e(final TransformationMethod transformationMethod) {
            if (this.f()) {
                return transformationMethod;
            }
            return this.a.e(transformationMethod);
        }
        
        public final boolean f() {
            return androidx.emoji2.text.c.h() ^ true;
        }
    }
}

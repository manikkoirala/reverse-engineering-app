import com.google.protobuf.a0;
import com.google.protobuf.GeneratedMessageLite;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ui0 extends GeneratedMessageLite implements xv0
{
    private static final ui0 DEFAULT_INSTANCE;
    public static final int LATITUDE_FIELD_NUMBER = 1;
    public static final int LONGITUDE_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    private double latitude_;
    private double longitude_;
    
    static {
        GeneratedMessageLite.V(ui0.class, DEFAULT_INSTANCE = new ui0());
    }
    
    public static /* synthetic */ ui0 Z() {
        return ui0.DEFAULT_INSTANCE;
    }
    
    public static ui0 c0() {
        return ui0.DEFAULT_INSTANCE;
    }
    
    public static b f0() {
        return (b)ui0.DEFAULT_INSTANCE.u();
    }
    
    public double d0() {
        return this.latitude_;
    }
    
    public double e0() {
        return this.longitude_;
    }
    
    public final void g0(final double latitude_) {
        this.latitude_ = latitude_;
    }
    
    public final void h0(final double longitude_) {
        this.longitude_ = longitude_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (ui0$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = ui0.PARSER) == null) {
                    synchronized (ui0.class) {
                        if (ui0.PARSER == null) {
                            ui0.PARSER = new GeneratedMessageLite.b(ui0.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return ui0.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(ui0.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0000\u0002\u0000", new Object[] { "latitude_", "longitude_" });
            }
            case 2: {
                return new b((ui0$a)null);
            }
            case 1: {
                return new ui0();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(ui0.Z());
        }
        
        public b A(final double n) {
            ((a)this).s();
            ((ui0)super.b).g0(n);
            return this;
        }
        
        public b B(final double n) {
            ((a)this).s();
            ((ui0)super.b).h0(n);
            return this;
        }
    }
}

import android.os.Build$VERSION;
import android.view.View;
import android.view.Window;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d62
{
    public static v72 a(final Window window, final View view) {
        return new v72(window, view);
    }
    
    public static void b(final Window window, final boolean b) {
        if (Build$VERSION.SDK_INT >= 30) {
            d62.b.a(window, b);
        }
        else {
            a.a(window, b);
        }
    }
    
    public abstract static class a
    {
        public static void a(final Window window, final boolean b) {
            final View decorView = window.getDecorView();
            final int systemUiVisibility = decorView.getSystemUiVisibility();
            int systemUiVisibility2;
            if (b) {
                systemUiVisibility2 = (systemUiVisibility & 0xFFFFF8FF);
            }
            else {
                systemUiVisibility2 = (systemUiVisibility | 0x700);
            }
            decorView.setSystemUiVisibility(systemUiVisibility2);
        }
    }
    
    public abstract static class b
    {
        public static void a(final Window window, final boolean decorFitsSystemWindows) {
            window.setDecorFitsSystemWindows(decorFitsSystemWindows);
        }
    }
}

import android.system.ErrnoException;
import android.system.OsConstants;
import android.system.Os;
import android.content.ContentResolver;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import android.os.ParcelFileDescriptor;
import android.os.CancellationSignal;
import java.io.File;
import android.content.res.Resources;
import android.content.Context;
import java.lang.reflect.Executable;
import android.util.Log;
import java.lang.reflect.Array;
import android.graphics.Typeface;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;

// 
// Decompiled by Procyon v0.6.0
// 

public class tz1 extends yz1
{
    public static Class b;
    public static Constructor c;
    public static Method d;
    public static Method e;
    public static boolean f = false;
    
    public static boolean h(final Object ex, final String s, final int i, final boolean b) {
        k();
        try {
            return (boolean)tz1.d.invoke(ex, s, i, b);
        }
        catch (final InvocationTargetException ex) {}
        catch (final IllegalAccessException ex2) {}
        throw new RuntimeException(ex);
    }
    
    public static Typeface i(Object cause) {
        k();
        try {
            final Object instance = Array.newInstance(tz1.b, 1);
            Array.set(instance, 0, cause);
            cause = (InvocationTargetException)tz1.e.invoke(null, instance);
            return (Typeface)cause;
        }
        catch (final InvocationTargetException cause) {}
        catch (final IllegalAccessException ex) {}
        throw new RuntimeException(cause);
    }
    
    public static void k() {
        if (tz1.f) {
            return;
        }
        tz1.f = true;
        Class<?> forName = null;
        Executable constructor = null;
        Method method = null;
        Method method2 = null;
        Label_0107: {
            try {
                forName = Class.forName("android.graphics.FontFamily");
                constructor = forName.getConstructor((Class<?>[])new Class[0]);
                method = forName.getMethod("addFontWeightStyle", String.class, Integer.TYPE, Boolean.TYPE);
                method2 = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(forName, 1).getClass());
                break Label_0107;
            }
            catch (final NoSuchMethodException forName) {}
            catch (final ClassNotFoundException ex) {}
            Log.e("TypefaceCompatApi21Impl", forName.getClass().getName(), (Throwable)forName);
            forName = null;
            method2 = null;
            constructor = (method = null);
        }
        tz1.c = (Constructor)constructor;
        tz1.b = forName;
        tz1.d = method;
        tz1.e = method2;
    }
    
    public static Object l() {
        k();
        InvocationTargetException instance = null;
        try {
            instance = tz1.c.newInstance(new Object[0]);
            return instance;
        }
        catch (final InvocationTargetException instance) {}
        catch (final InstantiationException instance) {}
        catch (final IllegalAccessException ex) {}
        throw new RuntimeException(instance);
    }
    
    @Override
    public Typeface a(final Context context, j70.c e, final Resources resources, int i) {
        final Object l = l();
        final j70.d[] a = e.a();
        final int length = a.length;
        i = 0;
        while (i < length) {
            final j70.d d = a[i];
            e = (j70.c)zz1.e(context);
            if (e == null) {
                return null;
            }
            try {
                if (!zz1.c((File)e, resources, d.b())) {
                    return null;
                }
                final boolean h = h(l, ((File)e).getPath(), d.e(), d.f());
                ((File)e).delete();
                if (!h) {
                    return null;
                }
                ++i;
                continue;
            }
            catch (final RuntimeException ex) {
                return null;
            }
            finally {
                ((File)e).delete();
            }
            break;
        }
        return i(l);
    }
    
    @Override
    public Typeface b(final Context context, CancellationSignal openFileDescriptor, k70.b[] array, final int n) {
        if (array.length < 1) {
            return null;
        }
        final k70.b g = this.g(array, n);
        final ContentResolver contentResolver = context.getContentResolver();
        try {
            openFileDescriptor = (CancellationSignal)contentResolver.openFileDescriptor(g.d(), "r", openFileDescriptor);
            if (openFileDescriptor == null) {
                if (openFileDescriptor != null) {
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                }
                return null;
            }
            try {
                final File j = this.j((ParcelFileDescriptor)openFileDescriptor);
                if (j != null && j.canRead()) {
                    final Typeface fromFile = Typeface.createFromFile(j);
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                    return fromFile;
                }
                array = (k70.b[])(Object)new FileInputStream(((ParcelFileDescriptor)openFileDescriptor).getFileDescriptor());
                try {
                    final Typeface c = super.c(context, (InputStream)(Object)array);
                    ((FileInputStream)(Object)array).close();
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                    return c;
                }
                finally {
                    try {
                        ((FileInputStream)(Object)array).close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)context).addSuppressed(exception);
                    }
                }
            }
            finally {
                try {
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                }
                finally {
                    final Throwable exception2;
                    ((Throwable)context).addSuppressed(exception2);
                }
            }
        }
        catch (final IOException ex) {
            return null;
        }
    }
    
    public final File j(final ParcelFileDescriptor parcelFileDescriptor) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("/proc/self/fd/");
            sb.append(parcelFileDescriptor.getFd());
            final String readlink = Os.readlink(sb.toString());
            if (OsConstants.S_ISREG(Os.stat(readlink).st_mode)) {
                return new File(readlink);
            }
            return null;
        }
        catch (final ErrnoException ex) {
            return null;
        }
    }
}

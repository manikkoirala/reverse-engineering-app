import android.app.Dialog;
import java.util.ArrayList;
import java.io.IOException;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import java.io.Writer;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import java.io.File;
import android.app.ProgressDialog;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.content.Context;
import android.os.AsyncTask;

// 
// Decompiled by Procyon v0.6.0
// 

public class tu extends AsyncTask
{
    public Context a;
    public SheetTemplate2 b;
    public boolean c;
    public String d;
    public String e;
    public ExamId f;
    public ProgressDialog g;
    
    public tu(final Context a, final ExamId f, final SheetTemplate2 b) {
        this.a = a;
        this.b = b;
        this.f = f;
        this.d = b.getName().replaceAll("[^a-zA-Z0-9_-]", "_");
        final File externalFilesDir = a.getExternalFilesDir(ok.c);
        if (!externalFilesDir.exists()) {
            externalFilesDir.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.d);
        sb.append("_key.csv");
        this.e = new File(externalFilesDir, sb.toString()).getAbsolutePath();
    }
    
    public final void a(final String[][] array, final LabelProfile labelProfile) {
        array[0][0] = labelProfile.getQueNoString();
        final String[] questionNumberLabels = labelProfile.getQuestionNumberLabels();
        for (int i = 1; i < array.length; ++i) {
            array[i][0] = questionNumberLabels[i - 1];
        }
    }
    
    public final boolean b(final String pathname) {
        Object answerOptionKeys = null;
        Object answerKeys;
        final Object o = answerKeys = null;
        FileOutputStream out = null;
        ArrayList list;
        try {
            try {
                answerKeys = o;
                answerKeys = o;
                final File file = new File(pathname);
                answerKeys = o;
                out = new FileOutputStream(file);
                try {
                    answerKeys = new OutputStreamWriter(out);
                    final zd zd = new zd((Writer)answerKeys);
                    final LabelProfile labelProfile = this.b.getLabelProfile();
                    answerKeys = this.b.getAnswerKeys();
                    if (answerKeys != null) {
                        if (((Writer)answerKeys).length != 0) {
                            final String[][] array = new String[answerKeys[0].getAnswerOptionKeys().size() + 1][((Writer)answerKeys).length + 1];
                            this.a(array, labelProfile);
                            int n;
                            for (int i = 0; i < ((Writer)answerKeys).length; i = n) {
                                answerOptionKeys = answerKeys[i].getAnswerOptionKeys();
                                final String[] array2 = array[0];
                                n = i + 1;
                                final StringBuilder sb = new StringBuilder();
                                sb.append(labelProfile.getExamSetString());
                                sb.append(" ");
                                sb.append(labelProfile.getExamSetLabels()[i]);
                                array2[n] = sb.toString();
                                String q;
                                for (int j = 0; j < ((ArrayList)answerOptionKeys).size(); ++j, array[j][n] = q) {
                                    q = e5.q((AnswerOptionKey)((ArrayList)answerOptionKeys).get(j), this.b.getLabelProfile());
                                }
                            }
                            if (((Writer)answerKeys).length == 1) {
                                array[0][1] = "KEY";
                            }
                            for (int k = 0; k < array.length; ++k) {
                                ((h)zd).a(array[k]);
                            }
                            ((h)zd).flush();
                            ((h)zd).close();
                            try {
                                out.flush();
                                out.close();
                            }
                            catch (final IOException ex) {
                                ex.printStackTrace();
                            }
                            return true;
                        }
                    }
                    try {
                        out.flush();
                        out.close();
                    }
                    catch (final IOException ex2) {
                        ex2.printStackTrace();
                    }
                    return false;
                }
                catch (final Exception answerKeys) {}
                finally {
                    answerKeys = out;
                }
            }
            finally {}
        }
        catch (final Exception out) {
            list = (ArrayList)answerOptionKeys;
        }
        ((Throwable)out).printStackTrace();
        if (list != null) {
            try {
                ((OutputStream)list).flush();
                ((FileOutputStream)list).close();
            }
            catch (final IOException ex3) {
                ex3.printStackTrace();
            }
        }
        return false;
        if (answerKeys != null) {
            try {
                ((OutputStream)answerKeys).flush();
                ((FileOutputStream)answerKeys).close();
            }
            catch (final IOException ex4) {
                ex4.printStackTrace();
            }
        }
    }
    
    public Void c(final Void... array) {
        this.c = this.b(this.e);
        return null;
    }
    
    public void d(final Void void1) {
        if (((Dialog)this.g).isShowing()) {
            ((Dialog)this.g).dismiss();
        }
        if (this.c) {
            new gn1(this.a, this.e, this.d);
        }
    }
    
    public void e(final Integer... array) {
    }
    
    public void onPreExecute() {
        (this.g = new ProgressDialog(this.a)).setMessage((CharSequence)"Creating csv, please wait...");
        ((Dialog)this.g).setCancelable(false);
        ((Dialog)this.g).show();
    }
}

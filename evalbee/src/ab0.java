import java.util.Objects;
import java.util.HashSet;
import java.util.Collection;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ab0
{
    public final List a;
    
    public ab0(final List a) {
        fg0.e((Object)a, "topics");
        this.a = a;
    }
    
    public final List a() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ab0)) {
            return false;
        }
        final int size = this.a.size();
        final ab0 ab0 = (ab0)o;
        return size == ab0.a.size() && fg0.a((Object)new HashSet(this.a), (Object)new HashSet(ab0.a));
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.a);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Topics=");
        sb.append(this.a);
        return sb.toString();
    }
}

import java.io.File;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public class jq1 implements Comparable
{
    public final Uri a;
    public final o30 b;
    
    public jq1(final Uri a, final o30 b) {
        final boolean b2 = true;
        Preconditions.checkArgument(a != null, (Object)"storageUri cannot be null");
        Preconditions.checkArgument(b != null && b2, (Object)"FirebaseApp cannot be null");
        this.a = a;
        this.b = b;
    }
    
    public jq1 a(String a) {
        Preconditions.checkArgument(TextUtils.isEmpty((CharSequence)a) ^ true, (Object)"childName cannot be null or empty");
        a = io1.a(a);
        return new jq1(this.a.buildUpon().appendEncodedPath(io1.b(a)).build(), this.b);
    }
    
    public int c(final jq1 jq1) {
        return this.a.compareTo(jq1.a);
    }
    
    public Task d() {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        br1.a().e(new js(this, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public r10 e() {
        return this.j().a();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof jq1 && ((jq1)o).toString().equals(this.toString());
    }
    
    public x00 f(final Uri uri) {
        final x00 x00 = new x00(this, uri);
        x00.X();
        return x00;
    }
    
    public x00 g(final File file) {
        return this.f(Uri.fromFile(file));
    }
    
    public jq1 h() {
        final String path = this.a.getPath();
        if (!TextUtils.isEmpty((CharSequence)path)) {
            String substring = "/";
            if (!path.equals("/")) {
                final int lastIndex = path.lastIndexOf(47);
                if (lastIndex != -1) {
                    substring = path.substring(0, lastIndex);
                }
                return new jq1(this.a.buildUpon().path(substring).build(), this.b);
            }
        }
        return null;
    }
    
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
    
    public o30 j() {
        return this.b;
    }
    
    public kq1 k() {
        final Uri a = this.a;
        this.b.e();
        return new kq1(a, null);
    }
    
    public i12 l(final Uri uri) {
        Preconditions.checkArgument(uri != null, (Object)"uri cannot be null");
        final i12 i12 = new i12(this, null, uri, null);
        i12.X();
        return i12;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("gs://");
        sb.append(this.a.getAuthority());
        sb.append(this.a.getEncodedPath());
        return sb.toString();
    }
}

import java.util.Arrays;
import java.util.HashMap;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import com.google.gson.internal.$Gson$Types;
import java.util.LinkedHashMap;
import java.util.Map;
import java.lang.reflect.InvocationTargetException;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import com.google.gson.ReflectionAccessFilter$FilterResult;
import com.google.gson.reflect.TypeToken;
import com.google.gson.JsonIOException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Member;
import java.lang.reflect.AccessibleObject;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class dd1 implements iz1
{
    public final bl a;
    public final r00 b;
    public final oy c;
    public final hh0 d;
    public final List e;
    
    public dd1(final bl a, final r00 b, final oy c, final hh0 d, final List e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static void c(Object o, final AccessibleObject accessibleObject) {
        if (Modifier.isStatic(((Member)accessibleObject).getModifiers())) {
            o = null;
        }
        if (ad1.a(accessibleObject, o)) {
            return;
        }
        final String g = cd1.g(accessibleObject, true);
        final StringBuilder sb = new StringBuilder();
        sb.append(g);
        sb.append(" is not accessible and ReflectionAccessFilter does not permit making it accessible. Register a TypeAdapter for the declaring type, adjust the access filter or increase the visibility of the element and its declaring type.");
        throw new JsonIOException(sb.toString());
    }
    
    @Override
    public hz1 a(final gc0 gc0, final TypeToken typeToken) {
        final Class rawType = typeToken.getRawType();
        if (!Object.class.isAssignableFrom(rawType)) {
            return null;
        }
        final ReflectionAccessFilter$FilterResult b = ad1.b(this.e, rawType);
        if (b == ReflectionAccessFilter$FilterResult.BLOCK_ALL) {
            final StringBuilder sb = new StringBuilder();
            sb.append("ReflectionAccessFilter does not permit using reflection for ");
            sb.append(rawType);
            sb.append(". Register a TypeAdapter for this type or adjust the access filter.");
            throw new JsonIOException(sb.toString());
        }
        final boolean b2 = b == ReflectionAccessFilter$FilterResult.BLOCK_INACCESSIBLE;
        if (cd1.k(rawType)) {
            return new e(rawType, this.e(gc0, typeToken, rawType, b2, true), b2);
        }
        return new d(this.a.b(typeToken), this.e(gc0, typeToken, rawType, b2, false));
    }
    
    public final c d(final gc0 gc0, final Field field, final Method method, final String s, final TypeToken typeToken, final boolean b, final boolean b2, final boolean b3) {
        final boolean a = d81.a(typeToken.getRawType());
        final int modifiers = field.getModifiers();
        final boolean b4 = Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers);
        final gh0 gh0 = field.getAnnotation(gh0.class);
        hz1 b5;
        if (gh0 != null) {
            b5 = this.d.b(this.a, gc0, typeToken, gh0);
        }
        else {
            b5 = null;
        }
        final boolean b6 = b5 != null;
        hz1 l = b5;
        if (b5 == null) {
            l = gc0.l(typeToken);
        }
        return (c)new c(this, s, field, b, b2, b3, method, b6, l, gc0, typeToken, a, b4) {
            public final boolean f;
            public final Method g;
            public final boolean h;
            public final hz1 i;
            public final gc0 j;
            public final TypeToken k;
            public final boolean l;
            public final boolean m;
            public final dd1 n;
            
            @Override
            public void a(final rh0 rh0, final int n, final Object[] array) {
                final Object b = this.i.b(rh0);
                if (b == null && this.l) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("null is not allowed as value for record component '");
                    sb.append(super.c);
                    sb.append("' of primitive type; at path ");
                    sb.append(rh0.getPath());
                    throw new JsonParseException(sb.toString());
                }
                array[n] = b;
            }
            
            @Override
            public void b(final rh0 rh0, final Object obj) {
                final Object b = this.i.b(rh0);
                if (b != null || !this.l) {
                    if (this.f) {
                        c(obj, super.b);
                    }
                    else if (this.m) {
                        final String g = cd1.g(super.b, false);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Cannot set value of 'static final' ");
                        sb.append(g);
                        throw new JsonIOException(sb.toString());
                    }
                    super.b.set(obj, b);
                }
            }
            
            @Override
            public void c(final vh0 vh0, Object i) {
                if (!super.d) {
                    return;
                }
                if (this.f) {
                    Member member;
                    if ((member = this.g) == null) {
                        member = super.b;
                    }
                    c(i, (AccessibleObject)member);
                }
                final Method g = this.g;
                Object o = null;
                Label_0124: {
                    if (g != null) {
                        try {
                            o = g.invoke(i, new Object[0]);
                            break Label_0124;
                        }
                        catch (final InvocationTargetException ex) {
                            final String g2 = cd1.g(this.g, false);
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Accessor ");
                            sb.append(g2);
                            sb.append(" threw exception");
                            throw new JsonIOException(sb.toString(), ex.getCause());
                        }
                    }
                    o = super.b.get(i);
                }
                if (o == i) {
                    return;
                }
                vh0.o(super.a);
                if (this.h) {
                    i = this.i;
                }
                else {
                    i = new jz1(this.j, this.i, this.k.getType());
                }
                ((hz1)i).d(vh0, o);
            }
        };
    }
    
    public final Map e(final gc0 gc0, final TypeToken typeToken, final Class obj, final boolean b, final boolean b2) {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (obj.isInterface()) {
            return linkedHashMap;
        }
        Class<?> rawType = obj;
        TypeToken<?> value = typeToken;
        boolean b3 = b;
        while (rawType != Object.class) {
            final Field[] declaredFields = rawType.getDeclaredFields();
            final int n = 0;
            boolean b4 = b3;
            if (rawType != obj) {
                b4 = b3;
                if (declaredFields.length > 0) {
                    final ReflectionAccessFilter$FilterResult b5 = ad1.b(this.e, rawType);
                    if (b5 == ReflectionAccessFilter$FilterResult.BLOCK_ALL) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("ReflectionAccessFilter does not permit using reflection for ");
                        sb.append(rawType);
                        sb.append(" (supertype of ");
                        sb.append(obj);
                        sb.append("). Register a TypeAdapter for this type or adjust the access filter.");
                        throw new JsonIOException(sb.toString());
                    }
                    b4 = (b5 == ReflectionAccessFilter$FilterResult.BLOCK_INACCESSIBLE);
                }
            }
            int n2;
            int n3;
            for (int length = declaredFields.length, i = 0; i < length; length = n3, i = n2) {
                final Field field = declaredFields[i];
                int g = this.g(field, true) ? 1 : 0;
                int g2 = this.g(field, (boolean)(n != 0)) ? 1 : 0;
                if (g == 0 && g2 == 0) {
                    n2 = i;
                    n3 = length;
                }
                else {
                    c c = null;
                    Method h;
                    if (b2) {
                        if (Modifier.isStatic(field.getModifiers())) {
                            h = null;
                            g2 = n;
                        }
                        else {
                            h = cd1.h(rawType, field);
                            if (!b4) {
                                cd1.l(h);
                            }
                            if (h.getAnnotation(ml1.class) != null && field.getAnnotation(ml1.class) == null) {
                                final String g3 = cd1.g(h, (boolean)(n != 0));
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("@SerializedName on ");
                                sb2.append(g3);
                                sb2.append(" is not supported");
                                throw new JsonIOException(sb2.toString());
                            }
                        }
                    }
                    else {
                        h = null;
                    }
                    if (!b4 && h == null) {
                        cd1.l(field);
                    }
                    final Type o = $Gson$Types.o(value.getType(), rawType, field.getGenericType());
                    final List f = this.f(field);
                    for (int size = f.size(), j = n; j < size; ++j) {
                        final String s = f.get(j);
                        if (j != 0) {
                            g = n;
                        }
                        final c c2 = (c)linkedHashMap.put(s, this.d(gc0, field, h, s, TypeToken.get(o), (boolean)(g != 0), (boolean)(g2 != 0), b4));
                        if (c == null) {
                            c = c2;
                        }
                    }
                    n2 = i;
                    n3 = length;
                    if (c != null) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Class ");
                        sb3.append(obj.getName());
                        sb3.append(" declares multiple JSON fields named '");
                        sb3.append(c.a);
                        sb3.append("'; conflict is caused by fields ");
                        sb3.append(cd1.f(c.b));
                        sb3.append(" and ");
                        sb3.append(cd1.f(field));
                        throw new IllegalArgumentException(sb3.toString());
                    }
                }
                ++n2;
            }
            value = TypeToken.get($Gson$Types.o(value.getType(), rawType, rawType.getGenericSuperclass()));
            rawType = value.getRawType();
            b3 = b4;
        }
        return linkedHashMap;
    }
    
    public final List f(final Field field) {
        final ml1 ml1 = field.getAnnotation(ml1.class);
        if (ml1 == null) {
            return Collections.singletonList(this.b.translateName(field));
        }
        final String value = ml1.value();
        final String[] alternate = ml1.alternate();
        if (alternate.length == 0) {
            return Collections.singletonList(value);
        }
        final ArrayList c = new ArrayList(alternate.length + 1);
        c.add((Object)value);
        Collections.addAll((Collection<? super String>)c, alternate);
        return c;
    }
    
    public final boolean g(final Field field, final boolean b) {
        return !this.c.d(field.getType(), b) && !this.c.h(field, b);
    }
    
    public abstract static class b extends hz1
    {
        public final Map a;
        
        public b(final Map a) {
            this.a = a;
        }
        
        @Override
        public Object b(final rh0 rh0) {
            if (rh0.o0() == JsonToken.NULL) {
                rh0.R();
                return null;
            }
            final Object e = this.e();
            try {
                rh0.b();
                while (rh0.k()) {
                    final c c = this.a.get(rh0.K());
                    if (c != null && c.e) {
                        this.g(e, rh0, c);
                    }
                    else {
                        rh0.y0();
                    }
                }
                rh0.g();
                return this.f(e);
            }
            catch (final IllegalAccessException ex) {
                throw cd1.e(ex);
            }
            catch (final IllegalStateException ex2) {
                throw new JsonSyntaxException(ex2);
            }
        }
        
        @Override
        public void d(final vh0 vh0, final Object o) {
            if (o == null) {
                vh0.u();
                return;
            }
            vh0.d();
            try {
                final Iterator iterator = this.a.values().iterator();
                while (iterator.hasNext()) {
                    ((c)iterator.next()).c(vh0, o);
                }
                vh0.g();
            }
            catch (final IllegalAccessException ex) {
                throw cd1.e(ex);
            }
        }
        
        public abstract Object e();
        
        public abstract Object f(final Object p0);
        
        public abstract void g(final Object p0, final rh0 p1, final c p2);
    }
    
    public abstract static class c
    {
        public final String a;
        public final Field b;
        public final String c;
        public final boolean d;
        public final boolean e;
        
        public c(final String a, final Field b, final boolean d, final boolean e) {
            this.a = a;
            this.b = b;
            this.c = b.getName();
            this.d = d;
            this.e = e;
        }
        
        public abstract void a(final rh0 p0, final int p1, final Object[] p2);
        
        public abstract void b(final rh0 p0, final Object p1);
        
        public abstract void c(final vh0 p0, final Object p1);
    }
    
    public static final class d extends b
    {
        public final u01 b;
        
        public d(final u01 b, final Map map) {
            super(map);
            this.b = b;
        }
        
        @Override
        public Object e() {
            return this.b.a();
        }
        
        @Override
        public Object f(final Object o) {
            return o;
        }
        
        @Override
        public void g(final Object o, final rh0 rh0, final c c) {
            c.b(rh0, o);
        }
    }
    
    public static final class e extends b
    {
        public static final Map e;
        public final Constructor b;
        public final Object[] c;
        public final Map d;
        
        static {
            e = j();
        }
        
        public e(final Class clazz, final Map map, final boolean b) {
            super(map);
            this.d = new HashMap();
            final Constructor i = cd1.i(clazz);
            this.b = i;
            if (b) {
                c(null, i);
            }
            else {
                cd1.l(i);
            }
            final String[] j = cd1.j(clazz);
            final int n = 0;
            for (int k = 0; k < j.length; ++k) {
                this.d.put(j[k], k);
            }
            final Class[] parameterTypes = this.b.getParameterTypes();
            this.c = new Object[parameterTypes.length];
            for (int l = n; l < parameterTypes.length; ++l) {
                this.c[l] = dd1.e.e.get(parameterTypes[l]);
            }
        }
        
        public static Map j() {
            final HashMap hashMap = new HashMap();
            hashMap.put(Byte.TYPE, 0);
            hashMap.put(Short.TYPE, 0);
            hashMap.put(Integer.TYPE, 0);
            hashMap.put(Long.TYPE, 0L);
            hashMap.put(Float.TYPE, 0.0f);
            hashMap.put(Double.TYPE, 0.0);
            hashMap.put(Character.TYPE, '\0');
            hashMap.put(Boolean.TYPE, Boolean.FALSE);
            return hashMap;
        }
        
        public Object[] h() {
            return this.c.clone();
        }
        
        public Object i(final Object[] array) {
            try {
                return this.b.newInstance(array);
            }
            catch (final InvocationTargetException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to invoke constructor '");
                sb.append(cd1.c(this.b));
                sb.append("' with args ");
                sb.append(Arrays.toString(array));
                throw new RuntimeException(sb.toString(), ex.getCause());
            }
            catch (final IllegalArgumentException instance) {
                goto Label_0076;
            }
            catch (final InstantiationException ex2) {}
            catch (final IllegalAccessException ex3) {
                throw cd1.e(ex3);
            }
        }
        
        public void k(final Object[] array, final rh0 rh0, final c c) {
            final Integer n = this.d.get(c.c);
            if (n != null) {
                c.a(rh0, n, array);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not find the index in the constructor '");
            sb.append(cd1.c(this.b));
            sb.append("' for field with name '");
            sb.append(c.c);
            sb.append("', unable to determine which argument in the constructor the field corresponds to. This is unexpected behavior, as we expect the RecordComponents to have the same names as the fields in the Java class, and that the order of the RecordComponents is the same as the order of the canonical constructor parameters.");
            throw new IllegalStateException(sb.toString());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class px0
{
    public static final Object f;
    public static final Object g;
    public double[][] a;
    public Object[] b;
    public int c;
    public double d;
    public final int e;
    
    static {
        f = new Object();
        g = new Object();
    }
    
    public px0(final int e) {
        final double[][] a = new double[2][0];
        this.a = a;
        this.b = new Object[a.length];
        this.c = 0;
        this.d = 1.0;
        if (e > 0) {
            this.e = e;
            return;
        }
        throw new IllegalArgumentException("dimension > 0 required");
    }
    
    public final void a(final double[] array, Object f) {
        if (array == null) {
            throw new IllegalArgumentException("Point cannot be null.");
        }
        if (array.length >= this.e) {
            final int c = this.c;
            if (c == 0) {
                f = px0.f;
            }
            this.b(c + 1);
            final double[][] a = this.a;
            final int c2 = this.c;
            a[c2] = array;
            this.b[c2] = f;
            this.c = c2 + 1;
            return;
        }
        throw new IllegalArgumentException("p.length >= dimension required");
    }
    
    public void b(int i) {
        final double[][] a = this.a;
        if (a.length < i) {
            final int n = a.length * 2;
            if (n >= i) {
                i = n;
            }
            final double[][] a2 = new double[i][];
            final int n2 = 0;
            for (int j = 0; j < this.c; ++j) {
                a2[j] = this.a[j];
            }
            final Object[] b = new Object[i];
            for (i = n2; i < this.c; ++i) {
                b[i] = this.b[i];
            }
            this.a = a2;
            this.b = b;
        }
    }
    
    public int c() {
        return this.e;
    }
    
    public double d() {
        return this.d;
    }
    
    public void e(final double[] array) {
        this.a(array, px0.g);
    }
    
    public void f(final double[] array) {
        this.a(array, px0.f);
    }
    
    public void g(final double d) {
        if (d > 0.0) {
            this.d = d;
            return;
        }
        throw new IllegalArgumentException("flatness > 0 required");
    }
}

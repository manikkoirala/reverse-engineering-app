import com.google.android.gms.common.internal.Preconditions;
import com.google.firebase.FirebaseException;

// 
// Decompiled by Procyon v0.6.0
// 

public final class fq extends u5
{
    public final String a;
    public final FirebaseException b;
    
    public fq(final String a, final FirebaseException b) {
        Preconditions.checkNotEmpty(a);
        this.a = a;
        this.b = b;
    }
    
    public static fq c(final s5 s5) {
        Preconditions.checkNotNull(s5);
        return new fq(s5.b(), null);
    }
    
    public static fq d(final FirebaseException ex) {
        return new fq("eyJlcnJvciI6IlVOS05PV05fRVJST1IifQ==", Preconditions.checkNotNull(ex));
    }
    
    @Override
    public Exception a() {
        return this.b;
    }
    
    @Override
    public String b() {
        return this.a;
    }
}

import java.nio.ByteBuffer;
import java.util.UUID;
import java.nio.charset.Charset;
import android.util.Base64;

// 
// Decompiled by Procyon v0.6.0
// 

public class dc1
{
    public static final byte a;
    public static final byte b;
    
    static {
        a = Byte.parseByte("01110000", 2);
        b = Byte.parseByte("00001111", 2);
    }
    
    public static String b(final byte[] array) {
        return new String(Base64.encode(array, 11), Charset.defaultCharset()).substring(0, 22);
    }
    
    public static byte[] c(final UUID uuid, final byte[] array) {
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }
    
    public String a() {
        final byte[] c = c(UUID.randomUUID(), new byte[17]);
        final byte b = c[0];
        c[16] = b;
        c[0] = (byte)((b & dc1.b) | dc1.a);
        return b(c);
    }
}

import android.content.pm.PackageInfo;
import com.android.volley.Request;
import android.content.pm.PackageManager$NameNotFoundException;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.serializable.UserProfile;
import com.android.volley.d;
import com.ekodroid.omrevaluator.serializable.UserAccount;
import android.os.Build$VERSION;
import com.google.firebase.auth.FirebaseAuth;
import android.content.Context;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public class cb0
{
    public String a;
    public SharedPreferences b;
    public ee1 c;
    public y01 d;
    public String e;
    public Context f;
    public int g;
    
    public cb0(final Context f, final ee1 c, final y01 d, final String e) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/userDataProfile");
        this.a = sb.toString();
        this.g = 0;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.b = f.getApplicationContext().getSharedPreferences("MyPref", 0);
        this.g();
    }
    
    public static /* synthetic */ String a(final cb0 cb0) {
        return cb0.e;
    }
    
    public static /* synthetic */ Context c(final cb0 cb0) {
        return cb0.f;
    }
    
    public static /* synthetic */ int d(final cb0 cb0, final int g) {
        return cb0.g = g;
    }
    
    public final String f(String i) {
        final r30 e = FirebaseAuth.getInstance().e();
        if (e == null) {
            return null;
        }
        try {
            final String j = b.i(e.getEmail(), "emailKey");
            final String displayName = e.getDisplayName();
            final int int1 = this.b.getInt("count_saveScan", 0);
            final int int2 = this.b.getInt("count_cancelScan", 0);
            final int int3 = this.b.getInt("count_email", 0);
            final int int4 = this.b.getInt("count_sms", 0);
            final int int5 = this.b.getInt("count_saveImportScan", 0);
            final int int6 = this.b.getInt("count_errorImportScan", 0);
            final String upperCase = this.b.getString("user_country", "").toUpperCase();
            final String string = this.b.getString("user_city", "");
            final String string2 = this.b.getString("user_organization", "");
            final String release = Build$VERSION.RELEASE;
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(a91.f());
            i = b.i(new gc0().s(new UserAccount(j, int1, int2, int3, int4, a91.n(), int5, int6, upperCase, displayName, release, sb.toString(), this.b.getString("fcmId", " "), i, string, string2)), i);
            return i;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public final void g() {
        final hr1 hr1 = new hr1(this, 1, this.a, new d.b(this) {
            public final cb0 a;
            
            public void b(final String s) {
                try {
                    final UserProfile userProfile = (UserProfile)new gc0().j(s, UserProfile.class);
                    if (userProfile.getUserData() != null) {
                        final UserAccount userAccount = (UserAccount)new gc0().j(b.g(userProfile.getUserData(), cb0.a(this.a)), UserAccount.class);
                        if (userAccount != null) {
                            this.a.b.edit().putInt("count_saveScan", userAccount.getScanSave()).commit();
                            this.a.b.edit().putInt("count_cancelScan", userAccount.getScanCancel()).commit();
                            this.a.b.edit().putInt("count_email", userAccount.getEmailSent()).commit();
                            this.a.b.edit().putInt("count_sms", userAccount.getSmsSent()).commit();
                            this.a.b.edit().putInt("count_saveImportScan", userAccount.getImageScan()).commit();
                            this.a.b.edit().putInt("count_errorImportScan", userAccount.getImageScanError()).commit();
                            this.a.b.edit().putString("user_country", userAccount.getCountry()).commit();
                            this.a.b.edit().putString("user_city", userAccount.getCity()).commit();
                            this.a.b.edit().putString("user_organization", userAccount.getOrganization()).commit();
                        }
                    }
                    this.a.h("Success");
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.h(null);
                }
            }
        }, new d.a(this) {
            public final cb0 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.h(null);
            }
        }) {
            public final cb0 w;
            
            @Override
            public byte[] k() {
                Object o = null;
                try {
                    o = cb0.c(this.w).getPackageManager().getPackageInfo(cb0.c(this.w).getPackageName(), 0);
                    cb0.d(this.w, ((PackageInfo)o).versionCode);
                    o = this.w;
                    o = new UserProfile(b.i(cb0.a(this.w), "phoneKey"), null, ((cb0)o).f(cb0.a((cb0)o)), null);
                    o = new gc0().s(o).getBytes("UTF-8");
                    return (byte[])o;
                }
                catch (final Exception o) {}
                catch (final PackageManager$NameNotFoundException ex) {}
                ((Throwable)o).printStackTrace();
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(40000, 0, 1.0f));
        this.c.a(hr1);
    }
    
    public final void h(final Object o) {
        final y01 d = this.d;
        if (d != null) {
            d.a(o);
        }
    }
}

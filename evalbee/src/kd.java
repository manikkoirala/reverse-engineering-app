import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.ArrayList;
import java.util.List;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class kd
{
    public Context a;
    public y01 b;
    public TextView c;
    public Spinner d;
    public List e;
    public ArrayList f;
    
    public kd(final Context a, final y01 b, final ArrayList list) {
        this.a = a;
        this.b = b;
        this.e = list;
        this.f = list;
        this.c();
    }
    
    public static /* synthetic */ Spinner a(final kd kd) {
        return kd.d;
    }
    
    public final ArrayList b(final List list) {
        final ArrayList list2 = new ArrayList();
        int i = 0;
    Label_0225_Outer:
        while (i < this.f.size()) {
            final String b = list.get(i).b();
            b.hashCode();
            final int hashCode = b.hashCode();
            int n = -1;
            switch (hashCode) {
                case 30765020: {
                    if (!b.equals("enterprise_20")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 30764994: {
                    if (!b.equals("enterprise_15")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 30764989: {
                    if (!b.equals("enterprise_10")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case 30764963: {
                    if (!b.equals("enterprise_05")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            int j = 5;
            while (true) {
                switch (n) {
                    default: {
                        j = j;
                    }
                    case 0: {
                        list2.add(j);
                        ++i;
                        continue Label_0225_Outer;
                    }
                    case 3: {
                        j = 20;
                        continue;
                    }
                    case 2: {
                        j = 15;
                        continue;
                    }
                    case 1: {
                        j = 10;
                        continue;
                    }
                }
                break;
            }
        }
        return list2;
    }
    
    public final void c() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492964, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886873);
        final PurchaseAccount r = a91.r(this.a);
        if (r != null && r.getStorageExpMinutes() - a91.n() > 43200) {
            materialAlertDialogBuilder.setTitle(2131886872);
        }
        this.c = (TextView)inflate.findViewById(2131297213);
        this.d = (Spinner)inflate.findViewById(2131297098);
        materialAlertDialogBuilder.setPositiveButton(2131886655, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final kd a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final kd a = this.a;
                final t81 t81 = a.e.get(((AdapterView)kd.a(a)).getSelectedItemPosition());
                final kd a2 = this.a;
                if (a2.f != null) {
                    a2.b.a(t81);
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final kd a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        this.d.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (List)this.b(this.e)));
        ((AdapterView)this.d).setSelection(0);
        this.c.setText((CharSequence)this.f.get(0).a().a());
        ((AdapterView)this.d).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final kd a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                final kd a = this.a;
                a.c.setText((CharSequence)((t81)a.f.get(index)).a().a());
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        materialAlertDialogBuilder.create().show();
    }
}

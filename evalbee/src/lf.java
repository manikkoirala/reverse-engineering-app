import android.view.View;
import android.graphics.drawable.Drawable;
import android.content.res.ColorStateList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class lf implements nf
{
    @Override
    public void a(final mf mf) {
        this.c(mf, this.n(mf));
    }
    
    @Override
    public float b(final mf mf) {
        return mf.e().getElevation();
    }
    
    @Override
    public void c(final mf mf, final float n) {
        this.p(mf).g(n, mf.a(), mf.d());
        this.g(mf);
    }
    
    @Override
    public void d(final mf mf, final float n) {
        this.p(mf).h(n);
    }
    
    @Override
    public float e(final mf mf) {
        return this.k(mf) * 2.0f;
    }
    
    @Override
    public void f(final mf mf) {
        this.c(mf, this.n(mf));
    }
    
    @Override
    public void g(final mf mf) {
        if (!mf.a()) {
            mf.setShadowPadding(0, 0, 0, 0);
            return;
        }
        final float n = this.n(mf);
        final float k = this.k(mf);
        final int n2 = (int)Math.ceil(vf1.a(n, k, mf.d()));
        final int n3 = (int)Math.ceil(vf1.b(n, k, mf.d()));
        mf.setShadowPadding(n2, n3, n2, n3);
    }
    
    @Override
    public float h(final mf mf) {
        return this.k(mf) * 2.0f;
    }
    
    @Override
    public void i(final mf mf, final Context context, final ColorStateList list, final float n, final float elevation, final float n2) {
        mf.b(new uf1(list, n));
        final View e = mf.e();
        e.setClipToOutline(true);
        e.setElevation(elevation);
        this.c(mf, n2);
    }
    
    @Override
    public void j(final mf mf, final ColorStateList list) {
        this.p(mf).f(list);
    }
    
    @Override
    public float k(final mf mf) {
        return this.p(mf).d();
    }
    
    @Override
    public ColorStateList l(final mf mf) {
        return this.p(mf).b();
    }
    
    @Override
    public void m(final mf mf, final float elevation) {
        mf.e().setElevation(elevation);
    }
    
    @Override
    public float n(final mf mf) {
        return this.p(mf).c();
    }
    
    @Override
    public void o() {
    }
    
    public final uf1 p(final mf mf) {
        return (uf1)mf.c();
    }
}

import java.util.NoSuchElementException;
import java.util.Queue;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class b80 extends t70 implements Queue
{
    @Override
    public abstract Queue delegate();
    
    @Override
    public Object element() {
        return this.delegate().element();
    }
    
    @Override
    public Object peek() {
        return this.delegate().peek();
    }
    
    @Override
    public Object poll() {
        return this.delegate().poll();
    }
    
    @Override
    public Object remove() {
        return this.delegate().remove();
    }
    
    public boolean standardOffer(final Object o) {
        try {
            return this.add(o);
        }
        catch (final IllegalStateException ex) {
            return false;
        }
    }
    
    public Object standardPeek() {
        try {
            return this.element();
        }
        catch (final NoSuchElementException ex) {
            return null;
        }
    }
    
    public Object standardPoll() {
        try {
            return this.remove();
        }
        catch (final NoSuchElementException ex) {
            return null;
        }
    }
}

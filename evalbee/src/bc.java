import android.graphics.Matrix;
import com.ekodroid.omrevaluator.templateui.models.NumericalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.DecimalOptionPayload;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.ByteArrayOutputStream;
import android.graphics.Bitmap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bc
{
    public static byte[] a(final Bitmap bitmap) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap$CompressFormat.JPEG, 100, (OutputStream)byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    public static DecimalOptionPayload b(final String s) {
        if (s == null) {
            return new DecimalOptionPayload(false, 2, false);
        }
        return (DecimalOptionPayload)new gc0().j(s, DecimalOptionPayload.class);
    }
    
    public static MatrixOptionPayload c(final String s) {
        if (s == null) {
            return new MatrixOptionPayload(4, 5);
        }
        return (MatrixOptionPayload)new gc0().j(s, MatrixOptionPayload.class);
    }
    
    public static NumericalOptionPayload d(final String s) {
        if (s == null) {
            return new NumericalOptionPayload(false, 1, 0);
        }
        return (NumericalOptionPayload)new gc0().j(s, NumericalOptionPayload.class);
    }
    
    public static Bitmap e(final Bitmap bitmap, final float n) {
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final Matrix matrix = new Matrix();
        matrix.postScale(n, n);
        final Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
        bitmap.recycle();
        return bitmap2;
    }
}

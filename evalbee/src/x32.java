import android.graphics.RectF;
import android.graphics.Matrix;
import android.view.ViewParent;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class x32
{
    public static final ThreadLocal a;
    public static final ThreadLocal b;
    
    static {
        a = new ThreadLocal();
        b = new ThreadLocal();
    }
    
    public static void a(final ViewGroup viewGroup, final View view, final Rect rect) {
        rect.set(0, 0, view.getWidth(), view.getHeight());
        c(viewGroup, view, rect);
    }
    
    public static void b(final ViewParent viewParent, final View view, final Matrix matrix) {
        final ViewParent parent = view.getParent();
        if (parent instanceof View && parent != viewParent) {
            final View view2 = (View)parent;
            b(viewParent, view2, matrix);
            matrix.preTranslate((float)(-view2.getScrollX()), (float)(-view2.getScrollY()));
        }
        matrix.preTranslate((float)view.getLeft(), (float)view.getTop());
        if (!view.getMatrix().isIdentity()) {
            matrix.preConcat(view.getMatrix());
        }
    }
    
    public static void c(final ViewGroup viewGroup, final View view, final Rect rect) {
        final ThreadLocal a = x32.a;
        Matrix value = a.get();
        if (value == null) {
            value = new Matrix();
            a.set(value);
        }
        else {
            value.reset();
        }
        b((ViewParent)viewGroup, view, value);
        final ThreadLocal b = x32.b;
        RectF value2;
        if ((value2 = b.get()) == null) {
            value2 = new RectF();
            b.set(value2);
        }
        value2.set(rect);
        value.mapRect(value2);
        rect.set((int)(value2.left + 0.5f), (int)(value2.top + 0.5f), (int)(value2.right + 0.5f), (int)(value2.bottom + 0.5f));
    }
}

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class of1
{
    public final List a;
    public final int b;
    
    public of1(final int b) {
        this.a = new ArrayList();
        this.b = b;
    }
    
    public List a() {
        final List b = this.b();
        final ArrayList list = new ArrayList();
        for (int i = 0; i < b.size(); ++i) {
            list.add(((nf1)b.get(i)).h());
        }
        return list;
    }
    
    public List b() {
        synchronized (this) {
            return Collections.unmodifiableList((List<?>)new ArrayList<Object>(this.a));
        }
    }
    
    public boolean c(List subList) {
        synchronized (this) {
            this.a.clear();
            if (subList.size() > this.b) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignored ");
                sb.append(0);
                sb.append(" entries when adding rollout assignments. Maximum allowable: ");
                sb.append(this.b);
                f.k(sb.toString());
                subList = subList.subList(0, this.b);
                return this.a.addAll(subList);
            }
            return this.a.addAll(subList);
        }
    }
}

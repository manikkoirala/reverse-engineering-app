import com.google.protobuf.a0;
import com.google.protobuf.d;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

// 
// Decompiled by Procyon v0.6.0
// 

public final class bq1 extends GeneratedMessageLite implements xv0
{
    public static final int CODE_FIELD_NUMBER = 1;
    private static final bq1 DEFAULT_INSTANCE;
    public static final int DETAILS_FIELD_NUMBER = 3;
    public static final int MESSAGE_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    private int code_;
    private t.e details_;
    private String message_;
    
    static {
        GeneratedMessageLite.V(bq1.class, DEFAULT_INSTANCE = new bq1());
    }
    
    public bq1() {
        this.message_ = "";
        this.details_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ bq1 Z() {
        return bq1.DEFAULT_INSTANCE;
    }
    
    public static bq1 b0() {
        return bq1.DEFAULT_INSTANCE;
    }
    
    public int a0() {
        return this.code_;
    }
    
    public String c0() {
        return this.message_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (bq1$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = bq1.PARSER) == null) {
                    synchronized (bq1.class) {
                        if (bq1.PARSER == null) {
                            bq1.PARSER = new GeneratedMessageLite.b(bq1.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return bq1.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(bq1.DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\u0004\u0002\u0208\u0003\u001b", new Object[] { "code_", "message_", "details_", d.class });
            }
            case 2: {
                return new b((bq1$a)null);
            }
            case 1: {
                return new bq1();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(bq1.Z());
        }
    }
}

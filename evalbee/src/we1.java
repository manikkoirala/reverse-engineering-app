import com.ekodroid.omrevaluator.templateui.models.Subject2;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.util.DataModels.SortResultList;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.Context;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class we1
{
    public final void a(final ArrayList list, final ArrayList list2, final int n, final int n2, final String s) {
        if (list2.size() < 1) {
            return;
        }
        if (list2.get(0).getRollNO() == n) {
            list2.remove(0);
            return;
        }
        if (list2.get(0).getRollNO() < n) {
            final String[] e = new String[n2];
            for (int i = 0; i < n2; ++i) {
                e[i] = "";
            }
            e[0] = s;
            final StringBuilder sb = new StringBuilder();
            sb.append(list2.get(0).getRollNO());
            sb.append("");
            e[2] = sb.toString();
            e[3] = list2.get(0).getStudentName();
            list.add(e);
            list2.remove(0);
            this.a(list, list2, n, n2, s);
        }
    }
    
    public final void b(final ArrayList list, final ArrayList list2, final int n, final String s) {
        for (int i = 0; i < list2.size(); ++i) {
            final String[] e = new String[n];
            for (int j = 0; j < n; ++j) {
                e[j] = "";
            }
            e[0] = s;
            final StringBuilder sb = new StringBuilder();
            sb.append(list2.get(i).getRollNO());
            sb.append("");
            e[2] = sb.toString();
            e[3] = list2.get(i).getStudentName();
            list.add(e);
        }
    }
    
    public String[][] c(final Context context, final ExamId examId, final ArrayList list, final SheetTemplate2 sheetTemplate2, final boolean b) {
        if (b) {
            return this.e(context, examId, list, sheetTemplate2);
        }
        LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        final int rollDigits = sheetTemplate2.getTemplateParams().getRollDigits();
        final ArrayList list2 = new ArrayList();
        list2.add(this.f(list.get(0).a(), sheetTemplate2));
        final AnswerSetKey[] answerKeys = sheetTemplate2.getAnswerKeys();
        for (int i = 0; i < list.size(); ++i) {
            final d8 d8 = list.get(i);
            final int g = d8.b().g();
            final ResultDataJsonModel a = d8.a();
            if (a != null) {
                final ResultItem resultItem = a.getResultItem(sheetTemplate2);
                final int g2 = this.g(sheetTemplate2.getTemplateParams().getSubjects());
                final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
                final double[][] k = ve1.k(resultItem, sheetTemplate2);
                final String[] e = new String[marksForEachSubject.length + 10 + g2 + resultItem.getAnswerValue2s().size() * 3];
                e[0] = sheetTemplate2.getName();
                e[1] = "";
                if (resultItem.getExamSet() > 0) {
                    e[1] = labelProfile.getExamSetLabels()[resultItem.getExamSet() - 1];
                }
                e[2] = this.h(g, rollDigits);
                e[3] = this.i(context, g, examId.getClassName());
                final StringBuilder sb = new StringBuilder();
                sb.append(ve1.o(d8.b().d()));
                sb.append("");
                e[4] = sb.toString();
                e[5] = d8.b().b();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(d8.b().f());
                sb2.append("");
                e[6] = sb2.toString();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(d8.b().a());
                sb3.append("");
                e[7] = sb3.toString();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(d8.b().c());
                sb4.append("");
                e[8] = sb4.toString();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(d8.b().h());
                sb5.append("");
                e[9] = sb5.toString();
                int j = 0;
                int n = 0;
                while (j < marksForEachSubject.length) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append(ve1.o(marksForEachSubject[j]));
                    sb6.append("");
                    e[n + 10] = sb6.toString();
                    int n2 = ++n;
                    LabelProfile labelProfile2 = labelProfile;
                    if (k[j].length > 1) {
                        int n3 = 0;
                        while (true) {
                            n = n2;
                            labelProfile2 = labelProfile;
                            if (n3 >= k[j].length) {
                                break;
                            }
                            final StringBuilder sb7 = new StringBuilder();
                            sb7.append(ve1.o(k[j][n3]));
                            sb7.append("");
                            e[n2 + 10] = sb7.toString();
                            ++n2;
                            ++n3;
                        }
                    }
                    ++j;
                    labelProfile = labelProfile2;
                }
                for (int l = 0; l < resultItem.getAnswerValue2s().size(); ++l) {
                    final AnswerValue answerValue = resultItem.getAnswerValue2s().get(l);
                    final String r = e5.r(answerValue, sheetTemplate2.getLabelProfile());
                    String q;
                    if (answerKeys != null && answerKeys.length > 0) {
                        int n4;
                        if (resultItem.getExamSet() > 0) {
                            n4 = resultItem.getExamSet() - 1;
                        }
                        else {
                            n4 = 0;
                        }
                        q = e5.q(answerKeys[n4].getAnswerOptionKeys().get(l), sheetTemplate2.getLabelProfile());
                    }
                    else {
                        q = "";
                    }
                    final int length = marksForEachSubject.length;
                    final int n5 = l * 3;
                    e[length + 10 + g2 + n5] = r;
                    e[marksForEachSubject.length + 11 + g2 + n5] = q;
                    final int length2 = marksForEachSubject.length;
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(answerValue.getMarksForAnswer());
                    sb8.append("");
                    e[length2 + 12 + g2 + n5] = sb8.toString();
                }
                list2.add(e);
            }
        }
        return list2.toArray(new String[list2.size()][]);
    }
    
    public String[][] d(final Context context, final ExamId examId, final ArrayList list, final SheetTemplate2 sheetTemplate2, final boolean b) {
        final ArrayList list2 = new ArrayList();
        final ResultRepository instance = ResultRepository.getInstance(context);
        for (final ye1 ye1 : list) {
            list2.add(new d8(instance.getResult(examId, ye1.g()), ye1));
        }
        return this.c(context, examId, list2, sheetTemplate2, b);
    }
    
    public final String[][] e(final Context context, final ExamId examId, final ArrayList list, final SheetTemplate2 sheetTemplate2) {
        new ArrayList();
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next());
        }
        int rollDigits = sheetTemplate2.getTemplateParams().getRollDigits();
        LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        final ArrayList list3 = new ArrayList();
        list3.add(this.f(list2.get(0).a(), sheetTemplate2));
        final ArrayList<StudentDataModel> students = ClassRepository.getInstance(context).getStudents(examId.getClassName());
        new SortResultList().a(SortResultList.ResultSortBy.ROLLNO, list2, true);
        final AnswerSetKey[] answerKeys = sheetTemplate2.getAnswerKeys();
        int n = 0;
        for (int i = 0; i < list2.size(); ++i) {
            final d8 d8 = list2.get(i);
            final int g = d8.b().g();
            final ResultDataJsonModel a = d8.a();
            if (a == null) {
                o4.c(FirebaseAnalytics.getInstance(context), "RA_EE_RESULT_NULL", "");
            }
            else {
                final ResultItem resultItem = a.getResultItem(sheetTemplate2);
                final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
                final double[][] k = ve1.k(resultItem, sheetTemplate2);
                final int g2 = this.g(sheetTemplate2.getTemplateParams().getSubjects());
                final int n2 = marksForEachSubject.length + 10 + g2 + resultItem.getAnswerValue2s().size() * 3;
                final String[] e = new String[n2];
                this.a(list3, students, g, n2, sheetTemplate2.getName());
                e[0] = sheetTemplate2.getName();
                e[1] = "";
                if (resultItem.getExamSet() > 0) {
                    e[1] = labelProfile.getExamSetLabels()[resultItem.getExamSet() - 1];
                }
                e[2] = this.h(g, rollDigits);
                e[3] = this.i(context, g, examId.getClassName());
                final StringBuilder sb = new StringBuilder();
                sb.append(ve1.o(d8.b().d()));
                sb.append("");
                e[4] = sb.toString();
                e[5] = d8.b().b();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(d8.b().f());
                sb2.append("");
                e[6] = sb2.toString();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(d8.b().a());
                sb3.append("");
                e[7] = sb3.toString();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(d8.b().c());
                sb4.append("");
                e[8] = sb4.toString();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(d8.b().h());
                sb5.append("");
                e[9] = sb5.toString();
                int j = 0;
                int n3 = 0;
                while (j < marksForEachSubject.length) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append(ve1.o(marksForEachSubject[j]));
                    sb6.append("");
                    e[n3 + 10] = sb6.toString();
                    int n4 = ++n3;
                    int n5 = rollDigits;
                    LabelProfile labelProfile2 = labelProfile;
                    if (k[j].length > 1) {
                        int n6 = 0;
                        while (true) {
                            n3 = n4;
                            n5 = rollDigits;
                            labelProfile2 = labelProfile;
                            if (n6 >= k[j].length) {
                                break;
                            }
                            final StringBuilder sb7 = new StringBuilder();
                            sb7.append(ve1.o(k[j][n6]));
                            sb7.append("");
                            e[n4 + 10] = sb7.toString();
                            ++n4;
                            ++n6;
                        }
                    }
                    ++j;
                    rollDigits = n5;
                    labelProfile = labelProfile2;
                }
                for (int l = 0; l < resultItem.getAnswerValue2s().size(); ++l) {
                    final AnswerValue answerValue = resultItem.getAnswerValue2s().get(l);
                    final String r = e5.r(answerValue, sheetTemplate2.getLabelProfile());
                    String q;
                    if (answerKeys != null && answerKeys.length > 0) {
                        int n7;
                        if (resultItem.getExamSet() > 0) {
                            n7 = resultItem.getExamSet() - 1;
                        }
                        else {
                            n7 = 0;
                        }
                        q = e5.q(answerKeys[n7].getAnswerOptionKeys().get(l), sheetTemplate2.getLabelProfile());
                    }
                    else {
                        q = "";
                    }
                    final int length = marksForEachSubject.length;
                    final int n8 = l * 3;
                    e[length + 10 + g2 + n8] = r;
                    e[marksForEachSubject.length + 11 + g2 + n8] = q;
                    final int length2 = marksForEachSubject.length;
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(answerValue.getMarksForAnswer());
                    sb8.append("");
                    e[length2 + 12 + g2 + n8] = sb8.toString();
                }
                list3.add(e);
                n = n2;
            }
        }
        if (students.size() > 0 && n > 0) {
            this.b(list3, students, n, sheetTemplate2.getName());
        }
        return list3.toArray(new String[list3.size()][]);
    }
    
    public final String[] f(final ResultDataJsonModel resultDataJsonModel, final SheetTemplate2 sheetTemplate2) {
        final LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        final TemplateParams2 templateParams = sheetTemplate2.getTemplateParams();
        final int length = templateParams.getSubjects().length;
        final int g = this.g(templateParams.getSubjects());
        final int n = length + 10 + g;
        final String[] array = new String[resultDataJsonModel.getResultItem(sheetTemplate2).getAnswerValue2s().size() * 3 + n];
        final String examNameString = labelProfile.getExamNameString();
        final int n2 = 0;
        array[0] = examNameString;
        array[1] = labelProfile.getExamSetString();
        array[2] = labelProfile.getRollNoString();
        array[3] = labelProfile.getNameString();
        array[4] = labelProfile.getTotalMarksString();
        array[5] = labelProfile.getGradeString();
        array[6] = labelProfile.getRankString();
        array[7] = labelProfile.getCorrectAnswerString();
        array[8] = labelProfile.getIncorrectAnswerString();
        array[9] = "Not attempted";
        int n3 = 0;
        int n4 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= length) {
                break;
            }
            array[n4 + 10] = templateParams.getSubjects()[n3].getSubName();
            int n5 = ++n4;
            if (templateParams.getSubjects()[n3].getSections().length > 1) {
                int n6 = 0;
                while (true) {
                    n5 = n4;
                    if (n6 >= templateParams.getSubjects()[n3].getSections().length) {
                        break;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append(templateParams.getSubjects()[n3].getSubName());
                    sb.append("-");
                    sb.append(templateParams.getSubjects()[n3].getSections()[n6].getName());
                    array[n4 + 10] = sb.toString();
                    ++n4;
                    ++n6;
                }
            }
            ++n3;
            n4 = n5;
        }
        while (i < resultDataJsonModel.getResultItem(sheetTemplate2).getAnswerValue2s().size()) {
            final int n7 = i * 3;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Q ");
            ++i;
            sb2.append(i);
            sb2.append(" Options");
            array[n + n7] = sb2.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Q ");
            sb3.append(i);
            sb3.append(" Key");
            array[length + 11 + g + n7] = sb3.toString();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Q ");
            sb4.append(i);
            sb4.append(" Marks");
            array[length + 12 + g + n7] = sb4.toString();
        }
        return array;
    }
    
    public final int g(final Subject2[] array) {
        final int length = array.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final Subject2 subject2 = array[i];
            int n2 = n;
            if (subject2.getSections().length > 1) {
                n2 = n + subject2.getSections().length;
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public final String h(int i, final int n) {
        final StringBuilder sb = new StringBuilder();
        final String s = "";
        sb.append("");
        sb.append(i);
        String s2;
        final String str = s2 = sb.toString();
        if (n > str.length()) {
            i = 0;
            String string = s;
            while (i < n - str.length()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append("0");
                string = sb2.toString();
                ++i;
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(str);
            s2 = sb3.toString();
        }
        return s2;
    }
    
    public final String i(final Context context, final int n, final String s) {
        final StudentDataModel student = ClassRepository.getInstance(context).getStudent(n, s);
        if (student != null) {
            return student.getStudentName();
        }
        return " ";
    }
}

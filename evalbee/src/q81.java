import android.content.Intent;
import java.util.concurrent.Callable;
import androidx.work.WorkerParameters;
import java.util.concurrent.ExecutionException;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Map;
import androidx.work.impl.WorkDatabase;
import androidx.work.a;
import android.content.Context;
import android.os.PowerManager$WakeLock;

// 
// Decompiled by Procyon v0.6.0
// 

public class q81 implements q70
{
    public static final String l;
    public PowerManager$WakeLock a;
    public Context b;
    public a c;
    public hu1 d;
    public WorkDatabase e;
    public Map f;
    public Map g;
    public Map h;
    public Set i;
    public final List j;
    public final Object k;
    
    static {
        l = xl0.i("Processor");
    }
    
    public q81(final Context b, final a c, final hu1 d, final WorkDatabase e) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.g = new HashMap();
        this.f = new HashMap();
        this.i = new HashSet();
        this.j = new ArrayList();
        this.a = null;
        this.k = new Object();
        this.h = new HashMap();
    }
    
    public static boolean i(final String s, final ca2 ca2, final int n) {
        if (ca2 != null) {
            ca2.g(n);
            final xl0 e = xl0.e();
            final String l = q81.l;
            final StringBuilder sb = new StringBuilder();
            sb.append("WorkerWrapper interrupted for ");
            sb.append(s);
            e.a(l, sb.toString());
            return true;
        }
        final xl0 e2 = xl0.e();
        final String i = q81.l;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("WorkerWrapper could not be found for ");
        sb2.append(s);
        e2.a(i, sb2.toString());
        return false;
    }
    
    @Override
    public void a(final String str, final p70 p2) {
        synchronized (this.k) {
            final xl0 e = xl0.e();
            final String l = q81.l;
            final StringBuilder sb = new StringBuilder();
            sb.append("Moving WorkSpec (");
            sb.append(str);
            sb.append(") to the foreground");
            e.f(l, sb.toString());
            final ca2 ca2 = this.g.remove(str);
            if (ca2 != null) {
                if (this.a == null) {
                    (this.a = s52.b(this.b, "ProcessorForegroundLck")).acquire();
                }
                this.f.put(str, ca2);
                sl.startForegroundService(this.b, androidx.work.impl.foreground.a.f(this.b, ca2.d(), p2));
            }
        }
    }
    
    public void e(final qy qy) {
        synchronized (this.k) {
            this.j.add(qy);
        }
    }
    
    public final ca2 f(final String s) {
        ca2 ca2 = this.f.remove(s);
        final boolean b = ca2 != null;
        if (!b) {
            ca2 = this.g.remove(s);
        }
        this.h.remove(s);
        if (b) {
            this.u();
        }
        return ca2;
    }
    
    public p92 g(final String s) {
        synchronized (this.k) {
            final ca2 h = this.h(s);
            if (h != null) {
                return h.e();
            }
            return null;
        }
    }
    
    public final ca2 h(final String s) {
        ca2 ca2;
        if ((ca2 = this.f.get(s)) == null) {
            ca2 = this.g.get(s);
        }
        return ca2;
    }
    
    public boolean j(final String s) {
        synchronized (this.k) {
            return this.i.contains(s);
        }
    }
    
    public boolean k(final String s) {
        synchronized (this.k) {
            return this.h(s) != null;
        }
    }
    
    public final void o(final ca2 ca2, final boolean b) {
        synchronized (this.k) {
            final x82 d = ca2.d();
            final String b2 = d.b();
            if (this.h(b2) == ca2) {
                this.f(b2);
            }
            final xl0 e = xl0.e();
            final String l = q81.l;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getClass().getSimpleName());
            sb.append(" ");
            sb.append(b2);
            sb.append(" executed; reschedule = ");
            sb.append(b);
            e.a(l, sb.toString());
            final Iterator iterator = this.j.iterator();
            while (iterator.hasNext()) {
                ((qy)iterator.next()).d(d, b);
            }
        }
    }
    
    public void p(final qy qy) {
        synchronized (this.k) {
            this.j.remove(qy);
        }
    }
    
    public final void q(final x82 x82, final boolean b) {
        this.d.c().execute(new p81(this, x82, b));
    }
    
    public boolean r(final op1 op1) {
        return this.s(op1, null);
    }
    
    public boolean s(final op1 e, final WorkerParameters.a a) {
        final x82 a2 = e.a();
        final String b = a2.b();
        final ArrayList list = new ArrayList();
        final p92 p2 = (p92)this.e.C(new n81(this, list, b));
        if (p2 == null) {
            final xl0 e2 = xl0.e();
            final String l = q81.l;
            final StringBuilder sb = new StringBuilder();
            sb.append("Didn't find WorkSpec for id ");
            sb.append(a2);
            e2.k(l, sb.toString());
            this.q(a2, false);
            return false;
        }
        Object k = this.k;
        synchronized (k) {
            if (this.k(b)) {
                final Set set = this.h.get(b);
                if (((op1)set.iterator().next()).a().a() == a2.a()) {
                    set.add(e);
                    final xl0 e3 = xl0.e();
                    final String i = q81.l;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Work ");
                    sb2.append(a2);
                    sb2.append(" is already enqueued for processing");
                    e3.a(i, sb2.toString());
                }
                else {
                    this.q(a2, false);
                }
                return false;
            }
            if (p2.f() != a2.a()) {
                this.q(a2, false);
                return false;
            }
            final ca2 b2 = new ca2.c(this.b, this.c, this.d, this, this.e, p2, list).c(a).b();
            final ik0 c = b2.c();
            c.addListener(new o81(this, c, b2), this.d.c());
            this.g.put(b, b2);
            final HashSet<op1> set2 = new HashSet<op1>();
            set2.add(e);
            this.h.put(b, set2);
            monitorexit(k);
            this.d.d().execute(b2);
            final xl0 e4 = xl0.e();
            final String j = q81.l;
            k = new StringBuilder();
            ((StringBuilder)k).append(this.getClass().getSimpleName());
            ((StringBuilder)k).append(": processing ");
            ((StringBuilder)k).append(a2);
            e4.a(j, ((StringBuilder)k).toString());
            return true;
        }
    }
    
    public boolean t(final String str, final int n) {
        synchronized (this.k) {
            final xl0 e = xl0.e();
            final String l = q81.l;
            final StringBuilder sb = new StringBuilder();
            sb.append("Processor cancelling ");
            sb.append(str);
            e.a(l, sb.toString());
            this.i.add(str);
            final ca2 f = this.f(str);
            monitorexit(this.k);
            return i(str, f, n);
        }
    }
    
    public final void u() {
        synchronized (this.k) {
            if (!(this.f.isEmpty() ^ true)) {
                final Intent g = androidx.work.impl.foreground.a.g(this.b);
                try {
                    this.b.startService(g);
                }
                finally {
                    final Throwable t;
                    xl0.e().d(q81.l, "Unable to stop foreground service", t);
                }
                final PowerManager$WakeLock a = this.a;
                if (a != null) {
                    a.release();
                    this.a = null;
                }
            }
        }
    }
    
    public boolean v(final op1 op1, final int n) {
        final String b = op1.a().b();
        synchronized (this.k) {
            final ca2 f = this.f(b);
            monitorexit(this.k);
            return i(b, f, n);
        }
    }
    
    public boolean w(final op1 op1, final int n) {
        final String b = op1.a().b();
        synchronized (this.k) {
            if (this.f.get(b) != null) {
                final xl0 e = xl0.e();
                final String l = q81.l;
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignored stopWork. WorkerWrapper ");
                sb.append(b);
                sb.append(" is in foreground");
                e.a(l, sb.toString());
                return false;
            }
            final Set set = this.h.get(b);
            if (set != null && set.contains(op1)) {
                final ca2 f = this.f(b);
                monitorexit(this.k);
                return i(b, f, n);
            }
            return false;
        }
    }
}

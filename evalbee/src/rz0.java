import android.os.BaseBundle;
import java.util.Iterator;
import android.app.Notification$InboxStyle;
import android.media.AudioAttributes$Builder;
import android.net.Uri;
import java.util.ArrayList;
import android.app.Notification$BubbleMetadata;
import android.app.Notification$BigTextStyle;
import android.widget.RemoteViews;
import android.graphics.drawable.Icon;
import android.content.Context;
import android.graphics.Bitmap;
import android.app.Notification$BigPictureStyle;
import android.os.Build$VERSION;
import android.content.res.Resources;
import android.app.PendingIntent;
import androidx.core.graphics.drawable.IconCompat;
import android.os.Bundle;
import android.app.Notification;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rz0
{
    public static Bundle a(final Notification notification) {
        return notification.extras;
    }
    
    public static class a
    {
        public final Bundle a;
        public IconCompat b;
        public final kd1[] c;
        public final kd1[] d;
        public boolean e;
        public boolean f;
        public final int g;
        public final boolean h;
        public int i;
        public CharSequence j;
        public PendingIntent k;
        public boolean l;
        
        public a(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
            IconCompat c = null;
            if (n != 0) {
                c = IconCompat.c(null, "", n);
            }
            this(c, charSequence, pendingIntent);
        }
        
        public a(final IconCompat iconCompat, final CharSequence charSequence, final PendingIntent pendingIntent) {
            this(iconCompat, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false, false);
        }
        
        public a(final IconCompat b, final CharSequence charSequence, final PendingIntent k, Bundle a, final kd1[] c, final kd1[] d, final boolean e, final int g, final boolean f, final boolean h, final boolean l) {
            this.f = true;
            this.b = b;
            if (b != null && b.g() == 2) {
                this.i = b.e();
            }
            this.j = e.d(charSequence);
            this.k = k;
            if (a == null) {
                a = new Bundle();
            }
            this.a = a;
            this.c = c;
            this.d = d;
            this.e = e;
            this.g = g;
            this.f = f;
            this.h = h;
            this.l = l;
        }
        
        public PendingIntent a() {
            return this.k;
        }
        
        public boolean b() {
            return this.e;
        }
        
        public Bundle c() {
            return this.a;
        }
        
        public IconCompat d() {
            if (this.b == null) {
                final int i = this.i;
                if (i != 0) {
                    this.b = IconCompat.c(null, "", i);
                }
            }
            return this.b;
        }
        
        public kd1[] e() {
            return this.c;
        }
        
        public int f() {
            return this.g;
        }
        
        public boolean g() {
            return this.f;
        }
        
        public CharSequence h() {
            return this.j;
        }
        
        public boolean i() {
            return this.l;
        }
        
        public boolean j() {
            return this.h;
        }
    }
    
    public static class b extends g
    {
        public IconCompat e;
        public IconCompat f;
        public boolean g;
        public CharSequence h;
        public boolean i;
        
        @Override
        public void b(final qz0 qz0) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            final Notification$BigPictureStyle setBigContentTitle = new Notification$BigPictureStyle(qz0.a()).setBigContentTitle(super.b);
            final IconCompat e = this.e;
            Context f = null;
            Notification$BigPictureStyle bigPicture = setBigContentTitle;
            if (e != null) {
                if (sdk_INT >= 31) {
                    Context f2;
                    if (qz0 instanceof g01) {
                        f2 = ((g01)qz0).f();
                    }
                    else {
                        f2 = null;
                    }
                    c.a(setBigContentTitle, this.e.m(f2));
                    bigPicture = setBigContentTitle;
                }
                else {
                    bigPicture = setBigContentTitle;
                    if (e.g() == 1) {
                        bigPicture = setBigContentTitle.bigPicture(this.e.d());
                    }
                }
            }
            if (this.g) {
                if (this.f == null) {
                    a.a(bigPicture, null);
                }
                else {
                    if (qz0 instanceof g01) {
                        f = ((g01)qz0).f();
                    }
                    rz0.b.b.a(bigPicture, this.f.m(f));
                }
            }
            if (super.d) {
                a.b(bigPicture, super.c);
            }
            if (sdk_INT >= 31) {
                c.c(bigPicture, this.i);
                c.b(bigPicture, this.h);
            }
        }
        
        @Override
        public String c() {
            return "androidx.core.app.NotificationCompat$BigPictureStyle";
        }
        
        public rz0.b h(final Bitmap bitmap) {
            IconCompat b;
            if (bitmap == null) {
                b = null;
            }
            else {
                b = IconCompat.b(bitmap);
            }
            this.f = b;
            this.g = true;
            return this;
        }
        
        public rz0.b i(final Bitmap bitmap) {
            IconCompat b;
            if (bitmap == null) {
                b = null;
            }
            else {
                b = IconCompat.b(bitmap);
            }
            this.e = b;
            return this;
        }
        
        public abstract static class a
        {
            public static void a(final Notification$BigPictureStyle notification$BigPictureStyle, final Bitmap bitmap) {
                notification$BigPictureStyle.bigLargeIcon(bitmap);
            }
            
            public static void b(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence summaryText) {
                notification$BigPictureStyle.setSummaryText(summaryText);
            }
        }
        
        public abstract static class b
        {
            public static void a(final Notification$BigPictureStyle notification$BigPictureStyle, final Icon icon) {
                notification$BigPictureStyle.bigLargeIcon(icon);
            }
        }
        
        public abstract static class c
        {
            public static void a(final Notification$BigPictureStyle notification$BigPictureStyle, final Icon icon) {
                notification$BigPictureStyle.bigPicture(icon);
            }
            
            public static void b(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence contentDescription) {
                notification$BigPictureStyle.setContentDescription(contentDescription);
            }
            
            public static void c(final Notification$BigPictureStyle notification$BigPictureStyle, final boolean b) {
                notification$BigPictureStyle.showBigPictureWhenCollapsed(b);
            }
        }
    }
    
    public abstract static class g
    {
        public e a;
        public CharSequence b;
        public CharSequence c;
        public boolean d;
        
        public g() {
            this.d = false;
        }
        
        public void a(final Bundle bundle) {
            if (this.d) {
                bundle.putCharSequence("android.summaryText", this.c);
            }
            final CharSequence b = this.b;
            if (b != null) {
                bundle.putCharSequence("android.title.big", b);
            }
            final String c = this.c();
            if (c != null) {
                ((BaseBundle)bundle).putString("androidx.core.app.extra.COMPAT_TEMPLATE", c);
            }
        }
        
        public abstract void b(final qz0 p0);
        
        public abstract String c();
        
        public RemoteViews d(final qz0 qz0) {
            return null;
        }
        
        public RemoteViews e(final qz0 qz0) {
            return null;
        }
        
        public RemoteViews f(final qz0 qz0) {
            return null;
        }
        
        public void g(final e a) {
            if (this.a != a && (this.a = a) != null) {
                a.x(this);
            }
        }
    }
    
    public static class c extends g
    {
        public CharSequence e;
        
        @Override
        public void a(final Bundle bundle) {
            super.a(bundle);
        }
        
        @Override
        public void b(final qz0 qz0) {
            final Notification$BigTextStyle bigText = new Notification$BigTextStyle(qz0.a()).setBigContentTitle(super.b).bigText(this.e);
            if (super.d) {
                bigText.setSummaryText(super.c);
            }
        }
        
        @Override
        public String c() {
            return "androidx.core.app.NotificationCompat$BigTextStyle";
        }
        
        public c h(final CharSequence charSequence) {
            this.e = rz0.e.d(charSequence);
            return this;
        }
    }
    
    public abstract static final class d
    {
        public static Notification$BubbleMetadata a(final d d) {
            return null;
        }
    }
    
    public static class e
    {
        public boolean A;
        public boolean B;
        public String C;
        public Bundle D;
        public int E;
        public int F;
        public Notification G;
        public RemoteViews H;
        public RemoteViews I;
        public RemoteViews J;
        public String K;
        public int L;
        public String M;
        public long N;
        public int O;
        public int P;
        public boolean Q;
        public Notification R;
        public boolean S;
        public Icon T;
        public ArrayList U;
        public Context a;
        public ArrayList b;
        public ArrayList c;
        public ArrayList d;
        public CharSequence e;
        public CharSequence f;
        public PendingIntent g;
        public PendingIntent h;
        public RemoteViews i;
        public Bitmap j;
        public CharSequence k;
        public int l;
        public int m;
        public boolean n;
        public boolean o;
        public g p;
        public CharSequence q;
        public CharSequence r;
        public CharSequence[] s;
        public int t;
        public int u;
        public boolean v;
        public String w;
        public boolean x;
        public String y;
        public boolean z;
        
        public e(final Context context) {
            this(context, null);
        }
        
        public e(final Context a, final String k) {
            this.b = new ArrayList();
            this.c = new ArrayList();
            this.d = new ArrayList();
            this.n = true;
            this.z = false;
            this.E = 0;
            this.F = 0;
            this.L = 0;
            this.O = 0;
            this.P = 0;
            final Notification r = new Notification();
            this.R = r;
            this.a = a;
            this.K = k;
            r.when = System.currentTimeMillis();
            this.R.audioStreamType = -1;
            this.m = 0;
            this.U = new ArrayList();
            this.Q = true;
        }
        
        public static CharSequence d(final CharSequence charSequence) {
            if (charSequence == null) {
                return charSequence;
            }
            CharSequence subSequence = charSequence;
            if (charSequence.length() > 5120) {
                subSequence = charSequence.subSequence(0, 5120);
            }
            return subSequence;
        }
        
        public e A(final int f) {
            this.F = f;
            return this;
        }
        
        public e B(final long when) {
            this.R.when = when;
            return this;
        }
        
        public e a(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
            this.b.add(new rz0.a(n, charSequence, pendingIntent));
            return this;
        }
        
        public Notification b() {
            return new g01(this).c();
        }
        
        public Bundle c() {
            if (this.D == null) {
                this.D = new Bundle();
            }
            return this.D;
        }
        
        public final Bitmap e(final Bitmap bitmap) {
            Bitmap scaledBitmap = bitmap;
            if (bitmap != null) {
                if (Build$VERSION.SDK_INT >= 27) {
                    scaledBitmap = bitmap;
                }
                else {
                    final Resources resources = this.a.getResources();
                    final int dimensionPixelSize = resources.getDimensionPixelSize(ya1.b);
                    final int dimensionPixelSize2 = resources.getDimensionPixelSize(ya1.a);
                    if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                        return bitmap;
                    }
                    final double min = Math.min(dimensionPixelSize / (double)Math.max(1, bitmap.getWidth()), dimensionPixelSize2 / (double)Math.max(1, bitmap.getHeight()));
                    scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int)Math.ceil(bitmap.getWidth() * min), (int)Math.ceil(bitmap.getHeight() * min), true);
                }
            }
            return scaledBitmap;
        }
        
        public e f(final boolean b) {
            this.n(16, b);
            return this;
        }
        
        public e g(final String k) {
            this.K = k;
            return this;
        }
        
        public e h(final int e) {
            this.E = e;
            return this;
        }
        
        public e i(final PendingIntent g) {
            this.g = g;
            return this;
        }
        
        public e j(final CharSequence charSequence) {
            this.f = d(charSequence);
            return this;
        }
        
        public e k(final CharSequence charSequence) {
            this.e = d(charSequence);
            return this;
        }
        
        public e l(final int defaults) {
            final Notification r = this.R;
            r.defaults = defaults;
            if ((defaults & 0x4) != 0x0) {
                r.flags |= 0x1;
            }
            return this;
        }
        
        public e m(final PendingIntent deleteIntent) {
            this.R.deleteIntent = deleteIntent;
            return this;
        }
        
        public final void n(int flags, final boolean b) {
            Notification notification;
            if (b) {
                notification = this.R;
                flags |= notification.flags;
            }
            else {
                notification = this.R;
                flags = (~flags & notification.flags);
            }
            notification.flags = flags;
        }
        
        public e o(final Bitmap bitmap) {
            this.j = this.e(bitmap);
            return this;
        }
        
        public e p(int ledARGB, final int ledOnMS, final int ledOffMS) {
            final Notification r = this.R;
            r.ledARGB = ledARGB;
            r.ledOnMS = ledOnMS;
            r.ledOffMS = ledOffMS;
            if (ledOnMS != 0 && ledOffMS != 0) {
                ledARGB = 1;
            }
            else {
                ledARGB = 0;
            }
            r.flags = (ledARGB | (r.flags & 0xFFFFFFFE));
            return this;
        }
        
        public e q(final boolean z) {
            this.z = z;
            return this;
        }
        
        public e r(final int l) {
            this.l = l;
            return this;
        }
        
        public e s(final int m) {
            this.m = m;
            return this;
        }
        
        public e t(final int t, final int u, final boolean v) {
            this.t = t;
            this.u = u;
            this.v = v;
            return this;
        }
        
        public e u(final boolean n) {
            this.n = n;
            return this;
        }
        
        public e v(final int icon) {
            this.R.icon = icon;
            return this;
        }
        
        public e w(final Uri sound) {
            final Notification r = this.R;
            r.sound = sound;
            r.audioStreamType = -1;
            r.audioAttributes = new AudioAttributes$Builder().setContentType(4).setUsage(5).build();
            return this;
        }
        
        public e x(final g p) {
            if (this.p != p && (this.p = p) != null) {
                p.g(this);
            }
            return this;
        }
        
        public e y(final CharSequence charSequence) {
            this.R.tickerText = d(charSequence);
            return this;
        }
        
        public e z(final long[] vibrate) {
            this.R.vibrate = vibrate;
            return this;
        }
    }
    
    public static class f extends g
    {
        public ArrayList e;
        
        public f() {
            this.e = new ArrayList();
        }
        
        @Override
        public void b(final qz0 qz0) {
            final Notification$InboxStyle setBigContentTitle = new Notification$InboxStyle(qz0.a()).setBigContentTitle(super.b);
            if (super.d) {
                setBigContentTitle.setSummaryText(super.c);
            }
            final Iterator iterator = this.e.iterator();
            while (iterator.hasNext()) {
                setBigContentTitle.addLine((CharSequence)iterator.next());
            }
        }
        
        @Override
        public String c() {
            return "androidx.core.app.NotificationCompat$InboxStyle";
        }
        
        public f h(final CharSequence charSequence) {
            if (charSequence != null) {
                this.e.add(rz0.e.d(charSequence));
            }
            return this;
        }
        
        public f i(final CharSequence charSequence) {
            super.c = rz0.e.d(charSequence);
            super.d = true;
            return this;
        }
    }
}

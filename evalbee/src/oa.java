// 
// Decompiled by Procyon v0.6.0
// 

public final class oa extends k21
{
    public final int a;
    public final wx0 b;
    
    public oa(final int a, final wx0 b) {
        this.a = a;
        if (b != null) {
            this.b = b;
            return;
        }
        throw new NullPointerException("Null mutation");
    }
    
    @Override
    public int c() {
        return this.a;
    }
    
    @Override
    public wx0 d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof k21) {
            final k21 k21 = (k21)o;
            if (this.a != k21.c() || !this.b.equals(k21.d())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (this.a ^ 0xF4243) * 1000003 ^ this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Overlay{largestBatchId=");
        sb.append(this.a);
        sb.append(", mutation=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}

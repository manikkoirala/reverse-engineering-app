import java.util.concurrent.ExecutorService;
import com.google.android.gms.common.internal.Preconditions;
import java.net.URLConnection;
import java.io.InputStream;
import java.io.IOException;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import java.net.MalformedURLException;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Future;
import java.net.URL;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public class ie0 implements Closeable
{
    public final URL a;
    public volatile Future b;
    public Task c;
    
    public ie0(final URL a) {
        this.a = a;
    }
    
    public static ie0 d(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            return new ie0(new URL(s));
        }
        catch (final MalformedURLException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Not downloading image, bad URL: ");
            sb.append(s);
            Log.w("FirebaseMessaging", sb.toString());
            return null;
        }
    }
    
    public Bitmap b() {
        if (Log.isLoggable("FirebaseMessaging", 4)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Starting download of: ");
            sb.append(this.a);
            Log.i("FirebaseMessaging", sb.toString());
        }
        final byte[] c = this.c();
        final Bitmap decodeByteArray = BitmapFactory.decodeByteArray(c, 0, c.length);
        if (decodeByteArray != null) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Successfully downloaded image: ");
                sb2.append(this.a);
                Log.d("FirebaseMessaging", sb2.toString());
            }
            return decodeByteArray;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Failed to decode image: ");
        sb3.append(this.a);
        throw new IOException(sb3.toString());
    }
    
    public final byte[] c() {
        final URLConnection openConnection = this.a.openConnection();
        if (openConnection.getContentLength() <= 1048576) {
            Object inputStream = openConnection.getInputStream();
            try {
                final byte[] d = sd.d(sd.b((InputStream)inputStream, 1048577L));
                if (inputStream != null) {
                    ((InputStream)inputStream).close();
                }
                if (Log.isLoggable("FirebaseMessaging", 2)) {
                    inputStream = new StringBuilder();
                    ((StringBuilder)inputStream).append("Downloaded ");
                    ((StringBuilder)inputStream).append(d.length);
                    ((StringBuilder)inputStream).append(" bytes from ");
                    ((StringBuilder)inputStream).append(this.a);
                    Log.v("FirebaseMessaging", ((StringBuilder)inputStream).toString());
                }
                if (d.length <= 1048576) {
                    return d;
                }
                throw new IOException("Image exceeds max size of 1048576");
            }
            finally {
                if (inputStream != null) {
                    try {
                        ((InputStream)inputStream).close();
                    }
                    finally {
                        final Throwable t;
                        final Throwable exception;
                        t.addSuppressed(exception);
                    }
                }
            }
        }
        throw new IOException("Content-Length exceeds max size of 1048576");
    }
    
    @Override
    public void close() {
        this.b.cancel(true);
    }
    
    public Task e() {
        return Preconditions.checkNotNull(this.c);
    }
    
    public void g(final ExecutorService executorService) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.b = executorService.submit(new he0(this, taskCompletionSource));
        this.c = taskCompletionSource.getTask();
    }
}

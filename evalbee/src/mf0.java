// 
// Decompiled by Procyon v0.6.0
// 

public abstract class mf0
{
    public static final String a;
    
    static {
        final String i = xl0.i("InputMerger");
        fg0.d((Object)i, "tagWithPrefix(\"InputMerger\")");
        a = i;
    }
    
    public static final kf0 a(final String s) {
        fg0.e((Object)s, "className");
        try {
            final Object instance = Class.forName(s).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            fg0.c(instance, "null cannot be cast to non-null type androidx.work.InputMerger");
            return (kf0)instance;
        }
        catch (final Exception ex) {
            final xl0 e = xl0.e();
            final String a = mf0.a;
            final StringBuilder sb = new StringBuilder();
            sb.append("Trouble instantiating ");
            sb.append(s);
            e.d(a, sb.toString(), ex);
            return null;
        }
    }
}

import android.os.AsyncTask;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamDataV1;
import java.io.InputStream;
import com.google.android.gms.common.util.IOUtils;
import java.io.FileInputStream;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import java.io.File;
import androidx.appcompat.app.a;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class bx0
{
    public String a;
    public Context b;
    public LinearLayout c;
    public LinearLayout d;
    public TextView e;
    public Button f;
    public Button g;
    public a h;
    
    public bx0(final Context b, final String a) {
        this.b = b;
        this.a = a;
        this.j();
    }
    
    public static /* synthetic */ a a(final bx0 bx0) {
        return bx0.h;
    }
    
    public static /* synthetic */ String b(final bx0 bx0) {
        return bx0.a;
    }
    
    public static /* synthetic */ Context c(final bx0 bx0) {
        return bx0.b;
    }
    
    public final boolean f(String string) {
        final o30 f = o30.f();
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null && f != null) {
            final File parent = new File(this.b.getExternalFilesDir(ok.c), ok.d);
            if (!parent.exists()) {
                parent.mkdirs();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append((String)string);
            sb.append(".exm");
            string = sb.toString();
            final File file = new File(parent, (String)string);
            final jq1 n = f.n();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(ok.d);
            sb2.append("/");
            sb2.append(e.O());
            sb2.append("/");
            sb2.append((String)string);
            final x00 g = n.a(sb2.toString()).g(file);
            string = new zs1();
            g.p((OnFailureListener)new OnFailureListener(this) {
                public final bx0 a;
                
                public void onFailure(final Exception ex) {
                }
            }).u((OnSuccessListener)new OnSuccessListener(this, string) {
                public final zs1 a;
                public final bx0 b;
                
                public void a(final x00.a a) {
                    final zs1 a2 = this.a;
                    a2.b = true;
                    a2.a = true;
                }
            }).s(new o11(this, string) {
                public final zs1 a;
                public final bx0 b;
                
                public void b(final x00.a a) {
                    final zs1 a2 = this.a;
                    a2.b = true;
                    a2.a = false;
                }
            });
            while (System.currentTimeMillis() - ((zs1)string).c < 600000L && !((zs1)string).b) {
                try {
                    Thread.sleep(500L);
                }
                catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            return ((zs1)string).a;
        }
        return false;
    }
    
    public final boolean g(final File file) {
        try {
            final ExamDataV1 a = yx.a(IOUtils.toByteArray(new FileInputStream(file)));
            if (a == null) {
                return false;
            }
            final ExamId examId = a.getExamId();
            if (!xu1.d(this.b, examId, a.getSheetTemplate2(), this.a)) {
                return false;
            }
            xu1.c(this.b, examId, a);
            return true;
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    public final void h() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final bx0 a;
            
            public void onClick(final View view) {
                bx0.a(this.a).dismiss();
            }
        });
    }
    
    public final void i() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final bx0 a;
            
            public void onClick(final View view) {
                ((View)this.a.d).setVisibility(8);
                ((View)this.a.c).setVisibility(0);
                this.a.new f(null).execute((Object[])new Void[0]);
            }
        });
    }
    
    public final void j() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.b, 2131951953);
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131492976, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886208);
        this.d = (LinearLayout)inflate.findViewById(2131296741);
        this.c = (LinearLayout)inflate.findViewById(2131296740);
        this.f = (Button)inflate.findViewById(2131296442);
        this.g = (Button)inflate.findViewById(2131296425);
        this.e = (TextView)inflate.findViewById(2131297215);
        ((View)this.c).setVisibility(8);
        this.e.setText(2131886466);
        this.i();
        this.h();
        (this.h = materialAlertDialogBuilder.create()).show();
    }
    
    public class f extends AsyncTask
    {
        public boolean a;
        public String b;
        public final bx0 c;
        
        public f(final bx0 c) {
            this.c = c;
        }
        
        public Void a(final Void... array) {
            final StringBuilder sb = new StringBuilder();
            sb.append(bx0.b(this.c));
            sb.append(".exm");
            final String string = sb.toString();
            final File parent = new File(bx0.c(this.c).getExternalFilesDir(ok.c), ok.d);
            if (!parent.exists()) {
                parent.mkdirs();
            }
            final File file = new File(parent, string);
            if (!file.exists()) {
                final bx0 c = this.c;
                c.f(bx0.b(c));
            }
            Context context;
            int n;
            if (!file.exists()) {
                this.a = true;
                context = bx0.c(this.c);
                n = 2131886495;
            }
            else {
                if (this.c.g(file)) {
                    return null;
                }
                this.a = true;
                context = bx0.c(this.c);
                n = 2131886578;
            }
            this.b = context.getString(n);
            return null;
        }
        
        public void b(final Void void1) {
            if (this.a) {
                ((View)this.c.d).setVisibility(0);
                ((View)this.c.c).setVisibility(8);
                this.c.e.setText((CharSequence)this.b);
                ((TextView)this.c.g).setText(2131886176);
                ((TextView)this.c.f).setText(2131886746);
            }
            else {
                bx0.a(this.c).dismiss();
                a91.G(bx0.c(this.c), 2131886205, 2131230927, 2131231085);
            }
        }
        
        public void onPreExecute() {
        }
    }
}

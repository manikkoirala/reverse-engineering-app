import android.content.res.ColorStateList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public interface nf
{
    void a(final mf p0);
    
    float b(final mf p0);
    
    void c(final mf p0, final float p1);
    
    void d(final mf p0, final float p1);
    
    float e(final mf p0);
    
    void f(final mf p0);
    
    void g(final mf p0);
    
    float h(final mf p0);
    
    void i(final mf p0, final Context p1, final ColorStateList p2, final float p3, final float p4, final float p5);
    
    void j(final mf p0, final ColorStateList p1);
    
    float k(final mf p0);
    
    ColorStateList l(final mf p0);
    
    void m(final mf p0, final float p1);
    
    float n(final mf p0);
    
    void o();
}

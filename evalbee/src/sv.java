import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

// 
// Decompiled by Procyon v0.6.0
// 

public class sv extends GoogleApi
{
    public static final Api.ClientKey a;
    public static final Api.AbstractClientBuilder b;
    public static final Api c;
    
    static {
        c = new Api("DynamicLinks.API", b = new Api.AbstractClientBuilder() {
            public tv a(final Context context, final Looper looper, final ClientSettings clientSettings, final NoOptions noOptions, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
                return new tv(context, looper, clientSettings, connectionCallbacks, onConnectionFailedListener);
            }
        }, a = new Api.ClientKey());
    }
    
    public sv(final Context context) {
        super(context, sv.c, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, Settings.DEFAULT_SETTINGS);
    }
}

import com.google.android.gms.tasks.Task;
import java.nio.charset.Charset;
import com.google.android.datatransport.TransportFactory;
import com.google.android.datatransport.Transport;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.runtime.Destination;
import com.google.android.datatransport.cct.CCTDestination;
import com.google.android.datatransport.runtime.TransportRuntime;
import android.content.Context;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import com.google.android.datatransport.Transformer;

// 
// Decompiled by Procyon v0.6.0
// 

public class op
{
    public static final un c;
    public static final String d;
    public static final String e;
    public static final Transformer f;
    public final ae1 a;
    public final Transformer b;
    
    static {
        c = new un();
        d = e("hts/cahyiseot-agolai.o/1frlglgc/aclg", "tp:/rsltcrprsp.ogepscmv/ieo/eaybtho");
        e = e("AzSBpY4F0rHiHFdinTvM", "IayrSTFL9eJ69YeSUO2");
        f = new np();
    }
    
    public op(final ae1 a, final Transformer b) {
        this.a = a;
        this.b = b;
    }
    
    public static op b(final Context context, final zm1 zm1, final m11 m11) {
        TransportRuntime.initialize(context);
        final TransportFactory factory = TransportRuntime.getInstance().newFactory(new CCTDestination(op.d, op.e));
        final Encoding of = Encoding.of("json");
        final Transformer f = op.f;
        return new op(new ae1(factory.getTransport("FIREBASE_CRASHLYTICS_REPORT", CrashlyticsReport.class, of, f), zm1.a(), m11), f);
    }
    
    public static String e(final String s, final String s2) {
        final int n = s.length() - s2.length();
        if (n >= 0 && n <= 1) {
            final StringBuilder sb = new StringBuilder(s.length() + s2.length());
            for (int i = 0; i < s.length(); ++i) {
                sb.append(s.charAt(i));
                if (s2.length() > i) {
                    sb.append(s2.charAt(i));
                }
            }
            return sb.toString();
        }
        throw new IllegalArgumentException("Invalid input received");
    }
    
    public Task c(final ao ao, final boolean b) {
        return this.a.i(ao, b).getTask();
    }
}

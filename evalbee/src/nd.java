import java.nio.channels.WritableByteChannel;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.lang.ref.SoftReference;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class nd
{
    public static final ThreadLocal a;
    public static final Class b;
    public static final long c;
    
    static {
        a = new ThreadLocal();
        c = b(b = e("java.io.FileOutputStream"));
    }
    
    public static byte[] a() {
        final SoftReference softReference = nd.a.get();
        byte[] array;
        if (softReference == null) {
            array = null;
        }
        else {
            array = (byte[])softReference.get();
        }
        return array;
    }
    
    public static long b(final Class clazz) {
        if (clazz == null) {
            return -1L;
        }
        try {
            if (e12.G()) {
                return e12.I(clazz.getDeclaredField("channel"));
            }
            return -1L;
        }
        finally {
            return -1L;
        }
    }
    
    public static byte[] c(int max) {
        max = Math.max(max, 1024);
        final byte[] a = a();
        if (a != null) {
            final byte[] array = a;
            if (!d(max, a.length)) {
                return array;
            }
        }
        byte[] array;
        final byte[] array2 = array = new byte[max];
        if (max <= 16384) {
            f(array2);
            array = array2;
        }
        return array;
    }
    
    public static boolean d(final int n, final int n2) {
        return n2 < n && n2 < n * 0.5f;
    }
    
    public static Class e(final String className) {
        try {
            return Class.forName(className);
        }
        catch (final ClassNotFoundException ex) {
            return null;
        }
    }
    
    public static void f(final byte[] referent) {
        nd.a.set(new SoftReference(referent));
    }
    
    public static void g(final ByteBuffer byteBuffer, final OutputStream outputStream) {
        final int position = byteBuffer.position();
        try {
            if (byteBuffer.hasArray()) {
                outputStream.write(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
            }
            else if (!h(byteBuffer, outputStream)) {
                final byte[] c = c(byteBuffer.remaining());
                while (byteBuffer.hasRemaining()) {
                    final int min = Math.min(byteBuffer.remaining(), c.length);
                    byteBuffer.get(c, 0, min);
                    outputStream.write(c, 0, min);
                }
            }
        }
        finally {
            byteBuffer.position(position);
        }
    }
    
    public static boolean h(final ByteBuffer byteBuffer, final OutputStream outputStream) {
        final long c = nd.c;
        if (c >= 0L && nd.b.isInstance(outputStream)) {
            WritableByteChannel writableByteChannel;
            try {
                writableByteChannel = (WritableByteChannel)e12.E(outputStream, c);
            }
            catch (final ClassCastException ex) {
                writableByteChannel = null;
            }
            if (writableByteChannel != null) {
                writableByteChannel.write(byteBuffer);
                return true;
            }
        }
        return false;
    }
}

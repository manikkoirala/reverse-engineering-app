import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class h11
{
    public boolean a;
    public final CopyOnWriteArrayList b;
    public a90 c;
    
    public h11(final boolean a) {
        this.a = a;
        this.b = new CopyOnWriteArrayList();
    }
    
    public final void a(final cf e) {
        fg0.e((Object)e, "cancellable");
        this.b.add(e);
    }
    
    public final a90 b() {
        return this.c;
    }
    
    public void c() {
    }
    
    public abstract void d();
    
    public void e(final xa xa) {
        fg0.e((Object)xa, "backEvent");
    }
    
    public void f(final xa xa) {
        fg0.e((Object)xa, "backEvent");
    }
    
    public final boolean g() {
        return this.a;
    }
    
    public final void h() {
        final Iterator iterator = this.b.iterator();
        while (iterator.hasNext()) {
            ((cf)iterator.next()).cancel();
        }
    }
    
    public final void i(final cf o) {
        fg0.e((Object)o, "cancellable");
        this.b.remove(o);
    }
    
    public final void j(final boolean a) {
        this.a = a;
        final a90 c = this.c;
        if (c != null) {
            c.invoke();
        }
    }
    
    public final void k(final a90 c) {
        this.c = c;
    }
}

import com.google.android.gms.common.api.internal.BackgroundDetector;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ee2 implements BackgroundStateChangeListener
{
    public final zd2 a;
    
    public ee2(final zd2 a) {
        this.a = a;
    }
    
    @Override
    public final void onBackgroundStateChanged(final boolean b) {
        if (b) {
            zd2.e(this.a, true);
            this.a.b();
            return;
        }
        zd2.e(this.a, false);
        if (zd2.g(this.a)) {
            zd2.a(this.a).c();
        }
    }
}

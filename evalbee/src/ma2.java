import com.google.android.gms.ads.formats.zze;
import com.google.android.gms.ads.formats.zzg;
import java.util.Map;
import android.view.View;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ma2 extends UnifiedNativeAdMapper
{
    public final UnifiedNativeAd a;
    
    public ma2(final UnifiedNativeAd a) {
        this.a = a;
        this.setHeadline(a.getHeadline());
        this.setImages(a.getImages());
        this.setBody(a.getBody());
        this.setIcon(a.getIcon());
        this.setCallToAction(a.getCallToAction());
        this.setAdvertiser(a.getAdvertiser());
        this.setStarRating(a.getStarRating());
        this.setStore(a.getStore());
        this.setPrice(a.getPrice());
        this.zzd(a.zza());
        this.setOverrideImpressionRecording(true);
        this.setOverrideClickHandling(true);
        this.zze(a.getVideoController());
    }
    
    @Override
    public final void trackViews(final View key, final Map map, final Map map2) {
        if (key instanceof zzg) {
            final zzg zzg = (zzg)key;
            throw null;
        }
        if (com.google.android.gms.ads.formats.zze.zza.get(key) == null) {
            return;
        }
        throw null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class q4
{
    public static final Class a;
    public static final boolean b;
    
    static {
        a = a("libcore.io.Memory");
        b = (a("org.robolectric.Robolectric") != null);
    }
    
    public static Class a(final String className) {
        try {
            return Class.forName(className);
        }
        finally {
            return null;
        }
    }
    
    public static Class b() {
        return q4.a;
    }
    
    public static boolean c() {
        return q4.a != null && !q4.b;
    }
}

import com.google.protobuf.ByteString;
import com.google.firebase.firestore.model.FieldIndex;

// 
// Decompiled by Procyon v0.6.0
// 

public class xe0
{
    public final h21 a;
    public final a b;
    public final b c;
    
    public xe0() {
        this.a = new h21();
        this.b = new a();
        this.c = new b();
    }
    
    public static /* synthetic */ h21 a(final xe0 xe0) {
        return xe0.a;
    }
    
    public bt b(final FieldIndex.Segment.Kind kind) {
        if (kind.equals(FieldIndex.Segment.Kind.DESCENDING)) {
            return this.c;
        }
        return this.b;
    }
    
    public byte[] c() {
        return this.a.a();
    }
    
    public void d(final byte[] array) {
        this.a.c(array);
    }
    
    public class a extends bt
    {
        public final xe0 a;
        
        public a(final xe0 a) {
            this.a = a;
        }
        
        @Override
        public void a(final ByteString byteString) {
            xe0.a(this.a).h(byteString);
        }
        
        @Override
        public void b(final double n) {
            xe0.a(this.a).j(n);
        }
        
        @Override
        public void c() {
            xe0.a(this.a).n();
        }
        
        @Override
        public void d(final long n) {
            xe0.a(this.a).r(n);
        }
        
        @Override
        public void e(final String s) {
            xe0.a(this.a).v(s);
        }
    }
    
    public class b extends bt
    {
        public final xe0 a;
        
        public b(final xe0 a) {
            this.a = a;
        }
        
        @Override
        public void a(final ByteString byteString) {
            xe0.a(this.a).i(byteString);
        }
        
        @Override
        public void b(final double n) {
            xe0.a(this.a).k(n);
        }
        
        @Override
        public void c() {
            xe0.a(this.a).o();
        }
        
        @Override
        public void d(final long n) {
            xe0.a(this.a).s(n);
        }
        
        @Override
        public void e(final String s) {
            xe0.a(this.a).w(s);
        }
    }
}

import android.widget.AdapterView;
import android.widget.ListView;
import android.view.View$OnClickListener;
import android.graphics.PorterDuff$Mode;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import com.ekodroid.omrevaluator.exams.ResultActivity;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class n3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    public ExamId c;
    
    public n3(final ResultActivity a, final ExamId c, final ArrayList b) {
        this.a = (Context)a;
        this.c = c;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final ye1 ye1 = this.b.get(index);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493028, (ViewGroup)null);
        final ImageView imageView = (ImageView)inflate.findViewById(2131296688);
        final ImageView imageView2 = (ImageView)inflate.findViewById(2131296689);
        final ImageView imageView3 = (ImageView)inflate.findViewById(2131296690);
        final ImageView imageView4 = (ImageView)inflate.findViewById(2131296685);
        final ImageButton imageButton = (ImageButton)inflate.findViewById(2131296409);
        final TextView textView = (TextView)inflate.findViewById(2131297194);
        final TextView textView2 = (TextView)inflate.findViewById(2131297297);
        final TextView textView3 = (TextView)inflate.findViewById(2131297192);
        final TextView textView4 = (TextView)inflate.findViewById(2131297193);
        final TextView textView5 = (TextView)inflate.findViewById(2131297195);
        final TextView textView6 = (TextView)inflate.findViewById(2131297190);
        final TextView textView7 = (TextView)inflate.findViewById(2131297191);
        final TextView textView8 = (TextView)inflate.findViewById(2131297196);
        final StringBuilder sb = new StringBuilder();
        sb.append(ye1.g());
        sb.append("");
        textView.setText((CharSequence)sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(ve1.o(ye1.d()));
        sb2.append("");
        textView3.setText((CharSequence)sb2.toString());
        textView5.setText((CharSequence)ye1.e());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(ye1.f());
        sb3.append("");
        textView4.setText((CharSequence)sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("");
        sb4.append(ye1.a());
        textView6.setText((CharSequence)sb4.toString());
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("");
        sb5.append(ye1.c());
        textView7.setText((CharSequence)sb5.toString());
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("");
        sb6.append(ye1.h());
        textView8.setText((CharSequence)sb6.toString());
        if (ye1.i()) {
            imageView.setVisibility(0);
        }
        else {
            imageView.setVisibility(8);
        }
        if (ye1.k()) {
            imageView2.setVisibility(0);
        }
        else {
            imageView2.setVisibility(8);
        }
        if (ye1.j()) {
            ((View)textView2).setVisibility(8);
            ((View)imageButton).setVisibility(0);
        }
        else {
            ((View)textView2).setVisibility(0);
            ((View)imageButton).setVisibility(8);
            textView2.setText((CharSequence)a91.l(ye1.e()));
        }
        if (ye1.l()) {
            imageView3.setVisibility(0);
        }
        else {
            imageView3.setVisibility(8);
        }
        final ArrayList<SheetImageModel> sheetImages = ResultRepository.getInstance(this.a).getSheetImages(this.c, ye1.g());
        if (sheetImages.size() > 0) {
            imageView4.setVisibility(0);
            Context context;
            int n;
            if (((SheetImageModel)sheetImages.get(0)).isSynced()) {
                context = this.a;
                n = 2131099711;
            }
            else {
                context = this.a;
                n = 2131099710;
            }
            imageView4.setColorFilter(sl.getColor(context, n), PorterDuff$Mode.MULTIPLY);
        }
        else {
            imageView4.setVisibility(8);
        }
        ((View)imageButton).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, viewGroup, index) {
            public final ViewGroup a;
            public final int b;
            public final n3 c;
            
            public void onClick(final View view) {
                ((AdapterView)this.a).performItemClick(view, this.b + 1, (long)view.getId());
            }
        });
        return inflate;
    }
}

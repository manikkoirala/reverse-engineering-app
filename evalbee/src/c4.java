import android.graphics.Rect;
import android.view.View;
import android.content.Context;
import java.util.Locale;
import android.text.method.TransformationMethod;

// 
// Decompiled by Procyon v0.6.0
// 

public class c4 implements TransformationMethod
{
    public Locale a;
    
    public c4(final Context context) {
        this.a = context.getResources().getConfiguration().locale;
    }
    
    public CharSequence getTransformation(final CharSequence charSequence, final View view) {
        String upperCase;
        if (charSequence != null) {
            upperCase = charSequence.toString().toUpperCase(this.a);
        }
        else {
            upperCase = null;
        }
        return upperCase;
    }
    
    public void onFocusChanged(final View view, final CharSequence charSequence, final boolean b, final int n, final Rect rect) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class p4
{
    public static boolean a;
    public static final Class b;
    public static final boolean c;
    
    static {
        b = a("libcore.io.Memory");
        c = (!p4.a && a("org.robolectric.Robolectric") != null);
    }
    
    public static Class a(final String className) {
        try {
            return Class.forName(className);
        }
        finally {
            return null;
        }
    }
    
    public static Class b() {
        return p4.b;
    }
    
    public static boolean c() {
        return p4.a || (p4.b != null && !p4.c);
    }
}

import android.graphics.drawable.Drawable;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class vf1 extends Drawable
{
    public static final double a;
    
    static {
        a = Math.cos(Math.toRadians(45.0));
    }
    
    public static float a(final float n, final float n2, final boolean b) {
        float n3 = n;
        if (b) {
            n3 = (float)(n + (1.0 - vf1.a) * n2);
        }
        return n3;
    }
    
    public static float b(float n, final float n2, final boolean b) {
        final float n3 = n *= 1.5f;
        if (b) {
            n = (float)(n3 + (1.0 - vf1.a) * n2);
        }
        return n;
    }
}

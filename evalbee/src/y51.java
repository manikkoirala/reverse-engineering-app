// 
// Decompiled by Procyon v0.6.0
// 

public class y51 implements w51
{
    public final Object[] a;
    public int b;
    
    public y51(final int n) {
        if (n > 0) {
            this.a = new Object[n];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }
    
    @Override
    public Object a() {
        final int b = this.b;
        if (b > 0) {
            final int n = b - 1;
            final Object[] a = this.a;
            final Object o = a[n];
            a[n] = null;
            this.b = b - 1;
            return o;
        }
        return null;
    }
    
    @Override
    public boolean b(final Object o) {
        if (this.c(o)) {
            throw new IllegalStateException("Already in the pool!");
        }
        final int b = this.b;
        final Object[] a = this.a;
        if (b < a.length) {
            a[b] = o;
            this.b = b + 1;
            return true;
        }
        return false;
    }
    
    public final boolean c(final Object o) {
        for (int i = 0; i < this.b; ++i) {
            if (this.a[i] == o) {
                return true;
            }
        }
        return false;
    }
}

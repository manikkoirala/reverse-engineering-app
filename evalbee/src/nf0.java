import android.graphics.Insets;
import android.graphics.Rect;

// 
// Decompiled by Procyon v0.6.0
// 

public final class nf0
{
    public static final nf0 e;
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    
    static {
        e = new nf0(0, 0, 0, 0);
    }
    
    public nf0(final int a, final int b, final int c, final int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static nf0 a(final nf0 nf0, final nf0 nf2) {
        return b(Math.max(nf0.a, nf2.a), Math.max(nf0.b, nf2.b), Math.max(nf0.c, nf2.c), Math.max(nf0.d, nf2.d));
    }
    
    public static nf0 b(final int n, final int n2, final int n3, final int n4) {
        if (n == 0 && n2 == 0 && n3 == 0 && n4 == 0) {
            return nf0.e;
        }
        return new nf0(n, n2, n3, n4);
    }
    
    public static nf0 c(final Rect rect) {
        return b(rect.left, rect.top, rect.right, rect.bottom);
    }
    
    public static nf0 d(final Insets insets) {
        return b(yu.a(insets), av.a(insets), cv.a(insets), ev.a(insets));
    }
    
    public Insets e() {
        return nf0.a.a(this.a, this.b, this.c, this.d);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && nf0.class == o.getClass()) {
            final nf0 nf0 = (nf0)o;
            return this.d == nf0.d && this.a == nf0.a && this.c == nf0.c && this.b == nf0.b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((this.a * 31 + this.b) * 31 + this.c) * 31 + this.d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Insets{left=");
        sb.append(this.a);
        sb.append(", top=");
        sb.append(this.b);
        sb.append(", right=");
        sb.append(this.c);
        sb.append(", bottom=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
    
    public abstract static class a
    {
        public static Insets a(final int n, final int n2, final int n3, final int n4) {
            return Insets.of(n, n2, n3, n4);
        }
    }
}

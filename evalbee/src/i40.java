import com.google.protobuf.j0;
import java.util.Map;
import com.google.firestore.v1.k;
import java.util.Iterator;
import com.google.firestore.v1.Value;
import com.google.firestore.v1.a;

// 
// Decompiled by Procyon v0.6.0
// 

public class i40
{
    public static final i40 a;
    
    static {
        a = new i40();
    }
    
    public final void a(final a a, final bt bt) {
        this.i(bt, 50);
        final Iterator iterator = a.h().iterator();
        while (iterator.hasNext()) {
            this.f((Value)iterator.next(), bt);
        }
    }
    
    public final void b(String h, final bt bt) {
        this.i(bt, 37);
        final ke1 q = ke1.q(h);
        for (int l = q.l(), i = 5; i < l; ++i) {
            h = q.h(i);
            this.i(bt, 60);
            this.h(h, bt);
        }
    }
    
    public final void c(final k k, final bt bt) {
        this.i(bt, 55);
        for (final Map.Entry<String, V> entry : k.d0().entrySet()) {
            final String s = entry.getKey();
            final Value value = (Value)entry.getValue();
            this.d(s, bt);
            this.f(value, bt);
        }
    }
    
    public final void d(final String s, final bt bt) {
        this.i(bt, 25);
        this.h(s, bt);
    }
    
    public void e(final Value value, final bt bt) {
        this.f(value, bt);
        bt.c();
    }
    
    public final void f(final Value value, final bt bt) {
        int n = 0;
        Label_0348: {
            double e0 = 0.0;
            Label_0266: {
                long n2 = 0L;
                Label_0244: {
                    switch (i40$a.a[value.w0().ordinal()]) {
                        default: {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("unknown index value type ");
                            sb.append(value.w0());
                            throw new IllegalArgumentException(sb.toString());
                        }
                        case 11: {
                            this.a(value.l0(), bt);
                            break;
                        }
                        case 10: {
                            if (a32.x(value)) {
                                n = Integer.MAX_VALUE;
                                break Label_0348;
                            }
                            this.c(value.s0(), bt);
                            break;
                        }
                        case 9: {
                            final ui0 q0 = value.q0();
                            this.i(bt, 45);
                            bt.b(q0.d0());
                            e0 = q0.e0();
                            break Label_0266;
                        }
                        case 8: {
                            this.b(value.t0(), bt);
                            return;
                        }
                        case 7: {
                            this.i(bt, 30);
                            bt.a(value.n0());
                            break;
                        }
                        case 6: {
                            this.d(value.u0(), bt);
                            break;
                        }
                        case 5: {
                            final j0 v0 = value.v0();
                            this.i(bt, 20);
                            bt.d(v0.e0());
                            n2 = v0.d0();
                            break Label_0244;
                        }
                        case 4: {
                            this.i(bt, 15);
                            e0 = (double)value.r0();
                            break Label_0266;
                        }
                        case 3: {
                            final double p2 = value.p0();
                            if (Double.isNaN(p2)) {
                                n = 13;
                                break Label_0348;
                            }
                            this.i(bt, 15);
                            if (p2 == 0.0) {
                                e0 = 0.0;
                                break Label_0266;
                            }
                            bt.b(p2);
                            return;
                        }
                        case 2: {
                            this.i(bt, 10);
                            if (value.m0()) {
                                n2 = 1L;
                                break Label_0244;
                            }
                            n2 = 0L;
                            break Label_0244;
                        }
                        case 1: {
                            n = 5;
                            break Label_0348;
                        }
                    }
                    this.g(bt);
                    return;
                }
                bt.d(n2);
                return;
            }
            bt.b(e0);
            return;
        }
        this.i(bt, n);
    }
    
    public final void g(final bt bt) {
        bt.d(2L);
    }
    
    public final void h(final String s, final bt bt) {
        bt.e(s);
    }
    
    public final void i(final bt bt, final int n) {
        bt.d(n);
    }
}

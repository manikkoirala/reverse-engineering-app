import android.graphics.Color;
import android.content.res.TypedArray;
import android.util.StateSet;
import android.os.Build$VERSION;
import android.util.Log;
import android.util.TypedValue;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParserException;
import android.util.Xml;
import android.content.res.ColorStateList;
import android.content.res.Resources$Theme;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.Resources;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bi
{
    public static final ThreadLocal a;
    
    static {
        a = new ThreadLocal();
    }
    
    public static ColorStateList a(final Resources resources, final XmlPullParser xmlPullParser, final Resources$Theme resources$Theme) {
        final AttributeSet attributeSet = Xml.asAttributeSet(xmlPullParser);
        int next;
        do {
            next = xmlPullParser.next();
        } while (next != 2 && next != 1);
        if (next == 2) {
            return b(resources, xmlPullParser, attributeSet, resources$Theme);
        }
        throw new XmlPullParserException("No start tag found");
    }
    
    public static ColorStateList b(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final String name = xmlPullParser.getName();
        if (name.equals("selector")) {
            return e(resources, xmlPullParser, set, resources$Theme);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(xmlPullParser.getPositionDescription());
        sb.append(": invalid color state list tag ");
        sb.append(name);
        throw new XmlPullParserException(sb.toString());
    }
    
    public static TypedValue c() {
        final ThreadLocal a = bi.a;
        TypedValue value;
        if ((value = a.get()) == null) {
            value = new TypedValue();
            a.set(value);
        }
        return value;
    }
    
    public static ColorStateList d(final Resources resources, final int n, final Resources$Theme resources$Theme) {
        try {
            return a(resources, (XmlPullParser)resources.getXml(n), resources$Theme);
        }
        catch (final Exception ex) {
            Log.e("CSLCompat", "Failed to inflate ColorStateList.", (Throwable)ex);
            return null;
        }
    }
    
    public static ColorStateList e(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final int n = xmlPullParser.getDepth() + 1;
        int[][] array = new int[20][];
        int[] array2 = new int[20];
        int n2 = 0;
        while (true) {
            final int next = xmlPullParser.next();
            if (next == 1) {
                break;
            }
            final int depth = xmlPullParser.getDepth();
            if (depth < n && next == 3) {
                break;
            }
            int[] a = array2;
            int[][] array3 = array;
            int n3 = n2;
            if (next == 2) {
                a = array2;
                array3 = array;
                n3 = n2;
                if (depth <= n) {
                    if (!xmlPullParser.getName().equals("item")) {
                        a = array2;
                        array3 = array;
                        n3 = n2;
                    }
                    else {
                        final TypedArray h = h(resources, resources$Theme, set, xb1.b);
                        final int c = xb1.c;
                        final int resourceId = h.getResourceId(c, -1);
                        int c2 = c;
                        int n4 = 0;
                        Label_0213: {
                            if (resourceId != -1) {
                                c2 = c;
                                if (!f(resources, resourceId)) {
                                    try {
                                        n4 = a(resources, (XmlPullParser)resources.getXml(resourceId), resources$Theme).getDefaultColor();
                                        break Label_0213;
                                    }
                                    catch (final Exception ex) {
                                        c2 = xb1.c;
                                    }
                                }
                            }
                            n4 = h.getColor(c2, -65281);
                        }
                        int n5 = xb1.d;
                        final boolean hasValue = h.hasValue(n5);
                        float float1 = 1.0f;
                        Label_0266: {
                            if (!hasValue) {
                                n5 = xb1.f;
                                if (!h.hasValue(n5)) {
                                    break Label_0266;
                                }
                            }
                            float1 = h.getFloat(n5, 1.0f);
                        }
                        int n6 = 0;
                        Label_0297: {
                            if (Build$VERSION.SDK_INT >= 31) {
                                n6 = xb1.e;
                                if (h.hasValue(n6)) {
                                    break Label_0297;
                                }
                            }
                            n6 = xb1.g;
                        }
                        final float float2 = h.getFloat(n6, -1.0f);
                        h.recycle();
                        final int attributeCount = set.getAttributeCount();
                        final int[] array4 = new int[attributeCount];
                        int i = 0;
                        int n7 = 0;
                        while (i < attributeCount) {
                            final int attributeNameResource = set.getAttributeNameResource(i);
                            int n8 = n7;
                            if (attributeNameResource != 16843173) {
                                n8 = n7;
                                if (attributeNameResource != 16843551) {
                                    n8 = n7;
                                    if (attributeNameResource != qa1.a) {
                                        n8 = n7;
                                        if (attributeNameResource != qa1.b) {
                                            int n9;
                                            if (set.getAttributeBooleanValue(i, false)) {
                                                n9 = attributeNameResource;
                                            }
                                            else {
                                                n9 = -attributeNameResource;
                                            }
                                            array4[n7] = n9;
                                            n8 = n7 + 1;
                                        }
                                    }
                                }
                            }
                            ++i;
                            n7 = n8;
                        }
                        final int[] trimStateSet = StateSet.trimStateSet(array4, n7);
                        a = wb0.a(array2, n2, g(n4, float1, float2));
                        array3 = (int[][])wb0.b(array, n2, trimStateSet);
                        n3 = n2 + 1;
                    }
                }
            }
            array2 = a;
            array = array3;
            n2 = n3;
        }
        final int[] array5 = new int[n2];
        final int[][] array6 = new int[n2][];
        System.arraycopy(array2, 0, array5, 0, n2);
        System.arraycopy(array, 0, array6, 0, n2);
        return new ColorStateList(array6, array5);
    }
    
    public static boolean f(final Resources resources, int type) {
        final TypedValue c = c();
        boolean b = true;
        resources.getValue(type, c, true);
        type = c.type;
        if (type < 28 || type > 31) {
            b = false;
        }
        return b;
    }
    
    public static int g(final int n, final float n2, final float n3) {
        final boolean b = n3 >= 0.0f && n3 <= 100.0f;
        if (n2 == 1.0f && !b) {
            return n;
        }
        final int b2 = ku0.b((int)(Color.alpha(n) * n2 + 0.5f), 0, 255);
        int m = n;
        if (b) {
            final te c = te.c(n);
            m = te.m(c.j(), c.i(), n3);
        }
        return (m & 0xFFFFFF) | b2 << 24;
    }
    
    public static TypedArray h(final Resources resources, final Resources$Theme resources$Theme, final AttributeSet set, final int[] array) {
        TypedArray typedArray;
        if (resources$Theme == null) {
            typedArray = resources.obtainAttributes(set, array);
        }
        else {
            typedArray = resources$Theme.obtainStyledAttributes(set, array, 0, 0);
        }
        return typedArray;
    }
}

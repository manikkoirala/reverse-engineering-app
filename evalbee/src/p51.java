import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class p51 extends v9
{
    public static final Parcelable$Creator<p51> CREATOR;
    public final String a;
    
    static {
        CREATOR = (Parcelable$Creator)new yb2();
    }
    
    public p51(final String s) {
        this.a = Preconditions.checkNotEmpty(s);
    }
    
    public static zzagt J(final p51 p2, final String s) {
        Preconditions.checkNotNull(p2);
        return (zzagt)new com.google.android.gms.internal.firebase_auth_api.zzagt((String)null, (String)null, p2.i(), (String)null, (String)null, p2.a, s, (String)null, (String)null);
    }
    
    @Override
    public String E() {
        return "playgames.google.com";
    }
    
    @Override
    public final v9 H() {
        return new p51(this.a);
    }
    
    @Override
    public String i() {
        return "playgames.google.com";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

import java.util.Collections;
import java.util.List;
import androidx.work.b;
import androidx.room.SharedSQLiteStatement;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class k92 implements j92
{
    public final RoomDatabase a;
    public final ix b;
    public final SharedSQLiteStatement c;
    public final SharedSQLiteStatement d;
    
    public k92(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final k92 d;
            
            @Override
            public String e() {
                return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
            }
            
            public void k(final ws1 ws1, final i92 i92) {
                if (i92.b() == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, i92.b());
                }
                final byte[] k = b.k(i92.a());
                if (k == null) {
                    ws1.I(2);
                }
                else {
                    ws1.D(2, k);
                }
            }
        };
        this.c = new SharedSQLiteStatement(this, a) {
            public final k92 d;
            
            @Override
            public String e() {
                return "DELETE from WorkProgress where work_spec_id=?";
            }
        };
        this.d = new SharedSQLiteStatement(this, a) {
            public final k92 d;
            
            @Override
            public String e() {
                return "DELETE FROM WorkProgress";
            }
        };
    }
    
    public static List d() {
        return Collections.emptyList();
    }
    
    @Override
    public void a(final String s) {
        this.a.d();
        final ws1 b = this.c.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.c.h(b);
        }
    }
    
    @Override
    public void b(final i92 i92) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(i92);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
    
    @Override
    public void c() {
        this.a.d();
        final ws1 b = this.d.b();
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.d.h(b);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class i71
{
    public static void A(final boolean b, final String s, final long l) {
        if (b) {
            return;
        }
        throw new IllegalStateException(ir1.c(s, l));
    }
    
    public static void B(final boolean b, final String s, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(ir1.c(s, o));
    }
    
    public static String a(final int n, final int n2, final String s) {
        if (n < 0) {
            return ir1.c("%s (%s) must not be negative", s, n);
        }
        if (n2 >= 0) {
            return ir1.c("%s (%s) must be less than size (%s)", s, n, n2);
        }
        final StringBuilder sb = new StringBuilder(26);
        sb.append("negative size: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static String b(final int n, final int n2, final String s) {
        if (n < 0) {
            return ir1.c("%s (%s) must not be negative", s, n);
        }
        if (n2 >= 0) {
            return ir1.c("%s (%s) must not be greater than size (%s)", s, n, n2);
        }
        final StringBuilder sb = new StringBuilder(26);
        sb.append("negative size: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static String c(final int i, final int j, final int n) {
        if (i < 0 || i > n) {
            return b(i, n, "start index");
        }
        if (j >= 0 && j <= n) {
            return ir1.c("end index (%s) must not be less than start index (%s)", j, i);
        }
        return b(j, n, "end index");
    }
    
    public static void d(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static void e(final boolean b, final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    public static void f(final boolean b, final String s, final char c) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, c));
    }
    
    public static void g(final boolean b, final String s, final char c, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, c, o));
    }
    
    public static void h(final boolean b, final String s, final int i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, i));
    }
    
    public static void i(final boolean b, final String s, final int i, final int j) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, i, j));
    }
    
    public static void j(final boolean b, final String s, final long l) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, l));
    }
    
    public static void k(final boolean b, final String s, final long l, final long i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, l, i));
    }
    
    public static void l(final boolean b, final String s, final long l, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, l, o));
    }
    
    public static void m(final boolean b, final String s, final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, o));
    }
    
    public static void n(final boolean b, final String s, final Object o, final Object o2) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, o, o2));
    }
    
    public static void o(final boolean b, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(ir1.c(s, o, o2, o3, o4));
    }
    
    public static int p(final int n, final int n2) {
        return q(n, n2, "index");
    }
    
    public static int q(final int n, final int n2, final String s) {
        if (n >= 0 && n < n2) {
            return n;
        }
        throw new IndexOutOfBoundsException(a(n, n2, s));
    }
    
    public static Object r(final Object o) {
        o.getClass();
        return o;
    }
    
    public static Object s(final Object o, final Object obj) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(String.valueOf(obj));
    }
    
    public static Object t(final Object o, final String s, final Object o2) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(ir1.c(s, o2));
    }
    
    public static int u(final int n, final int n2) {
        return v(n, n2, "index");
    }
    
    public static int v(final int n, final int n2, final String s) {
        if (n >= 0 && n <= n2) {
            return n;
        }
        throw new IndexOutOfBoundsException(b(n, n2, s));
    }
    
    public static void w(final int n, final int n2, final int n3) {
        if (n >= 0 && n2 >= n && n2 <= n3) {
            return;
        }
        throw new IndexOutOfBoundsException(c(n, n2, n3));
    }
    
    public static void x(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public static void y(final boolean b, final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.valueOf(obj));
    }
    
    public static void z(final boolean b, final String s, final int i) {
        if (b) {
            return;
        }
        throw new IllegalStateException(ir1.c(s, i));
    }
}

import android.os.BaseBundle;
import androidx.work.OutOfQuotaPolicy;
import java.util.HashSet;
import android.os.PersistableBundle;
import android.content.ComponentName;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Iterator;
import java.util.List;
import android.app.job.JobInfo;
import androidx.work.a;
import androidx.work.impl.WorkDatabase;
import android.app.job.JobScheduler;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class pt1 implements ij1
{
    public static final String f;
    public final Context a;
    public final JobScheduler b;
    public final ot1 c;
    public final WorkDatabase d;
    public final a e;
    
    static {
        f = xl0.i("SystemJobScheduler");
    }
    
    public pt1(final Context context, final WorkDatabase workDatabase, final a a) {
        this(context, workDatabase, a, (JobScheduler)context.getSystemService("jobscheduler"), new ot1(context, a.a()));
    }
    
    public pt1(final Context a, final WorkDatabase d, final a e, final JobScheduler b, final ot1 c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static void c(final Context context) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            final List g = g(context, jobScheduler);
            if (g != null && !g.isEmpty()) {
                final Iterator iterator = g.iterator();
                while (iterator.hasNext()) {
                    d(jobScheduler, ((JobInfo)iterator.next()).getId());
                }
            }
        }
    }
    
    public static void d(final JobScheduler jobScheduler, final int i) {
        try {
            jobScheduler.cancel(i);
        }
        finally {
            final Throwable t;
            xl0.e().d(pt1.f, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", i), t);
        }
    }
    
    public static List f(final Context context, final JobScheduler jobScheduler, final String s) {
        final List g = g(context, jobScheduler);
        if (g == null) {
            return null;
        }
        final ArrayList list = new ArrayList(2);
        for (final JobInfo jobInfo : g) {
            final x82 h = h(jobInfo);
            if (h != null && s.equals(h.b())) {
                list.add(jobInfo.getId());
            }
        }
        return list;
    }
    
    public static List g(final Context context, final JobScheduler jobScheduler) {
        List list = null;
        try {
            jobScheduler.getAllPendingJobs();
        }
        finally {
            final Throwable t;
            xl0.e().d(pt1.f, "getAllPendingJobs() is not reliable on this device.", t);
            list = null;
        }
        if (list == null) {
            return null;
        }
        final ArrayList list2 = new ArrayList(list.size());
        final ComponentName componentName = new ComponentName(context, (Class)SystemJobService.class);
        for (final JobInfo jobInfo : list) {
            if (componentName.equals((Object)jobInfo.getService())) {
                list2.add((Object)jobInfo);
            }
        }
        return list2;
    }
    
    public static x82 h(final JobInfo jobInfo) {
        final PersistableBundle extras = jobInfo.getExtras();
        Label_0043: {
            if (extras == null) {
                break Label_0043;
            }
            try {
                if (((BaseBundle)extras).containsKey("EXTRA_WORK_SPEC_ID")) {
                    return new x82(((BaseBundle)extras).getString("EXTRA_WORK_SPEC_ID"), ((BaseBundle)extras).getInt("EXTRA_WORK_SPEC_GENERATION", 0));
                }
                return null;
            }
            catch (final NullPointerException ex) {
                return null;
            }
        }
    }
    
    public static boolean i(final Context context, final WorkDatabase workDatabase) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService("jobscheduler");
        final List g = g(context, jobScheduler);
        final List d = workDatabase.H().d();
        final boolean b = false;
        int size;
        if (g != null) {
            size = g.size();
        }
        else {
            size = 0;
        }
        final HashSet set = new HashSet(size);
        if (g != null && !g.isEmpty()) {
            for (final JobInfo jobInfo : g) {
                final x82 h = h(jobInfo);
                if (h != null) {
                    set.add((Object)h.b());
                }
                else {
                    d(jobScheduler, jobInfo.getId());
                }
            }
        }
        final Iterator iterator2 = d.iterator();
        while (true) {
            do {
                final boolean b2 = b;
                if (iterator2.hasNext()) {
                    continue;
                }
                if (b2) {
                    workDatabase.e();
                    try {
                        final q92 k = workDatabase.K();
                        final Iterator iterator3 = d.iterator();
                        while (iterator3.hasNext()) {
                            k.v((String)iterator3.next(), -1L);
                        }
                        workDatabase.D();
                    }
                    finally {
                        workDatabase.i();
                    }
                }
                return b2;
            } while (set.contains(iterator2.next()));
            xl0.e().a(pt1.f, "Reconciling jobs");
            final boolean b2 = true;
            continue;
        }
    }
    
    @Override
    public void a(final p92... p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_0        
        //     5: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //     8: invokespecial   be0.<init>:(Landroidx/work/impl/WorkDatabase;)V
        //    11: astore          6
        //    13: aload_1        
        //    14: arraylength    
        //    15: istore          4
        //    17: iconst_0       
        //    18: istore_2       
        //    19: iload_2        
        //    20: iload           4
        //    22: if_icmpge       340
        //    25: aload_1        
        //    26: iload_2        
        //    27: aaload         
        //    28: astore          5
        //    30: aload_0        
        //    31: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //    34: invokevirtual   androidx/room/RoomDatabase.e:()V
        //    37: aload_0        
        //    38: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //    41: invokevirtual   androidx/work/impl/WorkDatabase.K:()Lq92;
        //    44: aload           5
        //    46: getfield        p92.a:Ljava/lang/String;
        //    49: invokeinterface q92.s:(Ljava/lang/String;)Lp92;
        //    54: astore          7
        //    56: aload           7
        //    58: ifnonnull       143
        //    61: invokestatic    xl0.e:()Lxl0;
        //    64: astore          7
        //    66: getstatic       pt1.f:Ljava/lang/String;
        //    69: astore          8
        //    71: new             Ljava/lang/StringBuilder;
        //    74: astore          9
        //    76: aload           9
        //    78: invokespecial   java/lang/StringBuilder.<init>:()V
        //    81: aload           9
        //    83: ldc_w           "Skipping scheduling "
        //    86: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    89: pop            
        //    90: aload           9
        //    92: aload           5
        //    94: getfield        p92.a:Ljava/lang/String;
        //    97: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   100: pop            
        //   101: aload           9
        //   103: ldc_w           " because it's no longer in the DB"
        //   106: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   109: pop            
        //   110: aload           7
        //   112: aload           8
        //   114: aload           9
        //   116: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   119: invokevirtual   xl0.k:(Ljava/lang/String;Ljava/lang/String;)V
        //   122: aload_0        
        //   123: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   126: astore          5
        //   128: aload           5
        //   130: invokevirtual   androidx/room/RoomDatabase.D:()V
        //   133: aload_0        
        //   134: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   137: invokevirtual   androidx/room/RoomDatabase.i:()V
        //   140: goto            324
        //   143: aload           7
        //   145: getfield        p92.b:Landroidx/work/WorkInfo$State;
        //   148: getstatic       androidx/work/WorkInfo$State.ENQUEUED:Landroidx/work/WorkInfo$State;
        //   151: if_acmpeq       224
        //   154: invokestatic    xl0.e:()Lxl0;
        //   157: astore          8
        //   159: getstatic       pt1.f:Ljava/lang/String;
        //   162: astore          7
        //   164: new             Ljava/lang/StringBuilder;
        //   167: astore          9
        //   169: aload           9
        //   171: invokespecial   java/lang/StringBuilder.<init>:()V
        //   174: aload           9
        //   176: ldc_w           "Skipping scheduling "
        //   179: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   182: pop            
        //   183: aload           9
        //   185: aload           5
        //   187: getfield        p92.a:Ljava/lang/String;
        //   190: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   193: pop            
        //   194: aload           9
        //   196: ldc_w           " because it is no longer enqueued"
        //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   202: pop            
        //   203: aload           8
        //   205: aload           7
        //   207: aload           9
        //   209: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   212: invokevirtual   xl0.k:(Ljava/lang/String;Ljava/lang/String;)V
        //   215: aload_0        
        //   216: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   219: astore          5
        //   221: goto            128
        //   224: aload           5
        //   226: invokestatic    u92.a:(Lp92;)Lx82;
        //   229: astore          7
        //   231: aload_0        
        //   232: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   235: invokevirtual   androidx/work/impl/WorkDatabase.H:()Lgt1;
        //   238: aload           7
        //   240: invokeinterface gt1.e:(Lx82;)Lft1;
        //   245: astore          8
        //   247: aload           8
        //   249: ifnull          261
        //   252: aload           8
        //   254: getfield        ft1.c:I
        //   257: istore_3       
        //   258: goto            281
        //   261: aload           6
        //   263: aload_0        
        //   264: getfield        pt1.e:Landroidx/work/a;
        //   267: invokevirtual   androidx/work/a.i:()I
        //   270: aload_0        
        //   271: getfield        pt1.e:Landroidx/work/a;
        //   274: invokevirtual   androidx/work/a.g:()I
        //   277: invokevirtual   be0.e:(II)I
        //   280: istore_3       
        //   281: aload           8
        //   283: ifnonnull       308
        //   286: aload           7
        //   288: iload_3        
        //   289: invokestatic    it1.a:(Lx82;I)Lft1;
        //   292: astore          7
        //   294: aload_0        
        //   295: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   298: invokevirtual   androidx/work/impl/WorkDatabase.H:()Lgt1;
        //   301: aload           7
        //   303: invokeinterface gt1.g:(Lft1;)V
        //   308: aload_0        
        //   309: aload           5
        //   311: iload_3        
        //   312: invokevirtual   pt1.j:(Lp92;I)V
        //   315: aload_0        
        //   316: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   319: astore          5
        //   321: goto            128
        //   324: iinc            2, 1
        //   327: goto            19
        //   330: astore_1       
        //   331: aload_0        
        //   332: getfield        pt1.d:Landroidx/work/impl/WorkDatabase;
        //   335: invokevirtual   androidx/room/RoomDatabase.i:()V
        //   338: aload_1        
        //   339: athrow         
        //   340: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  37     56     330    340    Any
        //  61     128    330    340    Any
        //  128    133    330    340    Any
        //  143    221    330    340    Any
        //  224    247    330    340    Any
        //  252    258    330    340    Any
        //  261    281    330    340    Any
        //  286    308    330    340    Any
        //  308    321    330    340    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean b() {
        return true;
    }
    
    @Override
    public void e(final String s) {
        final List f = f(this.a, this.b, s);
        if (f != null && !f.isEmpty()) {
            final Iterator iterator = f.iterator();
            while (iterator.hasNext()) {
                d(this.b, (int)iterator.next());
            }
            this.d.H().i(s);
        }
    }
    
    public void j(p92 ex, int size) {
        final JobInfo a = this.c.a((p92)ex, size);
        final xl0 e = xl0.e();
        final String f = pt1.f;
        final StringBuilder sb = new StringBuilder();
        sb.append("Scheduling work ID ");
        sb.append(((p92)ex).a);
        sb.append("Job ID ");
        sb.append(size);
        e.a(f, sb.toString());
        final int n = 0;
        try {
            if (this.b.schedule(a) == 0) {
                final xl0 e2 = xl0.e();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to schedule work ID ");
                sb2.append(((p92)ex).a);
                e2.k(f, sb2.toString());
                if (((p92)ex).q && ((p92)ex).r == OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST) {
                    ((p92)ex).q = false;
                    xl0.e().a(f, String.format("Scheduling a non-expedited job (work ID %s)", ((p92)ex).a));
                    this.j((p92)ex, size);
                }
            }
        }
        catch (IllegalStateException ex) {
            final List g = g(this.a, this.b);
            size = n;
            if (g != null) {
                size = g.size();
            }
            final String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", size, this.d.K().r().size(), this.e.h());
            xl0.e().c(pt1.f, format);
            ex = new IllegalStateException(format, ex);
            final dl l = this.e.l();
            if (l == null) {
                throw ex;
            }
            l.accept(ex);
        }
        finally {
            final xl0 e3 = xl0.e();
            final String f2 = pt1.f;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to schedule ");
            sb3.append(ex);
            final Throwable t;
            e3.d(f2, sb3.toString(), t);
        }
    }
}

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

// 
// Decompiled by Procyon v0.6.0
// 

public final class t00
{
    public static final Pattern b;
    public static final t00 c;
    public final s00 a;
    
    static {
        b = Pattern.compile("[~*/\\[\\]]");
        c = new t00(s00.b);
    }
    
    public t00(final List list) {
        this.a = s00.p(list);
    }
    
    public t00(final s00 a) {
        this.a = a;
    }
    
    public static t00 a(final String s) {
        k71.c(s, "Provided field path must not be null.");
        k71.a(t00.b.matcher(s).find() ^ true, "Use FieldPath.of() for field names containing '~*/[]'.", new Object[0]);
        try {
            return c(s.split("\\.", -1));
        }
        catch (final IllegalArgumentException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid field path (");
            sb.append(s);
            sb.append("). Paths must not be empty, begin with '.', end with '.', or contain '..'");
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    public static t00 c(final String... a) {
        k71.a(a.length > 0, "Invalid field path. Provided path must not be empty.", new Object[0]);
        int i = 0;
        while (i < a.length) {
            final String s = a[i];
            final boolean b = s != null && !s.isEmpty();
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid field name at argument ");
            ++i;
            sb.append(i);
            sb.append(". Field names must not be null or empty.");
            k71.a(b, sb.toString(), new Object[0]);
        }
        return new t00(Arrays.asList(a));
    }
    
    public s00 b() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && t00.class == o.getClass() && this.a.equals(((t00)o).a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
}

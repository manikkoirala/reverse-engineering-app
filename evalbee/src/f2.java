import android.widget.AbsListView;
import java.util.List;
import java.util.Collections;
import android.os.AsyncTask;
import java.io.Serializable;
import com.ekodroid.omrevaluator.exams.ExamDetailActivity;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.view.Menu;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.widget.AbsListView$MultiChoiceModeListener;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.Published;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamPartialResponse;
import java.util.Set;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.serializable.SharedType;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.activities.Services.ArchiveSyncService;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.google.firebase.auth.FirebaseAuth;
import android.content.IntentFilter;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.exams.archived.ArchiveExamsActivity;
import com.ekodroid.omrevaluator.templateui.createtemplate.SetTemplateActivity;
import android.view.View$OnClickListener;
import android.view.LayoutInflater;
import android.os.Bundle;
import java.io.FileNotFoundException;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamsPendingActionFile;
import com.ekodroid.omrevaluator.clients.SyncClients.DeleteSyncExams;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.provider.Settings$Secure;
import java.io.IOException;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamDataV1;
import com.google.android.gms.common.util.IOUtils;
import android.net.Uri;
import java.io.InputStream;
import android.widget.ListAdapter;
import java.util.Iterator;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SyncExamsFile;
import android.view.ActionMode;
import android.content.BroadcastReceiver;
import android.widget.TextView;
import java.util.ArrayList;
import android.widget.ListView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import androidx.fragment.app.Fragment;

// 
// Decompiled by Procyon v0.6.0
// 

public class f2 extends Fragment
{
    public Context a;
    public View b;
    public ViewGroup c;
    public SwipeRefreshLayout d;
    public ListView e;
    public ArrayList f;
    public d3 g;
    public TextView h;
    public BroadcastReceiver i;
    public BroadcastReceiver j;
    public ActionMode k;
    public boolean l;
    public String m;
    public int n;
    
    public f2() {
        this.f = new ArrayList();
        this.l = false;
        this.m = "All";
    }
    
    public static /* synthetic */ String i(final f2 f2, final String m) {
        return f2.m = m;
    }
    
    public static /* synthetic */ int n(final f2 f2, final int n) {
        return f2.n = n;
    }
    
    public final void A() {
        this.j = new BroadcastReceiver(this) {
            public final f2 a;
            
            public void onReceive(final Context context, final Intent intent) {
                this.a.l = a91.c(context);
                this.a.new q(null).execute((Object[])new Void[0]);
                final f2 a = this.a;
                if (a.l) {
                    a.H(false);
                }
            }
        };
        this.i = new BroadcastReceiver(this) {
            public final f2 a;
            
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getBooleanExtra("data_changed", false)) {
                    this.a.B();
                    return;
                }
                final String stringExtra = intent.getStringExtra("file_id");
                final int intExtra = intent.getIntExtra("progress", 0);
                while (true) {
                    for (final ay o : this.a.f) {
                        if (stringExtra.equals(o.a())) {
                            if (o != null) {
                                if (intExtra > 95) {
                                    this.a.f.remove(o);
                                }
                                else {
                                    o.g(intExtra);
                                }
                                this.a.g.notifyDataSetChanged();
                            }
                            return;
                        }
                    }
                    ay o = null;
                    continue;
                }
            }
        };
    }
    
    public final void B() {
        if (this.l && (System.currentTimeMillis() > a91.j(this.a) + 180000L || a91.i(this.a) != null)) {
            this.H(false);
        }
        new q(null).execute((Object[])new Void[0]);
    }
    
    public final void C() {
        final int int1 = this.a.getSharedPreferences("MyPref", 0).getInt("ARCHIVE_COUNT", 0);
        if (int1 > 0) {
            final TextView h = this.h;
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(int1);
            h.setText((CharSequence)sb.toString());
        }
        else {
            this.h.setText((CharSequence)"");
        }
        if (this.f != null) {
            ((View)this.e).setVisibility(0);
            final d3 d3 = new d3(this.a, this.f, true);
            this.g = d3;
            this.e.setAdapter((ListAdapter)d3);
            if (this.n > 0 && this.w(this.a) + 2880 < a91.n()) {
                this.a.getSharedPreferences("MyPref", 0).edit().putInt("LAST_ARCHIVE_ALERT", a91.n()).commit();
                final y01 y01 = new y01(this) {
                    public final f2 a;
                    
                    @Override
                    public void a(final Object o) {
                    }
                };
                final Context a = this.a;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(this.n);
                sb2.append(" ");
                sb2.append(this.getString(2131886522));
                xs.d(a, y01, 2131886443, sb2.toString(), 2131886679, 0, 0, 2131230932);
            }
        }
        else {
            ((View)this.e).setVisibility(4);
        }
    }
    
    public final void D(InputStream a, final Uri uri) {
        try {
            a = (Exception)yx.a(IOUtils.toByteArray((InputStream)a));
            if (a == null) {
                this.E(2131886282, 2131886515);
                return;
            }
            this.F((ExamDataV1)a);
            return;
        }
        catch (final Exception a) {}
        catch (final IOException ex) {}
        a.printStackTrace();
    }
    
    public final void E(final int n, final int n2) {
        xs.c(this.a, null, 0, n2, 2131886176, 0, 0, 0);
    }
    
    public final void F(final ExamDataV1 examDataV1) {
        new ti1(this.a, new y01(this, examDataV1) {
            public final ExamDataV1 a;
            public final f2 b;
            
            @Override
            public void a(final Object o) {
                final ExamId examId = (ExamId)o;
                final String string = Settings$Secure.getString(this.b.a.getContentResolver(), "android_id");
                final FirebaseAnalytics a = o4.a(this.b.a);
                final StringBuilder sb = new StringBuilder();
                sb.append("Q ");
                sb.append(this.a.getSheetTemplate2().getAnswerOptions().size() - 1);
                o4.b(a, "EXAM_IMPORT", string, sb.toString());
                xu1.c(this.b.a, examId, this.a);
                this.b.B();
            }
        }, examDataV1.getSheetTemplate2(), examDataV1.getExamId()).show();
    }
    
    public final void G() {
        xs.c(this.a, new y01(this) {
            public final f2 a;
            
            @Override
            public void a(final Object o) {
                this.a.x();
            }
        }, 2131886352, 2131886508, 2131886356, 2131886163, 0, 2131230964);
    }
    
    public final void H(final boolean b) {
        final ExamsPendingActionFile i = a91.i(this.a);
        if (i == null) {
            this.v(b);
        }
        else {
            new DeleteSyncExams(i, this.a, new zg(this, b) {
                public final boolean a;
                public final f2 b;
                
                @Override
                public void a(final boolean b, final int n, final Object o) {
                    if (b) {
                        a91.I(this.b.a, null);
                        this.b.v(this.a);
                    }
                }
            });
        }
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 4 && n2 == -1) {
            final Uri data = intent.getData();
            try {
                this.D(this.a.getContentResolver().openInputStream(data), data);
            }
            catch (final FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void onAttach(final Context a) {
        super.onAttach(a);
        this.a = a;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.b = layoutInflater.inflate(2131493004, viewGroup, false);
        this.z();
        this.y();
        this.h = (TextView)this.b.findViewById(2131297313);
        this.b.findViewById(2131296411).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final f2 a;
            
            public void onClick(final View view) {
                this.a.startActivity(new Intent(this.a.a, (Class)SetTemplateActivity.class));
            }
        });
        this.b.findViewById(2131296453).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final f2 a;
            
            public void onClick(final View view) {
                this.a.G();
            }
        });
        this.b.findViewById(2131296735).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final f2 a;
            
            public void onClick(final View view) {
                this.a.startActivity(new Intent(this.a.a, (Class)ArchiveExamsActivity.class));
            }
        });
        this.A();
        final OrgProfile instance = OrgProfile.getInstance(this.a);
        if (instance != null && instance.getRequest() != null) {
            new mg0(this.a, null);
        }
        return this.b;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.a.unregisterReceiver(this.i);
        this.a.unregisterReceiver(this.j);
        final ActionMode k = this.k;
        if (k != null) {
            k.finish();
            this.k = null;
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.l = a91.c(this.a);
        this.B();
        sl.registerReceiver(this.a, this.i, new IntentFilter("UPDATE_EXAM_LIST"), 4);
        sl.registerReceiver(this.a, this.j, new IntentFilter("UPDATE_PURCHASE"), 4);
    }
    
    public final void r(final ArrayList list) {
        xs.c(this.a, new y01(this, list) {
            public final ArrayList a;
            public final f2 b;
            
            @Override
            public void a(final Object o) {
                final OrgProfile instance = OrgProfile.getInstance(this.b.a);
                final String o2 = FirebaseAuth.getInstance().e().O();
                for (int i = 0; i < this.a.size(); ++i) {
                    final TemplateRepository instance2 = TemplateRepository.getInstance(this.b.a);
                    final TemplateDataJsonModel templateJson = instance2.getTemplateJson(this.a.get(i));
                    if (templateJson != null && !instance.getOrgId().equals(o2) && templateJson.isCloudSyncOn() && !o2.equals(templateJson.getUserId())) {
                        final Context a = this.b.a;
                        final StringBuilder sb = new StringBuilder();
                        sb.append(templateJson.getName());
                        sb.append(" exam is not owned by you");
                        a91.H(a, sb.toString(), 2131230909, 2131231086);
                    }
                    else {
                        templateJson.setArchiving(true);
                        if (templateJson.getArchiveId() == null) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("");
                            sb2.append(System.currentTimeMillis());
                            templateJson.setArchiveId(sb2.toString());
                        }
                        instance2.saveOrUpdateTemplateJson(templateJson);
                        ArchiveSyncService.n(this.b.a);
                    }
                }
                this.b.B();
            }
        }, 2131886143, 2131886453, 2131886142, 2131886163, 0, 2131230932);
    }
    
    public final void s(final SyncExamsFile syncExamsFile, final boolean b) {
        final Set<Long> examIds = syncExamsFile.getExamIds();
        final TemplateRepository instance = TemplateRepository.getInstance(this.a);
        final ArrayList<TemplateDataJsonModel> allSyncedExams = instance.getAllSyncedExams();
        if (allSyncedExams.size() != examIds.size()) {
            if (allSyncedExams.size() <= examIds.size()) {
                if (allSyncedExams.size() < examIds.size() && !b) {
                    a91.J(this.a, 0L);
                    this.H(true);
                }
                return;
            }
            final OrgProfile instance2 = OrgProfile.getInstance(this.a);
            final String o = FirebaseAuth.getInstance().e().O();
            final ResultRepository instance3 = ResultRepository.getInstance(this.a);
            for (final TemplateDataJsonModel templateDataJsonModel : allSyncedExams) {
                if (!examIds.contains(templateDataJsonModel.getCloudId())) {
                    if (!instance2.getOrgId().equals(o) && !o.equals(templateDataJsonModel.getUserId())) {
                        xu1.b(this.a, templateDataJsonModel.getExamId());
                    }
                    else {
                        templateDataJsonModel.setCloudSyncOn(false);
                        templateDataJsonModel.setSynced(false);
                        templateDataJsonModel.setSharedType(null);
                        templateDataJsonModel.setUserId(null);
                        templateDataJsonModel.setSyncImages(false);
                        instance.saveOrUpdateTemplateJsonAsSynced(templateDataJsonModel);
                        for (final ResultDataJsonModel resultDataJsonModel : instance3.getAllResultJson(templateDataJsonModel.getExamId())) {
                            resultDataJsonModel.setSynced(false);
                            instance3.saveOrUpdateResultJsonAsNotSynced(resultDataJsonModel);
                        }
                    }
                }
            }
        }
        a91.J(this.a, syncExamsFile.getLastSyncedOn());
    }
    
    public final void t(final ExamId examId) {
        new dm(this.a, examId, new y01(this) {
            public final f2 a;
            
            @Override
            public void a(final Object o) {
                f2.i(this.a, (String)o);
                a91.G(this.a.a, 2131886287, 2131230939, 2131231085);
                this.a.B();
            }
        }).show();
    }
    
    public final void u(final ArrayList list) {
        new gs(this.a, new y01(this) {
            public final f2 a;
            
            @Override
            public void a(final Object o) {
                this.a.B();
            }
        }, list);
    }
    
    public final void v(final boolean b) {
        new xa0(a91.j(this.a), this.a, new zg(this, b) {
            public final boolean a;
            public final f2 b;
            
            @Override
            public void a(final boolean b, final int n, Object o) {
                if (b) {
                    final SyncExamsFile syncExamsFile = (SyncExamsFile)o;
                    final TemplateRepository instance = TemplateRepository.getInstance(this.b.a);
                    if (syncExamsFile.getExams() != null) {
                        for (final ExamPartialResponse examPartialResponse : syncExamsFile.getExams()) {
                            final TemplateDataJsonModel templateJsonForCloudId = instance.getTemplateJsonForCloudId(examPartialResponse.getId());
                            final boolean b2 = false;
                            if (templateJsonForCloudId != null && (templateJsonForCloudId.isSynced() || !templateJsonForCloudId.isCloudSyncOn())) {
                                templateJsonForCloudId.setSheetTemplate((SheetTemplate2)new gc0().j(examPartialResponse.getTemplate(), SheetTemplate2.class));
                                templateJsonForCloudId.setPublished(examPartialResponse.getPublished() == Published.PUBLISHED);
                                templateJsonForCloudId.setLastUpdatedOn(examPartialResponse.getUpdatedOn());
                                templateJsonForCloudId.setUserId(examPartialResponse.getUserId());
                                templateJsonForCloudId.setCloudSyncOn(true);
                                templateJsonForCloudId.setSyncImages(examPartialResponse.isSyncImages());
                                templateJsonForCloudId.setSharedType(examPartialResponse.getSharedType());
                                instance.saveOrUpdateTemplateJsonAsSynced(templateJsonForCloudId);
                            }
                            if (templateJsonForCloudId == null) {
                                final ExamId examId = examPartialResponse.getExamId();
                                final TemplateDataJsonModel templateDataJsonModel = new TemplateDataJsonModel(examId.getExamName(), examId.getClassName(), examId.getExamDate(), (SheetTemplate2)new gc0().j(examPartialResponse.getTemplate(), SheetTemplate2.class), b10.a(this.b.a, examId.getExamDate()), null);
                                boolean published = b2;
                                if (examPartialResponse.getPublished() == Published.PUBLISHED) {
                                    published = true;
                                }
                                templateDataJsonModel.setPublished(published);
                                templateDataJsonModel.setLastUpdatedOn(examPartialResponse.getUpdatedOn());
                                templateDataJsonModel.setCloudSyncOn(true);
                                templateDataJsonModel.setCloudId(examPartialResponse.getId());
                                templateDataJsonModel.setLastSyncedOn(0L);
                                templateDataJsonModel.setSynced(true);
                                templateDataJsonModel.setUserId(examPartialResponse.getUserId());
                                templateDataJsonModel.setSyncImages(examPartialResponse.isSyncImages());
                                templateDataJsonModel.setSharedType(examPartialResponse.getSharedType());
                                instance.deleteTemplateJson(examId);
                                instance.saveOrUpdateTemplateJsonAsSynced(templateDataJsonModel);
                            }
                        }
                    }
                    this.b.s(syncExamsFile, this.a);
                    o = new Intent("UPDATE_EXAM_LIST");
                    ((Intent)o).putExtra("data_changed", true);
                    this.b.a.sendBroadcast((Intent)o);
                }
            }
        });
    }
    
    public final int w(final Context context) {
        return context.getSharedPreferences("MyPref", 0).getInt("LAST_ARCHIVE_ALERT", 0);
    }
    
    public void x() {
        final Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        this.startActivityForResult(Intent.createChooser(intent, (CharSequence)"Choose a file"), 4);
    }
    
    public final void y() {
        ((AbsListView)this.e).setChoiceMode(3);
        ((AbsListView)this.e).setMultiChoiceModeListener((AbsListView$MultiChoiceModeListener)new AbsListView$MultiChoiceModeListener(this) {
            public final f2 a;
            
            public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
                final SparseBooleanArray a = this.a.g.a();
                final ArrayList list = new ArrayList();
                switch (menuItem.getItemId()) {
                    default: {
                        return false;
                    }
                    case 2131296708: {
                        for (int i = a.size() - 1; i >= 0; --i) {
                            if (a.valueAt(i)) {
                                list.add(((ay)this.a.f.get(a.keyAt(i))).b());
                            }
                        }
                        this.a.u(list);
                        actionMode.finish();
                        return true;
                    }
                    case 2131296707: {
                        if (a.size() == 1 && a.valueAt(0)) {
                            final f2 a2 = this.a;
                            a2.t(((ay)a2.f.get(a.keyAt(0))).b());
                        }
                        actionMode.finish();
                        return true;
                    }
                    case 2131296706: {
                        for (int j = a.size() - 1; j >= 0; --j) {
                            if (a.valueAt(j)) {
                                final ay ay = this.a.f.get(a.keyAt(j));
                                if (ay.d() > 0) {
                                    list.add(ay.b());
                                }
                                else {
                                    final Context a3 = this.a.a;
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append(this.a.a.getString(2131886524));
                                    sb.append(" ");
                                    sb.append(ay.b().getExamName());
                                    a91.H(a3, sb.toString(), 2131230909, 2131231086);
                                }
                            }
                        }
                        if (list.size() > 0) {
                            this.a.r(list);
                        }
                        actionMode.finish();
                        return true;
                    }
                }
            }
            
            public boolean onCreateActionMode(final ActionMode k, final Menu menu) {
                this.a.getActivity().getMenuInflater().inflate(2131623938, menu);
                final f2 a = this.a;
                a.k = k;
                a.b.findViewById(2131296735).setVisibility(8);
                return true;
            }
            
            public void onDestroyActionMode(final ActionMode actionMode) {
                this.a.b.findViewById(2131296735).setVisibility(0);
                this.a.g.b();
                actionMode.finish();
                this.a.k = null;
            }
            
            public void onItemCheckedStateChanged(final ActionMode actionMode, final int n, final long n2, final boolean b) {
                boolean visible = true;
                final int checkedItemCount = ((AbsListView)this.a.e).getCheckedItemCount();
                final StringBuilder sb = new StringBuilder();
                sb.append(checkedItemCount);
                sb.append(" Selected");
                actionMode.setTitle((CharSequence)sb.toString());
                final MenuItem item = actionMode.getMenu().findItem(2131296707);
                if (checkedItemCount != 1) {
                    visible = false;
                }
                item.setVisible(visible);
                this.a.g.d(n - 1);
                this.a.g.notifyDataSetChanged();
            }
            
            public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
                return false;
            }
        });
    }
    
    public final void z() {
        this.e = (ListView)this.b.findViewById(2131296835);
        (this.d = (SwipeRefreshLayout)this.b.findViewById(2131297135)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.d.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final f2 a;
            
            @Override
            public void a() {
                this.a.new q(null).execute((Object[])new Void[0]);
                final f2 a = this.a;
                if (a.l) {
                    a.H(false);
                }
            }
        });
        ((AdapterView)this.e).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final f2 a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                final ay ay = this.a.f.get(n - 1);
                view.setBackgroundResource(2131230860);
                final Intent intent = new Intent(this.a.a, (Class)ExamDetailActivity.class);
                intent.putExtra("EXAM_ID", (Serializable)ay.b());
                this.a.startActivity(intent);
            }
        });
        final ViewGroup c = (ViewGroup)this.getLayoutInflater().inflate(2131493010, (ViewGroup)null);
        this.c = c;
        this.e.addHeaderView((View)c);
    }
    
    public class q extends AsyncTask
    {
        public final f2 a;
        
        public q(final f2 a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            final ArrayList<TemplateDataJsonModel> templatesJson = TemplateRepository.getInstance(this.a.a).getTemplatesJson();
            final ArrayList list = new ArrayList();
            final ResultRepository instance = ResultRepository.getInstance(this.a.a);
            final Iterator<TemplateDataJsonModel> iterator = templatesJson.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final TemplateDataJsonModel templateDataJsonModel = iterator.next();
                final ExamId examId = new ExamId(templateDataJsonModel.getName(), templateDataJsonModel.getClassName(), templateDataJsonModel.getExamDate());
                final SheetTemplate2 sheetTemplate = templateDataJsonModel.getSheetTemplate();
                final boolean b = sheetTemplate.getAnswerKeys() != null && sheetTemplate.getAnswerKeys().length > 0 && sheetTemplate.getAnswerKeys()[0] != null;
                final int size = sheetTemplate.getAnswerOptions().size();
                final int n2 = (int)instance.getSheetImageCountForExam(examId);
                final int n3 = (int)instance.getAllResultCount(examId);
                final boolean c = a91.C(examId.getExamDate(), 30);
                int n4 = n;
                if (c) {
                    n4 = n + 1;
                }
                final boolean archiving = templateDataJsonModel.isArchiving();
                int progress;
                if (archiving) {
                    progress = templateDataJsonModel.getArchivePayload().getProgress();
                }
                else {
                    progress = 0;
                }
                list.add(new ay(examId, size - 1, n3, n2, b, archiving, c, progress, templateDataJsonModel.getArchiveId(), templateDataJsonModel.isCloudSyncOn(), templateDataJsonModel.getSharedType()));
                n = n4;
            }
            Collections.reverse(list);
            final f2 a = this.a;
            a.f = list;
            f2.n(a, n);
            return null;
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            this.a.C();
            this.a.d.setRefreshing(false);
        }
        
        public void onPreExecute() {
            this.a.d.setRefreshing(true);
        }
    }
}

import android.util.Log;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.nio.channels.Channels;
import java.util.concurrent.Callable;
import java.io.File;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class bg1 implements ts1, pr
{
    public final Context a;
    public final String b;
    public final File c;
    public final Callable d;
    public final int e;
    public final ts1 f;
    public pp g;
    public boolean h;
    
    public bg1(final Context a, final String b, final File c, final Callable d, final int e, final ts1 f) {
        fg0.e((Object)a, "context");
        fg0.e((Object)f, "delegate");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    @Override
    public ss1 F() {
        if (!this.h) {
            this.d(true);
            this.h = true;
        }
        return this.getDelegate().F();
    }
    
    public final void a(final File dest, final boolean b) {
    Label_0172_Outer:
        while (true) {
            Label_0098: {
                Callable d = null;
                Block_3: {
                    ReadableByteChannel readableByteChannel;
                    String s;
                    if (this.b != null) {
                        readableByteChannel = Channels.newChannel(this.a.getAssets().open(this.b));
                        s = "newChannel(context.assets.open(copyFromAssetPath))";
                    }
                    else if (this.c != null) {
                        readableByteChannel = new FileInputStream(this.c).getChannel();
                        s = "FileInputStream(copyFromFile).channel";
                    }
                    else {
                        d = this.d;
                        if (d != null) {
                            break Block_3;
                        }
                        throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
                    }
                    fg0.d((Object)readableByteChannel, s);
                    break Label_0098;
                }
                try {
                    final ReadableByteChannel readableByteChannel = Channels.newChannel((InputStream)d.call());
                    final String s = "newChannel(inputStream)";
                    continue Label_0172_Outer;
                    File tempFile;
                    while (true) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to create directories for ");
                        sb.append(dest.getAbsolutePath());
                        throw new IOException(sb.toString());
                        Label_0208: {
                            fg0.d((Object)tempFile, "intermediateFile");
                        }
                        this.b(tempFile, b);
                        iftrue(Label_0232:)(!tempFile.renameTo(dest));
                        return;
                        tempFile = File.createTempFile("room-copy-helper", ".tmp", this.a.getCacheDir());
                        tempFile.deleteOnExit();
                        final FileChannel channel = new FileOutputStream(tempFile).getChannel();
                        fg0.d((Object)channel, "output");
                        a10.a(readableByteChannel, channel);
                        final File parentFile = dest.getParentFile();
                        iftrue(Label_0208:)(parentFile == null || parentFile.exists() || parentFile.mkdirs());
                        continue;
                    }
                    final StringBuilder sb2;
                    Label_0232: {
                        sb2 = new StringBuilder();
                    }
                    sb2.append("Failed to move intermediate file (");
                    sb2.append(tempFile.getAbsolutePath());
                    sb2.append(") to destination (");
                    sb2.append(dest.getAbsolutePath());
                    sb2.append(").");
                    throw new IOException(sb2.toString());
                }
                catch (final Exception cause) {
                    throw new IOException("inputStreamCallable exception on call", cause);
                }
            }
            break;
        }
        throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
    }
    
    public final void b(final File file, final boolean b) {
        pp g;
        if ((g = this.g) == null) {
            fg0.t("databaseConfiguration");
            g = null;
        }
        g.getClass();
    }
    
    public final void c(final pp g) {
        fg0.e((Object)g, "databaseConfiguration");
        this.g = g;
    }
    
    @Override
    public void close() {
        synchronized (this) {
            this.getDelegate().close();
            this.h = false;
        }
    }
    
    public final void d(final boolean b) {
        final String databaseName = this.getDatabaseName();
        if (databaseName != null) {
            final File databasePath = this.a.getDatabasePath(databaseName);
            final pp g = this.g;
            final pp pp = null;
            pp pp2;
            if ((pp2 = g) == null) {
                fg0.t("databaseConfiguration");
                pp2 = null;
            }
            final boolean s = pp2.s;
            final File filesDir = this.a.getFilesDir();
            fg0.d((Object)filesDir, "context.filesDir");
            final k81 k81 = new k81(databaseName, filesDir, s);
            try {
                k81.c(k81, false, 1, null);
                if (!databasePath.exists()) {
                    try {
                        fg0.d((Object)databasePath, "databaseFile");
                        this.a(databasePath, b);
                        return;
                    }
                    catch (final IOException cause) {
                        throw new RuntimeException("Unable to copy database file.", cause);
                    }
                }
                try {
                    fg0.d((Object)databasePath, "databaseFile");
                    final int c = bp.c(databasePath);
                    if (c == this.e) {
                        return;
                    }
                    pp g2 = this.g;
                    if (g2 == null) {
                        fg0.t("databaseConfiguration");
                        g2 = pp;
                    }
                    if (g2.a(c, this.e)) {
                        return;
                    }
                    if (this.a.deleteDatabase(databaseName)) {
                        try {
                            this.a(databasePath, b);
                        }
                        catch (final IOException ex) {
                            Log.w("ROOM", "Unable to copy database file.", (Throwable)ex);
                        }
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to delete database file (");
                        sb.append(databaseName);
                        sb.append(") for a copy destructive migration.");
                        Log.w("ROOM", sb.toString());
                    }
                    return;
                }
                catch (final IOException ex2) {
                    Log.w("ROOM", "Unable to read database version.", (Throwable)ex2);
                    return;
                }
            }
            finally {
                k81.d();
            }
        }
        throw new IllegalStateException("Required value was null.".toString());
    }
    
    @Override
    public String getDatabaseName() {
        return this.getDelegate().getDatabaseName();
    }
    
    @Override
    public ts1 getDelegate() {
        return this.f;
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.getDelegate().setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
}

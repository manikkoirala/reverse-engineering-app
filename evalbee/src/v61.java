import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.DateFormat;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v61
{
    public static String a(final int i) {
        if (i == 0) {
            return "EEEE, MMMM d, yyyy";
        }
        if (i == 1) {
            return "MMMM d, yyyy";
        }
        if (i == 2) {
            return "MMM d, yyyy";
        }
        if (i == 3) {
            return "M/d/yy";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown DateFormat style: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static String b(final int i) {
        if (i == 0 || i == 1) {
            return "h:mm:ss a z";
        }
        if (i == 2) {
            return "h:mm:ss a";
        }
        if (i == 3) {
            return "h:mm a";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown DateFormat style: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static DateFormat c(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(a(n));
        sb.append(" ");
        sb.append(b(n2));
        return new SimpleDateFormat(sb.toString(), Locale.US);
    }
}

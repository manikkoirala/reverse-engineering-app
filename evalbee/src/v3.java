import android.widget.AdapterView;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import com.ekodroid.omrevaluator.templateui.models.PartialAnswerOptionKey;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.Arrays;
import android.view.View;
import android.content.SharedPreferences;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class v3 extends u80
{
    public Context a;
    public k5.f b;
    public AnswerOption c;
    public AnswerOptionKey d;
    public LabelProfile e;
    public ArrayAdapter f;
    public Double[] g;
    public Double[] h;
    public k5 i;
    public ArrayList j;
    public LinearLayout k;
    public Button l;
    public y01 m;
    public View$OnClickListener n;
    public SharedPreferences p;
    
    public v3(final Context a, final k5.f b, final AnswerOption c, final AnswerOptionKey d, final LabelProfile e, final y01 m) {
        super(a);
        this.g = new Double[] { 0.25, 0.33, 0.5, 0.67, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0 };
        this.h = new Double[] { -5.0, -4.0, -3.0, -2.5, -2.0, -1.66, -1.5, -1.33, -1.0, -0.83, -0.66, -0.5, -0.33, -0.25, 0.0 };
        this.j = new ArrayList();
        this.n = (View$OnClickListener)new View$OnClickListener(this) {
            public final v3 a;
            
            public void onClick(final View view) {
                ((View)this.a.l).setEnabled(true);
                final v3 a = this.a;
                ((View)a.l).setBackgroundColor(sl.getColor(a.a, 2131099724));
                this.a.j.remove(((po)view).getIndex());
                final v3 a2 = this.a;
                a2.d(a2.j, a2.k);
            }
        };
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.m = m;
        if (d.getPartialAnswerOptionKeys() != null) {
            this.j = d.getPartialAnswerOptionKeys();
        }
    }
    
    public final ArrayList b(final String s, final Double[] a) {
        final ArrayList h = a91.h(s);
        h.addAll(Arrays.asList(a));
        Collections.sort((List<Comparable>)h);
        final ArrayList list = new ArrayList();
        for (final Double n : h) {
            if (!list.contains(n)) {
                list.add(n);
            }
        }
        return list;
    }
    
    public final String c(final PartialAnswerOptionKey partialAnswerOptionKey, final LabelProfile labelProfile) {
        StringBuilder sb;
        String str;
        if (partialAnswerOptionKey.getPartialType() != null) {
            sb = new StringBuilder();
            sb.append("");
            sb.append(partialAnswerOptionKey.getPartialType());
            str = " - ";
        }
        else {
            sb = new StringBuilder();
            sb.append("");
            str = "Exact - ";
        }
        sb.append(str);
        final String string = sb.toString();
        final String t = e5.t(partialAnswerOptionKey, labelProfile);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(t);
        sb2.append(" : ");
        sb2.append(partialAnswerOptionKey.getPartialMarks());
        return sb2.toString();
    }
    
    public final void d(final ArrayList list, final LinearLayout linearLayout) {
        if (list != null) {
            ((ViewGroup)linearLayout).removeAllViews();
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, -2);
            final int d = a91.d(16, this.a);
            int i = 0;
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, 0, 0, 0);
            linearLayout$LayoutParams.weight = 1.0f;
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(a91.d(32, this.a), a91.d(32, this.a));
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(0, 0, a91.d(12, this.a), 0);
            while (i < list.size()) {
                final LinearLayout b = yo.b(this.a);
                ((View)b).setBackground(g7.b(this.a, 2131230848));
                ((ViewGroup)b).addView((View)yo.c(this.a, this.c(list.get(i), this.e), 150), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                final po po = new po(this.a);
                po.setIndex(i);
                ((View)po).setBackground(g7.b(this.a, 2131230862));
                ((View)po).setOnClickListener(this.n);
                ((View)po).setPadding(a91.d(5, this.a), a91.d(5, this.a), a91.d(5, this.a), a91.d(5, this.a));
                ((ViewGroup)b).addView((View)po, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
                ((ViewGroup)linearLayout).addView((View)b);
                ++i;
            }
        }
    }
    
    public final void e() {
        ((Toolbar)this.findViewById(2131297352)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final v3 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492960);
        this.e();
        this.p = this.a.getSharedPreferences("MyPref", 0);
        final ArrayList list = new ArrayList();
        final ArrayList b = this.b(this.p.getString("minus_mark_json", ""), this.h);
        for (int i = 0; i < b.size(); ++i) {
            if ((double)b.get(i) >= this.c.incorrectMarks) {
                list.add(b.get(i));
            }
        }
        final ArrayList b2 = this.b(this.p.getString("plus_mark_json", ""), this.g);
        for (int j = 0; j < b2.size(); ++j) {
            if ((double)b2.get(j) <= this.c.correctMarks) {
                list.add(b2.get(j));
            }
        }
        final ArrayList list2 = new ArrayList();
        Label_0261: {
            String e;
            if (this.c.type == AnswerOption.AnswerOptionType.DECIMAL) {
                e = ok.N;
            }
            else {
                list2.add(ok.M);
                list2.add(ok.O);
                if (this.c.type != AnswerOption.AnswerOptionType.MATRIX) {
                    break Label_0261;
                }
                e = ok.P;
            }
            list2.add(e);
        }
        this.f = new ArrayAdapter(this.a, 2131493104, (List)list);
        final ArrayAdapter adapter = new ArrayAdapter(this.a, 2131493104, (List)list2);
        final Spinner spinner = (Spinner)this.findViewById(2131297090);
        final Spinner spinner2 = (Spinner)this.findViewById(2131297091);
        spinner2.setAdapter((SpinnerAdapter)adapter);
        final TextView textView = (TextView)this.findViewById(2131297227);
        final TableLayout tableLayout = (TableLayout)this.findViewById(2131297147);
        final LinearLayout k = (LinearLayout)this.findViewById(2131296778);
        this.k = k;
        this.d(this.j, k);
        ((View)(this.l = (Button)this.findViewById(2131296403))).setEnabled(false);
        spinner.setAdapter((SpinnerAdapter)this.f);
        if (this.c.type == AnswerOption.AnswerOptionType.NUMARICAL) {
            this.findViewById(2131296798).setVisibility(8);
        }
        (this.i = new k5(this.a, this.d, null, null, this.e)).p();
        final Iterator iterator = this.i.m().iterator();
        while (iterator.hasNext()) {
            tableLayout.addView((View)iterator.next());
        }
        this.findViewById(2131296407).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, textView, spinner2, spinner) {
            public final TextView a;
            public final Spinner b;
            public final Spinner c;
            public final v3 d;
            
            public void onClick(final View view) {
                final boolean[] k = this.d.i.k();
                this.a.setText((CharSequence)"");
                final boolean equals = ((String)((AdapterView)this.b).getSelectedItem()).equals(ok.O);
                int n = 0;
                int n2 = 0;
                if (equals) {
                    final Iterator iterator = this.d.j.iterator();
                    while (true) {
                        n = n2;
                        if (!iterator.hasNext()) {
                            break;
                        }
                        final PartialAnswerOptionKey partialAnswerOptionKey = (PartialAnswerOptionKey)iterator.next();
                        if (partialAnswerOptionKey.getPartialType() == null || !partialAnswerOptionKey.getPartialType().equals(ok.O)) {
                            continue;
                        }
                        partialAnswerOptionKey.setMarkedValues(k);
                        partialAnswerOptionKey.setPartialMarks((double)((AdapterView)this.c).getSelectedItem());
                        a91.G(this.d.a, 2131886531, 2131230909, 2131231086);
                        n2 = 1;
                    }
                }
                int n3 = n;
                if (((String)((AdapterView)this.b).getSelectedItem()).equals(ok.P)) {
                    final Iterator iterator2 = this.d.j.iterator();
                    while (true) {
                        n3 = n;
                        if (!iterator2.hasNext()) {
                            break;
                        }
                        final PartialAnswerOptionKey partialAnswerOptionKey2 = (PartialAnswerOptionKey)iterator2.next();
                        if (partialAnswerOptionKey2.getPartialType() == null || !partialAnswerOptionKey2.getPartialType().equals(ok.P)) {
                            continue;
                        }
                        partialAnswerOptionKey2.setMarkedValues(k);
                        partialAnswerOptionKey2.setPartialMarks((double)((AdapterView)this.c).getSelectedItem());
                        a91.G(this.d.a, 2131886532, 2131230909, 2131231086);
                        n = 1;
                    }
                }
                if (n3 == 0) {
                    final String l = this.d.i.l();
                    final v3 d = this.d;
                    final AnswerOption c = d.c;
                    final AnswerOption.AnswerOptionType type = c.type;
                    if (type == AnswerOption.AnswerOptionType.DECIMAL && l == null) {
                        a91.H(d.a, "Please enter valid range", 2131230909, 2131231086);
                        return;
                    }
                    this.d.j.add(new PartialAnswerOptionKey(type, c.questionNumber, k, c.subjectId, c.sectionId, (double)((AdapterView)this.c).getSelectedItem(), this.d.c.payload, l, (String)((AdapterView)this.b).getSelectedItem()));
                }
                ((View)this.d.l).setEnabled(true);
                this.d.i.p();
                final v3 d2 = this.d;
                d2.d(d2.j, d2.k);
            }
        });
        this.findViewById(2131296391).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final v3 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        ((View)this.l).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final v3 a;
            
            public void onClick(final View view) {
                final v3 a = this.a;
                final k5.f b = a.b;
                if (b != null) {
                    b.a(new c21(a.c.questionNumber, true, null, null, a.j));
                }
                final y01 m = this.a.m;
                if (m != null) {
                    m.a(null);
                }
                this.a.dismiss();
            }
        });
    }
}

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Collections;

// 
// Decompiled by Procyon v0.6.0
// 

public class fq1
{
    public String a;
    public o30 b;
    public jq1 c;
    public String d;
    public String e;
    public c f;
    public String g;
    public String h;
    public String i;
    public long j;
    public String k;
    public c l;
    public c m;
    public c n;
    public c o;
    public c p;
    
    public fq1() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = fq1.c.c("");
        this.g = null;
        this.h = null;
        this.i = null;
        this.k = null;
        this.l = fq1.c.c("");
        this.m = fq1.c.c("");
        this.n = fq1.c.c("");
        this.o = fq1.c.c("");
        this.p = fq1.c.c(Collections.emptyMap());
    }
    
    public fq1(final fq1 fq1, final boolean b) {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = fq1.c.c("");
        this.g = null;
        this.h = null;
        this.i = null;
        this.k = null;
        this.l = fq1.c.c("");
        this.m = fq1.c.c("");
        this.n = fq1.c.c("");
        this.o = fq1.c.c("");
        this.p = fq1.c.c(Collections.emptyMap());
        Preconditions.checkNotNull(fq1);
        this.a = fq1.a;
        this.b = fq1.b;
        this.c = fq1.c;
        this.d = fq1.d;
        this.f = fq1.f;
        this.l = fq1.l;
        this.m = fq1.m;
        this.n = fq1.n;
        this.o = fq1.o;
        this.p = fq1.p;
        if (b) {
            this.k = fq1.k;
            this.j = fq1.j;
            this.i = fq1.i;
            this.h = fq1.h;
            this.g = fq1.g;
            this.e = fq1.e;
        }
    }
    
    public static /* synthetic */ c a(final fq1 fq1, final c o) {
        return fq1.o = o;
    }
    
    public static /* synthetic */ jq1 b(final fq1 fq1, final jq1 c) {
        return fq1.c = c;
    }
    
    public static /* synthetic */ c c(final fq1 fq1, final c n) {
        return fq1.n = n;
    }
    
    public static /* synthetic */ c d(final fq1 fq1, final c m) {
        return fq1.m = m;
    }
    
    public static /* synthetic */ c e(final fq1 fq1, final c l) {
        return fq1.l = l;
    }
    
    public static /* synthetic */ c f(final fq1 fq1) {
        return fq1.p;
    }
    
    public static /* synthetic */ c g(final fq1 fq1, final c p2) {
        return fq1.p = p2;
    }
    
    public static /* synthetic */ c h(final fq1 fq1, final c f) {
        return fq1.f = f;
    }
    
    public static /* synthetic */ String i(final fq1 fq1, final String e) {
        return fq1.e = e;
    }
    
    public static /* synthetic */ String j(final fq1 fq1, final String a) {
        return fq1.a = a;
    }
    
    public static /* synthetic */ String k(final fq1 fq1, final String d) {
        return fq1.d = d;
    }
    
    public static /* synthetic */ String l(final fq1 fq1, final String g) {
        return fq1.g = g;
    }
    
    public static /* synthetic */ String m(final fq1 fq1, final String h) {
        return fq1.h = h;
    }
    
    public static /* synthetic */ String n(final fq1 fq1, final String i) {
        return fq1.i = i;
    }
    
    public static /* synthetic */ long o(final fq1 fq1, final long j) {
        return fq1.j = j;
    }
    
    public static /* synthetic */ String p(final fq1 fq1, final String k) {
        return fq1.k = k;
    }
    
    public JSONObject q() {
        final HashMap hashMap = new HashMap();
        if (this.f.b()) {
            hashMap.put("contentType", this.v());
        }
        if (this.p.b()) {
            hashMap.put("metadata", new JSONObject((Map)this.p.a()));
        }
        if (this.l.b()) {
            hashMap.put("cacheControl", this.r());
        }
        if (this.m.b()) {
            hashMap.put("contentDisposition", this.s());
        }
        if (this.n.b()) {
            hashMap.put("contentEncoding", this.t());
        }
        if (this.o.b()) {
            hashMap.put("contentLanguage", this.u());
        }
        return new JSONObject((Map)hashMap);
    }
    
    public String r() {
        return (String)this.l.a();
    }
    
    public String s() {
        return (String)this.m.a();
    }
    
    public String t() {
        return (String)this.n.a();
    }
    
    public String u() {
        return (String)this.o.a();
    }
    
    public String v() {
        return (String)this.f.a();
    }
    
    public static class b
    {
        public fq1 a;
        public boolean b;
        
        public b(final JSONObject jsonObject) {
            this.a = new fq1();
            if (jsonObject != null) {
                this.c(jsonObject);
                this.b = true;
            }
        }
        
        public b(final JSONObject jsonObject, final jq1 jq1) {
            this(jsonObject);
            fq1.b(this.a, jq1);
        }
        
        public fq1 a() {
            return new fq1(this.a, this.b, null);
        }
        
        public final String b(final JSONObject jsonObject, final String s) {
            if (jsonObject.has(s) && !jsonObject.isNull(s)) {
                return jsonObject.getString(s);
            }
            return null;
        }
        
        public final void c(final JSONObject jsonObject) {
            fq1.i(this.a, jsonObject.optString("generation"));
            fq1.j(this.a, jsonObject.optString("name"));
            fq1.k(this.a, jsonObject.optString("bucket"));
            fq1.l(this.a, jsonObject.optString("metageneration"));
            fq1.m(this.a, jsonObject.optString("timeCreated"));
            fq1.n(this.a, jsonObject.optString("updated"));
            fq1.o(this.a, jsonObject.optLong("size"));
            fq1.p(this.a, jsonObject.optString("md5Hash"));
            if (jsonObject.has("metadata") && !jsonObject.isNull("metadata")) {
                final JSONObject jsonObject2 = jsonObject.getJSONObject("metadata");
                final Iterator keys = jsonObject2.keys();
                while (keys.hasNext()) {
                    final String s = keys.next();
                    this.i(s, jsonObject2.getString(s));
                }
            }
            final String b = this.b(jsonObject, "contentType");
            if (b != null) {
                this.h(b);
            }
            final String b2 = this.b(jsonObject, "cacheControl");
            if (b2 != null) {
                this.d(b2);
            }
            final String b3 = this.b(jsonObject, "contentDisposition");
            if (b3 != null) {
                this.e(b3);
            }
            final String b4 = this.b(jsonObject, "contentEncoding");
            if (b4 != null) {
                this.f(b4);
            }
            final String b5 = this.b(jsonObject, "contentLanguage");
            if (b5 != null) {
                this.g(b5);
            }
        }
        
        public b d(final String s) {
            fq1.e(this.a, fq1.c.d(s));
            return this;
        }
        
        public b e(final String s) {
            fq1.d(this.a, fq1.c.d(s));
            return this;
        }
        
        public b f(final String s) {
            fq1.c(this.a, fq1.c.d(s));
            return this;
        }
        
        public b g(final String s) {
            fq1.a(this.a, fq1.c.d(s));
            return this;
        }
        
        public b h(final String s) {
            fq1.h(this.a, fq1.c.d(s));
            return this;
        }
        
        public b i(final String s, final String s2) {
            if (!fq1.f(this.a).b()) {
                fq1.g(this.a, fq1.c.d(new HashMap()));
            }
            ((Map)fq1.f(this.a).a()).put(s, s2);
            return this;
        }
    }
    
    public static class c
    {
        public final boolean a;
        public final Object b;
        
        public c(final Object b, final boolean a) {
            this.a = a;
            this.b = b;
        }
        
        public static c c(final Object o) {
            return new c(o, false);
        }
        
        public static c d(final Object o) {
            return new c(o, true);
        }
        
        public Object a() {
            return this.b;
        }
        
        public boolean b() {
            return this.a;
        }
    }
}

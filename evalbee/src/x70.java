import java.util.Collection;
import com.google.common.collect.Sets;
import com.google.common.collect.Maps;
import java.util.Iterator;
import com.google.common.collect.Iterators;
import java.util.Set;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class x70 extends a80 implements Map
{
    @Override
    public void clear() {
        this.delegate().clear();
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.delegate().containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.delegate().containsValue(o);
    }
    
    @Override
    public abstract Map delegate();
    
    @Override
    public Set<Entry<Object, Object>> entrySet() {
        return this.delegate().entrySet();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || this.delegate().equals(o);
    }
    
    @Override
    public Object get(final Object o) {
        return this.delegate().get(o);
    }
    
    @Override
    public int hashCode() {
        return this.delegate().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.delegate().isEmpty();
    }
    
    @Override
    public Set<Object> keySet() {
        return this.delegate().keySet();
    }
    
    @Override
    public Object put(final Object o, final Object o2) {
        return this.delegate().put(o, o2);
    }
    
    @Override
    public void putAll(final Map<Object, Object> map) {
        this.delegate().putAll(map);
    }
    
    @Override
    public Object remove(final Object o) {
        return this.delegate().remove(o);
    }
    
    @Override
    public int size() {
        return this.delegate().size();
    }
    
    public void standardClear() {
        Iterators.d(this.entrySet().iterator());
    }
    
    public boolean standardContainsKey(final Object o) {
        return Maps.g(this, o);
    }
    
    public boolean standardContainsValue(final Object o) {
        return Maps.h(this, o);
    }
    
    public boolean standardEquals(final Object o) {
        return Maps.i(this, o);
    }
    
    public int standardHashCode() {
        return Sets.b(this.entrySet());
    }
    
    public boolean standardIsEmpty() {
        return this.entrySet().iterator().hasNext() ^ true;
    }
    
    public void standardPutAll(final Map<Object, Object> map) {
        Maps.t(this, map);
    }
    
    public Object standardRemove(Object value) {
        final Iterator<Entry<Object, Object>> iterator = this.entrySet().iterator();
        while (iterator.hasNext()) {
            final Entry<Object, V> entry = (Entry<Object, V>)iterator.next();
            if (b11.a(entry.getKey(), value)) {
                value = entry.getValue();
                iterator.remove();
                return value;
            }
        }
        return null;
    }
    
    public String standardToString() {
        return Maps.y(this);
    }
    
    @Override
    public Collection<Object> values() {
        return this.delegate().values();
    }
}

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ra2 implements s30
{
    public static final Parcelable$Creator<ra2> CREATOR;
    public long a;
    public long b;
    
    static {
        CREATOR = (Parcelable$Creator)new xa2();
    }
    
    public ra2(final long a, final long b) {
        this.a = a;
        this.b = b;
    }
    
    public static ra2 e(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        try {
            return new ra2(jsonObject.getLong("lastSignInTimestamp"), jsonObject.getLong("creationTimestamp"));
        }
        catch (final JSONException ex) {
            return null;
        }
    }
    
    public final long b() {
        return this.b;
    }
    
    public final long c() {
        return this.a;
    }
    
    public final JSONObject d() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("lastSignInTimestamp", this.a);
            jsonObject.put("creationTimestamp", this.b);
            return jsonObject;
        }
        catch (final JSONException ex) {
            return jsonObject;
        }
    }
    
    public final int describeContents() {
        return 0;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeLong(parcel, 1, this.c());
        SafeParcelWriter.writeLong(parcel, 2, this.b());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

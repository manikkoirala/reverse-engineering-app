import com.google.firebase.sessions.EventType;

// 
// Decompiled by Procyon v0.6.0
// 

public final class xl1
{
    public final EventType a;
    public final am1 b;
    public final y7 c;
    
    public xl1(final EventType a, final am1 b, final y7 c) {
        fg0.e((Object)a, "eventType");
        fg0.e((Object)b, "sessionData");
        fg0.e((Object)c, "applicationInfo");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public final y7 a() {
        return this.c;
    }
    
    public final EventType b() {
        return this.a;
    }
    
    public final am1 c() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof xl1)) {
            return false;
        }
        final xl1 xl1 = (xl1)o;
        return this.a == xl1.a && fg0.a((Object)this.b, (Object)xl1.b) && fg0.a((Object)this.c, (Object)xl1.c);
    }
    
    @Override
    public int hashCode() {
        return (this.a.hashCode() * 31 + this.b.hashCode()) * 31 + this.c.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SessionEvent(eventType=");
        sb.append(this.a);
        sb.append(", sessionData=");
        sb.append(this.b);
        sb.append(", applicationInfo=");
        sb.append(this.c);
        sb.append(')');
        return sb.toString();
    }
}

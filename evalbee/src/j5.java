import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.serializable.SheetImageConfigure;
import android.graphics.Bitmap;

// 
// Decompiled by Procyon v0.6.0
// 

public class j5
{
    public Bitmap a;
    public SheetImageConfigure b;
    public SheetTemplate2 c;
    
    public j5(final Bitmap a, final SheetImageConfigure b, final SheetTemplate2 c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}

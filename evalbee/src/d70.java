import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import android.graphics.Rect;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d70
{
    public static boolean a(final int n, final Rect rect, final Rect rect2, final Rect rect3) {
        final boolean b = b(n, rect, rect2);
        final boolean b2 = b(n, rect, rect3);
        boolean b3 = false;
        if (b2 || !b) {
            return false;
        }
        if (!j(n, rect, rect3)) {
            return true;
        }
        if (n != 17 && n != 66) {
            if (k(n, rect, rect2) < m(n, rect, rect3)) {
                b3 = true;
            }
            return b3;
        }
        return true;
    }
    
    public static boolean b(final int n, final Rect rect, final Rect rect2) {
        boolean b = true;
        final boolean b2 = true;
        Label_0075: {
            if (n != 17) {
                if (n != 33) {
                    if (n == 66) {
                        break Label_0075;
                    }
                    if (n != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                }
                return rect2.right >= rect.left && rect2.left <= rect.right && b2;
            }
        }
        if (rect2.bottom < rect.top || rect2.top > rect.bottom) {
            b = false;
        }
        return b;
    }
    
    public static Object c(final Object o, final b b, final a a, final Object o2, final Rect rect, final int n) {
        final Rect rect2 = new Rect(rect);
        final int n2 = 0;
        Label_0116: {
            int n3 = 0;
            Label_0108: {
                if (n != 17) {
                    int n4;
                    if (n != 33) {
                        if (n == 66) {
                            n3 = -(rect.width() + 1);
                            break Label_0108;
                        }
                        if (n != 130) {
                            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                        }
                        n4 = -(rect.height() + 1);
                    }
                    else {
                        n4 = rect.height() + 1;
                    }
                    rect2.offset(0, n4);
                    break Label_0116;
                }
                n3 = rect.width() + 1;
            }
            rect2.offset(n3, 0);
        }
        final int b2 = b.b(o);
        final Rect rect3 = new Rect();
        Object o3 = null;
        for (int i = n2; i < b2; ++i) {
            final Object a2 = b.a(o, i);
            if (a2 != o2) {
                a.a(a2, rect3);
                if (h(n, rect, rect3, rect2)) {
                    rect2.set(rect3);
                    o3 = a2;
                }
            }
        }
        return o3;
    }
    
    public static Object d(final Object o, final b b, final a a, final Object o2, final int n, final boolean b2, final boolean b3) {
        final int b4 = b.b(o);
        final ArrayList list = new ArrayList<Object>(b4);
        for (int i = 0; i < b4; ++i) {
            list.add(b.a(o, i));
        }
        Collections.sort((List<Object>)list, new c(b2, a));
        if (n == 1) {
            return f(o2, list, b3);
        }
        if (n == 2) {
            return e(o2, list, b3);
        }
        throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD}.");
    }
    
    public static Object e(final Object o, final ArrayList list, final boolean b) {
        final int size = list.size();
        int lastIndex;
        if (o == null) {
            lastIndex = -1;
        }
        else {
            lastIndex = list.lastIndexOf(o);
        }
        if (++lastIndex < size) {
            return list.get(lastIndex);
        }
        if (b && size > 0) {
            return list.get(0);
        }
        return null;
    }
    
    public static Object f(final Object o, final ArrayList list, final boolean b) {
        final int size = list.size();
        int index;
        if (o == null) {
            index = size;
        }
        else {
            index = list.indexOf(o);
        }
        if (--index >= 0) {
            return list.get(index);
        }
        if (b && size > 0) {
            return list.get(size - 1);
        }
        return null;
    }
    
    public static int g(final int n, final int n2) {
        return n * 13 * n + n2 * n2;
    }
    
    public static boolean h(final int n, final Rect rect, final Rect rect2, final Rect rect3) {
        final boolean i = i(rect, rect2, n);
        boolean b = false;
        if (!i) {
            return false;
        }
        if (!i(rect, rect3, n)) {
            return true;
        }
        if (a(n, rect, rect2, rect3)) {
            return true;
        }
        if (a(n, rect, rect3, rect2)) {
            return false;
        }
        if (g(k(n, rect, rect2), o(n, rect, rect2)) < g(k(n, rect, rect3), o(n, rect, rect3))) {
            b = true;
        }
        return b;
    }
    
    public static boolean i(final Rect rect, final Rect rect2, int n) {
        final boolean b = true;
        final boolean b2 = true;
        final boolean b3 = true;
        boolean b4 = true;
        if (n == 17) {
            final int right = rect.right;
            n = rect2.right;
            return (right > n || rect.left >= n) && rect.left > rect2.left && b3;
        }
        if (n == 33) {
            n = rect.bottom;
            final int bottom = rect2.bottom;
            return (n > bottom || rect.top >= bottom) && rect.top > rect2.top && b2;
        }
        if (n == 66) {
            n = rect.left;
            final int left = rect2.left;
            return (n < left || rect.right <= left) && rect.right < rect2.right && b;
        }
        if (n == 130) {
            n = rect.top;
            final int top = rect2.top;
            if ((n >= top && rect.bottom > top) || rect.bottom >= rect2.bottom) {
                b4 = false;
            }
            return b4;
        }
        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
    }
    
    public static boolean j(final int n, final Rect rect, final Rect rect2) {
        boolean b = true;
        final boolean b2 = true;
        final boolean b3 = true;
        final boolean b4 = true;
        if (n == 17) {
            return rect.left >= rect2.right && b3;
        }
        if (n == 33) {
            return rect.top >= rect2.bottom && b2;
        }
        if (n == 66) {
            if (rect.right > rect2.left) {
                b = false;
            }
            return b;
        }
        if (n == 130) {
            return rect.bottom <= rect2.top && b4;
        }
        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
    }
    
    public static int k(final int n, final Rect rect, final Rect rect2) {
        return Math.max(0, l(n, rect, rect2));
    }
    
    public static int l(int n, final Rect rect, final Rect rect2) {
        int n2;
        if (n != 17) {
            if (n != 33) {
                if (n != 66) {
                    if (n != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                    n = rect2.top;
                    n2 = rect.bottom;
                }
                else {
                    n = rect2.left;
                    n2 = rect.right;
                }
            }
            else {
                n = rect.top;
                n2 = rect2.bottom;
            }
        }
        else {
            n = rect.left;
            n2 = rect2.right;
        }
        return n - n2;
    }
    
    public static int m(final int n, final Rect rect, final Rect rect2) {
        return Math.max(1, n(n, rect, rect2));
    }
    
    public static int n(int n, final Rect rect, final Rect rect2) {
        int n2;
        if (n != 17) {
            if (n != 33) {
                if (n != 66) {
                    if (n != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                    n = rect2.bottom;
                    n2 = rect.bottom;
                }
                else {
                    n = rect2.right;
                    n2 = rect.right;
                }
            }
            else {
                n = rect.top;
                n2 = rect2.top;
            }
        }
        else {
            n = rect.left;
            n2 = rect2.left;
        }
        return n - n2;
    }
    
    public static int o(int n, final Rect rect, final Rect rect2) {
        Label_0073: {
            if (n == 17) {
                break Label_0073;
            }
            if (n != 33) {
                if (n == 66) {
                    break Label_0073;
                }
                if (n != 130) {
                    throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                }
            }
            n = rect.left + rect.width() / 2;
            final int n2 = rect2.left;
            final int n3 = rect2.width();
            return Math.abs(n - (n2 + n3 / 2));
        }
        n = rect.top + rect.height() / 2;
        final int n2 = rect2.top;
        final int n3 = rect2.height();
        return Math.abs(n - (n2 + n3 / 2));
    }
    
    public interface a
    {
        void a(final Object p0, final Rect p1);
    }
    
    public interface b
    {
        Object a(final Object p0, final int p1);
        
        int b(final Object p0);
    }
    
    public static class c implements Comparator
    {
        public final Rect a;
        public final Rect b;
        public final boolean c;
        public final a d;
        
        public c(final boolean c, final a d) {
            this.a = new Rect();
            this.b = new Rect();
            this.c = c;
            this.d = d;
        }
        
        @Override
        public int compare(final Object o, final Object o2) {
            final Rect a = this.a;
            final Rect b = this.b;
            this.d.a(o, a);
            this.d.a(o2, b);
            final int top = a.top;
            final int top2 = b.top;
            int n = -1;
            if (top < top2) {
                return -1;
            }
            if (top > top2) {
                return 1;
            }
            final int left = a.left;
            final int left2 = b.left;
            if (left < left2) {
                if (this.c) {
                    n = 1;
                }
                return n;
            }
            if (left > left2) {
                if (!this.c) {
                    n = 1;
                }
                return n;
            }
            final int bottom = a.bottom;
            final int bottom2 = b.bottom;
            if (bottom < bottom2) {
                return -1;
            }
            if (bottom > bottom2) {
                return 1;
            }
            final int right = a.right;
            final int right2 = b.right;
            if (right < right2) {
                if (this.c) {
                    n = 1;
                }
                return n;
            }
            if (right > right2) {
                if (!this.c) {
                    n = 1;
                }
                return n;
            }
            return 0;
        }
    }
}

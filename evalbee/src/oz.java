// 
// Decompiled by Procyon v0.6.0
// 

public abstract class oz
{
    public oz a;
    
    public oz() {
        this.a = null;
    }
    
    public static void c(final oz oz, final eb eb, final boolean b) {
        if (oz instanceof w11) {
            final w11 w11 = (w11)oz;
            c(w11.b, eb, b);
            c(w11.c, eb, b);
        }
        else if (oz instanceof c32) {
            if (b) {
                final c32 c32 = (c32)oz;
                if (!eb.b(c32.b)) {
                    eb.a(c32.b);
                }
            }
        }
        else if (oz instanceof z80) {
            final z80 z80 = (z80)oz;
            if (!b && !eb.b(z80.b)) {
                eb.a(z80.b);
            }
            for (int i = 0; i < z80.p(); ++i) {
                c(z80.n(i), eb, b);
            }
        }
    }
    
    public static void g(final oz oz, final StringBuffer sb) {
        if (oz instanceof w11) {
            final w11 w11 = (w11)oz;
            sb.append("(");
            g(w11.b, sb);
            sb.append(w11.h());
            g(w11.c, sb);
        }
        else if (oz instanceof yu1) {
            final yu1 yu1 = (yu1)oz;
            if (yu1.i()) {
                sb.append("(");
                sb.append("-");
            }
            sb.append(yu1.h());
            if (yu1 instanceof z80) {
                final z80 z80 = (z80)yu1;
                sb.append("(");
                if (z80.p() > 0) {
                    g(z80.n(0), sb);
                }
                for (int i = 1; i < z80.p(); ++i) {
                    sb.append(", ");
                    g(z80.n(i), sb);
                }
                sb.append(")");
            }
            if (!yu1.i()) {
                return;
            }
        }
        else {
            if (oz instanceof w22) {
                sb.append(((w22)oz).b);
            }
            return;
        }
        sb.append(")");
    }
    
    public void a(final oz oz) {
        if (oz == null) {
            throw new IllegalArgumentException("expression cannot be null");
        }
        if (oz.a != null) {
            throw new IllegalArgumentException("expression must be removed parent");
        }
        if (!this.f(oz)) {
            return;
        }
        throw new IllegalArgumentException("cyclic reference");
    }
    
    public abstract double b(final b32 p0, final y80 p1);
    
    public final String[] d(final boolean b) {
        final eb eb = new eb();
        c(this, eb, b);
        final int h = eb.h();
        final String[] array = new String[h];
        for (int i = 0; i < h; ++i) {
            array[i] = (String)eb.e(i);
        }
        return array;
    }
    
    public String[] e() {
        return this.d(true);
    }
    
    public boolean f(final oz oz) {
        for (oz a = this; a != null; a = a.a) {
            if (a == oz) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        g(this, sb);
        return sb.toString();
    }
}

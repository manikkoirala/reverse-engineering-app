// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public class MediaBrowserCompat$MediaItem implements Parcelable
{
    public static final Parcelable$Creator<MediaBrowserCompat$MediaItem> CREATOR;
    public final int a;
    public final MediaDescriptionCompat b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public MediaBrowserCompat$MediaItem a(final Parcel parcel) {
                return new MediaBrowserCompat$MediaItem(parcel);
            }
            
            public MediaBrowserCompat$MediaItem[] b(final int n) {
                return new MediaBrowserCompat$MediaItem[n];
            }
        };
    }
    
    public MediaBrowserCompat$MediaItem(final Parcel parcel) {
        this.a = parcel.readInt();
        this.b = (MediaDescriptionCompat)MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MediaItem{");
        sb.append("mFlags=");
        sb.append(this.a);
        sb.append(", mDescription=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.a);
        this.b.writeToParcel(parcel, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.support.v4.media.session.MediaSessionCompat;
import android.os.Parcel;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class MediaMetadataCompat implements Parcelable
{
    public static final Parcelable$Creator<MediaMetadataCompat> CREATOR;
    public static final r8 b;
    public static final String[] c;
    public static final String[] d;
    public static final String[] e;
    public final Bundle a;
    
    static {
        final r8 r8 = b = new r8();
        final Integer value = 1;
        r8.put("android.media.metadata.TITLE", value);
        r8.put("android.media.metadata.ARTIST", value);
        final Integer value2 = 0;
        r8.put("android.media.metadata.DURATION", value2);
        r8.put("android.media.metadata.ALBUM", value);
        r8.put("android.media.metadata.AUTHOR", value);
        r8.put("android.media.metadata.WRITER", value);
        r8.put("android.media.metadata.COMPOSER", value);
        r8.put("android.media.metadata.COMPILATION", value);
        r8.put("android.media.metadata.DATE", value);
        r8.put("android.media.metadata.YEAR", value2);
        r8.put("android.media.metadata.GENRE", value);
        r8.put("android.media.metadata.TRACK_NUMBER", value2);
        r8.put("android.media.metadata.NUM_TRACKS", value2);
        r8.put("android.media.metadata.DISC_NUMBER", value2);
        r8.put("android.media.metadata.ALBUM_ARTIST", value);
        final Integer value3 = 2;
        r8.put("android.media.metadata.ART", value3);
        r8.put("android.media.metadata.ART_URI", value);
        r8.put("android.media.metadata.ALBUM_ART", value3);
        r8.put("android.media.metadata.ALBUM_ART_URI", value);
        final Integer value4 = 3;
        r8.put("android.media.metadata.USER_RATING", value4);
        r8.put("android.media.metadata.RATING", value4);
        r8.put("android.media.metadata.DISPLAY_TITLE", value);
        r8.put("android.media.metadata.DISPLAY_SUBTITLE", value);
        r8.put("android.media.metadata.DISPLAY_DESCRIPTION", value);
        r8.put("android.media.metadata.DISPLAY_ICON", value3);
        r8.put("android.media.metadata.DISPLAY_ICON_URI", value);
        r8.put("android.media.metadata.MEDIA_ID", value);
        r8.put("android.media.metadata.BT_FOLDER_TYPE", value2);
        r8.put("android.media.metadata.MEDIA_URI", value);
        r8.put("android.media.metadata.ADVERTISEMENT", value2);
        r8.put("android.media.metadata.DOWNLOAD_STATUS", value2);
        c = new String[] { "android.media.metadata.TITLE", "android.media.metadata.ARTIST", "android.media.metadata.ALBUM", "android.media.metadata.ALBUM_ARTIST", "android.media.metadata.WRITER", "android.media.metadata.AUTHOR", "android.media.metadata.COMPOSER" };
        d = new String[] { "android.media.metadata.DISPLAY_ICON", "android.media.metadata.ART", "android.media.metadata.ALBUM_ART" };
        e = new String[] { "android.media.metadata.DISPLAY_ICON_URI", "android.media.metadata.ART_URI", "android.media.metadata.ALBUM_ART_URI" };
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public MediaMetadataCompat a(final Parcel parcel) {
                return new MediaMetadataCompat(parcel);
            }
            
            public MediaMetadataCompat[] b(final int n) {
                return new MediaMetadataCompat[n];
            }
        };
    }
    
    public MediaMetadataCompat(final Parcel parcel) {
        this.a = parcel.readBundle(MediaSessionCompat.class.getClassLoader());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeBundle(this.a);
    }
}

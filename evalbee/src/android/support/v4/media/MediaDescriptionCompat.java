// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.os.BaseBundle;
import android.support.v4.media.session.MediaSessionCompat;
import android.os.Parcel;
import android.os.Bundle;
import android.net.Uri;
import android.graphics.Bitmap;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class MediaDescriptionCompat implements Parcelable
{
    public static final Parcelable$Creator<MediaDescriptionCompat> CREATOR;
    public final String a;
    public final CharSequence b;
    public final CharSequence c;
    public final CharSequence d;
    public final Bitmap e;
    public final Uri f;
    public final Bundle g;
    public final Uri h;
    public Object i;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public MediaDescriptionCompat a(final Parcel parcel) {
                return MediaDescriptionCompat.b(android.support.v4.media.a.a(parcel));
            }
            
            public MediaDescriptionCompat[] b(final int n) {
                return new MediaDescriptionCompat[n];
            }
        };
    }
    
    public MediaDescriptionCompat(final String a, final CharSequence b, final CharSequence c, final CharSequence d, final Bitmap e, final Uri f, final Bundle g, final Uri h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    public static MediaDescriptionCompat b(final Object i) {
        MediaDescriptionCompat a = null;
        Bundle bundle = null;
        if (i != null) {
            final b b = new b();
            b.f(android.support.v4.media.a.f(i));
            b.i(android.support.v4.media.a.h(i));
            b.h(android.support.v4.media.a.g(i));
            b.b(android.support.v4.media.a.b(i));
            b.d(android.support.v4.media.a.d(i));
            b.e(android.support.v4.media.a.e(i));
            final Bundle c = android.support.v4.media.a.c(i);
            Uri uri;
            if (c != null) {
                MediaSessionCompat.a(c);
                uri = (Uri)c.getParcelable("android.support.v4.media.description.MEDIA_URI");
            }
            else {
                uri = null;
            }
            Label_0143: {
                if (uri != null) {
                    if (((BaseBundle)c).containsKey("android.support.v4.media.description.NULL_BUNDLE_FLAG") && ((BaseBundle)c).size() == 2) {
                        break Label_0143;
                    }
                    c.remove("android.support.v4.media.description.MEDIA_URI");
                    c.remove("android.support.v4.media.description.NULL_BUNDLE_FLAG");
                }
                bundle = c;
            }
            b.c(bundle);
            if (uri != null) {
                b.g(uri);
            }
            else {
                b.g(android.support.v4.media.b.a(i));
            }
            a = b.a();
            a.i = i;
        }
        return a;
    }
    
    public Object c() {
        Object i;
        if ((i = this.i) == null) {
            final Object b = android.support.v4.media.a.a.b();
            android.support.v4.media.a.a.g(b, this.a);
            android.support.v4.media.a.a.i(b, this.b);
            android.support.v4.media.a.a.h(b, this.c);
            android.support.v4.media.a.a.c(b, this.d);
            android.support.v4.media.a.a.e(b, this.e);
            android.support.v4.media.a.a.f(b, this.f);
            android.support.v4.media.a.a.d(b, this.g);
            android.support.v4.media.b.a.a(b, this.h);
            i = android.support.v4.media.a.a.a(b);
            this.i = i;
        }
        return i;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append((Object)this.b);
        sb.append(", ");
        sb.append((Object)this.c);
        sb.append(", ");
        sb.append((Object)this.d);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        android.support.v4.media.a.i(this.c(), parcel, n);
    }
    
    public static final class b
    {
        public String a;
        public CharSequence b;
        public CharSequence c;
        public CharSequence d;
        public Bitmap e;
        public Uri f;
        public Bundle g;
        public Uri h;
        
        public MediaDescriptionCompat a() {
            return new MediaDescriptionCompat(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
        }
        
        public b b(final CharSequence d) {
            this.d = d;
            return this;
        }
        
        public b c(final Bundle g) {
            this.g = g;
            return this;
        }
        
        public b d(final Bitmap e) {
            this.e = e;
            return this;
        }
        
        public b e(final Uri f) {
            this.f = f;
            return this;
        }
        
        public b f(final String a) {
            this.a = a;
            return this;
        }
        
        public b g(final Uri h) {
            this.h = h;
            return this;
        }
        
        public b h(final CharSequence c) {
            this.c = c;
            return this;
        }
        
        public b i(final CharSequence b) {
            this.b = b;
            return this;
        }
    }
}

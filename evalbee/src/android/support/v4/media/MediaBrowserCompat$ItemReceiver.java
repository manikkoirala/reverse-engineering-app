// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.os.BaseBundle;
import android.os.Parcelable;
import android.support.v4.media.session.MediaSessionCompat;
import android.os.Bundle;

class MediaBrowserCompat$ItemReceiver extends af1
{
    @Override
    public void b(final int n, final Bundle bundle) {
        MediaSessionCompat.a(bundle);
        if (n != 0 || bundle == null || !((BaseBundle)bundle).containsKey("media_item")) {
            throw null;
        }
        final Parcelable parcelable = bundle.getParcelable("media_item");
        if (parcelable != null && !(parcelable instanceof MediaBrowserCompat$MediaItem)) {
            throw null;
        }
        final MediaBrowserCompat$MediaItem mediaBrowserCompat$MediaItem = (MediaBrowserCompat$MediaItem)parcelable;
        throw null;
    }
}

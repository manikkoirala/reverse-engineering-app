// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.media.MediaDescription$Builder;
import android.net.Uri;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.media.MediaDescription;
import android.os.Parcel;

public abstract class a
{
    public static Object a(final Parcel parcel) {
        return MediaDescription.CREATOR.createFromParcel(parcel);
    }
    
    public static CharSequence b(final Object o) {
        return ((MediaDescription)o).getDescription();
    }
    
    public static Bundle c(final Object o) {
        return ((MediaDescription)o).getExtras();
    }
    
    public static Bitmap d(final Object o) {
        return ((MediaDescription)o).getIconBitmap();
    }
    
    public static Uri e(final Object o) {
        return ((MediaDescription)o).getIconUri();
    }
    
    public static String f(final Object o) {
        return ((MediaDescription)o).getMediaId();
    }
    
    public static CharSequence g(final Object o) {
        return ((MediaDescription)o).getSubtitle();
    }
    
    public static CharSequence h(final Object o) {
        return ((MediaDescription)o).getTitle();
    }
    
    public static void i(final Object o, final Parcel parcel, final int n) {
        ((MediaDescription)o).writeToParcel(parcel, n);
    }
    
    public abstract static class a
    {
        public static Object a(final Object o) {
            return ((MediaDescription$Builder)o).build();
        }
        
        public static Object b() {
            return new MediaDescription$Builder();
        }
        
        public static void c(final Object o, final CharSequence description) {
            ((MediaDescription$Builder)o).setDescription(description);
        }
        
        public static void d(final Object o, final Bundle extras) {
            ((MediaDescription$Builder)o).setExtras(extras);
        }
        
        public static void e(final Object o, final Bitmap iconBitmap) {
            ((MediaDescription$Builder)o).setIconBitmap(iconBitmap);
        }
        
        public static void f(final Object o, final Uri iconUri) {
            ((MediaDescription$Builder)o).setIconUri(iconUri);
        }
        
        public static void g(final Object o, final String mediaId) {
            ((MediaDescription$Builder)o).setMediaId(mediaId);
        }
        
        public static void h(final Object o, final CharSequence subtitle) {
            ((MediaDescription$Builder)o).setSubtitle(subtitle);
        }
        
        public static void i(final Object o, final CharSequence title) {
            ((MediaDescription$Builder)o).setTitle(title);
        }
    }
}

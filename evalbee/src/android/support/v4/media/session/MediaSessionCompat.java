// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media.session;

import android.os.ResultReceiver;
import android.os.Parcel;
import android.support.v4.media.MediaDescriptionCompat;
import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Bundle;

public abstract class MediaSessionCompat
{
    public static void a(final Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(MediaSessionCompat.class.getClassLoader());
        }
    }
    
    public static final class QueueItem implements Parcelable
    {
        public static final Parcelable$Creator<QueueItem> CREATOR;
        public final MediaDescriptionCompat a;
        public final long b;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public QueueItem a(final Parcel parcel) {
                    return new QueueItem(parcel);
                }
                
                public QueueItem[] b(final int n) {
                    return new QueueItem[n];
                }
            };
        }
        
        public QueueItem(final Parcel parcel) {
            this.a = (MediaDescriptionCompat)MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
            this.b = parcel.readLong();
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("MediaSession.QueueItem {Description=");
            sb.append(this.a);
            sb.append(", Id=");
            sb.append(this.b);
            sb.append(" }");
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            this.a.writeToParcel(parcel, n);
            parcel.writeLong(this.b);
        }
    }
    
    public static final class ResultReceiverWrapper implements Parcelable
    {
        public static final Parcelable$Creator<ResultReceiverWrapper> CREATOR;
        public ResultReceiver a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public ResultReceiverWrapper a(final Parcel parcel) {
                    return new ResultReceiverWrapper(parcel);
                }
                
                public ResultReceiverWrapper[] b(final int n) {
                    return new ResultReceiverWrapper[n];
                }
            };
        }
        
        public ResultReceiverWrapper(final Parcel parcel) {
            this.a = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(parcel);
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            this.a.writeToParcel(parcel, n);
        }
    }
    
    public static final class Token implements Parcelable
    {
        public static final Parcelable$Creator<Token> CREATOR;
        public final Object a;
        public a b;
        public Bundle c;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public Token a(final Parcel parcel) {
                    return new Token(parcel.readParcelable((ClassLoader)null));
                }
                
                public Token[] b(final int n) {
                    return new Token[n];
                }
            };
        }
        
        public Token(final Object o) {
            this(o, null, null);
        }
        
        public Token(final Object a, final a b, final Bundle c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public boolean equals(Object a) {
            boolean b = true;
            if (this == a) {
                return true;
            }
            if (!(a instanceof Token)) {
                return false;
            }
            final Token token = (Token)a;
            a = this.a;
            final Object a2 = token.a;
            if (a == null) {
                if (a2 != null) {
                    b = false;
                }
                return b;
            }
            return a2 != null && a.equals(a2);
        }
        
        @Override
        public int hashCode() {
            final Object a = this.a;
            if (a == null) {
                return 0;
            }
            return a.hashCode();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeParcelable((Parcelable)this.a, n);
        }
    }
}

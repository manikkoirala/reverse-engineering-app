// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.media.MediaDescription$Builder;
import android.media.MediaDescription;
import android.net.Uri;

public abstract class b
{
    public static Uri a(final Object o) {
        return ((MediaDescription)o).getMediaUri();
    }
    
    public abstract static class a
    {
        public static void a(final Object o, final Uri mediaUri) {
            ((MediaDescription$Builder)o).setMediaUri(mediaUri);
        }
    }
}

import com.google.firebase.messaging.reporting.MessagingClientEvent;

// 
// Decompiled by Procyon v0.6.0
// 

public final class aw0
{
    public static final aw0 b;
    public final MessagingClientEvent a;
    
    static {
        b = new a().a();
    }
    
    public aw0(final MessagingClientEvent a) {
        this.a = a;
    }
    
    public static a b() {
        return new a();
    }
    
    public MessagingClientEvent a() {
        return this.a;
    }
    
    public byte[] c() {
        return i91.a(this);
    }
    
    public static final class a
    {
        public MessagingClientEvent a;
        
        public a() {
            this.a = null;
        }
        
        public aw0 a() {
            return new aw0(this.a);
        }
        
        public a b(final MessagingClientEvent a) {
            this.a = a;
            return this;
        }
    }
}

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class ar1
{
    public static final ar1 c;
    public final Map a;
    public final Object b;
    
    static {
        c = new ar1();
    }
    
    public ar1() {
        this.a = new HashMap();
        this.b = new Object();
    }
    
    public static ar1 b() {
        return ar1.c;
    }
    
    public void a(final zq1 referent) {
        synchronized (this.b) {
            this.a.put(referent.G().toString(), new WeakReference(referent));
        }
    }
    
    public void c(final zq1 zq1) {
        synchronized (this.b) {
            final String string = zq1.G().toString();
            final WeakReference weakReference = this.a.get(string);
            zq1 zq2;
            if (weakReference != null) {
                zq2 = (zq1)weakReference.get();
            }
            else {
                zq2 = null;
            }
            if (zq2 == null || zq2 == zq1) {
                this.a.remove(string);
            }
        }
    }
}

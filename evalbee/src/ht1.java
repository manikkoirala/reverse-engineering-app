import java.util.ArrayList;
import android.database.Cursor;
import android.os.CancellationSignal;
import java.util.Collections;
import java.util.List;
import androidx.room.SharedSQLiteStatement;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ht1 implements gt1
{
    public final RoomDatabase a;
    public final ix b;
    public final SharedSQLiteStatement c;
    public final SharedSQLiteStatement d;
    
    public ht1(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final ht1 d;
            
            @Override
            public String e() {
                return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`generation`,`system_id`) VALUES (?,?,?)";
            }
            
            public void k(final ws1 ws1, final ft1 ft1) {
                final String a = ft1.a;
                if (a == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, a);
                }
                ws1.A(2, ft1.a());
                ws1.A(3, ft1.c);
            }
        };
        this.c = new SharedSQLiteStatement(this, a) {
            public final ht1 d;
            
            @Override
            public String e() {
                return "DELETE FROM SystemIdInfo where work_spec_id=? AND generation=?";
            }
        };
        this.d = new SharedSQLiteStatement(this, a) {
            public final ht1 d;
            
            @Override
            public String e() {
                return "DELETE FROM SystemIdInfo where work_spec_id=?";
            }
        };
    }
    
    public static List j() {
        return Collections.emptyList();
    }
    
    @Override
    public void a(final x82 x82) {
        gt1.a.b(this, x82);
    }
    
    @Override
    public ft1 b(String string, int e) {
        final sf1 c = sf1.c("SELECT * FROM SystemIdInfo WHERE work_spec_id=? AND generation=?", 2);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        c.A(2, e);
        this.a.d();
        final RoomDatabase a = this.a;
        ft1 ft1 = null;
        final String s = null;
        final Cursor b = bp.b(a, c, false, null);
        try {
            final int e2 = lo.e(b, "work_spec_id");
            e = lo.e(b, "generation");
            final int e3 = lo.e(b, "system_id");
            if (b.moveToFirst()) {
                if (b.isNull(e2)) {
                    string = s;
                }
                else {
                    string = b.getString(e2);
                }
                ft1 = new ft1(string, b.getInt(e), b.getInt(e3));
            }
            return ft1;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public List d() {
        final sf1 c = sf1.c("SELECT DISTINCT work_spec_id FROM SystemIdInfo", 0);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                Object string;
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add(string);
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public ft1 e(final x82 x82) {
        return gt1.a.a(this, x82);
    }
    
    @Override
    public void g(final ft1 ft1) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(ft1);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
    
    @Override
    public void h(final String s, final int n) {
        this.a.d();
        final ws1 b = this.c.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        b.A(2, n);
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.c.h(b);
        }
    }
    
    @Override
    public void i(final String s) {
        this.a.d();
        final ws1 b = this.d.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.d.h(b);
        }
    }
}

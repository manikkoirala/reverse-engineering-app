import com.google.firebase.encoders.EncodingException;
import com.google.firebase.encoders.proto.b;

// 
// Decompiled by Procyon v0.6.0
// 

public class p91 implements y22
{
    public boolean a;
    public boolean b;
    public n00 c;
    public final b d;
    
    public p91(final b d) {
        this.a = false;
        this.b = false;
        this.d = d;
    }
    
    @Override
    public y22 a(final boolean b) {
        this.b();
        this.d.o(this.c, b, this.b);
        return this;
    }
    
    @Override
    public y22 add(final String s) {
        this.b();
        this.d.i(this.c, s, this.b);
        return this;
    }
    
    public final void b() {
        if (!this.a) {
            this.a = true;
            return;
        }
        throw new EncodingException("Cannot encode a second value in the ValueEncoderContext");
    }
    
    public void c(final n00 c, final boolean b) {
        this.a = false;
        this.c = c;
        this.b = b;
    }
}

import android.os.Bundle;
import android.app.Application;
import android.app.Application$ActivityLifecycleCallbacks;
import android.util.Log;
import android.os.Build$VERSION;
import android.content.res.Configuration;
import java.util.List;
import android.os.IBinder;
import android.app.Activity;
import android.os.Looper;
import android.os.Handler;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class k2
{
    public static final Class a;
    public static final Field b;
    public static final Field c;
    public static final Method d;
    public static final Method e;
    public static final Method f;
    public static final Handler g;
    
    static {
        g = new Handler(Looper.getMainLooper());
        final Class clazz = a = a();
        b = b();
        c = f();
        d = d(clazz);
        e = c(clazz);
        f = e(clazz);
    }
    
    public static Class a() {
        try {
            return Class.forName("android.app.ActivityThread");
        }
        finally {
            return null;
        }
    }
    
    public static Field b() {
        try {
            final Field declaredField = Activity.class.getDeclaredField("mMainThread");
            declaredField.setAccessible(true);
            return declaredField;
        }
        finally {
            return null;
        }
    }
    
    public static Method c(final Class clazz) {
        if (clazz == null) {
            return null;
        }
        try {
            final Method declaredMethod = clazz.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        }
        finally {
            return null;
        }
    }
    
    public static Method d(final Class clazz) {
        if (clazz == null) {
            return null;
        }
        try {
            final Method declaredMethod = clazz.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE, String.class);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        }
        finally {
            return null;
        }
    }
    
    public static Method e(final Class clazz) {
        Label_0084: {
            if (!g()) {
                break Label_0084;
            }
            if (clazz == null) {
                break Label_0084;
            }
            try {
                final Class<Integer> type = Integer.TYPE;
                final Class<Boolean> type2 = Boolean.TYPE;
                final Method declaredMethod = clazz.getDeclaredMethod("requestRelaunchActivity", IBinder.class, List.class, List.class, type, type2, Configuration.class, Configuration.class, type2, type2);
                declaredMethod.setAccessible(true);
                return declaredMethod;
                return null;
            }
            finally {
                return null;
            }
        }
    }
    
    public static Field f() {
        try {
            final Field declaredField = Activity.class.getDeclaredField("mToken");
            declaredField.setAccessible(true);
            return declaredField;
        }
        finally {
            return null;
        }
    }
    
    public static boolean g() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        return sdk_INT == 26 || sdk_INT == 27;
    }
    
    public static boolean h(Object value, final int n, final Activity activity) {
        try {
            final Object value2 = k2.c.get(activity);
            if (value2 == value && activity.hashCode() == n) {
                value = k2.b.get(activity);
                k2.g.postAtFrontOfQueue((Runnable)new Runnable(value, value2) {
                    public final Object a;
                    public final Object b;
                    
                    @Override
                    public void run() {
                        try {
                            final Method d = k2.d;
                            if (d != null) {
                                d.invoke(this.a, this.b, Boolean.FALSE, "AppCompat recreation");
                            }
                            else {
                                k2.e.invoke(this.a, this.b, Boolean.FALSE);
                            }
                        }
                        catch (final RuntimeException ex) {
                            if (ex.getClass() == RuntimeException.class && ex.getMessage() != null) {
                                if (ex.getMessage().startsWith("Unable to stop")) {
                                    throw ex;
                                }
                            }
                        }
                        finally {
                            final Throwable t;
                            Log.e("ActivityRecreator", "Exception while invoking performStopActivity", t);
                        }
                    }
                });
                return true;
            }
            return false;
        }
        finally {
            final Throwable t;
            Log.e("ActivityRecreator", "Exception while fetching field values", t);
            return false;
        }
    }
    
    public static boolean i(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 28) {
            activity.recreate();
            return true;
        }
        if (g() && k2.f == null) {
            return false;
        }
        if (k2.e == null && k2.d == null) {
            return false;
        }
        try {
            final Object value = k2.c.get(activity);
            if (value == null) {}
            final Object value2 = k2.b.get(activity);
            if (value2 == null) {}
            final Application application = activity.getApplication();
            final d d = new d(activity);
            application.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)d);
            final Handler g = k2.g;
            g.post((Runnable)new Runnable(d, value) {
                public final d a;
                public final Object b;
                
                @Override
                public void run() {
                    this.a.a = this.b;
                }
            });
            try {
                if (g()) {
                    final Method f = k2.f;
                    final Boolean false = Boolean.FALSE;
                    f.invoke(value2, value, null, null, 0, false, null, null, false, false);
                }
                else {
                    activity.recreate();
                }
                g.post((Runnable)new Runnable(application, d) {
                    public final Application a;
                    public final d b;
                    
                    @Override
                    public void run() {
                        this.a.unregisterActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)this.b);
                    }
                });
                return true;
            }
            finally {
                k2.g.post((Runnable)new Runnable(application, d) {
                    public final Application a;
                    public final d b;
                    
                    @Override
                    public void run() {
                        this.a.unregisterActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)this.b);
                    }
                });
            }
        }
        finally {
            return false;
        }
    }
    
    public static final class d implements Application$ActivityLifecycleCallbacks
    {
        public Object a;
        public Activity b;
        public final int c;
        public boolean d;
        public boolean e;
        public boolean f;
        
        public d(final Activity b) {
            this.d = false;
            this.e = false;
            this.f = false;
            this.b = b;
            this.c = b.hashCode();
        }
        
        public void onActivityCreated(final Activity activity, final Bundle bundle) {
        }
        
        public void onActivityDestroyed(final Activity activity) {
            if (this.b == activity) {
                this.b = null;
                this.e = true;
            }
        }
        
        public void onActivityPaused(final Activity activity) {
            if (this.e && !this.f && !this.d && k2.h(this.a, this.c, activity)) {
                this.f = true;
                this.a = null;
            }
        }
        
        public void onActivityResumed(final Activity activity) {
        }
        
        public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
        }
        
        public void onActivityStarted(final Activity activity) {
            if (this.b == activity) {
                this.d = true;
            }
        }
        
        public void onActivityStopped(final Activity activity) {
        }
    }
}

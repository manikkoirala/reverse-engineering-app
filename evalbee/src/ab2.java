import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ab2 implements ThreadFactory
{
    public final ThreadFactory a;
    public final AtomicInteger b;
    
    public ab2(final xb xb) {
        this.a = Executors.defaultThreadFactory();
        this.b = new AtomicInteger(1);
    }
    
    @Override
    public final Thread newThread(final Runnable runnable) {
        final AtomicInteger b = this.b;
        final Thread thread = this.a.newThread(runnable);
        final int andIncrement = b.getAndIncrement();
        final StringBuilder sb = new StringBuilder();
        sb.append("PlayBillingLibrary-");
        sb.append(andIncrement);
        thread.setName(sb.toString());
        return thread;
    }
}

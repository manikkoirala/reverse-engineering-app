import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class lz1
{
    public final Type capture() {
        final Type genericSuperclass = this.getClass().getGenericSuperclass();
        i71.m(genericSuperclass instanceof ParameterizedType, "%s isn't parameterized", genericSuperclass);
        return ((ParameterizedType)genericSuperclass).getActualTypeArguments()[0];
    }
}

import android.view.ViewGroup$MarginLayoutParams;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class fn0
{
    public static int a(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        return a.b(viewGroup$MarginLayoutParams);
    }
    
    public static int b(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        return a.c(viewGroup$MarginLayoutParams);
    }
    
    public static void c(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        a.g(viewGroup$MarginLayoutParams, n);
    }
    
    public static void d(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        a.h(viewGroup$MarginLayoutParams, n);
    }
    
    public abstract static class a
    {
        public static int a(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getLayoutDirection();
        }
        
        public static int b(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getMarginEnd();
        }
        
        public static int c(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getMarginStart();
        }
        
        public static boolean d(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.isMarginRelative();
        }
        
        public static void e(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
            viewGroup$MarginLayoutParams.resolveLayoutDirection(n);
        }
        
        public static void f(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int layoutDirection) {
            viewGroup$MarginLayoutParams.setLayoutDirection(layoutDirection);
        }
        
        public static void g(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginEnd) {
            viewGroup$MarginLayoutParams.setMarginEnd(marginEnd);
        }
        
        public static void h(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginStart) {
            viewGroup$MarginLayoutParams.setMarginStart(marginStart);
        }
    }
}

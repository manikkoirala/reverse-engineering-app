// 
// Decompiled by Procyon v0.6.0
// 

public class rm implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        return (Math.pow(2.718281828459045, array[0]) + Math.pow(2.718281828459045, -array[0])) / 2.0;
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "cosh(x)";
    }
}

import androidx.fragment.app.Fragment;
import android.view.ViewParent;
import android.graphics.RectF;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import android.view.ViewGroup;
import android.view.View;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class l80
{
    public static void d(final List list, View view) {
        final int size = list.size();
        if (h(list, view, size)) {
            return;
        }
        if (o32.J(view) != null) {
            list.add(view);
        }
        for (int i = size; i < list.size(); ++i) {
            view = (View)list.get(i);
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int childCount = viewGroup.getChildCount(), j = 0; j < childCount; ++j) {
                    final View child = viewGroup.getChildAt(j);
                    if (!h(list, child, size) && o32.J(child) != null) {
                        list.add(child);
                    }
                }
            }
        }
    }
    
    public static boolean h(final List list, final View view, final int n) {
        for (int i = 0; i < n; ++i) {
            if (list.get(i) == view) {
                return true;
            }
        }
        return false;
    }
    
    public static String i(final Map map, final String s) {
        for (final Map.Entry<K, Object> entry : map.entrySet()) {
            if (s.equals(entry.getValue())) {
                return (String)entry.getKey();
            }
        }
        return null;
    }
    
    public static boolean l(final List list) {
        return list == null || list.isEmpty();
    }
    
    public abstract void A(final Object p0, final ArrayList p1, final ArrayList p2);
    
    public abstract Object B(final Object p0);
    
    public abstract void a(final Object p0, final View p1);
    
    public abstract void b(final Object p0, final ArrayList p1);
    
    public abstract void c(final ViewGroup p0, final Object p1);
    
    public abstract boolean e(final Object p0);
    
    public void f(final ArrayList list, final View view) {
        if (view.getVisibility() == 0) {
            Object e = view;
            if (view instanceof ViewGroup) {
                e = view;
                if (!s32.a((ViewGroup)e)) {
                    for (int childCount = ((ViewGroup)e).getChildCount(), i = 0; i < childCount; ++i) {
                        this.f(list, ((ViewGroup)e).getChildAt(i));
                    }
                    return;
                }
            }
            list.add(e);
        }
    }
    
    public abstract Object g(final Object p0);
    
    public void j(final Map map, final View view) {
        if (view.getVisibility() == 0) {
            final String j = o32.J(view);
            if (j != null) {
                map.put(j, view);
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                    this.j(map, viewGroup.getChildAt(i));
                }
            }
        }
    }
    
    public void k(final View view, final Rect rect) {
        if (!o32.T(view)) {
            return;
        }
        final RectF rectF = new RectF();
        rectF.set(0.0f, 0.0f, (float)view.getWidth(), (float)view.getHeight());
        view.getMatrix().mapRect(rectF);
        rectF.offset((float)view.getLeft(), (float)view.getTop());
        View view2;
        for (ViewParent viewParent = view.getParent(); viewParent instanceof View; viewParent = view2.getParent()) {
            view2 = (View)viewParent;
            rectF.offset((float)(-view2.getScrollX()), (float)(-view2.getScrollY()));
            view2.getMatrix().mapRect(rectF);
            rectF.offset((float)view2.getLeft(), (float)view2.getTop());
        }
        final int[] array = new int[2];
        view.getRootView().getLocationOnScreen(array);
        rectF.offset((float)array[0], (float)array[1]);
        rect.set(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }
    
    public abstract Object m(final Object p0, final Object p1, final Object p2);
    
    public abstract Object n(final Object p0, final Object p1, final Object p2);
    
    public ArrayList o(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        for (int size = list.size(), i = 0; i < size; ++i) {
            final View view = list.get(i);
            list2.add(o32.J(view));
            o32.K0(view, null);
        }
        return list2;
    }
    
    public abstract void p(final Object p0, final View p1);
    
    public abstract void q(final Object p0, final ArrayList p1, final ArrayList p2);
    
    public abstract void r(final Object p0, final View p1, final ArrayList p2);
    
    public void s(final ViewGroup viewGroup, final ArrayList list, final Map map) {
        s11.a((View)viewGroup, new Runnable(this, list, map) {
            public final ArrayList a;
            public final Map b;
            public final l80 c;
            
            @Override
            public void run() {
                for (int size = this.a.size(), i = 0; i < size; ++i) {
                    final View view = this.a.get(i);
                    o32.K0(view, (String)this.b.get(o32.J(view)));
                }
            }
        });
    }
    
    public abstract void t(final Object p0, final Object p1, final ArrayList p2, final Object p3, final ArrayList p4, final Object p5, final ArrayList p6);
    
    public abstract void u(final Object p0, final Rect p1);
    
    public abstract void v(final Object p0, final View p1);
    
    public void w(final Fragment fragment, final Object o, final jf jf, final Runnable runnable) {
        runnable.run();
    }
    
    public void x(final View view, final ArrayList list, final Map map) {
        s11.a(view, new Runnable(this, list, map) {
            public final ArrayList a;
            public final Map b;
            public final l80 c;
            
            @Override
            public void run() {
                for (int size = this.a.size(), i = 0; i < size; ++i) {
                    final View view = this.a.get(i);
                    final String j = o32.J(view);
                    if (j != null) {
                        o32.K0(view, l80.i(this.b, j));
                    }
                }
            }
        });
    }
    
    public void y(final View view, final ArrayList list, final ArrayList list2, final ArrayList list3, final Map map) {
        final int size = list2.size();
        final ArrayList list4 = new ArrayList();
        for (int i = 0; i < size; ++i) {
            final View view2 = list.get(i);
            final String j = o32.J(view2);
            list4.add(j);
            if (j != null) {
                o32.K0(view2, null);
                final String s = map.get(j);
                for (int k = 0; k < size; ++k) {
                    if (s.equals(list3.get(k))) {
                        o32.K0((View)list2.get(k), j);
                        break;
                    }
                }
            }
        }
        s11.a(view, new Runnable(this, size, list2, list3, list, list4) {
            public final int a;
            public final ArrayList b;
            public final ArrayList c;
            public final ArrayList d;
            public final ArrayList e;
            public final l80 f;
            
            @Override
            public void run() {
                for (int i = 0; i < this.a; ++i) {
                    o32.K0((View)this.b.get(i), (String)this.c.get(i));
                    o32.K0((View)this.d.get(i), (String)this.e.get(i));
                }
            }
        });
    }
    
    public abstract void z(final Object p0, final View p1, final ArrayList p2);
}

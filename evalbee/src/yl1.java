import com.google.firebase.sessions.DataCollectionState;
import android.content.pm.PackageInfo;
import android.content.Context;
import com.google.firebase.sessions.LogEnvironment;
import android.os.Build;
import android.os.Build$VERSION;
import com.google.firebase.sessions.api.SessionSubscriber;
import com.google.firebase.sessions.EventType;
import java.util.Map;
import java.util.List;
import com.google.firebase.sessions.settings.SessionsSettings;

// 
// Decompiled by Procyon v0.6.0
// 

public final class yl1
{
    public static final yl1 a;
    public static final hp b;
    
    static {
        a = new yl1();
        final hp i = new mh0().j(ga.a).k(true).i();
        fg0.d((Object)i, "JsonDataEncoderBuilder()\u2026lues(true)\n      .build()");
        b = i;
    }
    
    public final xl1 a(final r10 r10, final wl1 wl1, final SessionsSettings sessionsSettings, final f81 f81, final List list, final Map map, final String s) {
        fg0.e((Object)r10, "firebaseApp");
        fg0.e((Object)wl1, "sessionDetails");
        fg0.e((Object)sessionsSettings, "sessionsSettings");
        fg0.e((Object)f81, "currentProcessDetails");
        fg0.e((Object)list, "appProcessDetails");
        fg0.e((Object)map, "subscribers");
        fg0.e((Object)s, "firebaseInstallationId");
        return new xl1(EventType.SESSION_START, new am1(wl1.b(), wl1.a(), wl1.c(), wl1.d(), new gp(this.d(map.get(SessionSubscriber.Name.PERFORMANCE)), this.d(map.get(SessionSubscriber.Name.CRASHLYTICS)), sessionsSettings.b()), s), this.b(r10));
    }
    
    public final y7 b(final r10 r10) {
        fg0.e((Object)r10, "firebaseApp");
        final Context l = r10.l();
        fg0.d((Object)l, "firebaseApp.applicationContext");
        final String packageName = l.getPackageName();
        final PackageInfo packageInfo = l.getPackageManager().getPackageInfo(packageName, 0);
        String s;
        if (Build$VERSION.SDK_INT >= 28) {
            s = String.valueOf(li2.a(packageInfo));
        }
        else {
            s = String.valueOf(packageInfo.versionCode);
        }
        final String c = r10.p().c();
        fg0.d((Object)c, "firebaseApp.options.applicationId");
        final String model = Build.MODEL;
        fg0.d((Object)model, "MODEL");
        final String release = Build$VERSION.RELEASE;
        fg0.d((Object)release, "RELEASE");
        final LogEnvironment log_ENVIRONMENT_PROD = LogEnvironment.LOG_ENVIRONMENT_PROD;
        fg0.d((Object)packageName, "packageName");
        String versionName = packageInfo.versionName;
        if (versionName == null) {
            versionName = s;
        }
        final String manufacturer = Build.MANUFACTURER;
        fg0.d((Object)manufacturer, "MANUFACTURER");
        final h81 a = h81.a;
        final Context i = r10.l();
        fg0.d((Object)i, "firebaseApp.applicationContext");
        final f81 d = a.d(i);
        final Context j = r10.l();
        fg0.d((Object)j, "firebaseApp.applicationContext");
        return new y7(c, model, "1.2.0", release, log_ENVIRONMENT_PROD, new r4(packageName, versionName, s, manufacturer, d, a.c(j)));
    }
    
    public final hp c() {
        return yl1.b;
    }
    
    public final DataCollectionState d(final SessionSubscriber sessionSubscriber) {
        DataCollectionState dataCollectionState;
        if (sessionSubscriber == null) {
            dataCollectionState = DataCollectionState.COLLECTION_SDK_NOT_INSTALLED;
        }
        else if (sessionSubscriber.a()) {
            dataCollectionState = DataCollectionState.COLLECTION_ENABLED;
        }
        else {
            dataCollectionState = DataCollectionState.COLLECTION_DISABLED;
        }
        return dataCollectionState;
    }
}

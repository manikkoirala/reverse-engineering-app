import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Map;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class aj2 implements x3
{
    public static final Parcelable$Creator<aj2> CREATOR;
    public final String a;
    public final String b;
    public Map c;
    public boolean d;
    
    static {
        CREATOR = (Parcelable$Creator)new oj2();
    }
    
    public aj2(final String a, final String b, final boolean d) {
        Preconditions.checkNotEmpty(a);
        Preconditions.checkNotEmpty(b);
        this.a = a;
        this.b = b;
        this.c = yc2.d(b);
        this.d = d;
    }
    
    public aj2(final boolean d) {
        this.d = d;
        this.b = null;
        this.a = null;
        this.c = null;
    }
    
    public final String b() {
        return this.a;
    }
    
    public final boolean c() {
        return this.d;
    }
    
    public final int describeContents() {
        return 0;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.b(), false);
        SafeParcelWriter.writeString(parcel, 2, this.b, false);
        SafeParcelWriter.writeBoolean(parcel, 3, this.c());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

import java.util.Set;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class e81
{
    public static final Map a;
    public static final Map b;
    
    static {
        final LinkedHashMap m = new LinkedHashMap(16);
        final LinkedHashMap i = new LinkedHashMap(16);
        a(m, i, Boolean.TYPE, Boolean.class);
        a(m, i, Byte.TYPE, Byte.class);
        a(m, i, Character.TYPE, Character.class);
        a(m, i, Double.TYPE, Double.class);
        a(m, i, Float.TYPE, Float.class);
        a(m, i, Integer.TYPE, Integer.class);
        a(m, i, Long.TYPE, Long.class);
        a(m, i, Short.TYPE, Short.class);
        a(m, i, Void.TYPE, Void.class);
        a = Collections.unmodifiableMap((Map<?, ?>)m);
        b = Collections.unmodifiableMap((Map<?, ?>)i);
    }
    
    public static void a(final Map map, final Map map2, final Class clazz, final Class clazz2) {
        map.put(clazz, clazz2);
        map2.put(clazz2, clazz);
    }
    
    public static Set b() {
        return e81.b.keySet();
    }
    
    public static Class c(Class clazz) {
        i71.r(clazz);
        final Class clazz2 = e81.b.get(clazz);
        if (clazz2 != null) {
            clazz = clazz2;
        }
        return clazz;
    }
    
    public static Class d(Class clazz) {
        i71.r(clazz);
        final Class clazz2 = e81.a.get(clazz);
        if (clazz2 != null) {
            clazz = clazz2;
        }
        return clazz;
    }
}

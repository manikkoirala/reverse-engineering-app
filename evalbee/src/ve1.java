import com.ekodroid.omrevaluator.templateui.models.InvalidQuestionSet;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import com.ekodroid.omrevaluator.templateui.models.DecimalRange;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.PartialAnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.GradeLevel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.util.DataModels.SortResultList;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ve1
{
    public static void a(final ArrayList list, final String s) {
        final SortResultList list2 = new SortResultList();
        final SortResultList.ResultSortBy mark = SortResultList.ResultSortBy.MARK;
        final int n = 0;
        final int n2 = 0;
        list2.j(mark, list, false);
        int n3 = 1;
        int i = n;
        if (s != null) {
            i = n;
            if (s.equals(ok.L)) {
                int n4;
                for (int j = n2; j < list.size(); ++j, n3 = n4) {
                    if (j == 0) {
                        n4 = n3;
                    }
                    else {
                        n4 = n3;
                        if (((ye1)list.get(j)).d() < ((ye1)list.get(j - 1)).d()) {
                            n4 = n3 + 1;
                        }
                    }
                    list.get(j).o(n4);
                }
                return;
            }
        }
        while (i < list.size()) {
            final ye1 ye1 = list.get(i);
            if (i == 0) {
                ye1.o(1);
            }
            else {
                final double d = ye1.d();
                final int n5 = i - 1;
                if (d < ((ye1)list.get(n5)).d()) {
                    ((ye1)list.get(i)).o(i + 1);
                }
                else {
                    list.get(i).o(((ye1)list.get(n5)).f());
                }
            }
            ++i;
        }
    }
    
    public static ArrayList b(final SheetTemplate2 sheetTemplate2, final int n) {
        if (n < 0) {
            return null;
        }
        if (sheetTemplate2.getAnswerKeys() != null) {
            if (sheetTemplate2.getAnswerKeys().length >= n) {
                final AnswerSetKey[] answerKeys = sheetTemplate2.getAnswerKeys();
                AnswerSetKey answerSetKey;
                if (n == 0) {
                    answerSetKey = answerKeys[0];
                }
                else {
                    answerSetKey = answerKeys[n - 1];
                }
                if (answerSetKey != null && answerSetKey.getAnswerOptionKeys() != null && answerSetKey.getAnswerOptionKeys().size() > 0) {
                    return answerSetKey.getAnswerOptionKeys();
                }
            }
        }
        return null;
    }
    
    public static int[][] c(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final int[] sectionsInSubject = sheetTemplate2.getSectionsInSubject();
        final int length = sectionsInSubject.length;
        final int[][] array = new int[length][];
        for (int i = 0; i < length; ++i) {
            array[i] = new int[sectionsInSubject[i]];
        }
        for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
            if (answerValue.getMarkedState() == AnswerValue.MarkedState.CORRECT) {
                final int[] array2 = array[answerValue.getSubjectId()];
                final int sectionId = answerValue.getSectionId();
                ++array2[sectionId];
            }
        }
        return array;
    }
    
    public static double d(final double[] array) {
        double n = 0.0;
        for (int i = 0; i < array.length; ++i) {
            n += array[i];
        }
        return n;
    }
    
    public static int e(final int[] array) {
        int i = 0;
        int n = 0;
        while (i < array.length) {
            n += array[i];
            ++i;
        }
        return n;
    }
    
    public static int f(final int[] array, final int n) {
        int i = 0;
        int n2 = 0;
        while (i < n) {
            n2 += array[i];
            ++i;
        }
        return n2;
    }
    
    public static double[] g(final double[][] array) {
        final double[] array2 = new double[array.length];
        for (int i = 0; i < array.length; ++i) {
            int n = 0;
            while (true) {
                final double[] array3 = array[i];
                if (n >= array3.length) {
                    break;
                }
                array2[i] += array3[n];
                ++n;
            }
        }
        return array2;
    }
    
    public static int[] h(final int[][] array) {
        final int[] array2 = new int[array.length];
        for (int i = 0; i < array.length; ++i) {
            int n = 0;
            while (true) {
                final int[] array3 = array[i];
                if (n >= array3.length) {
                    break;
                }
                array2[i] += array3[n];
                ++n;
            }
        }
        return array2;
    }
    
    public static String i(final double n, final GradeLevel[] array) {
        if (array == null) {
            return null;
        }
        for (final GradeLevel gradeLevel : array) {
            if (n >= gradeLevel.getMinMarks() && n < gradeLevel.getMaxMarks()) {
                return gradeLevel.getValue();
            }
        }
        return "NA";
    }
    
    public static int[][] j(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final int[] sectionsInSubject = sheetTemplate2.getSectionsInSubject();
        final int length = sectionsInSubject.length;
        final int[][] array = new int[length][];
        for (int i = 0; i < length; ++i) {
            array[i] = new int[sectionsInSubject[i]];
        }
        for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
            if (answerValue.getMarkedState() == AnswerValue.MarkedState.INCORRECT) {
                final int[] array2 = array[answerValue.getSubjectId()];
                final int sectionId = answerValue.getSectionId();
                ++array2[sectionId];
            }
        }
        return array;
    }
    
    public static double[][] k(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final int[] sectionsInSubject = sheetTemplate2.getSectionsInSubject();
        final int length = sectionsInSubject.length;
        final double[][] array = new double[length][];
        for (int i = 0; i < length; ++i) {
            array[i] = new double[sectionsInSubject[i]];
        }
        for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
            final double[] array2 = array[answerValue.getSubjectId()];
            final int sectionId = answerValue.getSectionId();
            array2[sectionId] += answerValue.getMarksForAnswer();
        }
        return array;
    }
    
    public static double l(final SheetTemplate2 sheetTemplate2) {
        return o(d(g(r(sheetTemplate2))));
    }
    
    public static double[] m(final double[] array, final double[] array2) {
        final int length = array.length;
        final double[] array3 = new double[length];
        for (int i = 0; i < length; ++i) {
            array3[i] = array[i] * 100.0 / array2[i];
        }
        return array3;
    }
    
    public static double[][] n(final double[][] array, final double[][] array2) {
        final int length = array.length;
        final double[][] array3 = new double[length][];
        for (int i = 0; i < length; ++i) {
            array3[i] = new double[array[i].length];
        }
        for (int j = 0; j < length; ++j) {
            int n = 0;
            while (true) {
                final double[] array4 = array3[j];
                if (n >= array4.length) {
                    break;
                }
                array4[n] = array[j][n] * 100.0 / array2[j][n];
                ++n;
            }
        }
        return array3;
    }
    
    public static double o(final double n) {
        return Math.round(n * 1000.0) / 1000.0;
    }
    
    public static int p(final ResultItem resultItem) {
        final Iterator<AnswerValue> iterator = resultItem.getAnswerValue2s().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            if (iterator.next().getMarkedState() == AnswerValue.MarkedState.CORRECT) {
                ++n;
            }
        }
        return n;
    }
    
    public static int q(final ResultItem resultItem) {
        final Iterator<AnswerValue> iterator = resultItem.getAnswerValue2s().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            if (iterator.next().getMarkedState() == AnswerValue.MarkedState.INCORRECT) {
                ++n;
            }
        }
        return n;
    }
    
    public static double[][] r(final SheetTemplate2 sheetTemplate2) {
        final Subject2[] subjects = sheetTemplate2.getTemplateParams().getSubjects();
        final int length = subjects.length;
        final double[][] array = new double[length][];
        for (int i = 0; i < length; ++i) {
            array[i] = new double[subjects[i].getSections().length];
        }
        for (int j = 1; j < sheetTemplate2.getAnswerOptions().size(); ++j) {
            final AnswerOption answerOption = sheetTemplate2.getAnswerOptions().get(j);
            final double[] array2 = array[answerOption.subjectId];
            final int sectionId = answerOption.sectionId;
            array2[sectionId] += answerOption.correctMarks;
        }
        for (int k = 0; k < subjects.length; ++k) {
            final Section2[] sections = subjects[k].getSections();
            for (int l = 0; l < sections.length; ++l) {
                if (sections[l].isOptionalAllowed()) {
                    array[k][l] = sections[l].getPositiveMark() * sections[l].getNoOfOptionalQue();
                }
            }
        }
        return array;
    }
    
    public static int s(final ResultItem resultItem) {
        final Iterator<AnswerValue> iterator = resultItem.getAnswerValue2s().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            if (iterator.next().getMarkedState() == AnswerValue.MarkedState.UNATTEMPTED) {
                ++n;
            }
        }
        return n;
    }
    
    public static void t(final AnswerValue answerValue, final AnswerOptionKey answerOptionKey, final AnswerOption answerOption) {
        if (answerValue.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
            u(answerValue, answerOptionKey, answerOption);
            return;
        }
        final boolean[] markedValues = answerValue.getMarkedValues();
        final int length = markedValues.length;
        int i = 0;
        boolean b = false;
        while (i < length) {
            b |= markedValues[i];
            ++i;
        }
        if (!b) {
            answerValue.setMarkedState(AnswerValue.MarkedState.UNATTEMPTED);
            answerValue.setMarksForAnswer(0.0);
        }
        if (answerOptionKey != null && answerOptionKey.getMarkedValues() != null) {
            if (b) {
                answerValue.setMarkedState(AnswerValue.MarkedState.CORRECT);
                answerValue.setMarksForAnswer(answerOption.correctMarks);
            }
            final boolean[] markedValues2 = answerOptionKey.getMarkedValues();
            for (int j = 0; j < markedValues.length; ++j) {
                if (markedValues[j] != markedValues2[j] && b) {
                    answerValue.setMarkedState(AnswerValue.MarkedState.INCORRECT);
                    answerValue.setMarksForAnswer(answerOption.incorrectMarks);
                    break;
                }
            }
            if (answerValue.getMarkedState() != AnswerValue.MarkedState.CORRECT && answerOptionKey.getPartialAnswerOptionKeys() != null && answerOption.partialAllowed) {
                for (final PartialAnswerOptionKey partialAnswerOptionKey : answerOptionKey.getPartialAnswerOptionKeys()) {
                    if (partialAnswerOptionKey == null) {
                        break;
                    }
                    if (partialAnswerOptionKey.getMarkedValues() == null) {
                        break;
                    }
                    final boolean[] markedValues3 = partialAnswerOptionKey.getMarkedValues();
                    if (partialAnswerOptionKey.getPartialType() != null && partialAnswerOptionKey.getPartialType().equals(ok.O)) {
                        int k = 0;
                        int n = 0;
                        while (k < markedValues.length) {
                            final boolean b2 = markedValues[k];
                            int n2 = n;
                            if (b2) {
                                n2 = n;
                                if (markedValues3[k]) {
                                    n2 = n + 1;
                                }
                            }
                            if (b2 && !markedValues3[k]) {
                                return;
                            }
                            ++k;
                            n = n2;
                        }
                        if (n <= 0) {
                            continue;
                        }
                        answerValue.setMarkedState(AnswerValue.MarkedState.PARTIAL_CORRECT);
                        answerValue.setMarksForAnswer(partialAnswerOptionKey.getPartialMarks() * n);
                    }
                    else {
                        final String partialType = partialAnswerOptionKey.getPartialType();
                        boolean b3 = true;
                        if (partialType != null && partialAnswerOptionKey.getPartialType().equals(ok.P) && partialAnswerOptionKey.getType() == AnswerOption.AnswerOptionType.MATRIX) {
                            final MatrixOptionPayload c = bc.c(partialAnswerOptionKey.getPayload());
                            int primaryOptions;
                            int secondaryOptions;
                            if (c != null) {
                                primaryOptions = c.primaryOptions;
                                secondaryOptions = c.secondaryOptions;
                            }
                            else {
                                primaryOptions = 4;
                                secondaryOptions = 5;
                            }
                            int l = 0;
                            int n3 = 0;
                            while (l < primaryOptions) {
                                int n4 = 0;
                                boolean b4 = true;
                                while (n4 < secondaryOptions) {
                                    final int n5 = l * secondaryOptions + n4;
                                    if (markedValues[n5] != markedValues3[n5]) {
                                        b4 = false;
                                    }
                                    ++n4;
                                }
                                int n6 = n3;
                                if (b4) {
                                    n6 = n3 + 1;
                                }
                                ++l;
                                n3 = n6;
                            }
                            if (n3 > 0) {
                                answerValue.setMarkedState(AnswerValue.MarkedState.PARTIAL_CORRECT);
                                answerValue.setMarksForAnswer(partialAnswerOptionKey.getPartialMarks() * n3);
                            }
                            return;
                        }
                        for (int n7 = 0; n7 < markedValues.length; ++n7) {
                            if (markedValues[n7] != markedValues3[n7]) {
                                b3 = false;
                            }
                        }
                        if (b3) {
                            answerValue.setMarkedState(AnswerValue.MarkedState.PARTIAL_CORRECT);
                            answerValue.setMarksForAnswer(partialAnswerOptionKey.getPartialMarks());
                            break;
                        }
                        continue;
                    }
                }
            }
            return;
        }
        if (b) {
            answerValue.setMarkedState(AnswerValue.MarkedState.ATTEMPTED);
            answerValue.setMarksForAnswer(0.0);
        }
    }
    
    public static void u(final AnswerValue answerValue, final AnswerOptionKey answerOptionKey, final AnswerOption answerOption) {
        final boolean[] markedValues = answerValue.getMarkedValues();
        final int length = markedValues.length;
        int i = 0;
        boolean b = false;
        while (i < length) {
            b |= markedValues[i];
            ++i;
        }
        if (!b) {
            answerValue.setMarkedState(AnswerValue.MarkedState.UNATTEMPTED);
            answerValue.setMarksForAnswer(0.0);
            return;
        }
        if (answerOptionKey == null || answerOptionKey.getMarkedPayload() == null) {
            answerValue.setMarkedState(AnswerValue.MarkedState.ATTEMPTED);
            answerValue.setMarksForAnswer(0.0);
            return;
        }
        final double b2 = e5.b(answerValue);
        final DecimalRange decimalRange = (DecimalRange)new gc0().j(answerOptionKey.getMarkedPayload(), DecimalRange.class);
        if (b2 >= decimalRange.getLower() && b2 <= decimalRange.getUpper()) {
            answerValue.setMarkedState(AnswerValue.MarkedState.CORRECT);
            answerValue.setMarksForAnswer(answerOption.correctMarks);
            return;
        }
        answerValue.setMarkedState(AnswerValue.MarkedState.INCORRECT);
        answerValue.setMarksForAnswer(answerOption.incorrectMarks);
        if (answerValue.getMarkedState() != AnswerValue.MarkedState.CORRECT && answerOptionKey.getPartialAnswerOptionKeys() != null && answerOption.partialAllowed) {
            for (final PartialAnswerOptionKey partialAnswerOptionKey : answerOptionKey.getPartialAnswerOptionKeys()) {
                if (partialAnswerOptionKey == null) {
                    break;
                }
                if (partialAnswerOptionKey.getMarkedPayload() == null) {
                    break;
                }
                final DecimalRange decimalRange2 = (DecimalRange)new gc0().j(partialAnswerOptionKey.getMarkedPayload(), DecimalRange.class);
                if (b2 >= decimalRange2.getLower() && b2 <= decimalRange2.getUpper()) {
                    answerValue.setMarkedState(AnswerValue.MarkedState.PARTIAL_CORRECT);
                    answerValue.setMarksForAnswer(partialAnswerOptionKey.getPartialMarks());
                    break;
                }
            }
        }
    }
    
    public static void v(int i, final int n, final Section2 section2, final ResultItem resultItem) {
        final ArrayList list = new ArrayList();
        final Iterator<AnswerValue> iterator = resultItem.getAnswerValue2s().iterator();
        int n2 = 0;
        while (iterator.hasNext()) {
            final AnswerValue e = iterator.next();
            if (e.getSubjectId() == i && e.getSectionId() == n) {
                if (e.getMarkedState() == AnswerValue.MarkedState.UNATTEMPTED) {
                    ++n2;
                }
                else {
                    list.add(e);
                }
            }
        }
        if (n2 >= section2.getNoOfQue() - section2.getNoOfOptionalQue()) {
            return;
        }
        final String optionalType = section2.getOptionalType();
        optionalType.hashCode();
        Label_0159: {
            Comparator c;
            if (!optionalType.equals("Max Marks")) {
                if (!optionalType.equals("Min Marks")) {
                    break Label_0159;
                }
                c = new Comparator() {
                    public int a(final AnswerValue answerValue, final AnswerValue answerValue2) {
                        return Double.compare(answerValue.getMarksForAnswer(), answerValue2.getMarksForAnswer());
                    }
                };
            }
            else {
                c = new Comparator() {
                    public int a(final AnswerValue answerValue, final AnswerValue answerValue2) {
                        return Double.compare(answerValue2.getMarksForAnswer(), answerValue.getMarksForAnswer());
                    }
                };
            }
            Collections.sort((List<Object>)list, c);
        }
        AnswerValue answerValue;
        for (i = section2.getNoOfOptionalQue(); i < list.size(); ++i) {
            answerValue = list.get(i);
            answerValue.setMarkedState(AnswerValue.MarkedState.INVALID);
            answerValue.setMarksForAnswer(0.0);
        }
    }
    
    public static void w(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final ArrayList<AnswerOption> answerOptions = sheetTemplate2.getAnswerOptions();
        for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
            final ArrayList b = b(sheetTemplate2, resultItem.getExamSet());
            AnswerOptionKey answerOptionKey;
            if (b != null) {
                answerOptionKey = b.get(answerValue.getQuestionNumber() - 1);
            }
            else {
                answerOptionKey = null;
            }
            t(answerValue, answerOptionKey, answerOptions.get(answerValue.getQuestionNumber()));
        }
        if (answerOptions.size() - 1 > resultItem.getAnswerValue2s().size()) {
            return;
        }
        int n;
        if ((n = resultItem.getExamSet() - 1) < 0) {
            n = 0;
        }
        final InvalidQuestionSet[] invalidQuestionSets = sheetTemplate2.getInvalidQuestionSets();
        if (invalidQuestionSets != null && invalidQuestionSets.length > n) {
            final int[] invalidQuestions = invalidQuestionSets[n].getInvalidQuestions();
            if (invalidQuestions != null) {
                for (final int index : invalidQuestions) {
                    final AnswerValue answerValue2 = resultItem.getAnswerValue2s().get(index - 1);
                    answerValue2.setMarkedState(AnswerValue.MarkedState.INVALID);
                    final AnswerOption answerOption = answerOptions.get(index);
                    double correctMarks;
                    if (invalidQuestionSets[n].getInvalidQueMarks() != null && !invalidQuestionSets[n].getInvalidQueMarks().equals("Max")) {
                        correctMarks = 0.0;
                    }
                    else {
                        correctMarks = answerOption.correctMarks;
                    }
                    answerValue2.setMarksForAnswer(correctMarks);
                }
            }
        }
        final Subject2[] subjects = sheetTemplate2.getTemplateParams().getSubjects();
        for (int j = 0; j < subjects.length; ++j) {
            final Section2[] sections = subjects[j].getSections();
            for (int k = 0; k < sections.length; ++k) {
                final Section2 section2 = sections[k];
                if (section2.isOptionalAllowed()) {
                    v(j, k, section2, resultItem);
                }
            }
        }
    }
}

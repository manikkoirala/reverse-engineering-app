import android.util.AndroidRuntimeException;
import android.os.Looper;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ap1 extends pv
{
    public bp1 A;
    public float B;
    public boolean C;
    
    public ap1(final Object o, final v40 v40) {
        super(o, v40);
        this.A = null;
        this.B = Float.MAX_VALUE;
        this.C = false;
    }
    
    @Override
    public void l() {
        this.r();
        this.A.g(this.e());
        super.l();
    }
    
    @Override
    public boolean n(long n) {
        if (this.C) {
            final float b = this.B;
            if (b != Float.MAX_VALUE) {
                this.A.e(b);
                this.B = Float.MAX_VALUE;
            }
            super.b = this.A.a();
            super.a = 0.0f;
            this.C = false;
            return true;
        }
        bp1 bp1;
        double n4;
        double n5;
        if (this.B != Float.MAX_VALUE) {
            this.A.a();
            final bp1 a = this.A;
            final double n2 = super.b;
            final double n3 = super.a;
            n /= 2L;
            final o h = a.h(n2, n3, n);
            this.A.e(this.B);
            this.B = Float.MAX_VALUE;
            bp1 = this.A;
            n4 = h.a;
            n5 = h.b;
        }
        else {
            bp1 = this.A;
            n4 = super.b;
            n5 = super.a;
        }
        final o h2 = bp1.h(n4, n5, n);
        super.b = h2.a;
        super.a = h2.b;
        final float max = Math.max(super.b, super.h);
        super.b = max;
        final float min = Math.min(max, super.g);
        super.b = min;
        if (this.q(min, super.a)) {
            super.b = this.A.a();
            super.a = 0.0f;
            return true;
        }
        return false;
    }
    
    public void o(final float b) {
        if (this.f()) {
            this.B = b;
        }
        else {
            if (this.A == null) {
                this.A = new bp1(b);
            }
            this.A.e(b);
            this.l();
        }
    }
    
    public boolean p() {
        return this.A.b > 0.0;
    }
    
    public boolean q(final float n, final float n2) {
        return this.A.c(n, n2);
    }
    
    public final void r() {
        final bp1 a = this.A;
        if (a == null) {
            throw new UnsupportedOperationException("Incomplete SpringAnimation: Either final position or a spring force needs to be set.");
        }
        final double n = a.a();
        if (n > super.g) {
            throw new UnsupportedOperationException("Final position of the spring cannot be greater than the max value.");
        }
        if (n >= super.h) {
            return;
        }
        throw new UnsupportedOperationException("Final position of the spring cannot be less than the min value.");
    }
    
    public ap1 s(final bp1 a) {
        this.A = a;
        return this;
    }
    
    public void t() {
        if (!this.p()) {
            throw new UnsupportedOperationException("Spring animations can only come to an end when there is damping");
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (super.f) {
                this.C = true;
            }
            return;
        }
        throw new AndroidRuntimeException("Animations may only be started on the main thread");
    }
}

import java.lang.reflect.Field;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class c12
{
    public static final c12 a;
    
    static {
        a = c();
    }
    
    public static void b(final Class clazz) {
        final String a = bl.a(clazz);
        if (a == null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("UnsafeAllocator is used for non-instantiable type: ");
        sb.append(a);
        throw new AssertionError((Object)sb.toString());
    }
    
    public static c12 c() {
        try {
            final Class<?> forName = Class.forName("sun.misc.Unsafe");
            final Field declaredField = forName.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return new c12(forName.getMethod("allocateInstance", Class.class), declaredField.get(null)) {
                public final Method b;
                public final Object c;
                
                @Override
                public Object d(final Class clazz) {
                    b(clazz);
                    return this.b.invoke(this.c, clazz);
                }
            };
        }
        catch (final Exception ex) {
            try {
                final Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                final int intValue = (int)declaredMethod.invoke(null, Object.class);
                final Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                return new c12(declaredMethod2, intValue) {
                    public final Method b;
                    public final int c;
                    
                    @Override
                    public Object d(final Class clazz) {
                        b(clazz);
                        return this.b.invoke(null, clazz, this.c);
                    }
                };
            }
            catch (final Exception ex2) {
                try {
                    final Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    return new c12(declaredMethod3) {
                        public final Method b;
                        
                        @Override
                        public Object d(final Class clazz) {
                            b(clazz);
                            return this.b.invoke(null, clazz, Object.class);
                        }
                    };
                }
                catch (final Exception ex3) {
                    return new c12() {
                        @Override
                        public Object d(final Class obj) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Cannot allocate ");
                            sb.append(obj);
                            sb.append(". Usage of JDK sun.misc.Unsafe is enabled, but it could not be used. Make sure your runtime is configured correctly.");
                            throw new UnsupportedOperationException(sb.toString());
                        }
                    };
                }
            }
        }
    }
    
    public abstract Object d(final Class p0);
}

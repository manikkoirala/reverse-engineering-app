import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class o5
{
    public static Object a(final Method method, final Object obj, final Object... args) {
        try {
            return method.invoke(obj, args);
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
    
    public static AssertionError b(final String detailMessage, final Throwable cause) {
        final AssertionError assertionError = new AssertionError((Object)detailMessage);
        assertionError.initCause(cause);
        return assertionError;
    }
}

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Iterator;
import java.util.ArrayList;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class kb2 extends ix0
{
    public static final Parcelable$Creator<kb2> CREATOR;
    public String a;
    public String b;
    public List c;
    public List d;
    public na2 e;
    
    static {
        CREATOR = (Parcelable$Creator)new hb2();
    }
    
    public kb2() {
    }
    
    public kb2(final String a, final String b, final List c, final List d, final na2 e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static kb2 i(final List list, String e) {
        Preconditions.checkNotNull(list);
        Preconditions.checkNotEmpty(e);
        final kb2 kb2 = new kb2();
        kb2.c = new ArrayList();
        kb2.d = new ArrayList();
        for (final gx0 gx0 : list) {
            List list2;
            gx0 gx2;
            if (gx0 instanceof i51) {
                list2 = kb2.c;
                gx2 = gx0;
            }
            else {
                if (!(gx0 instanceof fy1)) {
                    e = gx0.E();
                    final StringBuilder sb = new StringBuilder("MultiFactorInfo must be either PhoneMultiFactorInfo or TotpMultiFactorInfo. The factorId of this MultiFactorInfo: ");
                    sb.append(e);
                    throw new IllegalArgumentException(sb.toString());
                }
                list2 = kb2.d;
                gx2 = gx0;
            }
            list2.add(gx2);
        }
        kb2.b = e;
        return kb2;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.writeString(parcel, 2, this.b, false);
        SafeParcelWriter.writeTypedList(parcel, 3, (List<Parcelable>)this.c, false);
        SafeParcelWriter.writeTypedList(parcel, 4, (List<Parcelable>)this.d, false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.e, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zzb() {
        return this.a;
    }
    
    public final String zzc() {
        return this.b;
    }
}

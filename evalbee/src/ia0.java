// 
// Decompiled by Procyon v0.6.0
// 

public class ia0 implements Comparable
{
    public final double a;
    public final double b;
    
    public ia0(final double n, final double n2) {
        if (Double.isNaN(n) || n < -90.0 || n > 90.0) {
            throw new IllegalArgumentException("Latitude must be in the range of [-90, 90]");
        }
        if (!Double.isNaN(n2) && n2 >= -180.0 && n2 <= 180.0) {
            this.a = n;
            this.b = n2;
            return;
        }
        throw new IllegalArgumentException("Longitude must be in the range of [-180, 180]");
    }
    
    public int a(final ia0 ia0) {
        final int j = o22.j(this.a, ia0.a);
        if (j == 0) {
            return o22.j(this.b, ia0.b);
        }
        return j;
    }
    
    public double c() {
        return this.a;
    }
    
    public double d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof ia0;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final ia0 ia0 = (ia0)o;
        boolean b3 = b2;
        if (this.a == ia0.a) {
            b3 = b2;
            if (this.b == ia0.b) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        final long doubleToLongBits = Double.doubleToLongBits(this.a);
        final int n = (int)(doubleToLongBits ^ doubleToLongBits >>> 32);
        final long doubleToLongBits2 = Double.doubleToLongBits(this.b);
        return n * 31 + (int)(doubleToLongBits2 >>> 32 ^ doubleToLongBits2);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("GeoPoint { latitude=");
        sb.append(this.a);
        sb.append(", longitude=");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }
}

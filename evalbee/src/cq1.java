// 
// Decompiled by Procyon v0.6.0
// 

public final class cq1 implements Runnable
{
    public final q81 a;
    public final op1 b;
    public final boolean c;
    public final int d;
    
    public cq1(final q81 q81, final op1 op1, final boolean b) {
        fg0.e((Object)q81, "processor");
        fg0.e((Object)op1, "token");
        this(q81, op1, b, -512);
    }
    
    public cq1(final q81 a, final op1 b, final boolean c, final int d) {
        fg0.e((Object)a, "processor");
        fg0.e((Object)b, "token");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    @Override
    public void run() {
        boolean b;
        if (this.c) {
            b = this.a.v(this.b, this.d);
        }
        else {
            b = this.a.w(this.b, this.d);
        }
        final xl0 e = xl0.e();
        final String i = xl0.i("StopWorkRunnable");
        final StringBuilder sb = new StringBuilder();
        sb.append("StopWorkRunnable for ");
        sb.append(this.b.a().b());
        sb.append("; Processor.stopWork = ");
        sb.append(b);
        e.a(i, sb.toString());
    }
}

import kotlin.Result$a;
import java.util.concurrent.CancellationException;
import kotlin.Result;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jk0 implements Runnable
{
    public final df a;
    public final ik0 b;
    
    public jk0(final df a, final ik0 b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public final void run() {
        try {
            final df a = this.a;
            final Result$a companion = Result.Companion;
            ((vl)a).resumeWith(Result.constructor-impl(this.b.get()));
        }
        finally {
            final Throwable t;
            Throwable cause;
            if ((cause = t.getCause()) == null) {
                cause = t;
            }
            if (t instanceof CancellationException) {
                this.a.l(cause);
            }
            else {
                final df a2 = this.a;
                final Result$a companion2 = Result.Companion;
                ((vl)a2).resumeWith(Result.constructor-impl(xe1.a(cause)));
            }
        }
    }
}

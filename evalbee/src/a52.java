import android.graphics.Matrix;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class a52 extends x42
{
    public static boolean d = true;
    public static boolean e = true;
    
    @Override
    public void g(final View view, final Matrix matrix) {
        if (a52.d) {
            try {
                z42.a(view, matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                a52.d = false;
            }
        }
    }
    
    @Override
    public void h(final View view, final Matrix matrix) {
        if (a52.e) {
            try {
                y42.a(view, matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                a52.e = false;
            }
        }
    }
}

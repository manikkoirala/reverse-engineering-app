import android.app.ActivityManager;
import android.net.Uri;
import android.database.Cursor;

// 
// Decompiled by Procyon v0.6.0
// 

public final class os1
{
    public static final os1 a;
    
    static {
        a = new os1();
    }
    
    public static final Uri a(final Cursor cursor) {
        fg0.e((Object)cursor, "cursor");
        final Uri notificationUri = cursor.getNotificationUri();
        fg0.d((Object)notificationUri, "cursor.notificationUri");
        return notificationUri;
    }
    
    public static final boolean b(final ActivityManager activityManager) {
        fg0.e((Object)activityManager, "activityManager");
        return activityManager.isLowRamDevice();
    }
}

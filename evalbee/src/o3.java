import android.widget.AdapterView;
import android.content.res.Resources;
import android.widget.ListView;
import android.view.View$OnClickListener;
import android.widget.TextView;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.exams.sms.SmsListDataModel;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class o3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    
    public o3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final SmsListDataModel smsListDataModel = this.b.get(index);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493030, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297258);
        final TextView textView2 = (TextView)inflate.findViewById(2131297251);
        String phoneNo;
        if (!smsListDataModel.getPhoneNo().equals("")) {
            Resources resources;
            int n;
            if (smsListDataModel.isSmsSent()) {
                resources = this.a.getResources();
                n = 2131099721;
            }
            else {
                resources = this.a.getResources();
                n = 2131099715;
            }
            textView.setTextColor(resources.getColor(n));
            phoneNo = smsListDataModel.getPhoneNo();
        }
        else {
            phoneNo = "---";
        }
        textView.setText((CharSequence)phoneNo);
        textView2.setText((CharSequence)smsListDataModel.getMessage());
        inflate.findViewById(2131296674).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, viewGroup, index) {
            public final ViewGroup a;
            public final int b;
            public final o3 c;
            
            public void onClick(final View view) {
                ((AdapterView)this.a).performItemClick(view, this.b, (long)view.getId());
            }
        });
        return inflate;
    }
}

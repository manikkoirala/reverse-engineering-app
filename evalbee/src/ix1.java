// 
// Decompiled by Procyon v0.6.0
// 

public final class ix1
{
    public final long a;
    public final long b;
    public final int c;
    
    public ix1(final long a, final long b, final int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public final long a() {
        return this.b;
    }
    
    public final long b() {
        return this.a;
    }
    
    public final int c() {
        return this.c;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof ix1)) {
            return false;
        }
        final long a = this.a;
        final ix1 ix1 = (ix1)o;
        if (a != ix1.a || this.b != ix1.b || this.c != ix1.c) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return (Long.hashCode(this.a) * 31 + Long.hashCode(this.b)) * 31 + Integer.hashCode(this.c);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TaxonomyVersion=");
        sb.append(this.a);
        sb.append(", ModelVersion=");
        sb.append(this.b);
        sb.append(", TopicCode=");
        sb.append(this.c);
        sb.append(" }");
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Topic { ");
        sb2.append(string);
        return sb2.toString();
    }
}

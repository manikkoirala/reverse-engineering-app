import java.util.Set;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jd1
{
    public final qo1 a;
    public final Map b;
    public final Map c;
    public final Map d;
    public final Set e;
    
    public jd1(final qo1 a, final Map b, final Map c, final Map d, final Set e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public Map a() {
        return this.d;
    }
    
    public Set b() {
        return this.e;
    }
    
    public qo1 c() {
        return this.a;
    }
    
    public Map d() {
        return this.b;
    }
    
    public Map e() {
        return this.c;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RemoteEvent{snapshotVersion=");
        sb.append(this.a);
        sb.append(", targetChanges=");
        sb.append(this.b);
        sb.append(", targetMismatches=");
        sb.append(this.c);
        sb.append(", documentUpdates=");
        sb.append(this.d);
        sb.append(", resolvedLimboDocuments=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}

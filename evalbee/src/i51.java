import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONException;
import com.google.android.gms.internal.firebase_auth_api.zzxw;
import android.util.Log;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class i51 extends gx0
{
    public static final Parcelable$Creator<i51> CREATOR;
    public final String a;
    public final String b;
    public final long c;
    public final String d;
    
    static {
        CREATOR = (Parcelable$Creator)new wb2();
    }
    
    public i51(final String s, final String b, final long c, final String s2) {
        this.a = Preconditions.checkNotEmpty(s);
        this.b = b;
        this.c = c;
        this.d = Preconditions.checkNotEmpty(s2);
    }
    
    public static i51 J(final JSONObject jsonObject) {
        if (jsonObject.has("enrollmentTimestamp")) {
            return new i51(jsonObject.optString("uid"), jsonObject.optString("displayName"), jsonObject.optLong("enrollmentTimestamp"), jsonObject.optString("phoneNumber"));
        }
        throw new IllegalArgumentException("An enrollment timestamp in seconds of UTC time since Unix epoch is required to build a PhoneMultiFactorInfo instance.");
    }
    
    @Override
    public String E() {
        return "phone";
    }
    
    @Override
    public String H() {
        return this.a;
    }
    
    @Override
    public String getDisplayName() {
        return this.b;
    }
    
    public String getPhoneNumber() {
        return this.d;
    }
    
    @Override
    public long i() {
        return this.c;
    }
    
    @Override
    public JSONObject toJson() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt("factorIdKey", (Object)"phone");
            jsonObject.putOpt("uid", (Object)this.a);
            jsonObject.putOpt("displayName", (Object)this.b);
            jsonObject.putOpt("enrollmentTimestamp", (Object)this.c);
            jsonObject.putOpt("phoneNumber", (Object)this.d);
            return jsonObject;
        }
        catch (final JSONException ex) {
            Log.d("PhoneMultiFactorInfo", "Failed to jsonify this object");
            throw new zzxw((Throwable)ex);
        }
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.H(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getDisplayName(), false);
        SafeParcelWriter.writeLong(parcel, 3, this.i());
        SafeParcelWriter.writeString(parcel, 4, this.getPhoneNumber(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

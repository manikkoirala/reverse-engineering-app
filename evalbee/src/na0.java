import com.google.firebase.installations.local.b;
import com.google.android.gms.tasks.TaskCompletionSource;

// 
// Decompiled by Procyon v0.6.0
// 

public class na0 implements yp1
{
    public final TaskCompletionSource a;
    
    public na0(final TaskCompletionSource a) {
        this.a = a;
    }
    
    @Override
    public boolean a(final Exception ex) {
        return false;
    }
    
    @Override
    public boolean b(final b b) {
        if (!b.l() && !b.k() && !b.i()) {
            return false;
        }
        this.a.trySetResult((Object)b.d());
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class bu1
{
    public int a;
    public int b;
    
    public bu1(final int n, final int n2) {
        g9.d((n & 0x1) == n, "Generator ID %d contains more than %d reserved bits", n, 1);
        this.b = n;
        this.d(n2);
    }
    
    public static bu1 a() {
        return new bu1(1, 1);
    }
    
    public static bu1 b(final int n) {
        final bu1 bu1 = new bu1(0, n);
        bu1.c();
        return bu1;
    }
    
    public int c() {
        final int a = this.a;
        this.a = a + 2;
        return a;
    }
    
    public final void d(final int a) {
        g9.d((a & 0x1) == this.b, "Cannot supply target ID from different generator ID", new Object[0]);
        this.a = a;
    }
}

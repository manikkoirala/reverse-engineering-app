import java.util.NoSuchElementException;
import java.util.Comparator;
import java.util.SortedMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d80 extends x70 implements SortedMap
{
    public static int unsafeCompare(final Comparator<?> comparator, final Object o, final Object o2) {
        if (comparator == null) {
            return ((Comparable)o).compareTo(o2);
        }
        return comparator.compare(o, o2);
    }
    
    @Override
    public Comparator<Object> comparator() {
        return this.delegate().comparator();
    }
    
    @Override
    public abstract SortedMap delegate();
    
    @Override
    public Object firstKey() {
        return this.delegate().firstKey();
    }
    
    @Override
    public Object lastKey() {
        return this.delegate().lastKey();
    }
    
    @Override
    public boolean standardContainsKey(final Object o) {
        boolean b = false;
        try {
            if (unsafeCompare(this.comparator(), this.tailMap(o).firstKey(), o) == 0) {
                b = true;
            }
            return b;
        }
        catch (final ClassCastException | NoSuchElementException | NullPointerException ex) {
            return b;
        }
    }
    
    public SortedMap<Object, Object> standardSubMap(final Object o, final Object o2) {
        i71.e(unsafeCompare(this.comparator(), o, o2) <= 0, "fromKey must be <= toKey");
        return this.tailMap(o).headMap(o2);
    }
    
    @Override
    public abstract SortedMap tailMap(final Object p0);
}

import com.google.android.gms.internal.firebase-auth-api.zzafn;
import com.google.android.gms.common.api.internal.BackgroundDetector;
import android.app.Application;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zd2
{
    public volatile int a;
    public final sb2 b;
    public volatile boolean c;
    
    public zd2(final Context context, final sb2 b) {
        this.c = false;
        this.a = 0;
        this.b = b;
        BackgroundDetector.initialize((Application)context.getApplicationContext());
        BackgroundDetector.getInstance().addListener((BackgroundDetector.BackgroundStateChangeListener)new ee2(this));
    }
    
    public zd2(final r10 r10) {
        this(r10.l(), new sb2(r10));
    }
    
    public final void b() {
        this.b.b();
    }
    
    public final void c(final int n) {
        if (n > 0 && this.a == 0) {
            this.a = n;
            if (this.f()) {
                this.b.c();
            }
        }
        else if (n == 0 && this.a != 0) {
            this.b.b();
        }
        this.a = n;
    }
    
    public final void d(final zzafn zzafn) {
        if (zzafn == null) {
            return;
        }
        long zza;
        if ((zza = ((com.google.android.gms.internal.firebase_auth_api.zzafn)zzafn).zza()) <= 0L) {
            zza = 3600L;
        }
        final long zzb = ((com.google.android.gms.internal.firebase_auth_api.zzafn)zzafn).zzb();
        final sb2 b = this.b;
        b.b = zzb + zza * 1000L;
        b.c = -1L;
        if (this.f()) {
            this.b.c();
        }
    }
    
    public final boolean f() {
        return this.a > 0 && !this.c;
    }
}

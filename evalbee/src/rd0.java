import android.icu.util.ULocale;
import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rd0
{
    public static String a(final Locale locale) {
        return a.c(a.a(a.b(locale)));
    }
    
    public abstract static class a
    {
        public static ULocale a(final Object o) {
            return ULocale.addLikelySubtags((ULocale)o);
        }
        
        public static ULocale b(final Locale locale) {
            return ULocale.forLocale(locale);
        }
        
        public static String c(final Object o) {
            return ((ULocale)o).getScript();
        }
    }
}

import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.AbstractAdViewAdapter;
import com.google.android.gms.ads.FullScreenContentCallback;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ef2 extends FullScreenContentCallback
{
    public final AbstractAdViewAdapter a;
    public final MediationInterstitialListener b;
    
    public ef2(final AbstractAdViewAdapter a, final MediationInterstitialListener b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public final void onAdDismissedFullScreenContent() {
        this.b.onAdClosed(this.a);
    }
    
    @Override
    public final void onAdShowedFullScreenContent() {
        this.b.onAdOpened(this.a);
    }
}

import android.net.Uri$Builder;
import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import android.util.Log;
import android.net.Uri;
import com.google.android.gms.common.internal.Preconditions;

// 
// Decompiled by Procyon v0.6.0
// 

public class o30
{
    public final r10 a;
    public final r91 b;
    public final r91 c;
    public final String d;
    public long e;
    public long f;
    public long g;
    public long h;
    
    public o30(final String d, final r10 a, final r91 b, final r91 c) {
        this.e = 600000L;
        this.f = 60000L;
        this.g = 600000L;
        this.h = 120000L;
        this.d = d;
        this.a = a;
        this.b = b;
        this.c = c;
        if (c != null && c.get() != null) {
            ((dg0)c.get()).b(new t5(this) {
                public final o30 a;
                
                @Override
                public void a(final u5 u5) {
                }
            });
        }
    }
    
    public static o30 f() {
        final r10 m = r10.m();
        Preconditions.checkArgument(m != null, (Object)"You must call FirebaseApp.initialize() first.");
        return g(m);
    }
    
    public static o30 g(final r10 r10) {
        Preconditions.checkArgument(r10 != null, (Object)"Null is not a valid value for the FirebaseApp.");
        final String f = r10.p().f();
        if (f == null) {
            return j(r10, null);
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("gs://");
            sb.append(r10.p().f());
            return j(r10, m22.d(r10, sb.toString()));
        }
        catch (final UnsupportedEncodingException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to parse bucket:");
            sb2.append(f);
            Log.e("FirebaseStorage", sb2.toString(), (Throwable)ex);
            throw new IllegalArgumentException("The storage Uri could not be parsed.");
        }
    }
    
    public static o30 h(final r10 r10, final String str) {
        final boolean b = true;
        Preconditions.checkArgument(r10 != null, (Object)"Null is not a valid value for the FirebaseApp.");
        Preconditions.checkArgument(str != null && b, (Object)"Null is not a valid value for the Firebase Storage URL.");
        if (str.toLowerCase().startsWith("gs://")) {
            try {
                return j(r10, m22.d(r10, str));
            }
            catch (final UnsupportedEncodingException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to parse url:");
                sb.append(str);
                Log.e("FirebaseStorage", sb.toString(), (Throwable)ex);
                throw new IllegalArgumentException("The storage Uri could not be parsed.");
            }
        }
        throw new IllegalArgumentException("Please use a gs:// URL for your Firebase Storage bucket.");
    }
    
    public static o30 i(final String s) {
        final r10 m = r10.m();
        Preconditions.checkArgument(m != null, (Object)"You must call FirebaseApp.initialize() first.");
        return h(m, s);
    }
    
    public static o30 j(final r10 r10, final Uri uri) {
        String host;
        if (uri != null) {
            host = uri.getHost();
        }
        else {
            host = null;
        }
        if (uri != null && !TextUtils.isEmpty((CharSequence)uri.getPath())) {
            throw new IllegalArgumentException("The storage Uri cannot contain a path element.");
        }
        Preconditions.checkNotNull(r10, "Provided FirebaseApp must not be null.");
        final p30 p2 = (p30)r10.j(p30.class);
        Preconditions.checkNotNull(p2, "Firebase Storage component is not present.");
        return p2.a(host);
    }
    
    public r10 a() {
        return this.a;
    }
    
    public dg0 b() {
        final r91 c = this.c;
        dg0 dg0;
        if (c != null) {
            dg0 = (dg0)c.get();
        }
        else {
            dg0 = null;
        }
        return dg0;
    }
    
    public zf0 c() {
        final r91 b = this.b;
        zf0 zf0;
        if (b != null) {
            zf0 = (zf0)b.get();
        }
        else {
            zf0 = null;
        }
        return zf0;
    }
    
    public final String d() {
        return this.d;
    }
    
    public ww e() {
        return null;
    }
    
    public long k() {
        return this.f;
    }
    
    public long l() {
        return this.g;
    }
    
    public long m() {
        return this.e;
    }
    
    public jq1 n() {
        if (!TextUtils.isEmpty((CharSequence)this.d())) {
            return this.o(new Uri$Builder().scheme("gs").authority(this.d()).path("/").build());
        }
        throw new IllegalStateException("FirebaseApp was not initialized with a bucket name.");
    }
    
    public final jq1 o(final Uri uri) {
        Preconditions.checkNotNull(uri, "uri must not be null");
        final String d = this.d();
        Preconditions.checkArgument(TextUtils.isEmpty((CharSequence)d) || uri.getAuthority().equalsIgnoreCase(d), (Object)"The supplied bucketname does not match the storage bucket of the current instance.");
        return new jq1(uri, this);
    }
}

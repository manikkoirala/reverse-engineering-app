import android.window.OnBackInvokedDispatcher;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import androidx.savedstate.a;
import androidx.lifecycle.Lifecycle;
import android.content.Context;
import org.jetbrains.annotations.NotNull;
import androidx.activity.OnBackPressedDispatcher;
import org.jetbrains.annotations.Nullable;
import androidx.lifecycle.g;
import android.app.Dialog;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class jj extends Dialog implements qj0, j11, aj1
{
    @Nullable
    private g _lifecycleRegistry;
    @NotNull
    private final OnBackPressedDispatcher onBackPressedDispatcher;
    @NotNull
    private final zi1 savedStateRegistryController;
    
    public jj(final Context context) {
        fg0.e((Object)context, "context");
        this(context, 0, 2, null);
    }
    
    public jj(final Context context, final int n) {
        fg0.e((Object)context, "context");
        super(context, n);
        this.savedStateRegistryController = zi1.d.a(this);
        this.onBackPressedDispatcher = new OnBackPressedDispatcher(new ij(this));
    }
    
    public static final void c(final jj jj) {
        fg0.e((Object)jj, "this$0");
        jj.onBackPressed();
    }
    
    public final g b() {
        g lifecycleRegistry;
        if ((lifecycleRegistry = this._lifecycleRegistry) == null) {
            lifecycleRegistry = new g(this);
            this._lifecycleRegistry = lifecycleRegistry;
        }
        return lifecycleRegistry;
    }
    
    @NotNull
    public Lifecycle getLifecycle() {
        return this.b();
    }
    
    @NotNull
    public final OnBackPressedDispatcher getOnBackPressedDispatcher() {
        return this.onBackPressedDispatcher;
    }
    
    @NotNull
    public a getSavedStateRegistry() {
        return this.savedStateRegistryController.b();
    }
    
    public void initializeViewTreeOwners() {
        final Window window = this.getWindow();
        fg0.b((Object)window);
        final View decorView = window.getDecorView();
        fg0.d((Object)decorView, "window!!.decorView");
        o42.a(decorView, this);
        final Window window2 = this.getWindow();
        fg0.b((Object)window2);
        final View decorView2 = window2.getDecorView();
        fg0.d((Object)decorView2, "window!!.decorView");
        p42.a(decorView2, this);
        final Window window3 = this.getWindow();
        fg0.b((Object)window3);
        final View decorView3 = window3.getDecorView();
        fg0.d((Object)decorView3, "window!!.decorView");
        q42.a(decorView3, this);
    }
    
    public void onBackPressed() {
        this.onBackPressedDispatcher.k();
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (Build$VERSION.SDK_INT >= 33) {
            final OnBackPressedDispatcher onBackPressedDispatcher = this.onBackPressedDispatcher;
            final OnBackInvokedDispatcher a = hj.a(this);
            fg0.d((Object)a, "onBackInvokedDispatcher");
            onBackPressedDispatcher.n(a);
        }
        this.savedStateRegistryController.d(bundle);
        this.b().h(Lifecycle.Event.ON_CREATE);
    }
    
    @NotNull
    public Bundle onSaveInstanceState() {
        final Bundle onSaveInstanceState = super.onSaveInstanceState();
        fg0.d((Object)onSaveInstanceState, "super.onSaveInstanceState()");
        this.savedStateRegistryController.e(onSaveInstanceState);
        return onSaveInstanceState;
    }
    
    public void onStart() {
        super.onStart();
        this.b().h(Lifecycle.Event.ON_RESUME);
    }
    
    public void onStop() {
        this.b().h(Lifecycle.Event.ON_DESTROY);
        this._lifecycleRegistry = null;
        super.onStop();
    }
}

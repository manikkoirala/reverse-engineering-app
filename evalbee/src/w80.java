import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public final class w80
{
    public final Executor a;
    public final a90 b;
    public final Object c;
    public int d;
    public boolean e;
    public boolean f;
    public final List g;
    public final Runnable h;
    
    public w80(final Executor a, final a90 b) {
        fg0.e((Object)a, "executor");
        fg0.e((Object)b, "reportFullyDrawn");
        this.a = a;
        this.b = b;
        this.c = new Object();
        this.g = new ArrayList();
        this.h = new v80(this);
    }
    
    public static final void g(final w80 w80) {
        fg0.e((Object)w80, "this$0");
        synchronized (w80.c) {
            w80.e = false;
            if (w80.d == 0 && !w80.f) {
                w80.b.invoke();
                w80.c();
            }
            final u02 a = u02.a;
        }
    }
    
    public final void b() {
        synchronized (this.c) {
            if (!this.f) {
                ++this.d;
            }
            final u02 a = u02.a;
        }
    }
    
    public final void c() {
        synchronized (this.c) {
            this.f = true;
            final Iterator iterator = this.g.iterator();
            while (iterator.hasNext()) {
                ((a90)iterator.next()).invoke();
            }
            this.g.clear();
            final u02 a = u02.a;
        }
    }
    
    public final boolean d() {
        synchronized (this.c) {
            return this.f;
        }
    }
    
    public final void e() {
        if (!this.e && this.d == 0) {
            this.e = true;
            this.a.execute(this.h);
        }
    }
    
    public final void f() {
        synchronized (this.c) {
            if (!this.f) {
                final int d = this.d;
                if (d > 0) {
                    this.d = d - 1;
                    this.e();
                }
            }
            final u02 a = u02.a;
        }
    }
}

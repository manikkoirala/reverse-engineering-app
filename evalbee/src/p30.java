import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class p30
{
    public final Map a;
    public final r10 b;
    public final r91 c;
    public final r91 d;
    
    public p30(final r10 b, final r91 c, final r91 d, final Executor executor, final Executor executor2) {
        this.a = new HashMap();
        this.b = b;
        this.c = c;
        this.d = d;
        br1.c(executor, executor2);
    }
    
    public o30 a(final String s) {
        synchronized (this) {
            o30 o30;
            if ((o30 = this.a.get(s)) == null) {
                o30 = new o30(s, this.b, this.c, this.d);
                this.a.put(s, o30);
            }
            return o30;
        }
    }
}

import android.os.BaseBundle;
import java.util.Collections;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import android.os.Bundle;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import com.google.firebase.components.InvalidRegistrarException;
import android.content.Context;
import com.google.firebase.components.ComponentRegistrar;

// 
// Decompiled by Procyon v0.6.0
// 

public final class lj
{
    public final Object a;
    public final c b;
    
    public lj(final Object a, final c b) {
        this.a = a;
        this.b = b;
    }
    
    public static lj c(final Context context, final Class clazz) {
        return new lj(context, (c)new b(clazz, null));
    }
    
    public static ComponentRegistrar d(final String className) {
        try {
            final Class<?> forName = Class.forName(className);
            if (ComponentRegistrar.class.isAssignableFrom(forName)) {
                return (ComponentRegistrar)forName.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            }
            throw new InvalidRegistrarException(String.format("Class %s is not an instance of %s", className, "com.google.firebase.components.ComponentRegistrar"));
        }
        catch (final InvocationTargetException ex) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s", className), ex);
        }
        catch (final NoSuchMethodException ex2) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s", className), ex2);
        }
        catch (final InstantiationException ex3) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s.", className), ex3);
        }
        catch (final IllegalAccessException ex4) {
            throw new InvalidRegistrarException(String.format("Could not instantiate %s.", className), ex4);
        }
        catch (final ClassNotFoundException ex5) {
            Log.w("ComponentDiscovery", String.format("Class %s is not an found.", className));
            return null;
        }
    }
    
    public List b() {
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.b.a(this.a).iterator();
        while (iterator.hasNext()) {
            list.add(new kj((String)iterator.next()));
        }
        return list;
    }
    
    public static class b implements c
    {
        public final Class a;
        
        public b(final Class a) {
            this.a = a;
        }
        
        public final Bundle b(final Context context) {
            try {
                final PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("ComponentDiscovery", "Context has no PackageManager.");
                    return null;
                }
                final ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, this.a), 128);
                if (serviceInfo == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a);
                    sb.append(" has no service info.");
                    Log.w("ComponentDiscovery", sb.toString());
                    return null;
                }
                return serviceInfo.metaData;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.w("ComponentDiscovery", "Application info not found.");
                return null;
            }
        }
        
        public List c(final Context context) {
            final Bundle b = this.b(context);
            if (b == null) {
                Log.w("ComponentDiscovery", "Could not retrieve metadata, returning empty list of registrars.");
                return Collections.emptyList();
            }
            final ArrayList list = new ArrayList();
            for (final String s : ((BaseBundle)b).keySet()) {
                if ("com.google.firebase.components.ComponentRegistrar".equals(((BaseBundle)b).get(s)) && s.startsWith("com.google.firebase.components:")) {
                    list.add(s.substring(31));
                }
            }
            return list;
        }
    }
    
    public interface c
    {
        List a(final Object p0);
    }
}

import java.util.Iterator;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v70 extends a80 implements Iterator
{
    public abstract Iterator b();
    
    @Override
    public boolean hasNext() {
        return this.b().hasNext();
    }
    
    @Override
    public Object next() {
        return this.b().next();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class g9
{
    public static AssertionError a(final String s, final Object... array) {
        throw new AssertionError((Object)c(s, array));
    }
    
    public static AssertionError b(final Throwable t, final String s, final Object... array) {
        throw o5.b(c(s, array), t);
    }
    
    public static String c(final String format, final Object... args) {
        final StringBuilder sb = new StringBuilder();
        sb.append("INTERNAL ASSERTION FAILED: ");
        sb.append(String.format(format, args));
        return sb.toString();
    }
    
    public static void d(final boolean b, final String s, final Object... array) {
        if (b) {
            return;
        }
        throw a(s, array);
    }
    
    public static Object e(final Object o, final String s, final Object... array) {
        if (o != null) {
            return o;
        }
        throw a(s, array);
    }
}

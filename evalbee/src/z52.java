import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class z52 extends ConstraintWidget
{
    public ArrayList L0;
    
    public z52() {
        this.L0 = new ArrayList();
    }
    
    public void a(final ConstraintWidget e) {
        this.L0.add(e);
        if (e.K() != null) {
            ((z52)e.K()).t1(e);
        }
        e.c1(this);
    }
    
    public ArrayList r1() {
        return this.L0;
    }
    
    public abstract void s1();
    
    @Override
    public void t0() {
        this.L0.clear();
        super.t0();
    }
    
    public void t1(final ConstraintWidget o) {
        this.L0.remove(o);
        o.t0();
    }
    
    public void u1() {
        this.L0.clear();
    }
    
    @Override
    public void w0(final oe oe) {
        super.w0(oe);
        for (int size = this.L0.size(), i = 0; i < size; ++i) {
            ((ConstraintWidget)this.L0.get(i)).w0(oe);
        }
    }
}

import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import android.content.Intent;
import org.json.JSONException;
import android.text.TextUtils;
import org.json.JSONObject;
import com.google.android.gms.common.api.Status;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class je2
{
    public static final Map a;
    
    static {
        final HashMap a2 = new HashMap();
        (a = a2).put("auth/invalid-provider-id", "INVALID_PROVIDER_ID");
        a2.put("auth/invalid-cert-hash", "INVALID_CERT_HASH");
        a2.put("auth/network-request-failed", "WEB_NETWORK_REQUEST_FAILED");
        a2.put("auth/web-storage-unsupported", "WEB_STORAGE_UNSUPPORTED");
        a2.put("auth/operation-not-allowed", "OPERATION_NOT_ALLOWED");
    }
    
    public static Status a(final String s) {
        try {
            final JSONObject jsonObject = new JSONObject(s);
            final String string = jsonObject.getString("code");
            final String string2 = jsonObject.getString("message");
            if (!TextUtils.isEmpty((CharSequence)string) && !TextUtils.isEmpty((CharSequence)string2)) {
                final Map a = je2.a;
                if (a.containsKey(string)) {
                    final String str = a.get(string);
                    final StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(":");
                    sb.append(string2);
                    return ob2.a(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder("WEB_INTERNAL_ERROR:");
            sb2.append(s);
            return ob2.a(sb2.toString());
        }
        catch (final JSONException ex) {
            final String localizedMessage = ((Throwable)ex).getLocalizedMessage();
            final StringBuilder sb3 = new StringBuilder("WEB_INTERNAL_ERROR:");
            sb3.append(s);
            sb3.append("[ ");
            sb3.append(localizedMessage);
            sb3.append(" ]");
            return ob2.a(sb3.toString());
        }
    }
    
    public static void b(final Intent intent, final Status status) {
        SafeParcelableSerializer.serializeToIntentExtra(status, intent, "com.google.firebase.auth.internal.STATUS");
    }
}

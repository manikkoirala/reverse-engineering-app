import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import java.io.IOException;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.content.Context;
import android.util.Log;
import android.os.PowerManager;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import com.google.firebase.messaging.FirebaseMessaging;
import android.os.PowerManager$WakeLock;

// 
// Decompiled by Procyon v0.6.0
// 

public class at1 implements Runnable
{
    public final long a;
    public final PowerManager$WakeLock b;
    public final FirebaseMessaging c;
    public ExecutorService d;
    
    public at1(final FirebaseMessaging c, final long a) {
        this.d = new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new NamedThreadFactory("firebase-iid-executor"));
        this.c = c;
        this.a = a;
        (this.b = ((PowerManager)this.b().getSystemService("power")).newWakeLock(1, "fiid-sync")).setReferenceCounted(false);
    }
    
    public static /* synthetic */ FirebaseMessaging a(final at1 at1) {
        return at1.c;
    }
    
    public static boolean c() {
        return Log.isLoggable("FirebaseMessaging", 3);
    }
    
    public Context b() {
        return this.c.k();
    }
    
    public boolean d() {
        final ConnectivityManager connectivityManager = (ConnectivityManager)this.b().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        else {
            activeNetworkInfo = null;
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    public boolean e() {
        try {
            if (this.c.i() == null) {
                Log.e("FirebaseMessaging", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "Token successfully retrieved");
            }
            return true;
        }
        catch (final SecurityException ex) {}
        catch (final IOException ex2) {
            if (ib0.g(ex2.getMessage())) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Token retrieval failed: ");
                sb.append(ex2.getMessage());
                sb.append(". Will retry token retrieval");
                sb.toString();
                goto Label_0043;
            }
            if (ex2.getMessage() == null) {
                goto Label_0043;
            }
            throw ex2;
        }
    }
    
    @Override
    public void run() {
        Label_0020: {
            if (!sl1.b().e(this.b())) {
                break Label_0020;
            }
            this.b.acquire();
            try {
                while (true) {
                    try {
                        this.c.A(true);
                        if (!this.c.t()) {
                            this.c.A(false);
                            if (sl1.b().e(this.b())) {
                                this.b.release();
                            }
                            return;
                        }
                        if (sl1.b().d(this.b()) && !this.d()) {
                            new a(this).a();
                            if (sl1.b().e(this.b())) {
                                this.b.release();
                            }
                            return;
                        }
                        if (this.e()) {
                            this.c.A(false);
                        }
                        else {
                            this.c.D(this.a);
                        }
                        if (sl1.b().e(this.b())) {
                            this.b.release();
                        }
                    }
                    finally {
                        if (sl1.b().e(this.b())) {
                            this.b.release();
                        }
                        continue;
                    }
                    break;
                }
            }
            catch (final IOException ex) {}
        }
    }
    
    public static class a extends BroadcastReceiver
    {
        public at1 a;
        
        public a(final at1 a) {
            this.a = a;
        }
        
        public void a() {
            if (at1.c()) {
                Log.d("FirebaseMessaging", "Connectivity change received registered");
            }
            this.a.b().registerReceiver((BroadcastReceiver)this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        
        public void onReceive(final Context context, final Intent intent) {
            final at1 a = this.a;
            if (a == null) {
                return;
            }
            if (!a.d()) {
                return;
            }
            if (at1.c()) {
                Log.d("FirebaseMessaging", "Connectivity changed. Starting background sync.");
            }
            at1.a(this.a).j(this.a, 0L);
            this.a.b().unregisterReceiver((BroadcastReceiver)this);
            this.a = null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class w11 extends oz
{
    public oz b;
    public oz c;
    
    public w11(final oz oz, final oz oz2) {
        this.b = null;
        this.c = null;
        this.i(oz);
        this.j(oz2);
    }
    
    public abstract String h();
    
    public void i(final oz b) {
        this.a(b);
        final oz b2 = this.b;
        if (b2 != null) {
            b2.a = null;
        }
        b.a = this;
        this.b = b;
    }
    
    public void j(final oz c) {
        this.a(c);
        final oz c2 = this.c;
        if (c2 != null) {
            c2.a = null;
        }
        c.a = this;
        this.c = c;
    }
}

import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class av1
{
    public static final zu1 a;
    public static final zu1 b;
    public static final zu1 c;
    public static final zu1 d;
    public static final zu1 e;
    public static final zu1 f;
    
    static {
        a = new e(null, false);
        b = new e(null, true);
        final b a2 = av1.b.a;
        c = new e(a2, false);
        d = new e(a2, true);
        e = new e(av1.a.b, false);
        f = av1.f.b;
    }
    
    public static int a(final int n) {
        if (n == 0) {
            return 1;
        }
        if (n != 1 && n != 2) {
            return 2;
        }
        return 0;
    }
    
    public static int b(final int n) {
        if (n != 0) {
            if (n != 1 && n != 2) {
                switch (n) {
                    default: {
                        return 2;
                    }
                    case 16:
                    case 17: {
                        break;
                    }
                    case 14:
                    case 15: {
                        return 1;
                    }
                }
            }
            return 0;
        }
        return 1;
    }
    
    public static class a implements c
    {
        public static final a b;
        public final boolean a;
        
        static {
            b = new a(true);
        }
        
        public a(final boolean a) {
            this.a = a;
        }
        
        @Override
        public int a(final CharSequence charSequence, final int n, final int n2) {
            boolean b = false;
            for (int i = n; i < n2 + n; ++i) {
                final int a = av1.a(Character.getDirectionality(charSequence.charAt(i)));
                if (a != 0) {
                    if (a != 1) {
                        continue;
                    }
                    if (!this.a) {
                        return 1;
                    }
                }
                else if (this.a) {
                    return 0;
                }
                b = true;
            }
            if (b) {
                return this.a ? 1 : 0;
            }
            return 2;
        }
    }
    
    public static class b implements c
    {
        public static final b a;
        
        static {
            a = new b();
        }
        
        @Override
        public int a(final CharSequence charSequence, final int n, final int n2) {
            int b = 2;
            for (int n3 = n; n3 < n2 + n && b == 2; b = av1.b(Character.getDirectionality(charSequence.charAt(n3))), ++n3) {}
            return b;
        }
    }
    
    public interface c
    {
        int a(final CharSequence p0, final int p1, final int p2);
    }
    
    public abstract static class d implements zu1
    {
        public final c a;
        
        public d(final c a) {
            this.a = a;
        }
        
        @Override
        public boolean a(final CharSequence charSequence, final int n, final int n2) {
            if (charSequence == null || n < 0 || n2 < 0 || charSequence.length() - n2 < n) {
                throw new IllegalArgumentException();
            }
            if (this.a == null) {
                return this.b();
            }
            return this.c(charSequence, n, n2);
        }
        
        public abstract boolean b();
        
        public final boolean c(final CharSequence charSequence, int a, final int n) {
            a = this.a.a(charSequence, a, n);
            return a == 0 || (a != 1 && this.b());
        }
    }
    
    public static class e extends d
    {
        public final boolean b;
        
        public e(final c c, final boolean b) {
            super(c);
            this.b = b;
        }
        
        @Override
        public boolean b() {
            return this.b;
        }
    }
    
    public static class f extends d
    {
        public static final f b;
        
        static {
            b = new f();
        }
        
        public f() {
            super(null);
        }
        
        @Override
        public boolean b() {
            final int a = mv1.a(Locale.getDefault());
            boolean b = true;
            if (a != 1) {
                b = false;
            }
            return b;
        }
    }
}

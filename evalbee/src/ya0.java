import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class ya0
{
    public String a;
    public Map b;
    
    public ya0(final String a, final Map b) {
        this.a = a;
        this.b = b;
    }
    
    public Map a() {
        return this.b;
    }
    
    public String b() {
        final Map map = this.b.get("firebase");
        if (map != null) {
            return (String)map.get("sign_in_provider");
        }
        return null;
    }
    
    public String c() {
        return this.a;
    }
}

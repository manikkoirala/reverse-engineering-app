import android.util.Log;

// 
// Decompiled by Procyon v0.6.0
// 

public class yl0
{
    public static final yl0 c;
    public final String a;
    public int b;
    
    static {
        c = new yl0("FirebaseAppCheck");
    }
    
    public yl0(final String a) {
        this.a = a;
        this.b = 4;
    }
    
    public static yl0 f() {
        return yl0.c;
    }
    
    public final boolean a(final int n) {
        return this.b <= n || Log.isLoggable(this.a, n);
    }
    
    public void b(final String s) {
        this.c(s, null);
    }
    
    public void c(final String s, final Throwable t) {
        if (this.a(3)) {
            Log.d(this.a, s, t);
        }
    }
    
    public void d(final String s) {
        this.e(s, null);
    }
    
    public void e(final String s, final Throwable t) {
        if (this.a(6)) {
            Log.e(this.a, s, t);
        }
    }
}

import android.view.ViewOverlay;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

// 
// Decompiled by Procyon v0.6.0
// 

public class t32 implements u32
{
    public final ViewGroupOverlay a;
    
    public t32(final ViewGroup viewGroup) {
        this.a = viewGroup.getOverlay();
    }
    
    @Override
    public void add(final Drawable drawable) {
        ((ViewOverlay)this.a).add(drawable);
    }
    
    @Override
    public void add(final View view) {
        this.a.add(view);
    }
    
    @Override
    public void remove(final Drawable drawable) {
        ((ViewOverlay)this.a).remove(drawable);
    }
    
    @Override
    public void remove(final View view) {
        this.a.remove(view);
    }
}

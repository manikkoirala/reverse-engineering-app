import com.ekodroid.omrevaluator.serializable.SharedType;
import com.ekodroid.omrevaluator.templateui.models.ExamId;

// 
// Decompiled by Procyon v0.6.0
// 

public class ay
{
    public ExamId a;
    public int b;
    public int c;
    public int d;
    public boolean e;
    public boolean f;
    public boolean g;
    public int h;
    public String i;
    public boolean j;
    public SharedType k;
    
    public ay(final ExamId a, final int b, final int c, final int d, final boolean e, final boolean f, final boolean g, final int h, final String i, final boolean j, final SharedType k) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
    }
    
    public String a() {
        return this.i;
    }
    
    public ExamId b() {
        return this.a;
    }
    
    public int c() {
        return this.b;
    }
    
    public int d() {
        return this.c;
    }
    
    public SharedType e() {
        return this.k;
    }
    
    public boolean f() {
        return this.j;
    }
    
    public void g(final int h) {
        this.h = h;
    }
}

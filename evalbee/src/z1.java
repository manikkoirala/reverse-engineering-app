import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public class z1 extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<z1> CREATOR;
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final boolean e;
    public final String f;
    public final boolean g;
    public String h;
    public int i;
    public String j;
    
    static {
        CREATOR = (Parcelable$Creator)new mc2();
    }
    
    public z1(final String a, final String b, final String c, final String d, final boolean e, final String f, final boolean g, final String h, final int i, final String j) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }
    
    public z1(final a a) {
        this.a = z1.a.j(a);
        this.b = z1.a.i(a);
        this.c = null;
        this.d = z1.a.g(a);
        this.e = z1.a.k(a);
        this.f = z1.a.f(a);
        this.g = z1.a.l(a);
        this.j = z1.a.h(a);
    }
    
    public static a R() {
        return new a(null);
    }
    
    public boolean E() {
        return this.e;
    }
    
    public String H() {
        return this.f;
    }
    
    public String J() {
        return this.d;
    }
    
    public String K() {
        return this.b;
    }
    
    public String O() {
        return this.a;
    }
    
    public final void Z(final String h) {
        this.h = h;
    }
    
    public boolean i() {
        return this.g;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.O(), false);
        SafeParcelWriter.writeString(parcel, 2, this.K(), false);
        SafeParcelWriter.writeString(parcel, 3, this.c, false);
        SafeParcelWriter.writeString(parcel, 4, this.J(), false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.E());
        SafeParcelWriter.writeString(parcel, 6, this.H(), false);
        SafeParcelWriter.writeBoolean(parcel, 7, this.i());
        SafeParcelWriter.writeString(parcel, 8, this.h, false);
        SafeParcelWriter.writeInt(parcel, 9, this.i);
        SafeParcelWriter.writeString(parcel, 10, this.j, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final int zza() {
        return this.i;
    }
    
    public final void zza(final int i) {
        this.i = i;
    }
    
    public final String zzc() {
        return this.j;
    }
    
    public final String zzd() {
        return this.c;
    }
    
    public final String zze() {
        return this.h;
    }
    
    public static class a
    {
        public String a;
        public String b;
        public String c;
        public boolean d;
        public String e;
        public boolean f;
        public String g;
        
        public a() {
            this.f = false;
        }
        
        public z1 a() {
            if (this.a != null) {
                return new z1(this, null);
            }
            throw new IllegalArgumentException("Cannot build ActionCodeSettings with null URL. Call #setUrl(String) before calling build()");
        }
        
        public a b(final String c, final boolean d, final String e) {
            this.c = c;
            this.d = d;
            this.e = e;
            return this;
        }
        
        public a c(final boolean f) {
            this.f = f;
            return this;
        }
        
        public a d(final String b) {
            this.b = b;
            return this;
        }
        
        public a e(final String a) {
            this.a = a;
            return this;
        }
    }
}

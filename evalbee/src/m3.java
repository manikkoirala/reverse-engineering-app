import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.templateui.models.QueEditDataModel;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class m3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    
    public m3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int n, View view, final ViewGroup viewGroup) {
        final QueEditDataModel queEditDataModel = this.b.get(n);
        final LayoutInflater layoutInflater = (LayoutInflater)this.a.getSystemService("layout_inflater");
        if (queEditDataModel.i() == QueEditDataModel.ViewType.LABEL) {
            view = layoutInflater.inflate(2131493027, (ViewGroup)null);
            final TextView textView = (TextView)view.findViewById(2131297300);
            final TextView textView2 = (TextView)view.findViewById(2131297279);
            textView.setText((CharSequence)queEditDataModel.h());
            textView2.setText((CharSequence)queEditDataModel.f());
        }
        else {
            view = layoutInflater.inflate(2131493026, (ViewGroup)null);
            final TextView textView3 = (TextView)view.findViewById(2131297265);
            final TextView textView4 = (TextView)view.findViewById(2131297237);
            final TextView textView5 = (TextView)view.findViewById(2131297216);
            final ImageView imageView = (ImageView)view.findViewById(2131296698);
            final StringBuilder sb = new StringBuilder();
            sb.append(queEditDataModel.d());
            sb.append("");
            textView3.setText((CharSequence)sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(queEditDataModel.b());
            sb2.append("");
            textView4.setText((CharSequence)sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(queEditDataModel.a());
            sb3.append("");
            textView5.setText((CharSequence)sb3.toString());
            if (queEditDataModel.j()) {
                n = 0;
            }
            else {
                n = 4;
            }
            imageView.setVisibility(n);
        }
        return view;
    }
}

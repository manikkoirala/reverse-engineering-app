import android.view.View;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class sb0 extends m7
{
    public int a;
    public int b;
    
    public sb0(final Context context) {
        super(context);
        this.setTextSize(16.0f);
        this.setTextColor(((View)this).getResources().getColor(2131099715));
        ((View)this).setBackground(((View)this).getResources().getDrawable(2131230855));
        this.setGravity(17);
    }
    
    public int getColumn() {
        return this.b;
    }
    
    public int getRow() {
        return this.a;
    }
    
    public void setColumn(final int b) {
        this.b = b;
    }
    
    public void setRow(final int a) {
        this.a = a;
    }
}

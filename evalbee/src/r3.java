import android.widget.AdapterView;
import com.ekodroid.omrevaluator.more.models.Teacher;
import android.widget.ListView;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import java.util.ArrayList;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class r3 extends BaseAdapter
{
    public ArrayList a;
    public Context b;
    
    public r3(final ArrayList a, final Context b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.a.size();
    }
    
    public Object getItem(final int index) {
        return this.a.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final tu1 tu1 = this.a.get(index);
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131493032, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297387);
        final TextView textView2 = (TextView)inflate.findViewById(2131297388);
        final TextView textView3 = (TextView)inflate.findViewById(2131297380);
        final TextView textView4 = (TextView)inflate.findViewById(2131297385);
        final TextView textView5 = (TextView)inflate.findViewById(2131297384);
        final TextView textView6 = (TextView)inflate.findViewById(2131297383);
        final TextView textView7 = (TextView)inflate.findViewById(2131297391);
        final TextView textView8 = (TextView)inflate.findViewById(2131297389);
        final TextView textView9 = (TextView)inflate.findViewById(2131297303);
        final Teacher b = tu1.b;
        if (b != null) {
            textView.setText((CharSequence)b.getDisplayName());
            textView2.setText((CharSequence)tu1.b.getEmailId());
            if (tu1.b.getDisplayName() != null && tu1.b.getDisplayName().length() > 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append(tu1.b.getDisplayName().charAt(0));
                sb.append("");
                textView9.setText((CharSequence)sb.toString());
            }
            if (tu1.b.getUserRole() == UserRole.OWNER) {
                textView8.setText(2131886128);
            }
            else {
                ((View)textView8).setVisibility(8);
            }
            ((View)textView3).setVisibility(8);
            ((View)textView4).setVisibility(8);
            ((View)textView6).setVisibility(8);
            ((View)textView7).setVisibility(8);
            ((View)textView5).setVisibility(8);
            switch (r3$b.a[tu1.b.getMemberStatus().ordinal()]) {
                case 6: {
                    ((View)textView3).setVisibility(8);
                    ((View)textView4).setVisibility(8);
                    ((View)textView6).setVisibility(8);
                    ((View)textView7).setVisibility(8);
                    ((View)textView5).setVisibility(8);
                    break;
                }
                case 5: {
                    ((View)textView5).setVisibility(0);
                    break;
                }
                case 4: {
                    ((View)textView7).setVisibility(0);
                    break;
                }
                case 3: {
                    ((View)textView6).setVisibility(0);
                    break;
                }
                case 2: {
                    ((View)textView3).setVisibility(0);
                    break;
                }
                case 1: {
                    ((View)textView4).setVisibility(0);
                    break;
                }
            }
            if (tu1.a()) {
                inflate.findViewById(2131296657).setVisibility(0);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(tu1.b.getDisplayName());
                sb2.append(this.b.getResources().getString(2131886904));
                textView.setText((CharSequence)sb2.toString());
                inflate.findViewById(2131296657).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, viewGroup, index) {
                    public final ViewGroup a;
                    public final int b;
                    public final r3 c;
                    
                    public void onClick(final View view) {
                        ((AdapterView)this.a).performItemClick(view, this.b, (long)view.getId());
                    }
                });
            }
            else {
                inflate.findViewById(2131296657).setVisibility(8);
            }
        }
        return inflate;
    }
}

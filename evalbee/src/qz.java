import com.google.protobuf.l;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class qz
{
    public static final Class a;
    
    static {
        a = c();
    }
    
    public static l a() {
        l l = b("getEmptyRegistry");
        if (l == null) {
            l = com.google.protobuf.l.d;
        }
        return l;
    }
    
    public static final l b(final String name) {
        final Class a = qz.a;
        if (a == null) {
            return null;
        }
        try {
            return (l)a.getDeclaredMethod(name, (Class[])new Class[0]).invoke(null, new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public static Class c() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        }
        catch (final ClassNotFoundException ex) {
            return null;
        }
    }
}

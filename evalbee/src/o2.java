import java.util.ArrayList;
import java.util.Map;
import kotlin.Pair;
import java.util.LinkedHashMap;
import kotlin.collections.b;
import android.content.Intent;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class o2 extends n2
{
    public static final a a;
    
    static {
        a = new a(null);
    }
    
    public Intent d(final Context context, final String[] array) {
        fg0.e((Object)context, "context");
        fg0.e((Object)array, "input");
        return o2.a.a(array);
    }
    
    public n2.a e(final Context context, final String[] array) {
        fg0.e((Object)context, "context");
        fg0.e((Object)array, "input");
        final int length = array.length;
        final int n = 1;
        final int n2 = 0;
        if (length == 0) {
            return new n2.a(b.h());
        }
        final int length2 = array.length;
        int n3 = 0;
        int n4;
        while (true) {
            n4 = n;
            if (n3 >= length2) {
                break;
            }
            if (sl.checkSelfPermission(context, array[n3]) != 0) {
                n4 = 0;
                break;
            }
            ++n3;
        }
        Object o;
        if (n4 != 0) {
            final LinkedHashMap linkedHashMap = new LinkedHashMap(ic1.a(en0.e(array.length), 16));
            for (int length3 = array.length, i = n2; i < length3; ++i) {
                final Pair a = dz1.a((Object)array[i], (Object)Boolean.TRUE);
                linkedHashMap.put(a.getFirst(), a.getSecond());
            }
            o = new n2.a(linkedHashMap);
        }
        else {
            o = null;
        }
        return (n2.a)o;
    }
    
    public Map f(int i, final Intent intent) {
        if (i != -1) {
            return b.h();
        }
        if (intent == null) {
            return b.h();
        }
        final String[] stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS");
        final int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
        if (intArrayExtra != null && stringArrayExtra != null) {
            final ArrayList list = new ArrayList(intArrayExtra.length);
            int length;
            for (length = intArrayExtra.length, i = 0; i < length; ++i) {
                list.add((Object)(intArrayExtra[i] == 0));
            }
            return b.p((Iterable)vh.U((Iterable)a9.n((Object[])stringArrayExtra), (Iterable)list));
        }
        return b.h();
    }
    
    public static final class a
    {
        public final Intent a(final String[] array) {
            fg0.e((Object)array, "input");
            final Intent putExtra = new Intent("androidx.activity.result.contract.action.REQUEST_PERMISSIONS").putExtra("androidx.activity.result.contract.extra.PERMISSIONS", array);
            fg0.d((Object)putExtra, "Intent(ACTION_REQUEST_PE\u2026EXTRA_PERMISSIONS, input)");
            return putExtra;
        }
    }
}

import java.util.HashSet;
import com.google.firebase.firestore.util.Logger;
import com.google.firebase.firestore.local.IndexManager;
import java.util.Iterator;
import java.util.Map;
import com.google.firebase.firestore.model.FieldIndex;
import java.util.Objects;
import com.google.firebase.firestore.local.a;
import com.google.firebase.firestore.util.AsyncQueue;
import java.util.concurrent.TimeUnit;

// 
// Decompiled by Procyon v0.6.0
// 

public class we0
{
    public static final long f;
    public static final long g;
    public final a a;
    public final d51 b;
    public final is1 c;
    public final is1 d;
    public int e;
    
    static {
        f = TimeUnit.SECONDS.toMillis(15L);
        g = TimeUnit.MINUTES.toMillis(1L);
    }
    
    public we0(final d51 d51, final AsyncQueue asyncQueue, final com.google.firebase.firestore.local.a obj) {
        Objects.requireNonNull(obj);
        this(d51, asyncQueue, new se0(obj), new te0(obj));
    }
    
    public we0(final d51 b, final AsyncQueue asyncQueue, final is1 c, final is1 d) {
        this.e = 50;
        this.b = b;
        this.a = new a(asyncQueue);
        this.c = c;
        this.d = d;
    }
    
    public static /* synthetic */ long b() {
        return we0.f;
    }
    
    public int d() {
        return (int)this.b.j("Backfill Indexes", new ue0(this));
    }
    
    public final FieldIndex.a e(final FieldIndex.a a, final wk0 wk0) {
        final Iterator iterator = wk0.c().iterator();
        Comparable comparable = a;
        while (iterator.hasNext()) {
            final FieldIndex.a f = FieldIndex.a.f(iterator.next().getValue());
            if (f.c((FieldIndex.a)comparable) > 0) {
                comparable = f;
            }
        }
        return FieldIndex.a.d(((FieldIndex.a)comparable).j(), ((FieldIndex.a)comparable).g(), Math.max(wk0.b(), a.h()));
    }
    
    public a f() {
        return this.a;
    }
    
    public final int h(final String s, final int n) {
        final IndexManager indexManager = (IndexManager)this.c.get();
        final xk0 xk0 = (xk0)this.d.get();
        final FieldIndex.a e = indexManager.e(s);
        final wk0 k = xk0.k(s, e, n);
        indexManager.b(k.c());
        final FieldIndex.a e2 = this.e(e, k);
        Logger.a("IndexBackfiller", "Updating offset: %s", e2);
        indexManager.f(s, e2);
        return k.c().size();
    }
    
    public final int i() {
        final IndexManager indexManager = (IndexManager)this.c.get();
        final HashSet set = new HashSet();
        int i = this.e;
        while (i > 0) {
            final String h = indexManager.h();
            if (h == null) {
                break;
            }
            if (set.contains(h)) {
                break;
            }
            Logger.a("IndexBackfiller", "Processing collection: %s", h);
            i -= this.h(h, i);
            set.add(h);
        }
        return this.e - i;
    }
    
    public class a implements hj1
    {
        public AsyncQueue.b a;
        public final AsyncQueue b;
        public final we0 c;
        
        public a(final we0 c, final AsyncQueue b) {
            this.c = c;
            this.b = b;
        }
        
        public final void c(final long n) {
            this.a = this.b.h(AsyncQueue.TimerId.INDEX_BACKFILL, n, new ve0(this));
        }
        
        @Override
        public void start() {
            this.c(we0.b());
        }
    }
}

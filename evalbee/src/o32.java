import android.view.accessibility.AccessibilityRecord;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;
import android.util.SparseArray;
import android.view.OnReceiveContentListener;
import android.view.ContentInfo;
import android.view.WindowInsetsController;
import android.view.View$OnUnhandledKeyEventListener;
import java.util.Objects;
import java.util.Collection;
import android.view.View$DragShadowBuilder;
import android.content.ClipData;
import android.view.View$OnApplyWindowInsetsListener;
import android.graphics.Paint;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.Iterator;
import java.util.Map;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.view.Display;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import java.util.ArrayList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import java.util.List;
import android.view.View$AccessibilityDelegate;
import android.view.KeyEvent;
import android.os.Bundle;
import android.graphics.Rect;
import android.view.WindowInsets;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.text.TextUtils;
import android.view.ViewParent;
import android.view.Window;
import android.content.Context;
import android.app.Activity;
import android.content.ContextWrapper;
import android.view.PointerIcon;
import android.os.Build$VERSION;
import android.view.View;
import java.lang.reflect.Field;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class o32
{
    public static final AtomicInteger a;
    public static WeakHashMap b;
    public static Field c;
    public static boolean d;
    public static final int[] e;
    public static final q11 f;
    public static final e g;
    
    static {
        a = new AtomicInteger(1);
        o32.b = null;
        o32.d = false;
        e = new int[] { fb1.b, fb1.c, fb1.n, fb1.y, fb1.B, fb1.C, fb1.D, fb1.E, fb1.F, fb1.G, fb1.d, fb1.e, fb1.f, fb1.g, fb1.h, fb1.i, fb1.j, fb1.k, fb1.l, fb1.m, fb1.o, fb1.p, fb1.q, fb1.r, fb1.s, fb1.t, fb1.u, fb1.v, fb1.w, fb1.x, fb1.z, fb1.A };
        f = new n32();
        g = new e();
    }
    
    public static int A(final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return p.b(view);
        }
        return 0;
    }
    
    public static void A0(final View view, final boolean b) {
        h.r(view, b);
    }
    
    public static int B(final View view) {
        return i.d(view);
    }
    
    public static void B0(final View view, final int n) {
        h.s(view, n);
    }
    
    public static int C(final View view) {
        return h.d(view);
    }
    
    public static void C0(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            p.l(view, n);
        }
    }
    
    public static int D(final View view) {
        return h.e(view);
    }
    
    public static void D0(final View view, final int n) {
        i.h(view, n);
    }
    
    public static String[] E(final View view) {
        if (Build$VERSION.SDK_INT >= 31) {
            return t.a(view);
        }
        return (String[])view.getTag(fb1.N);
    }
    
    public static void E0(final View view, final g11 g11) {
        m.u(view, g11);
    }
    
    public static int F(final View view) {
        return i.e(view);
    }
    
    public static void F0(final View view, final int n, final int n2, final int n3, final int n4) {
        i.k(view, n, n2, n3, n4);
    }
    
    public static int G(final View view) {
        return i.f(view);
    }
    
    public static void G0(final View view, final t51 t51) {
        Object a;
        if (t51 != null) {
            a = t51.a();
        }
        else {
            a = null;
        }
        o.d(view, (PointerIcon)a);
    }
    
    public static u62 H(final View view) {
        return n.a(view);
    }
    
    public static void H0(final View view, final boolean b) {
        p0().g(view, b);
    }
    
    public static CharSequence I(final View view) {
        return (CharSequence)P0().f(view);
    }
    
    public static void I0(final View view, final int n, final int n2) {
        o32.n.d(view, n, n2);
    }
    
    public static String J(final View view) {
        return m.k(view);
    }
    
    public static void J0(final View view, final CharSequence charSequence) {
        P0().g(view, charSequence);
    }
    
    public static float K(final View view) {
        return m.l(view);
    }
    
    public static void K0(final View view, final String s) {
        m.v(view, s);
    }
    
    public static v72 L(final View view) {
        if (Build$VERSION.SDK_INT >= 30) {
            return s.b(view);
        }
        Context context = view.getContext();
        while (true) {
            final boolean b = context instanceof ContextWrapper;
            final v72 v72 = null;
            if (!b) {
                return null;
            }
            if (context instanceof Activity) {
                final Window window = ((Activity)context).getWindow();
                v72 a = v72;
                if (window != null) {
                    a = d62.a(window, view);
                }
                return a;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
    }
    
    public static void L0(final View view, final float n) {
        m.w(view, n);
    }
    
    public static int M(final View view) {
        return h.g(view);
    }
    
    public static void M0(final View view) {
        if (z(view) == 0) {
            B0(view, 1);
        }
        for (ViewParent viewParent = view.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
            if (z((View)viewParent) == 4) {
                B0(view, 2);
                break;
            }
        }
    }
    
    public static float N(final View view) {
        return m.m(view);
    }
    
    public static void N0(final View view, final h62.b b) {
        h62.d(view, b);
    }
    
    public static boolean O(final View view) {
        return m(view) != null;
    }
    
    public static void O0(final View view, final float n) {
        m.x(view, n);
    }
    
    public static boolean P(final View view) {
        return o32.g.a(view);
    }
    
    public static f P0() {
        return (f)new f(fb1.P, CharSequence.class, 64, 30) {
            public CharSequence i(final View view) {
                return s.a(view);
            }
            
            public void j(final View view, final CharSequence charSequence) {
                s.c(view, charSequence);
            }
            
            public boolean k(final CharSequence charSequence, final CharSequence charSequence2) {
                return TextUtils.equals(charSequence, charSequence2) ^ true;
            }
        };
    }
    
    public static boolean Q(final View view) {
        return h.h(view);
    }
    
    public static void Q0(final View view) {
        m.z(view);
    }
    
    public static boolean R(final View view) {
        return h.i(view);
    }
    
    public static boolean S(final View view) {
        final Boolean b = (Boolean)b().f(view);
        return b != null && b;
    }
    
    public static boolean T(final View view) {
        return k.b(view);
    }
    
    public static boolean U(final View view) {
        return k.c(view);
    }
    
    public static boolean V(final View view) {
        return m.p(view);
    }
    
    public static boolean W(final View view) {
        return i.g(view);
    }
    
    public static boolean X(final View view) {
        final Boolean b = (Boolean)p0().f(view);
        return b != null && b;
    }
    
    public static void Z(final View source, final int n) {
        final AccessibilityManager accessibilityManager = (AccessibilityManager)source.getContext().getSystemService("accessibility");
        if (!accessibilityManager.isEnabled()) {
            return;
        }
        final boolean b = p(source) != null && source.isShown() && source.getWindowVisibility() == 0;
        final int o = o(source);
        int eventType = 32;
        if (o == 0 && !b) {
            if (n == 32) {
                final AccessibilityEvent obtain = AccessibilityEvent.obtain();
                source.onInitializeAccessibilityEvent(obtain);
                obtain.setEventType(32);
                k.g(obtain, n);
                ((AccessibilityRecord)obtain).setSource(source);
                source.onPopulateAccessibilityEvent(obtain);
                ((AccessibilityRecord)obtain).getText().add(p(source));
                accessibilityManager.sendAccessibilityEvent(obtain);
            }
            else if (source.getParent() != null) {
                final ViewParent parent = source.getParent();
                try {
                    k.e(parent, source, source, n);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(source.getParent().getClass().getSimpleName());
                    sb.append(" does not fully implement ViewParent");
                    Log.e("ViewCompat", sb.toString(), (Throwable)abstractMethodError);
                }
            }
        }
        else {
            final AccessibilityEvent obtain2 = AccessibilityEvent.obtain();
            if (!b) {
                eventType = 2048;
            }
            obtain2.setEventType(eventType);
            k.g(obtain2, n);
            if (b) {
                ((AccessibilityRecord)obtain2).getText().add(p(source));
                M0(source);
            }
            source.sendAccessibilityEventUnchecked(obtain2);
        }
    }
    
    public static void a0(final View view, final int n) {
        view.offsetLeftAndRight(n);
    }
    
    public static f b() {
        return (f)new f(fb1.J, Boolean.class, 28) {
            public Boolean i(final View view) {
                return q.c(view);
            }
            
            public void j(final View view, final Boolean b) {
                q.g(view, b);
            }
            
            public boolean k(final Boolean b, final Boolean b2) {
                return ((f)this).a(b, b2) ^ true;
            }
        };
    }
    
    public static void b0(final View view, final int n) {
        view.offsetTopAndBottom(n);
    }
    
    public static int c(final View view, final CharSequence charSequence, final q1 q1) {
        final int r = r(view, charSequence);
        if (r != -1) {
            d(view, new n1.a(r, charSequence, q1));
        }
        return r;
    }
    
    public static u62 c0(final View view, final u62 u62) {
        final WindowInsets u63 = u62.u();
        if (u63 != null) {
            final WindowInsets b = l.b(view, u63);
            if (!b.equals((Object)u63)) {
                return u62.w(b, view);
            }
        }
        return u62;
    }
    
    public static void d(final View view, final n1.a a) {
        j(view);
        l0(a.b(), view);
        q(view).add(a);
        Z(view, 0);
    }
    
    public static void d0(final View view, final n1 n1) {
        view.onInitializeAccessibilityNodeInfo(n1.A0());
    }
    
    public static i42 e(final View view) {
        if (o32.b == null) {
            o32.b = new WeakHashMap();
        }
        i42 value;
        if ((value = o32.b.get(view)) == null) {
            value = new i42(view);
            o32.b.put(view, value);
        }
        return value;
    }
    
    public static f e0() {
        return (f)new f(fb1.K, CharSequence.class, 8, 28) {
            public CharSequence i(final View view) {
                return q.b(view);
            }
            
            public void j(final View view, final CharSequence charSequence) {
                q.h(view, charSequence);
            }
            
            public boolean k(final CharSequence charSequence, final CharSequence charSequence2) {
                return TextUtils.equals(charSequence, charSequence2) ^ true;
            }
        };
    }
    
    public static u62 f(final View view, final u62 u62, final Rect rect) {
        return m.b(view, u62, rect);
    }
    
    public static boolean f0(final View view, final int n, final Bundle bundle) {
        return h.j(view, n, bundle);
    }
    
    public static u62 g(final View view, final u62 u62) {
        final WindowInsets u63 = u62.u();
        if (u63 != null) {
            final WindowInsets a = l.a(view, u63);
            if (!a.equals((Object)u63)) {
                return u62.w(a, view);
            }
        }
        return u62;
    }
    
    public static gl g0(final View view, gl a) {
        if (Log.isLoggable("ViewCompat", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("performReceiveContent: ");
            sb.append(a);
            sb.append(", view=");
            sb.append(view.getClass().getSimpleName());
            sb.append("[");
            sb.append(view.getId());
            sb.append("]");
            Log.d("ViewCompat", sb.toString());
        }
        if (Build$VERSION.SDK_INT >= 31) {
            return t.b(view, a);
        }
        final p11 p2 = (p11)view.getTag(fb1.M);
        if (p2 != null) {
            a = p2.a(view, a);
            gl onReceiveContent;
            if (a == null) {
                onReceiveContent = null;
            }
            else {
                onReceiveContent = x(view).onReceiveContent(a);
            }
            return onReceiveContent;
        }
        return x(view).onReceiveContent(a);
    }
    
    public static boolean h(final View view, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT < 28 && w.a(view).b(view, keyEvent);
    }
    
    public static void h0(final View view) {
        h.k(view);
    }
    
    public static boolean i(final View view, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT < 28 && w.a(view).f(keyEvent);
    }
    
    public static void i0(final View view, final Runnable runnable) {
        h.m(view, runnable);
    }
    
    public static void j(final View view) {
        p0 l;
        if ((l = l(view)) == null) {
            l = new p0();
        }
        q0(view, l);
    }
    
    public static void j0(final View view, final Runnable runnable, final long n) {
        h.n(view, runnable, n);
    }
    
    public static int k() {
        return i.a();
    }
    
    public static void k0(final View view, final int n) {
        l0(n, view);
        Z(view, 0);
    }
    
    public static p0 l(final View view) {
        final View$AccessibilityDelegate m = m(view);
        if (m == null) {
            return null;
        }
        if (m instanceof p0.a) {
            return ((p0.a)m).a;
        }
        return new p0(m);
    }
    
    public static void l0(final int n, final View view) {
        final List q = q(view);
        for (int i = 0; i < q.size(); ++i) {
            if (((n1.a)q.get(i)).b() == n) {
                q.remove(i);
                break;
            }
        }
    }
    
    public static View$AccessibilityDelegate m(final View view) {
        if (Build$VERSION.SDK_INT >= 29) {
            return r.a(view);
        }
        return n(view);
    }
    
    public static void m0(final View view, final n1.a a, final CharSequence charSequence, final q1 q1) {
        if (q1 == null && charSequence == null) {
            k0(view, a.b());
        }
        else {
            d(view, a.a(charSequence, q1));
        }
    }
    
    public static View$AccessibilityDelegate n(final View obj) {
        if (o32.d) {
            return null;
        }
        if (o32.c == null) {
            try {
                (o32.c = View.class.getDeclaredField("mAccessibilityDelegate")).setAccessible(true);
            }
            finally {
                o32.d = true;
                return null;
            }
        }
        try {
            final Object value = o32.c.get(obj);
            if (value instanceof View$AccessibilityDelegate) {
                return (View$AccessibilityDelegate)value;
            }
            return null;
        }
        finally {
            o32.d = true;
            return null;
        }
    }
    
    public static void n0(final View view) {
        l.c(view);
    }
    
    public static int o(final View view) {
        return k.a(view);
    }
    
    public static void o0(final View view, final Context context, final int[] array, final AttributeSet set, final TypedArray typedArray, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 29) {
            r.c(view, context, array, set, typedArray, n, n2);
        }
    }
    
    public static CharSequence p(final View view) {
        return (CharSequence)e0().f(view);
    }
    
    public static f p0() {
        return (f)new f(fb1.O, Boolean.class, 28) {
            public Boolean i(final View view) {
                return q.d(view);
            }
            
            public void j(final View view, final Boolean b) {
                q.i(view, b);
            }
            
            public boolean k(final Boolean b, final Boolean b2) {
                return ((f)this).a(b, b2) ^ true;
            }
        };
    }
    
    public static List q(final View view) {
        final int h = fb1.H;
        ArrayList list;
        if ((list = (ArrayList)view.getTag(h)) == null) {
            list = new ArrayList();
            view.setTag(h, (Object)list);
        }
        return list;
    }
    
    public static void q0(final View view, final p0 p2) {
        p0 p3 = p2;
        if (p2 == null) {
            p3 = p2;
            if (m(view) instanceof p0.a) {
                p3 = new p0();
            }
        }
        View$AccessibilityDelegate bridge;
        if (p3 == null) {
            bridge = null;
        }
        else {
            bridge = p3.getBridge();
        }
        view.setAccessibilityDelegate(bridge);
    }
    
    public static int r(final View view, final CharSequence charSequence) {
        final List q = q(view);
        for (int i = 0; i < q.size(); ++i) {
            if (TextUtils.equals(charSequence, ((n1.a)q.get(i)).c())) {
                return ((n1.a)q.get(i)).b();
            }
        }
        int n = -1;
        int n2 = 0;
        while (true) {
            final int[] e = o32.e;
            if (n2 >= e.length || n != -1) {
                break;
            }
            final int n3 = e[n2];
            int j = 0;
            boolean b = true;
            while (j < q.size()) {
                b &= (((n1.a)q.get(j)).b() != n3);
                ++j;
            }
            if (b) {
                n = n3;
            }
            ++n2;
        }
        return n;
    }
    
    public static void r0(final View view, final boolean b) {
        b().g(view, b);
    }
    
    public static ColorStateList s(final View view) {
        return m.g(view);
    }
    
    public static void s0(final View view, final int n) {
        k.f(view, n);
    }
    
    public static PorterDuff$Mode t(final View view) {
        return m.h(view);
    }
    
    public static void t0(final View view, final CharSequence charSequence) {
        e0().g(view, charSequence);
        if (charSequence != null) {
            o32.g.a(view);
        }
        else {
            o32.g.d(view);
        }
    }
    
    public static Rect u(final View view) {
        return j.a(view);
    }
    
    public static void u0(final View view, final Drawable drawable) {
        h.q(view, drawable);
    }
    
    public static Display v(final View view) {
        return i.b(view);
    }
    
    public static void v0(final View view, final ColorStateList list) {
        m.q(view, list);
    }
    
    public static float w(final View view) {
        return m.i(view);
    }
    
    public static void w0(final View view, final PorterDuff$Mode porterDuff$Mode) {
        m.r(view, porterDuff$Mode);
    }
    
    public static q11 x(final View view) {
        if (view instanceof q11) {
            return (q11)view;
        }
        return o32.f;
    }
    
    public static void x0(final View view, final Rect rect) {
        j.c(view, rect);
    }
    
    public static boolean y(final View view) {
        return h.b(view);
    }
    
    public static void y0(final View view, final float n) {
        m.s(view, n);
    }
    
    public static int z(final View view) {
        return h.c(view);
    }
    
    public static void z0(final View view, final boolean fitsSystemWindows) {
        view.setFitsSystemWindows(fitsSystemWindows);
    }
    
    public static class e implements ViewTreeObserver$OnGlobalLayoutListener, View$OnAttachStateChangeListener
    {
        public final WeakHashMap a;
        
        public e() {
            this.a = new WeakHashMap();
        }
        
        public void a(final View key) {
            this.a.put(key, key.isShown() && key.getWindowVisibility() == 0);
            key.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            if (k.b(key)) {
                this.c(key);
            }
        }
        
        public final void b(final View key, final boolean b) {
            final boolean b2 = key.isShown() && key.getWindowVisibility() == 0;
            if (b != b2) {
                int n;
                if (b2) {
                    n = 16;
                }
                else {
                    n = 32;
                }
                o32.Z(key, n);
                this.a.put(key, b2);
            }
        }
        
        public final void c(final View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        
        public void d(final View key) {
            this.a.remove(key);
            key.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            this.e(key);
        }
        
        public final void e(final View view) {
            h.o(view.getViewTreeObserver(), (ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        
        public void onGlobalLayout() {
            if (Build$VERSION.SDK_INT < 28) {
                for (final Map.Entry<View, V> entry : this.a.entrySet()) {
                    this.b(entry.getKey(), (boolean)entry.getValue());
                }
            }
        }
        
        public void onViewAttachedToWindow(final View view) {
            this.c(view);
        }
        
        public void onViewDetachedFromWindow(final View view) {
        }
    }
    
    public abstract static class f
    {
        public final int a;
        public final Class b;
        public final int c;
        public final int d;
        
        public f(final int n, final Class clazz, final int n2) {
            this(n, clazz, 0, n2);
        }
        
        public f(final int a, final Class b, final int d, final int c) {
            this.a = a;
            this.b = b;
            this.d = d;
            this.c = c;
        }
        
        public boolean a(final Boolean b, final Boolean b2) {
            boolean b3 = true;
            if ((b != null && b) != (b2 != null && b2)) {
                b3 = false;
            }
            return b3;
        }
        
        public final boolean b() {
            return true;
        }
        
        public final boolean c() {
            return Build$VERSION.SDK_INT >= this.c;
        }
        
        public abstract Object d(final View p0);
        
        public abstract void e(final View p0, final Object p1);
        
        public Object f(final View view) {
            if (this.c()) {
                return this.d(view);
            }
            if (this.b()) {
                final Object tag = view.getTag(this.a);
                if (this.b.isInstance(tag)) {
                    return tag;
                }
            }
            return null;
        }
        
        public void g(final View view, final Object o) {
            if (this.c()) {
                this.e(view, o);
            }
            else if (this.b() && this.h(this.f(view), o)) {
                o32.j(view);
                view.setTag(this.a, o);
                o32.Z(view, this.d);
            }
        }
        
        public abstract boolean h(final Object p0, final Object p1);
    }
    
    public abstract static class g
    {
        public static boolean a(final View view) {
            return view.hasOnClickListeners();
        }
    }
    
    public abstract static class h
    {
        public static AccessibilityNodeProvider a(final View view) {
            return view.getAccessibilityNodeProvider();
        }
        
        public static boolean b(final View view) {
            return view.getFitsSystemWindows();
        }
        
        public static int c(final View view) {
            return view.getImportantForAccessibility();
        }
        
        public static int d(final View view) {
            return view.getMinimumHeight();
        }
        
        public static int e(final View view) {
            return view.getMinimumWidth();
        }
        
        public static ViewParent f(final View view) {
            return view.getParentForAccessibility();
        }
        
        public static int g(final View view) {
            return view.getWindowSystemUiVisibility();
        }
        
        public static boolean h(final View view) {
            return view.hasOverlappingRendering();
        }
        
        public static boolean i(final View view) {
            return view.hasTransientState();
        }
        
        public static boolean j(final View view, final int n, final Bundle bundle) {
            return view.performAccessibilityAction(n, bundle);
        }
        
        public static void k(final View view) {
            view.postInvalidateOnAnimation();
        }
        
        public static void l(final View view, final int n, final int n2, final int n3, final int n4) {
            view.postInvalidateOnAnimation(n, n2, n3, n4);
        }
        
        public static void m(final View view, final Runnable runnable) {
            view.postOnAnimation(runnable);
        }
        
        public static void n(final View view, final Runnable runnable, final long n) {
            view.postOnAnimationDelayed(runnable, n);
        }
        
        public static void o(final ViewTreeObserver viewTreeObserver, final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
        
        public static void p(final View view) {
            view.requestFitSystemWindows();
        }
        
        public static void q(final View view, final Drawable background) {
            view.setBackground(background);
        }
        
        public static void r(final View view, final boolean hasTransientState) {
            view.setHasTransientState(hasTransientState);
        }
        
        public static void s(final View view, final int importantForAccessibility) {
            view.setImportantForAccessibility(importantForAccessibility);
        }
    }
    
    public abstract static class i
    {
        public static int a() {
            return View.generateViewId();
        }
        
        public static Display b(final View view) {
            return view.getDisplay();
        }
        
        public static int c(final View view) {
            return view.getLabelFor();
        }
        
        public static int d(final View view) {
            return view.getLayoutDirection();
        }
        
        public static int e(final View view) {
            return view.getPaddingEnd();
        }
        
        public static int f(final View view) {
            return view.getPaddingStart();
        }
        
        public static boolean g(final View view) {
            return view.isPaddingRelative();
        }
        
        public static void h(final View view, final int labelFor) {
            view.setLabelFor(labelFor);
        }
        
        public static void i(final View view, final Paint layerPaint) {
            view.setLayerPaint(layerPaint);
        }
        
        public static void j(final View view, final int layoutDirection) {
            view.setLayoutDirection(layoutDirection);
        }
        
        public static void k(final View view, final int n, final int n2, final int n3, final int n4) {
            view.setPaddingRelative(n, n2, n3, n4);
        }
    }
    
    public abstract static class j
    {
        public static Rect a(final View view) {
            return view.getClipBounds();
        }
        
        public static boolean b(final View view) {
            return view.isInLayout();
        }
        
        public static void c(final View view, final Rect clipBounds) {
            view.setClipBounds(clipBounds);
        }
    }
    
    public abstract static class k
    {
        public static int a(final View view) {
            return view.getAccessibilityLiveRegion();
        }
        
        public static boolean b(final View view) {
            return view.isAttachedToWindow();
        }
        
        public static boolean c(final View view) {
            return view.isLaidOut();
        }
        
        public static boolean d(final View view) {
            return view.isLayoutDirectionResolved();
        }
        
        public static void e(final ViewParent viewParent, final View view, final View view2, final int n) {
            viewParent.notifySubtreeAccessibilityStateChanged(view, view2, n);
        }
        
        public static void f(final View view, final int accessibilityLiveRegion) {
            view.setAccessibilityLiveRegion(accessibilityLiveRegion);
        }
        
        public static void g(final AccessibilityEvent accessibilityEvent, final int contentChangeTypes) {
            accessibilityEvent.setContentChangeTypes(contentChangeTypes);
        }
    }
    
    public abstract static class l
    {
        public static WindowInsets a(final View view, final WindowInsets windowInsets) {
            return view.dispatchApplyWindowInsets(windowInsets);
        }
        
        public static WindowInsets b(final View view, final WindowInsets windowInsets) {
            return view.onApplyWindowInsets(windowInsets);
        }
        
        public static void c(final View view) {
            view.requestApplyInsets();
        }
    }
    
    public abstract static class m
    {
        public static void a(final WindowInsets windowInsets, final View view) {
            final View$OnApplyWindowInsetsListener view$OnApplyWindowInsetsListener = (View$OnApplyWindowInsetsListener)view.getTag(fb1.S);
            if (view$OnApplyWindowInsetsListener != null) {
                view$OnApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
            }
        }
        
        public static u62 b(final View view, final u62 u62, final Rect rect) {
            final WindowInsets u63 = u62.u();
            if (u63 != null) {
                return u62.w(view.computeSystemWindowInsets(u63, rect), view);
            }
            rect.setEmpty();
            return u62;
        }
        
        public static boolean c(final View view, final float n, final float n2, final boolean b) {
            return view.dispatchNestedFling(n, n2, b);
        }
        
        public static boolean d(final View view, final float n, final float n2) {
            return view.dispatchNestedPreFling(n, n2);
        }
        
        public static boolean e(final View view, final int n, final int n2, final int[] array, final int[] array2) {
            return view.dispatchNestedPreScroll(n, n2, array, array2);
        }
        
        public static boolean f(final View view, final int n, final int n2, final int n3, final int n4, final int[] array) {
            return view.dispatchNestedScroll(n, n2, n3, n4, array);
        }
        
        public static ColorStateList g(final View view) {
            return view.getBackgroundTintList();
        }
        
        public static PorterDuff$Mode h(final View view) {
            return view.getBackgroundTintMode();
        }
        
        public static float i(final View view) {
            return view.getElevation();
        }
        
        public static u62 j(final View view) {
            return u62.a.a(view);
        }
        
        public static String k(final View view) {
            return view.getTransitionName();
        }
        
        public static float l(final View view) {
            return view.getTranslationZ();
        }
        
        public static float m(final View view) {
            return view.getZ();
        }
        
        public static boolean n(final View view) {
            return view.hasNestedScrollingParent();
        }
        
        public static boolean o(final View view) {
            return view.isImportantForAccessibility();
        }
        
        public static boolean p(final View view) {
            return view.isNestedScrollingEnabled();
        }
        
        public static void q(final View view, final ColorStateList backgroundTintList) {
            view.setBackgroundTintList(backgroundTintList);
        }
        
        public static void r(final View view, final PorterDuff$Mode backgroundTintMode) {
            view.setBackgroundTintMode(backgroundTintMode);
        }
        
        public static void s(final View view, final float elevation) {
            view.setElevation(elevation);
        }
        
        public static void t(final View view, final boolean nestedScrollingEnabled) {
            view.setNestedScrollingEnabled(nestedScrollingEnabled);
        }
        
        public static void u(final View view, final g11 g11) {
            if (Build$VERSION.SDK_INT < 30) {
                view.setTag(fb1.L, (Object)g11);
            }
            if (g11 == null) {
                view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)view.getTag(fb1.S));
                return;
            }
            view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)new View$OnApplyWindowInsetsListener(view, g11) {
                public u62 a = null;
                public final View b;
                public final g11 c;
                
                public WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
                    final u62 w = u62.w(windowInsets, view);
                    final int sdk_INT = Build$VERSION.SDK_INT;
                    if (sdk_INT < 30) {
                        m.a(windowInsets, this.b);
                        if (w.equals(this.a)) {
                            return this.c.onApplyWindowInsets(view, w).u();
                        }
                    }
                    this.a = w;
                    final u62 onApplyWindowInsets = this.c.onApplyWindowInsets(view, w);
                    if (sdk_INT >= 30) {
                        return onApplyWindowInsets.u();
                    }
                    o32.n0(view);
                    return onApplyWindowInsets.u();
                }
            });
        }
        
        public static void v(final View view, final String transitionName) {
            view.setTransitionName(transitionName);
        }
        
        public static void w(final View view, final float translationZ) {
            view.setTranslationZ(translationZ);
        }
        
        public static void x(final View view, final float z) {
            view.setZ(z);
        }
        
        public static boolean y(final View view, final int n) {
            return view.startNestedScroll(n);
        }
        
        public static void z(final View view) {
            view.stopNestedScroll();
        }
    }
    
    public abstract static class n
    {
        public static u62 a(final View view) {
            final WindowInsets rootWindowInsets = view.getRootWindowInsets();
            if (rootWindowInsets == null) {
                return null;
            }
            final u62 v = u62.v(rootWindowInsets);
            v.s(v);
            v.d(view.getRootView());
            return v;
        }
        
        public static int b(final View view) {
            return view.getScrollIndicators();
        }
        
        public static void c(final View view, final int scrollIndicators) {
            view.setScrollIndicators(scrollIndicators);
        }
        
        public static void d(final View view, final int n, final int n2) {
            view.setScrollIndicators(n, n2);
        }
    }
    
    public abstract static class o
    {
        public static void a(final View view) {
            view.cancelDragAndDrop();
        }
        
        public static void b(final View view) {
            view.dispatchFinishTemporaryDetach();
        }
        
        public static void c(final View view) {
            view.dispatchStartTemporaryDetach();
        }
        
        public static void d(final View view, final PointerIcon pointerIcon) {
            view.setPointerIcon(pointerIcon);
        }
        
        public static boolean e(final View view, final ClipData clipData, final View$DragShadowBuilder view$DragShadowBuilder, final Object o, final int n) {
            return view.startDragAndDrop(clipData, view$DragShadowBuilder, o, n);
        }
        
        public static void f(final View view, final View$DragShadowBuilder view$DragShadowBuilder) {
            view.updateDragShadow(view$DragShadowBuilder);
        }
    }
    
    public abstract static class p
    {
        public static void a(final View view, final Collection<View> collection, final int n) {
            view.addKeyboardNavigationClusters((Collection)collection, n);
        }
        
        public static int b(final View view) {
            return view.getImportantForAutofill();
        }
        
        public static int c(final View view) {
            return view.getNextClusterForwardId();
        }
        
        public static boolean d(final View view) {
            return view.hasExplicitFocusable();
        }
        
        public static boolean e(final View view) {
            return view.isFocusedByDefault();
        }
        
        public static boolean f(final View view) {
            return view.isImportantForAutofill();
        }
        
        public static boolean g(final View view) {
            return view.isKeyboardNavigationCluster();
        }
        
        public static View h(final View view, final View view2, final int n) {
            return view.keyboardNavigationClusterSearch(view2, n);
        }
        
        public static boolean i(final View view) {
            return view.restoreDefaultFocus();
        }
        
        public static void j(final View view, final String... autofillHints) {
            view.setAutofillHints(autofillHints);
        }
        
        public static void k(final View view, final boolean focusedByDefault) {
            view.setFocusedByDefault(focusedByDefault);
        }
        
        public static void l(final View view, final int importantForAutofill) {
            view.setImportantForAutofill(importantForAutofill);
        }
        
        public static void m(final View view, final boolean keyboardNavigationCluster) {
            view.setKeyboardNavigationCluster(keyboardNavigationCluster);
        }
        
        public static void n(final View view, final int nextClusterForwardId) {
            view.setNextClusterForwardId(nextClusterForwardId);
        }
        
        public static void o(final View view, final CharSequence tooltipText) {
            view.setTooltipText(tooltipText);
        }
    }
    
    public abstract static class q
    {
        public static void a(final View view, final v obj) {
            final int r = fb1.R;
            co1 co1;
            if ((co1 = (co1)view.getTag(r)) == null) {
                co1 = new co1();
                view.setTag(r, (Object)co1);
            }
            Objects.requireNonNull(obj);
            final p32 p2 = new p32(obj);
            co1.put(obj, p2);
            view.addOnUnhandledKeyEventListener((View$OnUnhandledKeyEventListener)p2);
        }
        
        public static CharSequence b(final View view) {
            return view.getAccessibilityPaneTitle();
        }
        
        public static boolean c(final View view) {
            return view.isAccessibilityHeading();
        }
        
        public static boolean d(final View view) {
            return view.isScreenReaderFocusable();
        }
        
        public static void e(final View view, final v v) {
            final co1 co1 = (co1)view.getTag(fb1.R);
            if (co1 == null) {
                return;
            }
            final View$OnUnhandledKeyEventListener view$OnUnhandledKeyEventListener = (View$OnUnhandledKeyEventListener)co1.get(v);
            if (view$OnUnhandledKeyEventListener != null) {
                view.removeOnUnhandledKeyEventListener(view$OnUnhandledKeyEventListener);
            }
        }
        
        public static <T> T f(final View view, final int n) {
            return (T)view.requireViewById(n);
        }
        
        public static void g(final View view, final boolean accessibilityHeading) {
            view.setAccessibilityHeading(accessibilityHeading);
        }
        
        public static void h(final View view, final CharSequence accessibilityPaneTitle) {
            view.setAccessibilityPaneTitle(accessibilityPaneTitle);
        }
        
        public static void i(final View view, final boolean screenReaderFocusable) {
            view.setScreenReaderFocusable(screenReaderFocusable);
        }
    }
    
    public abstract static class r
    {
        public static View$AccessibilityDelegate a(final View view) {
            return view.getAccessibilityDelegate();
        }
        
        public static List<Rect> b(final View view) {
            return view.getSystemGestureExclusionRects();
        }
        
        public static void c(final View view, final Context context, final int[] array, final AttributeSet set, final TypedArray typedArray, final int n, final int n2) {
            view.saveAttributeDataForStyleable(context, array, set, typedArray, n, n2);
        }
        
        public static void d(final View view, final List<Rect> systemGestureExclusionRects) {
            view.setSystemGestureExclusionRects((List)systemGestureExclusionRects);
        }
    }
    
    public abstract static class s
    {
        public static CharSequence a(final View view) {
            return view.getStateDescription();
        }
        
        public static v72 b(final View view) {
            final WindowInsetsController windowInsetsController = view.getWindowInsetsController();
            v72 f;
            if (windowInsetsController != null) {
                f = v72.f(windowInsetsController);
            }
            else {
                f = null;
            }
            return f;
        }
        
        public static void c(final View view, final CharSequence stateDescription) {
            view.setStateDescription(stateDescription);
        }
    }
    
    public abstract static final class t
    {
        public static String[] a(final View view) {
            return view.getReceiveContentMimeTypes();
        }
        
        public static gl b(final View view, final gl gl) {
            final ContentInfo f = gl.f();
            final ContentInfo performReceiveContent = view.performReceiveContent(f);
            if (performReceiveContent == null) {
                return null;
            }
            if (performReceiveContent == f) {
                return gl;
            }
            return gl.g(performReceiveContent);
        }
        
        public static void c(final View view, final String[] array, final p11 p3) {
            if (p3 == null) {
                view.setOnReceiveContentListener(array, (OnReceiveContentListener)null);
            }
            else {
                view.setOnReceiveContentListener(array, (OnReceiveContentListener)new u(p3));
            }
        }
    }
    
    public static final class u implements OnReceiveContentListener
    {
        public final p11 a;
        
        public u(final p11 a) {
            this.a = a;
        }
        
        public ContentInfo onReceiveContent(final View view, final ContentInfo contentInfo) {
            final gl g = gl.g(contentInfo);
            final gl a = this.a.a(view, g);
            if (a == null) {
                return null;
            }
            if (a == g) {
                return contentInfo;
            }
            return a.f();
        }
    }
    
    public interface v
    {
    }
    
    public static class w
    {
        public static final ArrayList d;
        public WeakHashMap a;
        public SparseArray b;
        public WeakReference c;
        
        static {
            d = new ArrayList();
        }
        
        public w() {
            this.a = null;
            this.b = null;
            this.c = null;
        }
        
        public static w a(final View view) {
            final int q = fb1.Q;
            w w;
            if ((w = (w)view.getTag(q)) == null) {
                w = new w();
                view.setTag(q, (Object)w);
            }
            return w;
        }
        
        public boolean b(View c, final KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                this.g();
            }
            c = this.c(c, keyEvent);
            if (keyEvent.getAction() == 0) {
                final int keyCode = keyEvent.getKeyCode();
                if (c != null && !KeyEvent.isModifierKey(keyCode)) {
                    this.d().put(keyCode, (Object)new WeakReference(c));
                }
            }
            return c != null;
        }
        
        public final View c(final View key, final KeyEvent keyEvent) {
            final WeakHashMap a = this.a;
            if (a != null) {
                if (a.containsKey(key)) {
                    if (key instanceof ViewGroup) {
                        final ViewGroup viewGroup = (ViewGroup)key;
                        for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                            final View c = this.c(viewGroup.getChildAt(i), keyEvent);
                            if (c != null) {
                                return c;
                            }
                        }
                    }
                    if (this.e(key, keyEvent)) {
                        return key;
                    }
                }
            }
            return null;
        }
        
        public final SparseArray d() {
            if (this.b == null) {
                this.b = new SparseArray();
            }
            return this.b;
        }
        
        public final boolean e(final View view, final KeyEvent keyEvent) {
            final ArrayList list = (ArrayList)view.getTag(fb1.R);
            if (list != null) {
                final int index = list.size() - 1;
                if (index >= 0) {
                    zu0.a(list.get(index));
                    throw null;
                }
            }
            return false;
        }
        
        public boolean f(final KeyEvent referent) {
            final WeakReference c = this.c;
            if (c != null && c.get() == referent) {
                return false;
            }
            this.c = new WeakReference((T)referent);
            final SparseArray d = this.d();
            WeakReference weakReference = null;
            Label_0080: {
                if (referent.getAction() == 1) {
                    final int indexOfKey = d.indexOfKey(referent.getKeyCode());
                    if (indexOfKey >= 0) {
                        weakReference = (WeakReference)d.valueAt(indexOfKey);
                        d.removeAt(indexOfKey);
                        break Label_0080;
                    }
                }
                weakReference = null;
            }
            WeakReference weakReference2 = weakReference;
            if (weakReference == null) {
                weakReference2 = (WeakReference)d.get(referent.getKeyCode());
            }
            if (weakReference2 != null) {
                final View view = (View)weakReference2.get();
                if (view != null && o32.T(view)) {
                    this.e(view, referent);
                }
                return true;
            }
            return false;
        }
        
        public final void g() {
            final WeakHashMap a = this.a;
            if (a != null) {
                a.clear();
            }
            final ArrayList d = w.d;
            if (d.isEmpty()) {
                return;
            }
            synchronized (d) {
                if (this.a == null) {
                    this.a = new WeakHashMap();
                }
                for (int i = d.size() - 1; i >= 0; --i) {
                    final ArrayList d2 = w.d;
                    final View key = d2.get(i).get();
                    if (key == null) {
                        d2.remove(i);
                    }
                    else {
                        this.a.put(key, Boolean.TRUE);
                        for (ViewParent viewParent = key.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
                            this.a.put(viewParent, Boolean.TRUE);
                        }
                    }
                }
            }
        }
    }
}

import com.ekodroid.omrevaluator.templateui.scanner.ScannerUtil;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.NumericalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.DecimalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import com.ekodroid.omrevaluator.templateui.models.Point2Integer;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;

// 
// Decompiled by Procyon v0.6.0
// 

public class sn1
{
    public SheetDimension a;
    
    public sn1(final SheetDimension a) {
        this.a = a;
    }
    
    public Point2Double[] a(int i, final Point2Integer point2Integer, final int n, final int n2, final int n3) {
        final int[] array = new int[n];
        final int n4 = 0;
        for (int j = 0; j < n; ++j) {
            final int n5 = (int)Math.pow(10.0, n - 1 - j);
            array[j] = i / n5;
            i %= n5;
        }
        final Point2Double[][] h = this.h(point2Integer, n, n2);
        final Point2Double[] array2 = new Point2Double[n];
        for (i = n4; i < n; ++i) {
            array2[i] = h[(array[i] - n3 + 10) % 10][i];
        }
        return array2;
    }
    
    public Point2Double[] b(final AnswerOption answerOption, final int[] array) {
        final int n = sn1$a.a[answerOption.type.ordinal()];
        final int n2 = 5;
        final int n3 = 10;
        final int n4 = 4;
        final int n5 = 0;
        int i = 0;
        switch (n) {
            default: {
                return null;
            }
            case 11: {
                return this.c(new Point2Integer(answerOption.column, answerOption.row), 10, array[answerOption.column - 1]);
            }
            case 10: {
                return this.c(new Point2Integer(answerOption.column, answerOption.row), 8, array[answerOption.column - 1]);
            }
            case 9: {
                return this.c(new Point2Integer(answerOption.column, answerOption.row), 6, array[answerOption.column - 1]);
            }
            case 8: {
                final DecimalOptionPayload b = bc.b(answerOption.payload);
                int n6 = n3;
                if (b.decimalAllowed) {
                    n6 = 11;
                }
                int n8;
                final int n7 = n8 = b.digits * n6;
                if (b.hasNegetive) {
                    n8 = n7 + 1;
                }
                final Point2Double[] array2 = new Point2Double[n8];
                for (int j = 0; j < n6; ++j) {
                    final Point2Double[] c = this.c(new Point2Integer(answerOption.column, answerOption.row + 1 + j), b.digits + answerOption.subIndex * 4, array[answerOption.column - 1]);
                    int n9 = 0;
                    while (true) {
                        final int digits = b.digits;
                        if (n9 >= digits) {
                            break;
                        }
                        array2[digits * j + n9] = c[answerOption.subIndex * 4 + n9];
                        ++n9;
                    }
                }
                if (b.hasNegetive) {
                    array2[n8 - 1] = this.c(new Point2Integer(answerOption.column, answerOption.row), answerOption.subIndex * 4 + 2, array[answerOption.column - 1])[answerOption.subIndex * 4 + 1];
                }
                return array2;
            }
            case 7: {
                final NumericalOptionPayload d = bc.d(answerOption.payload);
                if (d != null) {
                    final int digits2 = d.digits;
                    if (digits2 > 1 || d.hasNegetive) {
                        int n11;
                        final int n10 = n11 = digits2 * 10;
                        if (d.hasNegetive) {
                            n11 = n10 + 1;
                        }
                        final Point2Double[] array3 = new Point2Double[n11];
                        for (int k = 0; k < 10; ++k) {
                            final Point2Double[] c2 = this.c(new Point2Integer(answerOption.column, answerOption.row + 1 + k), d.digits, array[answerOption.column - 1]);
                            for (int l = 0; l < c2.length; ++l) {
                                array3[d.digits * k + l] = c2[l];
                            }
                        }
                        Point2Double[] array4 = array3;
                        if (d.hasNegetive) {
                            array3[n11 - 1] = this.c(new Point2Integer(answerOption.column, answerOption.row), 2, array[answerOption.column - 1])[1];
                            array4 = array3;
                            return array4;
                        }
                        return array4;
                    }
                }
                final int subIndex = answerOption.subIndex;
                Point2Double[] array4 = new Point2Double[10];
                while (i < 10) {
                    array4[i] = this.c(new Point2Integer(answerOption.column, answerOption.row + 1 + i), 5, array[answerOption.column - 1])[subIndex];
                    ++i;
                }
                return array4;
            }
            case 6: {
                final MatrixOptionPayload c3 = bc.c(answerOption.payload);
                int secondaryOptions = n2;
                int primaryOptions = n4;
                if (c3 != null) {
                    primaryOptions = c3.primaryOptions;
                    secondaryOptions = c3.secondaryOptions;
                }
                final Point2Double[] array5 = new Point2Double[primaryOptions * secondaryOptions];
                for (int n12 = 0; n12 < primaryOptions; ++n12) {
                    final Point2Double[] c4 = this.c(new Point2Integer(answerOption.column, answerOption.row + 1 + n12), secondaryOptions, array[answerOption.column - 1]);
                    for (int n13 = 0; n13 < c4.length; ++n13) {
                        array5[n12 * secondaryOptions + n13] = c4[n13];
                    }
                }
                return array5;
            }
            case 5: {
                final Point2Double[] c5 = this.c(new Point2Integer(answerOption.column, answerOption.row), 4, array[answerOption.column - 1]);
                return new Point2Double[] { c5[1], c5[3] };
            }
            case 4: {
                return this.c(new Point2Integer(answerOption.column, answerOption.row), 3, array[answerOption.column - 1]);
            }
            case 3: {
                return this.c(new Point2Integer(answerOption.column, answerOption.row), 5, array[answerOption.column - 1]);
            }
            case 2: {
                return this.c(new Point2Integer(answerOption.column, answerOption.row), 4, array[answerOption.column - 1]);
            }
            case 1: {
                final int subIndex2 = answerOption.subIndex;
                final Point2Double[] array6 = new Point2Double[subIndex2];
                final Point2Double[][] array7 = new Point2Double[2][];
                final int column = answerOption.column;
                final int n14 = array[column - 1] - 3;
                array7[0] = this.c(new Point2Integer(column, answerOption.row + 1), n14, array[answerOption.column - 1]);
                int n15 = n5;
                if (subIndex2 > n14) {
                    array7[1] = this.c(new Point2Integer(answerOption.column, answerOption.row + 3), n14, array[answerOption.column - 1]);
                    n15 = n5;
                }
                while (n15 < subIndex2) {
                    array6[n15] = array7[n15 / n14][n15 % n14];
                    ++n15;
                }
                return array6;
            }
        }
    }
    
    public Point2Double[] c(final Point2Integer point2Integer, final int n, final int n2) {
        final Point2Double[] array = new Point2Double[n];
        final Point2Double[] e = this.e(point2Integer);
        if (e == null) {
            return null;
        }
        for (int i = 0; i < n; ++i) {
            array[i] = ScannerUtil.g(e[0], e[1], i * 2 + 5, n2 * 2 + 1);
        }
        return array;
    }
    
    public Point2Double d(final AnswerOption answerOption) {
        final Point2Double[] e = this.e(new Point2Integer(answerOption.column, answerOption.row));
        final Point2Double point2Double = e[1];
        final double x = point2Double.x;
        final Point2Double point2Double2 = e[0];
        final double x2 = point2Double2.x;
        final double n = (x - x2) / 4.0;
        return new Point2Double(x2 + n + answerOption.subIndex * n * 2.0, (point2Double2.y + point2Double.y) / 2.0);
    }
    
    public final Point2Double[] e(final Point2Integer point2Integer) {
        final int x = point2Integer.getX();
        final int y = point2Integer.getY();
        final int n = y - 1;
        final Point2Double[][] sheetMarkers = this.a.sheetMarkers;
        final int length = sheetMarkers.length;
        final int n2 = n / 5;
        Point2Double point2Double;
        int n4;
        Point2Double point2Double5;
        Point2Double point2Double6;
        Point2Double point2Double7;
        if (y == (length - 1) * 5 + 1) {
            final Point2Double[] array = sheetMarkers[n2 - 1];
            final int n3 = x - 1;
            point2Double = array[n3];
            final Point2Double point2Double2 = array[x];
            final Point2Double[] array2 = sheetMarkers[n2];
            final Point2Double point2Double3 = array2[n3];
            final Point2Double point2Double4 = array2[x];
            n4 = 5;
            point2Double5 = point2Double2;
            point2Double6 = point2Double3;
            point2Double7 = point2Double4;
        }
        else {
            final Point2Double[] array3 = sheetMarkers[n2];
            final int n5 = x - 1;
            point2Double = array3[n5];
            final Point2Double point2Double8 = array3[x];
            final Point2Double[] array4 = sheetMarkers[n2 + 1];
            point2Double6 = array4[n5];
            point2Double7 = array4[x];
            n4 = n % 5;
            point2Double5 = point2Double8;
        }
        return this.g(point2Double, point2Double5, point2Double6, point2Double7, n4);
    }
    
    public SheetDimension f() {
        return this.a;
    }
    
    public final Point2Double[] g(final Point2Double point2Double, final Point2Double point2Double2, final Point2Double point2Double3, final Point2Double point2Double4, final int n) {
        return new Point2Double[] { ScannerUtil.g(point2Double, point2Double3, n, 6), ScannerUtil.g(point2Double2, point2Double4, n, 6) };
    }
    
    public Point2Double[][] h(final Point2Integer point2Integer, final int n, final int n2) {
        final Point2Double[][] array = new Point2Double[10][n];
        for (int i = 0; i < array.length; ++i) {
            array[i] = this.c(point2Integer.addy(i + 2), n, n2);
        }
        return array;
    }
}

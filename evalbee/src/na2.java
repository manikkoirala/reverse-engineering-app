import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.ArrayList;
import java.util.Map;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import com.google.android.gms.internal.firebase-auth-api.zzafn;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class na2 extends r30
{
    public static final Parcelable$Creator<na2> CREATOR;
    public zzafn a;
    public tj2 b;
    public String c;
    public String d;
    public List e;
    public List f;
    public String g;
    public Boolean h;
    public ra2 i;
    public boolean j;
    public lf2 k;
    public cd2 l;
    public List m;
    
    static {
        CREATOR = (Parcelable$Creator)new ta2();
    }
    
    public na2(final zzafn a, final tj2 b, final String c, final String d, final List e, final List f, final String g, final Boolean h, final ra2 i, final boolean j, final lf2 k, final cd2 l, final List m) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
    }
    
    public na2(final r10 r10, final List list) {
        Preconditions.checkNotNull(r10);
        this.c = r10.o();
        this.d = "com.google.firebase.auth.internal.DefaultFirebaseUser";
        this.g = "2";
        this.m0(list);
    }
    
    public final boolean A0() {
        return this.j;
    }
    
    @Override
    public s30 E() {
        return this.i;
    }
    
    @Override
    public List J() {
        return this.e;
    }
    
    @Override
    public String K() {
        final zzafn a = this.a;
        String s2;
        final String s = s2 = null;
        if (a != null) {
            s2 = s;
            if (((com.google.android.gms.internal.firebase_auth_api.zzafn)a).zzc() != null) {
                final Map map = sc2.a(((com.google.android.gms.internal.firebase_auth_api.zzafn)this.a).zzc()).a().get("firebase");
                s2 = s;
                if (map != null) {
                    s2 = (String)map.get("tenant");
                }
            }
        }
        return s2;
    }
    
    @Override
    public String O() {
        return this.b.i();
    }
    
    @Override
    public boolean R() {
        final Boolean h = this.h;
        if (h == null || h) {
            final zzafn a = this.a;
            String b;
            final String s = b = "";
            if (a != null) {
                final ya0 a2 = sc2.a(((com.google.android.gms.internal.firebase_auth_api.zzafn)a).zzc());
                b = s;
                if (a2 != null) {
                    b = a2.b();
                }
            }
            final int size = this.J().size();
            final boolean b2 = true;
            boolean b3 = false;
            Label_0105: {
                if (size <= 1) {
                    b3 = b2;
                    if (b == null) {
                        break Label_0105;
                    }
                    if (!b.equals("custom")) {
                        b3 = b2;
                        break Label_0105;
                    }
                }
                b3 = false;
            }
            this.h = b3;
        }
        return this.h;
    }
    
    @Override
    public String b() {
        return this.b.b();
    }
    
    @Override
    public String getDisplayName() {
        return this.b.getDisplayName();
    }
    
    @Override
    public String getEmail() {
        return this.b.getEmail();
    }
    
    @Override
    public final r10 i0() {
        return r10.n(this.c);
    }
    
    @Override
    public final r30 m0(final List list) {
        synchronized (this) {
            Preconditions.checkNotNull(list);
            this.e = new ArrayList(list.size());
            this.f = new ArrayList(list.size());
            for (int i = 0; i < list.size(); ++i) {
                final c22 c22 = list.get(i);
                if (c22.b().equals("firebase")) {
                    this.b = (tj2)c22;
                }
                else {
                    this.f.add(c22.b());
                }
                this.e.add(c22);
            }
            if (this.b == null) {
                this.b = this.e.get(0);
            }
            return this;
        }
    }
    
    @Override
    public final void o0(final zzafn zzafn) {
        this.a = (zzafn)Preconditions.checkNotNull((com.google.android.gms.internal.firebase_auth_api.zzafn)zzafn);
    }
    
    @Override
    public final void q0(final List list) {
        this.l = cd2.E(list);
    }
    
    @Override
    public final zzafn r0() {
        return this.a;
    }
    
    @Override
    public final List s0() {
        return this.f;
    }
    
    public final na2 t0(final String g) {
        this.g = g;
        return this;
    }
    
    public final void u0(final ra2 i) {
        this.i = i;
    }
    
    public final void v0(final lf2 k) {
        this.k = k;
    }
    
    public final void w0(final boolean j) {
        this.j = j;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.r0(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.b, n, false);
        SafeParcelWriter.writeString(parcel, 3, this.c, false);
        SafeParcelWriter.writeString(parcel, 4, this.d, false);
        SafeParcelWriter.writeTypedList(parcel, 5, (List<Parcelable>)this.e, false);
        SafeParcelWriter.writeStringList(parcel, 6, this.s0(), false);
        SafeParcelWriter.writeString(parcel, 7, this.g, false);
        SafeParcelWriter.writeBooleanObject(parcel, 8, this.R(), false);
        SafeParcelWriter.writeParcelable(parcel, 9, (Parcelable)this.E(), n, false);
        SafeParcelWriter.writeBoolean(parcel, 10, this.j);
        SafeParcelWriter.writeParcelable(parcel, 11, (Parcelable)this.k, n, false);
        SafeParcelWriter.writeParcelable(parcel, 12, (Parcelable)this.l, n, false);
        SafeParcelWriter.writeTypedList(parcel, 13, (List<Parcelable>)this.m, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final void x0(final List m) {
        Preconditions.checkNotNull(m);
        this.m = m;
    }
    
    public final lf2 y0() {
        return this.k;
    }
    
    public final List z0() {
        return this.e;
    }
    
    @Override
    public final String zzd() {
        return ((com.google.android.gms.internal.firebase_auth_api.zzafn)this.r0()).zzc();
    }
    
    @Override
    public final String zze() {
        return ((com.google.android.gms.internal.firebase_auth_api.zzafn)this.a).zzf();
    }
    
    public final List zzh() {
        final cd2 l = this.l;
        if (l != null) {
            return l.i();
        }
        return new ArrayList();
    }
}

import androidx.appcompat.app.a;
import android.view.View;
import android.widget.EditText;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.database.LabelProfileJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ui1
{
    public y01 a;
    public Context b;
    public LabelProfile c;
    
    public ui1(final Context b, final y01 a, final LabelProfile c) {
        this.b = b;
        this.a = a;
        this.c = c;
        this.g();
    }
    
    public final boolean d(final String s) {
        return TemplateRepository.getInstance(this.b).getLabelProfileJsonModel(s) != null;
    }
    
    public final boolean e(final String labelProfileName) {
        TemplateRepository.getInstance(this.b).deleteLabelProfileJson(labelProfileName);
        this.c.setLabelProfileName(labelProfileName);
        return TemplateRepository.getInstance(this.b).saveOrUpdateLabelProfileJson(new LabelProfileJsonModel(labelProfileName, this.c));
    }
    
    public final void f(final String str) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.b, 2131951953);
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" ");
        sb.append(this.b.getString(2131886451));
        materialAlertDialogBuilder.setMessage((CharSequence)sb.toString()).setPositiveButton(2131886903, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, str) {
            public final String a;
            public final ui1 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (this.b.e(this.a)) {
                    a91.G(this.b.b, 2131886374, 2131230927, 2131231085);
                    this.b.a.a(this.a);
                }
                dialogInterface.dismiss();
            }
        }).setNegativeButton(2131886657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ui1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void g() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.b, 2131951953);
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131492986, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886763);
        materialAlertDialogBuilder.setPositiveButton(2131886759, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (EditText)inflate.findViewById(2131296595)) {
            public final EditText a;
            public final ui1 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (this.a.getText().toString().trim().equals("")) {
                    a91.G(this.b.b, 2131886274, 2131230909, 2131231086);
                    return;
                }
                if (this.b.d(this.a.getText().toString())) {
                    this.b.f(this.a.getText().toString());
                    return;
                }
                if (this.b.e(this.a.getText().toString())) {
                    a91.G(this.b.b, 2131886374, 2131230927, 2131231085);
                    this.b.a.a(this.a.getText().toString());
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ui1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
}

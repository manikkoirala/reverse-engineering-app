import android.os.BaseBundle;
import android.os.Bundle;
import android.view.View;
import android.text.style.ClickableSpan;

// 
// Decompiled by Procyon v0.6.0
// 

public final class o0 extends ClickableSpan
{
    public final int a;
    public final n1 b;
    public final int c;
    
    public o0(final int a, final n1 b, final int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public void onClick(final View view) {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.a);
        this.b.O(this.c, bundle);
    }
}

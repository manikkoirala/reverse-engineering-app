import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

// 
// Decompiled by Procyon v0.6.0
// 

@Retention(RetentionPolicy.RUNTIME)
public @interface nz {
    boolean deserialize() default true;
    
    boolean serialize() default true;
}

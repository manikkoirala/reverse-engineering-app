import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Executor;
import android.os.Handler;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sy
{
    public static Executor a(final Handler handler) {
        return new a(handler);
    }
    
    public static class a implements Executor
    {
        public final Handler a;
        
        public a(final Handler handler) {
            this.a = (Handler)l71.g(handler);
        }
        
        @Override
        public void execute(final Runnable runnable) {
            if (this.a.post((Runnable)l71.g(runnable))) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(" is shutting down");
            throw new RejectedExecutionException(sb.toString());
        }
    }
}

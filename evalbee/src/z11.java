import android.os.AsyncTask;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class z11
{
    public int a;
    public c b;
    public ArrayList c;
    public ArrayList d;
    public SheetTemplate2 e;
    
    public z11(final int a, final c b, final SheetTemplate2 e, final ArrayList d) {
        this.a = a;
        this.b = b;
        this.e = e;
        this.d = d;
        new b(null).execute((Object[])new Void[0]);
    }
    
    public final b21[] a(final SheetTemplate2 sheetTemplate2, int i) {
        final int n = sheetTemplate2.getAnswerOptions().size() - 1;
        final b21[] array = new b21[n];
        final int n2 = i - 1;
        final int n3 = 0;
        i = n2;
        if (n2 < 0) {
            i = 0;
        }
        final AnswerSetKey answerSetKey = sheetTemplate2.getAnswerKeys()[i];
        final LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        AnswerOptionKey answerOptionKey;
        for (i = n3; i < n; ++i) {
            answerOptionKey = answerSetKey.getAnswerOptionKeys().get(i);
            if (answerOptionKey.getType() != AnswerOption.AnswerOptionType.DECIMAL) {
                if (answerOptionKey.getType() != AnswerOption.AnswerOptionType.MATRIX) {
                    array[i] = new b21(i + 1, answerOptionKey.getMarkedValues(), labelProfile.getOptionLabels(answerOptionKey));
                }
            }
        }
        return array;
    }
    
    public ArrayList b(int i) {
        final ArrayList c = this.c(this.e);
        final b21[] a = this.a(this.e, i);
        final Iterator iterator = c.iterator();
        int n;
        while (true) {
            final boolean hasNext = iterator.hasNext();
            n = 0;
            if (!hasNext) {
                break;
            }
            final ResultItem resultItem = (ResultItem)iterator.next();
            if (resultItem.getExamSet() != i) {
                continue;
            }
            for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
                if (answerValue.getType() != AnswerOption.AnswerOptionType.DECIMAL) {
                    if (answerValue.getType() == AnswerOption.AnswerOptionType.MATRIX) {
                        continue;
                    }
                    final boolean[] markedValues = answerValue.getMarkedValues();
                    for (int j = 0; j < markedValues.length; ++j) {
                        if (markedValues[j]) {
                            final int[] b = a[answerValue.getQuestionNumber() - 1].b();
                            ++b[j];
                        }
                    }
                }
            }
        }
        final ArrayList<b21> list = new ArrayList<b21>();
        int length;
        b21 e;
        for (length = a.length, i = n; i < length; ++i) {
            e = a[i];
            if (e != null) {
                list.add(e);
            }
        }
        return list;
    }
    
    public final ArrayList c(final SheetTemplate2 sheetTemplate2) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.d.iterator();
        while (iterator.hasNext()) {
            list.add(((ResultDataJsonModel)iterator.next()).getResultItem(sheetTemplate2));
        }
        return list;
    }
    
    public class b extends AsyncTask
    {
        public final z11 a;
        
        public b(final z11 a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            final z11 a = this.a;
            a.c = a.b(a.a);
            return null;
        }
        
        public void b(final Void void1) {
            final z11 a = this.a;
            final c b = a.b;
            if (b != null) {
                final ArrayList c = a.c;
                if (c != null) {
                    b.a(c);
                }
            }
        }
    }
    
    public interface c
    {
        void a(final ArrayList p0);
    }
}

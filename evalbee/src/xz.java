import android.graphics.Paint;
import android.animation.AnimatorListenerAdapter;
import android.animation.Animator$AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.view.View;
import android.view.ViewGroup;

// 
// Decompiled by Procyon v0.6.0
// 

public class xz extends j52
{
    public xz() {
    }
    
    public xz(final int mode) {
        this.setMode(mode);
    }
    
    public static float w(final xy1 xy1, final float n) {
        float floatValue = n;
        if (xy1 != null) {
            final Float n2 = xy1.a.get("android:fade:transitionAlpha");
            floatValue = n;
            if (n2 != null) {
                floatValue = n2;
            }
        }
        return floatValue;
    }
    
    @Override
    public void captureStartValues(final xy1 xy1) {
        super.captureStartValues(xy1);
        xy1.a.put("android:fade:transitionAlpha", t42.c(xy1.b));
    }
    
    @Override
    public Animator onAppear(final ViewGroup viewGroup, final View view, final xy1 xy1, final xy1 xy2) {
        final float n = 0.0f;
        float w = w(xy1, 0.0f);
        if (w == 1.0f) {
            w = n;
        }
        return this.v(view, w, 1.0f);
    }
    
    @Override
    public Animator onDisappear(final ViewGroup viewGroup, final View view, final xy1 xy1, final xy1 xy2) {
        t42.e(view);
        return this.v(view, w(xy1, 1.0f), 0.0f);
    }
    
    public final Animator v(final View view, final float n, final float n2) {
        if (n == n2) {
            return null;
        }
        t42.g(view, n);
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat((Object)view, t42.b, new float[] { n2 });
        ((Animator)ofFloat).addListener((Animator$AnimatorListener)new b(view));
        this.addListener((g)new ry1(this, view) {
            public final View a;
            public final xz b;
            
            @Override
            public void onTransitionEnd(final qy1 qy1) {
                t42.g(this.a, 1.0f);
                t42.a(this.a);
                qy1.removeListener((g)this);
            }
        });
        return (Animator)ofFloat;
    }
    
    public static class b extends AnimatorListenerAdapter
    {
        public final View a;
        public boolean b;
        
        public b(final View a) {
            this.b = false;
            this.a = a;
        }
        
        public void onAnimationEnd(final Animator animator) {
            t42.g(this.a, 1.0f);
            if (this.b) {
                this.a.setLayerType(0, (Paint)null);
            }
        }
        
        public void onAnimationStart(final Animator animator) {
            if (o32.Q(this.a) && this.a.getLayerType() == 0) {
                this.b = true;
                this.a.setLayerType(2, (Paint)null);
            }
        }
    }
}

import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import android.os.CancellationSignal;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ns1
{
    public static final ns1 a;
    
    static {
        a = new ns1();
    }
    
    public static final void a(final CancellationSignal cancellationSignal) {
        fg0.e((Object)cancellationSignal, "cancellationSignal");
        cancellationSignal.cancel();
    }
    
    public static final boolean b(final File file) {
        fg0.e((Object)file, "file");
        return SQLiteDatabase.deleteDatabase(file);
    }
    
    public static final boolean c(final SQLiteDatabase sqLiteDatabase) {
        fg0.e((Object)sqLiteDatabase, "sQLiteDatabase");
        return sqLiteDatabase.isWriteAheadLoggingEnabled();
    }
    
    public static final Cursor d(final SQLiteDatabase sqLiteDatabase, final String s, final String[] array, final String s2, final CancellationSignal cancellationSignal, final SQLiteDatabase$CursorFactory sqLiteDatabase$CursorFactory) {
        fg0.e((Object)sqLiteDatabase, "sQLiteDatabase");
        fg0.e((Object)s, "sql");
        fg0.e((Object)array, "selectionArgs");
        fg0.e((Object)cancellationSignal, "cancellationSignal");
        fg0.e((Object)sqLiteDatabase$CursorFactory, "cursorFactory");
        final Cursor rawQueryWithFactory = sqLiteDatabase.rawQueryWithFactory(sqLiteDatabase$CursorFactory, s, array, s2, cancellationSignal);
        fg0.d((Object)rawQueryWithFactory, "sQLiteDatabase.rawQueryW\u2026ationSignal\n            )");
        return rawQueryWithFactory;
    }
    
    public static final void e(final SQLiteDatabase sqLiteDatabase, final boolean foreignKeyConstraintsEnabled) {
        fg0.e((Object)sqLiteDatabase, "sQLiteDatabase");
        sqLiteDatabase.setForeignKeyConstraintsEnabled(foreignKeyConstraintsEnabled);
    }
    
    public static final void f(final SQLiteOpenHelper sqLiteOpenHelper, final boolean writeAheadLoggingEnabled) {
        fg0.e((Object)sqLiteOpenHelper, "sQLiteOpenHelper");
        sqLiteOpenHelper.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
}

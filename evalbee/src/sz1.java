import android.os.Handler;
import android.content.res.Resources;
import android.os.CancellationSignal;
import android.graphics.Typeface;
import android.content.Context;
import android.os.Build$VERSION;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sz1
{
    public static final yz1 a;
    public static final jm0 b;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        yz1 a2;
        if (sdk_INT >= 29) {
            a2 = new xz1();
        }
        else if (sdk_INT >= 28) {
            a2 = new wz1();
        }
        else if (sdk_INT >= 26) {
            a2 = new vz1();
        }
        else if (uz1.j()) {
            a2 = new uz1();
        }
        else {
            a2 = new tz1();
        }
        a = a2;
        b = new jm0(16);
    }
    
    public static Typeface a(final Context context, final Typeface typeface, final int n) {
        if (context != null) {
            return Typeface.create(typeface, n);
        }
        throw new IllegalArgumentException("Context cannot be null");
    }
    
    public static Typeface b(final Context context, final CancellationSignal cancellationSignal, final k70.b[] array, final int n) {
        return sz1.a.b(context, cancellationSignal, array, n);
    }
    
    public static Typeface c(final Context context, final j70.b b, final Resources resources, final int n, final String s, final int n2, final int n3, final le1.e e, Handler handler, final boolean b2) {
        Typeface typeface;
        if (b instanceof j70.e) {
            final j70.e e2 = (j70.e)b;
            final Typeface g = g(e2.c());
            if (g != null) {
                if (e != null) {
                    e.callbackSuccessAsync(g, handler);
                }
                return g;
            }
            final boolean b3 = b2 ? (e2.a() == 0) : (e == null);
            int d;
            if (b2) {
                d = e2.d();
            }
            else {
                d = -1;
            }
            handler = le1.e.getHandler(handler);
            typeface = k70.c(context, e2.b(), n3, b3, d, handler, (k70.c)new a(e));
        }
        else {
            final Typeface typeface2 = typeface = sz1.a.a(context, (j70.c)b, resources, n3);
            if (e != null) {
                if (typeface2 != null) {
                    e.callbackSuccessAsync(typeface2, handler);
                    typeface = typeface2;
                }
                else {
                    e.callbackFailAsync(-3, handler);
                    typeface = typeface2;
                }
            }
        }
        if (typeface != null) {
            sz1.b.put(e(resources, n, s, n2, n3), typeface);
        }
        return typeface;
    }
    
    public static Typeface d(final Context context, final Resources resources, final int n, final String s, final int n2, final int n3) {
        final Typeface d = sz1.a.d(context, resources, n, s, n3);
        if (d != null) {
            sz1.b.put(e(resources, n, s, n2, n3), d);
        }
        return d;
    }
    
    public static String e(final Resources resources, final int i, final String str, final int j, final int k) {
        final StringBuilder sb = new StringBuilder(resources.getResourcePackageName(i));
        sb.append('-');
        sb.append(str);
        sb.append('-');
        sb.append(j);
        sb.append('-');
        sb.append(i);
        sb.append('-');
        sb.append(k);
        return sb.toString();
    }
    
    public static Typeface f(final Resources resources, final int n, final String s, final int n2, final int n3) {
        return (Typeface)sz1.b.get(e(resources, n, s, n2, n3));
    }
    
    public static Typeface g(final String s) {
        Typeface typeface2;
        final Typeface typeface = typeface2 = null;
        if (s != null) {
            if (s.isEmpty()) {
                typeface2 = typeface;
            }
            else {
                final Typeface create = Typeface.create(s, 0);
                final Typeface create2 = Typeface.create(Typeface.DEFAULT, 0);
                typeface2 = typeface;
                if (create != null) {
                    typeface2 = typeface;
                    if (!create.equals((Object)create2)) {
                        typeface2 = create;
                    }
                }
            }
        }
        return typeface2;
    }
    
    public static class a extends c
    {
        public le1.e a;
        
        public a(final le1.e a) {
            this.a = a;
        }
        
        @Override
        public void a(final int n) {
            final le1.e a = this.a;
            if (a != null) {
                a.onFontRetrievalFailed(n);
            }
        }
        
        @Override
        public void b(final Typeface typeface) {
            final le1.e a = this.a;
            if (a != null) {
                a.onFontRetrieved(typeface);
            }
        }
    }
}

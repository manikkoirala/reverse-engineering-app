import android.os.Looper;
import android.os.Handler;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class se
{
    public static Handler a() {
        Handler handler;
        if (Looper.myLooper() == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        else {
            handler = new Handler();
        }
        return handler;
    }
}

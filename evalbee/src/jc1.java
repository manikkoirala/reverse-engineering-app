import android.widget.CompoundButton;
import android.view.View;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.widget.RadioGroup$OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class jc1
{
    public Context a;
    public y01 b;
    public String c;
    public RadioGroup d;
    public RadioButton e;
    public RadioButton f;
    public TextView g;
    
    public jc1(final Context a, final y01 b, final String c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.e();
    }
    
    public static /* synthetic */ RadioButton a(final jc1 jc1) {
        return jc1.e;
    }
    
    public static /* synthetic */ TextView b(final jc1 jc1) {
        return jc1.g;
    }
    
    public final void c() {
        this.d();
        this.d.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener(this) {
            public final jc1 a;
            
            public void onCheckedChanged(final RadioGroup radioGroup, int text) {
                TextView textView;
                if (((CompoundButton)jc1.a(this.a)).isChecked()) {
                    textView = jc1.b(this.a);
                    text = 2131886471;
                }
                else {
                    textView = jc1.b(this.a);
                    text = 2131886465;
                }
                textView.setText(text);
            }
        });
    }
    
    public final void d() {
        final String c = this.c;
        TextView textView;
        int text;
        if (c != null && c.equals(ok.L)) {
            ((CompoundButton)this.f).setChecked(true);
            textView = this.g;
            text = 2131886465;
        }
        else {
            ((CompoundButton)this.e).setChecked(true);
            textView = this.g;
            text = 2131886471;
        }
        textView.setText(text);
    }
    
    public final void e() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492982, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886786);
        this.e = (RadioButton)inflate.findViewById(2131296984);
        this.f = (RadioButton)inflate.findViewById(2131296983);
        this.g = (TextView)inflate.findViewById(2131297269);
        this.d = (RadioGroup)inflate.findViewById(2131297003);
        this.c();
        materialAlertDialogBuilder.setPositiveButton(2131886759, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final jc1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final jc1 a = this.a;
                if (a.b != null) {
                    y01 y01;
                    String s;
                    if (((CompoundButton)jc1.a(a)).isChecked()) {
                        y01 = this.a.b;
                        s = ok.a;
                    }
                    else {
                        y01 = this.a.b;
                        s = ok.L;
                    }
                    y01.a(s);
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final jc1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
}

import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class yg extends b
{
    public final ch a;
    
    public yg(final ch a) {
        fg0.e((Object)a, "clock");
        this.a = a;
    }
    
    @Override
    public void c(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        super.c(ss1);
        ss1.m();
        try {
            ss1.M(this.e());
            ss1.S();
        }
        finally {
            ss1.X();
        }
    }
    
    public final long d() {
        return this.a.currentTimeMillis() - l82.a;
    }
    
    public final String e() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM workspec WHERE state IN (2, 3, 5) AND (last_enqueue_time + minimum_retention_duration) < ");
        sb.append(this.d());
        sb.append(" AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))");
        return sb.toString();
    }
}

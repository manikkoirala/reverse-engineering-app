// 
// Decompiled by Procyon v0.6.0
// 

public final class h30
{
    public final String a;
    
    public h30(final String a) {
        this.a = a;
    }
    
    public final String a() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof h30 && fg0.a((Object)this.a, (Object)((h30)o).a));
    }
    
    @Override
    public int hashCode() {
        final String a = this.a;
        int hashCode;
        if (a == null) {
            hashCode = 0;
        }
        else {
            hashCode = a.hashCode();
        }
        return hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FirebaseSessionsData(sessionId=");
        sb.append(this.a);
        sb.append(')');
        return sb.toString();
    }
}

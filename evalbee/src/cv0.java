import java.util.HashSet;
import java.util.SortedSet;
import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

// 
// Decompiled by Procyon v0.6.0
// 

public class cv0 implements eu
{
    public final TreeMap a;
    public final Map b;
    
    public cv0() {
        this.a = new TreeMap();
        this.b = new HashMap();
    }
    
    @Override
    public k21 a(final du key) {
        return this.a.get(key);
    }
    
    @Override
    public void b(final int i) {
        if (this.b.containsKey(i)) {
            final Set set = this.b.get(i);
            this.b.remove(i);
            final Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                this.a.remove(iterator.next());
            }
        }
    }
    
    @Override
    public Map c(final ke1 ke1, final int n) {
        final HashMap hashMap = new HashMap();
        final int l = ke1.l();
        for (final k21 k21 : this.a.tailMap(du.g((ke1)ke1.c(""))).values()) {
            final du b = k21.b();
            if (!ke1.k(b.m())) {
                break;
            }
            if (b.m().l() != l + 1) {
                continue;
            }
            if (k21.c() <= n) {
                continue;
            }
            hashMap.put(k21.b(), k21);
        }
        return hashMap;
    }
    
    @Override
    public Map d(final String anObject, final int n, final int n2) {
        final TreeMap treeMap = new TreeMap();
        for (final k21 k21 : this.a.values()) {
            if (!k21.b().j().equals(anObject)) {
                continue;
            }
            if (k21.c() <= n) {
                continue;
            }
            Map map;
            if ((map = (Map)treeMap.get(k21.c())) == null) {
                map = new HashMap();
                treeMap.put(k21.c(), map);
            }
            map.put(k21.b(), k21);
        }
        final HashMap hashMap = new HashMap();
        final Iterator iterator2 = treeMap.values().iterator();
        while (iterator2.hasNext()) {
            hashMap.putAll((Map)iterator2.next());
            if (hashMap.size() >= n2) {
                break;
            }
        }
        return hashMap;
    }
    
    @Override
    public void e(final int n, final Map map) {
        for (final Map.Entry<K, wx0> entry : map.entrySet()) {
            this.g(n, (wx0)k71.d(entry.getValue(), "null value for key: %s", entry.getKey()));
        }
    }
    
    @Override
    public Map f(final SortedSet set) {
        final HashMap hashMap = new HashMap();
        for (final du key : set) {
            final k21 k21 = this.a.get(key);
            if (k21 != null) {
                hashMap.put(key, k21);
            }
        }
        return hashMap;
    }
    
    public final void g(final int i, final wx0 wx0) {
        final k21 k21 = this.a.get(wx0.g());
        if (k21 != null) {
            ((Set)this.b.get(k21.c())).remove(wx0.g());
        }
        this.a.put(wx0.g(), k21.a(i, wx0));
        if (this.b.get(i) == null) {
            this.b.put(i, new HashSet());
        }
        this.b.get(i).add(wx0.g());
    }
}

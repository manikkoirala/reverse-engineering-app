import android.net.Uri$Builder;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;
import android.text.TextUtils;
import org.json.JSONObject;

// 
// Decompiled by Procyon v0.6.0
// 

public class ff1 extends bf1
{
    public final JSONObject m;
    public final String n;
    
    public ff1(final kq1 kq1, final r10 r10, final JSONObject m, final String n) {
        super(kq1, r10);
        this.m = m;
        this.n = n;
        if (TextUtils.isEmpty((CharSequence)n)) {
            super.a = new IllegalArgumentException("mContentType is null or empty");
        }
        super.G("X-Goog-Upload-Protocol", "resumable");
        super.G("X-Goog-Upload-Command", "start");
        super.G("X-Goog-Upload-Header-Content-Type", n);
    }
    
    @Override
    public String e() {
        return "POST";
    }
    
    @Override
    public JSONObject g() {
        return this.m;
    }
    
    @Override
    public Map l() {
        final HashMap hashMap = new HashMap();
        hashMap.put("name", this.j());
        hashMap.put("uploadType", "resumable");
        return hashMap;
    }
    
    @Override
    public Uri u() {
        final String authority = this.s().a().getAuthority();
        final Uri$Builder buildUpon = this.s().b().buildUpon();
        buildUpon.appendPath("b");
        buildUpon.appendPath(authority);
        buildUpon.appendPath("o");
        return buildUpon.build();
    }
}

import androidx.lifecycle.o;
import java.lang.reflect.Modifier;
import android.util.Log;
import android.os.Looper;
import android.os.Bundle;
import java.io.PrintWriter;
import java.io.FileDescriptor;

// 
// Decompiled by Procyon v0.6.0
// 

public class sk0 extends rk0
{
    public static boolean c = false;
    public final qj0 a;
    public final c b;
    
    public sk0(final qj0 a, final b42 b42) {
        this.a = a;
        this.b = sk0.c.h(b42);
    }
    
    @Override
    public void a(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        this.b.f(s, fileDescriptor, printWriter, array);
    }
    
    @Override
    public qk0 c(final int n, final Bundle obj, final rk0.a a) {
        if (this.b.j()) {
            throw new IllegalStateException("Called while creating a loader");
        }
        if (Looper.getMainLooper() != Looper.myLooper()) {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
        final a i = this.b.i(n);
        if (sk0.c) {
            final StringBuilder sb = new StringBuilder();
            sb.append("initLoader in ");
            sb.append(this);
            sb.append(": args=");
            sb.append(obj);
            Log.v("LoaderManager", sb.toString());
        }
        if (i == null) {
            return this.e(n, obj, a, null);
        }
        if (sk0.c) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  Re-using existing loader ");
            sb2.append(i);
            Log.v("LoaderManager", sb2.toString());
        }
        return i.s(this.a, a);
    }
    
    @Override
    public void d() {
        this.b.k();
    }
    
    public final qk0 e(final int n, final Bundle bundle, final rk0.a a, final qk0 qk0) {
        try {
            this.b.m();
            final qk0 onCreateLoader = a.onCreateLoader(n, bundle);
            if (onCreateLoader == null) {
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
            }
            if (onCreateLoader.getClass().isMemberClass() && !Modifier.isStatic(onCreateLoader.getClass().getModifiers())) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Object returned from onCreateLoader must not be a non-static inner member class: ");
                sb.append(onCreateLoader);
                throw new IllegalArgumentException(sb.toString());
            }
            final a obj = new a(n, bundle, onCreateLoader, qk0);
            if (sk0.c) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("  Created new loader ");
                sb2.append(obj);
                Log.v("LoaderManager", sb2.toString());
            }
            this.b.l(n, obj);
            this.b.g();
            return obj.s(this.a, a);
        }
        finally {
            this.b.g();
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        bq.a(this.a, sb);
        sb.append("}}");
        return sb.toString();
    }
    
    public static class a extends tx0 implements qk0.b
    {
        public final int l;
        public final Bundle m;
        public final qk0 n;
        public qj0 o;
        public sk0.b p;
        public qk0 q;
        
        public a(final int l, final Bundle m, final qk0 n, final qk0 q) {
            this.l = l;
            this.m = m;
            this.n = n;
            this.q = q;
            n.registerListener(l, (qk0.b)this);
        }
        
        @Override
        public void a(final qk0 qk0, final Object o) {
            if (sk0.c) {
                final StringBuilder sb = new StringBuilder();
                sb.append("onLoadComplete: ");
                sb.append(this);
                Log.v("LoaderManager", sb.toString());
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                this.n(o);
            }
            else {
                if (sk0.c) {
                    Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
                }
                this.l(o);
            }
        }
        
        @Override
        public void j() {
            if (sk0.c) {
                final StringBuilder sb = new StringBuilder();
                sb.append("  Starting: ");
                sb.append(this);
                Log.v("LoaderManager", sb.toString());
            }
            this.n.startLoading();
        }
        
        @Override
        public void k() {
            if (sk0.c) {
                final StringBuilder sb = new StringBuilder();
                sb.append("  Stopping: ");
                sb.append(this);
                Log.v("LoaderManager", sb.toString());
            }
            this.n.stopLoading();
        }
        
        @Override
        public void m(final d11 d11) {
            super.m(d11);
            this.o = null;
            this.p = null;
        }
        
        @Override
        public void n(final Object o) {
            super.n(o);
            final qk0 q = this.q;
            if (q != null) {
                q.reset();
                this.q = null;
            }
        }
        
        public qk0 o(final boolean b) {
            if (sk0.c) {
                final StringBuilder sb = new StringBuilder();
                sb.append("  Destroying: ");
                sb.append(this);
                Log.v("LoaderManager", sb.toString());
            }
            this.n.cancelLoad();
            this.n.abandon();
            final sk0.b p = this.p;
            if (p != null) {
                this.m(p);
                if (b) {
                    p.d();
                }
            }
            this.n.unregisterListener((qk0.b)this);
            if ((p != null && !p.c()) || b) {
                this.n.reset();
                return this.q;
            }
            return this.n;
        }
        
        public void p(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
            printWriter.print(s);
            printWriter.print("mId=");
            printWriter.print(this.l);
            printWriter.print(" mArgs=");
            printWriter.println(this.m);
            printWriter.print(s);
            printWriter.print("mLoader=");
            printWriter.println(this.n);
            final qk0 n = this.n;
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append("  ");
            n.dump(sb.toString(), fileDescriptor, printWriter, array);
            if (this.p != null) {
                printWriter.print(s);
                printWriter.print("mCallbacks=");
                printWriter.println(this.p);
                final sk0.b p4 = this.p;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s);
                sb2.append("  ");
                p4.b(sb2.toString(), printWriter);
            }
            printWriter.print(s);
            printWriter.print("mData=");
            printWriter.println(this.q().dataToString(this.f()));
            printWriter.print(s);
            printWriter.print("mStarted=");
            printWriter.println(this.g());
        }
        
        public qk0 q() {
            return this.n;
        }
        
        public void r() {
            final qj0 o = this.o;
            final sk0.b p = this.p;
            if (o != null && p != null) {
                super.m(p);
                this.h(o, p);
            }
        }
        
        public qk0 s(final qj0 o, final rk0.a a) {
            final sk0.b p2 = new sk0.b(this.n, a);
            this.h(o, p2);
            final sk0.b p3 = this.p;
            if (p3 != null) {
                this.m(p3);
            }
            this.o = o;
            this.p = p2;
            return this.n;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.l);
            sb.append(" : ");
            bq.a(this.n, sb);
            sb.append("}}");
            return sb.toString();
        }
    }
    
    public static class b implements d11
    {
        public final qk0 a;
        public final rk0.a b;
        public boolean c;
        
        public b(final qk0 a, final rk0.a b) {
            this.c = false;
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void a(final Object o) {
            if (sk0.c) {
                final StringBuilder sb = new StringBuilder();
                sb.append("  onLoadFinished in ");
                sb.append(this.a);
                sb.append(": ");
                sb.append(this.a.dataToString(o));
                Log.v("LoaderManager", sb.toString());
            }
            this.b.onLoadFinished(this.a, o);
            this.c = true;
        }
        
        public void b(final String s, final PrintWriter printWriter) {
            printWriter.print(s);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }
        
        public boolean c() {
            return this.c;
        }
        
        public void d() {
            if (this.c) {
                if (sk0.c) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("  Resetting: ");
                    sb.append(this.a);
                    Log.v("LoaderManager", sb.toString());
                }
                this.b.onLoaderReset(this.a);
            }
        }
        
        @Override
        public String toString() {
            return this.b.toString();
        }
    }
    
    public static class c extends y32
    {
        public static final o.b f;
        public wo1 d;
        public boolean e;
        
        static {
            f = new o.b() {
                @Override
                public y32 b(final Class clazz) {
                    return new sk0.c();
                }
            };
        }
        
        public c() {
            this.d = new wo1();
            this.e = false;
        }
        
        public static c h(final b42 b42) {
            return (c)new o(b42, c.f).a(c.class);
        }
        
        @Override
        public void d() {
            super.d();
            for (int l = this.d.l(), i = 0; i < l; ++i) {
                ((a)this.d.m(i)).o(true);
            }
            this.d.b();
        }
        
        public void f(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
            if (this.d.l() > 0) {
                printWriter.print(s);
                printWriter.println("Loaders:");
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append("    ");
                final String string = sb.toString();
                for (int i = 0; i < this.d.l(); ++i) {
                    final a a = (a)this.d.m(i);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(this.d.j(i));
                    printWriter.print(": ");
                    printWriter.println(a.toString());
                    a.p(string, fileDescriptor, printWriter, array);
                }
            }
        }
        
        public void g() {
            this.e = false;
        }
        
        public a i(final int n) {
            return (a)this.d.f(n);
        }
        
        public boolean j() {
            return this.e;
        }
        
        public void k() {
            for (int l = this.d.l(), i = 0; i < l; ++i) {
                ((a)this.d.m(i)).r();
            }
        }
        
        public void l(final int n, final a a) {
            this.d.k(n, a);
        }
        
        public void m() {
            this.e = true;
        }
    }
}

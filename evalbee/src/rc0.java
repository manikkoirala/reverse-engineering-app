// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rc0
{
    public static int a(int max, final double n) {
        max = Math.max(max, 2);
        final int highestOneBit = Integer.highestOneBit(max);
        if (max > (int)(n * highestOneBit)) {
            max = highestOneBit << 1;
            if (max <= 0) {
                max = 1073741824;
            }
            return max;
        }
        return highestOneBit;
    }
    
    public static boolean b(final int n, final int n2, final double n3) {
        return n > n3 * n2 && n2 < 1073741824;
    }
    
    public static int c(final int n) {
        return (int)(Integer.rotateLeft((int)(n * -862048943L), 15) * 461845907L);
    }
    
    public static int d(final Object o) {
        int hashCode;
        if (o == null) {
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
        }
        return c(hashCode);
    }
}

import android.util.LongSparseArray;
import android.util.Log;
import android.os.Build$VERSION;
import android.content.res.Resources;
import java.lang.reflect.Field;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class oe1
{
    public static Field a;
    public static boolean b;
    public static Class c;
    public static boolean d;
    public static Field e;
    public static boolean f;
    public static Field g;
    public static boolean h;
    
    public static void a(final Resources resources) {
        if (Build$VERSION.SDK_INT >= 28) {
            return;
        }
        b(resources);
    }
    
    public static void b(Resources value) {
        if (!oe1.h) {
            try {
                (oe1.g = Resources.class.getDeclaredField("mResourcesImpl")).setAccessible(true);
            }
            catch (final NoSuchFieldException ex) {
                Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", (Throwable)ex);
            }
            oe1.h = true;
        }
        final Field g = oe1.g;
        if (g == null) {
            return;
        }
        final Object o = null;
        try {
            value = g.get(value);
        }
        catch (final IllegalAccessException ex2) {
            Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", (Throwable)ex2);
            value = null;
        }
        if (value == null) {
            return;
        }
        if (!oe1.b) {
            try {
                (oe1.a = value.getClass().getDeclaredField("mDrawableCache")).setAccessible(true);
            }
            catch (final NoSuchFieldException ex3) {
                Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", (Throwable)ex3);
            }
            oe1.b = true;
        }
        final Field a = oe1.a;
        Object value2 = o;
        if (a != null) {
            try {
                value2 = a.get(value);
            }
            catch (final IllegalAccessException ex4) {
                Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", (Throwable)ex4);
                value2 = o;
            }
        }
        if (value2 != null) {
            c(value2);
        }
    }
    
    public static void c(final Object obj) {
        if (!oe1.d) {
            try {
                oe1.c = Class.forName("android.content.res.ThemedResourceCache");
            }
            catch (final ClassNotFoundException ex) {
                Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", (Throwable)ex);
            }
            oe1.d = true;
        }
        final Class c = oe1.c;
        if (c == null) {
            return;
        }
        if (!oe1.f) {
            try {
                (oe1.e = c.getDeclaredField("mUnthemedEntries")).setAccessible(true);
            }
            catch (final NoSuchFieldException ex2) {
                Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", (Throwable)ex2);
            }
            oe1.f = true;
        }
        final Field e = oe1.e;
        if (e == null) {
            return;
        }
        LongSparseArray longSparseArray;
        try {
            longSparseArray = (LongSparseArray)e.get(obj);
        }
        catch (final IllegalAccessException ex3) {
            Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", (Throwable)ex3);
            longSparseArray = null;
        }
        if (longSparseArray != null) {
            oe1.a.a(longSparseArray);
        }
    }
    
    public abstract static class a
    {
        public static void a(final LongSparseArray longSparseArray) {
            longSparseArray.clear();
        }
    }
}

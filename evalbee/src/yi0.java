// 
// Decompiled by Procyon v0.6.0
// 

public class yi0 implements r91
{
    public static final Object c;
    public volatile Object a;
    public volatile r91 b;
    
    static {
        c = new Object();
    }
    
    public yi0(final r91 b) {
        this.a = yi0.c;
        this.b = b;
    }
    
    @Override
    public Object get() {
        final Object a = this.a;
        final Object c = yi0.c;
        final Object o = a;
        if (a == c) {
            synchronized (this) {
                if (this.a == c) {
                    this.a = this.b.get();
                    this.b = null;
                }
            }
        }
        return o;
    }
}

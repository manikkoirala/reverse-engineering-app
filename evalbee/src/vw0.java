// 
// Decompiled by Procyon v0.6.0
// 

public class vw0 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        if (n == 0) {
            return Double.MIN_VALUE;
        }
        double n2 = Double.MAX_VALUE;
        double n4;
        for (int i = 0; i < n; ++i, n2 = n4) {
            final double n3 = array[i];
            n4 = n2;
            if (n3 < n2) {
                n4 = n3;
            }
        }
        return n2;
    }
    
    @Override
    public boolean b(final int n) {
        return n >= 0;
    }
    
    @Override
    public String toString() {
        return "min(x1, x2, ..., xn)";
    }
}

import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class a10
{
    public static final void a(final ReadableByteChannel readableByteChannel, final FileChannel fileChannel) {
        fg0.e((Object)readableByteChannel, "input");
        fg0.e((Object)fileChannel, "output");
        try {
            fileChannel.transferFrom(readableByteChannel, 0L, Long.MAX_VALUE);
            fileChannel.force(false);
        }
        finally {
            readableByteChannel.close();
            fileChannel.close();
        }
    }
}

import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup;
import java.util.ArrayList;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.os.Bundle;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.models.InvalidQuestionSet;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.Spinner;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ig0 extends u80
{
    public Context a;
    public lg[] b;
    public String c;
    public f d;
    public SheetTemplate2 e;
    public boolean[][] f;
    public int g;
    public int h;
    public Spinner i;
    public Spinner j;
    public CompoundButton$OnCheckedChangeListener k;
    
    public ig0(final Context a, final f d, final SheetTemplate2 e) {
        super(a);
        this.h = 0;
        this.k = (CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final ig0 a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                ig0.a(this.a)[this.a.h][((lg)compoundButton).getQuestionNo() - 1] = b;
            }
        };
        this.a = a;
        this.d = d;
        this.e = e;
        this.f = this.f(e);
    }
    
    public static /* synthetic */ boolean[][] a(final ig0 ig0) {
        return ig0.f;
    }
    
    public static /* synthetic */ Spinner b(final ig0 ig0) {
        return ig0.j;
    }
    
    public static /* synthetic */ Spinner d(final ig0 ig0) {
        return ig0.i;
    }
    
    public static /* synthetic */ f e(final ig0 ig0) {
        return ig0.d;
    }
    
    public final boolean[][] f(final SheetTemplate2 sheetTemplate2) {
        this.g = sheetTemplate2.getAnswerOptions().size() - 1;
        final boolean[][] array = new boolean[sheetTemplate2.getTemplateParams().getExamSets()][this.g];
        final InvalidQuestionSet[] invalidQuestionSets = sheetTemplate2.getInvalidQuestionSets();
        if (invalidQuestionSets != null) {
            this.c = invalidQuestionSets[0].getInvalidQueMarks();
            for (int i = 0; i < invalidQuestionSets.length; ++i) {
                final int[] invalidQuestions = invalidQuestionSets[i].getInvalidQuestions();
                if (invalidQuestions != null) {
                    for (int length = invalidQuestions.length, j = 0; j < length; ++j) {
                        array[i][invalidQuestions[j] - 1] = true;
                    }
                }
            }
        }
        return array;
    }
    
    public final void g() {
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296800);
        ((ViewGroup)linearLayout).removeAllViews();
        this.b = new lg[this.g];
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, -2);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(0, 0, a91.d(16, this.a), 0);
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(a91.d(40, this.a), a91.d(40, this.a));
        int n;
        for (int i = 0; i < this.b.length; i = n) {
            final LinearLayout b = yo.b(this.a);
            final Context a = this.a;
            final StringBuilder sb = new StringBuilder();
            n = i + 1;
            sb.append(n);
            sb.append("");
            final TextView c = yo.c(a, sb.toString(), 50);
            ((CompoundButton)(this.b[i] = new lg(this.a))).setChecked(false);
            this.b[i].setQuestionNo(n);
            ((CompoundButton)this.b[i]).setChecked(this.f[this.h][i]);
            ((CompoundButton)this.b[i]).setOnCheckedChangeListener(this.k);
            ((ViewGroup)b).addView((View)c, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            ((ViewGroup)b).addView((View)this.b[i], (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            ((ViewGroup)linearLayout).addView((View)b);
        }
    }
    
    public final void h() {
        ((Toolbar)this.findViewById(2131297349)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ig0 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492974);
        this.h();
        this.i = (Spinner)this.findViewById(2131297085);
        this.j = (Spinner)this.findViewById(2131297079);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296752);
        this.i.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])new String[] { "None", "Max" }));
        if (this.f.length > 1) {
            final String[] examSetLabels = this.e.getLabelProfile().getExamSetLabels();
            final int length = this.f.length;
            final String[] array = new String[length];
            for (int i = 0; i < length; ++i) {
                array[i] = examSetLabels[i];
            }
            this.j.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])array));
            ((AdapterView)this.j).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
                public final ig0 a;
                
                public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                    final ig0 a = this.a;
                    a.h = ((AdapterView)ig0.b(a)).getSelectedItemPosition();
                    this.a.g();
                }
                
                public void onNothingSelected(final AdapterView adapterView) {
                }
            });
            ((View)linearLayout).setVisibility(0);
        }
        else {
            ((View)linearLayout).setVisibility(8);
            this.g();
        }
        final String c = this.c;
        if (c != null && !c.equals("Max")) {
            ((AdapterView)this.i).setSelection(0);
        }
        else {
            ((AdapterView)this.i).setSelection(1);
        }
        this.findViewById(2131296390).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ig0 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296402).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ig0 a;
            
            public void onClick(final View view) {
                ig0 ig0;
                String c;
                if (((AdapterView)ig0.d(this.a)).getSelectedItemPosition() == 0) {
                    ig0 = this.a;
                    c = "None";
                }
                else {
                    ig0 = this.a;
                    c = "Max";
                }
                ig0.c = c;
                final int length = ig0.a(this.a).length;
                final InvalidQuestionSet[] array = new InvalidQuestionSet[length];
                int n;
                for (int i = 0; i < length; i = n) {
                    final ArrayList<Integer> list = new ArrayList<Integer>();
                    for (int j = 0; j < ig0.a(this.a)[i].length; ++j) {
                        if (ig0.a(this.a)[i][j]) {
                            list.add(j + 1);
                        }
                    }
                    int[] array3;
                    if (list.size() > 0) {
                        final int size = list.size();
                        final int[] array2 = new int[size];
                        int index = 0;
                        while (true) {
                            array3 = array2;
                            if (index >= size) {
                                break;
                            }
                            array2[index] = list.get(index);
                            ++index;
                        }
                    }
                    else {
                        array3 = null;
                    }
                    n = i + 1;
                    int n2;
                    if (length == 1) {
                        n2 = 0;
                    }
                    else {
                        n2 = n;
                    }
                    array[i] = new InvalidQuestionSet(n2, array3, this.a.c);
                }
                ig0.e(this.a).a(array);
                this.a.dismiss();
            }
        });
    }
    
    public interface f
    {
        void a(final InvalidQuestionSet[] p0);
    }
}

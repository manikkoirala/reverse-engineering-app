// 
// Decompiled by Procyon v0.6.0
// 

public final class os
{
    public final da1 a;
    public final int b;
    public final int c;
    
    public os(final da1 da1, final int b, final int c) {
        this.a = (da1)j71.c(da1, "Null dependency anInterface.");
        this.b = b;
        this.c = c;
    }
    
    public os(final Class clazz, final int n, final int n2) {
        this(da1.b(clazz), n, n2);
    }
    
    public static os a(final Class clazz) {
        return new os(clazz, 0, 2);
    }
    
    public static String b(final int i) {
        if (i == 0) {
            return "direct";
        }
        if (i == 1) {
            return "provider";
        }
        if (i == 2) {
            return "deferred";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unsupported injection: ");
        sb.append(i);
        throw new AssertionError((Object)sb.toString());
    }
    
    public static os h(final Class clazz) {
        return new os(clazz, 0, 0);
    }
    
    public static os i(final Class clazz) {
        return new os(clazz, 0, 1);
    }
    
    public static os j(final da1 da1) {
        return new os(da1, 1, 0);
    }
    
    public static os k(final Class clazz) {
        return new os(clazz, 1, 0);
    }
    
    public static os l(final da1 da1) {
        return new os(da1, 1, 1);
    }
    
    public static os m(final Class clazz) {
        return new os(clazz, 1, 1);
    }
    
    public static os n(final Class clazz) {
        return new os(clazz, 2, 0);
    }
    
    public da1 c() {
        return this.a;
    }
    
    public boolean d() {
        return this.c == 2;
    }
    
    public boolean e() {
        return this.c == 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof os;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final os os = (os)o;
            b3 = b2;
            if (this.a.equals(os.a)) {
                b3 = b2;
                if (this.b == os.b) {
                    b3 = b2;
                    if (this.c == os.c) {
                        b3 = true;
                    }
                }
            }
        }
        return b3;
    }
    
    public boolean f() {
        final int b = this.b;
        boolean b2 = true;
        if (b != 1) {
            b2 = false;
        }
        return b2;
    }
    
    public boolean g() {
        return this.b == 2;
    }
    
    @Override
    public int hashCode() {
        return ((this.a.hashCode() ^ 0xF4243) * 1000003 ^ this.b) * 1000003 ^ this.c;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", type=");
        final int b = this.b;
        String str;
        if (b == 1) {
            str = "required";
        }
        else if (b == 0) {
            str = "optional";
        }
        else {
            str = "set";
        }
        sb.append(str);
        sb.append(", injection=");
        sb.append(b(this.c));
        sb.append("}");
        return sb.toString();
    }
}

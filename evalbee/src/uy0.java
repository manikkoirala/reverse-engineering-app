import android.os.Build$VERSION;
import androidx.work.NetworkType;
import androidx.work.impl.constraints.controllers.ConstraintController;

// 
// Decompiled by Procyon v0.6.0
// 

public final class uy0 extends ConstraintController
{
    public final int b;
    
    public uy0(final tk tk) {
        fg0.e((Object)tk, "tracker");
        super(tk);
        this.b = 7;
    }
    
    @Override
    public int b() {
        return this.b;
    }
    
    @Override
    public boolean c(final p92 p) {
        fg0.e((Object)p, "workSpec");
        return p.j.d() == NetworkType.CONNECTED;
    }
    
    public boolean g(final zy0 zy0) {
        fg0.e((Object)zy0, "value");
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        if (sdk_INT >= 26) {
            boolean b2 = b;
            if (!zy0.a()) {
                return b2;
            }
            if (!zy0.d()) {
                b2 = b;
                return b2;
            }
        }
        else if (!zy0.a()) {
            return b;
        }
        return false;
    }
}

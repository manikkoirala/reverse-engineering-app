import java.util.Iterator;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public class ir implements v12
{
    public final String a;
    public final eb0 b;
    
    public ir(final Set set, final eb0 b) {
        this.a = e(set);
        this.b = b;
    }
    
    public static zi c() {
        return zi.e(v12.class).b(os.n(kj0.class)).f(new hr()).d();
    }
    
    public static String e(final Set set) {
        final StringBuilder sb = new StringBuilder();
        final Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            final kj0 kj0 = (kj0)iterator.next();
            sb.append(kj0.b());
            sb.append('/');
            sb.append(kj0.c());
            if (iterator.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
    
    @Override
    public String a() {
        if (this.b.b().isEmpty()) {
            return this.a;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(' ');
        sb.append(e(this.b.b()));
        return sb.toString();
    }
}

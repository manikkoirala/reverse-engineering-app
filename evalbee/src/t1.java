import android.view.ViewGroup$LayoutParams;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.KeyEvent;
import android.content.res.Configuration;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class t1
{
    public boolean f() {
        return false;
    }
    
    public abstract boolean g();
    
    public abstract void h(final boolean p0);
    
    public abstract int i();
    
    public abstract Context j();
    
    public boolean k() {
        return false;
    }
    
    public void l(final Configuration configuration) {
    }
    
    public void m() {
    }
    
    public abstract boolean n(final int p0, final KeyEvent p1);
    
    public boolean o(final KeyEvent keyEvent) {
        return false;
    }
    
    public boolean p() {
        return false;
    }
    
    public abstract void q(final boolean p0);
    
    public abstract void r(final boolean p0);
    
    public abstract void s(final CharSequence p0);
    
    public abstract void t(final CharSequence p0);
    
    public abstract void u(final CharSequence p0);
    
    public c2 v(final c2.a a) {
        return null;
    }
    
    public abstract static class a extends ViewGroup$MarginLayoutParams
    {
        public int a;
        
        public a(final int n, final int n2) {
            super(n, n2);
            this.a = 8388627;
        }
        
        public a(final Context context, final AttributeSet set) {
            super(context, set);
            this.a = 0;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, bc1.t);
            this.a = obtainStyledAttributes.getInt(bc1.u, 0);
            obtainStyledAttributes.recycle();
        }
        
        public a(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.a = 0;
        }
        
        public a(final a a) {
            super((ViewGroup$MarginLayoutParams)a);
            this.a = 0;
            this.a = a.a;
        }
    }
}

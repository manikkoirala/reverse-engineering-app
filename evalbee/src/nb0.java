import android.widget.TextView;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup;
import android.widget.EditText;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import java.util.Collection;
import java.util.Arrays;
import android.view.View;
import com.ekodroid.omrevaluator.templateui.models.GradeLevel;
import android.view.View$OnClickListener;
import android.widget.LinearLayout;
import java.util.ArrayList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class nb0 extends u80
{
    public Context a;
    public y01 b;
    public ArrayList c;
    public LinearLayout d;
    public View$OnClickListener e;
    
    public nb0(final Context a, final y01 b, final GradeLevel[] a2) {
        super(a);
        this.e = (View$OnClickListener)new View$OnClickListener(this) {
            public final nb0 a;
            
            public void onClick(final View view) {
                this.a.c.remove(((po)view).getIndex());
                final nb0 a = this.a;
                a.b(a.c, a.d);
            }
        };
        this.a = a;
        this.b = b;
        final ArrayList c = new ArrayList();
        this.c = c;
        if (a2 != null && a2.length > 0) {
            c.addAll(Arrays.asList(a2));
        }
    }
    
    public final void b(final ArrayList list, final LinearLayout linearLayout) {
        if (list != null) {
            ((ViewGroup)linearLayout).removeAllViews();
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, -2);
            final int d = a91.d(16, this.a);
            int i = 0;
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, 0, 0, 0);
            linearLayout$LayoutParams.weight = 1.0f;
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(a91.d(32, this.a), a91.d(32, this.a));
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(0, 0, a91.d(12, this.a), 0);
            while (i < list.size()) {
                final LinearLayout b = yo.b(this.a);
                ((View)b).setBackground(g7.b(this.a, 2131230848));
                ((ViewGroup)b).addView((View)yo.c(this.a, list.get(i).getStringLabel(), 150), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                final po po = new po(this.a);
                po.setIndex(i);
                ((View)po).setBackground(g7.b(this.a, 2131230862));
                ((View)po).setOnClickListener(this.e);
                ((ViewGroup)b).addView((View)po, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
                ((ViewGroup)linearLayout).addView((View)b);
                ++i;
            }
        }
    }
    
    public final void c() {
        ((Toolbar)this.findViewById(2131297346)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final nb0 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492972);
        this.setTitle(2131886813);
        this.c();
        this.d = (LinearLayout)this.findViewById(2131296815);
        final EditText editText = (EditText)this.findViewById(2131296580);
        final EditText editText2 = (EditText)this.findViewById(2131296585);
        final EditText editText3 = (EditText)this.findViewById(2131296583);
        this.b(this.c, this.d);
        this.findViewById(2131296406).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, editText, editText2, editText3) {
            public final EditText a;
            public final EditText b;
            public final EditText c;
            public final nb0 d;
            
            public void onClick(final View view) {
                final String string = this.a.getText().toString();
                if (string.trim().length() >= 1 && this.b.getText().toString().trim().length() >= 1) {
                    if (this.c.getText().toString().trim().length() >= 1) {
                        this.d.c.add(new GradeLevel(string, ve1.o(Double.parseDouble(this.b.getText().toString())), ve1.o(Double.parseDouble(this.c.getText().toString()))));
                        final nb0 d = this.d;
                        d.b(d.c, d.d);
                        ((TextView)this.a).setText((CharSequence)"");
                        ((TextView)this.c).setText((CharSequence)"");
                        ((TextView)this.b).setText((CharSequence)"");
                        ((View)this.a).requestFocus();
                    }
                }
            }
        });
        this.findViewById(2131296389).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final nb0 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296401).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final nb0 a;
            
            public void onClick(final View view) {
                GradeLevel[] array;
                if (this.a.c.size() > 0) {
                    final ArrayList c = this.a.c;
                    array = c.toArray(new GradeLevel[c.size()]);
                }
                else {
                    array = null;
                }
                final y01 b = this.a.b;
                if (b != null) {
                    b.a(array);
                }
                this.a.dismiss();
            }
        });
    }
}

import android.os.CancellationSignal;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jf
{
    public boolean a;
    public b b;
    public Object c;
    public boolean d;
    
    public void a() {
        synchronized (this) {
            if (this.a) {
                return;
            }
            this.a = true;
            this.d = true;
            final b b = this.b;
            final Object c = this.c;
            monitorexit(this);
            Label_0051: {
                if (b == null) {
                    break Label_0051;
                }
                try {
                    b.a();
                    break Label_0051;
                }
                finally {
                    synchronized (this) {
                        this.d = false;
                        this.notifyAll();
                    }
                    iftrue(Label_0082:)(c == null);
                    jf.a.a(c);
                }
            }
            Label_0082: {
                synchronized (this) {
                    this.d = false;
                    this.notifyAll();
                }
            }
        }
    }
    
    public boolean b() {
        synchronized (this) {
            return this.a;
        }
    }
    
    public void c(final b b) {
        synchronized (this) {
            this.d();
            if (this.b == b) {
                return;
            }
            this.b = b;
            if (this.a && b != null) {
                monitorexit(this);
                b.a();
            }
        }
    }
    
    public final void d() {
        while (true) {
            if (!this.d) {
                return;
            }
            try {
                this.wait();
                continue;
            }
            catch (final InterruptedException ex) {}
        }
    }
    
    public abstract static class a
    {
        public static void a(final Object o) {
            ((CancellationSignal)o).cancel();
        }
        
        public static CancellationSignal b() {
            return new CancellationSignal();
        }
    }
    
    public interface b
    {
        void a();
    }
}

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.d;

// 
// Decompiled by Procyon v0.6.0
// 

public class r6 extends d
{
    public r6() {
    }
    
    public r6(final int n) {
        super(n);
    }
    
    @Override
    public Dialog onCreateDialog(final Bundle bundle) {
        return new q6(this.getContext(), this.getTheme());
    }
    
    @Override
    public void setupDialog(final Dialog dialog, final int n) {
        if (dialog instanceof q6) {
            final q6 q6 = (q6)dialog;
            if (n != 1 && n != 2) {
                if (n != 3) {
                    return;
                }
                dialog.getWindow().addFlags(24);
            }
            q6.supportRequestWindowFeature(1);
        }
        else {
            super.setupDialog(dialog, n);
        }
    }
}

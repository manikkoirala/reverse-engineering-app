// 
// Decompiled by Procyon v0.6.0
// 

public class zl
{
    public eb a;
    public eb b;
    
    public zl() {
        this.a = new eb();
        this.b = new eb();
    }
    
    public void a(final r51 r51) {
        if (r51 != null) {
            this.b.a(r51);
            return;
        }
        throw new IllegalArgumentException("Point cannot be null.");
    }
    
    public r51 b(final int n) {
        return (r51)this.b.e(n);
    }
    
    public int c() {
        return this.b.h();
    }
}

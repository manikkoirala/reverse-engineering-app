import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.IInterface;

// 
// Decompiled by Procyon v0.6.0
// 

public interface vd0 extends IInterface
{
    void a(final String[] p0);
    
    public abstract static class a extends Binder implements vd0
    {
        public a() {
            this.attachInterface((IInterface)this, "androidx.room.IMultiInstanceInvalidationCallback");
        }
        
        public static vd0 p(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof vd0) {
                return (vd0)queryLocalInterface;
            }
            return new vd0.a.a(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationCallback");
            }
            if (n == 1598968902) {
                parcel2.writeString("androidx.room.IMultiInstanceInvalidationCallback");
                return true;
            }
            if (n != 1) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            this.a(parcel.createStringArray());
            return true;
        }
        
        public static class a implements vd0
        {
            public IBinder a;
            
            public a(final IBinder a) {
                this.a = a;
            }
            
            @Override
            public void a(final String[] array) {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationCallback");
                    obtain.writeStringArray(array);
                    this.a.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.a;
            }
        }
    }
}

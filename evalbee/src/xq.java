import android.os.Looper;
import android.os.Handler;

// 
// Decompiled by Procyon v0.6.0
// 

public class xq implements ag1
{
    public final Handler a;
    
    public xq() {
        this.a = kc0.a(Looper.getMainLooper());
    }
    
    @Override
    public void a(final Runnable runnable) {
        this.a.removeCallbacks(runnable);
    }
    
    @Override
    public void b(final long n, final Runnable runnable) {
        this.a.postDelayed(runnable, n);
    }
}

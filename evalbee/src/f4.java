import java.util.Iterator;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class f4 implements Comparable
{
    public static final a d;
    public static final f4 e;
    public final List a;
    public final int b;
    public final int c;
    
    static {
        d = new a(null);
        e = new f4(nh.g(), Integer.MAX_VALUE, Integer.MAX_VALUE);
    }
    
    public f4(final List a, final int b, final int c) {
        fg0.e((Object)a, "matches");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public int a(final f4 f4) {
        fg0.e((Object)f4, "other");
        final int f5 = fg0.f(this.c, f4.c);
        if (f5 != 0) {
            return f5;
        }
        return fg0.f(this.b, f4.b);
    }
    
    public static final class a
    {
        public final f4 a(final List list) {
            fg0.e((Object)list, "matches");
            final Iterable iterable = list;
            final Iterator iterator = iterable.iterator();
            final int n = 0;
            int n2 = 0;
            while (iterator.hasNext()) {
                final e4 e4 = (e4)iterator.next();
                n2 += ((vf0)e4.b()).b() - ((vf0)e4.b()).a() + 1 - e4.a().size();
            }
            final Iterator iterator2 = iterable.iterator();
            if (!iterator2.hasNext()) {
                throw new NoSuchElementException();
            }
            int a = ((vf0)((e4)iterator2.next()).b()).a();
            while (iterator2.hasNext()) {
                final int a2 = ((vf0)((e4)iterator2.next()).b()).a();
                if (a > a2) {
                    a = a2;
                }
            }
            final Iterator iterator3 = iterable.iterator();
            if (iterator3.hasNext()) {
                int b = ((vf0)((e4)iterator3.next()).b()).b();
                while (iterator3.hasNext()) {
                    final int b2 = ((vf0)((e4)iterator3.next()).b()).b();
                    if (b < b2) {
                        b = b2;
                    }
                }
                final xf0 xf0 = new xf0(a, b);
                int n3;
                if (xf0 instanceof Collection && ((Collection)xf0).isEmpty()) {
                    n3 = n;
                }
                else {
                    final Iterator iterator4 = ((Iterable)xf0).iterator();
                    n3 = 0;
                Label_0289:
                    while (iterator4.hasNext()) {
                        final int b3 = ((sf0)iterator4).b();
                        final Iterator iterator5 = iterable.iterator();
                        int n4 = 0;
                        while (true) {
                            while (iterator5.hasNext()) {
                                int n5 = n4;
                                if (((e4)iterator5.next()).b().l(b3)) {
                                    n5 = n4 + 1;
                                }
                                if ((n4 = n5) > 1) {
                                    final boolean b4 = true;
                                    if (!b4) {
                                        continue Label_0289;
                                    }
                                    final int n6 = n3 + 1;
                                    if ((n3 = n6) < 0) {
                                        nh.m();
                                        n3 = n6;
                                        continue Label_0289;
                                    }
                                    continue Label_0289;
                                }
                            }
                            final boolean b4 = false;
                            continue;
                        }
                    }
                }
                return new f4(list, n2, n3);
            }
            throw new NoSuchElementException();
        }
    }
}

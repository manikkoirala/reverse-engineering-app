import android.graphics.Path;
import java.util.Map;
import android.animation.PropertyValuesHolder;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.animation.TypeEvaluator;
import android.animation.ObjectAnimator;
import android.animation.Animator$AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.Animator;
import android.view.ViewGroup;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;
import android.graphics.PointF;
import android.util.Property;

// 
// Decompiled by Procyon v0.6.0
// 

public class uf extends qy1
{
    public static final String[] d;
    public static final Property e;
    public static final Property f;
    public static final Property g;
    public static final Property h;
    public static final Property i;
    public static final Property j;
    public static uc1 k;
    public int[] a;
    public boolean b;
    public boolean c;
    
    static {
        d = new String[] { "android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY" };
        e = new Property(PointF.class, "boundsOrigin") {
            public Rect a = new Rect();
            
            public PointF a(final Drawable drawable) {
                drawable.copyBounds(this.a);
                final Rect a = this.a;
                return new PointF((float)a.left, (float)a.top);
            }
            
            public void b(final Drawable drawable, final PointF pointF) {
                drawable.copyBounds(this.a);
                this.a.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
                drawable.setBounds(this.a);
            }
        };
        f = new Property(PointF.class, "topLeft") {
            public PointF a(final k k) {
                return null;
            }
            
            public void b(final k k, final PointF pointF) {
                k.c(pointF);
            }
        };
        g = new Property(PointF.class, "bottomRight") {
            public PointF a(final k k) {
                return null;
            }
            
            public void b(final k k, final PointF pointF) {
                k.a(pointF);
            }
        };
        h = new Property(PointF.class, "bottomRight") {
            public PointF a(final View view) {
                return null;
            }
            
            public void b(final View view, final PointF pointF) {
                t42.f(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
            }
        };
        i = new Property(PointF.class, "topLeft") {
            public PointF a(final View view) {
                return null;
            }
            
            public void b(final View view, final PointF pointF) {
                t42.f(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
            }
        };
        j = new Property(PointF.class, "position") {
            public PointF a(final View view) {
                return null;
            }
            
            public void b(final View view, final PointF pointF) {
                final int round = Math.round(pointF.x);
                final int round2 = Math.round(pointF.y);
                t42.f(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
            }
        };
        uf.k = new uc1();
    }
    
    public uf() {
        this.a = new int[2];
        this.b = false;
        this.c = false;
    }
    
    @Override
    public void captureEndValues(final xy1 xy1) {
        this.captureValues(xy1);
    }
    
    @Override
    public void captureStartValues(final xy1 xy1) {
        this.captureValues(xy1);
    }
    
    public final void captureValues(final xy1 xy1) {
        final View b = xy1.b;
        if (o32.U(b) || b.getWidth() != 0 || b.getHeight() != 0) {
            xy1.a.put("android:changeBounds:bounds", new Rect(b.getLeft(), b.getTop(), b.getRight(), b.getBottom()));
            xy1.a.put("android:changeBounds:parent", xy1.b.getParent());
            if (this.c) {
                xy1.b.getLocationInWindow(this.a);
                xy1.a.put("android:changeBounds:windowX", this.a[0]);
                xy1.a.put("android:changeBounds:windowY", this.a[1]);
            }
            if (this.b) {
                xy1.a.put("android:changeBounds:clip", o32.u(b));
            }
        }
    }
    
    @Override
    public Animator createAnimator(final ViewGroup viewGroup, final xy1 xy1, final xy1 xy2) {
        if (xy1 == null || xy2 == null) {
            return null;
        }
        final Map a = xy1.a;
        final Map a2 = xy2.a;
        final ViewGroup viewGroup2 = a.get("android:changeBounds:parent");
        final ViewGroup viewGroup3 = a2.get("android:changeBounds:parent");
        if (viewGroup2 != null && viewGroup3 != null) {
            final View b = xy2.b;
            if (this.u((View)viewGroup2, (View)viewGroup3)) {
                final Rect rect = xy1.a.get("android:changeBounds:bounds");
                final Rect rect2 = xy2.a.get("android:changeBounds:bounds");
                final int left = rect.left;
                final int left2 = rect2.left;
                final int top = rect.top;
                final int top2 = rect2.top;
                final int right = rect.right;
                final int right2 = rect2.right;
                final int bottom = rect.bottom;
                final int bottom2 = rect2.bottom;
                final int a3 = right - left;
                final int a4 = bottom - top;
                final int b2 = right2 - left2;
                final int b3 = bottom2 - top2;
                Rect rect3 = xy1.a.get("android:changeBounds:clip");
                final Rect rect4 = xy2.a.get("android:changeBounds:clip");
                int n2 = 0;
                Label_0299: {
                    if ((a3 != 0 && a4 != 0) || (b2 != 0 && b3 != 0)) {
                        int n;
                        if (left == left2 && top == top2) {
                            n = 0;
                        }
                        else {
                            n = 1;
                        }
                        if (right == right2) {
                            n2 = n;
                            if (bottom == bottom2) {
                                break Label_0299;
                            }
                        }
                        n2 = n + 1;
                    }
                    else {
                        n2 = 0;
                    }
                }
                int n3 = 0;
                Label_0335: {
                    if (rect3 == null || rect3.equals((Object)rect4)) {
                        n3 = n2;
                        if (rect3 != null) {
                            break Label_0335;
                        }
                        n3 = n2;
                        if (rect4 == null) {
                            break Label_0335;
                        }
                    }
                    n3 = n2 + 1;
                }
                if (n3 > 0) {
                    Object o = null;
                    Label_0795: {
                        if (!this.b) {
                            t42.f(b, left, top, right, bottom);
                            Path path;
                            Property property;
                            if (n3 == 2) {
                                if (a3 != b2 || a4 != b3) {
                                    final k k = new k(b);
                                    final ObjectAnimator a5 = s01.a(k, uf.f, this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2));
                                    final ObjectAnimator a6 = s01.a(k, uf.g, this.getPathMotion().getPath((float)right, (float)bottom, (float)right2, (float)bottom2));
                                    o = new AnimatorSet();
                                    ((AnimatorSet)o).playTogether(new Animator[] { (Animator)a5, (Animator)a6 });
                                    ((Animator)o).addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, k) {
                                        public final k a;
                                        public final uf b;
                                        private k mViewBounds = k;
                                    });
                                    break Label_0795;
                                }
                                path = this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2);
                                property = uf.j;
                            }
                            else if (left == left2 && top == top2) {
                                path = this.getPathMotion().getPath((float)right, (float)bottom, (float)right2, (float)bottom2);
                                property = uf.h;
                            }
                            else {
                                path = this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2);
                                property = uf.i;
                            }
                            o = s01.a(b, property, path);
                        }
                        else {
                            t42.f(b, left, top, Math.max(a3, b2) + left, Math.max(a4, b3) + top);
                            Object a7;
                            if (left == left2 && top == top2) {
                                a7 = null;
                            }
                            else {
                                a7 = s01.a(b, uf.j, this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2));
                            }
                            if (rect3 == null) {
                                rect3 = new Rect(0, 0, a3, a4);
                            }
                            Rect rect5;
                            if (rect4 == null) {
                                rect5 = new Rect(0, 0, b2, b3);
                            }
                            else {
                                rect5 = rect4;
                            }
                            Object ofObject;
                            if (!rect3.equals((Object)rect5)) {
                                o32.x0(b, rect3);
                                ofObject = ObjectAnimator.ofObject((Object)b, "clipBounds", (TypeEvaluator)uf.k, new Object[] { rect3, rect5 });
                                ((Animator)ofObject).addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, b, rect4, left2, top2, right2, bottom2) {
                                    public boolean a;
                                    public final View b;
                                    public final Rect c;
                                    public final int d;
                                    public final int e;
                                    public final int f;
                                    public final int g;
                                    public final uf h;
                                    
                                    public void onAnimationCancel(final Animator animator) {
                                        this.a = true;
                                    }
                                    
                                    public void onAnimationEnd(final Animator animator) {
                                        if (!this.a) {
                                            o32.x0(this.b, this.c);
                                            t42.f(this.b, this.d, this.e, this.f, this.g);
                                        }
                                    }
                                });
                            }
                            else {
                                ofObject = null;
                            }
                            o = wy1.c((Animator)a7, (Animator)ofObject);
                        }
                    }
                    if (b.getParent() instanceof ViewGroup) {
                        final ViewGroup viewGroup4 = (ViewGroup)b.getParent();
                        w32.c(viewGroup4, true);
                        this.addListener((g)new ry1(this, viewGroup4) {
                            public boolean a = false;
                            public final ViewGroup b;
                            public final uf c;
                            
                            @Override
                            public void onTransitionCancel(final qy1 qy1) {
                                w32.c(this.b, false);
                                this.a = true;
                            }
                            
                            @Override
                            public void onTransitionEnd(final qy1 qy1) {
                                if (!this.a) {
                                    w32.c(this.b, false);
                                }
                                qy1.removeListener((g)this);
                            }
                            
                            @Override
                            public void onTransitionPause(final qy1 qy1) {
                                w32.c(this.b, false);
                            }
                            
                            @Override
                            public void onTransitionResume(final qy1 qy1) {
                                w32.c(this.b, true);
                            }
                        });
                    }
                    return (Animator)o;
                }
            }
            else {
                final int intValue = xy1.a.get("android:changeBounds:windowX");
                final int intValue2 = xy1.a.get("android:changeBounds:windowY");
                final int intValue3 = xy2.a.get("android:changeBounds:windowX");
                final int intValue4 = xy2.a.get("android:changeBounds:windowY");
                if (intValue != intValue3 || intValue2 != intValue4) {
                    ((View)viewGroup).getLocationInWindow(this.a);
                    final Bitmap bitmap = Bitmap.createBitmap(b.getWidth(), b.getHeight(), Bitmap$Config.ARGB_8888);
                    b.draw(new Canvas(bitmap));
                    final BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
                    final float c = t42.c(b);
                    t42.g(b, 0.0f);
                    t42.b((View)viewGroup).add((Drawable)bitmapDrawable);
                    final g31 pathMotion = this.getPathMotion();
                    final int[] a8 = this.a;
                    final int n4 = a8[0];
                    final float n5 = (float)(intValue - n4);
                    final int n6 = a8[1];
                    final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder((Object)bitmapDrawable, new PropertyValuesHolder[] { h91.a(uf.e, pathMotion.getPath(n5, (float)(intValue2 - n6), (float)(intValue3 - n4), (float)(intValue4 - n6))) });
                    ((Animator)ofPropertyValuesHolder).addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, viewGroup, bitmapDrawable, b, c) {
                        public final ViewGroup a;
                        public final BitmapDrawable b;
                        public final View c;
                        public final float d;
                        public final uf e;
                        
                        public void onAnimationEnd(final Animator animator) {
                            t42.b((View)this.a).remove((Drawable)this.b);
                            t42.g(this.c, this.d);
                        }
                    });
                    return (Animator)ofPropertyValuesHolder;
                }
            }
            return null;
        }
        return null;
    }
    
    @Override
    public String[] getTransitionProperties() {
        return uf.d;
    }
    
    public final boolean u(final View view, final View view2) {
        final boolean c = this.c;
        boolean b = true;
        if (c) {
            final xy1 matchedTransitionValues = this.getMatchedTransitionValues(view, true);
            b = (((matchedTransitionValues != null) ? (view2 == matchedTransitionValues.b) : (view == view2)) && b);
        }
        return b;
    }
    
    public static class k
    {
        public int a;
        public int b;
        public int c;
        public int d;
        public View e;
        public int f;
        public int g;
        
        public k(final View e) {
            this.e = e;
        }
        
        public void a(final PointF pointF) {
            this.c = Math.round(pointF.x);
            this.d = Math.round(pointF.y);
            final int g = this.g + 1;
            this.g = g;
            if (this.f == g) {
                this.b();
            }
        }
        
        public final void b() {
            t42.f(this.e, this.a, this.b, this.c, this.d);
            this.f = 0;
            this.g = 0;
        }
        
        public void c(final PointF pointF) {
            this.a = Math.round(pointF.x);
            this.b = Math.round(pointF.y);
            final int f = this.f + 1;
            this.f = f;
            if (f == this.g) {
                this.b();
            }
        }
    }
}

import android.app.Notification;

// 
// Decompiled by Procyon v0.6.0
// 

public final class p70
{
    public final int a;
    public final int b;
    public final Notification c;
    
    public p70(final int a, final Notification c, final int b) {
        this.a = a;
        this.c = c;
        this.b = b;
    }
    
    public int a() {
        return this.b;
    }
    
    public Notification b() {
        return this.c;
    }
    
    public int c() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && p70.class == o.getClass()) {
            final p70 p = (p70)o;
            return this.a == p.a && this.b == p.b && this.c.equals(p.c);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (this.a * 31 + this.b) * 31 + this.c.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ForegroundInfo{");
        sb.append("mNotificationId=");
        sb.append(this.a);
        sb.append(", mForegroundServiceType=");
        sb.append(this.b);
        sb.append(", mNotification=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}

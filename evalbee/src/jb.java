import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class jb implements Comparable
{
    public final List a;
    
    public jb(final List a) {
        this.a = a;
    }
    
    public jb a(final jb jb) {
        final ArrayList list = new ArrayList(this.a);
        list.addAll(jb.a);
        return this.f(list);
    }
    
    public jb c(final String s) {
        final ArrayList list = new ArrayList(this.a);
        list.add(s);
        return this.f(list);
    }
    
    public abstract String d();
    
    public int e(final jb jb) {
        final int l = this.l();
        final int i = jb.l();
        for (int n = 0; n < l && n < i; ++n) {
            final int compareTo = this.h(n).compareTo(jb.h(n));
            if (compareTo != 0) {
                return compareTo;
            }
        }
        return o22.k(l, i);
    }
    
    @Override
    public final boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof jb) || this.e((jb)o) != 0) {
            b = false;
        }
        return b;
    }
    
    public abstract jb f(final List p0);
    
    public String g() {
        return this.a.get(this.l() - 1);
    }
    
    public String h(final int n) {
        return this.a.get(n);
    }
    
    @Override
    public int hashCode() {
        return (this.getClass().hashCode() + 37) * 37 + this.a.hashCode();
    }
    
    public boolean j() {
        return this.l() == 0;
    }
    
    public boolean k(final jb jb) {
        if (this.l() > jb.l()) {
            return false;
        }
        for (int i = 0; i < this.l(); ++i) {
            if (!this.h(i).equals(jb.h(i))) {
                return false;
            }
        }
        return true;
    }
    
    public int l() {
        return this.a.size();
    }
    
    public jb m(final int i) {
        final int l = this.l();
        g9.d(l >= i, "Can't call popFirst with count > length() (%d > %d)", i, l);
        return this.f(this.a.subList(i, l));
    }
    
    public jb n() {
        return this.f(this.a.subList(0, this.l() - 1));
    }
    
    @Override
    public String toString() {
        return this.d();
    }
}

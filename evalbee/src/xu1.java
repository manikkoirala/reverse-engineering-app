import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ResultDataV1;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamDataV1;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class xu1
{
    public static void a(final Context context) {
        final Iterator<TemplateDataJsonModel> iterator = TemplateRepository.getInstance(context).getTemplatesJson().iterator();
        while (iterator.hasNext()) {
            b(context, iterator.next().getExamId());
        }
    }
    
    public static void b(final Context context, final ExamId examId) {
        final TemplateRepository instance = TemplateRepository.getInstance(context);
        final TemplateDataJsonModel templateJson = instance.getTemplateJson(examId);
        if (templateJson != null && templateJson.getFolderPath() != null) {
            ResultRepository.getInstance(context).deleteAllSheetImageForExam(examId, templateJson.getFolderPath());
        }
        ResultRepository.getInstance(context).deleteAllResultForExam(examId);
        instance.deleteTemplateJson(examId);
    }
    
    public static boolean c(final Context context, final ExamId examId, final ExamDataV1 examDataV1) {
        for (final ResultDataV1 resultDataV1 : examDataV1.getResults()) {
            final ResultDataJsonModel resultDataJsonModel = new ResultDataJsonModel(examId.getExamName(), resultDataV1.getRollno(), examId.getClassName(), examId.getExamDate(), resultDataV1.getResultItem2(), false, false);
            final ResultRepository instance = ResultRepository.getInstance(context);
            instance.deleteStudentResult(examId, resultDataJsonModel.getRollNo());
            instance.deleteSheetImages(examId, resultDataJsonModel.getRollNo());
            if (instance.saveOrUpdateResultJsonAsNotSynced(resultDataJsonModel)) {
                FirebaseAnalytics.getInstance(context).a("ResultImportSave", null);
            }
        }
        return true;
    }
    
    public static boolean d(final Context context, final ExamId examId, final SheetTemplate2 sheetTemplate2, final String s) {
        final TemplateRepository instance = TemplateRepository.getInstance(context);
        final TemplateDataJsonModel templateJson = instance.getTemplateJson(examId);
        if (templateJson != null && templateJson.getFolderPath() != null) {
            ResultRepository.getInstance(context).deleteAllSheetImageForExam(examId, templateJson.getFolderPath());
        }
        ResultRepository.getInstance(context).deleteAllResultForExam(examId);
        instance.deleteTemplateJson(examId);
        sheetTemplate2.setName(examId.getExamName());
        return instance.saveOrUpdateTemplateJson(new TemplateDataJsonModel(examId.getExamName(), examId.getClassName(), examId.getExamDate(), sheetTemplate2, b10.a(context, examId.getExamDate()), s));
    }
}

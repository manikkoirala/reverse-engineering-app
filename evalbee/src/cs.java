import java.util.List;
import java.util.Collection;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public class cs implements ScheduledExecutorService
{
    public final ExecutorService a;
    public final ScheduledExecutorService b;
    
    public cs(final ExecutorService a, final ScheduledExecutorService b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public boolean awaitTermination(final long n, final TimeUnit timeUnit) {
        return this.a.awaitTermination(n, timeUnit);
    }
    
    @Override
    public void execute(final Runnable runnable) {
        this.a.execute(runnable);
    }
    
    @Override
    public List invokeAll(final Collection collection) {
        return this.a.invokeAll((Collection<? extends Callable<Object>>)collection);
    }
    
    @Override
    public List invokeAll(final Collection collection, final long n, final TimeUnit timeUnit) {
        return this.a.invokeAll((Collection<? extends Callable<Object>>)collection, n, timeUnit);
    }
    
    @Override
    public Object invokeAny(final Collection collection) {
        return this.a.invokeAny((Collection<? extends Callable<Object>>)collection);
    }
    
    @Override
    public Object invokeAny(final Collection collection, final long n, final TimeUnit timeUnit) {
        return this.a.invokeAny((Collection<? extends Callable<Object>>)collection, n, timeUnit);
    }
    
    @Override
    public boolean isShutdown() {
        return this.a.isShutdown();
    }
    
    @Override
    public boolean isTerminated() {
        return this.a.isTerminated();
    }
    
    @Override
    public ScheduledFuture schedule(final Runnable runnable, final long n, final TimeUnit timeUnit) {
        return new ds((ds.c)new tr(this, runnable, n, timeUnit));
    }
    
    @Override
    public ScheduledFuture schedule(final Callable callable, final long n, final TimeUnit timeUnit) {
        return new ds((ds.c)new qr(this, callable, n, timeUnit));
    }
    
    @Override
    public ScheduledFuture scheduleAtFixedRate(final Runnable runnable, final long n, final long n2, final TimeUnit timeUnit) {
        return new ds((ds.c)new vr(this, runnable, n, n2, timeUnit));
    }
    
    @Override
    public ScheduledFuture scheduleWithFixedDelay(final Runnable runnable, final long n, final long n2, final TimeUnit timeUnit) {
        return new ds((ds.c)new ur(this, runnable, n, n2, timeUnit));
    }
    
    @Override
    public void shutdown() {
        throw new UnsupportedOperationException("Shutting down is not allowed.");
    }
    
    @Override
    public List shutdownNow() {
        throw new UnsupportedOperationException("Shutting down is not allowed.");
    }
    
    @Override
    public Future submit(final Runnable runnable) {
        return this.a.submit(runnable);
    }
    
    @Override
    public Future submit(final Runnable runnable, final Object o) {
        return this.a.submit(runnable, o);
    }
    
    @Override
    public Future submit(final Callable callable) {
        return this.a.submit((Callable<Object>)callable);
    }
}

import androidx.work.a;
import java.util.Iterator;
import java.util.List;
import java.lang.reflect.Method;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.app.ActivityManager;
import android.os.Process;
import androidx.work.WorkManager;
import android.os.Build$VERSION;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class m81
{
    public static final String a;
    
    static {
        final String i = xl0.i("ProcessUtils");
        fg0.d((Object)i, "tagWithPrefix(\"ProcessUtils\")");
        a = i;
    }
    
    public static final String a(final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return m5.a.a();
        }
        final String s = null;
        try {
            final Method declaredMethod = Class.forName("android.app.ActivityThread", false, WorkManager.class.getClassLoader()).getDeclaredMethod("currentProcessName", (Class<?>[])new Class[0]);
            declaredMethod.setAccessible(true);
            final Object invoke = declaredMethod.invoke(null, new Object[0]);
            fg0.b(invoke);
            if (invoke instanceof String) {
                return (String)invoke;
            }
        }
        finally {
            final Throwable t;
            xl0.e().b(m81.a, "Unable to check ActivityThread for processName", t);
        }
        final int myPid = Process.myPid();
        final Object systemService = context.getSystemService("activity");
        fg0.c(systemService, "null cannot be cast to non-null type android.app.ActivityManager");
        final List runningAppProcesses = ((ActivityManager)systemService).getRunningAppProcesses();
        String processName = s;
        if (runningAppProcesses != null) {
            while (true) {
                for (final Object next : runningAppProcesses) {
                    if (((ActivityManager$RunningAppProcessInfo)next).pid == myPid) {
                        final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo = (ActivityManager$RunningAppProcessInfo)next;
                        processName = s;
                        if (activityManager$RunningAppProcessInfo != null) {
                            processName = activityManager$RunningAppProcessInfo.processName;
                            return processName;
                        }
                        return processName;
                    }
                }
                Object next = null;
                continue;
            }
        }
        return processName;
    }
    
    public static final boolean b(final Context context, final a a) {
        fg0.e((Object)context, "context");
        fg0.e((Object)a, "configuration");
        final String a2 = a(context);
        final String c = a.c();
        String s;
        if (c != null && c.length() != 0) {
            s = a.c();
        }
        else {
            s = context.getApplicationInfo().processName;
        }
        return fg0.a((Object)a2, (Object)s);
    }
}

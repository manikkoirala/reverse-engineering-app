import com.google.firebase.firestore.core.UserData$Source;

// 
// Decompiled by Procyon v0.6.0
// 

public class y12
{
    public final x12 a;
    public final s00 b;
    public final boolean c;
    
    public y12(final x12 a, final s00 b, final boolean c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public void a(final s00 s00) {
        this.a.b(s00);
    }
    
    public void b(final s00 s00, final oy1 oy1) {
        this.a.c(s00, oy1);
    }
    
    public y12 c(final int n) {
        return new y12(this.a, null, true);
    }
    
    public y12 d(s00 s00) {
        final s00 b = this.b;
        if (b == null) {
            s00 = null;
        }
        else {
            s00 = (s00)b.a(s00);
        }
        final y12 y12 = new y12(this.a, s00, false);
        y12.k();
        return y12;
    }
    
    public y12 e(final String s) {
        final s00 b = this.b;
        s00 s2;
        if (b == null) {
            s2 = null;
        }
        else {
            s2 = (s00)b.c(s);
        }
        final y12 y12 = new y12(this.a, s2, false);
        y12.l(s);
        return y12;
    }
    
    public RuntimeException f(final String str) {
        final s00 b = this.b;
        String string;
        if (b != null && !b.j()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" (found in field ");
            sb.append(this.b.toString());
            sb.append(")");
            string = sb.toString();
        }
        else {
            string = "";
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Invalid data. ");
        sb2.append(str);
        sb2.append(string);
        return new IllegalArgumentException(sb2.toString());
    }
    
    public UserData$Source g() {
        return x12.a(this.a);
    }
    
    public s00 h() {
        return this.b;
    }
    
    public boolean i() {
        return this.c;
    }
    
    public boolean j() {
        final int n = w12.a[x12.a(this.a).ordinal()];
        if (n == 1 || n == 2 || n == 3) {
            return true;
        }
        if (n != 4 && n != 5) {
            throw g9.a("Unexpected case for UserDataSource: %s", x12.a(this.a).name());
        }
        return false;
    }
    
    public final void k() {
        if (this.b == null) {
            return;
        }
        for (int i = 0; i < this.b.l(); ++i) {
            this.l(this.b.h(i));
        }
    }
    
    public final void l(final String s) {
        if (s.isEmpty()) {
            throw this.f("Document fields must not be empty");
        }
        if (this.j() && s.startsWith("__") && s.endsWith("__")) {
            throw this.f("Document fields cannot begin and end with \"__\"");
        }
    }
}

import com.google.firebase.firestore.util.Logger;
import com.google.android.gms.tasks.Continuation;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Task;

// 
// Decompiled by Procyon v0.6.0
// 

public final class x10 extends eo
{
    public lk0 a;
    public dg0 b;
    public boolean c;
    public final t5 d;
    
    public x10(final jr jr) {
        this.d = new u10(this);
        jr.a((jr.a)new v10(this));
    }
    
    @Override
    public Task a() {
        synchronized (this) {
            final dg0 b = this.b;
            if (b == null) {
                return Tasks.forException((Exception)new FirebaseApiNotAvailableException("AppCheck is not available"));
            }
            final Task a = b.a(this.c);
            this.c = false;
            return a.continueWithTask(wy.b, (Continuation)new w10());
        }
    }
    
    @Override
    public void b() {
        synchronized (this) {
            this.c = true;
        }
    }
    
    @Override
    public void c(final lk0 a) {
        synchronized (this) {
            this.a = a;
        }
    }
    
    public final void j(final u5 u5) {
        synchronized (this) {
            if (u5.a() != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error getting App Check token; using placeholder token instead. Error: ");
                sb.append(u5.a());
                Logger.d("FirebaseAppCheckTokenProvider", sb.toString(), new Object[0]);
            }
            final lk0 a = this.a;
            if (a != null) {
                a.a(u5.b());
            }
        }
    }
}

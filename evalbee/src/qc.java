import android.os.BaseBundle;
import org.json.JSONException;
import java.util.Iterator;
import org.json.JSONObject;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public class qc implements n4, sc
{
    public rc a;
    
    public static String c(final String s, final Bundle bundle) {
        final JSONObject jsonObject = new JSONObject();
        final JSONObject jsonObject2 = new JSONObject();
        for (final String s2 : ((BaseBundle)bundle).keySet()) {
            jsonObject2.put(s2, ((BaseBundle)bundle).get(s2));
        }
        jsonObject.put("name", (Object)s);
        jsonObject.put("parameters", (Object)jsonObject2);
        return jsonObject.toString();
    }
    
    @Override
    public void a(final rc a) {
        this.a = a;
        zl0.f().b("Registered Firebase Analytics event receiver for breadcrumbs");
    }
    
    @Override
    public void b(final String s, final Bundle bundle) {
        final rc a = this.a;
        if (a != null) {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("$A$:");
                sb.append(c(s, bundle));
                a.a(sb.toString());
            }
            catch (final JSONException ex) {
                zl0.f().k("Unable to serialize Firebase Analytics event to breadcrumb.");
            }
        }
    }
}

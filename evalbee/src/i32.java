import com.android.volley.Request;
import java.util.HashMap;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import com.android.volley.d;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseTransaction;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class i32
{
    public ee1 a;
    public zg b;
    public Context c;
    public PurchaseTransaction d;
    public String e;
    
    public i32(final PurchaseTransaction d, final Context c, final zg b) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.a = new n52(c, a91.u()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/ums-api/android/verifyPurchase526");
        this.e = sb.toString();
        this.e();
    }
    
    public static /* synthetic */ Context c(final i32 i32) {
        return i32.c;
    }
    
    public final void d(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public void e() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final i32 a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.f(((ya0)task.getResult()).c());
                }
                else {
                    this.a.d(false, 400, null);
                }
            }
        });
    }
    
    public final void f(final String s) {
        final hr1 hr1 = new hr1(this, 1, this.e, new d.b(this) {
            public final i32 a;
            
            public void b(final String s) {
                try {
                    final PurchaseAccount purchaseAccount = (PurchaseAccount)new gc0().j(s, PurchaseAccount.class);
                    if (purchaseAccount != null) {
                        i32.c(this.a).getApplicationContext().getSharedPreferences("MyPref", 0).edit().putString(ok.y, b.i(s, FirebaseAuth.getInstance().e().O())).commit();
                    }
                    this.a.d(true, 200, purchaseAccount);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.d(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final i32 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.d(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final i32 x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(this.x.d).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
}

import android.os.Build$VERSION;
import android.text.Spannable;
import java.util.concurrent.atomic.AtomicInteger;
import android.text.SpanWatcher;
import android.text.TextWatcher;
import java.lang.reflect.Array;
import android.text.Editable;
import java.util.ArrayList;
import java.util.List;
import android.text.SpannableStringBuilder;

// 
// Decompiled by Procyon v0.6.0
// 

public final class vo1 extends SpannableStringBuilder
{
    public final Class a;
    public final List b;
    
    public vo1(final Class a, final CharSequence charSequence) {
        super(charSequence);
        this.b = new ArrayList();
        l71.h(a, "watcherClass cannot be null");
        this.a = a;
    }
    
    public vo1(final Class a, final CharSequence charSequence, final int n, final int n2) {
        super(charSequence, n, n2);
        this.b = new ArrayList();
        l71.h(a, "watcherClass cannot be null");
        this.a = a;
    }
    
    public static vo1 c(final Class clazz, final CharSequence charSequence) {
        return new vo1(clazz, charSequence);
    }
    
    public void a() {
        this.b();
    }
    
    public SpannableStringBuilder append(final char c) {
        super.append(c);
        return this;
    }
    
    public SpannableStringBuilder append(final CharSequence charSequence) {
        super.append(charSequence);
        return this;
    }
    
    public SpannableStringBuilder append(final CharSequence charSequence, final int n, final int n2) {
        super.append(charSequence, n, n2);
        return this;
    }
    
    public SpannableStringBuilder append(final CharSequence charSequence, final Object o, final int n) {
        super.append(charSequence, o, n);
        return this;
    }
    
    public final void b() {
        for (int i = 0; i < this.b.size(); ++i) {
            ((a)this.b.get(i)).a();
        }
    }
    
    public void d() {
        this.i();
        this.e();
    }
    
    public SpannableStringBuilder delete(final int n, final int n2) {
        super.delete(n, n2);
        return this;
    }
    
    public final void e() {
        for (int i = 0; i < this.b.size(); ++i) {
            ((a)this.b.get(i)).onTextChanged((CharSequence)this, 0, this.length(), this.length());
        }
    }
    
    public final a f(final Object o) {
        for (int i = 0; i < this.b.size(); ++i) {
            final a a = this.b.get(i);
            if (a.a == o) {
                return a;
            }
        }
        return null;
    }
    
    public final boolean g(final Class clazz) {
        return this.a == clazz;
    }
    
    public int getSpanEnd(final Object o) {
        Object o2 = o;
        if (this.h(o)) {
            final a f = this.f(o);
            o2 = o;
            if (f != null) {
                o2 = f;
            }
        }
        return super.getSpanEnd(o2);
    }
    
    public int getSpanFlags(final Object o) {
        Object o2 = o;
        if (this.h(o)) {
            final a f = this.f(o);
            o2 = o;
            if (f != null) {
                o2 = f;
            }
        }
        return super.getSpanFlags(o2);
    }
    
    public int getSpanStart(final Object o) {
        Object o2 = o;
        if (this.h(o)) {
            final a f = this.f(o);
            o2 = o;
            if (f != null) {
                o2 = f;
            }
        }
        return super.getSpanStart(o2);
    }
    
    public Object[] getSpans(int i, final int n, final Class componentType) {
        if (this.g(componentType)) {
            final a[] array = (a[])super.getSpans(i, n, (Class)a.class);
            final Object[] array2 = (Object[])Array.newInstance(componentType, array.length);
            for (i = 0; i < array.length; ++i) {
                array2[i] = array[i].a;
            }
            return array2;
        }
        return super.getSpans(i, n, componentType);
    }
    
    public final boolean h(final Object o) {
        return o != null && this.g(o.getClass());
    }
    
    public final void i() {
        for (int i = 0; i < this.b.size(); ++i) {
            ((a)this.b.get(i)).c();
        }
    }
    
    public SpannableStringBuilder insert(final int n, final CharSequence charSequence) {
        super.insert(n, charSequence);
        return this;
    }
    
    public SpannableStringBuilder insert(final int n, final CharSequence charSequence, final int n2, final int n3) {
        super.insert(n, charSequence, n2, n3);
        return this;
    }
    
    public int nextSpanTransition(final int n, final int n2, final Class clazz) {
        if (clazz != null) {
            final Class<a> clazz2 = clazz;
            if (!this.g(clazz)) {
                return super.nextSpanTransition(n, n2, (Class)clazz2);
            }
        }
        final Class<a> clazz2 = a.class;
        return super.nextSpanTransition(n, n2, (Class)clazz2);
    }
    
    public void removeSpan(Object o) {
        a a;
        if (this.h(o)) {
            final a f = this.f(o);
            if ((a = f) != null) {
                o = f;
                a = f;
            }
        }
        else {
            a = null;
        }
        super.removeSpan(o);
        if (a != null) {
            this.b.remove(a);
        }
    }
    
    public SpannableStringBuilder replace(final int n, final int n2, final CharSequence charSequence) {
        this.b();
        super.replace(n, n2, charSequence);
        this.i();
        return this;
    }
    
    public SpannableStringBuilder replace(final int n, final int n2, final CharSequence charSequence, final int n3, final int n4) {
        this.b();
        super.replace(n, n2, charSequence, n3, n4);
        this.i();
        return this;
    }
    
    public void setSpan(final Object o, final int n, final int n2, final int n3) {
        Object o2 = o;
        if (this.h(o)) {
            o2 = new a(o);
            this.b.add(o2);
        }
        super.setSpan(o2, n, n2, n3);
    }
    
    public CharSequence subSequence(final int n, final int n2) {
        return (CharSequence)new vo1(this.a, (CharSequence)this, n, n2);
    }
    
    public static class a implements TextWatcher, SpanWatcher
    {
        public final Object a;
        public final AtomicInteger b;
        
        public a(final Object a) {
            this.b = new AtomicInteger(0);
            this.a = a;
        }
        
        public final void a() {
            this.b.incrementAndGet();
        }
        
        public void afterTextChanged(final Editable editable) {
            ((TextWatcher)this.a).afterTextChanged(editable);
        }
        
        public final boolean b(final Object o) {
            return o instanceof ow;
        }
        
        public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            ((TextWatcher)this.a).beforeTextChanged(charSequence, n, n2, n3);
        }
        
        public final void c() {
            this.b.decrementAndGet();
        }
        
        public void onSpanAdded(final Spannable spannable, final Object o, final int n, final int n2) {
            if (this.b.get() > 0 && this.b(o)) {
                return;
            }
            ((SpanWatcher)this.a).onSpanAdded(spannable, o, n, n2);
        }
        
        public void onSpanChanged(final Spannable spannable, final Object o, int n, final int n2, final int n3, final int n4) {
            if (this.b.get() > 0 && this.b(o)) {
                return;
            }
            int n5 = n;
            Label_0065: {
                if (Build$VERSION.SDK_INT < 28) {
                    int n6;
                    if ((n6 = n) > n2) {
                        n6 = 0;
                    }
                    n5 = n6;
                    if (n3 > n4) {
                        n = 0;
                        n5 = n6;
                        break Label_0065;
                    }
                }
                n = n3;
            }
            ((SpanWatcher)this.a).onSpanChanged(spannable, o, n5, n2, n, n4);
        }
        
        public void onSpanRemoved(final Spannable spannable, final Object o, final int n, final int n2) {
            if (this.b.get() > 0 && this.b(o)) {
                return;
            }
            ((SpanWatcher)this.a).onSpanRemoved(spannable, o, n, n2);
        }
        
        public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            ((TextWatcher)this.a).onTextChanged(charSequence, n, n2, n3);
        }
    }
}

import io.grpc.stub.AbstractStub;
import io.grpc.ConnectivityState;
import java.util.concurrent.Callable;
import io.grpc.android.AndroidChannelBuilder;
import java.util.concurrent.TimeUnit;
import io.grpc.ManagedChannelBuilder;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.android.gms.tasks.Continuation;
import com.google.firebase.firestore.util.Logger;
import io.grpc.Channel;
import com.google.android.gms.tasks.Tasks;
import io.grpc.MethodDescriptor;
import io.grpc.ManagedChannel;
import io.grpc.CallCredentials;
import android.content.Context;
import io.grpc.CallOptions;
import com.google.firebase.firestore.util.AsyncQueue;
import com.google.android.gms.tasks.Task;

// 
// Decompiled by Procyon v0.6.0
// 

public class ec0
{
    public static hs1 h;
    public Task a;
    public final AsyncQueue b;
    public CallOptions c;
    public AsyncQueue.b d;
    public final Context e;
    public final rp f;
    public final CallCredentials g;
    
    public ec0(final AsyncQueue b, final Context e, final rp f, final CallCredentials g) {
        this.b = b;
        this.e = e;
        this.f = f;
        this.g = g;
        this.k();
    }
    
    public final void h() {
        if (this.d != null) {
            Logger.a("GrpcCallProvider", "Clearing the connectivityAttemptTimer", new Object[0]);
            this.d.c();
            this.d = null;
        }
    }
    
    public Task i(final MethodDescriptor methodDescriptor) {
        return this.a.continueWithTask(this.b.j(), (Continuation)new bc0(this, methodDescriptor));
    }
    
    public final ManagedChannel j(final Context context, final rp rp) {
        Label_0031: {
            try {
                ProviderInstaller.installIfNeeded(context);
                break Label_0031;
            }
            catch (final IllegalStateException ex) {}
            catch (final GooglePlayServicesRepairableException ex) {}
            catch (final GooglePlayServicesNotAvailableException ex2) {}
            final IllegalStateException ex;
            Logger.d("GrpcCallProvider", "Failed to update ssl context: %s", ex);
        }
        final hs1 h = ec0.h;
        ManagedChannelBuilder managedChannelBuilder;
        if (h != null) {
            managedChannelBuilder = (ManagedChannelBuilder)h.get();
        }
        else {
            final ManagedChannelBuilder forTarget = ManagedChannelBuilder.forTarget(rp.b());
            if (!rp.d()) {
                forTarget.usePlaintext();
            }
            managedChannelBuilder = forTarget;
        }
        managedChannelBuilder.keepAliveTime(30L, TimeUnit.SECONDS);
        return AndroidChannelBuilder.usingBuilder(managedChannelBuilder).context(context).build();
    }
    
    public final void k() {
        this.a = Tasks.call(wy.c, (Callable)new xb0(this));
    }
    
    public final void s(final ManagedChannel managedChannel) {
        final ConnectivityState state = managedChannel.getState(true);
        final StringBuilder sb = new StringBuilder();
        sb.append("Current gRPC connectivity state: ");
        sb.append(state);
        Logger.a("GrpcCallProvider", sb.toString(), new Object[0]);
        this.h();
        if (state == ConnectivityState.CONNECTING) {
            Logger.a("GrpcCallProvider", "Setting the connectivityAttemptTimer", new Object[0]);
            this.d = this.b.h(AsyncQueue.TimerId.CONNECTIVITY_ATTEMPT_TIMER, 15000L, new zb0(this, managedChannel));
        }
        managedChannel.notifyWhenStateChanged(state, (Runnable)new ac0(this, managedChannel));
    }
    
    public final void t(final ManagedChannel managedChannel) {
        this.b.i(new cc0(this, managedChannel));
    }
}

import java.util.concurrent.ExecutorService;
import com.android.billingclient.api.a;
import android.app.Activity;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class wb
{
    public static a d(final Context context) {
        return new a(context, null);
    }
    
    public abstract void a();
    
    public abstract boolean b();
    
    public abstract com.android.billingclient.api.a c(final Activity p0, final zb p1);
    
    public abstract void e(final ha1 p0, final u81 p1);
    
    public abstract void f(final ia1 p0, final ba1 p1);
    
    public abstract void g(final yb p0);
    
    public static final class a
    {
        public volatile ce2 a;
        public final Context b = b;
        public volatile ca1 c;
        public volatile boolean d;
        
        public wb a() {
            if (this.b == null) {
                throw new IllegalArgumentException("Please provide a valid Context.");
            }
            if (this.c == null) {
                if (this.d) {
                    return new xb(null, this.b, null, null);
                }
                throw new IllegalArgumentException("Please provide a valid listener for purchases updates.");
            }
            else {
                if (this.a == null) {
                    throw new IllegalArgumentException("Pending purchases for one-time products must be supported.");
                }
                if (this.c != null) {
                    return new xb(null, this.a, this.b, this.c, null, null, null);
                }
                return new xb(null, this.a, this.b, null, null, null);
            }
        }
        
        public a b() {
            final yd2 yd2 = new yd2(null);
            yd2.a();
            this.a = yd2.b();
            return this;
        }
        
        public a c(final ca1 c) {
            this.c = c;
            return this;
        }
    }
}

import android.os.BaseBundle;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ListAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import android.widget.Spinner;
import android.widget.ListView;
import android.content.Context;
import androidx.fragment.app.Fragment;

// 
// Decompiled by Procyon v0.6.0
// 

public class a21 extends Fragment
{
    public Context a;
    public ListView b;
    public Spinner c;
    public ArrayList d;
    public l3 e;
    public ArrayList f;
    public SheetTemplate2 g;
    public ExamId h;
    public int i;
    public z11.c j;
    
    public a21() {
        this.d = new ArrayList();
        this.i = 0;
        this.j = new z11.c(this) {
            public final a21 a;
            
            @Override
            public void a(final ArrayList d) {
                final a21 a = this.a;
                a.d = d;
                a.l();
            }
        };
    }
    
    public static /* synthetic */ int h(final a21 a21) {
        return a21.i;
    }
    
    public static /* synthetic */ int i(final a21 a21, final int i) {
        return a21.i = i;
    }
    
    public static /* synthetic */ z11.c j(final a21 a21) {
        return a21.j;
    }
    
    public final void k(final int n, final String[] array) {
        final String[] array2 = new String[n];
        for (int i = 0; i < n; ++i) {
            array2[i] = array[i];
        }
        this.c.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])array2));
        ((AdapterView)this.c).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final a21 a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, int h, final long n) {
                a21.i(this.a, h + 1);
                h = a21.h(this.a);
                final z11.c j = a21.j(this.a);
                final a21 a = this.a;
                new z11(h, j, a.g, a.f);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void l() {
        final l3 l3 = new l3(this.a, this.d);
        this.e = l3;
        this.b.setAdapter((ListAdapter)l3);
    }
    
    @Override
    public void onAttach(final Context a) {
        super.onAttach(a);
        this.a = a;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131493008, viewGroup, false);
        this.b = (ListView)inflate.findViewById(2131296842);
        this.c = (Spinner)inflate.findViewById(2131297097);
        if (this.getArguments() != null) {
            this.h = (ExamId)this.getArguments().getSerializable("EXAM_ID");
            ArrayList<ResultDataJsonModel> f;
            if ("ARCHIVED".equals(((BaseBundle)this.getArguments()).getString("EXAM_TYPE"))) {
                this.g = e8.a;
                f = e8.a();
            }
            else {
                this.g = TemplateRepository.getInstance(this.a).getTemplateJson(this.h).getSheetTemplate();
                f = ResultRepository.getInstance(this.a).getAllResultJson(this.h);
            }
            this.f = f;
            final int examSets = this.g.getTemplateParams().getExamSets();
            if (examSets == 1) {
                this.i = 0;
                inflate.findViewById(2131297280).setVisibility(8);
                ((View)this.c).setVisibility(8);
            }
            else {
                this.k(examSets, this.g.getLabelProfile().getExamSetLabels());
            }
            new z11(this.i, this.j, this.g, this.f);
        }
        return inflate;
    }
}

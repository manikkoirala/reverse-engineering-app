import android.view.ViewGroup$MarginLayoutParams;
import android.content.res.Resources;
import android.content.res.Resources$Theme;
import com.ekodroid.omrevaluator.templateui.models.DecimalRange;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.NumericalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.DecimalOptionPayload;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import android.view.ViewGroup;
import com.google.android.flexbox.FlexboxLayout;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import android.widget.EditText;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class k5
{
    public Context a;
    public AnswerOptionKey b;
    public AnswerOption c;
    public ArrayList d;
    public f e;
    public sf[] f;
    public EditText[] g;
    public LabelProfile h;
    public boolean[] i;
    public boolean j;
    public int k;
    public int l;
    public View$OnClickListener m;
    
    public k5(final Context a, final AnswerOptionKey b, final AnswerOption c, final f e, final LabelProfile h) {
        this.d = new ArrayList();
        this.j = false;
        this.k = 32;
        this.l = 40;
        this.m = (View$OnClickListener)new View$OnClickListener(this) {
            public final k5 a;
            
            public void onClick(final View view) {
                final sf sf = (sf)view;
                final int questionNo = sf.getQuestionNo();
                final int subIndex = sf.getSubIndex();
                final k5 a = this.a;
                final boolean[] i = a.i;
                i[subIndex] ^= true;
                a.q(a.f, i);
                final k5 a2 = this.a;
                final f e = a2.e;
                if (e != null) {
                    e.a(new c21(questionNo, false, a2.i, null, null));
                }
            }
        };
        this.a = a;
        this.b = b;
        this.e = e;
        this.c = c;
        this.h = h;
        this.j = false;
        this.i = e5.a(b.getMarkedValues());
        this.d(a, b, c, this.m, h);
        if (b.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
            this.r(this.g, b.getMarkedPayload());
        }
        else {
            this.q(this.f, this.i);
        }
    }
    
    public k5(final Context a, final AnswerValue answerValue, final AnswerOption c, final f e, final LabelProfile h) {
        this.d = new ArrayList();
        this.j = false;
        this.k = 32;
        this.l = 40;
        this.m = (View$OnClickListener)new View$OnClickListener(this) {
            public final k5 a;
            
            public void onClick(final View view) {
                final sf sf = (sf)view;
                final int questionNo = sf.getQuestionNo();
                final int subIndex = sf.getSubIndex();
                final k5 a = this.a;
                final boolean[] i = a.i;
                i[subIndex] ^= true;
                a.q(a.f, i);
                final k5 a2 = this.a;
                final f e = a2.e;
                if (e != null) {
                    e.a(new c21(questionNo, false, a2.i, null, null));
                }
            }
        };
        this.a = a;
        final AnswerOptionKey b = new AnswerOptionKey(answerValue);
        this.b = b;
        this.e = e;
        this.c = c;
        this.h = h;
        this.j = true;
        this.i = e5.a(b.getMarkedValues());
        this.d(a, this.b, c, this.m, h);
        this.q(this.f, this.i);
    }
    
    public final void c(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener onClickListener, final LabelProfile labelProfile) {
        final int d = a91.d(8, context);
        final int d2 = a91.d(this.k, context);
        final LinearLayout e = new LinearLayout(context);
        ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(d2, d2)).setMargins(d, d, d, d);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(a91.d(this.l, context), d2);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, d, d, d);
        final sf sf = new sf(context);
        final StringBuilder sb = new StringBuilder();
        sb.append(answerOptionKey.getQuestionNumber());
        sb.append("");
        sf.setText((CharSequence)sb.toString());
        sf.setTextColor(-16777216);
        ((ViewGroup)e).addView((View)sf, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        sf.setGravity(17);
        final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
        flexboxLayout.setFlexDirection(0);
        flexboxLayout.setFlexWrap(1);
        final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(d2, d2);
        layoutParams.setMargins(d, d, d, d);
        final String[] optionLabels = labelProfile.getOptionLabels(answerOptionKey);
        final int length = optionLabels.length;
        final sf[] f = new sf[length];
        for (int i = 0; i < length; ++i) {
            (f[i] = new sf(context)).setQuestionNo(answerOptionKey.getQuestionNumber());
            f[i].setSubIndex(i);
            ((View)f[i]).setOnClickListener(onClickListener);
            f[i].setText((CharSequence)optionLabels[i]);
            f[i].setGravity(17);
            flexboxLayout.addView((View)f[i], (ViewGroup$LayoutParams)layoutParams);
        }
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
        linearLayout$LayoutParams2.weight = 1.0f;
        ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
        this.d.add(e);
        this.f = f;
    }
    
    public final void d(final Context context, final AnswerOptionKey answerOptionKey, final AnswerOption answerOption, final View$OnClickListener view$OnClickListener, final LabelProfile labelProfile) {
        switch (k5$e.a[answerOptionKey.getType().ordinal()]) {
            case 7: {
                this.e(context, answerOptionKey, view$OnClickListener, labelProfile);
                break;
            }
            case 6: {
                this.i(context, answerOptionKey, view$OnClickListener, labelProfile);
                break;
            }
            case 5: {
                this.h(context, answerOptionKey, view$OnClickListener, labelProfile);
                break;
            }
            case 4: {
                this.j(context, answerOptionKey, view$OnClickListener, labelProfile);
                break;
            }
            case 1:
            case 2:
            case 3:
            case 8:
            case 9:
            case 10: {
                this.c(context, answerOptionKey, view$OnClickListener, labelProfile);
                break;
            }
        }
        if (answerOption != null && answerOption.partialAllowed) {
            this.d.add(this.n(answerOption, answerOptionKey, labelProfile));
        }
    }
    
    public final void e(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener view$OnClickListener, final LabelProfile labelProfile) {
        if (this.j) {
            this.f(context, answerOptionKey, view$OnClickListener, labelProfile);
            return;
        }
        final int d = a91.d(8, context);
        final int d2 = a91.d(this.k, context);
        final int d3 = a91.d(this.l, context);
        this.g = new EditText[2];
        for (int i = 0; i < 2; ++i) {
            final LinearLayout e = new LinearLayout(context);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(d3, d2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, d, d, d);
            final sf sf = new sf(context);
            String string = "";
            if (i == 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append(answerOptionKey.getQuestionNumber());
                sb.append("");
                string = sb.toString();
            }
            sf.setText((CharSequence)string);
            sf.setTextColor(-16777216);
            ((ViewGroup)e).addView((View)sf, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            sf.setGravity(17);
            final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
            flexboxLayout.setFlexDirection(0);
            flexboxLayout.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(d2, d2);
            layoutParams.setMargins(d, d, d, d);
            final TextView textView = new TextView(context);
            textView.setGravity(17);
            String text;
            if (i == 0) {
                text = ">=";
            }
            else {
                text = "<=";
            }
            textView.setText((CharSequence)text);
            flexboxLayout.addView((View)textView, (ViewGroup$LayoutParams)layoutParams);
            final FlexboxLayout.LayoutParams layoutParams2 = new FlexboxLayout.LayoutParams(d2 * 4, -2);
            layoutParams2.setMargins(d, d, d, d);
            ((TextView)(this.g[i] = new EditText(context))).addTextChangedListener((TextWatcher)new TextWatcher(this) {
                public final k5 a;
                
                public void afterTextChanged(final Editable editable) {
                }
                
                public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                }
                
                public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                    this.a.o();
                }
            });
            ((TextView)this.g[i]).setInputType(12290);
            ((TextView)this.g[i]).setGravity(17);
            ((TextView)this.g[i]).setFilters(new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(10) });
            flexboxLayout.addView((View)this.g[i], (ViewGroup$LayoutParams)layoutParams2);
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams2.weight = 1.0f;
            ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            this.d.add(e);
        }
    }
    
    public final void f(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener view$OnClickListener, final LabelProfile labelProfile) {
        final int d = a91.d(8, context);
        final int d2 = a91.d(this.k, context);
        final DecimalOptionPayload b = bc.b(answerOptionKey.getPayload());
        int n;
        if (b.decimalAllowed) {
            n = 11;
        }
        else {
            n = 10;
        }
        final int digits = b.digits;
        final int n2 = digits * n;
        final boolean hasNegetive = b.hasNegetive;
        int subIndex = n2;
        if (hasNegetive) {
            subIndex = n2 + 1;
        }
        final sf[] f = new sf[subIndex];
        if (hasNegetive) {
            final LinearLayout e = new LinearLayout(context);
            ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(d2, d2)).setMargins(d, d, d, d);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(a91.d(this.l, context), d2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, d, d, d);
            ((ViewGroup)e).addView((View)new sf(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
            flexboxLayout.setFlexDirection(0);
            flexboxLayout.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(d2, d2);
            layoutParams.setMargins(d, d, d, d);
            final sf sf = new sf(context);
            sf.setText((CharSequence)"-");
            sf.setTextColor(-16777216);
            sf.setGravity(17);
            --subIndex;
            sf.setSubIndex(subIndex);
            ((View)sf).setOnClickListener(view$OnClickListener);
            sf.setQuestionNo(answerOptionKey.getQuestionNumber());
            flexboxLayout.addView((View)(f[subIndex] = sf), (ViewGroup$LayoutParams)layoutParams);
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams2.weight = 1.0f;
            ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            this.d.add(e);
        }
        final String[] numericalOptionLabels = labelProfile.getNumericalOptionLabels();
        final String[] array = new String[n];
        String[] array2 = numericalOptionLabels;
        if (b.decimalAllowed) {
            array[0] = ".";
            int n3;
            for (int i = 0; i < 10; i = n3) {
                n3 = i + 1;
                array[n3] = numericalOptionLabels[i];
            }
            array2 = array;
        }
        for (int j = 0; j < array2.length; ++j) {
            final LinearLayout e2 = new LinearLayout(context);
            ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(d2, d2)).setMargins(d, d, d, d);
            final LinearLayout$LayoutParams linearLayout$LayoutParams3 = new LinearLayout$LayoutParams(a91.d(this.l, context), d2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams3).setMargins(d, d, d, d);
            final sf sf2 = new sf(context);
            if (j == 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append(answerOptionKey.getQuestionNumber());
                sb.append("");
                sf2.setText((CharSequence)sb.toString());
            }
            sf2.setTextColor(-16777216);
            ((ViewGroup)e2).addView((View)sf2, (ViewGroup$LayoutParams)linearLayout$LayoutParams3);
            sf2.setGravity(17);
            final FlexboxLayout flexboxLayout2 = new FlexboxLayout(context);
            flexboxLayout2.setFlexDirection(0);
            flexboxLayout2.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams2 = new FlexboxLayout.LayoutParams(d2, d2);
            layoutParams2.setMargins(d, d, d, d);
            for (int k = 0; k < digits; ++k) {
                final int subIndex2 = j * digits + k;
                (f[subIndex2] = new sf(context)).setQuestionNo(answerOptionKey.getQuestionNumber());
                f[subIndex2].setSubIndex(subIndex2);
                ((View)f[subIndex2]).setOnClickListener(view$OnClickListener);
                f[subIndex2].setText((CharSequence)array2[j]);
                f[subIndex2].setGravity(17);
                flexboxLayout2.addView((View)f[subIndex2], (ViewGroup$LayoutParams)layoutParams2);
            }
            final LinearLayout$LayoutParams linearLayout$LayoutParams4 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams4.weight = 1.0f;
            ((ViewGroup)e2).addView((View)flexboxLayout2, (ViewGroup$LayoutParams)linearLayout$LayoutParams4);
            this.d.add(e2);
        }
        this.f = f;
    }
    
    public final void g(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener view$OnClickListener, final LabelProfile labelProfile) {
        final NumericalOptionPayload d = bc.d(answerOptionKey.getPayload());
        final int d2 = a91.d(8, context);
        final int d3 = a91.d(this.k, context);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(d3, d3);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d2, d2, d2, d2);
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(a91.d(this.l, context), d3);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(d2, d2, d2, d2);
        final int n = d.digits * 10;
        final boolean hasNegetive = d.hasNegetive;
        int n2 = n;
        if (hasNegetive) {
            n2 = n + 1;
        }
        final sf[] f = new sf[n2];
        if (hasNegetive) {
            final LinearLayout e = new LinearLayout(context);
            final sf sf = new sf(context);
            sf.setText((CharSequence)" ");
            ((ViewGroup)e).addView((View)sf, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            sf.setTextColor(-16777216);
            sf.setGravity(17);
            for (int i = 0; i < 5; ++i) {
                final sf sf2 = new sf(context);
                if (i == 1) {
                    sf2.setText((CharSequence)"-");
                    sf2.setTextColor(-16777216);
                    sf2.setGravity(17);
                    final int subIndex = n2 - 1;
                    sf2.setSubIndex(subIndex);
                    ((View)sf2).setOnClickListener(view$OnClickListener);
                    sf2.setQuestionNo(answerOptionKey.getQuestionNumber());
                    f[subIndex] = sf2;
                }
                ((ViewGroup)e).addView((View)sf2, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            }
            this.d.add(e);
        }
        final String[] numericalOptionLabels = labelProfile.getNumericalOptionLabels();
        final int n3 = (d.digits - 1) / 5;
        for (int j = 0; j < 10; ++j) {
            for (int k = 0; k < n3 + 1; ++k) {
                final LinearLayout e2 = new LinearLayout(context);
                final sf sf3 = new sf(context);
                sf3.setText((CharSequence)" ");
                if (k == 0 && j == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(answerOptionKey.getQuestionNumber());
                    sb.append("");
                    sf3.setText((CharSequence)sb.toString());
                }
                ((ViewGroup)e2).addView((View)sf3, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
                sf3.setTextColor(-16777216);
                sf3.setGravity(17);
                for (int l = 0; l < 5; ++l) {
                    final sf sf4 = new sf(context);
                    final int n4 = k * 5 + l;
                    if (n4 < d.digits) {
                        sf4.setQuestionNo(answerOptionKey.getQuestionNumber());
                        sf4.setSubIndex(d.digits * j + n4);
                        ((View)sf4).setOnClickListener(view$OnClickListener);
                        sf4.setText((CharSequence)numericalOptionLabels[j]);
                        f[d.digits * j + n4] = sf4;
                    }
                    ((ViewGroup)e2).addView((View)sf4, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                    sf4.setGravity(17);
                }
                this.d.add(e2);
            }
        }
        this.f = f;
    }
    
    public final void h(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener onClickListener, final LabelProfile labelProfile) {
        final int d = a91.d(8, context);
        final int d2 = a91.d(this.k, context);
        final MatrixOptionPayload c = bc.c(answerOptionKey.getPayload());
        int primaryOptions;
        int secondaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
            secondaryOptions = c.secondaryOptions;
        }
        else {
            primaryOptions = 4;
            secondaryOptions = 5;
        }
        final sf[] f = new sf[primaryOptions * secondaryOptions];
        final String[] matrixPrimary = labelProfile.getMatrixPrimary();
        final String[] matrixSecondary = labelProfile.getMatrixSecondary();
        for (int i = 0; i < primaryOptions; ++i) {
            final LinearLayout e = new LinearLayout(context);
            ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(d2, d2)).setMargins(d, d, d, d);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(a91.d(this.l, context), d2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, d, d, d);
            final sf sf = new sf(context);
            final StringBuilder sb = new StringBuilder();
            sb.append(answerOptionKey.getQuestionNumber());
            sb.append(matrixPrimary[i]);
            sf.setText((CharSequence)sb.toString());
            sf.setTextColor(-16777216);
            ((ViewGroup)e).addView((View)sf, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            sf.setGravity(17);
            final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
            flexboxLayout.setFlexDirection(0);
            flexboxLayout.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(d2, d2);
            layoutParams.setMargins(d, d, d, d);
            for (int j = 0; j < secondaryOptions; ++j) {
                final int subIndex = i * secondaryOptions + j;
                (f[subIndex] = new sf(context)).setQuestionNo(answerOptionKey.getQuestionNumber());
                f[subIndex].setSubIndex(subIndex);
                ((View)f[subIndex]).setOnClickListener(onClickListener);
                f[subIndex].setText((CharSequence)matrixSecondary[j]);
                f[subIndex].setGravity(17);
                flexboxLayout.addView((View)f[subIndex], (ViewGroup$LayoutParams)layoutParams);
            }
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams2.weight = 1.0f;
            ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            this.d.add(e);
        }
        this.f = f;
    }
    
    public final void i(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener onClickListener, final LabelProfile labelProfile) {
        final NumericalOptionPayload d = bc.d(answerOptionKey.getPayload());
        if (d != null && (d.hasNegetive || d.digits > 1)) {
            this.g(context, answerOptionKey, onClickListener, labelProfile);
            return;
        }
        final int d2 = a91.d(8, context);
        final int d3 = a91.d(this.k, context);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(d3, d3);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d2, d2, d2, d2);
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(a91.d(this.l, context), d3);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(d2, d2, d2, d2);
        final sf[] f = new sf[10];
        final String[] numericalOptionLabels = labelProfile.getNumericalOptionLabels();
        for (int i = 0; i < 2; ++i) {
            final LinearLayout e = new LinearLayout(context);
            final sf sf = new sf(context);
            Label_0223: {
                String string;
                if (i != 0) {
                    if (i != 1) {
                        break Label_0223;
                    }
                    string = " ";
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(answerOptionKey.getQuestionNumber());
                    sb.append("");
                    string = sb.toString();
                }
                sf.setText((CharSequence)string);
            }
            ((ViewGroup)e).addView((View)sf, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            sf.setTextColor(-16777216);
            sf.setGravity(17);
            for (int j = 0; j < 5; ++j) {
                final int subIndex = i * 5 + j;
                final sf sf2 = new sf(context);
                sf2.setQuestionNo(answerOptionKey.getQuestionNumber());
                sf2.setSubIndex(subIndex);
                ((View)sf2).setOnClickListener(onClickListener);
                sf2.setText((CharSequence)numericalOptionLabels[subIndex]);
                ((ViewGroup)e).addView((View)(f[subIndex] = sf2), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                sf2.setGravity(17);
            }
            this.d.add(e);
        }
        this.f = f;
    }
    
    public final void j(final Context context, final AnswerOptionKey answerOptionKey, final View$OnClickListener onClickListener, final LabelProfile labelProfile) {
        final LinearLayout e = new LinearLayout(context);
        final int d = a91.d(8, context);
        final int d2 = a91.d(this.k, context);
        final sf[] f = new sf[2];
        final sf sf = new sf(context);
        final StringBuilder sb = new StringBuilder();
        sb.append(answerOptionKey.getQuestionNumber());
        sb.append("");
        sf.setText((CharSequence)sb.toString());
        sf.setTextColor(-16777216);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, d2);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(d, d, d, d);
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(a91.d(this.l, context), d2);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(d, d, d, d);
        linearLayout$LayoutParams.weight = 2.0f;
        ((ViewGroup)e).addView((View)sf, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
        sf.setGravity(17);
        for (int i = 0; i < 2; ++i) {
            (f[i] = new sf(context)).setQuestionNo(answerOptionKey.getQuestionNumber());
            f[i].setSubIndex(i);
            ((View)f[i]).setOnClickListener(onClickListener);
            f[i].setText((CharSequence)labelProfile.getTrueOrFalseLabel()[i]);
            f[i].setGravity(17);
            ((ViewGroup)e).addView((View)f[i], (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        }
        ((ViewGroup)e).addView((View)new sf(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        this.d.add(e);
        this.f = f;
    }
    
    public boolean[] k() {
        return e5.a(this.i);
    }
    
    public String l() {
        if (this.b.getType() != AnswerOption.AnswerOptionType.DECIMAL) {
            return null;
        }
        int n = 0;
        try {
            while (true) {
                final EditText[] g = this.g;
                if (n < g.length) {
                    if (g[n].getText().toString().length() < 1) {
                        ((TextView)this.g[n]).setError((CharSequence)this.a.getString(2131886275));
                        return null;
                    }
                    ++n;
                }
                else {
                    final double double1 = Double.parseDouble(g[0].getText().toString());
                    final double double2 = Double.parseDouble(this.g[1].getText().toString());
                    if (double2 < double1) {
                        ((TextView)this.g[0]).setError((CharSequence)this.a.getString(2131886492));
                        return null;
                    }
                    ((TextView)this.g[0]).setError((CharSequence)null);
                    ((TextView)this.g[1]).setError((CharSequence)null);
                    return new gc0().s(new DecimalRange(double1, double2));
                }
            }
        }
        catch (final Exception ex) {
            ((TextView)this.g[0]).setError((CharSequence)this.a.getString(2131886275));
            ((TextView)this.g[1]).setError((CharSequence)this.a.getString(2131886275));
            return null;
        }
    }
    
    public ArrayList m() {
        return this.d;
    }
    
    public final LinearLayout n(final AnswerOption answerOption, final AnswerOptionKey answerOptionKey, final LabelProfile labelProfile) {
        final LinearLayout linearLayout = new LinearLayout(this.a);
        linearLayout.setGravity(8388613);
        final TextView textView = new TextView(this.a);
        textView.setText((CharSequence)this.a.getString(2131886448));
        if (answerOptionKey.getPartialAnswerOptionKeys() != null && answerOptionKey.getPartialAnswerOptionKeys().size() > 0) {
            textView.setText((CharSequence)this.a.getString(2131886486));
        }
        ((View)textView).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, answerOption, labelProfile, new y01(this, textView, answerOptionKey) {
            public final TextView a;
            public final AnswerOptionKey b;
            public final k5 c;
            
            @Override
            public void a(final Object o) {
                this.a.setText((CharSequence)this.c.a.getString(2131886448));
                if (this.b.getPartialAnswerOptionKeys() != null && this.b.getPartialAnswerOptionKeys().size() > 0) {
                    this.a.setText((CharSequence)this.c.a.getString(2131886486));
                }
            }
        }) {
            public final AnswerOption a;
            public final LabelProfile b;
            public final y01 c;
            public final k5 d;
            
            public void onClick(final View view) {
                final k5 d = this.d;
                new v3(d.a, d.e, this.a, d.b, this.b, this.c).show();
            }
        });
        final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-2, -2);
        ((ViewGroup$MarginLayoutParams)layoutParams).setMargins(a91.d(6, this.a), a91.d(0, this.a), a91.d(6, this.a), a91.d(4, this.a));
        textView.setTextColor(this.a.getResources().getColor(2131099724, (Resources$Theme)null));
        ((View)textView).setBackground(g7.b(this.a, 2131230858));
        textView.setPadding(a91.d(10, this.a), a91.d(6, this.a), a91.d(10, this.a), a91.d(6, this.a));
        textView.setTextSize(14.0f);
        ((View)textView).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
        ((ViewGroup)linearLayout).addView((View)textView);
        return linearLayout;
    }
    
    public final void o() {
        final String l = this.l();
        if (l != null) {
            final f e = this.e;
            if (e != null) {
                e.a(new c21(this.c.questionNumber, false, null, l, null));
            }
        }
    }
    
    public void p() {
        if (this.b.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
            this.r(this.g, null);
            return;
        }
        int n = 0;
        boolean[] i;
        while (true) {
            i = this.i;
            if (n >= i.length) {
                break;
            }
            i[n] = false;
            ++n;
        }
        this.q(this.f, i);
    }
    
    public final void q(final sf[] array, final boolean[] array2) {
        if (array != null && array2 != null && array.length == array2.length) {
            for (int i = 0; i < array.length; ++i) {
                sf sf;
                Resources resources;
                int n;
                if (array2[i]) {
                    array[i].setBackgroundResource(2131231082);
                    sf = array[i];
                    resources = this.a.getResources();
                    n = 2131099740;
                }
                else {
                    array[i].setBackgroundResource(2131230871);
                    sf = array[i];
                    resources = this.a.getResources();
                    n = 2131099702;
                }
                sf.setTextColor(resources.getColor(n));
            }
        }
    }
    
    public final void r(final EditText[] array, final String s) {
        if (s == null) {
            ((TextView)array[0]).setText((CharSequence)"");
            ((TextView)array[1]).setText((CharSequence)"");
        }
        else {
            final DecimalRange decimalRange = (DecimalRange)new gc0().j(s, DecimalRange.class);
            final EditText editText = array[0];
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(decimalRange.getLower());
            ((TextView)editText).setText((CharSequence)sb.toString());
            final EditText editText2 = array[1];
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(decimalRange.getUpper());
            ((TextView)editText2).setText((CharSequence)sb2.toString());
        }
    }
    
    public interface f
    {
        void a(final c21 p0);
    }
}

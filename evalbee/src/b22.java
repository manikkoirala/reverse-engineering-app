import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.j0;
import java.util.Date;
import com.google.firestore.v1.k;
import java.util.Iterator;
import com.google.protobuf.NullValue;
import com.google.firestore.v1.a;
import com.google.firebase.firestore.core.UserData$Source;
import java.util.List;
import java.util.Map;
import com.google.firestore.v1.Value;

// 
// Decompiled by Procyon v0.6.0
// 

public final class b22
{
    public final qp a;
    
    public b22(final qp a) {
        this.a = a;
    }
    
    public final a11 a(final Object o, final y12 y12) {
        if (o.getClass().isArray()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid data. Data must be a Map<String, Object> or a suitable POJO object, but it was ");
            sb.append("an array");
            throw new IllegalArgumentException(sb.toString());
        }
        final Value c = this.c(oo.c(o), y12);
        if (c.w0() == Value.ValueTypeCase.MAP_VALUE) {
            return new a11(c);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Invalid data. Data must be a Map<String, Object> or a suitable POJO object, but it was ");
        sb2.append("of type: ");
        sb2.append(o22.z(o));
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public Value b(final Object o, final y12 y12) {
        return this.c(oo.c(o), y12);
    }
    
    public final Value c(final Object o, final y12 y12) {
        if (o instanceof Map) {
            return this.e((Map)o, y12);
        }
        if (o instanceof v00) {
            this.h((v00)o, y12);
            return null;
        }
        if (y12.h() != null) {
            y12.a(y12.h());
        }
        if (!(o instanceof List)) {
            return this.g(o, y12);
        }
        if (y12.i() && y12.g() != UserData$Source.ArrayArgument) {
            throw y12.f("Nested arrays are not supported");
        }
        return this.d((List)o, y12);
    }
    
    public final Value d(final List list, final y12 y12) {
        final a.b j0 = com.google.firestore.v1.a.j0();
        final Iterator iterator = list.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            Value c;
            if ((c = this.c(iterator.next(), y12.c(n))) == null) {
                c = (Value)((GeneratedMessageLite.a)Value.x0().J(NullValue.NULL_VALUE)).p();
            }
            j0.B(c);
            ++n;
        }
        return (Value)((GeneratedMessageLite.a)Value.x0().A(j0)).p();
    }
    
    public final Value e(final Map map, final y12 y12) {
        Value.b b;
        if (map.isEmpty()) {
            if (y12.h() != null && !y12.h().j()) {
                y12.a(y12.h());
            }
            b = Value.x0().I(k.b0());
        }
        else {
            final k.b j0 = k.j0();
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                if (!(entry.getKey() instanceof String)) {
                    throw y12.f(String.format("Non-String Map key (%s) is not allowed", entry.getValue()));
                }
                final String s = entry.getKey();
                final Value c = this.c(entry.getValue(), y12.e(s));
                if (c == null) {
                    continue;
                }
                j0.C(s, c);
            }
            b = Value.x0().H(j0);
        }
        return (Value)((GeneratedMessageLite.a)b).p();
    }
    
    public z12 f(final Object o, final q00 q00) {
        final x12 x12 = new x12(UserData$Source.MergeSet);
        final a11 a = this.a(o, x12.e());
        if (q00 != null) {
            for (final s00 s00 : q00.c()) {
                if (x12.d(s00)) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Field '");
                sb.append(s00.toString());
                sb.append("' is specified in your field mask but not in your input data.");
                throw new IllegalArgumentException(sb.toString());
            }
            return x12.g(a, q00);
        }
        return x12.f(a);
    }
    
    public final Value g(final Object o, final y12 y12) {
        if (o == null) {
            return (Value)((GeneratedMessageLite.a)Value.x0().J(NullValue.NULL_VALUE)).p();
        }
        if (o instanceof Integer) {
            return (Value)((GeneratedMessageLite.a)Value.x0().G((int)o)).p();
        }
        if (o instanceof Long) {
            return (Value)((GeneratedMessageLite.a)Value.x0().G((long)o)).p();
        }
        if (o instanceof Float) {
            return (Value)((GeneratedMessageLite.a)Value.x0().E((double)o)).p();
        }
        if (o instanceof Double) {
            return (Value)((GeneratedMessageLite.a)Value.x0().E((double)o)).p();
        }
        if (o instanceof Boolean) {
            return (Value)((GeneratedMessageLite.a)Value.x0().C((boolean)o)).p();
        }
        if (o instanceof String) {
            return (Value)((GeneratedMessageLite.a)Value.x0().L((String)o)).p();
        }
        if (o instanceof Date) {
            return this.j(new pw1((Date)o));
        }
        if (o instanceof pw1) {
            return this.j((pw1)o);
        }
        if (o instanceof ia0) {
            final ia0 ia0 = (ia0)o;
            return (Value)((GeneratedMessageLite.a)Value.x0().F(ui0.f0().A(ia0.c()).B(ia0.d()))).p();
        }
        if (o instanceof ec) {
            return (Value)((GeneratedMessageLite.a)Value.x0().D(((ec)o).d())).p();
        }
        if (o instanceof com.google.firebase.firestore.a) {
            final com.google.firebase.firestore.a a = (com.google.firebase.firestore.a)o;
            if (a.j() != null) {
                final qp d = a.j().d();
                if (!d.equals(this.a)) {
                    throw y12.f(String.format("Document reference is for database %s/%s but should be for database %s/%s", d.f(), d.e(), this.a.f(), this.a.e()));
                }
            }
            return (Value)((GeneratedMessageLite.a)Value.x0().K(String.format("projects/%s/databases/%s/documents/%s", this.a.f(), this.a.e(), a.k()))).p();
        }
        if (o.getClass().isArray()) {
            throw y12.f("Arrays are not supported; use a List instead");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unsupported type: ");
        sb.append(o22.z(o));
        throw y12.f(sb.toString());
    }
    
    public final void h(final v00 v00, final y12 y12) {
        if (!y12.j()) {
            throw y12.f(String.format("%s() can only be used with set() and update()", v00.b()));
        }
        if (y12.h() != null) {
            if (v00 instanceof v00.a) {
                if (y12.g() == UserData$Source.MergeSet) {
                    y12.a(y12.h());
                }
                else {
                    if (y12.g() == UserData$Source.Update) {
                        g9.d(y12.h().l() > 0, "FieldValue.delete() at the top level should have already been handled.", new Object[0]);
                        throw y12.f("FieldValue.delete() can only appear at the top level of your update data");
                    }
                    throw y12.f("FieldValue.delete() can only be used with update() and set() with SetOptions.merge()");
                }
            }
            else {
                if (!(v00 instanceof v00.b)) {
                    throw g9.a("Unknown FieldValue type: %s", o22.z(v00));
                }
                y12.b(y12.h(), pl1.d());
            }
            return;
        }
        throw y12.f(String.format("%s() is not currently supported inside arrays", v00.b()));
    }
    
    public z12 i(final Object o) {
        final x12 x12 = new x12(UserData$Source.Set);
        return x12.h(this.a(o, x12.e()));
    }
    
    public final Value j(final pw1 pw1) {
        return (Value)((GeneratedMessageLite.a)Value.x0().M(j0.f0().B(pw1.e()).A(pw1.d() / 1000 * 1000))).p();
    }
    
    public a22 k(final Map map) {
        k71.c(map, "Provided update data must not be null.");
        final x12 x12 = new x12(UserData$Source.Update);
        final y12 e = x12.e();
        final a11 a11 = new a11();
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            final s00 b = t00.a(entry.getKey()).b();
            final V value = entry.getValue();
            if (value instanceof v00.a) {
                e.a(b);
            }
            else {
                final Value b2 = this.b(value, e.d(b));
                if (b2 == null) {
                    continue;
                }
                e.a(b);
                a11.m(b, b2);
            }
        }
        return x12.i(a11);
    }
}

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class ew1 implements Executor
{
    public final Executor a;
    public final Semaphore b;
    
    public ew1(final int permits, final Executor a) {
        this.b = new Semaphore(permits);
        this.a = a;
    }
    
    @Override
    public void execute(final Runnable runnable) {
        while (true) {
            if (!this.b.tryAcquire()) {
                break Label_0035;
            }
            try {
                this.a.execute(new dw1(this, runnable));
                return;
                runnable.run();
            }
            catch (final RejectedExecutionException ex) {
                continue;
            }
            break;
        }
    }
}

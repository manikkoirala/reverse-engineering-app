import android.view.inputmethod.InputContentInfo;
import android.os.Build$VERSION;
import android.content.ClipDescription;
import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jf0
{
    public final c a;
    
    public jf0(final Uri uri, final ClipDescription clipDescription, final Uri uri2) {
        c a;
        if (Build$VERSION.SDK_INT >= 25) {
            a = new a(uri, clipDescription, uri2);
        }
        else {
            a = new b(uri, clipDescription, uri2);
        }
        this.a = a;
    }
    
    public jf0(final c a) {
        this.a = a;
    }
    
    public static jf0 f(final Object o) {
        if (o == null) {
            return null;
        }
        if (Build$VERSION.SDK_INT < 25) {
            return null;
        }
        return new jf0((c)new a(o));
    }
    
    public Uri a() {
        return this.a.b();
    }
    
    public ClipDescription b() {
        return this.a.getDescription();
    }
    
    public Uri c() {
        return this.a.d();
    }
    
    public void d() {
        this.a.c();
    }
    
    public Object e() {
        return this.a.a();
    }
    
    public static final class a implements c
    {
        public final InputContentInfo a;
        
        public a(final Uri uri, final ClipDescription clipDescription, final Uri uri2) {
            this.a = new InputContentInfo(uri, clipDescription, uri2);
        }
        
        public a(final Object o) {
            this.a = (InputContentInfo)o;
        }
        
        @Override
        public Object a() {
            return this.a;
        }
        
        @Override
        public Uri b() {
            return this.a.getContentUri();
        }
        
        @Override
        public void c() {
            this.a.requestPermission();
        }
        
        @Override
        public Uri d() {
            return this.a.getLinkUri();
        }
        
        @Override
        public ClipDescription getDescription() {
            return this.a.getDescription();
        }
    }
    
    public static final class b implements c
    {
        public final Uri a;
        public final ClipDescription b;
        public final Uri c;
        
        public b(final Uri a, final ClipDescription b, final Uri c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public Object a() {
            return null;
        }
        
        @Override
        public Uri b() {
            return this.a;
        }
        
        @Override
        public void c() {
        }
        
        @Override
        public Uri d() {
            return this.c;
        }
        
        @Override
        public ClipDescription getDescription() {
            return this.b;
        }
    }
    
    public interface c
    {
        Object a();
        
        Uri b();
        
        void c();
        
        Uri d();
        
        ClipDescription getDescription();
    }
}

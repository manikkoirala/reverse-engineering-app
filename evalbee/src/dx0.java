import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.List;
import java.io.IOException;
import dalvik.system.BaseDexClassLoader;
import android.content.pm.ApplicationInfo;
import java.util.Arrays;
import java.lang.reflect.Array;
import android.util.Log;
import java.io.File;
import android.content.Context;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class dx0
{
    public static final Set a;
    public static final boolean b;
    
    static {
        a = new HashSet();
        b = n(System.getProperty("java.vm.version"));
    }
    
    public static void d(final Context context) {
        final File file = new File(context.getFilesDir(), "secondary-dexes");
        if (file.isDirectory()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Clearing old secondary dex dir (");
            sb.append(file.getPath());
            sb.append(").");
            Log.i("MultiDex", sb.toString());
            final File[] listFiles = file.listFiles();
            if (listFiles == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to list secondary dex dir content (");
                sb2.append(file.getPath());
                sb2.append(").");
                Log.w("MultiDex", sb2.toString());
                return;
            }
            for (final File file2 : listFiles) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Trying to delete old file ");
                sb3.append(file2.getPath());
                sb3.append(" of size ");
                sb3.append(file2.length());
                Log.i("MultiDex", sb3.toString());
                if (!file2.delete()) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Failed to delete old file ");
                    sb4.append(file2.getPath());
                    Log.w("MultiDex", sb4.toString());
                }
                else {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("Deleted old file ");
                    sb5.append(file2.getPath());
                    Log.i("MultiDex", sb5.toString());
                }
            }
            if (!file.delete()) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("Failed to delete secondary dex dir ");
                sb6.append(file.getPath());
                Log.w("MultiDex", sb6.toString());
            }
            else {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("Deleted old secondary dex dir ");
                sb7.append(file.getPath());
                Log.i("MultiDex", sb7.toString());
            }
        }
    }
    
    public static void e(final Context p0, final File p1, final File p2, final String p3, final String p4, final boolean p5) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          7
        //     5: aload           7
        //     7: monitorenter   
        //     8: aload           7
        //    10: aload_1        
        //    11: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //    16: ifeq            23
        //    19: aload           7
        //    21: monitorexit    
        //    22: return         
        //    23: aload           7
        //    25: aload_1        
        //    26: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //    31: pop            
        //    32: getstatic       android/os/Build$VERSION.SDK_INT:I
        //    35: istore          6
        //    37: new             Ljava/lang/StringBuilder;
        //    40: astore          8
        //    42: aload           8
        //    44: invokespecial   java/lang/StringBuilder.<init>:()V
        //    47: aload           8
        //    49: ldc             "MultiDex is not guaranteed to work in SDK version "
        //    51: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    54: pop            
        //    55: aload           8
        //    57: iload           6
        //    59: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    62: pop            
        //    63: aload           8
        //    65: ldc             ": SDK version higher than "
        //    67: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    70: pop            
        //    71: aload           8
        //    73: bipush          20
        //    75: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    78: pop            
        //    79: aload           8
        //    81: ldc             " should be backed by "
        //    83: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    86: pop            
        //    87: aload           8
        //    89: ldc             "runtime with built-in multidex capabilty but it's not the "
        //    91: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    94: pop            
        //    95: aload           8
        //    97: ldc             "case here: java.vm.version=\""
        //    99: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   102: pop            
        //   103: aload           8
        //   105: ldc             "java.vm.version"
        //   107: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   113: pop            
        //   114: aload           8
        //   116: ldc             "\""
        //   118: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   121: pop            
        //   122: ldc             "MultiDex"
        //   124: aload           8
        //   126: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   129: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   132: pop            
        //   133: aload_0        
        //   134: invokestatic    dx0.j:(Landroid/content/Context;)Ljava/lang/ClassLoader;
        //   137: astore          8
        //   139: aload           8
        //   141: ifnonnull       148
        //   144: aload           7
        //   146: monitorexit    
        //   147: return         
        //   148: aload_0        
        //   149: invokestatic    dx0.d:(Landroid/content/Context;)V
        //   152: goto            167
        //   155: astore          9
        //   157: ldc             "MultiDex"
        //   159: ldc             "Something went wrong when trying to clear old MultiDex extraction, continuing without cleaning."
        //   161: aload           9
        //   163: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   166: pop            
        //   167: aload_0        
        //   168: aload_2        
        //   169: aload_3        
        //   170: invokestatic    dx0.k:(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
        //   173: astore_3       
        //   174: new             Landroidx/multidex/MultiDexExtractor;
        //   177: astore_2       
        //   178: aload_2        
        //   179: aload_1        
        //   180: aload_3        
        //   181: invokespecial   androidx/multidex/MultiDexExtractor.<init>:(Ljava/io/File;Ljava/io/File;)V
        //   184: aload_2        
        //   185: aload_0        
        //   186: aload           4
        //   188: iconst_0       
        //   189: invokevirtual   androidx/multidex/MultiDexExtractor.h:(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
        //   192: astore_1       
        //   193: aload           8
        //   195: aload_3        
        //   196: aload_1        
        //   197: invokestatic    dx0.m:(Ljava/lang/ClassLoader;Ljava/io/File;Ljava/util/List;)V
        //   200: goto            232
        //   203: astore_1       
        //   204: iload           5
        //   206: ifeq            252
        //   209: ldc             "MultiDex"
        //   211: ldc             "Failed to install extracted secondary dex files, retrying with forced extraction"
        //   213: aload_1        
        //   214: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   217: pop            
        //   218: aload           8
        //   220: aload_3        
        //   221: aload_2        
        //   222: aload_0        
        //   223: aload           4
        //   225: iconst_1       
        //   226: invokevirtual   androidx/multidex/MultiDexExtractor.h:(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
        //   229: invokestatic    dx0.m:(Ljava/lang/ClassLoader;Ljava/io/File;Ljava/util/List;)V
        //   232: aload_2        
        //   233: invokevirtual   androidx/multidex/MultiDexExtractor.close:()V
        //   236: aconst_null    
        //   237: astore_0       
        //   238: goto            242
        //   241: astore_0       
        //   242: aload_0        
        //   243: ifnonnull       250
        //   246: aload           7
        //   248: monitorexit    
        //   249: return         
        //   250: aload_0        
        //   251: athrow         
        //   252: aload_1        
        //   253: athrow         
        //   254: astore_0       
        //   255: aload_2        
        //   256: invokevirtual   androidx/multidex/MultiDexExtractor.close:()V
        //   259: aload_0        
        //   260: athrow         
        //   261: astore_0       
        //   262: aload           7
        //   264: monitorexit    
        //   265: aload_0        
        //   266: athrow         
        //   267: astore_1       
        //   268: goto            259
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      22     261    267    Any
        //  23     139    261    267    Any
        //  144    147    261    267    Any
        //  148    152    155    167    Any
        //  157    167    261    267    Any
        //  167    184    261    267    Any
        //  184    193    254    261    Any
        //  193    200    203    232    Ljava/io/IOException;
        //  193    200    254    261    Any
        //  209    232    254    261    Any
        //  232    236    241    242    Ljava/io/IOException;
        //  232    236    261    267    Any
        //  246    249    261    267    Any
        //  250    252    261    267    Any
        //  252    254    254    261    Any
        //  255    259    267    271    Ljava/io/IOException;
        //  255    259    261    267    Any
        //  259    261    261    267    Any
        //  262    265    261    267    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0259:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void f(final Object o, final String s, final Object[] array) {
        final Field g = g(o, s);
        final Object[] array2 = (Object[])g.get(o);
        final Object[] value = (Object[])Array.newInstance(array2.getClass().getComponentType(), array2.length + array.length);
        System.arraycopy(array2, 0, value, 0, array2.length);
        System.arraycopy(array, 0, value, array2.length, array.length);
        g.set(o, value);
    }
    
    public static Field g(final Object o, final String s) {
        Class<?> clazz = o.getClass();
        while (clazz != null) {
            try {
                final Field declaredField = clazz.getDeclaredField(s);
                if (!declaredField.isAccessible()) {
                    declaredField.setAccessible(true);
                }
                return declaredField;
            }
            catch (final NoSuchFieldException ex) {
                clazz = clazz.getSuperclass();
                continue;
            }
            break;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Field ");
        sb.append(s);
        sb.append(" not found in ");
        sb.append(o.getClass());
        throw new NoSuchFieldException(sb.toString());
    }
    
    public static Method h(final Object o, final String s, final Class... array) {
        Class<?> clazz = o.getClass();
        while (clazz != null) {
            try {
                final Method declaredMethod = clazz.getDeclaredMethod(s, (Class<?>[])array);
                if (!declaredMethod.isAccessible()) {
                    declaredMethod.setAccessible(true);
                }
                return declaredMethod;
            }
            catch (final NoSuchMethodException ex) {
                clazz = clazz.getSuperclass();
                continue;
            }
            break;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Method ");
        sb.append(s);
        sb.append(" with parameters ");
        sb.append(Arrays.asList((Class[])array));
        sb.append(" not found in ");
        sb.append(o.getClass());
        throw new NoSuchMethodException(sb.toString());
    }
    
    public static ApplicationInfo i(final Context context) {
        try {
            return context.getApplicationInfo();
        }
        catch (final RuntimeException ex) {
            Log.w("MultiDex", "Failure while trying to obtain ApplicationInfo from Context. Must be running in test mode. Skip patching.", (Throwable)ex);
            return null;
        }
    }
    
    public static ClassLoader j(final Context context) {
        try {
            final ClassLoader classLoader = context.getClassLoader();
            if (classLoader instanceof BaseDexClassLoader) {
                return classLoader;
            }
            Log.e("MultiDex", "Context class loader is null or not dex-capable. Must be running in test mode. Skip patching.");
            return null;
        }
        catch (final RuntimeException ex) {
            Log.w("MultiDex", "Failure while trying to obtain Context class loader. Must be running in test mode. Skip patching.", (Throwable)ex);
            return null;
        }
    }
    
    public static File k(Context parent, File parent2, final String child) {
        parent2 = new File(parent2, "code_cache");
        try {
            o(parent2);
            parent = (Context)parent2;
        }
        catch (final IOException ex) {
            parent = (Context)new File(parent.getFilesDir(), "code_cache");
            o((File)parent);
        }
        final File file = new File((File)parent, child);
        o(file);
        return file;
    }
    
    public static void l(final Context context) {
        Log.i("MultiDex", "Installing application");
        String s;
        if (dx0.b) {
            s = "VM has multidex support, MultiDex support library is disabled.";
        }
        else {
            try {
                final ApplicationInfo i = i(context);
                if (i == null) {
                    Log.i("MultiDex", "No ApplicationInfo available, i.e. running on a test Context: MultiDex support library is disabled.");
                    return;
                }
                e(context, new File(i.sourceDir), new File(i.dataDir), "secondary-dexes", "", true);
                s = "install done";
            }
            catch (final Exception ex) {
                Log.e("MultiDex", "MultiDex installation failure", (Throwable)ex);
                final StringBuilder sb = new StringBuilder();
                sb.append("MultiDex installation failed (");
                sb.append(ex.getMessage());
                sb.append(").");
                throw new RuntimeException(sb.toString());
            }
        }
        Log.i("MultiDex", s);
    }
    
    public static void m(final ClassLoader classLoader, final File file, final List list) {
        if (!list.isEmpty()) {
            dx0.a.a(classLoader, list, file);
        }
    }
    
    public static boolean n(String str) {
        boolean b2;
        final boolean b = b2 = false;
        while (true) {
            if (str == null) {
                break Label_0115;
            }
            final StringTokenizer stringTokenizer = new StringTokenizer(str, ".");
            final boolean hasMoreTokens = stringTokenizer.hasMoreTokens();
            String nextToken = null;
            String nextToken2;
            if (hasMoreTokens) {
                nextToken2 = stringTokenizer.nextToken();
            }
            else {
                nextToken2 = null;
            }
            if (stringTokenizer.hasMoreTokens()) {
                nextToken = stringTokenizer.nextToken();
            }
            b2 = b;
            if (nextToken2 == null) {
                break Label_0115;
            }
            b2 = b;
            if (nextToken == null) {
                break Label_0115;
            }
            try {
                final int int1 = Integer.parseInt(nextToken2);
                final int int2 = Integer.parseInt(nextToken);
                if (int1 <= 2) {
                    b2 = b;
                    if (int1 != 2) {
                        break Label_0115;
                    }
                    b2 = b;
                    if (int2 < 1) {
                        break Label_0115;
                    }
                }
                b2 = true;
                final StringBuilder sb = new StringBuilder();
                sb.append("VM with version ");
                sb.append(str);
                if (b2) {
                    str = " has multidex support";
                }
                else {
                    str = " does not have multidex support";
                }
                sb.append(str);
                Log.i("MultiDex", sb.toString());
                return b2;
            }
            catch (final NumberFormatException ex) {
                b2 = b;
                continue;
            }
            break;
        }
    }
    
    public static void o(final File file) {
        file.mkdir();
        if (!file.isDirectory()) {
            final File parentFile = file.getParentFile();
            String s;
            if (parentFile == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to create dir ");
                sb.append(file.getPath());
                sb.append(". Parent file is null.");
                s = sb.toString();
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to create dir ");
                sb2.append(file.getPath());
                sb2.append(". parent file is a dir ");
                sb2.append(parentFile.isDirectory());
                sb2.append(", a file ");
                sb2.append(parentFile.isFile());
                sb2.append(", exists ");
                sb2.append(parentFile.exists());
                sb2.append(", readable ");
                sb2.append(parentFile.canRead());
                sb2.append(", writable ");
                sb2.append(parentFile.canWrite());
                s = sb2.toString();
            }
            Log.e("MultiDex", s);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to create directory ");
            sb3.append(file.getPath());
            throw new IOException(sb3.toString());
        }
    }
    
    public abstract static final class a
    {
        public static void a(final ClassLoader obj, final List c, final File file) {
            final Object value = g(obj, "pathList").get(obj);
            final ArrayList list = new ArrayList();
            f(value, "dexElements", b(value, new ArrayList(c), file, list));
            if (list.size() > 0) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    Log.w("MultiDex", "Exception in makeDexElement", (Throwable)iterator.next());
                }
                final Field a = g(value, "dexElementsSuppressedExceptions");
                final IOException[] array = (IOException[])a.get(value);
                IOException[] array2;
                if (array == null) {
                    array2 = list.toArray(new IOException[list.size()]);
                }
                else {
                    array2 = new IOException[list.size() + array.length];
                    list.toArray(array2);
                    System.arraycopy(array, 0, array2, list.size(), array.length);
                }
                a.set(value, array2);
                final IOException ex = new IOException("I/O exception during makeDexElement");
                ex.initCause(list.get(0));
                throw ex;
            }
        }
        
        public static Object[] b(final Object obj, final ArrayList list, final File file, final ArrayList list2) {
            return (Object[])h(obj, "makeDexElements", new Class[] { ArrayList.class, File.class, ArrayList.class }).invoke(obj, list, file, list2);
        }
    }
}

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;
import android.os.Build$VERSION;
import java.util.ArrayList;
import java.util.List;
import android.content.SharedPreferences$Editor;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import android.content.Context;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public class dd0
{
    public final SharedPreferences a;
    
    public dd0(final Context context, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("FirebaseHeartBeat");
        sb.append(str);
        this.a = context.getSharedPreferences(sb.toString(), 0);
    }
    
    public final void a() {
        synchronized (this) {
            final long long1 = this.a.getLong("fire-count", 0L);
            String s = "";
            final Iterator iterator = this.a.getAll().entrySet().iterator();
            String s2 = null;
            while (iterator.hasNext()) {
                final Map.Entry<K, Set> entry = (Map.Entry<K, Set>)iterator.next();
                if (entry.getValue() instanceof Set) {
                    final Iterator iterator2 = entry.getValue().iterator();
                    String s3 = s2;
                    String s4 = s;
                    while (true) {
                        s = s4;
                        s2 = s3;
                        if (!iterator2.hasNext()) {
                            break;
                        }
                        final String anotherString = (String)iterator2.next();
                        if (s3 != null && s3.compareTo(anotherString) <= 0) {
                            continue;
                        }
                        s4 = (String)entry.getKey();
                        s3 = anotherString;
                    }
                }
            }
            final HashSet set = new HashSet(this.a.getStringSet(s, (Set)new HashSet()));
            set.remove(s2);
            this.a.edit().putStringSet(s, (Set)set).putLong("fire-count", long1 - 1L).commit();
        }
    }
    
    public void b() {
        synchronized (this) {
            final SharedPreferences$Editor edit = this.a.edit();
            final Iterator iterator = this.a.getAll().entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final Map.Entry<K, Set> entry = (Map.Entry<K, Set>)iterator.next();
                if (entry.getValue() instanceof Set) {
                    final Set set = entry.getValue();
                    final String d = this.d(System.currentTimeMillis());
                    final String s = (String)entry.getKey();
                    if (set.contains(d)) {
                        final HashSet set2 = new HashSet();
                        set2.add(d);
                        ++n;
                        edit.putStringSet(s, (Set)set2);
                    }
                    else {
                        edit.remove(s);
                    }
                }
            }
            if (n == 0) {
                edit.remove("fire-count");
            }
            else {
                edit.putLong("fire-count", (long)n);
            }
            edit.commit();
        }
    }
    
    public List c() {
        synchronized (this) {
            final ArrayList<ed0> list = new ArrayList<ed0>();
            for (final Map.Entry<K, Set> entry : this.a.getAll().entrySet()) {
                if (entry.getValue() instanceof Set) {
                    final HashSet c = new HashSet(entry.getValue());
                    c.remove(this.d(System.currentTimeMillis()));
                    if (c.isEmpty()) {
                        continue;
                    }
                    list.add(ed0.a((String)entry.getKey(), new ArrayList(c)));
                }
            }
            this.l(System.currentTimeMillis());
            return list;
        }
    }
    
    public final String d(final long n) {
        synchronized (this) {
            if (Build$VERSION.SDK_INT >= 26) {
                return cd0.a(ad0.a(zc0.a(xc0.a(new Date(n)), yc0.a())), bd0.a());
            }
            return new SimpleDateFormat("yyyy-MM-dd", Locale.UK).format(new Date(n));
        }
    }
    
    public final String e(String s) {
        synchronized (this) {
            for (final Map.Entry<K, Set> entry : this.a.getAll().entrySet()) {
                if (entry.getValue() instanceof Set) {
                    final Iterator iterator2 = entry.getValue().iterator();
                    while (iterator2.hasNext()) {
                        if (s.equals(iterator2.next())) {
                            s = (String)entry.getKey();
                            return s;
                        }
                    }
                }
            }
            return null;
        }
    }
    
    public boolean f(final long n, final long n2) {
        synchronized (this) {
            return this.d(n).equals(this.d(n2));
        }
    }
    
    public void g() {
        synchronized (this) {
            final String d = this.d(System.currentTimeMillis());
            this.a.edit().putString("last-used-date", d).commit();
            this.h(d);
        }
    }
    
    public final void h(final String s) {
        synchronized (this) {
            final String e = this.e(s);
            if (e == null) {
                return;
            }
            final HashSet set = new HashSet(this.a.getStringSet(e, (Set)new HashSet()));
            set.remove(s);
            SharedPreferences$Editor sharedPreferences$Editor;
            if (set.isEmpty()) {
                sharedPreferences$Editor = this.a.edit().remove(e);
            }
            else {
                sharedPreferences$Editor = this.a.edit().putStringSet(e, (Set)set);
            }
            sharedPreferences$Editor.commit();
        }
    }
    
    public boolean i(final long n) {
        synchronized (this) {
            return this.j("fire-global", n);
        }
    }
    
    public boolean j(final String s, final long n) {
        synchronized (this) {
            if (!this.a.contains(s)) {
                this.a.edit().putLong(s, n).commit();
                return true;
            }
            if (!this.f(this.a.getLong(s, -1L), n)) {
                this.a.edit().putLong(s, n).commit();
                return true;
            }
            return false;
        }
    }
    
    public void k(long n, final String anObject) {
        synchronized (this) {
            final String d = this.d(n);
            if (!this.a.getString("last-used-date", "").equals(d)) {
                if ((n = this.a.getLong("fire-count", 0L)) + 1L == 30L) {
                    this.a();
                    n = this.a.getLong("fire-count", 0L);
                }
                final HashSet set = new HashSet(this.a.getStringSet(anObject, (Set)new HashSet()));
                set.add(d);
                this.a.edit().putStringSet(anObject, (Set)set).putLong("fire-count", n + 1L).putString("last-used-date", d).commit();
                return;
            }
            final String e = this.e(d);
            if (e == null) {
                return;
            }
            if (e.equals(anObject)) {
                return;
            }
            this.m(anObject, d);
        }
    }
    
    public void l(final long n) {
        synchronized (this) {
            this.a.edit().putLong("fire-global", n).commit();
        }
    }
    
    public final void m(final String s, final String s2) {
        synchronized (this) {
            this.h(s2);
            final HashSet set = new HashSet(this.a.getStringSet(s, (Set)new HashSet()));
            set.add(s2);
            this.a.edit().putStringSet(s, (Set)set).commit();
        }
    }
}

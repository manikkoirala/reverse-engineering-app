import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import io.grpc.Metadata$AsciiMarshaller;
import io.grpc.Metadata;
import io.grpc.Metadata$Key;

// 
// Decompiled by Procyon v0.6.0
// 

public class e20 implements fc0
{
    public static final Metadata$Key d;
    public static final Metadata$Key e;
    public static final Metadata$Key f;
    public final r91 a;
    public final r91 b;
    public final e30 c;
    
    static {
        final Metadata$AsciiMarshaller ascii_STRING_MARSHALLER = Metadata.ASCII_STRING_MARSHALLER;
        d = Metadata$Key.of("x-firebase-client-log-type", ascii_STRING_MARSHALLER);
        e = Metadata$Key.of("x-firebase-client", ascii_STRING_MARSHALLER);
        f = Metadata$Key.of("x-firebase-gmpid", ascii_STRING_MARSHALLER);
    }
    
    public e20(final r91 b, final r91 a, final e30 c) {
        this.b = b;
        this.a = a;
        this.c = c;
    }
    
    @Override
    public void a(final Metadata metadata) {
        if (this.a.get() != null) {
            if (this.b.get() != null) {
                final int code = ((HeartBeatInfo)this.a.get()).a("fire-fst").getCode();
                if (code != 0) {
                    metadata.put(e20.d, (Object)Integer.toString(code));
                }
                metadata.put(e20.e, (Object)((v12)this.b.get()).a());
                this.b(metadata);
            }
        }
    }
    
    public final void b(final Metadata metadata) {
        final e30 c = this.c;
        if (c == null) {
            return;
        }
        final String c2 = c.c();
        if (c2.length() != 0) {
            metadata.put(e20.f, (Object)c2);
        }
    }
}

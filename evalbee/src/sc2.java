import java.util.Map;
import com.google.android.gms.internal.firebase_auth_api.zzxw;
import java.util.HashMap;
import com.google.android.gms.common.logging.Logger;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sc2
{
    public static final Logger a;
    
    static {
        a = new Logger("GetTokenResultFactory", new String[0]);
    }
    
    public static ya0 a(final String s) {
        Map b;
        try {
            b = yc2.b(s);
        }
        catch (final zzxw zzxw) {
            sc2.a.e("Error parsing token claims", (Throwable)zzxw, new Object[0]);
            b = new HashMap();
        }
        return new ya0(s, b);
    }
}

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class lb0 extends v9
{
    public static final Parcelable$Creator<lb0> CREATOR;
    public final String a;
    public final String b;
    
    static {
        CREATOR = (Parcelable$Creator)new jb2();
    }
    
    public lb0(final String a, final String b) {
        if (a == null && b == null) {
            throw new IllegalArgumentException("Must specify an idToken or an accessToken.");
        }
        if (a != null && a.length() == 0) {
            throw new IllegalArgumentException("idToken cannot be empty");
        }
        if (b != null && b.length() == 0) {
            throw new IllegalArgumentException("accessToken cannot be empty");
        }
        this.a = a;
        this.b = b;
    }
    
    public static zzagt J(final lb0 lb0, final String s) {
        Preconditions.checkNotNull(lb0);
        return (zzagt)new com.google.android.gms.internal.firebase_auth_api.zzagt(lb0.a, lb0.b, lb0.i(), (String)null, (String)null, (String)null, s, (String)null, (String)null);
    }
    
    @Override
    public String E() {
        return "google.com";
    }
    
    @Override
    public final v9 H() {
        return new lb0(this.a, this.b);
    }
    
    @Override
    public String i() {
        return "google.com";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.writeString(parcel, 2, this.b, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

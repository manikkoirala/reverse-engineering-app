import java.util.Iterator;
import com.google.gson.stream.JsonToken;
import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Types;
import java.util.Collection;
import com.google.gson.reflect.TypeToken;

// 
// Decompiled by Procyon v0.6.0
// 

public final class kh implements iz1
{
    public final bl a;
    
    public kh(final bl a) {
        this.a = a;
    }
    
    @Override
    public hz1 a(final gc0 gc0, final TypeToken typeToken) {
        final Type type = typeToken.getType();
        final Class rawType = typeToken.getRawType();
        if (!Collection.class.isAssignableFrom(rawType)) {
            return null;
        }
        final Type h = $Gson$Types.h(type, rawType);
        return new a(gc0, h, gc0.l(TypeToken.get(h)), this.a.b(typeToken));
    }
    
    public static final class a extends hz1
    {
        public final hz1 a;
        public final u01 b;
        
        public a(final gc0 gc0, final Type type, final hz1 hz1, final u01 b) {
            this.a = new jz1(gc0, hz1, type);
            this.b = b;
        }
        
        public Collection e(final rh0 rh0) {
            if (rh0.o0() == JsonToken.NULL) {
                rh0.R();
                return null;
            }
            final Collection collection = (Collection)this.b.a();
            rh0.a();
            while (rh0.k()) {
                collection.add(this.a.b(rh0));
            }
            rh0.f();
            return collection;
        }
        
        public void f(final vh0 vh0, final Collection collection) {
            if (collection == null) {
                vh0.u();
                return;
            }
            vh0.c();
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.a.d(vh0, iterator.next());
            }
            vh0.f();
        }
    }
}

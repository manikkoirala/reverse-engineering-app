// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gg implements m71
{
    public static gg c() {
        return a.b;
    }
    
    public static gg d(final char c, final char c2) {
        return new c(c, c2);
    }
    
    public static gg f(final char c) {
        return new d(c);
    }
    
    public static gg i() {
        return f.b;
    }
    
    public static String j(char c) {
        final char[] array;
        final char[] data = array = new char[6];
        array[0] = '\\';
        array[1] = 'u';
        array[3] = (array[2] = '\0');
        array[5] = (array[4] = '\0');
        for (int i = 0; i < 4; ++i) {
            data[5 - i] = "0123456789ABCDEF".charAt(c & '\u000f');
            c >>= 4;
        }
        return String.copyValueOf(data);
    }
    
    public static gg k() {
        return g.c;
    }
    
    public boolean b(final Character c) {
        return this.g(c);
    }
    
    public int e(final CharSequence charSequence, int i) {
        final int length = charSequence.length();
        i71.u(i, length);
        while (i < length) {
            if (this.g(charSequence.charAt(i))) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public abstract boolean g(final char p0);
    
    public boolean h(final CharSequence charSequence) {
        for (int i = charSequence.length() - 1; i >= 0; --i) {
            if (!this.g(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static final class a extends e
    {
        public static final a b;
        
        static {
            b = new a();
        }
        
        public a() {
            super("CharMatcher.ascii()");
        }
        
        @Override
        public boolean g(final char c) {
            return c <= '\u007f';
        }
    }
    
    public abstract static class b extends gg
    {
    }
    
    public static final class c extends b
    {
        public final char a;
        public final char b;
        
        public c(final char a, final char b) {
            i71.d(b >= a);
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean g(final char c) {
            return this.a <= c && c <= this.b;
        }
        
        @Override
        public String toString() {
            final String a = j(this.a);
            final String a2 = j(this.b);
            final StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 27 + String.valueOf(a2).length());
            sb.append("CharMatcher.inRange('");
            sb.append(a);
            sb.append("', '");
            sb.append(a2);
            sb.append("')");
            return sb.toString();
        }
    }
    
    public static final class d extends b
    {
        public final char a;
        
        public d(final char a) {
            this.a = a;
        }
        
        @Override
        public boolean g(final char c) {
            return c == this.a;
        }
        
        @Override
        public String toString() {
            final String a = j(this.a);
            final StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 18);
            sb.append("CharMatcher.is('");
            sb.append(a);
            sb.append("')");
            return sb.toString();
        }
    }
    
    public abstract static class e extends b
    {
        public final String a;
        
        public e(final String s) {
            this.a = (String)i71.r(s);
        }
        
        @Override
        public final String toString() {
            return this.a;
        }
    }
    
    public static final class f extends e
    {
        public static final f b;
        
        static {
            b = new f();
        }
        
        public f() {
            super("CharMatcher.none()");
        }
        
        @Override
        public int e(final CharSequence charSequence, final int n) {
            i71.u(n, charSequence.length());
            return -1;
        }
        
        @Override
        public boolean g(final char c) {
            return false;
        }
        
        @Override
        public boolean h(final CharSequence charSequence) {
            return charSequence.length() == 0;
        }
    }
    
    public static final class g extends e
    {
        public static final int b;
        public static final g c;
        
        static {
            b = Integer.numberOfLeadingZeros(31);
            c = new g();
        }
        
        public g() {
            super("CharMatcher.whitespace()");
        }
        
        @Override
        public boolean g(final char c) {
            return "\u2002\u3000\r\u0085\u200a\u2005\u2000\u3000\u2029\u000b\u3000\u2008\u2003\u205f\u3000\u1680\t \u2006\u2001\u202f \f\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000".charAt(1682554634 * c >>> g.b) == c;
        }
    }
}

import androidx.appcompat.widget.Toolbar;
import android.view.Window;
import android.os.Build$VERSION;
import android.view.Menu;
import android.view.MenuItem;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.savedstate.a;
import android.view.MenuInflater;
import android.view.KeyEvent;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.content.res.Resources;
import androidx.fragment.app.e;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v5 extends e implements z5, ou1.a
{
    public f6 a;
    public Resources b;
    
    public v5() {
        this.o();
    }
    
    @Override
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.p();
        this.m().c(view, viewGroup$LayoutParams);
    }
    
    public void attachBaseContext(final Context context) {
        super.attachBaseContext(this.m().e(context));
    }
    
    @Override
    public Intent b() {
        return hy0.a(this);
    }
    
    public void closeOptionsMenu() {
        final t1 n = this.n();
        if (this.getWindow().hasFeature(0) && (n == null || !n.f())) {
            super.closeOptionsMenu();
        }
    }
    
    @Override
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final int keyCode = keyEvent.getKeyCode();
        final t1 n = this.n();
        return (keyCode == 82 && n != null && n.o(keyEvent)) || super.dispatchKeyEvent(keyEvent);
    }
    
    public View findViewById(final int n) {
        return this.m().h(n);
    }
    
    public MenuInflater getMenuInflater() {
        return this.m().n();
    }
    
    public Resources getResources() {
        if (this.b == null && f32.c()) {
            this.b = new f32((Context)this, super.getResources());
        }
        Resources resources;
        if ((resources = this.b) == null) {
            resources = super.getResources();
        }
        return resources;
    }
    
    public void invalidateOptionsMenu() {
        this.m().r();
    }
    
    public f6 m() {
        if (this.a == null) {
            this.a = f6.f(this, this);
        }
        return this.a;
    }
    
    public t1 n() {
        return this.m().p();
    }
    
    public final void o() {
        this.getSavedStateRegistry().h("androidx:appcompat", (androidx.savedstate.a.c)new androidx.savedstate.a.c(this) {
            public final v5 a;
            
            @Override
            public Bundle a() {
                final Bundle bundle = new Bundle();
                this.a.m().z(bundle);
                return bundle;
            }
        });
        this.addOnContextAvailableListener(new l11(this) {
            public final v5 a;
            
            @Override
            public void a(final Context context) {
                final f6 m = this.a.m();
                m.q();
                m.v(this.a.getSavedStateRegistry().b("androidx:appcompat"));
            }
        });
    }
    
    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.m().u(configuration);
        if (this.b != null) {
            configuration = super.getResources().getConfiguration();
            this.b.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
    }
    
    public void onContentChanged() {
        this.u();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.m().w();
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        return this.w(keyEvent) || super.onKeyDown(n, keyEvent);
    }
    
    @Override
    public final boolean onMenuItemSelected(final int n, final MenuItem menuItem) {
        if (super.onMenuItemSelected(n, menuItem)) {
            return true;
        }
        final t1 n2 = this.n();
        return menuItem.getItemId() == 16908332 && n2 != null && (n2.i() & 0x4) != 0x0 && this.v();
    }
    
    public boolean onMenuOpened(final int n, final Menu menu) {
        return super.onMenuOpened(n, menu);
    }
    
    @Override
    public void onPanelClosed(final int n, final Menu menu) {
        super.onPanelClosed(n, menu);
    }
    
    public void onPostCreate(final Bundle bundle) {
        super.onPostCreate(bundle);
        this.m().x(bundle);
    }
    
    @Override
    public void onPostResume() {
        super.onPostResume();
        this.m().y();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.m().A();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.m().B();
    }
    
    @Override
    public void onSupportActionModeFinished(final c2 c2) {
    }
    
    @Override
    public void onSupportActionModeStarted(final c2 c2) {
    }
    
    public void onTitleChanged(final CharSequence charSequence, final int n) {
        super.onTitleChanged(charSequence, n);
        this.m().L(charSequence);
    }
    
    @Override
    public c2 onWindowStartingSupportActionMode(final c2.a a) {
        return null;
    }
    
    public void openOptionsMenu() {
        final t1 n = this.n();
        if (this.getWindow().hasFeature(0) && (n == null || !n.p())) {
            super.openOptionsMenu();
        }
    }
    
    public final void p() {
        o42.a(this.getWindow().getDecorView(), this);
        r42.a(this.getWindow().getDecorView(), this);
        q42.a(this.getWindow().getDecorView(), this);
        p42.a(this.getWindow().getDecorView(), this);
    }
    
    public void q(final ou1 ou1) {
        ou1.b(this);
    }
    
    public void r(final nl0 nl0) {
    }
    
    public void s(final int n) {
    }
    
    @Override
    public void setContentView(final int n) {
        this.p();
        this.m().F(n);
    }
    
    @Override
    public void setContentView(final View view) {
        this.p();
        this.m().G(view);
    }
    
    @Override
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.p();
        this.m().H(view, viewGroup$LayoutParams);
    }
    
    public void setTheme(final int theme) {
        super.setTheme(theme);
        this.m().K(theme);
    }
    
    @Override
    public void supportInvalidateOptionsMenu() {
        this.m().r();
    }
    
    public void t(final ou1 ou1) {
    }
    
    public void u() {
    }
    
    public boolean v() {
        final Intent b = this.b();
        if (b != null) {
            if (this.z(b)) {
                final ou1 g = ou1.g((Context)this);
                this.q(g);
                this.t(g);
                g.i();
                try {
                    h2.b(this);
                }
                catch (final IllegalStateException ex) {
                    this.finish();
                }
            }
            else {
                this.y(b);
            }
            return true;
        }
        return false;
    }
    
    public final boolean w(final KeyEvent keyEvent) {
        if (Build$VERSION.SDK_INT < 26 && !keyEvent.isCtrlPressed() && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode())) {
            final Window window = this.getWindow();
            if (window != null && window.getDecorView() != null && window.getDecorView().dispatchKeyShortcutEvent(keyEvent)) {
                return true;
            }
        }
        return false;
    }
    
    public void x(final Toolbar toolbar) {
        this.m().J(toolbar);
    }
    
    public void y(final Intent intent) {
        hy0.e(this, intent);
    }
    
    public boolean z(final Intent intent) {
        return hy0.f(this, intent);
    }
}

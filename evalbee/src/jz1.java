import com.google.gson.reflect.TypeToken;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.Type;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jz1 extends hz1
{
    public final gc0 a;
    public final hz1 b;
    public final Type c;
    
    public jz1(final gc0 a, final hz1 b, final Type c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static Type e(final Type type, final Object o) {
        Type class1 = type;
        if (o != null) {
            if (!(type instanceof Class)) {
                class1 = type;
                if (!(type instanceof TypeVariable)) {
                    return class1;
                }
            }
            class1 = o.getClass();
        }
        return class1;
    }
    
    public static boolean f(hz1 hz1) {
        while (hz1 instanceof ll1) {
            final hz1 e = ((ll1)hz1).e();
            if (e == hz1) {
                break;
            }
            hz1 = e;
        }
        return hz1 instanceof dd1.b;
    }
    
    @Override
    public Object b(final rh0 rh0) {
        return this.b.b(rh0);
    }
    
    @Override
    public void d(final vh0 vh0, final Object o) {
        hz1 hz1 = this.b;
        final Type e = e(this.c, o);
        if (e != this.c) {
            hz1 = this.a.l(TypeToken.get(e));
            if (hz1 instanceof dd1.b) {
                if (!f(this.b)) {
                    hz1 = this.b;
                }
            }
        }
        hz1.d(vh0, o);
    }
}

import java.util.Date;
import android.util.Base64;
import java.util.Iterator;
import java.util.Collection;
import com.google.firebase.encoders.EncodingException;
import java.io.Writer;
import java.util.Map;
import android.util.JsonWriter;

// 
// Decompiled by Procyon v0.6.0
// 

public final class uh0 implements x01, y22
{
    public uh0 a;
    public boolean b;
    public final JsonWriter c;
    public final Map d;
    public final Map e;
    public final w01 f;
    public final boolean g;
    
    public uh0(final Writer writer, final Map d, final Map e, final w01 f, final boolean g) {
        this.a = null;
        this.b = true;
        this.c = new JsonWriter(writer);
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
    
    @Override
    public x01 b(final n00 n00, final boolean b) {
        return this.p(n00.b(), b);
    }
    
    @Override
    public x01 c(final n00 n00, final int n2) {
        return this.m(n00.b(), n2);
    }
    
    @Override
    public x01 d(final n00 n00, final double n2) {
        return this.l(n00.b(), n2);
    }
    
    @Override
    public x01 e(final n00 n00, final long n2) {
        return this.n(n00.b(), n2);
    }
    
    @Override
    public x01 f(final n00 n00, final Object o) {
        return this.o(n00.b(), o);
    }
    
    public uh0 g(final double n) {
        this.x();
        this.c.value(n);
        return this;
    }
    
    public uh0 h(final int n) {
        this.x();
        this.c.value((long)n);
        return this;
    }
    
    public uh0 i(final long n) {
        this.x();
        this.c.value(n);
        return this;
    }
    
    public uh0 j(Object key, final boolean b) {
        final int n = 0;
        int i = 0;
        final int n2 = 0;
        final int n3 = 0;
        if (b && this.s(key)) {
            Object class1;
            if (key == null) {
                class1 = null;
            }
            else {
                class1 = key.getClass();
            }
            throw new EncodingException(String.format("%s cannot be encoded inline", class1));
        }
        if (key == null) {
            this.c.nullValue();
            return this;
        }
        if (key instanceof Number) {
            this.c.value((Number)key);
            return this;
        }
        if (key.getClass().isArray()) {
            if (key instanceof byte[]) {
                return this.r((byte[])key);
            }
            this.c.beginArray();
            if (key instanceof int[]) {
                final int[] array = (int[])key;
                for (int length = array.length, j = n3; j < length; ++j) {
                    this.c.value((long)array[j]);
                }
            }
            else if (key instanceof long[]) {
                final long[] array2 = (long[])key;
                for (int length2 = array2.length, k = n; k < length2; ++k) {
                    this.i(array2[k]);
                }
            }
            else if (key instanceof double[]) {
                for (double[] array3 = (double[])key; i < array3.length; ++i) {
                    this.c.value(array3[i]);
                }
            }
            else if (key instanceof boolean[]) {
                final boolean[] array4 = (boolean[])key;
                for (int length3 = array4.length, l = n2; l < length3; ++l) {
                    this.c.value(array4[l]);
                }
            }
            else if (key instanceof Number[]) {
                final Number[] array5 = (Number[])key;
                for (int length4 = array5.length, n4 = 0; n4 < length4; ++n4) {
                    this.j(array5[n4], false);
                }
            }
            else {
                final Object[] array6 = (Object[])key;
                for (int length5 = array6.length, n5 = 0; n5 < length5; ++n5) {
                    this.j(array6[n5], false);
                }
            }
            this.c.endArray();
            return this;
        }
        else {
            if (key instanceof Collection) {
                final Collection collection = (Collection)key;
                this.c.beginArray();
                final Iterator iterator = collection.iterator();
                while (iterator.hasNext()) {
                    this.j(iterator.next(), false);
                }
                this.c.endArray();
                return this;
            }
            if (key instanceof Map) {
                final Map map = (Map)key;
                this.c.beginObject();
                for (final Map.Entry<Object, V> entry : map.entrySet()) {
                    key = entry.getKey();
                    try {
                        this.o((String)key, entry.getValue());
                        continue;
                    }
                    catch (final ClassCastException ex) {
                        throw new EncodingException(String.format("Only String keys are currently supported in maps, got %s of type %s instead.", key, key.getClass()), ex);
                    }
                    break;
                }
                this.c.endObject();
                return this;
            }
            final w01 w01 = this.d.get(key.getClass());
            if (w01 != null) {
                return this.u(w01, key, b);
            }
            final x22 x22 = this.e.get(key.getClass());
            if (x22 != null) {
                x22.encode(key, this);
                return this;
            }
            if (key instanceof Enum) {
                if (key instanceof p01) {
                    this.h(((p01)key).getNumber());
                }
                else {
                    this.k(((Enum)key).name());
                }
                return this;
            }
            return this.u(this.f, key, b);
        }
    }
    
    public uh0 k(final String s) {
        this.x();
        this.c.value(s);
        return this;
    }
    
    public uh0 l(final String s, final double n) {
        this.x();
        this.c.name(s);
        return this.g(n);
    }
    
    public uh0 m(final String s, final int n) {
        this.x();
        this.c.name(s);
        return this.h(n);
    }
    
    public uh0 n(final String s, final long n) {
        this.x();
        this.c.name(s);
        return this.i(n);
    }
    
    public uh0 o(final String s, final Object o) {
        if (this.g) {
            return this.w(s, o);
        }
        return this.v(s, o);
    }
    
    public uh0 p(final String s, final boolean b) {
        this.x();
        this.c.name(s);
        return this.q(b);
    }
    
    public uh0 q(final boolean b) {
        this.x();
        this.c.value(b);
        return this;
    }
    
    public uh0 r(final byte[] array) {
        this.x();
        if (array == null) {
            this.c.nullValue();
        }
        else {
            this.c.value(Base64.encodeToString(array, 2));
        }
        return this;
    }
    
    public final boolean s(final Object o) {
        return o == null || o.getClass().isArray() || o instanceof Collection || o instanceof Date || o instanceof Enum || o instanceof Number;
    }
    
    public void t() {
        this.x();
        this.c.flush();
    }
    
    public uh0 u(final w01 w01, final Object o, final boolean b) {
        if (!b) {
            this.c.beginObject();
        }
        w01.encode(o, this);
        if (!b) {
            this.c.endObject();
        }
        return this;
    }
    
    public final uh0 v(final String s, final Object o) {
        this.x();
        this.c.name(s);
        if (o == null) {
            this.c.nullValue();
            return this;
        }
        return this.j(o, false);
    }
    
    public final uh0 w(final String s, final Object o) {
        if (o == null) {
            return this;
        }
        this.x();
        this.c.name(s);
        return this.j(o, false);
    }
    
    public final void x() {
        if (this.b) {
            final uh0 a = this.a;
            if (a != null) {
                a.x();
                this.a.b = false;
                this.a = null;
                this.c.endObject();
            }
            return;
        }
        throw new IllegalStateException("Parent context used since this context was created. Cannot use this context anymore.");
    }
}

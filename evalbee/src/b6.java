import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.ActionMode$Callback;
import android.widget.TextView;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CheckedTextView;

// 
// Decompiled by Procyon v0.6.0
// 

public class b6 extends CheckedTextView
{
    public final c6 a;
    public final x5 b;
    public final l7 c;
    public v6 d;
    
    public b6(final Context context, final AttributeSet set) {
        this(context, set, sa1.p);
    }
    
    public b6(final Context context, final AttributeSet set, final int n) {
        super(qw1.b(context), set, n);
        pv1.a((View)this, ((View)this).getContext());
        final l7 c = new l7((TextView)this);
        (this.c = c).m(set, n);
        c.b();
        (this.b = new x5((View)this)).e(set, n);
        (this.a = new c6(this)).d(set, n);
        this.getEmojiTextViewHelper().c(set, n);
    }
    
    private v6 getEmojiTextViewHelper() {
        if (this.d == null) {
            this.d = new v6((TextView)this);
        }
        return this.d;
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final l7 c = this.c;
        if (c != null) {
            c.b();
        }
        final x5 b = this.b;
        if (b != null) {
            b.b();
        }
        final c6 a = this.a;
        if (a != null) {
            a.a();
        }
    }
    
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return nv1.q(super.getCustomSelectionActionModeCallback());
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 b = this.b;
        ColorStateList c;
        if (b != null) {
            c = b.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 b = this.b;
        PorterDuff$Mode d;
        if (b != null) {
            d = b.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public ColorStateList getSupportCheckMarkTintList() {
        final c6 a = this.a;
        ColorStateList b;
        if (a != null) {
            b = a.b();
        }
        else {
            b = null;
        }
        return b;
    }
    
    public PorterDuff$Mode getSupportCheckMarkTintMode() {
        final c6 a = this.a;
        PorterDuff$Mode c;
        if (a != null) {
            c = a.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.c.j();
    }
    
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.c.k();
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        return w6.a(super.onCreateInputConnection(editorInfo), editorInfo, (View)this);
    }
    
    public void setAllCaps(final boolean allCaps) {
        super.setAllCaps(allCaps);
        this.getEmojiTextViewHelper().d(allCaps);
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 b = this.b;
        if (b != null) {
            b.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 b = this.b;
        if (b != null) {
            b.g(backgroundResource);
        }
    }
    
    public void setCheckMarkDrawable(final int n) {
        this.setCheckMarkDrawable(g7.b(((View)this).getContext(), n));
    }
    
    public void setCheckMarkDrawable(final Drawable checkMarkDrawable) {
        super.setCheckMarkDrawable(checkMarkDrawable);
        final c6 a = this.a;
        if (a != null) {
            a.e();
        }
    }
    
    public void setCompoundDrawables(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        final l7 c = this.c;
        if (c != null) {
            c.p();
        }
    }
    
    public void setCompoundDrawablesRelative(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        final l7 c = this.c;
        if (c != null) {
            c.p();
        }
    }
    
    public void setCustomSelectionActionModeCallback(final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(nv1.r((TextView)this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean b) {
        this.getEmojiTextViewHelper().e(b);
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 b = this.b;
        if (b != null) {
            b.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 b = this.b;
        if (b != null) {
            b.j(porterDuff$Mode);
        }
    }
    
    public void setSupportCheckMarkTintList(final ColorStateList list) {
        final c6 a = this.a;
        if (a != null) {
            a.f(list);
        }
    }
    
    public void setSupportCheckMarkTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final c6 a = this.a;
        if (a != null) {
            a.g(porterDuff$Mode);
        }
    }
    
    public void setSupportCompoundDrawablesTintList(final ColorStateList list) {
        this.c.w(list);
        this.c.b();
    }
    
    public void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode porterDuff$Mode) {
        this.c.x(porterDuff$Mode);
        this.c.b();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final l7 c = this.c;
        if (c != null) {
            c.q(context, n);
        }
    }
}

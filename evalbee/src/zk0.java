import com.google.protobuf.GeneratedMessageLite;
import com.google.firebase.firestore.proto.b;
import com.google.firebase.firestore.core.q;
import com.google.protobuf.ByteString;
import com.google.firebase.firestore.local.QueryPurpose;
import com.google.firebase.firestore.proto.Target;
import com.google.firebase.firestore.proto.a;
import com.google.firestore.v1.DocumentTransform;
import com.google.firestore.v1.Write;
import com.google.firebase.firestore.proto.MaybeDocument;
import java.util.Iterator;
import com.google.firebase.firestore.model.FieldIndex;
import java.util.ArrayList;
import java.util.List;
import com.google.firestore.admin.v1.Index;
import com.google.firebase.firestore.model.MutableDocument;
import com.google.firestore.v1.e;
import com.google.firebase.firestore.remote.f;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zk0
{
    public final f a;
    
    public zk0(final f a) {
        this.a = a;
    }
    
    public final MutableDocument a(final e e, final boolean b) {
        MutableDocument mutableDocument = MutableDocument.p(this.a.k(e.g0()), this.a.v(e.h0()), a11.i(e.e0()));
        if (b) {
            mutableDocument = mutableDocument.t();
        }
        return mutableDocument;
    }
    
    public List b(final Index index) {
        final ArrayList list = new ArrayList();
        for (final Index.IndexField indexField : index.e0()) {
            final s00 q = s00.q(indexField.d0());
            FieldIndex.Segment.Kind kind;
            if (indexField.f0().equals(Index.IndexField.ValueModeCase.ARRAY_CONFIG)) {
                kind = FieldIndex.Segment.Kind.CONTAINS;
            }
            else if (indexField.e0().equals(Index.IndexField.Order.ASCENDING)) {
                kind = FieldIndex.Segment.Kind.ASCENDING;
            }
            else {
                kind = FieldIndex.Segment.Kind.DESCENDING;
            }
            list.add(FieldIndex.Segment.c(q, kind));
        }
        return list;
    }
    
    public MutableDocument c(final MaybeDocument maybeDocument) {
        final int n = zk0$a.a[maybeDocument.f0().ordinal()];
        if (n == 1) {
            return this.a(maybeDocument.e0(), maybeDocument.g0());
        }
        if (n == 2) {
            return this.f(maybeDocument.h0(), maybeDocument.g0());
        }
        if (n == 3) {
            return this.h(maybeDocument.i0());
        }
        throw g9.a("Unknown MaybeDocument %s", maybeDocument);
    }
    
    public wx0 d(final Write write) {
        return this.a.l(write);
    }
    
    public xx0 e(final ea2 ea2) {
        final int k0 = ea2.k0();
        final pw1 t = this.a.t(ea2.l0());
        final int j0 = ea2.j0();
        final ArrayList list = new ArrayList(j0);
        for (int i = 0; i < j0; ++i) {
            list.add((Object)this.a.l(ea2.i0(i)));
        }
        final ArrayList list2 = new ArrayList(ea2.n0());
        for (int l = 0; l < ea2.n0(); ++l) {
            final Write m0 = ea2.m0(l);
            final int n = l + 1;
            if (n < ea2.n0() && ea2.m0(n).r0()) {
                g9.d(ea2.m0(l).s0(), "TransformMutation should be preceded by a patch or set mutation", new Object[0]);
                final Write.b v0 = Write.v0(m0);
                final Iterator iterator = ea2.m0(n).l0().b0().iterator();
                while (iterator.hasNext()) {
                    v0.A((DocumentTransform.FieldTransform)iterator.next());
                }
                list2.add(this.a.l((Write)((GeneratedMessageLite.a)v0).p()));
                l = n;
            }
            else {
                list2.add(this.a.l(m0));
            }
        }
        return new xx0(k0, t, list, list2);
    }
    
    public final MutableDocument f(final a a, final boolean b) {
        MutableDocument mutableDocument = MutableDocument.r(this.a.k(a.d0()), this.a.v(a.e0()));
        if (b) {
            mutableDocument = mutableDocument.t();
        }
        return mutableDocument;
    }
    
    public au1 g(final Target target) {
        final int p = target.p0();
        final qo1 v = this.a.v(target.o0());
        final qo1 v2 = this.a.v(target.k0());
        final ByteString n0 = target.n0();
        final long l0 = target.l0();
        final int n2 = zk0$a.b[target.q0().ordinal()];
        q q;
        if (n2 != 1) {
            if (n2 != 2) {
                throw g9.a("Unknown targetType %d", target.q0());
            }
            q = this.a.q(target.m0());
        }
        else {
            q = this.a.e(target.j0());
        }
        return new au1(q, p, l0, QueryPurpose.LISTEN, v, v2, n0, null);
    }
    
    public final MutableDocument h(final b b) {
        return MutableDocument.s(this.a.k(b.d0()), this.a.v(b.e0()));
    }
    
    public final e i(final zt zt) {
        final e.b k0 = e.k0();
        k0.B(this.a.I(zt.getKey()));
        k0.A(zt.getData().l());
        k0.C(this.a.S(zt.getVersion().c()));
        return (e)((GeneratedMessageLite.a)k0).p();
    }
    
    public Index j(final List list) {
        final Index.b f0 = Index.f0();
        f0.B(Index.QueryScope.COLLECTION_GROUP);
        for (final FieldIndex.Segment segment : list) {
            final Index.IndexField.a g0 = Index.IndexField.g0();
            g0.B(segment.d().d());
            if (segment.e() == FieldIndex.Segment.Kind.CONTAINS) {
                g0.A(Index.IndexField.ArrayConfig.CONTAINS);
            }
            else {
                Index.IndexField.Order order;
                if (segment.e() == FieldIndex.Segment.Kind.ASCENDING) {
                    order = Index.IndexField.Order.ASCENDING;
                }
                else {
                    order = Index.IndexField.Order.DESCENDING;
                }
                g0.C(order);
            }
            f0.A(g0);
        }
        return (Index)((GeneratedMessageLite.a)f0).p();
    }
    
    public MaybeDocument k(final zt zt) {
        final MaybeDocument.b j0 = MaybeDocument.j0();
        if (zt.c()) {
            j0.C(this.n(zt));
        }
        else if (zt.d()) {
            j0.A(this.i(zt));
        }
        else {
            if (!zt.i()) {
                throw g9.a("Cannot encode invalid document %s", zt);
            }
            j0.D(this.p(zt));
        }
        j0.B(zt.f());
        return (MaybeDocument)((GeneratedMessageLite.a)j0).p();
    }
    
    public Write l(final wx0 wx0) {
        return this.a.L(wx0);
    }
    
    public ea2 m(final xx0 xx0) {
        final ea2.b o0 = ea2.o0();
        o0.C(xx0.e());
        o0.D(this.a.S(xx0.g()));
        final Iterator iterator = xx0.d().iterator();
        while (iterator.hasNext()) {
            o0.A(this.a.L((wx0)iterator.next()));
        }
        final Iterator iterator2 = xx0.h().iterator();
        while (iterator2.hasNext()) {
            o0.B(this.a.L((wx0)iterator2.next()));
        }
        return (ea2)((GeneratedMessageLite.a)o0).p();
    }
    
    public final a n(final zt zt) {
        final a.b f0 = com.google.firebase.firestore.proto.a.f0();
        f0.A(this.a.I(zt.getKey()));
        f0.B(this.a.S(zt.getVersion().c()));
        return (a)((GeneratedMessageLite.a)f0).p();
    }
    
    public Target o(final au1 au1) {
        final QueryPurpose listen = QueryPurpose.LISTEN;
        g9.d(listen.equals(au1.c()), "Only queries with purpose %s may be stored, got %s", listen, au1.c());
        final Target.b r0 = Target.r0();
        r0.H(au1.h()).D(au1.e()).C(this.a.U(au1.b())).G(this.a.U(au1.f())).F(au1.d());
        final q g = au1.g();
        if (g.s()) {
            r0.B(this.a.C(g));
        }
        else {
            r0.E(this.a.P(g));
        }
        return (Target)((GeneratedMessageLite.a)r0).p();
    }
    
    public final b p(final zt zt) {
        final b.b f0 = b.f0();
        f0.A(this.a.I(zt.getKey()));
        f0.B(this.a.S(zt.getVersion().c()));
        return (b)((GeneratedMessageLite.a)f0).p();
    }
}

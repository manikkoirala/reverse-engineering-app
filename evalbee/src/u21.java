import android.database.Observable;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.database.DataSetObserver;
import android.database.DataSetObservable;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class u21
{
    public final DataSetObservable a;
    public DataSetObserver b;
    
    public u21() {
        this.a = new DataSetObservable();
    }
    
    public abstract void a(final ViewGroup p0, final int p1, final Object p2);
    
    public abstract void b(final ViewGroup p0);
    
    public abstract int c();
    
    public int d(final Object o) {
        return -1;
    }
    
    public abstract CharSequence e(final int p0);
    
    public float f(final int n) {
        return 1.0f;
    }
    
    public abstract Object g(final ViewGroup p0, final int p1);
    
    public abstract boolean h(final View p0, final Object p1);
    
    public void i(final DataSetObserver dataSetObserver) {
        ((Observable)this.a).registerObserver((Object)dataSetObserver);
    }
    
    public abstract void j(final Parcelable p0, final ClassLoader p1);
    
    public abstract Parcelable k();
    
    public abstract void l(final ViewGroup p0, final int p1, final Object p2);
    
    public void m(final DataSetObserver b) {
        synchronized (this) {
            this.b = b;
        }
    }
    
    public abstract void n(final ViewGroup p0);
    
    public void o(final DataSetObserver dataSetObserver) {
        ((Observable)this.a).unregisterObserver((Object)dataSetObserver);
    }
}

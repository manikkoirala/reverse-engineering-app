import com.google.android.gms.common.internal.Objects;
import android.util.Log;
import android.text.TextUtils;
import java.util.regex.Pattern;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jx1
{
    public static final Pattern d;
    public final String a;
    public final String b;
    public final String c;
    
    static {
        d = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");
    }
    
    public jx1(final String s, final String str) {
        this.a = d(str, s);
        this.b = s;
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("!");
        sb.append(str);
        this.c = sb.toString();
    }
    
    public static jx1 a(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        final String[] split = s.split("!", -1);
        if (split.length != 2) {
            return null;
        }
        return new jx1(split[0], split[1]);
    }
    
    public static String d(final String s, final String s2) {
        String substring = s;
        if (s != null) {
            substring = s;
            if (s.startsWith("/topics/")) {
                Log.w("FirebaseMessaging", String.format("Format /topics/topic-name is deprecated. Only 'topic-name' should be used in %s.", s2));
                substring = s.substring(8);
            }
        }
        if (substring != null && jx1.d.matcher(substring).matches()) {
            return substring;
        }
        throw new IllegalArgumentException(String.format("Invalid topic name: %s does not match the allowed format %s.", substring, "[a-zA-Z0-9-_.~%]{1,900}"));
    }
    
    public String b() {
        return this.b;
    }
    
    public String c() {
        return this.a;
    }
    
    public String e() {
        return this.c;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof jx1;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final jx1 jx1 = (jx1)o;
        boolean b3 = b2;
        if (this.a.equals(jx1.a)) {
            b3 = b2;
            if (this.b.equals(jx1.b)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.b, this.a);
    }
}

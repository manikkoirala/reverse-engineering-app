import android.os.LocaleList;
import java.util.Locale;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.util.AttributeSet;
import android.graphics.PorterDuff$Mode;
import android.graphics.drawable.Drawable;
import android.content.res.Resources$NotFoundException;
import java.lang.ref.WeakReference;
import android.os.Build$VERSION;
import android.content.res.ColorStateList;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

// 
// Decompiled by Procyon v0.6.0
// 

public class l7
{
    public final TextView a;
    public rw1 b;
    public rw1 c;
    public rw1 d;
    public rw1 e;
    public rw1 f;
    public rw1 g;
    public rw1 h;
    public final n7 i;
    public int j;
    public int k;
    public Typeface l;
    public boolean m;
    
    public l7(final TextView a) {
        this.j = 0;
        this.k = -1;
        this.a = a;
        this.i = new n7(a);
    }
    
    public static rw1 d(final Context context, final s6 s6, final int n) {
        final ColorStateList f = s6.f(context, n);
        if (f != null) {
            final rw1 rw1 = new rw1();
            rw1.d = true;
            rw1.a = f;
            return rw1;
        }
        return null;
    }
    
    public void A(final int n, final float n2) {
        if (!u42.b && !this.l()) {
            this.B(n, n2);
        }
    }
    
    public final void B(final int n, final float n2) {
        this.i.t(n, n2);
    }
    
    public final void C(final Context context, final tw1 tw1) {
        this.j = tw1.k(bc1.d3, this.j);
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        if (sdk_INT >= 28 && (this.k = tw1.k(bc1.g3, -1)) != -1) {
            this.j = ((this.j & 0x2) | 0x0);
        }
        int n = bc1.f3;
        if (!tw1.s(n) && !tw1.s(bc1.h3)) {
            final int c3 = bc1.c3;
            if (tw1.s(c3)) {
                this.m = false;
                final int k = tw1.k(c3, 1);
                Typeface l;
                if (k != 1) {
                    if (k != 2) {
                        if (k != 3) {
                            return;
                        }
                        l = Typeface.MONOSPACE;
                    }
                    else {
                        l = Typeface.SERIF;
                    }
                }
                else {
                    l = Typeface.SANS_SERIF;
                }
                this.l = l;
            }
            return;
        }
        this.l = null;
        final int h3 = bc1.h3;
        if (tw1.s(h3)) {
            n = h3;
        }
        final int i = this.k;
        final int j = this.j;
        while (true) {
            if (context.isRestricted()) {
                break Label_0322;
            }
            final le1.e e = new le1.e(this, i, j, new WeakReference((T)this.a)) {
                public final int a;
                public final int b;
                public final WeakReference c;
                public final l7 d;
                
                @Override
                public void onFontRetrievalFailed(final int n) {
                }
                
                @Override
                public void onFontRetrieved(final Typeface typeface) {
                    Typeface a = typeface;
                    if (Build$VERSION.SDK_INT >= 28) {
                        final int a2 = this.a;
                        a = typeface;
                        if (a2 != -1) {
                            a = l7.f.a(typeface, a2, (this.b & 0x2) != 0x0);
                        }
                    }
                    this.d.n(this.c, a);
                }
            };
            try {
                final Typeface m = tw1.j(n, this.j, e);
                if (m != null) {
                    Typeface a = m;
                    if (sdk_INT >= 28) {
                        a = m;
                        if (this.k != -1) {
                            a = l7.f.a(Typeface.create(m, 0), this.k, (this.j & 0x2) != 0x0);
                        }
                    }
                    this.l = a;
                }
                this.m = (this.l == null);
                if (this.l == null) {
                    final String o = tw1.o(n);
                    if (o != null) {
                        Typeface l2;
                        if (Build$VERSION.SDK_INT >= 28 && this.k != -1) {
                            final Typeface create = Typeface.create(o, 0);
                            n = this.k;
                            boolean b2 = b;
                            if ((this.j & 0x2) != 0x0) {
                                b2 = true;
                            }
                            l2 = l7.f.a(create, n, b2);
                        }
                        else {
                            l2 = Typeface.create(o, this.j);
                        }
                        this.l = l2;
                    }
                }
            }
            catch (final UnsupportedOperationException | Resources$NotFoundException ex) {
                continue;
            }
            break;
        }
    }
    
    public final void a(final Drawable drawable, final rw1 rw1) {
        if (drawable != null && rw1 != null) {
            s6.i(drawable, rw1, ((View)this.a).getDrawableState());
        }
    }
    
    public void b() {
        if (this.b != null || this.c != null || this.d != null || this.e != null) {
            final Drawable[] compoundDrawables = this.a.getCompoundDrawables();
            this.a(compoundDrawables[0], this.b);
            this.a(compoundDrawables[1], this.c);
            this.a(compoundDrawables[2], this.d);
            this.a(compoundDrawables[3], this.e);
        }
        if (this.f != null || this.g != null) {
            final Drawable[] a = l7.c.a(this.a);
            this.a(a[0], this.f);
            this.a(a[2], this.g);
        }
    }
    
    public void c() {
        this.i.a();
    }
    
    public int e() {
        return this.i.f();
    }
    
    public int f() {
        return this.i.g();
    }
    
    public int g() {
        return this.i.h();
    }
    
    public int[] h() {
        return this.i.i();
    }
    
    public int i() {
        return this.i.j();
    }
    
    public ColorStateList j() {
        final rw1 h = this.h;
        ColorStateList a;
        if (h != null) {
            a = h.a;
        }
        else {
            a = null;
        }
        return a;
    }
    
    public PorterDuff$Mode k() {
        final rw1 h = this.h;
        PorterDuff$Mode b;
        if (h != null) {
            b = h.b;
        }
        else {
            b = null;
        }
        return b;
    }
    
    public boolean l() {
        return this.i.n();
    }
    
    public void m(final AttributeSet set, int n) {
        final Context context = ((View)this.a).getContext();
        final s6 b = s6.b();
        final int[] y = bc1.Y;
        final tw1 v = tw1.v(context, set, y, n, 0);
        final TextView a = this.a;
        o32.o0((View)a, ((View)a).getContext(), y, set, v.r(), n, 0);
        final int n2 = v.n(bc1.Z, -1);
        final int c0 = bc1.c0;
        if (v.s(c0)) {
            this.b = d(context, b, v.n(c0, 0));
        }
        final int a2 = bc1.a0;
        if (v.s(a2)) {
            this.c = d(context, b, v.n(a2, 0));
        }
        final int d0 = bc1.d0;
        if (v.s(d0)) {
            this.d = d(context, b, v.n(d0, 0));
        }
        final int b2 = bc1.b0;
        if (v.s(b2)) {
            this.e = d(context, b, v.n(b2, 0));
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int e0 = bc1.e0;
        if (v.s(e0)) {
            this.f = d(context, b, v.n(e0, 0));
        }
        final int f0 = bc1.f0;
        if (v.s(f0)) {
            this.g = d(context, b, v.n(f0, 0));
        }
        v.w();
        final boolean b3 = this.a.getTransformationMethod() instanceof PasswordTransformationMethod;
        boolean b4;
        boolean b5;
        String s;
        String o;
        if (n2 != -1) {
            final tw1 t = tw1.t(context, n2, bc1.a3);
            Label_0346: {
                if (!b3) {
                    final int j3 = bc1.j3;
                    if (t.s(j3)) {
                        b4 = t.a(j3, false);
                        b5 = true;
                        break Label_0346;
                    }
                }
                b4 = false;
                b5 = false;
            }
            this.C(context, t);
            final int k3 = bc1.k3;
            if (t.s(k3)) {
                s = t.o(k3);
            }
            else {
                s = null;
            }
            Label_0421: {
                if (sdk_INT >= 26) {
                    final int i3 = bc1.i3;
                    if (t.s(i3)) {
                        o = t.o(i3);
                        break Label_0421;
                    }
                }
                o = null;
            }
            t.w();
        }
        else {
            b4 = false;
            b5 = false;
            o = null;
            s = null;
        }
        final tw1 v2 = tw1.v(context, set, bc1.a3, n, 0);
        if (!b3) {
            final int j4 = bc1.j3;
            if (v2.s(j4)) {
                b4 = v2.a(j4, false);
                b5 = true;
            }
        }
        final int k4 = bc1.k3;
        if (v2.s(k4)) {
            s = v2.o(k4);
        }
        String o2 = o;
        if (sdk_INT >= 26) {
            final int i4 = bc1.i3;
            o2 = o;
            if (v2.s(i4)) {
                o2 = v2.o(i4);
            }
        }
        if (sdk_INT >= 28) {
            final int b6 = bc1.b3;
            if (v2.s(b6) && v2.f(b6, -1) == 0) {
                this.a.setTextSize(0, 0.0f);
            }
        }
        this.C(context, v2);
        v2.w();
        if (!b3 && b5) {
            this.s(b4);
        }
        final Typeface l = this.l;
        if (l != null) {
            if (this.k == -1) {
                this.a.setTypeface(l, this.j);
            }
            else {
                this.a.setTypeface(l);
            }
        }
        if (o2 != null) {
            l7.e.d(this.a, o2);
        }
        if (s != null) {
            l7.d.b(this.a, l7.d.a(s));
        }
        this.i.o(set, n);
        if (u42.b && this.i.j() != 0) {
            final int[] m = this.i.i();
            if (m.length > 0) {
                if (l7.e.a(this.a) != -1.0f) {
                    l7.e.b(this.a, this.i.g(), this.i.f(), this.i.h(), 0);
                }
                else {
                    l7.e.c(this.a, m, 0);
                }
            }
        }
        final tw1 u = tw1.u(context, set, bc1.g0);
        n = u.n(bc1.o0, -1);
        Drawable c2;
        if (n != -1) {
            c2 = b.c(context, n);
        }
        else {
            c2 = null;
        }
        n = u.n(bc1.t0, -1);
        Drawable c3;
        if (n != -1) {
            c3 = b.c(context, n);
        }
        else {
            c3 = null;
        }
        n = u.n(bc1.p0, -1);
        Drawable c4;
        if (n != -1) {
            c4 = b.c(context, n);
        }
        else {
            c4 = null;
        }
        n = u.n(bc1.m0, -1);
        Drawable c5;
        if (n != -1) {
            c5 = b.c(context, n);
        }
        else {
            c5 = null;
        }
        n = u.n(bc1.q0, -1);
        Drawable c6;
        if (n != -1) {
            c6 = b.c(context, n);
        }
        else {
            c6 = null;
        }
        n = u.n(bc1.n0, -1);
        Drawable c7;
        if (n != -1) {
            c7 = b.c(context, n);
        }
        else {
            c7 = null;
        }
        this.y(c2, c3, c4, c5, c6, c7);
        n = bc1.r0;
        if (u.s(n)) {
            nv1.h(this.a, u.c(n));
        }
        n = bc1.s0;
        if (u.s(n)) {
            nv1.i(this.a, fv.e(u.k(n, -1), null));
        }
        final int f2 = u.f(bc1.v0, -1);
        final int f3 = u.f(bc1.w0, -1);
        n = u.f(bc1.x0, -1);
        u.w();
        if (f2 != -1) {
            nv1.k(this.a, f2);
        }
        if (f3 != -1) {
            nv1.l(this.a, f3);
        }
        if (n != -1) {
            nv1.m(this.a, n);
        }
    }
    
    public void n(final WeakReference weakReference, final Typeface l) {
        if (this.m) {
            this.l = l;
            final TextView textView = (TextView)weakReference.get();
            if (textView != null) {
                if (o32.T((View)textView)) {
                    ((View)textView).post((Runnable)new Runnable(this, textView, l, this.j) {
                        public final TextView a;
                        public final Typeface b;
                        public final int c;
                        public final l7 d;
                        
                        @Override
                        public void run() {
                            this.a.setTypeface(this.b, this.c);
                        }
                    });
                }
                else {
                    textView.setTypeface(l, this.j);
                }
            }
        }
    }
    
    public void o(final boolean b, final int n, final int n2, final int n3, final int n4) {
        if (!u42.b) {
            this.c();
        }
    }
    
    public void p() {
        this.b();
    }
    
    public void q(final Context context, int n) {
        final tw1 t = tw1.t(context, n, bc1.a3);
        n = bc1.j3;
        if (t.s(n)) {
            this.s(t.a(n, false));
        }
        n = Build$VERSION.SDK_INT;
        final int b3 = bc1.b3;
        if (t.s(b3) && t.f(b3, -1) == 0) {
            this.a.setTextSize(0, 0.0f);
        }
        this.C(context, t);
        if (n >= 26) {
            n = bc1.i3;
            if (t.s(n)) {
                final String o = t.o(n);
                if (o != null) {
                    l7.e.d(this.a, o);
                }
            }
        }
        t.w();
        final Typeface l = this.l;
        if (l != null) {
            this.a.setTypeface(l, this.j);
        }
    }
    
    public void r(final TextView textView, final InputConnection inputConnection, final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT < 30 && inputConnection != null) {
            ew.f(editorInfo, textView.getText());
        }
    }
    
    public void s(final boolean allCaps) {
        this.a.setAllCaps(allCaps);
    }
    
    public void t(final int n, final int n2, final int n3, final int n4) {
        this.i.p(n, n2, n3, n4);
    }
    
    public void u(final int[] array, final int n) {
        this.i.q(array, n);
    }
    
    public void v(final int n) {
        this.i.r(n);
    }
    
    public void w(final ColorStateList a) {
        if (this.h == null) {
            this.h = new rw1();
        }
        final rw1 h = this.h;
        h.a = a;
        h.d = (a != null);
        this.z();
    }
    
    public void x(final PorterDuff$Mode b) {
        if (this.h == null) {
            this.h = new rw1();
        }
        final rw1 h = this.h;
        h.b = b;
        h.c = (b != null);
        this.z();
    }
    
    public final void y(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6) {
        if (drawable5 == null && drawable6 == null) {
            if (drawable != null || drawable2 != null || drawable3 != null || drawable4 != null) {
                final Drawable[] a = l7.c.a(this.a);
                drawable5 = a[0];
                if (drawable5 != null || a[2] != null) {
                    final TextView a2 = this.a;
                    if (drawable2 == null) {
                        drawable2 = a[1];
                    }
                    drawable3 = a[2];
                    if (drawable4 == null) {
                        drawable4 = a[3];
                    }
                    l7.c.b(a2, drawable5, drawable2, drawable3, drawable4);
                    return;
                }
                final Drawable[] compoundDrawables = this.a.getCompoundDrawables();
                final TextView a3 = this.a;
                if (drawable == null) {
                    drawable = compoundDrawables[0];
                }
                if (drawable2 == null) {
                    drawable2 = compoundDrawables[1];
                }
                if (drawable3 == null) {
                    drawable3 = compoundDrawables[2];
                }
                if (drawable4 == null) {
                    drawable4 = compoundDrawables[3];
                }
                a3.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            }
        }
        else {
            final Drawable[] a4 = l7.c.a(this.a);
            final TextView a5 = this.a;
            if (drawable5 == null) {
                drawable5 = a4[0];
            }
            if (drawable2 == null) {
                drawable2 = a4[1];
            }
            if (drawable6 == null) {
                drawable6 = a4[2];
            }
            if (drawable4 == null) {
                drawable4 = a4[3];
            }
            l7.c.b(a5, drawable5, drawable2, drawable6, drawable4);
        }
    }
    
    public final void z() {
        final rw1 h = this.h;
        this.b = h;
        this.c = h;
        this.d = h;
        this.e = h;
        this.f = h;
        this.g = h;
    }
    
    public abstract static class c
    {
        public static Drawable[] a(final TextView textView) {
            return textView.getCompoundDrawablesRelative();
        }
        
        public static void b(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        }
        
        public static void c(final TextView textView, final Locale textLocale) {
            textView.setTextLocale(textLocale);
        }
    }
    
    public abstract static class d
    {
        public static LocaleList a(final String s) {
            return LocaleList.forLanguageTags(s);
        }
        
        public static void b(final TextView textView, final LocaleList textLocales) {
            textView.setTextLocales(textLocales);
        }
    }
    
    public abstract static class e
    {
        public static int a(final TextView textView) {
            return textView.getAutoSizeStepGranularity();
        }
        
        public static void b(final TextView textView, final int n, final int n2, final int n3, final int n4) {
            textView.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
        }
        
        public static void c(final TextView textView, final int[] array, final int n) {
            textView.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
        }
        
        public static boolean d(final TextView textView, final String fontVariationSettings) {
            return textView.setFontVariationSettings(fontVariationSettings);
        }
    }
    
    public abstract static class f
    {
        public static Typeface a(final Typeface typeface, final int n, final boolean b) {
            return Typeface.create(typeface, n, b);
        }
    }
}

import com.google.protobuf.k0;
import com.google.protobuf.d0;
import com.google.protobuf.m;
import com.google.protobuf.c0;
import com.google.protobuf.h0;
import com.google.protobuf.v;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.g0;
import com.google.protobuf.ProtoSyntax;
import com.google.protobuf.q;
import com.google.protobuf.t;

// 
// Decompiled by Procyon v0.6.0
// 

public final class um0 implements oj1
{
    public static final vv0 b;
    public final vv0 a;
    
    static {
        b = new vv0() {
            @Override
            public tv0 a(final Class clazz) {
                throw new IllegalStateException("This should never be called.");
            }
            
            @Override
            public boolean b(final Class clazz) {
                return false;
            }
        };
    }
    
    public um0() {
        this(b());
    }
    
    public um0(final vv0 vv0) {
        this.a = (vv0)t.b(vv0, "messageInfoFactory");
    }
    
    public static vv0 b() {
        return new b(new vv0[] { q.c(), c() });
    }
    
    public static vv0 c() {
        try {
            return (vv0)Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
        }
        catch (final Exception ex) {
            return um0.b;
        }
    }
    
    public static boolean d(final tv0 tv0) {
        return tv0.c() == ProtoSyntax.PROTO2;
    }
    
    public static g0 e(final Class clazz, final tv0 tv0) {
        if (GeneratedMessageLite.class.isAssignableFrom(clazz)) {
            c0 c0;
            if (d(tv0)) {
                c0 = com.google.protobuf.c0.U(clazz, tv0, hz0.b(), v.b(), h0.L(), sz.b(), zm0.b());
            }
            else {
                c0 = com.google.protobuf.c0.U(clazz, tv0, hz0.b(), v.b(), h0.L(), null, zm0.b());
            }
            return c0;
        }
        c0 c2;
        if (d(tv0)) {
            c2 = c0.U(clazz, tv0, hz0.a(), v.a(), h0.G(), sz.a(), zm0.a());
        }
        else {
            c2 = c0.U(clazz, tv0, hz0.a(), v.a(), h0.H(), null, zm0.a());
        }
        return c2;
    }
    
    @Override
    public g0 a(final Class clazz) {
        h0.I(clazz);
        final tv0 a = this.a.a(clazz);
        if (a.a()) {
            k0 k0;
            m m;
            if (GeneratedMessageLite.class.isAssignableFrom(clazz)) {
                k0 = h0.L();
                m = sz.b();
            }
            else {
                k0 = h0.G();
                m = sz.a();
            }
            return d0.l(k0, m, a.b());
        }
        return e(clazz, a);
    }
    
    public static class b implements vv0
    {
        public vv0[] a;
        
        public b(final vv0... a) {
            this.a = a;
        }
        
        @Override
        public tv0 a(final Class clazz) {
            for (final vv0 vv0 : this.a) {
                if (vv0.b(clazz)) {
                    return vv0.a(clazz);
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("No factory is available for message type: ");
            sb.append(clazz.getName());
            throw new UnsupportedOperationException(sb.toString());
        }
        
        @Override
        public boolean b(final Class clazz) {
            final vv0[] a = this.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                if (a[i].b(clazz)) {
                    return true;
                }
            }
            return false;
        }
    }
}

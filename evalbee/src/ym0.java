import java.util.NoSuchElementException;
import java.lang.reflect.Array;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ym0
{
    public b a;
    public c b;
    public e c;
    
    public static boolean j(final Map map, final Collection collection) {
        final Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (!map.containsKey(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean k(final Set set, final Object o) {
        boolean b = true;
        if (set == o) {
            return true;
        }
        if (!(o instanceof Set)) {
            return false;
        }
        final Set set2 = (Set)o;
        try {
            if (set.size() != set2.size() || !set.containsAll(set2)) {
                b = false;
            }
            return b;
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    public static boolean o(final Map map, final Collection collection) {
        final int size = map.size();
        final Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            map.remove(iterator.next());
        }
        return size != map.size();
    }
    
    public static boolean p(final Map map, final Collection collection) {
        final int size = map.size();
        final Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            if (!collection.contains(iterator.next())) {
                iterator.remove();
            }
        }
        return size != map.size();
    }
    
    public abstract void a();
    
    public abstract Object b(final int p0, final int p1);
    
    public abstract Map c();
    
    public abstract int d();
    
    public abstract int e(final Object p0);
    
    public abstract int f(final Object p0);
    
    public abstract void g(final Object p0, final Object p1);
    
    public abstract void h(final int p0);
    
    public abstract Object i(final int p0, final Object p1);
    
    public Set l() {
        if (this.a == null) {
            this.a = new b();
        }
        return this.a;
    }
    
    public Set m() {
        if (this.b == null) {
            this.b = new c();
        }
        return this.b;
    }
    
    public Collection n() {
        if (this.c == null) {
            this.c = new e();
        }
        return this.c;
    }
    
    public Object[] q(final int n) {
        final int d = this.d();
        final Object[] array = new Object[d];
        for (int i = 0; i < d; ++i) {
            array[i] = this.b(i, n);
        }
        return array;
    }
    
    public Object[] r(final Object[] array, final int n) {
        final int d = this.d();
        Object[] array2 = array;
        if (array.length < d) {
            array2 = (Object[])Array.newInstance(array.getClass().getComponentType(), d);
        }
        for (int i = 0; i < d; ++i) {
            array2[i] = this.b(i, n);
        }
        if (array2.length > d) {
            array2[d] = null;
        }
        return array2;
    }
    
    public final class a implements Iterator
    {
        public final int a;
        public int b;
        public int c;
        public boolean d;
        public final ym0 e;
        
        public a(final ym0 e, final int a) {
            this.e = e;
            this.d = false;
            this.a = a;
            this.b = e.d();
        }
        
        @Override
        public boolean hasNext() {
            return this.c < this.b;
        }
        
        @Override
        public Object next() {
            if (this.hasNext()) {
                final Object b = this.e.b(this.c, this.a);
                ++this.c;
                this.d = true;
                return b;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            if (this.d) {
                final int c = this.c - 1;
                this.c = c;
                --this.b;
                this.d = false;
                this.e.h(c);
                return;
            }
            throw new IllegalStateException();
        }
    }
    
    public final class b implements Set
    {
        public final ym0 a;
        
        public b(final ym0 a) {
            this.a = a;
        }
        
        public boolean a(final Map.Entry entry) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean addAll(final Collection collection) {
            final int d = this.a.d();
            for (final Map.Entry<Object, V> entry : collection) {
                this.a.g(entry.getKey(), entry.getValue());
            }
            return d != this.a.d();
        }
        
        @Override
        public void clear() {
            this.a.a();
        }
        
        @Override
        public boolean contains(final Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            final Map.Entry entry = (Map.Entry)o;
            final int e = this.a.e(entry.getKey());
            return e >= 0 && el.c(this.a.b(e, 1), entry.getValue());
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                if (!this.contains(iterator.next())) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public boolean equals(final Object o) {
            return ym0.k(this, o);
        }
        
        @Override
        public int hashCode() {
            int i = this.a.d() - 1;
            int n = 0;
            while (i >= 0) {
                final Object b = this.a.b(i, 0);
                final Object b2 = this.a.b(i, 1);
                int hashCode;
                if (b == null) {
                    hashCode = 0;
                }
                else {
                    hashCode = b.hashCode();
                }
                int hashCode2;
                if (b2 == null) {
                    hashCode2 = 0;
                }
                else {
                    hashCode2 = b2.hashCode();
                }
                n += (hashCode ^ hashCode2);
                --i;
            }
            return n;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.d() == 0;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new d();
        }
        
        @Override
        public boolean remove(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int size() {
            return this.a.d();
        }
        
        @Override
        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            throw new UnsupportedOperationException();
        }
    }
    
    public final class c implements Set
    {
        public final ym0 a;
        
        public c(final ym0 a) {
            this.a = a;
        }
        
        @Override
        public boolean add(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean addAll(final Collection collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void clear() {
            this.a.a();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.e(o) >= 0;
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            return ym0.j(this.a.c(), collection);
        }
        
        @Override
        public boolean equals(final Object o) {
            return ym0.k(this, o);
        }
        
        @Override
        public int hashCode() {
            int i = this.a.d() - 1;
            int n = 0;
            while (i >= 0) {
                final Object b = this.a.b(i, 0);
                int hashCode;
                if (b == null) {
                    hashCode = 0;
                }
                else {
                    hashCode = b.hashCode();
                }
                n += hashCode;
                --i;
            }
            return n;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.d() == 0;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new a(0);
        }
        
        @Override
        public boolean remove(final Object o) {
            final int e = this.a.e(o);
            if (e >= 0) {
                this.a.h(e);
                return true;
            }
            return false;
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            return ym0.o(this.a.c(), collection);
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            return ym0.p(this.a.c(), collection);
        }
        
        @Override
        public int size() {
            return this.a.d();
        }
        
        @Override
        public Object[] toArray() {
            return this.a.q(0);
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            return this.a.r(array, 0);
        }
    }
    
    public final class d implements Iterator, Entry
    {
        public int a;
        public int b;
        public boolean c;
        public final ym0 d;
        
        public d(final ym0 d) {
            this.d = d;
            this.c = false;
            this.a = d.d() - 1;
            this.b = -1;
        }
        
        public Entry b() {
            if (this.hasNext()) {
                ++this.b;
                this.c = true;
                return this;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (!this.c) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            }
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)o;
            boolean b3 = b2;
            if (el.c(entry.getKey(), this.d.b(this.b, 0))) {
                b3 = b2;
                if (el.c(entry.getValue(), this.d.b(this.b, 1))) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public Object getKey() {
            if (this.c) {
                return this.d.b(this.b, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public Object getValue() {
            if (this.c) {
                return this.d.b(this.b, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public boolean hasNext() {
            return this.b < this.a;
        }
        
        @Override
        public int hashCode() {
            if (this.c) {
                final ym0 d = this.d;
                final int b = this.b;
                int hashCode = 0;
                final Object b2 = d.b(b, 0);
                final Object b3 = this.d.b(this.b, 1);
                int hashCode2;
                if (b2 == null) {
                    hashCode2 = 0;
                }
                else {
                    hashCode2 = b2.hashCode();
                }
                if (b3 != null) {
                    hashCode = b3.hashCode();
                }
                return hashCode2 ^ hashCode;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public void remove() {
            if (this.c) {
                this.d.h(this.b);
                --this.b;
                --this.a;
                this.c = false;
                return;
            }
            throw new IllegalStateException();
        }
        
        @Override
        public Object setValue(final Object o) {
            if (this.c) {
                return this.d.i(this.b, o);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getKey());
            sb.append("=");
            sb.append(this.getValue());
            return sb.toString();
        }
    }
    
    public final class e implements Collection
    {
        public final ym0 a;
        
        public e(final ym0 a) {
            this.a = a;
        }
        
        @Override
        public boolean add(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean addAll(final Collection collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void clear() {
            this.a.a();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.f(o) >= 0;
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                if (!this.contains(iterator.next())) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.d() == 0;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new a(1);
        }
        
        @Override
        public boolean remove(final Object o) {
            final int f = this.a.f(o);
            if (f >= 0) {
                this.a.h(f);
                return true;
            }
            return false;
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            int d = this.a.d();
            int i = 0;
            boolean b = false;
            while (i < d) {
                int n = d;
                int n2 = i;
                if (collection.contains(this.a.b(i, 1))) {
                    this.a.h(i);
                    n2 = i - 1;
                    n = d - 1;
                    b = true;
                }
                i = n2 + 1;
                d = n;
            }
            return b;
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            int d = this.a.d();
            int i = 0;
            boolean b = false;
            while (i < d) {
                int n = d;
                int n2 = i;
                if (!collection.contains(this.a.b(i, 1))) {
                    this.a.h(i);
                    n2 = i - 1;
                    n = d - 1;
                    b = true;
                }
                i = n2 + 1;
                d = n;
            }
            return b;
        }
        
        @Override
        public int size() {
            return this.a.d();
        }
        
        @Override
        public Object[] toArray() {
            return this.a.q(1);
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            return this.a.r(array, 1);
        }
    }
}

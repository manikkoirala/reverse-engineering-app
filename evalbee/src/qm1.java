import java.util.Map;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.List;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qm1 extends wx0
{
    public final a11 d;
    
    public qm1(final du du, final a11 a11, final h71 h71) {
        this(du, a11, h71, new ArrayList());
    }
    
    public qm1(final du du, final a11 d, final h71 h71, final List list) {
        super(du, h71, list);
        this.d = d;
    }
    
    @Override
    public q00 a(final MutableDocument mutableDocument, final q00 q00, final pw1 pw1) {
        this.n(mutableDocument);
        if (!this.h().e(mutableDocument)) {
            return q00;
        }
        final Map l = this.l(pw1, mutableDocument);
        final a11 d = this.d.d();
        d.n(l);
        mutableDocument.l(mutableDocument.getVersion(), d).u();
        return null;
    }
    
    @Override
    public void b(final MutableDocument mutableDocument, final ay0 ay0) {
        this.n(mutableDocument);
        final a11 d = this.d.d();
        d.n(this.m(mutableDocument, ay0.a()));
        mutableDocument.l(ay0.b(), d).t();
    }
    
    @Override
    public q00 e() {
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && qm1.class == o.getClass()) {
            final qm1 qm1 = (qm1)o;
            if (!this.i(qm1) || !this.d.equals(qm1.d) || !this.f().equals(qm1.f())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.j() * 31 + this.d.hashCode();
    }
    
    public a11 o() {
        return this.d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SetMutation{");
        sb.append(this.k());
        sb.append(", value=");
        sb.append(this.d);
        sb.append("}");
        return sb.toString();
    }
}

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.lang.reflect.Constructor;
import androidx.lifecycle.l;
import android.app.Application;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bj1
{
    public static final List a;
    public static final List b;
    
    static {
        a = nh.j((Object[])new Class[] { Application.class, l.class });
        b = mh.e((Object)l.class);
    }
    
    public static final Constructor c(final Class clazz, final List obj) {
        fg0.e((Object)clazz, "modelClass");
        fg0.e((Object)obj, "signature");
        final Constructor[] constructors = clazz.getConstructors();
        fg0.d((Object)constructors, "modelClass.constructors");
        for (final Constructor constructor : constructors) {
            final Class[] parameterTypes = constructor.getParameterTypes();
            fg0.d((Object)parameterTypes, "constructor.parameterTypes");
            final List t = a9.T((Object[])parameterTypes);
            if (fg0.a((Object)obj, (Object)t)) {
                fg0.c((Object)constructor, "null cannot be cast to non-null type java.lang.reflect.Constructor<T of androidx.lifecycle.SavedStateViewModelFactoryKt.findMatchingConstructor>");
                return constructor;
            }
            if (obj.size() == t.size() && t.containsAll(obj)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Class ");
                sb.append(clazz.getSimpleName());
                sb.append(" must have parameters in the proper order: ");
                sb.append(obj);
                throw new UnsupportedOperationException(sb.toString());
            }
        }
        return null;
    }
    
    public static final y32 d(final Class obj, final Constructor constructor, final Object... original) {
        fg0.e((Object)obj, "modelClass");
        fg0.e((Object)constructor, "constructor");
        fg0.e((Object)original, "params");
        try {
            return constructor.newInstance(Arrays.copyOf(original, original.length));
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("An exception happened in constructor of ");
            sb.append(obj);
            throw new RuntimeException(sb.toString(), ex.getCause());
        }
        catch (final InstantiationException cause) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("A ");
            sb2.append(obj);
            sb2.append(" cannot be instantiated.");
            throw new RuntimeException(sb2.toString(), cause);
        }
        catch (final IllegalAccessException cause2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to access ");
            sb3.append(obj);
            throw new RuntimeException(sb3.toString(), cause2);
        }
    }
}

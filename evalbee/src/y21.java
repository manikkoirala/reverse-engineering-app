// 
// Decompiled by Procyon v0.6.0
// 

public class y21
{
    public final Object a;
    public final Object b;
    
    public y21(final Object a, final Object b) {
        this.a = a;
        this.b = b;
    }
    
    public static y21 a(final Object o, final Object o2) {
        return new y21(o, o2);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof y21;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final y21 y21 = (y21)o;
        boolean b3 = b2;
        if (c11.a(y21.a, this.a)) {
            b3 = b2;
            if (c11.a(y21.b, this.b)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        final Object a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = a.hashCode();
        }
        final Object b = this.b;
        if (b != null) {
            hashCode = b.hashCode();
        }
        return hashCode2 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Pair{");
        sb.append(this.a);
        sb.append(" ");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}

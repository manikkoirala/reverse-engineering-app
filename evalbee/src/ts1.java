import android.content.Context;
import java.io.File;
import android.util.Log;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public interface ts1 extends Closeable
{
    ss1 F();
    
    void close();
    
    String getDatabaseName();
    
    void setWriteAheadLoggingEnabled(final boolean p0);
    
    public abstract static class a
    {
        public static final ts1.a.a b;
        public final int a;
        
        static {
            b = new ts1.a.a(null);
        }
        
        public a(final int a) {
            this.a = a;
        }
        
        public final void a(final String s) {
            final int n = 1;
            if (!qr1.k(s, ":memory:", true)) {
                int n2 = s.length() - 1;
                int i = 0;
                int n3 = 0;
                while (i <= n2) {
                    int n4;
                    if (n3 == 0) {
                        n4 = i;
                    }
                    else {
                        n4 = n2;
                    }
                    final boolean b = fg0.f((int)s.charAt(n4), 32) <= 0;
                    if (n3 == 0) {
                        if (!b) {
                            n3 = 1;
                        }
                        else {
                            ++i;
                        }
                    }
                    else {
                        if (!b) {
                            break;
                        }
                        --n2;
                    }
                }
                int n5;
                if (s.subSequence(i, n2 + 1).toString().length() == 0) {
                    n5 = n;
                }
                else {
                    n5 = 0;
                }
                if (n5 == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("deleting the database file: ");
                    sb.append(s);
                    Log.w("SupportSQLite", sb.toString());
                    try {
                        ns1.b(new File(s));
                    }
                    catch (final Exception ex) {
                        Log.w("SupportSQLite", "delete failed: ", (Throwable)ex);
                    }
                }
            }
        }
        
        public void b(final ss1 ss1) {
            fg0.e((Object)ss1, "db");
        }
        
        public void c(final ss1 p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: ldc             "db"
            //     3: invokestatic    fg0.e:(Ljava/lang/Object;Ljava/lang/String;)V
            //     6: new             Ljava/lang/StringBuilder;
            //     9: dup            
            //    10: invokespecial   java/lang/StringBuilder.<init>:()V
            //    13: astore_2       
            //    14: aload_2        
            //    15: ldc             "Corruption reported by sqlite on database: "
            //    17: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //    20: pop            
            //    21: aload_2        
            //    22: aload_1        
            //    23: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //    26: pop            
            //    27: aload_2        
            //    28: ldc             ".path"
            //    30: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //    33: pop            
            //    34: ldc             "SupportSQLite"
            //    36: aload_2        
            //    37: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //    40: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
            //    43: pop            
            //    44: aload_1        
            //    45: invokeinterface ss1.isOpen:()Z
            //    50: ifne            70
            //    53: aload_1        
            //    54: invokeinterface ss1.getPath:()Ljava/lang/String;
            //    59: astore_1       
            //    60: aload_1        
            //    61: ifnull          69
            //    64: aload_0        
            //    65: aload_1        
            //    66: invokevirtual   ts1$a.a:(Ljava/lang/String;)V
            //    69: return         
            //    70: aconst_null    
            //    71: astore_2       
            //    72: aconst_null    
            //    73: astore_3       
            //    74: aload_1        
            //    75: invokeinterface ss1.n:()Ljava/util/List;
            //    80: astore          4
            //    82: aload           4
            //    84: astore_2       
            //    85: goto            92
            //    88: astore_2       
            //    89: goto            103
            //    92: aload_2        
            //    93: astore_3       
            //    94: aload_1        
            //    95: invokeinterface java/io/Closeable.close:()V
            //   100: goto            174
            //   103: aload_3        
            //   104: ifnull          156
            //   107: aload_3        
            //   108: checkcast       Ljava/lang/Iterable;
            //   111: invokeinterface java/lang/Iterable.iterator:()Ljava/util/Iterator;
            //   116: astore_1       
            //   117: aload_1        
            //   118: invokeinterface java/util/Iterator.hasNext:()Z
            //   123: ifeq            172
            //   126: aload_1        
            //   127: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
            //   132: checkcast       Landroid/util/Pair;
            //   135: getfield        android/util/Pair.second:Ljava/lang/Object;
            //   138: astore_3       
            //   139: aload_3        
            //   140: ldc             "p.second"
            //   142: invokestatic    fg0.d:(Ljava/lang/Object;Ljava/lang/String;)V
            //   145: aload_0        
            //   146: aload_3        
            //   147: checkcast       Ljava/lang/String;
            //   150: invokevirtual   ts1$a.a:(Ljava/lang/String;)V
            //   153: goto            117
            //   156: aload_1        
            //   157: invokeinterface ss1.getPath:()Ljava/lang/String;
            //   162: astore_1       
            //   163: aload_1        
            //   164: ifnull          172
            //   167: aload_0        
            //   168: aload_1        
            //   169: invokevirtual   ts1$a.a:(Ljava/lang/String;)V
            //   172: aload_2        
            //   173: athrow         
            //   174: aload_2        
            //   175: ifnull          227
            //   178: aload_2        
            //   179: checkcast       Ljava/lang/Iterable;
            //   182: invokeinterface java/lang/Iterable.iterator:()Ljava/util/Iterator;
            //   187: astore_2       
            //   188: aload_2        
            //   189: invokeinterface java/util/Iterator.hasNext:()Z
            //   194: ifeq            243
            //   197: aload_2        
            //   198: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
            //   203: checkcast       Landroid/util/Pair;
            //   206: getfield        android/util/Pair.second:Ljava/lang/Object;
            //   209: astore_1       
            //   210: aload_1        
            //   211: ldc             "p.second"
            //   213: invokestatic    fg0.d:(Ljava/lang/Object;Ljava/lang/String;)V
            //   216: aload_0        
            //   217: aload_1        
            //   218: checkcast       Ljava/lang/String;
            //   221: invokevirtual   ts1$a.a:(Ljava/lang/String;)V
            //   224: goto            188
            //   227: aload_1        
            //   228: invokeinterface ss1.getPath:()Ljava/lang/String;
            //   233: astore_1       
            //   234: aload_1        
            //   235: ifnull          243
            //   238: aload_0        
            //   239: aload_1        
            //   240: invokevirtual   ts1$a.a:(Ljava/lang/String;)V
            //   243: return         
            //   244: astore_3       
            //   245: goto            92
            //   248: astore_3       
            //   249: goto            174
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                     
            //  -----  -----  -----  -----  -----------------------------------------
            //  74     82     244    248    Landroid/database/sqlite/SQLiteException;
            //  74     82     88     174    Any
            //  94     100    248    252    Ljava/io/IOException;
            //  94     100    88     174    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0103:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public abstract void d(final ss1 p0);
        
        public abstract void e(final ss1 p0, final int p1, final int p2);
        
        public void f(final ss1 ss1) {
            fg0.e((Object)ss1, "db");
        }
        
        public abstract void g(final ss1 p0, final int p1, final int p2);
        
        public static final class a
        {
        }
    }
    
    public static final class b
    {
        public static final ts1.b.b f;
        public final Context a;
        public final String b;
        public final ts1.a c;
        public final boolean d;
        public final boolean e;
        
        static {
            f = new ts1.b.b(null);
        }
        
        public b(final Context a, final String b, final ts1.a c, final boolean d, final boolean e) {
            fg0.e((Object)a, "context");
            fg0.e((Object)c, "callback");
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        public static final a a(final Context context) {
            return ts1.b.f.a(context);
        }
        
        public static class a
        {
            public final Context a;
            public String b;
            public ts1.a c;
            public boolean d;
            public boolean e;
            
            public a(final Context a) {
                fg0.e((Object)a, "context");
                this.a = a;
            }
            
            public a a(final boolean e) {
                this.e = e;
                return this;
            }
            
            public ts1.b b() {
                final ts1.a c = this.c;
                if (c == null) {
                    throw new IllegalArgumentException("Must set a callback to create the configuration.".toString());
                }
                final boolean d = this.d;
                int n = 1;
                if (d) {
                    final String b = this.b;
                    if (b != null && b.length() != 0) {
                        n = n;
                    }
                    else {
                        n = 0;
                    }
                }
                if (n != 0) {
                    return new ts1.b(this.a, this.b, c, this.d, this.e);
                }
                throw new IllegalArgumentException("Must set a non-null database name to a configuration that uses the no backup directory.".toString());
            }
            
            public a c(final ts1.a c) {
                fg0.e((Object)c, "callback");
                this.c = c;
                return this;
            }
            
            public a d(final String b) {
                this.b = b;
                return this;
            }
            
            public a e(final boolean d) {
                this.d = d;
                return this;
            }
        }
        
        public static final class b
        {
            public final a a(final Context context) {
                fg0.e((Object)context, "context");
                return new a(context);
            }
        }
    }
    
    public interface c
    {
        ts1 a(final ts1.b p0);
    }
}

import android.view.ViewPropertyAnimator;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.view.animation.Interpolator;
import android.animation.ValueAnimator;
import android.view.View;
import java.lang.ref.WeakReference;

// 
// Decompiled by Procyon v0.6.0
// 

public final class i42
{
    public final WeakReference a;
    public Runnable b;
    public Runnable c;
    public int d;
    
    public i42(final View referent) {
        this.b = null;
        this.c = null;
        this.d = -1;
        this.a = new WeakReference((T)referent);
    }
    
    public i42 b(final float n) {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().alpha(n);
        }
        return this;
    }
    
    public void c() {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }
    
    public long d() {
        final View view = (View)this.a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0L;
    }
    
    public i42 f(final long duration) {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().setDuration(duration);
        }
        return this;
    }
    
    public i42 g(final Interpolator interpolator) {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().setInterpolator((TimeInterpolator)interpolator);
        }
        return this;
    }
    
    public i42 h(final k42 k42) {
        final View view = (View)this.a.get();
        if (view != null) {
            this.i(view, k42);
        }
        return this;
    }
    
    public final void i(final View view, final k42 k42) {
        if (k42 != null) {
            view.animate().setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, k42, view) {
                public final k42 a;
                public final View b;
                public final i42 c;
                
                public void onAnimationCancel(final Animator animator) {
                    this.a.a(this.b);
                }
                
                public void onAnimationEnd(final Animator animator) {
                    this.a.b(this.b);
                }
                
                public void onAnimationStart(final Animator animator) {
                    this.a.c(this.b);
                }
            });
        }
        else {
            view.animate().setListener((Animator$AnimatorListener)null);
        }
    }
    
    public i42 j(final long startDelay) {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().setStartDelay(startDelay);
        }
        return this;
    }
    
    public i42 k(final m42 m42) {
        final View view = (View)this.a.get();
        if (view != null) {
            Object o;
            if (m42 != null) {
                o = new h42(m42, view);
            }
            else {
                o = null;
            }
            i42.b.a(view.animate(), (ValueAnimator$AnimatorUpdateListener)o);
        }
        return this;
    }
    
    public void l() {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().start();
        }
    }
    
    public i42 m(final float n) {
        final View view = (View)this.a.get();
        if (view != null) {
            view.animate().translationY(n);
        }
        return this;
    }
    
    public abstract static class b
    {
        public static ViewPropertyAnimator a(final ViewPropertyAnimator viewPropertyAnimator, final ValueAnimator$AnimatorUpdateListener updateListener) {
            return viewPropertyAnimator.setUpdateListener(updateListener);
        }
    }
}

import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

// 
// Decompiled by Procyon v0.6.0
// 

public class si1 implements Iterable
{
    public c a;
    public c b;
    public final WeakHashMap c;
    public int d;
    
    public si1() {
        this.c = new WeakHashMap();
        this.d = 0;
    }
    
    public Map.Entry a() {
        return this.a;
    }
    
    public c b(final Object obj) {
        c c;
        for (c = this.a; c != null && !c.a.equals(obj); c = c.c) {}
        return c;
    }
    
    public d c() {
        final d key = new d();
        this.c.put(key, Boolean.FALSE);
        return key;
    }
    
    public Iterator descendingIterator() {
        final b key = new b(this.b, this.a);
        this.c.put(key, Boolean.FALSE);
        return key;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (!(o instanceof si1)) {
            return false;
        }
        final si1 si1 = (si1)o;
        if (this.size() != si1.size()) {
            return false;
        }
        final Iterator iterator = this.iterator();
        final Iterator iterator2 = si1.iterator();
        while (iterator.hasNext() && iterator2.hasNext()) {
            final Map.Entry entry = iterator.next();
            final Object next = iterator2.next();
            if ((entry == null && next != null) || (entry != null && !entry.equals(next))) {
                return false;
            }
        }
        if (iterator.hasNext() || iterator2.hasNext()) {
            b = false;
        }
        return b;
    }
    
    public Map.Entry g() {
        return this.b;
    }
    
    @Override
    public int hashCode() {
        final Iterator iterator = this.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            n += iterator.next().hashCode();
        }
        return n;
    }
    
    public c i(final Object o, final Object o2) {
        final c b = new c(o, o2);
        ++this.d;
        final c b2 = this.b;
        if (b2 == null) {
            this.a = b;
        }
        else {
            b2.c = b;
            b.d = b2;
        }
        return this.b = b;
    }
    
    @Override
    public Iterator iterator() {
        final a key = new a(this.a, this.b);
        this.c.put(key, Boolean.FALSE);
        return key;
    }
    
    public Object l(final Object o, final Object o2) {
        final c b = this.b(o);
        if (b != null) {
            return b.b;
        }
        this.i(o, o2);
        return null;
    }
    
    public Object m(final Object o) {
        final c b = this.b(o);
        if (b == null) {
            return null;
        }
        --this.d;
        if (!this.c.isEmpty()) {
            final Iterator iterator = this.c.keySet().iterator();
            while (iterator.hasNext()) {
                ((f)iterator.next()).b(b);
            }
        }
        final c d = b.d;
        final c c = b.c;
        if (d != null) {
            d.c = c;
        }
        else {
            this.a = c;
        }
        final c c2 = b.c;
        if (c2 != null) {
            c2.d = d;
        }
        else {
            this.b = d;
        }
        b.c = null;
        b.d = null;
        return b.b;
    }
    
    public int size() {
        return this.d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        final Iterator iterator = this.iterator();
        while (iterator.hasNext()) {
            sb.append(((Map.Entry<?, ?>)iterator.next()).toString());
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
    
    public static class a extends e
    {
        public a(final c c, final c c2) {
            super(c, c2);
        }
        
        @Override
        public c c(final c c) {
            return c.d;
        }
        
        @Override
        public c d(final c c) {
            return c.c;
        }
    }
    
    public abstract static class e extends f implements Iterator
    {
        public c a;
        public c b;
        
        public e(final c b, final c a) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void b(final c c) {
            if (this.a == c && c == this.b) {
                this.b = null;
                this.a = null;
            }
            final c a = this.a;
            if (a == c) {
                this.a = this.c(a);
            }
            if (this.b == c) {
                this.b = this.f();
            }
        }
        
        public abstract c c(final c p0);
        
        public abstract c d(final c p0);
        
        public Map.Entry e() {
            final c b = this.b;
            this.b = this.f();
            return b;
        }
        
        public final c f() {
            final c b = this.b;
            final c a = this.a;
            if (b != a && a != null) {
                return this.d(b);
            }
            return null;
        }
        
        @Override
        public boolean hasNext() {
            return this.b != null;
        }
    }
    
    public abstract static class f
    {
        public abstract void b(final c p0);
    }
    
    public static class b extends e
    {
        public b(final c c, final c c2) {
            super(c, c2);
        }
        
        @Override
        public c c(final c c) {
            return c.c;
        }
        
        @Override
        public c d(final c c) {
            return c.d;
        }
    }
    
    public static class c implements Entry
    {
        public final Object a;
        public final Object b;
        public c c;
        public c d;
        
        public c(final Object a, final Object b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (o == this) {
                return true;
            }
            if (!(o instanceof c)) {
                return false;
            }
            final c c = (c)o;
            if (!this.a.equals(c.a) || !this.b.equals(c.b)) {
                b = false;
            }
            return b;
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public Object getValue() {
            return this.b;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }
        
        @Override
        public Object setValue(final Object o) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append("=");
            sb.append(this.b);
            return sb.toString();
        }
    }
    
    public class d extends f implements Iterator
    {
        public c a;
        public boolean b;
        public final si1 c;
        
        public d(final si1 c) {
            this.c = c;
            this.b = true;
        }
        
        @Override
        public void b(c d) {
            final c a = this.a;
            if (d == a) {
                d = a.d;
                this.a = d;
                this.b = (d == null);
            }
        }
        
        public Map.Entry c() {
            Map.Entry a;
            if (this.b) {
                this.b = false;
                a = this.c.a;
            }
            else {
                final c a2 = this.a;
                if (a2 != null) {
                    a = a2.c;
                }
                else {
                    a = null;
                }
            }
            return this.a = (c)a;
        }
        
        @Override
        public boolean hasNext() {
            final boolean b = this.b;
            boolean b2 = true;
            final boolean b3 = true;
            if (b) {
                return this.c.a != null && b3;
            }
            final c a = this.a;
            if (a == null || a.c == null) {
                b2 = false;
            }
            return b2;
        }
    }
}

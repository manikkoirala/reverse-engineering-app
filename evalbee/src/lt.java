import kotlinx.coroutines.CoroutineDispatcher;
import kotlin.coroutines.CoroutineContext;
import java.util.ArrayDeque;
import java.util.Queue;

// 
// Decompiled by Procyon v0.6.0
// 

public final class lt
{
    public boolean a;
    public boolean b;
    public boolean c;
    public final Queue d;
    
    public lt() {
        this.a = true;
        this.d = new ArrayDeque();
    }
    
    public static final void d(final lt lt, final Runnable runnable) {
        fg0.e((Object)lt, "this$0");
        fg0.e((Object)runnable, "$runnable");
        lt.f(runnable);
    }
    
    public final boolean b() {
        return this.b || !this.a;
    }
    
    public final void c(final CoroutineContext coroutineContext, final Runnable runnable) {
        fg0.e((Object)coroutineContext, "context");
        fg0.e((Object)runnable, "runnable");
        final pm0 s0 = pt.c().s0();
        if (!((CoroutineDispatcher)s0).q0(coroutineContext) && !this.b()) {
            this.f(runnable);
        }
        else {
            ((CoroutineDispatcher)s0).f(coroutineContext, (Runnable)new kt(this, runnable));
        }
    }
    
    public final void e() {
        if (this.c) {
            return;
        }
        try {
            this.c = true;
            while ((this.d.isEmpty() ^ true) && this.b()) {
                final Runnable runnable = this.d.poll();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }
        finally {
            this.c = false;
        }
    }
    
    public final void f(final Runnable runnable) {
        if (this.d.offer(runnable)) {
            this.e();
            return;
        }
        throw new IllegalStateException("cannot enqueue any more runnables".toString());
    }
    
    public final void g() {
        this.b = true;
        this.e();
    }
    
    public final void h() {
        this.a = true;
    }
    
    public final void i() {
        if (!this.a) {
            return;
        }
        if (this.b ^ true) {
            this.a = false;
            this.e();
            return;
        }
        throw new IllegalStateException("Cannot resume a finished dispatcher".toString());
    }
}

import android.os.BaseBundle;
import android.app.RemoteInput;
import androidx.core.graphics.drawable.IconCompat;
import android.app.Notification$Action$Builder;
import java.util.Collection;
import android.graphics.drawable.Icon;
import java.util.Iterator;
import android.app.Notification;
import android.net.Uri;
import android.text.TextUtils;
import android.os.Build$VERSION;
import java.util.ArrayList;
import android.os.Bundle;
import java.util.List;
import android.widget.RemoteViews;
import android.app.Notification$Builder;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class g01 implements qz0
{
    public final Context a;
    public final Notification$Builder b;
    public final rz0.e c;
    public RemoteViews d;
    public RemoteViews e;
    public final List f;
    public final Bundle g;
    public int h;
    public RemoteViews i;
    
    public g01(final rz0.e c) {
        this.f = new ArrayList();
        this.g = new Bundle();
        this.c = c;
        this.a = c.a;
        Notification$Builder a;
        if (Build$VERSION.SDK_INT >= 26) {
            tz0.a();
            a = f01.a(c.a, c.K);
        }
        else {
            a = new Notification$Builder(c.a);
        }
        this.b = a;
        final Notification r = c.R;
        this.b.setWhen(r.when).setSmallIcon(r.icon, r.iconLevel).setContent(r.contentView).setTicker(r.tickerText, c.i).setVibrate(r.vibrate).setLights(r.ledARGB, r.ledOnMS, r.ledOffMS).setOngoing((r.flags & 0x2) != 0x0).setOnlyAlertOnce((r.flags & 0x8) != 0x0).setAutoCancel((r.flags & 0x10) != 0x0).setDefaults(r.defaults).setContentTitle(c.e).setContentText(c.f).setContentInfo(c.k).setContentIntent(c.g).setDeleteIntent(r.deleteIntent).setFullScreenIntent(c.h, (r.flags & 0x80) != 0x0).setLargeIcon(c.j).setNumber(c.l).setProgress(c.t, c.u, c.v);
        this.b.setSubText(c.q).setUsesChronometer(c.o).setPriority(c.m);
        final Iterator iterator = c.b.iterator();
        while (iterator.hasNext()) {
            this.b((rz0.a)iterator.next());
        }
        final Bundle d = c.D;
        if (d != null) {
            this.g.putAll(d);
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        this.d = c.H;
        this.e = c.I;
        this.b.setShowWhen(c.n);
        this.b.setLocalOnly(c.z).setGroup(c.w).setGroupSummary(c.x).setSortKey(c.y);
        this.h = c.O;
        this.b.setCategory(c.C).setColor(c.E).setVisibility(c.F).setPublicVersion(c.G).setSound(r.sound, r.audioAttributes);
        List list;
        if (sdk_INT < 28) {
            list = e(g(c.c), c.U);
        }
        else {
            list = c.U;
        }
        if (list != null && !list.isEmpty()) {
            final Iterator iterator2 = list.iterator();
            while (iterator2.hasNext()) {
                this.b.addPerson((String)iterator2.next());
            }
        }
        this.i = c.J;
        if (c.d.size() > 0) {
            Bundle bundle;
            if ((bundle = c.c().getBundle("android.car.EXTENSIONS")) == null) {
                bundle = new Bundle();
            }
            final Bundle bundle2 = new Bundle(bundle);
            final Bundle bundle3 = new Bundle();
            for (int i = 0; i < c.d.size(); ++i) {
                bundle3.putBundle(Integer.toString(i), h01.a((rz0.a)c.d.get(i)));
            }
            bundle.putBundle("invisible_actions", bundle3);
            bundle2.putBundle("invisible_actions", bundle3);
            c.c().putBundle("android.car.EXTENSIONS", bundle);
            this.g.putBundle("android.car.EXTENSIONS", bundle2);
        }
        final int sdk_INT2 = Build$VERSION.SDK_INT;
        final Icon t = c.T;
        if (t != null) {
            this.b.setSmallIcon(t);
        }
        this.b.setExtras(c.D).setRemoteInputHistory(c.s);
        final RemoteViews h = c.H;
        if (h != null) {
            this.b.setCustomContentView(h);
        }
        final RemoteViews j = c.I;
        if (j != null) {
            this.b.setCustomBigContentView(j);
        }
        final RemoteViews k = c.J;
        if (k != null) {
            this.b.setCustomHeadsUpContentView(k);
        }
        if (sdk_INT2 >= 26) {
            yz0.a(b01.a(a01.a(zz0.a(sz0.a(this.b, c.L), c.r), c.M), c.N), c.O);
            if (c.B) {
                c01.a(this.b, c.A);
            }
            if (!TextUtils.isEmpty((CharSequence)c.K)) {
                this.b.setSound((Uri)null).setDefaults(0).setLights(0, 0, 0).setVibrate((long[])null);
            }
        }
        if (sdk_INT2 >= 28) {
            final Iterator iterator3 = c.c.iterator();
            if (iterator3.hasNext()) {
                zu0.a(iterator3.next());
                throw null;
            }
        }
        if (sdk_INT2 >= 29) {
            d01.a(this.b, c.Q);
            e01.a(this.b, rz0.d.a(null));
        }
        if (sdk_INT2 >= 31) {
            final int p = c.P;
            if (p != 0) {
                xz0.a(this.b, p);
            }
        }
        if (c.S) {
            if (this.c.x) {
                this.h = 2;
            }
            else {
                this.h = 1;
            }
            this.b.setVibrate((long[])null);
            this.b.setSound((Uri)null);
            final int n = r.defaults & 0xFFFFFFFE & 0xFFFFFFFD;
            r.defaults = n;
            this.b.setDefaults(n);
            if (sdk_INT2 >= 26) {
                if (TextUtils.isEmpty((CharSequence)this.c.w)) {
                    this.b.setGroup("silent");
                }
                yz0.a(this.b, this.h);
            }
        }
    }
    
    public static List e(final List list, final List list2) {
        if (list == null) {
            return list2;
        }
        if (list2 == null) {
            return list;
        }
        final s8 c = new s8(list.size() + list2.size());
        c.addAll(list);
        c.addAll(list2);
        return new ArrayList(c);
    }
    
    public static List g(final List list) {
        if (list == null) {
            return null;
        }
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        if (!iterator.hasNext()) {
            return list2;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    @Override
    public Notification$Builder a() {
        return this.b;
    }
    
    public final void b(final rz0.a a) {
        final IconCompat d = a.d();
        Icon l;
        if (d != null) {
            l = d.l();
        }
        else {
            l = null;
        }
        final Notification$Action$Builder notification$Action$Builder = new Notification$Action$Builder(l, a.h(), a.a());
        if (a.e() != null) {
            final RemoteInput[] b = kd1.b(a.e());
            for (int length = b.length, i = 0; i < length; ++i) {
                notification$Action$Builder.addRemoteInput(b[i]);
            }
        }
        Bundle bundle;
        if (a.c() != null) {
            bundle = new Bundle(a.c());
        }
        else {
            bundle = new Bundle();
        }
        ((BaseBundle)bundle).putBoolean("android.support.allowGeneratedReplies", a.b());
        final int sdk_INT = Build$VERSION.SDK_INT;
        notification$Action$Builder.setAllowGeneratedReplies(a.b());
        ((BaseBundle)bundle).putInt("android.support.action.semanticAction", a.f());
        if (sdk_INT >= 28) {
            uz0.a(notification$Action$Builder, a.f());
        }
        if (sdk_INT >= 29) {
            vz0.a(notification$Action$Builder, a.j());
        }
        if (sdk_INT >= 31) {
            wz0.a(notification$Action$Builder, a.i());
        }
        ((BaseBundle)bundle).putBoolean("android.support.action.showsUserInterface", a.g());
        notification$Action$Builder.addExtras(bundle);
        this.b.addAction(notification$Action$Builder.build());
    }
    
    public Notification c() {
        final rz0.g p = this.c.p;
        if (p != null) {
            p.b(this);
        }
        RemoteViews contentView;
        if (p != null) {
            contentView = p.e(this);
        }
        else {
            contentView = null;
        }
        final Notification d = this.d();
        Label_0064: {
            if (contentView == null) {
                contentView = this.c.H;
                if (contentView == null) {
                    break Label_0064;
                }
            }
            d.contentView = contentView;
        }
        if (p != null) {
            final RemoteViews d2 = p.d(this);
            if (d2 != null) {
                d.bigContentView = d2;
            }
        }
        if (p != null) {
            final RemoteViews f = this.c.p.f(this);
            if (f != null) {
                d.headsUpContentView = f;
            }
        }
        if (p != null) {
            final Bundle a = rz0.a(d);
            if (a != null) {
                p.a(a);
            }
        }
        return d;
    }
    
    public Notification d() {
        if (Build$VERSION.SDK_INT >= 26) {
            return this.b.build();
        }
        final Notification build = this.b.build();
        if (this.h != 0) {
            if (build.getGroup() != null && (build.flags & 0x200) != 0x0 && this.h == 2) {
                this.h(build);
            }
            if (build.getGroup() != null && (build.flags & 0x200) == 0x0 && this.h == 1) {
                this.h(build);
            }
        }
        return build;
    }
    
    public Context f() {
        return this.a;
    }
    
    public final void h(final Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults = (notification.defaults & 0xFFFFFFFE & 0xFFFFFFFD);
    }
}

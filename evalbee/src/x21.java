import android.graphics.Paint;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class x21
{
    public static final ThreadLocal a;
    
    static {
        a = new ThreadLocal();
    }
    
    public static boolean a(final Paint paint, final String s) {
        return x21.a.a(paint, s);
    }
    
    public abstract static class a
    {
        public static boolean a(final Paint paint, final String s) {
            return paint.hasGlyph(s);
        }
    }
}

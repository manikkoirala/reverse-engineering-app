import android.os.BaseBundle;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class o4
{
    public static FirebaseAnalytics a(final Context context) {
        return FirebaseAnalytics.getInstance(context);
    }
    
    public static void b(final FirebaseAnalytics firebaseAnalytics, final String s, final String s2, final String s3) {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putString("content_type", s);
        ((BaseBundle)bundle).putString("item_id", s2);
        ((BaseBundle)bundle).putInt("version", a91.f());
        ((BaseBundle)bundle).putString("tag", s3);
        firebaseAnalytics.a("select_content", bundle);
    }
    
    public static void c(final FirebaseAnalytics firebaseAnalytics, final String s, final String s2) {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putString("item_id", s);
        firebaseAnalytics.a("view_item", bundle);
    }
}

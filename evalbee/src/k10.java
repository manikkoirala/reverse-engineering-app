import android.widget.CompoundButton;
import com.ekodroid.omrevaluator.util.DataModels.SortArchiveExamList;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.widget.RadioButton;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class k10 extends u80
{
    public Context a;
    public j10 b;
    public zx c;
    public RadioButton d;
    public RadioButton e;
    public RadioButton f;
    public RadioButton g;
    public RadioButton h;
    
    public k10(final Context a, final zx c, final j10 b) {
        super(a);
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static /* synthetic */ j10 a(final k10 k10) {
        return k10.b;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492971);
        ((Toolbar)this.findViewById(2131297345)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final k10 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.d = (RadioButton)this.findViewById(2131297007);
        this.e = (RadioButton)this.findViewById(2131297010);
        this.h = (RadioButton)this.findViewById(2131297006);
        this.f = (RadioButton)this.findViewById(2131297005);
        this.g = (RadioButton)this.findViewById(2131297008);
        this.findViewById(2131296388).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final k10 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296416).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final k10 a;
            
            public void onClick(final View view) {
                if (k10.a(this.a) != null) {
                    final j10 a = k10.a(this.a);
                    SortArchiveExamList.SortBy sortBy;
                    if (((CompoundButton)this.a.e).isChecked()) {
                        sortBy = SortArchiveExamList.SortBy.NAME;
                    }
                    else if (((CompoundButton)this.a.d).isChecked()) {
                        sortBy = SortArchiveExamList.SortBy.DATE;
                    }
                    else {
                        sortBy = SortArchiveExamList.SortBy.CLASS;
                    }
                    a.a(new zx(sortBy, ((CompoundButton)this.a.f).isChecked()));
                }
                this.a.dismiss();
            }
        });
        final zx c = this.c;
        if (c != null) {
            final RadioButton g = this.g;
            final boolean b = c.b;
            final boolean b2 = true;
            ((CompoundButton)g).setChecked(b ^ true);
            ((CompoundButton)this.f).setChecked(this.c.b);
            ((CompoundButton)this.e).setChecked(this.c.a == SortArchiveExamList.SortBy.NAME);
            ((CompoundButton)this.d).setChecked(this.c.a == SortArchiveExamList.SortBy.DATE);
            ((CompoundButton)this.h).setChecked(this.c.a == SortArchiveExamList.SortBy.CLASS && b2);
        }
    }
}

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import java.util.HashSet;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.List;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public final class e31 extends wx0
{
    public final a11 d;
    public final q00 e;
    
    public e31(final du du, final a11 a11, final q00 q00, final h71 h71) {
        this(du, a11, q00, h71, new ArrayList());
    }
    
    public e31(final du du, final a11 d, final q00 e, final h71 h71, final List list) {
        super(du, h71, list);
        this.d = d;
        this.e = e;
    }
    
    @Override
    public q00 a(final MutableDocument mutableDocument, final q00 q00, final pw1 pw1) {
        this.n(mutableDocument);
        if (!this.h().e(mutableDocument)) {
            return q00;
        }
        final Map l = this.l(pw1, mutableDocument);
        final Map p3 = this.p();
        final a11 data = mutableDocument.getData();
        data.n(p3);
        data.n(l);
        mutableDocument.l(mutableDocument.getVersion(), mutableDocument.getData()).u();
        if (q00 == null) {
            return null;
        }
        final HashSet set = new HashSet(q00.c());
        set.addAll(this.e.c());
        set.addAll(this.o());
        return q00.b(set);
    }
    
    @Override
    public void b(final MutableDocument mutableDocument, final ay0 ay0) {
        this.n(mutableDocument);
        if (!this.h().e(mutableDocument)) {
            mutableDocument.n(ay0.b());
            return;
        }
        final Map m = this.m(mutableDocument, ay0.a());
        final a11 data = mutableDocument.getData();
        data.n(this.p());
        data.n(m);
        mutableDocument.l(ay0.b(), mutableDocument.getData()).t();
    }
    
    @Override
    public q00 e() {
        return this.e;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && e31.class == o.getClass()) {
            final e31 e31 = (e31)o;
            if (!this.i(e31) || !this.d.equals(e31.d) || !this.f().equals(e31.f())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.j() * 31 + this.d.hashCode();
    }
    
    public final List o() {
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.f().iterator();
        while (iterator.hasNext()) {
            list.add(((u00)iterator.next()).a());
        }
        return list;
    }
    
    public final Map p() {
        final HashMap hashMap = new HashMap();
        for (final s00 s00 : this.e.c()) {
            if (!s00.j()) {
                hashMap.put(s00, this.d.j(s00));
            }
        }
        return hashMap;
    }
    
    public a11 q() {
        return this.d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PatchMutation{");
        sb.append(this.k());
        sb.append(", mask=");
        sb.append(this.e);
        sb.append(", value=");
        sb.append(this.d);
        sb.append("}");
        return sb.toString();
    }
}

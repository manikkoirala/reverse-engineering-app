import android.app.Dialog;
import android.view.ViewGroup;
import android.view.ViewGroup$MarginLayoutParams;
import java.util.Iterator;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import android.widget.TableLayout$LayoutParams;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import android.widget.TableLayout;

// 
// Decompiled by Procyon v0.6.0
// 

public class aw
{
    public TableLayout a;
    public ArrayList b;
    public k5.f c;
    public SheetTemplate2 d;
    public ArrayList e;
    public ArrayList f;
    public Context g;
    public ArrayList h;
    
    public aw(final Context g, final TableLayout a, final ArrayList b, final k5.f c, final SheetTemplate2 d, final ArrayList e, final ArrayList f) {
        this.b = b;
        this.f = f;
        this.c = c;
        this.e = e;
        this.d = d;
        this.a = a;
        this.g = g;
        new b(null).execute((Object[])new Void[0]);
    }
    
    public class b extends AsyncTask
    {
        public ProgressDialog a;
        public final aw b;
        
        public b(final aw b) {
            this.b = b;
        }
        
        public final void a() {
            final ArrayList list = new ArrayList();
            final Iterator iterator = this.b.e.iterator();
            while (iterator.hasNext()) {
                list.add(new sn1((SheetDimension)iterator.next()));
            }
            final ArrayList<AnswerOption> answerOptions = this.b.d.getAnswerOptions();
            this.b.h = new ArrayList();
            final TableLayout$LayoutParams layoutParams = new TableLayout$LayoutParams(-1, -2);
            ((ViewGroup$MarginLayoutParams)layoutParams).setMargins(a91.d(15, this.b.g), 0, a91.d(15, this.b.g), 0);
            for (final AnswerValue answerValue : this.b.b) {
                final LinearLayout e = new LinearLayout(this.b.g);
                ((View)e).setMinimumHeight(2);
                ((View)e).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
                ((View)e).setBackgroundColor(this.b.g.getResources().getColor(2131099716));
                this.b.h.add(e);
                final int pageIndex = answerOptions.get(answerValue.getQuestionNumber()).pageIndex;
                final aw b = this.b;
                final Iterator iterator3 = new y11(b.g, answerValue, b.d, list.get(pageIndex), this.b.f.get(pageIndex)).i().iterator();
                while (iterator3.hasNext()) {
                    this.b.h.add(iterator3.next());
                }
                final aw b2 = this.b;
                final Iterator iterator4 = new k5(b2.g, answerValue, null, b2.c, b2.d.getLabelProfile()).m().iterator();
                while (iterator4.hasNext()) {
                    this.b.h.add(iterator4.next());
                }
            }
        }
        
        public final void b() {
            this.b.h = new ArrayList();
            for (final AnswerValue answerValue : this.b.b) {
                final aw b = this.b;
                final Iterator iterator2 = new k5(b.g, answerValue, null, b.c, b.d.getLabelProfile()).m().iterator();
                while (iterator2.hasNext()) {
                    this.b.h.add(iterator2.next());
                }
            }
        }
        
        public Void c(final Void... array) {
            final aw b = this.b;
            if (b.e != null && b.f != null) {
                this.a();
            }
            else {
                this.b();
            }
            return null;
        }
        
        public void d(final Void void1) {
            ((ViewGroup)this.b.a).removeAllViews();
            final Iterator iterator = this.b.h.iterator();
            while (iterator.hasNext()) {
                this.b.a.addView((View)iterator.next());
            }
            if (((Dialog)this.a).isShowing()) {
                ((Dialog)this.a).dismiss();
            }
        }
        
        public void onPreExecute() {
            (this.a = new ProgressDialog(this.b.g)).setMessage((CharSequence)"loading...");
            ((Dialog)this.a).setCancelable(false);
            ((Dialog)this.a).show();
        }
    }
}

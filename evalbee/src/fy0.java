import java.util.Iterator;
import java.io.IOException;
import java.util.List;
import java.io.Closeable;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.InputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class fy0
{
    public static void a(final InputStream inputStream, final File file) {
        if (inputStream == null) {
            return;
        }
        final byte[] array = new byte[8192];
        final Closeable closeable = null;
        Closeable closeable2;
        try {
            final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(new FileOutputStream(file));
            try {
                while (true) {
                    final int read = inputStream.read(array);
                    if (read <= 0) {
                        break;
                    }
                    gzipOutputStream.write(array, 0, read);
                }
                gzipOutputStream.finish();
                CommonUtils.g(gzipOutputStream);
                return;
            }
            finally {}
        }
        finally {
            closeable2 = closeable;
        }
        CommonUtils.g(closeable2);
    }
    
    public static void b(final File parent, List o) {
        for (final ey0 ey0 : o) {
            o = null;
            Closeable closeable = null;
            try {
                final InputStream stream = ey0.getStream();
                if (stream == null) {
                    o = stream;
                }
                else {
                    closeable = stream;
                    o = stream;
                    closeable = stream;
                    o = stream;
                    final File file = new File(parent, ey0.a());
                    closeable = stream;
                    o = stream;
                    a(stream, file);
                    o = stream;
                }
            }
            catch (final IOException ex) {}
            finally {
                CommonUtils.g(closeable);
            }
        }
        goto Label_0103;
    }
}

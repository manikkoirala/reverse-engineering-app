import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

// 
// Decompiled by Procyon v0.6.0
// 

public final class s00 extends jb
{
    public static final s00 b;
    public static final s00 c;
    
    static {
        b = r("__name__");
        c = new s00(Collections.emptyList());
    }
    
    public s00(final List list) {
        super(list);
    }
    
    public static s00 p(final List list) {
        s00 c;
        if (list.isEmpty()) {
            c = s00.c;
        }
        else {
            c = new s00(list);
        }
        return c;
    }
    
    public static s00 q(final String s) {
        final ArrayList list = new ArrayList();
        StringBuilder sb = new StringBuilder();
        int i = 0;
        int n = 0;
        while (i < s.length()) {
            final char char1 = s.charAt(i);
            Label_0202: {
                int index;
                char char2;
                if (char1 == '\\') {
                    index = i + 1;
                    if (index == s.length()) {
                        throw new IllegalArgumentException("Trailing escape character is not allowed");
                    }
                    char2 = s.charAt(index);
                }
                else if (char1 == '.') {
                    index = i;
                    char2 = char1;
                    if (n == 0) {
                        final String string = sb.toString();
                        if (!string.isEmpty()) {
                            sb = new StringBuilder();
                            list.add(string);
                            break Label_0202;
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Invalid field path (");
                        sb2.append(s);
                        sb2.append("). Paths must not be empty, begin with '.', end with '.', or contain '..'");
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
                else {
                    index = i;
                    if ((char2 = char1) == '`') {
                        n ^= 0x1;
                        break Label_0202;
                    }
                }
                sb.append(char2);
                i = index;
            }
            ++i;
        }
        final String string2 = sb.toString();
        if (!string2.isEmpty()) {
            list.add(string2);
            return new s00(list);
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Invalid field path (");
        sb3.append(s);
        sb3.append("). Paths must not be empty, begin with '.', end with '.', or contain '..'");
        throw new IllegalArgumentException(sb3.toString());
    }
    
    public static s00 r(final String o) {
        return new s00(Collections.singletonList(o));
    }
    
    public static boolean t(final String s) {
        if (s.isEmpty()) {
            return false;
        }
        final char char1 = s.charAt(0);
        if (char1 != '_' && (char1 < 'a' || char1 > 'z') && (char1 < 'A' || char1 > 'Z')) {
            return false;
        }
        for (int i = 1; i < s.length(); ++i) {
            final char char2 = s.charAt(i);
            if (char2 != '_' && (char2 < 'a' || char2 > 'z') && (char2 < 'A' || char2 > 'Z') && (char2 < '0' || char2 > '9')) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String d() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < super.a.size(); ++i) {
            if (i > 0) {
                sb.append(".");
            }
            String str2;
            final String str = str2 = super.a.get(i).replace("\\", "\\\\").replace("`", "\\`");
            if (!t(str)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append('`');
                sb2.append(str);
                sb2.append('`');
                str2 = sb2.toString();
            }
            sb.append(str2);
        }
        return sb.toString();
    }
    
    public s00 o(final List list) {
        return new s00(list);
    }
    
    public boolean s() {
        return this.equals(s00.b);
    }
}

import java.util.HashMap;
import java.util.Map;
import com.google.firestore.v1.Value;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.ArrayList;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class wx0
{
    public final du a;
    public final h71 b;
    public final List c;
    
    public wx0(final du du, final h71 h71) {
        this(du, h71, new ArrayList());
    }
    
    public wx0(final du a, final h71 b, final List c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static wx0 c(final MutableDocument mutableDocument, final q00 q00) {
        if (!mutableDocument.g() || (q00 != null && q00.c().isEmpty())) {
            return null;
        }
        if (q00 != null) {
            final a11 data = mutableDocument.getData();
            final a11 a11 = new a11();
            final HashSet set = new HashSet();
            for (final s00 o : q00.c()) {
                if (!set.contains(o)) {
                    s00 e = o;
                    if (data.j(o) == null) {
                        e = o;
                        if (o.l() > 1) {
                            e = (s00)o.n();
                        }
                    }
                    a11.m(e, data.j(e));
                    set.add(e);
                }
            }
            return new e31(mutableDocument.getKey(), a11, q00.b(set), h71.c);
        }
        if (mutableDocument.c()) {
            return new hs(mutableDocument.getKey(), h71.c);
        }
        return new qm1(mutableDocument.getKey(), mutableDocument.getData(), h71.c);
    }
    
    public abstract q00 a(final MutableDocument p0, final q00 p1, final pw1 p2);
    
    public abstract void b(final MutableDocument p0, final ay0 p1);
    
    public a11 d(final zt zt) {
        final Iterator iterator = this.c.iterator();
        a11 a11 = null;
        while (iterator.hasNext()) {
            final u00 u00 = (u00)iterator.next();
            final Value a12 = u00.b().a(zt.j(u00.a()));
            if (a12 != null) {
                a11 a13;
                if ((a13 = a11) == null) {
                    a13 = new a11();
                }
                a13.m(u00.a(), a12);
                a11 = a13;
            }
        }
        return a11;
    }
    
    public abstract q00 e();
    
    public List f() {
        return this.c;
    }
    
    public du g() {
        return this.a;
    }
    
    public h71 h() {
        return this.b;
    }
    
    public boolean i(final wx0 wx0) {
        return this.a.equals(wx0.a) && this.b.equals(wx0.b);
    }
    
    public int j() {
        return this.g().hashCode() * 31 + this.b.hashCode();
    }
    
    public String k() {
        final StringBuilder sb = new StringBuilder();
        sb.append("key=");
        sb.append(this.a);
        sb.append(", precondition=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public Map l(final pw1 pw1, final MutableDocument mutableDocument) {
        final HashMap hashMap = new HashMap(this.c.size());
        for (final u00 u00 : this.c) {
            hashMap.put(u00.a(), u00.b().b(mutableDocument.j(u00.a()), pw1));
        }
        return hashMap;
    }
    
    public Map m(final MutableDocument mutableDocument, final List list) {
        final HashMap hashMap = new HashMap(this.c.size());
        final int size = this.c.size();
        final int size2 = list.size();
        int i = 0;
        g9.d(size == size2, "server transform count (%d) should match field transform count (%d)", list.size(), this.c.size());
        while (i < list.size()) {
            final u00 u00 = this.c.get(i);
            hashMap.put(u00.a(), u00.b().c(mutableDocument.j(u00.a()), list.get(i)));
            ++i;
        }
        return hashMap;
    }
    
    public void n(final MutableDocument mutableDocument) {
        g9.d(mutableDocument.getKey().equals(this.g()), "Can only apply a mutation to a document with the same key", new Object[0]);
    }
}

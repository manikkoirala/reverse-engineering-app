import java.util.concurrent.TimeUnit;
import java.util.LinkedHashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jw1
{
    public final ag1 a;
    public final a92 b;
    public final long c;
    public final Object d;
    public final Map e;
    
    public jw1(final ag1 ag1, final a92 a92) {
        fg0.e((Object)ag1, "runnableScheduler");
        fg0.e((Object)a92, "launcher");
        this(ag1, a92, 0L, 4, null);
    }
    
    public jw1(final ag1 a, final a92 b, final long c) {
        fg0.e((Object)a, "runnableScheduler");
        fg0.e((Object)b, "launcher");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = new Object();
        this.e = new LinkedHashMap();
    }
    
    public static final void d(final jw1 jw1, final op1 op1) {
        fg0.e((Object)jw1, "this$0");
        fg0.e((Object)op1, "$token");
        jw1.b.d(op1, 3);
    }
    
    public final void b(final op1 op1) {
        fg0.e((Object)op1, "token");
        synchronized (this.d) {
            final Runnable runnable = this.e.remove(op1);
            monitorexit(this.d);
            if (runnable != null) {
                this.a.a(runnable);
            }
        }
    }
    
    public final void c(final op1 op1) {
        fg0.e((Object)op1, "token");
        final iw1 iw1 = new iw1(this, op1);
        synchronized (this.d) {
            final Runnable runnable = this.e.put(op1, iw1);
            monitorexit(this.d);
            this.a.b(this.c, iw1);
        }
    }
}

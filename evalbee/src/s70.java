import java.util.Map;
import com.google.common.collect.ImmutableMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class s70 extends a80 implements ne
{
    @Override
    public ConcurrentMap<Object, Object> asMap() {
        return this.delegate().asMap();
    }
    
    @Override
    public void cleanUp() {
        this.delegate().cleanUp();
    }
    
    @Override
    public abstract ne delegate();
    
    @Override
    public Object get(final Object o, final Callable<Object> callable) {
        return this.delegate().get(o, callable);
    }
    
    @Override
    public ImmutableMap<Object, Object> getAllPresent(final Iterable<?> iterable) {
        return this.delegate().getAllPresent(iterable);
    }
    
    @Override
    public Object getIfPresent(final Object o) {
        return this.delegate().getIfPresent(o);
    }
    
    @Override
    public void invalidate(final Object o) {
        this.delegate().invalidate(o);
    }
    
    @Override
    public void invalidateAll() {
        this.delegate().invalidateAll();
    }
    
    @Override
    public void invalidateAll(final Iterable<?> iterable) {
        this.delegate().invalidateAll(iterable);
    }
    
    @Override
    public void put(final Object o, final Object o2) {
        this.delegate().put(o, o2);
    }
    
    @Override
    public void putAll(final Map<Object, Object> map) {
        this.delegate().putAll(map);
    }
    
    @Override
    public long size() {
        return this.delegate().size();
    }
    
    @Override
    public pe stats() {
        return this.delegate().stats();
    }
}

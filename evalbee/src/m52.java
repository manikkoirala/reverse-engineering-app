import com.android.volley.a;
import java.io.File;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class m52
{
    public static ee1 a(final Context context, final gb gb) {
        mb mb;
        if (gb == null) {
            mb = new mb(new pd0());
        }
        else {
            mb = new mb(gb);
        }
        return b(context, mb);
    }
    
    public static ee1 b(final Context context, final qy0 qy0) {
        final ee1 ee1 = new ee1(new jt((jt.c)new jt.c(context.getApplicationContext()) {
            public File a = null;
            public final Context b;
            
            @Override
            public File get() {
                if (this.a == null) {
                    this.a = new File(this.b.getCacheDir(), "volley");
                }
                return this.a;
            }
        }), qy0);
        ee1.h();
        return ee1;
    }
}

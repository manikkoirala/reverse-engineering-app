import androidx.work.NetworkType;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.BackoffPolicy;
import androidx.work.WorkInfo$State;
import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import java.util.Collections;
import java.util.List;
import androidx.work.b;
import androidx.room.SharedSQLiteStatement;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class t92 implements q92
{
    public final RoomDatabase a;
    public final ix b;
    public final hx c;
    public final SharedSQLiteStatement d;
    public final SharedSQLiteStatement e;
    public final SharedSQLiteStatement f;
    public final SharedSQLiteStatement g;
    public final SharedSQLiteStatement h;
    public final SharedSQLiteStatement i;
    public final SharedSQLiteStatement j;
    public final SharedSQLiteStatement k;
    public final SharedSQLiteStatement l;
    public final SharedSQLiteStatement m;
    public final SharedSQLiteStatement n;
    public final SharedSQLiteStatement o;
    public final SharedSQLiteStatement p;
    public final SharedSQLiteStatement q;
    public final SharedSQLiteStatement r;
    
    public t92(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`last_enqueue_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`out_of_quota_policy`,`period_count`,`generation`,`next_schedule_time_override`,`next_schedule_time_override_generation`,`stop_reason`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
            
            public void k(final ws1 ws1, final p92 p2) {
                final String a = p2.a;
                if (a == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, a);
                }
                final z92 a2 = z92.a;
                ws1.A(2, z92.j(p2.b));
                final String c = p2.c;
                if (c == null) {
                    ws1.I(3);
                }
                else {
                    ws1.y(3, c);
                }
                final String d = p2.d;
                if (d == null) {
                    ws1.I(4);
                }
                else {
                    ws1.y(4, d);
                }
                final byte[] k = b.k(p2.e);
                if (k == null) {
                    ws1.I(5);
                }
                else {
                    ws1.D(5, k);
                }
                final byte[] i = b.k(p2.f);
                if (i == null) {
                    ws1.I(6);
                }
                else {
                    ws1.D(6, i);
                }
                ws1.A(7, p2.g);
                ws1.A(8, p2.h);
                ws1.A(9, p2.i);
                ws1.A(10, p2.k);
                ws1.A(11, z92.a(p2.l));
                ws1.A(12, p2.m);
                ws1.A(13, p2.n);
                ws1.A(14, p2.o);
                ws1.A(15, p2.p);
                ws1.A(16, p2.q ? 1 : 0);
                ws1.A(17, z92.h(p2.r));
                ws1.A(18, p2.i());
                ws1.A(19, p2.f());
                ws1.A(20, p2.g());
                ws1.A(21, p2.h());
                ws1.A(22, p2.j());
                final zk j = p2.j;
                if (j != null) {
                    ws1.A(23, z92.g(j.d()));
                    ws1.A(24, j.g() ? 1 : 0);
                    ws1.A(25, j.h() ? 1 : 0);
                    ws1.A(26, j.f() ? 1 : 0);
                    ws1.A(27, j.i() ? 1 : 0);
                    ws1.A(28, j.b());
                    ws1.A(29, j.a());
                    final byte[] l = z92.i(j.c());
                    if (l != null) {
                        ws1.D(30, l);
                        return;
                    }
                }
                else {
                    ws1.I(23);
                    ws1.I(24);
                    ws1.I(25);
                    ws1.I(26);
                    ws1.I(27);
                    ws1.I(28);
                    ws1.I(29);
                }
                ws1.I(30);
            }
        };
        this.c = new hx(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE OR ABORT `WorkSpec` SET `id` = ?,`state` = ?,`worker_class_name` = ?,`input_merger_class_name` = ?,`input` = ?,`output` = ?,`initial_delay` = ?,`interval_duration` = ?,`flex_duration` = ?,`run_attempt_count` = ?,`backoff_policy` = ?,`backoff_delay_duration` = ?,`last_enqueue_time` = ?,`minimum_retention_duration` = ?,`schedule_requested_at` = ?,`run_in_foreground` = ?,`out_of_quota_policy` = ?,`period_count` = ?,`generation` = ?,`next_schedule_time_override` = ?,`next_schedule_time_override_generation` = ?,`stop_reason` = ?,`required_network_type` = ?,`requires_charging` = ?,`requires_device_idle` = ?,`requires_battery_not_low` = ?,`requires_storage_not_low` = ?,`trigger_content_update_delay` = ?,`trigger_max_content_delay` = ?,`content_uri_triggers` = ? WHERE `id` = ?";
            }
        };
        this.d = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "DELETE FROM workspec WHERE id=?";
            }
        };
        this.e = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET state=? WHERE id=?";
            }
        };
        this.f = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET stop_reason = CASE WHEN state=1 THEN 1 ELSE -256 END, state=5 WHERE id=?";
            }
        };
        this.g = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET period_count=period_count+1 WHERE id=?";
            }
        };
        this.h = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET output=? WHERE id=?";
            }
        };
        this.i = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET last_enqueue_time=? WHERE id=?";
            }
        };
        this.j = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
            }
        };
        this.k = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
            }
        };
        this.l = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET next_schedule_time_override=? WHERE id=?";
            }
        };
        this.m = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET next_schedule_time_override=9223372036854775807 WHERE (id=? AND next_schedule_time_override_generation=?)";
            }
        };
        this.n = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
            }
        };
        this.o = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
            }
        };
        this.p = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
            }
        };
        this.q = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET generation=generation+1 WHERE id=?";
            }
        };
        this.r = new SharedSQLiteStatement(this, a) {
            public final t92 d;
            
            @Override
            public String e() {
                return "UPDATE workspec SET stop_reason=? WHERE id=?";
            }
        };
    }
    
    public static List B() {
        return Collections.emptyList();
    }
    
    @Override
    public int A(final String s) {
        this.a.d();
        final ws1 b = this.j.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            final int p = b.p();
            this.a.D();
            return p;
        }
        finally {
            this.a.i();
            this.j.h(b);
        }
    }
    
    @Override
    public void a(final String s) {
        this.a.d();
        final ws1 b = this.d.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.d.h(b);
        }
    }
    
    @Override
    public void b(final String s, final int n) {
        this.a.d();
        final ws1 b = this.r.b();
        b.A(1, n);
        if (s == null) {
            b.I(2);
        }
        else {
            b.y(2, s);
        }
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.r.h(b);
        }
    }
    
    @Override
    public List c(String string) {
        final sf1 c = sf1.c("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add((Object)string);
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public WorkInfo$State d(final String s) {
        final sf1 c = sf1.c("SELECT state FROM workspec WHERE id=?", 1);
        if (s == null) {
            c.I(1);
        }
        else {
            c.y(1, s);
        }
        this.a.d();
        final RoomDatabase a = this.a;
        final WorkInfo$State workInfo$State = null;
        final Cursor b = bp.b(a, c, false, null);
        WorkInfo$State f = workInfo$State;
        try {
            if (b.moveToFirst()) {
                Integer value;
                if (b.isNull(0)) {
                    value = null;
                }
                else {
                    value = b.getInt(0);
                }
                if (value == null) {
                    f = workInfo$State;
                }
                else {
                    final z92 a2 = z92.a;
                    f = z92.f(value);
                }
            }
            return f;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public int e(final String s) {
        this.a.d();
        final ws1 b = this.f.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            final int p = b.p();
            this.a.D();
            return p;
        }
        finally {
            this.a.i();
            this.f.h(b);
        }
    }
    
    @Override
    public List f(String string) {
        final sf1 c = sf1.c("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add((Object)string);
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public List g(final String s) {
        final sf1 c = sf1.c("SELECT output FROM workspec WHERE id IN\n             (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (s == null) {
            c.I(1);
        }
        else {
            c.y(1, s);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                byte[] blob;
                if (b.isNull(0)) {
                    blob = null;
                }
                else {
                    blob = b.getBlob(0);
                }
                list.add((Object)androidx.work.b.g(blob));
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public List h(int e) {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE state=0 ORDER BY last_enqueue_time LIMIT ?", 1);
        c.A(1, e);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e2 = lo.e(b, "id");
            final int e3 = lo.e(b, "state");
            final int e4 = lo.e(b, "worker_class_name");
            final int e5 = lo.e(b, "input_merger_class_name");
            final int e6 = lo.e(b, "input");
            final int e7 = lo.e(b, "output");
            final int e8 = lo.e(b, "initial_delay");
            final int e9 = lo.e(b, "interval_duration");
            final int e10 = lo.e(b, "flex_duration");
            final int e11 = lo.e(b, "run_attempt_count");
            final int e12 = lo.e(b, "backoff_policy");
            final int e13 = lo.e(b, "backoff_delay_duration");
            final int e14 = lo.e(b, "last_enqueue_time");
            e = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                final ArrayList list = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string;
                    if (b.isNull(e2)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e2);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e3));
                    String string2;
                    if (b.isNull(e4)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e4);
                    }
                    String string3;
                    if (b.isNull(e5)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e5);
                    }
                    byte[] blob;
                    if (b.isNull(e6)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e6);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e7)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e7);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    final long long1 = b.getLong(e8);
                    final long long2 = b.getLong(e9);
                    final long long3 = b.getLong(e10);
                    final int int1 = b.getInt(e11);
                    final BackoffPolicy c2 = z92.c(b.getInt(e12));
                    final long long4 = b.getLong(e13);
                    final long long5 = b.getLong(e14);
                    final long long6 = b.getLong(e);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    list.add((Object)new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5));
                }
                b.close();
                c.release();
                return list;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
    
    @Override
    public int i(final WorkInfo$State workInfo$State, final String s) {
        this.a.d();
        final ws1 b = this.e.b();
        b.A(1, z92.j(workInfo$State));
        if (s == null) {
            b.I(2);
        }
        else {
            b.y(2, s);
        }
        this.a.e();
        try {
            final int p2 = b.p();
            this.a.D();
            return p2;
        }
        finally {
            this.a.i();
            this.e.h(b);
        }
    }
    
    @Override
    public void j(final String s, final long n) {
        this.a.d();
        final ws1 b = this.i.b();
        b.A(1, n);
        if (s == null) {
            b.I(2);
        }
        else {
            b.y(2, s);
        }
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.i.h(b);
        }
    }
    
    @Override
    public boolean k() {
        final boolean b = false;
        final sf1 c = sf1.c("SELECT COUNT(*) > 0 FROM workspec WHERE state NOT IN (2, 3, 5) LIMIT 1", 0);
        this.a.d();
        final Cursor b2 = bp.b(this.a, c, false, null);
        boolean b3 = b;
        try {
            if (b2.moveToFirst()) {
                final int int1 = b2.getInt(0);
                b3 = b;
                if (int1 != 0) {
                    b3 = true;
                }
            }
            return b3;
        }
        finally {
            b2.close();
            c.release();
        }
    }
    
    @Override
    public List l() {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 AND LENGTH(content_uri_triggers)<>0 ORDER BY last_enqueue_time", 0);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e = lo.e(b, "id");
            final int e2 = lo.e(b, "state");
            final int e3 = lo.e(b, "worker_class_name");
            final int e4 = lo.e(b, "input_merger_class_name");
            final int e5 = lo.e(b, "input");
            final int e6 = lo.e(b, "output");
            final int e7 = lo.e(b, "initial_delay");
            final int e8 = lo.e(b, "interval_duration");
            final int e9 = lo.e(b, "flex_duration");
            final int e10 = lo.e(b, "run_attempt_count");
            final int e11 = lo.e(b, "backoff_policy");
            final int e12 = lo.e(b, "backoff_delay_duration");
            final int e13 = lo.e(b, "last_enqueue_time");
            final int e14 = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                final ArrayList list = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string;
                    if (b.isNull(e)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e2));
                    String string2;
                    if (b.isNull(e3)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e3);
                    }
                    String string3;
                    if (b.isNull(e4)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e4);
                    }
                    byte[] blob;
                    if (b.isNull(e5)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e5);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e6);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    final long long1 = b.getLong(e7);
                    final long long2 = b.getLong(e8);
                    final long long3 = b.getLong(e9);
                    final int int1 = b.getInt(e10);
                    final BackoffPolicy c2 = z92.c(b.getInt(e11));
                    final long long4 = b.getLong(e12);
                    final long long5 = b.getLong(e13);
                    final long long6 = b.getLong(e14);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    list.add((Object)new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5));
                }
                b.close();
                c.release();
                return list;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
    
    @Override
    public int m(final String s) {
        this.a.d();
        final ws1 b = this.k.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            final int p = b.p();
            this.a.D();
            return p;
        }
        finally {
            this.a.i();
            this.k.h(b);
        }
    }
    
    @Override
    public int n() {
        int int1 = 0;
        final sf1 c = sf1.c("Select COUNT(*) FROM workspec WHERE LENGTH(content_uri_triggers)<>0 AND state NOT IN (2, 3, 5)", 0);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            if (b.moveToFirst()) {
                int1 = b.getInt(0);
            }
            return int1;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public void o(final String s, final int n) {
        this.a.d();
        final ws1 b = this.m.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        b.A(2, n);
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.m.h(b);
        }
    }
    
    @Override
    public void p(final String s) {
        this.a.d();
        final ws1 b = this.g.b();
        if (s == null) {
            b.I(1);
        }
        else {
            b.y(1, s);
        }
        this.a.e();
        try {
            b.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.g.h(b);
        }
    }
    
    @Override
    public List q(long long1) {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE last_enqueue_time >= ? AND state IN (2, 3, 5) ORDER BY last_enqueue_time DESC", 1);
        c.A(1, long1);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e = lo.e(b, "id");
            final int e2 = lo.e(b, "state");
            final int e3 = lo.e(b, "worker_class_name");
            final int e4 = lo.e(b, "input_merger_class_name");
            final int e5 = lo.e(b, "input");
            final int e6 = lo.e(b, "output");
            final int e7 = lo.e(b, "initial_delay");
            final int e8 = lo.e(b, "interval_duration");
            final int e9 = lo.e(b, "flex_duration");
            final int e10 = lo.e(b, "run_attempt_count");
            final int e11 = lo.e(b, "backoff_policy");
            final int e12 = lo.e(b, "backoff_delay_duration");
            final int e13 = lo.e(b, "last_enqueue_time");
            final int e14 = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                final ArrayList list = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string;
                    if (b.isNull(e)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e2));
                    String string2;
                    if (b.isNull(e3)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e3);
                    }
                    String string3;
                    if (b.isNull(e4)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e4);
                    }
                    byte[] blob;
                    if (b.isNull(e5)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e5);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e6);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    long1 = b.getLong(e7);
                    final long long2 = b.getLong(e8);
                    final long long3 = b.getLong(e9);
                    final int int1 = b.getInt(e10);
                    final BackoffPolicy c2 = z92.c(b.getInt(e11));
                    final long long4 = b.getLong(e12);
                    final long long5 = b.getLong(e13);
                    final long long6 = b.getLong(e14);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    list.add((Object)new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5));
                }
                b.close();
                c.release();
                return list;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
    
    @Override
    public List r() {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e = lo.e(b, "id");
            final int e2 = lo.e(b, "state");
            final int e3 = lo.e(b, "worker_class_name");
            final int e4 = lo.e(b, "input_merger_class_name");
            final int e5 = lo.e(b, "input");
            final int e6 = lo.e(b, "output");
            final int e7 = lo.e(b, "initial_delay");
            final int e8 = lo.e(b, "interval_duration");
            final int e9 = lo.e(b, "flex_duration");
            final int e10 = lo.e(b, "run_attempt_count");
            final int e11 = lo.e(b, "backoff_policy");
            final int e12 = lo.e(b, "backoff_delay_duration");
            final int e13 = lo.e(b, "last_enqueue_time");
            final int e14 = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                final ArrayList list = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string;
                    if (b.isNull(e)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e2));
                    String string2;
                    if (b.isNull(e3)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e3);
                    }
                    String string3;
                    if (b.isNull(e4)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e4);
                    }
                    byte[] blob;
                    if (b.isNull(e5)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e5);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e6);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    final long long1 = b.getLong(e7);
                    final long long2 = b.getLong(e8);
                    final long long3 = b.getLong(e9);
                    final int int1 = b.getInt(e10);
                    final BackoffPolicy c2 = z92.c(b.getInt(e11));
                    final long long4 = b.getLong(e12);
                    final long long5 = b.getLong(e13);
                    final long long6 = b.getLong(e14);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    list.add((Object)new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5));
                }
                b.close();
                c.release();
                return list;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
    
    @Override
    public p92 s(String string) {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE id=?", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e = lo.e(b, "id");
            final int e2 = lo.e(b, "state");
            final int e3 = lo.e(b, "worker_class_name");
            final int e4 = lo.e(b, "input_merger_class_name");
            final int e5 = lo.e(b, "input");
            final int e6 = lo.e(b, "output");
            final int e7 = lo.e(b, "initial_delay");
            final int e8 = lo.e(b, "interval_duration");
            final int e9 = lo.e(b, "flex_duration");
            final int e10 = lo.e(b, "run_attempt_count");
            final int e11 = lo.e(b, "backoff_policy");
            final int e12 = lo.e(b, "backoff_delay_duration");
            final int e13 = lo.e(b, "last_enqueue_time");
            final int e14 = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                p92 p92;
                if (b.moveToFirst()) {
                    if (b.isNull(e)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e2));
                    String string2;
                    if (b.isNull(e3)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e3);
                    }
                    String string3;
                    if (b.isNull(e4)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e4);
                    }
                    byte[] blob;
                    if (b.isNull(e5)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e5);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e6);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    final long long1 = b.getLong(e7);
                    final long long2 = b.getLong(e8);
                    final long long3 = b.getLong(e9);
                    final int int1 = b.getInt(e10);
                    final BackoffPolicy c2 = z92.c(b.getInt(e11));
                    final long long4 = b.getLong(e12);
                    final long long5 = b.getLong(e13);
                    final long long6 = b.getLong(e14);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    p92 = new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5);
                }
                else {
                    p92 = null;
                }
                b.close();
                c.release();
                return p92;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
    
    @Override
    public void t(final p92 p) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(p);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
    
    @Override
    public int u() {
        this.a.d();
        final ws1 b = this.o.b();
        this.a.e();
        try {
            final int p = b.p();
            this.a.D();
            return p;
        }
        finally {
            this.a.i();
            this.o.h(b);
        }
    }
    
    @Override
    public int v(final String s, final long n) {
        this.a.d();
        final ws1 b = this.n.b();
        b.A(1, n);
        if (s == null) {
            b.I(2);
        }
        else {
            b.y(2, s);
        }
        this.a.e();
        try {
            final int p2 = b.p();
            this.a.D();
            return p2;
        }
        finally {
            this.a.i();
            this.n.h(b);
        }
    }
    
    @Override
    public List w(String string) {
        final sf1 c = sf1.c("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add((Object)new p92.b(string, z92.f(b.getInt(1))));
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public List x(int e) {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY last_enqueue_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND LENGTH(content_uri_triggers)=0 AND state NOT IN (2, 3, 5))", 1);
        c.A(1, e);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e2 = lo.e(b, "id");
            final int e3 = lo.e(b, "state");
            final int e4 = lo.e(b, "worker_class_name");
            final int e5 = lo.e(b, "input_merger_class_name");
            final int e6 = lo.e(b, "input");
            final int e7 = lo.e(b, "output");
            final int e8 = lo.e(b, "initial_delay");
            final int e9 = lo.e(b, "interval_duration");
            final int e10 = lo.e(b, "flex_duration");
            final int e11 = lo.e(b, "run_attempt_count");
            final int e12 = lo.e(b, "backoff_policy");
            final int e13 = lo.e(b, "backoff_delay_duration");
            final int e14 = lo.e(b, "last_enqueue_time");
            e = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                final ArrayList list = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string;
                    if (b.isNull(e2)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e2);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e3));
                    String string2;
                    if (b.isNull(e4)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e4);
                    }
                    String string3;
                    if (b.isNull(e5)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e5);
                    }
                    byte[] blob;
                    if (b.isNull(e6)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e6);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e7)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e7);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    final long long1 = b.getLong(e8);
                    final long long2 = b.getLong(e9);
                    final long long3 = b.getLong(e10);
                    final int int1 = b.getInt(e11);
                    final BackoffPolicy c2 = z92.c(b.getInt(e12));
                    final long long4 = b.getLong(e13);
                    final long long5 = b.getLong(e14);
                    final long long6 = b.getLong(e);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    list.add((Object)new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5));
                }
                b.close();
                c.release();
                return list;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
    
    @Override
    public void y(final String s, final b b) {
        this.a.d();
        final ws1 b2 = this.h.b();
        final byte[] k = b.k(b);
        if (k == null) {
            b2.I(1);
        }
        else {
            b2.D(1, k);
        }
        if (s == null) {
            b2.I(2);
        }
        else {
            b2.y(2, s);
        }
        this.a.e();
        try {
            b2.p();
            this.a.D();
        }
        finally {
            this.a.i();
            this.h.h(b2);
        }
    }
    
    @Override
    public List z() {
        final sf1 c = sf1.c("SELECT * FROM workspec WHERE state=1", 0);
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final int e = lo.e(b, "id");
            final int e2 = lo.e(b, "state");
            final int e3 = lo.e(b, "worker_class_name");
            final int e4 = lo.e(b, "input_merger_class_name");
            final int e5 = lo.e(b, "input");
            final int e6 = lo.e(b, "output");
            final int e7 = lo.e(b, "initial_delay");
            final int e8 = lo.e(b, "interval_duration");
            final int e9 = lo.e(b, "flex_duration");
            final int e10 = lo.e(b, "run_attempt_count");
            final int e11 = lo.e(b, "backoff_policy");
            final int e12 = lo.e(b, "backoff_delay_duration");
            final int e13 = lo.e(b, "last_enqueue_time");
            final int e14 = lo.e(b, "minimum_retention_duration");
            try {
                final int e15 = lo.e(b, "schedule_requested_at");
                final int e16 = lo.e(b, "run_in_foreground");
                final int e17 = lo.e(b, "out_of_quota_policy");
                final int e18 = lo.e(b, "period_count");
                final int e19 = lo.e(b, "generation");
                final int e20 = lo.e(b, "next_schedule_time_override");
                final int e21 = lo.e(b, "next_schedule_time_override_generation");
                final int e22 = lo.e(b, "stop_reason");
                final int e23 = lo.e(b, "required_network_type");
                final int e24 = lo.e(b, "requires_charging");
                final int e25 = lo.e(b, "requires_device_idle");
                final int e26 = lo.e(b, "requires_battery_not_low");
                final int e27 = lo.e(b, "requires_storage_not_low");
                final int e28 = lo.e(b, "trigger_content_update_delay");
                final int e29 = lo.e(b, "trigger_max_content_delay");
                final int e30 = lo.e(b, "content_uri_triggers");
                final ArrayList list = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string;
                    if (b.isNull(e)) {
                        string = null;
                    }
                    else {
                        string = b.getString(e);
                    }
                    final WorkInfo$State f = z92.f(b.getInt(e2));
                    String string2;
                    if (b.isNull(e3)) {
                        string2 = null;
                    }
                    else {
                        string2 = b.getString(e3);
                    }
                    String string3;
                    if (b.isNull(e4)) {
                        string3 = null;
                    }
                    else {
                        string3 = b.getString(e4);
                    }
                    byte[] blob;
                    if (b.isNull(e5)) {
                        blob = null;
                    }
                    else {
                        blob = b.getBlob(e5);
                    }
                    final b g = androidx.work.b.g(blob);
                    byte[] blob2;
                    if (b.isNull(e6)) {
                        blob2 = null;
                    }
                    else {
                        blob2 = b.getBlob(e6);
                    }
                    final b g2 = androidx.work.b.g(blob2);
                    final long long1 = b.getLong(e7);
                    final long long2 = b.getLong(e8);
                    final long long3 = b.getLong(e9);
                    final int int1 = b.getInt(e10);
                    final BackoffPolicy c2 = z92.c(b.getInt(e11));
                    final long long4 = b.getLong(e12);
                    final long long5 = b.getLong(e13);
                    final long long6 = b.getLong(e14);
                    final long long7 = b.getLong(e15);
                    final boolean b2 = b.getInt(e16) != 0;
                    final OutOfQuotaPolicy e31 = z92.e(b.getInt(e17));
                    final int int2 = b.getInt(e18);
                    final int int3 = b.getInt(e19);
                    final long long8 = b.getLong(e20);
                    final int int4 = b.getInt(e21);
                    final int int5 = b.getInt(e22);
                    final NetworkType d = z92.d(b.getInt(e23));
                    final boolean b3 = b.getInt(e24) != 0;
                    final boolean b4 = b.getInt(e25) != 0;
                    final boolean b5 = b.getInt(e26) != 0;
                    final boolean b6 = b.getInt(e27) != 0;
                    final long long9 = b.getLong(e28);
                    final long long10 = b.getLong(e29);
                    byte[] blob3;
                    if (b.isNull(e30)) {
                        blob3 = null;
                    }
                    else {
                        blob3 = b.getBlob(e30);
                    }
                    list.add((Object)new p92(string, f, string2, string3, g, g2, long1, long2, long3, new zk(d, b3, b4, b5, b6, long9, long10, z92.b(blob3)), int1, c2, long4, long5, long6, long7, b2, e31, int2, int3, long8, int4, int5));
                }
                b.close();
                c.release();
                return list;
            }
            finally {}
        }
        finally {}
        b.close();
        c.release();
    }
}

import java.nio.charset.Charset;
import com.google.common.hash.Funnel;
import com.google.common.hash.HashCode;
import java.nio.ByteBuffer;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class m implements oc0
{
    public HashCode hashBytes(final ByteBuffer byteBuffer) {
        return this.newHasher(byteBuffer.remaining()).i(byteBuffer).e();
    }
    
    public HashCode hashBytes(final byte[] array) {
        return this.hashBytes(array, 0, array.length);
    }
    
    public HashCode hashBytes(final byte[] array, final int n, final int n2) {
        i71.w(n, n + n2, array.length);
        return this.newHasher(n2).h(array, n, n2).e();
    }
    
    public HashCode hashInt(final int n) {
        return this.newHasher(4).a(n).e();
    }
    
    public HashCode hashLong(final long n) {
        return this.newHasher(8).b(n).e();
    }
    
    @Override
    public <T> HashCode hashObject(final T t, final Funnel<? super T> funnel) {
        return this.newHasher().g(t, funnel).e();
    }
    
    public HashCode hashString(final CharSequence charSequence, final Charset charset) {
        return this.newHasher().d(charSequence, charset).e();
    }
    
    public HashCode hashUnencodedChars(final CharSequence charSequence) {
        return this.newHasher(charSequence.length() * 2).c(charSequence).e();
    }
    
    public qc0 newHasher(final int n) {
        i71.h(n >= 0, "expectedInputSize must be >= 0 but was %s", n);
        return this.newHasher();
    }
}

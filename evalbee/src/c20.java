import com.google.android.gms.tasks.Continuation;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.util.Logger;
import com.google.android.gms.tasks.Task;

// 
// Decompiled by Procyon v0.6.0
// 

public final class c20 extends eo
{
    public final ee0 a;
    public zf0 b;
    public lk0 c;
    public int d;
    public boolean e;
    
    public c20(final jr jr) {
        this.a = new z10(this);
        jr.a((jr.a)new a20(this));
    }
    
    @Override
    public Task a() {
        synchronized (this) {
            final zf0 b = this.b;
            if (b == null) {
                return Tasks.forException((Exception)new FirebaseApiNotAvailableException("auth is not available"));
            }
            final Task c = b.c(this.e);
            this.e = false;
            return c.continueWithTask(wy.b, (Continuation)new b20(this, this.d));
        }
    }
    
    @Override
    public void b() {
        synchronized (this) {
            this.e = true;
        }
    }
    
    @Override
    public void c(final lk0 c) {
        synchronized (this) {
            (this.c = c).a(this.g());
        }
    }
    
    public final u12 g() {
        synchronized (this) {
            final zf0 b = this.b;
            String a;
            if (b == null) {
                a = null;
            }
            else {
                a = b.a();
            }
            u12 b2;
            if (a != null) {
                b2 = new u12(a);
            }
            else {
                b2 = u12.b;
            }
            return b2;
        }
    }
    
    public final void k() {
        synchronized (this) {
            ++this.d;
            final lk0 c = this.c;
            if (c != null) {
                c.a(this.g());
            }
        }
    }
}

import java.util.Arrays;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ka extends ye0
{
    public final int a;
    public final du b;
    public final byte[] c;
    public final byte[] d;
    
    public ka(final int a, final du b, final byte[] c, final byte[] d) {
        this.a = a;
        if (b == null) {
            throw new NullPointerException("Null documentKey");
        }
        this.b = b;
        if (c == null) {
            throw new NullPointerException("Null arrayValue");
        }
        this.c = c;
        if (d != null) {
            this.d = d;
            return;
        }
        throw new NullPointerException("Null directionalValue");
    }
    
    @Override
    public byte[] d() {
        return this.c;
    }
    
    @Override
    public byte[] e() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ye0) {
            final ye0 ye0 = (ye0)o;
            if (this.a == ye0.g() && this.b.equals(ye0.f())) {
                final byte[] c = this.c;
                final boolean b2 = ye0 instanceof ka;
                byte[] a2;
                if (b2) {
                    a2 = ((ka)ye0).c;
                }
                else {
                    a2 = ye0.d();
                }
                if (Arrays.equals(c, a2)) {
                    final byte[] d = this.d;
                    byte[] a3;
                    if (b2) {
                        a3 = ((ka)ye0).d;
                    }
                    else {
                        a3 = ye0.e();
                    }
                    if (Arrays.equals(d, a3)) {
                        return b;
                    }
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public du f() {
        return this.b;
    }
    
    @Override
    public int g() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        return (((this.a ^ 0xF4243) * 1000003 ^ this.b.hashCode()) * 1000003 ^ Arrays.hashCode(this.c)) * 1000003 ^ Arrays.hashCode(this.d);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IndexEntry{indexId=");
        sb.append(this.a);
        sb.append(", documentKey=");
        sb.append(this.b);
        sb.append(", arrayValue=");
        sb.append(Arrays.toString(this.c));
        sb.append(", directionalValue=");
        sb.append(Arrays.toString(this.d));
        sb.append("}");
        return sb.toString();
    }
}

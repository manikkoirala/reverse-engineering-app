import java.lang.reflect.Array;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;

// 
// Decompiled by Procyon v0.6.0
// 

public final class s8 implements Collection, Set
{
    public static final int[] e;
    public static final Object[] f;
    public static Object[] g;
    public static int h;
    public static Object[] i;
    public static int j;
    public int[] a;
    public Object[] b;
    public int c;
    public ym0 d;
    
    static {
        e = new int[0];
        f = new Object[0];
    }
    
    public s8() {
        this(0);
    }
    
    public s8(final int n) {
        if (n == 0) {
            this.a = s8.e;
            this.b = s8.f;
        }
        else {
            this.a(n);
        }
        this.c = 0;
    }
    
    public static void c(final int[] array, final Object[] array2, int i) {
        if (array.length == 8) {
            synchronized (s8.class) {
                if (s8.j < 10) {
                    array2[0] = s8.i;
                    array2[1] = array;
                    --i;
                    while (i >= 2) {
                        array2[i] = null;
                        --i;
                    }
                    s8.i = array2;
                    ++s8.j;
                }
                return;
            }
        }
        if (array.length == 4) {
            synchronized (s8.class) {
                if (s8.h < 10) {
                    array2[0] = s8.g;
                    array2[1] = array;
                    --i;
                    while (i >= 2) {
                        array2[i] = null;
                        --i;
                    }
                    s8.g = array2;
                    ++s8.h;
                }
            }
        }
    }
    
    public final void a(final int n) {
        Label_0145: {
            if (n == 8) {
                synchronized (s8.class) {
                    final Object[] i = s8.i;
                    if (i != null) {
                        this.b = i;
                        s8.i = (Object[])i[0];
                        this.a = (int[])i[1];
                        i[0] = (i[1] = null);
                        --s8.j;
                        return;
                    }
                    break Label_0145;
                }
            }
            if (n == 4) {
                synchronized (s8.class) {
                    final Object[] g = s8.g;
                    if (g != null) {
                        this.b = g;
                        s8.g = (Object[])g[0];
                        this.a = (int[])g[1];
                        g[0] = (g[1] = null);
                        --s8.h;
                        return;
                    }
                }
            }
        }
        this.a = new int[n];
        this.b = new Object[n];
    }
    
    @Override
    public boolean add(final Object o) {
        int n;
        int hashCode;
        if (o == null) {
            n = this.l();
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
            n = this.i(o, hashCode);
        }
        if (n >= 0) {
            return false;
        }
        final int n2 = ~n;
        final int c = this.c;
        final int[] a = this.a;
        if (c >= a.length) {
            int n3 = 8;
            if (c >= 8) {
                n3 = (c >> 1) + c;
            }
            else if (c < 4) {
                n3 = 4;
            }
            final Object[] b = this.b;
            this.a(n3);
            final int[] a2 = this.a;
            if (a2.length > 0) {
                System.arraycopy(a, 0, a2, 0, a.length);
                System.arraycopy(b, 0, this.b, 0, b.length);
            }
            c(a, b, this.c);
        }
        final int c2 = this.c;
        if (n2 < c2) {
            final int[] a3 = this.a;
            final int n4 = n2 + 1;
            System.arraycopy(a3, n2, a3, n4, c2 - n2);
            final Object[] b2 = this.b;
            System.arraycopy(b2, n2, b2, n4, this.c - n2);
        }
        this.a[n2] = hashCode;
        this.b[n2] = o;
        ++this.c;
        return true;
    }
    
    @Override
    public boolean addAll(final Collection collection) {
        this.b(this.c + collection.size());
        final Iterator iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            b |= this.add(iterator.next());
        }
        return b;
    }
    
    public void b(int c) {
        final int[] a = this.a;
        if (a.length < c) {
            final Object[] b = this.b;
            this.a(c);
            c = this.c;
            if (c > 0) {
                System.arraycopy(a, 0, this.a, 0, c);
                System.arraycopy(b, 0, this.b, 0, this.c);
            }
            c(a, b, this.c);
        }
    }
    
    @Override
    public void clear() {
        final int c = this.c;
        if (c != 0) {
            c(this.a, this.b, c);
            this.a = s8.e;
            this.b = s8.f;
            this.c = 0;
        }
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    public boolean containsAll(final Collection collection) {
        final Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (!this.contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Set)) {
            return false;
        }
        final Set set = (Set)o;
        if (this.size() != set.size()) {
            return false;
        }
        int i = 0;
        try {
            while (i < this.c) {
                if (!set.contains(this.n(i))) {
                    return false;
                }
                ++i;
            }
            return true;
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    public final ym0 g() {
        if (this.d == null) {
            this.d = new ym0(this) {
                public final s8 d;
                
                @Override
                public void a() {
                    this.d.clear();
                }
                
                @Override
                public Object b(final int n, final int n2) {
                    return this.d.b[n];
                }
                
                @Override
                public Map c() {
                    throw new UnsupportedOperationException("not a map");
                }
                
                @Override
                public int d() {
                    return this.d.c;
                }
                
                @Override
                public int e(final Object o) {
                    return this.d.indexOf(o);
                }
                
                @Override
                public int f(final Object o) {
                    return this.d.indexOf(o);
                }
                
                @Override
                public void g(final Object o, final Object o2) {
                    this.d.add(o);
                }
                
                @Override
                public void h(final int n) {
                    this.d.m(n);
                }
                
                @Override
                public Object i(final int n, final Object o) {
                    throw new UnsupportedOperationException("not a map");
                }
            };
        }
        return this.d;
    }
    
    @Override
    public int hashCode() {
        final int[] a = this.a;
        final int c = this.c;
        int i = 0;
        int n = 0;
        while (i < c) {
            n += a[i];
            ++i;
        }
        return n;
    }
    
    public final int i(final Object o, final int n) {
        final int c = this.c;
        if (c == 0) {
            return -1;
        }
        final int a = el.a(this.a, c, n);
        if (a < 0) {
            return a;
        }
        if (o.equals(this.b[a])) {
            return a;
        }
        int n2;
        for (n2 = a + 1; n2 < c && this.a[n2] == n; ++n2) {
            if (o.equals(this.b[n2])) {
                return n2;
            }
        }
        for (int n3 = a - 1; n3 >= 0 && this.a[n3] == n; --n3) {
            if (o.equals(this.b[n3])) {
                return n3;
            }
        }
        return ~n2;
    }
    
    public int indexOf(final Object o) {
        int n;
        if (o == null) {
            n = this.l();
        }
        else {
            n = this.i(o, o.hashCode());
        }
        return n;
    }
    
    @Override
    public boolean isEmpty() {
        return this.c <= 0;
    }
    
    @Override
    public Iterator iterator() {
        return this.g().m().iterator();
    }
    
    public final int l() {
        final int c = this.c;
        if (c == 0) {
            return -1;
        }
        final int a = el.a(this.a, c, 0);
        if (a < 0) {
            return a;
        }
        if (this.b[a] == null) {
            return a;
        }
        int n;
        for (n = a + 1; n < c && this.a[n] == 0; ++n) {
            if (this.b[n] == null) {
                return n;
            }
        }
        for (int n2 = a - 1; n2 >= 0 && this.a[n2] == 0; --n2) {
            if (this.b[n2] == null) {
                return n2;
            }
        }
        return ~n;
    }
    
    public Object m(final int n) {
        final Object[] b = this.b;
        final Object o = b[n];
        final int c = this.c;
        if (c <= 1) {
            c(this.a, b, c);
            this.a = s8.e;
            this.b = s8.f;
            this.c = 0;
        }
        else {
            final int[] a = this.a;
            final int length = a.length;
            int n2 = 8;
            if (length > 8 && c < a.length / 3) {
                if (c > 8) {
                    n2 = c + (c >> 1);
                }
                this.a(n2);
                --this.c;
                if (n > 0) {
                    System.arraycopy(a, 0, this.a, 0, n);
                    System.arraycopy(b, 0, this.b, 0, n);
                }
                final int c2 = this.c;
                if (n < c2) {
                    final int n3 = n + 1;
                    System.arraycopy(a, n3, this.a, n, c2 - n);
                    System.arraycopy(b, n3, this.b, n, this.c - n);
                }
            }
            else {
                final int c3 = c - 1;
                if (n < (this.c = c3)) {
                    final int n4 = n + 1;
                    System.arraycopy(a, n4, a, n, c3 - n);
                    final Object[] b2 = this.b;
                    System.arraycopy(b2, n4, b2, n, this.c - n);
                }
                this.b[this.c] = null;
            }
        }
        return o;
    }
    
    public Object n(final int n) {
        return this.b[n];
    }
    
    @Override
    public boolean remove(final Object o) {
        final int index = this.indexOf(o);
        if (index >= 0) {
            this.m(index);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean removeAll(final Collection collection) {
        final Iterator iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            b |= this.remove(iterator.next());
        }
        return b;
    }
    
    @Override
    public boolean retainAll(final Collection collection) {
        int i = this.c - 1;
        boolean b = false;
        while (i >= 0) {
            if (!collection.contains(this.b[i])) {
                this.m(i);
                b = true;
            }
            --i;
        }
        return b;
    }
    
    @Override
    public int size() {
        return this.c;
    }
    
    @Override
    public Object[] toArray() {
        final int c = this.c;
        final Object[] array = new Object[c];
        System.arraycopy(this.b, 0, array, 0, c);
        return array;
    }
    
    @Override
    public Object[] toArray(final Object[] array) {
        Object[] array2 = array;
        if (array.length < this.c) {
            array2 = (Object[])Array.newInstance(array.getClass().getComponentType(), this.c);
        }
        System.arraycopy(this.b, 0, array2, 0, this.c);
        final int length = array2.length;
        final int c = this.c;
        if (length > c) {
            array2[c] = null;
        }
        return array2;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(this.c * 14);
        sb.append('{');
        for (int i = 0; i < this.c; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            final Object n = this.n(i);
            if (n != this) {
                sb.append(n);
            }
            else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}

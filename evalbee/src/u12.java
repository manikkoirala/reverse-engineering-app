// 
// Decompiled by Procyon v0.6.0
// 

public final class u12
{
    public static final u12 b;
    public final String a;
    
    static {
        b = new u12(null);
    }
    
    public u12(final String a) {
        this.a = a;
    }
    
    public String a() {
        return this.a;
    }
    
    public boolean b() {
        return this.a != null;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (o != null && u12.class == o.getClass()) {
            final u12 u12 = (u12)o;
            final String a = this.a;
            final String a2 = u12.a;
            if (a != null) {
                equals = a.equals(a2);
            }
            else if (a2 != null) {
                equals = false;
            }
            return equals;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final String a = this.a;
        int hashCode;
        if (a != null) {
            hashCode = a.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("User(uid:");
        sb.append(this.a);
        sb.append(")");
        return sb.toString();
    }
}

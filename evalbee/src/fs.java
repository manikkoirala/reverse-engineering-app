import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import com.android.volley.d;
import java.util.ArrayList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class fs
{
    public ee1 a;
    public zg b;
    public Context c;
    public String d;
    public ArrayList e;
    
    public fs(final Context c, final ArrayList e, final zg b) {
        this.b = b;
        this.c = c;
        this.e = e;
        this.a = new n52(c, a91.x()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.x());
        sb.append(":");
        sb.append(a91.q());
        sb.append("/api-online/class-list/delete");
        this.d = sb.toString();
        this.f();
    }
    
    public static /* synthetic */ ArrayList c(final fs fs) {
        return fs.e;
    }
    
    public final void d(final String s) {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final fs a;
            
            public void b(final String s) {
                try {
                    final fs a = this.a;
                    a.e(true, 200, fs.c(a));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final fs a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.e(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final fs x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(fs.c(this.x)).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final fs a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
}

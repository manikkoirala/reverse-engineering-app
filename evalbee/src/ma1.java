import java.io.InputStream;
import java.io.IOException;
import java.util.Locale;
import java.io.Closeable;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.io.File;
import java.nio.charset.Charset;

// 
// Decompiled by Procyon v0.6.0
// 

public class ma1 implements y00
{
    public static final Charset d;
    public final File a;
    public final int b;
    public la1 c;
    
    static {
        d = Charset.forName("UTF-8");
    }
    
    public ma1(final File a, final int b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public byte[] a() {
        final b g = this.g();
        if (g == null) {
            return null;
        }
        final int b = g.b;
        final byte[] array = new byte[b];
        System.arraycopy(g.a, 0, array, 0, b);
        return array;
    }
    
    @Override
    public void b() {
        this.d();
        this.a.delete();
    }
    
    @Override
    public void c(final long n, final String s) {
        this.h();
        this.f(n, s);
    }
    
    @Override
    public void d() {
        CommonUtils.f(this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }
    
    @Override
    public String e() {
        final byte[] a = this.a();
        String s;
        if (a != null) {
            s = new String(a, ma1.d);
        }
        else {
            s = null;
        }
        return s;
    }
    
    public final void f(final long l, String s) {
        if (this.c == null) {
            return;
        }
        String s2;
        if ((s2 = s) == null) {
            s2 = "null";
        }
        try {
            final int n = this.b / 4;
            s = s2;
            if (s2.length() > n) {
                final StringBuilder sb = new StringBuilder();
                sb.append("...");
                sb.append(s2.substring(s2.length() - n));
                s = sb.toString();
            }
            s = s.replaceAll("\r", " ").replaceAll("\n", " ");
            this.c.e(String.format(Locale.US, "%d %s%n", l, s).getBytes(ma1.d));
            while (!this.c.k() && this.c.Z() > this.b) {
                this.c.J();
            }
        }
        catch (final IOException ex) {
            zl0.f().e("There was a problem writing to the Crashlytics log.", ex);
        }
    }
    
    public final b g() {
        if (!this.a.exists()) {
            return null;
        }
        this.h();
        final la1 c = this.c;
        if (c == null) {
            return null;
        }
        final int[] array = { 0 };
        final byte[] array2 = new byte[c.Z()];
        try {
            this.c.i((la1.d)new la1.d(this, array2, array) {
                public final byte[] a;
                public final int[] b;
                public final ma1 c;
                
                @Override
                public void a(final InputStream inputStream, final int len) {
                    try {
                        inputStream.read(this.a, this.b[0], len);
                        final int[] b = this.b;
                        b[0] += len;
                    }
                    finally {
                        inputStream.close();
                    }
                }
            });
        }
        catch (final IOException ex) {
            zl0.f().e("A problem occurred while reading the Crashlytics log file.", ex);
        }
        return new b(array2, array[0]);
    }
    
    public final void h() {
        if (this.c == null) {
            try {
                this.c = new la1(this.a);
            }
            catch (final IOException ex) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not open log file: ");
                sb.append(this.a);
                f.e(sb.toString(), ex);
            }
        }
    }
    
    public static class b
    {
        public final byte[] a;
        public final int b;
        
        public b(final byte[] a, final int b) {
            this.a = a;
            this.b = b;
        }
    }
}

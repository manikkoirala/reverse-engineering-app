import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.f;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;

// 
// Decompiled by Procyon v0.6.0
// 

public class hc0 extends WidgetRun
{
    public hc0(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        constraintWidget.e.f();
        constraintWidget.f.f();
        super.f = ((f)constraintWidget).s1();
    }
    
    @Override
    public void a(final ps ps) {
        final DependencyNode h = super.h;
        if (!h.c) {
            return;
        }
        if (h.j) {
            return;
        }
        super.h.d((int)(((DependencyNode)h.l.get(0)).g * ((f)super.b).v1() + 0.5f));
    }
    
    @Override
    public void d() {
        final f f = (f)super.b;
        int t1 = f.t1();
        final int u1 = f.u1();
        f.v1();
        WidgetRun widgetRun;
        if (f.s1() == 1) {
            final DependencyNode h = super.h;
            Label_0222: {
                DependencyNode dependencyNode;
                if (t1 != -1) {
                    h.l.add(super.b.a0.e.h);
                    super.b.a0.e.h.k.add(super.h);
                    dependencyNode = super.h;
                }
                else {
                    if (u1 == -1) {
                        h.b = true;
                        h.l.add(super.b.a0.e.i);
                        super.b.a0.e.i.k.add(super.h);
                        break Label_0222;
                    }
                    h.l.add(super.b.a0.e.i);
                    super.b.a0.e.i.k.add(super.h);
                    dependencyNode = super.h;
                    t1 = -u1;
                }
                dependencyNode.f = t1;
            }
            this.q(super.b.e.h);
            widgetRun = super.b.e;
        }
        else {
            final DependencyNode h2 = super.h;
            Label_0438: {
                DependencyNode dependencyNode2;
                if (t1 != -1) {
                    h2.l.add(super.b.a0.f.h);
                    super.b.a0.f.h.k.add(super.h);
                    dependencyNode2 = super.h;
                }
                else {
                    if (u1 == -1) {
                        h2.b = true;
                        h2.l.add(super.b.a0.f.i);
                        super.b.a0.f.i.k.add(super.h);
                        break Label_0438;
                    }
                    h2.l.add(super.b.a0.f.i);
                    super.b.a0.f.i.k.add(super.h);
                    dependencyNode2 = super.h;
                    t1 = -u1;
                }
                dependencyNode2.f = t1;
            }
            this.q(super.b.f.h);
            widgetRun = super.b.f;
        }
        this.q(widgetRun.i);
    }
    
    @Override
    public void e() {
        if (((f)super.b).s1() == 1) {
            super.b.m1(super.h.g);
        }
        else {
            super.b.n1(super.h.g);
        }
    }
    
    @Override
    public void f() {
        super.h.c();
    }
    
    @Override
    public boolean m() {
        return false;
    }
    
    public final void q(final DependencyNode dependencyNode) {
        super.h.k.add(dependencyNode);
        dependencyNode.l.add(super.h);
    }
}

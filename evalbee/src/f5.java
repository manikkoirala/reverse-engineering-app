import android.os.AsyncTask;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import java.util.Iterator;
import java.util.Collection;
import java.util.Arrays;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class f5
{
    public int a;
    public b b;
    public ArrayList c;
    public ArrayList d;
    public SheetTemplate2 e;
    
    public f5(final int a, final b b, final SheetTemplate2 e, final ArrayList d) {
        this.e = e;
        this.d = d;
        this.a = a;
        this.b = b;
        new c(null).execute((Object[])new Void[0]);
    }
    
    public final h5[] a(final int n) {
        final h5[] array = new h5[n];
        int n2;
        for (int i = 0; i < n; i = n2) {
            n2 = i + 1;
            array[i] = new h5(n2);
        }
        return array;
    }
    
    public ArrayList b(final int n) {
        if (this.e.getAnswerKeys() != null && this.e.getAnswerKeys()[0] != null) {
            final ArrayList c = this.c(this.e, this.d);
            final h5[] a = this.a(this.e.getAnswerOptions().size() - 1);
            for (final ResultItem resultItem : c) {
                if (resultItem.getExamSet() == n) {
                    for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
                        final int n2 = f5$a.a[answerValue.getMarkedState().ordinal()];
                        if (n2 != 1) {
                            if (n2 != 2) {
                                if (n2 != 3) {
                                    if (n2 != 4) {
                                        continue;
                                    }
                                    a[answerValue.getQuestionNumber() - 1].g();
                                }
                                else {
                                    a[answerValue.getQuestionNumber() - 1].h();
                                }
                            }
                            else {
                                a[answerValue.getQuestionNumber() - 1].f();
                            }
                        }
                        else {
                            a[answerValue.getQuestionNumber() - 1].e();
                        }
                    }
                }
            }
            return new ArrayList(Arrays.asList(a));
        }
        return new ArrayList();
    }
    
    public final ArrayList c(final SheetTemplate2 sheetTemplate2, final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((ResultDataJsonModel)iterator.next()).getResultItem(sheetTemplate2));
        }
        return list2;
    }
    
    public interface b
    {
        void a(final ArrayList p0);
    }
    
    public class c extends AsyncTask
    {
        public final f5 a;
        
        public c(final f5 a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            final f5 a = this.a;
            a.c = a.b(a.a);
            return null;
        }
        
        public void b(final Void void1) {
            final f5 a = this.a;
            final b b = a.b;
            if (b != null) {
                final ArrayList c = a.c;
                if (c != null) {
                    b.a(c);
                }
            }
        }
    }
}

import android.animation.AnimatorListenerAdapter;
import android.animation.Animator$AnimatorListener;
import android.view.View;
import android.animation.Animator;
import android.view.ViewGroup;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class j52 extends qy1
{
    public static final int MODE_IN = 1;
    public static final int MODE_OUT = 2;
    private static final String PROPNAME_PARENT = "android:visibility:parent";
    private static final String PROPNAME_SCREEN_LOCATION = "android:visibility:screenLocation";
    static final String PROPNAME_VISIBILITY = "android:visibility:visibility";
    private static final String[] sTransitionProperties;
    private int mMode;
    
    static {
        sTransitionProperties = new String[] { "android:visibility:visibility", "android:visibility:parent" };
    }
    
    public j52() {
        this.mMode = 3;
    }
    
    @Override
    public void captureEndValues(final xy1 xy1) {
        this.captureValues(xy1);
    }
    
    @Override
    public void captureStartValues(final xy1 xy1) {
        this.captureValues(xy1);
    }
    
    public final void captureValues(final xy1 xy1) {
        xy1.a.put("android:visibility:visibility", xy1.b.getVisibility());
        xy1.a.put("android:visibility:parent", xy1.b.getParent());
        final int[] array = new int[2];
        xy1.b.getLocationOnScreen(array);
        xy1.a.put("android:visibility:screenLocation", array);
    }
    
    @Override
    public Animator createAnimator(final ViewGroup viewGroup, final xy1 xy1, final xy1 xy2) {
        final c u = this.u(xy1, xy2);
        if (!u.a || (u.e == null && u.f == null)) {
            return null;
        }
        if (u.b) {
            return this.onAppear(viewGroup, xy1, u.c, xy2, u.d);
        }
        return this.onDisappear(viewGroup, xy1, u.c, xy2, u.d);
    }
    
    public int getMode() {
        return this.mMode;
    }
    
    @Override
    public String[] getTransitionProperties() {
        return j52.sTransitionProperties;
    }
    
    @Override
    public boolean isTransitionRequired(final xy1 xy1, final xy1 xy2) {
        final boolean b = false;
        if (xy1 == null && xy2 == null) {
            return false;
        }
        if (xy1 != null && xy2 != null && xy2.a.containsKey("android:visibility:visibility") != xy1.a.containsKey("android:visibility:visibility")) {
            return false;
        }
        final c u = this.u(xy1, xy2);
        boolean b2 = b;
        if (u.a) {
            if (u.c != 0) {
                b2 = b;
                if (u.d != 0) {
                    return b2;
                }
            }
            b2 = true;
        }
        return b2;
    }
    
    public boolean isVisible(final xy1 xy1) {
        final boolean b = false;
        if (xy1 == null) {
            return false;
        }
        final int intValue = xy1.a.get("android:visibility:visibility");
        final View view = xy1.a.get("android:visibility:parent");
        boolean b2 = b;
        if (intValue == 0) {
            b2 = b;
            if (view != null) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public abstract Animator onAppear(final ViewGroup p0, final View p1, final xy1 p2, final xy1 p3);
    
    public Animator onAppear(final ViewGroup viewGroup, final xy1 xy1, final int n, final xy1 xy2, final int n2) {
        if ((this.mMode & 0x1) == 0x1 && xy2 != null) {
            if (xy1 == null) {
                final View view = (View)xy2.b.getParent();
                if (this.u(this.getMatchedTransitionValues(view, false), this.getTransitionValues(view, false)).a) {
                    return null;
                }
            }
            return this.onAppear(viewGroup, xy2.b, xy1, xy2);
        }
        return null;
    }
    
    public abstract Animator onDisappear(final ViewGroup p0, final View p1, final xy1 p2, final xy1 p3);
    
    public Animator onDisappear(final ViewGroup viewGroup, final xy1 xy1, int n, final xy1 xy2, int n2) {
        if ((this.mMode & 0x2) != 0x2) {
            return null;
        }
        if (xy1 == null) {
            return null;
        }
        final View b = xy1.b;
        View b2;
        if (xy2 != null) {
            b2 = xy2.b;
        }
        else {
            b2 = null;
        }
        final int a = kb1.a;
        View view = (View)b.getTag(a);
        View view2 = null;
        Label_0291: {
            if (view != null) {
                view2 = null;
                n = 1;
            }
            else {
                View view3 = null;
                Label_0137: {
                    Label_0129: {
                        if (b2 != null && b2.getParent() != null) {
                            if (n2 != 4) {
                                if (b != b2) {
                                    break Label_0129;
                                }
                            }
                            view3 = b2;
                            n = 0;
                            b2 = null;
                            break Label_0137;
                        }
                        if (b2 != null) {
                            view3 = null;
                            n = 0;
                            break Label_0137;
                        }
                    }
                    b2 = null;
                    view3 = null;
                    n = 1;
                }
                View a2 = b2;
                Label_0281: {
                    if (n != 0) {
                        if (b.getParent() != null) {
                            a2 = b2;
                            if (!(b.getParent() instanceof View)) {
                                break Label_0281;
                            }
                            final View view4 = (View)b.getParent();
                            if (!this.u(this.getTransitionValues(view4, true), this.getMatchedTransitionValues(view4, true)).a) {
                                a2 = wy1.a(viewGroup, b, view4);
                                break Label_0281;
                            }
                            n = view4.getId();
                            a2 = b2;
                            if (view4.getParent() != null) {
                                break Label_0281;
                            }
                            a2 = b2;
                            if (n == -1) {
                                break Label_0281;
                            }
                            a2 = b2;
                            if (((View)viewGroup).findViewById(n) == null) {
                                break Label_0281;
                            }
                            a2 = b2;
                            if (!super.mCanRemoveViews) {
                                break Label_0281;
                            }
                        }
                        view2 = view3;
                        n = 0;
                        view = b;
                        break Label_0291;
                    }
                }
                n = 0;
                view2 = view3;
                view = a2;
            }
        }
        if (view != null) {
            if (n == 0) {
                final int[] array = xy1.a.get("android:visibility:screenLocation");
                final int n3 = array[0];
                n2 = array[1];
                final int[] array2 = new int[2];
                ((View)viewGroup).getLocationOnScreen(array2);
                view.offsetLeftAndRight(n3 - array2[0] - view.getLeft());
                view.offsetTopAndBottom(n2 - array2[1] - view.getTop());
                w32.a(viewGroup).add(view);
            }
            final Animator onDisappear = this.onDisappear(viewGroup, view, xy1, xy2);
            if (n == 0) {
                if (onDisappear == null) {
                    w32.a(viewGroup).remove(view);
                }
                else {
                    b.setTag(a, (Object)view);
                    this.addListener((g)new ry1(this, viewGroup, view, b) {
                        public final ViewGroup a;
                        public final View b;
                        public final View c;
                        public final j52 d;
                        
                        @Override
                        public void onTransitionEnd(final qy1 qy1) {
                            this.c.setTag(kb1.a, (Object)null);
                            w32.a(this.a).remove(this.b);
                            qy1.removeListener((g)this);
                        }
                        
                        @Override
                        public void onTransitionPause(final qy1 qy1) {
                            w32.a(this.a).remove(this.b);
                        }
                        
                        @Override
                        public void onTransitionResume(final qy1 qy1) {
                            if (this.b.getParent() == null) {
                                w32.a(this.a).add(this.b);
                            }
                            else {
                                this.d.cancel();
                            }
                        }
                    });
                }
            }
            return onDisappear;
        }
        if (view2 != null) {
            n = view2.getVisibility();
            t42.h(view2, 0);
            final Animator onDisappear2 = this.onDisappear(viewGroup, view2, xy1, xy2);
            if (onDisappear2 != null) {
                final b b3 = new b(view2, n2, true);
                onDisappear2.addListener((Animator$AnimatorListener)b3);
                d5.a(onDisappear2, b3);
                this.addListener((g)b3);
            }
            else {
                t42.h(view2, n);
            }
            return onDisappear2;
        }
        return null;
    }
    
    public void setMode(final int mMode) {
        if ((mMode & 0xFFFFFFFC) == 0x0) {
            this.mMode = mMode;
            return;
        }
        throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
    }
    
    public final c u(final xy1 xy1, final xy1 xy2) {
        final c c = new c();
        c.a = false;
        c.b = false;
        if (xy1 != null && xy1.a.containsKey("android:visibility:visibility")) {
            c.c = xy1.a.get("android:visibility:visibility");
            c.e = xy1.a.get("android:visibility:parent");
        }
        else {
            c.c = -1;
            c.e = null;
        }
        if (xy2 != null && xy2.a.containsKey("android:visibility:visibility")) {
            c.d = xy2.a.get("android:visibility:visibility");
            c.f = xy2.a.get("android:visibility:parent");
        }
        else {
            c.d = -1;
            c.f = null;
        }
        while (true) {
            Label_0294: {
                if (xy1 != null && xy2 != null) {
                    final int c2 = c.c;
                    final int d = c.d;
                    if (c2 == d && c.e == c.f) {
                        return c;
                    }
                    if (c2 != d) {
                        if (c2 == 0) {
                            break Label_0294;
                        }
                        if (d != 0) {
                            return c;
                        }
                    }
                    else {
                        if (c.f == null) {
                            break Label_0294;
                        }
                        if (c.e != null) {
                            return c;
                        }
                    }
                }
                else if (xy1 != null || c.d != 0) {
                    if (xy2 == null && c.c == 0) {
                        break Label_0294;
                    }
                    return c;
                }
                c.b = true;
                c.a = true;
                return c;
            }
            c.b = false;
            continue;
        }
    }
    
    public static class b extends AnimatorListenerAdapter implements g
    {
        public final View a;
        public final int b;
        public final ViewGroup c;
        public final boolean d;
        public boolean e;
        public boolean f;
        
        public b(final View a, final int b, final boolean d) {
            this.f = false;
            this.a = a;
            this.b = b;
            this.c = (ViewGroup)a.getParent();
            this.d = d;
            this.b(true);
        }
        
        public final void a() {
            if (!this.f) {
                t42.h(this.a, this.b);
                final ViewGroup c = this.c;
                if (c != null) {
                    ((View)c).invalidate();
                }
            }
            this.b(false);
        }
        
        public final void b(final boolean e) {
            if (this.d && this.e != e) {
                final ViewGroup c = this.c;
                if (c != null) {
                    w32.c(c, this.e = e);
                }
            }
        }
        
        public void onAnimationCancel(final Animator animator) {
            this.f = true;
        }
        
        public void onAnimationEnd(final Animator animator) {
            this.a();
        }
        
        public void onAnimationPause(final Animator animator) {
            if (!this.f) {
                t42.h(this.a, this.b);
            }
        }
        
        public void onAnimationRepeat(final Animator animator) {
        }
        
        public void onAnimationResume(final Animator animator) {
            if (!this.f) {
                t42.h(this.a, 0);
            }
        }
        
        public void onAnimationStart(final Animator animator) {
        }
        
        public void onTransitionCancel(final qy1 qy1) {
        }
        
        public void onTransitionEnd(final qy1 qy1) {
            this.a();
            qy1.removeListener((g)this);
        }
        
        public void onTransitionPause(final qy1 qy1) {
            this.b(false);
        }
        
        public void onTransitionResume(final qy1 qy1) {
            this.b(true);
        }
        
        public void onTransitionStart(final qy1 qy1) {
        }
    }
    
    public static class c
    {
        public boolean a;
        public boolean b;
        public int c;
        public int d;
        public ViewGroup e;
        public ViewGroup f;
    }
}

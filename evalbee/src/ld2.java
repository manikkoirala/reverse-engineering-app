import android.util.Log;
import com.google.android.gms.internal.firebase_auth_api.zzacf;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Continuation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.recaptcha.RecaptchaAction;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ld2
{
    public static Task d(final ud2 ud2, final RecaptchaAction recaptchaAction, final String s, final Continuation continuation) {
        return ud2.b(s, Boolean.FALSE, recaptchaAction).continueWithTask(continuation).continueWithTask((Continuation)new nd2(s, ud2, recaptchaAction, continuation));
    }
    
    public final Task b(final FirebaseAuth firebaseAuth, final String s, final RecaptchaAction recaptchaAction, final String s2) {
        final pd2 pd2 = new pd2(this);
        final ud2 a = firebaseAuth.A();
        if (a != null && a.d(s2)) {
            return d(a, recaptchaAction, s, (Continuation)pd2);
        }
        return this.c(null).continueWithTask((Continuation)new kd2(recaptchaAction, firebaseAuth, s, (Continuation)pd2));
    }
    
    public abstract Task c(final String p0);
}

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class db0 extends v9
{
    public static final Parcelable$Creator<db0> CREATOR;
    public String a;
    
    static {
        CREATOR = (Parcelable$Creator)new ib2();
    }
    
    public db0(final String s) {
        this.a = Preconditions.checkNotEmpty(s);
    }
    
    public static zzagt J(final db0 db0, final String s) {
        Preconditions.checkNotNull(db0);
        return (zzagt)new com.google.android.gms.internal.firebase_auth_api.zzagt((String)null, db0.a, db0.i(), (String)null, (String)null, (String)null, s, (String)null, (String)null);
    }
    
    @Override
    public String E() {
        return "github.com";
    }
    
    @Override
    public final v9 H() {
        return new db0(this.a);
    }
    
    @Override
    public String i() {
        return "github.com";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

import android.view.MenuItem;
import android.view.MenuInflater;
import java.lang.ref.WeakReference;
import androidx.appcompat.view.menu.e;
import android.view.Menu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.content.res.Configuration;
import android.view.ContextThemeWrapper;
import android.util.TypedValue;
import androidx.appcompat.widget.c;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.appcompat.widget.Toolbar;
import android.app.Dialog;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import java.util.ArrayList;
import android.view.View;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarContainer;
import android.app.Activity;
import android.content.Context;
import android.view.animation.Interpolator;
import androidx.appcompat.widget.ActionBarOverlayLayout;

// 
// Decompiled by Procyon v0.6.0
// 

public class e62 extends t1 implements ActionBarOverlayLayout.d
{
    public static final Interpolator D;
    public static final Interpolator E;
    public final k42 A;
    public final k42 B;
    public final m42 C;
    public Context a;
    public Context b;
    public Activity c;
    public ActionBarOverlayLayout d;
    public ActionBarContainer e;
    public dq f;
    public ActionBarContextView g;
    public View h;
    public ArrayList i;
    public int j;
    public boolean k;
    public d l;
    public c2 m;
    public c2.a n;
    public boolean o;
    public ArrayList p;
    public boolean q;
    public int r;
    public boolean s;
    public boolean t;
    public boolean u;
    public boolean v;
    public boolean w;
    public j42 x;
    public boolean y;
    public boolean z;
    
    static {
        D = (Interpolator)new AccelerateInterpolator();
        E = (Interpolator)new DecelerateInterpolator();
    }
    
    public e62(final Activity c, final boolean b) {
        this.i = new ArrayList();
        this.j = -1;
        this.p = new ArrayList();
        this.r = 0;
        this.s = true;
        this.w = true;
        this.A = new l42(this) {
            public final e62 a;
            
            @Override
            public void b(View h) {
                final e62 a = this.a;
                if (a.s) {
                    h = a.h;
                    if (h != null) {
                        h.setTranslationY(0.0f);
                        ((View)this.a.e).setTranslationY(0.0f);
                    }
                }
                this.a.e.setVisibility(8);
                this.a.e.setTransitioning(false);
                final e62 a2 = this.a;
                a2.x = null;
                a2.y();
                final ActionBarOverlayLayout d = this.a.d;
                if (d != null) {
                    o32.n0((View)d);
                }
            }
        };
        this.B = new l42(this) {
            public final e62 a;
            
            @Override
            public void b(final View view) {
                final e62 a = this.a;
                a.x = null;
                ((View)a.e).requestLayout();
            }
        };
        this.C = new m42(this) {
            public final e62 a;
            
            @Override
            public void a(final View view) {
                ((View)((View)this.a.e).getParent()).invalidate();
            }
        };
        this.c = c;
        final View decorView = c.getWindow().getDecorView();
        this.E(decorView);
        if (!b) {
            this.h = decorView.findViewById(16908290);
        }
    }
    
    public e62(final Dialog dialog) {
        this.i = new ArrayList();
        this.j = -1;
        this.p = new ArrayList();
        this.r = 0;
        this.s = true;
        this.w = true;
        this.A = new l42(this) {
            public final e62 a;
            
            @Override
            public void b(View h) {
                final e62 a = this.a;
                if (a.s) {
                    h = a.h;
                    if (h != null) {
                        h.setTranslationY(0.0f);
                        ((View)this.a.e).setTranslationY(0.0f);
                    }
                }
                this.a.e.setVisibility(8);
                this.a.e.setTransitioning(false);
                final e62 a2 = this.a;
                a2.x = null;
                a2.y();
                final ActionBarOverlayLayout d = this.a.d;
                if (d != null) {
                    o32.n0((View)d);
                }
            }
        };
        this.B = new l42(this) {
            public final e62 a;
            
            @Override
            public void b(final View view) {
                final e62 a = this.a;
                a.x = null;
                ((View)a.e).requestLayout();
            }
        };
        this.C = new m42(this) {
            public final e62 a;
            
            @Override
            public void a(final View view) {
                ((View)((View)this.a.e).getParent()).invalidate();
            }
        };
        this.E(dialog.getWindow().getDecorView());
    }
    
    public static boolean x(final boolean b, final boolean b2, final boolean b3) {
        return b3 || (!b && !b2);
    }
    
    public void A(final boolean b) {
        final j42 x = this.x;
        if (x != null) {
            x.a();
        }
        this.e.setVisibility(0);
        if (this.r == 0 && (this.y || b)) {
            ((View)this.e).setTranslationY(0.0f);
            float n2;
            final float n = n2 = (float)(-((View)this.e).getHeight());
            if (b) {
                final int[] array = { 0, 0 };
                ((View)this.e).getLocationInWindow(array);
                n2 = n - array[1];
            }
            ((View)this.e).setTranslationY(n2);
            final j42 x2 = new j42();
            final i42 m = o32.e((View)this.e).m(0.0f);
            m.k(this.C);
            x2.c(m);
            if (this.s) {
                final View h = this.h;
                if (h != null) {
                    h.setTranslationY(n2);
                    x2.c(o32.e(this.h).m(0.0f));
                }
            }
            x2.f(e62.E);
            x2.e(250L);
            x2.g(this.B);
            (this.x = x2).h();
        }
        else {
            ((View)this.e).setAlpha(1.0f);
            ((View)this.e).setTranslationY(0.0f);
            if (this.s) {
                final View h2 = this.h;
                if (h2 != null) {
                    h2.setTranslationY(0.0f);
                }
            }
            this.B.b(null);
        }
        final ActionBarOverlayLayout d = this.d;
        if (d != null) {
            o32.n0((View)d);
        }
    }
    
    public final dq B(final View view) {
        if (view instanceof dq) {
            return (dq)view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar)view).getWrapper();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        String simpleName;
        if (view != null) {
            simpleName = view.getClass().getSimpleName();
        }
        else {
            simpleName = "null";
        }
        sb.append(simpleName);
        throw new IllegalStateException(sb.toString());
    }
    
    public int C() {
        return this.f.j();
    }
    
    public final void D() {
        if (this.v) {
            this.v = false;
            final ActionBarOverlayLayout d = this.d;
            if (d != null) {
                d.setShowingForActionMode(false);
            }
            this.N(false);
        }
    }
    
    public final void E(final View view) {
        final ActionBarOverlayLayout d = (ActionBarOverlayLayout)view.findViewById(db1.p);
        this.d = d;
        if (d != null) {
            d.setActionBarVisibilityCallback((ActionBarOverlayLayout.d)this);
        }
        this.f = this.B(view.findViewById(db1.a));
        this.g = (ActionBarContextView)view.findViewById(db1.f);
        final ActionBarContainer e = (ActionBarContainer)view.findViewById(db1.c);
        this.e = e;
        final dq f = this.f;
        if (f != null && this.g != null && e != null) {
            this.a = f.getContext();
            final boolean b = (this.f.o() & 0x4) != 0x0;
            if (b) {
                this.k = true;
            }
            final v1 b2 = v1.b(this.a);
            this.K(b2.a() || b);
            this.I(b2.e());
            final TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes((AttributeSet)null, bc1.a, sa1.c, 0);
            if (obtainStyledAttributes.getBoolean(bc1.k, false)) {
                this.J(true);
            }
            final int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(bc1.i, 0);
            if (dimensionPixelSize != 0) {
                this.H((float)dimensionPixelSize);
            }
            obtainStyledAttributes.recycle();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(" can only be used with a compatible window decor layout");
        throw new IllegalStateException(sb.toString());
    }
    
    public void F(final boolean b) {
        int n;
        if (b) {
            n = 4;
        }
        else {
            n = 0;
        }
        this.G(n, 4);
    }
    
    public void G(final int n, final int n2) {
        final int o = this.f.o();
        if ((n2 & 0x4) != 0x0) {
            this.k = true;
        }
        this.f.i((n & n2) | (~n2 & o));
    }
    
    public void H(final float n) {
        o32.y0((View)this.e, n);
    }
    
    public final void I(final boolean q) {
        if (!(this.q = q)) {
            this.f.v(null);
            this.e.setTabContainer(null);
        }
        else {
            this.e.setTabContainer(null);
            this.f.v(null);
        }
        final int c = this.C();
        final boolean b = true;
        final boolean b2 = c == 2;
        this.f.l(!this.q && b2);
        this.d.setHasNonEmbeddedTabs(!this.q && b2 && b);
    }
    
    public void J(final boolean b) {
        if (b && !this.d.q()) {
            throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
        }
        this.z = b;
        this.d.setHideOnContentScrollEnabled(b);
    }
    
    public void K(final boolean b) {
        this.f.u(b);
    }
    
    public final boolean L() {
        return o32.U((View)this.e);
    }
    
    public final void M() {
        if (!this.v) {
            this.v = true;
            final ActionBarOverlayLayout d = this.d;
            if (d != null) {
                d.setShowingForActionMode(true);
            }
            this.N(false);
        }
    }
    
    public final void N(final boolean b) {
        if (x(this.t, this.u, this.v)) {
            if (!this.w) {
                this.w = true;
                this.A(b);
            }
        }
        else if (this.w) {
            this.w = false;
            this.z(b);
        }
    }
    
    @Override
    public void a() {
        if (this.u) {
            this.u = false;
            this.N(true);
        }
    }
    
    @Override
    public void b() {
    }
    
    @Override
    public void c() {
        if (!this.u) {
            this.N(this.u = true);
        }
    }
    
    @Override
    public void d() {
        final j42 x = this.x;
        if (x != null) {
            x.a();
            this.x = null;
        }
    }
    
    @Override
    public void e(final boolean s) {
        this.s = s;
    }
    
    @Override
    public boolean g() {
        final dq f = this.f;
        if (f != null && f.h()) {
            this.f.collapseActionView();
            return true;
        }
        return false;
    }
    
    @Override
    public void h(final boolean o) {
        if (o == this.o) {
            return;
        }
        this.o = o;
        if (this.p.size() <= 0) {
            return;
        }
        zu0.a(this.p.get(0));
        throw null;
    }
    
    @Override
    public int i() {
        return this.f.o();
    }
    
    @Override
    public Context j() {
        if (this.b == null) {
            final TypedValue typedValue = new TypedValue();
            this.a.getTheme().resolveAttribute(sa1.e, typedValue, true);
            final int resourceId = typedValue.resourceId;
            if (resourceId != 0) {
                this.b = (Context)new ContextThemeWrapper(this.a, resourceId);
            }
            else {
                this.b = this.a;
            }
        }
        return this.b;
    }
    
    @Override
    public void l(final Configuration configuration) {
        this.I(v1.b(this.a).e());
    }
    
    @Override
    public boolean n(final int n, final KeyEvent keyEvent) {
        final d l = this.l;
        if (l == null) {
            return false;
        }
        final Menu c = l.c();
        if (c != null) {
            int deviceId;
            if (keyEvent != null) {
                deviceId = keyEvent.getDeviceId();
            }
            else {
                deviceId = -1;
            }
            final int keyboardType = KeyCharacterMap.load(deviceId).getKeyboardType();
            boolean qwertyMode = true;
            if (keyboardType == 1) {
                qwertyMode = false;
            }
            c.setQwertyMode(qwertyMode);
            return c.performShortcut(n, keyEvent, 0);
        }
        return false;
    }
    
    @Override
    public void onWindowVisibilityChanged(final int r) {
        this.r = r;
    }
    
    @Override
    public void q(final boolean b) {
        if (!this.k) {
            this.F(b);
        }
    }
    
    @Override
    public void r(final boolean y) {
        if (!(this.y = y)) {
            final j42 x = this.x;
            if (x != null) {
                x.a();
            }
        }
    }
    
    @Override
    public void s(final CharSequence charSequence) {
        this.f.q(charSequence);
    }
    
    @Override
    public void t(final CharSequence title) {
        this.f.setTitle(title);
    }
    
    @Override
    public void u(final CharSequence windowTitle) {
        this.f.setWindowTitle(windowTitle);
    }
    
    @Override
    public c2 v(final c2.a a) {
        final d l = this.l;
        if (l != null) {
            l.a();
        }
        this.d.setHideOnContentScrollEnabled(false);
        this.g.k();
        final d i = new d(((View)this.g).getContext(), a);
        if (i.r()) {
            (this.l = i).i();
            this.g.h(i);
            this.w(true);
            return i;
        }
        return null;
    }
    
    public void w(final boolean b) {
        if (b) {
            this.M();
        }
        else {
            this.D();
        }
        if (this.L()) {
            i42 i42;
            i42 i43;
            if (b) {
                i42 = this.f.s(4, 100L);
                i43 = this.g.f(0, 200L);
            }
            else {
                i43 = this.f.s(0, 200L);
                i42 = this.g.f(8, 100L);
            }
            final j42 j42 = new j42();
            j42.d(i42, i43);
            j42.h();
        }
        else if (b) {
            this.f.n(4);
            this.g.setVisibility(0);
        }
        else {
            this.f.n(0);
            this.g.setVisibility(8);
        }
    }
    
    public void y() {
        final c2.a n = this.n;
        if (n != null) {
            n.b(this.m);
            this.m = null;
            this.n = null;
        }
    }
    
    public void z(final boolean b) {
        final j42 x = this.x;
        if (x != null) {
            x.a();
        }
        if (this.r == 0 && (this.y || b)) {
            ((View)this.e).setAlpha(1.0f);
            this.e.setTransitioning(true);
            final j42 x2 = new j42();
            float n2;
            final float n = n2 = (float)(-((View)this.e).getHeight());
            if (b) {
                final int[] array = { 0, 0 };
                ((View)this.e).getLocationInWindow(array);
                n2 = n - array[1];
            }
            final i42 m = o32.e((View)this.e).m(n2);
            m.k(this.C);
            x2.c(m);
            if (this.s) {
                final View h = this.h;
                if (h != null) {
                    x2.c(o32.e(h).m(n2));
                }
            }
            x2.f(e62.D);
            x2.e(250L);
            x2.g(this.A);
            (this.x = x2).h();
        }
        else {
            this.A.b(null);
        }
    }
    
    public class d extends c2 implements e.a
    {
        public final Context c;
        public final e d;
        public c2.a e;
        public WeakReference f;
        public final e62 g;
        
        public d(final e62 g, final Context c, final c2.a e) {
            this.g = g;
            this.c = c;
            this.e = e;
            (this.d = new e(c).setDefaultShowAsAction(1)).setCallback((e.a)this);
        }
        
        @Override
        public void a() {
            final e62 g = this.g;
            if (g.l != this) {
                return;
            }
            if (!e62.x(g.t, g.u, false)) {
                final e62 g2 = this.g;
                g2.m = this;
                g2.n = this.e;
            }
            else {
                this.e.b(this);
            }
            this.e = null;
            this.g.w(false);
            this.g.g.g();
            final e62 g3 = this.g;
            g3.d.setHideOnContentScrollEnabled(g3.z);
            this.g.l = null;
        }
        
        @Override
        public View b() {
            final WeakReference f = this.f;
            View view;
            if (f != null) {
                view = (View)f.get();
            }
            else {
                view = null;
            }
            return view;
        }
        
        @Override
        public Menu c() {
            return (Menu)this.d;
        }
        
        @Override
        public MenuInflater d() {
            return new ls1(this.c);
        }
        
        @Override
        public CharSequence e() {
            return this.g.g.getSubtitle();
        }
        
        @Override
        public CharSequence g() {
            return this.g.g.getTitle();
        }
        
        @Override
        public void i() {
            if (this.g.l != this) {
                return;
            }
            this.d.stopDispatchingItemsChanged();
            try {
                this.e.d(this, (Menu)this.d);
            }
            finally {
                this.d.startDispatchingItemsChanged();
            }
        }
        
        @Override
        public boolean j() {
            return this.g.g.j();
        }
        
        @Override
        public void k(final View view) {
            this.g.g.setCustomView(view);
            this.f = new WeakReference((T)view);
        }
        
        @Override
        public void l(final int n) {
            this.m(this.g.a.getResources().getString(n));
        }
        
        @Override
        public void m(final CharSequence subtitle) {
            this.g.g.setSubtitle(subtitle);
        }
        
        @Override
        public void o(final int n) {
            this.p(this.g.a.getResources().getString(n));
        }
        
        @Override
        public boolean onMenuItemSelected(final e e, final MenuItem menuItem) {
            final c2.a e2 = this.e;
            return e2 != null && e2.a(this, menuItem);
        }
        
        @Override
        public void onMenuModeChange(final e e) {
            if (this.e == null) {
                return;
            }
            this.i();
            this.g.g.l();
        }
        
        @Override
        public void p(final CharSequence title) {
            this.g.g.setTitle(title);
        }
        
        @Override
        public void q(final boolean titleOptional) {
            super.q(titleOptional);
            this.g.g.setTitleOptional(titleOptional);
        }
        
        public boolean r() {
            this.d.stopDispatchingItemsChanged();
            try {
                return this.e.c(this, (Menu)this.d);
            }
            finally {
                this.d.startDispatchingItemsChanged();
            }
        }
    }
}

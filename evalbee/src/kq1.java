import android.net.Uri$Builder;
import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public class kq1
{
    public final Uri a;
    public final Uri b;
    public final Uri c;
    
    public kq1(final Uri c, final ww ww) {
        this.c = c;
        final Uri k = xy0.k;
        this.a = k;
        final Uri$Builder appendEncodedPath = k.buildUpon().appendPath("b").appendEncodedPath(c.getAuthority());
        final String a = io1.a(c.getPath());
        Uri$Builder appendPath = appendEncodedPath;
        if (a.length() > 0) {
            appendPath = appendEncodedPath;
            if (!"/".equals(a)) {
                appendPath = appendEncodedPath.appendPath("o").appendPath(a);
            }
        }
        this.b = appendPath.build();
    }
    
    public Uri a() {
        return this.c;
    }
    
    public Uri b() {
        return this.a;
    }
    
    public Uri c() {
        return this.b;
    }
}

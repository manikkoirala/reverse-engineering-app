import com.google.protobuf.ProtoSyntax;
import com.google.protobuf.a0;

// 
// Decompiled by Procyon v0.6.0
// 

public final class kc1 implements tv0
{
    public final a0 a;
    public final String b;
    public final Object[] c;
    public final int d;
    
    public kc1(final a0 a, final String b, final Object[] c) {
        this.a = a;
        this.b = b;
        this.c = c;
        int char1 = b.charAt(0);
        if (char1 >= 55296) {
            int n = char1 & 0x1FFF;
            int n2 = 13;
            int index = 1;
            char char2;
            while (true) {
                char2 = b.charAt(index);
                if (char2 < '\ud800') {
                    break;
                }
                n |= (char2 & '\u1fff') << n2;
                n2 += 13;
                ++index;
            }
            char1 = (n | char2 << n2);
        }
        this.d = char1;
    }
    
    @Override
    public boolean a() {
        return (this.d & 0x2) == 0x2;
    }
    
    @Override
    public a0 b() {
        return this.a;
    }
    
    @Override
    public ProtoSyntax c() {
        ProtoSyntax protoSyntax;
        if ((this.d & 0x1) == 0x1) {
            protoSyntax = ProtoSyntax.PROTO2;
        }
        else {
            protoSyntax = ProtoSyntax.PROTO3;
        }
        return protoSyntax;
    }
    
    public Object[] d() {
        return this.c;
    }
    
    public String e() {
        return this.b;
    }
}

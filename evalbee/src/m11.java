import java.util.concurrent.atomic.AtomicInteger;

// 
// Decompiled by Procyon v0.6.0
// 

public final class m11
{
    public final AtomicInteger a;
    public final AtomicInteger b;
    
    public m11() {
        this.a = new AtomicInteger();
        this.b = new AtomicInteger();
    }
    
    public void a() {
        this.b.getAndIncrement();
    }
    
    public void b() {
        this.a.getAndIncrement();
    }
    
    public void c() {
        this.b.set(0);
    }
}

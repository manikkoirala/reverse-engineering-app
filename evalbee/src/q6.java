import android.app.Dialog;
import android.view.Window$Callback;
import android.view.KeyEvent;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.util.TypedValue;
import android.content.DialogInterface$OnCancelListener;
import android.os.Bundle;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class q6 extends jj implements z5
{
    private f6 mDelegate;
    private final mi0.a mKeyDispatcher;
    
    public q6(final Context context, final int n) {
        super(context, getThemeResId(context, n));
        this.mKeyDispatcher = new p6(this);
        final f6 delegate = this.getDelegate();
        delegate.K(getThemeResId(context, n));
        delegate.v(null);
    }
    
    public q6(final Context context, final boolean cancelable, final DialogInterface$OnCancelListener onCancelListener) {
        super(context);
        this.mKeyDispatcher = new p6(this);
        this.setCancelable(cancelable);
        this.setOnCancelListener(onCancelListener);
    }
    
    public static int getThemeResId(final Context context, final int n) {
        int resourceId = n;
        if (n == 0) {
            final TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(sa1.x, typedValue, true);
            resourceId = typedValue.resourceId;
        }
        return resourceId;
    }
    
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.getDelegate().c(view, viewGroup$LayoutParams);
    }
    
    public void dismiss() {
        super.dismiss();
        this.getDelegate().w();
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return mi0.e(this.mKeyDispatcher, this.getWindow().getDecorView(), (Window$Callback)this, keyEvent);
    }
    
    public <T extends View> T findViewById(final int n) {
        return (T)this.getDelegate().h(n);
    }
    
    public f6 getDelegate() {
        if (this.mDelegate == null) {
            this.mDelegate = f6.g(this, this);
        }
        return this.mDelegate;
    }
    
    public t1 getSupportActionBar() {
        return this.getDelegate().p();
    }
    
    public void invalidateOptionsMenu() {
        this.getDelegate().r();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.getDelegate().q();
        super.onCreate(bundle);
        this.getDelegate().v(bundle);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.getDelegate().B();
    }
    
    @Override
    public void onSupportActionModeFinished(final c2 c2) {
    }
    
    @Override
    public void onSupportActionModeStarted(final c2 c2) {
    }
    
    @Override
    public c2 onWindowStartingSupportActionMode(final c2.a a) {
        return null;
    }
    
    public void setContentView(final int n) {
        this.getDelegate().F(n);
    }
    
    public void setContentView(final View view) {
        this.getDelegate().G(view);
    }
    
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.getDelegate().H(view, viewGroup$LayoutParams);
    }
    
    public void setTitle(final int title) {
        super.setTitle(title);
        this.getDelegate().L(this.getContext().getString(title));
    }
    
    public void setTitle(final CharSequence title) {
        super.setTitle(title);
        this.getDelegate().L(title);
    }
    
    boolean superDispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
    
    public boolean supportRequestWindowFeature(final int n) {
        return this.getDelegate().E(n);
    }
}

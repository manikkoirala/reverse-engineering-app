import java.math.RoundingMode;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ju0
{
    public static void a(final boolean b, final double d, final RoundingMode obj) {
        if (b) {
            return;
        }
        final String value = String.valueOf(obj);
        final StringBuilder sb = new StringBuilder(value.length() + 83);
        sb.append("rounded value is out of range for input ");
        sb.append(d);
        sb.append(" and rounding mode ");
        sb.append(value);
        throw new ArithmeticException(sb.toString());
    }
    
    public static void b(final boolean b, final String s, final int i, final int j) {
        if (b) {
            return;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 36);
        sb.append("overflow: ");
        sb.append(s);
        sb.append("(");
        sb.append(i);
        sb.append(", ");
        sb.append(j);
        sb.append(")");
        throw new ArithmeticException(sb.toString());
    }
    
    public static void c(final boolean b, final String s, final long lng, final long lng2) {
        if (b) {
            return;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 54);
        sb.append("overflow: ");
        sb.append(s);
        sb.append("(");
        sb.append(lng);
        sb.append(", ");
        sb.append(lng2);
        sb.append(")");
        throw new ArithmeticException(sb.toString());
    }
    
    public static int d(final String s, final int i) {
        if (i > 0) {
            return i;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 26);
        sb.append(s);
        sb.append(" (");
        sb.append(i);
        sb.append(") must be > 0");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void e(final boolean b) {
        if (b) {
            return;
        }
        throw new ArithmeticException("mode was UNNECESSARY, but rounding was necessary");
    }
}

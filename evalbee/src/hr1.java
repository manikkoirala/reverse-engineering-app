import java.io.UnsupportedEncodingException;
import com.android.volley.d;
import com.android.volley.Request;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class hr1 extends Request
{
    public final Object t;
    public d.b v;
    
    public hr1(final int n, final String s, final d.b v, final d.a a) {
        super(n, s, a);
        this.t = new Object();
        this.v = v;
    }
    
    @Override
    public d G(final yy0 yy0) {
        String s;
        try {
            s = new String(yy0.b, id0.f(yy0.c));
        }
        catch (final UnsupportedEncodingException ex) {
            s = new String(yy0.b);
        }
        return com.android.volley.d.c(s, id0.e(yy0));
    }
    
    public void Q(final String s) {
        synchronized (this.t) {
            final d.b v = this.v;
            monitorexit(this.t);
            if (v != null) {
                v.a(s);
            }
        }
    }
}

import java.util.ListIterator;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class z02 extends w02 implements ListIterator
{
    @Override
    public final void add(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final void set(final Object o) {
        throw new UnsupportedOperationException();
    }
}

import java.util.List;
import java.util.TreeMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Collections;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.core.Query;
import java.util.HashSet;
import com.google.firebase.database.collection.b;
import java.util.Iterator;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import com.google.firebase.firestore.local.IndexManager;

// 
// Decompiled by Procyon v0.6.0
// 

public class xk0
{
    public final id1 a;
    public final zx0 b;
    public final eu c;
    public final IndexManager d;
    
    public xk0(final id1 a, final zx0 b, final eu c, final IndexManager d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public final Map a(final Map map, final Map map2, final Set set) {
        final HashMap hashMap = new HashMap();
        final HashMap hashMap2 = new HashMap();
        for (final MutableDocument mutableDocument : map.values()) {
            final k21 k21 = map2.get(mutableDocument.getKey());
            if (set.contains(mutableDocument.getKey()) && (k21 == null || k21.d() instanceof e31)) {
                hashMap.put(mutableDocument.getKey(), mutableDocument);
            }
            else if (k21 != null) {
                hashMap2.put(mutableDocument.getKey(), k21.d().e());
                k21.d().a(mutableDocument, k21.d().e(), pw1.f());
            }
            else {
                hashMap2.put(mutableDocument.getKey(), q00.b);
            }
        }
        hashMap2.putAll(this.n(hashMap));
        final HashMap hashMap3 = new HashMap();
        for (final Map.Entry<du, V> entry : map.entrySet()) {
            hashMap3.put(entry.getKey(), new m21((zt)entry.getValue(), (q00)hashMap2.get(entry.getKey())));
        }
        return hashMap3;
    }
    
    public final MutableDocument b(final du du, final k21 k21) {
        MutableDocument mutableDocument;
        if (k21 != null && !(k21.d() instanceof e31)) {
            mutableDocument = MutableDocument.q(du);
        }
        else {
            mutableDocument = this.a.d(du);
        }
        return mutableDocument;
    }
    
    public zt c(final du du) {
        final k21 a = this.c.a(du);
        final MutableDocument b = this.b(du, a);
        if (a != null) {
            a.d().a(b, q00.b, pw1.f());
        }
        return b;
    }
    
    public b d(final Iterable iterable) {
        return this.j(this.a.getAll(iterable), new HashSet());
    }
    
    public final b e(final Query query, final FieldIndex.a a, final ga1 ga1) {
        g9.d(query.l().j(), "Currently we only support collection group queries at the root.", new Object[0]);
        final String d = query.d();
        b a2 = au.a();
        final Iterator iterator = this.d.g(d).iterator();
        while (iterator.hasNext()) {
            final Iterator iterator2 = this.f(query.a((ke1)((ke1)iterator.next()).c(d)), a, ga1).iterator();
            b l = a2;
            while (true) {
                a2 = l;
                if (!iterator2.hasNext()) {
                    break;
                }
                final Map.Entry entry = iterator2.next();
                l = l.l(entry.getKey(), entry.getValue());
            }
        }
        return a2;
    }
    
    public final b f(final Query query, final FieldIndex.a a, final ga1 ga1) {
        final Map c = this.c.c(query.l(), a.h());
        final Map a2 = this.a.a(query, a, c.keySet(), ga1);
        for (final Map.Entry<Object, V> entry : c.entrySet()) {
            if (!a2.containsKey(entry.getKey())) {
                a2.put(entry.getKey(), MutableDocument.q(entry.getKey()));
            }
        }
        b b = au.a();
        for (final Map.Entry<Object, V> entry2 : a2.entrySet()) {
            final k21 k21 = c.get(entry2.getKey());
            if (k21 != null) {
                k21.d().a((MutableDocument)entry2.getValue(), q00.b, pw1.f());
            }
            if (query.r((zt)entry2.getValue())) {
                b = b.l(entry2.getKey(), entry2.getValue());
            }
        }
        return b;
    }
    
    public final b g(final ke1 ke1) {
        final b a = au.a();
        final zt c = this.c(du.g(ke1));
        b l = a;
        if (c.d()) {
            l = a.l(c.getKey(), c);
        }
        return l;
    }
    
    public b h(final Query query, final FieldIndex.a a) {
        return this.i(query, a, null);
    }
    
    public b i(final Query query, final FieldIndex.a a, final ga1 ga1) {
        final ke1 l = query.l();
        if (query.p()) {
            return this.g(l);
        }
        if (query.o()) {
            return this.e(query, a, ga1);
        }
        return this.f(query, a, ga1);
    }
    
    public b j(final Map map, final Set set) {
        final HashMap hashMap = new HashMap();
        this.m(hashMap, map.keySet());
        final b a = au.a();
        final Iterator iterator = this.a(map, hashMap, set).entrySet().iterator();
        b l = a;
        while (iterator.hasNext()) {
            final Map.Entry<du, V> entry = (Map.Entry<du, V>)iterator.next();
            l = l.l(entry.getKey(), ((m21)entry.getValue()).a());
        }
        return l;
    }
    
    public wk0 k(final String s, final FieldIndex.a a, int max) {
        final Map b = this.a.b(s, a, max);
        Map<Object, Object> map;
        if (max - b.size() > 0) {
            map = this.c.d(s, a.h(), max - b.size());
        }
        else {
            map = (Map<Object, Object>)Collections.emptyMap();
        }
        final Iterator<Object> iterator = (Iterator<Object>)map.values().iterator();
        max = -1;
        while (iterator.hasNext()) {
            final k21 k21 = iterator.next();
            if (!b.containsKey(k21.b())) {
                b.put(k21.b(), this.b(k21.b(), k21));
            }
            max = Math.max(max, k21.c());
        }
        this.m(map, b.keySet());
        return wk0.a(max, this.a(b, map, Collections.emptySet()));
    }
    
    public Map l(final Map map) {
        final HashMap hashMap = new HashMap();
        this.m(hashMap, map.keySet());
        return this.a(map, hashMap, new HashSet());
    }
    
    public final void m(final Map map, final Set set) {
        final TreeSet set2 = new TreeSet();
        for (final du du : set) {
            if (!map.containsKey(du)) {
                set2.add(du);
            }
        }
        map.putAll(this.c.f(set2));
    }
    
    public final Map n(final Map map) {
        final List b = this.b.b(map.keySet());
        final HashMap hashMap = new HashMap();
        final TreeMap treeMap = new TreeMap();
        for (final xx0 xx0 : b) {
            for (final du du : xx0.f()) {
                final MutableDocument mutableDocument = map.get(du);
                if (mutableDocument == null) {
                    continue;
                }
                q00 b2;
                if (hashMap.containsKey(du)) {
                    b2 = (q00)hashMap.get(du);
                }
                else {
                    b2 = q00.b;
                }
                hashMap.put(du, xx0.b(mutableDocument, b2));
                final int e = xx0.e();
                if (!treeMap.containsKey(e)) {
                    treeMap.put(e, new HashSet());
                }
                ((Set<du>)treeMap.get(e)).add(du);
            }
        }
        final HashSet set = new HashSet();
        for (final Map.Entry<K, Set> entry : treeMap.descendingMap().entrySet()) {
            final HashMap hashMap2 = new HashMap();
            for (final du du2 : entry.getValue()) {
                if (!set.contains(du2)) {
                    final wx0 c = wx0.c(map.get(du2), (q00)hashMap.get(du2));
                    if (c != null) {
                        hashMap2.put(du2, c);
                    }
                    set.add(du2);
                }
            }
            this.c.e((int)entry.getKey(), hashMap2);
        }
        return hashMap;
    }
    
    public void o(final Set set) {
        this.n(this.a.getAll(set));
    }
}

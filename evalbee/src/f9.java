// 
// Decompiled by Procyon v0.6.0
// 

public class f9 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        final double n2 = array[0];
        return Math.log(n2 + Math.sqrt(n2 * n2 + 1.0));
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "asinh(x)";
    }
}

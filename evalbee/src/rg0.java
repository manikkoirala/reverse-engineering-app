import java.util.NoSuchElementException;
import java.util.List;
import com.google.common.collect.Lists;
import java.util.Iterator;
import com.google.common.collect.Iterators;
import java.util.Collection;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rg0
{
    public static boolean a(final Collection collection, final Iterable iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll((Collection)iterable);
        }
        return Iterators.a(collection, ((Iterable)i71.r(iterable)).iterator());
    }
    
    public static Collection b(final Iterable iterable) {
        Collection k;
        if (iterable instanceof Collection) {
            k = (Collection)iterable;
        }
        else {
            k = Lists.k(iterable.iterator());
        }
        return k;
    }
    
    public static Iterable c(final Iterable iterable, final Iterable iterable2) {
        return c70.a(iterable, iterable2);
    }
    
    public static Iterable d(final Iterable iterable, final m71 m71) {
        i71.r(iterable);
        i71.r(m71);
        return new c70(iterable, m71) {
            public final Iterable b;
            public final m71 c;
            
            @Override
            public Iterator iterator() {
                return Iterators.k(this.b.iterator(), this.c);
            }
        };
    }
    
    public static Object e(final Iterable iterable, final Object o) {
        return Iterators.m(iterable.iterator(), o);
    }
    
    public static Object f(final Iterable iterable) {
        if (!(iterable instanceof List)) {
            return Iterators.l(iterable.iterator());
        }
        final List list = (List)iterable;
        if (!list.isEmpty()) {
            return g(list);
        }
        throw new NoSuchElementException();
    }
    
    public static Object g(final List list) {
        return list.get(list.size() - 1);
    }
    
    public static Object h(final Iterable iterable) {
        return Iterators.n(iterable.iterator());
    }
    
    public static boolean i(final Iterable iterable) {
        if (iterable instanceof Collection) {
            return ((Collection)iterable).isEmpty();
        }
        return iterable.iterator().hasNext() ^ true;
    }
    
    public static Iterable j(final Iterable iterable, final int n) {
        i71.r(iterable);
        i71.e(n >= 0, "number to skip cannot be negative");
        return new c70(iterable, n) {
            public final Iterable b;
            public final int c;
            
            @Override
            public Iterator iterator() {
                final Iterable b = this.b;
                if (b instanceof List) {
                    final List list = (List)b;
                    return list.subList(Math.min(list.size(), this.c), list.size()).iterator();
                }
                final Iterator iterator = b.iterator();
                Iterators.b(iterator, this.c);
                return new Iterator(this, iterator) {
                    public boolean a = true;
                    public final Iterator b;
                    
                    @Override
                    public boolean hasNext() {
                        return this.b.hasNext();
                    }
                    
                    @Override
                    public Object next() {
                        final Object next = this.b.next();
                        this.a = false;
                        return next;
                    }
                    
                    @Override
                    public void remove() {
                        hh.e(this.a ^ true);
                        this.b.remove();
                    }
                };
            }
        };
    }
    
    public static Object[] k(final Iterable iterable) {
        return b(iterable).toArray();
    }
    
    public static Object[] l(final Iterable iterable, final Object[] array) {
        return b(iterable).toArray(array);
    }
    
    public static String m(final Iterable iterable) {
        return Iterators.v(iterable.iterator());
    }
    
    public static Iterable n(final Iterable iterable, final m90 m90) {
        i71.r(iterable);
        i71.r(m90);
        return new c70(iterable, m90) {
            public final Iterable b;
            public final m90 c;
            
            @Override
            public Iterator iterator() {
                return Iterators.w(this.b.iterator(), this.c);
            }
        };
    }
}

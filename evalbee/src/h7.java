import android.graphics.Canvas;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.SeekBar;

// 
// Decompiled by Procyon v0.6.0
// 

public class h7 extends SeekBar
{
    public final i7 a;
    
    public h7(final Context context, final AttributeSet set) {
        this(context, set, sa1.J);
    }
    
    public h7(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        pv1.a((View)this, ((View)this).getContext());
        (this.a = new i7(this)).c(set, n);
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        this.a.h();
    }
    
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.a.i();
    }
    
    public void onDraw(final Canvas canvas) {
        synchronized (this) {
            super.onDraw(canvas);
            this.a.g(canvas);
        }
    }
}

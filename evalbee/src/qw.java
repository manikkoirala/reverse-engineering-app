import android.widget.TextView;
import android.view.View;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import android.text.Editable;
import android.text.Spannable;
import android.text.Selection;
import androidx.emoji2.text.c;
import android.widget.EditText;
import android.text.TextWatcher;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qw implements TextWatcher
{
    public final EditText a;
    public final boolean b;
    public c.e c;
    public int d;
    public int e;
    public boolean f;
    
    public qw(final EditText a, final boolean b) {
        this.d = Integer.MAX_VALUE;
        this.e = 0;
        this.a = a;
        this.b = b;
        this.f = true;
    }
    
    public static void c(final EditText editText, int selectionEnd) {
        if (selectionEnd == 1 && editText != null && ((View)editText).isAttachedToWindow()) {
            final Editable editableText = ((TextView)editText).getEditableText();
            final int selectionStart = Selection.getSelectionStart((CharSequence)editableText);
            selectionEnd = Selection.getSelectionEnd((CharSequence)editableText);
            c.b().o((CharSequence)editableText);
            lw.b((Spannable)editableText, selectionStart, selectionEnd);
        }
    }
    
    public final c.e a() {
        if (this.c == null) {
            this.c = new a(this.a);
        }
        return this.c;
    }
    
    public void afterTextChanged(final Editable editable) {
    }
    
    public boolean b() {
        return this.f;
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    public void d(final boolean f) {
        if (this.f != f) {
            if (this.c != null) {
                androidx.emoji2.text.c.b().t(this.c);
            }
            this.f = f;
            if (f) {
                c(this.a, androidx.emoji2.text.c.b().d());
            }
        }
    }
    
    public final boolean e() {
        return !this.f || (!this.b && !androidx.emoji2.text.c.h());
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, int d, final int n2) {
        if (!((View)this.a).isInEditMode()) {
            if (!this.e()) {
                if (d <= n2 && charSequence instanceof Spannable) {
                    d = androidx.emoji2.text.c.b().d();
                    if (d != 0) {
                        if (d == 1) {
                            androidx.emoji2.text.c.b().r(charSequence, n, n + n2, this.d, this.e);
                            return;
                        }
                        if (d != 3) {
                            return;
                        }
                    }
                    androidx.emoji2.text.c.b().s(this.a());
                }
            }
        }
    }
    
    public static class a extends e
    {
        public final Reference a;
        
        public a(final EditText referent) {
            this.a = new WeakReference(referent);
        }
        
        @Override
        public void b() {
            super.b();
            qw.c(this.a.get(), 1);
        }
    }
}

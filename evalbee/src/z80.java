// 
// Decompiled by Procyon v0.6.0
// 

public class z80 extends yu1
{
    public eb d;
    public double[] e;
    
    public z80(final String s, final boolean b) {
        super(s, b);
        this.d = new eb(1);
        this.e = new double[1];
    }
    
    @Override
    public double b(final b32 b32, final y80 y80) {
        final int h = this.d.h();
        for (int i = 0; i < h; ++i) {
            this.e[i] = this.n(i).b(b32, y80);
        }
        double a = y80.a(super.b, h).a(this.e, h);
        if (super.c) {
            a = -a;
        }
        return a;
    }
    
    public void m(final oz oz) {
        this.o(oz, this.d.h());
    }
    
    public oz n(final int n) {
        return (oz)this.d.e(n);
    }
    
    public void o(final oz oz, int f) {
        this.a(oz);
        final int f2 = this.d.f();
        this.d.g(oz, f);
        f = this.d.f();
        if (f2 != f) {
            this.e = new double[f];
        }
        oz.a = this;
    }
    
    public int p() {
        return this.d.h();
    }
}

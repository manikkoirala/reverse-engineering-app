import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import androidx.appcompat.app.a;
import android.content.Context;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

// 
// Decompiled by Procyon v0.6.0
// 

public class fj1 extends MaterialAlertDialogBuilder
{
    public Context a;
    public int b;
    public y01 c;
    public a d;
    
    public fj1(final Context a, final y01 c, final int b) {
        super(a);
        this.a = a;
        this.c = c;
        this.b = b;
        this.c();
    }
    
    public static /* synthetic */ y01 a(final fj1 fj1) {
        return fj1.c;
    }
    
    public boolean b() {
        final a d = this.d;
        return d != null && d.isShowing();
    }
    
    public final void c() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        materialAlertDialogBuilder.setView(((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(this.b, (ViewGroup)null));
        materialAlertDialogBuilder.setCancelable(true);
        materialAlertDialogBuilder.setTitle(2131886772);
        materialAlertDialogBuilder.setPositiveButton(2131886731, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final fj1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                fj1.a(this.a).a(null);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final fj1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        (this.d = materialAlertDialogBuilder.create()).show();
    }
}

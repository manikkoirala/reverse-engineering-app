// 
// Decompiled by Procyon v0.6.0
// 

public class cy0 extends wa
{
    public static double[] p;
    public static double[] q;
    public z22 n;
    public boolean o;
    
    static {
        cy0.p = new double[0];
        cy0.q = new double[0];
    }
    
    public cy0(final zl zl, final ub0 ub0) {
        super(zl, ub0);
        this.n = new z22(new double[] { 1.0, 1.0, 1.0, 1.0 }, 4);
        this.o = true;
    }
    
    @Override
    public void c(final double[] array) {
        final int n = array.length - 1;
        final double n2 = array[n];
        final int a = super.b.a();
        double n3 = 0.0;
        for (int i = 0; i < a; ++i) {
            cy0.p[i] = this.e(n2, i) * cy0.q[i];
            n3 += cy0.p[i];
        }
        double n4 = n3;
        if (n3 == 0.0) {
            n4 = 1.0;
        }
        for (int j = 0; j < n; ++j) {
            super.b.e(0, 0);
            double n5 = 0.0;
            for (int k = 0; k < a; ++k) {
                n5 += cy0.p[k] * super.a.b(super.b.c()).getLocation()[j];
            }
            array[j] = n5 / n4;
        }
    }
    
    @Override
    public void f(final px0 px0) {
        final ub0 b = super.b;
        final int c = super.a.c();
        int i = 0;
        final int n = 0;
        if (b.b(0, c)) {
            final int a = super.b.a();
            if (cy0.p.length < a) {
                final int n2 = a * 2;
                cy0.p = new double[n2];
                cy0.q = new double[n2];
            }
            if (this.o) {
                if (this.n.f() != a) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("weightVector.size(");
                    sb.append(this.n.f());
                    sb.append(") != group iterator size(");
                    sb.append(a);
                    sb.append(")");
                    throw new IllegalArgumentException(sb.toString());
                }
                for (int j = n; j < a; ++j) {
                    cy0.q[j] = this.n.c(j);
                    if (cy0.q[j] < 0.0) {
                        throw new IllegalArgumentException("Negative weight not allowed");
                    }
                }
            }
            else {
                while (i < a) {
                    cy0.q[i] = 1.0;
                    ++i;
                }
            }
            super.f(px0);
            return;
        }
        throw new IllegalArgumentException("Group iterator not in range");
    }
    
    public void j(final boolean o) {
        this.o = o;
    }
    
    public void k(final z22 n) {
        if (n != null) {
            this.n = n;
            return;
        }
        throw new IllegalArgumentException("Weight-vector cannot be null.");
    }
}

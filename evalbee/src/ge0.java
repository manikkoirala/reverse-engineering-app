import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.KeyFactory;
import org.json.JSONException;
import org.json.JSONObject;
import java.security.NoSuchAlgorithmException;
import android.util.Log;
import android.util.Base64;
import java.security.MessageDigest;
import java.security.PublicKey;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public class ge0
{
    public static final String[] c;
    public final SharedPreferences a;
    public final String b;
    
    static {
        c = new String[] { "*", "FCM", "GCM", "" };
    }
    
    public ge0(final r10 r10) {
        this.a = r10.l().getSharedPreferences("com.google.android.gms.appid", 0);
        this.b = b(r10);
    }
    
    public static String b(final r10 r10) {
        final String d = r10.p().d();
        if (d != null) {
            return d;
        }
        final String c = r10.p().c();
        if (!c.startsWith("1:") && !c.startsWith("2:")) {
            return c;
        }
        final String[] split = c.split(":");
        if (split.length != 4) {
            return null;
        }
        final String s = split[1];
        if (s.isEmpty()) {
            return null;
        }
        return s;
    }
    
    public static String c(final PublicKey publicKey) {
        final byte[] encoded = publicKey.getEncoded();
        try {
            final byte[] digest = MessageDigest.getInstance("SHA1").digest(encoded);
            digest[0] = (byte)((digest[0] & 0xF) + 112 & 0xFF);
            return Base64.encodeToString(digest, 0, 8, 11);
        }
        catch (final NoSuchAlgorithmException ex) {
            Log.w("ContentValues", "Unexpected error, device missing required algorithms");
            return null;
        }
    }
    
    public final String a(final String str, final String str2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("|T|");
        sb.append(str);
        sb.append("|");
        sb.append(str2);
        return sb.toString();
    }
    
    public final String d(String string) {
        try {
            string = new JSONObject(string).getString("token");
            return string;
        }
        catch (final JSONException ex) {
            return null;
        }
    }
    
    public final PublicKey e(String ex) {
        try {
            ex = (NoSuchAlgorithmException)(Object)Base64.decode((String)ex, 8);
            ex = (NoSuchAlgorithmException)KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec((byte[])(Object)ex));
            return (PublicKey)ex;
        }
        catch (final NoSuchAlgorithmException ex) {}
        catch (final InvalidKeySpecException ex) {}
        catch (final IllegalArgumentException ex2) {}
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid key stored ");
        sb.append(ex);
        Log.w("ContentValues", sb.toString());
        return null;
    }
    
    public String f() {
        synchronized (this.a) {
            final String g = this.g();
            if (g != null) {
                return g;
            }
            return this.h();
        }
    }
    
    public final String g() {
        synchronized (this.a) {
            return this.a.getString("|S|id", (String)null);
        }
    }
    
    public final String h() {
        synchronized (this.a) {
            final String string = this.a.getString("|S||P|", (String)null);
            if (string == null) {
                return null;
            }
            final PublicKey e = this.e(string);
            if (e == null) {
                return null;
            }
            return c(e);
        }
    }
    
    public String i() {
        synchronized (this.a) {
            final String[] c = ge0.c;
            for (int length = c.length, i = 0; i < length; ++i) {
                final String string = this.a.getString(this.a(this.b, c[i]), (String)null);
                if (string != null && !string.isEmpty()) {
                    String d = string;
                    if (string.startsWith("{")) {
                        d = this.d(string);
                    }
                    return d;
                }
            }
            return null;
        }
    }
}

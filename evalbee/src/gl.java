import android.view.ContentInfo$Builder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Build$VERSION;
import java.util.Objects;
import android.content.ClipData;
import android.view.ContentInfo;

// 
// Decompiled by Procyon v0.6.0
// 

public final class gl
{
    public final f a;
    
    public gl(final f a) {
        this.a = a;
    }
    
    public static String a(final int i) {
        if ((i & 0x1) != 0x0) {
            return "FLAG_CONVERT_TO_PLAIN_TEXT";
        }
        return String.valueOf(i);
    }
    
    public static String e(final int i) {
        if (i == 0) {
            return "SOURCE_APP";
        }
        if (i == 1) {
            return "SOURCE_CLIPBOARD";
        }
        if (i == 2) {
            return "SOURCE_INPUT_METHOD";
        }
        if (i == 3) {
            return "SOURCE_DRAG_AND_DROP";
        }
        if (i == 4) {
            return "SOURCE_AUTOFILL";
        }
        if (i != 5) {
            return String.valueOf(i);
        }
        return "SOURCE_PROCESS_TEXT";
    }
    
    public static gl g(final ContentInfo contentInfo) {
        return new gl((f)new e(contentInfo));
    }
    
    public ClipData b() {
        return this.a.b();
    }
    
    public int c() {
        return this.a.getFlags();
    }
    
    public int d() {
        return this.a.getSource();
    }
    
    public ContentInfo f() {
        final ContentInfo a = this.a.a();
        Objects.requireNonNull(a);
        return fl.a((Object)a);
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
    
    public static final class a
    {
        public final c a;
        
        public a(final ClipData clipData, final int n) {
            c a;
            if (Build$VERSION.SDK_INT >= 31) {
                a = new b(clipData, n);
            }
            else {
                a = new d(clipData, n);
            }
            this.a = a;
        }
        
        public gl a() {
            return this.a.build();
        }
        
        public a b(final Bundle extras) {
            this.a.setExtras(extras);
            return this;
        }
        
        public a c(final int n) {
            this.a.b(n);
            return this;
        }
        
        public a d(final Uri uri) {
            this.a.a(uri);
            return this;
        }
    }
    
    public static final class b implements c
    {
        public final ContentInfo$Builder a;
        
        public b(final ClipData clipData, final int n) {
            this.a = jl.a(clipData, n);
        }
        
        @Override
        public void a(final Uri uri) {
            ll.a(this.a, uri);
        }
        
        @Override
        public void b(final int n) {
            hl.a(this.a, n);
        }
        
        @Override
        public gl build() {
            return new gl((f)new e(il.a(this.a)));
        }
        
        @Override
        public void setExtras(final Bundle bundle) {
            kl.a(this.a, bundle);
        }
    }
    
    public interface c
    {
        void a(final Uri p0);
        
        void b(final int p0);
        
        gl build();
        
        void setExtras(final Bundle p0);
    }
    
    public static final class d implements c
    {
        public ClipData a;
        public int b;
        public int c;
        public Uri d;
        public Bundle e;
        
        public d(final ClipData a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void a(final Uri d) {
            this.d = d;
        }
        
        @Override
        public void b(final int c) {
            this.c = c;
        }
        
        @Override
        public gl build() {
            return new gl((f)new g(this));
        }
        
        @Override
        public void setExtras(final Bundle e) {
            this.e = e;
        }
    }
    
    public static final class e implements f
    {
        public final ContentInfo a;
        
        public e(final ContentInfo contentInfo) {
            this.a = fl.a(l71.g(contentInfo));
        }
        
        @Override
        public ContentInfo a() {
            return this.a;
        }
        
        @Override
        public ClipData b() {
            return ml.a(this.a);
        }
        
        @Override
        public int getFlags() {
            return nl.a(this.a);
        }
        
        @Override
        public int getSource() {
            return ol.a(this.a);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{");
            sb.append(this.a);
            sb.append("}");
            return sb.toString();
        }
    }
    
    public interface f
    {
        ContentInfo a();
        
        ClipData b();
        
        int getFlags();
        
        int getSource();
    }
    
    public static final class g implements f
    {
        public final ClipData a;
        public final int b;
        public final int c;
        public final Uri d;
        public final Bundle e;
        
        public g(final d d) {
            this.a = (ClipData)l71.g(d.a);
            this.b = l71.c(d.b, 0, 5, "source");
            this.c = l71.f(d.c, 1);
            this.d = d.d;
            this.e = d.e;
        }
        
        @Override
        public ContentInfo a() {
            return null;
        }
        
        @Override
        public ClipData b() {
            return this.a;
        }
        
        @Override
        public int getFlags() {
            return this.c;
        }
        
        @Override
        public int getSource() {
            return this.b;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{clip=");
            sb.append(this.a.getDescription());
            sb.append(", source=");
            sb.append(gl.e(this.b));
            sb.append(", flags=");
            sb.append(gl.a(this.c));
            final Uri d = this.d;
            final String s = "";
            String string;
            if (d == null) {
                string = "";
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(", hasLinkUri(");
                sb2.append(this.d.toString().length());
                sb2.append(")");
                string = sb2.toString();
            }
            sb.append(string);
            String str;
            if (this.e == null) {
                str = s;
            }
            else {
                str = ", hasExtras";
            }
            sb.append(str);
            sb.append("}");
            return sb.toString();
        }
    }
}

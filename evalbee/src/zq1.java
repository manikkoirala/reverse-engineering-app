import com.google.firebase.storage.StorageException;
import com.google.android.gms.common.api.Status;
import android.util.Log;
import java.util.concurrent.Executor;
import android.app.Activity;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnCanceledListener;
import java.util.Objects;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.CancellationTokenSource;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Continuation;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.HashMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class zq1 extends am
{
    public static final HashMap j;
    public static final HashMap k;
    public final Object a;
    public final nu1 b;
    public final nu1 c;
    public final nu1 d;
    public final nu1 e;
    public final nu1 f;
    public final nu1 g;
    public volatile int h;
    public a i;
    
    static {
        final HashMap hashMap = j = new HashMap();
        final HashMap hashMap2 = k = new HashMap();
        final Integer value = 1;
        final Integer value2 = 2;
        final Integer value3 = 16;
        final Integer value4 = 256;
        hashMap.put(value, new HashSet(Arrays.asList(value3, value4)));
        final Integer value5 = 8;
        final Integer value6 = 32;
        hashMap.put(value2, new HashSet(Arrays.asList(value5, value6)));
        final Integer value7 = 4;
        hashMap.put(value7, new HashSet(Arrays.asList(value5, value6)));
        hashMap.put(value3, new HashSet(Arrays.asList(value2, value4)));
        final Integer value8 = 64;
        hashMap.put(value8, new HashSet(Arrays.asList(value2, value4)));
        hashMap2.put(value, new HashSet(Arrays.asList(value2, value8)));
        final Integer value9 = 128;
        hashMap2.put(value2, new HashSet(Arrays.asList(value7, value8, value9)));
        hashMap2.put(value7, new HashSet(Arrays.asList(value7, value8, value9)));
        hashMap2.put(value5, new HashSet(Arrays.asList(value3, value8, value9)));
        hashMap2.put(value6, new HashSet(Arrays.asList(value4, value8, value9)));
    }
    
    public zq1() {
        this.a = new Object();
        this.b = new nu1(this, 128, (nu1.a)new sq1(this));
        this.c = new nu1(this, 64, (nu1.a)new tq1(this));
        this.d = new nu1(this, 448, (nu1.a)new uq1(this));
        this.e = new nu1(this, 256, (nu1.a)new vq1(this));
        this.f = new nu1(this, -465, (nu1.a)new wq1());
        this.g = new nu1(this, 16, (nu1.a)new xq1());
        this.h = 1;
    }
    
    public int A() {
        return this.h;
    }
    
    public a B() {
        if (this.z() == null) {
            throw new IllegalStateException();
        }
        final Exception error = this.z().getError();
        if (error == null) {
            return this.z();
        }
        throw new RuntimeExecutionException((Throwable)error);
    }
    
    public a C(final Class clazz) {
        if (this.z() == null) {
            throw new IllegalStateException();
        }
        if (clazz.isInstance(this.z().getError())) {
            throw (Throwable)clazz.cast(this.z().getError());
        }
        final Exception error = this.z().getError();
        if (error == null) {
            return this.z();
        }
        throw new RuntimeExecutionException((Throwable)error);
    }
    
    public Runnable D() {
        return new qq1(this);
    }
    
    public final String E(final int n) {
        if (n == 1) {
            return "INTERNAL_STATE_NOT_STARTED";
        }
        if (n == 2) {
            return "INTERNAL_STATE_QUEUED";
        }
        if (n == 4) {
            return "INTERNAL_STATE_IN_PROGRESS";
        }
        if (n == 8) {
            return "INTERNAL_STATE_PAUSING";
        }
        if (n == 16) {
            return "INTERNAL_STATE_PAUSED";
        }
        if (n == 32) {
            return "INTERNAL_STATE_CANCELING";
        }
        if (n == 64) {
            return "INTERNAL_STATE_FAILURE";
        }
        if (n == 128) {
            return "INTERNAL_STATE_SUCCESS";
        }
        if (n != 256) {
            return "Unknown Internal State!";
        }
        return "INTERNAL_STATE_CANCELED";
    }
    
    public final String F(final int[] array) {
        if (array.length == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(this.E(array[i]));
            sb.append(", ");
        }
        return sb.substring(0, sb.length() - 2);
    }
    
    public abstract jq1 G();
    
    public Object H() {
        return this.a;
    }
    
    public boolean I() {
        return (this.A() & 0x10) != 0x0;
    }
    
    public void R() {
    }
    
    public void S() {
    }
    
    public void T() {
    }
    
    public void U() {
    }
    
    public void V() {
    }
    
    public void W() {
    }
    
    public boolean X() {
        if (this.g0(2, false)) {
            this.c0();
            return true;
        }
        return false;
    }
    
    public zq1 Y(final OnCompleteListener onCompleteListener) {
        Preconditions.checkNotNull(onCompleteListener);
        this.d.i(onCompleteListener);
        return this;
    }
    
    public zq1 Z(final n11 n11) {
        Preconditions.checkNotNull(n11);
        this.g.i(n11);
        return this;
    }
    
    public zq1 a0(final o11 o11) {
        Preconditions.checkNotNull(o11);
        this.f.i(o11);
        return this;
    }
    
    public abstract void b0();
    
    public abstract void c0();
    
    public Task continueWith(final Continuation continuation) {
        return this.w(null, continuation);
    }
    
    public Task continueWith(final Executor executor, final Continuation continuation) {
        return this.w(executor, continuation);
    }
    
    public Task continueWithTask(final Continuation continuation) {
        return this.x(null, continuation);
    }
    
    public Task continueWithTask(final Executor executor, final Continuation continuation) {
        return this.x(executor, continuation);
    }
    
    public a d0() {
        synchronized (this.a) {
            return this.e0();
        }
    }
    
    public abstract a e0();
    
    public final Task f0(final Executor executor, final SuccessContinuation successContinuation) {
        final CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource(cancellationTokenSource.getToken());
        this.b.d(null, executor, new yq1(successContinuation, taskCompletionSource, cancellationTokenSource));
        return taskCompletionSource.getTask();
    }
    
    public boolean g0(final int n, final boolean b) {
        return this.h0(new int[] { n }, b);
    }
    
    public Exception getException() {
        if (this.z() == null) {
            return null;
        }
        return this.z().getError();
    }
    
    public boolean h0(final int[] array, final boolean b) {
        HashMap hashMap;
        if (b) {
            hashMap = zq1.j;
        }
        else {
            hashMap = zq1.k;
        }
        synchronized (this.a) {
            for (final int n : array) {
                final HashSet set = hashMap.get(this.A());
                if (set != null && set.contains(n)) {
                    this.h = n;
                    final int h = this.h;
                    if (h != 2) {
                        if (h != 4) {
                            if (h != 16) {
                                if (h != 64) {
                                    if (h != 128) {
                                        if (h == 256) {
                                            this.R();
                                        }
                                    }
                                    else {
                                        this.W();
                                    }
                                }
                                else {
                                    this.S();
                                }
                            }
                            else {
                                this.T();
                            }
                        }
                        else {
                            this.U();
                        }
                    }
                    else {
                        ar1.b().a(this);
                        this.V();
                    }
                    this.b.h();
                    this.c.h();
                    this.e.h();
                    this.d.h();
                    this.g.h();
                    this.f.h();
                    if (Log.isLoggable("StorageTask", 3)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("changed internal state to: ");
                        sb.append(this.E(n));
                        sb.append(" isUser: ");
                        sb.append(b);
                        sb.append(" from state:");
                        sb.append(this.E(this.h));
                        Log.d("StorageTask", sb.toString());
                    }
                    return true;
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("unable to change internal state to: ");
            sb2.append(this.F(array));
            sb2.append(" isUser: ");
            sb2.append(b);
            sb2.append(" from state:");
            sb2.append(this.E(this.h));
            Log.w("StorageTask", sb2.toString());
            return false;
        }
    }
    
    public zq1 i(final Activity activity, final OnCanceledListener onCanceledListener) {
        Preconditions.checkNotNull(onCanceledListener);
        Preconditions.checkNotNull(activity);
        this.e.d(activity, null, onCanceledListener);
        return this;
    }
    
    public boolean isCanceled() {
        return this.A() == 256;
    }
    
    public boolean isComplete() {
        return (this.A() & 0x1C0) != 0x0;
    }
    
    public boolean isSuccessful() {
        return (this.A() & 0x80) != 0x0;
    }
    
    public zq1 j(final OnCanceledListener onCanceledListener) {
        Preconditions.checkNotNull(onCanceledListener);
        this.e.d(null, null, onCanceledListener);
        return this;
    }
    
    public zq1 k(final Executor executor, final OnCanceledListener onCanceledListener) {
        Preconditions.checkNotNull(onCanceledListener);
        Preconditions.checkNotNull(executor);
        this.e.d(null, executor, onCanceledListener);
        return this;
    }
    
    public zq1 l(final Activity activity, final OnCompleteListener onCompleteListener) {
        Preconditions.checkNotNull(onCompleteListener);
        Preconditions.checkNotNull(activity);
        this.d.d(activity, null, onCompleteListener);
        return this;
    }
    
    public zq1 m(final OnCompleteListener onCompleteListener) {
        Preconditions.checkNotNull(onCompleteListener);
        this.d.d(null, null, onCompleteListener);
        return this;
    }
    
    public zq1 n(final Executor executor, final OnCompleteListener onCompleteListener) {
        Preconditions.checkNotNull(onCompleteListener);
        Preconditions.checkNotNull(executor);
        this.d.d(null, executor, onCompleteListener);
        return this;
    }
    
    public zq1 o(final Activity activity, final OnFailureListener onFailureListener) {
        Preconditions.checkNotNull(onFailureListener);
        Preconditions.checkNotNull(activity);
        this.c.d(activity, null, onFailureListener);
        return this;
    }
    
    public Task onSuccessTask(final SuccessContinuation successContinuation) {
        return this.f0(null, successContinuation);
    }
    
    public Task onSuccessTask(final Executor executor, final SuccessContinuation successContinuation) {
        return this.f0(executor, successContinuation);
    }
    
    public zq1 p(final OnFailureListener onFailureListener) {
        Preconditions.checkNotNull(onFailureListener);
        this.c.d(null, null, onFailureListener);
        return this;
    }
    
    public zq1 q(final Executor executor, final OnFailureListener onFailureListener) {
        Preconditions.checkNotNull(onFailureListener);
        Preconditions.checkNotNull(executor);
        this.c.d(null, executor, onFailureListener);
        return this;
    }
    
    public zq1 r(final n11 n11) {
        Preconditions.checkNotNull(n11);
        this.g.d(null, null, n11);
        return this;
    }
    
    public zq1 s(final o11 o11) {
        Preconditions.checkNotNull(o11);
        this.f.d(null, null, o11);
        return this;
    }
    
    public zq1 t(final Activity activity, final OnSuccessListener onSuccessListener) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(onSuccessListener);
        this.b.d(activity, null, onSuccessListener);
        return this;
    }
    
    public zq1 u(final OnSuccessListener onSuccessListener) {
        Preconditions.checkNotNull(onSuccessListener);
        this.b.d(null, null, onSuccessListener);
        return this;
    }
    
    public zq1 v(final Executor executor, final OnSuccessListener onSuccessListener) {
        Preconditions.checkNotNull(executor);
        Preconditions.checkNotNull(onSuccessListener);
        this.b.d(null, executor, onSuccessListener);
        return this;
    }
    
    public final Task w(final Executor executor, final Continuation continuation) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.d.d(null, executor, new mq1(this, continuation, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public final Task x(final Executor executor, final Continuation continuation) {
        final CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource(cancellationTokenSource.getToken());
        this.d.d(null, executor, new rq1(this, continuation, taskCompletionSource, cancellationTokenSource));
        return taskCompletionSource.getTask();
    }
    
    public final void y() {
        if (!this.isComplete() && !this.I() && this.A() != 2 && !this.g0(256, false)) {
            this.g0(64, false);
        }
    }
    
    public final a z() {
        final a i = this.i;
        if (i != null) {
            return i;
        }
        if (!this.isComplete()) {
            return null;
        }
        if (this.i == null) {
            this.i = this.d0();
        }
        return this.i;
    }
    
    public interface a
    {
        Exception getError();
    }
    
    public abstract class b implements a
    {
        public final Exception a;
        public final zq1 b;
        
        public b(final zq1 b, final Exception a) {
            this.b = b;
            if (a == null) {
                StorageException fromErrorStatus = null;
                Label_0029: {
                    Status status;
                    if (b.isCanceled()) {
                        status = Status.RESULT_CANCELED;
                    }
                    else {
                        if (b.A() != 64) {
                            fromErrorStatus = null;
                            break Label_0029;
                        }
                        status = Status.RESULT_INTERNAL_ERROR;
                    }
                    fromErrorStatus = StorageException.fromErrorStatus(status);
                }
                this.a = fromErrorStatus;
            }
            else {
                this.a = a;
            }
        }
        
        @Override
        public Exception getError() {
            return this.a;
        }
    }
}

import android.os.BaseBundle;
import java.util.Iterator;
import androidx.work.BackoffPolicy;
import android.os.PersistableBundle;
import android.app.job.JobInfo;
import android.net.NetworkRequest$Builder;
import android.app.job.JobInfo$Builder;
import android.os.Build$VERSION;
import androidx.work.NetworkType;
import android.app.job.JobInfo$TriggerContentUri;
import androidx.work.impl.background.systemjob.SystemJobService;
import android.content.Context;
import android.content.ComponentName;

// 
// Decompiled by Procyon v0.6.0
// 

public class ot1
{
    public static final String c;
    public final ComponentName a;
    public final ch b;
    
    static {
        c = xl0.i("SystemJobInfoConverter");
    }
    
    public ot1(final Context context, final ch b) {
        this.b = b;
        this.a = new ComponentName(context.getApplicationContext(), (Class)SystemJobService.class);
    }
    
    public static JobInfo$TriggerContentUri b(final zk.c c) {
        return new JobInfo$TriggerContentUri(c.a(), (int)(c.b() ? 1 : 0));
    }
    
    public static int c(final NetworkType obj) {
        final int n = ot1$a.a[obj.ordinal()];
        if (n == 1) {
            return 0;
        }
        if (n == 2) {
            return 1;
        }
        if (n == 3) {
            return 2;
        }
        if (n != 4) {
            if (n == 5) {
                if (Build$VERSION.SDK_INT >= 26) {
                    return 4;
                }
            }
            final xl0 e = xl0.e();
            final String c = ot1.c;
            final StringBuilder sb = new StringBuilder();
            sb.append("API version too low. Cannot convert network type value ");
            sb.append(obj);
            e.a(c, sb.toString());
            return 1;
        }
        return 3;
    }
    
    public static void d(final JobInfo$Builder jobInfo$Builder, final NetworkType networkType) {
        if (Build$VERSION.SDK_INT >= 30 && networkType == NetworkType.TEMPORARILY_UNMETERED) {
            nt1.a(jobInfo$Builder, new NetworkRequest$Builder().addCapability(25).build());
        }
        else {
            jobInfo$Builder.setRequiredNetworkType(c(networkType));
        }
    }
    
    public JobInfo a(final p92 p2, int n) {
        final zk j = p2.j;
        final PersistableBundle extras = new PersistableBundle();
        ((BaseBundle)extras).putString("EXTRA_WORK_SPEC_ID", p2.a);
        ((BaseBundle)extras).putInt("EXTRA_WORK_SPEC_GENERATION", p2.f());
        ((BaseBundle)extras).putBoolean("EXTRA_IS_PERIODIC", p2.m());
        final JobInfo$Builder setExtras = new JobInfo$Builder(n, this.a).setRequiresCharging(j.g()).setRequiresDeviceIdle(j.h()).setExtras(extras);
        d(setExtras, j.d());
        final boolean h = j.h();
        boolean b = false;
        if (!h) {
            if (p2.l == BackoffPolicy.LINEAR) {
                n = 0;
            }
            else {
                n = 1;
            }
            setExtras.setBackoffCriteria(p2.m, n);
        }
        final long max = Math.max(p2.c() - this.b.currentTimeMillis(), 0L);
        if (Build$VERSION.SDK_INT > 28 && max <= 0L) {
            if (!p2.q) {
                jt1.a(setExtras, true);
            }
        }
        else {
            setExtras.setMinimumLatency(max);
        }
        if (j.e()) {
            final Iterator iterator = j.c().iterator();
            while (iterator.hasNext()) {
                setExtras.addTriggerContentUri(b((zk.c)iterator.next()));
            }
            setExtras.setTriggerContentUpdateDelay(j.b());
            setExtras.setTriggerContentMaxDelay(j.a());
        }
        setExtras.setPersisted(false);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 26) {
            kt1.a(setExtras, j.f());
            lt1.a(setExtras, j.i());
        }
        if (p2.k > 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (max > 0L) {
            b = true;
        }
        if (sdk_INT >= 31 && p2.q && n == 0 && !b) {
            mt1.a(setExtras, true);
        }
        return setExtras.build();
    }
}

import android.net.Uri;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ImageView;

// 
// Decompiled by Procyon v0.6.0
// 

public class z6 extends ImageView
{
    private final x5 mBackgroundTintHelper;
    private boolean mHasLevel;
    private final y6 mImageHelper;
    
    public z6(final Context context) {
        this(context, null);
    }
    
    public z6(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public z6(final Context context, final AttributeSet set, final int n) {
        super(qw1.b(context), set, n);
        this.mHasLevel = false;
        pv1.a((View)this, ((View)this).getContext());
        (this.mBackgroundTintHelper = new x5((View)this)).e(set, n);
        (this.mImageHelper = new y6(this)).g(set, n);
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.b();
        }
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.c();
        }
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList c;
        if (mBackgroundTintHelper != null) {
            c = mBackgroundTintHelper.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode d;
        if (mBackgroundTintHelper != null) {
            d = mBackgroundTintHelper.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public ColorStateList getSupportImageTintList() {
        final y6 mImageHelper = this.mImageHelper;
        ColorStateList d;
        if (mImageHelper != null) {
            d = mImageHelper.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public PorterDuff$Mode getSupportImageTintMode() {
        final y6 mImageHelper = this.mImageHelper;
        PorterDuff$Mode e;
        if (mImageHelper != null) {
            e = mImageHelper.e();
        }
        else {
            e = null;
        }
        return e;
    }
    
    public boolean hasOverlappingRendering() {
        return this.mImageHelper.f() && super.hasOverlappingRendering();
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.g(backgroundResource);
        }
    }
    
    public void setImageBitmap(final Bitmap imageBitmap) {
        super.setImageBitmap(imageBitmap);
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.c();
        }
    }
    
    public void setImageDrawable(final Drawable imageDrawable) {
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null && imageDrawable != null && !this.mHasLevel) {
            mImageHelper.h(imageDrawable);
        }
        super.setImageDrawable(imageDrawable);
        final y6 mImageHelper2 = this.mImageHelper;
        if (mImageHelper2 != null) {
            mImageHelper2.c();
            if (!this.mHasLevel) {
                this.mImageHelper.b();
            }
        }
    }
    
    public void setImageLevel(final int imageLevel) {
        super.setImageLevel(imageLevel);
        this.mHasLevel = true;
    }
    
    public void setImageResource(final int n) {
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.i(n);
        }
    }
    
    public void setImageURI(final Uri imageURI) {
        super.setImageURI(imageURI);
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.c();
        }
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.j(porterDuff$Mode);
        }
    }
    
    public void setSupportImageTintList(final ColorStateList list) {
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.j(list);
        }
    }
    
    public void setSupportImageTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final y6 mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.k(porterDuff$Mode);
        }
    }
}

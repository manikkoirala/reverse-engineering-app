import java.io.File;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ia extends ao
{
    public final CrashlyticsReport a;
    public final String b;
    public final File c;
    
    public ia(final CrashlyticsReport a, final String b, final File c) {
        if (a == null) {
            throw new NullPointerException("Null report");
        }
        this.a = a;
        if (b == null) {
            throw new NullPointerException("Null sessionId");
        }
        this.b = b;
        if (c != null) {
            this.c = c;
            return;
        }
        throw new NullPointerException("Null reportFile");
    }
    
    @Override
    public CrashlyticsReport b() {
        return this.a;
    }
    
    @Override
    public File c() {
        return this.c;
    }
    
    @Override
    public String d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ao) {
            final ao ao = (ao)o;
            if (!this.a.equals(ao.b()) || !this.b.equals(ao.d()) || !this.c.equals(ao.c())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((this.a.hashCode() ^ 0xF4243) * 1000003 ^ this.b.hashCode()) * 1000003 ^ this.c.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CrashlyticsReportWithSessionId{report=");
        sb.append(this.a);
        sb.append(", sessionId=");
        sb.append(this.b);
        sb.append(", reportFile=");
        sb.append(this.c);
        sb.append("}");
        return sb.toString();
    }
}

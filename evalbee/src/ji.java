// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ji
{
    public static final ji a;
    public static final ji b;
    public static final ji c;
    
    static {
        a = new ji() {
            @Override
            public ji d(final Comparable comparable, final Comparable comparable2) {
                return this.g(comparable.compareTo(comparable2));
            }
            
            @Override
            public int e() {
                return 0;
            }
            
            public ji g(final int n) {
                ji ji;
                if (n < 0) {
                    ji = ji.a();
                }
                else if (n > 0) {
                    ji = ji.b();
                }
                else {
                    ji = ji.c();
                }
                return ji;
            }
        };
        b = new b(-1);
        c = new b(1);
    }
    
    public static /* synthetic */ ji a() {
        return ji.b;
    }
    
    public static /* synthetic */ ji b() {
        return ji.c;
    }
    
    public static /* synthetic */ ji c() {
        return ji.a;
    }
    
    public static ji f() {
        return ji.a;
    }
    
    public abstract ji d(final Comparable p0, final Comparable p1);
    
    public abstract int e();
    
    public static final class b extends ji
    {
        public final int d;
        
        public b(final int d) {
            super(null);
            this.d = d;
        }
        
        @Override
        public ji d(final Comparable comparable, final Comparable comparable2) {
            return this;
        }
        
        @Override
        public int e() {
            return this.d;
        }
    }
}

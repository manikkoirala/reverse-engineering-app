import android.content.res.Resources;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class l3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    
    public l3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int i, final View view, final ViewGroup viewGroup) {
        final b21 b21 = this.b.get(i);
        final LayoutInflater layoutInflater = (LayoutInflater)this.a.getSystemService("layout_inflater");
        final View inflate = layoutInflater.inflate(2131493025, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297267);
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131296729);
        final StringBuilder sb = new StringBuilder();
        sb.append(b21.d());
        sb.append("");
        textView.setText((CharSequence)sb.toString());
        LinearLayout linearLayout2;
        TextView textView2;
        TextView textView3;
        StringBuilder sb2;
        Resources resources;
        int n;
        for (i = 0; i < b21.a().length; ++i) {
            linearLayout2 = (LinearLayout)layoutInflater.inflate(2131493099, (ViewGroup)null);
            textView2 = (TextView)((View)linearLayout2).findViewById(2131297256);
            textView3 = (TextView)((View)linearLayout2).findViewById(2131297273);
            textView2.setText((CharSequence)b21.c()[i]);
            sb2 = new StringBuilder();
            sb2.append(b21.b()[i]);
            sb2.append("");
            textView3.setText((CharSequence)sb2.toString());
            if (b21.a()[i]) {
                resources = this.a.getResources();
                n = 2131099711;
            }
            else {
                resources = this.a.getResources();
                n = 2131099731;
            }
            textView2.setTextColor(resources.getColor(n));
            ((ViewGroup)linearLayout).addView((View)linearLayout2);
        }
        return inflate;
    }
}

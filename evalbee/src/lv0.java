import android.view.MenuItem;
import java.util.Iterator;
import android.view.MenuInflater;
import android.view.Menu;
import androidx.lifecycle.f;
import androidx.lifecycle.Lifecycle;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class lv0
{
    public final Runnable a;
    public final CopyOnWriteArrayList b;
    public final Map c;
    
    public lv0(final Runnable a) {
        this.b = new CopyOnWriteArrayList();
        this.c = new HashMap();
        this.a = a;
    }
    
    public void c(final rv0 e) {
        this.b.add(e);
        this.a.run();
    }
    
    public void d(final rv0 rv0, final qj0 qj0) {
        this.c(rv0);
        final Lifecycle lifecycle = qj0.getLifecycle();
        final a a = this.c.remove(rv0);
        if (a != null) {
            a.a();
        }
        this.c.put(rv0, new a(lifecycle, new jv0(this, rv0)));
    }
    
    public void e(final rv0 rv0, final qj0 qj0, final Lifecycle.State state) {
        final Lifecycle lifecycle = qj0.getLifecycle();
        final a a = this.c.remove(rv0);
        if (a != null) {
            a.a();
        }
        this.c.put(rv0, new a(lifecycle, new kv0(this, state, rv0)));
    }
    
    public void h(final Menu menu, final MenuInflater menuInflater) {
        final Iterator iterator = this.b.iterator();
        if (!iterator.hasNext()) {
            return;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    public void i(final Menu menu) {
        final Iterator iterator = this.b.iterator();
        if (!iterator.hasNext()) {
            return;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    public boolean j(final MenuItem menuItem) {
        final Iterator iterator = this.b.iterator();
        if (!iterator.hasNext()) {
            return false;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    public void k(final Menu menu) {
        final Iterator iterator = this.b.iterator();
        if (!iterator.hasNext()) {
            return;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    public void l(final rv0 o) {
        this.b.remove(o);
        final a a = this.c.remove(o);
        if (a != null) {
            a.a();
        }
        this.a.run();
    }
    
    public static class a
    {
        public final Lifecycle a;
        public f b;
        
        public a(final Lifecycle a, final f b) {
            (this.a = a).a(this.b = b);
        }
        
        public void a() {
            this.a.c(this.b);
            this.b = null;
        }
    }
}

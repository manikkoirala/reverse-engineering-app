import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import org.json.JSONException;
import android.net.Uri$Builder;
import java.net.URL;
import java.io.OutputStream;
import org.json.JSONObject;
import java.util.Iterator;
import java.io.BufferedOutputStream;
import android.text.TextUtils;
import com.google.firebase.storage.StorageException;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.net.NetworkInfo;
import java.net.SocketException;
import android.net.ConnectivityManager;
import java.io.IOException;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import java.util.HashMap;
import java.net.HttpURLConnection;
import java.io.InputStream;
import java.util.Map;
import android.content.Context;
import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class xy0
{
    public static final Uri k;
    public static nd0 l;
    public Exception a;
    public kq1 b;
    public Context c;
    public Map d;
    public int e;
    public String f;
    public int g;
    public InputStream h;
    public HttpURLConnection i;
    public Map j;
    
    static {
        k = Uri.parse("https://firebasestorage.googleapis.com/v0");
        xy0.l = new od0();
    }
    
    public xy0(final kq1 b, final r10 r10) {
        this.j = new HashMap();
        Preconditions.checkNotNull(b);
        Preconditions.checkNotNull(r10);
        this.b = b;
        this.c = r10.l();
        this.G("x-firebase-gmpid", r10.p().c());
    }
    
    public static String k(final Uri uri) {
        final String path = uri.getPath();
        if (path == null) {
            return "";
        }
        String substring = path;
        if (path.startsWith("/")) {
            substring = path.substring(1);
        }
        return substring;
    }
    
    public final void A(final String s, final String s2) {
        this.D(s, s2);
        try {
            this.E();
        }
        catch (final IOException a) {
            final StringBuilder sb = new StringBuilder();
            sb.append("error sending network request ");
            sb.append(this.e());
            sb.append(" ");
            sb.append(this.u());
            Log.w("NetworkRequest", sb.toString(), (Throwable)a);
            this.a = a;
            this.e = -2;
        }
        this.C();
    }
    
    public void B(final String s, final String s2, final Context context) {
        if (!this.d(context)) {
            return;
        }
        this.A(s, s2);
    }
    
    public void C() {
        final HttpURLConnection i = this.i;
        if (i != null) {
            i.disconnect();
        }
    }
    
    public void D(final String s, final String s2) {
        if (this.a != null) {
            this.e = -1;
            return;
        }
        if (Log.isLoggable("NetworkRequest", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("sending network request ");
            sb.append(this.e());
            sb.append(" ");
            sb.append(this.u());
            Log.d("NetworkRequest", sb.toString());
        }
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)this.c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            try {
                (this.i = this.c()).setRequestMethod(this.e());
                this.b(this.i, s, s2);
                this.y(this.i);
                if (Log.isLoggable("NetworkRequest", 3)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("network request result ");
                    sb2.append(this.e);
                    Log.d("NetworkRequest", sb2.toString());
                }
            }
            catch (final IOException a) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("error sending network request ");
                sb3.append(this.e());
                sb3.append(" ");
                sb3.append(this.u());
                Log.w("NetworkRequest", sb3.toString(), (Throwable)a);
                this.a = a;
                this.e = -2;
            }
            return;
        }
        this.e = -2;
        this.a = new SocketException("Network subsystem is unavailable");
    }
    
    public final void E() {
        if (this.v()) {
            this.z(this.h);
        }
        else {
            this.w(this.h);
        }
    }
    
    public final void F() {
        this.a = null;
        this.e = 0;
    }
    
    public void G(final String s, final String s2) {
        this.j.put(s, s2);
    }
    
    public void a(final TaskCompletionSource taskCompletionSource, final Object result) {
        final Exception f = this.f();
        if (this.v() && f == null) {
            taskCompletionSource.setResult(result);
        }
        else {
            taskCompletionSource.setException((Exception)StorageException.fromExceptionAndHttpCode(f, this.o()));
        }
    }
    
    public final void b(HttpURLConnection httpURLConnection, final String str, String string) {
        Preconditions.checkNotNull(httpURLConnection);
        if (!TextUtils.isEmpty((CharSequence)str)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Firebase ");
            sb.append(str);
            httpURLConnection.setRequestProperty("Authorization", sb.toString());
        }
        else {
            Log.w("NetworkRequest", "no auth token for request");
        }
        if (!TextUtils.isEmpty((CharSequence)string)) {
            httpURLConnection.setRequestProperty("x-firebase-appcheck", string);
        }
        else {
            Log.w("NetworkRequest", "No App Check token for request.");
        }
        httpURLConnection.setRequestProperty("X-Firebase-Storage-Version", "Android/20.3.0");
        for (final Map.Entry<String, V> entry : this.j.entrySet()) {
            httpURLConnection.setRequestProperty(entry.getKey(), (String)entry.getValue());
        }
        final JSONObject g = this.g();
        byte[] bytes;
        int n;
        if (g != null) {
            bytes = g.toString().getBytes("UTF-8");
            n = bytes.length;
        }
        else {
            final byte[] h = this.h();
            final int n2 = n = this.i();
            bytes = h;
            if (n2 == 0) {
                n = n2;
                if ((bytes = h) != null) {
                    n = h.length;
                    bytes = h;
                }
            }
        }
        if (bytes != null && bytes.length > 0) {
            if (g != null) {
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
            }
            httpURLConnection.setDoOutput(true);
            string = Integer.toString(n);
        }
        else {
            string = "0";
        }
        httpURLConnection.setRequestProperty("Content-Length", string);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDoInput(true);
        if (bytes != null && bytes.length > 0) {
            final OutputStream outputStream = httpURLConnection.getOutputStream();
            if (outputStream != null) {
                httpURLConnection = (HttpURLConnection)new BufferedOutputStream(outputStream);
                try {
                    ((BufferedOutputStream)httpURLConnection).write(bytes, 0, n);
                    return;
                }
                finally {
                    ((OutputStream)httpURLConnection).close();
                }
            }
            Log.e("NetworkRequest", "Unable to write to the http request!");
        }
    }
    
    public final HttpURLConnection c() {
        final Uri u = this.u();
        final Map l = this.l();
        Uri build = u;
        if (l != null) {
            final Uri$Builder buildUpon = u.buildUpon();
            for (final Map.Entry<String, V> entry : l.entrySet()) {
                buildUpon.appendQueryParameter((String)entry.getKey(), (String)entry.getValue());
            }
            build = buildUpon.build();
        }
        return xy0.l.a(new URL(build.toString()));
    }
    
    public final boolean d(final Context context) {
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        this.a = new SocketException("Network subsystem is unavailable");
        this.e = -2;
        return false;
    }
    
    public abstract String e();
    
    public Exception f() {
        return this.a;
    }
    
    public JSONObject g() {
        return null;
    }
    
    public byte[] h() {
        return null;
    }
    
    public int i() {
        return 0;
    }
    
    public String j() {
        return k(this.b.a());
    }
    
    public Map l() {
        return null;
    }
    
    public String m() {
        return this.f;
    }
    
    public JSONObject n() {
        JSONObject jsonObject;
        if (!TextUtils.isEmpty((CharSequence)this.f)) {
            try {
                jsonObject = new JSONObject(this.f);
            }
            catch (final JSONException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("error parsing result into JSON:");
                sb.append(this.f);
                Log.e("NetworkRequest", sb.toString(), (Throwable)ex);
                jsonObject = new JSONObject();
            }
        }
        else {
            jsonObject = new JSONObject();
        }
        return jsonObject;
    }
    
    public int o() {
        return this.e;
    }
    
    public Map p() {
        return this.d;
    }
    
    public String q(final String s) {
        final Map p = this.p();
        if (p != null) {
            final List list = p.get(s);
            if (list != null && list.size() > 0) {
                return (String)list.get(0);
            }
        }
        return null;
    }
    
    public int r() {
        return this.g;
    }
    
    public kq1 s() {
        return this.b;
    }
    
    public InputStream t() {
        return this.h;
    }
    
    public Uri u() {
        return this.b.c();
    }
    
    public boolean v() {
        final int e = this.e;
        return e >= 200 && e < 300;
    }
    
    public void w(final InputStream inputStream) {
        this.x(inputStream);
    }
    
    public final void x(InputStream in) {
        final StringBuilder sb = new StringBuilder();
        if (in != null) {
            in = (InputStream)new BufferedReader(new InputStreamReader(in, "UTF-8"));
            try {
                while (true) {
                    final String line = ((BufferedReader)in).readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(line);
                }
            }
            finally {
                ((BufferedReader)in).close();
            }
        }
        this.f = sb.toString();
        if (!this.v()) {
            this.a = new IOException(this.f);
        }
    }
    
    public final void y(final HttpURLConnection httpURLConnection) {
        Preconditions.checkNotNull(httpURLConnection);
        this.e = httpURLConnection.getResponseCode();
        this.d = httpURLConnection.getHeaderFields();
        this.g = httpURLConnection.getContentLength();
        InputStream h;
        if (this.v()) {
            h = httpURLConnection.getInputStream();
        }
        else {
            h = httpURLConnection.getErrorStream();
        }
        this.h = h;
    }
    
    public void z(final InputStream inputStream) {
        this.x(inputStream);
    }
}

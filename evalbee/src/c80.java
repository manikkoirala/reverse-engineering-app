import java.util.Collection;
import com.google.common.collect.Sets;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class c80 extends t70 implements Set
{
    @Override
    public abstract Set delegate();
    
    @Override
    public boolean equals(final Object o) {
        return o == this || this.delegate().equals(o);
    }
    
    @Override
    public int hashCode() {
        return this.delegate().hashCode();
    }
    
    public boolean standardEquals(final Object o) {
        return Sets.a(this, o);
    }
    
    public int standardHashCode() {
        return Sets.b(this);
    }
    
    @Override
    public boolean standardRemoveAll(final Collection<?> collection) {
        return Sets.f(this, (Collection)i71.r(collection));
    }
}

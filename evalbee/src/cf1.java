import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public class cf1 extends bf1
{
    public final Uri m;
    public final byte[] n;
    public final long o;
    public final boolean p;
    public final int q;
    
    public cf1(final kq1 kq1, final r10 r10, final Uri m, byte[] n, final long n2, final int q, final boolean p7) {
        super(kq1, r10);
        if (n == null && q != -1) {
            super.a = new IllegalArgumentException("contentType is null or empty");
        }
        if (n2 < 0L) {
            super.a = new IllegalArgumentException("offset cannot be negative");
        }
        this.q = q;
        this.m = m;
        if (q <= 0) {
            n = null;
        }
        this.n = n;
        this.o = n2;
        this.p = p7;
        super.G("X-Goog-Upload-Protocol", "resumable");
        String s;
        if (p7 && q > 0) {
            s = "upload, finalize";
        }
        else if (p7) {
            s = "finalize";
        }
        else {
            s = "upload";
        }
        super.G("X-Goog-Upload-Command", s);
        super.G("X-Goog-Upload-Offset", Long.toString(n2));
    }
    
    @Override
    public String e() {
        return "POST";
    }
    
    @Override
    public byte[] h() {
        return this.n;
    }
    
    @Override
    public int i() {
        int q = this.q;
        if (q <= 0) {
            q = 0;
        }
        return q;
    }
    
    @Override
    public Uri u() {
        return this.m;
    }
}

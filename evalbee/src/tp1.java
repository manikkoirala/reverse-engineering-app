import android.os.SystemClock;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class tp1
{
    public static tp1 a(final long n, final long n2, final long n3) {
        return new qa(n, n2, n3);
    }
    
    public static tp1 e() {
        return a(System.currentTimeMillis(), SystemClock.elapsedRealtime(), SystemClock.uptimeMillis());
    }
    
    public abstract long b();
    
    public abstract long c();
    
    public abstract long d();
}

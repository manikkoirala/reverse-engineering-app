import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.ByteArrayOutputStream;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import java.io.File;

// 
// Decompiled by Procyon v0.6.0
// 

public class w00 implements ey0
{
    public final File a;
    public final String b;
    public final String c;
    
    public w00(final String b, final String c, final File a) {
        this.b = b;
        this.c = c;
        this.a = a;
    }
    
    @Override
    public String a() {
        return this.c;
    }
    
    @Override
    public CrashlyticsReport.d.b b() {
        final byte[] c = this.c();
        Object a;
        if (c != null) {
            a = CrashlyticsReport.d.b.a().b(c).c(this.b).a();
        }
        else {
            a = null;
        }
        return (CrashlyticsReport.d.b)a;
    }
    
    public final byte[] c() {
        final byte[] array = new byte[8192];
        try {
            final InputStream stream = this.getStream();
            try {
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                try {
                    final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(out);
                    if (stream == null) {
                        gzipOutputStream.close();
                        out.close();
                        if (stream != null) {
                            stream.close();
                        }
                        return null;
                    }
                    Label_0054: {
                        break Label_0054;
                        try {
                            while (true) {
                                final int read = stream.read(array);
                                if (read <= 0) {
                                    break;
                                }
                                gzipOutputStream.write(array, 0, read);
                            }
                            gzipOutputStream.finish();
                            final byte[] byteArray = out.toByteArray();
                            gzipOutputStream.close();
                            out.close();
                            stream.close();
                            return byteArray;
                        }
                        finally {
                            try {
                                gzipOutputStream.close();
                            }
                            finally {
                                final Throwable t;
                                final Throwable exception;
                                t.addSuppressed(exception);
                            }
                        }
                    }
                }
                finally {
                    try {
                        out.close();
                    }
                    finally {
                        final Throwable t2;
                        final Throwable exception2;
                        t2.addSuppressed(exception2);
                    }
                }
            }
            finally {
                if (stream != null) {
                    try {
                        stream.close();
                    }
                    finally {
                        final Throwable t3;
                        final Throwable exception3;
                        t3.addSuppressed(exception3);
                    }
                }
            }
        }
        catch (final IOException ex) {
            return null;
        }
    }
    
    @Override
    public InputStream getStream() {
        Label_0037: {
            if (!this.a.exists()) {
                break Label_0037;
            }
            if (!this.a.isFile()) {
                break Label_0037;
            }
            try {
                return new FileInputStream(this.a);
                return null;
            }
            catch (final FileNotFoundException ex) {
                return null;
            }
        }
    }
}

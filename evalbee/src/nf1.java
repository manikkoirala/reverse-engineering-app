import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import org.json.JSONObject;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class nf1
{
    public static final hp a;
    
    static {
        a = new mh0().j(ea.a).i();
    }
    
    public static nf1 a(final String s) {
        final JSONObject jsonObject = new JSONObject(s);
        return b(jsonObject.getString("rolloutId"), jsonObject.getString("parameterKey"), jsonObject.getString("parameterValue"), jsonObject.getString("variantId"), jsonObject.getLong("templateVersion"));
    }
    
    public static nf1 b(final String s, final String s2, final String s3, final String s4, final long n) {
        return new pa(s, s2, i(s3), s4, n);
    }
    
    public static String i(final String s) {
        String substring = s;
        if (s.length() > 256) {
            substring = s.substring(0, 256);
        }
        return substring;
    }
    
    public abstract String c();
    
    public abstract String d();
    
    public abstract String e();
    
    public abstract long f();
    
    public abstract String g();
    
    public CrashlyticsReport.e.d.e h() {
        return CrashlyticsReport.e.d.e.a().d(CrashlyticsReport.e.d.e.b.a().c(this.g()).b(this.e()).a()).b(this.c()).c(this.d()).e(this.f()).a();
    }
}

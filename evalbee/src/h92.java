import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import java.util.Collections;
import java.util.List;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class h92 implements g92
{
    public final RoomDatabase a;
    public final ix b;
    
    public h92(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final h92 d;
            
            @Override
            public String e() {
                return "INSERT OR IGNORE INTO `WorkName` (`name`,`work_spec_id`) VALUES (?,?)";
            }
            
            public void k(final ws1 ws1, final f92 f92) {
                if (f92.a() == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, f92.a());
                }
                if (f92.b() == null) {
                    ws1.I(2);
                }
                else {
                    ws1.y(2, f92.b());
                }
            }
        };
    }
    
    public static List c() {
        return Collections.emptyList();
    }
    
    @Override
    public List a(String string) {
        final sf1 c = sf1.c("SELECT name FROM workname WHERE work_spec_id=?", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add((Object)string);
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public void b(final f92 f92) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(f92);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
}

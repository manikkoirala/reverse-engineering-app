import com.google.android.gms.measurement.api.AppMeasurementSdk$OnEventListener;
import com.google.android.gms.measurement.api.AppMeasurementSdk;

// 
// Decompiled by Procyon v0.6.0
// 

public final class uf2
{
    public g4.b a;
    public AppMeasurementSdk b;
    public kf2 c;
    
    public uf2(final AppMeasurementSdk b, final g4.b a) {
        this.a = a;
        this.b = b;
        final kf2 c = new kf2(this);
        this.c = c;
        this.b.registerOnMeasurementEventListener((AppMeasurementSdk$OnEventListener)c);
    }
}

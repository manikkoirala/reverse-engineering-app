import android.animation.AnimatorSet;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff$Mode;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.graphics.Region;
import android.graphics.Rect;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.ColorFilter;
import android.graphics.Canvas;
import java.util.Collection;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.res.Resources$Theme;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.graphics.drawable.Drawable$Callback;
import java.util.ArrayList;
import android.animation.Animator$AnimatorListener;
import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.drawable.Animatable;

// 
// Decompiled by Procyon v0.6.0
// 

public class z4 extends d32 implements Animatable
{
    public c b;
    public Context c;
    public ArgbEvaluator d;
    public d e;
    public Animator$AnimatorListener f;
    public ArrayList g;
    public final Drawable$Callback h;
    
    public z4() {
        this(null, null, null);
    }
    
    public z4(final Context context) {
        this(context, null, null);
    }
    
    public z4(final Context c, final c b, final Resources resources) {
        this.d = null;
        this.f = null;
        this.g = null;
        final Drawable$Callback h = (Drawable$Callback)new Drawable$Callback(this) {
            public final z4 a;
            
            public void invalidateDrawable(final Drawable drawable) {
                this.a.invalidateSelf();
            }
            
            public void scheduleDrawable(final Drawable drawable, final Runnable runnable, final long n) {
                this.a.scheduleSelf(runnable, n);
            }
            
            public void unscheduleDrawable(final Drawable drawable, final Runnable runnable) {
                this.a.unscheduleSelf(runnable);
            }
        };
        this.h = (Drawable$Callback)h;
        this.c = c;
        if (b != null) {
            this.b = b;
        }
        else {
            this.b = new c(c, b, (Drawable$Callback)h, resources);
        }
    }
    
    public static z4 a(final Context context, final int n) {
        final z4 z4 = new z4(context);
        (z4.a = le1.e(context.getResources(), n, context.getTheme())).setCallback(z4.h);
        z4.e = new d(z4.a.getConstantState());
        return z4;
    }
    
    public static void c(final AnimatedVectorDrawable animatedVectorDrawable, final y4 y4) {
        animatedVectorDrawable.registerAnimationCallback(y4.getPlatformCallback());
    }
    
    public static boolean g(final AnimatedVectorDrawable animatedVectorDrawable, final y4 y4) {
        return animatedVectorDrawable.unregisterAnimationCallback(y4.getPlatformCallback());
    }
    
    @Override
    public void applyTheme(final Resources$Theme resources$Theme) {
        final Drawable a = super.a;
        if (a != null) {
            wu.a(a, resources$Theme);
        }
    }
    
    public void b(final y4 y4) {
        final Drawable a = super.a;
        if (a != null) {
            c((AnimatedVectorDrawable)a, y4);
            return;
        }
        if (y4 == null) {
            return;
        }
        if (this.g == null) {
            this.g = new ArrayList();
        }
        if (this.g.contains(y4)) {
            return;
        }
        this.g.add(y4);
        if (this.f == null) {
            this.f = (Animator$AnimatorListener)new AnimatorListenerAdapter(this) {
                public final z4 a;
                
                public void onAnimationEnd(final Animator animator) {
                    final ArrayList list = new ArrayList(this.a.g);
                    for (int size = list.size(), i = 0; i < size; ++i) {
                        ((y4)list.get(i)).onAnimationEnd(this.a);
                    }
                }
                
                public void onAnimationStart(final Animator animator) {
                    final ArrayList list = new ArrayList(this.a.g);
                    for (int size = list.size(), i = 0; i < size; ++i) {
                        ((y4)list.get(i)).onAnimationStart(this.a);
                    }
                }
            };
        }
        ((Animator)this.b.c).addListener(this.f);
    }
    
    public boolean canApplyTheme() {
        final Drawable a = super.a;
        return a != null && wu.b(a);
    }
    
    public final void d() {
        final Animator$AnimatorListener f = this.f;
        if (f != null) {
            ((Animator)this.b.c).removeListener(f);
            this.f = null;
        }
    }
    
    public void draw(final Canvas canvas) {
        final Drawable a = super.a;
        if (a != null) {
            a.draw(canvas);
            return;
        }
        this.b.b.draw(canvas);
        if (this.b.c.isStarted()) {
            this.invalidateSelf();
        }
    }
    
    public final void e(final String s, final Animator e) {
        e.setTarget(this.b.b.c(s));
        final c b = this.b;
        if (b.d == null) {
            b.d = new ArrayList();
            this.b.e = new r8();
        }
        this.b.d.add(e);
        this.b.e.put(e, s);
    }
    
    public boolean f(final y4 o) {
        final Drawable a = super.a;
        if (a != null) {
            g((AnimatedVectorDrawable)a, o);
        }
        final ArrayList g = this.g;
        if (g != null && o != null) {
            final boolean remove = g.remove(o);
            if (this.g.size() == 0) {
                this.d();
            }
            return remove;
        }
        return false;
    }
    
    public int getAlpha() {
        final Drawable a = super.a;
        if (a != null) {
            return wu.d(a);
        }
        return this.b.b.getAlpha();
    }
    
    public int getChangingConfigurations() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.b.a;
    }
    
    public ColorFilter getColorFilter() {
        final Drawable a = super.a;
        if (a != null) {
            return wu.e(a);
        }
        return this.b.b.getColorFilter();
    }
    
    public Drawable$ConstantState getConstantState() {
        if (super.a != null) {
            return new d(super.a.getConstantState());
        }
        return null;
    }
    
    public int getIntrinsicHeight() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getIntrinsicHeight();
        }
        return this.b.b.getIntrinsicHeight();
    }
    
    public int getIntrinsicWidth() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getIntrinsicWidth();
        }
        return this.b.b.getIntrinsicWidth();
    }
    
    public int getOpacity() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getOpacity();
        }
        return this.b.b.getOpacity();
    }
    
    public void inflate(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set) {
        this.inflate(resources, xmlPullParser, set, null);
    }
    
    public void inflate(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final Drawable a = super.a;
        if (a != null) {
            wu.g(a, resources, xmlPullParser, set, resources$Theme);
            return;
        }
        for (int n = xmlPullParser.getEventType(), depth = xmlPullParser.getDepth(); n != 1 && (xmlPullParser.getDepth() >= depth + 1 || n != 3); n = xmlPullParser.next()) {
            if (n == 2) {
                final String name = xmlPullParser.getName();
                TypedArray typedArray;
                if ("animated-vector".equals(name)) {
                    final TypedArray i = rz1.i(resources, resources$Theme, set, w4.e);
                    final int resourceId = i.getResourceId(0, 0);
                    typedArray = i;
                    if (resourceId != 0) {
                        final e32 b = e32.b(resources, resourceId, resources$Theme);
                        b.g(false);
                        b.setCallback(this.h);
                        final e32 b2 = this.b.b;
                        if (b2 != null) {
                            b2.setCallback((Drawable$Callback)null);
                        }
                        this.b.b = b;
                        typedArray = i;
                    }
                }
                else {
                    if (!"target".equals(name)) {
                        continue;
                    }
                    final TypedArray obtainAttributes = resources.obtainAttributes(set, w4.f);
                    final String string = obtainAttributes.getString(0);
                    final int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    typedArray = obtainAttributes;
                    if (resourceId2 != 0) {
                        final Context c = this.c;
                        if (c == null) {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                        this.e(string, c5.a(c, resourceId2));
                        typedArray = obtainAttributes;
                    }
                }
                typedArray.recycle();
            }
        }
        this.b.a();
    }
    
    public boolean isAutoMirrored() {
        final Drawable a = super.a;
        if (a != null) {
            return wu.h(a);
        }
        return this.b.b.isAutoMirrored();
    }
    
    public boolean isRunning() {
        final Drawable a = super.a;
        if (a != null) {
            return ((AnimatedVectorDrawable)a).isRunning();
        }
        return this.b.c.isRunning();
    }
    
    public boolean isStateful() {
        final Drawable a = super.a;
        if (a != null) {
            return a.isStateful();
        }
        return this.b.b.isStateful();
    }
    
    public Drawable mutate() {
        final Drawable a = super.a;
        if (a != null) {
            a.mutate();
        }
        return this;
    }
    
    public void onBoundsChange(final Rect rect) {
        final Drawable a = super.a;
        if (a != null) {
            a.setBounds(rect);
            return;
        }
        this.b.b.setBounds(rect);
    }
    
    @Override
    public boolean onLevelChange(final int n) {
        final Drawable a = super.a;
        if (a != null) {
            return a.setLevel(n);
        }
        return this.b.b.setLevel(n);
    }
    
    public boolean onStateChange(final int[] array) {
        final Drawable a = super.a;
        if (a != null) {
            return a.setState(array);
        }
        return this.b.b.setState(array);
    }
    
    public void setAlpha(final int n) {
        final Drawable a = super.a;
        if (a != null) {
            a.setAlpha(n);
            return;
        }
        this.b.b.setAlpha(n);
    }
    
    public void setAutoMirrored(final boolean autoMirrored) {
        final Drawable a = super.a;
        if (a != null) {
            wu.j(a, autoMirrored);
            return;
        }
        this.b.b.setAutoMirrored(autoMirrored);
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        final Drawable a = super.a;
        if (a != null) {
            a.setColorFilter(colorFilter);
            return;
        }
        this.b.b.setColorFilter(colorFilter);
    }
    
    public void setTint(final int tint) {
        final Drawable a = super.a;
        if (a != null) {
            wu.n(a, tint);
            return;
        }
        this.b.b.setTint(tint);
    }
    
    public void setTintList(final ColorStateList tintList) {
        final Drawable a = super.a;
        if (a != null) {
            wu.o(a, tintList);
            return;
        }
        this.b.b.setTintList(tintList);
    }
    
    public void setTintMode(final PorterDuff$Mode tintMode) {
        final Drawable a = super.a;
        if (a != null) {
            wu.p(a, tintMode);
            return;
        }
        this.b.b.setTintMode(tintMode);
    }
    
    public boolean setVisible(final boolean b, final boolean b2) {
        final Drawable a = super.a;
        if (a != null) {
            return a.setVisible(b, b2);
        }
        this.b.b.setVisible(b, b2);
        return super.setVisible(b, b2);
    }
    
    public void start() {
        final Drawable a = super.a;
        if (a != null) {
            ((AnimatedVectorDrawable)a).start();
            return;
        }
        if (this.b.c.isStarted()) {
            return;
        }
        this.b.c.start();
        this.invalidateSelf();
    }
    
    public void stop() {
        final Drawable a = super.a;
        if (a != null) {
            ((AnimatedVectorDrawable)a).stop();
            return;
        }
        this.b.c.end();
    }
    
    public static class c extends Drawable$ConstantState
    {
        public int a;
        public e32 b;
        public AnimatorSet c;
        public ArrayList d;
        public r8 e;
        
        public c(final Context context, final c c, final Drawable$Callback callback, final Resources resources) {
            if (c != null) {
                this.a = c.a;
                final e32 b = c.b;
                int i = 0;
                if (b != null) {
                    final Drawable$ConstantState constantState = b.getConstantState();
                    Drawable drawable;
                    if (resources != null) {
                        drawable = constantState.newDrawable(resources);
                    }
                    else {
                        drawable = constantState.newDrawable();
                    }
                    this.b = (e32)drawable;
                    (this.b = (e32)this.b.mutate()).setCallback(callback);
                    this.b.setBounds(c.b.getBounds());
                    this.b.g(false);
                }
                final ArrayList d = c.d;
                if (d != null) {
                    final int size = d.size();
                    this.d = new ArrayList(size);
                    this.e = new r8(size);
                    while (i < size) {
                        final Animator animator = c.d.get(i);
                        final Animator clone = animator.clone();
                        final String s = (String)c.e.get(animator);
                        clone.setTarget(this.b.c(s));
                        this.d.add(clone);
                        this.e.put(clone, s);
                        ++i;
                    }
                    this.a();
                }
            }
        }
        
        public void a() {
            if (this.c == null) {
                this.c = new AnimatorSet();
            }
            this.c.playTogether((Collection)this.d);
        }
        
        public int getChangingConfigurations() {
            return this.a;
        }
        
        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
        
        public Drawable newDrawable(final Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }
    
    public static class d extends Drawable$ConstantState
    {
        public final Drawable$ConstantState a;
        
        public d(final Drawable$ConstantState a) {
            this.a = a;
        }
        
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }
        
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }
        
        public Drawable newDrawable() {
            final z4 z4 = new z4();
            (z4.a = this.a.newDrawable()).setCallback(z4.h);
            return z4;
        }
        
        public Drawable newDrawable(final Resources resources) {
            final z4 z4 = new z4();
            (z4.a = this.a.newDrawable(resources)).setCallback(z4.h);
            return z4;
        }
        
        public Drawable newDrawable(final Resources resources, final Resources$Theme resources$Theme) {
            final z4 z4 = new z4();
            (z4.a = this.a.newDrawable(resources, resources$Theme)).setCallback(z4.h);
            return z4;
        }
    }
}

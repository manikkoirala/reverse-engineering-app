import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Objects;
import java.lang.reflect.Type;
import java.io.StringReader;
import java.io.Reader;
import java.io.EOFException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import com.google.gson.internal.LazilyParsedNumber;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Collection;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Collections;
import com.google.gson.ToNumberPolicy;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.LongSerializationPolicy;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public final class gc0
{
    public static final r00 A;
    public static final vw1 B;
    public static final vw1 C;
    public static final String z;
    public final ThreadLocal a;
    public final ConcurrentMap b;
    public final bl c;
    public final hh0 d;
    public final List e;
    public final oy f;
    public final r00 g;
    public final Map h;
    public final boolean i;
    public final boolean j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public final boolean o;
    public final boolean p;
    public final String q;
    public final int r;
    public final int s;
    public final LongSerializationPolicy t;
    public final List u;
    public final List v;
    public final vw1 w;
    public final vw1 x;
    public final List y;
    
    static {
        A = FieldNamingPolicy.IDENTITY;
        B = ToNumberPolicy.DOUBLE;
        C = ToNumberPolicy.LAZILY_PARSED_NUMBER;
    }
    
    public gc0() {
        this(oy.g, gc0.A, Collections.emptyMap(), false, false, false, true, false, false, false, true, LongSerializationPolicy.DEFAULT, gc0.z, 2, 2, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), gc0.B, gc0.C, Collections.emptyList());
    }
    
    public gc0(final oy f, final r00 g, final Map h, final boolean i, final boolean j, final boolean k, final boolean l, final boolean m, final boolean n, final boolean o, final boolean p21, final LongSerializationPolicy t, final String q, final int r, final int s, final List u, final List v, final List list, final vw1 w, final vw1 x, final List y) {
        this.a = new ThreadLocal();
        this.b = new ConcurrentHashMap();
        this.f = f;
        this.g = g;
        this.h = h;
        final bl c = new bl(h, p21, y);
        this.c = c;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
        this.p = p21;
        this.t = t;
        this.q = q;
        this.r = r;
        this.s = s;
        this.u = u;
        this.v = v;
        this.w = w;
        this.x = x;
        this.y = y;
        final ArrayList list2 = new ArrayList();
        list2.add(kz1.W);
        list2.add(z01.e(w));
        list2.add(f);
        list2.addAll(list);
        list2.add(kz1.C);
        list2.add(kz1.m);
        list2.add(kz1.g);
        list2.add(kz1.i);
        list2.add(kz1.k);
        final hz1 o2 = o(t);
        list2.add(kz1.b(Long.TYPE, Long.class, o2));
        list2.add(kz1.b(Double.TYPE, Double.class, this.e(o)));
        list2.add(kz1.b(Float.TYPE, Float.class, this.f(o)));
        list2.add(o01.e(x));
        list2.add(kz1.o);
        list2.add(kz1.q);
        list2.add(kz1.a(AtomicLong.class, b(o2)));
        list2.add(kz1.a(AtomicLongArray.class, c(o2)));
        list2.add(kz1.s);
        list2.add(kz1.x);
        list2.add(kz1.E);
        list2.add(kz1.G);
        list2.add(kz1.a(BigDecimal.class, kz1.z));
        list2.add(kz1.a(BigInteger.class, kz1.A));
        list2.add(kz1.a(LazilyParsedNumber.class, kz1.B));
        list2.add(kz1.I);
        list2.add(kz1.K);
        list2.add(kz1.O);
        list2.add(kz1.Q);
        list2.add(kz1.U);
        list2.add(kz1.M);
        list2.add(kz1.d);
        list2.add(up.b);
        list2.add(kz1.S);
        if (fp1.a) {
            list2.add(fp1.e);
            list2.add(fp1.d);
            list2.add(fp1.f);
        }
        list2.add(v8.c);
        list2.add(kz1.b);
        list2.add(new kh(c));
        list2.add(new bn0(c, j));
        final hh0 d = new hh0(c);
        list2.add(this.d = d);
        list2.add(kz1.X);
        list2.add(new dd1(c, g, f, d, y));
        this.e = Collections.unmodifiableList((List<?>)list2);
    }
    
    public static void a(Object o, final rh0 rh0) {
        if (o != null) {
            try {
                if (rh0.o0() != JsonToken.END_DOCUMENT) {
                    o = new JsonSyntaxException("JSON document was not fully consumed.");
                    throw o;
                }
            }
            catch (final IOException ex) {
                throw new JsonIOException(ex);
            }
            catch (final MalformedJsonException ex2) {
                throw new JsonSyntaxException(ex2);
            }
        }
    }
    
    public static hz1 b(final hz1 hz1) {
        return new hz1(hz1) {
            public final hz1 a;
            
            public AtomicLong e(final rh0 rh0) {
                return new AtomicLong(((Number)this.a.b(rh0)).longValue());
            }
            
            public void f(final vh0 vh0, final AtomicLong atomicLong) {
                this.a.d(vh0, atomicLong.get());
            }
        }.a();
    }
    
    public static hz1 c(final hz1 hz1) {
        return new hz1(hz1) {
            public final hz1 a;
            
            public AtomicLongArray e(final rh0 rh0) {
                final ArrayList list = new ArrayList();
                rh0.a();
                while (rh0.k()) {
                    list.add(((Number)this.a.b(rh0)).longValue());
                }
                rh0.f();
                final int size = list.size();
                final AtomicLongArray atomicLongArray = new AtomicLongArray(size);
                for (int i = 0; i < size; ++i) {
                    atomicLongArray.set(i, (long)list.get(i));
                }
                return atomicLongArray;
            }
            
            public void f(final vh0 vh0, final AtomicLongArray atomicLongArray) {
                vh0.c();
                for (int length = atomicLongArray.length(), i = 0; i < length; ++i) {
                    this.a.d(vh0, atomicLongArray.get(i));
                }
                vh0.f();
            }
        }.a();
    }
    
    public static void d(final double d) {
        if (!Double.isNaN(d) && !Double.isInfinite(d)) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(d);
        sb.append(" is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static hz1 o(final LongSerializationPolicy longSerializationPolicy) {
        if (longSerializationPolicy == LongSerializationPolicy.DEFAULT) {
            return kz1.t;
        }
        return new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return rh0.J();
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                    return;
                }
                vh0.r0(n.toString());
            }
        };
    }
    
    public final hz1 e(final boolean b) {
        if (b) {
            return kz1.v;
        }
        return new hz1(this) {
            public final gc0 a;
            
            public Double e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return rh0.E();
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                    return;
                }
                final double doubleValue = n.doubleValue();
                gc0.d(doubleValue);
                vh0.m0(doubleValue);
            }
        };
    }
    
    public final hz1 f(final boolean b) {
        if (b) {
            return kz1.u;
        }
        return new hz1(this) {
            public final gc0 a;
            
            public Float e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return (float)rh0.E();
            }
            
            public void f(final vh0 vh0, Number value) {
                if (value == null) {
                    vh0.u();
                    return;
                }
                final float floatValue = value.floatValue();
                gc0.d(floatValue);
                if (!(value instanceof Float)) {
                    value = floatValue;
                }
                vh0.q0(value);
            }
        };
    }
    
    public Object g(final rh0 rh0, final TypeToken typeToken) {
        final boolean o = rh0.o();
        boolean b = true;
        rh0.t0(true);
        try {
            try {
                rh0.o0();
                b = false;
                final Object b2 = this.l(typeToken).b(rh0);
                rh0.t0(o);
                return b2;
            }
            finally {}
        }
        catch (final AssertionError cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("AssertionError (GSON 2.10.1): ");
            sb.append(cause.getMessage());
            throw new AssertionError(sb.toString(), cause);
        }
        catch (final IOException ex) {
            throw new JsonSyntaxException(ex);
        }
        catch (final IllegalStateException ex2) {
            throw new JsonSyntaxException(ex2);
        }
        catch (final EOFException ex3) {
            if (b) {
                rh0.t0(o);
                return null;
            }
            throw new JsonSyntaxException(ex3);
        }
        rh0.t0(o);
    }
    
    public Object h(final Reader reader, final TypeToken typeToken) {
        final rh0 p2 = this.p(reader);
        final Object g = this.g(p2, typeToken);
        a(g, p2);
        return g;
    }
    
    public Object i(final String s, final TypeToken typeToken) {
        if (s == null) {
            return null;
        }
        return this.h(new StringReader(s), typeToken);
    }
    
    public Object j(final String s, final Class clazz) {
        return d81.b(clazz).cast(this.i(s, TypeToken.get((Class<Object>)clazz)));
    }
    
    public Object k(final String s, final Type type) {
        return this.i(s, TypeToken.get(type));
    }
    
    public hz1 l(final TypeToken typeToken) {
        Objects.requireNonNull(typeToken, "type must not be null");
        final hz1 hz1 = (hz1)this.b.get(typeToken);
        if (hz1 != null) {
            return hz1;
        }
        Map value = this.a.get();
        boolean b;
        if (value == null) {
            value = new HashMap();
            this.a.set(value);
            b = true;
        }
        else {
            final hz1 hz2 = (hz1)value.get(typeToken);
            if (hz2 != null) {
                return hz2;
            }
            b = false;
        }
        try {
            final f f = new f();
            value.put(typeToken, f);
            final Iterator iterator = this.e.iterator();
            hz1 hz3 = null;
            while (iterator.hasNext()) {
                final hz1 a = ((iz1)iterator.next()).a(this, typeToken);
                if ((hz3 = a) != null) {
                    f.g(a);
                    value.put(typeToken, a);
                    hz3 = a;
                    break;
                }
            }
            if (b) {
                this.a.remove();
            }
            if (hz3 != null) {
                if (b) {
                    this.b.putAll(value);
                }
                return hz3;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("GSON (2.10.1) cannot handle ");
            sb.append(typeToken);
            throw new IllegalArgumentException(sb.toString());
        }
        finally {
            if (b) {
                this.a.remove();
            }
        }
    }
    
    public hz1 m(final Class clazz) {
        return this.l(TypeToken.get((Class<Object>)clazz));
    }
    
    public hz1 n(final iz1 iz1, final TypeToken obj) {
        iz1 d = iz1;
        if (!this.e.contains(iz1)) {
            d = this.d;
        }
        final Iterator iterator = this.e.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final iz1 iz2 = (iz1)iterator.next();
            if (n == 0) {
                if (iz2 != d) {
                    continue;
                }
                n = 1;
            }
            else {
                final hz1 a = iz2.a(this, obj);
                if (a != null) {
                    return a;
                }
                continue;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("GSON cannot serialize ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public rh0 p(final Reader reader) {
        final rh0 rh0 = new rh0(reader);
        rh0.t0(this.n);
        return rh0;
    }
    
    public vh0 q(final Writer writer) {
        if (this.k) {
            writer.write(")]}'\n");
        }
        final vh0 vh0 = new vh0(writer);
        if (this.m) {
            vh0.O("  ");
        }
        vh0.K(this.l);
        vh0.R(this.n);
        vh0.Z(this.i);
        return vh0;
    }
    
    public String r(final nh0 nh0) {
        final StringWriter stringWriter = new StringWriter();
        this.v(nh0, stringWriter);
        return stringWriter.toString();
    }
    
    public String s(final Object o) {
        if (o == null) {
            return this.r(oh0.a);
        }
        return this.t(o, o.getClass());
    }
    
    public String t(final Object o, final Type type) {
        final StringWriter stringWriter = new StringWriter();
        this.x(o, type, stringWriter);
        return stringWriter.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{serializeNulls:");
        sb.append(this.i);
        sb.append(",factories:");
        sb.append(this.e);
        sb.append(",instanceCreators:");
        sb.append(this.c);
        sb.append("}");
        return sb.toString();
    }
    
    public void u(final nh0 nh0, final vh0 vh0) {
        final boolean j = vh0.j();
        vh0.R(true);
        final boolean i = vh0.i();
        vh0.K(this.l);
        final boolean h = vh0.h();
        vh0.Z(this.i);
        try {
            try {
                er1.a(nh0, vh0);
                vh0.R(j);
                vh0.K(i);
                vh0.Z(h);
                return;
            }
            finally {}
        }
        catch (final AssertionError cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("AssertionError (GSON 2.10.1): ");
            sb.append(cause.getMessage());
            throw new AssertionError(sb.toString(), cause);
        }
        catch (final IOException ex) {
            throw new JsonIOException(ex);
        }
        vh0.R(j);
        vh0.K(i);
        vh0.Z(h);
    }
    
    public void v(final nh0 nh0, final Appendable appendable) {
        try {
            this.u(nh0, this.q(er1.b(appendable)));
        }
        catch (final IOException ex) {
            throw new JsonIOException(ex);
        }
    }
    
    public void w(final Object o, final Type type, final vh0 vh0) {
        final hz1 l = this.l(TypeToken.get(type));
        final boolean j = vh0.j();
        vh0.R(true);
        final boolean i = vh0.i();
        vh0.K(this.l);
        final boolean h = vh0.h();
        vh0.Z(this.i);
        try {
            try {
                l.d(vh0, o);
                vh0.R(j);
                vh0.K(i);
                vh0.Z(h);
                return;
            }
            finally {}
        }
        catch (final AssertionError cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("AssertionError (GSON 2.10.1): ");
            sb.append(cause.getMessage());
            throw new AssertionError(sb.toString(), cause);
        }
        catch (final IOException ex) {
            throw new JsonIOException(ex);
        }
        vh0.R(j);
        vh0.K(i);
        vh0.Z(h);
    }
    
    public void x(final Object o, final Type type, final Appendable appendable) {
        try {
            this.w(o, type, this.q(er1.b(appendable)));
        }
        catch (final IOException ex) {
            throw new JsonIOException(ex);
        }
    }
    
    public static class f extends ll1
    {
        public hz1 a;
        
        public f() {
            this.a = null;
        }
        
        @Override
        public Object b(final rh0 rh0) {
            return this.f().b(rh0);
        }
        
        @Override
        public void d(final vh0 vh0, final Object o) {
            this.f().d(vh0, o);
        }
        
        @Override
        public hz1 e() {
            return this.f();
        }
        
        public final hz1 f() {
            final hz1 a = this.a;
            if (a != null) {
                return a;
            }
            throw new IllegalStateException("Adapter for type with cyclic dependency has been used before dependency has been resolved");
        }
        
        public void g(final hz1 a) {
            if (this.a == null) {
                this.a = a;
                return;
            }
            throw new AssertionError((Object)"Delegate is already set");
        }
    }
}

import android.os.Bundle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

// 
// Decompiled by Procyon v0.6.0
// 

public class gc implements n4, m4
{
    public final hn a;
    public final int b;
    public final TimeUnit c;
    public final Object d;
    public CountDownLatch e;
    public boolean f;
    
    public gc(final hn a, final int b, final TimeUnit c) {
        this.d = new Object();
        this.f = false;
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public void a(final String str, final Bundle obj) {
        synchronized (this.d) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Logging event ");
            sb.append(str);
            sb.append(" to Firebase Analytics with params ");
            sb.append(obj);
            f.i(sb.toString());
            this.e = new CountDownLatch(1);
            this.f = false;
            this.a.a(str, obj);
            zl0.f().i("Awaiting app exception callback from Analytics...");
            try {
                if (this.e.await(this.b, this.c)) {
                    this.f = true;
                    zl0.f().i("App exception callback received from Analytics listener.");
                }
                else {
                    zl0.f().k("Timeout exceeded while awaiting app exception callback from Analytics listener.");
                }
            }
            catch (final InterruptedException ex) {
                zl0.f().d("Interrupted while awaiting app exception callback from Analytics listener.");
            }
            this.e = null;
        }
    }
    
    @Override
    public void b(final String anObject, final Bundle bundle) {
        final CountDownLatch e = this.e;
        if (e == null) {
            return;
        }
        if ("_ae".equals(anObject)) {
            e.countDown();
        }
    }
}

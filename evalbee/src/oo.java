import java.lang.reflect.Type;
import java.lang.reflect.Modifier;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.HashSet;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.net.URL;
import java.net.URI;
import android.net.Uri;
import com.google.firebase.firestore.a;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class oo
{
    public static final ConcurrentMap a;
    
    static {
        a = new ConcurrentHashMap();
    }
    
    public static Object c(final Object o) {
        return f(o);
    }
    
    public static void d(final boolean b, final String str) {
        if (b) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Hard assert failed: ");
        sb.append(str);
        throw new RuntimeException(sb.toString());
    }
    
    public static a e(final Class clazz) {
        final ConcurrentMap a = oo.a;
        a a2;
        if ((a2 = (a)a.get(clazz)) == null) {
            a2 = new a(clazz);
            a.put(clazz, a2);
        }
        return a2;
    }
    
    public static Object f(final Object o) {
        return g(o, b.d);
    }
    
    public static Object g(final Object o, b name) {
        if (name.b() > 500) {
            throw h(name, "Exceeded maximum depth of 500, which likely indicates there's an object cycle");
        }
        if (o == null) {
            return null;
        }
        if (o instanceof Number) {
            if (!(o instanceof Long) && !(o instanceof Integer) && !(o instanceof Double) && !(o instanceof Float)) {
                throw h(name, String.format("Numbers of type %s are not supported, please use an int, long, float or double", o.getClass().getSimpleName()));
            }
            return o;
        }
        else {
            if (o instanceof String) {
                return o;
            }
            if (o instanceof Boolean) {
                return o;
            }
            if (o instanceof Character) {
                throw h(name, "Characters are not supported, please use Strings");
            }
            if (o instanceof Map) {
                final HashMap hashMap = new HashMap();
                for (final Map.Entry<Object, V> entry : ((Map)o).entrySet()) {
                    final String key = entry.getKey();
                    if (!(key instanceof String)) {
                        throw h(name, "Maps with non-string keys are not supported");
                    }
                    final String s = key;
                    hashMap.put(s, g(entry.getValue(), name.a(s)));
                }
                return hashMap;
            }
            if (o instanceof Collection) {
                if (o instanceof List) {
                    final List list = (List)o;
                    final ArrayList list2 = new ArrayList(list.size());
                    for (int i = 0; i < list.size(); ++i) {
                        final Object value = list.get(i);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("[");
                        sb.append(i);
                        sb.append("]");
                        list2.add(g(value, name.a(sb.toString())));
                    }
                    return list2;
                }
                throw h(name, "Serializing Collections is not supported, please use Lists instead");
            }
            else {
                if (!o.getClass().isArray()) {
                    if (o instanceof Enum) {
                        name = (b)((Enum)o).name();
                        try {
                            return i(o.getClass().getField((String)name));
                        }
                        catch (final NoSuchFieldException ex) {
                            return name;
                        }
                    }
                    Object string = o;
                    if (!(o instanceof Date)) {
                        string = o;
                        if (!(o instanceof pw1)) {
                            string = o;
                            if (!(o instanceof ia0)) {
                                string = o;
                                if (!(o instanceof ec)) {
                                    string = o;
                                    if (!(o instanceof com.google.firebase.firestore.a)) {
                                        if (o instanceof v00) {
                                            string = o;
                                        }
                                        else {
                                            if (!(o instanceof Uri) && !(o instanceof URI) && !(o instanceof URL)) {
                                                return e(o.getClass()).k(o, name);
                                            }
                                            string = o.toString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return string;
                }
                throw h(name, "Serializing Arrays is not supported, please use Lists instead");
            }
        }
    }
    
    public static IllegalArgumentException h(final b b, String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Could not serialize object. ");
        sb.append(s);
        final String str = s = sb.toString();
        if (b.b() > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(" (found in field '");
            sb2.append(b.toString());
            sb2.append("')");
            s = sb2.toString();
        }
        return new IllegalArgumentException(s);
    }
    
    public static class a
    {
        public final Class a;
        public final Constructor b;
        public final boolean c;
        public final boolean d;
        public final Map e;
        public final Map f;
        public final Map g;
        public final Map h;
        public final HashSet i;
        public final HashSet j;
        
        public a(final Class a) {
            this.a = a;
            this.c = a.isAnnotationPresent(fw1.class);
            this.d = (a.isAnnotationPresent(fe0.class) ^ true);
            this.e = new HashMap();
            this.g = new HashMap();
            this.f = new HashMap();
            this.h = new HashMap();
            this.i = new HashSet();
            this.j = new HashSet();
            Constructor declaredConstructor;
            try {
                declaredConstructor = a.getDeclaredConstructor((Class[])new Class[0]);
                declaredConstructor.setAccessible(true);
            }
            catch (final NoSuchMethodException ex) {
                declaredConstructor = null;
            }
            this.b = declaredConstructor;
            for (final Method method : a.getMethods()) {
                if (n(method)) {
                    final String j = j(method);
                    this.b(j);
                    method.setAccessible(true);
                    if (this.f.containsKey(j)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Found conflicting getters for name ");
                        sb.append(method.getName());
                        sb.append(" on class ");
                        sb.append(a.getName());
                        throw new RuntimeException(sb.toString());
                    }
                    this.f.put(j, method);
                    this.e(method);
                }
            }
            for (final Field field : a.getFields()) {
                if (m(field)) {
                    this.b(i(field));
                    this.d(field);
                }
            }
            Class clazz = a;
            Class superclass;
            do {
                for (final Method method2 : clazz.getDeclaredMethods()) {
                    if (o(method2)) {
                        final String m = j(method2);
                        final String s = this.e.get(m.toLowerCase(Locale.US));
                        if (s != null) {
                            if (!s.equals(m)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Found setter on ");
                                sb2.append(clazz.getName());
                                sb2.append(" with invalid case-sensitive name: ");
                                sb2.append(method2.getName());
                                throw new RuntimeException(sb2.toString());
                            }
                            final Method method3 = this.g.get(m);
                            if (method3 == null) {
                                method2.setAccessible(true);
                                this.g.put(m, method2);
                                this.f(method2);
                            }
                            else if (!h(method2, method3)) {
                                if (clazz == a) {
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Class ");
                                    sb3.append(a.getName());
                                    sb3.append(" has multiple setter overloads with name ");
                                    sb3.append(method2.getName());
                                    throw new RuntimeException(sb3.toString());
                                }
                                final StringBuilder sb4 = new StringBuilder();
                                sb4.append("Found conflicting setters with name: ");
                                sb4.append(method2.getName());
                                sb4.append(" (conflicts with ");
                                sb4.append(method3.getName());
                                sb4.append(" defined on ");
                                sb4.append(method3.getDeclaringClass().getName());
                                sb4.append(")");
                                throw new RuntimeException(sb4.toString());
                            }
                        }
                    }
                }
                for (final Field field2 : clazz.getDeclaredFields()) {
                    final String i2 = i(field2);
                    if (this.e.containsKey(i2.toLowerCase(Locale.US)) && !this.h.containsKey(i2)) {
                        field2.setAccessible(true);
                        this.h.put(i2, field2);
                        this.d(field2);
                    }
                }
                superclass = clazz.getSuperclass();
                if (superclass == null) {
                    break;
                }
                clazz = superclass;
            } while (!superclass.equals(Object.class));
            if (!this.e.isEmpty()) {
                for (final String str : this.j) {
                    if (!this.g.containsKey(str)) {
                        if (this.h.containsKey(str)) {
                            continue;
                        }
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("@DocumentId is annotated on property ");
                        sb5.append(str);
                        sb5.append(" of class ");
                        sb5.append(a.getName());
                        sb5.append(" but no field or public setter was found");
                        throw new RuntimeException(sb5.toString());
                    }
                }
                return;
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("No properties to serialize found on class ");
            sb6.append(a.getName());
            throw new RuntimeException(sb6.toString());
        }
        
        public static String c(final AccessibleObject accessibleObject) {
            if (accessibleObject.isAnnotationPresent(g91.class)) {
                return accessibleObject.getAnnotation(g91.class).value();
            }
            return null;
        }
        
        public static boolean h(final Method method, final Method method2) {
            d(method.getDeclaringClass().isAssignableFrom(method2.getDeclaringClass()), "Expected override from a base class");
            d(method.getReturnType().equals(Void.TYPE), "Expected void return type");
            d(method2.getReturnType().equals(Void.TYPE), "Expected void return type");
            final Class<?>[] parameterTypes = method.getParameterTypes();
            final Class<?>[] parameterTypes2 = method2.getParameterTypes();
            final int length = parameterTypes.length;
            final boolean b = false;
            d(length == 1, "Expected exactly one parameter");
            d(parameterTypes2.length == 1, "Expected exactly one parameter");
            boolean b2 = b;
            if (method.getName().equals(method2.getName())) {
                b2 = b;
                if (parameterTypes[0].equals(parameterTypes2[0])) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        public static String i(final Field field) {
            final String c = c(field);
            String name;
            if (c != null) {
                name = c;
            }
            else {
                name = field.getName();
            }
            return name;
        }
        
        public static String j(final Method method) {
            final String c = c(method);
            String l;
            if (c != null) {
                l = c;
            }
            else {
                l = l(method.getName());
            }
            return l;
        }
        
        public static String l(final String str) {
            String s = null;
            final int n = 0;
            for (int i = 0; i < 3; ++i) {
                final String prefix = (new String[] { "get", "set", "is" })[i];
                if (str.startsWith(prefix)) {
                    s = prefix;
                }
            }
            if (s != null) {
                final char[] charArray = str.substring(s.length()).toCharArray();
                for (int n2 = n; n2 < charArray.length && Character.isUpperCase(charArray[n2]); ++n2) {
                    charArray[n2] = Character.toLowerCase(charArray[n2]);
                }
                return new String(charArray);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown Bean prefix for method: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public static boolean m(final Field field) {
            return !field.getDeclaringClass().equals(Object.class) && Modifier.isPublic(field.getModifiers()) && !Modifier.isStatic(field.getModifiers()) && !Modifier.isTransient(field.getModifiers()) && !field.isAnnotationPresent(ny.class);
        }
        
        public static boolean n(final Method method) {
            return (method.getName().startsWith("get") || method.getName().startsWith("is")) && !method.getDeclaringClass().equals(Object.class) && Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers()) && !method.getReturnType().equals(Void.TYPE) && method.getParameterTypes().length == 0 && !method.isAnnotationPresent(ny.class);
        }
        
        public static boolean o(final Method method) {
            return method.getName().startsWith("set") && !method.getDeclaringClass().equals(Object.class) && !Modifier.isStatic(method.getModifiers()) && method.getReturnType().equals(Void.TYPE) && method.getParameterTypes().length == 1 && !method.isAnnotationPresent(ny.class);
        }
        
        public final void b(final String s) {
            final Map e = this.e;
            final Locale us = Locale.US;
            final String anObject = e.put(s.toLowerCase(us), s);
            if (anObject != null && !s.equals(anObject)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Found two getters or fields with conflicting case sensitivity for property: ");
                sb.append(s.toLowerCase(us));
                throw new RuntimeException(sb.toString());
            }
        }
        
        public final void d(final Field field) {
            if (field.isAnnotationPresent(ol1.class)) {
                final Class<?> type = field.getType();
                if (type != Date.class && type != pw1.class) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Field ");
                    sb.append(field.getName());
                    sb.append(" is annotated with @ServerTimestamp but is ");
                    sb.append(type);
                    sb.append(" instead of Date or Timestamp.");
                    throw new IllegalArgumentException(sb.toString());
                }
                this.i.add(i(field));
            }
            if (field.isAnnotationPresent(bu.class)) {
                this.g("Field", "is", field.getType());
                this.j.add(i(field));
            }
        }
        
        public final void e(final Method method) {
            if (method.isAnnotationPresent(ol1.class)) {
                final Class<?> returnType = method.getReturnType();
                if (returnType != Date.class && returnType != pw1.class) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Method ");
                    sb.append(method.getName());
                    sb.append(" is annotated with @ServerTimestamp but returns ");
                    sb.append(returnType);
                    sb.append(" instead of Date or Timestamp.");
                    throw new IllegalArgumentException(sb.toString());
                }
                this.i.add(j(method));
            }
            if (method.isAnnotationPresent(bu.class)) {
                this.g("Method", "returns", method.getReturnType());
                this.j.add(j(method));
            }
        }
        
        public final void f(final Method method) {
            if (!method.isAnnotationPresent(ol1.class)) {
                if (method.isAnnotationPresent(bu.class)) {
                    this.g("Method", "accepts", method.getParameterTypes()[0]);
                    this.j.add(j(method));
                }
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Method ");
            sb.append(method.getName());
            sb.append(" is annotated with @ServerTimestamp but should not be. @ServerTimestamp can only be applied to fields and getters, not setters.");
            throw new IllegalArgumentException(sb.toString());
        }
        
        public final void g(final String str, final String str2, final Type obj) {
            if (obj != String.class && obj != com.google.firebase.firestore.a.class) {
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" is annotated with @DocumentId but ");
                sb.append(str2);
                sb.append(" ");
                sb.append(obj);
                sb.append(" instead of String or DocumentReference.");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        
        public Map k(final Object obj, final b b) {
            if (this.a.isAssignableFrom(obj.getClass())) {
                final HashMap hashMap = new HashMap();
                for (final String str : this.e.values()) {
                    if (this.j.contains(str)) {
                        continue;
                    }
                    Label_0141: {
                        if (this.f.containsKey(str)) {
                            final Object o = o5.a(this.f.get(str), obj, new Object[0]);
                            break Label_0141;
                        }
                        final Field field = this.h.get(str);
                        if (field == null) {
                            break Label_0141;
                        }
                        try {
                            final Object o = field.get(obj);
                            Object o2;
                            if (this.i.contains(str) && o == null) {
                                o2 = v00.c();
                            }
                            else {
                                o2 = g(o, b.a(str));
                            }
                            hashMap.put(str, o2);
                            continue;
                        }
                        catch (final IllegalAccessException cause) {
                            throw new RuntimeException(cause);
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Bean property without field or getter: ");
                    sb.append(str);
                    throw new IllegalStateException(sb.toString());
                }
                return hashMap;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Can't serialize object of class ");
            sb2.append(obj.getClass());
            sb2.append(" with BeanMapper for class ");
            sb2.append(this.a);
            throw new IllegalArgumentException(sb2.toString());
        }
    }
    
    public static class b
    {
        public static final b d;
        public final int a;
        public final b b;
        public final String c;
        
        static {
            d = new b(null, null, 0);
        }
        
        public b(final b b, final String c, final int a) {
            this.b = b;
            this.c = c;
            this.a = a;
        }
        
        public b a(final String s) {
            return new b(this, s, this.a + 1);
        }
        
        public int b() {
            return this.a;
        }
        
        @Override
        public String toString() {
            final int a = this.a;
            if (a == 0) {
                return "";
            }
            if (a == 1) {
                return this.c;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(this.b.toString());
            sb.append(".");
            sb.append(this.c);
            return sb.toString();
        }
    }
}

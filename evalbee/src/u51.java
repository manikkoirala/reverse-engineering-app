import java.util.concurrent.Executors;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import com.google.firebase.messaging.threads.ThreadPriority;
import java.util.concurrent.ThreadFactory;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class u51
{
    public static final uy a;
    public static volatile uy b;
    
    static {
        u51.b = (a = new b(null));
    }
    
    public static uy a() {
        return u51.b;
    }
    
    public static class b implements uy
    {
        @Override
        public ExecutorService a(final ThreadFactory threadFactory, final ThreadPriority threadPriority) {
            return this.b(1, threadFactory, threadPriority);
        }
        
        public ExecutorService b(final int n, final ThreadFactory threadFactory, final ThreadPriority threadPriority) {
            final ThreadPoolExecutor executor = new ThreadPoolExecutor(n, n, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), threadFactory);
            executor.allowCoreThreadTimeOut(true);
            return Executors.unconfigurableExecutorService(executor);
        }
    }
}

import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

// 
// Decompiled by Procyon v0.6.0
// 

public final class gn implements dn
{
    public static final gy0 c;
    public final jr a;
    public final AtomicReference b;
    
    static {
        c = new b(null);
    }
    
    public gn(final jr a) {
        this.b = new AtomicReference(null);
        (this.a = a).a((jr.a)new en(this));
    }
    
    @Override
    public void a(final String str, final String s, final long n, final zp1 zp1) {
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Deferring native open session: ");
        sb.append(str);
        f.i(sb.toString());
        this.a.a((jr.a)new fn(str, s, n, zp1));
    }
    
    @Override
    public gy0 b(final String s) {
        final dn dn = this.b.get();
        gy0 gy0;
        if (dn == null) {
            gy0 = gn.c;
        }
        else {
            gy0 = dn.b(s);
        }
        return gy0;
    }
    
    @Override
    public boolean c() {
        final dn dn = this.b.get();
        return dn != null && dn.c();
    }
    
    @Override
    public boolean d(final String s) {
        final dn dn = this.b.get();
        return dn != null && dn.d(s);
    }
    
    public static final class b implements gy0
    {
        @Override
        public File a() {
            return null;
        }
        
        @Override
        public CrashlyticsReport.a b() {
            return null;
        }
        
        @Override
        public File c() {
            return null;
        }
        
        @Override
        public File d() {
            return null;
        }
        
        @Override
        public File e() {
            return null;
        }
        
        @Override
        public File f() {
            return null;
        }
        
        @Override
        public File g() {
            return null;
        }
    }
}

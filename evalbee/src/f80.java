import android.view.View;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class f80
{
    public Fragment b(final Context context, final String s, final Bundle bundle) {
        return Fragment.instantiate(context, s, bundle);
    }
    
    public abstract View c(final int p0);
    
    public abstract boolean d();
}

import android.database.sqlite.SQLiteClosable;
import android.database.sqlite.SQLiteProgram;

// 
// Decompiled by Procyon v0.6.0
// 

public class s80 implements us1
{
    public final SQLiteProgram a;
    
    public s80(final SQLiteProgram a) {
        fg0.e((Object)a, "delegate");
        this.a = a;
    }
    
    @Override
    public void A(final int n, final long n2) {
        this.a.bindLong(n, n2);
    }
    
    @Override
    public void D(final int n, final byte[] array) {
        fg0.e((Object)array, "value");
        this.a.bindBlob(n, array);
    }
    
    @Override
    public void I(final int n) {
        this.a.bindNull(n);
    }
    
    @Override
    public void P(final int n, final double n2) {
        this.a.bindDouble(n, n2);
    }
    
    @Override
    public void close() {
        ((SQLiteClosable)this.a).close();
    }
    
    @Override
    public void y(final int n, final String s) {
        fg0.e((Object)s, "value");
        this.a.bindString(n, s);
    }
}

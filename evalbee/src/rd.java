import java.util.ArrayDeque;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Queue;
import java.io.OutputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rd
{
    public static final OutputStream a;
    
    static {
        a = new OutputStream() {
            @Override
            public String toString() {
                return "ByteStreams.nullOutputStream()";
            }
            
            @Override
            public void write(final int n) {
            }
            
            @Override
            public void write(final byte[] array) {
                i71.r(array);
            }
            
            @Override
            public void write(final byte[] array, final int n, final int n2) {
                i71.r(array);
                i71.w(n, n2 + n, array.length);
            }
        };
    }
    
    public static byte[] a(final Queue queue, final int newLength) {
        if (queue.isEmpty()) {
            return new byte[0];
        }
        final byte[] original = queue.remove();
        if (original.length == newLength) {
            return original;
        }
        int i = newLength - original.length;
        final byte[] copy = Arrays.copyOf(original, newLength);
        while (i > 0) {
            final byte[] array = queue.remove();
            final int min = Math.min(i, array.length);
            System.arraycopy(array, 0, copy, newLength - i, min);
            i -= min;
        }
        return copy;
    }
    
    public static long b(final InputStream inputStream, final OutputStream outputStream) {
        i71.r(inputStream);
        i71.r(outputStream);
        final byte[] c = c();
        long n = 0L;
        while (true) {
            final int read = inputStream.read(c);
            if (read == -1) {
                break;
            }
            outputStream.write(c, 0, read);
            n += read;
        }
        return n;
    }
    
    public static byte[] c() {
        return new byte[8192];
    }
    
    public static byte[] d(final InputStream inputStream) {
        i71.r(inputStream);
        return e(inputStream, new ArrayDeque(20), 0);
    }
    
    public static byte[] e(final InputStream inputStream, final Queue queue, int h) {
        final int min = Math.min(8192, Math.max(128, Integer.highestOneBit(h) * 2));
        int i = h;
        h = min;
        while (i < 2147483639) {
            final int min2 = Math.min(h, 2147483639 - i);
            final byte[] b = new byte[min2];
            queue.add(b);
            int read;
            for (int j = 0; j < min2; j += read, i += read) {
                read = inputStream.read(b, j, min2 - j);
                if (read == -1) {
                    return a(queue, i);
                }
            }
            int n;
            if (h < 4096) {
                n = 4;
            }
            else {
                n = 2;
            }
            h = tf0.h(h, n);
        }
        if (inputStream.read() == -1) {
            return a(queue, 2147483639);
        }
        throw new OutOfMemoryError("input is too large to fit in a byte array");
    }
}

import android.graphics.PorterDuff$Mode;
import android.graphics.Region;
import android.graphics.Rect;
import android.content.res.Resources$Theme;
import android.graphics.drawable.Drawable;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d32 extends Drawable
{
    public Drawable a;
    
    public void applyTheme(final Resources$Theme resources$Theme) {
        final Drawable a = this.a;
        if (a != null) {
            wu.a(a, resources$Theme);
        }
    }
    
    public void clearColorFilter() {
        final Drawable a = this.a;
        if (a != null) {
            a.clearColorFilter();
            return;
        }
        super.clearColorFilter();
    }
    
    public Drawable getCurrent() {
        final Drawable a = this.a;
        if (a != null) {
            return a.getCurrent();
        }
        return super.getCurrent();
    }
    
    public int getMinimumHeight() {
        final Drawable a = this.a;
        if (a != null) {
            return a.getMinimumHeight();
        }
        return super.getMinimumHeight();
    }
    
    public int getMinimumWidth() {
        final Drawable a = this.a;
        if (a != null) {
            return a.getMinimumWidth();
        }
        return super.getMinimumWidth();
    }
    
    public boolean getPadding(final Rect rect) {
        final Drawable a = this.a;
        if (a != null) {
            return a.getPadding(rect);
        }
        return super.getPadding(rect);
    }
    
    public int[] getState() {
        final Drawable a = this.a;
        if (a != null) {
            return a.getState();
        }
        return super.getState();
    }
    
    public Region getTransparentRegion() {
        final Drawable a = this.a;
        if (a != null) {
            return a.getTransparentRegion();
        }
        return super.getTransparentRegion();
    }
    
    public void jumpToCurrentState() {
        final Drawable a = this.a;
        if (a != null) {
            wu.i(a);
        }
    }
    
    public boolean onLevelChange(final int level) {
        final Drawable a = this.a;
        if (a != null) {
            return a.setLevel(level);
        }
        return super.onLevelChange(level);
    }
    
    public void setChangingConfigurations(final int n) {
        final Drawable a = this.a;
        if (a != null) {
            a.setChangingConfigurations(n);
            return;
        }
        super.setChangingConfigurations(n);
    }
    
    public void setColorFilter(final int n, final PorterDuff$Mode porterDuff$Mode) {
        final Drawable a = this.a;
        if (a != null) {
            a.setColorFilter(n, porterDuff$Mode);
            return;
        }
        super.setColorFilter(n, porterDuff$Mode);
    }
    
    public void setFilterBitmap(final boolean filterBitmap) {
        final Drawable a = this.a;
        if (a != null) {
            a.setFilterBitmap(filterBitmap);
        }
    }
    
    public void setHotspot(final float n, final float n2) {
        final Drawable a = this.a;
        if (a != null) {
            wu.k(a, n, n2);
        }
    }
    
    public void setHotspotBounds(final int n, final int n2, final int n3, final int n4) {
        final Drawable a = this.a;
        if (a != null) {
            wu.l(a, n, n2, n3, n4);
        }
    }
    
    public boolean setState(final int[] array) {
        final Drawable a = this.a;
        if (a != null) {
            return a.setState(array);
        }
        return super.setState(array);
    }
}

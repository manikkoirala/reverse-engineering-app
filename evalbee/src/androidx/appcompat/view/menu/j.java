// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

public interface j
{
    void initialize(final e p0);
    
    public interface a
    {
        g getItemData();
        
        void initialize(final g p0, final int p1);
        
        boolean prefersCondensedTitle();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.widget.Adapter;
import android.view.KeyEvent$DispatcherState;
import android.view.Window;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.content.DialogInterface;
import android.view.WindowManager$LayoutParams;
import android.view.View;
import android.os.IBinder;
import androidx.appcompat.app.a;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnClickListener;
import android.content.DialogInterface$OnKeyListener;

public class f implements DialogInterface$OnKeyListener, DialogInterface$OnClickListener, DialogInterface$OnDismissListener, a
{
    public e a;
    public androidx.appcompat.app.a b;
    public c c;
    public a d;
    
    public f(final e a) {
        this.a = a;
    }
    
    public boolean a(final e e) {
        final a d = this.d;
        return d != null && d.a(e);
    }
    
    public void b() {
        final androidx.appcompat.app.a b = this.b;
        if (b != null) {
            b.dismiss();
        }
    }
    
    public void c(final IBinder token) {
        final e a = this.a;
        final androidx.appcompat.app.a.a a2 = new androidx.appcompat.app.a.a(a.getContext());
        (this.c = new c(a2.getContext(), ob1.j)).setCallback(this);
        this.a.addMenuPresenter(this.c);
        a2.setAdapter(this.c.a(), (DialogInterface$OnClickListener)this);
        final View headerView = a.getHeaderView();
        if (headerView != null) {
            a2.setCustomTitle(headerView);
        }
        else {
            a2.setIcon(a.getHeaderIcon()).setTitle(a.getHeaderTitle());
        }
        a2.setOnKeyListener((DialogInterface$OnKeyListener)this);
        (this.b = a2.create()).setOnDismissListener((DialogInterface$OnDismissListener)this);
        final WindowManager$LayoutParams attributes = this.b.getWindow().getAttributes();
        attributes.type = 1003;
        if (token != null) {
            attributes.token = token;
        }
        attributes.flags |= 0x20000;
        this.b.show();
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        this.a.performItemAction((MenuItem)((Adapter)this.c.a()).getItem(n), 0);
    }
    
    public void onCloseMenu(final e e, final boolean b) {
        if (b || e == this.a) {
            this.b();
        }
        final a d = this.d;
        if (d != null) {
            d.onCloseMenu(e, b);
        }
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        this.c.onCloseMenu(this.a, true);
    }
    
    public boolean onKey(final DialogInterface dialogInterface, final int n, final KeyEvent keyEvent) {
        if (n == 82 || n == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                final Window window = this.b.getWindow();
                if (window != null) {
                    final View decorView = window.getDecorView();
                    if (decorView != null) {
                        final KeyEvent$DispatcherState keyDispatcherState = decorView.getKeyDispatcherState();
                        if (keyDispatcherState != null) {
                            keyDispatcherState.startTracking(keyEvent, (Object)this);
                            return true;
                        }
                    }
                }
            }
            else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled()) {
                final Window window2 = this.b.getWindow();
                if (window2 != null) {
                    final View decorView2 = window2.getDecorView();
                    if (decorView2 != null) {
                        final KeyEvent$DispatcherState keyDispatcherState2 = decorView2.getKeyDispatcherState();
                        if (keyDispatcherState2 != null && keyDispatcherState2.isTracking(keyEvent)) {
                            this.a.close(true);
                            dialogInterface.dismiss();
                            return true;
                        }
                    }
                }
            }
        }
        return this.a.performShortcut(n, keyEvent, 0);
    }
}

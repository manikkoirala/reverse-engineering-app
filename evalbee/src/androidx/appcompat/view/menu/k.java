// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.widget.TextView;
import android.widget.FrameLayout;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.AdapterView$OnItemClickListener;
import android.view.Gravity;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.widget.ListView;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver;
import android.view.View;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.content.Context;
import android.view.View$OnKeyListener;
import android.widget.PopupWindow$OnDismissListener;

public final class k extends pv0 implements PopupWindow$OnDismissListener, View$OnKeyListener
{
    public static final int z;
    public final Context b;
    public final e c;
    public final d d;
    public final boolean e;
    public final int f;
    public final int g;
    public final int h;
    public final qv0 i;
    public final ViewTreeObserver$OnGlobalLayoutListener j;
    public final View$OnAttachStateChangeListener k;
    public PopupWindow$OnDismissListener l;
    public View m;
    public View n;
    public a p;
    public ViewTreeObserver q;
    public boolean t;
    public boolean v;
    public int w;
    public int x;
    public boolean y;
    
    static {
        z = ob1.m;
    }
    
    public k(final Context b, final e c, final View m, final int g, final int h, final boolean e) {
        this.j = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
            public final k a;
            
            public void onGlobalLayout() {
                if (this.a.a() && !this.a.i.y()) {
                    final View n = this.a.n;
                    if (n != null && n.isShown()) {
                        this.a.i.show();
                    }
                    else {
                        this.a.dismiss();
                    }
                }
            }
        };
        this.k = (View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this) {
            public final k a;
            
            public void onViewAttachedToWindow(final View view) {
            }
            
            public void onViewDetachedFromWindow(final View view) {
                final ViewTreeObserver q = this.a.q;
                if (q != null) {
                    if (!q.isAlive()) {
                        this.a.q = view.getViewTreeObserver();
                    }
                    final k a = this.a;
                    a.q.removeGlobalOnLayoutListener(a.j);
                }
                view.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            }
        };
        this.x = 0;
        this.b = b;
        this.c = c;
        this.e = e;
        this.d = new d(c, LayoutInflater.from(b), e, androidx.appcompat.view.menu.k.z);
        this.g = g;
        this.h = h;
        final Resources resources = b.getResources();
        this.f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(ab1.b));
        this.m = m;
        this.i = new qv0(b, null, g, h);
        c.addMenuPresenter(this, b);
    }
    
    public boolean a() {
        return !this.t && this.i.a();
    }
    
    @Override
    public void b(final e e) {
    }
    
    public void dismiss() {
        if (this.a()) {
            this.i.dismiss();
        }
    }
    
    @Override
    public void f(final View m) {
        this.m = m;
    }
    
    public boolean flagActionItems() {
        return false;
    }
    
    public ListView h() {
        return this.i.h();
    }
    
    @Override
    public void i(final boolean b) {
        this.d.d(b);
    }
    
    @Override
    public void j(final int x) {
        this.x = x;
    }
    
    @Override
    public void k(final int n) {
        this.i.j(n);
    }
    
    @Override
    public void l(final PopupWindow$OnDismissListener l) {
        this.l = l;
    }
    
    @Override
    public void m(final boolean y) {
        this.y = y;
    }
    
    @Override
    public void n(final int n) {
        this.i.c(n);
    }
    
    public void onCloseMenu(final e e, final boolean b) {
        if (e != this.c) {
            return;
        }
        this.dismiss();
        final a p2 = this.p;
        if (p2 != null) {
            p2.onCloseMenu(e, b);
        }
    }
    
    public void onDismiss() {
        this.t = true;
        this.c.close();
        final ViewTreeObserver q = this.q;
        if (q != null) {
            if (!q.isAlive()) {
                this.q = this.n.getViewTreeObserver();
            }
            this.q.removeGlobalOnLayoutListener(this.j);
            this.q = null;
        }
        this.n.removeOnAttachStateChangeListener(this.k);
        final PopupWindow$OnDismissListener l = this.l;
        if (l != null) {
            l.onDismiss();
        }
    }
    
    public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1 && n == 82) {
            this.dismiss();
            return true;
        }
        return false;
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
    }
    
    public Parcelable onSaveInstanceState() {
        return null;
    }
    
    public boolean onSubMenuSelected(final l l) {
        if (l.hasVisibleItems()) {
            final h h = new h(this.b, l, this.n, this.e, this.g, this.h);
            h.j(this.p);
            h.g(pv0.o(l));
            h.i(this.l);
            this.l = null;
            this.c.close(false);
            final int i = this.i.i();
            final int f = this.i.f();
            int n = i;
            if ((Gravity.getAbsoluteGravity(this.x, o32.B(this.m)) & 0x7) == 0x5) {
                n = i + this.m.getWidth();
            }
            if (h.n(n, f)) {
                final a p = this.p;
                if (p != null) {
                    p.a(l);
                }
                return true;
            }
        }
        return false;
    }
    
    public final boolean q() {
        if (this.a()) {
            return true;
        }
        if (!this.t) {
            final View m = this.m;
            if (m != null) {
                this.n = m;
                this.i.H((PopupWindow$OnDismissListener)this);
                this.i.I((AdapterView$OnItemClickListener)this);
                this.i.G(true);
                final View n = this.n;
                final boolean b = this.q == null;
                final ViewTreeObserver viewTreeObserver = n.getViewTreeObserver();
                this.q = viewTreeObserver;
                if (b) {
                    viewTreeObserver.addOnGlobalLayoutListener(this.j);
                }
                n.addOnAttachStateChangeListener(this.k);
                this.i.A(n);
                this.i.D(this.x);
                if (!this.v) {
                    this.w = pv0.e((ListAdapter)this.d, null, this.b, this.f);
                    this.v = true;
                }
                this.i.C(this.w);
                this.i.F(2);
                this.i.E(this.d());
                this.i.show();
                final ListView h = this.i.h();
                ((View)h).setOnKeyListener((View$OnKeyListener)this);
                if (this.y && this.c.getHeaderTitle() != null) {
                    final FrameLayout frameLayout = (FrameLayout)LayoutInflater.from(this.b).inflate(ob1.l, (ViewGroup)h, false);
                    final TextView textView = (TextView)((View)frameLayout).findViewById(16908310);
                    if (textView != null) {
                        textView.setText(this.c.getHeaderTitle());
                    }
                    ((View)frameLayout).setEnabled(false);
                    h.addHeaderView((View)frameLayout, (Object)null, false);
                }
                this.i.m((ListAdapter)this.d);
                this.i.show();
                return true;
            }
        }
        return false;
    }
    
    public void setCallback(final a p) {
        this.p = p;
    }
    
    public void show() {
        if (this.q()) {
            return;
        }
        throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
    }
    
    public void updateMenuView(final boolean b) {
        this.v = false;
        final d d = this.d;
        if (d != null) {
            d.notifyDataSetChanged();
        }
    }
}

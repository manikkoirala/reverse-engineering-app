// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.os.BaseBundle;
import android.view.Menu;
import android.view.ViewConfiguration;
import java.util.Collection;
import android.view.KeyCharacterMap$KeyData;
import android.view.KeyEvent;
import android.util.SparseArray;
import android.os.Parcelable;
import android.os.Bundle;
import java.util.Iterator;
import android.view.SubMenu;
import android.content.pm.ActivityInfo;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.ComponentName;
import android.view.MenuItem;
import android.content.res.Resources;
import java.lang.ref.WeakReference;
import java.util.concurrent.CopyOnWriteArrayList;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu$ContextMenuInfo;
import android.content.Context;
import java.util.ArrayList;

public class e implements ks1
{
    private static final String ACTION_VIEW_STATES_KEY = "android:menu:actionviewstates";
    private static final String EXPANDED_ACTION_VIEW_ID = "android:menu:expandedactionview";
    private static final String PRESENTER_KEY = "android:menu:presenters";
    private static final String TAG = "MenuBuilder";
    private static final int[] sCategoryToOrder;
    private ArrayList<g> mActionItems;
    private a mCallback;
    private final Context mContext;
    private ContextMenu$ContextMenuInfo mCurrentMenuInfo;
    private int mDefaultShowAsAction;
    private g mExpandedItem;
    private boolean mGroupDividerEnabled;
    Drawable mHeaderIcon;
    CharSequence mHeaderTitle;
    View mHeaderView;
    private boolean mIsActionItemsStale;
    private boolean mIsClosing;
    private boolean mIsVisibleItemsStale;
    private ArrayList<g> mItems;
    private boolean mItemsChangedWhileDispatchPrevented;
    private ArrayList<g> mNonActionItems;
    private boolean mOptionalIconsVisible;
    private boolean mOverrideVisibleItems;
    private CopyOnWriteArrayList<WeakReference<i>> mPresenters;
    private boolean mPreventDispatchingItemsChanged;
    private boolean mQwertyMode;
    private final Resources mResources;
    private boolean mShortcutsVisible;
    private boolean mStructureChangedWhileDispatchPrevented;
    private ArrayList<g> mTempShortcutItemList;
    private ArrayList<g> mVisibleItems;
    
    static {
        sCategoryToOrder = new int[] { 1, 4, 5, 3, 2, 0 };
    }
    
    public e(final Context mContext) {
        this.mDefaultShowAsAction = 0;
        this.mPreventDispatchingItemsChanged = false;
        this.mItemsChangedWhileDispatchPrevented = false;
        this.mStructureChangedWhileDispatchPrevented = false;
        this.mOptionalIconsVisible = false;
        this.mIsClosing = false;
        this.mTempShortcutItemList = new ArrayList<g>();
        this.mPresenters = new CopyOnWriteArrayList<WeakReference<i>>();
        this.mGroupDividerEnabled = false;
        this.mContext = mContext;
        this.mResources = mContext.getResources();
        this.mItems = new ArrayList<g>();
        this.mVisibleItems = new ArrayList<g>();
        this.mIsVisibleItemsStale = true;
        this.mActionItems = new ArrayList<g>();
        this.mNonActionItems = new ArrayList<g>();
        this.j(this.mIsActionItemsStale = true);
    }
    
    public static int f(final ArrayList list, final int n) {
        for (int i = list.size() - 1; i >= 0; --i) {
            if (((g)list.get(i)).f() <= n) {
                return i + 1;
            }
        }
        return 0;
    }
    
    public static int g(final int n) {
        final int n2 = (0xFFFF0000 & n) >> 16;
        if (n2 >= 0) {
            final int[] sCategoryToOrder = e.sCategoryToOrder;
            if (n2 < sCategoryToOrder.length) {
                return (n & 0xFFFF) | sCategoryToOrder[n2] << 16;
            }
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }
    
    public final g a(final int n, final int n2, final int n3, final int n4, final CharSequence charSequence, final int n5) {
        return new g(this, n, n2, n3, n4, charSequence, n5);
    }
    
    public MenuItem add(final int n) {
        return this.addInternal(0, 0, 0, this.mResources.getString(n));
    }
    
    public MenuItem add(final int n, final int n2, final int n3, final int n4) {
        return this.addInternal(n, n2, n3, this.mResources.getString(n4));
    }
    
    public MenuItem add(final int n, final int n2, final int n3, final CharSequence charSequence) {
        return this.addInternal(n, n2, n3, charSequence);
    }
    
    public MenuItem add(final CharSequence charSequence) {
        return this.addInternal(0, 0, 0, charSequence);
    }
    
    public int addIntentOptions(final int n, final int n2, final int n3, final ComponentName componentName, final Intent[] array, final Intent intent, int n4, final MenuItem[] array2) {
        final PackageManager packageManager = this.mContext.getPackageManager();
        final int n5 = 0;
        final List queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, array, intent, 0);
        int size;
        if (queryIntentActivityOptions != null) {
            size = queryIntentActivityOptions.size();
        }
        else {
            size = 0;
        }
        int i = n5;
        if ((n4 & 0x1) == 0x0) {
            this.removeGroup(n);
            i = n5;
        }
        while (i < size) {
            final ResolveInfo resolveInfo = queryIntentActivityOptions.get(i);
            n4 = resolveInfo.specificIndex;
            Intent intent2;
            if (n4 < 0) {
                intent2 = intent;
            }
            else {
                intent2 = array[n4];
            }
            final Intent intent3 = new Intent(intent2);
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            intent3.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
            final MenuItem setIntent = this.add(n, n2, n3, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent3);
            if (array2 != null) {
                n4 = resolveInfo.specificIndex;
                if (n4 >= 0) {
                    array2[n4] = setIntent;
                }
            }
            ++i;
        }
        return size;
    }
    
    public MenuItem addInternal(final int n, final int n2, final int n3, final CharSequence charSequence) {
        final int g = g(n3);
        final g a = this.a(n, n2, n3, g, charSequence, this.mDefaultShowAsAction);
        final ContextMenu$ContextMenuInfo mCurrentMenuInfo = this.mCurrentMenuInfo;
        if (mCurrentMenuInfo != null) {
            a.v(mCurrentMenuInfo);
        }
        final ArrayList<g> mItems = this.mItems;
        mItems.add(f(mItems, g), a);
        this.onItemsChanged(true);
        return (MenuItem)a;
    }
    
    public void addMenuPresenter(final i i) {
        this.addMenuPresenter(i, this.mContext);
    }
    
    public void addMenuPresenter(final i referent, final Context context) {
        this.mPresenters.add(new WeakReference<i>(referent));
        referent.initForMenu(context, this);
        this.mIsActionItemsStale = true;
    }
    
    public SubMenu addSubMenu(final int n) {
        return this.addSubMenu(0, 0, 0, this.mResources.getString(n));
    }
    
    public SubMenu addSubMenu(final int n, final int n2, final int n3, final int n4) {
        return this.addSubMenu(n, n2, n3, this.mResources.getString(n4));
    }
    
    public SubMenu addSubMenu(final int n, final int n2, final int n3, final CharSequence charSequence) {
        final g g = (g)this.addInternal(n, n2, n3, charSequence);
        final l l = new l(this.mContext, this, g);
        g.x(l);
        return (SubMenu)l;
    }
    
    public SubMenu addSubMenu(final CharSequence charSequence) {
        return this.addSubMenu(0, 0, 0, charSequence);
    }
    
    public final void b(final boolean b) {
        if (this.mPresenters.isEmpty()) {
            return;
        }
        this.stopDispatchingItemsChanged();
        for (final WeakReference o : this.mPresenters) {
            final i i = (i)o.get();
            if (i == null) {
                this.mPresenters.remove(o);
            }
            else {
                i.updateMenuView(b);
            }
        }
        this.startDispatchingItemsChanged();
    }
    
    public final void c(final Bundle bundle) {
        final SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null) {
            if (!this.mPresenters.isEmpty()) {
                for (final WeakReference o : this.mPresenters) {
                    final i i = (i)o.get();
                    if (i == null) {
                        this.mPresenters.remove(o);
                    }
                    else {
                        final int id = i.getId();
                        if (id <= 0) {
                            continue;
                        }
                        final Parcelable parcelable = (Parcelable)sparseParcelableArray.get(id);
                        if (parcelable == null) {
                            continue;
                        }
                        i.onRestoreInstanceState(parcelable);
                    }
                }
            }
        }
    }
    
    public void changeMenuMode() {
        final a mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.onMenuModeChange(this);
        }
    }
    
    public void clear() {
        final g mExpandedItem = this.mExpandedItem;
        if (mExpandedItem != null) {
            this.collapseItemActionView(mExpandedItem);
        }
        this.mItems.clear();
        this.onItemsChanged(true);
    }
    
    public void clearAll() {
        this.mPreventDispatchingItemsChanged = true;
        this.clear();
        this.clearHeader();
        this.mPresenters.clear();
        this.mPreventDispatchingItemsChanged = false;
        this.mItemsChangedWhileDispatchPrevented = false;
        this.mStructureChangedWhileDispatchPrevented = false;
        this.onItemsChanged(true);
    }
    
    public void clearHeader() {
        this.mHeaderIcon = null;
        this.mHeaderTitle = null;
        this.mHeaderView = null;
        this.onItemsChanged(false);
    }
    
    public void close() {
        this.close(true);
    }
    
    public final void close(final boolean b) {
        if (this.mIsClosing) {
            return;
        }
        this.mIsClosing = true;
        for (final WeakReference o : this.mPresenters) {
            final i i = (i)o.get();
            if (i == null) {
                this.mPresenters.remove(o);
            }
            else {
                i.onCloseMenu(this, b);
            }
        }
        this.mIsClosing = false;
    }
    
    public boolean collapseItemActionView(final g g) {
        final boolean empty = this.mPresenters.isEmpty();
        final boolean b = false;
        final int n = 0;
        boolean b2 = b;
        if (!empty) {
            if (this.mExpandedItem != g) {
                b2 = b;
            }
            else {
                this.stopDispatchingItemsChanged();
                final Iterator<WeakReference<i>> iterator = this.mPresenters.iterator();
                boolean collapseItemActionView = n != 0;
                boolean b3;
                while (true) {
                    b3 = collapseItemActionView;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final WeakReference o = iterator.next();
                    final i i = (i)o.get();
                    if (i == null) {
                        this.mPresenters.remove(o);
                    }
                    else {
                        b3 = (collapseItemActionView = i.collapseItemActionView(this, g));
                        if (b3) {
                            break;
                        }
                        continue;
                    }
                }
                this.startDispatchingItemsChanged();
                b2 = b3;
                if (b3) {
                    this.mExpandedItem = null;
                    b2 = b3;
                }
            }
        }
        return b2;
    }
    
    public final void d(final Bundle bundle) {
        if (this.mPresenters.isEmpty()) {
            return;
        }
        final SparseArray sparseArray = new SparseArray();
        for (final WeakReference o : this.mPresenters) {
            final i i = (i)o.get();
            if (i == null) {
                this.mPresenters.remove(o);
            }
            else {
                final int id = i.getId();
                if (id <= 0) {
                    continue;
                }
                final Parcelable onSaveInstanceState = i.onSaveInstanceState();
                if (onSaveInstanceState == null) {
                    continue;
                }
                sparseArray.put(id, (Object)onSaveInstanceState);
            }
        }
        bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
    }
    
    public boolean dispatchMenuItemSelected(final e e, final MenuItem menuItem) {
        final a mCallback = this.mCallback;
        return mCallback != null && mCallback.onMenuItemSelected(e, menuItem);
    }
    
    public final boolean e(final l l, final i i) {
        final boolean empty = this.mPresenters.isEmpty();
        boolean b = false;
        if (empty) {
            return false;
        }
        if (i != null) {
            b = i.onSubMenuSelected(l);
        }
        for (final WeakReference o : this.mPresenters) {
            final i j = (i)o.get();
            if (j == null) {
                this.mPresenters.remove(o);
            }
            else {
                if (b) {
                    continue;
                }
                b = j.onSubMenuSelected(l);
            }
        }
        return b;
    }
    
    public boolean expandItemActionView(final g mExpandedItem) {
        final boolean empty = this.mPresenters.isEmpty();
        boolean expandItemActionView = false;
        if (empty) {
            return false;
        }
        this.stopDispatchingItemsChanged();
        final Iterator<WeakReference<i>> iterator = this.mPresenters.iterator();
        boolean b;
        while (true) {
            b = expandItemActionView;
            if (!iterator.hasNext()) {
                break;
            }
            final WeakReference o = iterator.next();
            final i i = (i)o.get();
            if (i == null) {
                this.mPresenters.remove(o);
            }
            else {
                b = (expandItemActionView = i.expandItemActionView(this, mExpandedItem));
                if (b) {
                    break;
                }
                continue;
            }
        }
        this.startDispatchingItemsChanged();
        if (b) {
            this.mExpandedItem = mExpandedItem;
        }
        return b;
    }
    
    public int findGroupIndex(final int n) {
        return this.findGroupIndex(n, 0);
    }
    
    public int findGroupIndex(final int n, final int n2) {
        final int size = this.size();
        int i = n2;
        if (n2 < 0) {
            i = 0;
        }
        while (i < size) {
            if (this.mItems.get(i).getGroupId() == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public MenuItem findItem(final int n) {
        for (int size = this.size(), i = 0; i < size; ++i) {
            final g g = this.mItems.get(i);
            if (g.getItemId() == n) {
                return (MenuItem)g;
            }
            if (g.hasSubMenu()) {
                final MenuItem item = ((Menu)g.getSubMenu()).findItem(n);
                if (item != null) {
                    return item;
                }
            }
        }
        return null;
    }
    
    public int findItemIndex(final int n) {
        for (int size = this.size(), i = 0; i < size; ++i) {
            if (this.mItems.get(i).getItemId() == n) {
                return i;
            }
        }
        return -1;
    }
    
    public g findItemWithShortcutForKey(final int n, final KeyEvent keyEvent) {
        final ArrayList<g> mTempShortcutItemList = this.mTempShortcutItemList;
        mTempShortcutItemList.clear();
        this.findItemsWithShortcutForKey(mTempShortcutItemList, n, keyEvent);
        if (mTempShortcutItemList.isEmpty()) {
            return null;
        }
        final int metaState = keyEvent.getMetaState();
        final KeyCharacterMap$KeyData keyCharacterMap$KeyData = new KeyCharacterMap$KeyData();
        keyEvent.getKeyData(keyCharacterMap$KeyData);
        final int size = mTempShortcutItemList.size();
        if (size == 1) {
            return (g)mTempShortcutItemList.get(0);
        }
        final boolean qwertyMode = this.isQwertyMode();
        for (int i = 0; i < size; ++i) {
            final g g = mTempShortcutItemList.get(i);
            char c;
            if (qwertyMode) {
                c = g.getAlphabeticShortcut();
            }
            else {
                c = g.getNumericShortcut();
            }
            final char[] meta = keyCharacterMap$KeyData.meta;
            if ((c == meta[0] && (metaState & 0x2) == 0x0) || (c == meta[2] && (metaState & 0x2) != 0x0) || (qwertyMode && c == '\b' && n == 67)) {
                return g;
            }
        }
        return null;
    }
    
    public void findItemsWithShortcutForKey(final List<g> list, final int n, final KeyEvent keyEvent) {
        final boolean qwertyMode = this.isQwertyMode();
        final int modifiers = keyEvent.getModifiers();
        final KeyCharacterMap$KeyData keyCharacterMap$KeyData = new KeyCharacterMap$KeyData();
        if (!keyEvent.getKeyData(keyCharacterMap$KeyData) && n != 67) {
            return;
        }
        for (int size = this.mItems.size(), i = 0; i < size; ++i) {
            final g g = this.mItems.get(i);
            if (g.hasSubMenu()) {
                ((e)g.getSubMenu()).findItemsWithShortcutForKey(list, n, keyEvent);
            }
            char c;
            if (qwertyMode) {
                c = g.getAlphabeticShortcut();
            }
            else {
                c = g.getNumericShortcut();
            }
            int n2;
            if (qwertyMode) {
                n2 = g.getAlphabeticModifiers();
            }
            else {
                n2 = g.getNumericModifiers();
            }
            if ((modifiers & 0x1100F) == (n2 & 0x1100F) && c != '\0') {
                final char[] meta = keyCharacterMap$KeyData.meta;
                if ((c == meta[0] || c == meta[2] || (qwertyMode && c == '\b' && n == 67)) && g.isEnabled()) {
                    list.add(g);
                }
            }
        }
    }
    
    public void flagActionItems() {
        final ArrayList<g> visibleItems = this.getVisibleItems();
        if (!this.mIsActionItemsStale) {
            return;
        }
        final Iterator<WeakReference<i>> iterator = this.mPresenters.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final WeakReference o = iterator.next();
            final i i = (i)o.get();
            if (i == null) {
                this.mPresenters.remove(o);
            }
            else {
                b |= i.flagActionItems();
            }
        }
        if (b) {
            this.mActionItems.clear();
            this.mNonActionItems.clear();
            for (int size = visibleItems.size(), j = 0; j < size; ++j) {
                final g e = visibleItems.get(j);
                ArrayList<g> list;
                if (e.l()) {
                    list = this.mActionItems;
                }
                else {
                    list = this.mNonActionItems;
                }
                list.add(e);
            }
        }
        else {
            this.mActionItems.clear();
            this.mNonActionItems.clear();
            this.mNonActionItems.addAll(this.getVisibleItems());
        }
        this.mIsActionItemsStale = false;
    }
    
    public ArrayList<g> getActionItems() {
        this.flagActionItems();
        return this.mActionItems;
    }
    
    public String getActionViewStatesKey() {
        return "android:menu:actionviewstates";
    }
    
    public Context getContext() {
        return this.mContext;
    }
    
    public g getExpandedItem() {
        return this.mExpandedItem;
    }
    
    public Drawable getHeaderIcon() {
        return this.mHeaderIcon;
    }
    
    public CharSequence getHeaderTitle() {
        return this.mHeaderTitle;
    }
    
    public View getHeaderView() {
        return this.mHeaderView;
    }
    
    public MenuItem getItem(final int index) {
        return (MenuItem)this.mItems.get(index);
    }
    
    public ArrayList<g> getNonActionItems() {
        this.flagActionItems();
        return this.mNonActionItems;
    }
    
    public boolean getOptionalIconsVisible() {
        return this.mOptionalIconsVisible;
    }
    
    public Resources getResources() {
        return this.mResources;
    }
    
    public e getRootMenu() {
        return this;
    }
    
    public ArrayList<g> getVisibleItems() {
        if (!this.mIsVisibleItemsStale) {
            return this.mVisibleItems;
        }
        this.mVisibleItems.clear();
        for (int size = this.mItems.size(), i = 0; i < size; ++i) {
            final g e = this.mItems.get(i);
            if (e.isVisible()) {
                this.mVisibleItems.add(e);
            }
        }
        this.mIsVisibleItemsStale = false;
        this.mIsActionItemsStale = true;
        return this.mVisibleItems;
    }
    
    public final void h(final int index, final boolean b) {
        if (index >= 0) {
            if (index < this.mItems.size()) {
                this.mItems.remove(index);
                if (b) {
                    this.onItemsChanged(true);
                }
            }
        }
    }
    
    public boolean hasVisibleItems() {
        if (this.mOverrideVisibleItems) {
            return true;
        }
        for (int size = this.size(), i = 0; i < size; ++i) {
            if (this.mItems.get(i).isVisible()) {
                return true;
            }
        }
        return false;
    }
    
    public final void i(final int n, final CharSequence mHeaderTitle, final int n2, final Drawable mHeaderIcon, final View mHeaderView) {
        final Resources resources = this.getResources();
        if (mHeaderView != null) {
            this.mHeaderView = mHeaderView;
            this.mHeaderTitle = null;
            this.mHeaderIcon = null;
        }
        else {
            if (n > 0) {
                this.mHeaderTitle = resources.getText(n);
            }
            else if (mHeaderTitle != null) {
                this.mHeaderTitle = mHeaderTitle;
            }
            if (n2 > 0) {
                this.mHeaderIcon = sl.getDrawable(this.getContext(), n2);
            }
            else if (mHeaderIcon != null) {
                this.mHeaderIcon = mHeaderIcon;
            }
            this.mHeaderView = null;
        }
        this.onItemsChanged(false);
    }
    
    public boolean isGroupDividerEnabled() {
        return this.mGroupDividerEnabled;
    }
    
    public boolean isQwertyMode() {
        return this.mQwertyMode;
    }
    
    public boolean isShortcutKey(final int n, final KeyEvent keyEvent) {
        return this.findItemWithShortcutForKey(n, keyEvent) != null;
    }
    
    public boolean isShortcutsVisible() {
        return this.mShortcutsVisible;
    }
    
    public final void j(final boolean b) {
        boolean mShortcutsVisible = false;
        Label_0044: {
            if (b) {
                final int keyboard = this.mResources.getConfiguration().keyboard;
                mShortcutsVisible = true;
                if (keyboard != 1 && q32.e(ViewConfiguration.get(this.mContext), this.mContext)) {
                    break Label_0044;
                }
            }
            mShortcutsVisible = false;
        }
        this.mShortcutsVisible = mShortcutsVisible;
    }
    
    public void onItemActionRequestChanged(final g g) {
        this.onItemsChanged(this.mIsActionItemsStale = true);
    }
    
    public void onItemVisibleChanged(final g g) {
        this.onItemsChanged(this.mIsVisibleItemsStale = true);
    }
    
    public void onItemsChanged(final boolean b) {
        if (!this.mPreventDispatchingItemsChanged) {
            if (b) {
                this.mIsVisibleItemsStale = true;
                this.mIsActionItemsStale = true;
            }
            this.b(b);
        }
        else {
            this.mItemsChangedWhileDispatchPrevented = true;
            if (b) {
                this.mStructureChangedWhileDispatchPrevented = true;
            }
        }
    }
    
    public boolean performIdentifierAction(final int n, final int n2) {
        return this.performItemAction(this.findItem(n), n2);
    }
    
    public boolean performItemAction(final MenuItem menuItem, final int n) {
        return this.performItemAction(menuItem, null, n);
    }
    
    public boolean performItemAction(final MenuItem menuItem, final i i, final int n) {
        final g g = (g)menuItem;
        if (g != null && g.isEnabled()) {
            final boolean k = g.k();
            final d2 b = g.b();
            final boolean b2 = b != null && b.a();
            boolean b4;
            if (g.j()) {
                final boolean b3 = b4 = (k | g.expandActionView());
                if (!b3) {
                    return b4;
                }
                b4 = b3;
            }
            else if (!g.hasSubMenu() && !b2) {
                b4 = k;
                if ((n & 0x1) != 0x0) {
                    return b4;
                }
                b4 = k;
            }
            else {
                if ((n & 0x4) == 0x0) {
                    this.close(false);
                }
                if (!g.hasSubMenu()) {
                    g.x(new l(this.getContext(), this, g));
                }
                final l l = (l)g.getSubMenu();
                if (b2) {
                    b.e((SubMenu)l);
                }
                final boolean b5 = b4 = (k | this.e(l, i));
                if (b5) {
                    return b4;
                }
                b4 = b5;
            }
            this.close(true);
            return b4;
        }
        return false;
    }
    
    public boolean performShortcut(final int n, final KeyEvent keyEvent, final int n2) {
        final g itemWithShortcutForKey = this.findItemWithShortcutForKey(n, keyEvent);
        final boolean b = itemWithShortcutForKey != null && this.performItemAction((MenuItem)itemWithShortcutForKey, n2);
        if ((n2 & 0x2) != 0x0) {
            this.close(true);
        }
        return b;
    }
    
    public void removeGroup(final int n) {
        final int groupIndex = this.findGroupIndex(n);
        if (groupIndex >= 0) {
            for (int size = this.mItems.size(), n2 = 0; n2 < size - groupIndex && this.mItems.get(groupIndex).getGroupId() == n; ++n2) {
                this.h(groupIndex, false);
            }
            this.onItemsChanged(true);
        }
    }
    
    public void removeItem(final int n) {
        this.h(this.findItemIndex(n), true);
    }
    
    public void removeItemAt(final int n) {
        this.h(n, true);
    }
    
    public void removeMenuPresenter(final i i) {
        for (final WeakReference o : this.mPresenters) {
            final i j = (i)o.get();
            if (j == null || j == i) {
                this.mPresenters.remove(o);
            }
        }
    }
    
    public void restoreActionViewStates(final Bundle bundle) {
        if (bundle == null) {
            return;
        }
        final SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(this.getActionViewStatesKey());
        for (int size = this.size(), i = 0; i < size; ++i) {
            final MenuItem item = this.getItem(i);
            final View actionView = item.getActionView();
            if (actionView != null && actionView.getId() != -1) {
                actionView.restoreHierarchyState(sparseParcelableArray);
            }
            if (item.hasSubMenu()) {
                ((l)item.getSubMenu()).restoreActionViewStates(bundle);
            }
        }
        final int int1 = ((BaseBundle)bundle).getInt("android:menu:expandedactionview");
        if (int1 > 0) {
            final MenuItem item2 = this.findItem(int1);
            if (item2 != null) {
                item2.expandActionView();
            }
        }
    }
    
    public void restorePresenterStates(final Bundle bundle) {
        this.c(bundle);
    }
    
    public void saveActionViewStates(final Bundle bundle) {
        final int size = this.size();
        SparseArray sparseArray = null;
        SparseArray sparseArray2;
        for (int i = 0; i < size; ++i, sparseArray = sparseArray2) {
            final MenuItem item = this.getItem(i);
            final View actionView = item.getActionView();
            sparseArray2 = sparseArray;
            if (actionView != null) {
                sparseArray2 = sparseArray;
                if (actionView.getId() != -1) {
                    SparseArray sparseArray3;
                    if ((sparseArray3 = sparseArray) == null) {
                        sparseArray3 = new SparseArray();
                    }
                    actionView.saveHierarchyState(sparseArray3);
                    sparseArray2 = sparseArray3;
                    if (item.isActionViewExpanded()) {
                        ((BaseBundle)bundle).putInt("android:menu:expandedactionview", item.getItemId());
                        sparseArray2 = sparseArray3;
                    }
                }
            }
            if (item.hasSubMenu()) {
                ((l)item.getSubMenu()).saveActionViewStates(bundle);
            }
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(this.getActionViewStatesKey(), sparseArray);
        }
    }
    
    public void savePresenterStates(final Bundle bundle) {
        this.d(bundle);
    }
    
    public void setCallback(final a mCallback) {
        this.mCallback = mCallback;
    }
    
    public void setCurrentMenuInfo(final ContextMenu$ContextMenuInfo mCurrentMenuInfo) {
        this.mCurrentMenuInfo = mCurrentMenuInfo;
    }
    
    public e setDefaultShowAsAction(final int mDefaultShowAsAction) {
        this.mDefaultShowAsAction = mDefaultShowAsAction;
        return this;
    }
    
    public void setExclusiveItemChecked(final MenuItem menuItem) {
        final int groupId = menuItem.getGroupId();
        final int size = this.mItems.size();
        this.stopDispatchingItemsChanged();
        for (int i = 0; i < size; ++i) {
            final g g = this.mItems.get(i);
            if (g.getGroupId() == groupId) {
                if (g.m()) {
                    if (g.isCheckable()) {
                        g.s(g == menuItem);
                    }
                }
            }
        }
        this.startDispatchingItemsChanged();
    }
    
    public void setGroupCheckable(final int n, final boolean checkable, final boolean b) {
        for (int size = this.mItems.size(), i = 0; i < size; ++i) {
            final g g = this.mItems.get(i);
            if (g.getGroupId() == n) {
                g.t(b);
                g.setCheckable(checkable);
            }
        }
    }
    
    public void setGroupDividerEnabled(final boolean mGroupDividerEnabled) {
        this.mGroupDividerEnabled = mGroupDividerEnabled;
    }
    
    public void setGroupEnabled(final int n, final boolean enabled) {
        for (int size = this.mItems.size(), i = 0; i < size; ++i) {
            final g g = this.mItems.get(i);
            if (g.getGroupId() == n) {
                g.setEnabled(enabled);
            }
        }
    }
    
    public void setGroupVisible(final int n, final boolean b) {
        final int size = this.mItems.size();
        int i = 0;
        int n2 = 0;
        while (i < size) {
            final g g = this.mItems.get(i);
            int n3 = n2;
            if (g.getGroupId() == n) {
                n3 = n2;
                if (g.y(b)) {
                    n3 = 1;
                }
            }
            ++i;
            n2 = n3;
        }
        if (n2 != 0) {
            this.onItemsChanged(true);
        }
    }
    
    public e setHeaderIconInt(final int n) {
        this.i(0, null, n, null, null);
        return this;
    }
    
    public e setHeaderIconInt(final Drawable drawable) {
        this.i(0, null, 0, drawable, null);
        return this;
    }
    
    public e setHeaderTitleInt(final int n) {
        this.i(n, null, 0, null, null);
        return this;
    }
    
    public e setHeaderTitleInt(final CharSequence charSequence) {
        this.i(0, charSequence, 0, null, null);
        return this;
    }
    
    public e setHeaderViewInt(final View view) {
        this.i(0, null, 0, null, view);
        return this;
    }
    
    public void setOptionalIconsVisible(final boolean mOptionalIconsVisible) {
        this.mOptionalIconsVisible = mOptionalIconsVisible;
    }
    
    public void setOverrideVisibleItems(final boolean mOverrideVisibleItems) {
        this.mOverrideVisibleItems = mOverrideVisibleItems;
    }
    
    public void setQwertyMode(final boolean mQwertyMode) {
        this.mQwertyMode = mQwertyMode;
        this.onItemsChanged(false);
    }
    
    public void setShortcutsVisible(final boolean b) {
        if (this.mShortcutsVisible == b) {
            return;
        }
        this.j(b);
        this.onItemsChanged(false);
    }
    
    public int size() {
        return this.mItems.size();
    }
    
    public void startDispatchingItemsChanged() {
        this.mPreventDispatchingItemsChanged = false;
        if (this.mItemsChangedWhileDispatchPrevented) {
            this.mItemsChangedWhileDispatchPrevented = false;
            this.onItemsChanged(this.mStructureChangedWhileDispatchPrevented);
        }
    }
    
    public void stopDispatchingItemsChanged() {
        if (!this.mPreventDispatchingItemsChanged) {
            this.mPreventDispatchingItemsChanged = true;
            this.mItemsChangedWhileDispatchPrevented = false;
            this.mStructureChangedWhileDispatchPrevented = false;
        }
    }
    
    public interface a
    {
        boolean onMenuItemSelected(final e p0, final MenuItem p1);
        
        void onMenuModeChange(final e p0);
    }
    
    public interface b
    {
        boolean c(final g p0);
    }
}

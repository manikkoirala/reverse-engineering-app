// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.widget.CompoundButton;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.widget.LinearLayout$LayoutParams;
import android.graphics.Rect;
import android.view.View;
import android.content.res.TypedArray;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.ImageView;
import android.widget.AbsListView$SelectionBoundsAdjuster;
import android.widget.LinearLayout;

public class ListMenuItemView extends LinearLayout implements a, AbsListView$SelectionBoundsAdjuster
{
    public g a;
    public ImageView b;
    public RadioButton c;
    public TextView d;
    public CheckBox e;
    public TextView f;
    public ImageView g;
    public ImageView h;
    public LinearLayout i;
    public Drawable j;
    public int k;
    public Context l;
    public boolean m;
    public Drawable n;
    public boolean p;
    public LayoutInflater q;
    public boolean t;
    
    public ListMenuItemView(final Context context, final AttributeSet set) {
        this(context, set, sa1.C);
    }
    
    public ListMenuItemView(final Context l, final AttributeSet set, int z) {
        super(l, set);
        final tw1 v = tw1.v(((View)this).getContext(), set, bc1.b2, z, 0);
        this.j = v.g(bc1.d2);
        this.k = v.n(bc1.c2, -1);
        this.m = v.a(bc1.e2, false);
        this.l = l;
        this.n = v.g(bc1.f2);
        final Resources$Theme theme = l.getTheme();
        z = sa1.z;
        final TypedArray obtainStyledAttributes = theme.obtainStyledAttributes((AttributeSet)null, new int[] { 16843049 }, z, 0);
        this.p = obtainStyledAttributes.hasValue(0);
        v.w();
        obtainStyledAttributes.recycle();
    }
    
    private LayoutInflater getInflater() {
        if (this.q == null) {
            this.q = LayoutInflater.from(((View)this).getContext());
        }
        return this.q;
    }
    
    private void setSubMenuArrowVisible(final boolean b) {
        final ImageView g = this.g;
        if (g != null) {
            int visibility;
            if (b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            g.setVisibility(visibility);
        }
    }
    
    public final void a(final View view) {
        this.b(view, -1);
    }
    
    public void adjustListItemSelectionBounds(final Rect rect) {
        final ImageView h = this.h;
        if (h != null && ((View)h).getVisibility() == 0) {
            final LinearLayout$LayoutParams linearLayout$LayoutParams = (LinearLayout$LayoutParams)((View)this.h).getLayoutParams();
            rect.top += ((View)this.h).getHeight() + linearLayout$LayoutParams.topMargin + linearLayout$LayoutParams.bottomMargin;
        }
    }
    
    public final void b(final View view, final int n) {
        final LinearLayout i = this.i;
        if (i != null) {
            ((ViewGroup)i).addView(view, n);
        }
        else {
            ((ViewGroup)this).addView(view, n);
        }
    }
    
    public final void c() {
        this.a((View)(this.e = (CheckBox)this.getInflater().inflate(ob1.h, (ViewGroup)this, false)));
    }
    
    public final void d() {
        this.b((View)(this.b = (ImageView)this.getInflater().inflate(ob1.i, (ViewGroup)this, false)), 0);
    }
    
    public final void e() {
        this.a((View)(this.c = (RadioButton)this.getInflater().inflate(ob1.k, (ViewGroup)this, false)));
    }
    
    public void f(final boolean b, final char c) {
        int visibility;
        if (b && this.a.A()) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        if (visibility == 0) {
            this.f.setText((CharSequence)this.a.h());
        }
        if (((View)this.f).getVisibility() != visibility) {
            ((View)this.f).setVisibility(visibility);
        }
    }
    
    public g getItemData() {
        return this.a;
    }
    
    public void initialize(final g a, int visibility) {
        this.a = a;
        if (a.isVisible()) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        ((View)this).setVisibility(visibility);
        this.setTitle(a.i(this));
        this.setCheckable(a.isCheckable());
        this.f(a.A(), a.g());
        this.setIcon(a.getIcon());
        ((View)this).setEnabled(a.isEnabled());
        this.setSubMenuArrowVisible(a.hasSubMenu());
        ((View)this).setContentDescription(a.getContentDescription());
    }
    
    public void onFinishInflate() {
        super.onFinishInflate();
        o32.u0((View)this, this.j);
        final TextView d = (TextView)((View)this).findViewById(db1.M);
        this.d = d;
        final int k = this.k;
        if (k != -1) {
            d.setTextAppearance(this.l, k);
        }
        this.f = (TextView)((View)this).findViewById(db1.F);
        final ImageView g = (ImageView)((View)this).findViewById(db1.I);
        if ((this.g = g) != null) {
            g.setImageDrawable(this.n);
        }
        this.h = (ImageView)((View)this).findViewById(db1.r);
        this.i = (LinearLayout)((View)this).findViewById(db1.l);
    }
    
    public void onMeasure(final int n, final int n2) {
        if (this.b != null && this.m) {
            final ViewGroup$LayoutParams layoutParams = ((View)this).getLayoutParams();
            final LinearLayout$LayoutParams linearLayout$LayoutParams = (LinearLayout$LayoutParams)((View)this.b).getLayoutParams();
            final int height = layoutParams.height;
            if (height > 0 && linearLayout$LayoutParams.width <= 0) {
                linearLayout$LayoutParams.width = height;
            }
        }
        super.onMeasure(n, n2);
    }
    
    public boolean prefersCondensedTitle() {
        return false;
    }
    
    public void setCheckable(final boolean b) {
        if (!b && this.c == null && this.e == null) {
            return;
        }
        Object o;
        Object o2;
        if (this.a.m()) {
            if (this.c == null) {
                this.e();
            }
            o = this.c;
            o2 = this.e;
        }
        else {
            if (this.e == null) {
                this.c();
            }
            o = this.e;
            o2 = this.c;
        }
        if (b) {
            ((CompoundButton)o).setChecked(this.a.isChecked());
            if (((View)o).getVisibility() != 0) {
                ((View)o).setVisibility(0);
            }
            if (o2 != null && ((View)o2).getVisibility() != 8) {
                ((View)o2).setVisibility(8);
            }
        }
        else {
            final CheckBox e = this.e;
            if (e != null) {
                ((View)e).setVisibility(8);
            }
            final RadioButton c = this.c;
            if (c != null) {
                ((View)c).setVisibility(8);
            }
        }
    }
    
    public void setChecked(final boolean checked) {
        Object o;
        if (this.a.m()) {
            if (this.c == null) {
                this.e();
            }
            o = this.c;
        }
        else {
            if (this.e == null) {
                this.c();
            }
            o = this.e;
        }
        ((CompoundButton)o).setChecked(checked);
    }
    
    public void setForceShowIcon(final boolean b) {
        this.t = b;
        this.m = b;
    }
    
    public void setGroupDividerEnabled(final boolean b) {
        final ImageView h = this.h;
        if (h != null) {
            int visibility;
            if (!this.p && b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            h.setVisibility(visibility);
        }
    }
    
    public void setIcon(Drawable imageDrawable) {
        final boolean b = this.a.z() || this.t;
        if (!b && !this.m) {
            return;
        }
        final ImageView b2 = this.b;
        if (b2 == null && imageDrawable == null && !this.m) {
            return;
        }
        if (b2 == null) {
            this.d();
        }
        if (imageDrawable == null && !this.m) {
            this.b.setVisibility(8);
        }
        else {
            final ImageView b3 = this.b;
            if (!b) {
                imageDrawable = null;
            }
            b3.setImageDrawable(imageDrawable);
            if (((View)this.b).getVisibility() != 0) {
                this.b.setVisibility(0);
            }
        }
    }
    
    public void setTitle(final CharSequence text) {
        TextView textView;
        int visibility;
        if (text != null) {
            this.d.setText(text);
            if (((View)this.d).getVisibility() == 0) {
                return;
            }
            textView = this.d;
            visibility = 0;
        }
        else {
            final int visibility2 = ((View)this.d).getVisibility();
            visibility = 8;
            if (visibility2 == 8) {
                return;
            }
            textView = this.d;
        }
        ((View)textView).setVisibility(visibility);
    }
}

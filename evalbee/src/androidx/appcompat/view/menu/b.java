// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.FrameLayout;
import android.os.Build$VERSION;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.graphics.Rect;
import android.widget.ListAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.AdapterView$OnItemClickListener;
import android.util.AttributeSet;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.widget.ListView;
import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MenuItem;
import java.util.Iterator;
import java.util.ArrayList;
import android.view.View;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import java.util.List;
import android.os.Handler;
import android.content.Context;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow$OnDismissListener;
import android.view.View$OnKeyListener;

public final class b extends pv0 implements View$OnKeyListener, PopupWindow$OnDismissListener
{
    public static final int H;
    public boolean A;
    public a C;
    public ViewTreeObserver D;
    public PopupWindow$OnDismissListener F;
    public boolean G;
    public final Context b;
    public final int c;
    public final int d;
    public final int e;
    public final boolean f;
    public final Handler g;
    public final List h;
    public final List i;
    public final ViewTreeObserver$OnGlobalLayoutListener j;
    public final View$OnAttachStateChangeListener k;
    public final nv0 l;
    public int m;
    public int n;
    public View p;
    public View q;
    public int t;
    public boolean v;
    public boolean w;
    public int x;
    public int y;
    public boolean z;
    
    static {
        H = ob1.e;
    }
    
    public b(final Context b, final View p5, final int d, final int e, final boolean f) {
        this.h = new ArrayList();
        this.i = new ArrayList();
        this.j = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
            public final b a;
            
            public void onGlobalLayout() {
                if (this.a.a() && this.a.i.size() > 0 && !this.a.i.get(0).a.y()) {
                    final View q = this.a.q;
                    if (q != null && q.isShown()) {
                        final Iterator iterator = this.a.i.iterator();
                        while (iterator.hasNext()) {
                            ((d)iterator.next()).a.show();
                        }
                    }
                    else {
                        this.a.dismiss();
                    }
                }
            }
        };
        this.k = (View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this) {
            public final b a;
            
            public void onViewAttachedToWindow(final View view) {
            }
            
            public void onViewDetachedFromWindow(final View view) {
                final ViewTreeObserver d = this.a.D;
                if (d != null) {
                    if (!d.isAlive()) {
                        this.a.D = view.getViewTreeObserver();
                    }
                    final b a = this.a;
                    a.D.removeGlobalOnLayoutListener(a.j);
                }
                view.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            }
        };
        this.l = new nv0(this) {
            public final b a;
            
            @Override
            public void b(final e e, final MenuItem menuItem) {
                final Handler g = this.a.g;
                d d = null;
                g.removeCallbacksAndMessages((Object)null);
                final int size = this.a.i.size();
                int i = 0;
                while (true) {
                    while (i < size) {
                        if (e == ((d)this.a.i.get(i)).b) {
                            if (i == -1) {
                                return;
                            }
                            if (++i < this.a.i.size()) {
                                d = (d)this.a.i.get(i);
                            }
                            this.a.g.postAtTime((Runnable)new Runnable(this, d, menuItem, e) {
                                public final d a;
                                public final MenuItem b;
                                public final e c;
                                public final b$c d;
                                
                                @Override
                                public void run() {
                                    final d a = this.a;
                                    if (a != null) {
                                        this.d.a.G = true;
                                        a.b.close(false);
                                        this.d.a.G = false;
                                    }
                                    if (this.b.isEnabled() && this.b.hasSubMenu()) {
                                        this.c.performItemAction(this.b, 4);
                                    }
                                }
                            }, (Object)e, SystemClock.uptimeMillis() + 200L);
                            return;
                        }
                        else {
                            ++i;
                        }
                    }
                    i = -1;
                    continue;
                }
            }
            
            @Override
            public void g(final e e, final MenuItem menuItem) {
                this.a.g.removeCallbacksAndMessages((Object)e);
            }
        };
        this.m = 0;
        this.n = 0;
        this.b = b;
        this.p = p5;
        this.d = d;
        this.e = e;
        this.f = f;
        this.z = false;
        this.t = this.u();
        final Resources resources = b.getResources();
        this.c = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(ab1.b));
        this.g = new Handler();
    }
    
    public boolean a() {
        final int size = this.i.size();
        boolean b = false;
        if (size > 0) {
            b = b;
            if (this.i.get(0).a.a()) {
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public void b(final e e) {
        e.addMenuPresenter(this, this.b);
        if (this.a()) {
            this.w(e);
        }
        else {
            this.h.add(e);
        }
    }
    
    @Override
    public boolean c() {
        return false;
    }
    
    public void dismiss() {
        int i = this.i.size();
        if (i > 0) {
            final d[] array = this.i.toArray(new d[i]);
            --i;
            while (i >= 0) {
                final d d = array[i];
                if (d.a.a()) {
                    d.a.dismiss();
                }
                --i;
            }
        }
    }
    
    @Override
    public void f(final View p) {
        if (this.p != p) {
            this.p = p;
            this.n = pb0.b(this.m, o32.B(p));
        }
    }
    
    public boolean flagActionItems() {
        return false;
    }
    
    public ListView h() {
        ListView a;
        if (this.i.isEmpty()) {
            a = null;
        }
        else {
            final List i = this.i;
            a = ((d)i.get(i.size() - 1)).a();
        }
        return a;
    }
    
    @Override
    public void i(final boolean z) {
        this.z = z;
    }
    
    @Override
    public void j(final int m) {
        if (this.m != m) {
            this.m = m;
            this.n = pb0.b(m, o32.B(this.p));
        }
    }
    
    @Override
    public void k(final int x) {
        this.v = true;
        this.x = x;
    }
    
    @Override
    public void l(final PopupWindow$OnDismissListener f) {
        this.F = f;
    }
    
    @Override
    public void m(final boolean a) {
        this.A = a;
    }
    
    @Override
    public void n(final int y) {
        this.w = true;
        this.y = y;
    }
    
    public void onCloseMenu(final e e, final boolean b) {
        final int r = this.r(e);
        if (r < 0) {
            return;
        }
        final int n = r + 1;
        if (n < this.i.size()) {
            ((d)this.i.get(n)).b.close(false);
        }
        final d d = this.i.remove(r);
        d.b.removeMenuPresenter(this);
        if (this.G) {
            d.a.Q(null);
            d.a.B(0);
        }
        d.a.dismiss();
        final int size = this.i.size();
        int t;
        if (size > 0) {
            t = this.i.get(size - 1).c;
        }
        else {
            t = this.u();
        }
        this.t = t;
        if (size == 0) {
            this.dismiss();
            final a c = this.C;
            if (c != null) {
                c.onCloseMenu(e, true);
            }
            final ViewTreeObserver d2 = this.D;
            if (d2 != null) {
                if (d2.isAlive()) {
                    this.D.removeGlobalOnLayoutListener(this.j);
                }
                this.D = null;
            }
            this.q.removeOnAttachStateChangeListener(this.k);
            this.F.onDismiss();
        }
        else if (b) {
            this.i.get(0).b.close(false);
        }
    }
    
    public void onDismiss() {
        while (true) {
            for (int size = this.i.size(), i = 0; i < size; ++i) {
                final d d = this.i.get(i);
                if (!d.a.a()) {
                    if (d != null) {
                        d.b.close(false);
                    }
                    return;
                }
            }
            final d d = null;
            continue;
        }
    }
    
    public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1 && n == 82) {
            this.dismiss();
            return true;
        }
        return false;
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
    }
    
    public Parcelable onSaveInstanceState() {
        return null;
    }
    
    public boolean onSubMenuSelected(final l l) {
        for (final d d : this.i) {
            if (l == d.b) {
                ((View)d.a()).requestFocus();
                return true;
            }
        }
        if (l.hasVisibleItems()) {
            this.b(l);
            final a c = this.C;
            if (c != null) {
                c.a(l);
            }
            return true;
        }
        return false;
    }
    
    public final qv0 q() {
        final qv0 qv0 = new qv0(this.b, null, this.d, this.e);
        qv0.R(this.l);
        qv0.I((AdapterView$OnItemClickListener)this);
        qv0.H((PopupWindow$OnDismissListener)this);
        qv0.A(this.p);
        qv0.D(this.n);
        qv0.G(true);
        qv0.F(2);
        return qv0;
    }
    
    public final int r(final e e) {
        for (int size = this.i.size(), i = 0; i < size; ++i) {
            if (e == ((d)this.i.get(i)).b) {
                return i;
            }
        }
        return -1;
    }
    
    public final MenuItem s(final e e, final e e2) {
        for (int size = e.size(), i = 0; i < size; ++i) {
            final MenuItem item = e.getItem(i);
            if (item.hasSubMenu() && e2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }
    
    public void setCallback(final a c) {
        this.C = c;
    }
    
    public void show() {
        if (this.a()) {
            return;
        }
        final Iterator iterator = this.h.iterator();
        while (iterator.hasNext()) {
            this.w((e)iterator.next());
        }
        this.h.clear();
        final View p = this.p;
        if ((this.q = p) != null) {
            final boolean b = this.D == null;
            final ViewTreeObserver viewTreeObserver = p.getViewTreeObserver();
            this.D = viewTreeObserver;
            if (b) {
                viewTreeObserver.addOnGlobalLayoutListener(this.j);
            }
            this.q.addOnAttachStateChangeListener(this.k);
        }
    }
    
    public final View t(final d d, final e e) {
        final MenuItem s = this.s(d.b, e);
        if (s == null) {
            return null;
        }
        final ListView a = d.a();
        final ListAdapter adapter = a.getAdapter();
        final boolean b = adapter instanceof HeaderViewListAdapter;
        int i = 0;
        int headersCount;
        androidx.appcompat.view.menu.d d2;
        if (b) {
            final HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter)adapter;
            headersCount = headerViewListAdapter.getHeadersCount();
            d2 = (androidx.appcompat.view.menu.d)headerViewListAdapter.getWrappedAdapter();
        }
        else {
            d2 = (androidx.appcompat.view.menu.d)adapter;
            headersCount = 0;
        }
        while (true) {
            while (i < d2.getCount()) {
                if (s == d2.c(i)) {
                    if (i == -1) {
                        return null;
                    }
                    final int n = i + headersCount - ((AdapterView)a).getFirstVisiblePosition();
                    if (n >= 0 && n < ((ViewGroup)a).getChildCount()) {
                        return ((ViewGroup)a).getChildAt(n);
                    }
                    return null;
                }
                else {
                    ++i;
                }
            }
            i = -1;
            continue;
        }
    }
    
    public final int u() {
        final int b = o32.B(this.p);
        int n = 1;
        if (b == 1) {
            n = 0;
        }
        return n;
    }
    
    public void updateMenuView(final boolean b) {
        final Iterator iterator = this.i.iterator();
        while (iterator.hasNext()) {
            pv0.p(((d)iterator.next()).a().getAdapter()).notifyDataSetChanged();
        }
    }
    
    public final int v(final int n) {
        final List i = this.i;
        final ListView a = i.get(i.size() - 1).a();
        final int[] array = new int[2];
        ((View)a).getLocationOnScreen(array);
        final Rect rect = new Rect();
        this.q.getWindowVisibleDisplayFrame(rect);
        if (this.t == 1) {
            if (array[0] + ((View)a).getWidth() + n > rect.right) {
                return 0;
            }
            return 1;
        }
        else {
            if (array[0] - n < 0) {
                return 1;
            }
            return 0;
        }
    }
    
    public final void w(final e e) {
        final LayoutInflater from = LayoutInflater.from(this.b);
        final androidx.appcompat.view.menu.d d = new androidx.appcompat.view.menu.d(e, from, this.f, androidx.appcompat.view.menu.b.H);
        if (!this.a() && this.z) {
            d.d(true);
        }
        else if (this.a()) {
            d.d(pv0.o(e));
        }
        int n = pv0.e((ListAdapter)d, null, this.b, this.c);
        final qv0 q = this.q();
        q.m((ListAdapter)d);
        q.C(n);
        q.D(this.n);
        d d2;
        View t;
        if (this.i.size() > 0) {
            final List i = this.i;
            d2 = i.get(i.size() - 1);
            t = this.t(d2, e);
        }
        else {
            d2 = null;
            t = null;
        }
        if (t != null) {
            q.S(false);
            q.P(null);
            final int v = this.v(n);
            final boolean b = v == 1;
            this.t = v;
            int n2;
            int n3;
            if (Build$VERSION.SDK_INT >= 26) {
                q.A(t);
                n2 = 0;
                n3 = 0;
            }
            else {
                final int[] array = new int[2];
                this.p.getLocationOnScreen(array);
                final int[] array2 = new int[2];
                t.getLocationOnScreen(array2);
                if ((this.n & 0x7) == 0x5) {
                    array[0] += this.p.getWidth();
                    array2[0] += t.getWidth();
                }
                n3 = array2[0] - array[0];
                n2 = array2[1] - array[1];
            }
            int n4 = 0;
            Label_0371: {
                Label_0366: {
                    if ((this.n & 0x5) == 0x5) {
                        if (!b) {
                            n = t.getWidth();
                            break Label_0366;
                        }
                    }
                    else {
                        if (!b) {
                            break Label_0366;
                        }
                        n = t.getWidth();
                    }
                    n4 = n3 + n;
                    break Label_0371;
                }
                n4 = n3 - n;
            }
            q.j(n4);
            q.K(true);
            q.c(n2);
        }
        else {
            if (this.v) {
                q.j(this.x);
            }
            if (this.w) {
                q.c(this.y);
            }
            q.E(this.d());
        }
        this.i.add(new d(q, e, this.t));
        q.show();
        final ListView h = q.h();
        ((View)h).setOnKeyListener((View$OnKeyListener)this);
        if (d2 == null && this.A && e.getHeaderTitle() != null) {
            final FrameLayout frameLayout = (FrameLayout)from.inflate(ob1.l, (ViewGroup)h, false);
            final TextView textView = (TextView)((View)frameLayout).findViewById(16908310);
            ((View)frameLayout).setEnabled(false);
            textView.setText(e.getHeaderTitle());
            h.addHeaderView((View)frameLayout, (Object)null, false);
            q.show();
        }
    }
    
    public static class d
    {
        public final qv0 a;
        public final e b;
        public final int c;
        
        public d(final qv0 a, final e b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public ListView a() {
            return this.a.h();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import java.util.ArrayList;
import android.view.ViewGroup;
import android.view.View;
import android.view.LayoutInflater;
import android.content.Context;

public abstract class a implements i
{
    public Context a;
    public Context b;
    public e c;
    public LayoutInflater d;
    public LayoutInflater e;
    public i.a f;
    public int g;
    public int h;
    public j i;
    public int j;
    
    public a(final Context a, final int g, final int h) {
        this.a = a;
        this.d = LayoutInflater.from(a);
        this.g = g;
        this.h = h;
    }
    
    public void a(final View view, final int n) {
        final ViewGroup viewGroup = (ViewGroup)view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup)this.i).addView(view, n);
    }
    
    public abstract void b(final g p0, final j.a p1);
    
    public j.a c(final ViewGroup viewGroup) {
        return (j.a)this.d.inflate(this.h, viewGroup, false);
    }
    
    @Override
    public boolean collapseItemActionView(final e e, final g g) {
        return false;
    }
    
    public boolean d(final ViewGroup viewGroup, final int n) {
        viewGroup.removeViewAt(n);
        return true;
    }
    
    public i.a e() {
        return this.f;
    }
    
    @Override
    public boolean expandItemActionView(final e e, final g g) {
        return false;
    }
    
    public View f(final g g, final View view, final ViewGroup viewGroup) {
        j.a c;
        if (view instanceof j.a) {
            c = (j.a)view;
        }
        else {
            c = this.c(viewGroup);
        }
        this.b(g, c);
        return (View)c;
    }
    
    public j g(final ViewGroup viewGroup) {
        if (this.i == null) {
            (this.i = (j)this.d.inflate(this.g, viewGroup, false)).initialize(this.c);
            this.updateMenuView(true);
        }
        return this.i;
    }
    
    @Override
    public int getId() {
        return this.j;
    }
    
    public void h(final int j) {
        this.j = j;
    }
    
    public abstract boolean i(final int p0, final g p1);
    
    @Override
    public void initForMenu(final Context b, final e c) {
        this.b = b;
        this.e = LayoutInflater.from(b);
        this.c = c;
    }
    
    @Override
    public void onCloseMenu(final e e, final boolean b) {
        final i.a f = this.f;
        if (f != null) {
            f.onCloseMenu(e, b);
        }
    }
    
    @Override
    public boolean onSubMenuSelected(l c) {
        final i.a f = this.f;
        if (f != null) {
            if (c == null) {
                c = (l)this.c;
            }
            return f.a(c);
        }
        return false;
    }
    
    @Override
    public void setCallback(final i.a f) {
        this.f = f;
    }
    
    @Override
    public void updateMenuView(final boolean b) {
        final ViewGroup viewGroup = (ViewGroup)this.i;
        if (viewGroup == null) {
            return;
        }
        final e c = this.c;
        int i = 0;
        if (c != null) {
            c.flagActionItems();
            final ArrayList<g> visibleItems = this.c.getVisibleItems();
            final int size = visibleItems.size();
            int j = 0;
            i = 0;
            while (j < size) {
                final g g = visibleItems.get(j);
                int n = i;
                if (this.i(i, g)) {
                    final View child = viewGroup.getChildAt(i);
                    g itemData;
                    if (child instanceof j.a) {
                        itemData = ((j.a)child).getItemData();
                    }
                    else {
                        itemData = null;
                    }
                    final View f = this.f(g, child, viewGroup);
                    if (g != itemData) {
                        f.setPressed(false);
                        f.jumpDrawablesToCurrentState();
                    }
                    if (f != child) {
                        this.a(f, i);
                    }
                    n = i + 1;
                }
                ++j;
                i = n;
            }
        }
        while (i < viewGroup.getChildCount()) {
            if (!this.d(viewGroup, i)) {
                ++i;
            }
        }
    }
}

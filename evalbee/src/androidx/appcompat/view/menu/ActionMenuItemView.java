// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.view.MotionEvent;
import android.os.Parcelable;
import android.view.View$MeasureSpec;
import android.widget.Button;
import android.view.View;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.ActionMenuView;
import android.view.View$OnClickListener;

public class ActionMenuItemView extends m7 implements j.a, View$OnClickListener, ActionMenuView.a
{
    public g a;
    public CharSequence b;
    public Drawable c;
    public androidx.appcompat.view.menu.e.b d;
    public w70 e;
    public b f;
    public boolean g;
    public boolean h;
    public int i;
    public int j;
    public int k;
    
    public ActionMenuItemView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ActionMenuItemView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final Resources resources = context.getResources();
        this.g = this.e();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, bc1.v, n, 0);
        this.i = obtainStyledAttributes.getDimensionPixelSize(bc1.w, 0);
        obtainStyledAttributes.recycle();
        this.k = (int)(resources.getDisplayMetrics().density * 32.0f + 0.5f);
        ((View)this).setOnClickListener((View$OnClickListener)this);
        this.j = -1;
        ((View)this).setSaveEnabled(false);
    }
    
    public boolean a() {
        return this.d();
    }
    
    public boolean b() {
        return this.d() && this.a.getIcon() == null;
    }
    
    public boolean d() {
        return TextUtils.isEmpty(this.getText()) ^ true;
    }
    
    public final boolean e() {
        final Configuration configuration = ((View)this).getContext().getResources().getConfiguration();
        final int screenWidthDp = configuration.screenWidthDp;
        final int screenHeightDp = configuration.screenHeightDp;
        return screenWidthDp >= 480 || (screenWidthDp >= 640 && screenHeightDp >= 480) || configuration.orientation == 2;
    }
    
    public final void f() {
        final boolean empty = TextUtils.isEmpty(this.b);
        boolean b2;
        final boolean b = b2 = true;
        Label_0052: {
            if (this.c != null) {
                if (this.a.B()) {
                    b2 = b;
                    if (this.g) {
                        break Label_0052;
                    }
                    if (this.h) {
                        b2 = b;
                        break Label_0052;
                    }
                }
                b2 = false;
            }
        }
        final boolean b3 = (empty ^ true) & b2;
        final CharSequence charSequence = null;
        CharSequence b4;
        if (b3) {
            b4 = this.b;
        }
        else {
            b4 = null;
        }
        this.setText(b4);
        CharSequence contentDescription;
        if (TextUtils.isEmpty(contentDescription = this.a.getContentDescription())) {
            if (b3) {
                contentDescription = null;
            }
            else {
                contentDescription = this.a.getTitle();
            }
        }
        ((View)this).setContentDescription(contentDescription);
        final CharSequence tooltipText = this.a.getTooltipText();
        if (TextUtils.isEmpty(tooltipText)) {
            CharSequence title;
            if (b3) {
                title = charSequence;
            }
            else {
                title = this.a.getTitle();
            }
            cx1.a((View)this, title);
        }
        else {
            cx1.a((View)this, tooltipText);
        }
    }
    
    public CharSequence getAccessibilityClassName() {
        return Button.class.getName();
    }
    
    @Override
    public g getItemData() {
        return this.a;
    }
    
    @Override
    public void initialize(final g a, int visibility) {
        this.a = a;
        this.setIcon(a.getIcon());
        this.setTitle(a.i(this));
        ((View)this).setId(a.getItemId());
        if (a.isVisible()) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        ((View)this).setVisibility(visibility);
        ((View)this).setEnabled(a.isEnabled());
        if (a.hasSubMenu() && this.e == null) {
            this.e = new a();
        }
    }
    
    public void onClick(final View view) {
        final androidx.appcompat.view.menu.e.b d = this.d;
        if (d != null) {
            d.c(this.a);
        }
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.g = this.e();
        this.f();
    }
    
    @Override
    public void onMeasure(int a, final int n) {
        final boolean d = this.d();
        if (d) {
            final int j = this.j;
            if (j >= 0) {
                super.setPadding(j, ((View)this).getPaddingTop(), ((View)this).getPaddingRight(), ((View)this).getPaddingBottom());
            }
        }
        super.onMeasure(a, n);
        final int mode = View$MeasureSpec.getMode(a);
        a = View$MeasureSpec.getSize(a);
        final int measuredWidth = ((View)this).getMeasuredWidth();
        if (mode == Integer.MIN_VALUE) {
            a = Math.min(a, this.i);
        }
        else {
            a = this.i;
        }
        if (mode != 1073741824 && this.i > 0 && measuredWidth < a) {
            super.onMeasure(View$MeasureSpec.makeMeasureSpec(a, 1073741824), n);
        }
        if (!d && this.c != null) {
            super.setPadding((((View)this).getMeasuredWidth() - this.c.getBounds().width()) / 2, ((View)this).getPaddingTop(), ((View)this).getPaddingRight(), ((View)this).getPaddingBottom());
        }
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        super.onRestoreInstanceState((Parcelable)null);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.a.hasSubMenu()) {
            final w70 e = this.e;
            if (e != null && e.onTouch((View)this, motionEvent)) {
                return true;
            }
        }
        return super.onTouchEvent(motionEvent);
    }
    
    @Override
    public boolean prefersCondensedTitle() {
        return true;
    }
    
    public void setCheckable(final boolean b) {
    }
    
    public void setChecked(final boolean b) {
    }
    
    public void setExpandedFormat(final boolean h) {
        if (this.h != h) {
            this.h = h;
            final g a = this.a;
            if (a != null) {
                a.c();
            }
        }
    }
    
    public void setIcon(final Drawable c) {
        this.c = c;
        if (c != null) {
            final int intrinsicWidth = c.getIntrinsicWidth();
            final int intrinsicHeight = c.getIntrinsicHeight();
            int k = this.k;
            int n = intrinsicWidth;
            int n2 = intrinsicHeight;
            if (intrinsicWidth > k) {
                n2 = (int)(intrinsicHeight * (k / (float)intrinsicWidth));
                n = k;
            }
            if (n2 > k) {
                n *= (int)(k / (float)n2);
            }
            else {
                k = n2;
            }
            c.setBounds(0, 0, n, k);
        }
        this.setCompoundDrawables(c, null, null, null);
        this.f();
    }
    
    public void setItemInvoker(final androidx.appcompat.view.menu.e.b d) {
        this.d = d;
    }
    
    public void setPadding(final int j, final int n, final int n2, final int n3) {
        super.setPadding(this.j = j, n, n2, n3);
    }
    
    public void setPopupCallback(final b f) {
        this.f = f;
    }
    
    public void setTitle(final CharSequence b) {
        this.b = b;
        this.f();
    }
    
    public class a extends w70
    {
        public final ActionMenuItemView j;
        
        public a(final ActionMenuItemView j) {
            super((View)(this.j = j));
        }
        
        @Override
        public wn1 b() {
            final ActionMenuItemView.b f = this.j.f;
            if (f != null) {
                return f.a();
            }
            return null;
        }
        
        @Override
        public boolean c() {
            final ActionMenuItemView j = this.j;
            final androidx.appcompat.view.menu.e.b d = j.d;
            boolean b2;
            final boolean b = b2 = false;
            if (d != null) {
                b2 = b;
                if (d.c(j.a)) {
                    final wn1 b3 = this.b();
                    b2 = b;
                    if (b3 != null) {
                        b2 = b;
                        if (b3.a()) {
                            b2 = true;
                        }
                    }
                }
            }
            return b2;
        }
    }
    
    public abstract static class b
    {
        public abstract wn1 a();
    }
}

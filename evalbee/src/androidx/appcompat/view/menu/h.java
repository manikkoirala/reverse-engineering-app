// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.graphics.Rect;
import android.view.Display;
import android.graphics.Point;
import android.view.WindowManager;
import android.widget.PopupWindow$OnDismissListener;
import android.view.View;
import android.content.Context;

public class h
{
    public final Context a;
    public final e b;
    public final boolean c;
    public final int d;
    public final int e;
    public View f;
    public int g;
    public boolean h;
    public i.a i;
    public pv0 j;
    public PopupWindow$OnDismissListener k;
    public final PopupWindow$OnDismissListener l;
    
    public h(final Context context, final e e, final View view, final boolean b, final int n) {
        this(context, e, view, b, n, 0);
    }
    
    public h(final Context a, final e b, final View f, final boolean c, final int d, final int e) {
        this.g = 8388611;
        this.l = (PopupWindow$OnDismissListener)new PopupWindow$OnDismissListener(this) {
            public final h a;
            
            public void onDismiss() {
                this.a.e();
            }
        };
        this.a = a;
        this.b = b;
        this.f = f;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public final pv0 a() {
        final Display defaultDisplay = ((WindowManager)this.a.getSystemService("window")).getDefaultDisplay();
        final Point point = new Point();
        androidx.appcompat.view.menu.h.b.a(defaultDisplay, point);
        pv0 pv0;
        if (Math.min(point.x, point.y) >= this.a.getResources().getDimensionPixelSize(ab1.a)) {
            pv0 = new androidx.appcompat.view.menu.b(this.a, this.f, this.d, this.e, this.c);
        }
        else {
            pv0 = new k(this.a, this.b, this.f, this.d, this.e, this.c);
        }
        pv0.b(this.b);
        pv0.l(this.l);
        pv0.f(this.f);
        pv0.setCallback(this.i);
        pv0.i(this.h);
        pv0.j(this.g);
        return pv0;
    }
    
    public void b() {
        if (this.d()) {
            this.j.dismiss();
        }
    }
    
    public pv0 c() {
        if (this.j == null) {
            this.j = this.a();
        }
        return this.j;
    }
    
    public boolean d() {
        final pv0 j = this.j;
        return j != null && j.a();
    }
    
    public void e() {
        this.j = null;
        final PopupWindow$OnDismissListener k = this.k;
        if (k != null) {
            k.onDismiss();
        }
    }
    
    public void f(final View f) {
        this.f = f;
    }
    
    public void g(final boolean h) {
        this.h = h;
        final pv0 j = this.j;
        if (j != null) {
            j.i(h);
        }
    }
    
    public void h(final int g) {
        this.g = g;
    }
    
    public void i(final PopupWindow$OnDismissListener k) {
        this.k = k;
    }
    
    public void j(final i.a a) {
        this.i = a;
        final pv0 j = this.j;
        if (j != null) {
            j.setCallback(a);
        }
    }
    
    public void k() {
        if (this.m()) {
            return;
        }
        throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
    }
    
    public final void l(int n, final int n2, final boolean b, final boolean b2) {
        final pv0 c = this.c();
        c.m(b2);
        if (b) {
            int n3 = n;
            if ((pb0.b(this.g, o32.B(this.f)) & 0x7) == 0x5) {
                n3 = n - this.f.getWidth();
            }
            c.k(n3);
            c.n(n2);
            n = (int)(this.a.getResources().getDisplayMetrics().density * 48.0f / 2.0f);
            c.g(new Rect(n3 - n, n2 - n, n3 + n, n2 + n));
        }
        c.show();
    }
    
    public boolean m() {
        if (this.d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        this.l(0, 0, false, false);
        return true;
    }
    
    public boolean n(final int n, final int n2) {
        if (this.d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        this.l(n, n2, true, true);
        return true;
    }
    
    public abstract static class b
    {
        public static void a(final Display display, final Point point) {
            display.getRealSize(point);
        }
    }
}

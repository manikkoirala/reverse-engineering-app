// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

public class d extends BaseAdapter
{
    public e a;
    public int b;
    public boolean c;
    public final boolean d;
    public final LayoutInflater e;
    public final int f;
    
    public d(final e a, final LayoutInflater e, final boolean d, final int f) {
        this.b = -1;
        this.d = d;
        this.e = e;
        this.a = a;
        this.f = f;
        this.a();
    }
    
    public void a() {
        final g expandedItem = this.a.getExpandedItem();
        if (expandedItem != null) {
            final ArrayList<g> nonActionItems = this.a.getNonActionItems();
            for (int size = nonActionItems.size(), i = 0; i < size; ++i) {
                if (nonActionItems.get(i) == expandedItem) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }
    
    public e b() {
        return this.a;
    }
    
    public g c(final int n) {
        ArrayList<g> list;
        if (this.d) {
            list = this.a.getNonActionItems();
        }
        else {
            list = this.a.getVisibleItems();
        }
        final int b = this.b;
        int index = n;
        if (b >= 0 && (index = n) >= b) {
            index = n + 1;
        }
        return list.get(index);
    }
    
    public void d(final boolean c) {
        this.c = c;
    }
    
    public int getCount() {
        ArrayList<g> list;
        if (this.d) {
            list = this.a.getNonActionItems();
        }
        else {
            list = this.a.getVisibleItems();
        }
        final int b = this.b;
        final int size = list.size();
        if (b < 0) {
            return size;
        }
        return size - 1;
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        View inflate = view;
        if (view == null) {
            inflate = this.e.inflate(this.f, viewGroup, false);
        }
        final int groupId = this.c(n).getGroupId();
        final int n2 = n - 1;
        int groupId2;
        if (n2 >= 0) {
            groupId2 = this.c(n2).getGroupId();
        }
        else {
            groupId2 = groupId;
        }
        final ListMenuItemView listMenuItemView = (ListMenuItemView)inflate;
        listMenuItemView.setGroupDividerEnabled(this.a.isGroupDividerEnabled() && groupId != groupId2);
        final j.a a = (j.a)inflate;
        if (this.c) {
            listMenuItemView.setForceShowIcon(true);
        }
        a.initialize(this.c(n), 0);
        return inflate;
    }
    
    public void notifyDataSetChanged() {
        this.a();
        super.notifyDataSetChanged();
    }
}

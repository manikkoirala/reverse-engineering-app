// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.os.Parcelable;
import android.content.Context;

public interface i
{
    boolean collapseItemActionView(final e p0, final g p1);
    
    boolean expandItemActionView(final e p0, final g p1);
    
    boolean flagActionItems();
    
    int getId();
    
    void initForMenu(final Context p0, final e p1);
    
    void onCloseMenu(final e p0, final boolean p1);
    
    void onRestoreInstanceState(final Parcelable p0);
    
    Parcelable onSaveInstanceState();
    
    boolean onSubMenuSelected(final l p0);
    
    void setCallback(final a p0);
    
    void updateMenuView(final boolean p0);
    
    public interface a
    {
        boolean a(final e p0);
        
        void onCloseMenu(final e p0, final boolean p1);
    }
}

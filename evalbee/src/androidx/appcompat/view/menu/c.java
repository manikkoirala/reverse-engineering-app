// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.widget.AbsListView;
import java.util.ArrayList;
import android.widget.BaseAdapter;
import android.os.IBinder;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.view.ContextThemeWrapper;
import android.util.SparseArray;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.view.LayoutInflater;
import android.content.Context;
import android.widget.AdapterView$OnItemClickListener;

public class c implements i, AdapterView$OnItemClickListener
{
    public Context a;
    public LayoutInflater b;
    public e c;
    public ExpandedMenuView d;
    public int e;
    public int f;
    public int g;
    public i.a h;
    public a i;
    public int j;
    
    public c(final int g, final int f) {
        this.g = g;
        this.f = f;
    }
    
    public c(final Context a, final int n) {
        this(n, 0);
        this.a = a;
        this.b = LayoutInflater.from(a);
    }
    
    public ListAdapter a() {
        if (this.i == null) {
            this.i = new a();
        }
        return (ListAdapter)this.i;
    }
    
    public j b(final ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView)this.b.inflate(ob1.g, viewGroup, false);
            if (this.i == null) {
                this.i = new a();
            }
            ((AbsListView)this.d).setAdapter((ListAdapter)this.i);
            ((AdapterView)this.d).setOnItemClickListener((AdapterView$OnItemClickListener)this);
        }
        return this.d;
    }
    
    public void c(final Bundle bundle) {
        final SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            ((View)this.d).restoreHierarchyState(sparseParcelableArray);
        }
    }
    
    @Override
    public boolean collapseItemActionView(final e e, final g g) {
        return false;
    }
    
    public void d(final Bundle bundle) {
        final SparseArray sparseArray = new SparseArray();
        final ExpandedMenuView d = this.d;
        if (d != null) {
            ((View)d).saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }
    
    @Override
    public boolean expandItemActionView(final e e, final g g) {
        return false;
    }
    
    @Override
    public boolean flagActionItems() {
        return false;
    }
    
    @Override
    public int getId() {
        return this.j;
    }
    
    @Override
    public void initForMenu(final Context a, final e c) {
        Label_0065: {
            LayoutInflater b;
            if (this.f != 0) {
                final ContextThemeWrapper a2 = new ContextThemeWrapper(a, this.f);
                this.a = (Context)a2;
                b = LayoutInflater.from((Context)a2);
            }
            else {
                if (this.a == null) {
                    break Label_0065;
                }
                this.a = a;
                if (this.b != null) {
                    break Label_0065;
                }
                b = LayoutInflater.from(a);
            }
            this.b = b;
        }
        this.c = c;
        final a i = this.i;
        if (i != null) {
            i.notifyDataSetChanged();
        }
    }
    
    @Override
    public void onCloseMenu(final e e, final boolean b) {
        final i.a h = this.h;
        if (h != null) {
            h.onCloseMenu(e, b);
        }
    }
    
    public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
        this.c.performItemAction((MenuItem)this.i.b(n), this, 0);
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        this.c((Bundle)parcelable);
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        if (this.d == null) {
            return null;
        }
        final Bundle bundle = new Bundle();
        this.d(bundle);
        return (Parcelable)bundle;
    }
    
    @Override
    public boolean onSubMenuSelected(final l l) {
        if (!l.hasVisibleItems()) {
            return false;
        }
        new f(l).c(null);
        final i.a h = this.h;
        if (h != null) {
            h.a(l);
        }
        return true;
    }
    
    @Override
    public void setCallback(final i.a h) {
        this.h = h;
    }
    
    @Override
    public void updateMenuView(final boolean b) {
        final a i = this.i;
        if (i != null) {
            i.notifyDataSetChanged();
        }
    }
    
    public class a extends BaseAdapter
    {
        public int a;
        public final c b;
        
        public a(final c b) {
            this.b = b;
            this.a = -1;
            this.a();
        }
        
        public void a() {
            final g expandedItem = this.b.c.getExpandedItem();
            if (expandedItem != null) {
                final ArrayList<g> nonActionItems = this.b.c.getNonActionItems();
                for (int size = nonActionItems.size(), i = 0; i < size; ++i) {
                    if (nonActionItems.get(i) == expandedItem) {
                        this.a = i;
                        return;
                    }
                }
            }
            this.a = -1;
        }
        
        public g b(int index) {
            final ArrayList<g> nonActionItems = this.b.c.getNonActionItems();
            final int n = index + this.b.e;
            final int a = this.a;
            index = n;
            if (a >= 0 && (index = n) >= a) {
                index = n + 1;
            }
            return nonActionItems.get(index);
        }
        
        public int getCount() {
            final int n = this.b.c.getNonActionItems().size() - this.b.e;
            if (this.a < 0) {
                return n;
            }
            return n - 1;
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View inflate = view;
            if (view == null) {
                final c b = this.b;
                inflate = b.b.inflate(b.g, viewGroup, false);
            }
            ((j.a)inflate).initialize(this.b(n), 0);
            return inflate;
        }
        
        public void notifyDataSetChanged() {
            this.a();
            super.notifyDataSetChanged();
        }
    }
}

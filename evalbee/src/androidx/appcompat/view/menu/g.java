// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.view.KeyEvent;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.view.LayoutInflater;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.content.res.Resources;
import android.view.ViewConfiguration;
import android.view.SubMenu;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.MenuItem$OnMenuItemClickListener;
import android.graphics.drawable.Drawable;
import android.content.Intent;
import android.view.ContextMenu$ContextMenuInfo;
import android.view.MenuItem$OnActionExpandListener;
import android.view.View;

public final class g implements ms1
{
    public View A;
    public d2 B;
    public MenuItem$OnActionExpandListener C;
    public boolean D;
    public ContextMenu$ContextMenuInfo E;
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public CharSequence e;
    public CharSequence f;
    public Intent g;
    public char h;
    public int i;
    public char j;
    public int k;
    public Drawable l;
    public int m;
    public e n;
    public l o;
    public Runnable p;
    public MenuItem$OnMenuItemClickListener q;
    public CharSequence r;
    public CharSequence s;
    public ColorStateList t;
    public PorterDuff$Mode u;
    public boolean v;
    public boolean w;
    public boolean x;
    public int y;
    public int z;
    
    public g(final e n, final int b, final int a, final int c, final int d, final CharSequence e, final int z) {
        this.i = 4096;
        this.k = 4096;
        this.m = 0;
        this.t = null;
        this.u = null;
        this.v = false;
        this.w = false;
        this.x = false;
        this.y = 16;
        this.D = false;
        this.n = n;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.z = z;
    }
    
    public static void d(final StringBuilder sb, final int n, final int n2, final String str) {
        if ((n & n2) == n2) {
            sb.append(str);
        }
    }
    
    public boolean A() {
        return this.n.isShortcutsVisible() && this.g() != '\0';
    }
    
    public boolean B() {
        return (this.z & 0x4) == 0x4;
    }
    
    @Override
    public ms1 a(d2 b) {
        final d2 b2 = this.B;
        if (b2 != null) {
            b2.g();
        }
        this.A = null;
        this.B = b;
        this.n.onItemsChanged(true);
        b = this.B;
        if (b != null) {
            b.i((d2.b)new d2.b(this) {
                public final g a;
                
                @Override
                public void onActionProviderVisibilityChanged(final boolean b) {
                    final g a = this.a;
                    a.n.onItemVisibleChanged(a);
                }
            });
        }
        return this;
    }
    
    @Override
    public d2 b() {
        return this.B;
    }
    
    public void c() {
        this.n.onItemActionRequestChanged(this);
    }
    
    @Override
    public boolean collapseActionView() {
        if ((this.z & 0x8) == 0x0) {
            return false;
        }
        if (this.A == null) {
            return true;
        }
        final MenuItem$OnActionExpandListener c = this.C;
        return (c == null || c.onMenuItemActionCollapse((MenuItem)this)) && this.n.collapseItemActionView(this);
    }
    
    public final Drawable e(final Drawable drawable) {
        Drawable mutate = drawable;
        if (drawable != null) {
            mutate = drawable;
            if (this.x) {
                if (!this.v) {
                    mutate = drawable;
                    if (!this.w) {
                        return mutate;
                    }
                }
                mutate = wu.r(drawable).mutate();
                if (this.v) {
                    wu.o(mutate, this.t);
                }
                if (this.w) {
                    wu.p(mutate, this.u);
                }
                this.x = false;
            }
        }
        return mutate;
    }
    
    @Override
    public boolean expandActionView() {
        if (!this.j()) {
            return false;
        }
        final MenuItem$OnActionExpandListener c = this.C;
        return (c == null || c.onMenuItemActionExpand((MenuItem)this)) && this.n.expandItemActionView(this);
    }
    
    public int f() {
        return this.d;
    }
    
    public char g() {
        char c;
        if (this.n.isQwertyMode()) {
            c = this.j;
        }
        else {
            c = this.h;
        }
        return c;
    }
    
    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }
    
    @Override
    public View getActionView() {
        final View a = this.A;
        if (a != null) {
            return a;
        }
        final d2 b = this.B;
        if (b != null) {
            return this.A = b.c((MenuItem)this);
        }
        return null;
    }
    
    @Override
    public int getAlphabeticModifiers() {
        return this.k;
    }
    
    public char getAlphabeticShortcut() {
        return this.j;
    }
    
    @Override
    public CharSequence getContentDescription() {
        return this.r;
    }
    
    public int getGroupId() {
        return this.b;
    }
    
    public Drawable getIcon() {
        final Drawable l = this.l;
        if (l != null) {
            return this.e(l);
        }
        if (this.m != 0) {
            final Drawable b = g7.b(this.n.getContext(), this.m);
            this.m = 0;
            this.l = b;
            return this.e(b);
        }
        return null;
    }
    
    @Override
    public ColorStateList getIconTintList() {
        return this.t;
    }
    
    @Override
    public PorterDuff$Mode getIconTintMode() {
        return this.u;
    }
    
    public Intent getIntent() {
        return this.g;
    }
    
    public int getItemId() {
        return this.a;
    }
    
    public ContextMenu$ContextMenuInfo getMenuInfo() {
        return this.E;
    }
    
    @Override
    public int getNumericModifiers() {
        return this.i;
    }
    
    public char getNumericShortcut() {
        return this.h;
    }
    
    public int getOrder() {
        return this.c;
    }
    
    public SubMenu getSubMenu() {
        return (SubMenu)this.o;
    }
    
    public CharSequence getTitle() {
        return this.e;
    }
    
    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f;
        if (charSequence == null) {
            charSequence = this.e;
        }
        return charSequence;
    }
    
    @Override
    public CharSequence getTooltipText() {
        return this.s;
    }
    
    public String h() {
        final char g = this.g();
        if (g == '\0') {
            return "";
        }
        final Resources resources = this.n.getContext().getResources();
        final StringBuilder sb = new StringBuilder();
        if (ViewConfiguration.get(this.n.getContext()).hasPermanentMenuKey()) {
            sb.append(resources.getString(qb1.m));
        }
        int n;
        if (this.n.isQwertyMode()) {
            n = this.k;
        }
        else {
            n = this.i;
        }
        d(sb, n, 65536, resources.getString(qb1.i));
        d(sb, n, 4096, resources.getString(qb1.e));
        d(sb, n, 2, resources.getString(qb1.d));
        d(sb, n, 1, resources.getString(qb1.j));
        d(sb, n, 4, resources.getString(qb1.l));
        d(sb, n, 8, resources.getString(qb1.h));
        int n2;
        if (g != '\b') {
            if (g != '\n') {
                if (g != ' ') {
                    sb.append(g);
                    return sb.toString();
                }
                n2 = qb1.k;
            }
            else {
                n2 = qb1.g;
            }
        }
        else {
            n2 = qb1.f;
        }
        sb.append(resources.getString(n2));
        return sb.toString();
    }
    
    public boolean hasSubMenu() {
        return this.o != null;
    }
    
    public CharSequence i(final j.a a) {
        CharSequence charSequence;
        if (a != null && a.prefersCondensedTitle()) {
            charSequence = this.getTitleCondensed();
        }
        else {
            charSequence = this.getTitle();
        }
        return charSequence;
    }
    
    @Override
    public boolean isActionViewExpanded() {
        return this.D;
    }
    
    public boolean isCheckable() {
        final int y = this.y;
        boolean b = true;
        if ((y & 0x1) != 0x1) {
            b = false;
        }
        return b;
    }
    
    public boolean isChecked() {
        return (this.y & 0x2) == 0x2;
    }
    
    public boolean isEnabled() {
        return (this.y & 0x10) != 0x0;
    }
    
    public boolean isVisible() {
        final d2 b = this.B;
        final boolean b2 = true;
        boolean b3 = true;
        if (b != null && b.f()) {
            if ((this.y & 0x8) != 0x0 || !this.B.b()) {
                b3 = false;
            }
            return b3;
        }
        return (this.y & 0x8) == 0x0 && b2;
    }
    
    public boolean j() {
        final int z = this.z;
        boolean b = false;
        if ((z & 0x8) != 0x0) {
            if (this.A == null) {
                final d2 b2 = this.B;
                if (b2 != null) {
                    this.A = b2.c((MenuItem)this);
                }
            }
            b = b;
            if (this.A != null) {
                b = true;
            }
        }
        return b;
    }
    
    public boolean k() {
        final MenuItem$OnMenuItemClickListener q = this.q;
        if (q != null && q.onMenuItemClick((MenuItem)this)) {
            return true;
        }
        final e n = this.n;
        if (n.dispatchMenuItemSelected(n, (MenuItem)this)) {
            return true;
        }
        final Runnable p = this.p;
        if (p != null) {
            p.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.n.getContext().startActivity(this.g);
                return true;
            }
            catch (final ActivityNotFoundException ex) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", (Throwable)ex);
            }
        }
        final d2 b = this.B;
        return b != null && b.d();
    }
    
    public boolean l() {
        return (this.y & 0x20) == 0x20;
    }
    
    public boolean m() {
        return (this.y & 0x4) != 0x0;
    }
    
    public boolean n() {
        final int z = this.z;
        boolean b = true;
        if ((z & 0x1) != 0x1) {
            b = false;
        }
        return b;
    }
    
    public boolean o() {
        return (this.z & 0x2) == 0x2;
    }
    
    public ms1 p(final int n) {
        final Context context = this.n.getContext();
        this.q(LayoutInflater.from(context).inflate(n, (ViewGroup)new LinearLayout(context), false));
        return this;
    }
    
    public ms1 q(final View a) {
        this.A = a;
        this.B = null;
        if (a != null && a.getId() == -1) {
            final int a2 = this.a;
            if (a2 > 0) {
                a.setId(a2);
            }
        }
        this.n.onItemActionRequestChanged(this);
        return this;
    }
    
    public void r(final boolean d) {
        this.D = d;
        this.n.onItemsChanged(false);
    }
    
    public void s(final boolean b) {
        final int y = this.y;
        int n;
        if (b) {
            n = 2;
        }
        else {
            n = 0;
        }
        final int y2 = n | (y & 0xFFFFFFFD);
        this.y = y2;
        if (y != y2) {
            this.n.onItemsChanged(false);
        }
    }
    
    public MenuItem setActionProvider(final ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }
    
    public MenuItem setAlphabeticShortcut(final char ch) {
        if (this.j == ch) {
            return (MenuItem)this;
        }
        this.j = Character.toLowerCase(ch);
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public MenuItem setAlphabeticShortcut(final char ch, final int n) {
        if (this.j == ch && this.k == n) {
            return (MenuItem)this;
        }
        this.j = Character.toLowerCase(ch);
        this.k = KeyEvent.normalizeMetaState(n);
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    public MenuItem setCheckable(final boolean b) {
        final int y = this.y;
        final int y2 = (b ? 1 : 0) | (y & 0xFFFFFFFE);
        this.y = y2;
        if (y != y2) {
            this.n.onItemsChanged(false);
        }
        return (MenuItem)this;
    }
    
    public MenuItem setChecked(final boolean b) {
        if ((this.y & 0x4) != 0x0) {
            this.n.setExclusiveItemChecked((MenuItem)this);
        }
        else {
            this.s(b);
        }
        return (MenuItem)this;
    }
    
    @Override
    public ms1 setContentDescription(final CharSequence r) {
        this.r = r;
        this.n.onItemsChanged(false);
        return this;
    }
    
    public MenuItem setEnabled(final boolean b) {
        int y;
        if (b) {
            y = (this.y | 0x10);
        }
        else {
            y = (this.y & 0xFFFFFFEF);
        }
        this.y = y;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    public MenuItem setIcon(final int m) {
        this.l = null;
        this.m = m;
        this.x = true;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    public MenuItem setIcon(final Drawable l) {
        this.m = 0;
        this.l = l;
        this.x = true;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public MenuItem setIconTintList(final ColorStateList t) {
        this.t = t;
        this.v = true;
        this.x = true;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public MenuItem setIconTintMode(final PorterDuff$Mode u) {
        this.u = u;
        this.w = true;
        this.x = true;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    public MenuItem setIntent(final Intent g) {
        this.g = g;
        return (MenuItem)this;
    }
    
    public MenuItem setNumericShortcut(final char h) {
        if (this.h == h) {
            return (MenuItem)this;
        }
        this.h = h;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public MenuItem setNumericShortcut(final char h, final int n) {
        if (this.h == h && this.i == n) {
            return (MenuItem)this;
        }
        this.h = h;
        this.i = KeyEvent.normalizeMetaState(n);
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    public MenuItem setOnActionExpandListener(final MenuItem$OnActionExpandListener c) {
        this.C = c;
        return (MenuItem)this;
    }
    
    public MenuItem setOnMenuItemClickListener(final MenuItem$OnMenuItemClickListener q) {
        this.q = q;
        return (MenuItem)this;
    }
    
    public MenuItem setShortcut(final char h, final char ch) {
        this.h = h;
        this.j = Character.toLowerCase(ch);
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public MenuItem setShortcut(final char h, final char ch, final int n, final int n2) {
        this.h = h;
        this.i = KeyEvent.normalizeMetaState(n);
        this.j = Character.toLowerCase(ch);
        this.k = KeyEvent.normalizeMetaState(n2);
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public void setShowAsAction(final int z) {
        final int n = z & 0x3;
        if (n != 0 && n != 1 && n != 2) {
            throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
        this.z = z;
        this.n.onItemActionRequestChanged(this);
    }
    
    public MenuItem setTitle(final int n) {
        return this.setTitle(this.n.getContext().getString(n));
    }
    
    public MenuItem setTitle(final CharSequence charSequence) {
        this.e = charSequence;
        this.n.onItemsChanged(false);
        final l o = this.o;
        if (o != null) {
            o.setHeaderTitle(charSequence);
        }
        return (MenuItem)this;
    }
    
    public MenuItem setTitleCondensed(final CharSequence f) {
        this.f = f;
        this.n.onItemsChanged(false);
        return (MenuItem)this;
    }
    
    @Override
    public ms1 setTooltipText(final CharSequence s) {
        this.s = s;
        this.n.onItemsChanged(false);
        return this;
    }
    
    public MenuItem setVisible(final boolean b) {
        if (this.y(b)) {
            this.n.onItemVisibleChanged(this);
        }
        return (MenuItem)this;
    }
    
    public void t(final boolean b) {
        final int y = this.y;
        int n;
        if (b) {
            n = 4;
        }
        else {
            n = 0;
        }
        this.y = (n | (y & 0xFFFFFFFB));
    }
    
    @Override
    public String toString() {
        final CharSequence e = this.e;
        String string;
        if (e != null) {
            string = e.toString();
        }
        else {
            string = null;
        }
        return string;
    }
    
    public void u(final boolean b) {
        int y;
        if (b) {
            y = (this.y | 0x20);
        }
        else {
            y = (this.y & 0xFFFFFFDF);
        }
        this.y = y;
    }
    
    public void v(final ContextMenu$ContextMenuInfo e) {
        this.E = e;
    }
    
    public ms1 w(final int showAsAction) {
        this.setShowAsAction(showAsAction);
        return this;
    }
    
    public void x(final l o) {
        (this.o = o).setHeaderTitle(this.getTitle());
    }
    
    public boolean y(final boolean b) {
        final int y = this.y;
        final boolean b2 = false;
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 8;
        }
        final int y2 = n | (y & 0xFFFFFFF7);
        this.y = y2;
        boolean b3 = b2;
        if (y != y2) {
            b3 = true;
        }
        return b3;
    }
    
    public boolean z() {
        return this.n.getOptionalIconsVisible();
    }
}

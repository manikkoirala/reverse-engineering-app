// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

import android.widget.Adapter;
import android.view.ViewGroup;
import android.view.View;
import android.widget.AdapterView;
import android.view.MenuItem;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements b, j, AdapterView$OnItemClickListener
{
    public static final int[] c;
    public e a;
    public int b;
    
    static {
        c = new int[] { 16842964, 16843049 };
    }
    
    public ExpandedMenuView(final Context context, final AttributeSet set) {
        this(context, set, 16842868);
    }
    
    public ExpandedMenuView(final Context context, final AttributeSet set, final int n) {
        super(context, set);
        ((AdapterView)this).setOnItemClickListener((AdapterView$OnItemClickListener)this);
        final tw1 v = tw1.v(context, set, ExpandedMenuView.c, n, 0);
        if (v.s(0)) {
            ((View)this).setBackgroundDrawable(v.g(0));
        }
        if (v.s(1)) {
            this.setDivider(v.g(1));
        }
        v.w();
    }
    
    public boolean c(final g g) {
        return this.a.performItemAction((MenuItem)g, 0);
    }
    
    public int getWindowAnimations() {
        return this.b;
    }
    
    public void initialize(final e a) {
        this.a = a;
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ((ViewGroup)this).setChildrenDrawingCacheEnabled(false);
    }
    
    public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
        this.c((g)((Adapter)this.getAdapter()).getItem(n));
    }
}

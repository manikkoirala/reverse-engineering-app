// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.accessibility.AccessibilityRecord;
import android.widget.LinearLayout$LayoutParams;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityEvent;
import android.graphics.Canvas;
import android.view.View$MeasureSpec;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;

public abstract class b extends ViewGroup
{
    private static final String ACCESSIBILITY_CLASS_NAME = "androidx.appcompat.widget.LinearLayoutCompat";
    public static final int HORIZONTAL = 0;
    private static final int INDEX_BOTTOM = 2;
    private static final int INDEX_CENTER_VERTICAL = 0;
    private static final int INDEX_FILL = 3;
    private static final int INDEX_TOP = 1;
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    public static final int VERTICAL = 1;
    private static final int VERTICAL_GRAVITY_COUNT = 4;
    private boolean mBaselineAligned;
    private int mBaselineAlignedChildIndex;
    private int mBaselineChildTop;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;
    private int mGravity;
    private int[] mMaxAscent;
    private int[] mMaxDescent;
    private int mOrientation;
    private int mShowDividers;
    private int mTotalLength;
    private boolean mUseLargestChild;
    private float mWeightSum;
    
    public b(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public b(final Context context, final AttributeSet set, int n) {
        super(context, set, n);
        this.mBaselineAligned = true;
        this.mBaselineAlignedChildIndex = -1;
        this.mBaselineChildTop = 0;
        this.mGravity = 8388659;
        final int[] i1 = bc1.i1;
        final tw1 v = tw1.v(context, set, i1, n, 0);
        o32.o0((View)this, context, i1, set, v.r(), n, 0);
        n = v.k(bc1.k1, -1);
        if (n >= 0) {
            this.setOrientation(n);
        }
        n = v.k(bc1.j1, -1);
        if (n >= 0) {
            this.setGravity(n);
        }
        final boolean a = v.a(bc1.l1, true);
        if (!a) {
            this.setBaselineAligned(a);
        }
        this.mWeightSum = v.i(bc1.n1, -1.0f);
        this.mBaselineAlignedChildIndex = v.k(bc1.m1, -1);
        this.mUseLargestChild = v.a(bc1.q1, false);
        this.setDividerDrawable(v.g(bc1.o1));
        this.mShowDividers = v.k(bc1.r1, 0);
        this.mDividerPadding = v.f(bc1.p1, 0);
        v.w();
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof a;
    }
    
    public final void d(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredHeight(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild.getVisibility() != 8) {
                final a a = (a)virtualChild.getLayoutParams();
                if (a.height == -1) {
                    final int width = a.width;
                    a.width = virtualChild.getMeasuredWidth();
                    this.measureChildWithMargins(virtualChild, n2, 0, measureSpec, 0);
                    a.width = width;
                }
            }
        }
    }
    
    public void drawDividersHorizontal(final Canvas canvas) {
        final int virtualChildCount = this.getVirtualChildCount();
        final boolean b = u42.b((View)this);
        for (int i = 0; i < virtualChildCount; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild != null && virtualChild.getVisibility() != 8 && this.hasDividerBeforeChildAt(i)) {
                final a a = (a)virtualChild.getLayoutParams();
                int n;
                if (b) {
                    n = virtualChild.getRight() + a.rightMargin;
                }
                else {
                    n = virtualChild.getLeft() - a.leftMargin - this.mDividerWidth;
                }
                this.drawVerticalDivider(canvas, n);
            }
        }
        if (this.hasDividerBeforeChildAt(virtualChildCount)) {
            final View virtualChild2 = this.getVirtualChildAt(virtualChildCount - 1);
            int paddingLeft = 0;
            Label_0210: {
                int n2;
                int n3;
                if (virtualChild2 == null) {
                    if (b) {
                        paddingLeft = ((View)this).getPaddingLeft();
                        break Label_0210;
                    }
                    n2 = ((View)this).getWidth();
                    n3 = ((View)this).getPaddingRight();
                }
                else {
                    final a a2 = (a)virtualChild2.getLayoutParams();
                    if (!b) {
                        paddingLeft = virtualChild2.getRight() + a2.rightMargin;
                        break Label_0210;
                    }
                    n2 = virtualChild2.getLeft();
                    n3 = a2.leftMargin;
                }
                paddingLeft = n2 - n3 - this.mDividerWidth;
            }
            this.drawVerticalDivider(canvas, paddingLeft);
        }
    }
    
    public void drawDividersVertical(final Canvas canvas) {
        final int virtualChildCount = this.getVirtualChildCount();
        for (int i = 0; i < virtualChildCount; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild != null && virtualChild.getVisibility() != 8 && this.hasDividerBeforeChildAt(i)) {
                this.drawHorizontalDivider(canvas, virtualChild.getTop() - ((a)virtualChild.getLayoutParams()).topMargin - this.mDividerHeight);
            }
        }
        if (this.hasDividerBeforeChildAt(virtualChildCount)) {
            final View virtualChild2 = this.getVirtualChildAt(virtualChildCount - 1);
            int n;
            if (virtualChild2 == null) {
                n = ((View)this).getHeight() - ((View)this).getPaddingBottom() - this.mDividerHeight;
            }
            else {
                n = virtualChild2.getBottom() + ((a)virtualChild2.getLayoutParams()).bottomMargin;
            }
            this.drawHorizontalDivider(canvas, n);
        }
    }
    
    public void drawHorizontalDivider(final Canvas canvas, final int n) {
        this.mDivider.setBounds(((View)this).getPaddingLeft() + this.mDividerPadding, n, ((View)this).getWidth() - ((View)this).getPaddingRight() - this.mDividerPadding, this.mDividerHeight + n);
        this.mDivider.draw(canvas);
    }
    
    public void drawVerticalDivider(final Canvas canvas, final int n) {
        this.mDivider.setBounds(n, ((View)this).getPaddingTop() + this.mDividerPadding, this.mDividerWidth + n, ((View)this).getHeight() - ((View)this).getPaddingBottom() - this.mDividerPadding);
        this.mDivider.draw(canvas);
    }
    
    public final void e(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredWidth(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild.getVisibility() != 8) {
                final a a = (a)virtualChild.getLayoutParams();
                if (a.width == -1) {
                    final int height = a.height;
                    a.height = virtualChild.getMeasuredHeight();
                    this.measureChildWithMargins(virtualChild, measureSpec, 0, n2, 0);
                    a.height = height;
                }
            }
        }
    }
    
    public final void f(final View view, final int n, final int n2, final int n3, final int n4) {
        view.layout(n, n2, n3 + n, n4 + n2);
    }
    
    public a generateDefaultLayoutParams() {
        final int mOrientation = this.mOrientation;
        if (mOrientation == 0) {
            return new a(-2, -2);
        }
        if (mOrientation == 1) {
            return new a(-1, -2);
        }
        return null;
    }
    
    public a generateLayoutParams(final AttributeSet set) {
        return new a(((View)this).getContext(), set);
    }
    
    public a generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return new a(viewGroup$LayoutParams);
    }
    
    public int getBaseline() {
        if (this.mBaselineAlignedChildIndex < 0) {
            return super.getBaseline();
        }
        final int childCount = this.getChildCount();
        final int mBaselineAlignedChildIndex = this.mBaselineAlignedChildIndex;
        if (childCount <= mBaselineAlignedChildIndex) {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
        final View child = this.getChildAt(mBaselineAlignedChildIndex);
        final int baseline = child.getBaseline();
        if (baseline != -1) {
            int mBaselineChildTop;
            final int n = mBaselineChildTop = this.mBaselineChildTop;
            if (this.mOrientation == 1) {
                final int n2 = this.mGravity & 0x70;
                mBaselineChildTop = n;
                if (n2 != 48) {
                    if (n2 != 16) {
                        if (n2 != 80) {
                            mBaselineChildTop = n;
                        }
                        else {
                            mBaselineChildTop = ((View)this).getBottom() - ((View)this).getTop() - ((View)this).getPaddingBottom() - this.mTotalLength;
                        }
                    }
                    else {
                        mBaselineChildTop = n + (((View)this).getBottom() - ((View)this).getTop() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom() - this.mTotalLength) / 2;
                    }
                }
            }
            return mBaselineChildTop + ((a)child.getLayoutParams()).topMargin + baseline;
        }
        if (this.mBaselineAlignedChildIndex == 0) {
            return -1;
        }
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
    }
    
    public int getBaselineAlignedChildIndex() {
        return this.mBaselineAlignedChildIndex;
    }
    
    public int getChildrenSkipCount(final View view, final int n) {
        return 0;
    }
    
    public Drawable getDividerDrawable() {
        return this.mDivider;
    }
    
    public int getDividerPadding() {
        return this.mDividerPadding;
    }
    
    public int getDividerWidth() {
        return this.mDividerWidth;
    }
    
    public int getGravity() {
        return this.mGravity;
    }
    
    public int getLocationOffset(final View view) {
        return 0;
    }
    
    public int getNextLocationOffset(final View view) {
        return 0;
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public int getShowDividers() {
        return this.mShowDividers;
    }
    
    public View getVirtualChildAt(final int n) {
        return this.getChildAt(n);
    }
    
    public int getVirtualChildCount() {
        return this.getChildCount();
    }
    
    public float getWeightSum() {
        return this.mWeightSum;
    }
    
    public boolean hasDividerBeforeChildAt(int n) {
        boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        if (n == 0) {
            boolean b4 = b3;
            if ((this.mShowDividers & 0x1) != 0x0) {
                b4 = true;
            }
            return b4;
        }
        if (n == this.getChildCount()) {
            if ((this.mShowDividers & 0x4) != 0x0) {
                b = true;
            }
            return b;
        }
        boolean b5 = b2;
        if ((this.mShowDividers & 0x2) != 0x0) {
            --n;
            while (true) {
                b5 = b2;
                if (n < 0) {
                    break;
                }
                if (this.getChildAt(n).getVisibility() != 8) {
                    b5 = true;
                    break;
                }
                --n;
            }
        }
        return b5;
    }
    
    public boolean isBaselineAligned() {
        return this.mBaselineAligned;
    }
    
    public boolean isMeasureWithLargestChildEnabled() {
        return this.mUseLargestChild;
    }
    
    public void layoutHorizontal(int n, int n2, int n3, int n4) {
        final boolean b = u42.b((View)this);
        final int paddingTop = ((View)this).getPaddingTop();
        final int n5 = n4 - n2;
        final int paddingBottom = ((View)this).getPaddingBottom();
        final int paddingBottom2 = ((View)this).getPaddingBottom();
        final int virtualChildCount = this.getVirtualChildCount();
        n2 = this.mGravity;
        n4 = (n2 & 0x70);
        final boolean mBaselineAligned = this.mBaselineAligned;
        final int[] mMaxAscent = this.mMaxAscent;
        final int[] mMaxDescent = this.mMaxDescent;
        n2 = pb0.b(0x800007 & n2, o32.B((View)this));
        if (n2 != 1) {
            if (n2 != 5) {
                n2 = ((View)this).getPaddingLeft();
            }
            else {
                n2 = ((View)this).getPaddingLeft() + n3 - n - this.mTotalLength;
            }
        }
        else {
            n2 = ((View)this).getPaddingLeft() + (n3 - n - this.mTotalLength) / 2;
        }
        int n6;
        int n7;
        if (b) {
            n6 = virtualChildCount - 1;
            n7 = -1;
        }
        else {
            n6 = 0;
            n7 = 1;
        }
        int i = 0;
        n3 = n4;
        n4 = paddingTop;
        while (i < virtualChildCount) {
            final int n8 = n6 + n7 * i;
            final View virtualChild = this.getVirtualChildAt(n8);
            if (virtualChild == null) {
                n2 += this.measureNullChild(n8);
            }
            else if (virtualChild.getVisibility() != 8) {
                final int measuredWidth = virtualChild.getMeasuredWidth();
                final int measuredHeight = virtualChild.getMeasuredHeight();
                final a a = (a)virtualChild.getLayoutParams();
                int baseline;
                if (mBaselineAligned && a.height != -1) {
                    baseline = virtualChild.getBaseline();
                }
                else {
                    baseline = -1;
                }
                if ((n = a.gravity) < 0) {
                    n = n3;
                }
                n &= 0x70;
                if (n != 16) {
                    if (n != 48) {
                        if (n != 80) {
                            n = n4;
                        }
                        else {
                            final int n9 = n = n5 - paddingBottom - measuredHeight - a.bottomMargin;
                            if (baseline != -1) {
                                n = virtualChild.getMeasuredHeight();
                                n = n9 - (mMaxDescent[2] - (n - baseline));
                            }
                        }
                    }
                    else {
                        final int n10 = n = a.topMargin + n4;
                        if (baseline != -1) {
                            n = n10 + (mMaxAscent[1] - baseline);
                        }
                    }
                }
                else {
                    n = (n5 - paddingTop - paddingBottom2 - measuredHeight) / 2 + n4 + a.topMargin - a.bottomMargin;
                }
                int n11 = n2;
                if (this.hasDividerBeforeChildAt(n8)) {
                    n11 = n2 + this.mDividerWidth;
                }
                n2 = a.leftMargin + n11;
                this.f(virtualChild, n2 + this.getLocationOffset(virtualChild), n, measuredWidth, measuredHeight);
                final int rightMargin = a.rightMargin;
                n = this.getNextLocationOffset(virtualChild);
                i += this.getChildrenSkipCount(virtualChild, n8);
                n2 += measuredWidth + rightMargin + n;
            }
            ++i;
        }
    }
    
    public void layoutVertical(int paddingTop, int i, int bottomMargin, int n) {
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int n2 = bottomMargin - paddingTop;
        final int paddingRight = ((View)this).getPaddingRight();
        final int paddingRight2 = ((View)this).getPaddingRight();
        final int virtualChildCount = this.getVirtualChildCount();
        final int mGravity = this.mGravity;
        paddingTop = (mGravity & 0x70);
        if (paddingTop != 16) {
            if (paddingTop != 80) {
                paddingTop = ((View)this).getPaddingTop();
            }
            else {
                paddingTop = ((View)this).getPaddingTop() + n - i - this.mTotalLength;
            }
        }
        else {
            paddingTop = ((View)this).getPaddingTop() + (n - i - this.mTotalLength) / 2;
        }
        View virtualChild;
        int measuredWidth;
        int measuredHeight;
        a a;
        for (i = 0; i < virtualChildCount; ++i) {
            virtualChild = this.getVirtualChildAt(i);
            if (virtualChild == null) {
                bottomMargin = paddingTop + this.measureNullChild(i);
            }
            else {
                bottomMargin = paddingTop;
                if (virtualChild.getVisibility() != 8) {
                    measuredWidth = virtualChild.getMeasuredWidth();
                    measuredHeight = virtualChild.getMeasuredHeight();
                    a = (a)virtualChild.getLayoutParams();
                    n = a.gravity;
                    if ((bottomMargin = n) < 0) {
                        bottomMargin = (mGravity & 0x800007);
                    }
                    bottomMargin = (pb0.b(bottomMargin, o32.B((View)this)) & 0x7);
                    Label_0262: {
                        if (bottomMargin != 1) {
                            if (bottomMargin != 5) {
                                bottomMargin = a.leftMargin + paddingLeft;
                                break Label_0262;
                            }
                            bottomMargin = n2 - paddingRight - measuredWidth;
                        }
                        else {
                            bottomMargin = (n2 - paddingLeft - paddingRight2 - measuredWidth) / 2 + paddingLeft + a.leftMargin;
                        }
                        bottomMargin -= a.rightMargin;
                    }
                    n = paddingTop;
                    if (this.hasDividerBeforeChildAt(i)) {
                        n = paddingTop + this.mDividerHeight;
                    }
                    paddingTop = n + a.topMargin;
                    this.f(virtualChild, bottomMargin, paddingTop + this.getLocationOffset(virtualChild), measuredWidth, measuredHeight);
                    bottomMargin = a.bottomMargin;
                    n = this.getNextLocationOffset(virtualChild);
                    i += this.getChildrenSkipCount(virtualChild, i);
                    paddingTop += measuredHeight + bottomMargin + n;
                    continue;
                }
            }
            paddingTop = bottomMargin;
        }
    }
    
    public void measureChildBeforeLayout(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        this.measureChildWithMargins(view, n2, n3, n4, n5);
    }
    
    public void measureHorizontal(final int n, final int n2) {
        this.mTotalLength = 0;
        final int virtualChildCount = this.getVirtualChildCount();
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        if (this.mMaxAscent == null || this.mMaxDescent == null) {
            this.mMaxAscent = new int[4];
            this.mMaxDescent = new int[4];
        }
        final int[] mMaxAscent = this.mMaxAscent;
        final int[] mMaxDescent = this.mMaxDescent;
        mMaxAscent[2] = (mMaxAscent[3] = -1);
        mMaxAscent[0] = (mMaxAscent[1] = -1);
        mMaxDescent[2] = (mMaxDescent[3] = -1);
        mMaxDescent[0] = (mMaxDescent[1] = -1);
        final boolean mBaselineAligned = this.mBaselineAligned;
        final boolean mUseLargestChild = this.mUseLargestChild;
        int n3 = 1073741824;
        final boolean b = mode == 1073741824;
        int i = 0;
        int b2 = 0;
        int max2;
        int max = max2 = b2;
        int n5;
        int n4 = n5 = max2;
        int n7;
        int n6 = n7 = n5;
        int n8 = 1;
        float n9 = 0.0f;
        while (i < virtualChildCount) {
            final View virtualChild = this.getVirtualChildAt(i);
            int n14 = 0;
            int n15 = 0;
            int n16 = 0;
            Label_0864: {
                if (virtualChild == null) {
                    this.mTotalLength += this.measureNullChild(i);
                }
                else {
                    if (virtualChild.getVisibility() != 8) {
                        if (this.hasDividerBeforeChildAt(i)) {
                            this.mTotalLength += this.mDividerWidth;
                        }
                        final a a = (a)virtualChild.getLayoutParams();
                        final float weight = a.weight;
                        n9 += weight;
                        Label_0594: {
                            int max4;
                            if (mode == n3 && a.width == 0 && weight > 0.0f) {
                                final int mTotalLength = this.mTotalLength;
                                int max3;
                                if (b) {
                                    max3 = mTotalLength + (a.leftMargin + a.rightMargin);
                                }
                                else {
                                    max3 = Math.max(mTotalLength, a.leftMargin + mTotalLength + a.rightMargin);
                                }
                                this.mTotalLength = max3;
                                if (!mBaselineAligned) {
                                    n5 = 1;
                                    break Label_0594;
                                }
                                final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
                                virtualChild.measure(measureSpec, measureSpec);
                                max4 = b2;
                            }
                            else {
                                int width;
                                if (a.width == 0 && weight > 0.0f) {
                                    a.width = -2;
                                    width = 0;
                                }
                                else {
                                    width = Integer.MIN_VALUE;
                                }
                                int mTotalLength2;
                                if (n9 == 0.0f) {
                                    mTotalLength2 = this.mTotalLength;
                                }
                                else {
                                    mTotalLength2 = 0;
                                }
                                this.measureChildBeforeLayout(virtualChild, i, n, mTotalLength2, n2, 0);
                                if (width != Integer.MIN_VALUE) {
                                    a.width = width;
                                }
                                final int measuredWidth = virtualChild.getMeasuredWidth();
                                int max5;
                                if (b) {
                                    max5 = this.mTotalLength + (a.leftMargin + measuredWidth + a.rightMargin + this.getNextLocationOffset(virtualChild));
                                }
                                else {
                                    final int mTotalLength3 = this.mTotalLength;
                                    max5 = Math.max(mTotalLength3, mTotalLength3 + measuredWidth + a.leftMargin + a.rightMargin + this.getNextLocationOffset(virtualChild));
                                }
                                this.mTotalLength = max5;
                                max4 = b2;
                                if (mUseLargestChild) {
                                    max4 = Math.max(measuredWidth, b2);
                                }
                            }
                            b2 = max4;
                        }
                        final int n10 = 1073741824;
                        boolean b3;
                        if (mode2 != 1073741824 && a.height == -1) {
                            b3 = true;
                            n7 = 1;
                        }
                        else {
                            b3 = false;
                        }
                        int n11 = a.topMargin + a.bottomMargin;
                        final int b4 = virtualChild.getMeasuredHeight() + n11;
                        final int combineMeasuredStates = View.combineMeasuredStates(n6, virtualChild.getMeasuredState());
                        if (mBaselineAligned) {
                            final int baseline = virtualChild.getBaseline();
                            if (baseline != -1) {
                                int n12;
                                if ((n12 = a.gravity) < 0) {
                                    n12 = this.mGravity;
                                }
                                final int n13 = ((n12 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
                                mMaxAscent[n13] = Math.max(mMaxAscent[n13], baseline);
                                mMaxDescent[n13] = Math.max(mMaxDescent[n13], b4 - baseline);
                            }
                        }
                        max = Math.max(max, b4);
                        if (n8 != 0 && a.height == -1) {
                            n8 = 1;
                        }
                        else {
                            n8 = 0;
                        }
                        int max6;
                        if (a.weight > 0.0f) {
                            if (!b3) {
                                n11 = b4;
                            }
                            max6 = Math.max(n4, n11);
                        }
                        else {
                            if (!b3) {
                                n11 = b4;
                            }
                            max2 = Math.max(max2, n11);
                            max6 = n4;
                        }
                        n14 = this.getChildrenSkipCount(virtualChild, i) + i;
                        n15 = combineMeasuredStates;
                        n4 = max6;
                        n16 = n10;
                        break Label_0864;
                    }
                    i += this.getChildrenSkipCount(virtualChild, i);
                }
                final int n17 = i;
                n16 = n3;
                n14 = n17;
                n15 = n6;
            }
            final int n18 = n16;
            i = n14 + 1;
            n3 = n18;
            n6 = n15;
        }
        if (this.mTotalLength > 0 && this.hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerWidth;
        }
        final int a2 = mMaxAscent[1];
        int max7;
        if (a2 == -1 && mMaxAscent[0] == -1 && mMaxAscent[2] == -1 && mMaxAscent[3] == -1) {
            max7 = max;
        }
        else {
            max7 = Math.max(max, Math.max(mMaxAscent[3], Math.max(mMaxAscent[0], Math.max(a2, mMaxAscent[2]))) + Math.max(mMaxDescent[3], Math.max(mMaxDescent[0], Math.max(mMaxDescent[1], mMaxDescent[2]))));
        }
        int n19 = n6;
        int n20 = max7;
        Label_1213: {
            if (mUseLargestChild) {
                if (mode != Integer.MIN_VALUE) {
                    n20 = max7;
                    if (mode != 0) {
                        break Label_1213;
                    }
                }
                this.mTotalLength = 0;
                int n21 = 0;
                while (true) {
                    n20 = max7;
                    if (n21 >= virtualChildCount) {
                        break;
                    }
                    final View virtualChild2 = this.getVirtualChildAt(n21);
                    if (virtualChild2 == null) {
                        this.mTotalLength += this.measureNullChild(n21);
                    }
                    else if (virtualChild2.getVisibility() == 8) {
                        n21 += this.getChildrenSkipCount(virtualChild2, n21);
                    }
                    else {
                        final a a3 = (a)virtualChild2.getLayoutParams();
                        final int mTotalLength4 = this.mTotalLength;
                        if (b) {
                            this.mTotalLength = mTotalLength4 + (a3.leftMargin + b2 + a3.rightMargin + this.getNextLocationOffset(virtualChild2));
                        }
                        else {
                            this.mTotalLength = Math.max(mTotalLength4, mTotalLength4 + b2 + a3.leftMargin + a3.rightMargin + this.getNextLocationOffset(virtualChild2));
                        }
                    }
                    ++n21;
                }
            }
        }
        final int n22 = this.mTotalLength + (((View)this).getPaddingLeft() + ((View)this).getPaddingRight());
        this.mTotalLength = n22;
        final int resolveSizeAndState = View.resolveSizeAndState(Math.max(n22, ((View)this).getSuggestedMinimumWidth()), n, 0);
        final int n23 = (0xFFFFFF & resolveSizeAndState) - this.mTotalLength;
        int n24;
        int max9;
        int n25;
        if (n5 == 0 && (n23 == 0 || n9 <= 0.0f)) {
            final int max8 = Math.max(max2, n4);
            if (mUseLargestChild && mode != 1073741824) {
                for (int j = 0; j < virtualChildCount; ++j) {
                    final View virtualChild3 = this.getVirtualChildAt(j);
                    if (virtualChild3 != null) {
                        if (virtualChild3.getVisibility() != 8) {
                            if (((a)virtualChild3.getLayoutParams()).weight > 0.0f) {
                                virtualChild3.measure(View$MeasureSpec.makeMeasureSpec(b2, 1073741824), View$MeasureSpec.makeMeasureSpec(virtualChild3.getMeasuredHeight(), 1073741824));
                            }
                        }
                    }
                }
            }
            n24 = virtualChildCount;
            max9 = n20;
            n25 = max8;
        }
        else {
            final float mWeightSum = this.mWeightSum;
            if (mWeightSum > 0.0f) {
                n9 = mWeightSum;
            }
            mMaxAscent[2] = (mMaxAscent[3] = -1);
            mMaxAscent[0] = (mMaxAscent[1] = -1);
            mMaxDescent[2] = (mMaxDescent[3] = -1);
            mMaxDescent[0] = (mMaxDescent[1] = -1);
            this.mTotalLength = 0;
            int max10 = -1;
            final int n26 = n19;
            int k = 0;
            int n27 = n8;
            n24 = virtualChildCount;
            int combineMeasuredStates2 = n26;
            int max11 = max2;
            int n28 = n23;
            while (k < n24) {
                final View virtualChild4 = this.getVirtualChildAt(k);
                if (virtualChild4 != null) {
                    if (virtualChild4.getVisibility() != 8) {
                        final a a4 = (a)virtualChild4.getLayoutParams();
                        final float weight2 = a4.weight;
                        if (weight2 > 0.0f) {
                            final int n29 = (int)(n28 * weight2 / n9);
                            final int childMeasureSpec = ViewGroup.getChildMeasureSpec(n2, ((View)this).getPaddingTop() + ((View)this).getPaddingBottom() + a4.topMargin + a4.bottomMargin, a4.height);
                            int n30 = 0;
                            Label_1642: {
                                if (a4.width == 0 && mode == 1073741824) {
                                    if (n29 > 0) {
                                        n30 = n29;
                                        break Label_1642;
                                    }
                                }
                                else if ((n30 = virtualChild4.getMeasuredWidth() + n29) >= 0) {
                                    break Label_1642;
                                }
                                n30 = 0;
                            }
                            virtualChild4.measure(View$MeasureSpec.makeMeasureSpec(n30, 1073741824), childMeasureSpec);
                            combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates2, virtualChild4.getMeasuredState() & 0xFF000000);
                            n9 -= weight2;
                            n28 -= n29;
                        }
                        final int mTotalLength5 = this.mTotalLength;
                        if (b) {
                            this.mTotalLength = mTotalLength5 + (virtualChild4.getMeasuredWidth() + a4.leftMargin + a4.rightMargin + this.getNextLocationOffset(virtualChild4));
                        }
                        else {
                            this.mTotalLength = Math.max(mTotalLength5, virtualChild4.getMeasuredWidth() + mTotalLength5 + a4.leftMargin + a4.rightMargin + this.getNextLocationOffset(virtualChild4));
                        }
                        final boolean b5 = mode2 != 1073741824 && a4.height == -1;
                        final int n31 = a4.topMargin + a4.bottomMargin;
                        final int b6 = virtualChild4.getMeasuredHeight() + n31;
                        max10 = Math.max(max10, b6);
                        int b7;
                        if (b5) {
                            b7 = n31;
                        }
                        else {
                            b7 = b6;
                        }
                        max11 = Math.max(max11, b7);
                        final boolean b8 = n27 != 0 && a4.height == -1;
                        if (mBaselineAligned) {
                            final int baseline2 = virtualChild4.getBaseline();
                            if (baseline2 != -1) {
                                int n32;
                                if ((n32 = a4.gravity) < 0) {
                                    n32 = this.mGravity;
                                }
                                final int n33 = ((n32 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
                                mMaxAscent[n33] = Math.max(mMaxAscent[n33], baseline2);
                                mMaxDescent[n33] = Math.max(mMaxDescent[n33], b6 - baseline2);
                            }
                        }
                        n27 = (b8 ? 1 : 0);
                    }
                }
                ++k;
            }
            this.mTotalLength += ((View)this).getPaddingLeft() + ((View)this).getPaddingRight();
            final int a5 = mMaxAscent[1];
            if (a5 == -1 && mMaxAscent[0] == -1 && mMaxAscent[2] == -1 && mMaxAscent[3] == -1) {
                max9 = max10;
            }
            else {
                max9 = Math.max(max10, Math.max(mMaxAscent[3], Math.max(mMaxAscent[0], Math.max(a5, mMaxAscent[2]))) + Math.max(mMaxDescent[3], Math.max(mMaxDescent[0], Math.max(mMaxDescent[1], mMaxDescent[2]))));
            }
            n25 = max11;
            n19 = combineMeasuredStates2;
            n8 = n27;
        }
        if (n8 != 0 || mode2 == 1073741824) {
            n25 = max9;
        }
        ((View)this).setMeasuredDimension(resolveSizeAndState | (n19 & 0xFF000000), View.resolveSizeAndState(Math.max(n25 + (((View)this).getPaddingTop() + ((View)this).getPaddingBottom()), ((View)this).getSuggestedMinimumHeight()), n2, n19 << 16));
        if (n7 != 0) {
            this.d(n24, n);
        }
    }
    
    public int measureNullChild(final int n) {
        return 0;
    }
    
    public void measureVertical(final int n, final int n2) {
        this.mTotalLength = 0;
        final int virtualChildCount = this.getVirtualChildCount();
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        final int mBaselineAlignedChildIndex = this.mBaselineAlignedChildIndex;
        final boolean mUseLargestChild = this.mUseLargestChild;
        int n3 = 0;
        int n4 = 0;
        int max2;
        int max = max2 = n4;
        int i;
        int max3 = i = max2;
        int n6;
        int n5 = n6 = i;
        int n7 = 1;
        float n8 = 0.0f;
        while (i < virtualChildCount) {
            final View virtualChild = this.getVirtualChildAt(i);
            if (virtualChild == null) {
                this.mTotalLength += this.measureNullChild(i);
            }
            else if (virtualChild.getVisibility() == 8) {
                i += this.getChildrenSkipCount(virtualChild, i);
            }
            else {
                if (this.hasDividerBeforeChildAt(i)) {
                    this.mTotalLength += this.mDividerHeight;
                }
                final a a = (a)virtualChild.getLayoutParams();
                final float weight = a.weight;
                n8 += weight;
                if (mode2 == 1073741824 && a.height == 0 && weight > 0.0f) {
                    final int mTotalLength = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength, a.topMargin + mTotalLength + a.bottomMargin);
                    n5 = 1;
                }
                else {
                    int height;
                    if (a.height == 0 && weight > 0.0f) {
                        a.height = -2;
                        height = 0;
                    }
                    else {
                        height = Integer.MIN_VALUE;
                    }
                    int mTotalLength2;
                    if (n8 == 0.0f) {
                        mTotalLength2 = this.mTotalLength;
                    }
                    else {
                        mTotalLength2 = 0;
                    }
                    this.measureChildBeforeLayout(virtualChild, i, n, 0, n2, mTotalLength2);
                    if (height != Integer.MIN_VALUE) {
                        a.height = height;
                    }
                    final int measuredHeight = virtualChild.getMeasuredHeight();
                    final int mTotalLength3 = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength3, mTotalLength3 + measuredHeight + a.topMargin + a.bottomMargin + this.getNextLocationOffset(virtualChild));
                    if (mUseLargestChild) {
                        max = Math.max(measuredHeight, max);
                    }
                }
                if (mBaselineAlignedChildIndex >= 0 && mBaselineAlignedChildIndex == i + 1) {
                    this.mBaselineChildTop = this.mTotalLength;
                }
                if (i < mBaselineAlignedChildIndex && a.weight > 0.0f) {
                    throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                }
                boolean b;
                if (mode != 1073741824 && a.width == -1) {
                    b = true;
                    n6 = 1;
                }
                else {
                    b = false;
                }
                int n9 = a.leftMargin + a.rightMargin;
                final int b2 = virtualChild.getMeasuredWidth() + n9;
                n4 = Math.max(n4, b2);
                final int combineMeasuredStates = View.combineMeasuredStates(n3, virtualChild.getMeasuredState());
                int n10;
                if (n7 != 0 && a.width == -1) {
                    n10 = 1;
                }
                else {
                    n10 = 0;
                }
                int max4;
                if (a.weight > 0.0f) {
                    if (!b) {
                        n9 = b2;
                    }
                    max2 = Math.max(max2, n9);
                    max4 = max3;
                }
                else {
                    if (!b) {
                        n9 = b2;
                    }
                    max4 = Math.max(max3, n9);
                }
                final int childrenSkipCount = this.getChildrenSkipCount(virtualChild, i);
                max3 = max4;
                final int n11 = combineMeasuredStates;
                i += childrenSkipCount;
                n7 = n10;
                n3 = n11;
            }
            ++i;
        }
        if (this.mTotalLength > 0 && this.hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength += this.mDividerHeight;
        }
        if (mUseLargestChild && (mode2 == Integer.MIN_VALUE || mode2 == 0)) {
            this.mTotalLength = 0;
            for (int j = 0; j < virtualChildCount; ++j) {
                final View virtualChild2 = this.getVirtualChildAt(j);
                int max5;
                if (virtualChild2 == null) {
                    max5 = this.mTotalLength + this.measureNullChild(j);
                }
                else {
                    if (virtualChild2.getVisibility() == 8) {
                        j += this.getChildrenSkipCount(virtualChild2, j);
                        continue;
                    }
                    final a a2 = (a)virtualChild2.getLayoutParams();
                    final int mTotalLength4 = this.mTotalLength;
                    max5 = Math.max(mTotalLength4, mTotalLength4 + max + a2.topMargin + a2.bottomMargin + this.getNextLocationOffset(virtualChild2));
                }
                this.mTotalLength = max5;
            }
        }
        final int n12 = this.mTotalLength + (((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
        this.mTotalLength = n12;
        final int resolveSizeAndState = View.resolveSizeAndState(Math.max(n12, ((View)this).getSuggestedMinimumHeight()), n2, 0);
        final int n13 = (0xFFFFFF & resolveSizeAndState) - this.mTotalLength;
        int n14;
        if (n5 == 0 && (n13 == 0 || n8 <= 0.0f)) {
            final int max6 = Math.max(max3, max2);
            if (mUseLargestChild && mode2 != 1073741824) {
                for (int k = 0; k < virtualChildCount; ++k) {
                    final View virtualChild3 = this.getVirtualChildAt(k);
                    if (virtualChild3 != null) {
                        if (virtualChild3.getVisibility() != 8) {
                            if (((a)virtualChild3.getLayoutParams()).weight > 0.0f) {
                                virtualChild3.measure(View$MeasureSpec.makeMeasureSpec(virtualChild3.getMeasuredWidth(), 1073741824), View$MeasureSpec.makeMeasureSpec(max, 1073741824));
                            }
                        }
                    }
                }
            }
            n14 = max6;
        }
        else {
            final float mWeightSum = this.mWeightSum;
            if (mWeightSum > 0.0f) {
                n8 = mWeightSum;
            }
            this.mTotalLength = 0;
            final int n15 = n13;
            int combineMeasuredStates2 = n3;
            final int n16 = 0;
            int n17 = n15;
            for (int l = n16; l < virtualChildCount; ++l) {
                final View virtualChild4 = this.getVirtualChildAt(l);
                if (virtualChild4.getVisibility() != 8) {
                    final a a3 = (a)virtualChild4.getLayoutParams();
                    final float weight2 = a3.weight;
                    if (weight2 > 0.0f) {
                        final int n18 = (int)(n17 * weight2 / n8);
                        final int paddingLeft = ((View)this).getPaddingLeft();
                        final int paddingRight = ((View)this).getPaddingRight();
                        final int leftMargin = a3.leftMargin;
                        final int rightMargin = a3.rightMargin;
                        final int width = a3.width;
                        final int n19 = n17 - n18;
                        final int childMeasureSpec = ViewGroup.getChildMeasureSpec(n, paddingLeft + paddingRight + leftMargin + rightMargin, width);
                        int n20 = 0;
                        Label_1197: {
                            if (a3.height == 0 && mode2 == 1073741824) {
                                if (n18 > 0) {
                                    n20 = n18;
                                    break Label_1197;
                                }
                            }
                            else if ((n20 = virtualChild4.getMeasuredHeight() + n18) >= 0) {
                                break Label_1197;
                            }
                            n20 = 0;
                        }
                        virtualChild4.measure(childMeasureSpec, View$MeasureSpec.makeMeasureSpec(n20, 1073741824));
                        combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates2, virtualChild4.getMeasuredState() & 0xFFFFFF00);
                        n8 -= weight2;
                        n17 = n19;
                    }
                    final int n21 = a3.leftMargin + a3.rightMargin;
                    final int b3 = virtualChild4.getMeasuredWidth() + n21;
                    n4 = Math.max(n4, b3);
                    int b4;
                    if (mode != 1073741824 && a3.width == -1) {
                        b4 = n21;
                    }
                    else {
                        b4 = b3;
                    }
                    max3 = Math.max(max3, b4);
                    if (n7 != 0 && a3.width == -1) {
                        n7 = 1;
                    }
                    else {
                        n7 = 0;
                    }
                    final int mTotalLength5 = this.mTotalLength;
                    this.mTotalLength = Math.max(mTotalLength5, virtualChild4.getMeasuredHeight() + mTotalLength5 + a3.topMargin + a3.bottomMargin + this.getNextLocationOffset(virtualChild4));
                }
            }
            this.mTotalLength += ((View)this).getPaddingTop() + ((View)this).getPaddingBottom();
            n3 = combineMeasuredStates2;
            n14 = max3;
        }
        if (n7 != 0 || mode == 1073741824) {
            n14 = n4;
        }
        ((View)this).setMeasuredDimension(View.resolveSizeAndState(Math.max(n14 + (((View)this).getPaddingLeft() + ((View)this).getPaddingRight()), ((View)this).getSuggestedMinimumWidth()), n, n3), resolveSizeAndState);
        if (n6 != 0) {
            this.e(virtualChildCount, n2);
        }
    }
    
    public void onDraw(final Canvas canvas) {
        if (this.mDivider == null) {
            return;
        }
        if (this.mOrientation == 1) {
            this.drawDividersVertical(canvas);
        }
        else {
            this.drawDividersHorizontal(canvas);
        }
    }
    
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)"androidx.appcompat.widget.LinearLayoutCompat");
    }
    
    public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName((CharSequence)"androidx.appcompat.widget.LinearLayoutCompat");
    }
    
    public void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        if (this.mOrientation == 1) {
            this.layoutVertical(n, n2, n3, n4);
        }
        else {
            this.layoutHorizontal(n, n2, n3, n4);
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        if (this.mOrientation == 1) {
            this.measureVertical(n, n2);
        }
        else {
            this.measureHorizontal(n, n2);
        }
    }
    
    public void setBaselineAligned(final boolean mBaselineAligned) {
        this.mBaselineAligned = mBaselineAligned;
    }
    
    public void setBaselineAlignedChildIndex(final int mBaselineAlignedChildIndex) {
        if (mBaselineAlignedChildIndex >= 0 && mBaselineAlignedChildIndex < this.getChildCount()) {
            this.mBaselineAlignedChildIndex = mBaselineAlignedChildIndex;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("base aligned child index out of range (0, ");
        sb.append(this.getChildCount());
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void setDividerDrawable(final Drawable mDivider) {
        if (mDivider == this.mDivider) {
            return;
        }
        this.mDivider = mDivider;
        boolean willNotDraw = false;
        if (mDivider != null) {
            this.mDividerWidth = mDivider.getIntrinsicWidth();
            this.mDividerHeight = mDivider.getIntrinsicHeight();
        }
        else {
            this.mDividerWidth = 0;
            this.mDividerHeight = 0;
        }
        if (mDivider == null) {
            willNotDraw = true;
        }
        ((View)this).setWillNotDraw(willNotDraw);
        ((View)this).requestLayout();
    }
    
    public void setDividerPadding(final int mDividerPadding) {
        this.mDividerPadding = mDividerPadding;
    }
    
    public void setGravity(int mGravity) {
        if (this.mGravity != mGravity) {
            int n = mGravity;
            if ((0x800007 & mGravity) == 0x0) {
                n = (mGravity | 0x800003);
            }
            mGravity = n;
            if ((n & 0x70) == 0x0) {
                mGravity = (n | 0x30);
            }
            this.mGravity = mGravity;
            ((View)this).requestLayout();
        }
    }
    
    public void setHorizontalGravity(int n) {
        n &= 0x800007;
        final int mGravity = this.mGravity;
        if ((0x800007 & mGravity) != n) {
            this.mGravity = (n | (0xFF7FFFF8 & mGravity));
            ((View)this).requestLayout();
        }
    }
    
    public void setMeasureWithLargestChildEnabled(final boolean mUseLargestChild) {
        this.mUseLargestChild = mUseLargestChild;
    }
    
    public void setOrientation(final int mOrientation) {
        if (this.mOrientation != mOrientation) {
            this.mOrientation = mOrientation;
            ((View)this).requestLayout();
        }
    }
    
    public void setShowDividers(final int mShowDividers) {
        if (mShowDividers != this.mShowDividers) {
            ((View)this).requestLayout();
        }
        this.mShowDividers = mShowDividers;
    }
    
    public void setVerticalGravity(int n) {
        n &= 0x70;
        final int mGravity = this.mGravity;
        if ((mGravity & 0x70) != n) {
            this.mGravity = (n | (mGravity & 0xFFFFFF8F));
            ((View)this).requestLayout();
        }
    }
    
    public void setWeightSum(final float b) {
        this.mWeightSum = Math.max(0.0f, b);
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    public static class a extends LinearLayout$LayoutParams
    {
        public a(final int n, final int n2) {
            super(n, n2);
        }
        
        public a(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        public a(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
        }
    }
}

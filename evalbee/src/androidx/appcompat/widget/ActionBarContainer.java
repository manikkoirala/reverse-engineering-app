// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.ViewGroup;
import android.view.ActionMode;
import android.view.ActionMode$Callback;
import android.graphics.drawable.Drawable$Callback;
import android.view.View$MeasureSpec;
import android.view.MotionEvent;
import android.widget.FrameLayout$LayoutParams;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;

public class ActionBarContainer extends FrameLayout
{
    public boolean a;
    public View b;
    public View c;
    public View d;
    public Drawable e;
    public Drawable f;
    public Drawable g;
    public boolean h;
    public boolean i;
    public int j;
    
    public ActionBarContainer(final Context context, final AttributeSet set) {
        super(context, set);
        o32.u0((View)this, new u1(this));
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, bc1.a);
        this.e = obtainStyledAttributes.getDrawable(bc1.b);
        this.f = obtainStyledAttributes.getDrawable(bc1.d);
        this.j = obtainStyledAttributes.getDimensionPixelSize(bc1.j, -1);
        final int id = ((View)this).getId();
        final int h = db1.H;
        boolean willNotDraw = true;
        if (id == h) {
            this.h = true;
            this.g = obtainStyledAttributes.getDrawable(bc1.c);
        }
        obtainStyledAttributes.recycle();
        Label_0137: {
            if (this.h) {
                if (this.g == null) {
                    break Label_0137;
                }
            }
            else if (this.e == null && this.f == null) {
                break Label_0137;
            }
            willNotDraw = false;
        }
        ((View)this).setWillNotDraw(willNotDraw);
    }
    
    public final int a(final View view) {
        final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)view.getLayoutParams();
        return view.getMeasuredHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin;
    }
    
    public final boolean b(final View view) {
        return view == null || view.getVisibility() == 8 || view.getMeasuredHeight() == 0;
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final Drawable e = this.e;
        if (e != null && e.isStateful()) {
            this.e.setState(((View)this).getDrawableState());
        }
        final Drawable f = this.f;
        if (f != null && f.isStateful()) {
            this.f.setState(((View)this).getDrawableState());
        }
        final Drawable g = this.g;
        if (g != null && g.isStateful()) {
            this.g.setState(((View)this).getDrawableState());
        }
    }
    
    public View getTabContainer() {
        return this.b;
    }
    
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        final Drawable e = this.e;
        if (e != null) {
            e.jumpToCurrentState();
        }
        final Drawable f = this.f;
        if (f != null) {
            f.jumpToCurrentState();
        }
        final Drawable g = this.g;
        if (g != null) {
            g.jumpToCurrentState();
        }
    }
    
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = ((View)this).findViewById(db1.a);
        this.d = ((View)this).findViewById(db1.f);
    }
    
    public boolean onHoverEvent(final MotionEvent motionEvent) {
        super.onHoverEvent(motionEvent);
        return true;
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        return this.a || super.onInterceptTouchEvent(motionEvent);
    }
    
    public void onLayout(final boolean b, int n, int n2, int n3, int n4) {
        super.onLayout(b, n, n2, n3, n4);
        final View b2 = this.b;
        n4 = 1;
        n2 = 0;
        final int n5 = 0;
        final boolean i = b2 != null && b2.getVisibility() != 8;
        if (b2 != null && b2.getVisibility() != 8) {
            final int measuredHeight = ((View)this).getMeasuredHeight();
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)b2.getLayoutParams();
            final int measuredHeight2 = b2.getMeasuredHeight();
            final int bottomMargin = frameLayout$LayoutParams.bottomMargin;
            b2.layout(n, measuredHeight - measuredHeight2 - bottomMargin, n3, measuredHeight - bottomMargin);
        }
        if (this.h) {
            final Drawable g = this.g;
            n = n5;
            if (g != null) {
                g.setBounds(0, 0, ((View)this).getMeasuredWidth(), ((View)this).getMeasuredHeight());
                n = n4;
            }
        }
        else {
            if (this.e != null) {
                Label_0299: {
                    Drawable drawable;
                    View view;
                    if (this.c.getVisibility() == 0) {
                        drawable = this.e;
                        n3 = this.c.getLeft();
                        n2 = this.c.getTop();
                        n = this.c.getRight();
                        view = this.c;
                    }
                    else {
                        final View d = this.d;
                        if (d == null || d.getVisibility() != 0) {
                            this.e.setBounds(0, 0, 0, 0);
                            break Label_0299;
                        }
                        drawable = this.e;
                        n3 = this.d.getLeft();
                        n2 = this.d.getTop();
                        n = this.d.getRight();
                        view = this.d;
                    }
                    drawable.setBounds(n3, n2, n, view.getBottom());
                }
                n2 = 1;
            }
            this.i = i;
            n = n2;
            if (i) {
                final Drawable f = this.f;
                n = n2;
                if (f != null) {
                    f.setBounds(b2.getLeft(), b2.getTop(), b2.getRight(), b2.getBottom());
                    n = n4;
                }
            }
        }
        if (n != 0) {
            ((View)this).invalidate();
        }
    }
    
    public void onMeasure(int a, int b) {
        int measureSpec = b;
        if (this.c == null) {
            measureSpec = b;
            if (View$MeasureSpec.getMode(b) == Integer.MIN_VALUE) {
                final int j = this.j;
                measureSpec = b;
                if (j >= 0) {
                    measureSpec = View$MeasureSpec.makeMeasureSpec(Math.min(j, View$MeasureSpec.getSize(b)), Integer.MIN_VALUE);
                }
            }
        }
        super.onMeasure(a, measureSpec);
        if (this.c == null) {
            return;
        }
        b = View$MeasureSpec.getMode(measureSpec);
        final View b2 = this.b;
        if (b2 != null && b2.getVisibility() != 8 && b != 1073741824) {
            Label_0143: {
                View view;
                if (!this.b(this.c)) {
                    view = this.c;
                }
                else {
                    if (this.b(this.d)) {
                        a = 0;
                        break Label_0143;
                    }
                    view = this.d;
                }
                a = this.a(view);
            }
            if (b == Integer.MIN_VALUE) {
                b = View$MeasureSpec.getSize(measureSpec);
            }
            else {
                b = Integer.MAX_VALUE;
            }
            ((View)this).setMeasuredDimension(((View)this).getMeasuredWidth(), Math.min(a + this.a(this.b), b));
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }
    
    public void setPrimaryBackground(final Drawable e) {
        final Drawable e2 = this.e;
        if (e2 != null) {
            e2.setCallback((Drawable$Callback)null);
            ((View)this).unscheduleDrawable(this.e);
        }
        if ((this.e = e) != null) {
            e.setCallback((Drawable$Callback)this);
            final View c = this.c;
            if (c != null) {
                this.e.setBounds(c.getLeft(), this.c.getTop(), this.c.getRight(), this.c.getBottom());
            }
        }
        final boolean h = this.h;
        boolean willNotDraw = true;
        Label_0120: {
            if (h) {
                if (this.g == null) {
                    break Label_0120;
                }
            }
            else if (this.e == null && this.f == null) {
                break Label_0120;
            }
            willNotDraw = false;
        }
        ((View)this).setWillNotDraw(willNotDraw);
        ((View)this).invalidate();
        ActionBarContainer.a.a(this);
    }
    
    public void setSplitBackground(Drawable g) {
        final Drawable g2 = this.g;
        if (g2 != null) {
            g2.setCallback((Drawable$Callback)null);
            ((View)this).unscheduleDrawable(this.g);
        }
        this.g = g;
        final boolean b = false;
        if (g != null) {
            g.setCallback((Drawable$Callback)this);
            if (this.h) {
                g = this.g;
                if (g != null) {
                    g.setBounds(0, 0, ((View)this).getMeasuredWidth(), ((View)this).getMeasuredHeight());
                }
            }
        }
        boolean willNotDraw = false;
        Label_0113: {
            if (this.h) {
                willNotDraw = b;
                if (this.g != null) {
                    break Label_0113;
                }
            }
            else {
                willNotDraw = b;
                if (this.e != null) {
                    break Label_0113;
                }
                willNotDraw = b;
                if (this.f != null) {
                    break Label_0113;
                }
            }
            willNotDraw = true;
        }
        ((View)this).setWillNotDraw(willNotDraw);
        ((View)this).invalidate();
        ActionBarContainer.a.a(this);
    }
    
    public void setStackedBackground(Drawable f) {
        final Drawable f2 = this.f;
        if (f2 != null) {
            f2.setCallback((Drawable$Callback)null);
            ((View)this).unscheduleDrawable(this.f);
        }
        if ((this.f = f) != null) {
            f.setCallback((Drawable$Callback)this);
            if (this.i) {
                f = this.f;
                if (f != null) {
                    f.setBounds(this.b.getLeft(), this.b.getTop(), this.b.getRight(), this.b.getBottom());
                }
            }
        }
        final boolean h = this.h;
        boolean willNotDraw = true;
        Label_0127: {
            if (h) {
                if (this.g == null) {
                    break Label_0127;
                }
            }
            else if (this.e == null && this.f == null) {
                break Label_0127;
            }
            willNotDraw = false;
        }
        ((View)this).setWillNotDraw(willNotDraw);
        ((View)this).invalidate();
        ActionBarContainer.a.a(this);
    }
    
    public void setTabContainer(final c b) {
        final View b2 = this.b;
        if (b2 != null) {
            ((ViewGroup)this).removeView(b2);
        }
        this.b = (View)b;
    }
    
    public void setTransitioning(final boolean a) {
        this.a = a;
        int descendantFocusability;
        if (a) {
            descendantFocusability = 393216;
        }
        else {
            descendantFocusability = 262144;
        }
        ((ViewGroup)this).setDescendantFocusability(descendantFocusability);
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        final boolean b = visibility == 0;
        final Drawable e = this.e;
        if (e != null) {
            e.setVisible(b, false);
        }
        final Drawable f = this.f;
        if (f != null) {
            f.setVisible(b, false);
        }
        final Drawable g = this.g;
        if (g != null) {
            g.setVisible(b, false);
        }
    }
    
    public ActionMode startActionModeForChild(final View view, final ActionMode$Callback actionMode$Callback) {
        return null;
    }
    
    public ActionMode startActionModeForChild(final View view, final ActionMode$Callback actionMode$Callback, final int n) {
        if (n != 0) {
            return super.startActionModeForChild(view, actionMode$Callback, n);
        }
        return null;
    }
    
    public boolean verifyDrawable(final Drawable drawable) {
        return (drawable == this.e && !this.h) || (drawable == this.f && this.i) || (drawable == this.g && this.h) || super.verifyDrawable(drawable);
    }
    
    public abstract static class a
    {
        public static void a(final ActionBarContainer actionBarContainer) {
            ((View)actionBarContainer).invalidateOutline();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$OnTouchListener;
import android.util.AttributeSet;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.h;
import android.content.res.Configuration;
import android.view.ViewParent;
import android.view.MenuItem;
import androidx.appcompat.view.menu.l;
import android.os.Parcelable;
import android.content.res.Resources;
import java.util.ArrayList;
import android.view.View$MeasureSpec;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.view.menu.g;
import android.view.View;
import androidx.appcompat.view.menu.j;
import androidx.appcompat.view.menu.e;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;

public class a extends a implements d2.a
{
    public int A;
    public final SparseBooleanArray C;
    public e D;
    public a F;
    public c G;
    public b H;
    public final f I;
    public int J;
    public d k;
    public Drawable l;
    public boolean m;
    public boolean n;
    public boolean p;
    public int q;
    public int t;
    public int v;
    public boolean w;
    public boolean x;
    public boolean y;
    public boolean z;
    
    public a(final Context context) {
        super(context, ob1.c, ob1.b);
        this.C = new SparseBooleanArray();
        this.I = new f();
    }
    
    public static /* synthetic */ androidx.appcompat.view.menu.e j(final a a) {
        return a.c;
    }
    
    public static /* synthetic */ androidx.appcompat.view.menu.e k(final a a) {
        return a.c;
    }
    
    public static /* synthetic */ j l(final a a) {
        return a.i;
    }
    
    public static /* synthetic */ androidx.appcompat.view.menu.e m(final a a) {
        return a.c;
    }
    
    public static /* synthetic */ androidx.appcompat.view.menu.e n(final a a) {
        return a.c;
    }
    
    public static /* synthetic */ androidx.appcompat.view.menu.e o(final a a) {
        return a.c;
    }
    
    public static /* synthetic */ j p(final a a) {
        return a.i;
    }
    
    public void A(final Drawable drawable) {
        final d k = this.k;
        if (k != null) {
            k.setImageDrawable(drawable);
        }
        else {
            this.m = true;
            this.l = drawable;
        }
    }
    
    public void B(final boolean n) {
        this.n = n;
        this.p = true;
    }
    
    public boolean C() {
        if (this.n && !this.w()) {
            final androidx.appcompat.view.menu.e c = super.c;
            if (c != null && super.i != null && this.G == null && !c.getNonActionItems().isEmpty()) {
                final c g = new c(new e(super.b, super.c, (View)this.k, true));
                this.G = g;
                ((View)super.i).post((Runnable)g);
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void b(final androidx.appcompat.view.menu.g g, final j.a a) {
        a.initialize(g, 0);
        final ActionMenuView itemInvoker = (ActionMenuView)super.i;
        final ActionMenuItemView actionMenuItemView = (ActionMenuItemView)a;
        actionMenuItemView.setItemInvoker(itemInvoker);
        if (this.H == null) {
            this.H = new b();
        }
        actionMenuItemView.setPopupCallback((ActionMenuItemView.b)this.H);
    }
    
    @Override
    public boolean d(final ViewGroup viewGroup, final int n) {
        return viewGroup.getChildAt(n) != this.k && super.d(viewGroup, n);
    }
    
    @Override
    public View f(final androidx.appcompat.view.menu.g g, final View view, final ViewGroup viewGroup) {
        View view2 = g.getActionView();
        if (view2 == null || g.j()) {
            view2 = super.f(g, view, viewGroup);
        }
        int visibility;
        if (g.isActionViewExpanded()) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        view2.setVisibility(visibility);
        final ActionMenuView actionMenuView = (ActionMenuView)viewGroup;
        final ViewGroup$LayoutParams layoutParams = view2.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            view2.setLayoutParams((ViewGroup$LayoutParams)actionMenuView.j(layoutParams));
        }
        return view2;
    }
    
    @Override
    public boolean flagActionItems() {
        final androidx.appcompat.view.menu.e c = super.c;
        final int n = 0;
        ArrayList<androidx.appcompat.view.menu.g> visibleItems;
        int size;
        if (c != null) {
            visibleItems = c.getVisibleItems();
            size = visibleItems.size();
        }
        else {
            visibleItems = null;
            size = 0;
        }
        final int v = this.v;
        final int t = this.t;
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
        final ViewGroup viewGroup = (ViewGroup)super.i;
        int i = 0;
        int n2 = 0;
        int n4;
        int n3 = n4 = n2;
        int n5 = v;
        while (i < size) {
            final androidx.appcompat.view.menu.g g = visibleItems.get(i);
            if (g.o()) {
                ++n4;
            }
            else if (g.n()) {
                ++n3;
            }
            else {
                n2 = 1;
            }
            int n6 = n5;
            if (this.z) {
                n6 = n5;
                if (g.isActionViewExpanded()) {
                    n6 = 0;
                }
            }
            ++i;
            n5 = n6;
        }
        int n7 = n5;
        if (this.n && (n2 != 0 || n3 + n4 > (n7 = n5))) {
            n7 = n5 - 1;
        }
        int n8 = n7 - n4;
        final SparseBooleanArray c2 = this.C;
        c2.clear();
        int n9;
        int n10;
        if (this.x) {
            final int a = this.A;
            n9 = t / a;
            n10 = a + t % a / n9;
        }
        else {
            n10 = 0;
            n9 = 0;
        }
        int j = 0;
        int n11 = 0;
        int n12 = t;
        final int n13 = size;
        int n14 = n;
        while (j < n13) {
            final androidx.appcompat.view.menu.g g2 = visibleItems.get(j);
            int n15;
            int n16;
            if (g2.o()) {
                final View f = this.f(g2, null, viewGroup);
                if (this.x) {
                    n9 -= ActionMenuView.q(f, n10, n9, measureSpec, n14);
                }
                else {
                    f.measure(measureSpec, measureSpec);
                }
                final int measuredWidth = f.getMeasuredWidth();
                n12 -= measuredWidth;
                n15 = n11;
                if (n11 == 0) {
                    n15 = measuredWidth;
                }
                final int groupId = g2.getGroupId();
                if (groupId != 0) {
                    c2.put(groupId, true);
                }
                g2.u(true);
                n16 = n14;
            }
            else if (g2.n()) {
                final int groupId2 = g2.getGroupId();
                final boolean value = c2.get(groupId2);
                boolean b3;
                boolean b2;
                final boolean b = b2 = (b3 = ((n8 > 0 || value) && n12 > 0 && (!this.x || n9 > 0)));
                int n17 = n12;
                int n18 = n9;
                int n19 = n11;
                if (b) {
                    final View f2 = this.f(g2, null, viewGroup);
                    if (this.x) {
                        final int q = ActionMenuView.q(f2, n10, n9, measureSpec, 0);
                        final int n20 = n9 -= q;
                        if (q == 0) {
                            b3 = false;
                            n9 = n20;
                        }
                    }
                    else {
                        f2.measure(measureSpec, measureSpec);
                    }
                    final int measuredWidth2 = f2.getMeasuredWidth();
                    n17 = n12 - measuredWidth2;
                    if ((n19 = n11) == 0) {
                        n19 = measuredWidth2;
                    }
                    b2 = (b3 & (this.x ? (n17 >= 0) : (n17 + n19 > 0)));
                    n18 = n9;
                }
                int n21;
                if (b2 && groupId2 != 0) {
                    c2.put(groupId2, true);
                    n21 = n8;
                }
                else {
                    n21 = n8;
                    if (value) {
                        c2.put(groupId2, false);
                        int index = 0;
                        while (true) {
                            n21 = n8;
                            if (index >= j) {
                                break;
                            }
                            final androidx.appcompat.view.menu.g g3 = visibleItems.get(index);
                            int n22 = n8;
                            if (g3.getGroupId() == groupId2) {
                                n22 = n8;
                                if (g3.l()) {
                                    n22 = n8 + 1;
                                }
                                g3.u(false);
                            }
                            ++index;
                            n8 = n22;
                        }
                    }
                }
                int n23 = n21;
                if (b2) {
                    n23 = n21 - 1;
                }
                g2.u(b2);
                n16 = 0;
                n8 = n23;
                n12 = n17;
                n9 = n18;
                n15 = n19;
            }
            else {
                g2.u((boolean)(n14 != 0));
                n15 = n11;
                n16 = n14;
            }
            ++j;
            n14 = n16;
            n11 = n15;
        }
        return true;
    }
    
    @Override
    public j g(final ViewGroup viewGroup) {
        final j i = super.i;
        final j g = super.g(viewGroup);
        if (i != g) {
            ((ActionMenuView)g).setPresenter(this);
        }
        return g;
    }
    
    @Override
    public boolean i(final int n, final androidx.appcompat.view.menu.g g) {
        return g.l();
    }
    
    @Override
    public void initForMenu(final Context context, final androidx.appcompat.view.menu.e e) {
        super.initForMenu(context, e);
        final Resources resources = context.getResources();
        final v1 b = v1.b(context);
        if (!this.p) {
            this.n = b.f();
        }
        if (!this.y) {
            this.q = b.c();
        }
        if (!this.w) {
            this.v = b.d();
        }
        int q = this.q;
        if (this.n) {
            if (this.k == null) {
                final d k = new d(super.a);
                this.k = k;
                if (this.m) {
                    k.setImageDrawable(this.l);
                    this.l = null;
                    this.m = false;
                }
                final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
                ((View)this.k).measure(measureSpec, measureSpec);
            }
            q -= ((View)this.k).getMeasuredWidth();
        }
        else {
            this.k = null;
        }
        this.t = q;
        this.A = (int)(resources.getDisplayMetrics().density * 56.0f);
    }
    
    @Override
    public void onCloseMenu(final androidx.appcompat.view.menu.e e, final boolean b) {
        this.q();
        super.onCloseMenu(e, b);
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof g)) {
            return;
        }
        final int a = ((g)parcelable).a;
        if (a > 0) {
            final MenuItem item = super.c.findItem(a);
            if (item != null) {
                this.onSubMenuSelected((l)item.getSubMenu());
            }
        }
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        final g g = new g();
        g.a = this.J;
        return (Parcelable)g;
    }
    
    @Override
    public boolean onSubMenuSelected(final l l) {
        final boolean hasVisibleItems = l.hasVisibleItems();
        final boolean b = false;
        if (!hasVisibleItems) {
            return false;
        }
        l i;
        for (i = l; i.getParentMenu() != super.c; i = (l)i.getParentMenu()) {}
        final View r = this.r(i.getItem());
        if (r == null) {
            return false;
        }
        this.J = l.getItem().getItemId();
        final int size = l.size();
        int n = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n >= size) {
                break;
            }
            final MenuItem item = l.getItem(n);
            if (item.isVisible() && item.getIcon() != null) {
                b2 = true;
                break;
            }
            ++n;
        }
        (this.F = new a(super.b, l, r)).g(b2);
        this.F.k();
        super.onSubMenuSelected(l);
        return true;
    }
    
    public boolean q() {
        return this.t() | this.u();
    }
    
    public final View r(final MenuItem menuItem) {
        final ViewGroup viewGroup = (ViewGroup)super.i;
        if (viewGroup == null) {
            return null;
        }
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(i);
            if (child instanceof j.a && ((j.a)child).getItemData() == menuItem) {
                return child;
            }
        }
        return null;
    }
    
    public Drawable s() {
        final d k = this.k;
        if (k != null) {
            return k.getDrawable();
        }
        if (this.m) {
            return this.l;
        }
        return null;
    }
    
    public boolean t() {
        final c g = this.G;
        if (g != null) {
            final j i = super.i;
            if (i != null) {
                ((View)i).removeCallbacks((Runnable)g);
                this.G = null;
                return true;
            }
        }
        final e d = this.D;
        if (d != null) {
            d.b();
            return true;
        }
        return false;
    }
    
    public boolean u() {
        final a f = this.F;
        if (f != null) {
            f.b();
            return true;
        }
        return false;
    }
    
    @Override
    public void updateMenuView(final boolean b) {
        super.updateMenuView(b);
        ((View)super.i).requestLayout();
        final androidx.appcompat.view.menu.e c = super.c;
        final boolean b2 = false;
        if (c != null) {
            final ArrayList<androidx.appcompat.view.menu.g> actionItems = c.getActionItems();
            for (int size = actionItems.size(), i = 0; i < size; ++i) {
                final d2 b3 = actionItems.get(i).b();
                if (b3 != null) {
                    b3.h((d2.a)this);
                }
            }
        }
        final androidx.appcompat.view.menu.e c2 = super.c;
        ArrayList<androidx.appcompat.view.menu.g> nonActionItems;
        if (c2 != null) {
            nonActionItems = c2.getNonActionItems();
        }
        else {
            nonActionItems = null;
        }
        int n = b2 ? 1 : 0;
        if (this.n) {
            n = (b2 ? 1 : 0);
            if (nonActionItems != null) {
                final int size2 = nonActionItems.size();
                if (size2 == 1) {
                    n = ((nonActionItems.get(0).isActionViewExpanded() ^ true) ? 1 : 0);
                }
                else {
                    n = (b2 ? 1 : 0);
                    if (size2 > 0) {
                        n = 1;
                    }
                }
            }
        }
        final d k = this.k;
        if (n != 0) {
            if (k == null) {
                this.k = new d(super.a);
            }
            final ViewGroup viewGroup = (ViewGroup)((View)this.k).getParent();
            if (viewGroup != super.i) {
                if (viewGroup != null) {
                    viewGroup.removeView((View)this.k);
                }
                final ActionMenuView actionMenuView = (ActionMenuView)super.i;
                actionMenuView.addView((View)this.k, (ViewGroup$LayoutParams)actionMenuView.k());
            }
        }
        else if (k != null) {
            final ViewParent parent = ((View)k).getParent();
            final j j = super.i;
            if (parent == j) {
                ((ViewGroup)j).removeView((View)this.k);
            }
        }
        ((ActionMenuView)super.i).setOverflowReserved(this.n);
    }
    
    public boolean v() {
        return this.G != null || this.w();
    }
    
    public boolean w() {
        final e d = this.D;
        return d != null && d.d();
    }
    
    public void x(final Configuration configuration) {
        if (!this.w) {
            this.v = v1.b(super.b).d();
        }
        final androidx.appcompat.view.menu.e c = super.c;
        if (c != null) {
            c.onItemsChanged(true);
        }
    }
    
    public void y(final boolean z) {
        this.z = z;
    }
    
    public void z(final ActionMenuView i) {
        ((ActionMenuView)(super.i = i)).initialize(super.c);
    }
    
    public class a extends h
    {
        public final androidx.appcompat.widget.a m;
        
        public a(final androidx.appcompat.widget.a m, final Context context, final l l, final View view) {
            this.m = m;
            super(context, l, view, false, sa1.i);
            if (!((androidx.appcompat.view.menu.g)l.getItem()).l()) {
                Object k;
                if ((k = m.k) == null) {
                    k = androidx.appcompat.widget.a.l(m);
                }
                this.f((View)k);
            }
            this.j(m.I);
        }
        
        @Override
        public void e() {
            final androidx.appcompat.widget.a m = this.m;
            m.F = null;
            m.J = 0;
            super.e();
        }
    }
    
    public class b extends ActionMenuItemView.b
    {
        public final a a;
        
        public b(final a a) {
            this.a = a;
        }
        
        @Override
        public wn1 a() {
            final a f = this.a.F;
            pv0 c;
            if (f != null) {
                c = f.c();
            }
            else {
                c = null;
            }
            return c;
        }
    }
    
    public class c implements Runnable
    {
        public e a;
        public final a b;
        
        public c(final a b, final e a) {
            this.b = b;
            this.a = a;
        }
        
        @Override
        public void run() {
            if (androidx.appcompat.widget.a.n(this.b) != null) {
                androidx.appcompat.widget.a.o(this.b).changeMenuMode();
            }
            final View view = (View)androidx.appcompat.widget.a.p(this.b);
            if (view != null && view.getWindowToken() != null && this.a.m()) {
                this.b.D = this.a;
            }
            this.b.G = null;
        }
    }
    
    public class d extends z6 implements ActionMenuView.a
    {
        public final a a;
        
        public d(final a a, final Context context) {
            this.a = a;
            super(context, null, sa1.h);
            ((View)this).setClickable(true);
            ((View)this).setFocusable(true);
            ((View)this).setVisibility(0);
            ((View)this).setEnabled(true);
            cx1.a((View)this, ((View)this).getContentDescription());
            ((View)this).setOnTouchListener((View$OnTouchListener)new w70(this, this, a) {
                public final a j;
                public final d k;
                
                @Override
                public wn1 b() {
                    final e d = this.k.a.D;
                    if (d == null) {
                        return null;
                    }
                    return d.c();
                }
                
                @Override
                public boolean c() {
                    this.k.a.C();
                    return true;
                }
                
                @Override
                public boolean d() {
                    final a a = this.k.a;
                    if (a.G != null) {
                        return false;
                    }
                    a.t();
                    return true;
                }
            });
        }
        
        @Override
        public boolean a() {
            return false;
        }
        
        @Override
        public boolean b() {
            return false;
        }
        
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            ((View)this).playSoundEffect(0);
            this.a.C();
            return true;
        }
        
        public boolean setFrame(int n, int paddingBottom, int height, int paddingTop) {
            final boolean setFrame = super.setFrame(n, paddingBottom, height, paddingTop);
            final Drawable drawable = this.getDrawable();
            final Drawable background = ((View)this).getBackground();
            if (drawable != null && background != null) {
                final int width = ((View)this).getWidth();
                height = ((View)this).getHeight();
                n = Math.max(width, height) / 2;
                final int paddingLeft = ((View)this).getPaddingLeft();
                final int paddingRight = ((View)this).getPaddingRight();
                paddingTop = ((View)this).getPaddingTop();
                paddingBottom = ((View)this).getPaddingBottom();
                final int n2 = (width + (paddingLeft - paddingRight)) / 2;
                paddingBottom = (height + (paddingTop - paddingBottom)) / 2;
                wu.l(background, n2 - n, paddingBottom - n, n2 + n, paddingBottom + n);
            }
            return setFrame;
        }
    }
    
    public class e extends h
    {
        public final a m;
        
        public e(final a m, final Context context, final androidx.appcompat.view.menu.e e, final View view, final boolean b) {
            this.m = m;
            super(context, e, view, b, sa1.i);
            this.h(8388613);
            this.j(m.I);
        }
        
        @Override
        public void e() {
            if (a.j(this.m) != null) {
                a.k(this.m).close();
            }
            this.m.D = null;
            super.e();
        }
    }
    
    public class f implements i.a
    {
        public final a a;
        
        public f(final a a) {
            this.a = a;
        }
        
        @Override
        public boolean a(final androidx.appcompat.view.menu.e e) {
            final androidx.appcompat.view.menu.e m = androidx.appcompat.widget.a.m(this.a);
            boolean a = false;
            if (e == m) {
                return false;
            }
            this.a.J = ((l)e).getItem().getItemId();
            final i.a e2 = this.a.e();
            if (e2 != null) {
                a = e2.a(e);
            }
            return a;
        }
        
        @Override
        public void onCloseMenu(final androidx.appcompat.view.menu.e e, final boolean b) {
            if (e instanceof l) {
                e.getRootMenu().close(false);
            }
            final i.a e2 = this.a.e();
            if (e2 != null) {
                e2.onCloseMenu(e, b);
            }
        }
    }
    
    public static class g implements Parcelable
    {
        public static final Parcelable$Creator<g> CREATOR;
        public int a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public g a(final Parcel parcel) {
                    return new g(parcel);
                }
                
                public g[] b(final int n) {
                    return new g[n];
                }
            };
        }
        
        public g() {
        }
        
        public g(final Parcel parcel) {
            this.a = parcel.readInt();
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.a);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.View;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;

public class AlertDialogLayout extends b
{
    public AlertDialogLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    private void e(final int n, final int n2) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredWidth(), 1073741824);
        for (int i = 0; i < n; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final a a = (a)child.getLayoutParams();
                if (a.width == -1) {
                    final int height = a.height;
                    a.height = child.getMeasuredHeight();
                    this.measureChildWithMargins(child, measureSpec, 0, n2, 0);
                    a.height = height;
                }
            }
        }
    }
    
    private void f(final View view, final int n, final int n2, final int n3, final int n4) {
        view.layout(n, n2, n3 + n, n4 + n2);
    }
    
    public static int g(final View view) {
        final int c = o32.C(view);
        if (c > 0) {
            return c;
        }
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            if (viewGroup.getChildCount() == 1) {
                return g(viewGroup.getChildAt(0));
            }
        }
        return 0;
    }
    
    public final boolean h(final int n, final int n2) {
        final int childCount = this.getChildCount();
        View view = null;
        View view2 = null;
        View view3 = null;
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final int id = child.getId();
                if (id == db1.P) {
                    view = child;
                }
                else if (id == db1.k) {
                    view2 = child;
                }
                else {
                    if (id != db1.m && id != db1.o) {
                        return false;
                    }
                    if (view3 != null) {
                        return false;
                    }
                    view3 = child;
                }
            }
        }
        final int mode = View$MeasureSpec.getMode(n2);
        final int size = View$MeasureSpec.getSize(n2);
        final int mode2 = View$MeasureSpec.getMode(n);
        int n3 = ((View)this).getPaddingTop() + ((View)this).getPaddingBottom();
        int n4;
        if (view != null) {
            view.measure(n, 0);
            n3 += view.getMeasuredHeight();
            n4 = View.combineMeasuredStates(0, view.getMeasuredState());
        }
        else {
            n4 = 0;
        }
        int g;
        int b;
        if (view2 != null) {
            view2.measure(n, 0);
            g = g(view2);
            b = view2.getMeasuredHeight() - g;
            n3 += g;
            n4 = View.combineMeasuredStates(n4, view2.getMeasuredState());
        }
        else {
            g = 0;
            b = 0;
        }
        int measuredHeight;
        if (view3 != null) {
            int measureSpec;
            if (mode == 0) {
                measureSpec = 0;
            }
            else {
                measureSpec = View$MeasureSpec.makeMeasureSpec(Math.max(0, size - n3), mode);
            }
            view3.measure(n, measureSpec);
            measuredHeight = view3.getMeasuredHeight();
            n3 += measuredHeight;
            n4 = View.combineMeasuredStates(n4, view3.getMeasuredState());
        }
        else {
            measuredHeight = 0;
        }
        final int a = size - n3;
        int n5 = n4;
        int n6 = a;
        int n7 = n3;
        if (view2 != null) {
            final int min = Math.min(a, b);
            int n8 = a;
            int n9 = g;
            if (min > 0) {
                n8 = a - min;
                n9 = g + min;
            }
            view2.measure(n, View$MeasureSpec.makeMeasureSpec(n9, 1073741824));
            n7 = n3 - g + view2.getMeasuredHeight();
            final int combineMeasuredStates = View.combineMeasuredStates(n4, view2.getMeasuredState());
            n6 = n8;
            n5 = combineMeasuredStates;
        }
        int combineMeasuredStates2 = n5;
        int n10 = n7;
        if (view3 != null) {
            combineMeasuredStates2 = n5;
            n10 = n7;
            if (n6 > 0) {
                view3.measure(n, View$MeasureSpec.makeMeasureSpec(measuredHeight + n6, mode));
                n10 = n7 - measuredHeight + view3.getMeasuredHeight();
                combineMeasuredStates2 = View.combineMeasuredStates(n5, view3.getMeasuredState());
            }
        }
        int j = 0;
        int a2 = 0;
        while (j < childCount) {
            final View child2 = this.getChildAt(j);
            int max = a2;
            if (child2.getVisibility() != 8) {
                max = Math.max(a2, child2.getMeasuredWidth());
            }
            ++j;
            a2 = max;
        }
        ((View)this).setMeasuredDimension(View.resolveSizeAndState(a2 + (((View)this).getPaddingLeft() + ((View)this).getPaddingRight()), n, combineMeasuredStates2), View.resolveSizeAndState(n10, n2, 0));
        if (mode2 != 1073741824) {
            this.e(childCount, n2);
        }
        return true;
    }
    
    @Override
    public void onLayout(final boolean b, int paddingTop, int intrinsicHeight, int i, int gravity) {
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int n = i - paddingTop;
        final int paddingRight = ((View)this).getPaddingRight();
        final int paddingRight2 = ((View)this).getPaddingRight();
        i = ((View)this).getMeasuredHeight();
        final int childCount = this.getChildCount();
        final int gravity2 = this.getGravity();
        paddingTop = (gravity2 & 0x70);
        if (paddingTop != 16) {
            if (paddingTop != 80) {
                paddingTop = ((View)this).getPaddingTop();
            }
            else {
                paddingTop = ((View)this).getPaddingTop() + gravity - intrinsicHeight - i;
            }
        }
        else {
            paddingTop = ((View)this).getPaddingTop() + (gravity - intrinsicHeight - i) / 2;
        }
        final Drawable dividerDrawable = this.getDividerDrawable();
        if (dividerDrawable == null) {
            intrinsicHeight = 0;
        }
        else {
            intrinsicHeight = dividerDrawable.getIntrinsicHeight();
        }
        View child;
        int measuredWidth;
        int measuredHeight;
        a a;
        int n2;
        for (i = 0; i < childCount; ++i, paddingTop = gravity) {
            child = this.getChildAt(i);
            gravity = paddingTop;
            if (child != null) {
                gravity = paddingTop;
                if (child.getVisibility() != 8) {
                    measuredWidth = child.getMeasuredWidth();
                    measuredHeight = child.getMeasuredHeight();
                    a = (a)child.getLayoutParams();
                    if ((gravity = a.gravity) < 0) {
                        gravity = (gravity2 & 0x800007);
                    }
                    gravity = (pb0.b(gravity, o32.B((View)this)) & 0x7);
                    Label_0293: {
                        if (gravity != 1) {
                            if (gravity != 5) {
                                gravity = a.leftMargin + paddingLeft;
                                break Label_0293;
                            }
                            gravity = n - paddingRight - measuredWidth;
                        }
                        else {
                            gravity = (n - paddingLeft - paddingRight2 - measuredWidth) / 2 + paddingLeft + a.leftMargin;
                        }
                        gravity -= a.rightMargin;
                    }
                    n2 = paddingTop;
                    if (this.hasDividerBeforeChildAt(i)) {
                        n2 = paddingTop + intrinsicHeight;
                    }
                    paddingTop = n2 + a.topMargin;
                    this.f(child, gravity, paddingTop, measuredWidth, measuredHeight);
                    gravity = paddingTop + (measuredHeight + a.bottomMargin);
                }
            }
        }
    }
    
    @Override
    public void onMeasure(final int n, final int n2) {
        if (!this.h(n, n2)) {
            super.onMeasure(n, n2);
        }
    }
}

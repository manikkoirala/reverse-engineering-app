// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.ViewParent;
import android.util.Log;
import android.content.Context;
import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import android.view.Menu;
import android.view.ViewGroup$LayoutParams;
import android.view.MenuItem;
import android.view.View$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Window$Callback;
import android.graphics.drawable.Drawable;
import android.view.View;

public class d implements dq
{
    public Toolbar a;
    public int b;
    public View c;
    public View d;
    public Drawable e;
    public Drawable f;
    public Drawable g;
    public boolean h;
    public CharSequence i;
    public CharSequence j;
    public CharSequence k;
    public Window$Callback l;
    public boolean m;
    public a n;
    public int o;
    public int p;
    public Drawable q;
    
    public d(final Toolbar toolbar, final boolean b) {
        this(toolbar, b, qb1.a, cb1.n);
    }
    
    public d(final Toolbar a, final boolean b, final int n, int popupTheme) {
        this.o = 0;
        this.p = 0;
        this.a = a;
        this.i = a.getTitle();
        this.j = a.getSubtitle();
        this.h = (this.i != null);
        this.g = a.getNavigationIcon();
        final tw1 v = tw1.v(((View)a).getContext(), null, bc1.a, sa1.c, 0);
        this.q = v.g(bc1.l);
        if (b) {
            final CharSequence p4 = v.p(bc1.r);
            if (!TextUtils.isEmpty(p4)) {
                this.setTitle(p4);
            }
            final CharSequence p5 = v.p(bc1.p);
            if (!TextUtils.isEmpty(p5)) {
                this.q(p5);
            }
            final Drawable g = v.g(bc1.n);
            if (g != null) {
                this.B(g);
            }
            final Drawable g2 = v.g(bc1.m);
            if (g2 != null) {
                this.setIcon(g2);
            }
            if (this.g == null) {
                final Drawable q = this.q;
                if (q != null) {
                    this.E(q);
                }
            }
            this.i(v.k(bc1.h, 0));
            popupTheme = v.n(bc1.g, 0);
            if (popupTheme != 0) {
                this.z(LayoutInflater.from(((View)this.a).getContext()).inflate(popupTheme, (ViewGroup)this.a, false));
                this.i(this.b | 0x10);
            }
            popupTheme = v.m(bc1.j, 0);
            if (popupTheme > 0) {
                final ViewGroup$LayoutParams layoutParams = ((View)this.a).getLayoutParams();
                layoutParams.height = popupTheme;
                ((View)this.a).setLayoutParams(layoutParams);
            }
            popupTheme = v.e(bc1.f, -1);
            final int e = v.e(bc1.e, -1);
            if (popupTheme >= 0 || e >= 0) {
                this.a.setContentInsetsRelative(Math.max(popupTheme, 0), Math.max(e, 0));
            }
            popupTheme = v.n(bc1.s, 0);
            if (popupTheme != 0) {
                final Toolbar a2 = this.a;
                a2.setTitleTextAppearance(((View)a2).getContext(), popupTheme);
            }
            popupTheme = v.n(bc1.q, 0);
            if (popupTheme != 0) {
                final Toolbar a3 = this.a;
                a3.setSubtitleTextAppearance(((View)a3).getContext(), popupTheme);
            }
            popupTheme = v.n(bc1.o, 0);
            if (popupTheme != 0) {
                this.a.setPopupTheme(popupTheme);
            }
        }
        else {
            this.b = this.y();
        }
        v.w();
        this.A(n);
        this.k = this.a.getNavigationContentDescription();
        this.a.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final b2 a = new b2(((View)b.a).getContext(), 0, 16908332, 0, 0, b.i);
            public final d b;
            
            public void onClick(final View view) {
                final d b = this.b;
                final Window$Callback l = b.l;
                if (l != null && b.m) {
                    l.onMenuItemSelected(0, (MenuItem)this.a);
                }
            }
        });
    }
    
    public void A(final int p) {
        if (p == this.p) {
            return;
        }
        this.p = p;
        if (TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
            this.C(this.p);
        }
    }
    
    public void B(final Drawable f) {
        this.f = f;
        this.I();
    }
    
    public void C(final int n) {
        CharSequence string;
        if (n == 0) {
            string = null;
        }
        else {
            string = this.getContext().getString(n);
        }
        this.D(string);
    }
    
    public void D(final CharSequence k) {
        this.k = k;
        this.G();
    }
    
    public void E(final Drawable g) {
        this.g = g;
        this.H();
    }
    
    public final void F(final CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 0x8) != 0x0) {
            this.a.setTitle(charSequence);
            if (this.h) {
                o32.t0(((View)this.a).getRootView(), charSequence);
            }
        }
    }
    
    public final void G() {
        if ((this.b & 0x4) != 0x0) {
            if (TextUtils.isEmpty(this.k)) {
                this.a.setNavigationContentDescription(this.p);
            }
            else {
                this.a.setNavigationContentDescription(this.k);
            }
        }
    }
    
    public final void H() {
        Toolbar toolbar;
        Drawable navigationIcon;
        if ((this.b & 0x4) != 0x0) {
            toolbar = this.a;
            navigationIcon = this.g;
            if (navigationIcon == null) {
                navigationIcon = this.q;
            }
        }
        else {
            toolbar = this.a;
            navigationIcon = null;
        }
        toolbar.setNavigationIcon(navigationIcon);
    }
    
    public final void I() {
        final int b = this.b;
        Drawable logo = null;
        Label_0039: {
            if ((b & 0x2) != 0x0) {
                if ((b & 0x1) != 0x0) {
                    logo = this.f;
                    if (logo != null) {
                        break Label_0039;
                    }
                }
                logo = this.e;
            }
            else {
                logo = null;
            }
        }
        this.a.setLogo(logo);
    }
    
    @Override
    public boolean a() {
        return this.a.canShowOverflowMenu();
    }
    
    @Override
    public boolean b() {
        return this.a.showOverflowMenu();
    }
    
    @Override
    public boolean c() {
        return this.a.isOverflowMenuShowing();
    }
    
    @Override
    public void collapseActionView() {
        this.a.collapseActionView();
    }
    
    @Override
    public boolean d() {
        return this.a.hideOverflowMenu();
    }
    
    @Override
    public void e(final Menu menu, final i.a callback) {
        if (this.n == null) {
            (this.n = new a(((View)this.a).getContext())).h(db1.g);
        }
        this.n.setCallback(callback);
        this.a.setMenu((e)menu, this.n);
    }
    
    @Override
    public void f() {
        this.m = true;
    }
    
    @Override
    public boolean g() {
        return this.a.isOverflowMenuShowPending();
    }
    
    @Override
    public Context getContext() {
        return ((View)this.a).getContext();
    }
    
    @Override
    public CharSequence getTitle() {
        return this.a.getTitle();
    }
    
    @Override
    public boolean h() {
        return this.a.hasExpandedActionView();
    }
    
    @Override
    public void i(final int b) {
        final int n = this.b ^ b;
        this.b = b;
        if (n != 0) {
            if ((n & 0x4) != 0x0) {
                if ((b & 0x4) != 0x0) {
                    this.G();
                }
                this.H();
            }
            if ((n & 0x3) != 0x0) {
                this.I();
            }
            if ((n & 0x8) != 0x0) {
                Toolbar toolbar;
                CharSequence j;
                if ((b & 0x8) != 0x0) {
                    this.a.setTitle(this.i);
                    toolbar = this.a;
                    j = this.j;
                }
                else {
                    final Toolbar a = this.a;
                    j = null;
                    a.setTitle(null);
                    toolbar = this.a;
                }
                toolbar.setSubtitle(j);
            }
            if ((n & 0x10) != 0x0) {
                final View d = this.d;
                if (d != null) {
                    if ((b & 0x10) != 0x0) {
                        this.a.addView(d);
                    }
                    else {
                        this.a.removeView(d);
                    }
                }
            }
        }
    }
    
    @Override
    public int j() {
        return this.o;
    }
    
    @Override
    public void k() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }
    
    @Override
    public void l(final boolean collapsible) {
        this.a.setCollapsible(collapsible);
    }
    
    @Override
    public void m() {
        this.a.dismissPopupMenus();
    }
    
    @Override
    public void n(final int visibility) {
        ((View)this.a).setVisibility(visibility);
    }
    
    @Override
    public int o() {
        return this.b;
    }
    
    @Override
    public void p() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }
    
    @Override
    public void q(final CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 0x8) != 0x0) {
            this.a.setSubtitle(charSequence);
        }
    }
    
    @Override
    public Menu r() {
        return this.a.getMenu();
    }
    
    @Override
    public i42 s(final int n, final long n2) {
        final i42 e = o32.e((View)this.a);
        float n3;
        if (n == 0) {
            n3 = 1.0f;
        }
        else {
            n3 = 0.0f;
        }
        return e.b(n3).f(n2).h(new l42(this, n) {
            public boolean a = false;
            public final int b;
            public final d c;
            
            @Override
            public void a(final View view) {
                this.a = true;
            }
            
            @Override
            public void b(final View view) {
                if (!this.a) {
                    ((View)this.c.a).setVisibility(this.b);
                }
            }
            
            @Override
            public void c(final View view) {
                ((View)this.c.a).setVisibility(0);
            }
        });
    }
    
    @Override
    public void setIcon(final int n) {
        Drawable b;
        if (n != 0) {
            b = g7.b(this.getContext(), n);
        }
        else {
            b = null;
        }
        this.setIcon(b);
    }
    
    @Override
    public void setIcon(final Drawable e) {
        this.e = e;
        this.I();
    }
    
    @Override
    public void setTitle(final CharSequence charSequence) {
        this.h = true;
        this.F(charSequence);
    }
    
    @Override
    public void setWindowCallback(final Window$Callback l) {
        this.l = l;
    }
    
    @Override
    public void setWindowTitle(final CharSequence charSequence) {
        if (!this.h) {
            this.F(charSequence);
        }
    }
    
    @Override
    public ViewGroup t() {
        return this.a;
    }
    
    @Override
    public void u(final boolean b) {
    }
    
    @Override
    public void v(final c c) {
        final View c2 = this.c;
        if (c2 != null) {
            final ViewParent parent = c2.getParent();
            final Toolbar a = this.a;
            if (parent == a) {
                a.removeView(this.c);
            }
        }
        this.c = (View)c;
    }
    
    @Override
    public void w(final int n) {
        Drawable b;
        if (n != 0) {
            b = g7.b(this.getContext(), n);
        }
        else {
            b = null;
        }
        this.B(b);
    }
    
    @Override
    public void x(final i.a a, final e.a a2) {
        this.a.setMenuCallbacks(a, a2);
    }
    
    public final int y() {
        int n;
        if (this.a.getNavigationIcon() != null) {
            this.q = this.a.getNavigationIcon();
            n = 15;
        }
        else {
            n = 11;
        }
        return n;
    }
    
    public void z(final View d) {
        final View d2 = this.d;
        if (d2 != null && (this.b & 0x10) != 0x0) {
            this.a.removeView(d2);
        }
        if ((this.d = d) != null && (this.b & 0x10) != 0x0) {
            this.a.addView(d);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.View$MeasureSpec;
import android.view.MotionEvent;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import androidx.appcompat.view.menu.i;
import androidx.appcompat.view.menu.e;
import android.view.View$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.view.View;

public class ActionBarContextView extends c
{
    public CharSequence i;
    public CharSequence j;
    public View k;
    public View l;
    public View m;
    public LinearLayout n;
    public TextView p;
    public TextView q;
    public int t;
    public int v;
    public boolean w;
    public int x;
    
    public ActionBarContextView(final Context context) {
        this(context, null);
    }
    
    public ActionBarContextView(final Context context, final AttributeSet set) {
        this(context, set, sa1.g);
    }
    
    public ActionBarContextView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final tw1 v = tw1.v(context, set, bc1.y, n, 0);
        o32.u0((View)this, v.g(bc1.z));
        this.t = v.n(bc1.D, 0);
        this.v = v.n(bc1.C, 0);
        super.e = v.m(bc1.B, 0);
        this.x = v.n(bc1.A, ob1.d);
        v.w();
    }
    
    public void g() {
        if (this.k == null) {
            this.k();
        }
    }
    
    public ViewGroup$LayoutParams generateDefaultLayoutParams() {
        return (ViewGroup$LayoutParams)new ViewGroup$MarginLayoutParams(-1, -2);
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final AttributeSet set) {
        return (ViewGroup$LayoutParams)new ViewGroup$MarginLayoutParams(((View)this).getContext(), set);
    }
    
    public CharSequence getSubtitle() {
        return this.j;
    }
    
    public CharSequence getTitle() {
        return this.i;
    }
    
    public void h(final c2 c2) {
        final View k = this.k;
        Label_0054: {
            View i;
            if (k == null) {
                i = LayoutInflater.from(((View)this).getContext()).inflate(this.x, (ViewGroup)this, false);
                this.k = i;
            }
            else {
                if (k.getParent() != null) {
                    break Label_0054;
                }
                i = this.k;
            }
            this.addView(i);
        }
        (this.l = this.k.findViewById(db1.i)).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, c2) {
            public final c2 a;
            public final ActionBarContextView b;
            
            public void onClick(final View view) {
                this.a.a();
            }
        });
        final e e = (e)c2.c();
        final androidx.appcompat.widget.a d = super.d;
        if (d != null) {
            d.q();
        }
        (super.d = new androidx.appcompat.widget.a(((View)this).getContext())).B(true);
        final ViewGroup$LayoutParams viewGroup$LayoutParams = new ViewGroup$LayoutParams(-2, -1);
        e.addMenuPresenter(super.d, super.b);
        o32.u0((View)(super.c = (ActionMenuView)super.d.g(this)), null);
        this.addView((View)super.c, viewGroup$LayoutParams);
    }
    
    public final void i() {
        if (this.n == null) {
            LayoutInflater.from(((View)this).getContext()).inflate(ob1.a, (ViewGroup)this);
            final LinearLayout n = (LinearLayout)this.getChildAt(this.getChildCount() - 1);
            this.n = n;
            this.p = (TextView)((View)n).findViewById(db1.e);
            this.q = (TextView)((View)this.n).findViewById(db1.d);
            if (this.t != 0) {
                this.p.setTextAppearance(((View)this).getContext(), this.t);
            }
            if (this.v != 0) {
                this.q.setTextAppearance(((View)this).getContext(), this.v);
            }
        }
        this.p.setText(this.i);
        this.q.setText(this.j);
        final boolean empty = TextUtils.isEmpty(this.i);
        final boolean b = TextUtils.isEmpty(this.j) ^ true;
        final TextView q = this.q;
        final int n2 = 0;
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        ((View)q).setVisibility(visibility);
        final LinearLayout n3 = this.n;
        int visibility2 = n2;
        if (!(empty ^ true)) {
            if (b) {
                visibility2 = n2;
            }
            else {
                visibility2 = 8;
            }
        }
        ((View)n3).setVisibility(visibility2);
        if (((View)this.n).getParent() == null) {
            this.addView((View)this.n);
        }
    }
    
    public boolean j() {
        return this.w;
    }
    
    public void k() {
        this.removeAllViews();
        this.m = null;
        super.c = null;
        super.d = null;
        final View l = this.l;
        if (l != null) {
            l.setOnClickListener((View$OnClickListener)null);
        }
    }
    
    public boolean l() {
        final androidx.appcompat.widget.a d = super.d;
        return d != null && d.C();
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final androidx.appcompat.widget.a d = super.d;
        if (d != null) {
            d.t();
            super.d.u();
        }
    }
    
    public void onLayout(final boolean b, int paddingLeft, int n, final int n2, int n3) {
        final boolean b2 = u42.b((View)this);
        int paddingLeft2;
        if (b2) {
            paddingLeft2 = n2 - paddingLeft - ((View)this).getPaddingRight();
        }
        else {
            paddingLeft2 = ((View)this).getPaddingLeft();
        }
        final int paddingTop = ((View)this).getPaddingTop();
        final int n4 = n3 - n - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom();
        final View k = this.k;
        n = paddingLeft2;
        if (k != null) {
            n = paddingLeft2;
            if (k.getVisibility() != 8) {
                final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)this.k.getLayoutParams();
                if (b2) {
                    n = viewGroup$MarginLayoutParams.rightMargin;
                }
                else {
                    n = viewGroup$MarginLayoutParams.leftMargin;
                }
                if (b2) {
                    n3 = viewGroup$MarginLayoutParams.leftMargin;
                }
                else {
                    n3 = viewGroup$MarginLayoutParams.rightMargin;
                }
                n = c.d(paddingLeft2, n, b2);
                n = c.d(n + this.e(this.k, n, paddingTop, n4, b2), n3, b2);
            }
        }
        n3 = n;
        final LinearLayout n5 = this.n;
        n = n3;
        if (n5 != null) {
            n = n3;
            if (this.m == null) {
                n = n3;
                if (((View)n5).getVisibility() != 8) {
                    n = n3 + this.e((View)this.n, n3, paddingTop, n4, b2);
                }
            }
        }
        final View m = this.m;
        if (m != null) {
            this.e(m, n, paddingTop, n4, b2);
        }
        if (b2) {
            paddingLeft = ((View)this).getPaddingLeft();
        }
        else {
            paddingLeft = n2 - paddingLeft - ((View)this).getPaddingRight();
        }
        final ActionMenuView c = super.c;
        if (c != null) {
            this.e((View)c, paddingLeft, paddingTop, n4, b2 ^ true);
        }
    }
    
    public void onMeasure(int i, int b) {
        final int mode = View$MeasureSpec.getMode(i);
        final int n = 1073741824;
        if (mode != 1073741824) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getClass().getSimpleName());
            sb.append(" can only be used with android:layout_width=\"match_parent\" (or fill_parent)");
            throw new IllegalStateException(sb.toString());
        }
        if (View$MeasureSpec.getMode(b) != 0) {
            final int size = View$MeasureSpec.getSize(i);
            int n2 = super.e;
            if (n2 <= 0) {
                n2 = View$MeasureSpec.getSize(b);
            }
            final int n3 = ((View)this).getPaddingTop() + ((View)this).getPaddingBottom();
            i = size - ((View)this).getPaddingLeft() - ((View)this).getPaddingRight();
            final int b2 = n2 - n3;
            final int measureSpec = View$MeasureSpec.makeMeasureSpec(b2, Integer.MIN_VALUE);
            final View k = this.k;
            final int n4 = 0;
            b = i;
            if (k != null) {
                i = this.c(k, i, measureSpec, 0);
                final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)this.k.getLayoutParams();
                b = i - (viewGroup$MarginLayoutParams.leftMargin + viewGroup$MarginLayoutParams.rightMargin);
            }
            final ActionMenuView c = super.c;
            i = b;
            if (c != null) {
                i = b;
                if (((View)c).getParent() == this) {
                    i = this.c((View)super.c, b, measureSpec, 0);
                }
            }
            final LinearLayout n5 = this.n;
            b = i;
            if (n5 != null) {
                b = i;
                if (this.m == null) {
                    if (this.w) {
                        b = View$MeasureSpec.makeMeasureSpec(0, 0);
                        ((View)this.n).measure(b, measureSpec);
                        final int measuredWidth = ((View)this.n).getMeasuredWidth();
                        final boolean b3 = measuredWidth <= i;
                        b = i;
                        if (b3) {
                            b = i - measuredWidth;
                        }
                        final LinearLayout n6 = this.n;
                        if (b3) {
                            i = 0;
                        }
                        else {
                            i = 8;
                        }
                        ((View)n6).setVisibility(i);
                    }
                    else {
                        b = this.c((View)n5, i, measureSpec, 0);
                    }
                }
            }
            final View m = this.m;
            if (m != null) {
                final ViewGroup$LayoutParams layoutParams = m.getLayoutParams();
                final int width = layoutParams.width;
                if (width != -2) {
                    i = 1073741824;
                }
                else {
                    i = Integer.MIN_VALUE;
                }
                int min = b;
                if (width >= 0) {
                    min = Math.min(width, b);
                }
                final int height = layoutParams.height;
                if (height != -2) {
                    b = n;
                }
                else {
                    b = Integer.MIN_VALUE;
                }
                int min2 = b2;
                if (height >= 0) {
                    min2 = Math.min(height, b2);
                }
                this.m.measure(View$MeasureSpec.makeMeasureSpec(min, i), View$MeasureSpec.makeMeasureSpec(min2, b));
            }
            if (super.e <= 0) {
                final int childCount = this.getChildCount();
                int n7 = 0;
                int n8;
                for (i = n4; i < childCount; ++i, n7 = b) {
                    n8 = this.getChildAt(i).getMeasuredHeight() + n3;
                    if (n8 > (b = n7)) {
                        b = n8;
                    }
                }
                ((View)this).setMeasuredDimension(size, n7);
            }
            else {
                ((View)this).setMeasuredDimension(size, n2);
            }
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.getClass().getSimpleName());
        sb2.append(" can only be used with android:layout_height=\"wrap_content\"");
        throw new IllegalStateException(sb2.toString());
    }
    
    @Override
    public void setContentHeight(final int e) {
        super.e = e;
    }
    
    public void setCustomView(final View m) {
        final View i = this.m;
        if (i != null) {
            this.removeView(i);
        }
        if ((this.m = m) != null) {
            final LinearLayout n = this.n;
            if (n != null) {
                this.removeView((View)n);
                this.n = null;
            }
        }
        if (m != null) {
            this.addView(m);
        }
        ((View)this).requestLayout();
    }
    
    public void setSubtitle(final CharSequence j) {
        this.j = j;
        this.i();
    }
    
    public void setTitle(final CharSequence i) {
        this.i = i;
        this.i();
        o32.t0((View)this, i);
    }
    
    public void setTitleOptional(final boolean w) {
        if (w != this.w) {
            ((View)this).requestLayout();
        }
        this.w = w;
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
}

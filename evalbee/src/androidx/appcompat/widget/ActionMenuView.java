// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.ContextThemeWrapper;
import android.view.ViewGroup;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.accessibility.AccessibilityEvent;
import android.view.ViewGroup$LayoutParams;
import android.view.MenuItem;
import androidx.appcompat.view.menu.g;
import androidx.appcompat.view.menu.ActionMenuItemView;
import android.view.View$MeasureSpec;
import android.view.View;
import android.util.AttributeSet;
import androidx.appcompat.view.menu.i;
import android.content.Context;
import androidx.appcompat.view.menu.j;
import androidx.appcompat.view.menu.e;

public class ActionMenuView extends androidx.appcompat.widget.b implements androidx.appcompat.view.menu.e.b, j
{
    public androidx.appcompat.view.menu.e a;
    public Context b;
    public int c;
    public boolean d;
    public androidx.appcompat.widget.a e;
    public i.a f;
    public androidx.appcompat.view.menu.e.a g;
    public boolean h;
    public int i;
    public int j;
    public int k;
    public e l;
    
    public ActionMenuView(final Context context) {
        this(context, null);
    }
    
    public ActionMenuView(final Context b, final AttributeSet set) {
        super(b, set);
        this.setBaselineAligned(false);
        final float density = b.getResources().getDisplayMetrics().density;
        this.j = (int)(56.0f * density);
        this.k = (int)(density * 4.0f);
        this.b = b;
        this.c = 0;
    }
    
    public static int q(final View view, final int n, int b, int n2, int n3) {
        final c c = (c)view.getLayoutParams();
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(View$MeasureSpec.getSize(n2) - n3, View$MeasureSpec.getMode(n2));
        ActionMenuItemView actionMenuItemView;
        if (view instanceof ActionMenuItemView) {
            actionMenuItemView = (ActionMenuItemView)view;
        }
        else {
            actionMenuItemView = null;
        }
        boolean d = true;
        if (actionMenuItemView != null && actionMenuItemView.d()) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        Label_0144: {
            if (b > 0) {
                n3 = 2;
                if (n2 == 0 || b >= 2) {
                    view.measure(View$MeasureSpec.makeMeasureSpec(b * n, Integer.MIN_VALUE), measureSpec);
                    final int measuredWidth = view.getMeasuredWidth();
                    final int n4 = b = measuredWidth / n;
                    if (measuredWidth % n != 0) {
                        b = n4 + 1;
                    }
                    if (n2 != 0 && b < 2) {
                        b = n3;
                    }
                    break Label_0144;
                }
            }
            b = 0;
        }
        if (c.a || n2 == 0) {
            d = false;
        }
        c.d = d;
        c.b = b;
        view.measure(View$MeasureSpec.makeMeasureSpec(n * b, 1073741824), measureSpec);
        return b;
    }
    
    @Override
    public boolean c(final g g) {
        return this.a.performItemAction((MenuItem)g, 0);
    }
    
    @Override
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof c;
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        return false;
    }
    
    public void g() {
        final androidx.appcompat.widget.a e = this.e;
        if (e != null) {
            e.q();
        }
    }
    
    public Menu getMenu() {
        if (this.a == null) {
            final Context context = ((View)this).getContext();
            (this.a = new androidx.appcompat.view.menu.e(context)).setCallback((androidx.appcompat.view.menu.e.a)new d());
            (this.e = new androidx.appcompat.widget.a(context)).B(true);
            final androidx.appcompat.widget.a e = this.e;
            i.a f = this.f;
            if (f == null) {
                f = new b();
            }
            e.setCallback(f);
            this.a.addMenuPresenter(this.e, this.b);
            this.e.z(this);
        }
        return (Menu)this.a;
    }
    
    public Drawable getOverflowIcon() {
        this.getMenu();
        return this.e.s();
    }
    
    public int getPopupTheme() {
        return this.c;
    }
    
    public int getWindowAnimations() {
        return 0;
    }
    
    public c h() {
        final c c = new c(-2, -2);
        c.gravity = 16;
        return c;
    }
    
    public c i(final AttributeSet set) {
        return new c(((View)this).getContext(), set);
    }
    
    @Override
    public void initialize(final androidx.appcompat.view.menu.e a) {
        this.a = a;
    }
    
    public c j(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams != null) {
            c c;
            if (viewGroup$LayoutParams instanceof c) {
                c = new c((c)viewGroup$LayoutParams);
            }
            else {
                c = new c(viewGroup$LayoutParams);
            }
            if (c.gravity <= 0) {
                c.gravity = 16;
            }
            return c;
        }
        return this.h();
    }
    
    public c k() {
        final c h = this.h();
        h.a = true;
        return h;
    }
    
    public boolean l(final int n) {
        final boolean b = false;
        if (n == 0) {
            return false;
        }
        final View child = this.getChildAt(n - 1);
        final View child2 = this.getChildAt(n);
        boolean b2 = b;
        if (n < this.getChildCount()) {
            b2 = b;
            if (child instanceof a) {
                b2 = (false | ((a)child).a());
            }
        }
        boolean b3 = b2;
        if (n > 0) {
            b3 = b2;
            if (child2 instanceof a) {
                b3 = (b2 | ((a)child2).b());
            }
        }
        return b3;
    }
    
    public boolean m() {
        final androidx.appcompat.widget.a e = this.e;
        return e != null && e.t();
    }
    
    public boolean n() {
        final androidx.appcompat.widget.a e = this.e;
        return e != null && e.v();
    }
    
    public boolean o() {
        final androidx.appcompat.widget.a e = this.e;
        return e != null && e.w();
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final androidx.appcompat.widget.a e = this.e;
        if (e != null) {
            e.updateMenuView(false);
            if (this.e.w()) {
                this.e.t();
                this.e.C();
            }
        }
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g();
    }
    
    @Override
    public void onLayout(final boolean b, int i, int j, int n, int n2) {
        if (!this.h) {
            super.onLayout(b, i, j, n, n2);
            return;
        }
        final int childCount = this.getChildCount();
        final int n3 = (n2 - j) / 2;
        final int dividerWidth = this.getDividerWidth();
        final int n4 = n - i;
        i = n4 - ((View)this).getPaddingRight() - ((View)this).getPaddingLeft();
        final boolean b2 = u42.b((View)this);
        j = 0;
        n2 = 0;
        n = 0;
        while (j < childCount) {
            final View child = this.getChildAt(j);
            if (child.getVisibility() != 8) {
                final c c = (c)child.getLayoutParams();
                if (c.a) {
                    final int n5 = n2 = child.getMeasuredWidth();
                    if (this.l(j)) {
                        n2 = n5 + dividerWidth;
                    }
                    final int measuredHeight = child.getMeasuredHeight();
                    int n6;
                    int n7;
                    if (b2) {
                        n6 = ((View)this).getPaddingLeft() + c.leftMargin;
                        n7 = n6 + n2;
                    }
                    else {
                        n7 = ((View)this).getWidth() - ((View)this).getPaddingRight() - c.rightMargin;
                        n6 = n7 - n2;
                    }
                    final int n8 = n3 - measuredHeight / 2;
                    child.layout(n6, n8, n7, measuredHeight + n8);
                    i -= n2;
                    n2 = 1;
                }
                else {
                    i -= child.getMeasuredWidth() + c.leftMargin + c.rightMargin;
                    this.l(j);
                    ++n;
                }
            }
            ++j;
        }
        if (childCount == 1 && n2 == 0) {
            final View child2 = this.getChildAt(0);
            i = child2.getMeasuredWidth();
            j = child2.getMeasuredHeight();
            n = n4 / 2 - i / 2;
            n2 = n3 - j / 2;
            child2.layout(n, n2, i + n, j + n2);
            return;
        }
        j = n - (n2 ^ 0x1);
        if (j > 0) {
            i /= j;
        }
        else {
            i = 0;
        }
        n2 = Math.max(0, i);
        if (b2) {
            n = ((View)this).getWidth() - ((View)this).getPaddingRight();
            View child3;
            c c2;
            int n9;
            int n10;
            for (i = 0; i < childCount; ++i, n = j) {
                child3 = this.getChildAt(i);
                c2 = (c)child3.getLayoutParams();
                j = n;
                if (child3.getVisibility() != 8) {
                    if (c2.a) {
                        j = n;
                    }
                    else {
                        n9 = n - c2.rightMargin;
                        n = child3.getMeasuredWidth();
                        j = child3.getMeasuredHeight();
                        n10 = n3 - j / 2;
                        child3.layout(n9 - n, n10, n9, j + n10);
                        j = n9 - (n + c2.leftMargin + n2);
                    }
                }
            }
        }
        else {
            n = ((View)this).getPaddingLeft();
            View child4;
            c c3;
            int measuredHeight2;
            int n11;
            for (i = 0; i < childCount; ++i, n = j) {
                child4 = this.getChildAt(i);
                c3 = (c)child4.getLayoutParams();
                j = n;
                if (child4.getVisibility() != 8) {
                    if (c3.a) {
                        j = n;
                    }
                    else {
                        j = n + c3.leftMargin;
                        n = child4.getMeasuredWidth();
                        measuredHeight2 = child4.getMeasuredHeight();
                        n11 = n3 - measuredHeight2 / 2;
                        child4.layout(j, n11, j + n, measuredHeight2 + n11);
                        j += n + c3.rightMargin + n2;
                    }
                }
            }
        }
    }
    
    @Override
    public void onMeasure(final int n, final int n2) {
        final boolean h = this.h;
        int h2;
        if (View$MeasureSpec.getMode(n) == 1073741824) {
            h2 = 1;
        }
        else {
            h2 = 0;
        }
        this.h = (h2 != 0);
        if ((h ? 1 : 0) != h2) {
            this.i = 0;
        }
        final int size = View$MeasureSpec.getSize(n);
        if (this.h) {
            final androidx.appcompat.view.menu.e a = this.a;
            if (a != null && size != this.i) {
                this.i = size;
                a.onItemsChanged(true);
            }
        }
        final int childCount = this.getChildCount();
        if (this.h && childCount > 0) {
            this.r(n, n2);
        }
        else {
            for (int i = 0; i < childCount; ++i) {
                final c c = (c)this.getChildAt(i).getLayoutParams();
                c.rightMargin = 0;
                c.leftMargin = 0;
            }
            super.onMeasure(n, n2);
        }
    }
    
    public boolean p() {
        return this.d;
    }
    
    public final void r(int i, int max) {
        final int mode = View$MeasureSpec.getMode(max);
        final int size = View$MeasureSpec.getSize(i);
        int size2 = View$MeasureSpec.getSize(max);
        i = ((View)this).getPaddingLeft();
        final int paddingRight = ((View)this).getPaddingRight();
        final int n = ((View)this).getPaddingTop() + ((View)this).getPaddingBottom();
        final int childMeasureSpec = ViewGroup.getChildMeasureSpec(max, n, -2);
        final int n2 = size - (i + paddingRight);
        i = this.j;
        final int n3 = n2 / i;
        if (n3 == 0) {
            ((View)this).setMeasuredDimension(n2, 0);
            return;
        }
        final int n4 = i + n2 % i / n3;
        final int childCount = this.getChildCount();
        final int n5 = 0;
        int j = 0;
        int n6 = i = j;
        int n7;
        max = (n7 = i);
        long k = 0L;
        int max2 = max;
        int n8 = i;
        max = n5;
        i = n3;
        while (j < childCount) {
            final View child = this.getChildAt(j);
            int n9;
            if (child.getVisibility() == 8) {
                n9 = n7;
            }
            else {
                final boolean b = child instanceof ActionMenuItemView;
                ++n8;
                if (b) {
                    final int l = this.k;
                    child.setPadding(l, 0, l, 0);
                }
                final c c = (c)child.getLayoutParams();
                c.f = false;
                c.c = 0;
                c.b = 0;
                c.d = false;
                c.leftMargin = 0;
                c.rightMargin = 0;
                c.e = (b && ((ActionMenuItemView)child).d());
                int n10;
                if (c.a) {
                    n10 = 1;
                }
                else {
                    n10 = i;
                }
                final int q = q(child, n4, n10, childMeasureSpec, n);
                max2 = Math.max(max2, q);
                n9 = n7;
                if (c.d) {
                    n9 = n7 + 1;
                }
                if (c.a) {
                    n6 = 1;
                }
                i -= q;
                max = Math.max(max, child.getMeasuredHeight());
                if (q == 1) {
                    k |= 1 << j;
                }
            }
            ++j;
            n7 = n9;
        }
        final boolean b2 = n6 != 0 && n8 == 2;
        int n11 = 0;
        int n12 = i;
        final boolean b3 = b2;
        final int n13 = n2;
        while (true) {
            while (n7 > 0 && n12 > 0) {
                int n14 = Integer.MAX_VALUE;
                int n15 = 0;
                int n16 = 0;
                long n17 = 0L;
                while (n16 < childCount) {
                    final c c2 = (c)this.getChildAt(n16).getLayoutParams();
                    int n18;
                    long n19;
                    if (!c2.d) {
                        i = n15;
                        n18 = n14;
                        n19 = n17;
                    }
                    else {
                        final int b4 = c2.b;
                        if (b4 < n14) {
                            n19 = 1L << n16;
                            n18 = b4;
                            i = 1;
                        }
                        else {
                            i = n15;
                            n18 = n14;
                            n19 = n17;
                            if (b4 == n14) {
                                i = n15 + 1;
                                n19 = (n17 | 1L << n16);
                                n18 = n14;
                            }
                        }
                    }
                    ++n16;
                    n15 = i;
                    n14 = n18;
                    n17 = n19;
                }
                i = n11;
                k |= n17;
                if (n15 > n12) {
                    final boolean b5 = n6 == 0 && n8 == 1;
                    int n25;
                    if (n12 > 0 && k != 0L && (n12 < n8 - 1 || b5 || max2 > 1)) {
                        float n20 = (float)Long.bitCount(k);
                        if (!b5) {
                            float n21 = n20;
                            if ((k & 0x1L) != 0x0L) {
                                n21 = n20;
                                if (!((c)this.getChildAt(0).getLayoutParams()).e) {
                                    n21 = n20 - 0.5f;
                                }
                            }
                            final int n22 = childCount - 1;
                            n20 = n21;
                            if ((k & (long)(1 << n22)) != 0x0L) {
                                n20 = n21;
                                if (!((c)this.getChildAt(n22).getLayoutParams()).e) {
                                    n20 = n21 - 0.5f;
                                }
                            }
                        }
                        int n23;
                        if (n20 > 0.0f) {
                            n23 = (int)(n12 * n4 / n20);
                        }
                        else {
                            n23 = 0;
                        }
                        int n24 = 0;
                        while (true) {
                            n25 = i;
                            if (n24 >= childCount) {
                                break;
                            }
                            int n26;
                            if ((k & (long)(1 << n24)) == 0x0L) {
                                n26 = i;
                            }
                            else {
                                final View child2 = this.getChildAt(n24);
                                final c c3 = (c)child2.getLayoutParams();
                                if (child2 instanceof ActionMenuItemView) {
                                    c3.c = n23;
                                    c3.f = true;
                                    if (n24 == 0 && !c3.e) {
                                        c3.leftMargin = -n23 / 2;
                                    }
                                    n26 = 1;
                                }
                                else if (c3.a) {
                                    c3.c = n23;
                                    c3.f = true;
                                    c3.rightMargin = -n23 / 2;
                                    n26 = 1;
                                }
                                else {
                                    if (n24 != 0) {
                                        c3.leftMargin = n23 / 2;
                                    }
                                    n26 = i;
                                    if (n24 != childCount - 1) {
                                        c3.rightMargin = n23 / 2;
                                        n26 = i;
                                    }
                                }
                            }
                            ++n24;
                            i = n26;
                        }
                    }
                    else {
                        n25 = i;
                    }
                    if (n25 != 0) {
                        View child3;
                        c c4;
                        for (i = 0; i < childCount; ++i) {
                            child3 = this.getChildAt(i);
                            c4 = (c)child3.getLayoutParams();
                            if (c4.f) {
                                child3.measure(View$MeasureSpec.makeMeasureSpec(c4.b * n4 + c4.c, 1073741824), childMeasureSpec);
                            }
                        }
                    }
                    if (mode != 1073741824) {
                        size2 = max;
                    }
                    ((View)this).setMeasuredDimension(n13, size2);
                    return;
                }
                View child4;
                c c5;
                long n27;
                long n28;
                int m;
                for (i = 0; i < childCount; ++i) {
                    child4 = this.getChildAt(i);
                    c5 = (c)child4.getLayoutParams();
                    n27 = 1 << i;
                    if ((n17 & n27) == 0x0L) {
                        n28 = k;
                        if (c5.b == n14 + 1) {
                            n28 = (k | n27);
                        }
                        k = n28;
                    }
                    else {
                        if (b3 && c5.e && n12 == 1) {
                            m = this.k;
                            child4.setPadding(m + n4, 0, m, 0);
                        }
                        ++c5.b;
                        c5.f = true;
                        --n12;
                    }
                }
                n11 = 1;
            }
            i = n11;
            continue;
        }
    }
    
    public androidx.appcompat.view.menu.e s() {
        return this.a;
    }
    
    public void setExpandedActionViewsExclusive(final boolean b) {
        this.e.y(b);
    }
    
    public void setOnMenuItemClickListener(final e l) {
        this.l = l;
    }
    
    public void setOverflowIcon(final Drawable drawable) {
        this.getMenu();
        this.e.A(drawable);
    }
    
    public void setOverflowReserved(final boolean d) {
        this.d = d;
    }
    
    public void setPopupTheme(final int c) {
        if (this.c != c) {
            if ((this.c = c) == 0) {
                this.b = ((View)this).getContext();
            }
            else {
                this.b = (Context)new ContextThemeWrapper(((View)this).getContext(), c);
            }
        }
    }
    
    public void setPresenter(final androidx.appcompat.widget.a e) {
        (this.e = e).z(this);
    }
    
    public void t(final i.a f, final androidx.appcompat.view.menu.e.a g) {
        this.f = f;
        this.g = g;
    }
    
    public boolean u() {
        final androidx.appcompat.widget.a e = this.e;
        return e != null && e.C();
    }
    
    public interface a
    {
        boolean a();
        
        boolean b();
    }
    
    public static class b implements i.a
    {
        @Override
        public boolean a(final androidx.appcompat.view.menu.e e) {
            return false;
        }
        
        @Override
        public void onCloseMenu(final androidx.appcompat.view.menu.e e, final boolean b) {
        }
    }
    
    public static class c extends a
    {
        public boolean a;
        public int b;
        public int c;
        public boolean d;
        public boolean e;
        public boolean f;
        
        public c(final int n, final int n2) {
            super(n, n2);
            this.a = false;
        }
        
        public c(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        public c(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
        }
        
        public c(final c c) {
            super((ViewGroup$LayoutParams)c);
            this.a = c.a;
        }
    }
    
    public class d implements e.a
    {
        public final ActionMenuView a;
        
        public d(final ActionMenuView a) {
            this.a = a;
        }
        
        @Override
        public boolean onMenuItemSelected(final e e, final MenuItem menuItem) {
            final ActionMenuView.e l = this.a.l;
            return l != null && l.onMenuItemClick(menuItem);
        }
        
        @Override
        public void onMenuModeChange(final e e) {
            final e.a g = this.a.g;
            if (g != null) {
                g.onMenuModeChange(e);
            }
        }
    }
    
    public interface e
    {
        boolean onMenuItemClick(final MenuItem p0);
    }
}

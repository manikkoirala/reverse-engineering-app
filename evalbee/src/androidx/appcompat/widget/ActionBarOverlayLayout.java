// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.ViewGroup$MarginLayoutParams;
import android.view.Window$Callback;
import android.content.res.TypedArray;
import android.content.res.Configuration;
import android.view.WindowInsets;
import android.view.View;
import androidx.appcompat.view.menu.i;
import android.view.Menu;
import android.graphics.Canvas;
import android.view.ViewGroup$LayoutParams;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewPropertyAnimator;
import android.widget.OverScroller;
import android.view.ViewGroup;

public class ActionBarOverlayLayout extends ViewGroup implements cq, ny0, oy0
{
    public static final int[] M;
    public u62 A;
    public u62 C;
    public d D;
    public OverScroller F;
    public ViewPropertyAnimator G;
    public final AnimatorListenerAdapter H;
    public final Runnable I;
    public final Runnable J;
    public final py0 K;
    public int a;
    public int b;
    public ContentFrameLayout c;
    public ActionBarContainer d;
    public dq e;
    public Drawable f;
    public boolean g;
    public boolean h;
    public boolean i;
    public boolean j;
    public boolean k;
    public int l;
    public int m;
    public final Rect n;
    public final Rect p;
    public final Rect q;
    public final Rect t;
    public final Rect v;
    public final Rect w;
    public final Rect x;
    public u62 y;
    public u62 z;
    
    static {
        M = new int[] { sa1.b, 16842841 };
    }
    
    public ActionBarOverlayLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.b = 0;
        this.n = new Rect();
        this.p = new Rect();
        this.q = new Rect();
        this.t = new Rect();
        this.v = new Rect();
        this.w = new Rect();
        this.x = new Rect();
        final u62 b = u62.b;
        this.y = b;
        this.z = b;
        this.A = b;
        this.C = b;
        this.H = new AnimatorListenerAdapter(this) {
            public final ActionBarOverlayLayout a;
            
            public void onAnimationCancel(final Animator animator) {
                final ActionBarOverlayLayout a = this.a;
                a.G = null;
                a.k = false;
            }
            
            public void onAnimationEnd(final Animator animator) {
                final ActionBarOverlayLayout a = this.a;
                a.G = null;
                a.k = false;
            }
        };
        this.I = new Runnable(this) {
            public final ActionBarOverlayLayout a;
            
            @Override
            public void run() {
                this.a.o();
                final ActionBarOverlayLayout a = this.a;
                a.G = ((View)a.d).animate().translationY(0.0f).setListener((Animator$AnimatorListener)this.a.H);
            }
        };
        this.J = new Runnable(this) {
            public final ActionBarOverlayLayout a;
            
            @Override
            public void run() {
                this.a.o();
                final ActionBarOverlayLayout a = this.a;
                a.G = ((View)a.d).animate().translationY((float)(-((View)this.a.d).getHeight())).setListener((Animator$AnimatorListener)this.a.H);
            }
        };
        this.p(context);
        this.K = new py0(this);
    }
    
    public boolean a() {
        this.t();
        return this.e.a();
    }
    
    public boolean b() {
        this.t();
        return this.e.b();
    }
    
    public boolean c() {
        this.t();
        return this.e.c();
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof e;
    }
    
    public boolean d() {
        this.t();
        return this.e.d();
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        if (this.f != null && !this.g) {
            int n;
            if (((View)this.d).getVisibility() == 0) {
                n = (int)(((View)this.d).getBottom() + ((View)this.d).getTranslationY() + 0.5f);
            }
            else {
                n = 0;
            }
            this.f.setBounds(0, n, ((View)this).getWidth(), this.f.getIntrinsicHeight() + n);
            this.f.draw(canvas);
        }
    }
    
    public void e(final Menu menu, final i.a a) {
        this.t();
        this.e.e(menu, a);
    }
    
    public void f() {
        this.t();
        this.e.f();
    }
    
    public boolean fitSystemWindows(final Rect rect) {
        return super.fitSystemWindows(rect);
    }
    
    public boolean g() {
        this.t();
        return this.e.g();
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return (ViewGroup$LayoutParams)new e(viewGroup$LayoutParams);
    }
    
    public int getActionBarHideOffset() {
        final ActionBarContainer d = this.d;
        int n;
        if (d != null) {
            n = -(int)((View)d).getTranslationY();
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public int getNestedScrollAxes() {
        return this.K.a();
    }
    
    public CharSequence getTitle() {
        this.t();
        return this.e.getTitle();
    }
    
    public void h(final int n) {
        this.t();
        if (n != 2) {
            if (n != 5) {
                if (n == 109) {
                    this.setOverlayMode(true);
                }
            }
            else {
                this.e.p();
            }
        }
        else {
            this.e.k();
        }
    }
    
    public void i() {
        this.t();
        this.e.m();
    }
    
    public final void j() {
        this.o();
        this.J.run();
    }
    
    public final boolean k(final View view, final Rect rect, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        final e e = (e)view.getLayoutParams();
        final boolean b5 = true;
        boolean b6 = false;
        Label_0049: {
            if (b) {
                final int leftMargin = e.leftMargin;
                final int left = rect.left;
                if (leftMargin != left) {
                    e.leftMargin = left;
                    b6 = true;
                    break Label_0049;
                }
            }
            b6 = false;
        }
        boolean b7 = b6;
        if (b2) {
            final int topMargin = e.topMargin;
            final int top = rect.top;
            b7 = b6;
            if (topMargin != top) {
                e.topMargin = top;
                b7 = true;
            }
        }
        boolean b8 = b7;
        if (b4) {
            final int rightMargin = e.rightMargin;
            final int right = rect.right;
            b8 = b7;
            if (rightMargin != right) {
                e.rightMargin = right;
                b8 = true;
            }
        }
        if (b3) {
            final int bottomMargin = e.bottomMargin;
            final int bottom = rect.bottom;
            if (bottomMargin != bottom) {
                e.bottomMargin = bottom;
                b8 = b5;
            }
        }
        return b8;
    }
    
    public e l() {
        return new e(-1, -1);
    }
    
    public e m(final AttributeSet set) {
        return new e(((View)this).getContext(), set);
    }
    
    public final dq n(final View view) {
        if (view instanceof dq) {
            return (dq)view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar)view).getWrapper();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view.getClass().getSimpleName());
        throw new IllegalStateException(sb.toString());
    }
    
    public void o() {
        ((View)this).removeCallbacks(this.I);
        ((View)this).removeCallbacks(this.J);
        final ViewPropertyAnimator g = this.G;
        if (g != null) {
            g.cancel();
        }
    }
    
    public WindowInsets onApplyWindowInsets(final WindowInsets windowInsets) {
        this.t();
        final u62 w = u62.w(windowInsets, (View)this);
        int k = this.k((View)this.d, new Rect(w.i(), w.k(), w.j(), w.h()), true, true, false, true) ? 1 : 0;
        o32.f((View)this, w, this.n);
        final Rect n = this.n;
        final u62 m = w.m(n.left, n.top, n.right, n.bottom);
        this.y = m;
        final boolean equals = this.z.equals(m);
        final int n2 = 1;
        if (!equals) {
            this.z = this.y;
            k = 1;
        }
        if (!this.p.equals((Object)this.n)) {
            this.p.set(this.n);
            k = n2;
        }
        if (k != 0) {
            ((View)this).requestLayout();
        }
        return w.a().c().b().u();
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.p(((View)this).getContext());
        o32.n0((View)this);
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.o();
    }
    
    public void onLayout(final boolean b, int i, int paddingLeft, int paddingTop, int childCount) {
        childCount = this.getChildCount();
        paddingLeft = ((View)this).getPaddingLeft();
        paddingTop = ((View)this).getPaddingTop();
        View child;
        e e;
        int measuredWidth;
        int measuredHeight;
        int n;
        int n2;
        for (i = 0; i < childCount; ++i) {
            child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                e = (e)child.getLayoutParams();
                measuredWidth = child.getMeasuredWidth();
                measuredHeight = child.getMeasuredHeight();
                n = e.leftMargin + paddingLeft;
                n2 = e.topMargin + paddingTop;
                child.layout(n, n2, measuredWidth + n, measuredHeight + n2);
            }
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        this.t();
        this.measureChildWithMargins((View)this.d, n, 0, n2, 0);
        final e e = (e)((View)this.d).getLayoutParams();
        final int max = Math.max(0, ((View)this.d).getMeasuredWidth() + e.leftMargin + e.rightMargin);
        final int max2 = Math.max(0, ((View)this.d).getMeasuredHeight() + e.topMargin + e.bottomMargin);
        final int combineMeasuredStates = View.combineMeasuredStates(0, ((View)this.d).getMeasuredState());
        final boolean b = (o32.M((View)this) & 0x100) != 0x0;
        int n4;
        if (b) {
            final int n3 = n4 = this.a;
            if (this.i) {
                n4 = n3;
                if (this.d.getTabContainer() != null) {
                    n4 = n3 + this.a;
                }
            }
        }
        else if (((View)this.d).getVisibility() != 8) {
            n4 = ((View)this.d).getMeasuredHeight();
        }
        else {
            n4 = 0;
        }
        this.q.set(this.n);
        final u62 y = this.y;
        this.A = y;
        u62 a;
        if (!this.h && !b) {
            final Rect q = this.q;
            q.top += n4;
            q.bottom += 0;
            a = y.m(0, n4, 0, 0);
        }
        else {
            a = new u62.b(this.A).d(nf0.b(y.i(), this.A.k() + n4, this.A.j(), this.A.h() + 0)).a();
        }
        this.A = a;
        this.k((View)this.c, this.q, true, true, true, true);
        if (!this.C.equals(this.A)) {
            final u62 a2 = this.A;
            this.C = a2;
            o32.g((View)this.c, a2);
        }
        this.measureChildWithMargins((View)this.c, n, 0, n2, 0);
        final e e2 = (e)((View)this.c).getLayoutParams();
        final int max3 = Math.max(max, ((View)this.c).getMeasuredWidth() + e2.leftMargin + e2.rightMargin);
        final int max4 = Math.max(max2, ((View)this.c).getMeasuredHeight() + e2.topMargin + e2.bottomMargin);
        final int combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates, ((View)this.c).getMeasuredState());
        ((View)this).setMeasuredDimension(View.resolveSizeAndState(Math.max(max3 + (((View)this).getPaddingLeft() + ((View)this).getPaddingRight()), ((View)this).getSuggestedMinimumWidth()), n, combineMeasuredStates2), View.resolveSizeAndState(Math.max(max4 + (((View)this).getPaddingTop() + ((View)this).getPaddingBottom()), ((View)this).getSuggestedMinimumHeight()), n2, combineMeasuredStates2 << 16));
    }
    
    public boolean onNestedFling(final View view, final float n, final float n2, final boolean b) {
        if (this.j && b) {
            if (this.v(n2)) {
                this.j();
            }
            else {
                this.u();
            }
            return this.k = true;
        }
        return false;
    }
    
    public boolean onNestedPreFling(final View view, final float n, final float n2) {
        return false;
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array) {
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int n3) {
        if (n3 == 0) {
            this.onNestedPreScroll(view, n, n2, array);
        }
    }
    
    public void onNestedScroll(final View view, int l, final int n, final int n2, final int n3) {
        l = this.l + n;
        this.setActionBarHideOffset(this.l = l);
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        if (n5 == 0) {
            this.onNestedScroll(view, n, n2, n3, n4);
        }
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int[] array) {
        this.onNestedScroll(view, n, n2, n3, n4, n5);
    }
    
    public void onNestedScrollAccepted(final View view, final View view2, final int n) {
        this.K.b(view, view2, n);
        this.l = this.getActionBarHideOffset();
        this.o();
        final d d = this.D;
        if (d != null) {
            d.d();
        }
    }
    
    public void onNestedScrollAccepted(final View view, final View view2, final int n, final int n2) {
        if (n2 == 0) {
            this.onNestedScrollAccepted(view, view2, n);
        }
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n) {
        return (n & 0x2) != 0x0 && ((View)this.d).getVisibility() == 0 && this.j;
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n, final int n2) {
        return n2 == 0 && this.onStartNestedScroll(view, view2, n);
    }
    
    public void onStopNestedScroll(final View view) {
        if (this.j && !this.k) {
            if (this.l <= ((View)this.d).getHeight()) {
                this.s();
            }
            else {
                this.r();
            }
        }
        final d d = this.D;
        if (d != null) {
            d.b();
        }
    }
    
    public void onStopNestedScroll(final View view, final int n) {
        if (n == 0) {
            this.onStopNestedScroll(view);
        }
    }
    
    public void onWindowSystemUiVisibilityChanged(final int m) {
        super.onWindowSystemUiVisibilityChanged(m);
        this.t();
        final int i = this.m;
        this.m = m;
        boolean b = false;
        final boolean b2 = (m & 0x4) == 0x0;
        if ((m & 0x100) != 0x0) {
            b = true;
        }
        final d d = this.D;
        if (d != null) {
            d.e(b ^ true);
            if (!b2 && b) {
                this.D.c();
            }
            else {
                this.D.a();
            }
        }
        if (((i ^ m) & 0x100) != 0x0 && this.D != null) {
            o32.n0((View)this);
        }
    }
    
    public void onWindowVisibilityChanged(final int b) {
        super.onWindowVisibilityChanged(b);
        this.b = b;
        final d d = this.D;
        if (d != null) {
            d.onWindowVisibilityChanged(b);
        }
    }
    
    public final void p(final Context context) {
        final TypedArray obtainStyledAttributes = ((View)this).getContext().getTheme().obtainStyledAttributes(ActionBarOverlayLayout.M);
        final boolean b = false;
        this.a = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        final Drawable drawable = obtainStyledAttributes.getDrawable(1);
        this.f = drawable;
        ((View)this).setWillNotDraw(drawable == null);
        obtainStyledAttributes.recycle();
        boolean g = b;
        if (context.getApplicationInfo().targetSdkVersion < 19) {
            g = true;
        }
        this.g = g;
        this.F = new OverScroller(context);
    }
    
    public boolean q() {
        return this.h;
    }
    
    public final void r() {
        this.o();
        ((View)this).postDelayed(this.J, 600L);
    }
    
    public final void s() {
        this.o();
        ((View)this).postDelayed(this.I, 600L);
    }
    
    public void setActionBarHideOffset(int max) {
        this.o();
        max = Math.max(0, Math.min(max, ((View)this.d).getHeight()));
        ((View)this.d).setTranslationY((float)(-max));
    }
    
    public void setActionBarVisibilityCallback(final d d) {
        this.D = d;
        if (((View)this).getWindowToken() != null) {
            this.D.onWindowVisibilityChanged(this.b);
            final int m = this.m;
            if (m != 0) {
                this.onWindowSystemUiVisibilityChanged(m);
                o32.n0((View)this);
            }
        }
    }
    
    public void setHasNonEmbeddedTabs(final boolean i) {
        this.i = i;
    }
    
    public void setHideOnContentScrollEnabled(final boolean j) {
        if (j != this.j && !(this.j = j)) {
            this.o();
            this.setActionBarHideOffset(0);
        }
    }
    
    public void setIcon(final int icon) {
        this.t();
        this.e.setIcon(icon);
    }
    
    public void setIcon(final Drawable icon) {
        this.t();
        this.e.setIcon(icon);
    }
    
    public void setLogo(final int n) {
        this.t();
        this.e.w(n);
    }
    
    public void setOverlayMode(final boolean h) {
        this.h = h;
        this.g = (h && ((View)this).getContext().getApplicationInfo().targetSdkVersion < 19);
    }
    
    public void setShowingForActionMode(final boolean b) {
    }
    
    public void setUiOptions(final int n) {
    }
    
    public void setWindowCallback(final Window$Callback windowCallback) {
        this.t();
        this.e.setWindowCallback(windowCallback);
    }
    
    public void setWindowTitle(final CharSequence windowTitle) {
        this.t();
        this.e.setWindowTitle(windowTitle);
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    public void t() {
        if (this.c == null) {
            this.c = (ContentFrameLayout)((View)this).findViewById(db1.b);
            this.d = (ActionBarContainer)((View)this).findViewById(db1.c);
            this.e = this.n(((View)this).findViewById(db1.a));
        }
    }
    
    public final void u() {
        this.o();
        this.I.run();
    }
    
    public final boolean v(final float n) {
        this.F.fling(0, 0, 0, (int)n, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return this.F.getFinalY() > ((View)this.d).getHeight();
    }
    
    public interface d
    {
        void a();
        
        void b();
        
        void c();
        
        void d();
        
        void e(final boolean p0);
        
        void onWindowVisibilityChanged(final int p0);
    }
    
    public static class e extends ViewGroup$MarginLayoutParams
    {
        public e(final int n, final int n2) {
            super(n, n2);
        }
        
        public e(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        public e(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
        }
    }
}

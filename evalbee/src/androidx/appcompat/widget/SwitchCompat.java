// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.accessibility.AccessibilityRecord;
import android.graphics.Paint;
import android.animation.Animator;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import android.text.InputFilter;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityEvent;
import android.graphics.Region$Op;
import androidx.emoji2.text.c;
import android.graphics.Typeface;
import android.os.Build$VERSION;
import android.text.StaticLayout;
import android.text.Layout$Alignment;
import android.view.ActionMode$Callback;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.graphics.Canvas;
import android.view.ViewConfiguration;
import android.widget.TextView;
import android.graphics.drawable.Drawable$Callback;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.view.VelocityTracker;
import android.graphics.PorterDuff$Mode;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.text.method.TransformationMethod;
import android.animation.ObjectAnimator;
import android.text.Layout;
import android.util.Property;
import android.widget.CompoundButton;

public class SwitchCompat extends CompoundButton
{
    private static final String ACCESSIBILITY_EVENT_CLASS_NAME = "android.widget.Switch";
    private static final int[] CHECKED_STATE_SET;
    private static final int MONOSPACE = 3;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    private static final int THUMB_ANIMATION_DURATION = 250;
    private static final Property<SwitchCompat, Float> THUMB_POS;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;
    private static final int TOUCH_MODE_IDLE = 0;
    private v6 mAppCompatEmojiTextHelper;
    private c mEmojiCompatInitCallback;
    private boolean mEnforceSwitchWidth;
    private boolean mHasThumbTint;
    private boolean mHasThumbTintMode;
    private boolean mHasTrackTint;
    private boolean mHasTrackTintMode;
    private int mMinFlingVelocity;
    private Layout mOffLayout;
    private Layout mOnLayout;
    ObjectAnimator mPositionAnimator;
    private boolean mShowText;
    private boolean mSplitTrack;
    private int mSwitchBottom;
    private int mSwitchHeight;
    private int mSwitchLeft;
    private int mSwitchMinWidth;
    private int mSwitchPadding;
    private int mSwitchRight;
    private int mSwitchTop;
    private TransformationMethod mSwitchTransformationMethod;
    private int mSwitchWidth;
    private final Rect mTempRect;
    private ColorStateList mTextColors;
    private final l7 mTextHelper;
    private CharSequence mTextOff;
    private CharSequence mTextOffTransformed;
    private CharSequence mTextOn;
    private CharSequence mTextOnTransformed;
    private final TextPaint mTextPaint;
    private Drawable mThumbDrawable;
    float mThumbPosition;
    private int mThumbTextPadding;
    private ColorStateList mThumbTintList;
    private PorterDuff$Mode mThumbTintMode;
    private int mThumbWidth;
    private int mTouchMode;
    private int mTouchSlop;
    private float mTouchX;
    private float mTouchY;
    private Drawable mTrackDrawable;
    private ColorStateList mTrackTintList;
    private PorterDuff$Mode mTrackTintMode;
    private VelocityTracker mVelocityTracker;
    
    static {
        THUMB_POS = new Property(Float.class, "thumbPos") {
            public Float a(final SwitchCompat switchCompat) {
                return switchCompat.mThumbPosition;
            }
            
            public void b(final SwitchCompat switchCompat, final Float n) {
                switchCompat.setThumbPosition(n);
            }
        };
        CHECKED_STATE_SET = new int[] { 16842912 };
    }
    
    public SwitchCompat(final Context context, final AttributeSet set) {
        this(context, set, sa1.L);
    }
    
    public SwitchCompat(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mThumbTintList = null;
        this.mThumbTintMode = null;
        this.mHasThumbTint = false;
        this.mHasThumbTintMode = false;
        this.mTrackTintList = null;
        this.mTrackTintMode = null;
        this.mHasTrackTint = false;
        this.mHasTrackTintMode = false;
        this.mVelocityTracker = VelocityTracker.obtain();
        this.mEnforceSwitchWidth = true;
        this.mTempRect = new Rect();
        pv1.a((View)this, ((View)this).getContext());
        final TextPaint mTextPaint = new TextPaint(1);
        this.mTextPaint = mTextPaint;
        mTextPaint.density = ((View)this).getResources().getDisplayMetrics().density;
        final int[] l2 = bc1.L2;
        final tw1 v = tw1.v(context, set, l2, n, 0);
        o32.o0((View)this, context, l2, set, v.r(), n, 0);
        final Drawable g = v.g(bc1.O2);
        this.mThumbDrawable = g;
        if (g != null) {
            g.setCallback((Drawable$Callback)this);
        }
        final Drawable g2 = v.g(bc1.X2);
        if ((this.mTrackDrawable = g2) != null) {
            g2.setCallback((Drawable$Callback)this);
        }
        this.setTextOnInternal(v.p(bc1.M2));
        this.setTextOffInternal(v.p(bc1.N2));
        this.mShowText = v.a(bc1.P2, true);
        this.mThumbTextPadding = v.f(bc1.U2, 0);
        this.mSwitchMinWidth = v.f(bc1.R2, 0);
        this.mSwitchPadding = v.f(bc1.S2, 0);
        this.mSplitTrack = v.a(bc1.Q2, false);
        final ColorStateList c = v.c(bc1.V2);
        if (c != null) {
            this.mThumbTintList = c;
            this.mHasThumbTint = true;
        }
        final PorterDuff$Mode e = fv.e(v.k(bc1.W2, -1), null);
        if (this.mThumbTintMode != e) {
            this.mThumbTintMode = e;
            this.mHasThumbTintMode = true;
        }
        if (this.mHasThumbTint || this.mHasThumbTintMode) {
            this.b();
        }
        final ColorStateList c2 = v.c(bc1.Y2);
        if (c2 != null) {
            this.mTrackTintList = c2;
            this.mHasTrackTint = true;
        }
        final PorterDuff$Mode e2 = fv.e(v.k(bc1.Z2, -1), null);
        if (this.mTrackTintMode != e2) {
            this.mTrackTintMode = e2;
            this.mHasTrackTintMode = true;
        }
        if (this.mHasTrackTint || this.mHasTrackTintMode) {
            this.c();
        }
        final int n2 = v.n(bc1.T2, 0);
        if (n2 != 0) {
            this.setSwitchTextAppearance(context, n2);
        }
        (this.mTextHelper = new l7((TextView)this)).m(set, n);
        v.w();
        final ViewConfiguration value = ViewConfiguration.get(context);
        this.mTouchSlop = value.getScaledTouchSlop();
        this.mMinFlingVelocity = value.getScaledMinimumFlingVelocity();
        this.getEmojiTextViewHelper().c(set, n);
        ((View)this).refreshDrawableState();
        this.setChecked(this.isChecked());
    }
    
    public static float f(final float n, float n2, final float n3) {
        if (n >= n2) {
            n2 = n;
            if (n > n3) {
                n2 = n3;
            }
        }
        return n2;
    }
    
    private v6 getEmojiTextViewHelper() {
        if (this.mAppCompatEmojiTextHelper == null) {
            this.mAppCompatEmojiTextHelper = new v6((TextView)this);
        }
        return this.mAppCompatEmojiTextHelper;
    }
    
    private boolean getTargetCheckedState() {
        return this.mThumbPosition > 0.5f;
    }
    
    private int getThumbOffset() {
        float mThumbPosition;
        if (u42.b((View)this)) {
            mThumbPosition = 1.0f - this.mThumbPosition;
        }
        else {
            mThumbPosition = this.mThumbPosition;
        }
        return (int)(mThumbPosition * this.getThumbScrollRange() + 0.5f);
    }
    
    private int getThumbScrollRange() {
        final Drawable mTrackDrawable = this.mTrackDrawable;
        if (mTrackDrawable != null) {
            final Rect mTempRect = this.mTempRect;
            mTrackDrawable.getPadding(mTempRect);
            final Drawable mThumbDrawable = this.mThumbDrawable;
            Rect rect;
            if (mThumbDrawable != null) {
                rect = fv.d(mThumbDrawable);
            }
            else {
                rect = fv.c;
            }
            return this.mSwitchWidth - this.mThumbWidth - mTempRect.left - mTempRect.right - rect.left - rect.right;
        }
        return 0;
    }
    
    private void setTextOffInternal(final CharSequence mTextOff) {
        this.mTextOff = mTextOff;
        this.mTextOffTransformed = this.g(mTextOff);
        this.mOffLayout = null;
        if (this.mShowText) {
            this.m();
        }
    }
    
    private void setTextOnInternal(final CharSequence mTextOn) {
        this.mTextOn = mTextOn;
        this.mTextOnTransformed = this.g(mTextOn);
        this.mOnLayout = null;
        if (this.mShowText) {
            this.m();
        }
    }
    
    public final void a(final boolean b) {
        float n;
        if (b) {
            n = 1.0f;
        }
        else {
            n = 0.0f;
        }
        (this.mPositionAnimator = ObjectAnimator.ofFloat((Object)this, (Property)SwitchCompat.THUMB_POS, new float[] { n })).setDuration(250L);
        SwitchCompat.b.a(this.mPositionAnimator, true);
        this.mPositionAnimator.start();
    }
    
    public final void b() {
        final Drawable mThumbDrawable = this.mThumbDrawable;
        if (mThumbDrawable != null && (this.mHasThumbTint || this.mHasThumbTintMode)) {
            final Drawable mutate = wu.r(mThumbDrawable).mutate();
            this.mThumbDrawable = mutate;
            if (this.mHasThumbTint) {
                wu.o(mutate, this.mThumbTintList);
            }
            if (this.mHasThumbTintMode) {
                wu.p(this.mThumbDrawable, this.mThumbTintMode);
            }
            if (this.mThumbDrawable.isStateful()) {
                this.mThumbDrawable.setState(((View)this).getDrawableState());
            }
        }
    }
    
    public final void c() {
        final Drawable mTrackDrawable = this.mTrackDrawable;
        if (mTrackDrawable != null && (this.mHasTrackTint || this.mHasTrackTintMode)) {
            final Drawable mutate = wu.r(mTrackDrawable).mutate();
            this.mTrackDrawable = mutate;
            if (this.mHasTrackTint) {
                wu.o(mutate, this.mTrackTintList);
            }
            if (this.mHasTrackTintMode) {
                wu.p(this.mTrackDrawable, this.mTrackTintMode);
            }
            if (this.mTrackDrawable.isStateful()) {
                this.mTrackDrawable.setState(((View)this).getDrawableState());
            }
        }
    }
    
    public final void d() {
        final ObjectAnimator mPositionAnimator = this.mPositionAnimator;
        if (mPositionAnimator != null) {
            ((Animator)mPositionAnimator).cancel();
        }
    }
    
    public void draw(final Canvas canvas) {
        final Rect mTempRect = this.mTempRect;
        int mSwitchLeft = this.mSwitchLeft;
        final int mSwitchTop = this.mSwitchTop;
        int mSwitchRight = this.mSwitchRight;
        final int mSwitchBottom = this.mSwitchBottom;
        final int n = this.getThumbOffset() + mSwitchLeft;
        final Drawable mThumbDrawable = this.mThumbDrawable;
        Rect rect;
        if (mThumbDrawable != null) {
            rect = fv.d(mThumbDrawable);
        }
        else {
            rect = fv.c;
        }
        final Drawable mTrackDrawable = this.mTrackDrawable;
        int n2 = n;
        if (mTrackDrawable != null) {
            mTrackDrawable.getPadding(mTempRect);
            final int left = mTempRect.left;
            final int n3 = n + left;
            int n4 = 0;
            int n5 = 0;
            int n6 = 0;
            int n8 = 0;
            Label_0258: {
                int n7;
                if (rect != null) {
                    final int left2 = rect.left;
                    n4 = mSwitchLeft;
                    if (left2 > left) {
                        n4 = mSwitchLeft + (left2 - left);
                    }
                    final int top = rect.top;
                    final int top2 = mTempRect.top;
                    if (top > top2) {
                        n5 = top - top2 + mSwitchTop;
                    }
                    else {
                        n5 = mSwitchTop;
                    }
                    final int right = rect.right;
                    final int right2 = mTempRect.right;
                    n6 = mSwitchRight;
                    if (right > right2) {
                        n6 = mSwitchRight - (right - right2);
                    }
                    final int bottom = rect.bottom;
                    final int bottom2 = mTempRect.bottom;
                    mSwitchLeft = n4;
                    mSwitchRight = n6;
                    n7 = n5;
                    if (bottom > bottom2) {
                        n8 = mSwitchBottom - (bottom - bottom2);
                        break Label_0258;
                    }
                }
                else {
                    n7 = mSwitchTop;
                }
                final int n9 = mSwitchBottom;
                n5 = n7;
                n8 = n9;
                n6 = mSwitchRight;
                n4 = mSwitchLeft;
            }
            this.mTrackDrawable.setBounds(n4, n5, n6, n8);
            n2 = n3;
        }
        final Drawable mThumbDrawable2 = this.mThumbDrawable;
        if (mThumbDrawable2 != null) {
            mThumbDrawable2.getPadding(mTempRect);
            final int n10 = n2 - mTempRect.left;
            final int n11 = n2 + this.mThumbWidth + mTempRect.right;
            this.mThumbDrawable.setBounds(n10, mSwitchTop, n11, mSwitchBottom);
            final Drawable background = ((View)this).getBackground();
            if (background != null) {
                wu.l(background, n10, mSwitchTop, n11, mSwitchBottom);
            }
        }
        super.draw(canvas);
    }
    
    public void drawableHotspotChanged(final float n, final float n2) {
        super.drawableHotspotChanged(n, n2);
        final Drawable mThumbDrawable = this.mThumbDrawable;
        if (mThumbDrawable != null) {
            wu.k(mThumbDrawable, n, n2);
        }
        final Drawable mTrackDrawable = this.mTrackDrawable;
        if (mTrackDrawable != null) {
            wu.k(mTrackDrawable, n, n2);
        }
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final int[] drawableState = ((View)this).getDrawableState();
        final Drawable mThumbDrawable = this.mThumbDrawable;
        int n2;
        final int n = n2 = 0;
        if (mThumbDrawable != null) {
            n2 = n;
            if (mThumbDrawable.isStateful()) {
                n2 = ((false | mThumbDrawable.setState(drawableState)) ? 1 : 0);
            }
        }
        final Drawable mTrackDrawable = this.mTrackDrawable;
        int n3 = n2;
        if (mTrackDrawable != null) {
            n3 = n2;
            if (mTrackDrawable.isStateful()) {
                n3 = (n2 | (mTrackDrawable.setState(drawableState) ? 1 : 0));
            }
        }
        if (n3 != 0) {
            ((View)this).invalidate();
        }
    }
    
    public final void e(MotionEvent obtain) {
        obtain = MotionEvent.obtain(obtain);
        obtain.setAction(3);
        super.onTouchEvent(obtain);
        obtain.recycle();
    }
    
    public final CharSequence g(final CharSequence charSequence) {
        final TransformationMethod f = this.getEmojiTextViewHelper().f(this.mSwitchTransformationMethod);
        CharSequence transformation = charSequence;
        if (f != null) {
            transformation = f.getTransformation(charSequence, (View)this);
        }
        return transformation;
    }
    
    public int getCompoundPaddingLeft() {
        if (!u42.b((View)this)) {
            return super.getCompoundPaddingLeft();
        }
        int n = super.getCompoundPaddingLeft() + this.mSwitchWidth;
        if (!TextUtils.isEmpty(((TextView)this).getText())) {
            n += this.mSwitchPadding;
        }
        return n;
    }
    
    public int getCompoundPaddingRight() {
        if (u42.b((View)this)) {
            return super.getCompoundPaddingRight();
        }
        int n = super.getCompoundPaddingRight() + this.mSwitchWidth;
        if (!TextUtils.isEmpty(((TextView)this).getText())) {
            n += this.mSwitchPadding;
        }
        return n;
    }
    
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return nv1.q(super.getCustomSelectionActionModeCallback());
    }
    
    public boolean getShowText() {
        return this.mShowText;
    }
    
    public boolean getSplitTrack() {
        return this.mSplitTrack;
    }
    
    public int getSwitchMinWidth() {
        return this.mSwitchMinWidth;
    }
    
    public int getSwitchPadding() {
        return this.mSwitchPadding;
    }
    
    public CharSequence getTextOff() {
        return this.mTextOff;
    }
    
    public CharSequence getTextOn() {
        return this.mTextOn;
    }
    
    public Drawable getThumbDrawable() {
        return this.mThumbDrawable;
    }
    
    public final float getThumbPosition() {
        return this.mThumbPosition;
    }
    
    public int getThumbTextPadding() {
        return this.mThumbTextPadding;
    }
    
    public ColorStateList getThumbTintList() {
        return this.mThumbTintList;
    }
    
    public PorterDuff$Mode getThumbTintMode() {
        return this.mThumbTintMode;
    }
    
    public Drawable getTrackDrawable() {
        return this.mTrackDrawable;
    }
    
    public ColorStateList getTrackTintList() {
        return this.mTrackTintList;
    }
    
    public PorterDuff$Mode getTrackTintMode() {
        return this.mTrackTintMode;
    }
    
    public final boolean h(final float n, final float n2) {
        final Drawable mThumbDrawable = this.mThumbDrawable;
        final boolean b = false;
        if (mThumbDrawable == null) {
            return false;
        }
        final int thumbOffset = this.getThumbOffset();
        this.mThumbDrawable.getPadding(this.mTempRect);
        final int mSwitchTop = this.mSwitchTop;
        final int mTouchSlop = this.mTouchSlop;
        final int n3 = this.mSwitchLeft + thumbOffset - mTouchSlop;
        final int mThumbWidth = this.mThumbWidth;
        final Rect mTempRect = this.mTempRect;
        final int left = mTempRect.left;
        final int right = mTempRect.right;
        final int mSwitchBottom = this.mSwitchBottom;
        boolean b2 = b;
        if (n > n3) {
            b2 = b;
            if (n < mThumbWidth + n3 + left + right + mTouchSlop) {
                b2 = b;
                if (n2 > mSwitchTop - mTouchSlop) {
                    b2 = b;
                    if (n2 < mSwitchBottom + mTouchSlop) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public final Layout i(final CharSequence charSequence) {
        final TextPaint mTextPaint = this.mTextPaint;
        int n;
        if (charSequence != null) {
            n = (int)Math.ceil(Layout.getDesiredWidth(charSequence, mTextPaint));
        }
        else {
            n = 0;
        }
        return (Layout)new StaticLayout(charSequence, mTextPaint, n, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
    }
    
    public boolean isEmojiCompatEnabled() {
        return this.getEmojiTextViewHelper().b();
    }
    
    public final void j() {
        if (Build$VERSION.SDK_INT >= 30) {
            CharSequence charSequence;
            if ((charSequence = this.mTextOff) == null) {
                charSequence = ((View)this).getResources().getString(qb1.b);
            }
            o32.J0((View)this, charSequence);
        }
    }
    
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        final Drawable mThumbDrawable = this.mThumbDrawable;
        if (mThumbDrawable != null) {
            mThumbDrawable.jumpToCurrentState();
        }
        final Drawable mTrackDrawable = this.mTrackDrawable;
        if (mTrackDrawable != null) {
            mTrackDrawable.jumpToCurrentState();
        }
        final ObjectAnimator mPositionAnimator = this.mPositionAnimator;
        if (mPositionAnimator != null && ((Animator)mPositionAnimator).isStarted()) {
            ((Animator)this.mPositionAnimator).end();
            this.mPositionAnimator = null;
        }
    }
    
    public final void k() {
        if (Build$VERSION.SDK_INT >= 30) {
            CharSequence charSequence;
            if ((charSequence = this.mTextOn) == null) {
                charSequence = ((View)this).getResources().getString(qb1.c);
            }
            o32.J0((View)this, charSequence);
        }
    }
    
    public final void l(final int n, final int n2) {
        Typeface typeface;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    typeface = null;
                }
                else {
                    typeface = Typeface.MONOSPACE;
                }
            }
            else {
                typeface = Typeface.SERIF;
            }
        }
        else {
            typeface = Typeface.SANS_SERIF;
        }
        this.setSwitchTypeface(typeface, n2);
    }
    
    public final void m() {
        if (this.mEmojiCompatInitCallback == null) {
            if (this.mAppCompatEmojiTextHelper.b()) {
                if (androidx.emoji2.text.c.h()) {
                    final androidx.emoji2.text.c b = androidx.emoji2.text.c.b();
                    final int d = b.d();
                    if (d == 3 || d == 0) {
                        b.s((androidx.emoji2.text.c.e)(this.mEmojiCompatInitCallback = new c(this)));
                    }
                }
            }
        }
    }
    
    public final void n(final MotionEvent motionEvent) {
        this.mTouchMode = 0;
        final int action = motionEvent.getAction();
        boolean targetCheckedState = true;
        final boolean b = action == 1 && ((View)this).isEnabled();
        final boolean checked = this.isChecked();
        Label_0117: {
            if (b) {
                this.mVelocityTracker.computeCurrentVelocity(1000);
                final float xVelocity = this.mVelocityTracker.getXVelocity();
                if (Math.abs(xVelocity) > this.mMinFlingVelocity) {
                    if (u42.b((View)this)) {
                        if (xVelocity < 0.0f) {
                            break Label_0117;
                        }
                    }
                    else if (xVelocity > 0.0f) {
                        break Label_0117;
                    }
                    targetCheckedState = false;
                }
                else {
                    targetCheckedState = this.getTargetCheckedState();
                }
            }
            else {
                targetCheckedState = checked;
            }
        }
        if (targetCheckedState != checked) {
            ((View)this).playSoundEffect(0);
        }
        this.setChecked(targetCheckedState);
        this.e(motionEvent);
    }
    
    public int[] onCreateDrawableState(final int n) {
        final int[] onCreateDrawableState = super.onCreateDrawableState(n + 1);
        if (this.isChecked()) {
            View.mergeDrawableStates(onCreateDrawableState, SwitchCompat.CHECKED_STATE_SET);
        }
        return onCreateDrawableState;
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final Rect mTempRect = this.mTempRect;
        final Drawable mTrackDrawable = this.mTrackDrawable;
        if (mTrackDrawable != null) {
            mTrackDrawable.getPadding(mTempRect);
        }
        else {
            mTempRect.setEmpty();
        }
        final int mSwitchTop = this.mSwitchTop;
        final int mSwitchBottom = this.mSwitchBottom;
        final int top = mTempRect.top;
        final int bottom = mTempRect.bottom;
        final Drawable mThumbDrawable = this.mThumbDrawable;
        if (mTrackDrawable != null) {
            if (this.mSplitTrack && mThumbDrawable != null) {
                final Rect d = fv.d(mThumbDrawable);
                mThumbDrawable.copyBounds(mTempRect);
                mTempRect.left += d.left;
                mTempRect.right -= d.right;
                final int save = canvas.save();
                canvas.clipRect(mTempRect, Region$Op.DIFFERENCE);
                mTrackDrawable.draw(canvas);
                canvas.restoreToCount(save);
            }
            else {
                mTrackDrawable.draw(canvas);
            }
        }
        final int save2 = canvas.save();
        if (mThumbDrawable != null) {
            mThumbDrawable.draw(canvas);
        }
        Layout layout;
        if (this.getTargetCheckedState()) {
            layout = this.mOnLayout;
        }
        else {
            layout = this.mOffLayout;
        }
        if (layout != null) {
            final int[] drawableState = ((View)this).getDrawableState();
            final ColorStateList mTextColors = this.mTextColors;
            if (mTextColors != null) {
                ((Paint)this.mTextPaint).setColor(mTextColors.getColorForState(drawableState, 0));
            }
            this.mTextPaint.drawableState = drawableState;
            int width;
            if (mThumbDrawable != null) {
                final Rect bounds = mThumbDrawable.getBounds();
                width = bounds.left + bounds.right;
            }
            else {
                width = ((View)this).getWidth();
            }
            canvas.translate((float)(width / 2 - layout.getWidth() / 2), (float)((mSwitchTop + top + (mSwitchBottom - bottom)) / 2 - layout.getHeight() / 2));
            layout.draw(canvas);
        }
        canvas.restoreToCount(save2);
    }
    
    public void onEmojiCompatInitializedForSwitchText() {
        this.setTextOnInternal(this.mTextOn);
        this.setTextOffInternal(this.mTextOff);
        ((View)this).requestLayout();
    }
    
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)"android.widget.Switch");
    }
    
    public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName((CharSequence)"android.widget.Switch");
        if (Build$VERSION.SDK_INT < 30) {
            CharSequence charSequence;
            if (this.isChecked()) {
                charSequence = this.mTextOn;
            }
            else {
                charSequence = this.mTextOff;
            }
            if (!TextUtils.isEmpty(charSequence)) {
                final CharSequence text = accessibilityNodeInfo.getText();
                if (TextUtils.isEmpty(text)) {
                    accessibilityNodeInfo.setText(charSequence);
                }
                else {
                    final StringBuilder text2 = new StringBuilder();
                    text2.append(text);
                    text2.append(' ');
                    text2.append(charSequence);
                    accessibilityNodeInfo.setText((CharSequence)text2);
                }
            }
        }
    }
    
    public void onLayout(final boolean b, int mSwitchTop, int max, int mSwitchRight, int mSwitchBottom) {
        super.onLayout(b, mSwitchTop, max, mSwitchRight, mSwitchBottom);
        final Drawable mThumbDrawable = this.mThumbDrawable;
        max = 0;
        if (mThumbDrawable != null) {
            final Rect mTempRect = this.mTempRect;
            final Drawable mTrackDrawable = this.mTrackDrawable;
            if (mTrackDrawable != null) {
                mTrackDrawable.getPadding(mTempRect);
            }
            else {
                mTempRect.setEmpty();
            }
            final Rect d = fv.d(this.mThumbDrawable);
            mSwitchTop = Math.max(0, d.left - mTempRect.left);
            max = Math.max(0, d.right - mTempRect.right);
        }
        else {
            mSwitchTop = 0;
        }
        if (u42.b((View)this)) {
            mSwitchRight = ((View)this).getPaddingLeft() + mSwitchTop;
            mSwitchTop = this.mSwitchWidth + mSwitchRight - mSwitchTop - max;
            max = mSwitchRight;
            mSwitchRight = mSwitchTop;
        }
        else {
            mSwitchRight = ((View)this).getWidth() - ((View)this).getPaddingRight() - max;
            max += mSwitchRight - this.mSwitchWidth + mSwitchTop;
        }
        mSwitchTop = (((TextView)this).getGravity() & 0x70);
        Label_0256: {
            if (mSwitchTop != 16) {
                if (mSwitchTop == 80) {
                    mSwitchBottom = ((View)this).getHeight() - ((View)this).getPaddingBottom();
                    mSwitchTop = mSwitchBottom - this.mSwitchHeight;
                    break Label_0256;
                }
                mSwitchTop = ((View)this).getPaddingTop();
                mSwitchBottom = this.mSwitchHeight;
            }
            else {
                mSwitchTop = (((View)this).getPaddingTop() + ((View)this).getHeight() - ((View)this).getPaddingBottom()) / 2;
                mSwitchBottom = this.mSwitchHeight;
                mSwitchTop -= mSwitchBottom / 2;
            }
            mSwitchBottom += mSwitchTop;
        }
        this.mSwitchLeft = max;
        this.mSwitchTop = mSwitchTop;
        this.mSwitchBottom = mSwitchBottom;
        this.mSwitchRight = mSwitchRight;
    }
    
    public void onMeasure(final int n, final int n2) {
        if (this.mShowText) {
            if (this.mOnLayout == null) {
                this.mOnLayout = this.i(this.mTextOnTransformed);
            }
            if (this.mOffLayout == null) {
                this.mOffLayout = this.i(this.mTextOffTransformed);
            }
        }
        final Rect mTempRect = this.mTempRect;
        final Drawable mThumbDrawable = this.mThumbDrawable;
        final int n3 = 0;
        int b;
        int intrinsicHeight;
        if (mThumbDrawable != null) {
            mThumbDrawable.getPadding(mTempRect);
            b = this.mThumbDrawable.getIntrinsicWidth() - mTempRect.left - mTempRect.right;
            intrinsicHeight = this.mThumbDrawable.getIntrinsicHeight();
        }
        else {
            b = 0;
            intrinsicHeight = 0;
        }
        int a;
        if (this.mShowText) {
            a = Math.max(this.mOnLayout.getWidth(), this.mOffLayout.getWidth()) + this.mThumbTextPadding * 2;
        }
        else {
            a = 0;
        }
        this.mThumbWidth = Math.max(a, b);
        final Drawable mTrackDrawable = this.mTrackDrawable;
        int intrinsicHeight2;
        if (mTrackDrawable != null) {
            mTrackDrawable.getPadding(mTempRect);
            intrinsicHeight2 = this.mTrackDrawable.getIntrinsicHeight();
        }
        else {
            mTempRect.setEmpty();
            intrinsicHeight2 = n3;
        }
        final int left = mTempRect.left;
        final int right = mTempRect.right;
        final Drawable mThumbDrawable2 = this.mThumbDrawable;
        int max = right;
        int max2 = left;
        if (mThumbDrawable2 != null) {
            final Rect d = fv.d(mThumbDrawable2);
            max2 = Math.max(left, d.left);
            max = Math.max(right, d.right);
        }
        int mSwitchWidth;
        if (this.mEnforceSwitchWidth) {
            mSwitchWidth = Math.max(this.mSwitchMinWidth, this.mThumbWidth * 2 + max2 + max);
        }
        else {
            mSwitchWidth = this.mSwitchMinWidth;
        }
        final int max3 = Math.max(intrinsicHeight2, intrinsicHeight);
        this.mSwitchWidth = mSwitchWidth;
        this.mSwitchHeight = max3;
        super.onMeasure(n, n2);
        if (((View)this).getMeasuredHeight() < max3) {
            ((View)this).setMeasuredDimension(((View)this).getMeasuredWidthAndState(), max3);
        }
    }
    
    public void onPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onPopulateAccessibilityEvent(accessibilityEvent);
        CharSequence charSequence;
        if (this.isChecked()) {
            charSequence = this.mTextOn;
        }
        else {
            charSequence = this.mTextOff;
        }
        if (charSequence != null) {
            ((AccessibilityRecord)accessibilityEvent).getText().add(charSequence);
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        this.mVelocityTracker.addMovement(motionEvent);
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                    if (actionMasked != 3) {
                        return super.onTouchEvent(motionEvent);
                    }
                }
                else {
                    final int mTouchMode = this.mTouchMode;
                    if (mTouchMode != 1) {
                        if (mTouchMode != 2) {
                            return super.onTouchEvent(motionEvent);
                        }
                        final float x = motionEvent.getX();
                        final int thumbScrollRange = this.getThumbScrollRange();
                        final float n = x - this.mTouchX;
                        float n2;
                        if (thumbScrollRange != 0) {
                            n2 = n / thumbScrollRange;
                        }
                        else if (n > 0.0f) {
                            n2 = 1.0f;
                        }
                        else {
                            n2 = -1.0f;
                        }
                        float n3 = n2;
                        if (u42.b((View)this)) {
                            n3 = -n2;
                        }
                        final float f = f(this.mThumbPosition + n3, 0.0f, 1.0f);
                        if (f != this.mThumbPosition) {
                            this.mTouchX = x;
                            this.setThumbPosition(f);
                        }
                        return true;
                    }
                    else {
                        final float x2 = motionEvent.getX();
                        final float y = motionEvent.getY();
                        if (Math.abs(x2 - this.mTouchX) > this.mTouchSlop || Math.abs(y - this.mTouchY) > this.mTouchSlop) {
                            this.mTouchMode = 2;
                            ((View)this).getParent().requestDisallowInterceptTouchEvent(true);
                            this.mTouchX = x2;
                            this.mTouchY = y;
                            return true;
                        }
                        return super.onTouchEvent(motionEvent);
                    }
                }
            }
            if (this.mTouchMode == 2) {
                this.n(motionEvent);
                super.onTouchEvent(motionEvent);
                return true;
            }
            this.mTouchMode = 0;
            this.mVelocityTracker.clear();
        }
        else {
            final float x3 = motionEvent.getX();
            final float y2 = motionEvent.getY();
            if (((View)this).isEnabled() && this.h(x3, y2)) {
                this.mTouchMode = 1;
                this.mTouchX = x3;
                this.mTouchY = y2;
            }
        }
        return super.onTouchEvent(motionEvent);
    }
    
    public void setAllCaps(final boolean allCaps) {
        super.setAllCaps(allCaps);
        this.getEmojiTextViewHelper().d(allCaps);
    }
    
    public void setChecked(final boolean checked) {
        super.setChecked(checked);
        final boolean checked2 = this.isChecked();
        if (checked2) {
            this.k();
        }
        else {
            this.j();
        }
        if (((View)this).getWindowToken() != null && o32.U((View)this)) {
            this.a(checked2);
        }
        else {
            this.d();
            float thumbPosition;
            if (checked2) {
                thumbPosition = 1.0f;
            }
            else {
                thumbPosition = 0.0f;
            }
            this.setThumbPosition(thumbPosition);
        }
    }
    
    public void setCustomSelectionActionModeCallback(final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(nv1.r((TextView)this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean b) {
        this.getEmojiTextViewHelper().e(b);
        this.setTextOnInternal(this.mTextOn);
        this.setTextOffInternal(this.mTextOff);
        ((View)this).requestLayout();
    }
    
    public final void setEnforceSwitchWidth(final boolean mEnforceSwitchWidth) {
        this.mEnforceSwitchWidth = mEnforceSwitchWidth;
        ((View)this).invalidate();
    }
    
    public void setFilters(final InputFilter[] array) {
        super.setFilters(this.getEmojiTextViewHelper().a(array));
    }
    
    public void setShowText(final boolean mShowText) {
        if (this.mShowText != mShowText) {
            this.mShowText = mShowText;
            ((View)this).requestLayout();
            if (mShowText) {
                this.m();
            }
        }
    }
    
    public void setSplitTrack(final boolean mSplitTrack) {
        this.mSplitTrack = mSplitTrack;
        ((View)this).invalidate();
    }
    
    public void setSwitchMinWidth(final int mSwitchMinWidth) {
        this.mSwitchMinWidth = mSwitchMinWidth;
        ((View)this).requestLayout();
    }
    
    public void setSwitchPadding(final int mSwitchPadding) {
        this.mSwitchPadding = mSwitchPadding;
        ((View)this).requestLayout();
    }
    
    public void setSwitchTextAppearance(final Context context, int f) {
        final tw1 t = tw1.t(context, f, bc1.a3);
        ColorStateList mTextColors = t.c(bc1.e3);
        if (mTextColors == null) {
            mTextColors = ((TextView)this).getTextColors();
        }
        this.mTextColors = mTextColors;
        f = t.f(bc1.b3, 0);
        if (f != 0) {
            final float textSize = (float)f;
            if (textSize != ((Paint)this.mTextPaint).getTextSize()) {
                ((Paint)this.mTextPaint).setTextSize(textSize);
                ((View)this).requestLayout();
            }
        }
        this.l(t.k(bc1.c3, -1), t.k(bc1.d3, -1));
        Object mSwitchTransformationMethod;
        if (t.a(bc1.j3, false)) {
            mSwitchTransformationMethod = new c4(((View)this).getContext());
        }
        else {
            mSwitchTransformationMethod = null;
        }
        this.mSwitchTransformationMethod = (TransformationMethod)mSwitchTransformationMethod;
        this.setTextOnInternal(this.mTextOn);
        this.setTextOffInternal(this.mTextOff);
        t.w();
    }
    
    public void setSwitchTypeface(final Typeface typeface) {
        if ((((Paint)this.mTextPaint).getTypeface() != null && !((Paint)this.mTextPaint).getTypeface().equals((Object)typeface)) || (((Paint)this.mTextPaint).getTypeface() == null && typeface != null)) {
            ((Paint)this.mTextPaint).setTypeface(typeface);
            ((View)this).requestLayout();
            ((View)this).invalidate();
        }
    }
    
    public void setSwitchTypeface(Typeface typeface, int n) {
        float textSkewX = 0.0f;
        boolean fakeBoldText = false;
        if (n > 0) {
            if (typeface == null) {
                typeface = Typeface.defaultFromStyle(n);
            }
            else {
                typeface = Typeface.create(typeface, n);
            }
            this.setSwitchTypeface(typeface);
            int style;
            if (typeface != null) {
                style = typeface.getStyle();
            }
            else {
                style = 0;
            }
            n &= ~style;
            final TextPaint mTextPaint = this.mTextPaint;
            if ((n & 0x1) != 0x0) {
                fakeBoldText = true;
            }
            ((Paint)mTextPaint).setFakeBoldText(fakeBoldText);
            final TextPaint mTextPaint2 = this.mTextPaint;
            if ((n & 0x2) != 0x0) {
                textSkewX = -0.25f;
            }
            ((Paint)mTextPaint2).setTextSkewX(textSkewX);
        }
        else {
            ((Paint)this.mTextPaint).setFakeBoldText(false);
            ((Paint)this.mTextPaint).setTextSkewX(0.0f);
            this.setSwitchTypeface(typeface);
        }
    }
    
    public void setTextOff(final CharSequence textOffInternal) {
        this.setTextOffInternal(textOffInternal);
        ((View)this).requestLayout();
        if (!this.isChecked()) {
            this.j();
        }
    }
    
    public void setTextOn(final CharSequence textOnInternal) {
        this.setTextOnInternal(textOnInternal);
        ((View)this).requestLayout();
        if (this.isChecked()) {
            this.k();
        }
    }
    
    public void setThumbDrawable(final Drawable mThumbDrawable) {
        final Drawable mThumbDrawable2 = this.mThumbDrawable;
        if (mThumbDrawable2 != null) {
            mThumbDrawable2.setCallback((Drawable$Callback)null);
        }
        if ((this.mThumbDrawable = mThumbDrawable) != null) {
            mThumbDrawable.setCallback((Drawable$Callback)this);
        }
        ((View)this).requestLayout();
    }
    
    public void setThumbPosition(final float mThumbPosition) {
        this.mThumbPosition = mThumbPosition;
        ((View)this).invalidate();
    }
    
    public void setThumbResource(final int n) {
        this.setThumbDrawable(g7.b(((View)this).getContext(), n));
    }
    
    public void setThumbTextPadding(final int mThumbTextPadding) {
        this.mThumbTextPadding = mThumbTextPadding;
        ((View)this).requestLayout();
    }
    
    public void setThumbTintList(final ColorStateList mThumbTintList) {
        this.mThumbTintList = mThumbTintList;
        this.mHasThumbTint = true;
        this.b();
    }
    
    public void setThumbTintMode(final PorterDuff$Mode mThumbTintMode) {
        this.mThumbTintMode = mThumbTintMode;
        this.mHasThumbTintMode = true;
        this.b();
    }
    
    public void setTrackDrawable(final Drawable mTrackDrawable) {
        final Drawable mTrackDrawable2 = this.mTrackDrawable;
        if (mTrackDrawable2 != null) {
            mTrackDrawable2.setCallback((Drawable$Callback)null);
        }
        if ((this.mTrackDrawable = mTrackDrawable) != null) {
            mTrackDrawable.setCallback((Drawable$Callback)this);
        }
        ((View)this).requestLayout();
    }
    
    public void setTrackResource(final int n) {
        this.setTrackDrawable(g7.b(((View)this).getContext(), n));
    }
    
    public void setTrackTintList(final ColorStateList mTrackTintList) {
        this.mTrackTintList = mTrackTintList;
        this.mHasTrackTint = true;
        this.c();
    }
    
    public void setTrackTintMode(final PorterDuff$Mode mTrackTintMode) {
        this.mTrackTintMode = mTrackTintMode;
        this.mHasTrackTintMode = true;
        this.c();
    }
    
    public void toggle() {
        this.setChecked(this.isChecked() ^ true);
    }
    
    public boolean verifyDrawable(final Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.mThumbDrawable || drawable == this.mTrackDrawable;
    }
    
    public abstract static class b
    {
        public static void a(final ObjectAnimator objectAnimator, final boolean autoCancel) {
            objectAnimator.setAutoCancel(autoCancel);
        }
    }
    
    public static class c extends e
    {
        public final Reference a;
        
        public c(final SwitchCompat referent) {
            this.a = new WeakReference(referent);
        }
        
        @Override
        public void a(final Throwable t) {
            final SwitchCompat switchCompat = this.a.get();
            if (switchCompat != null) {
                switchCompat.onEmojiCompatInitializedForSwitchText();
            }
        }
        
        @Override
        public void b() {
            final SwitchCompat switchCompat = this.a.get();
            if (switchCompat != null) {
                switchCompat.onEmojiCompatInitializedForSwitchText();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import androidx.appcompat.view.menu.l;
import android.view.ViewParent;
import java.util.Objects;
import android.os.Build$VERSION;
import android.text.TextUtils$TruncateAt;
import android.view.ContextThemeWrapper;
import java.util.Collection;
import android.view.View$MeasureSpec;
import android.os.Parcelable;
import android.view.MotionEvent;
import android.text.Layout;
import java.util.Iterator;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.View$OnClickListener;
import androidx.appcompat.view.menu.g;
import android.view.ViewGroup$LayoutParams;
import androidx.lifecycle.Lifecycle;
import java.util.List;
import android.view.MenuInflater;
import android.view.Menu;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import android.content.res.ColorStateList;
import android.view.MenuItem;
import android.content.Context;
import androidx.appcompat.view.menu.e;
import android.widget.ImageView;
import java.util.ArrayList;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.widget.ImageButton;
import android.window.OnBackInvokedDispatcher;
import android.window.OnBackInvokedCallback;
import androidx.appcompat.view.menu.i;
import android.view.ViewGroup;

public class Toolbar extends ViewGroup
{
    private static final String TAG = "Toolbar";
    private androidx.appcompat.view.menu.i.a mActionMenuPresenterCallback;
    private OnBackInvokedCallback mBackInvokedCallback;
    private boolean mBackInvokedCallbackEnabled;
    private OnBackInvokedDispatcher mBackInvokedDispatcher;
    int mButtonGravity;
    ImageButton mCollapseButtonView;
    private CharSequence mCollapseDescription;
    private Drawable mCollapseIcon;
    private boolean mCollapsible;
    private int mContentInsetEndWithActions;
    private int mContentInsetStartWithNavigation;
    private yf1 mContentInsets;
    private boolean mEatingHover;
    private boolean mEatingTouch;
    View mExpandedActionView;
    private f mExpandedMenuPresenter;
    private int mGravity;
    private final ArrayList<View> mHiddenViews;
    private ImageView mLogoView;
    private int mMaxButtonHeight;
    androidx.appcompat.view.menu.e.a mMenuBuilderCallback;
    final lv0 mMenuHostHelper;
    ActionMenuView mMenuView;
    private final ActionMenuView.e mMenuViewItemClickListener;
    private ImageButton mNavButtonView;
    h mOnMenuItemClickListener;
    private a mOuterActionMenuPresenter;
    private Context mPopupContext;
    private int mPopupTheme;
    private ArrayList<MenuItem> mProvidedMenuItems;
    private final Runnable mShowOverflowMenuRunnable;
    private CharSequence mSubtitleText;
    private int mSubtitleTextAppearance;
    private ColorStateList mSubtitleTextColor;
    private TextView mSubtitleTextView;
    private final int[] mTempMargins;
    private final ArrayList<View> mTempViews;
    private int mTitleMarginBottom;
    private int mTitleMarginEnd;
    private int mTitleMarginStart;
    private int mTitleMarginTop;
    private CharSequence mTitleText;
    private int mTitleTextAppearance;
    private ColorStateList mTitleTextColor;
    private TextView mTitleTextView;
    private d mWrapper;
    
    public Toolbar(final Context context, final AttributeSet set) {
        this(context, set, sa1.O);
    }
    
    public Toolbar(final Context context, final AttributeSet set, int n) {
        super(context, set, n);
        this.mGravity = 8388627;
        this.mTempViews = new ArrayList<View>();
        this.mHiddenViews = new ArrayList<View>();
        this.mTempMargins = new int[2];
        this.mMenuHostHelper = new lv0(new yw1(this));
        this.mProvidedMenuItems = new ArrayList<MenuItem>();
        this.mMenuViewItemClickListener = new ActionMenuView.e(this) {
            public final Toolbar a;
            
            @Override
            public boolean onMenuItemClick(final MenuItem menuItem) {
                if (this.a.mMenuHostHelper.j(menuItem)) {
                    return true;
                }
                final h mOnMenuItemClickListener = this.a.mOnMenuItemClickListener;
                return mOnMenuItemClickListener != null && mOnMenuItemClickListener.onMenuItemClick(menuItem);
            }
        };
        this.mShowOverflowMenuRunnable = new Runnable(this) {
            public final Toolbar a;
            
            @Override
            public void run() {
                this.a.showOverflowMenu();
            }
        };
        final Context context2 = ((View)this).getContext();
        final int[] l3 = bc1.l3;
        final tw1 v = tw1.v(context2, set, l3, n, 0);
        o32.o0((View)this, context, l3, set, v.r(), n, 0);
        this.mTitleTextAppearance = v.n(bc1.N3, 0);
        this.mSubtitleTextAppearance = v.n(bc1.E3, 0);
        this.mGravity = v.l(bc1.m3, this.mGravity);
        this.mButtonGravity = v.l(bc1.n3, 48);
        final int e = v.e(bc1.H3, 0);
        final int m3 = bc1.M3;
        n = e;
        if (v.s(m3)) {
            n = v.e(m3, e);
        }
        this.mTitleMarginBottom = n;
        this.mTitleMarginTop = n;
        this.mTitleMarginEnd = n;
        this.mTitleMarginStart = n;
        n = v.e(bc1.K3, -1);
        if (n >= 0) {
            this.mTitleMarginStart = n;
        }
        n = v.e(bc1.J3, -1);
        if (n >= 0) {
            this.mTitleMarginEnd = n;
        }
        n = v.e(bc1.L3, -1);
        if (n >= 0) {
            this.mTitleMarginTop = n;
        }
        n = v.e(bc1.I3, -1);
        if (n >= 0) {
            this.mTitleMarginBottom = n;
        }
        this.mMaxButtonHeight = v.f(bc1.y3, -1);
        final int e2 = v.e(bc1.u3, Integer.MIN_VALUE);
        final int e3 = v.e(bc1.q3, Integer.MIN_VALUE);
        final int f = v.f(bc1.s3, 0);
        n = v.f(bc1.t3, 0);
        this.c();
        this.mContentInsets.e(f, n);
        if (e2 != Integer.MIN_VALUE || e3 != Integer.MIN_VALUE) {
            this.mContentInsets.g(e2, e3);
        }
        this.mContentInsetStartWithNavigation = v.e(bc1.v3, Integer.MIN_VALUE);
        this.mContentInsetEndWithActions = v.e(bc1.r3, Integer.MIN_VALUE);
        this.mCollapseIcon = v.g(bc1.p3);
        this.mCollapseDescription = v.p(bc1.o3);
        final CharSequence p3 = v.p(bc1.G3);
        if (!TextUtils.isEmpty(p3)) {
            this.setTitle(p3);
        }
        final CharSequence p4 = v.p(bc1.D3);
        if (!TextUtils.isEmpty(p4)) {
            this.setSubtitle(p4);
        }
        this.mPopupContext = ((View)this).getContext();
        this.setPopupTheme(v.n(bc1.C3, 0));
        final Drawable g = v.g(bc1.B3);
        if (g != null) {
            this.setNavigationIcon(g);
        }
        final CharSequence p5 = v.p(bc1.A3);
        if (!TextUtils.isEmpty(p5)) {
            this.setNavigationContentDescription(p5);
        }
        final Drawable g2 = v.g(bc1.w3);
        if (g2 != null) {
            this.setLogo(g2);
        }
        final CharSequence p6 = v.p(bc1.x3);
        if (!TextUtils.isEmpty(p6)) {
            this.setLogoDescription(p6);
        }
        n = bc1.O3;
        if (v.s(n)) {
            this.setTitleTextColor(v.c(n));
        }
        n = bc1.F3;
        if (v.s(n)) {
            this.setSubtitleTextColor(v.c(n));
        }
        n = bc1.z3;
        if (v.s(n)) {
            this.inflateMenu(v.n(n, 0));
        }
        v.w();
    }
    
    private ArrayList<MenuItem> getCurrentMenuItems() {
        final ArrayList list = new ArrayList();
        final Menu menu = this.getMenu();
        for (int i = 0; i < menu.size(); ++i) {
            list.add(menu.getItem(i));
        }
        return list;
    }
    
    private MenuInflater getMenuInflater() {
        return new ls1(((View)this).getContext());
    }
    
    public final void a(final List list, int i) {
        final int b = o32.B((View)this);
        final int n = 0;
        final boolean b2 = b == 1;
        final int childCount = this.getChildCount();
        final int b3 = pb0.b(i, o32.B((View)this));
        list.clear();
        i = n;
        if (b2) {
            View child;
            g g;
            for (i = childCount - 1; i >= 0; --i) {
                child = this.getChildAt(i);
                g = (g)child.getLayoutParams();
                if (g.b == 0 && this.v(child) && this.h(g.a) == b3) {
                    list.add(child);
                }
            }
        }
        else {
            while (i < childCount) {
                final View child2 = this.getChildAt(i);
                final g g2 = (g)child2.getLayoutParams();
                if (g2.b == 0 && this.v(child2) && this.h(g2.a) == b3) {
                    list.add(child2);
                }
                ++i;
            }
        }
    }
    
    public void addChildrenForExpandedActionView() {
        for (int i = this.mHiddenViews.size() - 1; i >= 0; --i) {
            this.addView((View)this.mHiddenViews.get(i));
        }
        this.mHiddenViews.clear();
    }
    
    public void addMenuProvider(final rv0 rv0) {
        this.mMenuHostHelper.c(rv0);
    }
    
    public void addMenuProvider(final rv0 rv0, final qj0 qj0) {
        this.mMenuHostHelper.d(rv0, qj0);
    }
    
    public void addMenuProvider(final rv0 rv0, final qj0 qj0, final Lifecycle.State state) {
        this.mMenuHostHelper.e(rv0, qj0, state);
    }
    
    public final void b(final View e, final boolean b) {
        final ViewGroup$LayoutParams layoutParams = e.getLayoutParams();
        g layoutParams2;
        if (layoutParams == null) {
            layoutParams2 = this.generateDefaultLayoutParams();
        }
        else if (!this.checkLayoutParams(layoutParams)) {
            layoutParams2 = this.generateLayoutParams(layoutParams);
        }
        else {
            layoutParams2 = (g)layoutParams;
        }
        layoutParams2.b = 1;
        if (b && this.mExpandedActionView != null) {
            e.setLayoutParams((ViewGroup$LayoutParams)layoutParams2);
            this.mHiddenViews.add(e);
        }
        else {
            this.addView(e, (ViewGroup$LayoutParams)layoutParams2);
        }
    }
    
    public final void c() {
        if (this.mContentInsets == null) {
            this.mContentInsets = new yf1();
        }
    }
    
    public boolean canShowOverflowMenu() {
        if (((View)this).getVisibility() == 0) {
            final ActionMenuView mMenuView = this.mMenuView;
            if (mMenuView != null && mMenuView.p()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return super.checkLayoutParams(viewGroup$LayoutParams) && viewGroup$LayoutParams instanceof g;
    }
    
    public void collapseActionView() {
        final f mExpandedMenuPresenter = this.mExpandedMenuPresenter;
        androidx.appcompat.view.menu.g b;
        if (mExpandedMenuPresenter == null) {
            b = null;
        }
        else {
            b = mExpandedMenuPresenter.b;
        }
        if (b != null) {
            b.collapseActionView();
        }
    }
    
    public final void d() {
        if (this.mLogoView == null) {
            this.mLogoView = new z6(((View)this).getContext());
        }
    }
    
    public void dismissPopupMenus() {
        final ActionMenuView mMenuView = this.mMenuView;
        if (mMenuView != null) {
            mMenuView.g();
        }
    }
    
    public final void e() {
        this.f();
        if (this.mMenuView.s() == null) {
            final androidx.appcompat.view.menu.e e = (androidx.appcompat.view.menu.e)this.mMenuView.getMenu();
            if (this.mExpandedMenuPresenter == null) {
                this.mExpandedMenuPresenter = new f();
            }
            this.mMenuView.setExpandedActionViewsExclusive(true);
            e.addMenuPresenter(this.mExpandedMenuPresenter, this.mPopupContext);
            this.updateBackInvokedCallbackState();
        }
    }
    
    public void ensureCollapseButtonView() {
        if (this.mCollapseButtonView == null) {
            ((ImageView)(this.mCollapseButtonView = new x6(((View)this).getContext(), null, sa1.N))).setImageDrawable(this.mCollapseIcon);
            ((View)this.mCollapseButtonView).setContentDescription(this.mCollapseDescription);
            final g generateDefaultLayoutParams = this.generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = ((this.mButtonGravity & 0x70) | 0x800003);
            generateDefaultLayoutParams.b = 2;
            ((View)this.mCollapseButtonView).setLayoutParams((ViewGroup$LayoutParams)generateDefaultLayoutParams);
            ((View)this.mCollapseButtonView).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
                public final Toolbar a;
                
                public void onClick(final View view) {
                    this.a.collapseActionView();
                }
            });
        }
    }
    
    public final void f() {
        if (this.mMenuView == null) {
            (this.mMenuView = new ActionMenuView(((View)this).getContext())).setPopupTheme(this.mPopupTheme);
            this.mMenuView.setOnMenuItemClickListener(this.mMenuViewItemClickListener);
            this.mMenuView.t(this.mActionMenuPresenterCallback, new androidx.appcompat.view.menu.e.a(this) {
                public final Toolbar a;
                
                @Override
                public boolean onMenuItemSelected(final e e, final MenuItem menuItem) {
                    final a mMenuBuilderCallback = this.a.mMenuBuilderCallback;
                    return mMenuBuilderCallback != null && mMenuBuilderCallback.onMenuItemSelected(e, menuItem);
                }
                
                @Override
                public void onMenuModeChange(final e e) {
                    if (!this.a.mMenuView.o()) {
                        this.a.mMenuHostHelper.k((Menu)e);
                    }
                    final a mMenuBuilderCallback = this.a.mMenuBuilderCallback;
                    if (mMenuBuilderCallback != null) {
                        mMenuBuilderCallback.onMenuModeChange(e);
                    }
                }
            });
            final g generateDefaultLayoutParams = this.generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = ((this.mButtonGravity & 0x70) | 0x800005);
            ((View)this.mMenuView).setLayoutParams((ViewGroup$LayoutParams)generateDefaultLayoutParams);
            this.b((View)this.mMenuView, false);
        }
    }
    
    public final void g() {
        if (this.mNavButtonView == null) {
            this.mNavButtonView = new x6(((View)this).getContext(), null, sa1.N);
            final g generateDefaultLayoutParams = this.generateDefaultLayoutParams();
            generateDefaultLayoutParams.a = ((this.mButtonGravity & 0x70) | 0x800003);
            ((View)this.mNavButtonView).setLayoutParams((ViewGroup$LayoutParams)generateDefaultLayoutParams);
        }
    }
    
    public g generateDefaultLayoutParams() {
        return new g(-2, -2);
    }
    
    public g generateLayoutParams(final AttributeSet set) {
        return new g(((View)this).getContext(), set);
    }
    
    public g generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof g) {
            return new g((g)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof t1.a) {
            return new g((t1.a)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return new g((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return new g(viewGroup$LayoutParams);
    }
    
    public CharSequence getCollapseContentDescription() {
        final ImageButton mCollapseButtonView = this.mCollapseButtonView;
        CharSequence contentDescription;
        if (mCollapseButtonView != null) {
            contentDescription = ((View)mCollapseButtonView).getContentDescription();
        }
        else {
            contentDescription = null;
        }
        return contentDescription;
    }
    
    public Drawable getCollapseIcon() {
        final ImageButton mCollapseButtonView = this.mCollapseButtonView;
        Drawable drawable;
        if (mCollapseButtonView != null) {
            drawable = ((ImageView)mCollapseButtonView).getDrawable();
        }
        else {
            drawable = null;
        }
        return drawable;
    }
    
    public int getContentInsetEnd() {
        final yf1 mContentInsets = this.mContentInsets;
        int a;
        if (mContentInsets != null) {
            a = mContentInsets.a();
        }
        else {
            a = 0;
        }
        return a;
    }
    
    public int getContentInsetEndWithActions() {
        int n = this.mContentInsetEndWithActions;
        if (n == Integer.MIN_VALUE) {
            n = this.getContentInsetEnd();
        }
        return n;
    }
    
    public int getContentInsetLeft() {
        final yf1 mContentInsets = this.mContentInsets;
        int b;
        if (mContentInsets != null) {
            b = mContentInsets.b();
        }
        else {
            b = 0;
        }
        return b;
    }
    
    public int getContentInsetRight() {
        final yf1 mContentInsets = this.mContentInsets;
        int c;
        if (mContentInsets != null) {
            c = mContentInsets.c();
        }
        else {
            c = 0;
        }
        return c;
    }
    
    public int getContentInsetStart() {
        final yf1 mContentInsets = this.mContentInsets;
        int d;
        if (mContentInsets != null) {
            d = mContentInsets.d();
        }
        else {
            d = 0;
        }
        return d;
    }
    
    public int getContentInsetStartWithNavigation() {
        int n = this.mContentInsetStartWithNavigation;
        if (n == Integer.MIN_VALUE) {
            n = this.getContentInsetStart();
        }
        return n;
    }
    
    public int getCurrentContentInsetEnd() {
        final ActionMenuView mMenuView = this.mMenuView;
        boolean b = false;
        Label_0032: {
            if (mMenuView != null) {
                final androidx.appcompat.view.menu.e s = mMenuView.s();
                if (s != null && s.hasVisibleItems()) {
                    b = true;
                    break Label_0032;
                }
            }
            b = false;
        }
        int n;
        if (b) {
            n = Math.max(this.getContentInsetEnd(), Math.max(this.mContentInsetEndWithActions, 0));
        }
        else {
            n = this.getContentInsetEnd();
        }
        return n;
    }
    
    public int getCurrentContentInsetLeft() {
        int n;
        if (o32.B((View)this) == 1) {
            n = this.getCurrentContentInsetEnd();
        }
        else {
            n = this.getCurrentContentInsetStart();
        }
        return n;
    }
    
    public int getCurrentContentInsetRight() {
        int n;
        if (o32.B((View)this) == 1) {
            n = this.getCurrentContentInsetStart();
        }
        else {
            n = this.getCurrentContentInsetEnd();
        }
        return n;
    }
    
    public int getCurrentContentInsetStart() {
        int n;
        if (this.getNavigationIcon() != null) {
            n = Math.max(this.getContentInsetStart(), Math.max(this.mContentInsetStartWithNavigation, 0));
        }
        else {
            n = this.getContentInsetStart();
        }
        return n;
    }
    
    public Drawable getLogo() {
        final ImageView mLogoView = this.mLogoView;
        Drawable drawable;
        if (mLogoView != null) {
            drawable = mLogoView.getDrawable();
        }
        else {
            drawable = null;
        }
        return drawable;
    }
    
    public CharSequence getLogoDescription() {
        final ImageView mLogoView = this.mLogoView;
        CharSequence contentDescription;
        if (mLogoView != null) {
            contentDescription = ((View)mLogoView).getContentDescription();
        }
        else {
            contentDescription = null;
        }
        return contentDescription;
    }
    
    public Menu getMenu() {
        this.e();
        return this.mMenuView.getMenu();
    }
    
    public View getNavButtonView() {
        return (View)this.mNavButtonView;
    }
    
    public CharSequence getNavigationContentDescription() {
        final ImageButton mNavButtonView = this.mNavButtonView;
        CharSequence contentDescription;
        if (mNavButtonView != null) {
            contentDescription = ((View)mNavButtonView).getContentDescription();
        }
        else {
            contentDescription = null;
        }
        return contentDescription;
    }
    
    public Drawable getNavigationIcon() {
        final ImageButton mNavButtonView = this.mNavButtonView;
        Drawable drawable;
        if (mNavButtonView != null) {
            drawable = ((ImageView)mNavButtonView).getDrawable();
        }
        else {
            drawable = null;
        }
        return drawable;
    }
    
    public a getOuterActionMenuPresenter() {
        return this.mOuterActionMenuPresenter;
    }
    
    public Drawable getOverflowIcon() {
        this.e();
        return this.mMenuView.getOverflowIcon();
    }
    
    Context getPopupContext() {
        return this.mPopupContext;
    }
    
    public int getPopupTheme() {
        return this.mPopupTheme;
    }
    
    public CharSequence getSubtitle() {
        return this.mSubtitleText;
    }
    
    public final TextView getSubtitleTextView() {
        return this.mSubtitleTextView;
    }
    
    public CharSequence getTitle() {
        return this.mTitleText;
    }
    
    public int getTitleMarginBottom() {
        return this.mTitleMarginBottom;
    }
    
    public int getTitleMarginEnd() {
        return this.mTitleMarginEnd;
    }
    
    public int getTitleMarginStart() {
        return this.mTitleMarginStart;
    }
    
    public int getTitleMarginTop() {
        return this.mTitleMarginTop;
    }
    
    public final TextView getTitleTextView() {
        return this.mTitleTextView;
    }
    
    public dq getWrapper() {
        if (this.mWrapper == null) {
            this.mWrapper = new d(this, true);
        }
        return this.mWrapper;
    }
    
    public final int h(int n) {
        final int b = o32.B((View)this);
        final int n2 = pb0.b(n, b) & 0x7;
        if (n2 != 1) {
            n = 3;
            if (n2 != 3 && n2 != 5) {
                if (b == 1) {
                    n = 5;
                }
                return n;
            }
        }
        return n2;
    }
    
    public boolean hasExpandedActionView() {
        final f mExpandedMenuPresenter = this.mExpandedMenuPresenter;
        return mExpandedMenuPresenter != null && mExpandedMenuPresenter.b != null;
    }
    
    public boolean hideOverflowMenu() {
        final ActionMenuView mMenuView = this.mMenuView;
        return mMenuView != null && mMenuView.m();
    }
    
    public final int i(final View view, int n) {
        final g g = (g)view.getLayoutParams();
        final int measuredHeight = view.getMeasuredHeight();
        if (n > 0) {
            n = (measuredHeight - n) / 2;
        }
        else {
            n = 0;
        }
        final int j = this.j(g.a);
        if (j == 48) {
            return ((View)this).getPaddingTop() - n;
        }
        if (j != 80) {
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            final int height = ((View)this).getHeight();
            final int n2 = (height - paddingTop - paddingBottom - measuredHeight) / 2;
            n = g.topMargin;
            if (n2 >= n) {
                final int n3 = height - paddingBottom - measuredHeight - n2 - paddingTop;
                final int bottomMargin = g.bottomMargin;
                n = n2;
                if (n3 < bottomMargin) {
                    n = Math.max(0, n2 - (bottomMargin - n3));
                }
            }
            return paddingTop + n;
        }
        return ((View)this).getHeight() - ((View)this).getPaddingBottom() - measuredHeight - g.bottomMargin - n;
    }
    
    public void inflateMenu(final int n) {
        this.getMenuInflater().inflate(n, this.getMenu());
    }
    
    public void invalidateMenu() {
        final Iterator<MenuItem> iterator = this.mProvidedMenuItems.iterator();
        while (iterator.hasNext()) {
            this.getMenu().removeItem(iterator.next().getItemId());
        }
        this.s();
    }
    
    public boolean isBackInvokedCallbackEnabled() {
        return this.mBackInvokedCallbackEnabled;
    }
    
    public boolean isOverflowMenuShowPending() {
        final ActionMenuView mMenuView = this.mMenuView;
        return mMenuView != null && mMenuView.n();
    }
    
    public boolean isOverflowMenuShowing() {
        final ActionMenuView mMenuView = this.mMenuView;
        return mMenuView != null && mMenuView.o();
    }
    
    public boolean isTitleTruncated() {
        final TextView mTitleTextView = this.mTitleTextView;
        if (mTitleTextView == null) {
            return false;
        }
        final Layout layout = mTitleTextView.getLayout();
        if (layout == null) {
            return false;
        }
        for (int lineCount = layout.getLineCount(), i = 0; i < lineCount; ++i) {
            if (layout.getEllipsisCount(i) > 0) {
                return true;
            }
        }
        return false;
    }
    
    public final int j(int n) {
        final int n2 = n &= 0x70;
        if (n2 != 16 && (n = n2) != 48 && (n = n2) != 80) {
            n = (this.mGravity & 0x70);
        }
        return n;
    }
    
    public final int k(final View view) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        return fn0.b(viewGroup$MarginLayoutParams) + fn0.a(viewGroup$MarginLayoutParams);
    }
    
    public final int l(final View view) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        return viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin;
    }
    
    public final int m(final List list, final int[] array) {
        int max = array[0];
        int max2 = array[1];
        final int size = list.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            final View view = list.get(i);
            final g g = (g)view.getLayoutParams();
            final int b = g.leftMargin - max;
            final int b2 = g.rightMargin - max2;
            final int max3 = Math.max(0, b);
            final int max4 = Math.max(0, b2);
            max = Math.max(0, -b);
            max2 = Math.max(0, -b2);
            n += max3 + view.getMeasuredWidth() + max4;
            ++i;
        }
        return n;
    }
    
    public final boolean n(final View o) {
        return o.getParent() == this || this.mHiddenViews.contains(o);
    }
    
    public final int o(final View view, int n, final int[] array, int i) {
        final g g = (g)view.getLayoutParams();
        final int b = g.leftMargin - array[0];
        n += Math.max(0, b);
        array[0] = Math.max(0, -b);
        i = this.i(view, i);
        final int measuredWidth = view.getMeasuredWidth();
        view.layout(n, i, n + measuredWidth, view.getMeasuredHeight() + i);
        return n + (measuredWidth + g.rightMargin);
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.updateBackInvokedCallbackState();
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ((View)this).removeCallbacks(this.mShowOverflowMenuRunnable);
        this.updateBackInvokedCallbackState();
    }
    
    public boolean onHoverEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.mEatingHover = false;
        }
        if (!this.mEatingHover) {
            final boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.mEatingHover = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.mEatingHover = false;
        }
        return true;
    }
    
    public void onLayout(final boolean b, int n, int i, int a, int a2) {
        final boolean b2 = o32.B((View)this) == 1;
        final int width = ((View)this).getWidth();
        final int height = ((View)this).getHeight();
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int paddingRight = ((View)this).getPaddingRight();
        final int paddingTop = ((View)this).getPaddingTop();
        final int paddingBottom = ((View)this).getPaddingBottom();
        final int n2 = width - paddingRight;
        final int[] mTempMargins = this.mTempMargins;
        mTempMargins[mTempMargins[1] = 0] = 0;
        n = o32.C((View)this);
        int min;
        if (n >= 0) {
            min = Math.min(n, a2 - i);
        }
        else {
            min = 0;
        }
        Label_0169: {
            if (this.v((View)this.mNavButtonView)) {
                final ImageButton mNavButtonView = this.mNavButtonView;
                if (b2) {
                    a = this.p((View)mNavButtonView, n2, mTempMargins, min);
                    a2 = paddingLeft;
                    break Label_0169;
                }
                a2 = this.o((View)mNavButtonView, paddingLeft, mTempMargins, min);
            }
            else {
                a2 = paddingLeft;
            }
            a = n2;
        }
        n = a2;
        i = a;
        if (this.v((View)this.mCollapseButtonView)) {
            final ImageButton mCollapseButtonView = this.mCollapseButtonView;
            if (b2) {
                i = this.p((View)mCollapseButtonView, a, mTempMargins, min);
                n = a2;
            }
            else {
                n = this.o((View)mCollapseButtonView, a2, mTempMargins, min);
                i = a;
            }
        }
        a2 = n;
        a = i;
        if (this.v((View)this.mMenuView)) {
            final ActionMenuView mMenuView = this.mMenuView;
            if (b2) {
                a2 = this.o((View)mMenuView, n, mTempMargins, min);
                a = i;
            }
            else {
                a = this.p((View)mMenuView, i, mTempMargins, min);
                a2 = n;
            }
        }
        i = this.getCurrentContentInsetLeft();
        n = this.getCurrentContentInsetRight();
        mTempMargins[0] = Math.max(0, i - a2);
        mTempMargins[1] = Math.max(0, n - (n2 - a));
        i = Math.max(a2, i);
        a = Math.min(a, n2 - n);
        a2 = i;
        n = a;
        if (this.v(this.mExpandedActionView)) {
            final View mExpandedActionView = this.mExpandedActionView;
            if (b2) {
                n = this.p(mExpandedActionView, a, mTempMargins, min);
                a2 = i;
            }
            else {
                a2 = this.o(mExpandedActionView, i, mTempMargins, min);
                n = a;
            }
        }
        a = a2;
        i = n;
        if (this.v((View)this.mLogoView)) {
            final ImageView mLogoView = this.mLogoView;
            if (b2) {
                i = this.p((View)mLogoView, n, mTempMargins, min);
                a = a2;
            }
            else {
                a = this.o((View)mLogoView, a2, mTempMargins, min);
                i = n;
            }
        }
        final boolean v = this.v((View)this.mTitleTextView);
        final boolean v2 = this.v((View)this.mSubtitleTextView);
        if (v) {
            final g g = (g)((View)this.mTitleTextView).getLayoutParams();
            n = g.topMargin + ((View)this.mTitleTextView).getMeasuredHeight() + g.bottomMargin + 0;
        }
        else {
            n = 0;
        }
        if (v2) {
            final g g2 = (g)((View)this.mSubtitleTextView).getLayoutParams();
            n += g2.topMargin + ((View)this.mSubtitleTextView).getMeasuredHeight() + g2.bottomMargin;
        }
        Label_1319: {
            if (!v && !v2) {
                n = a;
            }
            else {
                TextView textView;
                if (v) {
                    textView = this.mTitleTextView;
                }
                else {
                    textView = this.mSubtitleTextView;
                }
                TextView textView2;
                if (v2) {
                    textView2 = this.mSubtitleTextView;
                }
                else {
                    textView2 = this.mTitleTextView;
                }
                final g g3 = (g)((View)textView).getLayoutParams();
                final g g4 = (g)((View)textView2).getLayoutParams();
                if ((v && ((View)this.mTitleTextView).getMeasuredWidth() > 0) || (v2 && ((View)this.mSubtitleTextView).getMeasuredWidth() > 0)) {
                    a2 = 1;
                }
                else {
                    a2 = 0;
                }
                final int n3 = this.mGravity & 0x70;
                if (n3 != 48) {
                    if (n3 != 80) {
                        final int n4 = (height - paddingTop - paddingBottom - n) / 2;
                        final int topMargin = g3.topMargin;
                        final int mTitleMarginTop = this.mTitleMarginTop;
                        if (n4 < topMargin + mTitleMarginTop) {
                            n = topMargin + mTitleMarginTop;
                        }
                        else {
                            final int n5 = height - paddingBottom - n - n4 - paddingTop;
                            final int bottomMargin = g3.bottomMargin;
                            final int mTitleMarginBottom = this.mTitleMarginBottom;
                            n = n4;
                            if (n5 < bottomMargin + mTitleMarginBottom) {
                                n = Math.max(0, n4 - (g4.bottomMargin + mTitleMarginBottom - n5));
                            }
                        }
                        n += paddingTop;
                    }
                    else {
                        n = height - paddingBottom - g4.bottomMargin - this.mTitleMarginBottom - n;
                    }
                }
                else {
                    n = ((View)this).getPaddingTop() + g3.topMargin + this.mTitleMarginTop;
                }
                if (b2) {
                    int mTitleMarginStart;
                    if (a2 != 0) {
                        mTitleMarginStart = this.mTitleMarginStart;
                    }
                    else {
                        mTitleMarginStart = 0;
                    }
                    final int b3 = mTitleMarginStart - mTempMargins[1];
                    i -= Math.max(0, b3);
                    mTempMargins[1] = Math.max(0, -b3);
                    int n8;
                    if (v) {
                        final g g5 = (g)((View)this.mTitleTextView).getLayoutParams();
                        final int n6 = i - ((View)this.mTitleTextView).getMeasuredWidth();
                        final int n7 = ((View)this.mTitleTextView).getMeasuredHeight() + n;
                        ((View)this.mTitleTextView).layout(n6, n, i, n7);
                        n = n6 - this.mTitleMarginEnd;
                        n8 = n7 + g5.bottomMargin;
                    }
                    else {
                        final int n9 = i;
                        n8 = n;
                        n = n9;
                    }
                    int b4;
                    if (v2) {
                        final int n10 = n8 + ((g)((View)this.mSubtitleTextView).getLayoutParams()).topMargin;
                        ((View)this.mSubtitleTextView).layout(i - ((View)this.mSubtitleTextView).getMeasuredWidth(), n10, i, ((View)this.mSubtitleTextView).getMeasuredHeight() + n10);
                        b4 = i - this.mTitleMarginEnd;
                    }
                    else {
                        b4 = i;
                    }
                    if (a2 != 0) {
                        i = Math.min(n, b4);
                    }
                    n = a;
                }
                else {
                    int mTitleMarginStart2;
                    if (a2 != 0) {
                        mTitleMarginStart2 = this.mTitleMarginStart;
                    }
                    else {
                        mTitleMarginStart2 = 0;
                    }
                    final int b5 = mTitleMarginStart2 - mTempMargins[0];
                    a += Math.max(0, b5);
                    mTempMargins[0] = Math.max(0, -b5);
                    int a3;
                    if (v) {
                        final g g6 = (g)((View)this.mTitleTextView).getLayoutParams();
                        final int n11 = ((View)this.mTitleTextView).getMeasuredWidth() + a;
                        final int n12 = ((View)this.mTitleTextView).getMeasuredHeight() + n;
                        ((View)this.mTitleTextView).layout(a, n, n11, n12);
                        a3 = n11 + this.mTitleMarginEnd;
                        n = n12 + g6.bottomMargin;
                    }
                    else {
                        a3 = a;
                    }
                    int b6;
                    if (v2) {
                        final int n13 = n + ((g)((View)this.mSubtitleTextView).getLayoutParams()).topMargin;
                        n = ((View)this.mSubtitleTextView).getMeasuredWidth() + a;
                        ((View)this.mSubtitleTextView).layout(a, n13, n, ((View)this.mSubtitleTextView).getMeasuredHeight() + n13);
                        b6 = n + this.mTitleMarginEnd;
                    }
                    else {
                        b6 = a;
                    }
                    n = a;
                    a = i;
                    if (a2 != 0) {
                        n = Math.max(a3, b6);
                        a = i;
                    }
                    break Label_1319;
                }
            }
            a = i;
        }
        a2 = 0;
        this.a(this.mTempViews, 3);
        int size;
        for (size = this.mTempViews.size(), i = 0; i < size; ++i) {
            n = this.o(this.mTempViews.get(i), n, mTempMargins, min);
        }
        this.a(this.mTempViews, 5);
        int size2;
        for (size2 = this.mTempViews.size(), i = 0; i < size2; ++i) {
            a = this.p(this.mTempViews.get(i), a, mTempMargins, min);
        }
        this.a(this.mTempViews, 1);
        final int m = this.m(this.mTempViews, mTempMargins);
        i = paddingLeft + (width - paddingLeft - paddingRight) / 2 - m / 2;
        final int n14 = m + i;
        if (i >= n) {
            if (n14 > a) {
                n = i - (n14 - a);
            }
            else {
                n = i;
            }
        }
        for (a = this.mTempViews.size(), i = a2; i < a; ++i) {
            n = this.o(this.mTempViews.get(i), n, mTempMargins, min);
        }
        this.mTempViews.clear();
    }
    
    public void onMeasure(int resolveSizeAndState, final int n) {
        final int[] mTempMargins = this.mTempMargins;
        final int b = u42.b((View)this) ? 1 : 0;
        final int n2 = 0;
        int n3;
        int max;
        int combineMeasuredStates;
        if (this.v((View)this.mNavButtonView)) {
            this.r((View)this.mNavButtonView, resolveSizeAndState, 0, n, 0, this.mMaxButtonHeight);
            n3 = ((View)this.mNavButtonView).getMeasuredWidth() + this.k((View)this.mNavButtonView);
            max = Math.max(0, ((View)this.mNavButtonView).getMeasuredHeight() + this.l((View)this.mNavButtonView));
            combineMeasuredStates = View.combineMeasuredStates(0, ((View)this.mNavButtonView).getMeasuredState());
        }
        else {
            n3 = 0;
            max = (combineMeasuredStates = 0);
        }
        int b2 = n3;
        int n4 = max;
        int n5 = combineMeasuredStates;
        if (this.v((View)this.mCollapseButtonView)) {
            this.r((View)this.mCollapseButtonView, resolveSizeAndState, 0, n, 0, this.mMaxButtonHeight);
            b2 = ((View)this.mCollapseButtonView).getMeasuredWidth() + this.k((View)this.mCollapseButtonView);
            n4 = Math.max(max, ((View)this.mCollapseButtonView).getMeasuredHeight() + this.l((View)this.mCollapseButtonView));
            n5 = View.combineMeasuredStates(combineMeasuredStates, ((View)this.mCollapseButtonView).getMeasuredState());
        }
        final int currentContentInsetStart = this.getCurrentContentInsetStart();
        final int n6 = 0 + Math.max(currentContentInsetStart, b2);
        mTempMargins[b] = Math.max(0, currentContentInsetStart - b2);
        int b3;
        if (this.v((View)this.mMenuView)) {
            this.r((View)this.mMenuView, resolveSizeAndState, n6, n, 0, this.mMaxButtonHeight);
            b3 = ((View)this.mMenuView).getMeasuredWidth() + this.k((View)this.mMenuView);
            n4 = Math.max(n4, ((View)this.mMenuView).getMeasuredHeight() + this.l((View)this.mMenuView));
            n5 = View.combineMeasuredStates(n5, ((View)this.mMenuView).getMeasuredState());
        }
        else {
            b3 = 0;
        }
        final int currentContentInsetEnd = this.getCurrentContentInsetEnd();
        final int n7 = n6 + Math.max(currentContentInsetEnd, b3);
        mTempMargins[b ^ 0x1] = Math.max(0, currentContentInsetEnd - b3);
        int max2 = n4;
        int combineMeasuredStates2 = n5;
        int n8 = n7;
        if (this.v(this.mExpandedActionView)) {
            n8 = n7 + this.q(this.mExpandedActionView, resolveSizeAndState, n7, n, 0, mTempMargins);
            max2 = Math.max(n4, this.mExpandedActionView.getMeasuredHeight() + this.l(this.mExpandedActionView));
            combineMeasuredStates2 = View.combineMeasuredStates(n5, this.mExpandedActionView.getMeasuredState());
        }
        int max3 = max2;
        int n9 = combineMeasuredStates2;
        int n10 = n8;
        if (this.v((View)this.mLogoView)) {
            n10 = n8 + this.q((View)this.mLogoView, resolveSizeAndState, n8, n, 0, mTempMargins);
            max3 = Math.max(max2, ((View)this.mLogoView).getMeasuredHeight() + this.l((View)this.mLogoView));
            n9 = View.combineMeasuredStates(combineMeasuredStates2, ((View)this.mLogoView).getMeasuredState());
        }
        final int childCount = this.getChildCount();
        final int n11 = 0;
        int n12 = n10;
        int max4;
        int combineMeasuredStates3;
        int n13;
        for (int i = n11; i < childCount; ++i, max3 = max4, n9 = combineMeasuredStates3, n12 = n13) {
            final View child = this.getChildAt(i);
            max4 = max3;
            combineMeasuredStates3 = n9;
            n13 = n12;
            if (((g)child.getLayoutParams()).b == 0) {
                if (!this.v(child)) {
                    max4 = max3;
                    combineMeasuredStates3 = n9;
                    n13 = n12;
                }
                else {
                    n13 = n12 + this.q(child, resolveSizeAndState, n12, n, 0, mTempMargins);
                    max4 = Math.max(max3, child.getMeasuredHeight() + this.l(child));
                    combineMeasuredStates3 = View.combineMeasuredStates(n9, child.getMeasuredState());
                }
            }
        }
        final int n14 = this.mTitleMarginTop + this.mTitleMarginBottom;
        final int n15 = this.mTitleMarginStart + this.mTitleMarginEnd;
        int b4;
        int max5;
        if (this.v((View)this.mTitleTextView)) {
            this.q((View)this.mTitleTextView, resolveSizeAndState, n12 + n15, n, n14, mTempMargins);
            final int measuredWidth = ((View)this.mTitleTextView).getMeasuredWidth();
            final int k = this.k((View)this.mTitleTextView);
            final int measuredHeight = ((View)this.mTitleTextView).getMeasuredHeight();
            final int l = this.l((View)this.mTitleTextView);
            n9 = View.combineMeasuredStates(n9, ((View)this.mTitleTextView).getMeasuredState());
            b4 = measuredHeight + l;
            max5 = measuredWidth + k;
        }
        else {
            b4 = 0;
            max5 = 0;
        }
        if (this.v((View)this.mSubtitleTextView)) {
            max5 = Math.max(max5, this.q((View)this.mSubtitleTextView, resolveSizeAndState, n12 + n15, n, b4 + n14, mTempMargins));
            b4 += ((View)this.mSubtitleTextView).getMeasuredHeight() + this.l((View)this.mSubtitleTextView);
            n9 = View.combineMeasuredStates(n9, ((View)this.mSubtitleTextView).getMeasuredState());
        }
        final int max6 = Math.max(max3, b4);
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int paddingRight = ((View)this).getPaddingRight();
        final int paddingTop = ((View)this).getPaddingTop();
        final int paddingBottom = ((View)this).getPaddingBottom();
        final int resolveSizeAndState2 = View.resolveSizeAndState(Math.max(n12 + max5 + (paddingLeft + paddingRight), ((View)this).getSuggestedMinimumWidth()), resolveSizeAndState, 0xFF000000 & n9);
        resolveSizeAndState = View.resolveSizeAndState(Math.max(max6 + (paddingTop + paddingBottom), ((View)this).getSuggestedMinimumHeight()), n, n9 << 16);
        if (this.u()) {
            resolveSizeAndState = n2;
        }
        ((View)this).setMeasuredDimension(resolveSizeAndState2, resolveSizeAndState);
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof i)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final i i = (i)parcelable;
        super.onRestoreInstanceState(i.getSuperState());
        final ActionMenuView mMenuView = this.mMenuView;
        Object s;
        if (mMenuView != null) {
            s = mMenuView.s();
        }
        else {
            s = null;
        }
        final int a = i.a;
        if (a != 0 && this.mExpandedMenuPresenter != null && s != null) {
            final MenuItem item = ((Menu)s).findItem(a);
            if (item != null) {
                item.expandActionView();
            }
        }
        if (i.b) {
            this.t();
        }
    }
    
    public void onRtlPropertiesChanged(final int n) {
        super.onRtlPropertiesChanged(n);
        this.c();
        final yf1 mContentInsets = this.mContentInsets;
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        mContentInsets.f(b);
    }
    
    public Parcelable onSaveInstanceState() {
        final i i = new i(super.onSaveInstanceState());
        final f mExpandedMenuPresenter = this.mExpandedMenuPresenter;
        if (mExpandedMenuPresenter != null) {
            final androidx.appcompat.view.menu.g b = mExpandedMenuPresenter.b;
            if (b != null) {
                i.a = b.getItemId();
            }
        }
        i.b = this.isOverflowMenuShowing();
        return (Parcelable)i;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.mEatingTouch = false;
        }
        if (!this.mEatingTouch) {
            final boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.mEatingTouch = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.mEatingTouch = false;
        }
        return true;
    }
    
    public final int p(final View view, int n, final int[] array, int measuredWidth) {
        final g g = (g)view.getLayoutParams();
        final int b = g.rightMargin - array[1];
        n -= Math.max(0, b);
        array[1] = Math.max(0, -b);
        final int i = this.i(view, measuredWidth);
        measuredWidth = view.getMeasuredWidth();
        view.layout(n - measuredWidth, i, n, view.getMeasuredHeight() + i);
        return n - (measuredWidth + g.leftMargin);
    }
    
    public final int q(final View view, final int n, final int n2, final int n3, final int n4, final int[] array) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        final int b = viewGroup$MarginLayoutParams.leftMargin - array[0];
        final int b2 = viewGroup$MarginLayoutParams.rightMargin - array[1];
        final int n5 = Math.max(0, b) + Math.max(0, b2);
        array[0] = Math.max(0, -b);
        array[1] = Math.max(0, -b2);
        view.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight() + n5 + n2, viewGroup$MarginLayoutParams.width), ViewGroup.getChildMeasureSpec(n3, ((View)this).getPaddingTop() + ((View)this).getPaddingBottom() + viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin + n4, viewGroup$MarginLayoutParams.height));
        return view.getMeasuredWidth() + n5;
    }
    
    public final void r(final View view, int n, int childMeasureSpec, int mode, final int n2, final int b) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        final int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight() + viewGroup$MarginLayoutParams.leftMargin + viewGroup$MarginLayoutParams.rightMargin + childMeasureSpec, viewGroup$MarginLayoutParams.width);
        childMeasureSpec = ViewGroup.getChildMeasureSpec(mode, ((View)this).getPaddingTop() + ((View)this).getPaddingBottom() + viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin + n2, viewGroup$MarginLayoutParams.height);
        mode = View$MeasureSpec.getMode(childMeasureSpec);
        n = childMeasureSpec;
        if (mode != 1073741824) {
            n = childMeasureSpec;
            if (b >= 0) {
                n = b;
                if (mode != 0) {
                    n = Math.min(View$MeasureSpec.getSize(childMeasureSpec), b);
                }
                n = View$MeasureSpec.makeMeasureSpec(n, 1073741824);
            }
        }
        view.measure(childMeasureSpec2, n);
    }
    
    public void removeChildrenForExpandedActionView() {
        for (int i = this.getChildCount() - 1; i >= 0; --i) {
            final View child = this.getChildAt(i);
            if (((g)child.getLayoutParams()).b != 2 && child != this.mMenuView) {
                this.removeViewAt(i);
                this.mHiddenViews.add(child);
            }
        }
    }
    
    public void removeMenuProvider(final rv0 rv0) {
        this.mMenuHostHelper.l(rv0);
    }
    
    public final void s() {
        final Menu menu = this.getMenu();
        final ArrayList<MenuItem> currentMenuItems = this.getCurrentMenuItems();
        this.mMenuHostHelper.h(menu, this.getMenuInflater());
        final ArrayList<MenuItem> currentMenuItems2 = this.getCurrentMenuItems();
        currentMenuItems2.removeAll(currentMenuItems);
        this.mProvidedMenuItems = currentMenuItems2;
    }
    
    public void setBackInvokedCallbackEnabled(final boolean mBackInvokedCallbackEnabled) {
        if (this.mBackInvokedCallbackEnabled != mBackInvokedCallbackEnabled) {
            this.mBackInvokedCallbackEnabled = mBackInvokedCallbackEnabled;
            this.updateBackInvokedCallbackState();
        }
    }
    
    public void setCollapseContentDescription(final int n) {
        CharSequence text;
        if (n != 0) {
            text = ((View)this).getContext().getText(n);
        }
        else {
            text = null;
        }
        this.setCollapseContentDescription(text);
    }
    
    public void setCollapseContentDescription(final CharSequence contentDescription) {
        if (!TextUtils.isEmpty(contentDescription)) {
            this.ensureCollapseButtonView();
        }
        final ImageButton mCollapseButtonView = this.mCollapseButtonView;
        if (mCollapseButtonView != null) {
            ((View)mCollapseButtonView).setContentDescription(contentDescription);
        }
    }
    
    public void setCollapseIcon(final int n) {
        this.setCollapseIcon(g7.b(((View)this).getContext(), n));
    }
    
    public void setCollapseIcon(final Drawable imageDrawable) {
        if (imageDrawable != null) {
            this.ensureCollapseButtonView();
            ((ImageView)this.mCollapseButtonView).setImageDrawable(imageDrawable);
        }
        else {
            final ImageButton mCollapseButtonView = this.mCollapseButtonView;
            if (mCollapseButtonView != null) {
                ((ImageView)mCollapseButtonView).setImageDrawable(this.mCollapseIcon);
            }
        }
    }
    
    public void setCollapsible(final boolean mCollapsible) {
        this.mCollapsible = mCollapsible;
        ((View)this).requestLayout();
    }
    
    public void setContentInsetEndWithActions(final int n) {
        int mContentInsetEndWithActions = n;
        if (n < 0) {
            mContentInsetEndWithActions = Integer.MIN_VALUE;
        }
        if (mContentInsetEndWithActions != this.mContentInsetEndWithActions) {
            this.mContentInsetEndWithActions = mContentInsetEndWithActions;
            if (this.getNavigationIcon() != null) {
                ((View)this).requestLayout();
            }
        }
    }
    
    public void setContentInsetStartWithNavigation(final int n) {
        int mContentInsetStartWithNavigation = n;
        if (n < 0) {
            mContentInsetStartWithNavigation = Integer.MIN_VALUE;
        }
        if (mContentInsetStartWithNavigation != this.mContentInsetStartWithNavigation) {
            this.mContentInsetStartWithNavigation = mContentInsetStartWithNavigation;
            if (this.getNavigationIcon() != null) {
                ((View)this).requestLayout();
            }
        }
    }
    
    public void setContentInsetsAbsolute(final int n, final int n2) {
        this.c();
        this.mContentInsets.e(n, n2);
    }
    
    public void setContentInsetsRelative(final int n, final int n2) {
        this.c();
        this.mContentInsets.g(n, n2);
    }
    
    public void setLogo(final int n) {
        this.setLogo(g7.b(((View)this).getContext(), n));
    }
    
    public void setLogo(final Drawable imageDrawable) {
        if (imageDrawable != null) {
            this.d();
            if (!this.n((View)this.mLogoView)) {
                this.b((View)this.mLogoView, true);
            }
        }
        else {
            final ImageView mLogoView = this.mLogoView;
            if (mLogoView != null && this.n((View)mLogoView)) {
                this.removeView((View)this.mLogoView);
                this.mHiddenViews.remove(this.mLogoView);
            }
        }
        final ImageView mLogoView2 = this.mLogoView;
        if (mLogoView2 != null) {
            mLogoView2.setImageDrawable(imageDrawable);
        }
    }
    
    public void setLogoDescription(final int n) {
        this.setLogoDescription(((View)this).getContext().getText(n));
    }
    
    public void setLogoDescription(final CharSequence contentDescription) {
        if (!TextUtils.isEmpty(contentDescription)) {
            this.d();
        }
        final ImageView mLogoView = this.mLogoView;
        if (mLogoView != null) {
            ((View)mLogoView).setContentDescription(contentDescription);
        }
    }
    
    public void setMenu(final androidx.appcompat.view.menu.e e, final a a) {
        if (e == null && this.mMenuView == null) {
            return;
        }
        this.f();
        final androidx.appcompat.view.menu.e s = this.mMenuView.s();
        if (s == e) {
            return;
        }
        if (s != null) {
            s.removeMenuPresenter(this.mOuterActionMenuPresenter);
            s.removeMenuPresenter(this.mExpandedMenuPresenter);
        }
        if (this.mExpandedMenuPresenter == null) {
            this.mExpandedMenuPresenter = new f();
        }
        a.y(true);
        if (e != null) {
            e.addMenuPresenter(a, this.mPopupContext);
            e.addMenuPresenter(this.mExpandedMenuPresenter, this.mPopupContext);
        }
        else {
            a.initForMenu(this.mPopupContext, null);
            this.mExpandedMenuPresenter.initForMenu(this.mPopupContext, null);
            a.updateMenuView(true);
            this.mExpandedMenuPresenter.updateMenuView(true);
        }
        this.mMenuView.setPopupTheme(this.mPopupTheme);
        this.mMenuView.setPresenter(a);
        this.mOuterActionMenuPresenter = a;
        this.updateBackInvokedCallbackState();
    }
    
    public void setMenuCallbacks(final androidx.appcompat.view.menu.i.a mActionMenuPresenterCallback, final androidx.appcompat.view.menu.e.a mMenuBuilderCallback) {
        this.mActionMenuPresenterCallback = mActionMenuPresenterCallback;
        this.mMenuBuilderCallback = mMenuBuilderCallback;
        final ActionMenuView mMenuView = this.mMenuView;
        if (mMenuView != null) {
            mMenuView.t(mActionMenuPresenterCallback, mMenuBuilderCallback);
        }
    }
    
    public void setNavigationContentDescription(final int n) {
        CharSequence text;
        if (n != 0) {
            text = ((View)this).getContext().getText(n);
        }
        else {
            text = null;
        }
        this.setNavigationContentDescription(text);
    }
    
    public void setNavigationContentDescription(final CharSequence contentDescription) {
        if (!TextUtils.isEmpty(contentDescription)) {
            this.g();
        }
        final ImageButton mNavButtonView = this.mNavButtonView;
        if (mNavButtonView != null) {
            ((View)mNavButtonView).setContentDescription(contentDescription);
            cx1.a((View)this.mNavButtonView, contentDescription);
        }
    }
    
    public void setNavigationIcon(final int n) {
        this.setNavigationIcon(g7.b(((View)this).getContext(), n));
    }
    
    public void setNavigationIcon(final Drawable imageDrawable) {
        if (imageDrawable != null) {
            this.g();
            if (!this.n((View)this.mNavButtonView)) {
                this.b((View)this.mNavButtonView, true);
            }
        }
        else {
            final ImageButton mNavButtonView = this.mNavButtonView;
            if (mNavButtonView != null && this.n((View)mNavButtonView)) {
                this.removeView((View)this.mNavButtonView);
                this.mHiddenViews.remove(this.mNavButtonView);
            }
        }
        final ImageButton mNavButtonView2 = this.mNavButtonView;
        if (mNavButtonView2 != null) {
            ((ImageView)mNavButtonView2).setImageDrawable(imageDrawable);
        }
    }
    
    public void setNavigationOnClickListener(final View$OnClickListener onClickListener) {
        this.g();
        ((View)this.mNavButtonView).setOnClickListener(onClickListener);
    }
    
    public void setOnMenuItemClickListener(final h mOnMenuItemClickListener) {
        this.mOnMenuItemClickListener = mOnMenuItemClickListener;
    }
    
    public void setOverflowIcon(final Drawable overflowIcon) {
        this.e();
        this.mMenuView.setOverflowIcon(overflowIcon);
    }
    
    public void setPopupTheme(final int mPopupTheme) {
        if (this.mPopupTheme != mPopupTheme) {
            if ((this.mPopupTheme = mPopupTheme) == 0) {
                this.mPopupContext = ((View)this).getContext();
            }
            else {
                this.mPopupContext = (Context)new ContextThemeWrapper(((View)this).getContext(), mPopupTheme);
            }
        }
    }
    
    public void setSubtitle(final int n) {
        this.setSubtitle(((View)this).getContext().getText(n));
    }
    
    public void setSubtitle(final CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.mSubtitleTextView == null) {
                final Context context = ((View)this).getContext();
                (this.mSubtitleTextView = new m7(context)).setSingleLine();
                this.mSubtitleTextView.setEllipsize(TextUtils$TruncateAt.END);
                final int mSubtitleTextAppearance = this.mSubtitleTextAppearance;
                if (mSubtitleTextAppearance != 0) {
                    this.mSubtitleTextView.setTextAppearance(context, mSubtitleTextAppearance);
                }
                final ColorStateList mSubtitleTextColor = this.mSubtitleTextColor;
                if (mSubtitleTextColor != null) {
                    this.mSubtitleTextView.setTextColor(mSubtitleTextColor);
                }
            }
            if (!this.n((View)this.mSubtitleTextView)) {
                this.b((View)this.mSubtitleTextView, true);
            }
        }
        else {
            final TextView mSubtitleTextView = this.mSubtitleTextView;
            if (mSubtitleTextView != null && this.n((View)mSubtitleTextView)) {
                this.removeView((View)this.mSubtitleTextView);
                this.mHiddenViews.remove(this.mSubtitleTextView);
            }
        }
        final TextView mSubtitleTextView2 = this.mSubtitleTextView;
        if (mSubtitleTextView2 != null) {
            mSubtitleTextView2.setText(charSequence);
        }
        this.mSubtitleText = charSequence;
    }
    
    public void setSubtitleTextAppearance(final Context context, final int mSubtitleTextAppearance) {
        this.mSubtitleTextAppearance = mSubtitleTextAppearance;
        final TextView mSubtitleTextView = this.mSubtitleTextView;
        if (mSubtitleTextView != null) {
            mSubtitleTextView.setTextAppearance(context, mSubtitleTextAppearance);
        }
    }
    
    public void setSubtitleTextColor(final int n) {
        this.setSubtitleTextColor(ColorStateList.valueOf(n));
    }
    
    public void setSubtitleTextColor(final ColorStateList list) {
        this.mSubtitleTextColor = list;
        final TextView mSubtitleTextView = this.mSubtitleTextView;
        if (mSubtitleTextView != null) {
            mSubtitleTextView.setTextColor(list);
        }
    }
    
    public void setTitle(final int n) {
        this.setTitle(((View)this).getContext().getText(n));
    }
    
    public void setTitle(final CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.mTitleTextView == null) {
                final Context context = ((View)this).getContext();
                (this.mTitleTextView = new m7(context)).setSingleLine();
                this.mTitleTextView.setEllipsize(TextUtils$TruncateAt.END);
                final int mTitleTextAppearance = this.mTitleTextAppearance;
                if (mTitleTextAppearance != 0) {
                    this.mTitleTextView.setTextAppearance(context, mTitleTextAppearance);
                }
                final ColorStateList mTitleTextColor = this.mTitleTextColor;
                if (mTitleTextColor != null) {
                    this.mTitleTextView.setTextColor(mTitleTextColor);
                }
            }
            if (!this.n((View)this.mTitleTextView)) {
                this.b((View)this.mTitleTextView, true);
            }
        }
        else {
            final TextView mTitleTextView = this.mTitleTextView;
            if (mTitleTextView != null && this.n((View)mTitleTextView)) {
                this.removeView((View)this.mTitleTextView);
                this.mHiddenViews.remove(this.mTitleTextView);
            }
        }
        final TextView mTitleTextView2 = this.mTitleTextView;
        if (mTitleTextView2 != null) {
            mTitleTextView2.setText(charSequence);
        }
        this.mTitleText = charSequence;
    }
    
    public void setTitleMargin(final int mTitleMarginStart, final int mTitleMarginTop, final int mTitleMarginEnd, final int mTitleMarginBottom) {
        this.mTitleMarginStart = mTitleMarginStart;
        this.mTitleMarginTop = mTitleMarginTop;
        this.mTitleMarginEnd = mTitleMarginEnd;
        this.mTitleMarginBottom = mTitleMarginBottom;
        ((View)this).requestLayout();
    }
    
    public void setTitleMarginBottom(final int mTitleMarginBottom) {
        this.mTitleMarginBottom = mTitleMarginBottom;
        ((View)this).requestLayout();
    }
    
    public void setTitleMarginEnd(final int mTitleMarginEnd) {
        this.mTitleMarginEnd = mTitleMarginEnd;
        ((View)this).requestLayout();
    }
    
    public void setTitleMarginStart(final int mTitleMarginStart) {
        this.mTitleMarginStart = mTitleMarginStart;
        ((View)this).requestLayout();
    }
    
    public void setTitleMarginTop(final int mTitleMarginTop) {
        this.mTitleMarginTop = mTitleMarginTop;
        ((View)this).requestLayout();
    }
    
    public void setTitleTextAppearance(final Context context, final int mTitleTextAppearance) {
        this.mTitleTextAppearance = mTitleTextAppearance;
        final TextView mTitleTextView = this.mTitleTextView;
        if (mTitleTextView != null) {
            mTitleTextView.setTextAppearance(context, mTitleTextAppearance);
        }
    }
    
    public void setTitleTextColor(final int n) {
        this.setTitleTextColor(ColorStateList.valueOf(n));
    }
    
    public void setTitleTextColor(final ColorStateList list) {
        this.mTitleTextColor = list;
        final TextView mTitleTextView = this.mTitleTextView;
        if (mTitleTextView != null) {
            mTitleTextView.setTextColor(list);
        }
    }
    
    public boolean showOverflowMenu() {
        final ActionMenuView mMenuView = this.mMenuView;
        return mMenuView != null && mMenuView.u();
    }
    
    public final void t() {
        ((View)this).removeCallbacks(this.mShowOverflowMenuRunnable);
        ((View)this).post(this.mShowOverflowMenuRunnable);
    }
    
    public final boolean u() {
        if (!this.mCollapsible) {
            return false;
        }
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (this.v(child) && child.getMeasuredWidth() > 0 && child.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }
    
    void updateBackInvokedCallbackState() {
        if (Build$VERSION.SDK_INT >= 33) {
            OnBackInvokedDispatcher a = e.a((View)this);
            final boolean b = this.hasExpandedActionView() && a != null && o32.T((View)this) && this.mBackInvokedCallbackEnabled;
            if (b && this.mBackInvokedDispatcher == null) {
                if (this.mBackInvokedCallback == null) {
                    this.mBackInvokedCallback = e.b(new zw1(this));
                }
                e.c(a, this.mBackInvokedCallback);
            }
            else {
                if (b) {
                    return;
                }
                final OnBackInvokedDispatcher mBackInvokedDispatcher = this.mBackInvokedDispatcher;
                if (mBackInvokedDispatcher == null) {
                    return;
                }
                e.d(mBackInvokedDispatcher, this.mBackInvokedCallback);
                a = null;
            }
            this.mBackInvokedDispatcher = a;
        }
    }
    
    public final boolean v(final View view) {
        return view != null && view.getParent() == this && view.getVisibility() != 8;
    }
    
    public abstract static class e
    {
        public static OnBackInvokedDispatcher a(final View view) {
            return view.findOnBackInvokedDispatcher();
        }
        
        public static OnBackInvokedCallback b(final Runnable obj) {
            Objects.requireNonNull(obj);
            return new ax1(obj);
        }
        
        public static void c(final Object o, final Object o2) {
            ((OnBackInvokedDispatcher)o).registerOnBackInvokedCallback(1000000, (OnBackInvokedCallback)o2);
        }
        
        public static void d(final Object o, final Object o2) {
            ((OnBackInvokedDispatcher)o).unregisterOnBackInvokedCallback((OnBackInvokedCallback)o2);
        }
    }
    
    public class f implements i
    {
        public androidx.appcompat.view.menu.e a;
        public androidx.appcompat.view.menu.g b;
        public final Toolbar c;
        
        public f(final Toolbar c) {
            this.c = c;
        }
        
        @Override
        public boolean collapseItemActionView(final androidx.appcompat.view.menu.e e, final androidx.appcompat.view.menu.g g) {
            final View mExpandedActionView = this.c.mExpandedActionView;
            if (mExpandedActionView instanceof gh) {
                ((gh)mExpandedActionView).a();
            }
            final Toolbar c = this.c;
            c.removeView(c.mExpandedActionView);
            final Toolbar c2 = this.c;
            c2.removeView((View)c2.mCollapseButtonView);
            final Toolbar c3 = this.c;
            c3.mExpandedActionView = null;
            c3.addChildrenForExpandedActionView();
            this.b = null;
            ((View)this.c).requestLayout();
            g.r(false);
            this.c.updateBackInvokedCallbackState();
            return true;
        }
        
        @Override
        public boolean expandItemActionView(final androidx.appcompat.view.menu.e e, final androidx.appcompat.view.menu.g b) {
            this.c.ensureCollapseButtonView();
            final ViewParent parent = ((View)this.c.mCollapseButtonView).getParent();
            final Toolbar c = this.c;
            if (parent != c) {
                if (parent instanceof ViewGroup) {
                    ((ViewGroup)parent).removeView((View)c.mCollapseButtonView);
                }
                final Toolbar c2 = this.c;
                c2.addView((View)c2.mCollapseButtonView);
            }
            this.c.mExpandedActionView = b.getActionView();
            this.b = b;
            final ViewParent parent2 = this.c.mExpandedActionView.getParent();
            final Toolbar c3 = this.c;
            if (parent2 != c3) {
                if (parent2 instanceof ViewGroup) {
                    ((ViewGroup)parent2).removeView(c3.mExpandedActionView);
                }
                final g generateDefaultLayoutParams = this.c.generateDefaultLayoutParams();
                final Toolbar c4 = this.c;
                generateDefaultLayoutParams.a = ((c4.mButtonGravity & 0x70) | 0x800003);
                generateDefaultLayoutParams.b = 2;
                c4.mExpandedActionView.setLayoutParams((ViewGroup$LayoutParams)generateDefaultLayoutParams);
                final Toolbar c5 = this.c;
                c5.addView(c5.mExpandedActionView);
            }
            this.c.removeChildrenForExpandedActionView();
            ((View)this.c).requestLayout();
            b.r(true);
            final View mExpandedActionView = this.c.mExpandedActionView;
            if (mExpandedActionView instanceof gh) {
                ((gh)mExpandedActionView).b();
            }
            this.c.updateBackInvokedCallbackState();
            return true;
        }
        
        @Override
        public boolean flagActionItems() {
            return false;
        }
        
        @Override
        public int getId() {
            return 0;
        }
        
        @Override
        public void initForMenu(final Context context, final androidx.appcompat.view.menu.e a) {
            final androidx.appcompat.view.menu.e a2 = this.a;
            if (a2 != null) {
                final androidx.appcompat.view.menu.g b = this.b;
                if (b != null) {
                    a2.collapseItemActionView(b);
                }
            }
            this.a = a;
        }
        
        @Override
        public void onCloseMenu(final androidx.appcompat.view.menu.e e, final boolean b) {
        }
        
        @Override
        public void onRestoreInstanceState(final Parcelable parcelable) {
        }
        
        @Override
        public Parcelable onSaveInstanceState() {
            return null;
        }
        
        @Override
        public boolean onSubMenuSelected(final l l) {
            return false;
        }
        
        @Override
        public void updateMenuView(final boolean b) {
            if (this.b != null) {
                final androidx.appcompat.view.menu.e a = this.a;
                int n = 0;
                if (a != null) {
                    final int size = a.size();
                    int n2 = 0;
                    while (true) {
                        n = n;
                        if (n2 >= size) {
                            break;
                        }
                        if (this.a.getItem(n2) == this.b) {
                            n = 1;
                            break;
                        }
                        ++n2;
                    }
                }
                if (n == 0) {
                    this.collapseItemActionView(this.a, this.b);
                }
            }
        }
    }
    
    public static class g extends a
    {
        public int b;
        
        public g(final int n, final int n2) {
            super(n, n2);
            this.b = 0;
            super.a = 8388627;
        }
        
        public g(final Context context, final AttributeSet set) {
            super(context, set);
            this.b = 0;
        }
        
        public g(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.b = 0;
        }
        
        public g(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super((ViewGroup$LayoutParams)viewGroup$MarginLayoutParams);
            this.b = 0;
            this.a(viewGroup$MarginLayoutParams);
        }
        
        public g(final g g) {
            super((t1.a)g);
            this.b = 0;
            this.b = g.b;
        }
        
        public g(final a a) {
            super(a);
            this.b = 0;
        }
        
        public void a(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super.leftMargin = viewGroup$MarginLayoutParams.leftMargin;
            super.topMargin = viewGroup$MarginLayoutParams.topMargin;
            super.rightMargin = viewGroup$MarginLayoutParams.rightMargin;
            super.bottomMargin = viewGroup$MarginLayoutParams.bottomMargin;
        }
    }
    
    public interface h
    {
        boolean onMenuItemClick(final MenuItem p0);
    }
    
    public static class i extends e
    {
        public static final Parcelable$Creator<i> CREATOR;
        public int a;
        public boolean b;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator() {
                public i a(final Parcel parcel) {
                    return new i(parcel, null);
                }
                
                public i b(final Parcel parcel, final ClassLoader classLoader) {
                    return new i(parcel, classLoader);
                }
                
                public i[] c(final int n) {
                    return new i[n];
                }
            };
        }
        
        public i(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.a = parcel.readInt();
            this.b = (parcel.readInt() != 0);
        }
        
        public i(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.a);
            parcel.writeInt((int)(this.b ? 1 : 0));
        }
    }
}

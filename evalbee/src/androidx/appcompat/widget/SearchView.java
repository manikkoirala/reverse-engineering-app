// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.EditText;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import java.lang.reflect.Method;
import android.view.KeyEvent$DispatcherState;
import android.util.TypedValue;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.content.res.Configuration;
import android.view.View$MeasureSpec;
import android.view.TouchDelegate;
import android.text.style.ImageSpan;
import android.text.SpannableStringBuilder;
import android.widget.AutoCompleteTextView;
import android.content.ComponentName;
import android.os.Parcelable;
import android.app.PendingIntent;
import android.net.Uri;
import android.content.res.Resources;
import android.widget.ListAdapter;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.text.TextUtils;
import android.view.View$OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.text.Editable;
import android.widget.AdapterView;
import android.widget.TextView;
import android.view.KeyEvent;
import android.database.Cursor;
import android.util.AttributeSet;
import android.content.Context;
import android.os.Build$VERSION;
import android.view.View$OnFocusChangeListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextWatcher;
import android.graphics.Rect;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.TextView$OnEditorActionListener;
import android.view.View$OnKeyListener;
import android.view.View;
import android.os.Bundle;
import android.app.SearchableInfo;
import java.util.WeakHashMap;
import android.view.View$OnClickListener;

public class SearchView extends b implements gh
{
    public static final o p1;
    public View$OnClickListener A;
    public final WeakHashMap A0;
    public boolean C;
    public final View$OnClickListener C0;
    public boolean D;
    public jo F;
    public boolean G;
    public CharSequence H;
    public boolean I;
    public boolean J;
    public int K;
    public boolean M;
    public CharSequence O;
    public CharSequence P;
    public boolean Q;
    public int U;
    public SearchableInfo V;
    public Bundle W;
    public final SearchAutoComplete a;
    public final View b;
    public View$OnKeyListener b1;
    public final View c;
    public final Runnable c0;
    public final TextView$OnEditorActionListener c1;
    public final View d;
    public final AdapterView$OnItemClickListener d1;
    public final ImageView e;
    public final ImageView f;
    public final ImageView g;
    public final AdapterView$OnItemSelectedListener g1;
    public final ImageView h;
    public final View i;
    public q j;
    public Rect k;
    public Runnable k0;
    public TextWatcher k1;
    public Rect l;
    public int[] m;
    public int[] n;
    public final ImageView p;
    public final Drawable q;
    public final int t;
    public final int v;
    public final Intent w;
    public final Intent x;
    public final CharSequence y;
    public View$OnFocusChangeListener z;
    
    static {
        o p2;
        if (Build$VERSION.SDK_INT < 29) {
            p2 = new o();
        }
        else {
            p2 = null;
        }
        p1 = p2;
    }
    
    public SearchView(final Context context) {
        this(context, null);
    }
    
    public SearchView(final Context context, final AttributeSet set) {
        this(context, set, sa1.I);
    }
    
    public SearchView(final Context context, final AttributeSet set, int inputType) {
        super(context, set, inputType);
        this.k = new Rect();
        this.l = new Rect();
        this.m = new int[2];
        this.n = new int[2];
        this.c0 = new Runnable(this) {
            public final SearchView a;
            
            @Override
            public void run() {
                this.a.K();
            }
        };
        this.k0 = new Runnable(this) {
            public final SearchView a;
            
            @Override
            public void run() {
                final jo f = this.a.F;
                if (f instanceof ds1) {
                    f.a(null);
                }
            }
        };
        this.A0 = new WeakHashMap();
        final View$OnClickListener view$OnClickListener = (View$OnClickListener)new View$OnClickListener(this) {
            public final SearchView a;
            
            public void onClick(final View view) {
                final SearchView a = this.a;
                if (view == a.e) {
                    a.A();
                }
                else if (view == a.g) {
                    a.w();
                }
                else if (view == a.f) {
                    a.B();
                }
                else if (view == a.h) {
                    a.F();
                }
                else if (view == a.a) {
                    a.m();
                }
            }
        };
        this.C0 = (View$OnClickListener)view$OnClickListener;
        this.b1 = (View$OnKeyListener)new View$OnKeyListener(this) {
            public final SearchView a;
            
            public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
                final SearchView a = this.a;
                if (a.V == null) {
                    return false;
                }
                if (a.a.isPopupShowing() && this.a.a.getListSelection() != -1) {
                    return this.a.C(view, n, keyEvent);
                }
                if (!this.a.a.b() && keyEvent.hasNoModifiers() && keyEvent.getAction() == 1 && n == 66) {
                    view.cancelLongPress();
                    final SearchView a2 = this.a;
                    a2.u(0, null, ((EditText)a2.a).getText().toString());
                    return true;
                }
                return false;
            }
        };
        final TextView$OnEditorActionListener textView$OnEditorActionListener = (TextView$OnEditorActionListener)new TextView$OnEditorActionListener(this) {
            public final SearchView a;
            
            public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
                this.a.B();
                return true;
            }
        };
        this.c1 = (TextView$OnEditorActionListener)textView$OnEditorActionListener;
        final AdapterView$OnItemClickListener adapterView$OnItemClickListener = (AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final SearchView a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                this.a.x(n, 0, null);
            }
        };
        this.d1 = (AdapterView$OnItemClickListener)adapterView$OnItemClickListener;
        final AdapterView$OnItemSelectedListener adapterView$OnItemSelectedListener = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final SearchView a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                this.a.y(n);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        };
        this.g1 = (AdapterView$OnItemSelectedListener)adapterView$OnItemSelectedListener;
        this.k1 = (TextWatcher)new TextWatcher(this) {
            public final SearchView a;
            
            public void afterTextChanged(final Editable editable) {
            }
            
            public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
            
            public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                this.a.D(charSequence);
            }
        };
        final int[] n2 = bc1.n2;
        final tw1 v = tw1.v(context, set, n2, inputType, 0);
        o32.o0((View)this, context, n2, set, v.r(), inputType, 0);
        LayoutInflater.from(context).inflate(v.n(bc1.x2, ob1.r), (ViewGroup)this, true);
        final SearchAutoComplete a = (SearchAutoComplete)((View)this).findViewById(db1.D);
        (this.a = a).setSearchView(this);
        this.b = ((View)this).findViewById(db1.z);
        final View viewById = ((View)this).findViewById(db1.C);
        this.c = viewById;
        final View viewById2 = ((View)this).findViewById(db1.J);
        this.d = viewById2;
        final ImageView e = (ImageView)((View)this).findViewById(db1.x);
        this.e = e;
        final ImageView f = (ImageView)((View)this).findViewById(db1.A);
        this.f = f;
        final ImageView g = (ImageView)((View)this).findViewById(db1.y);
        this.g = g;
        final ImageView h = (ImageView)((View)this).findViewById(db1.E);
        this.h = h;
        final ImageView p3 = (ImageView)((View)this).findViewById(db1.B);
        this.p = p3;
        o32.u0(viewById, v.g(bc1.y2));
        o32.u0(viewById2, v.g(bc1.C2));
        inputType = bc1.B2;
        e.setImageDrawable(v.g(inputType));
        f.setImageDrawable(v.g(bc1.v2));
        g.setImageDrawable(v.g(bc1.s2));
        h.setImageDrawable(v.g(bc1.E2));
        p3.setImageDrawable(v.g(inputType));
        this.q = v.g(bc1.A2);
        cx1.a((View)e, ((View)this).getResources().getString(qb1.n));
        this.t = v.n(bc1.D2, ob1.q);
        this.v = v.n(bc1.t2, 0);
        ((View)e).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)g).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)f).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)h).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)a).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((TextView)a).addTextChangedListener(this.k1);
        ((TextView)a).setOnEditorActionListener((TextView$OnEditorActionListener)textView$OnEditorActionListener);
        a.setOnItemClickListener((AdapterView$OnItemClickListener)adapterView$OnItemClickListener);
        a.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)adapterView$OnItemSelectedListener);
        ((View)a).setOnKeyListener(this.b1);
        ((View)a).setOnFocusChangeListener((View$OnFocusChangeListener)new View$OnFocusChangeListener(this) {
            public final SearchView a;
            
            public void onFocusChange(final View view, final boolean b) {
                final SearchView a = this.a;
                final View$OnFocusChangeListener z = a.z;
                if (z != null) {
                    z.onFocusChange((View)a, b);
                }
            }
        });
        this.setIconifiedByDefault(v.a(bc1.w2, true));
        inputType = v.f(bc1.p2, -1);
        if (inputType != -1) {
            this.setMaxWidth(inputType);
        }
        this.y = v.p(bc1.u2);
        this.H = v.p(bc1.z2);
        inputType = v.k(bc1.r2, -1);
        if (inputType != -1) {
            this.setImeOptions(inputType);
        }
        inputType = v.k(bc1.q2, -1);
        if (inputType != -1) {
            this.setInputType(inputType);
        }
        ((View)this).setFocusable(v.a(bc1.o2, true));
        v.w();
        final Intent w = new Intent("android.speech.action.WEB_SEARCH");
        (this.w = w).addFlags(268435456);
        w.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        (this.x = new Intent("android.speech.action.RECOGNIZE_SPEECH")).addFlags(268435456);
        final View viewById3 = ((View)this).findViewById(a.getDropDownAnchor());
        if ((this.i = viewById3) != null) {
            viewById3.addOnLayoutChangeListener((View$OnLayoutChangeListener)new View$OnLayoutChangeListener(this) {
                public final SearchView a;
                
                public void onLayoutChange(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
                    this.a.g();
                }
            });
        }
        this.P(this.C);
        this.L();
    }
    
    private int getPreferredHeight() {
        return ((View)this).getContext().getResources().getDimensionPixelSize(ab1.e);
    }
    
    private int getPreferredWidth() {
        return ((View)this).getContext().getResources().getDimensionPixelSize(ab1.f);
    }
    
    public static boolean r(final Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }
    
    private void setQuery(final CharSequence text) {
        ((TextView)this.a).setText(text);
        final SearchAutoComplete a = this.a;
        int length;
        if (TextUtils.isEmpty(text)) {
            length = 0;
        }
        else {
            length = text.length();
        }
        ((EditText)a).setSelection(length);
    }
    
    public void A() {
        this.P(false);
        ((View)this.a).requestFocus();
        this.a.setImeVisibility(true);
        final View$OnClickListener a = this.A;
        if (a != null) {
            a.onClick((View)this);
        }
    }
    
    public void B() {
        final Editable text = ((EditText)this.a).getText();
        if (text != null && TextUtils.getTrimmedLength((CharSequence)text) > 0) {
            if (this.V != null) {
                this.u(0, null, ((CharSequence)text).toString());
            }
            this.a.setImeVisibility(false);
            this.l();
        }
    }
    
    public boolean C(final View view, int length, final KeyEvent keyEvent) {
        if (this.V == null) {
            return false;
        }
        if (this.F == null) {
            return false;
        }
        if (keyEvent.getAction() == 0 && keyEvent.hasNoModifiers()) {
            if (length == 66 || length == 84 || length == 61) {
                return this.x(this.a.getListSelection(), 0, null);
            }
            if (length == 21 || length == 22) {
                if (length == 21) {
                    length = 0;
                }
                else {
                    length = ((TextView)this.a).length();
                }
                ((EditText)this.a).setSelection(length);
                this.a.setListSelection(0);
                this.a.clearListSelection();
                this.a.a();
                return true;
            }
            if (length == 19) {
                this.a.getListSelection();
                return false;
            }
        }
        return false;
    }
    
    public void D(final CharSequence charSequence) {
        final Editable text = ((EditText)this.a).getText();
        this.P = (CharSequence)text;
        final boolean b = TextUtils.isEmpty((CharSequence)text) ^ true;
        this.O(b);
        this.Q(b ^ true);
        this.J();
        this.N();
        this.O = charSequence.toString();
    }
    
    public void E() {
        this.P(this.q());
        this.G();
        if (((View)this.a).hasFocus()) {
            this.m();
        }
    }
    
    public void F() {
        final SearchableInfo v = this.V;
        if (v == null) {
            return;
        }
        try {
            Intent intent;
            if (v.getVoiceSearchLaunchWebSearch()) {
                intent = this.k(this.w, v);
            }
            else {
                if (!v.getVoiceSearchLaunchRecognizer()) {
                    return;
                }
                intent = this.j(this.x, v);
            }
            ((View)this).getContext().startActivity(intent);
        }
        catch (final ActivityNotFoundException ex) {
            Log.w("SearchView", "Could not find voice search activity");
        }
    }
    
    public final void G() {
        ((View)this).post(this.c0);
    }
    
    public final void H(final int n) {
        final Editable text = ((EditText)this.a).getText();
        final Cursor b = this.F.b();
        if (b == null) {
            return;
        }
        if (b.moveToPosition(n)) {
            final CharSequence c = this.F.c(b);
            if (c != null) {
                this.setQuery(c);
                return;
            }
        }
        this.setQuery((CharSequence)text);
    }
    
    public void I(final CharSequence charSequence, final boolean b) {
        ((TextView)this.a).setText(charSequence);
        if (charSequence != null) {
            final SearchAutoComplete a = this.a;
            ((EditText)a).setSelection(((TextView)a).length());
            this.P = charSequence;
        }
        if (b && !TextUtils.isEmpty(charSequence)) {
            this.B();
        }
    }
    
    public final void J() {
        final boolean empty = TextUtils.isEmpty((CharSequence)((EditText)this.a).getText());
        final boolean b = true;
        final boolean b2 = empty ^ true;
        final int n = 0;
        int n2 = b ? 1 : 0;
        if (!b2) {
            if (this.C && !this.Q) {
                n2 = (b ? 1 : 0);
            }
            else {
                n2 = 0;
            }
        }
        final ImageView g = this.g;
        int visibility;
        if (n2 != 0) {
            visibility = n;
        }
        else {
            visibility = 8;
        }
        g.setVisibility(visibility);
        final Drawable drawable = this.g.getDrawable();
        if (drawable != null) {
            int[] state;
            if (b2) {
                state = ViewGroup.ENABLED_STATE_SET;
            }
            else {
                state = ViewGroup.EMPTY_STATE_SET;
            }
            drawable.setState(state);
        }
    }
    
    public void K() {
        int[] array;
        if (((View)this.a).hasFocus()) {
            array = ViewGroup.FOCUSED_STATE_SET;
        }
        else {
            array = ViewGroup.EMPTY_STATE_SET;
        }
        final Drawable background = this.c.getBackground();
        if (background != null) {
            background.setState(array);
        }
        final Drawable background2 = this.d.getBackground();
        if (background2 != null) {
            background2.setState(array);
        }
        ((View)this).invalidate();
    }
    
    public final void L() {
        final CharSequence queryHint = this.getQueryHint();
        final SearchAutoComplete a = this.a;
        CharSequence charSequence = queryHint;
        if (queryHint == null) {
            charSequence = "";
        }
        ((TextView)a).setHint(this.o(charSequence));
    }
    
    public final void M() {
        this.a.setThreshold(this.V.getSuggestThreshold());
        ((TextView)this.a).setImeOptions(this.V.getImeOptions());
        final int inputType = this.V.getInputType();
        final int n = 1;
        int inputType2 = inputType;
        if ((inputType & 0xF) == 0x1) {
            inputType2 = (inputType & 0xFFFEFFFF);
            if (this.V.getSuggestAuthority() != null) {
                inputType2 = (inputType2 | 0x10000 | 0x80000);
            }
        }
        ((TextView)this.a).setInputType(inputType2);
        final jo f = this.F;
        if (f != null) {
            f.a(null);
        }
        if (this.V.getSuggestAuthority() != null) {
            final ds1 ds1 = new ds1(((View)this).getContext(), this, this.V, this.A0);
            this.F = ds1;
            this.a.setAdapter((ListAdapter)ds1);
            final ds1 ds2 = (ds1)this.F;
            int n2 = n;
            if (this.I) {
                n2 = 2;
            }
            ds2.x(n2);
        }
    }
    
    public final void N() {
        int visibility;
        if (this.s() && (((View)this.f).getVisibility() == 0 || ((View)this.h).getVisibility() == 0)) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.d.setVisibility(visibility);
    }
    
    public final void O(final boolean b) {
        int visibility;
        if (this.G && this.s() && ((View)this).hasFocus() && (b || !this.M)) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.f.setVisibility(visibility);
    }
    
    public final void P(final boolean d) {
        this.D = d;
        final int n = 0;
        int visibility;
        if (d) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        final boolean b = TextUtils.isEmpty((CharSequence)((EditText)this.a).getText()) ^ true;
        this.e.setVisibility(visibility);
        this.O(b);
        final View b2 = this.b;
        int visibility2;
        if (d) {
            visibility2 = 8;
        }
        else {
            visibility2 = 0;
        }
        b2.setVisibility(visibility2);
        int visibility3 = 0;
        Label_0093: {
            if (this.p.getDrawable() != null) {
                visibility3 = n;
                if (!this.C) {
                    break Label_0093;
                }
            }
            visibility3 = 8;
        }
        this.p.setVisibility(visibility3);
        this.J();
        this.Q(b ^ true);
        this.N();
    }
    
    public final void Q(final boolean b) {
        final boolean m = this.M;
        int visibility;
        final int n = visibility = 8;
        if (m) {
            visibility = n;
            if (!this.q()) {
                visibility = n;
                if (b) {
                    this.f.setVisibility(8);
                    visibility = 0;
                }
            }
        }
        this.h.setVisibility(visibility);
    }
    
    @Override
    public void a() {
        this.I("", false);
        this.clearFocus();
        this.P(true);
        ((TextView)this.a).setImeOptions(this.U);
        this.Q = false;
    }
    
    @Override
    public void b() {
        if (this.Q) {
            return;
        }
        this.Q = true;
        final int imeOptions = ((TextView)this.a).getImeOptions();
        this.U = imeOptions;
        ((TextView)this.a).setImeOptions(imeOptions | 0x2000000);
        ((TextView)this.a).setText((CharSequence)"");
        this.setIconified(false);
    }
    
    public void clearFocus() {
        this.J = true;
        super.clearFocus();
        ((View)this.a).clearFocus();
        this.a.setImeVisibility(false);
        this.J = false;
    }
    
    public void g() {
        if (this.i.getWidth() > 1) {
            final Resources resources = ((View)this).getContext().getResources();
            final int paddingLeft = this.c.getPaddingLeft();
            final Rect rect = new Rect();
            final boolean b = u42.b((View)this);
            int n;
            if (this.C) {
                n = resources.getDimensionPixelSize(ab1.c) + resources.getDimensionPixelSize(ab1.d);
            }
            else {
                n = 0;
            }
            this.a.getDropDownBackground().getPadding(rect);
            final int left = rect.left;
            int dropDownHorizontalOffset;
            if (b) {
                dropDownHorizontalOffset = -left;
            }
            else {
                dropDownHorizontalOffset = paddingLeft - (left + n);
            }
            this.a.setDropDownHorizontalOffset(dropDownHorizontalOffset);
            this.a.setDropDownWidth(this.i.getWidth() + rect.left + rect.right + n - paddingLeft);
        }
    }
    
    public int getImeOptions() {
        return ((TextView)this.a).getImeOptions();
    }
    
    public int getInputType() {
        return ((TextView)this.a).getInputType();
    }
    
    public int getMaxWidth() {
        return this.K;
    }
    
    public CharSequence getQuery() {
        return (CharSequence)((EditText)this.a).getText();
    }
    
    public CharSequence getQueryHint() {
        CharSequence charSequence = this.H;
        if (charSequence == null) {
            final SearchableInfo v = this.V;
            if (v != null && v.getHintId() != 0) {
                charSequence = ((View)this).getContext().getText(this.V.getHintId());
            }
            else {
                charSequence = this.y;
            }
        }
        return charSequence;
    }
    
    public int getSuggestionCommitIconResId() {
        return this.v;
    }
    
    public int getSuggestionRowLayout() {
        return this.t;
    }
    
    public jo getSuggestionsAdapter() {
        return this.F;
    }
    
    public final Intent h(final String s, final Uri data, final String s2, final String s3, final int n, final String s4) {
        final Intent intent = new Intent(s);
        intent.addFlags(268435456);
        if (data != null) {
            intent.setData(data);
        }
        intent.putExtra("user_query", this.P);
        if (s3 != null) {
            intent.putExtra("query", s3);
        }
        if (s2 != null) {
            intent.putExtra("intent_extra_data_key", s2);
        }
        final Bundle w = this.W;
        if (w != null) {
            intent.putExtra("app_data", w);
        }
        if (n != 0) {
            intent.putExtra("action_key", n);
            intent.putExtra("action_msg", s4);
        }
        intent.setComponent(this.V.getSearchActivity());
        return intent;
    }
    
    public final Intent i(final Cursor cursor, int position, final String ex) {
        try {
            String s;
            if ((s = ds1.o(cursor, "suggest_intent_action")) == null) {
                s = this.V.getSuggestIntentAction();
            }
            String s2;
            if ((s2 = s) == null) {
                s2 = "android.intent.action.SEARCH";
            }
            String str;
            if ((str = ds1.o(cursor, "suggest_intent_data")) == null) {
                str = this.V.getSuggestIntentData();
            }
            String string;
            if ((string = str) != null) {
                final String o = ds1.o(cursor, "suggest_intent_data_id");
                string = str;
                if (o != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(Uri.encode(o));
                    string = sb.toString();
                }
            }
            Uri parse;
            if (string == null) {
                parse = null;
            }
            else {
                parse = Uri.parse(string);
            }
            return this.h(s2, parse, ds1.o(cursor, "suggest_intent_extra_data"), ds1.o(cursor, "suggest_intent_query"), position, (String)ex);
        }
        catch (final RuntimeException ex) {
            try {
                position = cursor.getPosition();
            }
            catch (final RuntimeException ex2) {
                position = -1;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Search suggestions cursor at row ");
            sb2.append(position);
            sb2.append(" returned exception.");
            Log.w("SearchView", sb2.toString(), (Throwable)ex);
            return null;
        }
    }
    
    public final Intent j(final Intent intent, final SearchableInfo searchableInfo) {
        final ComponentName searchActivity = searchableInfo.getSearchActivity();
        final Intent intent2 = new Intent("android.intent.action.SEARCH");
        intent2.setComponent(searchActivity);
        final PendingIntent activity = PendingIntent.getActivity(((View)this).getContext(), 0, intent2, 1107296256);
        final Bundle bundle = new Bundle();
        final Bundle w = this.W;
        if (w != null) {
            bundle.putParcelable("app_data", (Parcelable)w);
        }
        final Intent intent3 = new Intent(intent);
        final Resources resources = ((View)this).getResources();
        String string;
        if (searchableInfo.getVoiceLanguageModeId() != 0) {
            string = resources.getString(searchableInfo.getVoiceLanguageModeId());
        }
        else {
            string = "free_form";
        }
        final int voicePromptTextId = searchableInfo.getVoicePromptTextId();
        final String s = null;
        String string2;
        if (voicePromptTextId != 0) {
            string2 = resources.getString(searchableInfo.getVoicePromptTextId());
        }
        else {
            string2 = null;
        }
        String string3;
        if (searchableInfo.getVoiceLanguageId() != 0) {
            string3 = resources.getString(searchableInfo.getVoiceLanguageId());
        }
        else {
            string3 = null;
        }
        int voiceMaxResults;
        if (searchableInfo.getVoiceMaxResults() != 0) {
            voiceMaxResults = searchableInfo.getVoiceMaxResults();
        }
        else {
            voiceMaxResults = 1;
        }
        intent3.putExtra("android.speech.extra.LANGUAGE_MODEL", string);
        intent3.putExtra("android.speech.extra.PROMPT", string2);
        intent3.putExtra("android.speech.extra.LANGUAGE", string3);
        intent3.putExtra("android.speech.extra.MAX_RESULTS", voiceMaxResults);
        String flattenToShortString;
        if (searchActivity == null) {
            flattenToShortString = s;
        }
        else {
            flattenToShortString = searchActivity.flattenToShortString();
        }
        intent3.putExtra("calling_package", flattenToShortString);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", (Parcelable)activity);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle);
        return intent3;
    }
    
    public final Intent k(final Intent intent, final SearchableInfo searchableInfo) {
        final Intent intent2 = new Intent(intent);
        final ComponentName searchActivity = searchableInfo.getSearchActivity();
        String flattenToShortString;
        if (searchActivity == null) {
            flattenToShortString = null;
        }
        else {
            flattenToShortString = searchActivity.flattenToShortString();
        }
        intent2.putExtra("calling_package", flattenToShortString);
        return intent2;
    }
    
    public final void l() {
        this.a.dismissDropDown();
    }
    
    public void m() {
        if (Build$VERSION.SDK_INT >= 29) {
            SearchView.k.a(this.a);
        }
        else {
            final o p1 = SearchView.p1;
            p1.b(this.a);
            p1.a(this.a);
        }
    }
    
    public final void n(final View view, final Rect rect) {
        view.getLocationInWindow(this.m);
        ((View)this).getLocationInWindow(this.n);
        final int[] m = this.m;
        final int n = m[1];
        final int[] n2 = this.n;
        final int n3 = n - n2[1];
        final int n4 = m[0] - n2[0];
        rect.set(n4, n3, view.getWidth() + n4, view.getHeight() + n3);
    }
    
    public final CharSequence o(final CharSequence charSequence) {
        if (this.C && this.q != null) {
            final int n = (int)(((TextView)this.a).getTextSize() * 1.25);
            this.q.setBounds(0, 0, n, n);
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder((CharSequence)"   ");
            spannableStringBuilder.setSpan((Object)new ImageSpan(this.q), 1, 2, 33);
            spannableStringBuilder.append(charSequence);
            return (CharSequence)spannableStringBuilder;
        }
        return charSequence;
    }
    
    public void onDetachedFromWindow() {
        ((View)this).removeCallbacks(this.c0);
        ((View)this).post(this.k0);
        super.onDetachedFromWindow();
    }
    
    @Override
    public void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (b) {
            this.n((View)this.a, this.k);
            final Rect l = this.l;
            final Rect k = this.k;
            l.set(k.left, 0, k.right, n4 - n2);
            final q j = this.j;
            if (j == null) {
                ((View)this).setTouchDelegate((TouchDelegate)(this.j = new q(this.l, this.k, (View)this.a)));
            }
            else {
                j.a(this.l, this.k);
            }
        }
    }
    
    @Override
    public void onMeasure(int a, int b) {
        if (this.q()) {
            super.onMeasure(a, b);
            return;
        }
        final int mode = View$MeasureSpec.getMode(a);
        final int size = View$MeasureSpec.getSize(a);
        Label_0113: {
            if (mode != Integer.MIN_VALUE) {
                if (mode != 0) {
                    if (mode != 1073741824) {
                        a = size;
                        break Label_0113;
                    }
                    final int k = this.K;
                    a = size;
                    if (k <= 0) {
                        break Label_0113;
                    }
                    a = k;
                }
                else {
                    a = this.K;
                    if (a > 0) {
                        break Label_0113;
                    }
                    a = this.getPreferredWidth();
                    break Label_0113;
                }
            }
            else {
                a = this.K;
                if (a <= 0) {
                    a = this.getPreferredWidth();
                }
            }
            a = Math.min(a, size);
        }
        final int mode2 = View$MeasureSpec.getMode(b);
        b = View$MeasureSpec.getSize(b);
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                b = this.getPreferredHeight();
            }
        }
        else {
            b = Math.min(this.getPreferredHeight(), b);
        }
        super.onMeasure(View$MeasureSpec.makeMeasureSpec(a, 1073741824), View$MeasureSpec.makeMeasureSpec(b, 1073741824));
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof p)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final p p = (p)parcelable;
        super.onRestoreInstanceState(p.getSuperState());
        this.P(p.a);
        ((View)this).requestLayout();
    }
    
    public Parcelable onSaveInstanceState() {
        final p p = new p(super.onSaveInstanceState());
        p.a = this.q();
        return (Parcelable)p;
    }
    
    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        this.G();
    }
    
    public final boolean p() {
        final SearchableInfo v = this.V;
        boolean b2;
        final boolean b = b2 = false;
        if (v != null) {
            b2 = b;
            if (v.getVoiceSearchEnabled()) {
                Intent intent;
                if (this.V.getVoiceSearchLaunchWebSearch()) {
                    intent = this.w;
                }
                else if (this.V.getVoiceSearchLaunchRecognizer()) {
                    intent = this.x;
                }
                else {
                    intent = null;
                }
                b2 = b;
                if (intent != null) {
                    b2 = b;
                    if (((View)this).getContext().getPackageManager().resolveActivity(intent, 65536) != null) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public boolean q() {
        return this.D;
    }
    
    public boolean requestFocus(final int n, final Rect rect) {
        if (this.J) {
            return false;
        }
        if (!((View)this).isFocusable()) {
            return false;
        }
        if (!this.q()) {
            final boolean requestFocus = ((View)this.a).requestFocus(n, rect);
            if (requestFocus) {
                this.P(false);
            }
            return requestFocus;
        }
        return super.requestFocus(n, rect);
    }
    
    public final boolean s() {
        return (this.G || this.M) && !this.q();
    }
    
    public void setAppSearchData(final Bundle w) {
        this.W = w;
    }
    
    public void setIconified(final boolean b) {
        if (b) {
            this.w();
        }
        else {
            this.A();
        }
    }
    
    public void setIconifiedByDefault(final boolean c) {
        if (this.C == c) {
            return;
        }
        this.P(this.C = c);
        this.L();
    }
    
    public void setImeOptions(final int imeOptions) {
        ((TextView)this.a).setImeOptions(imeOptions);
    }
    
    public void setInputType(final int inputType) {
        ((TextView)this.a).setInputType(inputType);
    }
    
    public void setMaxWidth(final int k) {
        this.K = k;
        ((View)this).requestLayout();
    }
    
    public void setOnCloseListener(final l l) {
    }
    
    public void setOnQueryTextFocusChangeListener(final View$OnFocusChangeListener z) {
        this.z = z;
    }
    
    public void setOnQueryTextListener(final m m) {
    }
    
    public void setOnSearchClickListener(final View$OnClickListener a) {
        this.A = a;
    }
    
    public void setOnSuggestionListener(final n n) {
    }
    
    public void setQueryHint(final CharSequence h) {
        this.H = h;
        this.L();
    }
    
    public void setQueryRefinementEnabled(final boolean i) {
        this.I = i;
        final jo f = this.F;
        if (f instanceof ds1) {
            final ds1 ds1 = (ds1)f;
            int n;
            if (i) {
                n = 2;
            }
            else {
                n = 1;
            }
            ds1.x(n);
        }
    }
    
    public void setSearchableInfo(final SearchableInfo v) {
        this.V = v;
        if (v != null) {
            this.M();
            this.L();
        }
        final boolean p = this.p();
        this.M = p;
        if (p) {
            ((TextView)this.a).setPrivateImeOptions("nm");
        }
        this.P(this.q());
    }
    
    public void setSubmitButtonEnabled(final boolean g) {
        this.G = g;
        this.P(this.q());
    }
    
    public void setSuggestionsAdapter(final jo jo) {
        this.F = jo;
        this.a.setAdapter((ListAdapter)jo);
    }
    
    public final void t(final Intent obj) {
        if (obj == null) {
            return;
        }
        try {
            ((View)this).getContext().startActivity(obj);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed launch activity: ");
            sb.append(obj);
            Log.e("SearchView", sb.toString(), (Throwable)ex);
        }
    }
    
    public void u(final int n, final String s, final String s2) {
        ((View)this).getContext().startActivity(this.h("android.intent.action.SEARCH", null, null, s2, n, s));
    }
    
    public final boolean v(final int n, final int n2, final String s) {
        final Cursor b = this.F.b();
        if (b != null && b.moveToPosition(n)) {
            this.t(this.i(b, n2, s));
            return true;
        }
        return false;
    }
    
    public void w() {
        if (TextUtils.isEmpty((CharSequence)((EditText)this.a).getText())) {
            if (this.C) {
                this.clearFocus();
                this.P(true);
            }
        }
        else {
            ((TextView)this.a).setText((CharSequence)"");
            ((View)this.a).requestFocus();
            this.a.setImeVisibility(true);
        }
    }
    
    public boolean x(final int n, final int n2, final String s) {
        this.v(n, 0, null);
        this.a.setImeVisibility(false);
        this.l();
        return true;
    }
    
    public boolean y(final int n) {
        this.H(n);
        return true;
    }
    
    public void z(final CharSequence query) {
        this.setQuery(query);
    }
    
    public static class SearchAutoComplete extends w5
    {
        public int a;
        public SearchView b;
        public boolean c;
        public final Runnable d;
        
        public SearchAutoComplete(final Context context, final AttributeSet set) {
            this(context, set, sa1.m);
        }
        
        public SearchAutoComplete(final Context context, final AttributeSet set, final int n) {
            super(context, set, n);
            this.d = new Runnable(this) {
                public final SearchAutoComplete a;
                
                @Override
                public void run() {
                    this.a.c();
                }
            };
            this.a = this.getThreshold();
        }
        
        private int getSearchViewTextMinWidthDp() {
            final Configuration configuration = ((View)this).getResources().getConfiguration();
            final int screenWidthDp = configuration.screenWidthDp;
            final int screenHeightDp = configuration.screenHeightDp;
            if (screenWidthDp >= 960 && screenHeightDp >= 720 && configuration.orientation == 2) {
                return 256;
            }
            if (screenWidthDp < 600 && (screenWidthDp < 640 || screenHeightDp < 480)) {
                return 160;
            }
            return 192;
        }
        
        public void a() {
            if (Build$VERSION.SDK_INT >= 29) {
                SearchView.k.b(this, 1);
                if (this.enoughToFilter()) {
                    this.showDropDown();
                }
            }
            else {
                SearchView.p1.c(this);
            }
        }
        
        public boolean b() {
            return TextUtils.getTrimmedLength((CharSequence)((EditText)this).getText()) == 0;
        }
        
        public void c() {
            if (this.c) {
                ((InputMethodManager)((View)this).getContext().getSystemService("input_method")).showSoftInput((View)this, 0);
                this.c = false;
            }
        }
        
        public boolean enoughToFilter() {
            return this.a <= 0 || super.enoughToFilter();
        }
        
        @Override
        public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
            final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (this.c) {
                ((View)this).removeCallbacks(this.d);
                ((View)this).post(this.d);
            }
            return onCreateInputConnection;
        }
        
        public void onFinishInflate() {
            super.onFinishInflate();
            ((TextView)this).setMinWidth((int)TypedValue.applyDimension(1, (float)this.getSearchViewTextMinWidthDp(), ((View)this).getResources().getDisplayMetrics()));
        }
        
        public void onFocusChanged(final boolean b, final int n, final Rect rect) {
            super.onFocusChanged(b, n, rect);
            this.b.E();
        }
        
        public boolean onKeyPreIme(final int n, final KeyEvent keyEvent) {
            if (n == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    final KeyEvent$DispatcherState keyDispatcherState = ((View)this).getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent, (Object)this);
                    }
                    return true;
                }
                if (keyEvent.getAction() == 1) {
                    final KeyEvent$DispatcherState keyDispatcherState2 = ((View)this).getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.b.clearFocus();
                        this.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(n, keyEvent);
        }
        
        public void onWindowFocusChanged(final boolean b) {
            super.onWindowFocusChanged(b);
            if (b && ((View)this.b).hasFocus() && ((View)this).getVisibility() == 0) {
                this.c = true;
                if (SearchView.r(((View)this).getContext())) {
                    this.a();
                }
            }
        }
        
        public void performCompletion() {
        }
        
        public void replaceText(final CharSequence charSequence) {
        }
        
        public void setImeVisibility(final boolean b) {
            final InputMethodManager inputMethodManager = (InputMethodManager)((View)this).getContext().getSystemService("input_method");
            if (!b) {
                this.c = false;
                ((View)this).removeCallbacks(this.d);
                inputMethodManager.hideSoftInputFromWindow(((View)this).getWindowToken(), 0);
                return;
            }
            if (inputMethodManager.isActive((View)this)) {
                this.c = false;
                ((View)this).removeCallbacks(this.d);
                inputMethodManager.showSoftInput((View)this, 0);
                return;
            }
            this.c = true;
        }
        
        public void setSearchView(final SearchView b) {
            this.b = b;
        }
        
        public void setThreshold(final int n) {
            super.setThreshold(n);
            this.a = n;
        }
    }
    
    public abstract static class k
    {
        public static void a(final AutoCompleteTextView autoCompleteTextView) {
            autoCompleteTextView.refreshAutoCompleteResults();
        }
        
        public static void b(final SearchAutoComplete searchAutoComplete, final int inputMethodMode) {
            searchAutoComplete.setInputMethodMode(inputMethodMode);
        }
    }
    
    public interface l
    {
    }
    
    public interface m
    {
    }
    
    public interface n
    {
    }
    
    public static class o
    {
        public Method a;
        public Method b;
        public Method c;
        
        public o() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: invokespecial   java/lang/Object.<init>:()V
            //     4: aload_0        
            //     5: aconst_null    
            //     6: putfield        androidx/appcompat/widget/SearchView$o.a:Ljava/lang/reflect/Method;
            //     9: aload_0        
            //    10: aconst_null    
            //    11: putfield        androidx/appcompat/widget/SearchView$o.b:Ljava/lang/reflect/Method;
            //    14: aload_0        
            //    15: aconst_null    
            //    16: putfield        androidx/appcompat/widget/SearchView$o.c:Ljava/lang/reflect/Method;
            //    19: invokestatic    androidx/appcompat/widget/SearchView$o.d:()V
            //    22: ldc             Landroid/widget/AutoCompleteTextView;.class
            //    24: ldc             "doBeforeTextChanged"
            //    26: iconst_0       
            //    27: anewarray       Ljava/lang/Class;
            //    30: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    33: astore_1       
            //    34: aload_0        
            //    35: aload_1        
            //    36: putfield        androidx/appcompat/widget/SearchView$o.a:Ljava/lang/reflect/Method;
            //    39: aload_1        
            //    40: iconst_1       
            //    41: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
            //    44: ldc             Landroid/widget/AutoCompleteTextView;.class
            //    46: ldc             "doAfterTextChanged"
            //    48: iconst_0       
            //    49: anewarray       Ljava/lang/Class;
            //    52: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    55: astore_1       
            //    56: aload_0        
            //    57: aload_1        
            //    58: putfield        androidx/appcompat/widget/SearchView$o.b:Ljava/lang/reflect/Method;
            //    61: aload_1        
            //    62: iconst_1       
            //    63: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
            //    66: ldc             Landroid/widget/AutoCompleteTextView;.class
            //    68: ldc             "ensureImeVisible"
            //    70: iconst_1       
            //    71: anewarray       Ljava/lang/Class;
            //    74: dup            
            //    75: iconst_0       
            //    76: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
            //    79: aastore        
            //    80: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    83: astore_1       
            //    84: aload_0        
            //    85: aload_1        
            //    86: putfield        androidx/appcompat/widget/SearchView$o.c:Ljava/lang/reflect/Method;
            //    89: aload_1        
            //    90: iconst_1       
            //    91: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
            //    94: return         
            //    95: astore_1       
            //    96: goto            44
            //    99: astore_1       
            //   100: goto            66
            //   103: astore_1       
            //   104: goto            94
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                             
            //  -----  -----  -----  -----  ---------------------------------
            //  22     44     95     99     Ljava/lang/NoSuchMethodException;
            //  44     66     99     103    Ljava/lang/NoSuchMethodException;
            //  66     94     103    107    Ljava/lang/NoSuchMethodException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 59 out of bounds for length 59
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public static void d() {
            if (Build$VERSION.SDK_INT < 29) {
                return;
            }
            throw new UnsupportedClassVersionError("This function can only be used for API Level < 29.");
        }
        
        public void a(final AutoCompleteTextView obj) {
            d();
            final Method b = this.b;
            if (b == null) {
                return;
            }
            try {
                b.invoke(obj, new Object[0]);
            }
            catch (final Exception ex) {}
        }
        
        public void b(final AutoCompleteTextView obj) {
            d();
            final Method a = this.a;
            if (a == null) {
                return;
            }
            try {
                a.invoke(obj, new Object[0]);
            }
            catch (final Exception ex) {}
        }
        
        public void c(final AutoCompleteTextView obj) {
            d();
            final Method c = this.c;
            if (c == null) {
                return;
            }
            try {
                c.invoke(obj, Boolean.TRUE);
            }
            catch (final Exception ex) {}
        }
    }
    
    public static class p extends e
    {
        public static final Parcelable$Creator<p> CREATOR;
        public boolean a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator() {
                public p a(final Parcel parcel) {
                    return new p(parcel, null);
                }
                
                public p b(final Parcel parcel, final ClassLoader classLoader) {
                    return new p(parcel, classLoader);
                }
                
                public p[] c(final int n) {
                    return new p[n];
                }
            };
        }
        
        public p(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.a = (boolean)parcel.readValue((ClassLoader)null);
        }
        
        public p(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SearchView.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" isIconified=");
            sb.append(this.a);
            sb.append("}");
            return sb.toString();
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeValue((Object)this.a);
        }
    }
    
    public static class q extends TouchDelegate
    {
        public final View a;
        public final Rect b;
        public final Rect c;
        public final Rect d;
        public final int e;
        public boolean f;
        
        public q(final Rect rect, final Rect rect2, final View a) {
            super(rect, a);
            this.e = ViewConfiguration.get(a.getContext()).getScaledTouchSlop();
            this.b = new Rect();
            this.d = new Rect();
            this.c = new Rect();
            this.a(rect, rect2);
            this.a = a;
        }
        
        public void a(Rect d, final Rect rect) {
            this.b.set(d);
            this.d.set(d);
            d = this.d;
            final int e = this.e;
            d.inset(-e, -e);
            this.c.set(rect);
        }
        
        public boolean onTouchEvent(final MotionEvent motionEvent) {
            final int n = (int)motionEvent.getX();
            final int n2 = (int)motionEvent.getY();
            final int action = motionEvent.getAction();
            final boolean b = false;
            int f = 1;
            boolean b2 = false;
            Label_0137: {
                Label_0132: {
                    if (action != 0) {
                        if (action != 1 && action != 2) {
                            if (action != 3) {
                                break Label_0132;
                            }
                            f = (this.f ? 1 : 0);
                            this.f = false;
                        }
                        else {
                            final boolean f2 = this.f;
                            if ((f = (f2 ? 1 : 0)) != 0) {
                                f = (f2 ? 1 : 0);
                                if (!this.d.contains(n, n2)) {
                                    f = (f2 ? 1 : 0);
                                    b2 = false;
                                    break Label_0137;
                                }
                            }
                        }
                        b2 = true;
                        break Label_0137;
                    }
                    if (this.b.contains(n, n2)) {
                        this.f = true;
                        b2 = true;
                        break Label_0137;
                    }
                }
                b2 = true;
                f = 0;
            }
            boolean dispatchTouchEvent = b;
            if (f != 0) {
                float n3;
                int n4;
                if (b2 && !this.c.contains(n, n2)) {
                    n3 = (float)(this.a.getWidth() / 2);
                    n4 = this.a.getHeight() / 2;
                }
                else {
                    final Rect c = this.c;
                    n3 = (float)(n - c.left);
                    n4 = n2 - c.top;
                }
                motionEvent.setLocation(n3, (float)n4);
                dispatchTouchEvent = this.a.dispatchTouchEvent(motionEvent);
            }
            return dispatchTouchEvent;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.widget.AbsListView;
import java.lang.ref.WeakReference;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.SimpleCursorAdapter;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.ArrayAdapter;
import android.content.DialogInterface$OnKeyListener;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnCancelListener;
import android.widget.AdapterView$OnItemSelectedListener;
import android.database.Cursor;
import android.content.DialogInterface$OnMultiChoiceClickListener;
import androidx.appcompat.widget.b;
import android.widget.FrameLayout;
import android.view.LayoutInflater;
import android.text.TextUtils;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.KeyEvent;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.content.res.Resources$Theme;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.DialogInterface;
import android.os.Message;
import android.widget.Button;
import android.widget.ListView;
import android.view.Window;
import android.content.Context;
import android.view.View$OnClickListener;
import android.os.Handler;
import android.widget.ListAdapter;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.graphics.drawable.Drawable;
import androidx.core.widget.NestedScrollView;

public class AlertController
{
    public NestedScrollView A;
    public int B;
    public Drawable C;
    public ImageView D;
    public TextView E;
    public TextView F;
    public View G;
    public ListAdapter H;
    public int I;
    public int J;
    public int K;
    public int L;
    public int M;
    public int N;
    public int O;
    public boolean P;
    public int Q;
    public Handler R;
    public final View$OnClickListener S;
    public final Context a;
    public final q6 b;
    public final Window c;
    public final int d;
    public CharSequence e;
    public CharSequence f;
    public ListView g;
    public View h;
    public int i;
    public int j;
    public int k;
    public int l;
    public int m;
    public boolean n;
    public Button o;
    public CharSequence p;
    public Message q;
    public Drawable r;
    public Button s;
    public CharSequence t;
    public Message u;
    public Drawable v;
    public Button w;
    public CharSequence x;
    public Message y;
    public Drawable z;
    
    public AlertController(final Context a, final q6 b, final Window c) {
        this.n = false;
        this.B = 0;
        this.I = -1;
        this.Q = 0;
        this.S = (View$OnClickListener)new View$OnClickListener(this) {
            public final AlertController a;
            
            public void onClick(final View view) {
                final AlertController a = this.a;
                Message message2 = null;
                Label_0081: {
                    while (true) {
                        Label_0032: {
                            if (view != a.o) {
                                break Label_0032;
                            }
                            final Message q = a.q;
                            if (q == null) {
                                break Label_0032;
                            }
                            final Message message = q;
                            message2 = Message.obtain(message);
                            break Label_0081;
                        }
                        if (view == a.s) {
                            final Message u = a.u;
                            if (u != null) {
                                final Message message = u;
                                continue;
                            }
                        }
                        break;
                    }
                    if (view == a.w) {
                        final Message y = a.y;
                        if (y != null) {
                            message2 = Message.obtain(y);
                            break Label_0081;
                        }
                    }
                    message2 = null;
                }
                if (message2 != null) {
                    message2.sendToTarget();
                }
                final AlertController a2 = this.a;
                a2.R.obtainMessage(1, (Object)a2.b).sendToTarget();
            }
        };
        this.a = a;
        this.b = b;
        this.c = c;
        this.R = new c((DialogInterface)b);
        final TypedArray obtainStyledAttributes = a.obtainStyledAttributes((AttributeSet)null, bc1.F, sa1.k, 0);
        this.J = obtainStyledAttributes.getResourceId(bc1.G, 0);
        this.K = obtainStyledAttributes.getResourceId(bc1.I, 0);
        this.L = obtainStyledAttributes.getResourceId(bc1.K, 0);
        this.M = obtainStyledAttributes.getResourceId(bc1.L, 0);
        this.N = obtainStyledAttributes.getResourceId(bc1.N, 0);
        this.O = obtainStyledAttributes.getResourceId(bc1.J, 0);
        this.P = obtainStyledAttributes.getBoolean(bc1.M, true);
        this.d = obtainStyledAttributes.getDimensionPixelSize(bc1.H, 0);
        obtainStyledAttributes.recycle();
        b.supportRequestWindowFeature(1);
    }
    
    public static boolean a(final View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        final ViewGroup viewGroup = (ViewGroup)view;
        int i = viewGroup.getChildCount();
        while (i > 0) {
            if (a(viewGroup.getChildAt(--i))) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean y(final Context context) {
        final TypedValue typedValue = new TypedValue();
        final Resources$Theme theme = context.getTheme();
        final int j = sa1.j;
        boolean b = true;
        theme.resolveAttribute(j, typedValue, true);
        if (typedValue.data == 0) {
            b = false;
        }
        return b;
    }
    
    public final void b(final Button button) {
        final LinearLayout$LayoutParams layoutParams = (LinearLayout$LayoutParams)((View)button).getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        ((View)button).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public int c(final int n) {
        final TypedValue typedValue = new TypedValue();
        this.a.getTheme().resolveAttribute(n, typedValue, true);
        return typedValue.resourceId;
    }
    
    public ListView d() {
        return this.g;
    }
    
    public void e() {
        this.b.setContentView(this.i());
        this.x();
    }
    
    public boolean f(final int n, final KeyEvent keyEvent) {
        final NestedScrollView a = this.A;
        return a != null && a.m(keyEvent);
    }
    
    public boolean g(final int n, final KeyEvent keyEvent) {
        final NestedScrollView a = this.A;
        return a != null && a.m(keyEvent);
    }
    
    public final ViewGroup h(View inflate, View inflate2) {
        if (inflate == null) {
            inflate = inflate2;
            if (inflate2 instanceof ViewStub) {
                inflate = ((ViewStub)inflate2).inflate();
            }
            return (ViewGroup)inflate;
        }
        if (inflate2 != null) {
            final ViewParent parent = inflate2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup)parent).removeView(inflate2);
            }
        }
        inflate2 = inflate;
        if (inflate instanceof ViewStub) {
            inflate2 = ((ViewStub)inflate).inflate();
        }
        return (ViewGroup)inflate2;
    }
    
    public final int i() {
        final int k = this.K;
        if (k == 0) {
            return this.J;
        }
        if (this.Q == 1) {
            return k;
        }
        return this.J;
    }
    
    public void j(final int n, final CharSequence x, final DialogInterface$OnClickListener dialogInterface$OnClickListener, final Message message, final Drawable z) {
        Message obtainMessage = message;
        if (message == null) {
            obtainMessage = message;
            if (dialogInterface$OnClickListener != null) {
                obtainMessage = this.R.obtainMessage(n, (Object)dialogInterface$OnClickListener);
            }
        }
        if (n != -3) {
            if (n != -2) {
                if (n != -1) {
                    throw new IllegalArgumentException("Button does not exist");
                }
                this.p = x;
                this.q = obtainMessage;
                this.r = z;
            }
            else {
                this.t = x;
                this.u = obtainMessage;
                this.v = z;
            }
        }
        else {
            this.x = x;
            this.y = obtainMessage;
            this.z = z;
        }
    }
    
    public void k(final View g) {
        this.G = g;
    }
    
    public void l(final int b) {
        this.C = null;
        this.B = b;
        final ImageView d = this.D;
        if (d != null) {
            if (b != 0) {
                d.setVisibility(0);
                this.D.setImageResource(this.B);
            }
            else {
                d.setVisibility(8);
            }
        }
    }
    
    public void m(final Drawable drawable) {
        this.C = drawable;
        this.B = 0;
        final ImageView d = this.D;
        if (d != null) {
            if (drawable != null) {
                d.setVisibility(0);
                this.D.setImageDrawable(drawable);
            }
            else {
                d.setVisibility(8);
            }
        }
    }
    
    public void n(final CharSequence charSequence) {
        this.f = charSequence;
        final TextView f = this.F;
        if (f != null) {
            f.setText(charSequence);
        }
    }
    
    public final void o(final ViewGroup viewGroup, final View view, final int n, final int n2) {
        final View viewById = this.c.findViewById(db1.v);
        final View viewById2 = this.c.findViewById(db1.u);
        o32.I0(view, n, n2);
        if (viewById != null) {
            viewGroup.removeView(viewById);
        }
        if (viewById2 != null) {
            viewGroup.removeView(viewById2);
        }
    }
    
    public void p(final CharSequence charSequence) {
        this.e = charSequence;
        final TextView e = this.E;
        if (e != null) {
            e.setText(charSequence);
        }
    }
    
    public void q(final int i) {
        this.h = null;
        this.i = i;
        this.n = false;
    }
    
    public void r(final View h) {
        this.h = h;
        this.i = 0;
        this.n = false;
    }
    
    public void s(final View h, final int j, final int k, final int l, final int m) {
        this.h = h;
        this.i = 0;
        this.n = true;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
    }
    
    public final void t(final ViewGroup viewGroup) {
        ((View)(this.o = (Button)((View)viewGroup).findViewById(16908313))).setOnClickListener(this.S);
        final boolean empty = TextUtils.isEmpty(this.p);
        final int n = 1;
        int n2;
        if (empty && this.r == null) {
            ((View)this.o).setVisibility(8);
            n2 = 0;
        }
        else {
            ((TextView)this.o).setText(this.p);
            final Drawable r = this.r;
            if (r != null) {
                final int d = this.d;
                r.setBounds(0, 0, d, d);
                ((TextView)this.o).setCompoundDrawables(this.r, (Drawable)null, (Drawable)null, (Drawable)null);
            }
            ((View)this.o).setVisibility(0);
            n2 = 1;
        }
        ((View)(this.s = (Button)((View)viewGroup).findViewById(16908314))).setOnClickListener(this.S);
        if (TextUtils.isEmpty(this.t) && this.v == null) {
            ((View)this.s).setVisibility(8);
        }
        else {
            ((TextView)this.s).setText(this.t);
            final Drawable v = this.v;
            if (v != null) {
                final int d2 = this.d;
                v.setBounds(0, 0, d2, d2);
                ((TextView)this.s).setCompoundDrawables(this.v, (Drawable)null, (Drawable)null, (Drawable)null);
            }
            ((View)this.s).setVisibility(0);
            n2 |= 0x2;
        }
        ((View)(this.w = (Button)((View)viewGroup).findViewById(16908315))).setOnClickListener(this.S);
        if (TextUtils.isEmpty(this.x) && this.z == null) {
            ((View)this.w).setVisibility(8);
        }
        else {
            ((TextView)this.w).setText(this.x);
            final Drawable z = this.z;
            if (z != null) {
                final int d3 = this.d;
                z.setBounds(0, 0, d3, d3);
                ((TextView)this.w).setCompoundDrawables(this.z, (Drawable)null, (Drawable)null, (Drawable)null);
            }
            ((View)this.w).setVisibility(0);
            n2 |= 0x4;
        }
        Label_0424: {
            if (y(this.a)) {
                Button button;
                if (n2 == 1) {
                    button = this.o;
                }
                else if (n2 == 2) {
                    button = this.s;
                }
                else {
                    if (n2 != 4) {
                        break Label_0424;
                    }
                    button = this.w;
                }
                this.b(button);
            }
        }
        int n3;
        if (n2 != 0) {
            n3 = n;
        }
        else {
            n3 = 0;
        }
        if (n3 == 0) {
            ((View)viewGroup).setVisibility(8);
        }
    }
    
    public final void u(ViewGroup viewGroup) {
        ((View)(this.A = (NestedScrollView)this.c.findViewById(db1.w))).setFocusable(false);
        this.A.setNestedScrollingEnabled(false);
        final TextView f = (TextView)((View)viewGroup).findViewById(16908299);
        this.F = f;
        if (f == null) {
            return;
        }
        final CharSequence f2 = this.f;
        if (f2 != null) {
            f.setText(f2);
        }
        else {
            ((View)f).setVisibility(8);
            ((ViewGroup)this.A).removeView((View)this.F);
            if (this.g != null) {
                viewGroup = (ViewGroup)((View)this.A).getParent();
                final int indexOfChild = viewGroup.indexOfChild((View)this.A);
                viewGroup.removeViewAt(indexOfChild);
                viewGroup.addView((View)this.g, indexOfChild, new ViewGroup$LayoutParams(-1, -1));
            }
            else {
                ((View)viewGroup).setVisibility(8);
            }
        }
    }
    
    public final void v(final ViewGroup viewGroup) {
        View view = this.h;
        boolean b = false;
        if (view == null) {
            if (this.i != 0) {
                view = LayoutInflater.from(this.a).inflate(this.i, viewGroup, false);
            }
            else {
                view = null;
            }
        }
        if (view != null) {
            b = true;
        }
        if (!b || !a(view)) {
            this.c.setFlags(131072, 131072);
        }
        if (b) {
            final FrameLayout frameLayout = (FrameLayout)this.c.findViewById(db1.n);
            ((ViewGroup)frameLayout).addView(view, new ViewGroup$LayoutParams(-1, -1));
            if (this.n) {
                ((View)frameLayout).setPadding(this.j, this.k, this.l, this.m);
            }
            if (this.g != null) {
                ((androidx.appcompat.widget.b.a)((View)viewGroup).getLayoutParams()).weight = 0.0f;
            }
        }
        else {
            ((View)viewGroup).setVisibility(8);
        }
    }
    
    public final void w(ViewGroup viewById) {
        if (this.G != null) {
            viewById.addView(this.G, 0, new ViewGroup$LayoutParams(-1, -2));
            viewById = (ViewGroup)this.c.findViewById(db1.O);
        }
        else {
            this.D = (ImageView)this.c.findViewById(16908294);
            if ((TextUtils.isEmpty(this.e) ^ true) && this.P) {
                (this.E = (TextView)this.c.findViewById(db1.j)).setText(this.e);
                final int b = this.B;
                if (b != 0) {
                    this.D.setImageResource(b);
                    return;
                }
                final Drawable c = this.C;
                if (c != null) {
                    this.D.setImageDrawable(c);
                    return;
                }
                this.E.setPadding(((View)this.D).getPaddingLeft(), ((View)this.D).getPaddingTop(), ((View)this.D).getPaddingRight(), ((View)this.D).getPaddingBottom());
                this.D.setVisibility(8);
                return;
            }
            else {
                this.c.findViewById(db1.O).setVisibility(8);
                this.D.setVisibility(8);
            }
        }
        ((View)viewById).setVisibility(8);
    }
    
    public final void x() {
        final View viewById = this.c.findViewById(db1.t);
        final int p = db1.P;
        final View viewById2 = viewById.findViewById(p);
        final int m = db1.m;
        final View viewById3 = viewById.findViewById(m);
        final int k = db1.k;
        final View viewById4 = viewById.findViewById(k);
        final ViewGroup viewGroup = (ViewGroup)viewById.findViewById(db1.o);
        this.v(viewGroup);
        final View viewById5 = ((View)viewGroup).findViewById(p);
        final View viewById6 = ((View)viewGroup).findViewById(m);
        final View viewById7 = ((View)viewGroup).findViewById(k);
        final ViewGroup h = this.h(viewById5, viewById2);
        final ViewGroup h2 = this.h(viewById6, viewById3);
        final ViewGroup h3 = this.h(viewById7, viewById4);
        this.u(h2);
        this.t(h3);
        this.w(h);
        final int visibility = ((View)viewGroup).getVisibility();
        final int n = false ? 1 : 0;
        final boolean b = visibility != 8;
        final boolean b2 = h != null && ((View)h).getVisibility() != 8;
        final boolean b3 = h3 != null && ((View)h3).getVisibility() != 8;
        if (!b3 && h2 != null) {
            final View viewById8 = ((View)h2).findViewById(db1.K);
            if (viewById8 != null) {
                viewById8.setVisibility(0);
            }
        }
        Label_0328: {
            View view;
            if (b2) {
                final NestedScrollView a = this.A;
                if (a != null) {
                    ((ViewGroup)a).setClipToPadding(true);
                }
                if (this.f == null && this.g == null) {
                    view = null;
                }
                else {
                    view = ((View)h).findViewById(db1.N);
                }
                if (view == null) {
                    break Label_0328;
                }
            }
            else {
                if (h2 == null) {
                    break Label_0328;
                }
                view = ((View)h2).findViewById(db1.L);
                if (view == null) {
                    break Label_0328;
                }
            }
            view.setVisibility(0);
        }
        final ListView g = this.g;
        if (g instanceof RecycleListView) {
            ((RecycleListView)g).a(b2, b3);
        }
        if (!b) {
            Object o = this.g;
            if (o == null) {
                o = this.A;
            }
            if (o != null) {
                int n2 = n;
                if (b3) {
                    n2 = 2;
                }
                this.o(h2, (View)o, (b2 ? 1 : 0) | n2, 3);
            }
        }
        final ListView g2 = this.g;
        if (g2 != null) {
            final ListAdapter h4 = this.H;
            if (h4 != null) {
                g2.setAdapter(h4);
                final int i = this.I;
                if (i > -1) {
                    ((AbsListView)g2).setItemChecked(i, true);
                    g2.setSelection(i);
                }
            }
        }
    }
    
    public static class RecycleListView extends ListView
    {
        public final int a;
        public final int b;
        
        public RecycleListView(final Context context, final AttributeSet set) {
            super(context, set);
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, bc1.k2);
            this.b = obtainStyledAttributes.getDimensionPixelOffset(bc1.l2, -1);
            this.a = obtainStyledAttributes.getDimensionPixelOffset(bc1.m2, -1);
        }
        
        public void a(final boolean b, final boolean b2) {
            if (!b2 || !b) {
                final int paddingLeft = ((View)this).getPaddingLeft();
                int n;
                if (b) {
                    n = ((View)this).getPaddingTop();
                }
                else {
                    n = this.a;
                }
                final int paddingRight = ((View)this).getPaddingRight();
                int n2;
                if (b2) {
                    n2 = ((View)this).getPaddingBottom();
                }
                else {
                    n2 = this.b;
                }
                ((View)this).setPadding(paddingLeft, n, paddingRight, n2);
            }
        }
    }
    
    public static class b
    {
        public int A;
        public int B;
        public int C;
        public int D;
        public boolean E;
        public boolean[] F;
        public boolean G;
        public boolean H;
        public int I;
        public DialogInterface$OnMultiChoiceClickListener J;
        public Cursor K;
        public String L;
        public String M;
        public boolean N;
        public AdapterView$OnItemSelectedListener O;
        public boolean P;
        public final Context a;
        public final LayoutInflater b;
        public int c;
        public Drawable d;
        public int e;
        public CharSequence f;
        public View g;
        public CharSequence h;
        public CharSequence i;
        public Drawable j;
        public DialogInterface$OnClickListener k;
        public CharSequence l;
        public Drawable m;
        public DialogInterface$OnClickListener n;
        public CharSequence o;
        public Drawable p;
        public DialogInterface$OnClickListener q;
        public boolean r;
        public DialogInterface$OnCancelListener s;
        public DialogInterface$OnDismissListener t;
        public DialogInterface$OnKeyListener u;
        public CharSequence[] v;
        public ListAdapter w;
        public DialogInterface$OnClickListener x;
        public int y;
        public View z;
        
        public b(final Context a) {
            this.c = 0;
            this.e = 0;
            this.E = false;
            this.I = -1;
            this.P = true;
            this.a = a;
            this.r = true;
            this.b = (LayoutInflater)a.getSystemService("layout_inflater");
        }
        
        public void a(final AlertController alertController) {
            final View g = this.g;
            if (g != null) {
                alertController.k(g);
            }
            else {
                final CharSequence f = this.f;
                if (f != null) {
                    alertController.p(f);
                }
                final Drawable d = this.d;
                if (d != null) {
                    alertController.m(d);
                }
                final int c = this.c;
                if (c != 0) {
                    alertController.l(c);
                }
                final int e = this.e;
                if (e != 0) {
                    alertController.l(alertController.c(e));
                }
            }
            final CharSequence h = this.h;
            if (h != null) {
                alertController.n(h);
            }
            final CharSequence i = this.i;
            if (i != null || this.j != null) {
                alertController.j(-1, i, this.k, null, this.j);
            }
            final CharSequence l = this.l;
            if (l != null || this.m != null) {
                alertController.j(-2, l, this.n, null, this.m);
            }
            final CharSequence o = this.o;
            if (o != null || this.p != null) {
                alertController.j(-3, o, this.q, null, this.p);
            }
            if (this.v != null || this.K != null || this.w != null) {
                this.b(alertController);
            }
            final View z = this.z;
            if (z != null) {
                if (this.E) {
                    alertController.s(z, this.A, this.B, this.C, this.D);
                }
                else {
                    alertController.r(z);
                }
            }
            else {
                final int y = this.y;
                if (y != 0) {
                    alertController.q(y);
                }
            }
        }
        
        public final void b(final AlertController alertController) {
            final RecycleListView g = (RecycleListView)this.b.inflate(alertController.L, (ViewGroup)null);
            Object w;
            if (this.G) {
                if (this.K == null) {
                    w = new ArrayAdapter(this, this.a, alertController.M, 16908308, this.v, g) {
                        public final RecycleListView a;
                        public final b b;
                        
                        public View getView(final int n, View view, final ViewGroup viewGroup) {
                            view = super.getView(n, view, viewGroup);
                            final boolean[] f = this.b.F;
                            if (f != null && f[n]) {
                                ((AbsListView)this.a).setItemChecked(n, true);
                            }
                            return view;
                        }
                    };
                }
                else {
                    w = new CursorAdapter(this, this.a, this.K, false, g, alertController) {
                        public final int a;
                        public final int b;
                        public final RecycleListView c;
                        public final AlertController d;
                        public final b e;
                        
                        {
                            final Cursor cursor2 = this.getCursor();
                            this.a = cursor2.getColumnIndexOrThrow(e.L);
                            this.b = cursor2.getColumnIndexOrThrow(e.M);
                        }
                        
                        public void bindView(final View view, final Context context, final Cursor cursor) {
                            ((TextView)view.findViewById(16908308)).setText((CharSequence)cursor.getString(this.a));
                            final RecycleListView c = this.c;
                            final int position = cursor.getPosition();
                            final int int1 = cursor.getInt(this.b);
                            boolean b = true;
                            if (int1 != 1) {
                                b = false;
                            }
                            ((AbsListView)c).setItemChecked(position, b);
                        }
                        
                        public View newView(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
                            return this.e.b.inflate(this.d.M, viewGroup, false);
                        }
                    };
                }
            }
            else {
                int n;
                if (this.H) {
                    n = alertController.N;
                }
                else {
                    n = alertController.O;
                }
                if (this.K != null) {
                    w = new SimpleCursorAdapter(this.a, n, this.K, new String[] { this.L }, new int[] { 16908308 });
                }
                else {
                    w = this.w;
                    if (w == null) {
                        w = new d(this.a, n, 16908308, this.v);
                    }
                }
            }
            alertController.H = (ListAdapter)w;
            alertController.I = this.I;
            Label_0241: {
                Object onItemClickListener;
                if (this.x != null) {
                    onItemClickListener = new AdapterView$OnItemClickListener(this, alertController) {
                        public final AlertController a;
                        public final b b;
                        
                        public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                            this.b.x.onClick((DialogInterface)this.a.b, n);
                            if (!this.b.H) {
                                this.a.b.dismiss();
                            }
                        }
                    };
                }
                else {
                    if (this.J == null) {
                        break Label_0241;
                    }
                    onItemClickListener = new AdapterView$OnItemClickListener(this, g, alertController) {
                        public final RecycleListView a;
                        public final AlertController b;
                        public final b c;
                        
                        public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                            final boolean[] f = this.c.F;
                            if (f != null) {
                                f[n] = ((AbsListView)this.a).isItemChecked(n);
                            }
                            this.c.J.onClick((DialogInterface)this.b.b, n, ((AbsListView)this.a).isItemChecked(n));
                        }
                    };
                }
                ((AdapterView)g).setOnItemClickListener((AdapterView$OnItemClickListener)onItemClickListener);
            }
            final AdapterView$OnItemSelectedListener o = this.O;
            if (o != null) {
                ((AdapterView)g).setOnItemSelectedListener(o);
            }
            Label_0286: {
                int choiceMode;
                if (this.H) {
                    choiceMode = 1;
                }
                else {
                    if (!this.G) {
                        break Label_0286;
                    }
                    choiceMode = 2;
                }
                ((AbsListView)g).setChoiceMode(choiceMode);
            }
            alertController.g = g;
        }
    }
    
    public static final class c extends Handler
    {
        public WeakReference a;
        
        public c(final DialogInterface referent) {
            this.a = new WeakReference((T)referent);
        }
        
        public void handleMessage(final Message message) {
            final int what = message.what;
            if (what != -3 && what != -2 && what != -1) {
                if (what == 1) {
                    ((DialogInterface)message.obj).dismiss();
                }
            }
            else {
                ((DialogInterface$OnClickListener)message.obj).onClick((DialogInterface)this.a.get(), message.what);
            }
        }
    }
    
    public static class d extends ArrayAdapter
    {
        public d(final Context context, final int n, final int n2, final CharSequence[] array) {
            super(context, n, n2, (Object[])array);
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public boolean hasStableIds() {
            return true;
        }
    }
}

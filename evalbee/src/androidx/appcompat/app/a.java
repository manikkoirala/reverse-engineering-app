// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.widget.AdapterView$OnItemSelectedListener;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnCancelListener;
import android.content.DialogInterface$OnMultiChoiceClickListener;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.database.Cursor;
import android.content.DialogInterface$OnClickListener;
import android.widget.ListAdapter;
import android.content.DialogInterface$OnKeyListener;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.os.Bundle;
import android.widget.ListView;
import android.util.TypedValue;
import android.content.Context;
import android.content.DialogInterface;

public class a extends q6 implements DialogInterface
{
    public final AlertController a;
    
    public a(final Context context, final int n) {
        super(context, e(context, n));
        this.a = new AlertController(this.getContext(), this, this.getWindow());
    }
    
    public static int e(final Context context, final int n) {
        if ((n >>> 24 & 0xFF) >= 1) {
            return n;
        }
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(sa1.l, typedValue, true);
        return typedValue.resourceId;
    }
    
    public ListView d() {
        return this.a.d();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.a.e();
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        return this.a.f(n, keyEvent) || super.onKeyDown(n, keyEvent);
    }
    
    public boolean onKeyUp(final int n, final KeyEvent keyEvent) {
        return this.a.g(n, keyEvent) || super.onKeyUp(n, keyEvent);
    }
    
    @Override
    public void setTitle(final CharSequence title) {
        super.setTitle(title);
        this.a.p(title);
    }
    
    public static class a
    {
        private final AlertController.b P;
        private final int mTheme;
        
        public a(final Context context) {
            this(context, androidx.appcompat.app.a.e(context, 0));
        }
        
        public a(final Context context, final int mTheme) {
            this.P = new AlertController.b((Context)new ContextThemeWrapper(context, androidx.appcompat.app.a.e(context, mTheme)));
            this.mTheme = mTheme;
        }
        
        public androidx.appcompat.app.a create() {
            final androidx.appcompat.app.a a = new androidx.appcompat.app.a(this.P.a, this.mTheme);
            this.P.a(a.a);
            a.setCancelable(this.P.r);
            if (this.P.r) {
                a.setCanceledOnTouchOutside(true);
            }
            a.setOnCancelListener(this.P.s);
            a.setOnDismissListener(this.P.t);
            final DialogInterface$OnKeyListener u = this.P.u;
            if (u != null) {
                a.setOnKeyListener(u);
            }
            return a;
        }
        
        public Context getContext() {
            return this.P.a;
        }
        
        public a setAdapter(final ListAdapter w, final DialogInterface$OnClickListener x) {
            final AlertController.b p2 = this.P;
            p2.w = w;
            p2.x = x;
            return this;
        }
        
        public a setCancelable(final boolean r) {
            this.P.r = r;
            return this;
        }
        
        public a setCursor(final Cursor k, final DialogInterface$OnClickListener x, final String l) {
            final AlertController.b p3 = this.P;
            p3.K = k;
            p3.L = l;
            p3.x = x;
            return this;
        }
        
        public a setCustomTitle(final View g) {
            this.P.g = g;
            return this;
        }
        
        public a setIcon(final int c) {
            this.P.c = c;
            return this;
        }
        
        public a setIcon(final Drawable d) {
            this.P.d = d;
            return this;
        }
        
        public a setIconAttribute(final int n) {
            final TypedValue typedValue = new TypedValue();
            this.P.a.getTheme().resolveAttribute(n, typedValue, true);
            this.P.c = typedValue.resourceId;
            return this;
        }
        
        @Deprecated
        public a setInverseBackgroundForced(final boolean n) {
            this.P.N = n;
            return this;
        }
        
        public a setItems(final int n, final DialogInterface$OnClickListener x) {
            final AlertController.b p2 = this.P;
            p2.v = p2.a.getResources().getTextArray(n);
            this.P.x = x;
            return this;
        }
        
        public a setItems(final CharSequence[] v, final DialogInterface$OnClickListener x) {
            final AlertController.b p2 = this.P;
            p2.v = v;
            p2.x = x;
            return this;
        }
        
        public a setMessage(final int n) {
            final AlertController.b p = this.P;
            p.h = p.a.getText(n);
            return this;
        }
        
        public a setMessage(final CharSequence h) {
            this.P.h = h;
            return this;
        }
        
        public a setMultiChoiceItems(final int n, final boolean[] f, final DialogInterface$OnMultiChoiceClickListener j) {
            final AlertController.b p3 = this.P;
            p3.v = p3.a.getResources().getTextArray(n);
            final AlertController.b p4 = this.P;
            p4.J = j;
            p4.F = f;
            p4.G = true;
            return this;
        }
        
        public a setMultiChoiceItems(final Cursor k, final String m, final String l, final DialogInterface$OnMultiChoiceClickListener j) {
            final AlertController.b p4 = this.P;
            p4.K = k;
            p4.J = j;
            p4.M = m;
            p4.L = l;
            p4.G = true;
            return this;
        }
        
        public a setMultiChoiceItems(final CharSequence[] v, final boolean[] f, final DialogInterface$OnMultiChoiceClickListener j) {
            final AlertController.b p3 = this.P;
            p3.v = v;
            p3.J = j;
            p3.F = f;
            p3.G = true;
            return this;
        }
        
        public a setNegativeButton(final int n, final DialogInterface$OnClickListener n2) {
            final AlertController.b p2 = this.P;
            p2.l = p2.a.getText(n);
            this.P.n = n2;
            return this;
        }
        
        public a setNegativeButton(final CharSequence l, final DialogInterface$OnClickListener n) {
            final AlertController.b p2 = this.P;
            p2.l = l;
            p2.n = n;
            return this;
        }
        
        public a setNegativeButtonIcon(final Drawable m) {
            this.P.m = m;
            return this;
        }
        
        public a setNeutralButton(final int n, final DialogInterface$OnClickListener q) {
            final AlertController.b p2 = this.P;
            p2.o = p2.a.getText(n);
            this.P.q = q;
            return this;
        }
        
        public a setNeutralButton(final CharSequence o, final DialogInterface$OnClickListener q) {
            final AlertController.b p2 = this.P;
            p2.o = o;
            p2.q = q;
            return this;
        }
        
        public a setNeutralButtonIcon(final Drawable p) {
            this.P.p = p;
            return this;
        }
        
        public a setOnCancelListener(final DialogInterface$OnCancelListener s) {
            this.P.s = s;
            return this;
        }
        
        public a setOnDismissListener(final DialogInterface$OnDismissListener t) {
            this.P.t = t;
            return this;
        }
        
        public a setOnItemSelectedListener(final AdapterView$OnItemSelectedListener o) {
            this.P.O = o;
            return this;
        }
        
        public a setOnKeyListener(final DialogInterface$OnKeyListener u) {
            this.P.u = u;
            return this;
        }
        
        public a setPositiveButton(final int n, final DialogInterface$OnClickListener k) {
            final AlertController.b p2 = this.P;
            p2.i = p2.a.getText(n);
            this.P.k = k;
            return this;
        }
        
        public a setPositiveButton(final CharSequence i, final DialogInterface$OnClickListener k) {
            final AlertController.b p2 = this.P;
            p2.i = i;
            p2.k = k;
            return this;
        }
        
        public a setPositiveButtonIcon(final Drawable j) {
            this.P.j = j;
            return this;
        }
        
        public a setRecycleOnMeasureEnabled(final boolean p) {
            this.P.P = p;
            return this;
        }
        
        public a setSingleChoiceItems(final int n, final int i, final DialogInterface$OnClickListener x) {
            final AlertController.b p3 = this.P;
            p3.v = p3.a.getResources().getTextArray(n);
            final AlertController.b p4 = this.P;
            p4.x = x;
            p4.I = i;
            p4.H = true;
            return this;
        }
        
        public a setSingleChoiceItems(final Cursor k, final int i, final String l, final DialogInterface$OnClickListener x) {
            final AlertController.b p4 = this.P;
            p4.K = k;
            p4.x = x;
            p4.I = i;
            p4.L = l;
            p4.H = true;
            return this;
        }
        
        public a setSingleChoiceItems(final ListAdapter w, final int i, final DialogInterface$OnClickListener x) {
            final AlertController.b p3 = this.P;
            p3.w = w;
            p3.x = x;
            p3.I = i;
            p3.H = true;
            return this;
        }
        
        public a setSingleChoiceItems(final CharSequence[] v, final int i, final DialogInterface$OnClickListener x) {
            final AlertController.b p3 = this.P;
            p3.v = v;
            p3.x = x;
            p3.I = i;
            p3.H = true;
            return this;
        }
        
        public a setTitle(final int n) {
            final AlertController.b p = this.P;
            p.f = p.a.getText(n);
            return this;
        }
        
        public a setTitle(final CharSequence f) {
            this.P.f = f;
            return this;
        }
        
        public a setView(final int y) {
            final AlertController.b p = this.P;
            p.z = null;
            p.y = y;
            p.E = false;
            return this;
        }
        
        public a setView(final View z) {
            final AlertController.b p = this.P;
            p.z = z;
            p.y = 0;
            p.E = false;
            return this;
        }
        
        @Deprecated
        public a setView(final View z, final int a, final int b, final int c, final int d) {
            final AlertController.b p5 = this.P;
            p5.z = z;
            p5.y = 0;
            p5.E = true;
            p5.A = a;
            p5.B = b;
            p5.C = c;
            p5.D = d;
            return this;
        }
        
        public androidx.appcompat.app.a show() {
            final androidx.appcompat.app.a create = this.create();
            create.show();
            return create;
        }
    }
}

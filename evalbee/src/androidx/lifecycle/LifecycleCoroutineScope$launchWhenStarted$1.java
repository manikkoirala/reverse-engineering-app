// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.lifecycle.LifecycleCoroutineScope$launchWhenStarted$1", f = "Lifecycle.kt", l = { 356 }, m = "invokeSuspend")
final class LifecycleCoroutineScope$launchWhenStarted$1 extends SuspendLambda implements q90
{
    final q90 $block;
    int label;
    final oj0 this$0;
    
    public LifecycleCoroutineScope$launchWhenStarted$1(final oj0 this$0, final q90 $block, final vl vl) {
        this.this$0 = this$0;
        this.$block = $block;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new LifecycleCoroutineScope$launchWhenStarted$1(this.this$0, this.$block, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((LifecycleCoroutineScope$launchWhenStarted$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        final Object d = gg0.d();
        final int label = this.label;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(o);
        }
        else {
            xe1.b(o);
            final Lifecycle i = this.this$0.i();
            final q90 $block = this.$block;
            this.label = 1;
            if (PausingDispatcherKt.c(i, $block, (vl)this) == d) {
                return d;
            }
        }
        return u02.a;
    }
}

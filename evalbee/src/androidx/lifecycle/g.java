// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.lang.ref.WeakReference;

public class g extends Lifecycle
{
    public static final a j;
    public final boolean b;
    public b00 c;
    public State d;
    public final WeakReference e;
    public int f;
    public boolean g;
    public boolean h;
    public ArrayList i;
    
    static {
        j = new a(null);
    }
    
    public g(final qj0 qj0) {
        fg0.e((Object)qj0, "provider");
        this(qj0, true);
    }
    
    public g(final qj0 referent, final boolean b) {
        this.b = b;
        this.c = new b00();
        this.d = State.INITIALIZED;
        this.i = new ArrayList();
        this.e = new WeakReference((T)referent);
    }
    
    @Override
    public void a(final pj0 pj0) {
        fg0.e((Object)pj0, "observer");
        this.f("addObserver");
        final State d = this.d;
        State state = State.DESTROYED;
        if (d != state) {
            state = State.INITIALIZED;
        }
        final b b = new b(pj0, state);
        if (this.c.l(pj0, b) != null) {
            return;
        }
        final qj0 qj0 = (qj0)this.e.get();
        if (qj0 == null) {
            return;
        }
        final boolean b2 = this.f != 0 || this.g;
        State o = this.e(pj0);
        ++this.f;
        while (b.b().compareTo(o) < 0 && this.c.contains(pj0)) {
            this.m(b.b());
            final Event c = Event.Companion.c(b.b());
            if (c == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("no event up from ");
                sb.append(b.b());
                throw new IllegalStateException(sb.toString());
            }
            b.a(qj0, c);
            this.l();
            o = this.e(pj0);
        }
        if (!b2) {
            this.o();
        }
        --this.f;
    }
    
    @Override
    public State b() {
        return this.d;
    }
    
    @Override
    public void c(final pj0 pj0) {
        fg0.e((Object)pj0, "observer");
        this.f("removeObserver");
        this.c.m(pj0);
    }
    
    public final void d(final qj0 qj0) {
        final Iterator descendingIterator = this.c.descendingIterator();
        fg0.d((Object)descendingIterator, "observerMap.descendingIterator()");
        while (descendingIterator.hasNext() && !this.h) {
            final Map.Entry<pj0, V> entry = descendingIterator.next();
            fg0.d((Object)entry, "next()");
            final pj0 pj0 = entry.getKey();
            final b b = (b)entry.getValue();
            while (b.b().compareTo(this.d) > 0 && !this.h && this.c.contains(pj0)) {
                final Event a = Event.Companion.a(b.b());
                if (a == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no event down from ");
                    sb.append(b.b());
                    throw new IllegalStateException(sb.toString());
                }
                this.m(a.getTargetState());
                b.a(qj0, a);
                this.l();
            }
        }
    }
    
    public final State e(final pj0 pj0) {
        final Map.Entry n = this.c.n(pj0);
        State state = null;
        State b2 = null;
        Label_0039: {
            if (n != null) {
                final b b = n.getValue();
                if (b != null) {
                    b2 = b.b();
                    break Label_0039;
                }
            }
            b2 = null;
        }
        if (this.i.isEmpty() ^ true) {
            final ArrayList i = this.i;
            state = (State)i.get(i.size() - 1);
        }
        final a j = androidx.lifecycle.g.j;
        return j.a(j.a(this.d, b2), state);
    }
    
    public final void f(final String str) {
        if (this.b && !b8.g().b()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Method ");
            sb.append(str);
            sb.append(" must be called on the main thread");
            throw new IllegalStateException(sb.toString().toString());
        }
    }
    
    public final void g(final qj0 qj0) {
        final si1.d c = this.c.c();
        fg0.d((Object)c, "observerMap.iteratorWithAdditions()");
        while (c.hasNext() && !this.h) {
            final Map.Entry<pj0, V> entry = c.next();
            final pj0 pj0 = entry.getKey();
            final b b = (b)entry.getValue();
            while (b.b().compareTo(this.d) < 0 && !this.h && this.c.contains(pj0)) {
                this.m(b.b());
                final Event c2 = Event.Companion.c(b.b());
                if (c2 == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no event up from ");
                    sb.append(b.b());
                    throw new IllegalStateException(sb.toString());
                }
                b.a(qj0, c2);
                this.l();
            }
        }
    }
    
    public void h(final Event event) {
        fg0.e((Object)event, "event");
        this.f("handleLifecycleEvent");
        this.k(event.getTargetState());
    }
    
    public final boolean i() {
        final int size = this.c.size();
        boolean b = true;
        if (size == 0) {
            return true;
        }
        final Map.Entry a = this.c.a();
        fg0.b((Object)a);
        final State b2 = a.getValue().b();
        final Map.Entry g = this.c.g();
        fg0.b((Object)g);
        final State b3 = g.getValue().b();
        if (b2 != b3 || this.d != b3) {
            b = false;
        }
        return b;
    }
    
    public void j(final State state) {
        fg0.e((Object)state, "state");
        this.f("markState");
        this.n(state);
    }
    
    public final void k(final State d) {
        final State d2 = this.d;
        if (d2 == d) {
            return;
        }
        if (d2 == State.INITIALIZED && d == State.DESTROYED) {
            final StringBuilder sb = new StringBuilder();
            sb.append("no event down from ");
            sb.append(this.d);
            sb.append(" in component ");
            sb.append(this.e.get());
            throw new IllegalStateException(sb.toString().toString());
        }
        this.d = d;
        if (!this.g && this.f == 0) {
            this.g = true;
            this.o();
            this.g = false;
            if (this.d == State.DESTROYED) {
                this.c = new b00();
            }
            return;
        }
        this.h = true;
    }
    
    public final void l() {
        final ArrayList i = this.i;
        i.remove(i.size() - 1);
    }
    
    public final void m(final State e) {
        this.i.add(e);
    }
    
    public void n(final State state) {
        fg0.e((Object)state, "state");
        this.f("setCurrentState");
        this.k(state);
    }
    
    public final void o() {
        final qj0 qj0 = (qj0)this.e.get();
        if (qj0 != null) {
            while (true) {
                final boolean i = this.i();
                this.h = false;
                if (i) {
                    break;
                }
                final State d = this.d;
                final Map.Entry a = this.c.a();
                fg0.b((Object)a);
                if (d.compareTo(a.getValue().b()) < 0) {
                    this.d(qj0);
                }
                final Map.Entry g = this.c.g();
                if (this.h || g == null || this.d.compareTo(g.getValue().b()) <= 0) {
                    continue;
                }
                this.g(qj0);
            }
            return;
        }
        throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is already garbage collected. It is too late to change lifecycle state.");
    }
    
    public static final class a
    {
        public final State a(final State o, final State state) {
            fg0.e((Object)o, "state1");
            State state2 = o;
            if (state != null) {
                state2 = o;
                if (state.compareTo(o) < 0) {
                    state2 = state;
                }
            }
            return state2;
        }
    }
    
    public static final class b
    {
        public State a;
        public f b;
        
        public b(final pj0 pj0, final State a) {
            fg0.e((Object)a, "initialState");
            fg0.b((Object)pj0);
            this.b = h.f(pj0);
            this.a = a;
        }
        
        public final void a(final qj0 qj0, final Event event) {
            fg0.e((Object)event, "event");
            final State targetState = event.getTargetState();
            this.a = androidx.lifecycle.g.j.a(this.a, targetState);
            final f b = this.b;
            fg0.b((Object)qj0);
            b.d(qj0, event);
            this.a = targetState;
        }
        
        public final State b() {
            return this.a;
        }
    }
}

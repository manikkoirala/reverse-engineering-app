// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

public final class CompositeGeneratedAdaptersObserver implements f
{
    public final b[] a;
    
    public CompositeGeneratedAdaptersObserver(final b[] a) {
        fg0.e((Object)a, "generatedAdapters");
        this.a = a;
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)qj0, "source");
        fg0.e((Object)event, "event");
        new gw0();
        final b[] a = this.a;
        if (a.length > 0) {
            final b b = a[0];
            throw null;
        }
        if (a.length <= 0) {
            return;
        }
        final b b2 = a[0];
        throw null;
    }
}

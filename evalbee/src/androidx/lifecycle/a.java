// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public final class a
{
    public static a c;
    public final Map a;
    public final Map b;
    
    static {
        a.c = new a();
    }
    
    public a() {
        this.a = new HashMap();
        this.b = new HashMap();
    }
    
    public final a a(final Class clazz, Method[] b) {
        final Class superclass = clazz.getSuperclass();
        final HashMap hashMap = new HashMap();
        if (superclass != null) {
            final a c = this.c(superclass);
            if (c != null) {
                hashMap.putAll(c.b);
            }
        }
        final Class[] interfaces = clazz.getInterfaces();
        for (int length = interfaces.length, i = 0; i < length; ++i) {
            for (final Map.Entry<b, V> entry : this.c(interfaces[i]).b.entrySet()) {
                this.e(hashMap, entry.getKey(), (Lifecycle.Event)entry.getValue(), clazz);
            }
        }
        if (b == null) {
            b = this.b(clazz);
        }
        final int length2 = b.length;
        int j = 0;
        boolean b2 = false;
        while (j < length2) {
            final Method method = b[j];
            final i k = method.getAnnotation(i.class);
            if (k != null) {
                final Class<?>[] parameterTypes = method.getParameterTypes();
                int n;
                if (parameterTypes.length > 0) {
                    if (!qj0.class.isAssignableFrom(parameterTypes[0])) {
                        throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                    }
                    n = 1;
                }
                else {
                    n = 0;
                }
                final Lifecycle.Event value = k.value();
                if (parameterTypes.length > 1) {
                    if (!Lifecycle.Event.class.isAssignableFrom(parameterTypes[1])) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    }
                    if (value != Lifecycle.Event.ON_ANY) {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                    n = 2;
                }
                if (parameterTypes.length > 2) {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
                this.e(hashMap, new b(n, method), value, clazz);
                b2 = true;
            }
            ++j;
        }
        final a a = new a(hashMap);
        this.a.put(clazz, a);
        this.b.put(clazz, b2);
        return a;
    }
    
    public final Method[] b(final Class clazz) {
        try {
            return clazz.getDeclaredMethods();
        }
        catch (final NoClassDefFoundError cause) {
            throw new IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", cause);
        }
    }
    
    public a c(final Class clazz) {
        final a a = this.a.get(clazz);
        if (a != null) {
            return a;
        }
        return this.a(clazz, null);
    }
    
    public boolean d(final Class clazz) {
        final Boolean b = this.b.get(clazz);
        if (b != null) {
            return b;
        }
        final Method[] b2 = this.b(clazz);
        for (int length = b2.length, i = 0; i < length; ++i) {
            if (b2[i].getAnnotation(i.class) != null) {
                this.a(clazz, b2);
                return true;
            }
        }
        this.b.put(clazz, Boolean.FALSE);
        return false;
    }
    
    public final void e(final Map map, final b b, final Lifecycle.Event obj, final Class clazz) {
        final Lifecycle.Event obj2 = map.get(b);
        if (obj2 != null && obj != obj2) {
            final Method b2 = b.b;
            final StringBuilder sb = new StringBuilder();
            sb.append("Method ");
            sb.append(b2.getName());
            sb.append(" in ");
            sb.append(clazz.getName());
            sb.append(" already declared with different @OnLifecycleEvent value: previous value ");
            sb.append(obj2);
            sb.append(", new value ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (obj2 == null) {
            map.put(b, obj);
        }
    }
    
    public static class a
    {
        public final Map a;
        public final Map b;
        
        public a(final Map b) {
            this.b = b;
            this.a = new HashMap();
            for (final Map.Entry<K, Lifecycle.Event> entry : b.entrySet()) {
                final Lifecycle.Event event = entry.getValue();
                List list;
                if ((list = this.a.get(event)) == null) {
                    list = new ArrayList();
                    this.a.put(event, list);
                }
                list.add(entry.getKey());
            }
        }
        
        public static void b(final List list, final qj0 qj0, final Lifecycle.Event event, final Object o) {
            if (list != null) {
                for (int i = list.size() - 1; i >= 0; --i) {
                    ((b)list.get(i)).a(qj0, event, o);
                }
            }
        }
        
        public void a(final qj0 qj0, final Lifecycle.Event event, final Object o) {
            b(this.a.get(event), qj0, event, o);
            b(this.a.get(Lifecycle.Event.ON_ANY), qj0, event, o);
        }
    }
    
    public static final class b
    {
        public final int a;
        public final Method b;
        
        public b(final int a, final Method b) {
            this.a = a;
            (this.b = b).setAccessible(true);
        }
        
        public void a(final qj0 qj0, final Lifecycle.Event event, final Object obj) {
            try {
                final int a = this.a;
                if (a != 0) {
                    if (a != 1) {
                        if (a == 2) {
                            this.b.invoke(obj, qj0, event);
                        }
                    }
                    else {
                        this.b.invoke(obj, qj0);
                    }
                }
                else {
                    this.b.invoke(obj, new Object[0]);
                }
            }
            catch (final IllegalAccessException cause) {
                throw new RuntimeException(cause);
            }
            catch (final InvocationTargetException ex) {
                throw new RuntimeException("Failed to call observer method", ex.getCause());
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof b)) {
                return false;
            }
            final b b2 = (b)o;
            if (this.a != b2.a || !this.b.getName().equals(b2.b.getName())) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            return this.a * 31 + this.b.getName().hashCode();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n$a;
import kotlinx.coroutines.n;

public final class d
{
    public final Lifecycle a;
    public final Lifecycle.State b;
    public final lt c;
    public final f d;
    
    public d(final Lifecycle a, final Lifecycle.State b, final lt c, final n n) {
        fg0.e((Object)a, "lifecycle");
        fg0.e((Object)b, "minState");
        fg0.e((Object)c, "dispatchQueue");
        fg0.e((Object)n, "parentJob");
        this.a = a;
        this.b = b;
        this.c = c;
        final nj0 d = new nj0(this, n);
        this.d = d;
        if (a.b() == Lifecycle.State.DESTROYED) {
            n$a.a(n, (CancellationException)null, 1, (Object)null);
            this.b();
        }
        else {
            a.a(d);
        }
    }
    
    public static final void c(final d d, final n n, final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)d, "this$0");
        fg0.e((Object)n, "$parentJob");
        fg0.e((Object)qj0, "source");
        fg0.e((Object)event, "<anonymous parameter 1>");
        if (qj0.getLifecycle().b() == Lifecycle.State.DESTROYED) {
            n$a.a(n, (CancellationException)null, 1, (Object)null);
            d.b();
        }
        else {
            final int compareTo = qj0.getLifecycle().b().compareTo(d.b);
            final lt c = d.c;
            if (compareTo < 0) {
                c.h();
            }
            else {
                c.i();
            }
        }
    }
    
    public final void b() {
        this.a.c(this.d);
        this.c.g();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.jvm.internal.Lambda;

final class ViewTreeViewModelStoreOwner$findViewTreeViewModelStoreOwner$2 extends Lambda implements c90
{
    public static final ViewTreeViewModelStoreOwner$findViewTreeViewModelStoreOwner$2 INSTANCE;
    
    static {
        INSTANCE = new ViewTreeViewModelStoreOwner$findViewTreeViewModelStoreOwner$2();
    }
    
    public ViewTreeViewModelStoreOwner$findViewTreeViewModelStoreOwner$2() {
        super(1);
    }
    
    @Nullable
    public final c42 invoke(@NotNull final View view) {
        fg0.e((Object)view, "view");
        final Object tag = view.getTag(ib1.a);
        c42 c42;
        if (tag instanceof c42) {
            c42 = (c42)tag;
        }
        else {
            c42 = null;
        }
        return c42;
    }
}

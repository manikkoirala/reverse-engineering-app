// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import org.jetbrains.annotations.Nullable;
import android.os.Bundle;
import org.jetbrains.annotations.NotNull;
import android.app.Activity;
import android.app.Application$ActivityLifecycleCallbacks;
import android.app.Application;
import android.content.Context;
import java.util.concurrent.atomic.AtomicBoolean;

public final class e
{
    public static final e a;
    public static final AtomicBoolean b;
    
    static {
        a = new e();
        b = new AtomicBoolean(false);
    }
    
    public static final void a(Context applicationContext) {
        fg0.e((Object)applicationContext, "context");
        if (e.b.getAndSet(true)) {
            return;
        }
        applicationContext = applicationContext.getApplicationContext();
        fg0.c((Object)applicationContext, "null cannot be cast to non-null type android.app.Application");
        ((Application)applicationContext).registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)new a());
    }
    
    public static final class a extends tw
    {
        @Override
        public void onActivityCreated(@NotNull final Activity activity, @Nullable final Bundle bundle) {
            fg0.e((Object)activity, "activity");
            k.b.c(activity);
        }
    }
}

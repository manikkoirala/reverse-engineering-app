// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.jvm.internal.Lambda;

final class ViewTreeLifecycleOwner$findViewTreeLifecycleOwner$2 extends Lambda implements c90
{
    public static final ViewTreeLifecycleOwner$findViewTreeLifecycleOwner$2 INSTANCE;
    
    static {
        INSTANCE = new ViewTreeLifecycleOwner$findViewTreeLifecycleOwner$2();
    }
    
    public ViewTreeLifecycleOwner$findViewTreeLifecycleOwner$2() {
        super(1);
    }
    
    @Nullable
    public final qj0 invoke(@NotNull final View view) {
        fg0.e((Object)view, "viewParent");
        final Object tag = view.getTag(hb1.a);
        qj0 qj0;
        if (tag instanceof qj0) {
            qj0 = (qj0)tag;
        }
        else {
            qj0 = null;
        }
        return qj0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import android.os.BaseBundle;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import kotlin.Pair;
import kotlin.collections.b;
import java.util.LinkedHashMap;
import android.util.SizeF;
import android.util.Size;
import android.util.SparseArray;
import java.io.Serializable;
import android.os.Parcelable;
import java.util.ArrayList;
import android.os.Bundle;
import android.os.Binder;
import androidx.savedstate.a;
import java.util.Map;

public final class l
{
    public static final a f;
    public static final Class[] g;
    public final Map a;
    public final Map b;
    public final Map c;
    public final Map d;
    public final androidx.savedstate.a.c e;
    
    static {
        f = new a(null);
        g = new Class[] { Boolean.TYPE, boolean[].class, Double.TYPE, double[].class, Integer.TYPE, int[].class, Long.TYPE, long[].class, String.class, String[].class, Binder.class, Bundle.class, Byte.TYPE, byte[].class, Character.TYPE, char[].class, CharSequence.class, CharSequence[].class, ArrayList.class, Float.TYPE, float[].class, Parcelable.class, Parcelable[].class, Serializable.class, Short.TYPE, short[].class, SparseArray.class, Size.class, SizeF.class };
    }
    
    public l() {
        this.a = new LinkedHashMap();
        this.b = new LinkedHashMap();
        this.c = new LinkedHashMap();
        this.d = new LinkedHashMap();
        this.e = new wi1(this);
    }
    
    public l(final Map map) {
        fg0.e((Object)map, "initialState");
        final LinkedHashMap a = new LinkedHashMap();
        this.a = a;
        this.b = new LinkedHashMap();
        this.c = new LinkedHashMap();
        this.d = new LinkedHashMap();
        this.e = new wi1(this);
        a.putAll(map);
    }
    
    public static final /* synthetic */ Class[] b() {
        return l.g;
    }
    
    public static final Bundle d(final l l) {
        fg0.e((Object)l, "this$0");
        for (final Map.Entry<String, V> entry : b.r(l.b).entrySet()) {
            l.e(entry.getKey(), ((androidx.savedstate.a.c)entry.getValue()).a());
        }
        final Set keySet = l.a.keySet();
        final ArrayList list = new ArrayList(keySet.size());
        final ArrayList list2 = new ArrayList<Object>(list.size());
        for (final String e : keySet) {
            list.add((Object)e);
            list2.add(l.a.get(e));
        }
        return id.a(dz1.a((Object)"keys", (Object)list), dz1.a((Object)"values", (Object)list2));
    }
    
    public final androidx.savedstate.a.c c() {
        return this.e;
    }
    
    public final void e(final String s, final Object value) {
        fg0.e((Object)s, "key");
        if (l.f.b(value)) {
            final tx0 value2 = this.c.get(s);
            tx0 tx0;
            if (value2 instanceof tx0) {
                tx0 = value2;
            }
            else {
                tx0 = null;
            }
            if (tx0 != null) {
                tx0.n(value);
            }
            else {
                this.a.put(s, value);
            }
            final vx0 vx0 = this.d.get(s);
            if (vx0 != null) {
                vx0.setValue(value);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't put value with type ");
        fg0.b(value);
        sb.append(value.getClass());
        sb.append(" into saved state");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static final class a
    {
        public final l a(final Bundle bundle, final Bundle bundle2) {
            if (bundle == null) {
                l l;
                if (bundle2 == null) {
                    l = new l();
                }
                else {
                    final HashMap hashMap = new HashMap();
                    for (final String s : ((BaseBundle)bundle2).keySet()) {
                        fg0.d((Object)s, "key");
                        hashMap.put(s, ((BaseBundle)bundle2).get(s));
                    }
                    l = new l(hashMap);
                }
                return l;
            }
            final ArrayList parcelableArrayList = bundle.getParcelableArrayList("keys");
            final ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("values");
            final int n = 0;
            if (parcelableArrayList != null && parcelableArrayList2 != null && parcelableArrayList.size() == parcelableArrayList2.size()) {
                final LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (int size = parcelableArrayList.size(), i = n; i < size; ++i) {
                    final Object value = parcelableArrayList.get(i);
                    fg0.c(value, "null cannot be cast to non-null type kotlin.String");
                    linkedHashMap.put(value, parcelableArrayList2.get(i));
                }
                return new l(linkedHashMap);
            }
            throw new IllegalStateException("Invalid bundle passed as restored state".toString());
        }
        
        public final boolean b(final Object o) {
            if (o == null) {
                return true;
            }
            for (final Class clazz : l.b()) {
                fg0.b((Object)clazz);
                if (clazz.isInstance(o)) {
                    return true;
                }
            }
            return false;
        }
    }
}

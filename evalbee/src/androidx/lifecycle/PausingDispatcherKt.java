// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.CoroutineContext;

public abstract class PausingDispatcherKt
{
    public static final Object a(final Lifecycle lifecycle, final q90 q90, final vl vl) {
        return d(lifecycle, Lifecycle.State.CREATED, q90, vl);
    }
    
    public static final Object b(final Lifecycle lifecycle, final q90 q90, final vl vl) {
        return d(lifecycle, Lifecycle.State.RESUMED, q90, vl);
    }
    
    public static final Object c(final Lifecycle lifecycle, final q90 q90, final vl vl) {
        return d(lifecycle, Lifecycle.State.STARTED, q90, vl);
    }
    
    public static final Object d(final Lifecycle lifecycle, final Lifecycle.State state, final q90 q90, final vl vl) {
        return ad.g((CoroutineContext)pt.c().s0(), (q90)new PausingDispatcherKt$whenStateAtLeast.PausingDispatcherKt$whenStateAtLeast$2(lifecycle, state, q90, (vl)null), vl);
    }
}

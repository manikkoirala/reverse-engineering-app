// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.savedstate.a;

public final class SavedStateHandleController implements f
{
    public final String a;
    public final l b;
    public boolean c;
    
    public SavedStateHandleController(final String a, final l b) {
        fg0.e((Object)a, "key");
        fg0.e((Object)b, "handle");
        this.a = a;
        this.b = b;
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)qj0, "source");
        fg0.e((Object)event, "event");
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.c = false;
            qj0.getLifecycle().c(this);
        }
    }
    
    public final void f(final a a, final Lifecycle lifecycle) {
        fg0.e((Object)a, "registry");
        fg0.e((Object)lifecycle, "lifecycle");
        if (this.c ^ true) {
            this.c = true;
            lifecycle.a(this);
            a.h(this.a, this.b.c());
            return;
        }
        throw new IllegalStateException("Already attached to lifecycleOwner".toString());
    }
    
    public final l i() {
        return this.b;
    }
    
    public final boolean j() {
        return this.c;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import android.os.BaseBundle;
import java.util.Iterator;
import java.util.Map;
import android.os.Bundle;
import androidx.savedstate.a;

public final class SavedStateHandlesProvider implements c
{
    public final a a;
    public boolean b;
    public Bundle c;
    public final xi0 d;
    
    public SavedStateHandlesProvider(final a a, final c42 c42) {
        fg0.e((Object)a, "savedStateRegistry");
        fg0.e((Object)c42, "viewModelStoreOwner");
        this.a = a;
        this.d = kotlin.a.a((a90)new SavedStateHandlesProvider$viewModel.SavedStateHandlesProvider$viewModel$2(c42));
    }
    
    @Override
    public Bundle a() {
        final Bundle bundle = new Bundle();
        final Bundle c = this.c;
        if (c != null) {
            bundle.putAll(c);
        }
        for (final Map.Entry<String, V> entry : this.c().f().entrySet()) {
            final String s = entry.getKey();
            final Bundle a = ((l)entry.getValue()).c().a();
            if (!fg0.a((Object)a, (Object)Bundle.EMPTY)) {
                bundle.putBundle(s, a);
            }
        }
        this.b = false;
        return bundle;
    }
    
    public final Bundle b(final String s) {
        fg0.e((Object)s, "key");
        this.d();
        final Bundle c = this.c;
        Bundle bundle;
        if (c != null) {
            bundle = c.getBundle(s);
        }
        else {
            bundle = null;
        }
        final Bundle c2 = this.c;
        if (c2 != null) {
            c2.remove(s);
        }
        final Bundle c3 = this.c;
        int n = 0;
        if (c3 != null) {
            n = n;
            if (((BaseBundle)c3).isEmpty()) {
                n = 1;
            }
        }
        if (n != 0) {
            this.c = null;
        }
        return bundle;
    }
    
    public final xi1 c() {
        return (xi1)this.d.getValue();
    }
    
    public final void d() {
        if (!this.b) {
            this.c = this.a.b("androidx.lifecycle.internal.SavedStateHandlesProvider");
            this.b = true;
            this.c();
        }
    }
}

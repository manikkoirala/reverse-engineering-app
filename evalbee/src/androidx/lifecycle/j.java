// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import android.app.Application$ActivityLifecycleCallbacks;
import android.os.Build$VERSION;
import org.jetbrains.annotations.Nullable;
import android.os.Bundle;
import org.jetbrains.annotations.NotNull;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Handler;

public final class j implements qj0
{
    public static final b i;
    public static final j j;
    public int a;
    public int b;
    public boolean c;
    public boolean d;
    public Handler e;
    public final g f;
    public final Runnable g;
    public final k.a h;
    
    static {
        i = new b(null);
        j = new j();
    }
    
    public j() {
        this.c = true;
        this.d = true;
        this.f = new g(this);
        this.g = new j81(this);
        this.h = new k.a(this) {
            public final j a;
            
            @Override
            public void a() {
            }
            
            @Override
            public void onResume() {
                this.a.e();
            }
            
            @Override
            public void onStart() {
                this.a.f();
            }
        };
    }
    
    public static final /* synthetic */ k.a b(final j j) {
        return j.h;
    }
    
    public static final /* synthetic */ j c() {
        return androidx.lifecycle.j.j;
    }
    
    public static final void i(final j j) {
        fg0.e((Object)j, "this$0");
        j.j();
        j.k();
    }
    
    public final void d() {
        final int b = this.b - 1;
        this.b = b;
        if (b == 0) {
            final Handler e = this.e;
            fg0.b((Object)e);
            e.postDelayed(this.g, 700L);
        }
    }
    
    public final void e() {
        final int b = this.b + 1;
        this.b = b;
        if (b == 1) {
            if (this.c) {
                this.f.h(Lifecycle.Event.ON_RESUME);
                this.c = false;
            }
            else {
                final Handler e = this.e;
                fg0.b((Object)e);
                e.removeCallbacks(this.g);
            }
        }
    }
    
    public final void f() {
        final int a = this.a + 1;
        this.a = a;
        if (a == 1 && this.d) {
            this.f.h(Lifecycle.Event.ON_START);
            this.d = false;
        }
    }
    
    public final void g() {
        --this.a;
        this.k();
    }
    
    @Override
    public Lifecycle getLifecycle() {
        return this.f;
    }
    
    public final void h(Context applicationContext) {
        fg0.e((Object)applicationContext, "context");
        this.e = new Handler();
        this.f.h(Lifecycle.Event.ON_CREATE);
        applicationContext = applicationContext.getApplicationContext();
        fg0.c((Object)applicationContext, "null cannot be cast to non-null type android.app.Application");
        ((Application)applicationContext).registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)new tw(this) {
            final j this$0;
            
            @Override
            public void onActivityCreated(@NotNull final Activity activity, @Nullable final Bundle bundle) {
                fg0.e((Object)activity, "activity");
                if (Build$VERSION.SDK_INT < 29) {
                    k.b.b(activity).f(androidx.lifecycle.j.b(this.this$0));
                }
            }
            
            @Override
            public void onActivityPaused(@NotNull final Activity activity) {
                fg0.e((Object)activity, "activity");
                this.this$0.d();
            }
            
            public void onActivityPreCreated(@NotNull final Activity activity, @Nullable final Bundle bundle) {
                fg0.e((Object)activity, "activity");
                androidx.lifecycle.j.a.a(activity, (Application$ActivityLifecycleCallbacks)new tw(this.this$0) {
                    final j this$0;
                    
                    public void onActivityPostResumed(@NotNull final Activity activity) {
                        fg0.e((Object)activity, "activity");
                        this.this$0.e();
                    }
                    
                    public void onActivityPostStarted(@NotNull final Activity activity) {
                        fg0.e((Object)activity, "activity");
                        this.this$0.f();
                    }
                });
            }
            
            @Override
            public void onActivityStopped(@NotNull final Activity activity) {
                fg0.e((Object)activity, "activity");
                this.this$0.g();
            }
        });
    }
    
    public final void j() {
        if (this.b == 0) {
            this.c = true;
            this.f.h(Lifecycle.Event.ON_PAUSE);
        }
    }
    
    public final void k() {
        if (this.a == 0 && this.c) {
            this.f.h(Lifecycle.Event.ON_STOP);
            this.d = true;
        }
    }
    
    public static final class a
    {
        public static final a a;
        
        static {
            a = new a();
        }
        
        public static final void a(@NotNull final Activity activity, @NotNull final Application$ActivityLifecycleCallbacks application$ActivityLifecycleCallbacks) {
            fg0.e((Object)activity, "activity");
            fg0.e((Object)application$ActivityLifecycleCallbacks, "callback");
            activity.registerActivityLifecycleCallbacks(application$ActivityLifecycleCallbacks);
        }
    }
    
    public static final class b
    {
        public final qj0 a() {
            return androidx.lifecycle.j.c();
        }
        
        public final void b(final Context context) {
            fg0.e((Object)context, "context");
            androidx.lifecycle.j.c().h(context);
        }
    }
}

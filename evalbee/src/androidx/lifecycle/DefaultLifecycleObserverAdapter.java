// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

public final class DefaultLifecycleObserverAdapter implements f
{
    public final vq a;
    public final f b;
    
    public DefaultLifecycleObserverAdapter(final vq a, final f b) {
        fg0.e((Object)a, "defaultLifecycleObserver");
        this.a = a;
        this.b = b;
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)qj0, "source");
        fg0.e((Object)event, "event");
        switch (DefaultLifecycleObserverAdapter.a.a[event.ordinal()]) {
            case 7: {
                throw new IllegalArgumentException("ON_ANY must not been send by anybody");
            }
            case 6: {
                this.a.a(qj0);
                break;
            }
            case 5: {
                this.a.g(qj0);
                break;
            }
            case 4: {
                this.a.c(qj0);
                break;
            }
            case 3: {
                this.a.b(qj0);
                break;
            }
            case 2: {
                this.a.e(qj0);
                break;
            }
            case 1: {
                this.a.h(qj0);
                break;
            }
        }
        final f b = this.b;
        if (b != null) {
            b.d(qj0, event);
        }
    }
}

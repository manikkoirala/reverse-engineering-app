// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.reflect.Constructor;
import java.util.List;
import androidx.savedstate.a;
import android.os.Bundle;
import android.app.Application;

public final class m extends d implements b
{
    public Application b;
    public final b c;
    public Bundle d;
    public Lifecycle e;
    public a f;
    
    public m(final Application b, final aj1 aj1, final Bundle d) {
        fg0.e((Object)aj1, "owner");
        this.f = aj1.getSavedStateRegistry();
        this.e = aj1.getLifecycle();
        this.d = d;
        this.b = b;
        c a;
        if (b != null) {
            a = o.a.f.a(b);
        }
        else {
            a = new o.a();
        }
        this.c = a;
    }
    
    @Override
    public y32 a(final Class clazz, final do do1) {
        fg0.e((Object)clazz, "modelClass");
        fg0.e((Object)do1, "extras");
        final String s = (String)do1.a(o.c.d);
        if (s != null) {
            y32 y32;
            if (do1.a(SavedStateHandleSupport.a) != null && do1.a(SavedStateHandleSupport.b) != null) {
                final Application application = (Application)do1.a(o.a.h);
                final boolean assignable = x4.class.isAssignableFrom(clazz);
                List list;
                if (assignable && application != null) {
                    list = bj1.a();
                }
                else {
                    list = bj1.b();
                }
                final Constructor c = bj1.c(clazz, list);
                if (c == null) {
                    return this.c.a(clazz, do1);
                }
                if (assignable && application != null) {
                    y32 = bj1.d(clazz, c, application, SavedStateHandleSupport.a(do1));
                }
                else {
                    y32 = bj1.d(clazz, c, SavedStateHandleSupport.a(do1));
                }
            }
            else {
                if (this.e == null) {
                    throw new IllegalStateException("SAVED_STATE_REGISTRY_OWNER_KEY andVIEW_MODEL_STORE_OWNER_KEY must be provided in the creation extras tosuccessfully create a ViewModel.");
                }
                y32 = this.d(s, clazz);
            }
            return y32;
        }
        throw new IllegalStateException("VIEW_MODEL_KEY must always be provided by ViewModelProvider");
    }
    
    @Override
    public y32 b(final Class clazz) {
        fg0.e((Object)clazz, "modelClass");
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            return this.d(canonicalName, clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    @Override
    public void c(final y32 y32) {
        fg0.e((Object)y32, "viewModel");
        if (this.e != null) {
            final a f = this.f;
            fg0.b((Object)f);
            final Lifecycle e = this.e;
            fg0.b((Object)e);
            LegacySavedStateHandleController.a(y32, f, e);
        }
    }
    
    public final y32 d(final String s, final Class clazz) {
        fg0.e((Object)s, "key");
        fg0.e((Object)clazz, "modelClass");
        final Lifecycle e = this.e;
        if (e == null) {
            throw new UnsupportedOperationException("SavedStateViewModelFactory constructed with empty constructor supports only calls to create(modelClass: Class<T>, extras: CreationExtras).");
        }
        final boolean assignable = x4.class.isAssignableFrom(clazz);
        List list;
        if (assignable && this.b != null) {
            list = bj1.a();
        }
        else {
            list = bj1.b();
        }
        final Constructor c = bj1.c(clazz, list);
        if (c == null) {
            y32 y32;
            if (this.b != null) {
                y32 = this.c.b(clazz);
            }
            else {
                y32 = o.c.b.a().b(clazz);
            }
            return y32;
        }
        final a f = this.f;
        fg0.b((Object)f);
        final SavedStateHandleController b = LegacySavedStateHandleController.b(f, e, s, this.d);
        y32 y33 = null;
        Label_0188: {
            if (assignable) {
                final Application b2 = this.b;
                if (b2 != null) {
                    fg0.b((Object)b2);
                    y33 = bj1.d(clazz, c, b2, b.i());
                    break Label_0188;
                }
            }
            y33 = bj1.d(clazz, c, b.i());
        }
        y33.e("androidx.lifecycle.savedstate.vm.tag", b);
        return y33;
    }
}

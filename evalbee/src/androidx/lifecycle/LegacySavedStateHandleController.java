// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Iterator;
import java.util.Collection;
import android.os.Bundle;
import androidx.savedstate.a;

public final class LegacySavedStateHandleController
{
    public static final LegacySavedStateHandleController a;
    
    static {
        a = new LegacySavedStateHandleController();
    }
    
    public static final void a(final y32 y32, final androidx.savedstate.a a, final Lifecycle lifecycle) {
        fg0.e((Object)y32, "viewModel");
        fg0.e((Object)a, "registry");
        fg0.e((Object)lifecycle, "lifecycle");
        final SavedStateHandleController savedStateHandleController = (SavedStateHandleController)y32.c("androidx.lifecycle.savedstate.vm.tag");
        if (savedStateHandleController != null && !savedStateHandleController.j()) {
            savedStateHandleController.f(a, lifecycle);
            LegacySavedStateHandleController.a.c(a, lifecycle);
        }
    }
    
    public static final SavedStateHandleController b(final androidx.savedstate.a a, final Lifecycle lifecycle, final String s, final Bundle bundle) {
        fg0.e((Object)a, "registry");
        fg0.e((Object)lifecycle, "lifecycle");
        fg0.b((Object)s);
        final SavedStateHandleController savedStateHandleController = new SavedStateHandleController(s, l.f.a(a.b(s), bundle));
        savedStateHandleController.f(a, lifecycle);
        LegacySavedStateHandleController.a.c(a, lifecycle);
        return savedStateHandleController;
    }
    
    public final void c(final androidx.savedstate.a a, final Lifecycle lifecycle) {
        final Lifecycle.State b = lifecycle.b();
        if (b != Lifecycle.State.INITIALIZED && !b.isAtLeast(Lifecycle.State.STARTED)) {
            lifecycle.a((pj0)new LegacySavedStateHandleController$tryToAddRecreator.LegacySavedStateHandleController$tryToAddRecreator$1(lifecycle, a));
        }
        else {
            a.i(a.class);
        }
    }
    
    public static final class a implements androidx.savedstate.a.a
    {
        @Override
        public void a(final aj1 aj1) {
            fg0.e((Object)aj1, "owner");
            if (aj1 instanceof c42) {
                final b42 viewModelStore = ((c42)aj1).getViewModelStore();
                final androidx.savedstate.a savedStateRegistry = aj1.getSavedStateRegistry();
                final Iterator iterator = viewModelStore.c().iterator();
                while (iterator.hasNext()) {
                    final y32 b = viewModelStore.b((String)iterator.next());
                    fg0.b((Object)b);
                    LegacySavedStateHandleController.a(b, savedStateRegistry, aj1.getLifecycle());
                }
                if (viewModelStore.c().isEmpty() ^ true) {
                    savedStateRegistry.i(a.class);
                }
                return;
            }
            throw new IllegalStateException("Internal error: OnRecreation should be registered only on components that implement ViewModelStoreOwner".toString());
        }
    }
}

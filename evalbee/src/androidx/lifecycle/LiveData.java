// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Map;

public abstract class LiveData
{
    public static final Object k;
    public final Object a;
    public si1 b;
    public int c;
    public boolean d;
    public volatile Object e;
    public volatile Object f;
    public int g;
    public boolean h;
    public boolean i;
    public final Runnable j;
    
    static {
        k = new Object();
    }
    
    public LiveData() {
        this.a = new Object();
        this.b = new si1();
        this.c = 0;
        final Object k = LiveData.k;
        this.f = k;
        this.j = new Runnable(this) {
            public final LiveData a;
            
            @Override
            public void run() {
                synchronized (this.a.a) {
                    final Object f = this.a.f;
                    this.a.f = LiveData.k;
                    monitorexit(this.a.a);
                    this.a.n(f);
                }
            }
        };
        this.e = k;
        this.g = -1;
    }
    
    public static void b(final String str) {
        if (b8.g().b()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot invoke ");
        sb.append(str);
        sb.append(" on a background thread");
        throw new IllegalStateException(sb.toString());
    }
    
    public void c(int n) {
        int c = this.c;
        this.c = n + c;
        if (this.d) {
            return;
        }
        this.d = true;
        try {
            while (true) {
                final int c2 = this.c;
                if (c == c2) {
                    break;
                }
                if (c == 0 && c2 > 0) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                final boolean b = c > 0 && c2 == 0;
                if (n != 0) {
                    this.j();
                }
                else if (b) {
                    this.k();
                }
                c = c2;
            }
        }
        finally {
            this.d = false;
        }
    }
    
    public final void d(final c c) {
        if (!c.b) {
            return;
        }
        if (!c.k()) {
            c.f(false);
            return;
        }
        final int c2 = c.c;
        final int g = this.g;
        if (c2 >= g) {
            return;
        }
        c.c = g;
        c.a.a(this.e);
    }
    
    public void e(c c) {
        if (this.h) {
            this.i = true;
            return;
        }
        this.h = true;
        do {
            this.i = false;
            c c2 = null;
            Label_0086: {
                if (c != null) {
                    this.d(c);
                    c2 = null;
                }
                else {
                    final si1.d c3 = this.b.c();
                    do {
                        c2 = c;
                        if (!c3.hasNext()) {
                            break Label_0086;
                        }
                        this.d(((Map.Entry<K, c>)c3.next()).getValue());
                    } while (!this.i);
                    c2 = c;
                }
            }
            c = c2;
        } while (this.i);
        this.h = false;
    }
    
    public Object f() {
        final Object e = this.e;
        if (e != LiveData.k) {
            return e;
        }
        return null;
    }
    
    public boolean g() {
        return this.c > 0;
    }
    
    public void h(final qj0 qj0, final d11 d11) {
        b("observe");
        if (qj0.getLifecycle().b() == Lifecycle.State.DESTROYED) {
            return;
        }
        final LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(qj0, d11);
        final c c = (c)this.b.l(d11, lifecycleBoundObserver);
        if (c != null && !c.j(qj0)) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (c != null) {
            return;
        }
        qj0.getLifecycle().a(lifecycleBoundObserver);
    }
    
    public void i(final d11 d11) {
        b("observeForever");
        final b b = new b(d11);
        final c c = (c)this.b.l(d11, b);
        if (c instanceof LifecycleBoundObserver) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (c != null) {
            return;
        }
        ((c)b).f(true);
    }
    
    public void j() {
    }
    
    public void k() {
    }
    
    public void l(final Object f) {
        synchronized (this.a) {
            final boolean b = this.f == LiveData.k;
            this.f = f;
            monitorexit(this.a);
            if (!b) {
                return;
            }
            b8.g().c(this.j);
        }
    }
    
    public void m(final d11 d11) {
        b("removeObserver");
        final c c = (c)this.b.m(d11);
        if (c == null) {
            return;
        }
        c.i();
        c.f(false);
    }
    
    public void n(final Object e) {
        b("setValue");
        ++this.g;
        this.e = e;
        this.e(null);
    }
    
    public class LifecycleBoundObserver extends c implements f
    {
        public final qj0 e;
        public final LiveData f;
        
        public LifecycleBoundObserver(final LiveData f, final qj0 e, final d11 d11) {
            this.f = f.super(d11);
            this.e = e;
        }
        
        @Override
        public void d(final qj0 qj0, final Lifecycle.Event event) {
            Enum<Lifecycle.State> b = this.e.getLifecycle().b();
            if (b == Lifecycle.State.DESTROYED) {
                this.f.m(super.a);
                return;
            }
            Lifecycle.State b2;
            for (Enum<Lifecycle.State> enum1 = null; enum1 != b; enum1 = b, b = b2) {
                ((c)this).f(this.k());
                b2 = this.e.getLifecycle().b();
            }
        }
        
        @Override
        public void i() {
            this.e.getLifecycle().c(this);
        }
        
        @Override
        public boolean j(final qj0 qj0) {
            return this.e == qj0;
        }
        
        @Override
        public boolean k() {
            return this.e.getLifecycle().b().isAtLeast(Lifecycle.State.STARTED);
        }
    }
    
    public abstract class c
    {
        public final d11 a;
        public boolean b;
        public int c;
        public final LiveData d;
        
        public c(final LiveData d, final d11 a) {
            this.d = d;
            this.c = -1;
            this.a = a;
        }
        
        public void f(final boolean b) {
            if (b == this.b) {
                return;
            }
            this.b = b;
            final LiveData d = this.d;
            int n;
            if (b) {
                n = 1;
            }
            else {
                n = -1;
            }
            d.c(n);
            if (this.b) {
                this.d.e(this);
            }
        }
        
        public void i() {
        }
        
        public boolean j(final qj0 qj0) {
            return false;
        }
        
        public abstract boolean k();
    }
    
    public class b extends c
    {
        public final LiveData e;
        
        public b(final LiveData e, final d11 d11) {
            this.e = e.super(d11);
        }
        
        @Override
        public boolean k() {
            return true;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.ArrayList;
import java.util.Collection;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public final class h
{
    public static final h a;
    public static final Map b;
    public static final Map c;
    
    static {
        a = new h();
        b = new HashMap();
        c = new HashMap();
    }
    
    public static final String c(final String s) {
        fg0.e((Object)s, "className");
        final StringBuilder sb = new StringBuilder();
        sb.append(qr1.r(s, ".", "_", false, 4, (Object)null));
        sb.append("_LifecycleAdapter");
        return sb.toString();
    }
    
    public static final f f(final Object o) {
        fg0.e(o, "object");
        final boolean b = o instanceof f;
        final boolean b2 = o instanceof vq;
        if (b && b2) {
            return new DefaultLifecycleObserverAdapter((vq)o, (f)o);
        }
        if (b2) {
            return new DefaultLifecycleObserverAdapter((vq)o, null);
        }
        if (b) {
            return (f)o;
        }
        final Class<?> class1 = o.getClass();
        final h a = h.a;
        if (a.d(class1) != 2) {
            return new ReflectiveGenericLifecycleObserver(o);
        }
        final Object value = h.c.get(class1);
        fg0.b(value);
        final List list = (List)value;
        final int size = list.size();
        int i = 0;
        if (size == 1) {
            a.a((Constructor)list.get(0), o);
            return new SingleGeneratedAdapterObserver(null);
        }
        final int size2 = list.size();
        final b[] array = new b[size2];
        while (i < size2) {
            h.a.a((Constructor)list.get(i), o);
            array[i] = null;
            ++i;
        }
        return new CompositeGeneratedAdaptersObserver(array);
    }
    
    public final b a(final Constructor constructor, final Object o) {
        try {
            final Object instance = constructor.newInstance(o);
            fg0.d(instance, "{\n            constructo\u2026tance(`object`)\n        }");
            zu0.a(instance);
            return null;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final InstantiationException cause2) {
            throw new RuntimeException(cause2);
        }
        catch (final IllegalAccessException cause3) {
            throw new RuntimeException(cause3);
        }
    }
    
    public final Constructor b(final Class clazz) {
        Constructor<?> declaredConstructor;
        try {
            final Package package1 = clazz.getPackage();
            String s = clazz.getCanonicalName();
            String name;
            if (package1 != null) {
                name = package1.getName();
            }
            else {
                name = "";
            }
            fg0.d((Object)name, "fullPackage");
            if (name.length() != 0) {
                fg0.d((Object)s, "name");
                s = s.substring(name.length() + 1);
                fg0.d((Object)s, "this as java.lang.String).substring(startIndex)");
            }
            fg0.d((Object)s, "if (fullPackage.isEmpty(\u2026g(fullPackage.length + 1)");
            final String c = c(s);
            String string;
            if (name.length() == 0) {
                string = c;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(name);
                sb.append('.');
                sb.append(c);
                string = sb.toString();
            }
            final Class<?> forName = Class.forName(string);
            fg0.c((Object)forName, "null cannot be cast to non-null type java.lang.Class<out androidx.lifecycle.GeneratedAdapter>");
            final Constructor<?> constructor = declaredConstructor = forName.getDeclaredConstructor(clazz);
            if (!constructor.isAccessible()) {
                constructor.setAccessible(true);
                declaredConstructor = constructor;
            }
        }
        catch (final NoSuchMethodException cause) {
            throw new RuntimeException(cause);
        }
        catch (final ClassNotFoundException ex) {
            declaredConstructor = null;
        }
        return declaredConstructor;
    }
    
    public final int d(final Class clazz) {
        final Map b = h.b;
        final Integer n = b.get(clazz);
        if (n != null) {
            return n;
        }
        final int g = this.g(clazz);
        b.put(clazz, g);
        return g;
    }
    
    public final boolean e(final Class clazz) {
        return clazz != null && pj0.class.isAssignableFrom(clazz);
    }
    
    public final int g(final Class clazz) {
        if (clazz.getCanonicalName() == null) {
            return 1;
        }
        final Constructor b = this.b(clazz);
        if (b != null) {
            h.c.put(clazz, mh.e((Object)b));
            return 2;
        }
        if (androidx.lifecycle.a.c.d(clazz)) {
            return 1;
        }
        final Class superclass = clazz.getSuperclass();
        ArrayList list;
        if (this.e(superclass)) {
            fg0.d((Object)superclass, "superclass");
            if (this.d(superclass) == 1) {
                return 1;
            }
            final Object value = h.c.get(superclass);
            fg0.b(value);
            list = new ArrayList((Collection)value);
        }
        else {
            list = null;
        }
        final Class[] interfaces = clazz.getInterfaces();
        fg0.d((Object)interfaces, "klass.interfaces");
        ArrayList list2;
        for (int length = interfaces.length, i = 0; i < length; ++i, list = list2) {
            final Class clazz2 = interfaces[i];
            if (!this.e(clazz2)) {
                list2 = list;
            }
            else {
                fg0.d((Object)clazz2, "intrface");
                if (this.d(clazz2) == 1) {
                    return 1;
                }
                if ((list2 = list) == null) {
                    list2 = new ArrayList();
                }
                final Object value2 = h.c.get(clazz2);
                fg0.b(value2);
                list2.addAll((Collection)value2);
            }
        }
        if (list != null) {
            h.c.put(clazz, list);
            return 2;
        }
        return 1;
    }
}

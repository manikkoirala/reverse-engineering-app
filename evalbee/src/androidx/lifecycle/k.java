// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.app.Application$ActivityLifecycleCallbacks;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Build$VERSION;
import android.app.Activity;
import android.app.Fragment;

public class k extends Fragment
{
    public static final b b;
    public a a;
    
    static {
        b = new b(null);
    }
    
    public static final void e(final Activity activity) {
        k.b.c(activity);
    }
    
    public final void a(final Lifecycle.Event event) {
        if (Build$VERSION.SDK_INT < 29) {
            final b b = k.b;
            final Activity activity = this.getActivity();
            fg0.d((Object)activity, "activity");
            b.a(activity, event);
        }
    }
    
    public final void b(final a a) {
        if (a != null) {
            a.a();
        }
    }
    
    public final void c(final a a) {
        if (a != null) {
            a.onResume();
        }
    }
    
    public final void d(final a a) {
        if (a != null) {
            a.onStart();
        }
    }
    
    public final void f(final a a) {
        this.a = a;
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.b(this.a);
        this.a(Lifecycle.Event.ON_CREATE);
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.a(Lifecycle.Event.ON_DESTROY);
        this.a = null;
    }
    
    public void onPause() {
        super.onPause();
        this.a(Lifecycle.Event.ON_PAUSE);
    }
    
    public void onResume() {
        super.onResume();
        this.c(this.a);
        this.a(Lifecycle.Event.ON_RESUME);
    }
    
    public void onStart() {
        super.onStart();
        this.d(this.a);
        this.a(Lifecycle.Event.ON_START);
    }
    
    public void onStop() {
        super.onStop();
        this.a(Lifecycle.Event.ON_STOP);
    }
    
    public interface a
    {
        void a();
        
        void onResume();
        
        void onStart();
    }
    
    public static final class b
    {
        public final void a(final Activity activity, final Lifecycle.Event event) {
            fg0.e((Object)activity, "activity");
            fg0.e((Object)event, "event");
            if (activity instanceof qj0) {
                final Lifecycle lifecycle = ((qj0)activity).getLifecycle();
                if (lifecycle instanceof g) {
                    ((g)lifecycle).h(event);
                }
            }
        }
        
        public final k b(final Activity activity) {
            fg0.e((Object)activity, "<this>");
            final Fragment fragmentByTag = activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
            fg0.c((Object)fragmentByTag, "null cannot be cast to non-null type androidx.lifecycle.ReportFragment");
            return (k)fragmentByTag;
        }
        
        public final void c(final Activity activity) {
            fg0.e((Object)activity, "activity");
            if (Build$VERSION.SDK_INT >= 29) {
                c.Companion.a(activity);
            }
            final FragmentManager fragmentManager = activity.getFragmentManager();
            if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
                fragmentManager.beginTransaction().add((Fragment)new k(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
                fragmentManager.executePendingTransactions();
            }
        }
    }
    
    public static final class c implements Application$ActivityLifecycleCallbacks
    {
        @NotNull
        public static final a Companion;
        
        static {
            Companion = new a(null);
        }
        
        public static final void registerIn(@NotNull final Activity activity) {
            c.Companion.a(activity);
        }
        
        public void onActivityCreated(@NotNull final Activity activity, @Nullable final Bundle bundle) {
            fg0.e((Object)activity, "activity");
        }
        
        public void onActivityDestroyed(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
        }
        
        public void onActivityPaused(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
        }
        
        public void onActivityPostCreated(@NotNull final Activity activity, @Nullable final Bundle bundle) {
            fg0.e((Object)activity, "activity");
            k.b.a(activity, Lifecycle.Event.ON_CREATE);
        }
        
        public void onActivityPostResumed(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
            k.b.a(activity, Lifecycle.Event.ON_RESUME);
        }
        
        public void onActivityPostStarted(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
            k.b.a(activity, Lifecycle.Event.ON_START);
        }
        
        public void onActivityPreDestroyed(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
            k.b.a(activity, Lifecycle.Event.ON_DESTROY);
        }
        
        public void onActivityPrePaused(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
            k.b.a(activity, Lifecycle.Event.ON_PAUSE);
        }
        
        public void onActivityPreStopped(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
            k.b.a(activity, Lifecycle.Event.ON_STOP);
        }
        
        public void onActivityResumed(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
        }
        
        public void onActivitySaveInstanceState(@NotNull final Activity activity, @NotNull final Bundle bundle) {
            fg0.e((Object)activity, "activity");
            fg0.e((Object)bundle, "bundle");
        }
        
        public void onActivityStarted(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
        }
        
        public void onActivityStopped(@NotNull final Activity activity) {
            fg0.e((Object)activity, "activity");
        }
        
        public static final class a
        {
            public final void a(final Activity activity) {
                fg0.e((Object)activity, "activity");
                vd1.a(activity, (Application$ActivityLifecycleCallbacks)new c());
            }
        }
    }
}

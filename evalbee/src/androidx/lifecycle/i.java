// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.RUNTIME)
public @interface i {
    Lifecycle.Event value();
}

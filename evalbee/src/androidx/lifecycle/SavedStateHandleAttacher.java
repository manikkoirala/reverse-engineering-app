// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

public final class SavedStateHandleAttacher implements f
{
    public final SavedStateHandlesProvider a;
    
    public SavedStateHandleAttacher(final SavedStateHandlesProvider a) {
        fg0.e((Object)a, "provider");
        this.a = a;
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event obj) {
        fg0.e((Object)qj0, "source");
        fg0.e((Object)obj, "event");
        if (obj == Lifecycle.Event.ON_CREATE) {
            qj0.getLifecycle().c(this);
            this.a.d();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Next event must be ON_CREATE, it was ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString().toString());
    }
}

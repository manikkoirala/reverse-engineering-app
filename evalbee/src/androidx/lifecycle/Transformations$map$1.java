// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Lambda;

final class Transformations$map$1 extends Lambda implements c90
{
    final av0 $result;
    final c90 $transform;
    
    public Transformations$map$1(final av0 av0, final c90 $transform) {
        this.$transform = $transform;
        super(1);
    }
    
    public final void invoke(final Object o) {
        this.$transform.invoke(o);
        throw null;
    }
}

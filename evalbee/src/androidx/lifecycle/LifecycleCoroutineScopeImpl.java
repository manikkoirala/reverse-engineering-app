// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;

public final class LifecycleCoroutineScopeImpl extends oj0 implements f
{
    public final Lifecycle a;
    public final CoroutineContext b;
    
    public CoroutineContext C() {
        return this.b;
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)qj0, "source");
        fg0.e((Object)event, "event");
        if (this.i().b().compareTo(Lifecycle.State.DESTROYED) <= 0) {
            this.i().c(this);
            ah0.d(this.C(), (CancellationException)null, 1, (Object)null);
        }
    }
    
    @Override
    public Lifecycle i() {
        return this.a;
    }
}

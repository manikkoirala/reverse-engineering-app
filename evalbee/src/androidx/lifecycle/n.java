// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import android.os.Handler;

public class n
{
    public final g a;
    public final Handler b;
    public a c;
    
    public n(final qj0 qj0) {
        fg0.e((Object)qj0, "provider");
        this.a = new g(qj0);
        this.b = new Handler();
    }
    
    public Lifecycle a() {
        return this.a;
    }
    
    public void b() {
        this.f(Lifecycle.Event.ON_START);
    }
    
    public void c() {
        this.f(Lifecycle.Event.ON_CREATE);
    }
    
    public void d() {
        this.f(Lifecycle.Event.ON_STOP);
        this.f(Lifecycle.Event.ON_DESTROY);
    }
    
    public void e() {
        this.f(Lifecycle.Event.ON_START);
    }
    
    public final void f(final Lifecycle.Event event) {
        final a c = this.c;
        if (c != null) {
            c.run();
        }
        final a c2 = new a(this.a, event);
        this.c = c2;
        final Handler b = this.b;
        fg0.b((Object)c2);
        b.postAtFrontOfQueue((Runnable)c2);
    }
    
    public static final class a implements Runnable
    {
        public final g a;
        public final Lifecycle.Event b;
        public boolean c;
        
        public a(final g a, final Lifecycle.Event b) {
            fg0.e((Object)a, "registry");
            fg0.e((Object)b, "event");
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            if (!this.c) {
                this.a.h(this.b);
                this.c = true;
            }
        }
    }
}

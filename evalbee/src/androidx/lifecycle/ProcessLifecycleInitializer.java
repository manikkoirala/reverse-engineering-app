// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.List;
import android.content.Context;

public final class ProcessLifecycleInitializer implements af0
{
    public qj0 b(final Context context) {
        fg0.e((Object)context, "context");
        final t7 e = t7.e(context);
        fg0.d((Object)e, "getInstance(context)");
        if (e.g(ProcessLifecycleInitializer.class)) {
            androidx.lifecycle.e.a(context);
            final j.b i = j.i;
            i.b(context);
            return i.a();
        }
        throw new IllegalStateException("ProcessLifecycleInitializer cannot be initialized lazily.\n               Please ensure that you have:\n               <meta-data\n                   android:name='androidx.lifecycle.ProcessLifecycleInitializer'\n                   android:value='androidx.startup' />\n               under InitializationProvider in your AndroidManifest.xml".toString());
    }
    
    @Override
    public List dependencies() {
        return nh.g();
    }
}

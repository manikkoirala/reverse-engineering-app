// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Lambda;

final class Transformations$map$2 extends Lambda implements c90
{
    final ba0 $mapFunction;
    final av0 $result;
    
    public Transformations$map$2(final av0 av0, final ba0 $mapFunction) {
        this.$mapFunction = $mapFunction;
        super(1);
    }
    
    public final void invoke(final Object o) {
        this.$mapFunction.apply(o);
        throw null;
    }
}

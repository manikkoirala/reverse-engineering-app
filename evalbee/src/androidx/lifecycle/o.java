// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.reflect.InvocationTargetException;
import android.app.Application;

public class o
{
    public final b42 a;
    public final b b;
    public final do c;
    
    public o(final b42 b42, final b b43) {
        fg0.e((Object)b42, "store");
        fg0.e((Object)b43, "factory");
        this(b42, b43, null, 4, null);
    }
    
    public o(final b42 a, final b b, final do c) {
        fg0.e((Object)a, "store");
        fg0.e((Object)b, "factory");
        fg0.e((Object)c, "defaultCreationExtras");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public o(final c42 c42, final b b) {
        fg0.e((Object)c42, "owner");
        fg0.e((Object)b, "factory");
        this(c42.getViewModelStore(), b, a42.a(c42));
    }
    
    public y32 a(final Class clazz) {
        fg0.e((Object)clazz, "modelClass");
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("androidx.lifecycle.ViewModelProvider.DefaultKey:");
            sb.append(canonicalName);
            return this.b(sb.toString(), clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    public y32 b(final String s, Class y32) {
        fg0.e((Object)s, "key");
        fg0.e((Object)y32, "modelClass");
        final y32 b = this.a.b(s);
        if (((Class)y32).isInstance(b)) {
            final b b2 = this.b;
            d d;
            if (b2 instanceof d) {
                d = (d)b2;
            }
            else {
                d = null;
            }
            if (d != null) {
                fg0.b((Object)b);
                d.c(b);
            }
            fg0.c((Object)b, "null cannot be cast to non-null type T of androidx.lifecycle.ViewModelProvider.get");
            return b;
        }
        final sx0 sx0 = new sx0(this.c);
        sx0.c(o.c.d, s);
        try {
            y32 = this.b.a((Class)y32, sx0);
        }
        catch (final AbstractMethodError abstractMethodError) {
            y32 = this.b.b((Class)y32);
        }
        this.a.d(s, y32);
        return y32;
    }
    
    public static class a extends c
    {
        public static final o.a.a f;
        public static o.a g;
        public static final do.b h;
        public final Application e;
        
        static {
            f = new o.a.a(null);
            h = o.a.a.a.a;
        }
        
        public a() {
            this(null, 0);
        }
        
        public a(final Application application) {
            fg0.e((Object)application, "application");
            this(application, 0);
        }
        
        public a(final Application e, final int n) {
            this.e = e;
        }
        
        public static final /* synthetic */ o.a e() {
            return o.a.g;
        }
        
        public static final /* synthetic */ void f(final o.a g) {
            o.a.g = g;
        }
        
        @Override
        public y32 a(final Class clazz, final do do1) {
            fg0.e((Object)clazz, "modelClass");
            fg0.e((Object)do1, "extras");
            y32 y32;
            if (this.e != null) {
                y32 = this.b(clazz);
            }
            else {
                final Application application = (Application)do1.a(o.a.h);
                if (application != null) {
                    y32 = this.g(clazz, application);
                }
                else {
                    if (x4.class.isAssignableFrom(clazz)) {
                        throw new IllegalArgumentException("CreationExtras must have an application by `APPLICATION_KEY`");
                    }
                    y32 = super.b(clazz);
                }
            }
            return y32;
        }
        
        @Override
        public y32 b(final Class clazz) {
            fg0.e((Object)clazz, "modelClass");
            final Application e = this.e;
            if (e != null) {
                return this.g(clazz, e);
            }
            throw new UnsupportedOperationException("AndroidViewModelFactory constructed with empty constructor works only with create(modelClass: Class<T>, extras: CreationExtras).");
        }
        
        public final y32 g(Class b, final Application application) {
            if (x4.class.isAssignableFrom((Class<?>)b)) {
                try {
                    final y32 y32 = (y32)((Class<?>)b).getConstructor(Application.class).newInstance(application);
                    fg0.d((Object)y32, "{\n                try {\n\u2026          }\n            }");
                    b = y32;
                    return (y32)b;
                }
                catch (final InvocationTargetException cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Cannot create an instance of ");
                    sb.append(b);
                    throw new RuntimeException(sb.toString(), cause);
                }
                catch (final InstantiationException cause2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot create an instance of ");
                    sb2.append(b);
                    throw new RuntimeException(sb2.toString(), cause2);
                }
                catch (final IllegalAccessException cause3) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Cannot create an instance of ");
                    sb3.append(b);
                    throw new RuntimeException(sb3.toString(), cause3);
                }
                catch (final NoSuchMethodException cause4) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Cannot create an instance of ");
                    sb4.append(b);
                    throw new RuntimeException(sb4.toString(), cause4);
                }
            }
            b = super.b((Class)b);
            return (y32)b;
        }
        
        public static final class a
        {
            public final o.a a(final Application application) {
                fg0.e((Object)application, "application");
                if (o.a.e() == null) {
                    o.a.f(new o.a(application));
                }
                final o.a e = o.a.e();
                fg0.b((Object)e);
                return e;
            }
            
            public static final class a implements do.b
            {
                public static final a a;
                
                static {
                    a = new a();
                }
            }
        }
    }
    
    public static class c implements b
    {
        public static final a b;
        public static c c;
        public static final do.b d;
        
        static {
            b = new a(null);
            d = a.a.a;
        }
        
        public static final /* synthetic */ c c() {
            return o.c.c;
        }
        
        public static final /* synthetic */ void d(final c c) {
            o.c.c = c;
        }
        
        @Override
        public y32 b(final Class obj) {
            fg0.e((Object)obj, "modelClass");
            try {
                final y32 instance = obj.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                fg0.d((Object)instance, "{\n                modelC\u2026wInstance()\n            }");
                return instance;
            }
            catch (final IllegalAccessException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot create an instance of ");
                sb.append(obj);
                throw new RuntimeException(sb.toString(), cause);
            }
            catch (final InstantiationException cause2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Cannot create an instance of ");
                sb2.append(obj);
                throw new RuntimeException(sb2.toString(), cause2);
            }
            catch (final NoSuchMethodException cause3) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Cannot create an instance of ");
                sb3.append(obj);
                throw new RuntimeException(sb3.toString(), cause3);
            }
        }
        
        public static final class a
        {
            public final c a() {
                if (o.c.c() == null) {
                    o.c.d(new c());
                }
                final c c = o.c.c();
                fg0.b((Object)c);
                return c;
            }
            
            public static final class a implements do.b
            {
                public static final a a;
                
                static {
                    a = new a();
                }
            }
        }
    }
    
    public interface b
    {
        public static final a a = b.a.a;
        
        default y32 a(final Class clazz, final do do1) {
            fg0.e((Object)clazz, "modelClass");
            fg0.e((Object)do1, "extras");
            return this.b(clazz);
        }
        
        default y32 b(final Class clazz) {
            fg0.e((Object)clazz, "modelClass");
            throw new UnsupportedOperationException("Factory.create(String) is unsupported.  This Factory requires `CreationExtras` to be passed into `create` method.");
        }
        
        public static final class a
        {
            public static final a a;
            
            static {
                a = new a();
            }
        }
    }
    
    public abstract static class d
    {
        public abstract void c(final y32 p0);
    }
}

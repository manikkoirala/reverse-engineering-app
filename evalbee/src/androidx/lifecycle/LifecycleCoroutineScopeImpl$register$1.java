// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.lifecycle.LifecycleCoroutineScopeImpl$register$1", f = "Lifecycle.kt", l = {}, m = "invokeSuspend")
final class LifecycleCoroutineScopeImpl$register$1 extends SuspendLambda implements q90
{
    private Object L$0;
    int label;
    final LifecycleCoroutineScopeImpl this$0;
    
    public LifecycleCoroutineScopeImpl$register$1(final LifecycleCoroutineScopeImpl this$0, final vl vl) {
        this.this$0 = this$0;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
        final LifecycleCoroutineScopeImpl$register$1 lifecycleCoroutineScopeImpl$register$1 = new LifecycleCoroutineScopeImpl$register$1(this.this$0, vl);
        lifecycleCoroutineScopeImpl$register$1.L$0 = l$0;
        return (vl)lifecycleCoroutineScopeImpl$register$1;
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((LifecycleCoroutineScopeImpl$register$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        if (this.label == 0) {
            xe1.b(o);
            final lm lm = (lm)this.L$0;
            if (this.this$0.i().b().compareTo(Lifecycle.State.INITIALIZED) >= 0) {
                this.this$0.i().a(this.this$0);
            }
            else {
                ah0.d(lm.C(), (CancellationException)null, 1, (Object)null);
            }
            return u02.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

@Deprecated
class ReflectiveGenericLifecycleObserver implements f
{
    public final Object a;
    public final a.a b;
    
    public ReflectiveGenericLifecycleObserver(final Object a) {
        this.a = a;
        this.b = androidx.lifecycle.a.c.c(a.getClass());
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        this.b.a(qj0, event, this.a);
    }
}

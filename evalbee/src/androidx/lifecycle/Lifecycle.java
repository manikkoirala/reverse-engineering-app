// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReference;

public abstract class Lifecycle
{
    public AtomicReference a;
    
    public Lifecycle() {
        this.a = new AtomicReference();
    }
    
    public abstract void a(final pj0 p0);
    
    public abstract State b();
    
    public abstract void c(final pj0 p0);
    
    public enum Event
    {
        private static final Event[] $VALUES;
        @NotNull
        public static final a Companion;
        
        ON_ANY, 
        ON_CREATE, 
        ON_DESTROY, 
        ON_PAUSE, 
        ON_RESUME, 
        ON_START, 
        ON_STOP;
        
        private static final /* synthetic */ Event[] $values() {
            return new Event[] { Event.ON_CREATE, Event.ON_START, Event.ON_RESUME, Event.ON_PAUSE, Event.ON_STOP, Event.ON_DESTROY, Event.ON_ANY };
        }
        
        static {
            $VALUES = $values();
            Companion = new a(null);
        }
        
        @Nullable
        public static final Event downFrom(@NotNull final State state) {
            return Event.Companion.a(state);
        }
        
        @Nullable
        public static final Event downTo(@NotNull final State state) {
            return Event.Companion.b(state);
        }
        
        @Nullable
        public static final Event upFrom(@NotNull final State state) {
            return Event.Companion.c(state);
        }
        
        @Nullable
        public static final Event upTo(@NotNull final State state) {
            return Event.Companion.d(state);
        }
        
        @NotNull
        public final State getTargetState() {
            switch (b.a[this.ordinal()]) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this);
                    sb.append(" has no target state");
                    throw new IllegalArgumentException(sb.toString());
                }
                case 6: {
                    return State.DESTROYED;
                }
                case 5: {
                    return State.RESUMED;
                }
                case 3:
                case 4: {
                    return State.STARTED;
                }
                case 1:
                case 2: {
                    return State.CREATED;
                }
            }
        }
        
        public static final class a
        {
            public final Event a(final State state) {
                fg0.e((Object)state, "state");
                final int n = Event.a.a.a[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            event = null;
                        }
                        else {
                            event = Event.ON_PAUSE;
                        }
                    }
                    else {
                        event = Event.ON_STOP;
                    }
                }
                else {
                    event = Event.ON_DESTROY;
                }
                return event;
            }
            
            public final Event b(final State state) {
                fg0.e((Object)state, "state");
                final int n = Event.a.a.a[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 4) {
                            event = null;
                        }
                        else {
                            event = Event.ON_DESTROY;
                        }
                    }
                    else {
                        event = Event.ON_PAUSE;
                    }
                }
                else {
                    event = Event.ON_STOP;
                }
                return event;
            }
            
            public final Event c(final State state) {
                fg0.e((Object)state, "state");
                final int n = Event.a.a.a[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 5) {
                            event = null;
                        }
                        else {
                            event = Event.ON_CREATE;
                        }
                    }
                    else {
                        event = Event.ON_RESUME;
                    }
                }
                else {
                    event = Event.ON_START;
                }
                return event;
            }
            
            public final Event d(final State state) {
                fg0.e((Object)state, "state");
                final int n = Event.a.a.a[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            event = null;
                        }
                        else {
                            event = Event.ON_RESUME;
                        }
                    }
                    else {
                        event = Event.ON_START;
                    }
                }
                else {
                    event = Event.ON_CREATE;
                }
                return event;
            }
        }
    }
    
    public enum State
    {
        private static final State[] $VALUES;
        
        CREATED, 
        DESTROYED, 
        INITIALIZED, 
        RESUMED, 
        STARTED;
        
        private static final /* synthetic */ State[] $values() {
            return new State[] { State.DESTROYED, State.INITIALIZED, State.CREATED, State.STARTED, State.RESUMED };
        }
        
        static {
            $VALUES = $values();
        }
        
        public final boolean isAtLeast(@NotNull final State o) {
            fg0.e((Object)o, "state");
            return this.compareTo(o) >= 0;
        }
    }
}

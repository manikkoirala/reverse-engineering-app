// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.savedstate.a;
import android.os.Bundle;

public abstract class SavedStateHandleSupport
{
    public static final do.b a;
    public static final do.b b;
    public static final do.b c;
    
    static {
        a = new do.b() {};
        b = new do.b() {};
        c = new do.b() {};
    }
    
    public static final l a(final do do1) {
        fg0.e((Object)do1, "<this>");
        final aj1 aj1 = (aj1)do1.a(SavedStateHandleSupport.a);
        if (aj1 == null) {
            throw new IllegalArgumentException("CreationExtras must have a value by `SAVED_STATE_REGISTRY_OWNER_KEY`");
        }
        final c42 c42 = (c42)do1.a(SavedStateHandleSupport.b);
        if (c42 == null) {
            throw new IllegalArgumentException("CreationExtras must have a value by `VIEW_MODEL_STORE_OWNER_KEY`");
        }
        final Bundle bundle = (Bundle)do1.a(SavedStateHandleSupport.c);
        final String s = (String)do1.a(o.c.d);
        if (s != null) {
            return b(aj1, c42, s, bundle);
        }
        throw new IllegalArgumentException("CreationExtras must have a value by `VIEW_MODEL_KEY`");
    }
    
    public static final l b(final aj1 aj1, final c42 c42, final String s, final Bundle bundle) {
        final SavedStateHandlesProvider d = d(aj1);
        final xi1 e = e(c42);
        l a;
        if ((a = e.f().get(s)) == null) {
            a = l.f.a(d.b(s), bundle);
            e.f().put(s, a);
        }
        return a;
    }
    
    public static final void c(final aj1 aj1) {
        fg0.e((Object)aj1, "<this>");
        final Lifecycle.State b = aj1.getLifecycle().b();
        if (b == Lifecycle.State.INITIALIZED || b == Lifecycle.State.CREATED) {
            if (aj1.getSavedStateRegistry().c("androidx.lifecycle.internal.SavedStateHandlesProvider") == null) {
                final SavedStateHandlesProvider savedStateHandlesProvider = new SavedStateHandlesProvider(aj1.getSavedStateRegistry(), (c42)aj1);
                aj1.getSavedStateRegistry().h("androidx.lifecycle.internal.SavedStateHandlesProvider", (a.c)savedStateHandlesProvider);
                aj1.getLifecycle().a(new SavedStateHandleAttacher(savedStateHandlesProvider));
            }
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    public static final SavedStateHandlesProvider d(final aj1 aj1) {
        fg0.e((Object)aj1, "<this>");
        final a.c c = aj1.getSavedStateRegistry().c("androidx.lifecycle.internal.SavedStateHandlesProvider");
        SavedStateHandlesProvider savedStateHandlesProvider;
        if (c instanceof SavedStateHandlesProvider) {
            savedStateHandlesProvider = (SavedStateHandlesProvider)c;
        }
        else {
            savedStateHandlesProvider = null;
        }
        if (savedStateHandlesProvider != null) {
            return savedStateHandlesProvider;
        }
        throw new IllegalStateException("enableSavedStateHandles() wasn't called prior to createSavedStateHandle() call");
    }
    
    public static final xi1 e(final c42 c42) {
        fg0.e((Object)c42, "<this>");
        final df0 df0 = new df0();
        df0.a(yc1.b((Class)xi1.class), (c90)SavedStateHandleSupport$savedStateHandlesVM$1.SavedStateHandleSupport$savedStateHandlesVM$1$1.INSTANCE);
        return (xi1)new o(c42, df0.b()).b("androidx.lifecycle.internal.SavedStateHandlesVM", xi1.class);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.coordinatorlayout.widget;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.view.ViewParent;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.View$BaseSavedState;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable$Callback;
import android.util.SparseArray;
import android.os.Parcelable;
import android.view.View$MeasureSpec;
import android.os.SystemClock;
import android.view.MotionEvent;
import java.util.Collection;
import android.view.ViewGroup$MarginLayoutParams;
import java.util.Collections;
import android.util.Log;
import android.graphics.Region$Op;
import android.graphics.Canvas;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewTreeObserver$OnPreDrawListener;
import java.util.HashMap;
import android.text.TextUtils;
import android.graphics.Rect;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.Paint;
import android.view.ViewGroup$OnHierarchyChangeListener;
import java.util.List;
import java.lang.reflect.Constructor;
import java.util.Map;
import android.view.View;
import java.util.Comparator;
import android.view.ViewGroup;

public class CoordinatorLayout extends ViewGroup implements ny0, oy0
{
    static final Class<?>[] CONSTRUCTOR_PARAMS;
    static final int EVENT_NESTED_SCROLL = 1;
    static final int EVENT_PRE_DRAW = 0;
    static final int EVENT_VIEW_REMOVED = 2;
    static final String TAG = "CoordinatorLayout";
    static final Comparator<View> TOP_SORTED_CHILDREN_COMPARATOR;
    private static final int TYPE_ON_INTERCEPT = 0;
    private static final int TYPE_ON_TOUCH = 1;
    static final String WIDGET_PACKAGE_NAME;
    static final ThreadLocal<Map<String, Constructor<c>>> sConstructors;
    private static final w51 sRectPool;
    private g11 mApplyWindowInsetsListener;
    private final int[] mBehaviorConsumed;
    private View mBehaviorTouchView;
    private final at mChildDag;
    private final List<View> mDependencySortedChildren;
    private boolean mDisallowInterceptReset;
    private boolean mDrawStatusBarBackground;
    private boolean mIsAttachedToWindow;
    private int[] mKeylines;
    private u62 mLastInsets;
    private boolean mNeedsPreDrawListener;
    private final py0 mNestedScrollingParentHelper;
    private View mNestedScrollingTarget;
    private final int[] mNestedScrollingV2ConsumedCompat;
    ViewGroup$OnHierarchyChangeListener mOnHierarchyChangeListener;
    private g mOnPreDrawListener;
    private Paint mScrimPaint;
    private Drawable mStatusBarBackground;
    private final List<View> mTempDependenciesList;
    private final List<View> mTempList1;
    
    static {
        final Package package1 = CoordinatorLayout.class.getPackage();
        String name;
        if (package1 != null) {
            name = package1.getName();
        }
        else {
            name = null;
        }
        WIDGET_PACKAGE_NAME = name;
        TOP_SORTED_CHILDREN_COMPARATOR = new i();
        CONSTRUCTOR_PARAMS = new Class[] { Context.class, AttributeSet.class };
        sConstructors = new ThreadLocal<Map<String, Constructor<c>>>();
        sRectPool = new a61(12);
    }
    
    public CoordinatorLayout(final Context context, final AttributeSet set) {
        this(context, set, pa1.a);
    }
    
    public CoordinatorLayout(final Context context, final AttributeSet set, int i) {
        super(context, set, i);
        this.mDependencySortedChildren = new ArrayList<View>();
        this.mChildDag = new at();
        this.mTempList1 = new ArrayList<View>();
        this.mTempDependenciesList = new ArrayList<View>();
        this.mBehaviorConsumed = new int[2];
        this.mNestedScrollingV2ConsumedCompat = new int[2];
        this.mNestedScrollingParentHelper = new py0(this);
        final int n = 0;
        final int[] b = wb1.b;
        TypedArray typedArray;
        if (i == 0) {
            typedArray = context.obtainStyledAttributes(set, b, 0, sb1.a);
        }
        else {
            typedArray = context.obtainStyledAttributes(set, b, i, 0);
        }
        if (Build$VERSION.SDK_INT >= 29) {
            final int[] b2 = wb1.b;
            if (i == 0) {
                bm.a(this, context, b2, set, typedArray, 0, sb1.a);
            }
            else {
                bm.a(this, context, b2, set, typedArray, i, 0);
            }
        }
        i = typedArray.getResourceId(wb1.c, 0);
        if (i != 0) {
            final Resources resources = context.getResources();
            this.mKeylines = resources.getIntArray(i);
            final float density = resources.getDisplayMetrics().density;
            int length;
            int[] mKeylines;
            for (length = this.mKeylines.length, i = n; i < length; ++i) {
                mKeylines = this.mKeylines;
                mKeylines[i] *= (int)density;
            }
        }
        this.mStatusBarBackground = typedArray.getDrawable(wb1.d);
        typedArray.recycle();
        this.v();
        super.setOnHierarchyChangeListener((ViewGroup$OnHierarchyChangeListener)new e());
        if (o32.z((View)this) == 0) {
            o32.B0((View)this, 1);
        }
    }
    
    public static Rect a() {
        Rect rect;
        if ((rect = (Rect)CoordinatorLayout.sRectPool.a()) == null) {
            rect = new Rect();
        }
        return rect;
    }
    
    public static int b(final int n, final int n2, final int n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static void o(final Rect rect) {
        rect.setEmpty();
        CoordinatorLayout.sRectPool.b(rect);
    }
    
    public static c parseBehavior(final Context context, final AttributeSet set, final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        String s2;
        if (s.startsWith(".")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(context.getPackageName());
            sb.append(s);
            s2 = sb.toString();
        }
        else if (s.indexOf(46) >= 0) {
            s2 = s;
        }
        else {
            final String widget_PACKAGE_NAME = CoordinatorLayout.WIDGET_PACKAGE_NAME;
            s2 = s;
            if (!TextUtils.isEmpty((CharSequence)widget_PACKAGE_NAME)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(widget_PACKAGE_NAME);
                sb2.append('.');
                sb2.append(s);
                s2 = sb2.toString();
            }
        }
        try {
            final ThreadLocal<Map<String, Constructor<c>>> sConstructors = CoordinatorLayout.sConstructors;
            Map value;
            if ((value = sConstructors.get()) == null) {
                value = new HashMap();
                sConstructors.set(value);
            }
            Constructor<?> constructor;
            if ((constructor = (Constructor)value.get(s2)) == null) {
                constructor = Class.forName(s2, false, context.getClassLoader()).getConstructor(CoordinatorLayout.CONSTRUCTOR_PARAMS);
                constructor.setAccessible(true);
                value.put(s2, constructor);
            }
            return (c)constructor.newInstance(context, set);
        }
        catch (final Exception cause) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Could not inflate Behavior subclass ");
            sb3.append(s2);
            throw new RuntimeException(sb3.toString(), cause);
        }
    }
    
    public static int q(final int n) {
        int n2 = n;
        if (n == 0) {
            n2 = 17;
        }
        return n2;
    }
    
    public static int r(int n) {
        int n2 = n;
        if ((n & 0x7) == 0x0) {
            n2 = (n | 0x800003);
        }
        n = n2;
        if ((n2 & 0x70) == 0x0) {
            n = (n2 | 0x30);
        }
        return n;
    }
    
    public static int s(final int n) {
        int n2 = n;
        if (n == 0) {
            n2 = 8388661;
        }
        return n2;
    }
    
    public void addPreDrawListener() {
        if (this.mIsAttachedToWindow) {
            if (this.mOnPreDrawListener == null) {
                this.mOnPreDrawListener = new g();
            }
            ((View)this).getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this.mOnPreDrawListener);
        }
        this.mNeedsPreDrawListener = true;
    }
    
    public final void c(final f f, final Rect rect, final int n, final int n2) {
        final int width = ((View)this).getWidth();
        final int height = ((View)this).getHeight();
        final int max = Math.max(((View)this).getPaddingLeft() + f.leftMargin, Math.min(rect.left, width - ((View)this).getPaddingRight() - n - f.rightMargin));
        final int max2 = Math.max(((View)this).getPaddingTop() + f.topMargin, Math.min(rect.top, height - ((View)this).getPaddingBottom() - n2 - f.bottomMargin));
        rect.set(max, max2, n + max, n2 + max2);
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof f && super.checkLayoutParams(viewGroup$LayoutParams);
    }
    
    public final u62 d(u62 u62) {
        if (u62.o()) {
            return u62;
        }
        final int childCount = this.getChildCount();
        int n = 0;
        u62 u63;
        while (true) {
            u63 = u62;
            if (n >= childCount) {
                break;
            }
            final View child = this.getChildAt(n);
            u62 onApplyWindowInsets = u62;
            if (o32.y(child)) {
                final c f = ((f)child.getLayoutParams()).f();
                onApplyWindowInsets = u62;
                if (f != null) {
                    u62 = (onApplyWindowInsets = f.onApplyWindowInsets(this, child, u62));
                    if (u62.o()) {
                        u63 = u62;
                        break;
                    }
                }
            }
            ++n;
            u62 = onApplyWindowInsets;
        }
        return u63;
    }
    
    public void dispatchDependentViewsChanged(final View view) {
        final List g = this.mChildDag.g(view);
        if (g != null && !g.isEmpty()) {
            for (int i = 0; i < g.size(); ++i) {
                final View view2 = g.get(i);
                final c f = ((f)view2.getLayoutParams()).f();
                if (f != null) {
                    f.onDependentViewChanged(this, view2, view);
                }
            }
        }
    }
    
    public boolean doViewsOverlap(View a, final View view) {
        final int visibility = a.getVisibility();
        final boolean b = false;
        if (visibility == 0 && view.getVisibility() == 0) {
            final Rect a2 = a();
            this.getChildRect(a, a.getParent() != this, a2);
            a = (View)a();
            this.getChildRect(view, view.getParent() != this, (Rect)a);
            boolean b2 = b;
            try {
                if (a2.left <= ((Rect)a).right) {
                    b2 = b;
                    if (a2.top <= ((Rect)a).bottom) {
                        b2 = b;
                        if (a2.right >= ((Rect)a).left) {
                            final int bottom = a2.bottom;
                            final int top = ((Rect)a).top;
                            b2 = b;
                            if (bottom >= top) {
                                b2 = true;
                            }
                        }
                    }
                }
                return b2;
            }
            finally {
                o(a2);
                o((Rect)a);
            }
        }
        return false;
    }
    
    public boolean drawChild(final Canvas canvas, final View view, final long n) {
        final f f = (f)view.getLayoutParams();
        final c a = f.a;
        if (a != null) {
            final float scrimOpacity = a.getScrimOpacity(this, view);
            if (scrimOpacity > 0.0f) {
                if (this.mScrimPaint == null) {
                    this.mScrimPaint = new Paint();
                }
                this.mScrimPaint.setColor(f.a.getScrimColor(this, view));
                this.mScrimPaint.setAlpha(b(Math.round(scrimOpacity * 255.0f), 0, 255));
                final int save = canvas.save();
                if (view.isOpaque()) {
                    canvas.clipRect((float)view.getLeft(), (float)view.getTop(), (float)view.getRight(), (float)view.getBottom(), Region$Op.DIFFERENCE);
                }
                canvas.drawRect((float)((View)this).getPaddingLeft(), (float)((View)this).getPaddingTop(), (float)(((View)this).getWidth() - ((View)this).getPaddingRight()), (float)(((View)this).getHeight() - ((View)this).getPaddingBottom()), this.mScrimPaint);
                canvas.restoreToCount(save);
            }
        }
        return super.drawChild(canvas, view, n);
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final int[] drawableState = ((View)this).getDrawableState();
        final Drawable mStatusBarBackground = this.mStatusBarBackground;
        int n = 0;
        if (mStatusBarBackground != null) {
            n = n;
            if (mStatusBarBackground.isStateful()) {
                n = ((false | mStatusBarBackground.setState(drawableState)) ? 1 : 0);
            }
        }
        if (n != 0) {
            ((View)this).invalidate();
        }
    }
    
    public final void e(final View view, int n, final Rect rect, final Rect rect2, final f f, final int n2, final int n3) {
        final int b = pb0.b(q(f.c), n);
        n = pb0.b(r(f.d), n);
        final int n4 = b & 0x7;
        final int n5 = b & 0x70;
        final int n6 = n & 0x7;
        final int n7 = n & 0x70;
        if (n6 != 1) {
            if (n6 != 5) {
                n = rect.left;
            }
            else {
                n = rect.right;
            }
        }
        else {
            n = rect.left + rect.width() / 2;
        }
        int n8;
        if (n7 != 16) {
            if (n7 != 80) {
                n8 = rect.top;
            }
            else {
                n8 = rect.bottom;
            }
        }
        else {
            n8 = rect.top + rect.height() / 2;
        }
        int n9;
        if (n4 != 1) {
            n9 = n;
            if (n4 != 5) {
                n9 = n - n2;
            }
        }
        else {
            n9 = n - n2 / 2;
        }
        if (n5 != 16) {
            n = n8;
            if (n5 != 80) {
                n = n8 - n3;
            }
        }
        else {
            n = n8 - n3 / 2;
        }
        rect2.set(n9, n, n2 + n9, n3 + n);
    }
    
    public void ensurePreDrawListener() {
        final int childCount = this.getChildCount();
        final boolean b = false;
        int n = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n >= childCount) {
                break;
            }
            if (this.h(this.getChildAt(n))) {
                b2 = true;
                break;
            }
            ++n;
        }
        if (b2 != this.mNeedsPreDrawListener) {
            if (b2) {
                this.addPreDrawListener();
            }
            else {
                this.removePreDrawListener();
            }
        }
    }
    
    public final int f(final int n) {
        final int[] mKeylines = this.mKeylines;
        StringBuilder sb;
        if (mKeylines == null) {
            sb = new StringBuilder();
            sb.append("No keylines defined for ");
            sb.append(this);
            sb.append(" - attempted index lookup ");
            sb.append(n);
        }
        else {
            if (n >= 0 && n < mKeylines.length) {
                return mKeylines[n];
            }
            sb = new StringBuilder();
            sb.append("Keyline index ");
            sb.append(n);
            sb.append(" out of range for ");
            sb.append(this);
        }
        Log.e("CoordinatorLayout", sb.toString());
        return 0;
    }
    
    public final void g(final List list) {
        list.clear();
        final boolean childrenDrawingOrderEnabled = this.isChildrenDrawingOrderEnabled();
        final int childCount = this.getChildCount();
        for (int i = childCount - 1; i >= 0; --i) {
            int childDrawingOrder;
            if (childrenDrawingOrderEnabled) {
                childDrawingOrder = this.getChildDrawingOrder(childCount, i);
            }
            else {
                childDrawingOrder = i;
            }
            list.add(this.getChildAt(childDrawingOrder));
        }
        final Comparator<View> top_SORTED_CHILDREN_COMPARATOR = CoordinatorLayout.TOP_SORTED_CHILDREN_COMPARATOR;
        if (top_SORTED_CHILDREN_COMPARATOR != null) {
            Collections.sort((List<Object>)list, (Comparator<? super Object>)top_SORTED_CHILDREN_COMPARATOR);
        }
    }
    
    public f generateDefaultLayoutParams() {
        return new f(-2, -2);
    }
    
    public f generateLayoutParams(final AttributeSet set) {
        return new f(((View)this).getContext(), set);
    }
    
    public f generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof f) {
            return new f((f)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return new f((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return new f(viewGroup$LayoutParams);
    }
    
    public void getChildRect(final View view, final boolean b, final Rect rect) {
        if (!view.isLayoutRequested() && view.getVisibility() != 8) {
            if (b) {
                this.getDescendantRect(view, rect);
            }
            else {
                rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            return;
        }
        rect.setEmpty();
    }
    
    public List<View> getDependencies(final View view) {
        final List h = this.mChildDag.h(view);
        this.mTempDependenciesList.clear();
        if (h != null) {
            this.mTempDependenciesList.addAll(h);
        }
        return this.mTempDependenciesList;
    }
    
    public final List<View> getDependencySortedChildren() {
        this.n();
        return Collections.unmodifiableList((List<? extends View>)this.mDependencySortedChildren);
    }
    
    public List<View> getDependents(final View view) {
        final List g = this.mChildDag.g(view);
        this.mTempDependenciesList.clear();
        if (g != null) {
            this.mTempDependenciesList.addAll(g);
        }
        return this.mTempDependenciesList;
    }
    
    public void getDescendantRect(final View view, final Rect rect) {
        x32.a(this, view, rect);
    }
    
    public void getDesiredAnchoredChildRect(final View view, final int n, final Rect rect, final Rect rect2) {
        final f f = (f)view.getLayoutParams();
        final int measuredWidth = view.getMeasuredWidth();
        final int measuredHeight = view.getMeasuredHeight();
        this.e(view, n, rect, rect2, f, measuredWidth, measuredHeight);
        this.c(f, rect2, measuredWidth, measuredHeight);
    }
    
    public void getLastChildRect(final View view, final Rect rect) {
        rect.set(((f)view.getLayoutParams()).h());
    }
    
    public final u62 getLastWindowInsets() {
        return this.mLastInsets;
    }
    
    public int getNestedScrollAxes() {
        return this.mNestedScrollingParentHelper.a();
    }
    
    public f getResolvedLayoutParams(View o) {
        final f f = (f)((View)o).getLayoutParams();
        if (!f.b) {
            if (o instanceof b) {
                final c behavior = ((b)o).getBehavior();
                if (behavior == null) {
                    Log.e("CoordinatorLayout", "Attached behavior class is null");
                }
                f.o(behavior);
            }
            else {
                Class<?> clazz = o.getClass();
                o = null;
                while (clazz != null) {
                    final d d = clazz.getAnnotation(d.class);
                    if ((o = d) != null) {
                        break;
                    }
                    clazz = clazz.getSuperclass();
                    o = d;
                }
                if (o != null) {
                    try {
                        f.o(((d)o).value().getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]));
                    }
                    catch (final Exception ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Default behavior class ");
                        sb.append(((d)o).value().getName());
                        sb.append(" could not be instantiated. Did you forget a default constructor?");
                        Log.e("CoordinatorLayout", sb.toString(), (Throwable)ex);
                    }
                }
            }
            f.b = true;
        }
        return f;
    }
    
    public Drawable getStatusBarBackground() {
        return this.mStatusBarBackground;
    }
    
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), ((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
    }
    
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), ((View)this).getPaddingLeft() + ((View)this).getPaddingRight());
    }
    
    public final boolean h(final View view) {
        return this.mChildDag.j(view);
    }
    
    public final void i(final View view, final int n) {
        final f f = (f)view.getLayoutParams();
        final Rect a = a();
        a.set(((View)this).getPaddingLeft() + f.leftMargin, ((View)this).getPaddingTop() + f.topMargin, ((View)this).getWidth() - ((View)this).getPaddingRight() - f.rightMargin, ((View)this).getHeight() - ((View)this).getPaddingBottom() - f.bottomMargin);
        if (this.mLastInsets != null && o32.y((View)this) && !o32.y(view)) {
            a.left += this.mLastInsets.i();
            a.top += this.mLastInsets.k();
            a.right -= this.mLastInsets.j();
            a.bottom -= this.mLastInsets.h();
        }
        final Rect a2 = a();
        pb0.a(r(f.c), view.getMeasuredWidth(), view.getMeasuredHeight(), a, a2, n);
        view.layout(a2.left, a2.top, a2.right, a2.bottom);
        o(a);
        o(a2);
    }
    
    public boolean isPointInChildBounds(final View view, final int n, final int n2) {
        final Rect a = a();
        this.getDescendantRect(view, a);
        try {
            return a.contains(n, n2);
        }
        finally {
            o(a);
        }
    }
    
    public final void j(final View view, final View view2, final int n) {
        final Rect a = a();
        final Rect a2 = a();
        try {
            this.getDescendantRect(view2, a);
            this.getDesiredAnchoredChildRect(view, n, a, a2);
            view.layout(a2.left, a2.top, a2.right, a2.bottom);
        }
        finally {
            o(a);
            o(a2);
        }
    }
    
    public final void k(final View view, int max, int max2) {
        final f f = (f)view.getLayoutParams();
        final int b = pb0.b(s(f.c), max2);
        final int n = b & 0x7;
        final int n2 = b & 0x70;
        final int width = ((View)this).getWidth();
        final int height = ((View)this).getHeight();
        final int measuredWidth = view.getMeasuredWidth();
        final int measuredHeight = view.getMeasuredHeight();
        int n3 = max;
        if (max2 == 1) {
            n3 = width - max;
        }
        max = this.f(n3) - measuredWidth;
        if (n != 1) {
            if (n == 5) {
                max += measuredWidth;
            }
        }
        else {
            max += measuredWidth / 2;
        }
        max2 = 0;
        if (n2 != 16) {
            if (n2 == 80) {
                max2 = measuredHeight + 0;
            }
        }
        else {
            max2 = 0 + measuredHeight / 2;
        }
        max = Math.max(((View)this).getPaddingLeft() + f.leftMargin, Math.min(max, width - ((View)this).getPaddingRight() - measuredWidth - f.rightMargin));
        max2 = Math.max(((View)this).getPaddingTop() + f.topMargin, Math.min(max2, height - ((View)this).getPaddingBottom() - measuredHeight - f.bottomMargin));
        view.layout(max, max2, measuredWidth + max, measuredHeight + max2);
    }
    
    public final void l(final View view, final Rect rect, int left) {
        if (!o32.U(view)) {
            return;
        }
        if (view.getWidth() > 0) {
            if (view.getHeight() > 0) {
                final f f = (f)view.getLayoutParams();
                final c f2 = f.f();
                final Rect a = a();
                final Rect a2 = a();
                a2.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
                if (f2 != null && f2.getInsetDodgeRect(this, view, a)) {
                    if (!a2.contains(a)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Rect should be within the child's bounds. Rect:");
                        sb.append(a.toShortString());
                        sb.append(" | Bounds:");
                        sb.append(a2.toShortString());
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                else {
                    a.set(a2);
                }
                o(a2);
                if (a.isEmpty()) {
                    o(a);
                    return;
                }
                final int b = pb0.b(f.h, left);
                final int n = 1;
                Label_0254: {
                    if ((b & 0x30) == 0x30) {
                        left = a.top - f.topMargin - f.j;
                        final int top = rect.top;
                        if (left < top) {
                            this.u(view, top - left);
                            left = 1;
                            break Label_0254;
                        }
                    }
                    left = 0;
                }
                int n2 = left;
                if ((b & 0x50) == 0x50) {
                    final int n3 = ((View)this).getHeight() - a.bottom - f.bottomMargin + f.j;
                    final int bottom = rect.bottom;
                    n2 = left;
                    if (n3 < bottom) {
                        this.u(view, n3 - bottom);
                        n2 = 1;
                    }
                }
                if (n2 == 0) {
                    this.u(view, 0);
                }
                Label_0385: {
                    if ((b & 0x3) == 0x3) {
                        final int n4 = a.left - f.leftMargin - f.i;
                        left = rect.left;
                        if (n4 < left) {
                            this.t(view, left - n4);
                            left = 1;
                            break Label_0385;
                        }
                    }
                    left = 0;
                }
                if ((b & 0x5) == 0x5) {
                    final int n5 = ((View)this).getWidth() - a.right - f.rightMargin + f.i;
                    final int right = rect.right;
                    if (n5 < right) {
                        this.t(view, n5 - right);
                        left = n;
                    }
                }
                if (left == 0) {
                    this.t(view, 0);
                }
                o(a);
            }
        }
    }
    
    public final boolean m(final MotionEvent motionEvent, final int n) {
        final int actionMasked = motionEvent.getActionMasked();
        final List<View> mTempList1 = this.mTempList1;
        this.g(mTempList1);
        final int size = mTempList1.size();
        MotionEvent motionEvent2 = null;
        int n2 = 0;
        int n4;
        int n3 = n4 = 0;
        boolean b;
        while (true) {
            b = (n3 != 0);
            if (n2 >= size) {
                break;
            }
            final View mBehaviorTouchView = mTempList1.get(n2);
            final f f = (f)mBehaviorTouchView.getLayoutParams();
            final c f2 = f.f();
            MotionEvent obtain;
            boolean b2;
            int n5;
            if ((n3 || n4 != 0) && actionMasked != 0) {
                obtain = motionEvent2;
                b2 = (n3 != 0);
                n5 = n4;
                if (f2 != null) {
                    if ((obtain = motionEvent2) == null) {
                        final long uptimeMillis = SystemClock.uptimeMillis();
                        obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    }
                    if (n != 0) {
                        if (n != 1) {
                            b2 = (n3 != 0);
                            n5 = n4;
                        }
                        else {
                            f2.onTouchEvent(this, mBehaviorTouchView, obtain);
                            b2 = (n3 != 0);
                            n5 = n4;
                        }
                    }
                    else {
                        f2.onInterceptTouchEvent(this, mBehaviorTouchView, obtain);
                        b2 = (n3 != 0);
                        n5 = n4;
                    }
                }
            }
            else {
                boolean b3 = n3 != 0;
                if (n3 == 0) {
                    b3 = (n3 != 0);
                    if (f2 != null) {
                        if (n != 0) {
                            if (n == 1) {
                                n3 = (f2.onTouchEvent(this, mBehaviorTouchView, motionEvent) ? 1 : 0);
                            }
                        }
                        else {
                            n3 = (f2.onInterceptTouchEvent(this, mBehaviorTouchView, motionEvent) ? 1 : 0);
                        }
                        b3 = (n3 != 0);
                        if (n3 != 0) {
                            this.mBehaviorTouchView = mBehaviorTouchView;
                            b3 = (n3 != 0);
                        }
                    }
                }
                final boolean c = f.c();
                final boolean i = f.i(this, mBehaviorTouchView);
                final boolean b4 = i && !c;
                obtain = motionEvent2;
                b2 = b3;
                n5 = (b4 ? 1 : 0);
                if (i) {
                    obtain = motionEvent2;
                    b2 = b3;
                    if ((n5 = (b4 ? 1 : 0)) == 0) {
                        b = b3;
                        break;
                    }
                }
            }
            ++n2;
            motionEvent2 = obtain;
            n3 = (b2 ? 1 : 0);
            n4 = n5;
        }
        mTempList1.clear();
        return b;
    }
    
    public final void n() {
        this.mDependencySortedChildren.clear();
        this.mChildDag.c();
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final f resolvedLayoutParams = this.getResolvedLayoutParams(child);
            resolvedLayoutParams.d(this, child);
            this.mChildDag.b(child);
            for (int j = 0; j < childCount; ++j) {
                if (j != i) {
                    final View child2 = this.getChildAt(j);
                    if (resolvedLayoutParams.b(this, child, child2)) {
                        if (!this.mChildDag.d(child2)) {
                            this.mChildDag.b(child2);
                        }
                        this.mChildDag.a(child2, child);
                    }
                }
            }
        }
        this.mDependencySortedChildren.addAll(this.mChildDag.i());
        Collections.reverse(this.mDependencySortedChildren);
    }
    
    public void offsetChildToAnchor(final View view, int n) {
        final f f = (f)view.getLayoutParams();
        if (f.k != null) {
            final Rect a = a();
            final Rect a2 = a();
            final Rect a3 = a();
            this.getDescendantRect(f.k, a);
            final int n2 = 0;
            this.getChildRect(view, false, a2);
            final int measuredWidth = view.getMeasuredWidth();
            final int measuredHeight = view.getMeasuredHeight();
            this.e(view, n, a, a3, f, measuredWidth, measuredHeight);
            Label_0111: {
                if (a3.left == a2.left) {
                    n = n2;
                    if (a3.top == a2.top) {
                        break Label_0111;
                    }
                }
                n = 1;
            }
            this.c(f, a3, measuredWidth, measuredHeight);
            final int n3 = a3.left - a2.left;
            final int n4 = a3.top - a2.top;
            if (n3 != 0) {
                o32.a0(view, n3);
            }
            if (n4 != 0) {
                o32.b0(view, n4);
            }
            if (n != 0) {
                final c f2 = f.f();
                if (f2 != null) {
                    f2.onDependentViewChanged(this, view, f.k);
                }
            }
            o(a);
            o(a2);
            o(a3);
        }
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.p(false);
        if (this.mNeedsPreDrawListener) {
            if (this.mOnPreDrawListener == null) {
                this.mOnPreDrawListener = new g();
            }
            ((View)this).getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this.mOnPreDrawListener);
        }
        if (this.mLastInsets == null && o32.y((View)this)) {
            o32.n0((View)this);
        }
        this.mIsAttachedToWindow = true;
    }
    
    public final void onChildViewsChanged(final int n) {
        final int b = o32.B((View)this);
        final int size = this.mDependencySortedChildren.size();
        final Rect a = a();
        final Rect a2 = a();
        final Rect a3 = a();
        for (int i = 0; i < size; ++i) {
            final View view = this.mDependencySortedChildren.get(i);
            final f f = (f)view.getLayoutParams();
            if (n != 0 || view.getVisibility() != 8) {
                for (int j = 0; j < i; ++j) {
                    if (f.l == this.mDependencySortedChildren.get(j)) {
                        this.offsetChildToAnchor(view, b);
                    }
                }
                this.getChildRect(view, true, a2);
                if (f.g != 0 && !a2.isEmpty()) {
                    final int b2 = pb0.b(f.g, b);
                    final int n2 = b2 & 0x70;
                    if (n2 != 48) {
                        if (n2 == 80) {
                            a.bottom = Math.max(a.bottom, ((View)this).getHeight() - a2.top);
                        }
                    }
                    else {
                        a.top = Math.max(a.top, a2.bottom);
                    }
                    final int n3 = b2 & 0x7;
                    if (n3 != 3) {
                        if (n3 == 5) {
                            a.right = Math.max(a.right, ((View)this).getWidth() - a2.left);
                        }
                    }
                    else {
                        a.left = Math.max(a.left, a2.right);
                    }
                }
                if (f.h != 0 && view.getVisibility() == 0) {
                    this.l(view, a, b);
                }
                if (n != 2) {
                    this.getLastChildRect(view, a3);
                    if (a3.equals((Object)a2)) {
                        continue;
                    }
                    this.recordLastChildRect(view, a2);
                }
                for (int k = i + 1; k < size; ++k) {
                    final View view2 = this.mDependencySortedChildren.get(k);
                    final f f2 = (f)view2.getLayoutParams();
                    final c f3 = f2.f();
                    if (f3 != null && f3.layoutDependsOn(this, view2, view)) {
                        if (n == 0 && f2.g()) {
                            f2.k();
                        }
                        else {
                            boolean onDependentViewChanged;
                            if (n != 2) {
                                onDependentViewChanged = f3.onDependentViewChanged(this, view2, view);
                            }
                            else {
                                f3.onDependentViewRemoved(this, view2, view);
                                onDependentViewChanged = true;
                            }
                            if (n == 1) {
                                f2.p(onDependentViewChanged);
                            }
                        }
                    }
                }
            }
        }
        o(a);
        o(a2);
        o(a3);
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.p(false);
        if (this.mNeedsPreDrawListener && this.mOnPreDrawListener != null) {
            ((View)this).getViewTreeObserver().removeOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this.mOnPreDrawListener);
        }
        final View mNestedScrollingTarget = this.mNestedScrollingTarget;
        if (mNestedScrollingTarget != null) {
            this.onStopNestedScroll(mNestedScrollingTarget);
        }
        this.mIsAttachedToWindow = false;
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.mDrawStatusBarBackground && this.mStatusBarBackground != null) {
            final u62 mLastInsets = this.mLastInsets;
            int k;
            if (mLastInsets != null) {
                k = mLastInsets.k();
            }
            else {
                k = 0;
            }
            if (k > 0) {
                this.mStatusBarBackground.setBounds(0, 0, ((View)this).getWidth(), k);
                this.mStatusBarBackground.draw(canvas);
            }
        }
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.p(true);
        }
        final boolean m = this.m(motionEvent, 0);
        if (actionMasked == 1 || actionMasked == 3) {
            this.p(true);
        }
        return m;
    }
    
    public void onLayout(final boolean b, int i, int b2, int size, final int n) {
        b2 = o32.B((View)this);
        View view;
        c f;
        for (size = this.mDependencySortedChildren.size(), i = 0; i < size; ++i) {
            view = this.mDependencySortedChildren.get(i);
            if (view.getVisibility() != 8) {
                f = ((f)view.getLayoutParams()).f();
                if (f == null || !f.onLayoutChild(this, view, b2)) {
                    this.onLayoutChild(view, b2);
                }
            }
        }
    }
    
    public void onLayoutChild(final View view, final int n) {
        final f f = (f)view.getLayoutParams();
        if (!f.a()) {
            final View k = f.k;
            if (k != null) {
                this.j(view, k, n);
            }
            else {
                final int e = f.e;
                if (e >= 0) {
                    this.k(view, e, n);
                }
                else {
                    this.i(view, n);
                }
            }
            return;
        }
        throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
    }
    
    public void onMeasure(final int n, final int n2) {
        this.n();
        this.ensurePreDrawListener();
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int paddingTop = ((View)this).getPaddingTop();
        final int paddingRight = ((View)this).getPaddingRight();
        final int paddingBottom = ((View)this).getPaddingBottom();
        final int b = o32.B((View)this);
        final boolean b2 = b == 1;
        final int mode = View$MeasureSpec.getMode(n);
        final int size = View$MeasureSpec.getSize(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        final int size2 = View$MeasureSpec.getSize(n2);
        int a = this.getSuggestedMinimumWidth();
        int a2 = this.getSuggestedMinimumHeight();
        final boolean b3 = this.mLastInsets != null && o32.y((View)this);
        final int size3 = this.mDependencySortedChildren.size();
        int combineMeasuredStates = 0;
        int n3 = 0;
        int n4 = paddingLeft;
        while (true) {
            final int n5 = n4;
            if (n3 >= size3) {
                break;
            }
            final View view = this.mDependencySortedChildren.get(n3);
            if (view.getVisibility() != 8) {
                final f f = (f)view.getLayoutParams();
                final int e = f.e;
                int n7 = 0;
                Label_0291: {
                    if (e >= 0 && mode != 0) {
                        final int f2 = this.f(e);
                        final int n6 = pb0.b(s(f.c), b) & 0x7;
                        if ((n6 == 3 && !b2) || (n6 == 5 && b2)) {
                            n7 = Math.max(0, size - paddingRight - f2);
                            break Label_0291;
                        }
                        if ((n6 == 5 && !b2) || (n6 == 3 && b2)) {
                            n7 = Math.max(0, f2 - n5);
                            break Label_0291;
                        }
                    }
                    n7 = 0;
                }
                int measureSpec;
                int measureSpec2;
                if (b3 && !o32.y(view)) {
                    final int i = this.mLastInsets.i();
                    final int j = this.mLastInsets.j();
                    final int k = this.mLastInsets.k();
                    final int h = this.mLastInsets.h();
                    measureSpec = View$MeasureSpec.makeMeasureSpec(size - (i + j), mode);
                    measureSpec2 = View$MeasureSpec.makeMeasureSpec(size2 - (k + h), mode2);
                }
                else {
                    measureSpec2 = n2;
                    measureSpec = n;
                }
                final c f3 = f.f();
                if (f3 == null || !f3.onMeasureChild(this, view, measureSpec, n7, measureSpec2, 0)) {
                    this.onMeasureChild(view, measureSpec, n7, measureSpec2, 0);
                }
                a = Math.max(a, paddingLeft + paddingRight + view.getMeasuredWidth() + f.leftMargin + f.rightMargin);
                a2 = Math.max(a2, paddingTop + paddingBottom + view.getMeasuredHeight() + f.topMargin + f.bottomMargin);
                combineMeasuredStates = View.combineMeasuredStates(combineMeasuredStates, view.getMeasuredState());
            }
            ++n3;
            n4 = n5;
        }
        ((View)this).setMeasuredDimension(View.resolveSizeAndState(a, n, 0xFF000000 & combineMeasuredStates), View.resolveSizeAndState(a2, n2, combineMeasuredStates << 16));
    }
    
    public void onMeasureChild(final View view, final int n, final int n2, final int n3, final int n4) {
        this.measureChildWithMargins(view, n, n2, n3, n4);
    }
    
    public boolean onNestedFling(final View view, final float n, final float n2, final boolean b) {
        final int childCount = this.getChildCount();
        int i = 0;
        int n3 = 0;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            boolean b2;
            if (child.getVisibility() == 8) {
                b2 = (n3 != 0);
            }
            else {
                final f f = (f)child.getLayoutParams();
                if (!f.j(0)) {
                    b2 = (n3 != 0);
                }
                else {
                    final c f2 = f.f();
                    b2 = (n3 != 0);
                    if (f2 != null) {
                        b2 = ((n3 | (f2.onNestedFling(this, child, view, n, n2, b) ? 1 : 0)) != 0x0);
                    }
                }
            }
            ++i;
            n3 = (b2 ? 1 : 0);
        }
        if (n3 != 0) {
            this.onChildViewsChanged(1);
        }
        return n3 != 0;
    }
    
    public boolean onNestedPreFling(final View view, final float n, final float n2) {
        final int childCount = this.getChildCount();
        int i = 0;
        int n3 = 0;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            boolean b;
            if (child.getVisibility() == 8) {
                b = (n3 != 0);
            }
            else {
                final f f = (f)child.getLayoutParams();
                if (!f.j(0)) {
                    b = (n3 != 0);
                }
                else {
                    final c f2 = f.f();
                    b = (n3 != 0);
                    if (f2 != null) {
                        b = ((n3 | (f2.onNestedPreFling(this, child, view, n, n2) ? 1 : 0)) != 0x0);
                    }
                }
            }
            ++i;
            n3 = (b ? 1 : 0);
        }
        return n3 != 0;
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array) {
        this.onNestedPreScroll(view, n, n2, array, 0);
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int n3) {
        final int childCount = this.getChildCount();
        boolean b = false;
        int i = 0;
        int n5;
        int n4 = n5 = i;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            int n6;
            int n7;
            if (child.getVisibility() == 8) {
                n6 = n5;
                n7 = n4;
            }
            else {
                final f f = (f)child.getLayoutParams();
                if (!f.j(n3)) {
                    n6 = n5;
                    n7 = n4;
                }
                else {
                    final c f2 = f.f();
                    n6 = n5;
                    n7 = n4;
                    if (f2 != null) {
                        final int[] mBehaviorConsumed = this.mBehaviorConsumed;
                        mBehaviorConsumed[1] = (mBehaviorConsumed[0] = 0);
                        f2.onNestedPreScroll(this, child, view, n, n2, mBehaviorConsumed, n3);
                        final int[] mBehaviorConsumed2 = this.mBehaviorConsumed;
                        int n8;
                        if (n > 0) {
                            n8 = Math.max(n5, mBehaviorConsumed2[0]);
                        }
                        else {
                            n8 = Math.min(n5, mBehaviorConsumed2[0]);
                        }
                        n6 = n8;
                        final int[] mBehaviorConsumed3 = this.mBehaviorConsumed;
                        if (n2 > 0) {
                            n7 = Math.max(n4, mBehaviorConsumed3[1]);
                        }
                        else {
                            n7 = Math.min(n4, mBehaviorConsumed3[1]);
                        }
                        b = true;
                    }
                }
            }
            ++i;
            n5 = n6;
            n4 = n7;
        }
        array[0] = n5;
        array[1] = n4;
        if (b) {
            this.onChildViewsChanged(1);
        }
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4) {
        this.onNestedScroll(view, n, n2, n3, n4, 0);
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        this.onNestedScroll(view, n, n2, n3, n4, 0, this.mNestedScrollingV2ConsumedCompat);
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int[] array) {
        final int childCount = this.getChildCount();
        boolean b = false;
        int i = 0;
        int n7;
        int n6 = n7 = i;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            int n8;
            int n9;
            if (child.getVisibility() == 8) {
                n8 = n7;
                n9 = n6;
            }
            else {
                final f f = (f)child.getLayoutParams();
                if (!f.j(n5)) {
                    n8 = n7;
                    n9 = n6;
                }
                else {
                    final c f2 = f.f();
                    n8 = n7;
                    n9 = n6;
                    if (f2 != null) {
                        final int[] mBehaviorConsumed = this.mBehaviorConsumed;
                        mBehaviorConsumed[1] = (mBehaviorConsumed[0] = 0);
                        f2.onNestedScroll(this, child, view, n, n2, n3, n4, n5, mBehaviorConsumed);
                        final int[] mBehaviorConsumed2 = this.mBehaviorConsumed;
                        int n10;
                        if (n3 > 0) {
                            n10 = Math.max(n7, mBehaviorConsumed2[0]);
                        }
                        else {
                            n10 = Math.min(n7, mBehaviorConsumed2[0]);
                        }
                        final int n11 = n10;
                        if (n4 > 0) {
                            n9 = Math.max(n6, this.mBehaviorConsumed[1]);
                        }
                        else {
                            n9 = Math.min(n6, this.mBehaviorConsumed[1]);
                        }
                        b = true;
                        n8 = n11;
                    }
                }
            }
            ++i;
            n7 = n8;
            n6 = n9;
        }
        array[0] += n7;
        array[1] += n6;
        if (b) {
            this.onChildViewsChanged(1);
        }
    }
    
    public void onNestedScrollAccepted(final View view, final View view2, final int n) {
        this.onNestedScrollAccepted(view, view2, n, 0);
    }
    
    public void onNestedScrollAccepted(final View view, final View mNestedScrollingTarget, final int n, final int n2) {
        this.mNestedScrollingParentHelper.c(view, mNestedScrollingTarget, n, n2);
        this.mNestedScrollingTarget = mNestedScrollingTarget;
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final f f = (f)child.getLayoutParams();
            if (f.j(n2)) {
                final c f2 = f.f();
                if (f2 != null) {
                    f2.onNestedScrollAccepted(this, child, view, mNestedScrollingTarget, n, n2);
                }
            }
        }
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof h)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final h h = (h)parcelable;
        super.onRestoreInstanceState(h.getSuperState());
        final SparseArray a = h.a;
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final int id = child.getId();
            final c f = this.getResolvedLayoutParams(child).f();
            if (id != -1 && f != null) {
                final Parcelable parcelable2 = (Parcelable)a.get(id);
                if (parcelable2 != null) {
                    f.onRestoreInstanceState(this, child, parcelable2);
                }
            }
        }
    }
    
    public Parcelable onSaveInstanceState() {
        final h h = new h(super.onSaveInstanceState());
        final SparseArray a = new SparseArray();
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final int id = child.getId();
            final c f = ((f)child.getLayoutParams()).f();
            if (id != -1 && f != null) {
                final Parcelable onSaveInstanceState = f.onSaveInstanceState(this, child);
                if (onSaveInstanceState != null) {
                    a.append(id, (Object)onSaveInstanceState);
                }
            }
        }
        h.a = a;
        return (Parcelable)h;
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n) {
        return this.onStartNestedScroll(view, view2, n, 0);
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n, final int n2) {
        final int childCount = this.getChildCount();
        int i = 0;
        boolean b = false;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final f f = (f)child.getLayoutParams();
                final c f2 = f.f();
                if (f2 != null) {
                    final boolean onStartNestedScroll = f2.onStartNestedScroll(this, child, view, view2, n, n2);
                    b |= onStartNestedScroll;
                    f.r(n2, onStartNestedScroll);
                }
                else {
                    f.r(n2, false);
                }
            }
            ++i;
        }
        return b;
    }
    
    public void onStopNestedScroll(final View view) {
        this.onStopNestedScroll(view, 0);
    }
    
    public void onStopNestedScroll(final View view, final int n) {
        this.mNestedScrollingParentHelper.e(view, n);
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final f f = (f)child.getLayoutParams();
            if (f.j(n)) {
                final c f2 = f.f();
                if (f2 != null) {
                    f2.onStopNestedScroll(this, child, view, n);
                }
                f.l(n);
                f.k();
            }
        }
        this.mNestedScrollingTarget = null;
    }
    
    public boolean onTouchEvent(MotionEvent obtain) {
        final int actionMasked = obtain.getActionMasked();
        int n = 0;
        int n2 = 0;
        Label_0086: {
            Label_0083: {
                int m;
                if (this.mBehaviorTouchView == null) {
                    m = (this.m(obtain, 1) ? 1 : 0);
                    if ((n = m) == 0) {
                        break Label_0083;
                    }
                }
                else {
                    m = 0;
                }
                final c f = ((f)this.mBehaviorTouchView.getLayoutParams()).f();
                n = m;
                if (f != null) {
                    final boolean onTouchEvent = f.onTouchEvent(this, this.mBehaviorTouchView, obtain);
                    n = m;
                    n2 = (onTouchEvent ? 1 : 0);
                    break Label_0086;
                }
            }
            n2 = 0;
        }
        final View mBehaviorTouchView = this.mBehaviorTouchView;
        final MotionEvent motionEvent = null;
        boolean b;
        if (mBehaviorTouchView == null) {
            b = ((n2 | (super.onTouchEvent(obtain) ? 1 : 0)) != 0x0);
            obtain = motionEvent;
        }
        else {
            b = (n2 != 0);
            obtain = motionEvent;
            if (n != 0) {
                final long uptimeMillis = SystemClock.uptimeMillis();
                obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                super.onTouchEvent(obtain);
                b = (n2 != 0);
            }
        }
        if (obtain != null) {
            obtain.recycle();
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.p(false);
        }
        return b;
    }
    
    public final void p(final boolean b) {
        final int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final c f = ((f)child.getLayoutParams()).f();
            if (f != null) {
                final long uptimeMillis = SystemClock.uptimeMillis();
                final MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                if (b) {
                    f.onInterceptTouchEvent(this, child, obtain);
                }
                else {
                    f.onTouchEvent(this, child, obtain);
                }
                obtain.recycle();
            }
        }
        for (int j = 0; j < childCount; ++j) {
            ((f)this.getChildAt(j).getLayoutParams()).m();
        }
        this.mBehaviorTouchView = null;
        this.mDisallowInterceptReset = false;
    }
    
    public void recordLastChildRect(final View view, final Rect rect) {
        ((f)view.getLayoutParams()).q(rect);
    }
    
    public void removePreDrawListener() {
        if (this.mIsAttachedToWindow && this.mOnPreDrawListener != null) {
            ((View)this).getViewTreeObserver().removeOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this.mOnPreDrawListener);
        }
        this.mNeedsPreDrawListener = false;
    }
    
    public boolean requestChildRectangleOnScreen(final View view, final Rect rect, final boolean b) {
        final c f = ((f)view.getLayoutParams()).f();
        return (f != null && f.onRequestChildRectangleOnScreen(this, view, rect, b)) || super.requestChildRectangleOnScreen(view, rect, b);
    }
    
    public void requestDisallowInterceptTouchEvent(final boolean b) {
        super.requestDisallowInterceptTouchEvent(b);
        if (b && !this.mDisallowInterceptReset) {
            this.p(false);
            this.mDisallowInterceptReset = true;
        }
    }
    
    public void setFitsSystemWindows(final boolean fitsSystemWindows) {
        super.setFitsSystemWindows(fitsSystemWindows);
        this.v();
    }
    
    public void setOnHierarchyChangeListener(final ViewGroup$OnHierarchyChangeListener mOnHierarchyChangeListener) {
        this.mOnHierarchyChangeListener = mOnHierarchyChangeListener;
    }
    
    public void setStatusBarBackground(Drawable mStatusBarBackground) {
        final Drawable mStatusBarBackground2 = this.mStatusBarBackground;
        if (mStatusBarBackground2 != mStatusBarBackground) {
            Drawable mutate = null;
            if (mStatusBarBackground2 != null) {
                mStatusBarBackground2.setCallback((Drawable$Callback)null);
            }
            if (mStatusBarBackground != null) {
                mutate = mStatusBarBackground.mutate();
            }
            if ((this.mStatusBarBackground = mutate) != null) {
                if (mutate.isStateful()) {
                    this.mStatusBarBackground.setState(((View)this).getDrawableState());
                }
                wu.m(this.mStatusBarBackground, o32.B((View)this));
                mStatusBarBackground = this.mStatusBarBackground;
                mStatusBarBackground.setVisible(((View)this).getVisibility() == 0, false);
                this.mStatusBarBackground.setCallback((Drawable$Callback)this);
            }
            o32.h0((View)this);
        }
    }
    
    public void setStatusBarBackgroundColor(final int n) {
        this.setStatusBarBackground((Drawable)new ColorDrawable(n));
    }
    
    public void setStatusBarBackgroundResource(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = sl.getDrawable(((View)this).getContext(), n);
        }
        else {
            drawable = null;
        }
        this.setStatusBarBackground(drawable);
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        final boolean b = visibility == 0;
        final Drawable mStatusBarBackground = this.mStatusBarBackground;
        if (mStatusBarBackground != null && mStatusBarBackground.isVisible() != b) {
            this.mStatusBarBackground.setVisible(b, false);
        }
    }
    
    public final u62 setWindowInsets(final u62 mLastInsets) {
        u62 d = mLastInsets;
        if (!c11.a(this.mLastInsets, mLastInsets)) {
            this.mLastInsets = mLastInsets;
            final boolean b = true;
            final boolean mDrawStatusBarBackground = mLastInsets != null && mLastInsets.k() > 0;
            this.mDrawStatusBarBackground = mDrawStatusBarBackground;
            ((View)this).setWillNotDraw(!mDrawStatusBarBackground && ((View)this).getBackground() == null && b);
            d = this.d(mLastInsets);
            ((View)this).requestLayout();
        }
        return d;
    }
    
    public final void t(final View view, final int i) {
        final f f = (f)view.getLayoutParams();
        final int j = f.i;
        if (j != i) {
            o32.a0(view, i - j);
            f.i = i;
        }
    }
    
    public final void u(final View view, final int j) {
        final f f = (f)view.getLayoutParams();
        final int i = f.j;
        if (i != j) {
            o32.b0(view, j - i);
            f.j = j;
        }
    }
    
    public final void v() {
        if (o32.y((View)this)) {
            if (this.mApplyWindowInsetsListener == null) {
                this.mApplyWindowInsetsListener = new g11(this) {
                    public final CoordinatorLayout a;
                    
                    @Override
                    public u62 onApplyWindowInsets(final View view, final u62 windowInsets) {
                        return this.a.setWindowInsets(windowInsets);
                    }
                };
            }
            o32.E0((View)this, this.mApplyWindowInsetsListener);
            ((View)this).setSystemUiVisibility(1280);
        }
        else {
            o32.E0((View)this, null);
        }
    }
    
    public boolean verifyDrawable(final Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.mStatusBarBackground;
    }
    
    public interface b
    {
        c getBehavior();
    }
    
    public abstract static class c
    {
        public c() {
        }
        
        public c(final Context context, final AttributeSet set) {
        }
        
        public static Object getTag(final View view) {
            return ((f)view.getLayoutParams()).r;
        }
        
        public static void setTag(final View view, final Object r) {
            ((f)view.getLayoutParams()).r = r;
        }
        
        public boolean blocksInteractionBelow(final CoordinatorLayout coordinatorLayout, final View view) {
            return this.getScrimOpacity(coordinatorLayout, view) > 0.0f;
        }
        
        public boolean getInsetDodgeRect(final CoordinatorLayout coordinatorLayout, final View view, final Rect rect) {
            return false;
        }
        
        public int getScrimColor(final CoordinatorLayout coordinatorLayout, final View view) {
            return -16777216;
        }
        
        public float getScrimOpacity(final CoordinatorLayout coordinatorLayout, final View view) {
            return 0.0f;
        }
        
        public boolean layoutDependsOn(final CoordinatorLayout coordinatorLayout, final View view, final View view2) {
            return false;
        }
        
        public u62 onApplyWindowInsets(final CoordinatorLayout coordinatorLayout, final View view, final u62 u62) {
            return u62;
        }
        
        public void onAttachedToLayoutParams(final f f) {
        }
        
        public boolean onDependentViewChanged(final CoordinatorLayout coordinatorLayout, final View view, final View view2) {
            return false;
        }
        
        public void onDependentViewRemoved(final CoordinatorLayout coordinatorLayout, final View view, final View view2) {
        }
        
        public void onDetachedFromLayoutParams() {
        }
        
        public boolean onInterceptTouchEvent(final CoordinatorLayout coordinatorLayout, final View view, final MotionEvent motionEvent) {
            return false;
        }
        
        public boolean onLayoutChild(final CoordinatorLayout coordinatorLayout, final View view, final int n) {
            return false;
        }
        
        public boolean onMeasureChild(final CoordinatorLayout coordinatorLayout, final View view, final int n, final int n2, final int n3, final int n4) {
            return false;
        }
        
        public boolean onNestedFling(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final float n, final float n2, final boolean b) {
            return false;
        }
        
        public boolean onNestedPreFling(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final float n, final float n2) {
            return false;
        }
        
        @Deprecated
        public void onNestedPreScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final int n, final int n2, final int[] array) {
        }
        
        public void onNestedPreScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final int n, final int n2, final int[] array, final int n3) {
            if (n3 == 0) {
                this.onNestedPreScroll(coordinatorLayout, view, view2, n, n2, array);
            }
        }
        
        @Deprecated
        public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final int n, final int n2, final int n3, final int n4) {
        }
        
        @Deprecated
        public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final int n, final int n2, final int n3, final int n4, final int n5) {
            if (n5 == 0) {
                this.onNestedScroll(coordinatorLayout, view, view2, n, n2, n3, n4);
            }
        }
        
        public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final int n, final int n2, final int n3, final int n4, final int n5, final int[] array) {
            array[0] += n3;
            array[1] += n4;
            this.onNestedScroll(coordinatorLayout, view, view2, n, n2, n3, n4, n5);
        }
        
        @Deprecated
        public void onNestedScrollAccepted(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final View view3, final int n) {
        }
        
        public void onNestedScrollAccepted(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final View view3, final int n, final int n2) {
            if (n2 == 0) {
                this.onNestedScrollAccepted(coordinatorLayout, view, view2, view3, n);
            }
        }
        
        public boolean onRequestChildRectangleOnScreen(final CoordinatorLayout coordinatorLayout, final View view, final Rect rect, final boolean b) {
            return false;
        }
        
        public void onRestoreInstanceState(final CoordinatorLayout coordinatorLayout, final View view, final Parcelable parcelable) {
        }
        
        public Parcelable onSaveInstanceState(final CoordinatorLayout coordinatorLayout, final View view) {
            return (Parcelable)View$BaseSavedState.EMPTY_STATE;
        }
        
        @Deprecated
        public boolean onStartNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final View view3, final int n) {
            return false;
        }
        
        public boolean onStartNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final View view3, final int n, final int n2) {
            return n2 == 0 && this.onStartNestedScroll(coordinatorLayout, view, view2, view3, n);
        }
        
        @Deprecated
        public void onStopNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2) {
        }
        
        public void onStopNestedScroll(final CoordinatorLayout coordinatorLayout, final View view, final View view2, final int n) {
            if (n == 0) {
                this.onStopNestedScroll(coordinatorLayout, view, view2);
            }
        }
        
        public boolean onTouchEvent(final CoordinatorLayout coordinatorLayout, final View view, final MotionEvent motionEvent) {
            return false;
        }
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    public @interface d {
        Class value();
    }
    
    public class e implements ViewGroup$OnHierarchyChangeListener
    {
        public final CoordinatorLayout a;
        
        public e(final CoordinatorLayout a) {
            this.a = a;
        }
        
        public void onChildViewAdded(final View view, final View view2) {
            final ViewGroup$OnHierarchyChangeListener mOnHierarchyChangeListener = this.a.mOnHierarchyChangeListener;
            if (mOnHierarchyChangeListener != null) {
                mOnHierarchyChangeListener.onChildViewAdded(view, view2);
            }
        }
        
        public void onChildViewRemoved(final View view, final View view2) {
            this.a.onChildViewsChanged(2);
            final ViewGroup$OnHierarchyChangeListener mOnHierarchyChangeListener = this.a.mOnHierarchyChangeListener;
            if (mOnHierarchyChangeListener != null) {
                mOnHierarchyChangeListener.onChildViewRemoved(view, view2);
            }
        }
    }
    
    public static class f extends ViewGroup$MarginLayoutParams
    {
        public c a;
        public boolean b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;
        public int j;
        public View k;
        public View l;
        public boolean m;
        public boolean n;
        public boolean o;
        public boolean p;
        public final Rect q;
        public Object r;
        
        public f(final int n, final int n2) {
            super(n, n2);
            this.b = false;
            this.c = 0;
            this.d = 0;
            this.e = -1;
            this.f = -1;
            this.g = 0;
            this.h = 0;
            this.q = new Rect();
        }
        
        public f(final Context context, final AttributeSet set) {
            super(context, set);
            this.b = false;
            this.c = 0;
            this.d = 0;
            this.e = -1;
            this.f = -1;
            this.g = 0;
            this.h = 0;
            this.q = new Rect();
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, wb1.e);
            this.c = obtainStyledAttributes.getInteger(wb1.f, 0);
            this.f = obtainStyledAttributes.getResourceId(wb1.g, -1);
            this.d = obtainStyledAttributes.getInteger(wb1.h, 0);
            this.e = obtainStyledAttributes.getInteger(wb1.l, -1);
            this.g = obtainStyledAttributes.getInt(wb1.k, 0);
            this.h = obtainStyledAttributes.getInt(wb1.j, 0);
            final int i = wb1.i;
            final boolean hasValue = obtainStyledAttributes.hasValue(i);
            this.b = hasValue;
            if (hasValue) {
                this.a = CoordinatorLayout.parseBehavior(context, set, obtainStyledAttributes.getString(i));
            }
            obtainStyledAttributes.recycle();
            final c a = this.a;
            if (a != null) {
                a.onAttachedToLayoutParams(this);
            }
        }
        
        public f(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.b = false;
            this.c = 0;
            this.d = 0;
            this.e = -1;
            this.f = -1;
            this.g = 0;
            this.h = 0;
            this.q = new Rect();
        }
        
        public f(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.b = false;
            this.c = 0;
            this.d = 0;
            this.e = -1;
            this.f = -1;
            this.g = 0;
            this.h = 0;
            this.q = new Rect();
        }
        
        public f(final f f) {
            super((ViewGroup$MarginLayoutParams)f);
            this.b = false;
            this.c = 0;
            this.d = 0;
            this.e = -1;
            this.f = -1;
            this.g = 0;
            this.h = 0;
            this.q = new Rect();
        }
        
        public boolean a() {
            return this.k == null && this.f != -1;
        }
        
        public boolean b(final CoordinatorLayout coordinatorLayout, final View view, final View view2) {
            if (view2 != this.l && !this.s(view2, o32.B((View)coordinatorLayout))) {
                final c a = this.a;
                if (a == null || !a.layoutDependsOn(coordinatorLayout, view, view2)) {
                    return false;
                }
            }
            return true;
        }
        
        public boolean c() {
            if (this.a == null) {
                this.m = false;
            }
            return this.m;
        }
        
        public View d(final CoordinatorLayout coordinatorLayout, final View view) {
            if (this.f == -1) {
                this.l = null;
                return this.k = null;
            }
            if (this.k == null || !this.t(view, coordinatorLayout)) {
                this.n(view, coordinatorLayout);
            }
            return this.k;
        }
        
        public int e() {
            return this.f;
        }
        
        public c f() {
            return this.a;
        }
        
        public boolean g() {
            return this.p;
        }
        
        public Rect h() {
            return this.q;
        }
        
        public boolean i(final CoordinatorLayout coordinatorLayout, final View view) {
            final boolean m = this.m;
            if (m) {
                return true;
            }
            final c a = this.a;
            return this.m = ((a != null && a.blocksInteractionBelow(coordinatorLayout, view)) | m);
        }
        
        public boolean j(final int n) {
            if (n != 0) {
                return n == 1 && this.o;
            }
            return this.n;
        }
        
        public void k() {
            this.p = false;
        }
        
        public void l(final int n) {
            this.r(n, false);
        }
        
        public void m() {
            this.m = false;
        }
        
        public final void n(final View obj, final CoordinatorLayout coordinatorLayout) {
            View viewById = ((View)coordinatorLayout).findViewById(this.f);
            this.k = viewById;
            Label_0034: {
                if (viewById != null) {
                    if (viewById != coordinatorLayout) {
                        ViewParent viewParent = viewById.getParent();
                        while (viewParent != coordinatorLayout && viewParent != null) {
                            if (viewParent == obj) {
                                if (((View)coordinatorLayout).isInEditMode()) {
                                    break Label_0034;
                                }
                                throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                            }
                            else {
                                if (viewParent instanceof View) {
                                    viewById = (View)viewParent;
                                }
                                viewParent = viewParent.getParent();
                            }
                        }
                        this.l = viewById;
                        return;
                    }
                    if (!((View)coordinatorLayout).isInEditMode()) {
                        throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
                    }
                }
                else if (!((View)coordinatorLayout).isInEditMode()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Could not find CoordinatorLayout descendant view with id ");
                    sb.append(((View)coordinatorLayout).getResources().getResourceName(this.f));
                    sb.append(" to anchor view ");
                    sb.append(obj);
                    throw new IllegalStateException(sb.toString());
                }
            }
            this.l = null;
            this.k = null;
        }
        
        public void o(final c a) {
            final c a2 = this.a;
            if (a2 != a) {
                if (a2 != null) {
                    a2.onDetachedFromLayoutParams();
                }
                this.a = a;
                this.r = null;
                this.b = true;
                if (a != null) {
                    a.onAttachedToLayoutParams(this);
                }
            }
        }
        
        public void p(final boolean p) {
            this.p = p;
        }
        
        public void q(final Rect rect) {
            this.q.set(rect);
        }
        
        public void r(final int n, final boolean b) {
            if (n != 0) {
                if (n == 1) {
                    this.o = b;
                }
            }
            else {
                this.n = b;
            }
        }
        
        public final boolean s(final View view, final int n) {
            final int b = pb0.b(((f)view.getLayoutParams()).g, n);
            return b != 0 && (pb0.b(this.h, n) & b) == b;
        }
        
        public final boolean t(final View view, final CoordinatorLayout coordinatorLayout) {
            if (this.k.getId() != this.f) {
                return false;
            }
            View k = this.k;
            for (ViewParent viewParent = k.getParent(); viewParent != coordinatorLayout; viewParent = viewParent.getParent()) {
                if (viewParent == null || viewParent == view) {
                    this.l = null;
                    this.k = null;
                    return false;
                }
                if (viewParent instanceof View) {
                    k = (View)viewParent;
                }
            }
            this.l = k;
            return true;
        }
    }
    
    public class g implements ViewTreeObserver$OnPreDrawListener
    {
        public final CoordinatorLayout a;
        
        public g(final CoordinatorLayout a) {
            this.a = a;
        }
        
        public boolean onPreDraw() {
            this.a.onChildViewsChanged(0);
            return true;
        }
    }
    
    public static class h extends e
    {
        public static final Parcelable$Creator<h> CREATOR;
        public SparseArray a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator() {
                public h a(final Parcel parcel) {
                    return new h(parcel, null);
                }
                
                public h b(final Parcel parcel, final ClassLoader classLoader) {
                    return new h(parcel, classLoader);
                }
                
                public h[] c(final int n) {
                    return new h[n];
                }
            };
        }
        
        public h(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            final int int1 = parcel.readInt();
            final int[] array = new int[int1];
            parcel.readIntArray(array);
            final Parcelable[] parcelableArray = parcel.readParcelableArray(classLoader);
            this.a = new SparseArray(int1);
            for (int i = 0; i < int1; ++i) {
                this.a.append(array[i], (Object)parcelableArray[i]);
            }
        }
        
        public h(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            final SparseArray a = this.a;
            int i = 0;
            int size;
            if (a != null) {
                size = a.size();
            }
            else {
                size = 0;
            }
            parcel.writeInt(size);
            final int[] array = new int[size];
            final Parcelable[] array2 = new Parcelable[size];
            while (i < size) {
                array[i] = this.a.keyAt(i);
                array2[i] = (Parcelable)this.a.valueAt(i);
                ++i;
            }
            parcel.writeIntArray(array);
            parcel.writeParcelableArray(array2, n);
        }
    }
    
    public static class i implements Comparator
    {
        public int a(final View view, final View view2) {
            final float n = o32.N(view);
            final float n2 = o32.N(view2);
            if (n > n2) {
                return -1;
            }
            if (n < n2) {
                return 1;
            }
            return 0;
        }
    }
}

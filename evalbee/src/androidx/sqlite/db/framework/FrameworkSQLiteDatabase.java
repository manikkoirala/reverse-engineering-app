// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import android.database.sqlite.SQLiteClosable;
import java.util.Locale;
import java.util.Iterator;
import android.text.TextUtils;
import android.database.sqlite.SQLiteStatement;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.os.CancellationSignal;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteProgram;
import android.database.Cursor;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteCursorDriver;
import java.util.List;
import android.database.sqlite.SQLiteDatabase;

public final class FrameworkSQLiteDatabase implements ss1
{
    public static final a c;
    public static final String[] d;
    public static final String[] e;
    public final SQLiteDatabase a;
    public final List b;
    
    static {
        c = new a(null);
        d = new String[] { "", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE " };
        e = new String[0];
    }
    
    public FrameworkSQLiteDatabase(final SQLiteDatabase a) {
        fg0.e((Object)a, "delegate");
        this.a = a;
        this.b = a.getAttachedDbs();
    }
    
    public static final Cursor d(final u90 u90, final SQLiteDatabase sqLiteDatabase, final SQLiteCursorDriver sqLiteCursorDriver, final String s, final SQLiteQuery sqLiteQuery) {
        fg0.e((Object)u90, "$tmp0");
        return (Cursor)u90.invoke((Object)sqLiteDatabase, (Object)sqLiteCursorDriver, (Object)s, (Object)sqLiteQuery);
    }
    
    public static final Cursor e(final vs1 vs1, final SQLiteDatabase sqLiteDatabase, final SQLiteCursorDriver sqLiteCursorDriver, final String s, final SQLiteQuery sqLiteQuery) {
        fg0.e((Object)vs1, "$query");
        fg0.b((Object)sqLiteQuery);
        vs1.b(new s80((SQLiteProgram)sqLiteQuery));
        return (Cursor)new SQLiteCursor(sqLiteCursorDriver, s, sqLiteQuery);
    }
    
    @Override
    public Cursor B(final vs1 vs1, final CancellationSignal cancellationSignal) {
        fg0.e((Object)vs1, "query");
        final SQLiteDatabase a = this.a;
        final String a2 = vs1.a();
        final String[] e = FrameworkSQLiteDatabase.e;
        fg0.b((Object)cancellationSignal);
        return ns1.d(a, a2, e, null, cancellationSignal, (SQLiteDatabase$CursorFactory)new o80(vs1));
    }
    
    @Override
    public long C() {
        return this.a.getMaximumSize();
    }
    
    @Override
    public long G(final String s, final int n, final ContentValues contentValues) {
        fg0.e((Object)s, "table");
        fg0.e((Object)contentValues, "values");
        return this.a.insertWithOnConflict(s, (String)null, contentValues, n);
    }
    
    @Override
    public Cursor L(final vs1 vs1) {
        fg0.e((Object)vs1, "query");
        final Cursor rawQueryWithFactory = this.a.rawQueryWithFactory((SQLiteDatabase$CursorFactory)new p80((u90)new FrameworkSQLiteDatabase$query$cursorFactory.FrameworkSQLiteDatabase$query$cursorFactory$1(vs1)), vs1.a(), FrameworkSQLiteDatabase.e, (String)null);
        fg0.d((Object)rawQueryWithFactory, "delegate.rawQueryWithFac\u2026EMPTY_STRING_ARRAY, null)");
        return rawQueryWithFactory;
    }
    
    @Override
    public void M(final String s) {
        fg0.e((Object)s, "sql");
        this.a.execSQL(s);
    }
    
    @Override
    public boolean N() {
        return this.a.isDatabaseIntegrityOk();
    }
    
    @Override
    public void S() {
        this.a.setTransactionSuccessful();
    }
    
    @Override
    public void T(final String s, final Object[] array) {
        fg0.e((Object)s, "sql");
        fg0.e((Object)array, "bindArgs");
        this.a.execSQL(s, array);
    }
    
    @Override
    public long U(final long maximumSize) {
        this.a.setMaximumSize(maximumSize);
        return this.a.getMaximumSize();
    }
    
    @Override
    public void X() {
        this.a.endTransaction();
    }
    
    public final boolean c(final SQLiteDatabase sqLiteDatabase) {
        fg0.e((Object)sqLiteDatabase, "sqLiteDatabase");
        return fg0.a((Object)this.a, (Object)sqLiteDatabase);
    }
    
    @Override
    public void c0(final int version) {
        this.a.setVersion(version);
    }
    
    @Override
    public void close() {
        ((SQLiteClosable)this.a).close();
    }
    
    @Override
    public ws1 d0(final String s) {
        fg0.e((Object)s, "sql");
        final SQLiteStatement compileStatement = this.a.compileStatement(s);
        fg0.d((Object)compileStatement, "delegate.compileStatement(sql)");
        return new t80(compileStatement);
    }
    
    @Override
    public int f0(String string, int i, final ContentValues contentValues, final String str, final Object[] array) {
        fg0.e((Object)string, "table");
        fg0.e((Object)contentValues, "values");
        final int size = contentValues.size();
        final int n = 0;
        if (size != 0) {
            final int size2 = contentValues.size();
            int n2;
            if (array == null) {
                n2 = size2;
            }
            else {
                n2 = array.length + size2;
            }
            final Object[] array2 = new Object[n2];
            final StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append(FrameworkSQLiteDatabase.d[i]);
            sb.append(string);
            sb.append(" SET ");
            final Iterator iterator = contentValues.keySet().iterator();
            i = n;
            while (iterator.hasNext()) {
                final String str2 = (String)iterator.next();
                if (i > 0) {
                    string = ",";
                }
                else {
                    string = "";
                }
                sb.append(string);
                sb.append(str2);
                array2[i] = contentValues.get(str2);
                sb.append("=?");
                ++i;
            }
            if (array != null) {
                for (i = size2; i < n2; ++i) {
                    array2[i] = array[i - size2];
                }
            }
            if (!TextUtils.isEmpty((CharSequence)str)) {
                sb.append(" WHERE ");
                sb.append(str);
            }
            string = sb.toString();
            fg0.d((Object)string, "StringBuilder().apply(builderAction).toString()");
            final ws1 d0 = this.d0(string);
            do1.c.b(d0, array2);
            return d0.p();
        }
        throw new IllegalArgumentException("Empty values".toString());
    }
    
    @Override
    public boolean g0() {
        return this.a.yieldIfContendedSafely();
    }
    
    @Override
    public long getPageSize() {
        return this.a.getPageSize();
    }
    
    @Override
    public String getPath() {
        return this.a.getPath();
    }
    
    @Override
    public int getVersion() {
        return this.a.getVersion();
    }
    
    @Override
    public Cursor h0(final String s) {
        fg0.e((Object)s, "query");
        return this.L(new do1(s));
    }
    
    @Override
    public boolean isOpen() {
        return this.a.isOpen();
    }
    
    @Override
    public boolean isReadOnly() {
        return this.a.isReadOnly();
    }
    
    @Override
    public boolean j0() {
        return this.a.inTransaction();
    }
    
    @Override
    public boolean k0() {
        return ns1.c(this.a);
    }
    
    @Override
    public int l(String string, final String str, final Object[] array) {
        fg0.e((Object)string, "table");
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(string);
        if (str != null && str.length() != 0) {
            sb.append(" WHERE ");
            sb.append(str);
        }
        string = sb.toString();
        fg0.d((Object)string, "StringBuilder().apply(builderAction).toString()");
        final ws1 d0 = this.d0(string);
        do1.c.b(d0, array);
        return d0.p();
    }
    
    @Override
    public void l0(final int maxSqlCacheSize) {
        this.a.setMaxSqlCacheSize(maxSqlCacheSize);
    }
    
    @Override
    public void m() {
        this.a.beginTransaction();
    }
    
    @Override
    public List n() {
        return this.b;
    }
    
    @Override
    public void n0(final long pageSize) {
        this.a.setPageSize(pageSize);
    }
    
    @Override
    public void r() {
        this.a.beginTransactionNonExclusive();
    }
    
    @Override
    public boolean s() {
        return this.a.isDbLockedByCurrentThread();
    }
    
    @Override
    public void setLocale(final Locale locale) {
        fg0.e((Object)locale, "locale");
        this.a.setLocale(locale);
    }
    
    @Override
    public boolean t(final int n) {
        return this.a.needUpgrade(n);
    }
    
    @Override
    public void z(final boolean b) {
        ns1.e(this.a, b);
    }
    
    public static final class a
    {
    }
}

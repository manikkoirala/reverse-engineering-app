// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import org.jetbrains.annotations.NotNull;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import java.util.UUID;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import kotlin.a;
import android.content.Context;

public final class FrameworkSQLiteOpenHelper implements ts1
{
    public static final a h;
    public final Context a;
    public final String b;
    public final ts1.a c;
    public final boolean d;
    public final boolean e;
    public final xi0 f;
    public boolean g;
    
    static {
        h = new a(null);
    }
    
    public FrameworkSQLiteOpenHelper(final Context a, final String b, final ts1.a c, final boolean d, final boolean e) {
        fg0.e((Object)a, "context");
        fg0.e((Object)c, "callback");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = kotlin.a.a((a90)new FrameworkSQLiteOpenHelper$lazyDelegate.FrameworkSQLiteOpenHelper$lazyDelegate$1(this));
    }
    
    @Override
    public ss1 F() {
        return this.g().c(true);
    }
    
    @Override
    public void close() {
        if (this.f.isInitialized()) {
            this.g().close();
        }
    }
    
    public final OpenHelper g() {
        return (OpenHelper)this.f.getValue();
    }
    
    @Override
    public String getDatabaseName() {
        return this.b;
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean g) {
        if (this.f.isInitialized()) {
            ns1.f(this.g(), g);
        }
        this.g = g;
    }
    
    public static final class OpenHelper extends SQLiteOpenHelper
    {
        public static final a h;
        public final Context a;
        public final FrameworkSQLiteOpenHelper.b b;
        public final ts1.a c;
        public final boolean d;
        public boolean e;
        public final k81 f;
        public boolean g;
        
        static {
            h = new a(null);
        }
        
        public OpenHelper(final Context a, final String s, final FrameworkSQLiteOpenHelper.b b, final ts1.a c, final boolean d) {
            fg0.e((Object)a, "context");
            fg0.e((Object)b, "dbRef");
            fg0.e((Object)c, "callback");
            super(a, s, (SQLiteDatabase$CursorFactory)null, c.a, (DatabaseErrorHandler)new q80(c, b));
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            String string = s;
            if (s == null) {
                string = UUID.randomUUID().toString();
                fg0.d((Object)string, "randomUUID().toString()");
            }
            final File cacheDir = a.getCacheDir();
            fg0.d((Object)cacheDir, "context.cacheDir");
            this.f = new k81(string, cacheDir, false);
        }
        
        public static final void b(final ts1.a a, final FrameworkSQLiteOpenHelper.b b, final SQLiteDatabase sqLiteDatabase) {
            fg0.e((Object)a, "$callback");
            fg0.e((Object)b, "$dbRef");
            final a h = OpenHelper.h;
            fg0.d((Object)sqLiteDatabase, "dbObj");
            a.c(h.a(b, sqLiteDatabase));
        }
        
        public final ss1 c(final boolean b) {
            try {
                this.f.b(!this.g && this.getDatabaseName() != null);
                this.e = false;
                final SQLiteDatabase f = this.f(b);
                if (this.e) {
                    this.close();
                    return this.c(b);
                }
                return this.d(f);
            }
            finally {
                this.f.d();
            }
        }
        
        public void close() {
            try {
                k81.c(this.f, false, 1, null);
                super.close();
                this.b.b(null);
                this.g = false;
            }
            finally {
                this.f.d();
            }
        }
        
        public final FrameworkSQLiteDatabase d(final SQLiteDatabase sqLiteDatabase) {
            fg0.e((Object)sqLiteDatabase, "sqLiteDatabase");
            return OpenHelper.h.a(this.b, sqLiteDatabase);
        }
        
        public final SQLiteDatabase e(final boolean b) {
            SQLiteDatabase sqLiteDatabase;
            if (b) {
                sqLiteDatabase = super.getWritableDatabase();
            }
            else {
                sqLiteDatabase = super.getReadableDatabase();
            }
            fg0.d((Object)sqLiteDatabase, "{\n                super.\u2026eDatabase()\n            }");
            return sqLiteDatabase;
        }
        
        public final SQLiteDatabase f(final boolean b) {
            final String databaseName = this.getDatabaseName();
            if (databaseName != null) {
                final File parentFile = this.a.getDatabasePath(databaseName).getParentFile();
                if (parentFile != null) {
                    parentFile.mkdirs();
                    if (!parentFile.isDirectory()) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid database parent file, not a directory: ");
                        sb.append(parentFile);
                        Log.w("SupportSQLite", sb.toString());
                    }
                }
            }
            try {
                return this.e(b);
            }
            finally {
                super.close();
                try {
                    Thread.sleep(500L);
                    try {
                        return this.e(b);
                    }
                    finally {
                        super.close();
                        final CallbackException ex;
                        if (ex instanceof CallbackException) {
                            final CallbackException ex2 = ex;
                            final Throwable cause = ex2.getCause();
                            final int n = OpenHelper.b.a[ex2.getCallbackName().ordinal()];
                            if (n == 1 || n == 2 || n == 3 || n == 4) {
                                throw cause;
                            }
                            if (!(cause instanceof SQLiteException)) {
                                throw cause;
                            }
                        }
                        else if (ex instanceof SQLiteException && (databaseName != null && this.d)) {}
                        this.a.deleteDatabase(databaseName);
                        try {
                            return this.e(b);
                        }
                        catch (final CallbackException ex3) {
                            throw ex3.getCause();
                        }
                    }
                }
                catch (final InterruptedException ex4) {}
            }
        }
        
        public void onConfigure(final SQLiteDatabase sqLiteDatabase) {
            fg0.e((Object)sqLiteDatabase, "db");
            try {
                this.c.b(this.d(sqLiteDatabase));
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_CONFIGURE, t);
            }
        }
        
        public void onCreate(final SQLiteDatabase sqLiteDatabase) {
            fg0.e((Object)sqLiteDatabase, "sqLiteDatabase");
            try {
                this.c.d(this.d(sqLiteDatabase));
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_CREATE, t);
            }
        }
        
        public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            fg0.e((Object)sqLiteDatabase, "db");
            this.e = true;
            try {
                this.c.e(this.d(sqLiteDatabase), n, n2);
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_DOWNGRADE, t);
            }
        }
        
        public void onOpen(final SQLiteDatabase sqLiteDatabase) {
            fg0.e((Object)sqLiteDatabase, "db");
            if (!this.e) {
                try {
                    this.c.f(this.d(sqLiteDatabase));
                }
                finally {
                    final Throwable t;
                    throw new CallbackException(CallbackName.ON_OPEN, t);
                }
            }
            this.g = true;
        }
        
        public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            fg0.e((Object)sqLiteDatabase, "sqLiteDatabase");
            this.e = true;
            try {
                this.c.g(this.d(sqLiteDatabase), n, n2);
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_UPGRADE, t);
            }
        }
        
        public static final class CallbackException extends RuntimeException
        {
            @NotNull
            private final CallbackName callbackName;
            @NotNull
            private final Throwable cause;
            
            public CallbackException(@NotNull final CallbackName callbackName, @NotNull final Throwable t) {
                fg0.e((Object)callbackName, "callbackName");
                fg0.e((Object)t, "cause");
                super(t);
                this.callbackName = callbackName;
                this.cause = t;
            }
            
            @NotNull
            public final CallbackName getCallbackName() {
                return this.callbackName;
            }
            
            @NotNull
            @Override
            public Throwable getCause() {
                return this.cause;
            }
        }
        
        public enum CallbackName
        {
            private static final CallbackName[] $VALUES;
            
            ON_CONFIGURE, 
            ON_CREATE, 
            ON_DOWNGRADE, 
            ON_OPEN, 
            ON_UPGRADE;
            
            private static final /* synthetic */ CallbackName[] $values() {
                return new CallbackName[] { CallbackName.ON_CONFIGURE, CallbackName.ON_CREATE, CallbackName.ON_UPGRADE, CallbackName.ON_DOWNGRADE, CallbackName.ON_OPEN };
            }
            
            static {
                $VALUES = $values();
            }
        }
        
        public static final class a
        {
            public final FrameworkSQLiteDatabase a(final FrameworkSQLiteOpenHelper.b b, final SQLiteDatabase sqLiteDatabase) {
                fg0.e((Object)b, "refHolder");
                fg0.e((Object)sqLiteDatabase, "sqLiteDatabase");
                final FrameworkSQLiteDatabase a = b.a();
                if (a != null) {
                    final FrameworkSQLiteDatabase frameworkSQLiteDatabase = a;
                    if (a.c(sqLiteDatabase)) {
                        return frameworkSQLiteDatabase;
                    }
                }
                final FrameworkSQLiteDatabase frameworkSQLiteDatabase = new FrameworkSQLiteDatabase(sqLiteDatabase);
                b.b(frameworkSQLiteDatabase);
                return frameworkSQLiteDatabase;
            }
        }
    }
    
    public static final class a
    {
    }
    
    public static final class b
    {
        public FrameworkSQLiteDatabase a;
        
        public b(final FrameworkSQLiteDatabase a) {
            this.a = a;
        }
        
        public final FrameworkSQLiteDatabase a() {
            return this.a;
        }
        
        public final void b(final FrameworkSQLiteDatabase a) {
            this.a = a;
        }
    }
}

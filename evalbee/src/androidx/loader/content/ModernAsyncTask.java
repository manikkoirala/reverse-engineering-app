// 
// Decompiled by Procyon v0.6.0
// 

package androidx.loader.content;

import android.os.Message;
import android.os.Looper;
import android.os.Handler;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Callable;
import android.os.Binder;
import android.os.Process;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Executor;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;

public abstract class ModernAsyncTask
{
    public static final ThreadFactory f;
    public static final BlockingQueue g;
    public static final Executor h;
    public static f i;
    public static volatile Executor j;
    public final g a;
    public final FutureTask b;
    public volatile Status c;
    public final AtomicBoolean d;
    public final AtomicBoolean e;
    
    static {
        ModernAsyncTask.j = (h = new ThreadPoolExecutor(5, 128, 1L, TimeUnit.SECONDS, g = new LinkedBlockingQueue(10), f = new ThreadFactory() {
            public final AtomicInteger a = new AtomicInteger(1);
            
            @Override
            public Thread newThread(final Runnable target) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ModernAsyncTask #");
                sb.append(this.a.getAndIncrement());
                return new Thread(target, sb.toString());
            }
        }));
    }
    
    public ModernAsyncTask() {
        this.c = Status.PENDING;
        this.d = new AtomicBoolean();
        this.e = new AtomicBoolean();
        final g a = new g(this) {
            public final ModernAsyncTask b;
            
            @Override
            public Object call() {
                this.b.e.set(true);
                Object b = null;
                try {
                    Process.setThreadPriority(10);
                    b = b;
                    final Object o = b = this.b.b(super.a);
                    Binder.flushPendingCommands();
                    this.b.k(o);
                    return o;
                }
                finally {
                    try {
                        this.b.d.set(true);
                    }
                    finally {
                        this.b.k(b);
                    }
                }
            }
        };
        this.a = (g)a;
        this.b = new FutureTask(this, a) {
            public final ModernAsyncTask a;
            
            public void done() {
                try {
                    this.a.l(this.get());
                }
                catch (final CancellationException ex) {
                    this.a.l(null);
                }
                catch (final ExecutionException ex2) {
                    throw new RuntimeException("An error occurred while executing doInBackground()", ex2.getCause());
                }
                catch (final InterruptedException ex3) {
                    Log.w("AsyncTask", (Throwable)ex3);
                }
                finally {
                    final Throwable cause;
                    throw new RuntimeException("An error occurred while executing doInBackground()", cause);
                }
            }
        };
    }
    
    public static Handler e() {
        synchronized (ModernAsyncTask.class) {
            if (ModernAsyncTask.i == null) {
                ModernAsyncTask.i = new f();
            }
            return ModernAsyncTask.i;
        }
    }
    
    public final boolean a(final boolean mayInterruptIfRunning) {
        this.d.set(true);
        return this.b.cancel(mayInterruptIfRunning);
    }
    
    public abstract Object b(final Object... p0);
    
    public final ModernAsyncTask c(final Executor executor, final Object... a) {
        if (this.c == Status.PENDING) {
            this.c = Status.RUNNING;
            this.i();
            this.a.a = a;
            executor.execute(this.b);
            return this;
        }
        final int n = ModernAsyncTask$d.a[this.c.ordinal()];
        if (n == 1) {
            throw new IllegalStateException("Cannot execute task: the task is already running.");
        }
        if (n != 2) {
            throw new IllegalStateException("We should never reach this state");
        }
        throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
    }
    
    public void d(final Object o) {
        if (this.f()) {
            this.g(o);
        }
        else {
            this.h(o);
        }
        this.c = Status.FINISHED;
    }
    
    public final boolean f() {
        return this.d.get();
    }
    
    public abstract void g(final Object p0);
    
    public abstract void h(final Object p0);
    
    public void i() {
    }
    
    public void j(final Object... array) {
    }
    
    public Object k(final Object o) {
        e().obtainMessage(1, (Object)new e(this, new Object[] { o })).sendToTarget();
        return o;
    }
    
    public void l(final Object o) {
        if (!this.e.get()) {
            this.k(o);
        }
    }
    
    public enum Status
    {
        private static final Status[] $VALUES;
        
        FINISHED, 
        PENDING, 
        RUNNING;
    }
    
    public static class e
    {
        public final ModernAsyncTask a;
        public final Object[] b;
        
        public e(final ModernAsyncTask a, final Object... b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static class f extends Handler
    {
        public f() {
            super(Looper.getMainLooper());
        }
        
        public void handleMessage(final Message message) {
            final e e = (e)message.obj;
            final int what = message.what;
            if (what != 1) {
                if (what == 2) {
                    e.a.j(e.b);
                }
            }
            else {
                e.a.d(e.b[0]);
            }
        }
    }
    
    public abstract static class g implements Callable
    {
        public Object[] a;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import java.util.Iterator;
import java.util.Collection;
import android.animation.ValueAnimator;
import java.util.List;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.view.ViewPropertyAnimator;
import android.view.View;
import android.animation.AnimatorListenerAdapter;
import java.util.ArrayList;
import android.animation.TimeInterpolator;

public class c extends m
{
    public static TimeInterpolator s;
    public ArrayList h;
    public ArrayList i;
    public ArrayList j;
    public ArrayList k;
    public ArrayList l;
    public ArrayList m;
    public ArrayList n;
    public ArrayList o;
    public ArrayList p;
    public ArrayList q;
    public ArrayList r;
    
    public c() {
        this.h = new ArrayList();
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.l = new ArrayList();
        this.m = new ArrayList();
        this.n = new ArrayList();
        this.o = new ArrayList();
        this.p = new ArrayList();
        this.q = new ArrayList();
        this.r = new ArrayList();
    }
    
    public void Q(final d0 e) {
        final View itemView = e.itemView;
        final ViewPropertyAnimator animate = itemView.animate();
        this.o.add(e);
        animate.alpha(1.0f).setDuration(((RecyclerView.l)this).l()).setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, e, itemView, animate) {
            public final d0 a;
            public final View b;
            public final ViewPropertyAnimator c;
            public final c d;
            
            public void onAnimationCancel(final Animator animator) {
                this.b.setAlpha(1.0f);
            }
            
            public void onAnimationEnd(final Animator animator) {
                this.c.setListener((Animator$AnimatorListener)null);
                this.d.A(this.a);
                this.d.o.remove(this.a);
                this.d.V();
            }
            
            public void onAnimationStart(final Animator animator) {
                this.d.B(this.a);
            }
        }).start();
    }
    
    public void R(final i i) {
        final d0 a = i.a;
        View itemView = null;
        View itemView2;
        if (a == null) {
            itemView2 = null;
        }
        else {
            itemView2 = a.itemView;
        }
        final d0 b = i.b;
        if (b != null) {
            itemView = b.itemView;
        }
        if (itemView2 != null) {
            final ViewPropertyAnimator setDuration = itemView2.animate().setDuration(((RecyclerView.l)this).m());
            this.r.add(i.a);
            setDuration.translationX((float)(i.e - i.c));
            setDuration.translationY((float)(i.f - i.d));
            setDuration.alpha(0.0f).setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, i, setDuration, itemView2) {
                public final i a;
                public final ViewPropertyAnimator b;
                public final View c;
                public final c d;
                
                public void onAnimationEnd(final Animator animator) {
                    this.b.setListener((Animator$AnimatorListener)null);
                    this.c.setAlpha(1.0f);
                    this.c.setTranslationX(0.0f);
                    this.c.setTranslationY(0.0f);
                    this.d.C(this.a.a, true);
                    this.d.r.remove(this.a.a);
                    this.d.V();
                }
                
                public void onAnimationStart(final Animator animator) {
                    this.d.D(this.a.a, true);
                }
            }).start();
        }
        if (itemView != null) {
            final ViewPropertyAnimator animate = itemView.animate();
            this.r.add(i.b);
            animate.translationX(0.0f).translationY(0.0f).setDuration(((RecyclerView.l)this).m()).alpha(1.0f).setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, i, animate, itemView) {
                public final i a;
                public final ViewPropertyAnimator b;
                public final View c;
                public final c d;
                
                public void onAnimationEnd(final Animator animator) {
                    this.b.setListener((Animator$AnimatorListener)null);
                    this.c.setAlpha(1.0f);
                    this.c.setTranslationX(0.0f);
                    this.c.setTranslationY(0.0f);
                    this.d.C(this.a.b, false);
                    this.d.r.remove(this.a.b);
                    this.d.V();
                }
                
                public void onAnimationStart(final Animator animator) {
                    this.d.D(this.a.b, false);
                }
            }).start();
        }
    }
    
    public void S(final d0 e, int n, int n2, final int n3, final int n4) {
        final View itemView = e.itemView;
        n = n3 - n;
        n2 = n4 - n2;
        if (n != 0) {
            itemView.animate().translationX(0.0f);
        }
        if (n2 != 0) {
            itemView.animate().translationY(0.0f);
        }
        final ViewPropertyAnimator animate = itemView.animate();
        this.p.add(e);
        animate.setDuration(((RecyclerView.l)this).n()).setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, e, n, itemView, n2, animate) {
            public final d0 a;
            public final int b;
            public final View c;
            public final int d;
            public final ViewPropertyAnimator e;
            public final c f;
            
            public void onAnimationCancel(final Animator animator) {
                if (this.b != 0) {
                    this.c.setTranslationX(0.0f);
                }
                if (this.d != 0) {
                    this.c.setTranslationY(0.0f);
                }
            }
            
            public void onAnimationEnd(final Animator animator) {
                this.e.setListener((Animator$AnimatorListener)null);
                this.f.E(this.a);
                this.f.p.remove(this.a);
                this.f.V();
            }
            
            public void onAnimationStart(final Animator animator) {
                this.f.F(this.a);
            }
        }).start();
    }
    
    public final void T(final d0 e) {
        final View itemView = e.itemView;
        final ViewPropertyAnimator animate = itemView.animate();
        this.q.add(e);
        animate.setDuration(((RecyclerView.l)this).o()).alpha(0.0f).setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, e, animate, itemView) {
            public final d0 a;
            public final ViewPropertyAnimator b;
            public final View c;
            public final c d;
            
            public void onAnimationEnd(final Animator animator) {
                this.b.setListener((Animator$AnimatorListener)null);
                this.c.setAlpha(1.0f);
                this.d.G(this.a);
                this.d.q.remove(this.a);
                this.d.V();
            }
            
            public void onAnimationStart(final Animator animator) {
                this.d.H(this.a);
            }
        }).start();
    }
    
    public void U(final List list) {
        for (int i = list.size() - 1; i >= 0; --i) {
            ((d0)list.get(i)).itemView.animate().cancel();
        }
    }
    
    public void V() {
        if (!this.p()) {
            ((RecyclerView.l)this).i();
        }
    }
    
    public final void W(final List list, final d0 d0) {
        for (int i = list.size() - 1; i >= 0; --i) {
            final i j = list.get(i);
            if (this.Y(j, d0) && j.a == null && j.b == null) {
                list.remove(j);
            }
        }
    }
    
    public final void X(final i i) {
        final d0 a = i.a;
        if (a != null) {
            this.Y(i, a);
        }
        final d0 b = i.b;
        if (b != null) {
            this.Y(i, b);
        }
    }
    
    public final boolean Y(final i i, final d0 d0) {
        final d0 b = i.b;
        boolean b2 = false;
        if (b == d0) {
            i.b = null;
        }
        else {
            if (i.a != d0) {
                return false;
            }
            i.a = null;
            b2 = true;
        }
        d0.itemView.setAlpha(1.0f);
        d0.itemView.setTranslationX(0.0f);
        d0.itemView.setTranslationY(0.0f);
        this.C(d0, b2);
        return true;
    }
    
    public final void Z(final d0 d0) {
        if (c.s == null) {
            c.s = new ValueAnimator().getInterpolator();
        }
        d0.itemView.animate().setInterpolator(c.s);
        this.j(d0);
    }
    
    @Override
    public boolean g(final d0 d0, final List list) {
        return !list.isEmpty() || super.g(d0, list);
    }
    
    @Override
    public void j(final d0 o) {
        final View itemView = o.itemView;
        itemView.animate().cancel();
        for (int i = this.j.size() - 1; i >= 0; --i) {
            if (((j)this.j.get(i)).a == o) {
                itemView.setTranslationY(0.0f);
                itemView.setTranslationX(0.0f);
                this.E(o);
                this.j.remove(i);
            }
        }
        this.W(this.k, o);
        if (this.h.remove(o)) {
            itemView.setAlpha(1.0f);
            this.G(o);
        }
        if (this.i.remove(o)) {
            itemView.setAlpha(1.0f);
            this.A(o);
        }
        for (int j = this.n.size() - 1; j >= 0; --j) {
            final ArrayList list = this.n.get(j);
            this.W(list, o);
            if (list.isEmpty()) {
                this.n.remove(j);
            }
        }
        for (int k = this.m.size() - 1; k >= 0; --k) {
            final ArrayList list2 = this.m.get(k);
            int l = list2.size() - 1;
            while (l >= 0) {
                if (((j)list2.get(l)).a == o) {
                    itemView.setTranslationY(0.0f);
                    itemView.setTranslationX(0.0f);
                    this.E(o);
                    list2.remove(l);
                    if (list2.isEmpty()) {
                        this.m.remove(k);
                        break;
                    }
                    break;
                }
                else {
                    --l;
                }
            }
        }
        for (int n = this.l.size() - 1; n >= 0; --n) {
            final ArrayList list3 = this.l.get(n);
            if (list3.remove(o)) {
                itemView.setAlpha(1.0f);
                this.A(o);
                if (list3.isEmpty()) {
                    this.l.remove(n);
                }
            }
        }
        this.q.remove(o);
        this.o.remove(o);
        this.r.remove(o);
        this.p.remove(o);
        this.V();
    }
    
    @Override
    public void k() {
        for (int i = this.j.size() - 1; i >= 0; --i) {
            final j j = this.j.get(i);
            final View itemView = j.a.itemView;
            itemView.setTranslationY(0.0f);
            itemView.setTranslationX(0.0f);
            this.E(j.a);
            this.j.remove(i);
        }
        for (int k = this.h.size() - 1; k >= 0; --k) {
            this.G((d0)this.h.get(k));
            this.h.remove(k);
        }
        for (int l = this.i.size() - 1; l >= 0; --l) {
            final d0 d0 = this.i.get(l);
            d0.itemView.setAlpha(1.0f);
            this.A(d0);
            this.i.remove(l);
        }
        for (int index = this.k.size() - 1; index >= 0; --index) {
            this.X((i)this.k.get(index));
        }
        this.k.clear();
        if (!this.p()) {
            return;
        }
        for (int index2 = this.m.size() - 1; index2 >= 0; --index2) {
            final ArrayList o = this.m.get(index2);
            for (int n = o.size() - 1; n >= 0; --n) {
                final j m = (j)o.get(n);
                final View itemView2 = m.a.itemView;
                itemView2.setTranslationY(0.0f);
                itemView2.setTranslationX(0.0f);
                this.E(m.a);
                o.remove(n);
                if (o.isEmpty()) {
                    this.m.remove(o);
                }
            }
        }
        for (int index3 = this.l.size() - 1; index3 >= 0; --index3) {
            final ArrayList o2 = this.l.get(index3);
            for (int n2 = o2.size() - 1; n2 >= 0; --n2) {
                final d0 d2 = (d0)o2.get(n2);
                d2.itemView.setAlpha(1.0f);
                this.A(d2);
                o2.remove(n2);
                if (o2.isEmpty()) {
                    this.l.remove(o2);
                }
            }
        }
        for (int index4 = this.n.size() - 1; index4 >= 0; --index4) {
            final ArrayList o3 = this.n.get(index4);
            for (int index5 = o3.size() - 1; index5 >= 0; --index5) {
                this.X((i)o3.get(index5));
                if (o3.isEmpty()) {
                    this.n.remove(o3);
                }
            }
        }
        this.U(this.q);
        this.U(this.p);
        this.U(this.o);
        this.U(this.r);
        ((RecyclerView.l)this).i();
    }
    
    @Override
    public boolean p() {
        return !this.i.isEmpty() || !this.k.isEmpty() || !this.j.isEmpty() || !this.h.isEmpty() || !this.p.isEmpty() || !this.q.isEmpty() || !this.o.isEmpty() || !this.r.isEmpty() || !this.m.isEmpty() || !this.l.isEmpty() || !this.n.isEmpty();
    }
    
    @Override
    public void u() {
        final boolean b = this.h.isEmpty() ^ true;
        final boolean b2 = this.j.isEmpty() ^ true;
        final boolean b3 = this.k.isEmpty() ^ true;
        final boolean b4 = this.i.isEmpty() ^ true;
        if (!b && !b2 && !b4 && !b3) {
            return;
        }
        final Iterator iterator = this.h.iterator();
        while (iterator.hasNext()) {
            this.T((d0)iterator.next());
        }
        this.h.clear();
        if (b2) {
            final ArrayList e = new ArrayList();
            e.addAll(this.j);
            this.m.add(e);
            this.j.clear();
            final Runnable runnable = new Runnable(this, e) {
                public final ArrayList a;
                public final c b;
                
                @Override
                public void run() {
                    for (final j j : this.a) {
                        this.b.S(j.a, j.b, j.c, j.d, j.e);
                    }
                    this.a.clear();
                    this.b.m.remove(this.a);
                }
            };
            if (b) {
                o32.j0(e.get(0).a.itemView, runnable, ((RecyclerView.l)this).o());
            }
            else {
                runnable.run();
            }
        }
        if (b3) {
            final ArrayList e2 = new ArrayList();
            e2.addAll(this.k);
            this.n.add(e2);
            this.k.clear();
            final Runnable runnable2 = new Runnable(this, e2) {
                public final ArrayList a;
                public final c b;
                
                @Override
                public void run() {
                    final Iterator iterator = this.a.iterator();
                    while (iterator.hasNext()) {
                        this.b.R((i)iterator.next());
                    }
                    this.a.clear();
                    this.b.n.remove(this.a);
                }
            };
            if (b) {
                o32.j0(((i)e2.get(0)).a.itemView, runnable2, ((RecyclerView.l)this).o());
            }
            else {
                runnable2.run();
            }
        }
        if (b4) {
            final ArrayList e3 = new ArrayList();
            e3.addAll(this.i);
            this.l.add(e3);
            this.i.clear();
            final Runnable runnable3 = new Runnable(this, e3) {
                public final ArrayList a;
                public final c b;
                
                @Override
                public void run() {
                    final Iterator iterator = this.a.iterator();
                    while (iterator.hasNext()) {
                        this.b.Q((d0)iterator.next());
                    }
                    this.a.clear();
                    this.b.l.remove(this.a);
                }
            };
            if (!b && !b2 && !b3) {
                runnable3.run();
            }
            else {
                long m = 0L;
                long o;
                if (b) {
                    o = ((RecyclerView.l)this).o();
                }
                else {
                    o = 0L;
                }
                long n;
                if (b2) {
                    n = ((RecyclerView.l)this).n();
                }
                else {
                    n = 0L;
                }
                if (b3) {
                    m = ((RecyclerView.l)this).m();
                }
                o32.j0(((d0)e3.get(0)).itemView, runnable3, o + Math.max(n, m));
            }
        }
    }
    
    @Override
    public boolean w(final d0 e) {
        this.Z(e);
        e.itemView.setAlpha(0.0f);
        this.i.add(e);
        return true;
    }
    
    @Override
    public boolean x(final d0 d0, final d0 d2, final int n, final int n2, final int n3, final int n4) {
        if (d0 == d2) {
            return this.y(d0, n, n2, n3, n4);
        }
        final float translationX = d0.itemView.getTranslationX();
        final float translationY = d0.itemView.getTranslationY();
        final float alpha = d0.itemView.getAlpha();
        this.Z(d0);
        final int n5 = (int)(n3 - n - translationX);
        final int n6 = (int)(n4 - n2 - translationY);
        d0.itemView.setTranslationX(translationX);
        d0.itemView.setTranslationY(translationY);
        d0.itemView.setAlpha(alpha);
        if (d2 != null) {
            this.Z(d2);
            d2.itemView.setTranslationX((float)(-n5));
            d2.itemView.setTranslationY((float)(-n6));
            d2.itemView.setAlpha(0.0f);
        }
        this.k.add(new i(d0, d2, n, n2, n3, n4));
        return true;
    }
    
    @Override
    public boolean y(final d0 d0, int n, int n2, final int n3, final int n4) {
        final View itemView = d0.itemView;
        n += (int)itemView.getTranslationX();
        final int n5 = n2 + (int)d0.itemView.getTranslationY();
        this.Z(d0);
        n2 = n3 - n;
        final int n6 = n4 - n5;
        if (n2 == 0 && n6 == 0) {
            this.E(d0);
            return false;
        }
        if (n2 != 0) {
            itemView.setTranslationX((float)(-n2));
        }
        if (n6 != 0) {
            itemView.setTranslationY((float)(-n6));
        }
        this.j.add(new j(d0, n, n5, n3, n4));
        return true;
    }
    
    @Override
    public boolean z(final d0 e) {
        this.Z(e);
        this.h.add(e);
        return true;
    }
    
    public static class i
    {
        public d0 a;
        public d0 b;
        public int c;
        public int d;
        public int e;
        public int f;
        
        public i(final d0 a, final d0 b) {
            this.a = a;
            this.b = b;
        }
        
        public i(final d0 d0, final d0 d2, final int c, final int d3, final int e, final int f) {
            this(d0, d2);
            this.c = c;
            this.d = d3;
            this.e = e;
            this.f = f;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ChangeInfo{oldHolder=");
            sb.append(this.a);
            sb.append(", newHolder=");
            sb.append(this.b);
            sb.append(", fromX=");
            sb.append(this.c);
            sb.append(", fromY=");
            sb.append(this.d);
            sb.append(", toX=");
            sb.append(this.e);
            sb.append(", toY=");
            sb.append(this.f);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static class j
    {
        public d0 a;
        public int b;
        public int c;
        public int d;
        public int e;
        
        public j(final d0 a, final int b, final int c, final int d, final int e) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
    }
}

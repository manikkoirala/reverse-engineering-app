// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.View;

public abstract class l
{
    public static int a(final RecyclerView.a0 a0, final i i, final View view, final View view2, final RecyclerView.o o, final boolean b) {
        if (o.getChildCount() == 0 || a0.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!b) {
            return Math.abs(o.getPosition(view) - o.getPosition(view2)) + 1;
        }
        return Math.min(i.n(), i.d(view2) - i.g(view));
    }
    
    public static int b(final RecyclerView.a0 a0, final i i, final View view, final View view2, final RecyclerView.o o, final boolean b, final boolean b2) {
        if (o.getChildCount() == 0 || a0.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        final int min = Math.min(o.getPosition(view), o.getPosition(view2));
        final int max = Math.max(o.getPosition(view), o.getPosition(view2));
        int n;
        if (b2) {
            n = Math.max(0, a0.b() - max - 1);
        }
        else {
            n = Math.max(0, min);
        }
        if (!b) {
            return n;
        }
        return Math.round(n * (Math.abs(i.d(view2) - i.g(view)) / (float)(Math.abs(o.getPosition(view) - o.getPosition(view2)) + 1)) + (i.m() - i.g(view)));
    }
    
    public static int c(final RecyclerView.a0 a0, final i i, final View view, final View view2, final RecyclerView.o o, final boolean b) {
        if (o.getChildCount() == 0 || a0.b() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!b) {
            return a0.b();
        }
        return (int)((i.d(view2) - i.g(view)) / (float)(Math.abs(o.getPosition(view) - o.getPosition(view2)) + 1) * a0.b());
    }
}

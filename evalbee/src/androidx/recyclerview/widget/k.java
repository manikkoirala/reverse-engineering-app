// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.ViewGroup;
import java.util.WeakHashMap;
import java.util.Map;
import android.os.Bundle;
import android.view.accessibility.AccessibilityEvent;
import android.view.View;

public class k extends p0
{
    private final a mItemDelegate;
    final RecyclerView mRecyclerView;
    
    public k(final RecyclerView mRecyclerView) {
        this.mRecyclerView = mRecyclerView;
        final p0 itemDelegate = this.getItemDelegate();
        a mItemDelegate;
        if (itemDelegate != null && itemDelegate instanceof a) {
            mItemDelegate = (a)itemDelegate;
        }
        else {
            mItemDelegate = new a(this);
        }
        this.mItemDelegate = mItemDelegate;
    }
    
    public p0 getItemDelegate() {
        return this.mItemDelegate;
    }
    
    @Override
    public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        if (view instanceof RecyclerView && !this.shouldIgnore()) {
            final RecyclerView recyclerView = (RecyclerView)view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().onInitializeAccessibilityEvent(accessibilityEvent);
            }
        }
    }
    
    @Override
    public void onInitializeAccessibilityNodeInfo(final View view, final n1 n1) {
        super.onInitializeAccessibilityNodeInfo(view, n1);
        if (!this.shouldIgnore() && this.mRecyclerView.getLayoutManager() != null) {
            this.mRecyclerView.getLayoutManager().onInitializeAccessibilityNodeInfo(n1);
        }
    }
    
    @Override
    public boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
        return super.performAccessibilityAction(view, n, bundle) || (!this.shouldIgnore() && this.mRecyclerView.getLayoutManager() != null && this.mRecyclerView.getLayoutManager().performAccessibilityAction(n, bundle));
    }
    
    public boolean shouldIgnore() {
        return this.mRecyclerView.hasPendingAdapterUpdates();
    }
    
    public static class a extends p0
    {
        public final k a;
        public Map b;
        
        public a(final k a) {
            this.b = new WeakHashMap();
            this.a = a;
        }
        
        public p0 c(final View view) {
            return this.b.remove(view);
        }
        
        public void d(final View view) {
            final p0 l = o32.l(view);
            if (l != null && l != this) {
                this.b.put(view, l);
            }
        }
        
        @Override
        public boolean dispatchPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            final p0 p2 = this.b.get(view);
            if (p2 != null) {
                return p2.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
            }
            return super.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
        }
        
        @Override
        public o1 getAccessibilityNodeProvider(final View view) {
            final p0 p = this.b.get(view);
            if (p != null) {
                return p.getAccessibilityNodeProvider(view);
            }
            return super.getAccessibilityNodeProvider(view);
        }
        
        @Override
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            final p0 p2 = this.b.get(view);
            if (p2 != null) {
                p2.onInitializeAccessibilityEvent(view, accessibilityEvent);
            }
            else {
                super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            }
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(final View view, final n1 n1) {
            if (!this.a.shouldIgnore() && this.a.mRecyclerView.getLayoutManager() != null) {
                this.a.mRecyclerView.getLayoutManager().onInitializeAccessibilityNodeInfoForItem(view, n1);
                final p0 p2 = this.b.get(view);
                if (p2 != null) {
                    p2.onInitializeAccessibilityNodeInfo(view, n1);
                    return;
                }
            }
            super.onInitializeAccessibilityNodeInfo(view, n1);
        }
        
        @Override
        public void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            final p0 p2 = this.b.get(view);
            if (p2 != null) {
                p2.onPopulateAccessibilityEvent(view, accessibilityEvent);
            }
            else {
                super.onPopulateAccessibilityEvent(view, accessibilityEvent);
            }
        }
        
        @Override
        public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
            final p0 p3 = this.b.get(viewGroup);
            if (p3 != null) {
                return p3.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
            }
            return super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
        }
        
        @Override
        public boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
            if (!this.a.shouldIgnore() && this.a.mRecyclerView.getLayoutManager() != null) {
                final p0 p3 = this.b.get(view);
                if (p3 != null) {
                    if (p3.performAccessibilityAction(view, n, bundle)) {
                        return true;
                    }
                }
                else if (super.performAccessibilityAction(view, n, bundle)) {
                    return true;
                }
                return this.a.mRecyclerView.getLayoutManager().performAccessibilityActionForItem(view, n, bundle);
            }
            return super.performAccessibilityAction(view, n, bundle);
        }
        
        @Override
        public void sendAccessibilityEvent(final View view, final int n) {
            final p0 p2 = this.b.get(view);
            if (p2 != null) {
                p2.sendAccessibilityEvent(view, n);
            }
            else {
                super.sendAccessibilityEvent(view, n);
            }
        }
        
        @Override
        public void sendAccessibilityEventUnchecked(final View view, final AccessibilityEvent accessibilityEvent) {
            final p0 p2 = this.b.get(view);
            if (p2 != null) {
                p2.sendAccessibilityEventUnchecked(view, accessibilityEvent);
            }
            else {
                super.sendAccessibilityEventUnchecked(view, accessibilityEvent);
            }
        }
    }
}

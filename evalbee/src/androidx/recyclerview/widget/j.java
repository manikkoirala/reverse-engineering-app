// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.graphics.PointF;
import android.view.animation.Interpolator;
import android.util.DisplayMetrics;
import android.content.Context;
import android.view.View;

public class j extends n
{
    public androidx.recyclerview.widget.i a;
    public androidx.recyclerview.widget.i b;
    
    @Override
    public int[] calculateDistanceToFinalSnap(final o o, final View view) {
        final int[] array = new int[2];
        if (o.canScrollHorizontally()) {
            array[0] = this.d(o, view, this.f(o));
        }
        else {
            array[0] = 0;
        }
        if (o.canScrollVertically()) {
            array[1] = this.d(o, view, this.h(o));
        }
        else {
            array[1] = 0;
        }
        return array;
    }
    
    @Override
    public androidx.recyclerview.widget.g createSnapScroller(final o o) {
        if (!(o instanceof z.b)) {
            return null;
        }
        return new androidx.recyclerview.widget.g(this, ((View)super.mRecyclerView).getContext()) {
            public final j a;
            
            @Override
            public float calculateSpeedPerPixel(final DisplayMetrics displayMetrics) {
                return 100.0f / displayMetrics.densityDpi;
            }
            
            @Override
            public int calculateTimeForScrolling(final int n) {
                return Math.min(100, super.calculateTimeForScrolling(n));
            }
            
            @Override
            public void onTargetFound(final View view, final a0 a0, final z.a a2) {
                final j a3 = this.a;
                final int[] calculateDistanceToFinalSnap = a3.calculateDistanceToFinalSnap(a3.mRecyclerView.getLayoutManager(), view);
                final int a4 = calculateDistanceToFinalSnap[0];
                final int a5 = calculateDistanceToFinalSnap[1];
                final int calculateTimeForDeceleration = this.calculateTimeForDeceleration(Math.max(Math.abs(a4), Math.abs(a5)));
                if (calculateTimeForDeceleration > 0) {
                    a2.d(a4, a5, calculateTimeForDeceleration, (Interpolator)super.mDecelerateInterpolator);
                }
            }
        };
    }
    
    public final int d(final o o, final View view, final androidx.recyclerview.widget.i i) {
        return i.g(view) + i.e(view) / 2 - (i.m() + i.n() / 2);
    }
    
    public final View e(final o o, final androidx.recyclerview.widget.i i) {
        final int childCount = o.getChildCount();
        View view = null;
        if (childCount == 0) {
            return null;
        }
        final int m = i.m();
        final int n = i.n() / 2;
        int n2 = Integer.MAX_VALUE;
        int n3;
        for (int j = 0; j < childCount; ++j, n2 = n3) {
            final View child = o.getChildAt(j);
            final int abs = Math.abs(i.g(child) + i.e(child) / 2 - (m + n));
            if (abs < (n3 = n2)) {
                view = child;
                n3 = abs;
            }
        }
        return view;
    }
    
    public final androidx.recyclerview.widget.i f(final o o) {
        final androidx.recyclerview.widget.i b = this.b;
        if (b == null || b.a != o) {
            this.b = androidx.recyclerview.widget.i.a(o);
        }
        return this.b;
    }
    
    @Override
    public View findSnapView(final o o) {
        androidx.recyclerview.widget.i i;
        if (o.canScrollVertically()) {
            i = this.h(o);
        }
        else {
            if (!o.canScrollHorizontally()) {
                return null;
            }
            i = this.f(o);
        }
        return this.e(o, i);
    }
    
    @Override
    public int findTargetSnapPosition(final o o, int n, int position) {
        final int itemCount = o.getItemCount();
        if (itemCount == 0) {
            return -1;
        }
        final androidx.recyclerview.widget.i g = this.g(o);
        if (g == null) {
            return -1;
        }
        final int childCount = o.getChildCount();
        View view = null;
        int i = 0;
        int n2 = Integer.MAX_VALUE;
        int n3 = Integer.MIN_VALUE;
        View view2 = null;
        while (i < childCount) {
            final View child = o.getChildAt(i);
            View view3;
            int n4;
            if (child == null) {
                view3 = view;
                n4 = n2;
            }
            else {
                final int d = this.d(o, child, g);
                View view4 = view2;
                int n5 = n3;
                if (d <= 0) {
                    view4 = view2;
                    if (d > (n5 = n3)) {
                        view4 = child;
                        n5 = d;
                    }
                }
                view3 = view;
                view2 = view4;
                n3 = n5;
                n4 = n2;
                if (d >= 0) {
                    view3 = view;
                    view2 = view4;
                    n3 = n5;
                    if (d < (n4 = n2)) {
                        n4 = d;
                        n3 = n5;
                        view2 = view4;
                        view3 = child;
                    }
                }
            }
            ++i;
            view = view3;
            n2 = n4;
        }
        final boolean forwardFling = this.isForwardFling(o, n, position);
        if (forwardFling && view != null) {
            return o.getPosition(view);
        }
        if (!forwardFling && view2 != null) {
            return o.getPosition(view2);
        }
        if (forwardFling) {
            view = view2;
        }
        if (view == null) {
            return -1;
        }
        position = o.getPosition(view);
        if (this.isReverseLayout(o) == forwardFling) {
            n = -1;
        }
        else {
            n = 1;
        }
        n += position;
        if (n >= 0 && n < itemCount) {
            return n;
        }
        return -1;
    }
    
    public final androidx.recyclerview.widget.i g(final o o) {
        if (o.canScrollVertically()) {
            return this.h(o);
        }
        if (o.canScrollHorizontally()) {
            return this.f(o);
        }
        return null;
    }
    
    public final androidx.recyclerview.widget.i h(final o o) {
        final androidx.recyclerview.widget.i a = this.a;
        if (a == null || a.a != o) {
            this.a = androidx.recyclerview.widget.i.c(o);
        }
        return this.a;
    }
    
    public final boolean isForwardFling(final o o, final int n, final int n2) {
        final boolean canScrollHorizontally = o.canScrollHorizontally();
        boolean b = true;
        final boolean b2 = true;
        if (canScrollHorizontally) {
            return n > 0 && b2;
        }
        if (n2 <= 0) {
            b = false;
        }
        return b;
    }
    
    public final boolean isReverseLayout(final o o) {
        final int itemCount = o.getItemCount();
        final boolean b = o instanceof z.b;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final PointF computeScrollVectorForPosition = ((z.b)o).computeScrollVectorForPosition(itemCount - 1);
            b3 = b2;
            if (computeScrollVectorForPosition != null) {
                if (computeScrollVectorForPosition.x >= 0.0f) {
                    b3 = b2;
                    if (computeScrollVectorForPosition.y >= 0.0f) {
                        return b3;
                    }
                }
                b3 = true;
            }
        }
        return b3;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.View;

public abstract class m extends l
{
    public boolean g;
    
    public m() {
        this.g = true;
    }
    
    public final void A(final d0 d0) {
        this.I(d0);
        ((RecyclerView.l)this).h(d0);
    }
    
    public final void B(final d0 d0) {
        this.J(d0);
    }
    
    public final void C(final d0 d0, final boolean b) {
        this.K(d0, b);
        ((RecyclerView.l)this).h(d0);
    }
    
    public final void D(final d0 d0, final boolean b) {
        this.L(d0, b);
    }
    
    public final void E(final d0 d0) {
        this.M(d0);
        ((RecyclerView.l)this).h(d0);
    }
    
    public final void F(final d0 d0) {
        this.N(d0);
    }
    
    public final void G(final d0 d0) {
        this.O(d0);
        ((RecyclerView.l)this).h(d0);
    }
    
    public final void H(final d0 d0) {
        this.P(d0);
    }
    
    public void I(final d0 d0) {
    }
    
    public void J(final d0 d0) {
    }
    
    public void K(final d0 d0, final boolean b) {
    }
    
    public void L(final d0 d0, final boolean b) {
    }
    
    public void M(final d0 d0) {
    }
    
    public void N(final d0 d0) {
    }
    
    public void O(final d0 d0) {
    }
    
    public void P(final d0 d0) {
    }
    
    @Override
    public boolean a(final d0 d0, final b b, final b b2) {
        if (b != null) {
            final int a = b.a;
            final int a2 = b2.a;
            if (a != a2 || b.b != b2.b) {
                return this.y(d0, a, b.b, a2, b2.b);
            }
        }
        return this.w(d0);
    }
    
    @Override
    public boolean b(final d0 d0, final d0 d2, final b b, final b b2) {
        final int a = b.a;
        final int b3 = b.b;
        int n;
        int n2;
        if (d2.shouldIgnore()) {
            n = b.a;
            n2 = b.b;
        }
        else {
            n = b2.a;
            n2 = b2.b;
        }
        return this.x(d0, d2, a, b3, n, n2);
    }
    
    @Override
    public boolean c(final d0 d0, final b b, final b b2) {
        final int a = b.a;
        final int b3 = b.b;
        final View itemView = d0.itemView;
        int n;
        if (b2 == null) {
            n = itemView.getLeft();
        }
        else {
            n = b2.a;
        }
        int n2;
        if (b2 == null) {
            n2 = itemView.getTop();
        }
        else {
            n2 = b2.b;
        }
        if (!d0.isRemoved() && (a != n || b3 != n2)) {
            itemView.layout(n, n2, itemView.getWidth() + n, itemView.getHeight() + n2);
            return this.y(d0, a, b3, n, n2);
        }
        return this.z(d0);
    }
    
    @Override
    public boolean d(final d0 d0, final b b, final b b2) {
        final int a = b.a;
        final int a2 = b2.a;
        if (a == a2 && b.b == b2.b) {
            this.E(d0);
            return false;
        }
        return this.y(d0, a, b.b, a2, b2.b);
    }
    
    @Override
    public boolean f(final d0 d0) {
        return !this.g || d0.isInvalid();
    }
    
    public abstract boolean w(final d0 p0);
    
    public abstract boolean x(final d0 p0, final d0 p1, final int p2, final int p3, final int p4, final int p5);
    
    public abstract boolean y(final d0 p0, final int p1, final int p2, final int p3, final int p4);
    
    public abstract boolean z(final d0 p0);
}

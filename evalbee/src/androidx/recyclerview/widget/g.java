// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.animation.Interpolator;
import android.view.View;
import android.content.Context;
import android.graphics.PointF;
import android.view.animation.LinearInterpolator;
import android.util.DisplayMetrics;
import android.view.animation.DecelerateInterpolator;

public class g extends z
{
    private static final boolean DEBUG = false;
    private static final float MILLISECONDS_PER_INCH = 25.0f;
    public static final int SNAP_TO_ANY = 0;
    public static final int SNAP_TO_END = 1;
    public static final int SNAP_TO_START = -1;
    private static final float TARGET_SEEK_EXTRA_SCROLL_RATIO = 1.2f;
    private static final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;
    protected final DecelerateInterpolator mDecelerateInterpolator;
    private final DisplayMetrics mDisplayMetrics;
    private boolean mHasCalculatedMillisPerPixel;
    protected int mInterimTargetDx;
    protected int mInterimTargetDy;
    protected final LinearInterpolator mLinearInterpolator;
    private float mMillisPerPixel;
    protected PointF mTargetVector;
    
    public g(final Context context) {
        this.mLinearInterpolator = new LinearInterpolator();
        this.mDecelerateInterpolator = new DecelerateInterpolator();
        this.mHasCalculatedMillisPerPixel = false;
        this.mInterimTargetDx = 0;
        this.mInterimTargetDy = 0;
        this.mDisplayMetrics = context.getResources().getDisplayMetrics();
    }
    
    public final int a(final int n, int n2) {
        n2 = n - n2;
        if (n * n2 <= 0) {
            return 0;
        }
        return n2;
    }
    
    public final float b() {
        if (!this.mHasCalculatedMillisPerPixel) {
            this.mMillisPerPixel = this.calculateSpeedPerPixel(this.mDisplayMetrics);
            this.mHasCalculatedMillisPerPixel = true;
        }
        return this.mMillisPerPixel;
    }
    
    public int calculateDtToFit(int n, final int n2, final int n3, final int n4, final int n5) {
        if (n5 == -1) {
            return n3 - n;
        }
        if (n5 != 0) {
            if (n5 == 1) {
                return n4 - n2;
            }
            throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
        else {
            n = n3 - n;
            if (n > 0) {
                return n;
            }
            n = n4 - n2;
            if (n < 0) {
                return n;
            }
            return 0;
        }
    }
    
    public int calculateDxToMakeVisible(final View view, final int n) {
        final o layoutManager = ((RecyclerView.z)this).getLayoutManager();
        if (layoutManager != null && layoutManager.canScrollHorizontally()) {
            final p p2 = (p)view.getLayoutParams();
            return this.calculateDtToFit(layoutManager.getDecoratedLeft(view) - p2.leftMargin, layoutManager.getDecoratedRight(view) + p2.rightMargin, layoutManager.getPaddingLeft(), layoutManager.getWidth() - layoutManager.getPaddingRight(), n);
        }
        return 0;
    }
    
    public int calculateDyToMakeVisible(final View view, final int n) {
        final o layoutManager = ((RecyclerView.z)this).getLayoutManager();
        if (layoutManager != null && layoutManager.canScrollVertically()) {
            final p p2 = (p)view.getLayoutParams();
            return this.calculateDtToFit(layoutManager.getDecoratedTop(view) - p2.topMargin, layoutManager.getDecoratedBottom(view) + p2.bottomMargin, layoutManager.getPaddingTop(), layoutManager.getHeight() - layoutManager.getPaddingBottom(), n);
        }
        return 0;
    }
    
    public float calculateSpeedPerPixel(final DisplayMetrics displayMetrics) {
        return 25.0f / displayMetrics.densityDpi;
    }
    
    public int calculateTimeForDeceleration(final int n) {
        return (int)Math.ceil(this.calculateTimeForScrolling(n) / 0.3356);
    }
    
    public int calculateTimeForScrolling(final int a) {
        return (int)Math.ceil(Math.abs(a) * this.b());
    }
    
    public int getHorizontalSnapPreference() {
        final PointF mTargetVector = this.mTargetVector;
        if (mTargetVector != null) {
            final float x = mTargetVector.x;
            if (x != 0.0f) {
                if (x > 0.0f) {
                    return 1;
                }
                return -1;
            }
        }
        return 0;
    }
    
    public int getVerticalSnapPreference() {
        final PointF mTargetVector = this.mTargetVector;
        if (mTargetVector != null) {
            final float y = mTargetVector.y;
            if (y != 0.0f) {
                if (y > 0.0f) {
                    return 1;
                }
                return -1;
            }
        }
        return 0;
    }
    
    @Override
    public void onSeekTargetStep(int a, final int n, final a0 a2, final a a3) {
        if (((RecyclerView.z)this).getChildCount() == 0) {
            ((RecyclerView.z)this).stop();
            return;
        }
        this.mInterimTargetDx = this.a(this.mInterimTargetDx, a);
        a = this.a(this.mInterimTargetDy, n);
        this.mInterimTargetDy = a;
        if (this.mInterimTargetDx == 0 && a == 0) {
            this.updateActionForInterimTarget(a3);
        }
    }
    
    @Override
    public void onStart() {
    }
    
    @Override
    public void onStop() {
        this.mInterimTargetDy = 0;
        this.mInterimTargetDx = 0;
        this.mTargetVector = null;
    }
    
    @Override
    public void onTargetFound(final View view, final a0 a0, final a a2) {
        final int calculateDxToMakeVisible = this.calculateDxToMakeVisible(view, this.getHorizontalSnapPreference());
        final int calculateDyToMakeVisible = this.calculateDyToMakeVisible(view, this.getVerticalSnapPreference());
        final int calculateTimeForDeceleration = this.calculateTimeForDeceleration((int)Math.sqrt(calculateDxToMakeVisible * calculateDxToMakeVisible + calculateDyToMakeVisible * calculateDyToMakeVisible));
        if (calculateTimeForDeceleration > 0) {
            a2.d(-calculateDxToMakeVisible, -calculateDyToMakeVisible, calculateTimeForDeceleration, (Interpolator)this.mDecelerateInterpolator);
        }
    }
    
    public void updateActionForInterimTarget(final a a) {
        final PointF computeScrollVectorForPosition = ((RecyclerView.z)this).computeScrollVectorForPosition(((RecyclerView.z)this).getTargetPosition());
        if (computeScrollVectorForPosition != null && (computeScrollVectorForPosition.x != 0.0f || computeScrollVectorForPosition.y != 0.0f)) {
            ((RecyclerView.z)this).normalize(computeScrollVectorForPosition);
            this.mTargetVector = computeScrollVectorForPosition;
            this.mInterimTargetDx = (int)(computeScrollVectorForPosition.x * 10000.0f);
            this.mInterimTargetDy = (int)(computeScrollVectorForPosition.y * 10000.0f);
            a.d((int)(this.mInterimTargetDx * 1.2f), (int)(this.mInterimTargetDy * 1.2f), (int)(this.calculateTimeForScrolling(10000) * 1.2f), (Interpolator)this.mLinearInterpolator);
            return;
        }
        a.b(((RecyclerView.z)this).getTargetPosition());
        ((RecyclerView.z)this).stop();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.accessibility.AccessibilityRecord;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import java.util.List;
import android.os.Parcelable;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.graphics.PointF;
import java.util.Arrays;
import android.view.View$MeasureSpec;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import java.util.BitSet;

public class StaggeredGridLayoutManager extends o implements z.b
{
    public int[] A;
    public final Runnable C;
    public int a;
    public f[] b;
    public androidx.recyclerview.widget.i c;
    public androidx.recyclerview.widget.i d;
    public int e;
    public int f;
    public final androidx.recyclerview.widget.f g;
    public boolean h;
    public boolean i;
    public BitSet j;
    public int k;
    public int l;
    public d m;
    public int n;
    public boolean p;
    public boolean q;
    public e t;
    public int v;
    public final Rect w;
    public final b x;
    public boolean y;
    public boolean z;
    
    public StaggeredGridLayoutManager(final Context context, final AttributeSet set, final int n, final int n2) {
        this.a = -1;
        this.h = false;
        this.i = false;
        this.k = -1;
        this.l = Integer.MIN_VALUE;
        this.m = new d();
        this.n = 2;
        this.w = new Rect();
        this.x = new b();
        this.y = false;
        this.z = true;
        this.C = new Runnable(this) {
            public final StaggeredGridLayoutManager a;
            
            @Override
            public void run() {
                this.a.l();
            }
        };
        final o.d properties = RecyclerView.o.getProperties(context, set, n, n2);
        this.setOrientation(properties.a);
        this.U(properties.b);
        this.setReverseLayout(properties.c);
        this.g = new androidx.recyclerview.widget.f();
        this.p();
    }
    
    public final int A(final int n) {
        int l = this.b[0].l(n);
        int n2;
        for (int i = 1; i < this.a; ++i, l = n2) {
            final int j = this.b[i].l(n);
            if (j > (n2 = l)) {
                n2 = j;
            }
        }
        return l;
    }
    
    public final int B(final int n) {
        int p = this.b[0].p(n);
        int n2;
        for (int i = 1; i < this.a; ++i, p = n2) {
            final int p2 = this.b[i].p(n);
            if (p2 > (n2 = p)) {
                n2 = p2;
            }
        }
        return p;
    }
    
    public final int C(final int n) {
        int l = this.b[0].l(n);
        int n2;
        for (int i = 1; i < this.a; ++i, l = n2) {
            final int j = this.b[i].l(n);
            if (j < (n2 = l)) {
                n2 = j;
            }
        }
        return l;
    }
    
    public final int D(final int n) {
        int p = this.b[0].p(n);
        int n2;
        for (int i = 1; i < this.a; ++i, p = n2) {
            final int p2 = this.b[i].p(n);
            if (p2 < (n2 = p)) {
                n2 = p2;
            }
        }
        return p;
    }
    
    public final f E(final androidx.recyclerview.widget.f f) {
        int i;
        int a;
        int n;
        if (this.L(f.e)) {
            i = this.a - 1;
            a = -1;
            n = -1;
        }
        else {
            a = this.a;
            i = 0;
            n = 1;
        }
        final int e = f.e;
        f f2 = null;
        final f f3 = null;
        if (e == 1) {
            final int m = this.c.m();
            int n2 = Integer.MAX_VALUE;
            f f4 = f3;
            int n3;
            for (int j = i; j != a; j += n, n2 = n3) {
                final f f5 = this.b[j];
                final int l = f5.l(m);
                if (l < (n3 = n2)) {
                    f4 = f5;
                    n3 = l;
                }
            }
            return f4;
        }
        final int k = this.c.i();
        int n4 = Integer.MIN_VALUE;
        while (i != a) {
            final f f6 = this.b[i];
            final int p = f6.p(k);
            int n5;
            if (p > (n5 = n4)) {
                f2 = f6;
                n5 = p;
            }
            i += n;
            n4 = n5;
        }
        return f2;
    }
    
    public final void F(int n, final int n2, final int n3) {
        int n4;
        if (this.i) {
            n4 = this.z();
        }
        else {
            n4 = this.y();
        }
        int n5 = 0;
        int n6 = 0;
        Label_0060: {
            if (n3 == 8) {
                if (n >= n2) {
                    n5 = n + 1;
                    n6 = n2;
                    break Label_0060;
                }
                n5 = n2 + 1;
            }
            else {
                n5 = n + n2;
            }
            n6 = n;
        }
        this.m.h(n6);
        if (n3 != 1) {
            if (n3 != 2) {
                if (n3 == 8) {
                    this.m.k(n, 1);
                    this.m.j(n2, 1);
                }
            }
            else {
                this.m.k(n, n2);
            }
        }
        else {
            this.m.j(n, n2);
        }
        if (n5 <= n4) {
            return;
        }
        if (this.i) {
            n = this.y();
        }
        else {
            n = this.z();
        }
        if (n6 <= n) {
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    public View G() {
        int n = ((RecyclerView.o)this).getChildCount() - 1;
        final BitSet set = new BitSet(this.a);
        set.set(0, this.a, true);
        final int e = this.e;
        int n2 = -1;
        int n3;
        if (e == 1 && this.isLayoutRTL()) {
            n3 = 1;
        }
        else {
            n3 = -1;
        }
        int n4;
        if (this.i) {
            n4 = -1;
        }
        else {
            n4 = n + 1;
            n = 0;
        }
        int i = n;
        if (n < n4) {
            n2 = 1;
            i = n;
        }
        while (i != n4) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            final c c = (c)child.getLayoutParams();
            if (set.get(c.a.e)) {
                if (this.m(c.a)) {
                    return child;
                }
                set.clear(c.a.e);
            }
            if (!c.b) {
                final int n5 = i + n2;
                if (n5 != n4) {
                    final View child2 = ((RecyclerView.o)this).getChildAt(n5);
                    boolean b = false;
                    Label_0276: {
                        Label_0274: {
                            if (this.i) {
                                final int d = this.c.d(child);
                                final int d2 = this.c.d(child2);
                                if (d < d2) {
                                    return child;
                                }
                                if (d != d2) {
                                    break Label_0274;
                                }
                            }
                            else {
                                final int g = this.c.g(child);
                                final int g2 = this.c.g(child2);
                                if (g > g2) {
                                    return child;
                                }
                                if (g != g2) {
                                    break Label_0274;
                                }
                            }
                            b = true;
                            break Label_0276;
                        }
                        b = false;
                    }
                    if (b && c.a.e - ((c)child2.getLayoutParams()).a.e < 0 != n3 < 0) {
                        return child;
                    }
                }
            }
            i += n2;
        }
        return null;
    }
    
    public void H() {
        this.m.b();
        ((RecyclerView.o)this).requestLayout();
    }
    
    public final void I(final View view, int c0, int c2, final boolean b) {
        ((RecyclerView.o)this).calculateItemDecorationsForChild(view, this.w);
        final c c3 = (c)view.getLayoutParams();
        final int leftMargin = c3.leftMargin;
        final Rect w = this.w;
        c0 = this.c0(c0, leftMargin + w.left, c3.rightMargin + w.right);
        final int topMargin = c3.topMargin;
        final Rect w2 = this.w;
        c2 = this.c0(c2, topMargin + w2.top, c3.bottomMargin + w2.bottom);
        boolean b2;
        if (b) {
            b2 = ((RecyclerView.o)this).shouldReMeasureChild(view, c0, c2, c3);
        }
        else {
            b2 = ((RecyclerView.o)this).shouldMeasureChild(view, c0, c2, c3);
        }
        if (b2) {
            view.measure(c0, c2);
        }
    }
    
    public final void J(final View view, final c c, final boolean b) {
        int childMeasureSpec = 0;
        int childMeasureSpec2 = 0;
        Label_0173: {
            int n;
            if (c.b) {
                if (this.e != 1) {
                    this.I(view, RecyclerView.o.getChildMeasureSpec(((RecyclerView.o)this).getWidth(), ((RecyclerView.o)this).getWidthMode(), ((RecyclerView.o)this).getPaddingLeft() + ((RecyclerView.o)this).getPaddingRight(), c.width, true), this.v, b);
                    return;
                }
                n = this.v;
            }
            else {
                if (this.e != 1) {
                    childMeasureSpec = RecyclerView.o.getChildMeasureSpec(((RecyclerView.o)this).getWidth(), ((RecyclerView.o)this).getWidthMode(), ((RecyclerView.o)this).getPaddingLeft() + ((RecyclerView.o)this).getPaddingRight(), c.width, true);
                    childMeasureSpec2 = RecyclerView.o.getChildMeasureSpec(this.f, ((RecyclerView.o)this).getHeightMode(), 0, c.height, false);
                    break Label_0173;
                }
                n = RecyclerView.o.getChildMeasureSpec(this.f, ((RecyclerView.o)this).getWidthMode(), 0, c.width, false);
            }
            final int childMeasureSpec3 = RecyclerView.o.getChildMeasureSpec(((RecyclerView.o)this).getHeight(), ((RecyclerView.o)this).getHeightMode(), ((RecyclerView.o)this).getPaddingTop() + ((RecyclerView.o)this).getPaddingBottom(), c.height, true);
            childMeasureSpec = n;
            childMeasureSpec2 = childMeasureSpec3;
        }
        this.I(view, childMeasureSpec, childMeasureSpec2, b);
    }
    
    public final void K(final v v, final a0 a0, final boolean b) {
        final b x = this.x;
        if ((this.t != null || this.k != -1) && a0.b() == 0) {
            ((RecyclerView.o)this).removeAndRecycleAllViews(v);
            x.c();
            return;
        }
        final boolean e = x.e;
        final int n = 1;
        final boolean b2 = !e || this.k != -1 || this.t != null;
        if (b2) {
            x.c();
            if (this.t != null) {
                this.g(x);
            }
            else {
                this.S();
                x.c = this.i;
            }
            this.Y(a0, x);
            x.e = true;
        }
        if (this.t == null && this.k == -1 && (x.c != this.p || this.isLayoutRTL() != this.q)) {
            this.m.b();
            x.d = true;
        }
        if (((RecyclerView.o)this).getChildCount() > 0) {
            final e t = this.t;
            if (t == null || t.c < 1) {
                if (x.d) {
                    for (int i = 0; i < this.a; ++i) {
                        this.b[i].e();
                        final int b3 = x.b;
                        if (b3 != Integer.MIN_VALUE) {
                            this.b[i].v(b3);
                        }
                    }
                }
                else if (!b2 && this.x.f != null) {
                    for (int j = 0; j < this.a; ++j) {
                        final f f = this.b[j];
                        f.e();
                        f.v(this.x.f[j]);
                    }
                }
                else {
                    for (int k = 0; k < this.a; ++k) {
                        this.b[k].b(this.i, x.b);
                    }
                    this.x.d(this.b);
                }
            }
        }
        ((RecyclerView.o)this).detachAndScrapAttachedViews(v);
        this.g.a = false;
        this.y = false;
        this.a0(this.d.n());
        this.Z(x.a, a0);
        if (x.c) {
            this.T(-1);
            this.q(v, this.g, a0);
            this.T(1);
        }
        else {
            this.T(1);
            this.q(v, this.g, a0);
            this.T(-1);
        }
        final androidx.recyclerview.widget.f g = this.g;
        g.c = x.a + g.d;
        this.q(v, g, a0);
        this.R();
        if (((RecyclerView.o)this).getChildCount() > 0) {
            if (this.i) {
                this.w(v, a0, true);
                this.x(v, a0, false);
            }
            else {
                this.x(v, a0, true);
                this.w(v, a0, false);
            }
        }
        int n2 = 0;
        Label_0637: {
            if (b && !a0.e() && (this.n != 0 && ((RecyclerView.o)this).getChildCount() > 0 && (this.y || this.G() != null))) {
                ((RecyclerView.o)this).removeCallbacks(this.C);
                if (this.l()) {
                    n2 = n;
                    break Label_0637;
                }
            }
            n2 = 0;
        }
        if (a0.e()) {
            this.x.c();
        }
        this.p = x.c;
        this.q = this.isLayoutRTL();
        if (n2 != 0) {
            this.x.c();
            this.K(v, a0, false);
        }
    }
    
    public final boolean L(final int n) {
        final int e = this.e;
        final boolean b = true;
        final boolean b2 = true;
        if (e == 0) {
            return n == -1 != this.i && b2;
        }
        return n == -1 == this.i == this.isLayoutRTL() && b;
    }
    
    public void M(final int a, final a0 a2) {
        int n;
        int n2;
        if (a > 0) {
            n = this.z();
            n2 = 1;
        }
        else {
            n = this.y();
            n2 = -1;
        }
        this.g.a = true;
        this.Z(n, a2);
        this.T(n2);
        final androidx.recyclerview.widget.f g = this.g;
        g.c = n + g.d;
        g.b = Math.abs(a);
    }
    
    public final void N(final View view) {
        for (int i = this.a - 1; i >= 0; --i) {
            this.b[i].u(view);
        }
    }
    
    public final void O(final v v, final androidx.recyclerview.widget.f f) {
        if (f.a) {
            if (!f.i) {
                int f3 = 0;
                Label_0051: {
                    Label_0046: {
                        int g = 0;
                        Label_0037: {
                            if (f.b == 0) {
                                if (f.e != -1) {
                                    break Label_0046;
                                }
                            }
                            else if (f.e == -1) {
                                final int f2 = f.f;
                                final int a = f2 - this.B(f2);
                                if (a >= 0) {
                                    g = f.g - Math.min(a, f.b);
                                    break Label_0037;
                                }
                            }
                            else {
                                final int a2 = this.C(f.g) - f.g;
                                if (a2 < 0) {
                                    break Label_0046;
                                }
                                f3 = Math.min(a2, f.b) + f.f;
                                break Label_0051;
                            }
                            g = f.g;
                        }
                        this.P(v, g);
                        return;
                    }
                    f3 = f.f;
                }
                this.Q(v, f3);
            }
        }
    }
    
    public final void P(final v v, final int n) {
        for (int i = ((RecyclerView.o)this).getChildCount() - 1; i >= 0; --i) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            if (this.c.g(child) < n || this.c.q(child) < n) {
                break;
            }
            final c c = (c)child.getLayoutParams();
            if (c.b) {
                final int n2 = 0;
                int n3 = 0;
                int j;
                while (true) {
                    j = n2;
                    if (n3 >= this.a) {
                        break;
                    }
                    if (this.b[n3].a.size() == 1) {
                        return;
                    }
                    ++n3;
                }
                while (j < this.a) {
                    this.b[j].s();
                    ++j;
                }
            }
            else {
                if (c.a.a.size() == 1) {
                    return;
                }
                c.a.s();
            }
            ((RecyclerView.o)this).removeAndRecycleView(child, v);
        }
    }
    
    public final void Q(final v v, final int n) {
        while (((RecyclerView.o)this).getChildCount() > 0) {
            final int n2 = 0;
            final View child = ((RecyclerView.o)this).getChildAt(0);
            if (this.c.d(child) > n || this.c.p(child) > n) {
                break;
            }
            final c c = (c)child.getLayoutParams();
            if (c.b) {
                int n3 = 0;
                int i;
                while (true) {
                    i = n2;
                    if (n3 >= this.a) {
                        break;
                    }
                    if (this.b[n3].a.size() == 1) {
                        return;
                    }
                    ++n3;
                }
                while (i < this.a) {
                    this.b[i].t();
                    ++i;
                }
            }
            else {
                if (c.a.a.size() == 1) {
                    return;
                }
                c.a.t();
            }
            ((RecyclerView.o)this).removeAndRecycleView(child, v);
        }
    }
    
    public final void R() {
        if (this.d.k() == 1073741824) {
            return;
        }
        final int childCount = ((RecyclerView.o)this).getChildCount();
        final int n = 0;
        float max = 0.0f;
        for (int i = 0; i < childCount; ++i) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            final float n2 = (float)this.d.e(child);
            if (n2 >= max) {
                float b = n2;
                if (((c)child.getLayoutParams()).c()) {
                    b = n2 * 1.0f / this.a;
                }
                max = Math.max(max, b);
            }
        }
        final int f = this.f;
        int a = Math.round(max * this.a);
        if (this.d.k() == Integer.MIN_VALUE) {
            a = Math.min(a, this.d.n());
        }
        this.a0(a);
        int j = n;
        if (this.f == f) {
            return;
        }
        while (j < childCount) {
            final View child2 = ((RecyclerView.o)this).getChildAt(j);
            final c c = (c)child2.getLayoutParams();
            if (!c.b) {
                if (this.isLayoutRTL() && this.e == 1) {
                    final int a2 = this.a;
                    final int e = c.a.e;
                    child2.offsetLeftAndRight(-(a2 - 1 - e) * this.f - -(a2 - 1 - e) * f);
                }
                else {
                    final int e2 = c.a.e;
                    final int f2 = this.f;
                    final int e3 = this.e;
                    final int n3 = f2 * e2 - e2 * f;
                    if (e3 == 1) {
                        child2.offsetLeftAndRight(n3);
                    }
                    else {
                        child2.offsetTopAndBottom(n3);
                    }
                }
            }
            ++j;
        }
    }
    
    public final void S() {
        boolean h;
        if (this.e != 1 && this.isLayoutRTL()) {
            h = (this.h ^ true);
        }
        else {
            h = this.h;
        }
        this.i = h;
    }
    
    public final void T(int n) {
        final androidx.recyclerview.widget.f g = this.g;
        g.e = n;
        final boolean i = this.i;
        final int n2 = 1;
        if (i == (n == -1)) {
            n = n2;
        }
        else {
            n = -1;
        }
        g.d = n;
    }
    
    public void U(int i) {
        this.assertNotInLayoutOrScroll(null);
        if (i != this.a) {
            this.H();
            this.a = i;
            this.j = new BitSet(this.a);
            this.b = new f[this.a];
            for (i = 0; i < this.a; ++i) {
                this.b[i] = new f(i);
            }
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    public final void V(final int n, final int n2) {
        for (int i = 0; i < this.a; ++i) {
            if (!this.b[i].a.isEmpty()) {
                this.b0(this.b[i], n, n2);
            }
        }
    }
    
    public final boolean W(final a0 a0, final b b) {
        final boolean p2 = this.p;
        final int b2 = a0.b();
        int a2;
        if (p2) {
            a2 = this.v(b2);
        }
        else {
            a2 = this.r(b2);
        }
        b.a = a2;
        b.b = Integer.MIN_VALUE;
        return true;
    }
    
    public boolean X(final a0 a0, final b b) {
        final boolean e = a0.e();
        boolean c = false;
        if (!e) {
            final int k = this.k;
            if (k != -1) {
                if (k >= 0 && k < a0.b()) {
                    final e t = this.t;
                    if (t != null && t.a != -1 && t.c >= 1) {
                        b.b = Integer.MIN_VALUE;
                        b.a = this.k;
                    }
                    else {
                        final View viewByPosition = ((RecyclerView.o)this).findViewByPosition(this.k);
                        if (viewByPosition != null) {
                            int a2;
                            if (this.i) {
                                a2 = this.z();
                            }
                            else {
                                a2 = this.y();
                            }
                            b.a = a2;
                            if (this.l != Integer.MIN_VALUE) {
                                int n;
                                int n2;
                                if (b.c) {
                                    n = this.c.i() - this.l;
                                    n2 = this.c.d(viewByPosition);
                                }
                                else {
                                    n = this.c.m() + this.l;
                                    n2 = this.c.g(viewByPosition);
                                }
                                b.b = n - n2;
                                return true;
                            }
                            if (this.c.e(viewByPosition) > this.c.n()) {
                                int b2;
                                if (b.c) {
                                    b2 = this.c.i();
                                }
                                else {
                                    b2 = this.c.m();
                                }
                                b.b = b2;
                                return true;
                            }
                            final int n3 = this.c.g(viewByPosition) - this.c.m();
                            if (n3 < 0) {
                                b.b = -n3;
                                return true;
                            }
                            final int b3 = this.c.i() - this.c.d(viewByPosition);
                            if (b3 < 0) {
                                b.b = b3;
                                return true;
                            }
                            b.b = Integer.MIN_VALUE;
                        }
                        else {
                            final int i = this.k;
                            b.a = i;
                            final int l = this.l;
                            if (l == Integer.MIN_VALUE) {
                                if (this.k(i) == 1) {
                                    c = true;
                                }
                                b.c = c;
                                b.a();
                            }
                            else {
                                b.b(l);
                            }
                            b.d = true;
                        }
                    }
                    return true;
                }
                this.k = -1;
                this.l = Integer.MIN_VALUE;
            }
        }
        return false;
    }
    
    public void Y(final a0 a0, final b b) {
        if (this.X(a0, b)) {
            return;
        }
        if (this.W(a0, b)) {
            return;
        }
        b.a();
        b.a = 0;
    }
    
    public final void Z(int n, final a0 a0) {
        final androidx.recyclerview.widget.f g = this.g;
        final boolean b = false;
        g.b = 0;
        g.c = n;
        int n2 = 0;
        Label_0095: {
            if (((RecyclerView.o)this).isSmoothScrolling()) {
                final int c = a0.c();
                if (c != -1) {
                    if (this.i == c < n) {
                        n = this.c.n();
                        n2 = 0;
                        break Label_0095;
                    }
                    n2 = this.c.n();
                    n = 0;
                    break Label_0095;
                }
            }
            n = 0;
            n2 = 0;
        }
        if (((RecyclerView.o)this).getClipToPadding()) {
            this.g.f = this.c.m() - n2;
            this.g.g = this.c.i() + n;
        }
        else {
            this.g.g = this.c.h() + n;
            this.g.f = -n2;
        }
        final androidx.recyclerview.widget.f g2 = this.g;
        g2.h = false;
        g2.a = true;
        boolean i = b;
        if (this.c.k() == 0) {
            i = b;
            if (this.c.h() == 0) {
                i = true;
            }
        }
        g2.i = i;
    }
    
    public void a0(final int n) {
        this.f = n / this.a;
        this.v = View$MeasureSpec.makeMeasureSpec(n, this.d.k());
    }
    
    @Override
    public void assertNotInLayoutOrScroll(final String s) {
        if (this.t == null) {
            super.assertNotInLayoutOrScroll(s);
        }
    }
    
    public final void b0(final f f, final int n, final int n2) {
        final int j = f.j();
        if (n == -1) {
            if (f.o() + j > n2) {
                return;
            }
        }
        else if (f.k() - j < n2) {
            return;
        }
        this.j.set(f.e, false);
    }
    
    public final int c0(final int n, final int n2, final int n3) {
        if (n2 == 0 && n3 == 0) {
            return n;
        }
        final int mode = View$MeasureSpec.getMode(n);
        if (mode != Integer.MIN_VALUE && mode != 1073741824) {
            return n;
        }
        return View$MeasureSpec.makeMeasureSpec(Math.max(0, View$MeasureSpec.getSize(n) - n2 - n3), mode);
    }
    
    @Override
    public boolean canScrollHorizontally() {
        return this.e == 0;
    }
    
    @Override
    public boolean canScrollVertically() {
        final int e = this.e;
        boolean b = true;
        if (e != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean checkLayoutParams(final p p) {
        return p instanceof c;
    }
    
    @Override
    public void collectAdjacentPrefetchPositions(int toIndex, int i, final a0 a0, final o.c c) {
        if (this.e != 0) {
            toIndex = i;
        }
        if (((RecyclerView.o)this).getChildCount() != 0) {
            if (toIndex != 0) {
                this.M(toIndex, a0);
                final int[] a2 = this.A;
                if (a2 == null || a2.length < this.a) {
                    this.A = new int[this.a];
                }
                final int n = 0;
                i = 0;
                toIndex = 0;
                while (i < this.a) {
                    final androidx.recyclerview.widget.f g = this.g;
                    int n2;
                    int n3;
                    if (g.d == -1) {
                        n2 = g.f;
                        n3 = this.b[i].p(n2);
                    }
                    else {
                        n2 = this.b[i].l(g.g);
                        n3 = this.g.g;
                    }
                    final int n4 = n2 - n3;
                    int n5 = toIndex;
                    if (n4 >= 0) {
                        this.A[toIndex] = n4;
                        n5 = toIndex + 1;
                    }
                    ++i;
                    toIndex = n5;
                }
                Arrays.sort(this.A, 0, toIndex);
                androidx.recyclerview.widget.f g2;
                for (i = n; i < toIndex && this.g.a(a0); ++i) {
                    c.a(this.g.c, this.A[i]);
                    g2 = this.g;
                    g2.c += g2.d;
                }
            }
        }
    }
    
    @Override
    public int computeHorizontalScrollExtent(final a0 a0) {
        return this.computeScrollExtent(a0);
    }
    
    @Override
    public int computeHorizontalScrollOffset(final a0 a0) {
        return this.computeScrollOffset(a0);
    }
    
    @Override
    public int computeHorizontalScrollRange(final a0 a0) {
        return this.computeScrollRange(a0);
    }
    
    public final int computeScrollExtent(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        return androidx.recyclerview.widget.l.a(a0, this.c, this.t(this.z ^ true), this.s(this.z ^ true), this, this.z);
    }
    
    public final int computeScrollOffset(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        return androidx.recyclerview.widget.l.b(a0, this.c, this.t(this.z ^ true), this.s(this.z ^ true), this, this.z, this.i);
    }
    
    public final int computeScrollRange(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        return androidx.recyclerview.widget.l.c(a0, this.c, this.t(this.z ^ true), this.s(this.z ^ true), this, this.z);
    }
    
    @Override
    public PointF computeScrollVectorForPosition(int k) {
        k = this.k(k);
        final PointF pointF = new PointF();
        if (k == 0) {
            return null;
        }
        if (this.e == 0) {
            pointF.x = (float)k;
            pointF.y = 0.0f;
        }
        else {
            pointF.x = 0.0f;
            pointF.y = (float)k;
        }
        return pointF;
    }
    
    @Override
    public int computeVerticalScrollExtent(final a0 a0) {
        return this.computeScrollExtent(a0);
    }
    
    @Override
    public int computeVerticalScrollOffset(final a0 a0) {
        return this.computeScrollOffset(a0);
    }
    
    @Override
    public int computeVerticalScrollRange(final a0 a0) {
        return this.computeScrollRange(a0);
    }
    
    public final int convertFocusDirectionToLayoutDirection(int n) {
        int n2 = -1;
        final int n3 = 1;
        final int n4 = 1;
        if (n != 1) {
            if (n != 2) {
                if (n == 17) {
                    if (this.e != 0) {
                        n2 = Integer.MIN_VALUE;
                    }
                    return n2;
                }
                if (n == 33) {
                    if (this.e != 1) {
                        n2 = Integer.MIN_VALUE;
                    }
                    return n2;
                }
                if (n == 66) {
                    if (this.e == 0) {
                        n = n3;
                    }
                    else {
                        n = Integer.MIN_VALUE;
                    }
                    return n;
                }
                if (n != 130) {
                    return Integer.MIN_VALUE;
                }
                if (this.e == 1) {
                    n = n4;
                }
                else {
                    n = Integer.MIN_VALUE;
                }
                return n;
            }
            else {
                if (this.e == 1) {
                    return 1;
                }
                if (this.isLayoutRTL()) {
                    return -1;
                }
                return 1;
            }
        }
        else {
            if (this.e == 1) {
                return -1;
            }
            if (this.isLayoutRTL()) {
                return 1;
            }
            return -1;
        }
    }
    
    public final void f(final View view) {
        for (int i = this.a - 1; i >= 0; --i) {
            this.b[i].a(view);
        }
    }
    
    public final void g(final b b) {
        final e t = this.t;
        final int c = t.c;
        if (c > 0) {
            if (c == this.a) {
                for (int i = 0; i < this.a; ++i) {
                    this.b[i].e();
                    final e t2 = this.t;
                    final int n = t2.d[i];
                    int n2;
                    if ((n2 = n) != Integer.MIN_VALUE) {
                        int n3;
                        if (t2.i) {
                            n3 = this.c.i();
                        }
                        else {
                            n3 = this.c.m();
                        }
                        n2 = n + n3;
                    }
                    this.b[i].v(n2);
                }
            }
            else {
                t.c();
                final e t3 = this.t;
                t3.a = t3.b;
            }
        }
        final e t4 = this.t;
        this.q = t4.j;
        this.setReverseLayout(t4.h);
        this.S();
        final e t5 = this.t;
        final int a = t5.a;
        boolean c2;
        if (a != -1) {
            this.k = a;
            c2 = t5.i;
        }
        else {
            c2 = this.i;
        }
        b.c = c2;
        if (t5.e > 1) {
            final d m = this.m;
            m.a = t5.f;
            m.b = t5.g;
        }
    }
    
    @Override
    public p generateDefaultLayoutParams() {
        if (this.e == 0) {
            return new c(-2, -1);
        }
        return new c(-1, -2);
    }
    
    @Override
    public p generateLayoutParams(final Context context, final AttributeSet set) {
        return new c(context, set);
    }
    
    @Override
    public p generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return new c((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return new c(viewGroup$LayoutParams);
    }
    
    @Override
    public int getColumnCountForAccessibility(final v v, final a0 a0) {
        if (this.e == 1) {
            return this.a;
        }
        return super.getColumnCountForAccessibility(v, a0);
    }
    
    @Override
    public int getRowCountForAccessibility(final v v, final a0 a0) {
        if (this.e == 0) {
            return this.a;
        }
        return super.getRowCountForAccessibility(v, a0);
    }
    
    public boolean h() {
        final int l = this.b[0].l(Integer.MIN_VALUE);
        for (int i = 1; i < this.a; ++i) {
            if (this.b[i].l(Integer.MIN_VALUE) != l) {
                return false;
            }
        }
        return true;
    }
    
    public boolean i() {
        final int p = this.b[0].p(Integer.MIN_VALUE);
        for (int i = 1; i < this.a; ++i) {
            if (this.b[i].p(Integer.MIN_VALUE) != p) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean isAutoMeasureEnabled() {
        return this.n != 0;
    }
    
    public boolean isLayoutRTL() {
        final int layoutDirection = ((RecyclerView.o)this).getLayoutDirection();
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    public final void j(final View view, final c c, final androidx.recyclerview.widget.f f) {
        if (f.e == 1) {
            if (c.b) {
                this.f(view);
            }
            else {
                c.a.a(view);
            }
        }
        else if (c.b) {
            this.N(view);
        }
        else {
            c.a.u(view);
        }
    }
    
    public final int k(final int n) {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int n2 = -1;
        if (childCount == 0) {
            if (this.i) {
                n2 = 1;
            }
            return n2;
        }
        if (n < this.y() == this.i) {
            n2 = 1;
        }
        return n2;
    }
    
    public boolean l() {
        if (((RecyclerView.o)this).getChildCount() != 0 && this.n != 0 && ((RecyclerView.o)this).isAttachedToWindow()) {
            int n;
            int n2;
            if (this.i) {
                n = this.z();
                n2 = this.y();
            }
            else {
                n = this.y();
                n2 = this.z();
            }
            if (n == 0 && this.G() != null) {
                this.m.b();
            }
            else {
                if (!this.y) {
                    return false;
                }
                int n3;
                if (this.i) {
                    n3 = -1;
                }
                else {
                    n3 = 1;
                }
                final d m = this.m;
                ++n2;
                final a e = m.e(n, n2, n3, true);
                if (e == null) {
                    this.y = false;
                    this.m.d(n2);
                    return false;
                }
                final a e2 = this.m.e(n, e.a, n3 * -1, true);
                if (e2 == null) {
                    this.m.d(e.a);
                }
                else {
                    this.m.d(e2.a + 1);
                }
            }
            ((RecyclerView.o)this).requestSimpleAnimationsInNextLayout();
            ((RecyclerView.o)this).requestLayout();
            return true;
        }
        return false;
    }
    
    public final boolean m(final f f) {
        if (this.i) {
            if (f.k() < this.c.i()) {
                final ArrayList a = f.a;
                return f.n((View)a.get(a.size() - 1)).b ^ true;
            }
        }
        else if (f.o() > this.c.m()) {
            return f.n(f.a.get(0)).b ^ true;
        }
        return false;
    }
    
    public final a n(final int n) {
        final a a = new a();
        a.c = new int[this.a];
        for (int i = 0; i < this.a; ++i) {
            a.c[i] = n - this.b[i].l(n);
        }
        return a;
    }
    
    public final a o(final int n) {
        final a a = new a();
        a.c = new int[this.a];
        for (int i = 0; i < this.a; ++i) {
            a.c[i] = this.b[i].p(n) - n;
        }
        return a;
    }
    
    @Override
    public void offsetChildrenHorizontal(final int n) {
        super.offsetChildrenHorizontal(n);
        for (int i = 0; i < this.a; ++i) {
            this.b[i].r(n);
        }
    }
    
    @Override
    public void offsetChildrenVertical(final int n) {
        super.offsetChildrenVertical(n);
        for (int i = 0; i < this.a; ++i) {
            this.b[i].r(n);
        }
    }
    
    @Override
    public void onDetachedFromWindow(final RecyclerView recyclerView, final v v) {
        super.onDetachedFromWindow(recyclerView, v);
        ((RecyclerView.o)this).removeCallbacks(this.C);
        for (int i = 0; i < this.a; ++i) {
            this.b[i].e();
        }
        recyclerView.requestLayout();
    }
    
    @Override
    public View onFocusSearchFailed(View containingItemView, int n, final v v, final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return null;
        }
        containingItemView = ((RecyclerView.o)this).findContainingItemView(containingItemView);
        if (containingItemView == null) {
            return null;
        }
        this.S();
        final int convertFocusDirectionToLayoutDirection = this.convertFocusDirectionToLayoutDirection(n);
        if (convertFocusDirectionToLayoutDirection == Integer.MIN_VALUE) {
            return null;
        }
        final c c = (c)containingItemView.getLayoutParams();
        final boolean b = c.b;
        final f a2 = c.a;
        if (convertFocusDirectionToLayoutDirection == 1) {
            n = this.z();
        }
        else {
            n = this.y();
        }
        this.Z(n, a0);
        this.T(convertFocusDirectionToLayoutDirection);
        final androidx.recyclerview.widget.f g = this.g;
        g.c = g.d + n;
        g.b = (int)(this.c.n() * 0.33333334f);
        final androidx.recyclerview.widget.f g2 = this.g;
        g2.h = true;
        final int n2 = 0;
        g2.a = false;
        this.q(v, g2, a0);
        this.p = this.i;
        if (!b) {
            final View m = a2.m(n, convertFocusDirectionToLayoutDirection);
            if (m != null && m != containingItemView) {
                return m;
            }
        }
        if (this.L(convertFocusDirectionToLayoutDirection)) {
            for (int i = this.a - 1; i >= 0; --i) {
                final View j = this.b[i].m(n, convertFocusDirectionToLayoutDirection);
                if (j != null && j != containingItemView) {
                    return j;
                }
            }
        }
        else {
            for (int k = 0; k < this.a; ++k) {
                final View l = this.b[k].m(n, convertFocusDirectionToLayoutDirection);
                if (l != null && l != containingItemView) {
                    return l;
                }
            }
        }
        final boolean h = this.h;
        if (convertFocusDirectionToLayoutDirection == -1) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (((h ^ true) ? 1 : 0) == n) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (!b) {
            int n3;
            if (n != 0) {
                n3 = a2.f();
            }
            else {
                n3 = a2.g();
            }
            final View viewByPosition = ((RecyclerView.o)this).findViewByPosition(n3);
            if (viewByPosition != null && viewByPosition != containingItemView) {
                return viewByPosition;
            }
        }
        int n4 = n2;
        if (this.L(convertFocusDirectionToLayoutDirection)) {
            for (int n5 = this.a - 1; n5 >= 0; --n5) {
                if (n5 != a2.e) {
                    final f[] b2 = this.b;
                    int n6;
                    if (n != 0) {
                        n6 = b2[n5].f();
                    }
                    else {
                        n6 = b2[n5].g();
                    }
                    final View viewByPosition2 = ((RecyclerView.o)this).findViewByPosition(n6);
                    if (viewByPosition2 != null && viewByPosition2 != containingItemView) {
                        return viewByPosition2;
                    }
                }
            }
        }
        else {
            while (n4 < this.a) {
                final f[] b3 = this.b;
                int n7;
                if (n != 0) {
                    n7 = b3[n4].f();
                }
                else {
                    n7 = b3[n4].g();
                }
                final View viewByPosition3 = ((RecyclerView.o)this).findViewByPosition(n7);
                if (viewByPosition3 != null && viewByPosition3 != containingItemView) {
                    return viewByPosition3;
                }
                ++n4;
            }
        }
        return null;
    }
    
    @Override
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (((RecyclerView.o)this).getChildCount() > 0) {
            final View t = this.t(false);
            final View s = this.s(false);
            if (t != null) {
                if (s != null) {
                    final int position = ((RecyclerView.o)this).getPosition(t);
                    final int position2 = ((RecyclerView.o)this).getPosition(s);
                    if (position < position2) {
                        ((AccessibilityRecord)accessibilityEvent).setFromIndex(position);
                        ((AccessibilityRecord)accessibilityEvent).setToIndex(position2);
                    }
                    else {
                        ((AccessibilityRecord)accessibilityEvent).setFromIndex(position2);
                        ((AccessibilityRecord)accessibilityEvent).setToIndex(position);
                    }
                }
            }
        }
    }
    
    @Override
    public void onInitializeAccessibilityNodeInfoForItem(final v v, final a0 a0, final View view, final n1 n1) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof c)) {
            super.onInitializeAccessibilityNodeInfoForItem(view, n1);
            return;
        }
        final c c = (c)layoutParams;
        final int e = this.e;
        int a2 = 1;
        final int n2 = 1;
        int b;
        int a3;
        int b2;
        int n3;
        if (e == 0) {
            b = c.b();
            a3 = n2;
            if (c.b) {
                a3 = this.a;
            }
            b2 = -1;
            n3 = -1;
        }
        else {
            b = -1;
            final int n4 = -1;
            b2 = c.b();
            if (c.b) {
                a2 = this.a;
            }
            n3 = a2;
            a3 = n4;
        }
        n1.b0(n1.c.a(b, a3, b2, n3, false, false));
    }
    
    @Override
    public void onItemsAdded(final RecyclerView recyclerView, final int n, final int n2) {
        this.F(n, n2, 1);
    }
    
    @Override
    public void onItemsChanged(final RecyclerView recyclerView) {
        this.m.b();
        ((RecyclerView.o)this).requestLayout();
    }
    
    @Override
    public void onItemsMoved(final RecyclerView recyclerView, final int n, final int n2, final int n3) {
        this.F(n, n2, 8);
    }
    
    @Override
    public void onItemsRemoved(final RecyclerView recyclerView, final int n, final int n2) {
        this.F(n, n2, 2);
    }
    
    @Override
    public void onItemsUpdated(final RecyclerView recyclerView, final int n, final int n2, final Object o) {
        this.F(n, n2, 4);
    }
    
    @Override
    public void onLayoutChildren(final v v, final a0 a0) {
        this.K(v, a0, true);
    }
    
    @Override
    public void onLayoutCompleted(final a0 a0) {
        super.onLayoutCompleted(a0);
        this.k = -1;
        this.l = Integer.MIN_VALUE;
        this.t = null;
        this.x.c();
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable instanceof e) {
            this.t = (e)parcelable;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        if (this.t != null) {
            return (Parcelable)new e(this.t);
        }
        final e e = new e();
        e.h = this.h;
        e.i = this.p;
        e.j = this.q;
        final d m = this.m;
        int i = 0;
        Label_0114: {
            if (m != null) {
                final int[] a = m.a;
                if (a != null) {
                    e.f = a;
                    e.e = a.length;
                    e.g = m.b;
                    break Label_0114;
                }
            }
            e.e = 0;
        }
        if (((RecyclerView.o)this).getChildCount() > 0) {
            int a2;
            if (this.p) {
                a2 = this.z();
            }
            else {
                a2 = this.y();
            }
            e.a = a2;
            e.b = this.u();
            final int a3 = this.a;
            e.c = a3;
            e.d = new int[a3];
            while (i < this.a) {
                int n2 = 0;
                Label_0253: {
                    int n;
                    int n3;
                    if (this.p) {
                        n = this.b[i].l(Integer.MIN_VALUE);
                        if ((n2 = n) == Integer.MIN_VALUE) {
                            break Label_0253;
                        }
                        n3 = this.c.i();
                    }
                    else {
                        n = this.b[i].p(Integer.MIN_VALUE);
                        if ((n2 = n) == Integer.MIN_VALUE) {
                            break Label_0253;
                        }
                        n3 = this.c.m();
                    }
                    n2 = n - n3;
                }
                e.d[i] = n2;
                ++i;
            }
        }
        else {
            e.a = -1;
            e.b = -1;
            e.c = 0;
        }
        return (Parcelable)e;
    }
    
    @Override
    public void onScrollStateChanged(final int n) {
        if (n == 0) {
            this.l();
        }
    }
    
    public final void p() {
        this.c = androidx.recyclerview.widget.i.b(this, this.e);
        this.d = androidx.recyclerview.widget.i.b(this, 1 - this.e);
    }
    
    public final int q(final v v, final androidx.recyclerview.widget.f f, final a0 a0) {
        this.j.set(0, this.a, true);
        int n;
        if (this.g.i) {
            if (f.e == 1) {
                n = Integer.MAX_VALUE;
            }
            else {
                n = Integer.MIN_VALUE;
            }
        }
        else if (f.e == 1) {
            n = f.g + f.b;
        }
        else {
            n = f.f - f.b;
        }
        this.V(f.e, n);
        int n2;
        if (this.i) {
            n2 = this.c.i();
        }
        else {
            n2 = this.c.m();
        }
        boolean b = false;
        while (f.a(a0) && (this.g.i || !this.j.isEmpty())) {
            final View b2 = f.b(v);
            final c c = (c)b2.getLayoutParams();
            final int viewLayoutPosition = ((RecyclerView.p)c).getViewLayoutPosition();
            final int g = this.m.g(viewLayoutPosition);
            final boolean b3 = g == -1;
            f e;
            if (b3) {
                if (c.b) {
                    e = this.b[0];
                }
                else {
                    e = this.E(f);
                }
                this.m.n(viewLayoutPosition, e);
            }
            else {
                e = this.b[g];
            }
            c.a = e;
            if (f.e == 1) {
                ((RecyclerView.o)this).addView(b2);
            }
            else {
                ((RecyclerView.o)this).addView(b2, 0);
            }
            this.J(b2, c, false);
            int n6;
            int n7;
            if (f.e == 1) {
                int n3;
                if (c.b) {
                    n3 = this.A(n2);
                }
                else {
                    n3 = e.l(n2);
                }
                final int e2 = this.c.e(b2);
                if (b3 && c.b) {
                    final a n4 = this.n(n3);
                    n4.b = -1;
                    n4.a = viewLayoutPosition;
                    this.m.a(n4);
                }
                final int n5 = e2 + n3;
                n6 = n3;
                n7 = n5;
            }
            else {
                if (c.b) {
                    n7 = this.D(n2);
                }
                else {
                    n7 = e.p(n2);
                }
                n6 = n7 - this.c.e(b2);
                if (b3 && c.b) {
                    final a o = this.o(n7);
                    o.b = 1;
                    o.a = viewLayoutPosition;
                    this.m.a(o);
                }
            }
            Label_0576: {
                if (c.b && f.d == -1) {
                    if (!b3) {
                        boolean b4;
                        if (f.e == 1) {
                            b4 = this.h();
                        }
                        else {
                            b4 = this.i();
                        }
                        if (!(b4 ^ true)) {
                            break Label_0576;
                        }
                        final a f2 = this.m.f(viewLayoutPosition);
                        if (f2 != null) {
                            f2.d = true;
                        }
                    }
                    this.y = true;
                }
            }
            this.j(b2, c, f);
            int n8;
            int n9;
            if (this.isLayoutRTL() && this.e == 1) {
                int i;
                if (c.b) {
                    i = this.d.i();
                }
                else {
                    i = this.d.i() - (this.a - 1 - e.e) * this.f;
                }
                final int e3 = this.d.e(b2);
                n8 = i;
                n9 = i - e3;
            }
            else {
                int m;
                if (c.b) {
                    m = this.d.m();
                }
                else {
                    m = e.e * this.f + this.d.m();
                }
                final int e4 = this.d.e(b2);
                final int n10 = m;
                n8 = e4 + m;
                n9 = n10;
            }
            if (this.e == 1) {
                final int n11 = n6;
                n6 = n9;
                n9 = n11;
            }
            else {
                final int n12 = n7;
                n7 = n8;
                n8 = n12;
            }
            ((RecyclerView.o)this).layoutDecoratedWithMargins(b2, n6, n9, n8, n7);
            if (c.b) {
                this.V(this.g.e, n);
            }
            else {
                this.b0(e, this.g.e, n);
            }
            this.O(v, this.g);
            if (this.g.h && b2.hasFocusable()) {
                if (c.b) {
                    this.j.clear();
                }
                else {
                    this.j.set(e.e, false);
                }
            }
            b = true;
        }
        if (!b) {
            this.O(v, this.g);
        }
        int b5;
        if (this.g.e == -1) {
            b5 = this.c.m() - this.D(this.c.m());
        }
        else {
            b5 = this.A(this.c.i()) - this.c.i();
        }
        int min;
        if (b5 > 0) {
            min = Math.min(f.b, b5);
        }
        else {
            min = 0;
        }
        return min;
    }
    
    public final int r(final int n) {
        for (int childCount = ((RecyclerView.o)this).getChildCount(), i = 0; i < childCount; ++i) {
            final int position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(i));
            if (position >= 0 && position < n) {
                return position;
            }
        }
        return 0;
    }
    
    public View s(final boolean b) {
        final int m = this.c.m();
        final int i = this.c.i();
        int j = ((RecyclerView.o)this).getChildCount() - 1;
        View view = null;
        while (j >= 0) {
            final View child = ((RecyclerView.o)this).getChildAt(j);
            final int g = this.c.g(child);
            final int d = this.c.d(child);
            View view2 = view;
            if (d > m) {
                if (g >= i) {
                    view2 = view;
                }
                else {
                    if (d <= i || !b) {
                        return child;
                    }
                    if ((view2 = view) == null) {
                        view2 = child;
                    }
                }
            }
            --j;
            view = view2;
        }
        return view;
    }
    
    public int scrollBy(int n, final v v, final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() != 0 && n != 0) {
            this.M(n, a0);
            final int q = this.q(v, this.g, a0);
            if (this.g.b >= q) {
                if (n < 0) {
                    n = -q;
                }
                else {
                    n = q;
                }
            }
            this.c.r(-n);
            this.p = this.i;
            final androidx.recyclerview.widget.f g = this.g;
            g.b = 0;
            this.O(v, g);
            return n;
        }
        return 0;
    }
    
    @Override
    public int scrollHorizontallyBy(final int n, final v v, final a0 a0) {
        return this.scrollBy(n, v, a0);
    }
    
    @Override
    public void scrollToPosition(final int k) {
        final e t = this.t;
        if (t != null && t.a != k) {
            t.b();
        }
        this.k = k;
        this.l = Integer.MIN_VALUE;
        ((RecyclerView.o)this).requestLayout();
    }
    
    @Override
    public int scrollVerticallyBy(final int n, final v v, final a0 a0) {
        return this.scrollBy(n, v, a0);
    }
    
    @Override
    public void setMeasuredDimension(final Rect rect, int chooseSize, int chooseSize2) {
        final int n = ((RecyclerView.o)this).getPaddingLeft() + ((RecyclerView.o)this).getPaddingRight();
        final int n2 = ((RecyclerView.o)this).getPaddingTop() + ((RecyclerView.o)this).getPaddingBottom();
        if (this.e == 1) {
            chooseSize2 = RecyclerView.o.chooseSize(chooseSize2, rect.height() + n2, ((RecyclerView.o)this).getMinimumHeight());
            final int chooseSize3 = RecyclerView.o.chooseSize(chooseSize, this.f * this.a + n, ((RecyclerView.o)this).getMinimumWidth());
            chooseSize = chooseSize2;
            chooseSize2 = chooseSize3;
        }
        else {
            chooseSize = RecyclerView.o.chooseSize(chooseSize, rect.width() + n, ((RecyclerView.o)this).getMinimumWidth());
            final int chooseSize4 = RecyclerView.o.chooseSize(chooseSize2, this.f * this.a + n2, ((RecyclerView.o)this).getMinimumHeight());
            chooseSize2 = chooseSize;
            chooseSize = chooseSize4;
        }
        ((RecyclerView.o)this).setMeasuredDimension(chooseSize2, chooseSize);
    }
    
    public void setOrientation(final int e) {
        if (e != 0 && e != 1) {
            throw new IllegalArgumentException("invalid orientation.");
        }
        this.assertNotInLayoutOrScroll(null);
        if (e == this.e) {
            return;
        }
        this.e = e;
        final androidx.recyclerview.widget.i c = this.c;
        this.c = this.d;
        this.d = c;
        ((RecyclerView.o)this).requestLayout();
    }
    
    public void setReverseLayout(final boolean b) {
        this.assertNotInLayoutOrScroll(null);
        final e t = this.t;
        if (t != null && t.h != b) {
            t.h = b;
        }
        this.h = b;
        ((RecyclerView.o)this).requestLayout();
    }
    
    @Override
    public void smoothScrollToPosition(final RecyclerView recyclerView, final a0 a0, final int targetPosition) {
        final androidx.recyclerview.widget.g g = new androidx.recyclerview.widget.g(((View)recyclerView).getContext());
        ((RecyclerView.z)g).setTargetPosition(targetPosition);
        ((RecyclerView.o)this).startSmoothScroll(g);
    }
    
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return this.t == null;
    }
    
    public View t(final boolean b) {
        final int m = this.c.m();
        final int i = this.c.i();
        final int childCount = ((RecyclerView.o)this).getChildCount();
        View view = null;
        View view2;
        for (int j = 0; j < childCount; ++j, view = view2) {
            final View child = ((RecyclerView.o)this).getChildAt(j);
            final int g = this.c.g(child);
            view2 = view;
            if (this.c.d(child) > m) {
                if (g >= i) {
                    view2 = view;
                }
                else {
                    if (g >= m || !b) {
                        return child;
                    }
                    if ((view2 = view) == null) {
                        view2 = child;
                    }
                }
            }
        }
        return view;
    }
    
    public int u() {
        View view;
        if (this.i) {
            view = this.s(true);
        }
        else {
            view = this.t(true);
        }
        int position;
        if (view == null) {
            position = -1;
        }
        else {
            position = ((RecyclerView.o)this).getPosition(view);
        }
        return position;
    }
    
    public final int v(final int n) {
        for (int i = ((RecyclerView.o)this).getChildCount() - 1; i >= 0; --i) {
            final int position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(i));
            if (position >= 0 && position < n) {
                return position;
            }
        }
        return 0;
    }
    
    public final void w(final v v, final a0 a0, final boolean b) {
        final int a2 = this.A(Integer.MIN_VALUE);
        if (a2 == Integer.MIN_VALUE) {
            return;
        }
        final int n = this.c.i() - a2;
        if (n > 0) {
            final int n2 = n - -this.scrollBy(-n, v, a0);
            if (b && n2 > 0) {
                this.c.r(n2);
            }
        }
    }
    
    public final void x(final v v, final a0 a0, final boolean b) {
        final int d = this.D(Integer.MAX_VALUE);
        if (d == Integer.MAX_VALUE) {
            return;
        }
        final int n = d - this.c.m();
        if (n > 0) {
            final int n2 = n - this.scrollBy(n, v, a0);
            if (b && n2 > 0) {
                this.c.r(-n2);
            }
        }
    }
    
    public int y() {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int position = 0;
        if (childCount != 0) {
            position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(0));
        }
        return position;
    }
    
    public int z() {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int position;
        if (childCount == 0) {
            position = 0;
        }
        else {
            position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(childCount - 1));
        }
        return position;
    }
    
    public class b
    {
        public int a;
        public int b;
        public boolean c;
        public boolean d;
        public boolean e;
        public int[] f;
        public final StaggeredGridLayoutManager g;
        
        public b(final StaggeredGridLayoutManager g) {
            this.g = g;
            this.c();
        }
        
        public void a() {
            int b;
            if (this.c) {
                b = this.g.c.i();
            }
            else {
                b = this.g.c.m();
            }
            this.b = b;
        }
        
        public void b(int b) {
            if (this.c) {
                b = this.g.c.i() - b;
            }
            else {
                b += this.g.c.m();
            }
            this.b = b;
        }
        
        public void c() {
            this.a = -1;
            this.b = Integer.MIN_VALUE;
            this.c = false;
            this.d = false;
            this.e = false;
            final int[] f = this.f;
            if (f != null) {
                Arrays.fill(f, -1);
            }
        }
        
        public void d(final f[] array) {
            final int length = array.length;
            final int[] f = this.f;
            if (f == null || f.length < length) {
                this.f = new int[this.g.b.length];
            }
            for (int i = 0; i < length; ++i) {
                this.f[i] = array[i].p(Integer.MIN_VALUE);
            }
        }
    }
    
    public static class c extends p
    {
        public f a;
        public boolean b;
        
        public c(final int n, final int n2) {
            super(n, n2);
        }
        
        public c(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        public c(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
        }
        
        public c(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
        }
        
        public final int b() {
            final f a = this.a;
            if (a == null) {
                return -1;
            }
            return a.e;
        }
        
        public boolean c() {
            return this.b;
        }
    }
    
    public static class d
    {
        public int[] a;
        public List b;
        
        public void a(final a a) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            for (int size = this.b.size(), i = 0; i < size; ++i) {
                final a a2 = this.b.get(i);
                if (a2.a == a.a) {
                    this.b.remove(i);
                }
                if (a2.a >= a.a) {
                    this.b.add(i, a);
                    return;
                }
            }
            this.b.add(a);
        }
        
        public void b() {
            final int[] a = this.a;
            if (a != null) {
                Arrays.fill(a, -1);
            }
            this.b = null;
        }
        
        public void c(final int a) {
            final int[] a2 = this.a;
            if (a2 == null) {
                Arrays.fill(this.a = new int[Math.max(a, 10) + 1], -1);
            }
            else if (a >= a2.length) {
                System.arraycopy(a2, 0, this.a = new int[this.o(a)], 0, a2.length);
                final int[] a3 = this.a;
                Arrays.fill(a3, a2.length, a3.length, -1);
            }
        }
        
        public int d(final int n) {
            final List b = this.b;
            if (b != null) {
                for (int i = b.size() - 1; i >= 0; --i) {
                    if (((a)this.b.get(i)).a >= n) {
                        this.b.remove(i);
                    }
                }
            }
            return this.h(n);
        }
        
        public a e(final int n, final int n2, final int n3, final boolean b) {
            final List b2 = this.b;
            if (b2 == null) {
                return null;
            }
            for (int size = b2.size(), i = 0; i < size; ++i) {
                final a a = this.b.get(i);
                final int a2 = a.a;
                if (a2 >= n2) {
                    return null;
                }
                if (a2 >= n && (n3 == 0 || a.b == n3 || (b && a.d))) {
                    return a;
                }
            }
            return null;
        }
        
        public a f(final int n) {
            final List b = this.b;
            if (b == null) {
                return null;
            }
            for (int i = b.size() - 1; i >= 0; --i) {
                final a a = this.b.get(i);
                if (a.a == n) {
                    return a;
                }
            }
            return null;
        }
        
        public int g(final int n) {
            final int[] a = this.a;
            if (a != null && n < a.length) {
                return a[n];
            }
            return -1;
        }
        
        public int h(final int n) {
            final int[] a = this.a;
            if (a == null) {
                return -1;
            }
            if (n >= a.length) {
                return -1;
            }
            int i = this.i(n);
            if (i == -1) {
                final int[] a2 = this.a;
                Arrays.fill(a2, n, a2.length, -1);
                return this.a.length;
            }
            final int[] a3 = this.a;
            ++i;
            Arrays.fill(a3, n, i, -1);
            return i;
        }
        
        public final int i(final int n) {
            if (this.b == null) {
                return -1;
            }
            final a f = this.f(n);
            if (f != null) {
                this.b.remove(f);
            }
            final int size = this.b.size();
            int i = 0;
            while (true) {
                while (i < size) {
                    if (((a)this.b.get(i)).a >= n) {
                        if (i != -1) {
                            final a a = this.b.get(i);
                            this.b.remove(i);
                            return a.a;
                        }
                        return -1;
                    }
                    else {
                        ++i;
                    }
                }
                i = -1;
                continue;
            }
        }
        
        public void j(final int fromIndex, final int n) {
            final int[] a = this.a;
            if (a != null) {
                if (fromIndex < a.length) {
                    final int toIndex = fromIndex + n;
                    this.c(toIndex);
                    final int[] a2 = this.a;
                    System.arraycopy(a2, fromIndex, a2, toIndex, a2.length - fromIndex - n);
                    Arrays.fill(this.a, fromIndex, toIndex, -1);
                    this.l(fromIndex, n);
                }
            }
        }
        
        public void k(final int n, final int n2) {
            final int[] a = this.a;
            if (a != null) {
                if (n < a.length) {
                    final int n3 = n + n2;
                    this.c(n3);
                    final int[] a2 = this.a;
                    System.arraycopy(a2, n3, a2, n, a2.length - n - n2);
                    final int[] a3 = this.a;
                    Arrays.fill(a3, a3.length - n2, a3.length, -1);
                    this.m(n, n2);
                }
            }
        }
        
        public final void l(final int n, final int n2) {
            final List b = this.b;
            if (b == null) {
                return;
            }
            for (int i = b.size() - 1; i >= 0; --i) {
                final a a = this.b.get(i);
                final int a2 = a.a;
                if (a2 >= n) {
                    a.a = a2 + n2;
                }
            }
        }
        
        public final void m(final int n, final int n2) {
            final List b = this.b;
            if (b == null) {
                return;
            }
            for (int i = b.size() - 1; i >= 0; --i) {
                final a a = this.b.get(i);
                final int a2 = a.a;
                if (a2 >= n) {
                    if (a2 < n + n2) {
                        this.b.remove(i);
                    }
                    else {
                        a.a = a2 - n2;
                    }
                }
            }
        }
        
        public void n(final int n, final f f) {
            this.c(n);
            this.a[n] = f.e;
        }
        
        public int o(final int n) {
            int i;
            for (i = this.a.length; i <= n; i *= 2) {}
            return i;
        }
        
        public static class a implements Parcelable
        {
            public static final Parcelable$Creator<a> CREATOR;
            public int a;
            public int b;
            public int[] c;
            public boolean d;
            
            static {
                CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                    public a a(final Parcel parcel) {
                        return new a(parcel);
                    }
                    
                    public a[] b(final int n) {
                        return new a[n];
                    }
                };
            }
            
            public a() {
            }
            
            public a(final Parcel parcel) {
                this.a = parcel.readInt();
                this.b = parcel.readInt();
                final int int1 = parcel.readInt();
                boolean d = true;
                if (int1 != 1) {
                    d = false;
                }
                this.d = d;
                final int int2 = parcel.readInt();
                if (int2 > 0) {
                    parcel.readIntArray(this.c = new int[int2]);
                }
            }
            
            public int b(int n) {
                final int[] c = this.c;
                if (c == null) {
                    n = 0;
                }
                else {
                    n = c[n];
                }
                return n;
            }
            
            public int describeContents() {
                return 0;
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("FullSpanItem{mPosition=");
                sb.append(this.a);
                sb.append(", mGapDir=");
                sb.append(this.b);
                sb.append(", mHasUnwantedGapAfter=");
                sb.append(this.d);
                sb.append(", mGapPerSpan=");
                sb.append(Arrays.toString(this.c));
                sb.append('}');
                return sb.toString();
            }
            
            public void writeToParcel(final Parcel parcel, final int n) {
                parcel.writeInt(this.a);
                parcel.writeInt(this.b);
                parcel.writeInt((int)(this.d ? 1 : 0));
                final int[] c = this.c;
                if (c != null && c.length > 0) {
                    parcel.writeInt(c.length);
                    parcel.writeIntArray(this.c);
                }
                else {
                    parcel.writeInt(0);
                }
            }
        }
    }
    
    public static class e implements Parcelable
    {
        public static final Parcelable$Creator<e> CREATOR;
        public int a;
        public int b;
        public int c;
        public int[] d;
        public int e;
        public int[] f;
        public List g;
        public boolean h;
        public boolean i;
        public boolean j;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public e a(final Parcel parcel) {
                    return new e(parcel);
                }
                
                public e[] b(final int n) {
                    return new e[n];
                }
            };
        }
        
        public e() {
        }
        
        public e(final Parcel parcel) {
            this.a = parcel.readInt();
            this.b = parcel.readInt();
            final int int1 = parcel.readInt();
            this.c = int1;
            if (int1 > 0) {
                parcel.readIntArray(this.d = new int[int1]);
            }
            final int int2 = parcel.readInt();
            if ((this.e = int2) > 0) {
                parcel.readIntArray(this.f = new int[int2]);
            }
            final int int3 = parcel.readInt();
            final boolean b = false;
            this.h = (int3 == 1);
            this.i = (parcel.readInt() == 1);
            boolean j = b;
            if (parcel.readInt() == 1) {
                j = true;
            }
            this.j = j;
            this.g = parcel.readArrayList(a.class.getClassLoader());
        }
        
        public e(final e e) {
            this.c = e.c;
            this.a = e.a;
            this.b = e.b;
            this.d = e.d;
            this.e = e.e;
            this.f = e.f;
            this.h = e.h;
            this.i = e.i;
            this.j = e.j;
            this.g = e.g;
        }
        
        public void b() {
            this.d = null;
            this.c = 0;
            this.a = -1;
            this.b = -1;
        }
        
        public void c() {
            this.d = null;
            this.c = 0;
            this.e = 0;
            this.f = null;
            this.g = null;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            if (this.c > 0) {
                parcel.writeIntArray(this.d);
            }
            parcel.writeInt(this.e);
            if (this.e > 0) {
                parcel.writeIntArray(this.f);
            }
            parcel.writeInt((int)(this.h ? 1 : 0));
            parcel.writeInt((int)(this.i ? 1 : 0));
            parcel.writeInt((int)(this.j ? 1 : 0));
            parcel.writeList(this.g);
        }
    }
    
    public class f
    {
        public ArrayList a;
        public int b;
        public int c;
        public int d;
        public final int e;
        public final StaggeredGridLayoutManager f;
        
        public f(final StaggeredGridLayoutManager f, final int e) {
            this.f = f;
            this.a = new ArrayList();
            this.b = Integer.MIN_VALUE;
            this.c = Integer.MIN_VALUE;
            this.d = 0;
            this.e = e;
        }
        
        public void a(final View e) {
            final c n = this.n(e);
            n.a = this;
            this.a.add(e);
            this.c = Integer.MIN_VALUE;
            if (this.a.size() == 1) {
                this.b = Integer.MIN_VALUE;
            }
            if (((RecyclerView.p)n).isItemRemoved() || ((RecyclerView.p)n).isItemChanged()) {
                this.d += this.f.c.e(e);
            }
        }
        
        public void b(final boolean b, final int n) {
            int n2;
            if (b) {
                n2 = this.l(Integer.MIN_VALUE);
            }
            else {
                n2 = this.p(Integer.MIN_VALUE);
            }
            this.e();
            if (n2 == Integer.MIN_VALUE) {
                return;
            }
            if ((b && n2 < this.f.c.i()) || (!b && n2 > this.f.c.m())) {
                return;
            }
            int n3 = n2;
            if (n != Integer.MIN_VALUE) {
                n3 = n2 + n;
            }
            this.c = n3;
            this.b = n3;
        }
        
        public void c() {
            final ArrayList a = this.a;
            final View view = a.get(a.size() - 1);
            final c n = this.n(view);
            this.c = this.f.c.d(view);
            if (n.b) {
                final a f = this.f.m.f(((RecyclerView.p)n).getViewLayoutPosition());
                if (f != null && f.b == 1) {
                    this.c += f.b(this.e);
                }
            }
        }
        
        public void d() {
            final View view = this.a.get(0);
            final c n = this.n(view);
            this.b = this.f.c.g(view);
            if (n.b) {
                final a f = this.f.m.f(((RecyclerView.p)n).getViewLayoutPosition());
                if (f != null && f.b == -1) {
                    this.b -= f.b(this.e);
                }
            }
        }
        
        public void e() {
            this.a.clear();
            this.q();
            this.d = 0;
        }
        
        public int f() {
            int n;
            if (this.f.h) {
                n = this.i(this.a.size() - 1, -1, true);
            }
            else {
                n = this.i(0, this.a.size(), true);
            }
            return n;
        }
        
        public int g() {
            int n;
            if (this.f.h) {
                n = this.i(0, this.a.size(), true);
            }
            else {
                n = this.i(this.a.size() - 1, -1, true);
            }
            return n;
        }
        
        public int h(int i, final int n, final boolean b, final boolean b2, final boolean b3) {
            final int m = this.f.c.m();
            final int j = this.f.c.i();
            int n2;
            if (n > i) {
                n2 = 1;
            }
            else {
                n2 = -1;
            }
            while (i != n) {
                final View view = this.a.get(i);
                final int g = this.f.c.g(view);
                final int d = this.f.c.d(view);
                boolean b4 = false;
                final boolean b5 = b3 ? (g <= j) : (g < j);
                Label_0143: {
                    if (b3) {
                        if (d < m) {
                            break Label_0143;
                        }
                    }
                    else if (d <= m) {
                        break Label_0143;
                    }
                    b4 = true;
                }
                Label_0211: {
                    if (b5 && b4) {
                        if (b && b2) {
                            if (g < m || d > j) {
                                break Label_0211;
                            }
                        }
                        else if (!b2) {
                            if (g >= m) {
                                if (d <= j) {
                                    break Label_0211;
                                }
                            }
                        }
                        return ((RecyclerView.o)this.f).getPosition(view);
                    }
                }
                i += n2;
            }
            return -1;
        }
        
        public int i(final int n, final int n2, final boolean b) {
            return this.h(n, n2, false, false, b);
        }
        
        public int j() {
            return this.d;
        }
        
        public int k() {
            final int c = this.c;
            if (c != Integer.MIN_VALUE) {
                return c;
            }
            this.c();
            return this.c;
        }
        
        public int l(final int n) {
            final int c = this.c;
            if (c != Integer.MIN_VALUE) {
                return c;
            }
            if (this.a.size() == 0) {
                return n;
            }
            this.c();
            return this.c;
        }
        
        public View m(final int n, int n2) {
            View view = null;
            final View view2 = null;
            View view4;
            if (n2 == -1) {
                final int size = this.a.size();
                n2 = 0;
                View view3 = view2;
                while (true) {
                    view4 = view3;
                    if (n2 >= size) {
                        break;
                    }
                    final View view5 = this.a.get(n2);
                    final StaggeredGridLayoutManager f = this.f;
                    if (f.h) {
                        view4 = view3;
                        if (((RecyclerView.o)f).getPosition(view5) <= n) {
                            break;
                        }
                    }
                    final StaggeredGridLayoutManager f2 = this.f;
                    if (!f2.h && ((RecyclerView.o)f2).getPosition(view5) >= n) {
                        view4 = view3;
                        break;
                    }
                    view4 = view3;
                    if (!view5.hasFocusable()) {
                        break;
                    }
                    ++n2;
                    view3 = view5;
                }
            }
            else {
                n2 = this.a.size() - 1;
                while (true) {
                    view4 = view;
                    if (n2 < 0) {
                        break;
                    }
                    final View view6 = this.a.get(n2);
                    final StaggeredGridLayoutManager f3 = this.f;
                    if (f3.h) {
                        view4 = view;
                        if (((RecyclerView.o)f3).getPosition(view6) >= n) {
                            break;
                        }
                    }
                    final StaggeredGridLayoutManager f4 = this.f;
                    if (!f4.h && ((RecyclerView.o)f4).getPosition(view6) <= n) {
                        view4 = view;
                        break;
                    }
                    view4 = view;
                    if (!view6.hasFocusable()) {
                        break;
                    }
                    --n2;
                    view = view6;
                }
            }
            return view4;
        }
        
        public c n(final View view) {
            return (c)view.getLayoutParams();
        }
        
        public int o() {
            final int b = this.b;
            if (b != Integer.MIN_VALUE) {
                return b;
            }
            this.d();
            return this.b;
        }
        
        public int p(final int n) {
            final int b = this.b;
            if (b != Integer.MIN_VALUE) {
                return b;
            }
            if (this.a.size() == 0) {
                return n;
            }
            this.d();
            return this.b;
        }
        
        public void q() {
            this.b = Integer.MIN_VALUE;
            this.c = Integer.MIN_VALUE;
        }
        
        public void r(final int n) {
            final int b = this.b;
            if (b != Integer.MIN_VALUE) {
                this.b = b + n;
            }
            final int c = this.c;
            if (c != Integer.MIN_VALUE) {
                this.c = c + n;
            }
        }
        
        public void s() {
            final int size = this.a.size();
            final View view = this.a.remove(size - 1);
            final c n = this.n(view);
            n.a = null;
            if (((RecyclerView.p)n).isItemRemoved() || ((RecyclerView.p)n).isItemChanged()) {
                this.d -= this.f.c.e(view);
            }
            if (size == 1) {
                this.b = Integer.MIN_VALUE;
            }
            this.c = Integer.MIN_VALUE;
        }
        
        public void t() {
            final View view = this.a.remove(0);
            final c n = this.n(view);
            n.a = null;
            if (this.a.size() == 0) {
                this.c = Integer.MIN_VALUE;
            }
            if (((RecyclerView.p)n).isItemRemoved() || ((RecyclerView.p)n).isItemChanged()) {
                this.d -= this.f.c.e(view);
            }
            this.b = Integer.MIN_VALUE;
        }
        
        public void u(final View element) {
            final c n = this.n(element);
            n.a = this;
            this.a.add(0, element);
            this.b = Integer.MIN_VALUE;
            if (this.a.size() == 1) {
                this.c = Integer.MIN_VALUE;
            }
            if (((RecyclerView.p)n).isItemRemoved() || ((RecyclerView.p)n).isItemChanged()) {
                this.d += this.f.c.e(element);
            }
        }
        
        public void v(final int n) {
            this.b = n;
            this.c = n;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import java.util.List;

public class h
{
    public final a a;
    
    public h(final a a) {
        this.a = a;
    }
    
    public final int a(final List list) {
        int i = list.size() - 1;
        int n = 0;
        while (i >= 0) {
            int n2;
            if (((androidx.recyclerview.widget.a.b)list.get(i)).a == 8) {
                if ((n2 = n) != 0) {
                    return i;
                }
            }
            else {
                n2 = 1;
            }
            --i;
            n = n2;
        }
        return -1;
    }
    
    public void b(final List list) {
        while (true) {
            final int a = this.a(list);
            if (a == -1) {
                break;
            }
            this.d(list, a, a + 1);
        }
    }
    
    public final void c(final List list, final int n, final androidx.recyclerview.widget.a.b b, final int n2, final androidx.recyclerview.widget.a.b b2) {
        final int d = b.d;
        final int b3 = b2.b;
        int n3;
        if (d < b3) {
            n3 = -1;
        }
        else {
            n3 = 0;
        }
        final int b4 = b.b;
        int n4 = n3;
        if (b4 < b3) {
            n4 = n3 + 1;
        }
        if (b3 <= b4) {
            b.b = b4 + b2.d;
        }
        final int b5 = b2.b;
        if (b5 <= d) {
            b.d = d + b2.d;
        }
        b2.b = b5 + n4;
        list.set(n, b2);
        list.set(n2, b);
    }
    
    public final void d(final List list, final int n, final int n2) {
        final androidx.recyclerview.widget.a.b b = list.get(n);
        final androidx.recyclerview.widget.a.b b2 = list.get(n2);
        final int a = b2.a;
        if (a != 1) {
            if (a != 2) {
                if (a == 4) {
                    this.f(list, n, b, n2, b2);
                }
            }
            else {
                this.e(list, n, b, n2, b2);
            }
        }
        else {
            this.c(list, n, b, n2, b2);
        }
    }
    
    public void e(final List list, final int n, final androidx.recyclerview.widget.a.b b, final int n2, final androidx.recyclerview.widget.a.b b2) {
        final int b3 = b.b;
        final int d = b.d;
        boolean b4 = false;
        final int b5 = b2.b;
        boolean b6;
        if (b3 < d) {
            if (b5 == b3 && b2.d == d - b3) {
                b6 = false;
                b4 = true;
            }
            else {
                b6 = false;
            }
        }
        else if (b5 == d + 1 && b2.d == b3 - d) {
            b6 = true;
            b4 = true;
        }
        else {
            b6 = true;
        }
        final int b7 = b2.b;
        if (d < b7) {
            b2.b = b7 - 1;
        }
        else {
            final int d2 = b2.d;
            if (d < b7 + d2) {
                b2.d = d2 - 1;
                b.a = 2;
                b.d = 1;
                if (b2.d == 0) {
                    list.remove(n2);
                    this.a.b(b2);
                }
                return;
            }
        }
        final int b8 = b.b;
        final int b9 = b2.b;
        androidx.recyclerview.widget.a.b a = null;
        if (b8 <= b9) {
            b2.b = b9 + 1;
        }
        else {
            final int d3 = b2.d;
            if (b8 < b9 + d3) {
                a = this.a.a(2, b8 + 1, b9 + d3 - b8, null);
                b2.d = b.b - b2.b;
            }
        }
        if (b4) {
            list.set(n, b2);
            list.remove(n2);
            this.a.b(b);
            return;
        }
        Label_0546: {
            int n3;
            if (b6) {
                if (a != null) {
                    final int b10 = b.b;
                    if (b10 > a.b) {
                        b.b = b10 - a.d;
                    }
                    final int d4 = b.d;
                    if (d4 > a.b) {
                        b.d = d4 - a.d;
                    }
                }
                final int b11 = b.b;
                if (b11 > b2.b) {
                    b.b = b11 - b2.d;
                }
                n3 = b.d;
                if (n3 <= b2.b) {
                    break Label_0546;
                }
            }
            else {
                if (a != null) {
                    final int b12 = b.b;
                    if (b12 >= a.b) {
                        b.b = b12 - a.d;
                    }
                    final int d5 = b.d;
                    if (d5 >= a.b) {
                        b.d = d5 - a.d;
                    }
                }
                final int b13 = b.b;
                if (b13 >= b2.b) {
                    b.b = b13 - b2.d;
                }
                n3 = b.d;
                if (n3 < b2.b) {
                    break Label_0546;
                }
            }
            b.d = n3 - b2.d;
        }
        list.set(n, b2);
        if (b.b != b.d) {
            list.set(n2, b);
        }
        else {
            list.remove(n2);
        }
        if (a != null) {
            list.add(n, a);
        }
    }
    
    public void f(final List list, final int n, final androidx.recyclerview.widget.a.b b, final int n2, final androidx.recyclerview.widget.a.b b2) {
        final int d = b.d;
        final int b3 = b2.b;
        androidx.recyclerview.widget.a.b a = null;
        androidx.recyclerview.widget.a.b a2 = null;
        Label_0089: {
            if (d < b3) {
                b2.b = b3 - 1;
            }
            else {
                final int d2 = b2.d;
                if (d < b3 + d2) {
                    b2.d = d2 - 1;
                    a2 = this.a.a(4, b.b, 1, b2.c);
                    break Label_0089;
                }
            }
            a2 = null;
        }
        final int b4 = b.b;
        final int b5 = b2.b;
        if (b4 <= b5) {
            b2.b = b5 + 1;
        }
        else {
            final int d3 = b2.d;
            if (b4 < b5 + d3) {
                final int n3 = b5 + d3 - b4;
                a = this.a.a(4, b4 + 1, n3, b2.c);
                b2.d -= n3;
            }
        }
        list.set(n2, b);
        if (b2.d > 0) {
            list.set(n, b2);
        }
        else {
            list.remove(n);
            this.a.b(b2);
        }
        if (a2 != null) {
            list.add(n, a2);
        }
        if (a != null) {
            list.add(n, a);
        }
    }
    
    public interface a
    {
        androidx.recyclerview.widget.a.b a(final int p0, final int p1, final int p2, final Object p3);
        
        void b(final androidx.recyclerview.widget.a.b p0);
    }
}

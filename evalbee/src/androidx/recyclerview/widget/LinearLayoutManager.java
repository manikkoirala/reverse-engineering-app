// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.accessibility.AccessibilityRecord;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.view.accessibility.AccessibilityEvent;
import android.util.Log;
import java.util.List;
import android.view.View;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.content.Context;

public class LinearLayoutManager extends o implements z.b
{
    static final boolean DEBUG = false;
    public static final int HORIZONTAL = 0;
    public static final int INVALID_OFFSET = Integer.MIN_VALUE;
    private static final float MAX_SCROLL_FACTOR = 0.33333334f;
    private static final String TAG = "LinearLayoutManager";
    public static final int VERTICAL = 1;
    final a mAnchorInfo;
    private int mInitialPrefetchItemCount;
    private boolean mLastStackFromEnd;
    private final b mLayoutChunkResult;
    private c mLayoutState;
    int mOrientation;
    androidx.recyclerview.widget.i mOrientationHelper;
    d mPendingSavedState;
    int mPendingScrollPosition;
    int mPendingScrollPositionOffset;
    private boolean mRecycleChildrenOnDetach;
    private int[] mReusableIntPair;
    private boolean mReverseLayout;
    boolean mShouldReverseLayout;
    private boolean mSmoothScrollbarEnabled;
    private boolean mStackFromEnd;
    
    public LinearLayoutManager(final Context context, final int orientation, final boolean reverseLayout) {
        this.mOrientation = 1;
        this.mReverseLayout = false;
        this.mShouldReverseLayout = false;
        this.mStackFromEnd = false;
        this.mSmoothScrollbarEnabled = true;
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mPendingSavedState = null;
        this.mAnchorInfo = new a();
        this.mLayoutChunkResult = new b();
        this.mInitialPrefetchItemCount = 2;
        this.mReusableIntPair = new int[2];
        this.setOrientation(orientation);
        this.setReverseLayout(reverseLayout);
    }
    
    public LinearLayoutManager(final Context context, final AttributeSet set, final int n, final int n2) {
        this.mOrientation = 1;
        this.mReverseLayout = false;
        this.mShouldReverseLayout = false;
        this.mStackFromEnd = false;
        this.mSmoothScrollbarEnabled = true;
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mPendingSavedState = null;
        this.mAnchorInfo = new a();
        this.mLayoutChunkResult = new b();
        this.mInitialPrefetchItemCount = 2;
        this.mReusableIntPair = new int[2];
        final o.d properties = RecyclerView.o.getProperties(context, set, n, n2);
        this.setOrientation(properties.a);
        this.setReverseLayout(properties.c);
        this.setStackFromEnd(properties.d);
    }
    
    public final void A(final a a) {
        this.z(a.b, a.c);
    }
    
    @Override
    public void assertNotInLayoutOrScroll(final String s) {
        if (this.mPendingSavedState == null) {
            super.assertNotInLayoutOrScroll(s);
        }
    }
    
    public void calculateExtraLayoutSpace(final a0 a0, final int[] array) {
        int extraLayoutSpace = this.getExtraLayoutSpace(a0);
        int n;
        if (this.mLayoutState.f == -1) {
            n = 0;
        }
        else {
            n = extraLayoutSpace;
            extraLayoutSpace = 0;
        }
        array[0] = extraLayoutSpace;
        array[1] = n;
    }
    
    @Override
    public boolean canScrollHorizontally() {
        return this.mOrientation == 0;
    }
    
    @Override
    public boolean canScrollVertically() {
        final int mOrientation = this.mOrientation;
        boolean b = true;
        if (mOrientation != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void collectAdjacentPrefetchPositions(int a, int n, final a0 a2, final o.c c) {
        if (this.mOrientation != 0) {
            a = n;
        }
        if (((RecyclerView.o)this).getChildCount() != 0) {
            if (a != 0) {
                this.ensureLayoutState();
                if (a > 0) {
                    n = 1;
                }
                else {
                    n = -1;
                }
                this.w(n, Math.abs(a), true, a2);
                this.collectPrefetchPositionsForLayoutState(a2, this.mLayoutState, c);
            }
        }
    }
    
    @Override
    public void collectInitialPrefetchPositions(final int n, final o.c c) {
        final d mPendingSavedState = this.mPendingSavedState;
        int n2 = -1;
        boolean c2;
        int n3;
        if (mPendingSavedState != null && mPendingSavedState.b()) {
            final d mPendingSavedState2 = this.mPendingSavedState;
            c2 = mPendingSavedState2.c;
            n3 = mPendingSavedState2.a;
        }
        else {
            this.s();
            final boolean mShouldReverseLayout = this.mShouldReverseLayout;
            final int n4 = n3 = this.mPendingScrollPosition;
            c2 = mShouldReverseLayout;
            if (n4 == -1) {
                if (mShouldReverseLayout) {
                    n3 = n - 1;
                    c2 = mShouldReverseLayout;
                }
                else {
                    n3 = 0;
                    c2 = mShouldReverseLayout;
                }
            }
        }
        if (!c2) {
            n2 = 1;
        }
        for (int n5 = 0; n5 < this.mInitialPrefetchItemCount && n3 >= 0 && n3 < n; n3 += n2, ++n5) {
            c.a(n3, 0);
        }
    }
    
    public void collectPrefetchPositionsForLayoutState(final a0 a0, final c c, final o.c c2) {
        final int d = c.d;
        if (d >= 0 && d < a0.b()) {
            c2.a(d, Math.max(0, c.g));
        }
    }
    
    @Override
    public int computeHorizontalScrollExtent(final a0 a0) {
        return this.computeScrollExtent(a0);
    }
    
    @Override
    public int computeHorizontalScrollOffset(final a0 a0) {
        return this.computeScrollOffset(a0);
    }
    
    @Override
    public int computeHorizontalScrollRange(final a0 a0) {
        return this.computeScrollRange(a0);
    }
    
    public final int computeScrollExtent(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        this.ensureLayoutState();
        return androidx.recyclerview.widget.l.a(a0, this.mOrientationHelper, this.findFirstVisibleChildClosestToStart(this.mSmoothScrollbarEnabled ^ true, true), this.findFirstVisibleChildClosestToEnd(this.mSmoothScrollbarEnabled ^ true, true), this, this.mSmoothScrollbarEnabled);
    }
    
    public final int computeScrollOffset(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        this.ensureLayoutState();
        return androidx.recyclerview.widget.l.b(a0, this.mOrientationHelper, this.findFirstVisibleChildClosestToStart(this.mSmoothScrollbarEnabled ^ true, true), this.findFirstVisibleChildClosestToEnd(this.mSmoothScrollbarEnabled ^ true, true), this, this.mSmoothScrollbarEnabled, this.mShouldReverseLayout);
    }
    
    public final int computeScrollRange(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        this.ensureLayoutState();
        return androidx.recyclerview.widget.l.c(a0, this.mOrientationHelper, this.findFirstVisibleChildClosestToStart(this.mSmoothScrollbarEnabled ^ true, true), this.findFirstVisibleChildClosestToEnd(this.mSmoothScrollbarEnabled ^ true, true), this, this.mSmoothScrollbarEnabled);
    }
    
    @Override
    public PointF computeScrollVectorForPosition(int n) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return null;
        }
        boolean b = false;
        final int position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(0));
        final int n2 = 1;
        if (n < position) {
            b = true;
        }
        n = n2;
        if (b != this.mShouldReverseLayout) {
            n = -1;
        }
        if (this.mOrientation == 0) {
            return new PointF((float)n, 0.0f);
        }
        return new PointF(0.0f, (float)n);
    }
    
    @Override
    public int computeVerticalScrollExtent(final a0 a0) {
        return this.computeScrollExtent(a0);
    }
    
    @Override
    public int computeVerticalScrollOffset(final a0 a0) {
        return this.computeScrollOffset(a0);
    }
    
    @Override
    public int computeVerticalScrollRange(final a0 a0) {
        return this.computeScrollRange(a0);
    }
    
    public int convertFocusDirectionToLayoutDirection(int n) {
        int n2 = -1;
        final int n3 = 1;
        final int n4 = 1;
        if (n != 1) {
            if (n != 2) {
                if (n == 17) {
                    if (this.mOrientation != 0) {
                        n2 = Integer.MIN_VALUE;
                    }
                    return n2;
                }
                if (n == 33) {
                    if (this.mOrientation != 1) {
                        n2 = Integer.MIN_VALUE;
                    }
                    return n2;
                }
                if (n == 66) {
                    if (this.mOrientation == 0) {
                        n = n3;
                    }
                    else {
                        n = Integer.MIN_VALUE;
                    }
                    return n;
                }
                if (n != 130) {
                    return Integer.MIN_VALUE;
                }
                if (this.mOrientation == 1) {
                    n = n4;
                }
                else {
                    n = Integer.MIN_VALUE;
                }
                return n;
            }
            else {
                if (this.mOrientation == 1) {
                    return 1;
                }
                if (this.isLayoutRTL()) {
                    return -1;
                }
                return 1;
            }
        }
        else {
            if (this.mOrientation == 1) {
                return -1;
            }
            if (this.isLayoutRTL()) {
                return 1;
            }
            return -1;
        }
    }
    
    public c createLayoutState() {
        return new c();
    }
    
    public void ensureLayoutState() {
        if (this.mLayoutState == null) {
            this.mLayoutState = this.createLayoutState();
        }
    }
    
    public final View f() {
        return this.findOnePartiallyOrCompletelyInvisibleChild(0, ((RecyclerView.o)this).getChildCount());
    }
    
    public int fill(final v v, final c c, final a0 a0, final boolean b) {
        final int c2 = c.c;
        final int g = c.g;
        if (g != Integer.MIN_VALUE) {
            if (c2 < 0) {
                c.g = g + c2;
            }
            this.p(v, c);
        }
        int n = c.c + c.h;
        final b mLayoutChunkResult = this.mLayoutChunkResult;
        while ((c.m || n > 0) && c.c(a0)) {
            mLayoutChunkResult.a();
            this.layoutChunk(v, a0, c, mLayoutChunkResult);
            if (mLayoutChunkResult.b) {
                break;
            }
            c.b += mLayoutChunkResult.a * c.f;
            int n2 = 0;
            Label_0175: {
                if (mLayoutChunkResult.c && c.l == null) {
                    n2 = n;
                    if (a0.e()) {
                        break Label_0175;
                    }
                }
                final int c3 = c.c;
                final int a2 = mLayoutChunkResult.a;
                c.c = c3 - a2;
                n2 = n - a2;
            }
            final int g2 = c.g;
            if (g2 != Integer.MIN_VALUE) {
                final int g3 = g2 + mLayoutChunkResult.a;
                c.g = g3;
                final int c4 = c.c;
                if (c4 < 0) {
                    c.g = g3 + c4;
                }
                this.p(v, c);
            }
            n = n2;
            if (!b) {
                continue;
            }
            n = n2;
            if (mLayoutChunkResult.d) {
                break;
            }
        }
        return c2 - c.c;
    }
    
    public int findFirstCompletelyVisibleItemPosition() {
        final View oneVisibleChild = this.findOneVisibleChild(0, ((RecyclerView.o)this).getChildCount(), true, false);
        int position;
        if (oneVisibleChild == null) {
            position = -1;
        }
        else {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public View findFirstVisibleChildClosestToEnd(final boolean b, final boolean b2) {
        int n;
        int childCount;
        if (this.mShouldReverseLayout) {
            n = 0;
            childCount = ((RecyclerView.o)this).getChildCount();
        }
        else {
            n = ((RecyclerView.o)this).getChildCount() - 1;
            childCount = -1;
        }
        return this.findOneVisibleChild(n, childCount, b, b2);
    }
    
    public View findFirstVisibleChildClosestToStart(final boolean b, final boolean b2) {
        int n;
        int childCount;
        if (this.mShouldReverseLayout) {
            n = ((RecyclerView.o)this).getChildCount() - 1;
            childCount = -1;
        }
        else {
            n = 0;
            childCount = ((RecyclerView.o)this).getChildCount();
        }
        return this.findOneVisibleChild(n, childCount, b, b2);
    }
    
    public int findFirstVisibleItemPosition() {
        final View oneVisibleChild = this.findOneVisibleChild(0, ((RecyclerView.o)this).getChildCount(), false, true);
        int position;
        if (oneVisibleChild == null) {
            position = -1;
        }
        else {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public int findLastCompletelyVisibleItemPosition() {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int position = -1;
        final View oneVisibleChild = this.findOneVisibleChild(childCount - 1, -1, true, false);
        if (oneVisibleChild != null) {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public int findLastVisibleItemPosition() {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int position = -1;
        final View oneVisibleChild = this.findOneVisibleChild(childCount - 1, -1, false, true);
        if (oneVisibleChild != null) {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public View findOnePartiallyOrCompletelyInvisibleChild(final int n, final int n2) {
        this.ensureLayoutState();
        int n3;
        if (n2 > n) {
            n3 = 1;
        }
        else if (n2 < n) {
            n3 = -1;
        }
        else {
            n3 = 0;
        }
        if (n3 == 0) {
            return ((RecyclerView.o)this).getChildAt(n);
        }
        int n4;
        int n5;
        if (this.mOrientationHelper.g(((RecyclerView.o)this).getChildAt(n)) < this.mOrientationHelper.m()) {
            n4 = 16644;
            n5 = 16388;
        }
        else {
            n4 = 4161;
            n5 = 4097;
        }
        androidx.recyclerview.widget.o o;
        if (this.mOrientation == 0) {
            o = super.mHorizontalBoundCheck;
        }
        else {
            o = super.mVerticalBoundCheck;
        }
        return o.a(n, n2, n4, n5);
    }
    
    public View findOneVisibleChild(final int n, final int n2, final boolean b, final boolean b2) {
        this.ensureLayoutState();
        int n3 = 320;
        int n4;
        if (b) {
            n4 = 24579;
        }
        else {
            n4 = 320;
        }
        if (!b2) {
            n3 = 0;
        }
        androidx.recyclerview.widget.o o;
        if (this.mOrientation == 0) {
            o = super.mHorizontalBoundCheck;
        }
        else {
            o = super.mVerticalBoundCheck;
        }
        return o.a(n, n2, n4, n3);
    }
    
    public View findReferenceChild(final v v, final a0 a0, int i, final int n, final int n2) {
        this.ensureLayoutState();
        final int m = this.mOrientationHelper.m();
        final int j = this.mOrientationHelper.i();
        int n3;
        if (n > i) {
            n3 = 1;
        }
        else {
            n3 = -1;
        }
        View view = null;
        View view2 = null;
        while (i != n) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            final int position = ((RecyclerView.o)this).getPosition(child);
            View view3 = view;
            View view4 = view2;
            if (position >= 0) {
                view3 = view;
                view4 = view2;
                if (position < n2) {
                    if (((p)child.getLayoutParams()).isItemRemoved()) {
                        view3 = view;
                        if ((view4 = view2) == null) {
                            view4 = child;
                            view3 = view;
                        }
                    }
                    else {
                        if (this.mOrientationHelper.g(child) < j && this.mOrientationHelper.d(child) >= m) {
                            return child;
                        }
                        view3 = view;
                        view4 = view2;
                        if (view == null) {
                            view3 = child;
                            view4 = view2;
                        }
                    }
                }
            }
            i += n3;
            view = view3;
            view2 = view4;
        }
        if (view == null) {
            view = view2;
        }
        return view;
    }
    
    @Override
    public View findViewByPosition(final int n) {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        if (childCount == 0) {
            return null;
        }
        final int n2 = n - ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(0));
        if (n2 >= 0 && n2 < childCount) {
            final View child = ((RecyclerView.o)this).getChildAt(n2);
            if (((RecyclerView.o)this).getPosition(child) == n) {
                return child;
            }
        }
        return super.findViewByPosition(n);
    }
    
    public final int fixLayoutEndGap(int n, final v v, final a0 a0, final boolean b) {
        final int n2 = this.mOrientationHelper.i() - n;
        if (n2 > 0) {
            final int n3 = -this.scrollBy(-n2, v, a0);
            if (b) {
                n = this.mOrientationHelper.i() - (n + n3);
                if (n > 0) {
                    this.mOrientationHelper.r(n);
                    return n + n3;
                }
            }
            return n3;
        }
        return 0;
    }
    
    public final int fixLayoutStartGap(int n, final v v, final a0 a0, final boolean b) {
        final int n2 = n - this.mOrientationHelper.m();
        if (n2 > 0) {
            int n4;
            final int n3 = n4 = -this.scrollBy(n2, v, a0);
            if (b) {
                n = n + n3 - this.mOrientationHelper.m();
                n4 = n3;
                if (n > 0) {
                    this.mOrientationHelper.r(-n);
                    n4 = n3 - n;
                }
            }
            return n4;
        }
        return 0;
    }
    
    public final View g(final v v, final a0 a0) {
        return this.findReferenceChild(v, a0, 0, ((RecyclerView.o)this).getChildCount(), a0.b());
    }
    
    @Override
    public p generateDefaultLayoutParams() {
        return new RecyclerView.p(-2, -2);
    }
    
    public final View getChildClosestToEnd() {
        int n;
        if (this.mShouldReverseLayout) {
            n = 0;
        }
        else {
            n = ((RecyclerView.o)this).getChildCount() - 1;
        }
        return ((RecyclerView.o)this).getChildAt(n);
    }
    
    public final View getChildClosestToStart() {
        int n;
        if (this.mShouldReverseLayout) {
            n = ((RecyclerView.o)this).getChildCount() - 1;
        }
        else {
            n = 0;
        }
        return ((RecyclerView.o)this).getChildAt(n);
    }
    
    @Deprecated
    public int getExtraLayoutSpace(final a0 a0) {
        if (a0.d()) {
            return this.mOrientationHelper.n();
        }
        return 0;
    }
    
    public int getInitialPrefetchItemCount() {
        return this.mInitialPrefetchItemCount;
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public boolean getRecycleChildrenOnDetach() {
        return this.mRecycleChildrenOnDetach;
    }
    
    public boolean getReverseLayout() {
        return this.mReverseLayout;
    }
    
    public boolean getStackFromEnd() {
        return this.mStackFromEnd;
    }
    
    public final View h() {
        return this.findOnePartiallyOrCompletelyInvisibleChild(((RecyclerView.o)this).getChildCount() - 1, -1);
    }
    
    public final View i(final v v, final a0 a0) {
        return this.findReferenceChild(v, a0, ((RecyclerView.o)this).getChildCount() - 1, -1, a0.b());
    }
    
    @Override
    public boolean isAutoMeasureEnabled() {
        return true;
    }
    
    public boolean isLayoutRTL() {
        final int layoutDirection = ((RecyclerView.o)this).getLayoutDirection();
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean isSmoothScrollbarEnabled() {
        return this.mSmoothScrollbarEnabled;
    }
    
    public final View j() {
        View view;
        if (this.mShouldReverseLayout) {
            view = this.f();
        }
        else {
            view = this.h();
        }
        return view;
    }
    
    public final View k() {
        View view;
        if (this.mShouldReverseLayout) {
            view = this.h();
        }
        else {
            view = this.f();
        }
        return view;
    }
    
    public final View l(final v v, final a0 a0) {
        View view;
        if (this.mShouldReverseLayout) {
            view = this.g(v, a0);
        }
        else {
            view = this.i(v, a0);
        }
        return view;
    }
    
    public void layoutChunk(final v v, final a0 a0, final c c, final b b) {
        final View d = c.d(v);
        if (d == null) {
            b.b = true;
            return;
        }
        final p p4 = (p)d.getLayoutParams();
        if (c.l == null) {
            if (this.mShouldReverseLayout == (c.f == -1)) {
                ((RecyclerView.o)this).addView(d);
            }
            else {
                ((RecyclerView.o)this).addView(d, 0);
            }
        }
        else if (this.mShouldReverseLayout == (c.f == -1)) {
            ((RecyclerView.o)this).addDisappearingView(d);
        }
        else {
            ((RecyclerView.o)this).addDisappearingView(d, 0);
        }
        ((RecyclerView.o)this).measureChildWithMargins(d, 0, 0);
        b.a = this.mOrientationHelper.e(d);
        int paddingLeft;
        int n2;
        int n4;
        int n5;
        if (this.mOrientation == 1) {
            int n;
            if (this.isLayoutRTL()) {
                n = ((RecyclerView.o)this).getWidth() - ((RecyclerView.o)this).getPaddingRight();
                paddingLeft = n - this.mOrientationHelper.f(d);
            }
            else {
                paddingLeft = ((RecyclerView.o)this).getPaddingLeft();
                n = this.mOrientationHelper.f(d) + paddingLeft;
            }
            final int f = c.f;
            final int b2 = c.b;
            if (f == -1) {
                final int a2 = b.a;
                n2 = b2;
                final int n3 = n;
                n4 = b2 - a2;
                n5 = n3;
            }
            else {
                final int a3 = b.a;
                final int n6 = b2;
                final int n7 = n;
                final int n8 = a3 + b2;
                n4 = n6;
                n5 = n7;
                n2 = n8;
            }
        }
        else {
            final int paddingTop = ((RecyclerView.o)this).getPaddingTop();
            n2 = this.mOrientationHelper.f(d) + paddingTop;
            final int f2 = c.f;
            final int b3 = c.b;
            if (f2 == -1) {
                final int a4 = b.a;
                n5 = b3;
                final int n9 = paddingTop;
                paddingLeft = b3 - a4;
                n4 = n9;
            }
            else {
                n5 = b.a + b3;
                final int n10 = b3;
                n4 = paddingTop;
                paddingLeft = n10;
            }
        }
        ((RecyclerView.o)this).layoutDecoratedWithMargins(d, paddingLeft, n4, n5, n2);
        if (p4.isItemRemoved() || p4.isItemChanged()) {
            b.c = true;
        }
        b.d = d.hasFocusable();
    }
    
    public final View m(final v v, final a0 a0) {
        View view;
        if (this.mShouldReverseLayout) {
            view = this.i(v, a0);
        }
        else {
            view = this.g(v, a0);
        }
        return view;
    }
    
    public final void n(final v v, final a0 a0, final int n, final int n2) {
        if (a0.g() && ((RecyclerView.o)this).getChildCount() != 0 && !a0.e()) {
            if (this.supportsPredictiveItemAnimations()) {
                final List k = v.k();
                final int size = k.size();
                final int position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(0));
                int i = 0;
                int h2;
                int h = h2 = 0;
                while (i < size) {
                    final d0 d0 = k.get(i);
                    if (!d0.isRemoved()) {
                        final int layoutPosition = d0.getLayoutPosition();
                        int n3 = 1;
                        if (layoutPosition < position != this.mShouldReverseLayout) {
                            n3 = -1;
                        }
                        final int e = this.mOrientationHelper.e(d0.itemView);
                        if (n3 == -1) {
                            h += e;
                        }
                        else {
                            h2 += e;
                        }
                    }
                    ++i;
                }
                this.mLayoutState.l = k;
                if (h > 0) {
                    this.z(((RecyclerView.o)this).getPosition(this.getChildClosestToStart()), n);
                    final c mLayoutState = this.mLayoutState;
                    mLayoutState.h = h;
                    mLayoutState.c = 0;
                    mLayoutState.a();
                    this.fill(v, this.mLayoutState, a0, false);
                }
                if (h2 > 0) {
                    this.x(((RecyclerView.o)this).getPosition(this.getChildClosestToEnd()), n2);
                    final c mLayoutState2 = this.mLayoutState;
                    mLayoutState2.h = h2;
                    mLayoutState2.c = 0;
                    mLayoutState2.a();
                    this.fill(v, this.mLayoutState, a0, false);
                }
                this.mLayoutState.l = null;
            }
        }
    }
    
    public final void o() {
        Log.d("LinearLayoutManager", "internal representation of views on the screen");
        for (int i = 0; i < ((RecyclerView.o)this).getChildCount(); ++i) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            final StringBuilder sb = new StringBuilder();
            sb.append("item ");
            sb.append(((RecyclerView.o)this).getPosition(child));
            sb.append(", coord:");
            sb.append(this.mOrientationHelper.g(child));
            Log.d("LinearLayoutManager", sb.toString());
        }
        Log.d("LinearLayoutManager", "==============");
    }
    
    public void onAnchorReady(final v v, final a0 a0, final a a2, final int n) {
    }
    
    @Override
    public void onDetachedFromWindow(final RecyclerView recyclerView, final v v) {
        super.onDetachedFromWindow(recyclerView, v);
        if (this.mRecycleChildrenOnDetach) {
            ((RecyclerView.o)this).removeAndRecycleAllViews(v);
            v.c();
        }
    }
    
    @Override
    public View onFocusSearchFailed(View view, int convertFocusDirectionToLayoutDirection, final v v, final a0 a0) {
        this.s();
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return null;
        }
        convertFocusDirectionToLayoutDirection = this.convertFocusDirectionToLayoutDirection(convertFocusDirectionToLayoutDirection);
        if (convertFocusDirectionToLayoutDirection == Integer.MIN_VALUE) {
            return null;
        }
        this.ensureLayoutState();
        this.w(convertFocusDirectionToLayoutDirection, (int)(this.mOrientationHelper.n() * 0.33333334f), false, a0);
        final c mLayoutState = this.mLayoutState;
        mLayoutState.g = Integer.MIN_VALUE;
        mLayoutState.a = false;
        this.fill(v, mLayoutState, a0, true);
        if (convertFocusDirectionToLayoutDirection == -1) {
            view = this.k();
        }
        else {
            view = this.j();
        }
        View view2;
        if (convertFocusDirectionToLayoutDirection == -1) {
            view2 = this.getChildClosestToStart();
        }
        else {
            view2 = this.getChildClosestToEnd();
        }
        if (!view2.hasFocusable()) {
            return view;
        }
        if (view == null) {
            return null;
        }
        return view2;
    }
    
    @Override
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (((RecyclerView.o)this).getChildCount() > 0) {
            ((AccessibilityRecord)accessibilityEvent).setFromIndex(this.findFirstVisibleItemPosition());
            ((AccessibilityRecord)accessibilityEvent).setToIndex(this.findLastVisibleItemPosition());
        }
    }
    
    @Override
    public void onLayoutChildren(final v v, final a0 a0) {
        final d mPendingSavedState = this.mPendingSavedState;
        int n = -1;
        if ((mPendingSavedState != null || this.mPendingScrollPosition != -1) && a0.b() == 0) {
            ((RecyclerView.o)this).removeAndRecycleAllViews(v);
            return;
        }
        final d mPendingSavedState2 = this.mPendingSavedState;
        if (mPendingSavedState2 != null && mPendingSavedState2.b()) {
            this.mPendingScrollPosition = this.mPendingSavedState.a;
        }
        this.ensureLayoutState();
        this.mLayoutState.a = false;
        this.s();
        final View focusedChild = ((RecyclerView.o)this).getFocusedChild();
        final a mAnchorInfo = this.mAnchorInfo;
        if (mAnchorInfo.e && this.mPendingScrollPosition == -1 && this.mPendingSavedState == null) {
            if (focusedChild != null && (this.mOrientationHelper.g(focusedChild) >= this.mOrientationHelper.i() || this.mOrientationHelper.d(focusedChild) <= this.mOrientationHelper.m())) {
                this.mAnchorInfo.c(focusedChild, ((RecyclerView.o)this).getPosition(focusedChild));
            }
        }
        else {
            mAnchorInfo.e();
            final a mAnchorInfo2 = this.mAnchorInfo;
            mAnchorInfo2.d = (this.mShouldReverseLayout ^ this.mStackFromEnd);
            this.v(v, a0, mAnchorInfo2);
            this.mAnchorInfo.e = true;
        }
        final c mLayoutState = this.mLayoutState;
        int f;
        if (mLayoutState.k >= 0) {
            f = 1;
        }
        else {
            f = -1;
        }
        mLayoutState.f = f;
        final int[] mReusableIntPair = this.mReusableIntPair;
        mReusableIntPair[1] = (mReusableIntPair[0] = 0);
        this.calculateExtraLayoutSpace(a0, mReusableIntPair);
        final int n2 = Math.max(0, this.mReusableIntPair[0]) + this.mOrientationHelper.m();
        final int n3 = Math.max(0, this.mReusableIntPair[1]) + this.mOrientationHelper.j();
        int h = n2;
        int h2 = n3;
        if (a0.e()) {
            final int mPendingScrollPosition = this.mPendingScrollPosition;
            h = n2;
            h2 = n3;
            if (mPendingScrollPosition != -1) {
                h = n2;
                h2 = n3;
                if (this.mPendingScrollPositionOffset != Integer.MIN_VALUE) {
                    final View viewByPosition = this.findViewByPosition(mPendingScrollPosition);
                    h = n2;
                    h2 = n3;
                    if (viewByPosition != null) {
                        int mPendingScrollPositionOffset;
                        int mPendingScrollPositionOffset2;
                        if (this.mShouldReverseLayout) {
                            mPendingScrollPositionOffset = this.mOrientationHelper.i() - this.mOrientationHelper.d(viewByPosition);
                            mPendingScrollPositionOffset2 = this.mPendingScrollPositionOffset;
                        }
                        else {
                            mPendingScrollPositionOffset2 = this.mOrientationHelper.g(viewByPosition) - this.mOrientationHelper.m();
                            mPendingScrollPositionOffset = this.mPendingScrollPositionOffset;
                        }
                        final int n4 = mPendingScrollPositionOffset - mPendingScrollPositionOffset2;
                        if (n4 > 0) {
                            h = n2 + n4;
                            h2 = n3;
                        }
                        else {
                            h2 = n3 - n4;
                            h = n2;
                        }
                    }
                }
            }
        }
        final a mAnchorInfo3 = this.mAnchorInfo;
        Label_0502: {
            if (mAnchorInfo3.d) {
                if (!this.mShouldReverseLayout) {
                    break Label_0502;
                }
            }
            else if (this.mShouldReverseLayout) {
                break Label_0502;
            }
            n = 1;
        }
        this.onAnchorReady(v, a0, mAnchorInfo3, n);
        ((RecyclerView.o)this).detachAndScrapAttachedViews(v);
        this.mLayoutState.m = this.resolveIsInfinite();
        this.mLayoutState.j = a0.e();
        this.mLayoutState.i = 0;
        final a mAnchorInfo4 = this.mAnchorInfo;
        int b3;
        int b4;
        if (mAnchorInfo4.d) {
            this.A(mAnchorInfo4);
            final c mLayoutState2 = this.mLayoutState;
            mLayoutState2.h = h;
            this.fill(v, mLayoutState2, a0, false);
            final c mLayoutState3 = this.mLayoutState;
            final int b = mLayoutState3.b;
            final int d = mLayoutState3.d;
            final int c = mLayoutState3.c;
            int h3 = h2;
            if (c > 0) {
                h3 = h2 + c;
            }
            this.y(this.mAnchorInfo);
            final c mLayoutState4 = this.mLayoutState;
            mLayoutState4.h = h3;
            mLayoutState4.d += mLayoutState4.e;
            this.fill(v, mLayoutState4, a0, false);
            final c mLayoutState5 = this.mLayoutState;
            final int b2 = mLayoutState5.b;
            final int c2 = mLayoutState5.c;
            b3 = b;
            b4 = b2;
            if (c2 > 0) {
                this.z(d, b);
                final c mLayoutState6 = this.mLayoutState;
                mLayoutState6.h = c2;
                this.fill(v, mLayoutState6, a0, false);
                b3 = this.mLayoutState.b;
                b4 = b2;
            }
        }
        else {
            this.y(mAnchorInfo4);
            final c mLayoutState7 = this.mLayoutState;
            mLayoutState7.h = h2;
            this.fill(v, mLayoutState7, a0, false);
            final c mLayoutState8 = this.mLayoutState;
            final int b5 = mLayoutState8.b;
            final int d2 = mLayoutState8.d;
            final int c3 = mLayoutState8.c;
            int h4 = h;
            if (c3 > 0) {
                h4 = h + c3;
            }
            this.A(this.mAnchorInfo);
            final c mLayoutState9 = this.mLayoutState;
            mLayoutState9.h = h4;
            mLayoutState9.d += mLayoutState9.e;
            this.fill(v, mLayoutState9, a0, false);
            final c mLayoutState10 = this.mLayoutState;
            final int b6 = mLayoutState10.b;
            final int c4 = mLayoutState10.c;
            b3 = b6;
            b4 = b5;
            if (c4 > 0) {
                this.x(d2, b5);
                final c mLayoutState11 = this.mLayoutState;
                mLayoutState11.h = c4;
                this.fill(v, mLayoutState11, a0, false);
                b4 = this.mLayoutState.b;
                b3 = b6;
            }
        }
        int n5 = b3;
        int n6 = b4;
        if (((RecyclerView.o)this).getChildCount() > 0) {
            int n7;
            int n8;
            int n9;
            if (this.mShouldReverseLayout ^ this.mStackFromEnd) {
                final int fixLayoutEndGap = this.fixLayoutEndGap(b4, v, a0, true);
                n7 = b3 + fixLayoutEndGap;
                n8 = b4 + fixLayoutEndGap;
                n9 = this.fixLayoutStartGap(n7, v, a0, false);
            }
            else {
                final int fixLayoutStartGap = this.fixLayoutStartGap(b3, v, a0, true);
                n7 = b3 + fixLayoutStartGap;
                n8 = b4 + fixLayoutStartGap;
                n9 = this.fixLayoutEndGap(n8, v, a0, false);
            }
            n5 = n7 + n9;
            n6 = n8 + n9;
        }
        this.n(v, a0, n5, n6);
        if (!a0.e()) {
            this.mOrientationHelper.s();
        }
        else {
            this.mAnchorInfo.e();
        }
        this.mLastStackFromEnd = this.mStackFromEnd;
    }
    
    @Override
    public void onLayoutCompleted(final a0 a0) {
        super.onLayoutCompleted(a0);
        this.mPendingSavedState = null;
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mAnchorInfo.e();
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable instanceof d) {
            this.mPendingSavedState = (d)parcelable;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        if (this.mPendingSavedState != null) {
            return (Parcelable)new d(this.mPendingSavedState);
        }
        final d d = new d();
        if (((RecyclerView.o)this).getChildCount() > 0) {
            this.ensureLayoutState();
            final boolean c = this.mLastStackFromEnd ^ this.mShouldReverseLayout;
            d.c = c;
            if (c) {
                final View childClosestToEnd = this.getChildClosestToEnd();
                d.b = this.mOrientationHelper.i() - this.mOrientationHelper.d(childClosestToEnd);
                d.a = ((RecyclerView.o)this).getPosition(childClosestToEnd);
            }
            else {
                final View childClosestToStart = this.getChildClosestToStart();
                d.a = ((RecyclerView.o)this).getPosition(childClosestToStart);
                d.b = this.mOrientationHelper.g(childClosestToStart) - this.mOrientationHelper.m();
            }
        }
        else {
            d.c();
        }
        return (Parcelable)d;
    }
    
    public final void p(final v v, final c c) {
        if (c.a) {
            if (!c.m) {
                final int g = c.g;
                final int i = c.i;
                if (c.f == -1) {
                    this.q(v, g, i);
                }
                else {
                    this.r(v, g, i);
                }
            }
        }
    }
    
    public void prepareForDrop(final View view, final View view2, int n, int position) {
        this.assertNotInLayoutOrScroll("Cannot drop a view during a scroll or layout calculation");
        this.ensureLayoutState();
        this.s();
        n = ((RecyclerView.o)this).getPosition(view);
        position = ((RecyclerView.o)this).getPosition(view2);
        if (n < position) {
            n = 1;
        }
        else {
            n = -1;
        }
        if (this.mShouldReverseLayout) {
            if (n == 1) {
                this.scrollToPositionWithOffset(position, this.mOrientationHelper.i() - (this.mOrientationHelper.g(view2) + this.mOrientationHelper.e(view)));
                return;
            }
            n = this.mOrientationHelper.i() - this.mOrientationHelper.d(view2);
        }
        else {
            if (n != -1) {
                this.scrollToPositionWithOffset(position, this.mOrientationHelper.d(view2) - this.mOrientationHelper.e(view));
                return;
            }
            n = this.mOrientationHelper.g(view2);
        }
        this.scrollToPositionWithOffset(position, n);
    }
    
    public final void q(final v v, int i, int n) {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        if (i < 0) {
            return;
        }
        final int n2 = this.mOrientationHelper.h() - i + n;
        if (this.mShouldReverseLayout) {
            View child;
            for (i = 0; i < childCount; ++i) {
                child = ((RecyclerView.o)this).getChildAt(i);
                if (this.mOrientationHelper.g(child) < n2 || this.mOrientationHelper.q(child) < n2) {
                    this.recycleChildren(v, 0, i);
                    return;
                }
            }
        }
        else {
            View child2;
            for (n = (i = childCount - 1); i >= 0; --i) {
                child2 = ((RecyclerView.o)this).getChildAt(i);
                if (this.mOrientationHelper.g(child2) < n2 || this.mOrientationHelper.q(child2) < n2) {
                    this.recycleChildren(v, n, i);
                    break;
                }
            }
        }
    }
    
    public final void r(final v v, int i, int childCount) {
        if (i < 0) {
            return;
        }
        final int n = i - childCount;
        childCount = ((RecyclerView.o)this).getChildCount();
        if (this.mShouldReverseLayout) {
            View child;
            for (i = --childCount; i >= 0; --i) {
                child = ((RecyclerView.o)this).getChildAt(i);
                if (this.mOrientationHelper.d(child) > n || this.mOrientationHelper.p(child) > n) {
                    this.recycleChildren(v, childCount, i);
                    return;
                }
            }
        }
        else {
            View child2;
            for (i = 0; i < childCount; ++i) {
                child2 = ((RecyclerView.o)this).getChildAt(i);
                if (this.mOrientationHelper.d(child2) > n || this.mOrientationHelper.p(child2) > n) {
                    this.recycleChildren(v, 0, i);
                    break;
                }
            }
        }
    }
    
    public final void recycleChildren(final v v, final int n, int i) {
        if (n == i) {
            return;
        }
        int j;
        if (i > (j = n)) {
            --i;
            while (i >= n) {
                ((RecyclerView.o)this).removeAndRecycleViewAt(i, v);
                --i;
            }
        }
        else {
            while (j > i) {
                ((RecyclerView.o)this).removeAndRecycleViewAt(j, v);
                --j;
            }
        }
    }
    
    public boolean resolveIsInfinite() {
        return this.mOrientationHelper.k() == 0 && this.mOrientationHelper.h() == 0;
    }
    
    public final void s() {
        boolean mReverseLayout;
        if (this.mOrientation != 1 && this.isLayoutRTL()) {
            mReverseLayout = (this.mReverseLayout ^ true);
        }
        else {
            mReverseLayout = this.mReverseLayout;
        }
        this.mShouldReverseLayout = mReverseLayout;
    }
    
    public int scrollBy(int n, final v v, final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0 || n == 0) {
            return 0;
        }
        this.ensureLayoutState();
        this.mLayoutState.a = true;
        int n2;
        if (n > 0) {
            n2 = 1;
        }
        else {
            n2 = -1;
        }
        final int abs = Math.abs(n);
        this.w(n2, abs, true, a0);
        final c mLayoutState = this.mLayoutState;
        final int n3 = mLayoutState.g + this.fill(v, mLayoutState, a0, false);
        if (n3 < 0) {
            return 0;
        }
        if (abs > n3) {
            n = n2 * n3;
        }
        this.mOrientationHelper.r(-n);
        return this.mLayoutState.k = n;
    }
    
    @Override
    public int scrollHorizontallyBy(final int n, final v v, final a0 a0) {
        if (this.mOrientation == 1) {
            return 0;
        }
        return this.scrollBy(n, v, a0);
    }
    
    @Override
    public void scrollToPosition(final int mPendingScrollPosition) {
        this.mPendingScrollPosition = mPendingScrollPosition;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        final d mPendingSavedState = this.mPendingSavedState;
        if (mPendingSavedState != null) {
            mPendingSavedState.c();
        }
        ((RecyclerView.o)this).requestLayout();
    }
    
    public void scrollToPositionWithOffset(final int mPendingScrollPosition, final int mPendingScrollPositionOffset) {
        this.mPendingScrollPosition = mPendingScrollPosition;
        this.mPendingScrollPositionOffset = mPendingScrollPositionOffset;
        final d mPendingSavedState = this.mPendingSavedState;
        if (mPendingSavedState != null) {
            mPendingSavedState.c();
        }
        ((RecyclerView.o)this).requestLayout();
    }
    
    @Override
    public int scrollVerticallyBy(final int n, final v v, final a0 a0) {
        if (this.mOrientation == 0) {
            return 0;
        }
        return this.scrollBy(n, v, a0);
    }
    
    public void setInitialPrefetchItemCount(final int mInitialPrefetchItemCount) {
        this.mInitialPrefetchItemCount = mInitialPrefetchItemCount;
    }
    
    public void setOrientation(final int n) {
        if (n != 0 && n != 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("invalid orientation:");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        this.assertNotInLayoutOrScroll(null);
        if (n != this.mOrientation || this.mOrientationHelper == null) {
            final androidx.recyclerview.widget.i b = androidx.recyclerview.widget.i.b(this, n);
            this.mOrientationHelper = b;
            this.mAnchorInfo.a = b;
            this.mOrientation = n;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    public void setRecycleChildrenOnDetach(final boolean mRecycleChildrenOnDetach) {
        this.mRecycleChildrenOnDetach = mRecycleChildrenOnDetach;
    }
    
    public void setReverseLayout(final boolean mReverseLayout) {
        this.assertNotInLayoutOrScroll(null);
        if (mReverseLayout == this.mReverseLayout) {
            return;
        }
        this.mReverseLayout = mReverseLayout;
        ((RecyclerView.o)this).requestLayout();
    }
    
    public void setSmoothScrollbarEnabled(final boolean mSmoothScrollbarEnabled) {
        this.mSmoothScrollbarEnabled = mSmoothScrollbarEnabled;
    }
    
    public void setStackFromEnd(final boolean mStackFromEnd) {
        this.assertNotInLayoutOrScroll(null);
        if (this.mStackFromEnd == mStackFromEnd) {
            return;
        }
        this.mStackFromEnd = mStackFromEnd;
        ((RecyclerView.o)this).requestLayout();
    }
    
    @Override
    public boolean shouldMeasureTwice() {
        return ((RecyclerView.o)this).getHeightMode() != 1073741824 && ((RecyclerView.o)this).getWidthMode() != 1073741824 && ((RecyclerView.o)this).hasFlexibleChildInBothOrientations();
    }
    
    @Override
    public void smoothScrollToPosition(final RecyclerView recyclerView, final a0 a0, final int targetPosition) {
        final androidx.recyclerview.widget.g g = new androidx.recyclerview.widget.g(((View)recyclerView).getContext());
        ((RecyclerView.z)g).setTargetPosition(targetPosition);
        ((RecyclerView.o)this).startSmoothScroll(g);
    }
    
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return this.mPendingSavedState == null && this.mLastStackFromEnd == this.mStackFromEnd;
    }
    
    public final boolean t(final v v, final a0 a0, final a a2) {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        boolean b = false;
        if (childCount == 0) {
            return false;
        }
        final View focusedChild = ((RecyclerView.o)this).getFocusedChild();
        if (focusedChild != null && a2.d(focusedChild, a0)) {
            a2.c(focusedChild, ((RecyclerView.o)this).getPosition(focusedChild));
            return true;
        }
        if (this.mLastStackFromEnd != this.mStackFromEnd) {
            return false;
        }
        View view;
        if (a2.d) {
            view = this.l(v, a0);
        }
        else {
            view = this.m(v, a0);
        }
        if (view != null) {
            a2.b(view, ((RecyclerView.o)this).getPosition(view));
            if (!a0.e() && this.supportsPredictiveItemAnimations()) {
                if (this.mOrientationHelper.g(view) >= this.mOrientationHelper.i() || this.mOrientationHelper.d(view) < this.mOrientationHelper.m()) {
                    b = true;
                }
                if (b) {
                    int c;
                    if (a2.d) {
                        c = this.mOrientationHelper.i();
                    }
                    else {
                        c = this.mOrientationHelper.m();
                    }
                    a2.c = c;
                }
            }
            return true;
        }
        return false;
    }
    
    public final boolean u(final a0 a0, final a a2) {
        final boolean e = a0.e();
        boolean d = false;
        if (!e) {
            final int mPendingScrollPosition = this.mPendingScrollPosition;
            if (mPendingScrollPosition != -1) {
                if (mPendingScrollPosition >= 0 && mPendingScrollPosition < a0.b()) {
                    a2.b = this.mPendingScrollPosition;
                    final d mPendingSavedState = this.mPendingSavedState;
                    if (mPendingSavedState != null && mPendingSavedState.b()) {
                        final boolean c = this.mPendingSavedState.c;
                        a2.d = c;
                        int c2;
                        if (c) {
                            c2 = this.mOrientationHelper.i() - this.mPendingSavedState.b;
                        }
                        else {
                            c2 = this.mOrientationHelper.m() + this.mPendingSavedState.b;
                        }
                        a2.c = c2;
                        return true;
                    }
                    if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
                        final View viewByPosition = this.findViewByPosition(this.mPendingScrollPosition);
                        if (viewByPosition != null) {
                            if (this.mOrientationHelper.e(viewByPosition) > this.mOrientationHelper.n()) {
                                a2.a();
                                return true;
                            }
                            if (this.mOrientationHelper.g(viewByPosition) - this.mOrientationHelper.m() < 0) {
                                a2.c = this.mOrientationHelper.m();
                                a2.d = false;
                                return true;
                            }
                            if (this.mOrientationHelper.i() - this.mOrientationHelper.d(viewByPosition) < 0) {
                                a2.c = this.mOrientationHelper.i();
                                return a2.d = true;
                            }
                            int g;
                            if (a2.d) {
                                g = this.mOrientationHelper.d(viewByPosition) + this.mOrientationHelper.o();
                            }
                            else {
                                g = this.mOrientationHelper.g(viewByPosition);
                            }
                            a2.c = g;
                        }
                        else {
                            if (((RecyclerView.o)this).getChildCount() > 0) {
                                if (this.mPendingScrollPosition < ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(0)) == this.mShouldReverseLayout) {
                                    d = true;
                                }
                                a2.d = d;
                            }
                            a2.a();
                        }
                        return true;
                    }
                    final boolean mShouldReverseLayout = this.mShouldReverseLayout;
                    a2.d = mShouldReverseLayout;
                    int c3;
                    if (mShouldReverseLayout) {
                        c3 = this.mOrientationHelper.i() - this.mPendingScrollPositionOffset;
                    }
                    else {
                        c3 = this.mOrientationHelper.m() + this.mPendingScrollPositionOffset;
                    }
                    a2.c = c3;
                    return true;
                }
                else {
                    this.mPendingScrollPosition = -1;
                    this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
                }
            }
        }
        return false;
    }
    
    public final void v(final v v, final a0 a0, final a a2) {
        if (this.u(a0, a2)) {
            return;
        }
        if (this.t(v, a0, a2)) {
            return;
        }
        a2.a();
        int b;
        if (this.mStackFromEnd) {
            b = a0.b() - 1;
        }
        else {
            b = 0;
        }
        a2.b = b;
    }
    
    public void validateChildOrder() {
        final StringBuilder sb = new StringBuilder();
        sb.append("validating child count ");
        sb.append(((RecyclerView.o)this).getChildCount());
        Log.d("LinearLayoutManager", sb.toString());
        final int childCount = ((RecyclerView.o)this).getChildCount();
        boolean b = true;
        final boolean b2 = true;
        if (childCount < 1) {
            return;
        }
        final int position = ((RecyclerView.o)this).getPosition(((RecyclerView.o)this).getChildAt(0));
        final int g = this.mOrientationHelper.g(((RecyclerView.o)this).getChildAt(0));
        if (this.mShouldReverseLayout) {
            for (int i = 1; i < ((RecyclerView.o)this).getChildCount(); ++i) {
                final View child = ((RecyclerView.o)this).getChildAt(i);
                final int position2 = ((RecyclerView.o)this).getPosition(child);
                final int g2 = this.mOrientationHelper.g(child);
                if (position2 < position) {
                    this.o();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("detected invalid position. loc invalid? ");
                    sb2.append(g2 < g && b2);
                    throw new RuntimeException(sb2.toString());
                }
                if (g2 > g) {
                    this.o();
                    throw new RuntimeException("detected invalid location");
                }
            }
        }
        else {
            for (int j = 1; j < ((RecyclerView.o)this).getChildCount(); ++j) {
                final View child2 = ((RecyclerView.o)this).getChildAt(j);
                final int position3 = ((RecyclerView.o)this).getPosition(child2);
                final int g3 = this.mOrientationHelper.g(child2);
                if (position3 < position) {
                    this.o();
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("detected invalid position. loc invalid? ");
                    if (g3 >= g) {
                        b = false;
                    }
                    sb3.append(b);
                    throw new RuntimeException(sb3.toString());
                }
                if (g3 < g) {
                    this.o();
                    throw new RuntimeException("detected invalid location");
                }
            }
        }
    }
    
    public final void w(int g, final int c, final boolean b, final a0 a0) {
        this.mLayoutState.m = this.resolveIsInfinite();
        this.mLayoutState.f = g;
        final int[] mReusableIntPair = this.mReusableIntPair;
        boolean b2 = false;
        mReusableIntPair[0] = 0;
        final int n = 1;
        final int n2 = 1;
        mReusableIntPair[1] = 0;
        this.calculateExtraLayoutSpace(a0, mReusableIntPair);
        int max = Math.max(0, this.mReusableIntPair[0]);
        final int max2 = Math.max(0, this.mReusableIntPair[1]);
        if (g == 1) {
            b2 = true;
        }
        final c mLayoutState = this.mLayoutState;
        if (b2) {
            g = max2;
        }
        else {
            g = max;
        }
        mLayoutState.h = g;
        if (!b2) {
            max = max2;
        }
        mLayoutState.i = max;
        if (b2) {
            mLayoutState.h = g + this.mOrientationHelper.j();
            final View childClosestToEnd = this.getChildClosestToEnd();
            final c mLayoutState2 = this.mLayoutState;
            g = n2;
            if (this.mShouldReverseLayout) {
                g = -1;
            }
            mLayoutState2.e = g;
            g = ((RecyclerView.o)this).getPosition(childClosestToEnd);
            final c mLayoutState3 = this.mLayoutState;
            mLayoutState2.d = g + mLayoutState3.e;
            mLayoutState3.b = this.mOrientationHelper.d(childClosestToEnd);
            g = this.mOrientationHelper.d(childClosestToEnd) - this.mOrientationHelper.i();
        }
        else {
            final View childClosestToStart = this.getChildClosestToStart();
            final c mLayoutState4 = this.mLayoutState;
            mLayoutState4.h += this.mOrientationHelper.m();
            final c mLayoutState5 = this.mLayoutState;
            if (this.mShouldReverseLayout) {
                g = n;
            }
            else {
                g = -1;
            }
            mLayoutState5.e = g;
            g = ((RecyclerView.o)this).getPosition(childClosestToStart);
            final c mLayoutState6 = this.mLayoutState;
            mLayoutState5.d = g + mLayoutState6.e;
            mLayoutState6.b = this.mOrientationHelper.g(childClosestToStart);
            g = -this.mOrientationHelper.g(childClosestToStart) + this.mOrientationHelper.m();
        }
        final c mLayoutState7 = this.mLayoutState;
        mLayoutState7.c = c;
        if (b) {
            mLayoutState7.c = c - g;
        }
        mLayoutState7.g = g;
    }
    
    public final void x(final int d, final int b) {
        this.mLayoutState.c = this.mOrientationHelper.i() - b;
        final c mLayoutState = this.mLayoutState;
        int e;
        if (this.mShouldReverseLayout) {
            e = -1;
        }
        else {
            e = 1;
        }
        mLayoutState.e = e;
        mLayoutState.d = d;
        mLayoutState.f = 1;
        mLayoutState.b = b;
        mLayoutState.g = Integer.MIN_VALUE;
    }
    
    public final void y(final a a) {
        this.x(a.b, a.c);
    }
    
    public final void z(int n, final int b) {
        this.mLayoutState.c = b - this.mOrientationHelper.m();
        final c mLayoutState = this.mLayoutState;
        mLayoutState.d = n;
        if (this.mShouldReverseLayout) {
            n = 1;
        }
        else {
            n = -1;
        }
        mLayoutState.e = n;
        mLayoutState.f = -1;
        mLayoutState.b = b;
        mLayoutState.g = Integer.MIN_VALUE;
    }
    
    public static class a
    {
        public androidx.recyclerview.widget.i a;
        public int b;
        public int c;
        public boolean d;
        public boolean e;
        
        public a() {
            this.e();
        }
        
        public void a() {
            int c;
            if (this.d) {
                c = this.a.i();
            }
            else {
                c = this.a.m();
            }
            this.c = c;
        }
        
        public void b(final View view, final int b) {
            int g;
            if (this.d) {
                g = this.a.d(view) + this.a.o();
            }
            else {
                g = this.a.g(view);
            }
            this.c = g;
            this.b = b;
        }
        
        public void c(final View view, int a) {
            final int o = this.a.o();
            if (o >= 0) {
                this.b(view, a);
                return;
            }
            this.b = a;
            if (this.d) {
                a = this.a.i() - o - this.a.d(view);
                this.c = this.a.i() - a;
                if (a > 0) {
                    final int e = this.a.e(view);
                    final int c = this.c;
                    final int m = this.a.m();
                    final int n = c - e - (m + Math.min(this.a.g(view) - m, 0));
                    if (n < 0) {
                        this.c += Math.min(a, -n);
                    }
                }
            }
            else {
                final int g = this.a.g(view);
                a = g - this.a.m();
                this.c = g;
                if (a > 0) {
                    final int n2 = this.a.i() - Math.min(0, this.a.i() - o - this.a.d(view)) - (g + this.a.e(view));
                    if (n2 < 0) {
                        this.c -= Math.min(a, -n2);
                    }
                }
            }
        }
        
        public boolean d(final View view, final a0 a0) {
            final p p2 = (p)view.getLayoutParams();
            return !p2.isItemRemoved() && p2.getViewLayoutPosition() >= 0 && p2.getViewLayoutPosition() < a0.b();
        }
        
        public void e() {
            this.b = -1;
            this.c = Integer.MIN_VALUE;
            this.d = false;
            this.e = false;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("AnchorInfo{mPosition=");
            sb.append(this.b);
            sb.append(", mCoordinate=");
            sb.append(this.c);
            sb.append(", mLayoutFromEnd=");
            sb.append(this.d);
            sb.append(", mValid=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static class b
    {
        public int a;
        public boolean b;
        public boolean c;
        public boolean d;
        
        public void a() {
            this.a = 0;
            this.b = false;
            this.c = false;
            this.d = false;
        }
    }
    
    public static class c
    {
        public boolean a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public int i;
        public boolean j;
        public int k;
        public List l;
        public boolean m;
        
        public c() {
            this.a = true;
            this.h = 0;
            this.i = 0;
            this.j = false;
            this.l = null;
        }
        
        public void a() {
            this.b(null);
        }
        
        public void b(View f) {
            f = this.f(f);
            int viewLayoutPosition;
            if (f == null) {
                viewLayoutPosition = -1;
            }
            else {
                viewLayoutPosition = ((p)f.getLayoutParams()).getViewLayoutPosition();
            }
            this.d = viewLayoutPosition;
        }
        
        public boolean c(final a0 a0) {
            final int d = this.d;
            return d >= 0 && d < a0.b();
        }
        
        public View d(final v v) {
            if (this.l != null) {
                return this.e();
            }
            final View o = v.o(this.d);
            this.d += this.e;
            return o;
        }
        
        public final View e() {
            for (int size = this.l.size(), i = 0; i < size; ++i) {
                final View itemView = this.l.get(i).itemView;
                final p p = (p)itemView.getLayoutParams();
                if (!p.isItemRemoved()) {
                    if (this.d == p.getViewLayoutPosition()) {
                        this.b(itemView);
                        return itemView;
                    }
                }
            }
            return null;
        }
        
        public View f(final View view) {
            final int size = this.l.size();
            View view2 = null;
            int n = Integer.MAX_VALUE;
            int n2 = 0;
            View view3;
            while (true) {
                view3 = view2;
                if (n2 >= size) {
                    break;
                }
                final View itemView = this.l.get(n2).itemView;
                final p p = (p)itemView.getLayoutParams();
                View view4 = view2;
                int n3 = n;
                if (itemView != view) {
                    if (p.isItemRemoved()) {
                        view4 = view2;
                        n3 = n;
                    }
                    else {
                        final int n4 = (p.getViewLayoutPosition() - this.d) * this.e;
                        if (n4 < 0) {
                            view4 = view2;
                            n3 = n;
                        }
                        else {
                            view4 = view2;
                            if (n4 < (n3 = n)) {
                                final View view5 = itemView;
                                if (n4 == 0) {
                                    view3 = view5;
                                    break;
                                }
                                n3 = n4;
                                view4 = view5;
                            }
                        }
                    }
                }
                ++n2;
                view2 = view4;
                n = n3;
            }
            return view3;
        }
    }
    
    public static class d implements Parcelable
    {
        public static final Parcelable$Creator<d> CREATOR;
        public int a;
        public int b;
        public boolean c;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public d a(final Parcel parcel) {
                    return new d(parcel);
                }
                
                public d[] b(final int n) {
                    return new d[n];
                }
            };
        }
        
        public d() {
        }
        
        public d(final Parcel parcel) {
            this.a = parcel.readInt();
            this.b = parcel.readInt();
            final int int1 = parcel.readInt();
            boolean c = true;
            if (int1 != 1) {
                c = false;
            }
            this.c = c;
        }
        
        public d(final d d) {
            this.a = d.a;
            this.b = d.b;
            this.c = d.c;
        }
        
        public boolean b() {
            return this.a >= 0;
        }
        
        public void c() {
            this.a = -1;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
            parcel.writeInt((int)(this.c ? 1 : 0));
        }
    }
}

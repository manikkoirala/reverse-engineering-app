// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

public class p
{
    public final co1 a;
    public final hm0 b;
    
    public p() {
        this.a = new co1();
        this.b = new hm0();
    }
    
    public void a(final RecyclerView.d0 d0, final RecyclerView.l.b b) {
        a b2;
        if ((b2 = (a)this.a.get(d0)) == null) {
            b2 = p.a.b();
            this.a.put(d0, b2);
        }
        b2.a |= 0x2;
        b2.b = b;
    }
    
    public void b(final RecyclerView.d0 d0) {
        a b;
        if ((b = (a)this.a.get(d0)) == null) {
            b = p.a.b();
            this.a.put(d0, b);
        }
        b.a |= 0x1;
    }
    
    public void c(final long n, final RecyclerView.d0 d0) {
        this.b.j(n, d0);
    }
    
    public void d(final RecyclerView.d0 d0, final RecyclerView.l.b c) {
        a b;
        if ((b = (a)this.a.get(d0)) == null) {
            b = p.a.b();
            this.a.put(d0, b);
        }
        b.c = c;
        b.a |= 0x8;
    }
    
    public void e(final RecyclerView.d0 d0, final RecyclerView.l.b b) {
        a b2;
        if ((b2 = (a)this.a.get(d0)) == null) {
            b2 = p.a.b();
            this.a.put(d0, b2);
        }
        b2.b = b;
        b2.a |= 0x4;
    }
    
    public void f() {
        this.a.clear();
        this.b.a();
    }
    
    public RecyclerView.d0 g(final long n) {
        return (RecyclerView.d0)this.b.e(n);
    }
    
    public boolean h(final RecyclerView.d0 d0) {
        final a a = (a)this.a.get(d0);
        if (a != null) {
            final int a2 = a.a;
            final boolean b = true;
            if ((a2 & 0x1) != 0x0) {
                return b;
            }
        }
        return false;
    }
    
    public boolean i(final RecyclerView.d0 d0) {
        final a a = (a)this.a.get(d0);
        return a != null && (a.a & 0x4) != 0x0;
    }
    
    public void j() {
        p.a.a();
    }
    
    public void k(final RecyclerView.d0 d0) {
        this.p(d0);
    }
    
    public final RecyclerView.l.b l(final RecyclerView.d0 d0, final int n) {
        final int f = this.a.f(d0);
        if (f < 0) {
            return null;
        }
        final a a = (a)this.a.m(f);
        if (a != null) {
            final int a2 = a.a;
            if ((a2 & n) != 0x0) {
                final int a3 = ~n & a2;
                a.a = a3;
                RecyclerView.l.b b;
                if (n == 4) {
                    b = a.b;
                }
                else {
                    if (n != 8) {
                        throw new IllegalArgumentException("Must provide flag PRE or POST");
                    }
                    b = a.c;
                }
                if ((a3 & 0xC) == 0x0) {
                    this.a.k(f);
                    p.a.c(a);
                }
                return b;
            }
        }
        return null;
    }
    
    public RecyclerView.l.b m(final RecyclerView.d0 d0) {
        return this.l(d0, 8);
    }
    
    public RecyclerView.l.b n(final RecyclerView.d0 d0) {
        return this.l(d0, 4);
    }
    
    public void o(final b b) {
        for (int i = this.a.size() - 1; i >= 0; --i) {
            final RecyclerView.d0 d0 = (RecyclerView.d0)this.a.i(i);
            final a a = (a)this.a.k(i);
            final int a2 = a.a;
            Label_0196: {
                Label_0053: {
                    if ((a2 & 0x3) != 0x3) {
                        RecyclerView.l.b b2 = null;
                        RecyclerView.l.b c = null;
                        Label_0092: {
                            if ((a2 & 0x1) == 0x0) {
                                if ((a2 & 0xE) != 0xE) {
                                    if ((a2 & 0xC) == 0xC) {
                                        b.d(d0, a.b, a.c);
                                        break Label_0196;
                                    }
                                    if ((a2 & 0x4) != 0x0) {
                                        b2 = a.b;
                                        c = null;
                                        break Label_0092;
                                    }
                                    if ((a2 & 0x8) == 0x0) {
                                        break Label_0196;
                                    }
                                }
                                b.a(d0, a.b, a.c);
                                break Label_0196;
                            }
                            b2 = a.b;
                            if (b2 == null) {
                                break Label_0053;
                            }
                            c = a.c;
                        }
                        b.c(d0, b2, c);
                        break Label_0196;
                    }
                }
                b.b(d0);
            }
            p.a.c(a);
        }
    }
    
    public void p(final RecyclerView.d0 d0) {
        final a a = (a)this.a.get(d0);
        if (a == null) {
            return;
        }
        a.a &= 0xFFFFFFFE;
    }
    
    public void q(final RecyclerView.d0 d0) {
        for (int i = this.b.m() - 1; i >= 0; --i) {
            if (d0 == this.b.n(i)) {
                this.b.l(i);
                break;
            }
        }
        final a a = (a)this.a.remove(d0);
        if (a != null) {
            p.a.c(a);
        }
    }
    
    public static class a
    {
        public static w51 d;
        public int a;
        public RecyclerView.l.b b;
        public RecyclerView.l.b c;
        
        static {
            a.d = new y51(20);
        }
        
        public static void a() {
            while (a.d.a() != null) {}
        }
        
        public static a b() {
            a a;
            if ((a = (a)p.a.d.a()) == null) {
                a = new a();
            }
            return a;
        }
        
        public static void c(final a a) {
            a.a = 0;
            a.b = null;
            a.c = null;
            p.a.d.b(a);
        }
    }
    
    public interface b
    {
        void a(final RecyclerView.d0 p0, final RecyclerView.l.b p1, final RecyclerView.l.b p2);
        
        void b(final RecyclerView.d0 p0);
        
        void c(final RecyclerView.d0 p0, final RecyclerView.l.b p1, final RecyclerView.l.b p2);
        
        void d(final RecyclerView.d0 p0, final RecyclerView.l.b p1, final RecyclerView.l.b p2);
    }
}

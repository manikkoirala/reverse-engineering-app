// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.util.DisplayMetrics;
import android.content.Context;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

public abstract class n extends r
{
    static final float MILLISECONDS_PER_INCH = 100.0f;
    private Scroller mGravityScroller;
    RecyclerView mRecyclerView;
    private final t mScrollListener;
    
    public n() {
        this.mScrollListener = new t(this) {
            public boolean a = false;
            public final n b;
            
            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, final int n) {
                super.onScrollStateChanged(recyclerView, n);
                if (n == 0 && this.a) {
                    this.a = false;
                    this.b.snapToTargetExistingView();
                }
            }
            
            @Override
            public void onScrolled(final RecyclerView recyclerView, final int n, final int n2) {
                if (n != 0 || n2 != 0) {
                    this.a = true;
                }
            }
        };
    }
    
    public final void a() {
        this.mRecyclerView.removeOnScrollListener(this.mScrollListener);
        this.mRecyclerView.setOnFlingListener(null);
    }
    
    public void attachToRecyclerView(final RecyclerView mRecyclerView) {
        final RecyclerView mRecyclerView2 = this.mRecyclerView;
        if (mRecyclerView2 == mRecyclerView) {
            return;
        }
        if (mRecyclerView2 != null) {
            this.a();
        }
        if ((this.mRecyclerView = mRecyclerView) != null) {
            this.b();
            this.mGravityScroller = new Scroller(((View)this.mRecyclerView).getContext(), (Interpolator)new DecelerateInterpolator());
            this.snapToTargetExistingView();
        }
    }
    
    public final void b() {
        if (this.mRecyclerView.getOnFlingListener() == null) {
            this.mRecyclerView.addOnScrollListener(this.mScrollListener);
            this.mRecyclerView.setOnFlingListener((RecyclerView.r)this);
            return;
        }
        throw new IllegalStateException("An instance of OnFlingListener already set.");
    }
    
    public final boolean c(final o o, int targetSnapPosition, final int n) {
        if (!(o instanceof z.b)) {
            return false;
        }
        final z scroller = this.createScroller(o);
        if (scroller == null) {
            return false;
        }
        targetSnapPosition = this.findTargetSnapPosition(o, targetSnapPosition, n);
        if (targetSnapPosition == -1) {
            return false;
        }
        scroller.setTargetPosition(targetSnapPosition);
        o.startSmoothScroll(scroller);
        return true;
    }
    
    public abstract int[] calculateDistanceToFinalSnap(final o p0, final View p1);
    
    public int[] calculateScrollDistance(final int n, final int n2) {
        this.mGravityScroller.fling(0, 0, n, n2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return new int[] { this.mGravityScroller.getFinalX(), this.mGravityScroller.getFinalY() };
    }
    
    public z createScroller(final o o) {
        return this.createSnapScroller(o);
    }
    
    @Deprecated
    public androidx.recyclerview.widget.g createSnapScroller(final o o) {
        if (!(o instanceof z.b)) {
            return null;
        }
        return new androidx.recyclerview.widget.g(this, ((View)this.mRecyclerView).getContext()) {
            public final n a;
            
            @Override
            public float calculateSpeedPerPixel(final DisplayMetrics displayMetrics) {
                return 100.0f / displayMetrics.densityDpi;
            }
            
            @Override
            public void onTargetFound(final View view, final a0 a0, final z.a a2) {
                final n a3 = this.a;
                final RecyclerView mRecyclerView = a3.mRecyclerView;
                if (mRecyclerView == null) {
                    return;
                }
                final int[] calculateDistanceToFinalSnap = a3.calculateDistanceToFinalSnap(mRecyclerView.getLayoutManager(), view);
                final int a4 = calculateDistanceToFinalSnap[0];
                final int a5 = calculateDistanceToFinalSnap[1];
                final int calculateTimeForDeceleration = this.calculateTimeForDeceleration(Math.max(Math.abs(a4), Math.abs(a5)));
                if (calculateTimeForDeceleration > 0) {
                    a2.d(a4, a5, calculateTimeForDeceleration, (Interpolator)super.mDecelerateInterpolator);
                }
            }
        };
    }
    
    public abstract View findSnapView(final o p0);
    
    public abstract int findTargetSnapPosition(final o p0, final int p1, final int p2);
    
    @Override
    public boolean onFling(final int a, final int a2) {
        final RecyclerView.o layoutManager = this.mRecyclerView.getLayoutManager();
        final boolean b = false;
        if (layoutManager == null) {
            return false;
        }
        if (this.mRecyclerView.getAdapter() == null) {
            return false;
        }
        final int minFlingVelocity = this.mRecyclerView.getMinFlingVelocity();
        if (Math.abs(a2) <= minFlingVelocity) {
            final boolean b2 = b;
            if (Math.abs(a) <= minFlingVelocity) {
                return b2;
            }
        }
        boolean b2 = b;
        if (this.c(layoutManager, a, a2)) {
            b2 = true;
        }
        return b2;
    }
    
    public void snapToTargetExistingView() {
        final RecyclerView mRecyclerView = this.mRecyclerView;
        if (mRecyclerView == null) {
            return;
        }
        final RecyclerView.o layoutManager = mRecyclerView.getLayoutManager();
        if (layoutManager == null) {
            return;
        }
        final View snapView = this.findSnapView(layoutManager);
        if (snapView == null) {
            return;
        }
        final int[] calculateDistanceToFinalSnap = this.calculateDistanceToFinalSnap(layoutManager, snapView);
        final int n = calculateDistanceToFinalSnap[0];
        if (n != 0 || calculateDistanceToFinalSnap[1] != 0) {
            this.mRecyclerView.smoothScrollBy(n, calculateDistanceToFinalSnap[1]);
        }
    }
}

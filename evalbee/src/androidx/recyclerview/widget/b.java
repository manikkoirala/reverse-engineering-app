// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.ViewGroup$LayoutParams;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class b
{
    public final b a;
    public final a b;
    public final List c;
    
    public b(final b a) {
        this.a = a;
        this.b = new a();
        this.c = new ArrayList();
    }
    
    public void a(final View view, int n, final boolean b) {
        if (n < 0) {
            n = this.a.c();
        }
        else {
            n = this.h(n);
        }
        this.b.e(n, b);
        if (b) {
            this.l(view);
        }
        this.a.addView(view, n);
    }
    
    public void b(final View view, final boolean b) {
        this.a(view, -1, b);
    }
    
    public void c(final View view, int n, final ViewGroup$LayoutParams viewGroup$LayoutParams, final boolean b) {
        if (n < 0) {
            n = this.a.c();
        }
        else {
            n = this.h(n);
        }
        this.b.e(n, b);
        if (b) {
            this.l(view);
        }
        this.a.e(view, n, viewGroup$LayoutParams);
    }
    
    public void d(int h) {
        h = this.h(h);
        this.b.f(h);
        this.a.f(h);
    }
    
    public View e(final int n) {
        for (int size = this.c.size(), i = 0; i < size; ++i) {
            final View view = this.c.get(i);
            final RecyclerView.d0 d = this.a.d(view);
            if (d.getLayoutPosition() == n && !d.isInvalid() && !d.isRemoved()) {
                return view;
            }
        }
        return null;
    }
    
    public View f(int h) {
        h = this.h(h);
        return this.a.a(h);
    }
    
    public int g() {
        return this.a.c() - this.c.size();
    }
    
    public final int h(final int n) {
        if (n < 0) {
            return -1;
        }
        int n2;
        for (int c = this.a.c(), i = n; i < c; i += n2) {
            n2 = n - (i - this.b.b(i));
            if (n2 == 0) {
                while (this.b.d(i)) {
                    ++i;
                }
                return i;
            }
        }
        return -1;
    }
    
    public View i(final int n) {
        return this.a.a(n);
    }
    
    public int j() {
        return this.a.c();
    }
    
    public void k(final View obj) {
        final int g = this.a.g(obj);
        if (g >= 0) {
            this.b.h(g);
            this.l(obj);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("view is not a child, cannot hide ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void l(final View view) {
        this.c.add(view);
        this.a.b(view);
    }
    
    public int m(final View view) {
        final int g = this.a.g(view);
        if (g == -1) {
            return -1;
        }
        if (this.b.d(g)) {
            return -1;
        }
        return g - this.b.b(g);
    }
    
    public boolean n(final View view) {
        return this.c.contains(view);
    }
    
    public void o() {
        this.b.g();
        for (int i = this.c.size() - 1; i >= 0; --i) {
            this.a.h((View)this.c.get(i));
            this.c.remove(i);
        }
        this.a.removeAllViews();
    }
    
    public void p(final View view) {
        final int g = this.a.g(view);
        if (g < 0) {
            return;
        }
        if (this.b.f(g)) {
            this.t(view);
        }
        this.a.removeViewAt(g);
    }
    
    public void q(int h) {
        h = this.h(h);
        final View a = this.a.a(h);
        if (a == null) {
            return;
        }
        if (this.b.f(h)) {
            this.t(a);
        }
        this.a.removeViewAt(h);
    }
    
    public boolean r(final View view) {
        final int g = this.a.g(view);
        if (g == -1) {
            this.t(view);
            return true;
        }
        if (this.b.d(g)) {
            this.b.f(g);
            this.t(view);
            this.a.removeViewAt(g);
            return true;
        }
        return false;
    }
    
    public void s(final View view) {
        final int g = this.a.g(view);
        if (g < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("view is not a child, cannot hide ");
            sb.append(view);
            throw new IllegalArgumentException(sb.toString());
        }
        if (this.b.d(g)) {
            this.b.a(g);
            this.t(view);
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("trying to unhide a view that was not hidden");
        sb2.append(view);
        throw new RuntimeException(sb2.toString());
    }
    
    public final boolean t(final View view) {
        if (this.c.remove(view)) {
            this.a.h(view);
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.b.toString());
        sb.append(", hidden list:");
        sb.append(this.c.size());
        return sb.toString();
    }
    
    public static class a
    {
        public long a;
        public a b;
        
        public a() {
            this.a = 0L;
        }
        
        public void a(final int n) {
            if (n >= 64) {
                final a b = this.b;
                if (b != null) {
                    b.a(n - 64);
                }
            }
            else {
                this.a &= ~(1L << n);
            }
        }
        
        public int b(final int n) {
            final a b = this.b;
            if (b == null) {
                if (n >= 64) {
                    return Long.bitCount(this.a);
                }
                return Long.bitCount(this.a & (1L << n) - 1L);
            }
            else {
                if (n < 64) {
                    return Long.bitCount(this.a & (1L << n) - 1L);
                }
                return b.b(n - 64) + Long.bitCount(this.a);
            }
        }
        
        public final void c() {
            if (this.b == null) {
                this.b = new a();
            }
        }
        
        public boolean d(final int n) {
            if (n >= 64) {
                this.c();
                return this.b.d(n - 64);
            }
            return (this.a & 1L << n) != 0x0L;
        }
        
        public void e(final int n, final boolean b) {
            if (n >= 64) {
                this.c();
                this.b.e(n - 64, b);
            }
            else {
                final long a = this.a;
                final boolean b2 = (Long.MIN_VALUE & a) != 0x0L;
                final long n2 = (1L << n) - 1L;
                this.a = ((a & ~n2) << 1 | (a & n2));
                if (b) {
                    this.h(n);
                }
                else {
                    this.a(n);
                }
                if (b2 || this.b != null) {
                    this.c();
                    this.b.e(0, b2);
                }
            }
        }
        
        public boolean f(final int n) {
            if (n >= 64) {
                this.c();
                return this.b.f(n - 64);
            }
            final long n2 = 1L << n;
            final long a = this.a;
            final boolean b = (a & n2) != 0x0L;
            final long a2 = a & ~n2;
            this.a = a2;
            final long n3 = n2 - 1L;
            this.a = ((a2 & n3) | Long.rotateRight(~n3 & a2, 1));
            final a b2 = this.b;
            if (b2 != null) {
                if (b2.d(0)) {
                    this.h(63);
                }
                this.b.f(0);
            }
            return b;
        }
        
        public void g() {
            this.a = 0L;
            final a b = this.b;
            if (b != null) {
                b.g();
            }
        }
        
        public void h(final int n) {
            if (n >= 64) {
                this.c();
                this.b.h(n - 64);
            }
            else {
                this.a |= 1L << n;
            }
        }
        
        @Override
        public String toString() {
            String s;
            if (this.b == null) {
                s = Long.toBinaryString(this.a);
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.b.toString());
                sb.append("xx");
                sb.append(Long.toBinaryString(this.a));
                s = sb.toString();
            }
            return s;
        }
    }
    
    public interface b
    {
        View a(final int p0);
        
        void addView(final View p0, final int p1);
        
        void b(final View p0);
        
        int c();
        
        RecyclerView.d0 d(final View p0);
        
        void e(final View p0, final int p1, final ViewGroup$LayoutParams p2);
        
        void f(final int p0);
        
        int g(final View p0);
        
        void h(final View p0);
        
        void removeAllViews();
        
        void removeViewAt(final int p0);
    }
}

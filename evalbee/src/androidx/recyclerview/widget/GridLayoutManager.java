// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import java.util.Arrays;
import android.view.View$MeasureSpec;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.util.Log;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.util.SparseIntArray;
import android.view.View;

public class GridLayoutManager extends LinearLayoutManager
{
    public boolean a;
    public int b;
    public int[] c;
    public View[] d;
    public final SparseIntArray e;
    public final SparseIntArray f;
    public c g;
    public final Rect h;
    public boolean i;
    
    public GridLayoutManager(final Context context, final int n, final int n2, final boolean b) {
        super(context, n2, b);
        this.a = false;
        this.b = -1;
        this.e = new SparseIntArray();
        this.f = new SparseIntArray();
        this.g = (c)new a();
        this.h = new Rect();
        this.S(n);
    }
    
    public GridLayoutManager(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.a = false;
        this.b = -1;
        this.e = new SparseIntArray();
        this.f = new SparseIntArray();
        this.g = (c)new a();
        this.h = new Rect();
        this.S(RecyclerView.o.getProperties(context, set, n, n2).b);
    }
    
    public static int[] E(final int[] array, final int n, int n2) {
        int i = 1;
        int[] array2 = null;
        Label_0035: {
            if (array != null && array.length == n + 1) {
                array2 = array;
                if (array[array.length - 1] == n2) {
                    break Label_0035;
                }
            }
            array2 = new int[n + 1];
        }
        final int n3 = 0;
        array2[0] = 0;
        final int n4 = n2 / n;
        final int n5 = n2 % n;
        int n6 = 0;
        n2 = n3;
        while (i <= n) {
            n2 += n5;
            int n7;
            if (n2 > 0 && n - n2 < n5) {
                n7 = n4 + 1;
                n2 -= n;
            }
            else {
                n7 = n4;
            }
            n6 += n7;
            array2[i] = n6;
            ++i;
        }
        return array2;
    }
    
    public final void B(final v v, final a0 a0, int i, final boolean b) {
        int a2 = 0;
        int n;
        int n2;
        if (b) {
            n = 1;
            n2 = i;
            i = 0;
        }
        else {
            --i;
            n2 = -1;
            n = -1;
        }
        while (i != n2) {
            final View view = this.d[i];
            final b b2 = (b)view.getLayoutParams();
            final int o = this.O(v, a0, ((RecyclerView.o)this).getPosition(view));
            b2.b = o;
            b2.a = a2;
            a2 += o;
            i += n;
        }
    }
    
    public final void C() {
        for (int childCount = ((RecyclerView.o)this).getChildCount(), i = 0; i < childCount; ++i) {
            final b b = (b)((RecyclerView.o)this).getChildAt(i).getLayoutParams();
            final int viewLayoutPosition = ((RecyclerView.p)b).getViewLayoutPosition();
            this.e.put(viewLayoutPosition, b.c());
            this.f.put(viewLayoutPosition, b.b());
        }
    }
    
    public final void D(final int n) {
        this.c = E(this.c, this.b, n);
    }
    
    public final void F() {
        this.e.clear();
        this.f.clear();
    }
    
    public final int G(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() != 0) {
            if (a0.b() != 0) {
                this.ensureLayoutState();
                final boolean smoothScrollbarEnabled = this.isSmoothScrollbarEnabled();
                final View firstVisibleChildClosestToStart = this.findFirstVisibleChildClosestToStart(smoothScrollbarEnabled ^ true, true);
                final View firstVisibleChildClosestToEnd = this.findFirstVisibleChildClosestToEnd(smoothScrollbarEnabled ^ true, true);
                if (firstVisibleChildClosestToStart != null) {
                    if (firstVisibleChildClosestToEnd != null) {
                        final int b = this.g.b(((RecyclerView.o)this).getPosition(firstVisibleChildClosestToStart), this.b);
                        final int b2 = this.g.b(((RecyclerView.o)this).getPosition(firstVisibleChildClosestToEnd), this.b);
                        final int min = Math.min(b, b2);
                        final int max = Math.max(b, b2);
                        final int b3 = this.g.b(a0.b() - 1, this.b);
                        int n;
                        if (super.mShouldReverseLayout) {
                            n = Math.max(0, b3 + 1 - max - 1);
                        }
                        else {
                            n = Math.max(0, min);
                        }
                        if (!smoothScrollbarEnabled) {
                            return n;
                        }
                        return Math.round(n * (Math.abs(super.mOrientationHelper.d(firstVisibleChildClosestToEnd) - super.mOrientationHelper.g(firstVisibleChildClosestToStart)) / (float)(this.g.b(((RecyclerView.o)this).getPosition(firstVisibleChildClosestToEnd), this.b) - this.g.b(((RecyclerView.o)this).getPosition(firstVisibleChildClosestToStart), this.b) + 1)) + (super.mOrientationHelper.m() - super.mOrientationHelper.g(firstVisibleChildClosestToStart)));
                    }
                }
            }
        }
        return 0;
    }
    
    public final int H(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() != 0) {
            if (a0.b() != 0) {
                this.ensureLayoutState();
                final View firstVisibleChildClosestToStart = this.findFirstVisibleChildClosestToStart(this.isSmoothScrollbarEnabled() ^ true, true);
                final View firstVisibleChildClosestToEnd = this.findFirstVisibleChildClosestToEnd(this.isSmoothScrollbarEnabled() ^ true, true);
                if (firstVisibleChildClosestToStart != null) {
                    if (firstVisibleChildClosestToEnd != null) {
                        if (!this.isSmoothScrollbarEnabled()) {
                            return this.g.b(a0.b() - 1, this.b) + 1;
                        }
                        return (int)((super.mOrientationHelper.d(firstVisibleChildClosestToEnd) - super.mOrientationHelper.g(firstVisibleChildClosestToStart)) / (float)(this.g.b(((RecyclerView.o)this).getPosition(firstVisibleChildClosestToEnd), this.b) - this.g.b(((RecyclerView.o)this).getPosition(firstVisibleChildClosestToStart), this.b) + 1) * (this.g.b(a0.b() - 1, this.b) + 1));
                    }
                }
            }
        }
        return 0;
    }
    
    public final void I(final v v, final a0 a0, final LinearLayoutManager.a a2, int i) {
        final boolean b = i == 1;
        i = this.N(v, a0, a2.b);
        if (b) {
            while (i > 0) {
                i = a2.b;
                if (i <= 0) {
                    break;
                }
                --i;
                a2.b = i;
                i = this.N(v, a0, i);
            }
        }
        else {
            int b2;
            int j;
            int n;
            int n2;
            for (b2 = a0.b(), j = a2.b; j < b2 - 1; j = n, i = n2) {
                n = j + 1;
                n2 = this.N(v, a0, n);
                if (n2 <= i) {
                    break;
                }
            }
            a2.b = j;
        }
    }
    
    public final void J() {
        final View[] d = this.d;
        if (d == null || d.length != this.b) {
            this.d = new View[this.b];
        }
    }
    
    public int K(final int n, final int n2) {
        if (super.mOrientation == 1 && this.isLayoutRTL()) {
            final int[] c = this.c;
            final int b = this.b;
            return c[b - n] - c[b - n - n2];
        }
        final int[] c2 = this.c;
        return c2[n2 + n] - c2[n];
    }
    
    public int L() {
        return this.b;
    }
    
    public final int M(final v v, final a0 a0, final int i) {
        if (!a0.e()) {
            return this.g.b(i, this.b);
        }
        final int f = v.f(i);
        if (f == -1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find span size for pre layout position. ");
            sb.append(i);
            Log.w("GridLayoutManager", sb.toString());
            return 0;
        }
        return this.g.b(f, this.b);
    }
    
    public final int N(final v v, final a0 a0, final int i) {
        if (!a0.e()) {
            return this.g.c(i, this.b);
        }
        final int value = this.f.get(i, -1);
        if (value != -1) {
            return value;
        }
        final int f = v.f(i);
        if (f == -1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
            sb.append(i);
            Log.w("GridLayoutManager", sb.toString());
            return 0;
        }
        return this.g.c(f, this.b);
    }
    
    public final int O(final v v, final a0 a0, final int i) {
        if (!a0.e()) {
            return this.g.f(i);
        }
        final int value = this.e.get(i, -1);
        if (value != -1) {
            return value;
        }
        final int f = v.f(i);
        if (f == -1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
            sb.append(i);
            Log.w("GridLayoutManager", sb.toString());
            return 1;
        }
        return this.g.f(f);
    }
    
    public final void P(final float n, final int b) {
        this.D(Math.max(Math.round(n * this.b), b));
    }
    
    public final void Q(final View view, int n, final boolean b) {
        final b b2 = (b)view.getLayoutParams();
        final Rect mDecorInsets = b2.mDecorInsets;
        final int n2 = mDecorInsets.top + mDecorInsets.bottom + b2.topMargin + b2.bottomMargin;
        final int n3 = mDecorInsets.left + mDecorInsets.right + b2.leftMargin + b2.rightMargin;
        final int k = this.K(b2.a, b2.b);
        int n4;
        if (super.mOrientation == 1) {
            n4 = RecyclerView.o.getChildMeasureSpec(k, n, n3, b2.width, false);
            n = RecyclerView.o.getChildMeasureSpec(super.mOrientationHelper.n(), ((RecyclerView.o)this).getHeightMode(), n2, b2.height, true);
        }
        else {
            n = RecyclerView.o.getChildMeasureSpec(k, n, n2, b2.height, false);
            n4 = RecyclerView.o.getChildMeasureSpec(super.mOrientationHelper.n(), ((RecyclerView.o)this).getWidthMode(), n3, b2.width, true);
        }
        this.R(view, n4, n, b);
    }
    
    public final void R(final View view, final int n, final int n2, final boolean b) {
        final p p4 = (p)view.getLayoutParams();
        boolean b2;
        if (b) {
            b2 = ((RecyclerView.o)this).shouldReMeasureChild(view, n, n2, p4);
        }
        else {
            b2 = ((RecyclerView.o)this).shouldMeasureChild(view, n, n2, p4);
        }
        if (b2) {
            view.measure(n, n2);
        }
    }
    
    public void S(final int n) {
        if (n == this.b) {
            return;
        }
        this.a = true;
        if (n >= 1) {
            this.b = n;
            this.g.h();
            ((RecyclerView.o)this).requestLayout();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Span count should be at least 1. Provided ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void T() {
        int n;
        int n2;
        if (this.getOrientation() == 1) {
            n = ((RecyclerView.o)this).getWidth() - ((RecyclerView.o)this).getPaddingRight();
            n2 = ((RecyclerView.o)this).getPaddingLeft();
        }
        else {
            n = ((RecyclerView.o)this).getHeight() - ((RecyclerView.o)this).getPaddingBottom();
            n2 = ((RecyclerView.o)this).getPaddingTop();
        }
        this.D(n - n2);
    }
    
    @Override
    public boolean checkLayoutParams(final p p) {
        return p instanceof b;
    }
    
    @Override
    public void collectPrefetchPositionsForLayoutState(final a0 a0, final LinearLayoutManager.c c, final o.c c2) {
        int d;
        for (int b = this.b, n = 0; n < this.b && c.c(a0) && b > 0; b -= this.g.f(d), c.d += c.e, ++n) {
            d = c.d;
            c2.a(d, Math.max(0, c.g));
        }
    }
    
    @Override
    public int computeHorizontalScrollOffset(final a0 a0) {
        if (this.i) {
            return this.G(a0);
        }
        return super.computeHorizontalScrollOffset(a0);
    }
    
    @Override
    public int computeHorizontalScrollRange(final a0 a0) {
        if (this.i) {
            return this.H(a0);
        }
        return super.computeHorizontalScrollRange(a0);
    }
    
    @Override
    public int computeVerticalScrollOffset(final a0 a0) {
        if (this.i) {
            return this.G(a0);
        }
        return super.computeVerticalScrollOffset(a0);
    }
    
    @Override
    public int computeVerticalScrollRange(final a0 a0) {
        if (this.i) {
            return this.H(a0);
        }
        return super.computeVerticalScrollRange(a0);
    }
    
    @Override
    public View findReferenceChild(final v v, final a0 a0, int i, final int n, final int n2) {
        this.ensureLayoutState();
        final int m = super.mOrientationHelper.m();
        final int j = super.mOrientationHelper.i();
        int n3;
        if (n > i) {
            n3 = 1;
        }
        else {
            n3 = -1;
        }
        View view = null;
        View view2 = null;
        while (i != n) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            final int position = ((RecyclerView.o)this).getPosition(child);
            View view3 = view;
            View view4 = view2;
            if (position >= 0) {
                view3 = view;
                view4 = view2;
                if (position < n2) {
                    if (this.N(v, a0, position) != 0) {
                        view3 = view;
                        view4 = view2;
                    }
                    else if (((p)child.getLayoutParams()).isItemRemoved()) {
                        view3 = view;
                        if ((view4 = view2) == null) {
                            view4 = child;
                            view3 = view;
                        }
                    }
                    else {
                        if (super.mOrientationHelper.g(child) < j && super.mOrientationHelper.d(child) >= m) {
                            return child;
                        }
                        view3 = view;
                        view4 = view2;
                        if (view == null) {
                            view3 = child;
                            view4 = view2;
                        }
                    }
                }
            }
            i += n3;
            view = view3;
            view2 = view4;
        }
        if (view != null) {
            view2 = view;
        }
        return view2;
    }
    
    @Override
    public p generateDefaultLayoutParams() {
        if (super.mOrientation == 0) {
            return new b(-2, -1);
        }
        return new b(-1, -2);
    }
    
    @Override
    public p generateLayoutParams(final Context context, final AttributeSet set) {
        return new b(context, set);
    }
    
    @Override
    public p generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return new b((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return new b(viewGroup$LayoutParams);
    }
    
    @Override
    public int getColumnCountForAccessibility(final v v, final a0 a0) {
        if (super.mOrientation == 1) {
            return this.b;
        }
        if (a0.b() < 1) {
            return 0;
        }
        return this.M(v, a0, a0.b() - 1) + 1;
    }
    
    @Override
    public int getRowCountForAccessibility(final v v, final a0 a0) {
        if (super.mOrientation == 0) {
            return this.b;
        }
        if (a0.b() < 1) {
            return 0;
        }
        return this.M(v, a0, a0.b() - 1) + 1;
    }
    
    @Override
    public void layoutChunk(final v v, final a0 a0, final LinearLayoutManager.c c, final LinearLayoutManager.b b) {
        final int l = super.mOrientationHelper.l();
        final boolean b2 = l != 1073741824;
        int n;
        if (((RecyclerView.o)this).getChildCount() > 0) {
            n = this.c[this.b];
        }
        else {
            n = 0;
        }
        if (b2) {
            this.T();
        }
        final boolean b3 = c.e == 1;
        int b4 = this.b;
        if (!b3) {
            b4 = this.N(v, a0, c.d) + this.O(v, a0, c.d);
        }
        int n2;
        for (n2 = 0; n2 < this.b && c.c(a0) && b4 > 0; ++n2) {
            final int d = c.d;
            final int o = this.O(v, a0, d);
            if (o > this.b) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Item at position ");
                sb.append(d);
                sb.append(" requires ");
                sb.append(o);
                sb.append(" spans but GridLayoutManager has only ");
                sb.append(this.b);
                sb.append(" spans.");
                throw new IllegalArgumentException(sb.toString());
            }
            b4 -= o;
            if (b4 < 0) {
                break;
            }
            final View d2 = c.d(v);
            if (d2 == null) {
                break;
            }
            this.d[n2] = d2;
        }
        if (n2 == 0) {
            b.b = true;
            return;
        }
        this.B(v, a0, n2, b3);
        float n3 = 0.0f;
        int i = 0;
        int n4 = 0;
        while (i < n2) {
            final View view = this.d[i];
            if (c.l == null) {
                if (b3) {
                    ((RecyclerView.o)this).addView(view);
                }
                else {
                    ((RecyclerView.o)this).addView(view, 0);
                }
            }
            else if (b3) {
                ((RecyclerView.o)this).addDisappearingView(view);
            }
            else {
                ((RecyclerView.o)this).addDisappearingView(view, 0);
            }
            ((RecyclerView.o)this).calculateItemDecorationsForChild(view, this.h);
            this.Q(view, l, false);
            final int e = super.mOrientationHelper.e(view);
            int n5 = n4;
            if (e > n4) {
                n5 = e;
            }
            final float n6 = super.mOrientationHelper.f(view) * 1.0f / ((b)view.getLayoutParams()).b;
            float n7 = n3;
            if (n6 > n3) {
                n7 = n6;
            }
            ++i;
            n3 = n7;
            n4 = n5;
        }
        int a2 = n4;
        if (b2) {
            this.P(n3, n);
            int n8 = 0;
            int n9 = 0;
            while (true) {
                a2 = n9;
                if (n8 >= n2) {
                    break;
                }
                final View view2 = this.d[n8];
                this.Q(view2, 1073741824, true);
                final int e2 = super.mOrientationHelper.e(view2);
                int n10;
                if (e2 > (n10 = n9)) {
                    n10 = e2;
                }
                ++n8;
                n9 = n10;
            }
        }
        for (int j = 0; j < n2; ++j) {
            final View view3 = this.d[j];
            if (super.mOrientationHelper.e(view3) != a2) {
                final b b5 = (b)view3.getLayoutParams();
                final Rect mDecorInsets = b5.mDecorInsets;
                final int n11 = mDecorInsets.top + mDecorInsets.bottom + b5.topMargin + b5.bottomMargin;
                final int n12 = mDecorInsets.left + mDecorInsets.right + b5.leftMargin + b5.rightMargin;
                final int k = this.K(b5.a, b5.b);
                int n13;
                int n14;
                if (super.mOrientation == 1) {
                    n13 = RecyclerView.o.getChildMeasureSpec(k, 1073741824, n12, b5.width, false);
                    n14 = View$MeasureSpec.makeMeasureSpec(a2 - n11, 1073741824);
                }
                else {
                    n13 = View$MeasureSpec.makeMeasureSpec(a2 - n12, 1073741824);
                    n14 = RecyclerView.o.getChildMeasureSpec(k, 1073741824, n11, b5.height, false);
                }
                this.R(view3, n13, n14, true);
            }
        }
        b.a = a2;
        int b6;
        int b7;
        int n15;
        int n16;
        if (super.mOrientation == 1) {
            if (c.f == -1) {
                b6 = c.b;
                b7 = b6 - a2;
            }
            else {
                b7 = c.b;
                b6 = b7 + a2;
            }
            n15 = 0;
            n16 = 0;
        }
        else {
            int b8;
            int b9;
            if (c.f == -1) {
                b8 = c.b;
                b9 = b8 - a2;
            }
            else {
                b9 = c.b;
                b8 = b9 + a2;
            }
            final int n17 = 0;
            n15 = b8;
            final int n18 = 0;
            n16 = b9;
            b7 = n17;
            b6 = n18;
        }
        int n22 = 0;
        int n23 = 0;
        int n25 = 0;
        int n28;
        for (int n19 = 0; n19 < n2; ++n19, n28 = n25, b7 = n23, n16 = n22, b6 = n28) {
            final View view4 = this.d[n19];
            final b b10 = (b)view4.getLayoutParams();
            Label_1060: {
                int n26;
                if (super.mOrientation == 1) {
                    if (!this.isLayoutRTL()) {
                        final int n20 = ((RecyclerView.o)this).getPaddingLeft() + this.c[b10.a];
                        final int f = super.mOrientationHelper.f(view4);
                        final int n21 = b6;
                        n22 = n20;
                        n23 = b7;
                        final int n24 = f + n20;
                        n25 = n21;
                        n15 = n24;
                        break Label_1060;
                    }
                    n15 = ((RecyclerView.o)this).getPaddingLeft() + this.c[this.b - b10.a];
                    n16 = n15 - super.mOrientationHelper.f(view4);
                    n26 = b7;
                    n25 = b6;
                }
                else {
                    final int n27 = ((RecyclerView.o)this).getPaddingTop() + this.c[b10.a];
                    final int f2 = super.mOrientationHelper.f(view4);
                    n26 = n27;
                    n25 = f2 + n27;
                }
                n22 = n16;
                n23 = n26;
            }
            ((RecyclerView.o)this).layoutDecoratedWithMargins(view4, n22, n23, n15, n25);
            if (((RecyclerView.p)b10).isItemRemoved() || ((RecyclerView.p)b10).isItemChanged()) {
                b.c = true;
            }
            b.d |= view4.hasFocusable();
        }
        Arrays.fill(this.d, null);
    }
    
    @Override
    public void onAnchorReady(final v v, final a0 a0, final LinearLayoutManager.a a2, final int n) {
        super.onAnchorReady(v, a0, a2, n);
        this.T();
        if (a0.b() > 0 && !a0.e()) {
            this.I(v, a0, a2, n);
        }
        this.J();
    }
    
    @Override
    public View onFocusSearchFailed(View view, int n, final v v, final a0 a0) {
        final View containingItemView = ((RecyclerView.o)this).findContainingItemView(view);
        View view2 = null;
        if (containingItemView == null) {
            return null;
        }
        final b b = (b)containingItemView.getLayoutParams();
        final int a2 = b.a;
        final int b2 = b.b + a2;
        if (super.onFocusSearchFailed(view, n, v, a0) == null) {
            return null;
        }
        if (this.convertFocusDirectionToLayoutDirection(n) == 1 != super.mShouldReverseLayout) {
            n = 1;
        }
        else {
            n = 0;
        }
        int childCount;
        int n2;
        if (n != 0) {
            n = ((RecyclerView.o)this).getChildCount() - 1;
            childCount = -1;
            n2 = -1;
        }
        else {
            childCount = ((RecyclerView.o)this).getChildCount();
            n2 = 1;
            n = 0;
        }
        final boolean b3 = super.mOrientation == 1 && this.isLayoutRTL();
        final int m = this.M(v, a0, n);
        int a3 = -1;
        int a4 = -1;
        final int n3 = 0;
        final int n4 = 0;
        int i = n;
        view = null;
        n = n4;
        final int n5 = childCount;
        int n6 = n3;
        while (i != n5) {
            final int j = this.M(v, a0, i);
            final View child = ((RecyclerView.o)this).getChildAt(i);
            if (child == containingItemView) {
                break;
            }
            if (child.hasFocusable() && j != m) {
                if (view2 != null) {
                    break;
                }
            }
            else {
                final b b4 = (b)child.getLayoutParams();
                final int a5 = b4.a;
                final int a6 = b4.b + a5;
                if (child.hasFocusable() && a5 == a2 && a6 == b2) {
                    return child;
                }
                int n9 = 0;
                Label_0472: {
                    Label_0324: {
                        if ((child.hasFocusable() || view2 != null) && (child.hasFocusable() || view != null)) {
                            final int n7 = Math.min(a6, b2) - Math.max(a5, a2);
                            if (child.hasFocusable()) {
                                if (n7 > n6 || (n7 == n6 && b3 == a5 > a3)) {
                                    break Label_0324;
                                }
                            }
                            else if (view2 == null) {
                                final int n8 = 1;
                                boolean b5 = true;
                                if (((RecyclerView.o)this).isViewPartiallyVisible(child, false, true)) {
                                    if (n7 > n) {
                                        n9 = n8;
                                        break Label_0472;
                                    }
                                    if (n7 == n) {
                                        if (a5 <= a4) {
                                            b5 = false;
                                        }
                                        if (b3 == b5) {
                                            break Label_0324;
                                        }
                                    }
                                }
                            }
                            n9 = 0;
                            break Label_0472;
                        }
                    }
                    n9 = 1;
                }
                if (n9 != 0) {
                    if (child.hasFocusable()) {
                        a3 = b4.a;
                        final int min = Math.min(a6, b2);
                        final int max = Math.max(a5, a2);
                        view2 = child;
                        n6 = min - max;
                    }
                    else {
                        a4 = b4.a;
                        n = Math.min(a6, b2) - Math.max(a5, a2);
                        view = child;
                    }
                }
            }
            i += n2;
        }
        if (view2 == null) {
            view2 = view;
        }
        return view2;
    }
    
    @Override
    public void onInitializeAccessibilityNodeInfoForItem(final v v, final a0 a0, final View view, final n1 n1) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.onInitializeAccessibilityNodeInfoForItem(view, n1);
            return;
        }
        final b b = (b)layoutParams;
        int m = this.M(v, a0, ((RecyclerView.p)b).getViewLayoutPosition());
        int c;
        int c2;
        int b3;
        if (super.mOrientation == 0) {
            final int b2 = b.b();
            c = b.c();
            c2 = 1;
            b3 = m;
            m = b2;
        }
        else {
            c = 1;
            b3 = b.b();
            c2 = b.c();
        }
        n1.b0(n1.c.a(m, c, b3, c2, false, false));
    }
    
    @Override
    public void onItemsAdded(final RecyclerView recyclerView, final int n, final int n2) {
        this.g.h();
        this.g.g();
    }
    
    @Override
    public void onItemsChanged(final RecyclerView recyclerView) {
        this.g.h();
        this.g.g();
    }
    
    @Override
    public void onItemsMoved(final RecyclerView recyclerView, final int n, final int n2, final int n3) {
        this.g.h();
        this.g.g();
    }
    
    @Override
    public void onItemsRemoved(final RecyclerView recyclerView, final int n, final int n2) {
        this.g.h();
        this.g.g();
    }
    
    @Override
    public void onItemsUpdated(final RecyclerView recyclerView, final int n, final int n2, final Object o) {
        this.g.h();
        this.g.g();
    }
    
    @Override
    public void onLayoutChildren(final v v, final a0 a0) {
        if (a0.e()) {
            this.C();
        }
        super.onLayoutChildren(v, a0);
        this.F();
    }
    
    @Override
    public void onLayoutCompleted(final a0 a0) {
        super.onLayoutCompleted(a0);
        this.a = false;
    }
    
    @Override
    public int scrollHorizontallyBy(final int n, final v v, final a0 a0) {
        this.T();
        this.J();
        return super.scrollHorizontallyBy(n, v, a0);
    }
    
    @Override
    public int scrollVerticallyBy(final int n, final v v, final a0 a0) {
        this.T();
        this.J();
        return super.scrollVerticallyBy(n, v, a0);
    }
    
    @Override
    public void setMeasuredDimension(final Rect rect, int n, int n2) {
        if (this.c == null) {
            super.setMeasuredDimension(rect, n, n2);
        }
        final int n3 = ((RecyclerView.o)this).getPaddingLeft() + ((RecyclerView.o)this).getPaddingRight();
        final int n4 = ((RecyclerView.o)this).getPaddingTop() + ((RecyclerView.o)this).getPaddingBottom();
        if (super.mOrientation == 1) {
            n2 = RecyclerView.o.chooseSize(n2, rect.height() + n4, ((RecyclerView.o)this).getMinimumHeight());
            final int[] c = this.c;
            n = RecyclerView.o.chooseSize(n, c[c.length - 1] + n3, ((RecyclerView.o)this).getMinimumWidth());
        }
        else {
            n = RecyclerView.o.chooseSize(n, rect.width() + n3, ((RecyclerView.o)this).getMinimumWidth());
            final int[] c2 = this.c;
            n2 = RecyclerView.o.chooseSize(n2, c2[c2.length - 1] + n4, ((RecyclerView.o)this).getMinimumHeight());
        }
        ((RecyclerView.o)this).setMeasuredDimension(n, n2);
    }
    
    @Override
    public void setStackFromEnd(final boolean b) {
        if (!b) {
            super.setStackFromEnd(false);
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }
    
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return super.mPendingSavedState == null && !this.a;
    }
    
    public static final class a extends c
    {
        @Override
        public int e(final int n, final int n2) {
            return n % n2;
        }
        
        @Override
        public int f(final int n) {
            return 1;
        }
    }
    
    public static class b extends p
    {
        public int a;
        public int b;
        
        public b(final int n, final int n2) {
            super(n, n2);
            this.a = -1;
            this.b = 0;
        }
        
        public b(final Context context, final AttributeSet set) {
            super(context, set);
            this.a = -1;
            this.b = 0;
        }
        
        public b(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.a = -1;
            this.b = 0;
        }
        
        public b(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.a = -1;
            this.b = 0;
        }
        
        public int b() {
            return this.a;
        }
        
        public int c() {
            return this.b;
        }
    }
    
    public abstract static class c
    {
        public final SparseIntArray a;
        public final SparseIntArray b;
        public boolean c;
        public boolean d;
        
        public c() {
            this.a = new SparseIntArray();
            this.b = new SparseIntArray();
            this.c = false;
            this.d = false;
        }
        
        public static int a(final SparseIntArray sparseIntArray, int n) {
            int n2 = sparseIntArray.size() - 1;
            int i = 0;
            while (i <= n2) {
                final int n3 = i + n2 >>> 1;
                if (sparseIntArray.keyAt(n3) < n) {
                    i = n3 + 1;
                }
                else {
                    n2 = n3 - 1;
                }
            }
            n = i - 1;
            if (n >= 0 && n < sparseIntArray.size()) {
                return sparseIntArray.keyAt(n);
            }
            return -1;
        }
        
        public int b(final int n, int d) {
            if (!this.d) {
                return this.d(n, d);
            }
            final int value = this.b.get(n, -1);
            if (value != -1) {
                return value;
            }
            d = this.d(n, d);
            this.b.put(n, d);
            return d;
        }
        
        public int c(final int n, int e) {
            if (!this.c) {
                return this.e(n, e);
            }
            final int value = this.a.get(n, -1);
            if (value != -1) {
                return value;
            }
            e = this.e(n, e);
            this.a.put(n, e);
            return e;
        }
        
        public int d(int n, final int n2) {
            int n5 = 0;
            int n6 = 0;
            int n7 = 0;
            Label_0091: {
                if (this.d) {
                    final int a = a(this.b, n);
                    if (a != -1) {
                        final int value = this.b.get(a);
                        final int n3 = a + 1;
                        final int n4 = this.c(a, n2) + this.f(a);
                        n5 = value;
                        n6 = n3;
                        if ((n7 = n4) == n2) {
                            n5 = value + 1;
                            n7 = 0;
                            n6 = n3;
                        }
                        break Label_0091;
                    }
                }
                n5 = 0;
                n6 = (n7 = 0);
            }
            final int f = this.f(n);
            int n8 = n7;
            int i = n6;
            int n9 = n5;
            while (i < n) {
                final int f2 = this.f(i);
                final int n10 = n8 + f2;
                int n11;
                int n12;
                if (n10 == n2) {
                    n11 = n9 + 1;
                    n12 = 0;
                }
                else {
                    n11 = n9;
                    if ((n12 = n10) > n2) {
                        n11 = n9 + 1;
                        n12 = f2;
                    }
                }
                ++i;
                n9 = n11;
                n8 = n12;
            }
            n = n9;
            if (n8 + f > n2) {
                n = n9 + 1;
            }
            return n;
        }
        
        public abstract int e(final int p0, final int p1);
        
        public abstract int f(final int p0);
        
        public void g() {
            this.b.clear();
        }
        
        public void h() {
            this.a.clear();
        }
    }
}

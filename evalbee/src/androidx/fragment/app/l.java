// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.List;
import android.os.Bundle;
import android.os.Parcel;
import java.util.ArrayList;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class l implements Parcelable
{
    public static final Parcelable$Creator<l> CREATOR;
    public ArrayList a;
    public ArrayList b;
    public b[] c;
    public int d;
    public String e;
    public ArrayList f;
    public ArrayList g;
    public ArrayList h;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public l a(final Parcel parcel) {
                return new l(parcel);
            }
            
            public l[] b(final int n) {
                return new l[n];
            }
        };
    }
    
    public l() {
        this.e = null;
        this.f = new ArrayList();
        this.g = new ArrayList();
    }
    
    public l(final Parcel parcel) {
        this.e = null;
        this.f = new ArrayList();
        this.g = new ArrayList();
        this.a = parcel.createTypedArrayList((Parcelable$Creator)n.CREATOR);
        this.b = parcel.createStringArrayList();
        this.c = (b[])parcel.createTypedArray((Parcelable$Creator)androidx.fragment.app.b.CREATOR);
        this.d = parcel.readInt();
        this.e = parcel.readString();
        this.f = parcel.createStringArrayList();
        this.g = parcel.createTypedArrayList(Bundle.CREATOR);
        this.h = parcel.createTypedArrayList((Parcelable$Creator)k.l.CREATOR);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeTypedList((List)this.a);
        parcel.writeStringList((List)this.b);
        parcel.writeTypedArray((Parcelable[])this.c, n);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
        parcel.writeStringList((List)this.f);
        parcel.writeTypedList((List)this.g);
        parcel.writeTypedList((List)this.h);
    }
}

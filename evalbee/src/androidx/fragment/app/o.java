// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.BaseBundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ViewParent;
import java.util.Iterator;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View$OnAttachStateChangeListener;
import android.content.res.Resources$NotFoundException;
import android.view.ViewGroup;
import android.view.View;
import android.util.Log;
import androidx.lifecycle.Lifecycle;
import android.os.Bundle;

public class o
{
    public final j a;
    public final p b;
    public final Fragment c;
    public boolean d;
    public int e;
    
    public o(final j a, final p b, final Fragment c) {
        this.d = false;
        this.e = -1;
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public o(final j a, final p b, final Fragment c, final n n) {
        this.d = false;
        this.e = -1;
        this.a = a;
        this.b = b;
        this.c = c;
        c.mSavedViewState = null;
        c.mSavedViewRegistryState = null;
        c.mBackStackNesting = 0;
        c.mInLayout = false;
        c.mAdded = false;
        final Fragment mTarget = c.mTarget;
        String mWho;
        if (mTarget != null) {
            mWho = mTarget.mWho;
        }
        else {
            mWho = null;
        }
        c.mTargetWho = mWho;
        c.mTarget = null;
        Bundle m = n.m;
        if (m == null) {
            m = new Bundle();
        }
        c.mSavedFragmentState = m;
    }
    
    public o(final j a, final p b, final ClassLoader classLoader, final g g, final n n) {
        this.d = false;
        this.e = -1;
        this.a = a;
        this.b = b;
        final Fragment a2 = g.a(classLoader, n.a);
        this.c = a2;
        final Bundle j = n.j;
        if (j != null) {
            j.setClassLoader(classLoader);
        }
        a2.setArguments(n.j);
        a2.mWho = n.b;
        a2.mFromLayout = n.c;
        a2.mRestored = true;
        a2.mFragmentId = n.d;
        a2.mContainerId = n.e;
        a2.mTag = n.f;
        a2.mRetainInstance = n.g;
        a2.mRemoving = n.h;
        a2.mDetached = n.i;
        a2.mHidden = n.k;
        a2.mMaxState = Lifecycle.State.values()[n.l];
        Bundle m = n.m;
        if (m == null) {
            m = new Bundle();
        }
        a2.mSavedFragmentState = m;
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Instantiated fragment ");
            sb.append(a2);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    public void a() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto ACTIVITY_CREATED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment c = this.c;
        c.performActivityCreated(c.mSavedFragmentState);
        final j a = this.a;
        final Fragment c2 = this.c;
        a.a(c2, c2.mSavedFragmentState, false);
    }
    
    public void b() {
        final int j = this.b.j(this.c);
        final Fragment c = this.c;
        c.mContainer.addView(c.mView, j);
    }
    
    public void c() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto ATTACHED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment c = this.c;
        final Fragment mTarget = c.mTarget;
        o o = null;
        if (mTarget != null) {
            o = this.b.m(mTarget.mWho);
            if (o == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(this.c);
                sb2.append(" declared target fragment ");
                sb2.append(this.c.mTarget);
                sb2.append(" that does not belong to this FragmentManager!");
                throw new IllegalStateException(sb2.toString());
            }
            final Fragment c2 = this.c;
            c2.mTargetWho = c2.mTarget.mWho;
            c2.mTarget = null;
        }
        else {
            final String mTargetWho = c.mTargetWho;
            if (mTargetWho != null) {
                o = this.b.m(mTargetWho);
                if (o == null) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Fragment ");
                    sb3.append(this.c);
                    sb3.append(" declared target fragment ");
                    sb3.append(this.c.mTargetWho);
                    sb3.append(" that does not belong to this FragmentManager!");
                    throw new IllegalStateException(sb3.toString());
                }
            }
        }
        if (o != null && (k.P || o.k().mState < 1)) {
            o.m();
        }
        final Fragment c3 = this.c;
        c3.mHost = c3.mFragmentManager.t0();
        final Fragment c4 = this.c;
        c4.mParentFragment = c4.mFragmentManager.w0();
        this.a.g(this.c, false);
        this.c.performAttach();
        this.a.b(this.c, false);
    }
    
    public int d() {
        final Fragment c = this.c;
        if (c.mFragmentManager == null) {
            return c.mState;
        }
        final int e = this.e;
        final int n = o$b.a[c.mMaxState.ordinal()];
        int n2 = e;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        n2 = Math.min(e, -1);
                    }
                    else {
                        n2 = Math.min(e, 0);
                    }
                }
                else {
                    n2 = Math.min(e, 1);
                }
            }
            else {
                n2 = Math.min(e, 5);
            }
        }
        final Fragment c2 = this.c;
        int a = n2;
        if (c2.mFromLayout) {
            if (c2.mInLayout) {
                final int max = Math.max(this.e, 2);
                final View mView = this.c.mView;
                a = max;
                if (mView != null) {
                    a = max;
                    if (mView.getParent() == null) {
                        a = Math.min(max, 2);
                    }
                }
            }
            else if (this.e < 4) {
                a = Math.min(n2, c2.mState);
            }
            else {
                a = Math.min(n2, 1);
            }
        }
        int min = a;
        if (!this.c.mAdded) {
            min = Math.min(a, 1);
        }
        Enum<SpecialEffectsController.Operation.LifecycleImpact> l = null;
        Label_0252: {
            if (k.P) {
                final Fragment c3 = this.c;
                final ViewGroup mContainer = c3.mContainer;
                if (mContainer != null) {
                    l = SpecialEffectsController.o(mContainer, c3.getParentFragmentManager()).l(this);
                    break Label_0252;
                }
            }
            l = null;
        }
        int a2;
        if (l == SpecialEffectsController.Operation.LifecycleImpact.ADDING) {
            a2 = Math.min(min, 6);
        }
        else if (l == SpecialEffectsController.Operation.LifecycleImpact.REMOVING) {
            a2 = Math.max(min, 3);
        }
        else {
            final Fragment c4 = this.c;
            a2 = min;
            if (c4.mRemoving) {
                if (c4.isInBackStack()) {
                    a2 = Math.min(min, 1);
                }
                else {
                    a2 = Math.min(min, -1);
                }
            }
        }
        final Fragment c5 = this.c;
        int min2 = a2;
        if (c5.mDeferStart) {
            min2 = a2;
            if (c5.mState < 5) {
                min2 = Math.min(a2, 4);
            }
        }
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("computeExpectedState() of ");
            sb.append(min2);
            sb.append(" for ");
            sb.append(this.c);
            Log.v("FragmentManager", sb.toString());
        }
        return min2;
    }
    
    public void e() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto CREATED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment c = this.c;
        if (!c.mIsCreated) {
            this.a.h(c, c.mSavedFragmentState, false);
            final Fragment c2 = this.c;
            c2.performCreate(c2.mSavedFragmentState);
            final j a = this.a;
            final Fragment c3 = this.c;
            a.c(c3, c3.mSavedFragmentState, false);
        }
        else {
            c.restoreChildFragmentState(c.mSavedFragmentState);
            this.c.mState = 1;
        }
    }
    
    public void f() {
        if (this.c.mFromLayout) {
            return;
        }
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto CREATE_VIEW: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment c = this.c;
        final LayoutInflater performGetLayoutInflater = c.performGetLayoutInflater(c.mSavedFragmentState);
        final Fragment c2 = this.c;
        ViewGroup mContainer = c2.mContainer;
        if (mContainer == null) {
            final int mContainerId = c2.mContainerId;
            if (mContainerId != 0) {
                if (mContainerId == -1) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot create fragment ");
                    sb2.append(this.c);
                    sb2.append(" for a container view with no id");
                    throw new IllegalArgumentException(sb2.toString());
                }
                final ViewGroup viewGroup = (ViewGroup)c2.mFragmentManager.o0().c(this.c.mContainerId);
                if ((mContainer = viewGroup) == null) {
                    final Fragment c3 = this.c;
                    if (!c3.mRestored) {
                        String resourceName;
                        try {
                            resourceName = c3.getResources().getResourceName(this.c.mContainerId);
                        }
                        catch (final Resources$NotFoundException ex) {
                            resourceName = "unknown";
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("No view found for id 0x");
                        sb3.append(Integer.toHexString(this.c.mContainerId));
                        sb3.append(" (");
                        sb3.append(resourceName);
                        sb3.append(") for fragment ");
                        sb3.append(this.c);
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    mContainer = viewGroup;
                }
            }
            else {
                mContainer = null;
            }
        }
        final Fragment c4 = this.c;
        c4.performCreateView(performGetLayoutInflater, c4.mContainer = mContainer, c4.mSavedFragmentState);
        final View mView = this.c.mView;
        if (mView != null) {
            final boolean b = false;
            mView.setSaveFromParentEnabled(false);
            final Fragment c5 = this.c;
            c5.mView.setTag(gb1.a, (Object)c5);
            if (mContainer != null) {
                this.b();
            }
            final Fragment c6 = this.c;
            if (c6.mHidden) {
                c6.mView.setVisibility(8);
            }
            if (o32.T(this.c.mView)) {
                o32.n0(this.c.mView);
            }
            else {
                final View mView2 = this.c.mView;
                mView2.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this, mView2) {
                    public final View a;
                    public final o b;
                    
                    public void onViewAttachedToWindow(final View view) {
                        this.a.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
                        o32.n0(this.a);
                    }
                    
                    public void onViewDetachedFromWindow(final View view) {
                    }
                });
            }
            this.c.performViewCreated();
            final j a = this.a;
            final Fragment c7 = this.c;
            a.m(c7, c7.mView, c7.mSavedFragmentState, false);
            final int visibility = this.c.mView.getVisibility();
            final float alpha = this.c.mView.getAlpha();
            if (k.P) {
                this.c.setPostOnViewCreatedAlpha(alpha);
                final Fragment c8 = this.c;
                if (c8.mContainer != null && visibility == 0) {
                    final View focus = c8.mView.findFocus();
                    if (focus != null) {
                        this.c.setFocusedView(focus);
                        if (k.F0(2)) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("requestFocus: Saved focused view ");
                            sb4.append(focus);
                            sb4.append(" for Fragment ");
                            sb4.append(this.c);
                            Log.v("FragmentManager", sb4.toString());
                        }
                    }
                    this.c.mView.setAlpha(0.0f);
                }
            }
            else {
                final Fragment c9 = this.c;
                boolean mIsNewlyAdded = b;
                if (visibility == 0) {
                    mIsNewlyAdded = b;
                    if (c9.mContainer != null) {
                        mIsNewlyAdded = true;
                    }
                }
                c9.mIsNewlyAdded = mIsNewlyAdded;
            }
        }
        this.c.mState = 2;
    }
    
    public void g() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom CREATED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment c = this.c;
        final boolean mRemoving = c.mRemoving;
        boolean m = true;
        final boolean b = mRemoving && !c.isInBackStack();
        if (b || this.b.o().p(this.c)) {
            final h mHost = this.c.mHost;
            if (mHost instanceof c42) {
                m = this.b.o().m();
            }
            else if (mHost.f() instanceof Activity) {
                m = (true ^ ((Activity)mHost.f()).isChangingConfigurations());
            }
            if (b || m) {
                this.b.o().g(this.c);
            }
            this.c.performDestroy();
            this.a.d(this.c, false);
            for (final o o : this.b.k()) {
                if (o != null) {
                    final Fragment k = o.k();
                    if (!this.c.mWho.equals(k.mTargetWho)) {
                        continue;
                    }
                    k.mTarget = this.c;
                    k.mTargetWho = null;
                }
            }
            final Fragment c2 = this.c;
            final String mTargetWho = c2.mTargetWho;
            if (mTargetWho != null) {
                c2.mTarget = this.b.f(mTargetWho);
            }
            this.b.q(this);
        }
        else {
            final String mTargetWho2 = this.c.mTargetWho;
            if (mTargetWho2 != null) {
                final Fragment f = this.b.f(mTargetWho2);
                if (f != null && f.mRetainInstance) {
                    this.c.mTarget = f;
                }
            }
            this.c.mState = 0;
        }
    }
    
    public void h() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom CREATE_VIEW: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment c = this.c;
        final ViewGroup mContainer = c.mContainer;
        if (mContainer != null) {
            final View mView = c.mView;
            if (mView != null) {
                mContainer.removeView(mView);
            }
        }
        this.c.performDestroyView();
        this.a.n(this.c, false);
        final Fragment c2 = this.c;
        c2.mContainer = null;
        c2.mView = null;
        c2.mViewLifecycleOwner = null;
        c2.mViewLifecycleOwnerLiveData.n(null);
        this.c.mInLayout = false;
    }
    
    public void i() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom ATTACHED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        this.c.performDetach();
        final j a = this.a;
        final Fragment c = this.c;
        final int n = 0;
        a.e(c, false);
        final Fragment c2 = this.c;
        c2.mState = -1;
        c2.mHost = null;
        c2.mParentFragment = null;
        c2.mFragmentManager = null;
        int n2 = n;
        if (c2.mRemoving) {
            n2 = n;
            if (!c2.isInBackStack()) {
                n2 = 1;
            }
        }
        if (n2 != 0 || this.b.o().p(this.c)) {
            if (k.F0(3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("initState called for fragment: ");
                sb2.append(this.c);
                Log.d("FragmentManager", sb2.toString());
            }
            this.c.initState();
        }
    }
    
    public void j() {
        final Fragment c = this.c;
        if (c.mFromLayout && c.mInLayout && !c.mPerformedCreateView) {
            if (k.F0(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("moveto CREATE_VIEW: ");
                sb.append(this.c);
                Log.d("FragmentManager", sb.toString());
            }
            final Fragment c2 = this.c;
            c2.performCreateView(c2.performGetLayoutInflater(c2.mSavedFragmentState), null, this.c.mSavedFragmentState);
            final View mView = this.c.mView;
            if (mView != null) {
                mView.setSaveFromParentEnabled(false);
                final Fragment c3 = this.c;
                c3.mView.setTag(gb1.a, (Object)c3);
                final Fragment c4 = this.c;
                if (c4.mHidden) {
                    c4.mView.setVisibility(8);
                }
                this.c.performViewCreated();
                final j a = this.a;
                final Fragment c5 = this.c;
                a.m(c5, c5.mView, c5.mSavedFragmentState, false);
                this.c.mState = 2;
            }
        }
    }
    
    public Fragment k() {
        return this.c;
    }
    
    public final boolean l(final View view) {
        if (view == this.c.mView) {
            return true;
        }
        for (ViewParent viewParent = view.getParent(); viewParent != null; viewParent = viewParent.getParent()) {
            if (viewParent == this.c.mView) {
                return true;
            }
        }
        return false;
    }
    
    public void m() {
        if (this.d) {
            if (k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignoring re-entrant call to moveToExpectedState() for ");
                sb.append(this.k());
                Log.v("FragmentManager", sb.toString());
            }
            return;
        }
        try {
            this.d = true;
            Fragment c;
            while (true) {
                final int d = this.d();
                c = this.c;
                final int mState = c.mState;
                if (d == mState) {
                    break;
                }
                if (d > mState) {
                    switch (mState + 1) {
                        default: {
                            continue;
                        }
                        case 7: {
                            this.p();
                            continue;
                        }
                        case 6: {
                            c.mState = 6;
                            continue;
                        }
                        case 5: {
                            this.u();
                            continue;
                        }
                        case 4: {
                            if (c.mView != null) {
                                final ViewGroup mContainer = c.mContainer;
                                if (mContainer != null) {
                                    SpecialEffectsController.o(mContainer, c.getParentFragmentManager()).b(SpecialEffectsController.Operation.State.from(this.c.mView.getVisibility()), this);
                                }
                            }
                            this.c.mState = 4;
                            continue;
                        }
                        case 3: {
                            this.a();
                            continue;
                        }
                        case 2: {
                            this.j();
                            this.f();
                            continue;
                        }
                        case 1: {
                            this.e();
                            continue;
                        }
                        case 0: {
                            this.c();
                            continue;
                        }
                    }
                }
                else {
                    switch (mState - 1) {
                        default: {
                            continue;
                        }
                        case 6: {
                            this.n();
                            continue;
                        }
                        case 5: {
                            c.mState = 5;
                            continue;
                        }
                        case 4: {
                            this.v();
                            continue;
                        }
                        case 3: {
                            if (k.F0(3)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("movefrom ACTIVITY_CREATED: ");
                                sb2.append(this.c);
                                Log.d("FragmentManager", sb2.toString());
                            }
                            final Fragment c2 = this.c;
                            if (c2.mView != null && c2.mSavedViewState == null) {
                                this.s();
                            }
                            final Fragment c3 = this.c;
                            if (c3.mView != null) {
                                final ViewGroup mContainer2 = c3.mContainer;
                                if (mContainer2 != null) {
                                    SpecialEffectsController.o(mContainer2, c3.getParentFragmentManager()).d(this);
                                }
                            }
                            this.c.mState = 3;
                            continue;
                        }
                        case 2: {
                            c.mInLayout = false;
                            c.mState = 2;
                            continue;
                        }
                        case 1: {
                            this.h();
                            this.c.mState = 1;
                            continue;
                        }
                        case 0: {
                            this.g();
                            continue;
                        }
                        case -1: {
                            this.i();
                            continue;
                        }
                    }
                }
            }
            if (k.P && c.mHiddenChanged) {
                if (c.mView != null) {
                    final ViewGroup mContainer3 = c.mContainer;
                    if (mContainer3 != null) {
                        final SpecialEffectsController o = SpecialEffectsController.o(mContainer3, c.getParentFragmentManager());
                        if (this.c.mHidden) {
                            o.c(this);
                        }
                        else {
                            o.e(this);
                        }
                    }
                }
                final Fragment c4 = this.c;
                final k mFragmentManager = c4.mFragmentManager;
                if (mFragmentManager != null) {
                    mFragmentManager.D0(c4);
                }
                final Fragment c5 = this.c;
                c5.mHiddenChanged = false;
                c5.onHiddenChanged(c5.mHidden);
            }
        }
        finally {
            this.d = false;
        }
    }
    
    public void n() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom RESUMED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        this.c.performPause();
        this.a.f(this.c, false);
    }
    
    public void o(final ClassLoader classLoader) {
        final Bundle mSavedFragmentState = this.c.mSavedFragmentState;
        if (mSavedFragmentState == null) {
            return;
        }
        mSavedFragmentState.setClassLoader(classLoader);
        final Fragment c = this.c;
        c.mSavedViewState = (SparseArray<Parcelable>)c.mSavedFragmentState.getSparseParcelableArray("android:view_state");
        final Fragment c2 = this.c;
        c2.mSavedViewRegistryState = c2.mSavedFragmentState.getBundle("android:view_registry_state");
        final Fragment c3 = this.c;
        c3.mTargetWho = ((BaseBundle)c3.mSavedFragmentState).getString("android:target_state");
        final Fragment c4 = this.c;
        if (c4.mTargetWho != null) {
            c4.mTargetRequestCode = ((BaseBundle)c4.mSavedFragmentState).getInt("android:target_req_state", 0);
        }
        final Fragment c5 = this.c;
        final Boolean mSavedUserVisibleHint = c5.mSavedUserVisibleHint;
        if (mSavedUserVisibleHint != null) {
            c5.mUserVisibleHint = mSavedUserVisibleHint;
            this.c.mSavedUserVisibleHint = null;
        }
        else {
            c5.mUserVisibleHint = ((BaseBundle)c5.mSavedFragmentState).getBoolean("android:user_visible_hint", true);
        }
        final Fragment c6 = this.c;
        if (!c6.mUserVisibleHint) {
            c6.mDeferStart = true;
        }
    }
    
    public void p() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto RESUMED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        final View focusedView = this.c.getFocusedView();
        if (focusedView != null && this.l(focusedView)) {
            final boolean requestFocus = focusedView.requestFocus();
            if (k.F0(2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("requestFocus: Restoring focused view ");
                sb2.append(focusedView);
                sb2.append(" ");
                String str;
                if (requestFocus) {
                    str = "succeeded";
                }
                else {
                    str = "failed";
                }
                sb2.append(str);
                sb2.append(" on Fragment ");
                sb2.append(this.c);
                sb2.append(" resulting in focused view ");
                sb2.append(this.c.mView.findFocus());
                Log.v("FragmentManager", sb2.toString());
            }
        }
        this.c.setFocusedView(null);
        this.c.performResume();
        this.a.i(this.c, false);
        final Fragment c = this.c;
        c.mSavedFragmentState = null;
        c.mSavedViewState = null;
        c.mSavedViewRegistryState = null;
    }
    
    public final Bundle q() {
        final Bundle bundle = new Bundle();
        this.c.performSaveInstanceState(bundle);
        this.a.j(this.c, bundle, false);
        Bundle bundle2 = bundle;
        if (((BaseBundle)bundle).isEmpty()) {
            bundle2 = null;
        }
        if (this.c.mView != null) {
            this.s();
        }
        Object o = bundle2;
        if (this.c.mSavedViewState != null) {
            if ((o = bundle2) == null) {
                o = new Bundle();
            }
            ((Bundle)o).putSparseParcelableArray("android:view_state", (SparseArray)this.c.mSavedViewState);
        }
        Bundle bundle3 = (Bundle)o;
        if (this.c.mSavedViewRegistryState != null) {
            if ((bundle3 = (Bundle)o) == null) {
                bundle3 = new Bundle();
            }
            bundle3.putBundle("android:view_registry_state", this.c.mSavedViewRegistryState);
        }
        Object o2 = bundle3;
        if (!this.c.mUserVisibleHint) {
            if ((o2 = bundle3) == null) {
                o2 = new Bundle();
            }
            ((BaseBundle)o2).putBoolean("android:user_visible_hint", this.c.mUserVisibleHint);
        }
        return (Bundle)o2;
    }
    
    public n r() {
        final n n = new n(this.c);
        final Fragment c = this.c;
        if (c.mState > -1 && n.m == null) {
            final Bundle q = this.q();
            n.m = q;
            if (this.c.mTargetWho != null) {
                if (q == null) {
                    n.m = new Bundle();
                }
                ((BaseBundle)n.m).putString("android:target_state", this.c.mTargetWho);
                final int mTargetRequestCode = this.c.mTargetRequestCode;
                if (mTargetRequestCode != 0) {
                    ((BaseBundle)n.m).putInt("android:target_req_state", mTargetRequestCode);
                }
            }
        }
        else {
            n.m = c.mSavedFragmentState;
        }
        return n;
    }
    
    public void s() {
        if (this.c.mView == null) {
            return;
        }
        final SparseArray mSavedViewState = new SparseArray();
        this.c.mView.saveHierarchyState(mSavedViewState);
        if (mSavedViewState.size() > 0) {
            this.c.mSavedViewState = (SparseArray<Parcelable>)mSavedViewState;
        }
        final Bundle mSavedViewRegistryState = new Bundle();
        this.c.mViewLifecycleOwner.e(mSavedViewRegistryState);
        if (!((BaseBundle)mSavedViewRegistryState).isEmpty()) {
            this.c.mSavedViewRegistryState = mSavedViewRegistryState;
        }
    }
    
    public void t(final int e) {
        this.e = e;
    }
    
    public void u() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto STARTED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        this.c.performStart();
        this.a.k(this.c, false);
    }
    
    public void v() {
        if (k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom STARTED: ");
            sb.append(this.c);
            Log.d("FragmentManager", sb.toString());
        }
        this.c.performStop();
        this.a.l(this.c, false);
    }
}

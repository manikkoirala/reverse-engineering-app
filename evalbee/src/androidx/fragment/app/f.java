// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.animation.Transformation;
import android.view.animation.AnimationSet;
import android.animation.AnimatorInflater;
import android.content.res.Resources$NotFoundException;
import android.view.animation.AnimationUtils;
import android.content.Context;
import android.animation.Animator$AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.view.animation.Animation;
import android.view.ViewGroup;
import android.view.animation.Animation$AnimationListener;
import android.animation.Animator;
import android.view.View;

public abstract class f
{
    public static void a(final Fragment fragment, final d d, final r.g g) {
        final View mView = fragment.mView;
        final ViewGroup mContainer = fragment.mContainer;
        mContainer.startViewTransition(mView);
        final jf jf = new jf();
        jf.c((jf.b)new jf.b(fragment) {
            public final Fragment a;
            
            @Override
            public void a() {
                if (this.a.getAnimatingAway() != null) {
                    final View animatingAway = this.a.getAnimatingAway();
                    this.a.setAnimatingAway(null);
                    animatingAway.clearAnimation();
                }
                this.a.setAnimator(null);
            }
        });
        g.b(fragment, jf);
        if (d.a != null) {
            final e e = new e(d.a, mContainer, mView);
            fragment.setAnimatingAway(fragment.mView);
            ((Animation)e).setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener(mContainer, fragment, g, jf) {
                public final ViewGroup a;
                public final Fragment b;
                public final r.g c;
                public final jf d;
                
                public void onAnimationEnd(final Animation animation) {
                    ((View)this.a).post((Runnable)new Runnable(this) {
                        public final f$b a;
                        
                        @Override
                        public void run() {
                            if (this.a.b.getAnimatingAway() != null) {
                                this.a.b.setAnimatingAway(null);
                                final Animation$AnimationListener a = (Animation$AnimationListener)this.a;
                                a.c.a(a.b, a.d);
                            }
                        }
                    });
                }
                
                public void onAnimationRepeat(final Animation animation) {
                }
                
                public void onAnimationStart(final Animation animation) {
                }
            });
            fragment.mView.startAnimation((Animation)e);
        }
        else {
            final Animator b = d.b;
            fragment.setAnimator(b);
            b.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(mContainer, mView, fragment, g, jf) {
                public final ViewGroup a;
                public final View b;
                public final Fragment c;
                public final r.g d;
                public final jf e;
                
                public void onAnimationEnd(Animator animator) {
                    this.a.endViewTransition(this.b);
                    animator = this.c.getAnimator();
                    this.c.setAnimator(null);
                    if (animator != null && this.a.indexOfChild(this.b) < 0) {
                        this.d.a(this.c, this.e);
                    }
                }
            });
            b.setTarget((Object)fragment.mView);
            b.start();
        }
    }
    
    public static int b(final Fragment fragment, final boolean b, final boolean b2) {
        if (b2) {
            if (b) {
                return fragment.getPopEnterAnim();
            }
            return fragment.getPopExitAnim();
        }
        else {
            if (b) {
                return fragment.getEnterAnim();
            }
            return fragment.getExitAnim();
        }
    }
    
    public static d c(final Context context, final Fragment fragment, boolean equals, final boolean b) {
        final int nextTransition = fragment.getNextTransition();
        final int b2 = b(fragment, equals, b);
        fragment.setAnimations(0, 0, 0, 0);
        final ViewGroup mContainer = fragment.mContainer;
        if (mContainer != null) {
            final int c = gb1.c;
            if (((View)mContainer).getTag(c) != null) {
                ((View)fragment.mContainer).setTag(c, (Object)null);
            }
        }
        final ViewGroup mContainer2 = fragment.mContainer;
        if (mContainer2 != null && mContainer2.getLayoutTransition() != null) {
            return null;
        }
        final Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, equals, b2);
        if (onCreateAnimation != null) {
            return new d(onCreateAnimation);
        }
        final Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, equals, b2);
        if (onCreateAnimator != null) {
            return new d(onCreateAnimator);
        }
        int d;
        if ((d = b2) == 0) {
            d = b2;
            if (nextTransition != 0) {
                d = d(nextTransition, equals);
            }
        }
        if (d == 0) {
            goto Label_0270;
        }
        equals = "anim".equals(context.getResources().getResourceTypeName(d));
        if (!equals) {
            goto Label_0216;
        }
        try {
            final Animation loadAnimation = AnimationUtils.loadAnimation(context, d);
            if (loadAnimation != null) {
                return new d(loadAnimation);
            }
            goto Label_0216;
        }
        catch (final Resources$NotFoundException ex) {
            throw ex;
        }
        catch (final RuntimeException ex2) {
            goto Label_0216;
        }
        try {
            final Animator loadAnimator = AnimatorInflater.loadAnimator(context, d);
            if (loadAnimator != null) {
                return new d(loadAnimator);
            }
            goto Label_0270;
        }
        catch (final RuntimeException ex3) {}
    }
    
    public static int d(int n, final boolean b) {
        if (n != 4097) {
            if (n != 4099) {
                if (n != 8194) {
                    n = -1;
                }
                else if (b) {
                    n = na1.a;
                }
                else {
                    n = na1.b;
                }
            }
            else if (b) {
                n = na1.c;
            }
            else {
                n = na1.d;
            }
        }
        else if (b) {
            n = na1.e;
        }
        else {
            n = na1.f;
        }
        return n;
    }
    
    public static class d
    {
        public final Animation a;
        public final Animator b;
        
        public d(final Animator b) {
            this.a = null;
            this.b = b;
            if (b != null) {
                return;
            }
            throw new IllegalStateException("Animator cannot be null");
        }
        
        public d(final Animation a) {
            this.a = a;
            this.b = null;
            if (a != null) {
                return;
            }
            throw new IllegalStateException("Animation cannot be null");
        }
    }
    
    public static class e extends AnimationSet implements Runnable
    {
        public final ViewGroup a;
        public final View b;
        public boolean c;
        public boolean d;
        public boolean e;
        
        public e(final Animation animation, final ViewGroup a, final View b) {
            super(false);
            this.e = true;
            this.a = a;
            this.b = b;
            this.addAnimation(animation);
            ((View)a).post((Runnable)this);
        }
        
        public boolean getTransformation(final long n, final Transformation transformation) {
            this.e = true;
            if (this.c) {
                return this.d ^ true;
            }
            if (!super.getTransformation(n, transformation)) {
                this.c = true;
                s11.a((View)this.a, this);
            }
            return true;
        }
        
        public boolean getTransformation(final long n, final Transformation transformation, final float n2) {
            this.e = true;
            if (this.c) {
                return this.d ^ true;
            }
            if (!super.getTransformation(n, transformation, n2)) {
                this.c = true;
                s11.a((View)this.a, this);
            }
            return true;
        }
        
        public void run() {
            if (!this.c && this.e) {
                this.e = false;
                ((View)this.a).post((Runnable)this);
            }
            else {
                this.a.endViewTransition(this.b);
                this.d = true;
            }
        }
    }
}

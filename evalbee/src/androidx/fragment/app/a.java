// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.io.Writer;
import java.io.PrintWriter;
import androidx.lifecycle.Lifecycle;
import android.util.Log;
import java.util.ArrayList;

public final class a extends q implements m
{
    public final k t;
    public boolean u;
    public int v;
    
    public a(final k t) {
        final g q0 = t.q0();
        ClassLoader classLoader;
        if (t.t0() != null) {
            classLoader = t.t0().f().getClassLoader();
        }
        else {
            classLoader = null;
        }
        super(q0, classLoader);
        this.v = -1;
        this.t = t;
    }
    
    public static boolean C(final q.a a) {
        final Fragment b = a.b;
        return b != null && b.mAdded && b.mView != null && !b.mDetached && !b.mHidden && b.isPostponed();
    }
    
    public boolean A(final int n) {
        for (int size = super.c.size(), i = 0; i < size; ++i) {
            final Fragment b = super.c.get(i).b;
            int mContainerId;
            if (b != null) {
                mContainerId = b.mContainerId;
            }
            else {
                mContainerId = 0;
            }
            if (mContainerId != 0 && mContainerId == n) {
                return true;
            }
        }
        return false;
    }
    
    public boolean B(final ArrayList list, final int n, final int n2) {
        if (n2 == n) {
            return false;
        }
        final int size = super.c.size();
        int n3 = -1;
        int n4;
        for (int i = 0; i < size; ++i, n3 = n4) {
            final Fragment b = super.c.get(i).b;
            int mContainerId;
            if (b != null) {
                mContainerId = b.mContainerId;
            }
            else {
                mContainerId = 0;
            }
            n4 = n3;
            if (mContainerId != 0 && mContainerId != (n4 = n3)) {
                for (int j = n; j < n2; ++j) {
                    final a a = list.get(j);
                    for (int size2 = a.c.size(), k = 0; k < size2; ++k) {
                        final Fragment b2 = a.c.get(k).b;
                        int mContainerId2;
                        if (b2 != null) {
                            mContainerId2 = b2.mContainerId;
                        }
                        else {
                            mContainerId2 = 0;
                        }
                        if (mContainerId2 == mContainerId) {
                            return true;
                        }
                    }
                }
                n4 = mContainerId;
            }
        }
        return false;
    }
    
    public boolean D() {
        for (int i = 0; i < super.c.size(); ++i) {
            if (C((q.a)super.c.get(i))) {
                return true;
            }
        }
        return false;
    }
    
    public void E() {
        if (super.s != null) {
            for (int i = 0; i < super.s.size(); ++i) {
                ((Runnable)super.s.get(i)).run();
            }
            super.s = null;
        }
    }
    
    public void F(final Fragment.k onStartEnterTransitionListener) {
        for (int i = 0; i < super.c.size(); ++i) {
            final q.a a = super.c.get(i);
            if (C(a)) {
                a.b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }
    
    public Fragment G(final ArrayList list, Fragment b) {
        for (int i = super.c.size() - 1; i >= 0; --i) {
            final q.a a = super.c.get(i);
            final int a2 = a.a;
            Label_0127: {
                if (a2 != 1) {
                    if (a2 != 3) {
                        switch (a2) {
                            default: {
                                continue;
                            }
                            case 10: {
                                a.h = a.g;
                                continue;
                            }
                            case 9: {
                                b = a.b;
                                continue;
                            }
                            case 8: {
                                b = null;
                                continue;
                            }
                            case 6: {
                                break;
                            }
                            case 7: {
                                break Label_0127;
                            }
                        }
                    }
                    list.add(a.b);
                    continue;
                }
            }
            list.remove(a.b);
        }
        return b;
    }
    
    @Override
    public boolean a(final ArrayList list, final ArrayList list2) {
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Run: ");
            sb.append(this);
            Log.v("FragmentManager", sb.toString());
        }
        list.add(this);
        list2.add(Boolean.FALSE);
        if (super.i) {
            this.t.e(this);
        }
        return true;
    }
    
    @Override
    public int g() {
        return this.t(false);
    }
    
    @Override
    public int h() {
        return this.t(true);
    }
    
    @Override
    public void i() {
        this.l();
        this.t.b0((m)this, false);
    }
    
    @Override
    public void j() {
        this.l();
        this.t.b0((m)this, true);
    }
    
    @Override
    public q k(final Fragment fragment) {
        final k mFragmentManager = fragment.mFragmentManager;
        if (mFragmentManager != null && mFragmentManager != this.t) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot detach Fragment attached to a different FragmentManager. Fragment ");
            sb.append(fragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.k(fragment);
    }
    
    @Override
    public void m(final int n, final Fragment fragment, final String s, final int n2) {
        super.m(n, fragment, s, n2);
        fragment.mFragmentManager = this.t;
    }
    
    @Override
    public q n(final Fragment fragment) {
        final k mFragmentManager = fragment.mFragmentManager;
        if (mFragmentManager != null && mFragmentManager != this.t) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot remove Fragment attached to a different FragmentManager. Fragment ");
            sb.append(fragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.n(fragment);
    }
    
    @Override
    public q q(final Fragment fragment, final Lifecycle.State state) {
        if (fragment.mFragmentManager != this.t) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot setMaxLifecycle for Fragment not attached to FragmentManager ");
            sb.append(this.t);
            throw new IllegalArgumentException(sb.toString());
        }
        if (state == Lifecycle.State.INITIALIZED && fragment.mState > -1) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot set maximum Lifecycle to ");
            sb2.append(state);
            sb2.append(" after the Fragment has been created");
            throw new IllegalArgumentException(sb2.toString());
        }
        if (state != Lifecycle.State.DESTROYED) {
            return super.q(fragment, state);
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Cannot set maximum Lifecycle to ");
        sb3.append(state);
        sb3.append(". Use remove() to remove the fragment from the FragmentManager and trigger its destruction.");
        throw new IllegalArgumentException(sb3.toString());
    }
    
    public void s(final int i) {
        if (!super.i) {
            return;
        }
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bump nesting in ");
            sb.append(this);
            sb.append(" by ");
            sb.append(i);
            Log.v("FragmentManager", sb.toString());
        }
        for (int size = super.c.size(), j = 0; j < size; ++j) {
            final q.a a = super.c.get(j);
            final Fragment b = a.b;
            if (b != null) {
                b.mBackStackNesting += i;
                if (k.F0(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Bump nesting of ");
                    sb2.append(a.b);
                    sb2.append(" to ");
                    sb2.append(a.b.mBackStackNesting);
                    Log.v("FragmentManager", sb2.toString());
                }
            }
        }
    }
    
    public int t(final boolean b) {
        if (!this.u) {
            if (k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Commit: ");
                sb.append(this);
                Log.v("FragmentManager", sb.toString());
                final PrintWriter printWriter = new PrintWriter(new wl0("FragmentManager"));
                this.u("  ", printWriter);
                printWriter.close();
            }
            this.u = true;
            int j;
            if (super.i) {
                j = this.t.j();
            }
            else {
                j = -1;
            }
            this.v = j;
            this.t.Y((m)this, b);
            return this.v;
        }
        throw new IllegalStateException("commit already called");
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.v >= 0) {
            sb.append(" #");
            sb.append(this.v);
        }
        if (super.k != null) {
            sb.append(" ");
            sb.append(super.k);
        }
        sb.append("}");
        return sb.toString();
    }
    
    public void u(final String s, final PrintWriter printWriter) {
        this.v(s, printWriter, true);
    }
    
    public void v(final String s, final PrintWriter printWriter, final boolean b) {
        if (b) {
            printWriter.print(s);
            printWriter.print("mName=");
            printWriter.print(super.k);
            printWriter.print(" mIndex=");
            printWriter.print(this.v);
            printWriter.print(" mCommitted=");
            printWriter.println(this.u);
            if (super.h != 0) {
                printWriter.print(s);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(super.h));
            }
            if (super.d != 0 || super.e != 0) {
                printWriter.print(s);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(super.d));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(super.e));
            }
            if (super.f != 0 || super.g != 0) {
                printWriter.print(s);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(super.f));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(super.g));
            }
            if (super.l != 0 || super.m != null) {
                printWriter.print(s);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(super.l));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(super.m);
            }
            if (super.n != 0 || super.o != null) {
                printWriter.print(s);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(super.n));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(super.o);
            }
        }
        if (!super.c.isEmpty()) {
            printWriter.print(s);
            printWriter.println("Operations:");
            for (int size = super.c.size(), i = 0; i < size; ++i) {
                final q.a a = super.c.get(i);
                String string = null;
                switch (a.a) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("cmd=");
                        sb.append(a.a);
                        string = sb.toString();
                        break;
                    }
                    case 10: {
                        string = "OP_SET_MAX_LIFECYCLE";
                        break;
                    }
                    case 9: {
                        string = "UNSET_PRIMARY_NAV";
                        break;
                    }
                    case 8: {
                        string = "SET_PRIMARY_NAV";
                        break;
                    }
                    case 7: {
                        string = "ATTACH";
                        break;
                    }
                    case 6: {
                        string = "DETACH";
                        break;
                    }
                    case 5: {
                        string = "SHOW";
                        break;
                    }
                    case 4: {
                        string = "HIDE";
                        break;
                    }
                    case 3: {
                        string = "REMOVE";
                        break;
                    }
                    case 2: {
                        string = "REPLACE";
                        break;
                    }
                    case 1: {
                        string = "ADD";
                        break;
                    }
                    case 0: {
                        string = "NULL";
                        break;
                    }
                }
                printWriter.print(s);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(string);
                printWriter.print(" ");
                printWriter.println(a.b);
                if (b) {
                    if (a.c != 0 || a.d != 0) {
                        printWriter.print(s);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(a.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(a.d));
                    }
                    if (a.e != 0 || a.f != 0) {
                        printWriter.print(s);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(a.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(a.f));
                    }
                }
            }
        }
    }
    
    public void w() {
        for (int size = super.c.size(), i = 0; i < size; ++i) {
            final q.a a = super.c.get(i);
            final Fragment b = a.b;
            if (b != null) {
                b.setPopDirection(false);
                b.setNextTransition(super.h);
                b.setSharedElementNames(super.p, super.q);
            }
            switch (a.a) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown cmd: ");
                    sb.append(a.a);
                    throw new IllegalArgumentException(sb.toString());
                }
                case 10: {
                    this.t.l1(b, a.h);
                    break;
                }
                case 9: {
                    this.t.m1(null);
                    break;
                }
                case 8: {
                    this.t.m1(b);
                    break;
                }
                case 7: {
                    b.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.k1(b, false);
                    this.t.l(b);
                    break;
                }
                case 6: {
                    b.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.x(b);
                    break;
                }
                case 5: {
                    b.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.k1(b, false);
                    this.t.o1(b);
                    break;
                }
                case 4: {
                    b.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.C0(b);
                    break;
                }
                case 3: {
                    b.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.c1(b);
                    break;
                }
                case 1: {
                    b.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.k1(b, false);
                    this.t.g(b);
                    break;
                }
            }
            if (!super.r && a.a != 1 && b != null && !k.P) {
                this.t.P0(b);
            }
        }
        if (!super.r && !k.P) {
            final k t = this.t;
            t.Q0(t.q, true);
        }
    }
    
    public void x(final boolean b) {
        for (int i = super.c.size() - 1; i >= 0; --i) {
            final q.a a = super.c.get(i);
            final Fragment b2 = a.b;
            if (b2 != null) {
                b2.setPopDirection(true);
                b2.setNextTransition(k.h1(super.h));
                b2.setSharedElementNames(super.q, super.p);
            }
            switch (a.a) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown cmd: ");
                    sb.append(a.a);
                    throw new IllegalArgumentException(sb.toString());
                }
                case 10: {
                    this.t.l1(b2, a.g);
                    break;
                }
                case 9: {
                    this.t.m1(b2);
                    break;
                }
                case 8: {
                    this.t.m1(null);
                    break;
                }
                case 7: {
                    b2.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.k1(b2, true);
                    this.t.x(b2);
                    break;
                }
                case 6: {
                    b2.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.l(b2);
                    break;
                }
                case 5: {
                    b2.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.k1(b2, true);
                    this.t.C0(b2);
                    break;
                }
                case 4: {
                    b2.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.o1(b2);
                    break;
                }
                case 3: {
                    b2.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.g(b2);
                    break;
                }
                case 1: {
                    b2.setAnimations(a.c, a.d, a.e, a.f);
                    this.t.k1(b2, true);
                    this.t.c1(b2);
                    break;
                }
            }
            if (!super.r && a.a != 3 && b2 != null && !k.P) {
                this.t.P0(b2);
            }
        }
        if (!super.r && b && !k.P) {
            final k t = this.t;
            t.Q0(t.q, true);
        }
    }
    
    public Fragment y(final ArrayList list, Fragment b) {
        int i = 0;
        Fragment fragment = b;
        while (i < super.c.size()) {
            final q.a a = super.c.get(i);
            final int a2 = a.a;
            int n = 0;
            Label_0438: {
                if (a2 != 1) {
                    if (a2 != 2) {
                        if (a2 != 3 && a2 != 6) {
                            if (a2 != 7) {
                                if (a2 != 8) {
                                    b = fragment;
                                    n = i;
                                    break Label_0438;
                                }
                                super.c.add(i, new q.a(9, fragment));
                                n = i + 1;
                                b = a.b;
                                break Label_0438;
                            }
                        }
                        else {
                            list.remove(a.b);
                            final Fragment b2 = a.b;
                            b = fragment;
                            n = i;
                            if (b2 == fragment) {
                                super.c.add(i, new q.a(9, b2));
                                n = i + 1;
                                b = null;
                            }
                            break Label_0438;
                        }
                    }
                    else {
                        final Fragment b3 = a.b;
                        final int mContainerId = b3.mContainerId;
                        int j = list.size() - 1;
                        int n2 = 0;
                        n = i;
                        b = fragment;
                        while (j >= 0) {
                            final Fragment o = list.get(j);
                            Fragment fragment2 = b;
                            int index = n;
                            int n3 = n2;
                            if (o.mContainerId == mContainerId) {
                                if (o == b3) {
                                    n3 = 1;
                                    fragment2 = b;
                                    index = n;
                                }
                                else {
                                    fragment2 = b;
                                    index = n;
                                    if (o == b) {
                                        super.c.add(n, new q.a(9, o));
                                        index = n + 1;
                                        fragment2 = null;
                                    }
                                    final q.a element = new q.a(3, o);
                                    element.c = a.c;
                                    element.e = a.e;
                                    element.d = a.d;
                                    element.f = a.f;
                                    super.c.add(index, element);
                                    list.remove(o);
                                    ++index;
                                    n3 = n2;
                                }
                            }
                            --j;
                            b = fragment2;
                            n = index;
                            n2 = n3;
                        }
                        if (n2 != 0) {
                            super.c.remove(n);
                            --n;
                            break Label_0438;
                        }
                        a.a = 1;
                        list.add(b3);
                        break Label_0438;
                    }
                }
                list.add(a.b);
                n = i;
                b = fragment;
            }
            i = n + 1;
            fragment = b;
        }
        return fragment;
    }
    
    public String z() {
        return super.k;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

public abstract class g
{
    public static final co1 a;
    
    static {
        a = new co1();
    }
    
    public static boolean b(final ClassLoader classLoader, final String s) {
        try {
            return Fragment.class.isAssignableFrom(c(classLoader, s));
        }
        catch (final ClassNotFoundException ex) {
            return false;
        }
    }
    
    public static Class c(final ClassLoader loader, final String name) {
        final co1 a = g.a;
        co1 co1;
        if ((co1 = (co1)a.get(loader)) == null) {
            co1 = new co1();
            a.put(loader, co1);
        }
        Class<?> forName;
        if ((forName = (Class)co1.get(name)) == null) {
            forName = Class.forName(name, false, loader);
            co1.put(name, forName);
        }
        return forName;
    }
    
    public static Class d(final ClassLoader classLoader, final String s) {
        try {
            return c(classLoader, s);
        }
        catch (final ClassCastException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to instantiate fragment ");
            sb.append(s);
            sb.append(": make sure class is a valid subclass of Fragment");
            throw new Fragment.InstantiationException(sb.toString(), ex);
        }
        catch (final ClassNotFoundException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to instantiate fragment ");
            sb2.append(s);
            sb2.append(": make sure class name exists");
            throw new Fragment.InstantiationException(sb2.toString(), ex2);
        }
    }
    
    public abstract Fragment a(final ClassLoader p0, final String p1);
}

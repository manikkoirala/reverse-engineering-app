// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.content.IntentSender;
import android.os.Bundle;
import android.content.Intent;
import android.view.LayoutInflater;
import android.os.Handler;
import android.content.Context;
import android.app.Activity;

public abstract class h extends f80
{
    public final Activity a;
    public final Context b;
    public final Handler c;
    public final int d;
    public final k e;
    
    public h(final Activity a, final Context context, final Handler handler, final int d) {
        this.e = new h80();
        this.a = a;
        this.b = (Context)l71.h(context, "context == null");
        this.c = (Handler)l71.h(handler, "handler == null");
        this.d = d;
    }
    
    public h(final e e) {
        this(e, (Context)e, new Handler(), 0);
    }
    
    Activity e() {
        return this.a;
    }
    
    Context f() {
        return this.b;
    }
    
    Handler g() {
        return this.c;
    }
    
    public abstract Object h();
    
    public abstract LayoutInflater i();
    
    public void j(final Fragment fragment, final String[] array, final int n) {
    }
    
    public abstract boolean k(final Fragment p0);
    
    public abstract boolean l(final String p0);
    
    public void m(final Fragment fragment, final Intent intent, final int n, final Bundle bundle) {
        if (n == -1) {
            sl.startActivity(this.b, intent, bundle);
            return;
        }
        throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
    }
    
    public void n(final Fragment fragment, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) {
        if (n == -1) {
            h2.l(this.a, intentSender, n, intent, n2, n3, n4, bundle);
            return;
        }
        throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
    }
    
    public abstract void o();
}

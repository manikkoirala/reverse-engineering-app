// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.util.Log;
import java.util.List;
import android.view.View;
import android.view.ViewGroup;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;

public class p
{
    public final ArrayList a;
    public final HashMap b;
    public m c;
    
    public p() {
        this.a = new ArrayList();
        this.b = new HashMap();
    }
    
    public void a(final Fragment obj) {
        if (!this.a.contains(obj)) {
            synchronized (this.a) {
                this.a.add(obj);
                monitorexit(this.a);
                obj.mAdded = true;
                return;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment already added: ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString());
    }
    
    public void b() {
        this.b.values().removeAll(Collections.singleton((Object)null));
    }
    
    public boolean c(final String key) {
        return this.b.get(key) != null;
    }
    
    public void d(final int n) {
        for (final o o : this.b.values()) {
            if (o != null) {
                o.t(n);
            }
        }
    }
    
    public void e(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("    ");
        final String string = sb.toString();
        if (!this.b.isEmpty()) {
            printWriter.print(s);
            printWriter.println("Active Fragments:");
            for (final o o : this.b.values()) {
                printWriter.print(s);
                if (o != null) {
                    final Fragment k = o.k();
                    printWriter.println(k);
                    k.dump(string, fileDescriptor, printWriter, array);
                }
                else {
                    printWriter.println("null");
                }
            }
        }
        final int size = this.a.size();
        if (size > 0) {
            printWriter.print(s);
            printWriter.println("Added Fragments:");
            for (int i = 0; i < size; ++i) {
                final Fragment fragment = this.a.get(i);
                printWriter.print(s);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(fragment.toString());
            }
        }
    }
    
    public Fragment f(final String key) {
        final o o = this.b.get(key);
        if (o != null) {
            return o.k();
        }
        return null;
    }
    
    public Fragment g(final int n) {
        for (int i = this.a.size() - 1; i >= 0; --i) {
            final Fragment fragment = this.a.get(i);
            if (fragment != null && fragment.mFragmentId == n) {
                return fragment;
            }
        }
        for (final o o : this.b.values()) {
            if (o != null) {
                final Fragment k = o.k();
                if (k.mFragmentId == n) {
                    return k;
                }
                continue;
            }
        }
        return null;
    }
    
    public Fragment h(final String s) {
        if (s != null) {
            for (int i = this.a.size() - 1; i >= 0; --i) {
                final Fragment fragment = this.a.get(i);
                if (fragment != null && s.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (s != null) {
            for (final o o : this.b.values()) {
                if (o != null) {
                    final Fragment k = o.k();
                    if (s.equals(k.mTag)) {
                        return k;
                    }
                    continue;
                }
            }
        }
        return null;
    }
    
    public Fragment i(final String s) {
        for (final o o : this.b.values()) {
            if (o != null) {
                final Fragment fragmentByWho = o.k().findFragmentByWho(s);
                if (fragmentByWho != null) {
                    return fragmentByWho;
                }
                continue;
            }
        }
        return null;
    }
    
    public int j(Fragment o) {
        final ViewGroup mContainer = o.mContainer;
        if (mContainer == null) {
            return -1;
        }
        final int index = this.a.indexOf(o);
        int index2 = index - 1;
        int index3;
        while (true) {
            index3 = index;
            if (index2 < 0) {
                break;
            }
            o = (Fragment)this.a.get(index2);
            if (o.mContainer == mContainer) {
                final View mView = o.mView;
                if (mView != null) {
                    return mContainer.indexOfChild(mView) + 1;
                }
            }
            --index2;
        }
        while (++index3 < this.a.size()) {
            o = (Fragment)this.a.get(index3);
            if (o.mContainer == mContainer) {
                final View mView2 = o.mView;
                if (mView2 != null) {
                    return mContainer.indexOfChild(mView2);
                }
                continue;
            }
        }
        return -1;
    }
    
    public List k() {
        final ArrayList list = new ArrayList();
        for (final o e : this.b.values()) {
            if (e != null) {
                list.add(e);
            }
        }
        return list;
    }
    
    public List l() {
        final ArrayList list = new ArrayList();
        for (final o o : this.b.values()) {
            Fragment k;
            if (o != null) {
                k = o.k();
            }
            else {
                k = null;
            }
            list.add(k);
        }
        return list;
    }
    
    public o m(final String key) {
        return this.b.get(key);
    }
    
    public List n() {
        if (this.a.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.a) {
            return new ArrayList(this.a);
        }
    }
    
    public m o() {
        return this.c;
    }
    
    public void p(final o value) {
        final Fragment k = value.k();
        if (this.c(k.mWho)) {
            return;
        }
        this.b.put(k.mWho, value);
        if (k.mRetainInstanceChangedWhileDetached) {
            if (k.mRetainInstance) {
                this.c.f(k);
            }
            else {
                this.c.n(k);
            }
            k.mRetainInstanceChangedWhileDetached = false;
        }
        if (androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Added fragment to active set ");
            sb.append(k);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    public void q(final o o) {
        final Fragment k = o.k();
        if (k.mRetainInstance) {
            this.c.n(k);
        }
        if (this.b.put(k.mWho, null) == null) {
            return;
        }
        if (androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Removed fragment from active set ");
            sb.append(k);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    public void r() {
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            final o o = this.b.get(((Fragment)iterator.next()).mWho);
            if (o != null) {
                o.m();
            }
        }
        for (final o o2 : this.b.values()) {
            if (o2 != null) {
                o2.m();
                final Fragment k = o2.k();
                if (!k.mRemoving || k.isInBackStack()) {
                    continue;
                }
                this.q(o2);
            }
        }
    }
    
    public void s(final Fragment o) {
        synchronized (this.a) {
            this.a.remove(o);
            monitorexit(this.a);
            o.mAdded = false;
        }
    }
    
    public void t() {
        this.b.clear();
    }
    
    public void u(final List list) {
        this.a.clear();
        if (list != null) {
            for (final String s : list) {
                final Fragment f = this.f(s);
                if (f == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No instantiated fragment for (");
                    sb.append(s);
                    sb.append(")");
                    throw new IllegalStateException(sb.toString());
                }
                if (k.F0(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("restoreSaveState: added (");
                    sb2.append(s);
                    sb2.append("): ");
                    sb2.append(f);
                    Log.v("FragmentManager", sb2.toString());
                }
                this.a(f);
            }
        }
    }
    
    public ArrayList v() {
        final ArrayList list = new ArrayList(this.b.size());
        for (final o o : this.b.values()) {
            if (o != null) {
                final Fragment k = o.k();
                final n r = o.r();
                list.add(r);
                if (!androidx.fragment.app.k.F0(2)) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Saved state of ");
                sb.append(k);
                sb.append(": ");
                sb.append(r.m);
                Log.v("FragmentManager", sb.toString());
            }
        }
        return list;
    }
    
    public ArrayList w() {
        synchronized (this.a) {
            if (this.a.isEmpty()) {
                return null;
            }
            final ArrayList<String> list = new ArrayList<String>(this.a.size());
            for (final Fragment obj : this.a) {
                list.add(obj.mWho);
                if (k.F0(2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("saveAllState: adding fragment (");
                    sb.append(obj.mWho);
                    sb.append("): ");
                    sb.append(obj);
                    Log.v("FragmentManager", sb.toString());
                }
            }
            return list;
        }
    }
    
    public void x(final m c) {
        this.c = c;
    }
}

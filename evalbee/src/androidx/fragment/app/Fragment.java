// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.content.IntentSender;
import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import androidx.lifecycle.f;
import java.util.Iterator;
import android.util.AttributeSet;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.ContextMenu$ContextMenuInfo;
import android.view.ContextMenu;
import android.view.animation.Animation;
import android.view.MenuItem;
import android.content.res.Configuration;
import android.app.Activity;
import android.content.Intent;
import androidx.lifecycle.LiveData;
import androidx.savedstate.a;
import android.content.res.Resources;
import androidx.lifecycle.m;
import android.app.Application;
import android.content.ContextWrapper;
import android.animation.Animator;
import android.util.Log;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import androidx.activity.result.ActivityResultRegistry;
import java.util.concurrent.atomic.AtomicReference;
import java.lang.reflect.InvocationTargetException;
import android.content.Context;
import java.util.UUID;
import android.view.View;
import android.os.Parcelable;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.g;
import android.view.LayoutInflater;
import androidx.lifecycle.o;
import android.view.ViewGroup;
import android.os.Bundle;
import androidx.lifecycle.c;
import android.view.View$OnCreateContextMenuListener;
import android.content.ComponentCallbacks;

public abstract class Fragment implements ComponentCallbacks, View$OnCreateContextMenuListener, qj0, c42, c, aj1
{
    static final int ACTIVITY_CREATED = 4;
    static final int ATTACHED = 0;
    static final int AWAITING_ENTER_EFFECTS = 6;
    static final int AWAITING_EXIT_EFFECTS = 3;
    static final int CREATED = 1;
    static final int INITIALIZING = -1;
    static final int RESUMED = 7;
    static final int STARTED = 5;
    static final Object USE_DEFAULT_TRANSITION;
    static final int VIEW_CREATED = 2;
    boolean mAdded;
    i mAnimationInfo;
    Bundle mArguments;
    int mBackStackNesting;
    private boolean mCalled;
    androidx.fragment.app.k mChildFragmentManager;
    ViewGroup mContainer;
    int mContainerId;
    private int mContentLayoutId;
    o.b mDefaultFactory;
    boolean mDeferStart;
    boolean mDetached;
    int mFragmentId;
    androidx.fragment.app.k mFragmentManager;
    boolean mFromLayout;
    boolean mHasMenu;
    boolean mHidden;
    boolean mHiddenChanged;
    h mHost;
    boolean mInLayout;
    boolean mIsCreated;
    boolean mIsNewlyAdded;
    private Boolean mIsPrimaryNavigationFragment;
    LayoutInflater mLayoutInflater;
    g mLifecycleRegistry;
    Lifecycle.State mMaxState;
    boolean mMenuVisible;
    private final AtomicInteger mNextLocalRequestCode;
    private final ArrayList<j> mOnPreAttachedListeners;
    Fragment mParentFragment;
    boolean mPerformedCreateView;
    float mPostponedAlpha;
    Runnable mPostponedDurationRunnable;
    boolean mRemoving;
    boolean mRestored;
    boolean mRetainInstance;
    boolean mRetainInstanceChangedWhileDetached;
    Bundle mSavedFragmentState;
    zi1 mSavedStateRegistryController;
    Boolean mSavedUserVisibleHint;
    Bundle mSavedViewRegistryState;
    SparseArray<Parcelable> mSavedViewState;
    int mState;
    String mTag;
    Fragment mTarget;
    int mTargetRequestCode;
    String mTargetWho;
    boolean mUserVisibleHint;
    View mView;
    n80 mViewLifecycleOwner;
    tx0 mViewLifecycleOwnerLiveData;
    String mWho;
    
    static {
        USE_DEFAULT_TRANSITION = new Object();
    }
    
    public Fragment() {
        this.mState = -1;
        this.mWho = UUID.randomUUID().toString();
        this.mTargetWho = null;
        this.mIsPrimaryNavigationFragment = null;
        this.mChildFragmentManager = new h80();
        this.mMenuVisible = true;
        this.mUserVisibleHint = true;
        this.mPostponedDurationRunnable = new Runnable(this) {
            public final Fragment a;
            
            @Override
            public void run() {
                this.a.startPostponedEnterTransition();
            }
        };
        this.mMaxState = Lifecycle.State.RESUMED;
        this.mViewLifecycleOwnerLiveData = new tx0();
        this.mNextLocalRequestCode = new AtomicInteger();
        this.mOnPreAttachedListeners = new ArrayList<j>();
        this.c();
    }
    
    public Fragment(final int mContentLayoutId) {
        this();
        this.mContentLayoutId = mContentLayoutId;
    }
    
    @Deprecated
    public static Fragment instantiate(final Context context, final String s) {
        return instantiate(context, s, null);
    }
    
    @Deprecated
    public static Fragment instantiate(final Context context, final String s, final Bundle arguments) {
        try {
            final Fragment fragment = androidx.fragment.app.g.d(context.getClassLoader(), s).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            if (arguments != null) {
                arguments.setClassLoader(fragment.getClass().getClassLoader());
                fragment.setArguments(arguments);
            }
            return fragment;
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to instantiate fragment ");
            sb.append(s);
            sb.append(": calling Fragment constructor caused an exception");
            throw new InstantiationException(sb.toString(), ex);
        }
        catch (final NoSuchMethodException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to instantiate fragment ");
            sb2.append(s);
            sb2.append(": could not find Fragment constructor");
            throw new InstantiationException(sb2.toString(), ex2);
        }
        catch (final IllegalAccessException ex3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to instantiate fragment ");
            sb3.append(s);
            sb3.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new InstantiationException(sb3.toString(), ex3);
        }
        catch (final java.lang.InstantiationException ex4) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Unable to instantiate fragment ");
            sb4.append(s);
            sb4.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new InstantiationException(sb4.toString(), ex4);
        }
    }
    
    public final i a() {
        if (this.mAnimationInfo == null) {
            this.mAnimationInfo = new i();
        }
        return this.mAnimationInfo;
    }
    
    public final int b() {
        final Lifecycle.State mMaxState = this.mMaxState;
        if (mMaxState != Lifecycle.State.INITIALIZED && this.mParentFragment != null) {
            return Math.min(mMaxState.ordinal(), this.mParentFragment.b());
        }
        return mMaxState.ordinal();
    }
    
    public final void c() {
        this.mLifecycleRegistry = new g(this);
        this.mSavedStateRegistryController = zi1.a(this);
        this.mDefaultFactory = null;
    }
    
    public void callStartTransitionListener(final boolean b) {
        final i mAnimationInfo = this.mAnimationInfo;
        Object v = null;
        if (mAnimationInfo != null) {
            mAnimationInfo.u = false;
            v = mAnimationInfo.v;
            mAnimationInfo.v = null;
        }
        if (v != null) {
            ((k)v).b();
        }
        else if (androidx.fragment.app.k.P && this.mView != null) {
            final ViewGroup mContainer = this.mContainer;
            if (mContainer != null) {
                final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
                if (mFragmentManager != null) {
                    final SpecialEffectsController o = SpecialEffectsController.o(mContainer, mFragmentManager);
                    o.p();
                    if (b) {
                        this.mHost.g().post((Runnable)new Runnable(this, o) {
                            public final SpecialEffectsController a;
                            public final Fragment b;
                            
                            @Override
                            public void run() {
                                this.a.g();
                            }
                        });
                    }
                    else {
                        o.g();
                    }
                }
            }
        }
    }
    
    public f80 createFragmentContainer() {
        return new f80(this) {
            public final Fragment a;
            
            @Override
            public View c(final int n) {
                final View mView = this.a.mView;
                if (mView != null) {
                    return mView.findViewById(n);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(this.a);
                sb.append(" does not have a view");
                throw new IllegalStateException(sb.toString());
            }
            
            @Override
            public boolean d() {
                return this.a.mView != null;
            }
        };
    }
    
    public final r2 d(final n2 n2, final ba0 ba0, final m2 m2) {
        if (this.mState <= 1) {
            final AtomicReference atomicReference = new AtomicReference();
            this.e((j)new j(this, ba0, atomicReference, n2, m2) {
                public final ba0 a;
                public final AtomicReference b;
                public final n2 c;
                public final m2 d;
                public final Fragment e;
                
                @Override
                public void a() {
                    this.b.set(((ActivityResultRegistry)this.a.apply(null)).j(this.e.generateActivityResultKey(), this.e, this.c, this.d));
                }
            });
            return new r2(this, atomicReference, n2) {
                public final AtomicReference a;
                public final n2 b;
                public final Fragment c;
                
                @Override
                public void b(final Object o, final j2 j2) {
                    final r2 r2 = this.a.get();
                    if (r2 != null) {
                        r2.b(o, j2);
                        return;
                    }
                    throw new IllegalStateException("Operation cannot be started before fragment is in created state");
                }
                
                @Override
                public void c() {
                    final r2 r2 = this.a.getAndSet(null);
                    if (r2 != null) {
                        r2.c();
                    }
                }
            };
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" is attempting to registerForActivityResult after being created. Fragments must call registerForActivityResult() before they are created (i.e. initialization, onAttach(), or onCreate()).");
        throw new IllegalStateException(sb.toString());
    }
    
    public void dump(final String str, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.mFragmentId));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.mContainerId));
        printWriter.print(" mTag=");
        printWriter.println(this.mTag);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.mState);
        printWriter.print(" mWho=");
        printWriter.print(this.mWho);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.mBackStackNesting);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.mAdded);
        printWriter.print(" mRemoving=");
        printWriter.print(this.mRemoving);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.mFromLayout);
        printWriter.print(" mInLayout=");
        printWriter.println(this.mInLayout);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.mHidden);
        printWriter.print(" mDetached=");
        printWriter.print(this.mDetached);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.mMenuVisible);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.mHasMenu);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.mRetainInstance);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.mUserVisibleHint);
        if (this.mFragmentManager != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.mFragmentManager);
        }
        if (this.mHost != null) {
            printWriter.print(str);
            printWriter.print("mHost=");
            printWriter.println(this.mHost);
        }
        if (this.mParentFragment != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.mParentFragment);
        }
        if (this.mArguments != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.mArguments);
        }
        if (this.mSavedFragmentState != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.mSavedFragmentState);
        }
        if (this.mSavedViewState != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.mSavedViewState);
        }
        if (this.mSavedViewRegistryState != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewRegistryState=");
            printWriter.println(this.mSavedViewRegistryState);
        }
        final Fragment targetFragment = this.getTargetFragment();
        if (targetFragment != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(targetFragment);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.mTargetRequestCode);
        }
        printWriter.print(str);
        printWriter.print("mPopDirection=");
        printWriter.println(this.getPopDirection());
        if (this.getEnterAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getEnterAnim=");
            printWriter.println(this.getEnterAnim());
        }
        if (this.getExitAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getExitAnim=");
            printWriter.println(this.getExitAnim());
        }
        if (this.getPopEnterAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getPopEnterAnim=");
            printWriter.println(this.getPopEnterAnim());
        }
        if (this.getPopExitAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getPopExitAnim=");
            printWriter.println(this.getPopExitAnim());
        }
        if (this.mContainer != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.mContainer);
        }
        if (this.mView != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.mView);
        }
        if (this.getAnimatingAway() != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(this.getAnimatingAway());
        }
        if (this.getContext() != null) {
            rk0.b(this).a(str, fileDescriptor, printWriter, array);
        }
        printWriter.print(str);
        final StringBuilder sb = new StringBuilder();
        sb.append("Child ");
        sb.append(this.mChildFragmentManager);
        sb.append(":");
        printWriter.println(sb.toString());
        final androidx.fragment.app.k mChildFragmentManager = this.mChildFragmentManager;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("  ");
        mChildFragmentManager.W(sb2.toString(), fileDescriptor, printWriter, array);
    }
    
    public final void e(final j e) {
        if (this.mState >= 0) {
            e.a();
        }
        else {
            this.mOnPreAttachedListeners.add(e);
        }
    }
    
    @Override
    public final boolean equals(final Object obj) {
        return super.equals(obj);
    }
    
    public final void f() {
        if (androidx.fragment.app.k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto RESTORE_VIEW_STATE: ");
            sb.append(this);
            Log.d("FragmentManager", sb.toString());
        }
        if (this.mView != null) {
            this.restoreViewState(this.mSavedFragmentState);
        }
        this.mSavedFragmentState = null;
    }
    
    public Fragment findFragmentByWho(final String s) {
        if (s.equals(this.mWho)) {
            return this;
        }
        return this.mChildFragmentManager.j0(s);
    }
    
    public String generateActivityResultKey() {
        final StringBuilder sb = new StringBuilder();
        sb.append("fragment_");
        sb.append(this.mWho);
        sb.append("_rq#");
        sb.append(this.mNextLocalRequestCode.getAndIncrement());
        return sb.toString();
    }
    
    public final e getActivity() {
        final h mHost = this.mHost;
        e e;
        if (mHost == null) {
            e = null;
        }
        else {
            e = (e)mHost.e();
        }
        return e;
    }
    
    public boolean getAllowEnterTransitionOverlap() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null) {
            final Boolean r = mAnimationInfo.r;
            if (r != null) {
                return r;
            }
        }
        return true;
    }
    
    public boolean getAllowReturnTransitionOverlap() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null) {
            final Boolean q = mAnimationInfo.q;
            if (q != null) {
                return q;
            }
        }
        return true;
    }
    
    public View getAnimatingAway() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.a;
    }
    
    public Animator getAnimator() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.b;
    }
    
    public final Bundle getArguments() {
        return this.mArguments;
    }
    
    public final androidx.fragment.app.k getChildFragmentManager() {
        if (this.mHost != null) {
            return this.mChildFragmentManager;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" has not been attached yet.");
        throw new IllegalStateException(sb.toString());
    }
    
    public Context getContext() {
        final h mHost = this.mHost;
        Context f;
        if (mHost == null) {
            f = null;
        }
        else {
            f = mHost.f();
        }
        return f;
    }
    
    public o.b getDefaultViewModelProviderFactory() {
        if (this.mFragmentManager != null) {
            if (this.mDefaultFactory == null) {
                while (true) {
                    for (Context context = this.requireContext().getApplicationContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
                        if (context instanceof Application) {
                            final Application application = (Application)context;
                            if (application == null && androidx.fragment.app.k.F0(3)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Could not find Application instance from Context ");
                                sb.append(this.requireContext().getApplicationContext());
                                sb.append(", you will not be able to use AndroidViewModel with the default ViewModelProvider.Factory");
                                Log.d("FragmentManager", sb.toString());
                            }
                            this.mDefaultFactory = new m(application, this, this.getArguments());
                            return this.mDefaultFactory;
                        }
                    }
                    final Application application = null;
                    continue;
                }
            }
            return this.mDefaultFactory;
        }
        throw new IllegalStateException("Can't access ViewModels from detached fragment");
    }
    
    public int getEnterAnim() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.d;
    }
    
    public Object getEnterTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.k;
    }
    
    public in1 getEnterTransitionCallback() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        mAnimationInfo.getClass();
        return null;
    }
    
    public int getExitAnim() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.e;
    }
    
    public Object getExitTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.m;
    }
    
    public in1 getExitTransitionCallback() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        mAnimationInfo.getClass();
        return null;
    }
    
    public View getFocusedView() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.t;
    }
    
    @Deprecated
    public final androidx.fragment.app.k getFragmentManager() {
        return this.mFragmentManager;
    }
    
    public final Object getHost() {
        final h mHost = this.mHost;
        Object h;
        if (mHost == null) {
            h = null;
        }
        else {
            h = mHost.h();
        }
        return h;
    }
    
    public final int getId() {
        return this.mFragmentId;
    }
    
    public final LayoutInflater getLayoutInflater() {
        LayoutInflater layoutInflater;
        if ((layoutInflater = this.mLayoutInflater) == null) {
            layoutInflater = this.performGetLayoutInflater(null);
        }
        return layoutInflater;
    }
    
    @Deprecated
    public LayoutInflater getLayoutInflater(final Bundle bundle) {
        final h mHost = this.mHost;
        if (mHost != null) {
            final LayoutInflater i = mHost.i();
            wi0.a(i, this.mChildFragmentManager.u0());
            return i;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    @Deprecated
    public rk0 getLoaderManager() {
        return rk0.b(this);
    }
    
    public int getNextTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.h;
    }
    
    public final Fragment getParentFragment() {
        return this.mParentFragment;
    }
    
    public final androidx.fragment.app.k getParentFragmentManager() {
        final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            return mFragmentManager;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not associated with a fragment manager.");
        throw new IllegalStateException(sb.toString());
    }
    
    public boolean getPopDirection() {
        final i mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo != null && mAnimationInfo.c;
    }
    
    public int getPopEnterAnim() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.f;
    }
    
    public int getPopExitAnim() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.g;
    }
    
    public float getPostOnViewCreatedAlpha() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 1.0f;
        }
        return mAnimationInfo.s;
    }
    
    public Object getReenterTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        Object o;
        if ((o = mAnimationInfo.n) == Fragment.USE_DEFAULT_TRANSITION) {
            o = this.getExitTransition();
        }
        return o;
    }
    
    public final Resources getResources() {
        return this.requireContext().getResources();
    }
    
    @Deprecated
    public final boolean getRetainInstance() {
        return this.mRetainInstance;
    }
    
    public Object getReturnTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        Object o;
        if ((o = mAnimationInfo.l) == Fragment.USE_DEFAULT_TRANSITION) {
            o = this.getEnterTransition();
        }
        return o;
    }
    
    public final a getSavedStateRegistry() {
        return this.mSavedStateRegistryController.b();
    }
    
    public Object getSharedElementEnterTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.o;
    }
    
    public Object getSharedElementReturnTransition() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        Object o;
        if ((o = mAnimationInfo.p) == Fragment.USE_DEFAULT_TRANSITION) {
            o = this.getSharedElementEnterTransition();
        }
        return o;
    }
    
    public ArrayList<String> getSharedElementSourceNames() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null) {
            final ArrayList i = mAnimationInfo.i;
            if (i != null) {
                return i;
            }
        }
        return new ArrayList<String>();
    }
    
    public ArrayList<String> getSharedElementTargetNames() {
        final i mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null) {
            final ArrayList j = mAnimationInfo.j;
            if (j != null) {
                return j;
            }
        }
        return new ArrayList<String>();
    }
    
    public final String getString(final int n) {
        return this.getResources().getString(n);
    }
    
    public final String getString(final int n, final Object... array) {
        return this.getResources().getString(n, array);
    }
    
    public final String getTag() {
        return this.mTag;
    }
    
    @Deprecated
    public final Fragment getTargetFragment() {
        final Fragment mTarget = this.mTarget;
        if (mTarget != null) {
            return mTarget;
        }
        final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            final String mTargetWho = this.mTargetWho;
            if (mTargetWho != null) {
                return mFragmentManager.g0(mTargetWho);
            }
        }
        return null;
    }
    
    @Deprecated
    public final int getTargetRequestCode() {
        return this.mTargetRequestCode;
    }
    
    public final CharSequence getText(final int n) {
        return this.getResources().getText(n);
    }
    
    @Deprecated
    public boolean getUserVisibleHint() {
        return this.mUserVisibleHint;
    }
    
    public View getView() {
        return this.mView;
    }
    
    public qj0 getViewLifecycleOwner() {
        final n80 mViewLifecycleOwner = this.mViewLifecycleOwner;
        if (mViewLifecycleOwner != null) {
            return mViewLifecycleOwner;
        }
        throw new IllegalStateException("Can't access the Fragment View's LifecycleOwner when getView() is null i.e., before onCreateView() or after onDestroyView()");
    }
    
    public LiveData getViewLifecycleOwnerLiveData() {
        return this.mViewLifecycleOwnerLiveData;
    }
    
    public b42 getViewModelStore() {
        if (this.mFragmentManager == null) {
            throw new IllegalStateException("Can't access ViewModels from detached fragment");
        }
        if (this.b() != Lifecycle.State.INITIALIZED.ordinal()) {
            return this.mFragmentManager.A0(this);
        }
        throw new IllegalStateException("Calling getViewModelStore() before a Fragment reaches onCreate() when using setMaxLifecycle(INITIALIZED) is not supported");
    }
    
    public final boolean hasOptionsMenu() {
        return this.mHasMenu;
    }
    
    @Override
    public final int hashCode() {
        return super.hashCode();
    }
    
    public void initState() {
        this.c();
        this.mWho = UUID.randomUUID().toString();
        this.mAdded = false;
        this.mRemoving = false;
        this.mFromLayout = false;
        this.mInLayout = false;
        this.mRestored = false;
        this.mBackStackNesting = 0;
        this.mFragmentManager = null;
        this.mChildFragmentManager = new h80();
        this.mHost = null;
        this.mFragmentId = 0;
        this.mContainerId = 0;
        this.mTag = null;
        this.mHidden = false;
        this.mDetached = false;
    }
    
    public final boolean isAdded() {
        return this.mHost != null && this.mAdded;
    }
    
    public final boolean isDetached() {
        return this.mDetached;
    }
    
    public final boolean isHidden() {
        return this.mHidden;
    }
    
    public boolean isHideReplaced() {
        final i mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo != null && mAnimationInfo.w;
    }
    
    public final boolean isInBackStack() {
        return this.mBackStackNesting > 0;
    }
    
    public final boolean isInLayout() {
        return this.mInLayout;
    }
    
    public final boolean isMenuVisible() {
        if (this.mMenuVisible) {
            final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
            if (mFragmentManager == null || mFragmentManager.H0(this.mParentFragment)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isPostponed() {
        final i mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo != null && mAnimationInfo.u;
    }
    
    public final boolean isRemoving() {
        return this.mRemoving;
    }
    
    public final boolean isRemovingParent() {
        final Fragment parentFragment = this.getParentFragment();
        return parentFragment != null && (parentFragment.isRemoving() || parentFragment.isRemovingParent());
    }
    
    public final boolean isResumed() {
        return this.mState >= 7;
    }
    
    public final boolean isStateSaved() {
        final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
        return mFragmentManager != null && mFragmentManager.K0();
    }
    
    public final boolean isVisible() {
        if (this.isAdded() && !this.isHidden()) {
            final View mView = this.mView;
            if (mView != null && mView.getWindowToken() != null && this.mView.getVisibility() == 0) {
                return true;
            }
        }
        return false;
    }
    
    public void noteStateNotSaved() {
        this.mChildFragmentManager.T0();
    }
    
    @Deprecated
    public void onActivityCreated(final Bundle bundle) {
        this.mCalled = true;
    }
    
    @Deprecated
    public void onActivityResult(final int i, final int j, final Intent obj) {
        if (androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(this);
            sb.append(" received the following in onActivityResult(): requestCode: ");
            sb.append(i);
            sb.append(" resultCode: ");
            sb.append(j);
            sb.append(" data: ");
            sb.append(obj);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    @Deprecated
    public void onAttach(final Activity activity) {
        this.mCalled = true;
    }
    
    public void onAttach(final Context context) {
        this.mCalled = true;
        final h mHost = this.mHost;
        Activity e;
        if (mHost == null) {
            e = null;
        }
        else {
            e = mHost.e();
        }
        if (e != null) {
            this.mCalled = false;
            this.onAttach(e);
        }
    }
    
    @Deprecated
    public void onAttachFragment(final Fragment fragment) {
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        this.mCalled = true;
    }
    
    public boolean onContextItemSelected(final MenuItem menuItem) {
        return false;
    }
    
    public void onCreate(final Bundle bundle) {
        this.mCalled = true;
        this.restoreChildFragmentState(bundle);
        if (!this.mChildFragmentManager.J0(1)) {
            this.mChildFragmentManager.C();
        }
    }
    
    public Animation onCreateAnimation(final int n, final boolean b, final int n2) {
        return null;
    }
    
    public Animator onCreateAnimator(final int n, final boolean b, final int n2) {
        return null;
    }
    
    public void onCreateContextMenu(final ContextMenu contextMenu, final View view, final ContextMenu$ContextMenuInfo contextMenu$ContextMenuInfo) {
        this.requireActivity().onCreateContextMenu(contextMenu, view, contextMenu$ContextMenuInfo);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final int mContentLayoutId = this.mContentLayoutId;
        if (mContentLayoutId != 0) {
            return layoutInflater.inflate(mContentLayoutId, viewGroup, false);
        }
        return null;
    }
    
    public void onDestroy() {
        this.mCalled = true;
    }
    
    public void onDestroyOptionsMenu() {
    }
    
    public void onDestroyView() {
        this.mCalled = true;
    }
    
    public void onDetach() {
        this.mCalled = true;
    }
    
    public LayoutInflater onGetLayoutInflater(final Bundle bundle) {
        return this.getLayoutInflater(bundle);
    }
    
    public void onHiddenChanged(final boolean b) {
    }
    
    @Deprecated
    public void onInflate(final Activity activity, final AttributeSet set, final Bundle bundle) {
        this.mCalled = true;
    }
    
    public void onInflate(final Context context, final AttributeSet set, final Bundle bundle) {
        this.mCalled = true;
        final h mHost = this.mHost;
        Activity e;
        if (mHost == null) {
            e = null;
        }
        else {
            e = mHost.e();
        }
        if (e != null) {
            this.mCalled = false;
            this.onInflate(e, set, bundle);
        }
    }
    
    public void onLowMemory() {
        this.mCalled = true;
    }
    
    public void onMultiWindowModeChanged(final boolean b) {
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        return false;
    }
    
    public void onOptionsMenuClosed(final Menu menu) {
    }
    
    public void onPause() {
        this.mCalled = true;
    }
    
    public void onPictureInPictureModeChanged(final boolean b) {
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
    }
    
    public void onPrimaryNavigationFragmentChanged(final boolean b) {
    }
    
    @Deprecated
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
    }
    
    public void onResume() {
        this.mCalled = true;
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
    }
    
    public void onStart() {
        this.mCalled = true;
    }
    
    public void onStop() {
        this.mCalled = true;
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
    }
    
    public void onViewStateRestored(final Bundle bundle) {
        this.mCalled = true;
    }
    
    public void performActivityCreated(final Bundle bundle) {
        this.mChildFragmentManager.T0();
        this.mState = 3;
        this.mCalled = false;
        this.onActivityCreated(bundle);
        if (this.mCalled) {
            this.f();
            this.mChildFragmentManager.y();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onActivityCreated()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performAttach() {
        final Iterator<j> iterator = this.mOnPreAttachedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().a();
        }
        this.mOnPreAttachedListeners.clear();
        this.mChildFragmentManager.k(this.mHost, this.createFragmentContainer(), this);
        this.mState = 0;
        this.mCalled = false;
        this.onAttach(this.mHost.f());
        if (this.mCalled) {
            this.mFragmentManager.I(this);
            this.mChildFragmentManager.z();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onAttach()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performConfigurationChanged(final Configuration configuration) {
        this.onConfigurationChanged(configuration);
        this.mChildFragmentManager.A(configuration);
    }
    
    public boolean performContextItemSelected(final MenuItem menuItem) {
        return !this.mHidden && (this.onContextItemSelected(menuItem) || this.mChildFragmentManager.B(menuItem));
    }
    
    public void performCreate(final Bundle bundle) {
        this.mChildFragmentManager.T0();
        this.mState = 1;
        this.mCalled = false;
        this.mLifecycleRegistry.a(new f(this) {
            public final Fragment a;
            
            @Override
            public void d(final qj0 qj0, final Lifecycle.Event event) {
                if (event == Lifecycle.Event.ON_STOP) {
                    final View mView = this.a.mView;
                    if (mView != null) {
                        mView.cancelPendingInputEvents();
                    }
                }
            }
        });
        this.mSavedStateRegistryController.d(bundle);
        this.onCreate(bundle);
        this.mIsCreated = true;
        if (this.mCalled) {
            this.mLifecycleRegistry.h(Lifecycle.Event.ON_CREATE);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onCreate()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public boolean performCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final boolean mHidden = this.mHidden;
        boolean b = false;
        final boolean b2 = false;
        if (!mHidden) {
            boolean b3 = b2;
            if (this.mHasMenu) {
                b3 = b2;
                if (this.mMenuVisible) {
                    this.onCreateOptionsMenu(menu, menuInflater);
                    b3 = true;
                }
            }
            b = (b3 | this.mChildFragmentManager.D(menu, menuInflater));
        }
        return b;
    }
    
    public void performCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mChildFragmentManager.T0();
        this.mPerformedCreateView = true;
        this.mViewLifecycleOwner = new n80(this, this.getViewModelStore());
        final View onCreateView = this.onCreateView(layoutInflater, viewGroup, bundle);
        this.mView = onCreateView;
        if (onCreateView != null) {
            this.mViewLifecycleOwner.b();
            o42.a(this.mView, this.mViewLifecycleOwner);
            r42.a(this.mView, this.mViewLifecycleOwner);
            q42.a(this.mView, this.mViewLifecycleOwner);
            this.mViewLifecycleOwnerLiveData.n(this.mViewLifecycleOwner);
        }
        else {
            if (this.mViewLifecycleOwner.c()) {
                throw new IllegalStateException("Called getViewLifecycleOwner() but onCreateView() returned null");
            }
            this.mViewLifecycleOwner = null;
        }
    }
    
    public void performDestroy() {
        this.mChildFragmentManager.E();
        this.mLifecycleRegistry.h(Lifecycle.Event.ON_DESTROY);
        this.mState = 0;
        this.mCalled = false;
        this.mIsCreated = false;
        this.onDestroy();
        if (this.mCalled) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onDestroy()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performDestroyView() {
        this.mChildFragmentManager.F();
        if (this.mView != null && this.mViewLifecycleOwner.getLifecycle().b().isAtLeast(Lifecycle.State.CREATED)) {
            this.mViewLifecycleOwner.a(Lifecycle.Event.ON_DESTROY);
        }
        this.mState = 1;
        this.mCalled = false;
        this.onDestroyView();
        if (this.mCalled) {
            rk0.b(this).d();
            this.mPerformedCreateView = false;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onDestroyView()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performDetach() {
        this.mState = -1;
        this.mCalled = false;
        this.onDetach();
        this.mLayoutInflater = null;
        if (this.mCalled) {
            if (!this.mChildFragmentManager.E0()) {
                this.mChildFragmentManager.E();
                this.mChildFragmentManager = new h80();
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onDetach()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public LayoutInflater performGetLayoutInflater(final Bundle bundle) {
        return this.mLayoutInflater = this.onGetLayoutInflater(bundle);
    }
    
    public void performLowMemory() {
        this.onLowMemory();
        this.mChildFragmentManager.G();
    }
    
    public void performMultiWindowModeChanged(final boolean b) {
        this.onMultiWindowModeChanged(b);
        this.mChildFragmentManager.H(b);
    }
    
    public boolean performOptionsItemSelected(final MenuItem menuItem) {
        return !this.mHidden && ((this.mHasMenu && this.mMenuVisible && this.onOptionsItemSelected(menuItem)) || this.mChildFragmentManager.J(menuItem));
    }
    
    public void performOptionsMenuClosed(final Menu menu) {
        if (!this.mHidden) {
            if (this.mHasMenu && this.mMenuVisible) {
                this.onOptionsMenuClosed(menu);
            }
            this.mChildFragmentManager.K(menu);
        }
    }
    
    public void performPause() {
        this.mChildFragmentManager.M();
        if (this.mView != null) {
            this.mViewLifecycleOwner.a(Lifecycle.Event.ON_PAUSE);
        }
        this.mLifecycleRegistry.h(Lifecycle.Event.ON_PAUSE);
        this.mState = 6;
        this.mCalled = false;
        this.onPause();
        if (this.mCalled) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onPause()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performPictureInPictureModeChanged(final boolean b) {
        this.onPictureInPictureModeChanged(b);
        this.mChildFragmentManager.N(b);
    }
    
    public boolean performPrepareOptionsMenu(final Menu menu) {
        final boolean mHidden = this.mHidden;
        boolean b = false;
        final boolean b2 = false;
        if (!mHidden) {
            boolean b3 = b2;
            if (this.mHasMenu) {
                b3 = b2;
                if (this.mMenuVisible) {
                    this.onPrepareOptionsMenu(menu);
                    b3 = true;
                }
            }
            b = (b3 | this.mChildFragmentManager.O(menu));
        }
        return b;
    }
    
    public void performPrimaryNavigationFragmentChanged() {
        final boolean i0 = this.mFragmentManager.I0(this);
        final Boolean mIsPrimaryNavigationFragment = this.mIsPrimaryNavigationFragment;
        if (mIsPrimaryNavigationFragment == null || mIsPrimaryNavigationFragment != i0) {
            this.mIsPrimaryNavigationFragment = i0;
            this.onPrimaryNavigationFragmentChanged(i0);
            this.mChildFragmentManager.P();
        }
    }
    
    public void performResume() {
        this.mChildFragmentManager.T0();
        this.mChildFragmentManager.a0(true);
        this.mState = 7;
        this.mCalled = false;
        this.onResume();
        if (this.mCalled) {
            final g mLifecycleRegistry = this.mLifecycleRegistry;
            final Lifecycle.Event on_RESUME = Lifecycle.Event.ON_RESUME;
            mLifecycleRegistry.h(on_RESUME);
            if (this.mView != null) {
                this.mViewLifecycleOwner.a(on_RESUME);
            }
            this.mChildFragmentManager.Q();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onResume()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performSaveInstanceState(final Bundle bundle) {
        this.onSaveInstanceState(bundle);
        this.mSavedStateRegistryController.e(bundle);
        final Parcelable i1 = this.mChildFragmentManager.i1();
        if (i1 != null) {
            bundle.putParcelable("android:support:fragments", i1);
        }
    }
    
    public void performStart() {
        this.mChildFragmentManager.T0();
        this.mChildFragmentManager.a0(true);
        this.mState = 5;
        this.mCalled = false;
        this.onStart();
        if (this.mCalled) {
            final g mLifecycleRegistry = this.mLifecycleRegistry;
            final Lifecycle.Event on_START = Lifecycle.Event.ON_START;
            mLifecycleRegistry.h(on_START);
            if (this.mView != null) {
                this.mViewLifecycleOwner.a(on_START);
            }
            this.mChildFragmentManager.R();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onStart()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performStop() {
        this.mChildFragmentManager.T();
        if (this.mView != null) {
            this.mViewLifecycleOwner.a(Lifecycle.Event.ON_STOP);
        }
        this.mLifecycleRegistry.h(Lifecycle.Event.ON_STOP);
        this.mState = 4;
        this.mCalled = false;
        this.onStop();
        if (this.mCalled) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onStop()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void performViewCreated() {
        this.onViewCreated(this.mView, this.mSavedFragmentState);
        this.mChildFragmentManager.U();
    }
    
    public void postponeEnterTransition() {
        this.a().u = true;
    }
    
    public final void postponeEnterTransition(final long duration, final TimeUnit timeUnit) {
        this.a().u = true;
        final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
        Handler g;
        if (mFragmentManager != null) {
            g = mFragmentManager.t0().g();
        }
        else {
            g = new Handler(Looper.getMainLooper());
        }
        g.removeCallbacks(this.mPostponedDurationRunnable);
        g.postDelayed(this.mPostponedDurationRunnable, timeUnit.toMillis(duration));
    }
    
    public final <I, O> r2 registerForActivityResult(final n2 n2, final ActivityResultRegistry activityResultRegistry, final m2 m2) {
        return this.d(n2, new ba0(this, activityResultRegistry) {
            public final ActivityResultRegistry a;
            public final Fragment b;
            
            public ActivityResultRegistry a(final Void void1) {
                return this.a;
            }
        }, m2);
    }
    
    public final <I, O> r2 registerForActivityResult(final n2 n2, final m2 m2) {
        return this.d(n2, new ba0(this) {
            public final Fragment a;
            
            public ActivityResultRegistry a(final Void void1) {
                final Fragment a = this.a;
                final h mHost = a.mHost;
                if (mHost instanceof s2) {
                    return ((s2)mHost).getActivityResultRegistry();
                }
                return a.requireActivity().getActivityResultRegistry();
            }
        }, m2);
    }
    
    public void registerForContextMenu(final View view) {
        view.setOnCreateContextMenuListener((View$OnCreateContextMenuListener)this);
    }
    
    @Deprecated
    public final void requestPermissions(final String[] array, final int n) {
        if (this.mHost != null) {
            this.getParentFragmentManager().L0(this, array, n);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }
    
    public final e requireActivity() {
        final e activity = this.getActivity();
        if (activity != null) {
            return activity;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to an activity.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Bundle requireArguments() {
        final Bundle arguments = this.getArguments();
        if (arguments != null) {
            return arguments;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" does not have any arguments.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Context requireContext() {
        final Context context = this.getContext();
        if (context != null) {
            return context;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to a context.");
        throw new IllegalStateException(sb.toString());
    }
    
    @Deprecated
    public final androidx.fragment.app.k requireFragmentManager() {
        return this.getParentFragmentManager();
    }
    
    public final Object requireHost() {
        final Object host = this.getHost();
        if (host != null) {
            return host;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to a host.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Fragment requireParentFragment() {
        final Fragment parentFragment = this.getParentFragment();
        if (parentFragment != null) {
            return parentFragment;
        }
        if (this.getContext() == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(this);
            sb.append(" is not attached to any Fragment or host");
            throw new IllegalStateException(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Fragment ");
        sb2.append(this);
        sb2.append(" is not a child Fragment, it is directly attached to ");
        sb2.append(this.getContext());
        throw new IllegalStateException(sb2.toString());
    }
    
    public final View requireView() {
        final View view = this.getView();
        if (view != null) {
            return view;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not return a View from onCreateView() or this was called before onCreateView().");
        throw new IllegalStateException(sb.toString());
    }
    
    public void restoreChildFragmentState(final Bundle bundle) {
        if (bundle != null) {
            final Parcelable parcelable = bundle.getParcelable("android:support:fragments");
            if (parcelable != null) {
                this.mChildFragmentManager.g1(parcelable);
                this.mChildFragmentManager.C();
            }
        }
    }
    
    public final void restoreViewState(final Bundle bundle) {
        final SparseArray<Parcelable> mSavedViewState = this.mSavedViewState;
        if (mSavedViewState != null) {
            this.mView.restoreHierarchyState((SparseArray)mSavedViewState);
            this.mSavedViewState = null;
        }
        if (this.mView != null) {
            this.mViewLifecycleOwner.d(this.mSavedViewRegistryState);
            this.mSavedViewRegistryState = null;
        }
        this.mCalled = false;
        this.onViewStateRestored(bundle);
        if (this.mCalled) {
            if (this.mView != null) {
                this.mViewLifecycleOwner.a(Lifecycle.Event.ON_CREATE);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onViewStateRestored()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void setAllowEnterTransitionOverlap(final boolean b) {
        this.a().r = b;
    }
    
    public void setAllowReturnTransitionOverlap(final boolean b) {
        this.a().q = b;
    }
    
    public void setAnimatingAway(final View a) {
        this.a().a = a;
    }
    
    public void setAnimations(final int d, final int e, final int f, final int g) {
        if (this.mAnimationInfo == null && d == 0 && e == 0 && f == 0 && g == 0) {
            return;
        }
        this.a().d = d;
        this.a().e = e;
        this.a().f = f;
        this.a().g = g;
    }
    
    public void setAnimator(final Animator b) {
        this.a().b = b;
    }
    
    public void setArguments(final Bundle mArguments) {
        if (this.mFragmentManager != null && this.isStateSaved()) {
            throw new IllegalStateException("Fragment already added and state has been saved");
        }
        this.mArguments = mArguments;
    }
    
    public void setEnterSharedElementCallback(final in1 in1) {
        this.a().getClass();
    }
    
    public void setEnterTransition(final Object k) {
        this.a().k = k;
    }
    
    public void setExitSharedElementCallback(final in1 in1) {
        this.a().getClass();
    }
    
    public void setExitTransition(final Object m) {
        this.a().m = m;
    }
    
    public void setFocusedView(final View t) {
        this.a().t = t;
    }
    
    public void setHasOptionsMenu(final boolean mHasMenu) {
        if (this.mHasMenu != mHasMenu) {
            this.mHasMenu = mHasMenu;
            if (this.isAdded() && !this.isHidden()) {
                this.mHost.o();
            }
        }
    }
    
    public void setHideReplaced(final boolean w) {
        this.a().w = w;
    }
    
    public void setInitialSavedState(final l l) {
        if (this.mFragmentManager == null) {
            Bundle a = null;
            Label_0025: {
                if (l != null) {
                    a = l.a;
                    if (a != null) {
                        break Label_0025;
                    }
                }
                a = null;
            }
            this.mSavedFragmentState = a;
            return;
        }
        throw new IllegalStateException("Fragment already added");
    }
    
    public void setMenuVisibility(final boolean mMenuVisible) {
        if (this.mMenuVisible != mMenuVisible) {
            this.mMenuVisible = mMenuVisible;
            if (this.mHasMenu && this.isAdded() && !this.isHidden()) {
                this.mHost.o();
            }
        }
    }
    
    public void setNextTransition(final int h) {
        if (this.mAnimationInfo == null && h == 0) {
            return;
        }
        this.a();
        this.mAnimationInfo.h = h;
    }
    
    public void setOnStartEnterTransitionListener(final k v) {
        this.a();
        final i mAnimationInfo = this.mAnimationInfo;
        final k v2 = mAnimationInfo.v;
        if (v == v2) {
            return;
        }
        if (v != null && v2 != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Trying to set a replacement startPostponedEnterTransition on ");
            sb.append(this);
            throw new IllegalStateException(sb.toString());
        }
        if (mAnimationInfo.u) {
            mAnimationInfo.v = v;
        }
        if (v != null) {
            v.a();
        }
    }
    
    public void setPopDirection(final boolean c) {
        if (this.mAnimationInfo == null) {
            return;
        }
        this.a().c = c;
    }
    
    public void setPostOnViewCreatedAlpha(final float s) {
        this.a().s = s;
    }
    
    public void setReenterTransition(final Object n) {
        this.a().n = n;
    }
    
    @Deprecated
    public void setRetainInstance(final boolean mRetainInstance) {
        this.mRetainInstance = mRetainInstance;
        final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            if (mRetainInstance) {
                mFragmentManager.i(this);
            }
            else {
                mFragmentManager.e1(this);
            }
        }
        else {
            this.mRetainInstanceChangedWhileDetached = true;
        }
    }
    
    public void setReturnTransition(final Object l) {
        this.a().l = l;
    }
    
    public void setSharedElementEnterTransition(final Object o) {
        this.a().o = o;
    }
    
    public void setSharedElementNames(final ArrayList<String> i, final ArrayList<String> j) {
        this.a();
        final i mAnimationInfo = this.mAnimationInfo;
        mAnimationInfo.i = i;
        mAnimationInfo.j = j;
    }
    
    public void setSharedElementReturnTransition(final Object p) {
        this.a().p = p;
    }
    
    @Deprecated
    public void setTargetFragment(final Fragment mTarget, final int mTargetRequestCode) {
        final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
        androidx.fragment.app.k mFragmentManager2;
        if (mTarget != null) {
            mFragmentManager2 = mTarget.mFragmentManager;
        }
        else {
            mFragmentManager2 = null;
        }
        if (mFragmentManager != null && mFragmentManager2 != null && mFragmentManager != mFragmentManager2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(mTarget);
            sb.append(" must share the same FragmentManager to be set as a target fragment");
            throw new IllegalArgumentException(sb.toString());
        }
        for (Fragment targetFragment = mTarget; targetFragment != null; targetFragment = targetFragment.getTargetFragment()) {
            if (targetFragment.equals(this)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Setting ");
                sb2.append(mTarget);
                sb2.append(" as the target of ");
                sb2.append(this);
                sb2.append(" would create a target cycle");
                throw new IllegalArgumentException(sb2.toString());
            }
        }
        Label_0210: {
            if (mTarget == null) {
                this.mTargetWho = null;
            }
            else {
                if (this.mFragmentManager == null || mTarget.mFragmentManager == null) {
                    this.mTargetWho = null;
                    this.mTarget = mTarget;
                    break Label_0210;
                }
                this.mTargetWho = mTarget.mWho;
            }
            this.mTarget = null;
        }
        this.mTargetRequestCode = mTargetRequestCode;
    }
    
    @Deprecated
    public void setUserVisibleHint(final boolean b) {
        if (!this.mUserVisibleHint && b && this.mState < 5 && this.mFragmentManager != null && this.isAdded() && this.mIsCreated) {
            final androidx.fragment.app.k mFragmentManager = this.mFragmentManager;
            mFragmentManager.V0(mFragmentManager.v(this));
        }
        this.mUserVisibleHint = b;
        this.mDeferStart = (this.mState < 5 && !b);
        if (this.mSavedFragmentState != null) {
            this.mSavedUserVisibleHint = b;
        }
    }
    
    public boolean shouldShowRequestPermissionRationale(final String s) {
        final h mHost = this.mHost;
        return mHost != null && mHost.l(s);
    }
    
    public void startActivity(final Intent intent) {
        this.startActivity(intent, null);
    }
    
    public void startActivity(final Intent intent, final Bundle bundle) {
        final h mHost = this.mHost;
        if (mHost != null) {
            mHost.m(this, intent, -1, bundle);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n) {
        this.startActivityForResult(intent, n, null);
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n, final Bundle bundle) {
        if (this.mHost != null) {
            this.getParentFragmentManager().M0(this, intent, n, bundle);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }
    
    @Deprecated
    public void startIntentSenderForResult(final IntentSender obj, final int i, final Intent obj2, final int n, final int n2, final int n3, final Bundle obj3) {
        if (this.mHost != null) {
            if (androidx.fragment.app.k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(this);
                sb.append(" received the following in startIntentSenderForResult() requestCode: ");
                sb.append(i);
                sb.append(" IntentSender: ");
                sb.append(obj);
                sb.append(" fillInIntent: ");
                sb.append(obj2);
                sb.append(" options: ");
                sb.append(obj3);
                Log.v("FragmentManager", sb.toString());
            }
            this.getParentFragmentManager().N0(this, obj, i, obj2, n, n2, n3, obj3);
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Fragment ");
        sb2.append(this);
        sb2.append(" not attached to Activity");
        throw new IllegalStateException(sb2.toString());
    }
    
    public void startPostponedEnterTransition() {
        if (this.mAnimationInfo != null) {
            if (this.a().u) {
                if (this.mHost == null) {
                    this.a().u = false;
                }
                else if (Looper.myLooper() != this.mHost.g().getLooper()) {
                    this.mHost.g().postAtFrontOfQueue((Runnable)new Runnable(this) {
                        public final Fragment a;
                        
                        @Override
                        public void run() {
                            this.a.callStartTransitionListener(false);
                        }
                    });
                }
                else {
                    this.callStartTransitionListener(true);
                }
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append(this.getClass().getSimpleName());
        sb.append("{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("}");
        sb.append(" (");
        sb.append(this.mWho);
        if (this.mFragmentId != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.mFragmentId));
        }
        if (this.mTag != null) {
            sb.append(" tag=");
            sb.append(this.mTag);
        }
        sb.append(")");
        return sb.toString();
    }
    
    public void unregisterForContextMenu(final View view) {
        view.setOnCreateContextMenuListener((View$OnCreateContextMenuListener)null);
    }
    
    public static class InstantiationException extends RuntimeException
    {
        public InstantiationException(final String message, final Exception cause) {
            super(message, cause);
        }
    }
    
    public static class i
    {
        public View a;
        public Animator b;
        public boolean c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h;
        public ArrayList i;
        public ArrayList j;
        public Object k;
        public Object l;
        public Object m;
        public Object n;
        public Object o;
        public Object p;
        public Boolean q;
        public Boolean r;
        public float s;
        public View t;
        public boolean u;
        public k v;
        public boolean w;
        
        public i() {
            this.k = null;
            final Object use_DEFAULT_TRANSITION = Fragment.USE_DEFAULT_TRANSITION;
            this.l = use_DEFAULT_TRANSITION;
            this.m = null;
            this.n = use_DEFAULT_TRANSITION;
            this.o = null;
            this.p = use_DEFAULT_TRANSITION;
            this.s = 1.0f;
            this.t = null;
        }
    }
    
    public abstract static class j
    {
        public abstract void a();
    }
    
    public interface k
    {
        void a();
        
        void b();
    }
    
    public static class l implements Parcelable
    {
        public static final Parcelable$Creator<l> CREATOR;
        public final Bundle a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator() {
                public l a(final Parcel parcel) {
                    return new l(parcel, null);
                }
                
                public l b(final Parcel parcel, final ClassLoader classLoader) {
                    return new l(parcel, classLoader);
                }
                
                public l[] c(final int n) {
                    return new l[n];
                }
            };
        }
        
        public l(final Parcel parcel, final ClassLoader classLoader) {
            final Bundle bundle = parcel.readBundle();
            this.a = bundle;
            if (classLoader != null && bundle != null) {
                bundle.setClassLoader(classLoader);
            }
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeBundle(this.a);
        }
    }
}

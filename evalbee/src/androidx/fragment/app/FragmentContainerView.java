// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.animation.LayoutTransition;
import android.graphics.Canvas;
import android.view.WindowInsets;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View$OnApplyWindowInsetsListener;
import java.util.ArrayList;
import android.widget.FrameLayout;

public final class FragmentContainerView extends FrameLayout
{
    public ArrayList a;
    public ArrayList b;
    public View$OnApplyWindowInsetsListener c;
    public boolean d;
    
    public FragmentContainerView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FragmentContainerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.d = true;
        if (set != null) {
            final String classAttribute = set.getClassAttribute();
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, yb1.h);
            String string;
            String str;
            if (classAttribute == null) {
                string = obtainStyledAttributes.getString(yb1.i);
                str = "android:name";
            }
            else {
                str = "class";
                string = classAttribute;
            }
            obtainStyledAttributes.recycle();
            if (string != null) {
                if (!((View)this).isInEditMode()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("FragmentContainerView must be within a FragmentActivity to use ");
                    sb.append(str);
                    sb.append("=\"");
                    sb.append(string);
                    sb.append("\"");
                    throw new UnsupportedOperationException(sb.toString());
                }
            }
        }
    }
    
    public FragmentContainerView(final Context context, final AttributeSet set, final k k) {
        super(context, set);
        this.d = true;
        final String classAttribute = set.getClassAttribute();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, yb1.h);
        String string = classAttribute;
        if (classAttribute == null) {
            string = obtainStyledAttributes.getString(yb1.i);
        }
        final String string2 = obtainStyledAttributes.getString(yb1.j);
        obtainStyledAttributes.recycle();
        final int id = ((View)this).getId();
        final Fragment h0 = k.h0(id);
        if (string != null && h0 == null) {
            if (id <= 0) {
                String string3;
                if (string2 != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(" with tag ");
                    sb.append(string2);
                    string3 = sb.toString();
                }
                else {
                    string3 = "";
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("FragmentContainerView must have an android:id to add Fragment ");
                sb2.append(string);
                sb2.append(string3);
                throw new IllegalStateException(sb2.toString());
            }
            final Fragment a = k.q0().a(context.getClassLoader(), string);
            a.onInflate(context, set, null);
            k.m().r(true).c((ViewGroup)this, a, string2).j();
        }
        k.U0(this);
    }
    
    public final void a(final View view) {
        final ArrayList b = this.b;
        if (b != null && b.contains(view)) {
            if (this.a == null) {
                this.a = new ArrayList();
            }
            this.a.add(view);
        }
    }
    
    public void addView(final View obj, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (k.z0(obj) != null) {
            super.addView(obj, n, viewGroup$LayoutParams);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Views added to a FragmentContainerView must be associated with a Fragment. View ");
        sb.append(obj);
        sb.append(" is not associated with a Fragment.");
        throw new IllegalStateException(sb.toString());
    }
    
    public boolean addViewInLayout(final View obj, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams, final boolean b) {
        if (k.z0(obj) != null) {
            return super.addViewInLayout(obj, n, viewGroup$LayoutParams, b);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Views added to a FragmentContainerView must be associated with a Fragment. View ");
        sb.append(obj);
        sb.append(" is not associated with a Fragment.");
        throw new IllegalStateException(sb.toString());
    }
    
    public WindowInsets dispatchApplyWindowInsets(final WindowInsets windowInsets) {
        final u62 v = u62.v(windowInsets);
        final View$OnApplyWindowInsetsListener c = this.c;
        u62 u62;
        if (c != null) {
            u62 = u62.v(c.onApplyWindowInsets((View)this, windowInsets));
        }
        else {
            u62 = o32.c0((View)this, v);
        }
        if (!u62.o()) {
            for (int childCount = ((ViewGroup)this).getChildCount(), i = 0; i < childCount; ++i) {
                o32.g(((ViewGroup)this).getChildAt(i), u62);
            }
        }
        return windowInsets;
    }
    
    public void dispatchDraw(final Canvas canvas) {
        if (this.d && this.a != null) {
            for (int i = 0; i < this.a.size(); ++i) {
                super.drawChild(canvas, (View)this.a.get(i), ((View)this).getDrawingTime());
            }
        }
        super.dispatchDraw(canvas);
    }
    
    public boolean drawChild(final Canvas canvas, final View o, final long n) {
        if (this.d) {
            final ArrayList a = this.a;
            if (a != null && a.size() > 0 && this.a.contains(o)) {
                return false;
            }
        }
        return super.drawChild(canvas, o, n);
    }
    
    public void endViewTransition(final View view) {
        final ArrayList b = this.b;
        if (b != null) {
            b.remove(view);
            final ArrayList a = this.a;
            if (a != null && a.remove(view)) {
                this.d = true;
            }
        }
        super.endViewTransition(view);
    }
    
    public WindowInsets onApplyWindowInsets(final WindowInsets windowInsets) {
        return windowInsets;
    }
    
    public void removeAllViewsInLayout() {
        for (int i = ((ViewGroup)this).getChildCount() - 1; i >= 0; --i) {
            this.a(((ViewGroup)this).getChildAt(i));
        }
        super.removeAllViewsInLayout();
    }
    
    public void removeDetachedView(final View view, final boolean b) {
        if (b) {
            this.a(view);
        }
        super.removeDetachedView(view, b);
    }
    
    public void removeView(final View view) {
        this.a(view);
        super.removeView(view);
    }
    
    public void removeViewAt(final int n) {
        this.a(((ViewGroup)this).getChildAt(n));
        super.removeViewAt(n);
    }
    
    public void removeViewInLayout(final View view) {
        this.a(view);
        super.removeViewInLayout(view);
    }
    
    public void removeViews(final int n, final int n2) {
        for (int i = n; i < n + n2; ++i) {
            this.a(((ViewGroup)this).getChildAt(i));
        }
        super.removeViews(n, n2);
    }
    
    public void removeViewsInLayout(final int n, final int n2) {
        for (int i = n; i < n + n2; ++i) {
            this.a(((ViewGroup)this).getChildAt(i));
        }
        super.removeViewsInLayout(n, n2);
    }
    
    public void setDrawDisappearingViewsLast(final boolean d) {
        this.d = d;
    }
    
    public void setLayoutTransition(final LayoutTransition layoutTransition) {
        throw new UnsupportedOperationException("FragmentContainerView does not support Layout Transitions or animateLayoutChanges=\"true\".");
    }
    
    public void setOnApplyWindowInsetsListener(final View$OnApplyWindowInsetsListener c) {
        this.c = c;
    }
    
    public void startViewTransition(final View e) {
        if (e.getParent() == this) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            this.b.add(e);
        }
        super.startViewTransition(e);
    }
}

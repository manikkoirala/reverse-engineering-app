// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.content.res.TypedArray;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewGroup;
import android.util.Log;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.view.LayoutInflater$Factory2;

public class i implements LayoutInflater$Factory2
{
    public final k a;
    
    public i(final k a) {
        this.a = a;
    }
    
    public View onCreateView(View mView, final String s, final Context context, final AttributeSet set) {
        if (FragmentContainerView.class.getName().equals(s)) {
            return (View)new FragmentContainerView(context, set, this.a);
        }
        final boolean equals = "fragment".equals(s);
        Fragment h0 = null;
        if (!equals) {
            return null;
        }
        final String attributeValue = set.getAttributeValue((String)null, "class");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, yb1.d);
        String string;
        if ((string = attributeValue) == null) {
            string = obtainStyledAttributes.getString(yb1.e);
        }
        final int resourceId = obtainStyledAttributes.getResourceId(yb1.f, -1);
        final String string2 = obtainStyledAttributes.getString(yb1.g);
        obtainStyledAttributes.recycle();
        if (string == null || !g.b(context.getClassLoader(), string)) {
            return null;
        }
        int id;
        if (mView != null) {
            id = mView.getId();
        }
        else {
            id = 0;
        }
        if (id == -1 && resourceId == -1 && string2 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(set.getPositionDescription());
            sb.append(": Must specify unique android:id, android:tag, or have a parent with an id for ");
            sb.append(string);
            throw new IllegalArgumentException(sb.toString());
        }
        if (resourceId != -1) {
            h0 = this.a.h0(resourceId);
        }
        Fragment i0;
        if ((i0 = h0) == null) {
            i0 = h0;
            if (string2 != null) {
                i0 = this.a.i0(string2);
            }
        }
        Fragment h2;
        if ((h2 = i0) == null) {
            h2 = i0;
            if (id != -1) {
                h2 = this.a.h0(id);
            }
        }
        Fragment fragment = null;
        o o = null;
        Label_0596: {
            o g;
            StringBuilder sb2;
            String str;
            if (h2 == null) {
                final Fragment a = this.a.q0().a(context.getClassLoader(), string);
                a.mFromLayout = true;
                int mFragmentId;
                if (resourceId != 0) {
                    mFragmentId = resourceId;
                }
                else {
                    mFragmentId = id;
                }
                a.mFragmentId = mFragmentId;
                a.mContainerId = id;
                a.mTag = string2;
                a.mInLayout = true;
                final k a2 = this.a;
                a.mFragmentManager = a2;
                a.mHost = a2.t0();
                a.onInflate(this.a.t0().f(), set, a.mSavedFragmentState);
                g = this.a.g(a);
                fragment = a;
                o = g;
                if (!k.F0(2)) {
                    break Label_0596;
                }
                sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(a);
                str = " has been inflated via the <fragment> tag: id=0x";
                fragment = a;
            }
            else {
                if (h2.mInLayout) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(set.getPositionDescription());
                    sb3.append(": Duplicate id 0x");
                    sb3.append(Integer.toHexString(resourceId));
                    sb3.append(", tag ");
                    sb3.append(string2);
                    sb3.append(", or parent id 0x");
                    sb3.append(Integer.toHexString(id));
                    sb3.append(" with another fragment for ");
                    sb3.append(string);
                    throw new IllegalArgumentException(sb3.toString());
                }
                h2.mInLayout = true;
                final k a3 = this.a;
                h2.mFragmentManager = a3;
                h2.mHost = a3.t0();
                h2.onInflate(this.a.t0().f(), set, h2.mSavedFragmentState);
                final o v = this.a.v(h2);
                fragment = h2;
                o = v;
                if (!k.F0(2)) {
                    break Label_0596;
                }
                sb2 = new StringBuilder();
                sb2.append("Retained Fragment ");
                sb2.append(h2);
                str = " has been re-attached via the <fragment> tag: id=0x";
                fragment = h2;
                g = v;
            }
            sb2.append(str);
            sb2.append(Integer.toHexString(resourceId));
            Log.v("FragmentManager", sb2.toString());
            o = g;
        }
        fragment.mContainer = (ViewGroup)mView;
        o.m();
        o.j();
        mView = fragment.mView;
        if (mView != null) {
            if (resourceId != 0) {
                mView.setId(resourceId);
            }
            if (fragment.mView.getTag() == null) {
                fragment.mView.setTag((Object)string2);
            }
            fragment.mView.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this, o) {
                public final o a;
                public final i b;
                
                public void onViewAttachedToWindow(final View view) {
                    final Fragment k = this.a.k();
                    this.a.m();
                    SpecialEffectsController.o((ViewGroup)k.mView.getParent(), this.b.a).j();
                }
                
                public void onViewDetachedFromWindow(final View view) {
                }
            });
            return fragment.mView;
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("Fragment ");
        sb4.append(string);
        sb4.append(" did not create a view.");
        throw new IllegalStateException(sb4.toString());
    }
    
    public View onCreateView(final String s, final Context context, final AttributeSet set) {
        return this.onCreateView(null, s, context, set);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import android.view.View;
import java.util.List;
import android.util.Log;
import java.util.ArrayList;
import android.view.ViewGroup;

public abstract class SpecialEffectsController
{
    public final ViewGroup a;
    public final ArrayList b;
    public final ArrayList c;
    public boolean d;
    public boolean e;
    
    public SpecialEffectsController(final ViewGroup a) {
        this.b = new ArrayList();
        this.c = new ArrayList();
        this.d = false;
        this.e = false;
        this.a = a;
    }
    
    public static SpecialEffectsController n(final ViewGroup viewGroup, final xo1 xo1) {
        final int b = gb1.b;
        final Object tag = ((View)viewGroup).getTag(b);
        if (tag instanceof SpecialEffectsController) {
            return (SpecialEffectsController)tag;
        }
        final SpecialEffectsController a = xo1.a(viewGroup);
        ((View)viewGroup).setTag(b, (Object)a);
        return a;
    }
    
    public static SpecialEffectsController o(final ViewGroup viewGroup, final k k) {
        return n(viewGroup, k.y0());
    }
    
    public final void a(final State state, final LifecycleImpact lifecycleImpact, final o o) {
        synchronized (this.b) {
            final jf jf = new jf();
            final Operation h = this.h(o.k());
            if (h != null) {
                h.k(state, lifecycleImpact);
                return;
            }
            final d e = new d(state, lifecycleImpact, o, jf);
            this.b.add(e);
            ((Operation)e).a(new Runnable(this, e) {
                public final d a;
                public final SpecialEffectsController b;
                
                @Override
                public void run() {
                    if (this.b.b.contains(this.a)) {
                        ((Operation)this.a).e().applyState(((Operation)this.a).f().mView);
                    }
                }
            });
            ((Operation)e).a(new Runnable(this, e) {
                public final d a;
                public final SpecialEffectsController b;
                
                @Override
                public void run() {
                    this.b.b.remove(this.a);
                    this.b.c.remove(this.a);
                }
            });
        }
    }
    
    public void b(final State state, final o o) {
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing add operation for fragment ");
            sb.append(o.k());
            Log.v("FragmentManager", sb.toString());
        }
        this.a(state, LifecycleImpact.ADDING, o);
    }
    
    public void c(final o o) {
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing hide operation for fragment ");
            sb.append(o.k());
            Log.v("FragmentManager", sb.toString());
        }
        this.a(State.GONE, LifecycleImpact.NONE, o);
    }
    
    public void d(final o o) {
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing remove operation for fragment ");
            sb.append(o.k());
            Log.v("FragmentManager", sb.toString());
        }
        this.a(State.REMOVED, LifecycleImpact.REMOVING, o);
    }
    
    public void e(final o o) {
        if (k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing show operation for fragment ");
            sb.append(o.k());
            Log.v("FragmentManager", sb.toString());
        }
        this.a(State.VISIBLE, LifecycleImpact.NONE, o);
    }
    
    public abstract void f(final List p0, final boolean p1);
    
    public void g() {
        if (this.e) {
            return;
        }
        if (!o32.T((View)this.a)) {
            this.j();
            this.d = false;
            return;
        }
        synchronized (this.b) {
            if (!this.b.isEmpty()) {
                final ArrayList list = new ArrayList(this.c);
                this.c.clear();
                for (final Operation operation : list) {
                    if (k.F0(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("SpecialEffectsController: Cancelling operation ");
                        sb.append(operation);
                        Log.v("FragmentManager", sb.toString());
                    }
                    operation.b();
                    if (!operation.i()) {
                        this.c.add(operation);
                    }
                }
                this.q();
                final ArrayList c = new ArrayList(this.b);
                this.b.clear();
                this.c.addAll(c);
                final Iterator iterator2 = c.iterator();
                while (iterator2.hasNext()) {
                    ((Operation)iterator2.next()).l();
                }
                this.f(c, this.d);
                this.d = false;
            }
        }
    }
    
    public final Operation h(final Fragment fragment) {
        for (final Operation operation : this.b) {
            if (operation.f().equals(fragment) && !operation.h()) {
                return operation;
            }
        }
        return null;
    }
    
    public final Operation i(final Fragment fragment) {
        for (final Operation operation : this.c) {
            if (operation.f().equals(fragment) && !operation.h()) {
                return operation;
            }
        }
        return null;
    }
    
    public void j() {
        final boolean t = o32.T((View)this.a);
        synchronized (this.b) {
            this.q();
            final Iterator iterator = this.b.iterator();
            while (iterator.hasNext()) {
                ((Operation)iterator.next()).l();
            }
            for (final Operation obj : new ArrayList(this.c)) {
                if (k.F0(2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("SpecialEffectsController: ");
                    String string;
                    if (t) {
                        string = "";
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Container ");
                        sb2.append(this.a);
                        sb2.append(" is not attached to window. ");
                        string = sb2.toString();
                    }
                    sb.append(string);
                    sb.append("Cancelling running operation ");
                    sb.append(obj);
                    Log.v("FragmentManager", sb.toString());
                }
                obj.b();
            }
            for (final Operation obj2 : new ArrayList(this.b)) {
                if (k.F0(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: ");
                    String string2;
                    if (t) {
                        string2 = "";
                    }
                    else {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Container ");
                        sb4.append(this.a);
                        sb4.append(" is not attached to window. ");
                        string2 = sb4.toString();
                    }
                    sb3.append(string2);
                    sb3.append("Cancelling pending operation ");
                    sb3.append(obj2);
                    Log.v("FragmentManager", sb3.toString());
                }
                obj2.b();
            }
        }
    }
    
    public void k() {
        if (this.e) {
            this.e = false;
            this.g();
        }
    }
    
    public LifecycleImpact l(final o o) {
        final Operation h = this.h(o.k());
        Enum<LifecycleImpact> g;
        if (h != null) {
            g = h.g();
        }
        else {
            g = null;
        }
        final Operation i = this.i(o.k());
        if (i != null && (g == null || g == LifecycleImpact.NONE)) {
            return i.g();
        }
        return (LifecycleImpact)g;
    }
    
    public ViewGroup m() {
        return this.a;
    }
    
    public void p() {
        synchronized (this.b) {
            this.q();
            this.e = false;
            for (int i = this.b.size() - 1; i >= 0; --i) {
                final Operation operation = this.b.get(i);
                final State from = State.from(operation.f().mView);
                final State e = operation.e();
                final State visible = State.VISIBLE;
                if (e == visible && from != visible) {
                    this.e = operation.f().isPostponed();
                    break;
                }
            }
        }
    }
    
    public final void q() {
        for (final Operation operation : this.b) {
            if (operation.g() == LifecycleImpact.ADDING) {
                operation.k(State.from(operation.f().requireView().getVisibility()), LifecycleImpact.NONE);
            }
        }
    }
    
    public void r(final boolean d) {
        this.d = d;
    }
    
    public abstract static class Operation
    {
        public State a;
        public LifecycleImpact b;
        public final Fragment c;
        public final List d;
        public final HashSet e;
        public boolean f;
        public boolean g;
        
        public Operation(final State a, final LifecycleImpact b, final Fragment c, final jf jf) {
            this.d = new ArrayList();
            this.e = new HashSet();
            this.f = false;
            this.g = false;
            this.a = a;
            this.b = b;
            this.c = c;
            jf.c((jf.b)new jf.b(this) {
                public final Operation a;
                
                @Override
                public void a() {
                    this.a.b();
                }
            });
        }
        
        public final void a(final Runnable runnable) {
            this.d.add(runnable);
        }
        
        public final void b() {
            if (this.h()) {
                return;
            }
            this.f = true;
            if (this.e.isEmpty()) {
                this.c();
            }
            else {
                final Iterator iterator = new ArrayList(this.e).iterator();
                while (iterator.hasNext()) {
                    ((jf)iterator.next()).a();
                }
            }
        }
        
        public void c() {
            if (this.g) {
                return;
            }
            if (k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SpecialEffectsController: ");
                sb.append(this);
                sb.append(" has called complete.");
                Log.v("FragmentManager", sb.toString());
            }
            this.g = true;
            final Iterator iterator = this.d.iterator();
            while (iterator.hasNext()) {
                ((Runnable)iterator.next()).run();
            }
        }
        
        public final void d(final jf o) {
            if (this.e.remove(o) && this.e.isEmpty()) {
                this.c();
            }
        }
        
        public State e() {
            return this.a;
        }
        
        public final Fragment f() {
            return this.c;
        }
        
        public LifecycleImpact g() {
            return this.b;
        }
        
        public final boolean h() {
            return this.f;
        }
        
        public final boolean i() {
            return this.g;
        }
        
        public final void j(final jf e) {
            this.l();
            this.e.add(e);
        }
        
        public final void k(final State state, final LifecycleImpact lifecycleImpact) {
            final int n = SpecialEffectsController$c.b[lifecycleImpact.ordinal()];
            LifecycleImpact b;
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        return;
                    }
                    if (this.a != State.REMOVED) {
                        if (k.F0(2)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("SpecialEffectsController: For fragment ");
                            sb.append(this.c);
                            sb.append(" mFinalState = ");
                            sb.append(this.a);
                            sb.append(" -> ");
                            sb.append(state);
                            sb.append(". ");
                            Log.v("FragmentManager", sb.toString());
                        }
                        this.a = state;
                    }
                    return;
                }
                else {
                    if (k.F0(2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("SpecialEffectsController: For fragment ");
                        sb2.append(this.c);
                        sb2.append(" mFinalState = ");
                        sb2.append(this.a);
                        sb2.append(" -> REMOVED. mLifecycleImpact  = ");
                        sb2.append(this.b);
                        sb2.append(" to REMOVING.");
                        Log.v("FragmentManager", sb2.toString());
                    }
                    this.a = State.REMOVED;
                    b = LifecycleImpact.REMOVING;
                }
            }
            else {
                if (this.a != State.REMOVED) {
                    return;
                }
                if (k.F0(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: For fragment ");
                    sb3.append(this.c);
                    sb3.append(" mFinalState = REMOVED -> VISIBLE. mLifecycleImpact = ");
                    sb3.append(this.b);
                    sb3.append(" to ADDING.");
                    Log.v("FragmentManager", sb3.toString());
                }
                this.a = State.VISIBLE;
                b = LifecycleImpact.ADDING;
            }
            this.b = b;
        }
        
        public abstract void l();
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Operation ");
            sb.append("{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append("} ");
            sb.append("{");
            sb.append("mFinalState = ");
            sb.append(this.a);
            sb.append("} ");
            sb.append("{");
            sb.append("mLifecycleImpact = ");
            sb.append(this.b);
            sb.append("} ");
            sb.append("{");
            sb.append("mFragment = ");
            sb.append(this.c);
            sb.append("}");
            return sb.toString();
        }
        
        public enum LifecycleImpact
        {
            private static final LifecycleImpact[] $VALUES;
            
            ADDING, 
            NONE, 
            REMOVING;
        }
        
        public enum State
        {
            private static final State[] $VALUES;
            
            GONE, 
            INVISIBLE, 
            REMOVED, 
            VISIBLE;
            
            public static State from(final int i) {
                if (i == 0) {
                    return State.VISIBLE;
                }
                if (i == 4) {
                    return State.INVISIBLE;
                }
                if (i == 8) {
                    return State.GONE;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown visibility ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
            
            public static State from(final View view) {
                if (view.getAlpha() == 0.0f && view.getVisibility() == 0) {
                    return State.INVISIBLE;
                }
                return from(view.getVisibility());
            }
            
            public void applyState(final View view) {
                final int n = SpecialEffectsController$c.a[this.ordinal()];
                if (n != 1) {
                    int visibility;
                    if (n != 2) {
                        if (n != 3) {
                            if (n != 4) {
                                return;
                            }
                            if (k.F0(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("SpecialEffectsController: Setting view ");
                                sb.append(view);
                                sb.append(" to INVISIBLE");
                                Log.v("FragmentManager", sb.toString());
                            }
                            view.setVisibility(4);
                            return;
                        }
                        else {
                            if (k.F0(2)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("SpecialEffectsController: Setting view ");
                                sb2.append(view);
                                sb2.append(" to GONE");
                                Log.v("FragmentManager", sb2.toString());
                            }
                            visibility = 8;
                        }
                    }
                    else {
                        if (k.F0(2)) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("SpecialEffectsController: Setting view ");
                            sb3.append(view);
                            sb3.append(" to VISIBLE");
                            Log.v("FragmentManager", sb3.toString());
                        }
                        visibility = 0;
                    }
                    view.setVisibility(visibility);
                }
                else {
                    final ViewGroup obj = (ViewGroup)view.getParent();
                    if (obj != null) {
                        if (k.F0(2)) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("SpecialEffectsController: Removing view ");
                            sb4.append(view);
                            sb4.append(" from container ");
                            sb4.append(obj);
                            Log.v("FragmentManager", sb4.toString());
                        }
                        obj.removeView(view);
                    }
                }
            }
        }
    }
    
    public static class d extends Operation
    {
        public final o h;
        
        public d(final State state, final LifecycleImpact lifecycleImpact, final o h, final jf jf) {
            super(state, lifecycleImpact, h.k(), jf);
            this.h = h;
        }
        
        @Override
        public void c() {
            super.c();
            this.h.m();
        }
        
        @Override
        public void l() {
            if (((Operation)this).g() == LifecycleImpact.ADDING) {
                final Fragment k = this.h.k();
                final View focus = k.mView.findFocus();
                if (focus != null) {
                    k.setFocusedView(focus);
                    if (androidx.fragment.app.k.F0(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("requestFocus: Saved focused view ");
                        sb.append(focus);
                        sb.append(" for Fragment ");
                        sb.append(k);
                        Log.v("FragmentManager", sb.toString());
                    }
                }
                final View requireView = ((Operation)this).f().requireView();
                if (requireView.getParent() == null) {
                    this.h.b();
                    requireView.setAlpha(0.0f);
                }
                if (requireView.getAlpha() == 0.0f && requireView.getVisibility() == 0) {
                    requireView.setVisibility(4);
                }
                requireView.setAlpha(k.getPostOnViewCreatedAlpha());
            }
        }
    }
}

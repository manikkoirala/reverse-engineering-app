// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.LayoutInflater;
import androidx.activity.OnBackPressedDispatcher;
import androidx.activity.result.ActivityResultRegistry;
import android.view.Window;
import android.content.IntentSender;
import android.app.Activity;
import android.view.MenuItem;
import android.view.Menu;
import android.content.res.Configuration;
import android.content.Intent;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.util.AttributeSet;
import android.view.View;
import java.util.Iterator;
import android.content.Context;
import android.os.Parcelable;
import androidx.lifecycle.Lifecycle;
import android.os.Bundle;
import androidx.savedstate.a;
import androidx.lifecycle.g;
import androidx.activity.ComponentActivity;

public abstract class e extends ComponentActivity implements h2.d
{
    static final String FRAGMENTS_TAG = "android:support:fragments";
    boolean mCreated;
    final g mFragmentLifecycleRegistry;
    final g80 mFragments;
    boolean mResumed;
    boolean mStopped;
    
    public e() {
        this.mFragments = g80.b(new c());
        this.mFragmentLifecycleRegistry = new g(this);
        this.mStopped = true;
        this.k();
    }
    
    private void k() {
        this.getSavedStateRegistry().h("android:support:fragments", (androidx.savedstate.a.c)new androidx.savedstate.a.c(this) {
            public final e a;
            
            @Override
            public Bundle a() {
                final Bundle bundle = new Bundle();
                this.a.markFragmentsCreated();
                this.a.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_STOP);
                final Parcelable x = this.a.mFragments.x();
                if (x != null) {
                    bundle.putParcelable("android:support:fragments", x);
                }
                return bundle;
            }
        });
        this.addOnContextAvailableListener(new l11(this) {
            public final e a;
            
            @Override
            public void a(final Context context) {
                this.a.mFragments.a(null);
                final Bundle b = this.a.getSavedStateRegistry().b("android:support:fragments");
                if (b != null) {
                    this.a.mFragments.w(b.getParcelable("android:support:fragments"));
                }
            }
        });
    }
    
    public static boolean l(final k k, final Lifecycle.State state) {
        final Iterator iterator = k.s0().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Fragment fragment = (Fragment)iterator.next();
            if (fragment == null) {
                continue;
            }
            boolean b = n != 0;
            if (fragment.getHost() != null) {
                b = ((n | (l(fragment.getChildFragmentManager(), state) ? 1 : 0)) != 0x0);
            }
            final n80 mViewLifecycleOwner = fragment.mViewLifecycleOwner;
            n = (b ? 1 : 0);
            if (mViewLifecycleOwner != null) {
                n = (b ? 1 : 0);
                if (mViewLifecycleOwner.getLifecycle().b().isAtLeast(Lifecycle.State.STARTED)) {
                    fragment.mViewLifecycleOwner.f(state);
                    n = (true ? 1 : 0);
                }
            }
            if (!fragment.mLifecycleRegistry.b().isAtLeast(Lifecycle.State.STARTED)) {
                continue;
            }
            fragment.mLifecycleRegistry.n(state);
            n = (true ? 1 : 0);
        }
        return n != 0;
    }
    
    public final View dispatchFragmentsOnCreateView(final View view, final String s, final Context context, final AttributeSet set) {
        return this.mFragments.v(view, s, context, set);
    }
    
    public void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        printWriter.print(s);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("  ");
        final String string = sb.toString();
        printWriter.print(string);
        printWriter.print("mCreated=");
        printWriter.print(this.mCreated);
        printWriter.print(" mResumed=");
        printWriter.print(this.mResumed);
        printWriter.print(" mStopped=");
        printWriter.print(this.mStopped);
        if (this.getApplication() != null) {
            rk0.b(this).a(string, fileDescriptor, printWriter, array);
        }
        this.mFragments.t().W(s, fileDescriptor, printWriter, array);
    }
    
    public k getSupportFragmentManager() {
        return this.mFragments.t();
    }
    
    @Deprecated
    public rk0 getSupportLoaderManager() {
        return rk0.b(this);
    }
    
    public void markFragmentsCreated() {
        while (l(this.getSupportFragmentManager(), Lifecycle.State.CREATED)) {}
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        this.mFragments.u();
        super.onActivityResult(n, n2, intent);
    }
    
    @Deprecated
    public void onAttachFragment(final Fragment fragment) {
    }
    
    @Override
    public void onConfigurationChanged(final Configuration configuration) {
        this.mFragments.u();
        super.onConfigurationChanged(configuration);
        this.mFragments.d(configuration);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_CREATE);
        this.mFragments.f();
    }
    
    @Override
    public boolean onCreatePanelMenu(final int n, final Menu menu) {
        if (n == 0) {
            return super.onCreatePanelMenu(n, menu) | this.mFragments.g(menu, this.getMenuInflater());
        }
        return super.onCreatePanelMenu(n, menu);
    }
    
    public View onCreateView(final View view, final String s, final Context context, final AttributeSet set) {
        final View dispatchFragmentsOnCreateView = this.dispatchFragmentsOnCreateView(view, s, context, set);
        if (dispatchFragmentsOnCreateView == null) {
            return super.onCreateView(view, s, context, set);
        }
        return dispatchFragmentsOnCreateView;
    }
    
    public View onCreateView(final String s, final Context context, final AttributeSet set) {
        final View dispatchFragmentsOnCreateView = this.dispatchFragmentsOnCreateView(null, s, context, set);
        if (dispatchFragmentsOnCreateView == null) {
            return super.onCreateView(s, context, set);
        }
        return dispatchFragmentsOnCreateView;
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.mFragments.h();
        this.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_DESTROY);
    }
    
    public void onLowMemory() {
        super.onLowMemory();
        this.mFragments.i();
    }
    
    @Override
    public boolean onMenuItemSelected(final int n, final MenuItem menuItem) {
        if (super.onMenuItemSelected(n, menuItem)) {
            return true;
        }
        if (n != 0) {
            return n == 6 && this.mFragments.e(menuItem);
        }
        return this.mFragments.k(menuItem);
    }
    
    @Override
    public void onMultiWindowModeChanged(final boolean b) {
        this.mFragments.j(b);
    }
    
    @Override
    public void onNewIntent(final Intent intent) {
        this.mFragments.u();
        super.onNewIntent(intent);
    }
    
    @Override
    public void onPanelClosed(final int n, final Menu menu) {
        if (n == 0) {
            this.mFragments.l(menu);
        }
        super.onPanelClosed(n, menu);
    }
    
    public void onPause() {
        super.onPause();
        this.mResumed = false;
        this.mFragments.m();
        this.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_PAUSE);
    }
    
    @Override
    public void onPictureInPictureModeChanged(final boolean b) {
        this.mFragments.n(b);
    }
    
    public void onPostResume() {
        super.onPostResume();
        this.onResumeFragments();
    }
    
    @Deprecated
    public boolean onPrepareOptionsPanel(final View view, final Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }
    
    @Override
    public boolean onPreparePanel(final int n, final View view, final Menu menu) {
        if (n == 0) {
            return this.onPrepareOptionsPanel(view, menu) | this.mFragments.o(menu);
        }
        return super.onPreparePanel(n, view, menu);
    }
    
    @Override
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        this.mFragments.u();
        super.onRequestPermissionsResult(n, array, array2);
    }
    
    public void onResume() {
        this.mFragments.u();
        super.onResume();
        this.mResumed = true;
        this.mFragments.s();
    }
    
    public void onResumeFragments() {
        this.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_RESUME);
        this.mFragments.p();
    }
    
    public void onStart() {
        this.mFragments.u();
        super.onStart();
        this.mStopped = false;
        if (!this.mCreated) {
            this.mCreated = true;
            this.mFragments.c();
        }
        this.mFragments.s();
        this.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_START);
        this.mFragments.q();
    }
    
    public void onStateNotSaved() {
        this.mFragments.u();
    }
    
    public void onStop() {
        super.onStop();
        this.mStopped = true;
        this.markFragmentsCreated();
        this.mFragments.r();
        this.mFragmentLifecycleRegistry.h(Lifecycle.Event.ON_STOP);
    }
    
    public void setEnterSharedElementCallback(final in1 in1) {
        h2.h(this, in1);
    }
    
    public void setExitSharedElementCallback(final in1 in1) {
        h2.i(this, in1);
    }
    
    public void startActivityFromFragment(final Fragment fragment, final Intent intent, final int n) {
        this.startActivityFromFragment(fragment, intent, n, null);
    }
    
    public void startActivityFromFragment(final Fragment fragment, final Intent intent, final int n, final Bundle bundle) {
        if (n == -1) {
            h2.k(this, intent, -1, bundle);
            return;
        }
        fragment.startActivityForResult(intent, n, bundle);
    }
    
    @Deprecated
    public void startIntentSenderFromFragment(final Fragment fragment, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) {
        if (n == -1) {
            h2.l(this, intentSender, n, intent, n2, n3, n4, bundle);
            return;
        }
        fragment.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    public void supportFinishAfterTransition() {
        h2.c(this);
    }
    
    @Deprecated
    public void supportInvalidateOptionsMenu() {
        this.invalidateOptionsMenu();
    }
    
    public void supportPostponeEnterTransition() {
        h2.e(this);
    }
    
    public void supportStartPostponedEnterTransition() {
        h2.m(this);
    }
    
    @Deprecated
    @Override
    public final void validateRequestPermissionsRequestCode(final int n) {
    }
    
    public class c extends h implements c42, j11, s2, i80
    {
        public final e f;
        
        public c(final e f) {
            super(this.f = f);
        }
        
        @Override
        public void a(final k k, final Fragment fragment) {
            this.f.onAttachFragment(fragment);
        }
        
        @Override
        public View c(final int n) {
            return this.f.findViewById(n);
        }
        
        @Override
        public boolean d() {
            final Window window = this.f.getWindow();
            return window != null && window.peekDecorView() != null;
        }
        
        @Override
        public ActivityResultRegistry getActivityResultRegistry() {
            return this.f.getActivityResultRegistry();
        }
        
        @Override
        public Lifecycle getLifecycle() {
            return this.f.mFragmentLifecycleRegistry;
        }
        
        @Override
        public OnBackPressedDispatcher getOnBackPressedDispatcher() {
            return this.f.getOnBackPressedDispatcher();
        }
        
        @Override
        public b42 getViewModelStore() {
            return this.f.getViewModelStore();
        }
        
        @Override
        public LayoutInflater i() {
            return this.f.getLayoutInflater().cloneInContext((Context)this.f);
        }
        
        @Override
        public boolean k(final Fragment fragment) {
            return this.f.isFinishing() ^ true;
        }
        
        @Override
        public boolean l(final String s) {
            return h2.j(this.f, s);
        }
        
        @Override
        public void o() {
            this.f.supportInvalidateOptionsMenu();
        }
        
        public e p() {
            return this.f;
        }
    }
}

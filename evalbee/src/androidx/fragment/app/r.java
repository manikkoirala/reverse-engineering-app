// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.graphics.Rect;
import java.util.Map;
import java.util.List;
import java.util.Collection;
import android.view.ViewGroup;
import android.util.SparseArray;
import android.content.Context;
import android.view.View;
import java.util.ArrayList;

public abstract class r
{
    public static final int[] a;
    public static final l80 b;
    public static final l80 c;
    
    static {
        a = new int[] { 0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10 };
        b = new k80();
        c = w();
    }
    
    public static void A(final ArrayList list, final int visibility) {
        if (list == null) {
            return;
        }
        for (int i = list.size() - 1; i >= 0; --i) {
            ((View)list.get(i)).setVisibility(visibility);
        }
    }
    
    public static void B(final Context context, final f80 f80, final ArrayList list, final ArrayList list2, final int n, final int n2, final boolean b, final g g) {
        final SparseArray sparseArray = new SparseArray();
        for (int i = n; i < n2; ++i) {
            final a a = list.get(i);
            if (list2.get(i)) {
                e(a, sparseArray, b);
            }
            else {
                c(a, sparseArray, b);
            }
        }
        if (sparseArray.size() != 0) {
            final View view = new View(context);
            for (int size = sparseArray.size(), j = 0; j < size; ++j) {
                final int key = sparseArray.keyAt(j);
                final r8 d = d(key, list, list2, n, n2);
                final h h = (h)sparseArray.valueAt(j);
                if (f80.d()) {
                    final ViewGroup viewGroup = (ViewGroup)f80.c(key);
                    if (viewGroup != null) {
                        if (b) {
                            o(viewGroup, h, view, d, g);
                        }
                        else {
                            n(viewGroup, h, view, d, g);
                        }
                    }
                }
            }
        }
    }
    
    public static void a(final ArrayList list, final r8 r8, final Collection collection) {
        for (int i = r8.size() - 1; i >= 0; --i) {
            final View e = (View)r8.m(i);
            if (collection.contains(o32.J(e))) {
                list.add(e);
            }
        }
    }
    
    public static void b(final a a, final q.a a2, final SparseArray sparseArray, final boolean b, final boolean b2) {
        final Fragment b3 = a2.b;
        if (b3 == null) {
            return;
        }
        final int mContainerId = b3.mContainerId;
        if (mContainerId == 0) {
            return;
        }
        int a3;
        if (b) {
            a3 = r.a[a2.a];
        }
        else {
            a3 = a2.a;
        }
        boolean b4 = false;
        final int n = 1;
        while (true) {
            boolean b5 = false;
            int n3 = 0;
            int n2 = 0;
            Label_0333: {
                Label_0323: {
                    Label_0320: {
                        Label_0314: {
                            Label_0283: {
                                if (a3 != 1) {
                                    int n4 = 0;
                                    Label_0270: {
                                        Label_0248: {
                                            if (a3 != 3) {
                                                if (a3 != 4) {
                                                    if (a3 != 5) {
                                                        if (a3 != 6) {
                                                            if (a3 != 7) {
                                                                b5 = false;
                                                                n2 = (n3 = 0);
                                                                break Label_0333;
                                                            }
                                                            break Label_0283;
                                                        }
                                                    }
                                                    else {
                                                        if (!b2) {
                                                            b4 = b3.mHidden;
                                                            break Label_0323;
                                                        }
                                                        if (b3.mHiddenChanged && !b3.mHidden && b3.mAdded) {
                                                            break Label_0314;
                                                        }
                                                        break Label_0320;
                                                    }
                                                }
                                                else {
                                                    if (b2 ? (b3.mHiddenChanged && b3.mAdded && b3.mHidden) : (b3.mAdded && !b3.mHidden)) {
                                                        break Label_0242;
                                                    }
                                                    break Label_0248;
                                                }
                                            }
                                            final boolean mAdded = b3.mAdded;
                                            if (b2) {
                                                if (mAdded) {
                                                    break Label_0248;
                                                }
                                                final View mView = b3.mView;
                                                if (mView == null || mView.getVisibility() != 0 || b3.mPostponedAlpha < 0.0f) {
                                                    break Label_0248;
                                                }
                                            }
                                            else if (!mAdded || b3.mHidden) {
                                                break Label_0248;
                                            }
                                            n4 = 1;
                                            break Label_0270;
                                        }
                                        n4 = 0;
                                    }
                                    n3 = n4;
                                    b5 = true;
                                    n2 = 0;
                                    break Label_0333;
                                }
                            }
                            if (b2) {
                                b4 = b3.mIsNewlyAdded;
                                break Label_0323;
                            }
                            if (b3.mAdded || b3.mHidden) {
                                break Label_0320;
                            }
                        }
                        b4 = true;
                        break Label_0323;
                    }
                    b4 = false;
                }
                n3 = 0;
                b5 = false;
                n2 = n;
            }
            h p5;
            final h h = p5 = (h)sparseArray.get(mContainerId);
            if (b4) {
                p5 = p(h, sparseArray, mContainerId);
                p5.a = b3;
                p5.b = b;
                p5.c = a;
            }
            if (!b2 && n2 != 0) {
                if (p5 != null && p5.d == b3) {
                    p5.d = null;
                }
                if (!a.r) {
                    final k t = a.t;
                    t.r0().p(t.v(b3));
                    t.R0(b3);
                }
            }
            h p6 = p5;
            Label_0494: {
                if (n3 != 0) {
                    if (p5 != null) {
                        p6 = p5;
                        if (p5.d != null) {
                            break Label_0494;
                        }
                    }
                    p6 = p(p5, sparseArray, mContainerId);
                    p6.d = b3;
                    p6.e = b;
                    p6.f = a;
                }
            }
            if (!b2 && b5 && p6 != null && p6.a == b3) {
                p6.a = null;
            }
            return;
            continue;
        }
    }
    
    public static void c(final a a, final SparseArray sparseArray, final boolean b) {
        for (int size = a.c.size(), i = 0; i < size; ++i) {
            b(a, (q.a)a.c.get(i), sparseArray, false, b);
        }
    }
    
    public static r8 d(final int n, final ArrayList list, final ArrayList list2, final int n2, int i) {
        final r8 r8 = new r8();
        --i;
        while (i >= n2) {
            final a a = list.get(i);
            if (a.A(n)) {
                final boolean booleanValue = list2.get(i);
                final ArrayList p5 = a.p;
                if (p5 != null) {
                    final int size = p5.size();
                    ArrayList list3;
                    ArrayList list4;
                    if (booleanValue) {
                        list3 = a.p;
                        list4 = a.q;
                    }
                    else {
                        list4 = a.p;
                        list3 = a.q;
                    }
                    for (int j = 0; j < size; ++j) {
                        final String s = list4.get(j);
                        final String s2 = list3.get(j);
                        final String s3 = (String)r8.remove(s2);
                        if (s3 != null) {
                            r8.put(s, s3);
                        }
                        else {
                            r8.put(s, s2);
                        }
                    }
                }
            }
            --i;
        }
        return r8;
    }
    
    public static void e(final a a, final SparseArray sparseArray, final boolean b) {
        if (!a.t.o0().d()) {
            return;
        }
        for (int i = a.c.size() - 1; i >= 0; --i) {
            b(a, (q.a)a.c.get(i), sparseArray, true, b);
        }
    }
    
    public static void f(final Fragment fragment, final Fragment fragment2, final boolean b, final r8 r8, final boolean b2) {
        if (b) {
            fragment2.getEnterTransitionCallback();
        }
        else {
            fragment.getEnterTransitionCallback();
        }
    }
    
    public static boolean g(final l80 l80, final List list) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (!l80.e(list.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static r8 h(final l80 l80, final r8 r8, final Object o, final h h) {
        final Fragment a = h.a;
        final View view = a.getView();
        if (!r8.isEmpty() && o != null && view != null) {
            final r8 r9 = new r8();
            l80.j(r9, view);
            final a c = h.c;
            ArrayList list;
            if (h.b) {
                a.getExitTransitionCallback();
                list = c.p;
            }
            else {
                a.getEnterTransitionCallback();
                list = c.q;
            }
            if (list != null) {
                r9.o(list);
                r9.o(r8.values());
            }
            x(r8, r9);
            return r9;
        }
        r8.clear();
        return null;
    }
    
    public static r8 i(final l80 l80, final r8 r8, final Object o, final h h) {
        if (!r8.isEmpty() && o != null) {
            final Fragment d = h.d;
            final r8 r9 = new r8();
            l80.j(r9, d.requireView());
            final a f = h.f;
            ArrayList list;
            if (h.e) {
                d.getEnterTransitionCallback();
                list = f.q;
            }
            else {
                d.getExitTransitionCallback();
                list = f.p;
            }
            if (list != null) {
                r9.o(list);
            }
            r8.o(r9.keySet());
            return r9;
        }
        r8.clear();
        return null;
    }
    
    public static l80 j(final Fragment fragment, final Fragment fragment2) {
        final ArrayList list = new ArrayList();
        if (fragment != null) {
            final Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                list.add(exitTransition);
            }
            final Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                list.add(returnTransition);
            }
            final Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                list.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            final Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                list.add(enterTransition);
            }
            final Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                list.add(reenterTransition);
            }
            final Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                list.add(sharedElementEnterTransition);
            }
        }
        if (list.isEmpty()) {
            return null;
        }
        final l80 b = r.b;
        if (b != null && g(b, list)) {
            return b;
        }
        final l80 c = r.c;
        if (c != null && g(c, list)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }
    
    public static ArrayList k(final l80 l80, final Object o, final Fragment fragment, final ArrayList c, final View e) {
        ArrayList list2;
        if (o != null) {
            final ArrayList list = new ArrayList();
            final View view = fragment.getView();
            if (view != null) {
                l80.f(list, view);
            }
            if (c != null) {
                list.removeAll(c);
            }
            list2 = list;
            if (!list.isEmpty()) {
                list.add(e);
                l80.b(o, list);
                list2 = list;
            }
        }
        else {
            list2 = null;
        }
        return list2;
    }
    
    public static Object l(final l80 l80, final ViewGroup viewGroup, final View view, final r8 r8, final h h, final ArrayList list, final ArrayList list2, final Object o, Object o2) {
        final Fragment a = h.a;
        final Fragment d = h.d;
        if (a == null || d == null) {
            return null;
        }
        final boolean b = h.b;
        Object t;
        if (r8.isEmpty()) {
            t = null;
        }
        else {
            t = t(l80, a, d, b);
        }
        final r8 i = i(l80, r8, t, h);
        if (r8.isEmpty()) {
            t = null;
        }
        else {
            list.addAll(i.values());
        }
        if (o == null && o2 == null && t == null) {
            return null;
        }
        f(a, d, b, i, true);
        if (t != null) {
            final Rect rect = new Rect();
            l80.z(t, view, list);
            z(l80, t, o2, i, h.e, h.f);
            o2 = rect;
            if (o != null) {
                l80.u(o, rect);
                o2 = rect;
            }
        }
        else {
            o2 = null;
        }
        s11.a((View)viewGroup, new Runnable(l80, r8, t, h, list2, view, a, d, b, list, o, o2) {
            public final l80 a;
            public final r8 b;
            public final Object c;
            public final h d;
            public final ArrayList e;
            public final View f;
            public final Fragment g;
            public final Fragment h;
            public final boolean i;
            public final ArrayList j;
            public final Object k;
            public final Rect l;
            
            @Override
            public void run() {
                final r8 h = r.h(this.a, this.b, this.c, this.d);
                if (h != null) {
                    this.e.addAll(h.values());
                    this.e.add(this.f);
                }
                r.f(this.g, this.h, this.i, h, false);
                final Object c = this.c;
                if (c != null) {
                    this.a.A(c, this.j, this.e);
                    final View s = r.s(h, this.d, this.k, this.i);
                    if (s != null) {
                        this.a.k(s, this.l);
                    }
                }
            }
        });
        return t;
    }
    
    public static Object m(final l80 l80, final ViewGroup viewGroup, final View e, final r8 r8, final h h, final ArrayList list, final ArrayList list2, final Object o, final Object o2) {
        final Fragment a = h.a;
        final Fragment d = h.d;
        if (a != null) {
            a.requireView().setVisibility(0);
        }
        if (a == null || d == null) {
            return null;
        }
        final boolean b = h.b;
        Object t;
        if (r8.isEmpty()) {
            t = null;
        }
        else {
            t = t(l80, a, d, b);
        }
        final r8 i = i(l80, r8, t, h);
        final r8 h2 = h(l80, r8, t, h);
        Object o3;
        if (r8.isEmpty()) {
            if (i != null) {
                i.clear();
            }
            if (h2 != null) {
                h2.clear();
            }
            o3 = null;
        }
        else {
            a(list, i, r8.keySet());
            a(list2, h2, r8.values());
            o3 = t;
        }
        if (o == null && o2 == null && o3 == null) {
            return null;
        }
        f(a, d, b, i, true);
        Rect rect;
        View s;
        if (o3 != null) {
            list2.add(e);
            l80.z(o3, e, list);
            z(l80, o3, o2, i, h.e, h.f);
            rect = new Rect();
            s = s(h2, h, o, b);
            if (s != null) {
                l80.u(o, rect);
            }
        }
        else {
            s = null;
            rect = null;
        }
        s11.a((View)viewGroup, new Runnable(a, d, b, h2, s, l80, rect) {
            public final Fragment a;
            public final Fragment b;
            public final boolean c;
            public final r8 d;
            public final View e;
            public final l80 f;
            public final Rect g;
            
            @Override
            public void run() {
                r.f(this.a, this.b, this.c, this.d, false);
                final View e = this.e;
                if (e != null) {
                    this.f.k(e, this.g);
                }
            }
        });
        return o3;
    }
    
    public static void n(final ViewGroup viewGroup, final h h, final View view, final r8 r8, final g g) {
        final Fragment a = h.a;
        final Fragment d = h.d;
        final l80 j = j(d, a);
        if (j == null) {
            return;
        }
        final boolean b = h.b;
        final boolean e = h.e;
        final Object q = q(j, a, b);
        Object r9 = r(j, d, e);
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final Object l = l(j, viewGroup, view, r8, h, list, list2, q, r9);
        if (q == null && l == null && r9 == null) {
            return;
        }
        final ArrayList k = k(j, r9, d, list, view);
        if (k == null || k.isEmpty()) {
            r9 = null;
        }
        j.a(q, view);
        final Object u = u(j, q, r9, l, a, h.b);
        if (d != null && k != null && (k.size() > 0 || list.size() > 0)) {
            final jf jf = new jf();
            g.b(d, jf);
            j.w(d, u, jf, new Runnable(g, d, jf) {
                public final g a;
                public final Fragment b;
                public final jf c;
                
                @Override
                public void run() {
                    this.a.a(this.b, this.c);
                }
            });
        }
        if (u != null) {
            final ArrayList list3 = new ArrayList();
            j.t(u, q, list3, r9, k, l, list2);
            y(j, viewGroup, a, view, list2, q, list3, r9, k);
            j.x((View)viewGroup, list2, r8);
            j.c(viewGroup, u);
            j.s(viewGroup, list2, r8);
        }
    }
    
    public static void o(final ViewGroup viewGroup, final h h, final View view, final r8 r8, final g g) {
        final Fragment a = h.a;
        final Fragment d = h.d;
        final l80 j = j(d, a);
        if (j == null) {
            return;
        }
        final boolean b = h.b;
        final boolean e = h.e;
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final Object q = q(j, a, b);
        final Object r9 = r(j, d, e);
        final Object m = m(j, viewGroup, view, r8, h, list2, list, q, r9);
        if (q == null && m == null && r9 == null) {
            return;
        }
        final ArrayList k = k(j, r9, d, list2, view);
        final ArrayList i = k(j, q, a, list, view);
        A(i, 4);
        final Object u = u(j, q, r9, m, a, b);
        if (d != null && k != null && (k.size() > 0 || list2.size() > 0)) {
            final jf jf = new jf();
            g.b(d, jf);
            j.w(d, u, jf, new Runnable(g, d, jf) {
                public final g a;
                public final Fragment b;
                public final jf c;
                
                @Override
                public void run() {
                    this.a.a(this.b, this.c);
                }
            });
        }
        if (u != null) {
            v(j, r9, d, k);
            final ArrayList o = j.o(list);
            j.t(u, q, i, r9, k, m, list);
            j.c(viewGroup, u);
            j.y((View)viewGroup, list2, list, o, r8);
            A(i, 0);
            j.A(m, list2, list);
        }
    }
    
    public static h p(final h h, final SparseArray sparseArray, final int n) {
        h h2 = h;
        if (h == null) {
            h2 = new h();
            sparseArray.put(n, (Object)h2);
        }
        return h2;
    }
    
    public static Object q(final l80 l80, final Fragment fragment, final boolean b) {
        if (fragment == null) {
            return null;
        }
        Object o;
        if (b) {
            o = fragment.getReenterTransition();
        }
        else {
            o = fragment.getEnterTransition();
        }
        return l80.g(o);
    }
    
    public static Object r(final l80 l80, final Fragment fragment, final boolean b) {
        if (fragment == null) {
            return null;
        }
        Object o;
        if (b) {
            o = fragment.getReturnTransition();
        }
        else {
            o = fragment.getExitTransition();
        }
        return l80.g(o);
    }
    
    public static View s(final r8 r8, final h h, final Object o, final boolean b) {
        final a c = h.c;
        if (o != null && r8 != null) {
            final ArrayList p4 = c.p;
            if (p4 != null && !p4.isEmpty()) {
                ArrayList list;
                if (b) {
                    list = c.p;
                }
                else {
                    list = c.q;
                }
                return (View)r8.get(list.get(0));
            }
        }
        return null;
    }
    
    public static Object t(final l80 l80, final Fragment fragment, final Fragment fragment2, final boolean b) {
        if (fragment != null && fragment2 != null) {
            Object o;
            if (b) {
                o = fragment2.getSharedElementReturnTransition();
            }
            else {
                o = fragment.getSharedElementEnterTransition();
            }
            return l80.B(l80.g(o));
        }
        return null;
    }
    
    public static Object u(final l80 l80, final Object o, final Object o2, final Object o3, final Fragment fragment, final boolean b) {
        boolean b2;
        if (o != null && o2 != null && fragment != null) {
            if (b) {
                b2 = fragment.getAllowReturnTransitionOverlap();
            }
            else {
                b2 = fragment.getAllowEnterTransitionOverlap();
            }
        }
        else {
            b2 = true;
        }
        Object o4;
        if (b2) {
            o4 = l80.n(o2, o, o3);
        }
        else {
            o4 = l80.m(o2, o, o3);
        }
        return o4;
    }
    
    public static void v(final l80 l80, final Object o, final Fragment fragment, final ArrayList list) {
        if (fragment != null && o != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            l80.r(o, fragment.getView(), list);
            s11.a((View)fragment.mContainer, new Runnable(list) {
                public final ArrayList a;
                
                @Override
                public void run() {
                    r.A(this.a, 4);
                }
            });
        }
    }
    
    public static l80 w() {
        try {
            return m80.class.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public static void x(final r8 r8, final r8 r9) {
        for (int i = r8.size() - 1; i >= 0; --i) {
            if (!r9.containsKey(r8.m(i))) {
                r8.k(i);
            }
        }
    }
    
    public static void y(final l80 l80, final ViewGroup viewGroup, final Fragment fragment, final View view, final ArrayList list, final Object o, final ArrayList list2, final Object o2, final ArrayList list3) {
        s11.a((View)viewGroup, new Runnable(o, l80, view, fragment, list, list2, list3, o2) {
            public final Object a;
            public final l80 b;
            public final View c;
            public final Fragment d;
            public final ArrayList e;
            public final ArrayList f;
            public final ArrayList g;
            public final Object h;
            
            @Override
            public void run() {
                final Object a = this.a;
                if (a != null) {
                    this.b.p(a, this.c);
                    this.f.addAll(r.k(this.b, this.a, this.d, this.e, this.c));
                }
                if (this.g != null) {
                    if (this.h != null) {
                        final ArrayList<View> list = new ArrayList<View>();
                        list.add(this.c);
                        this.b.q(this.h, this.g, list);
                    }
                    this.g.clear();
                    this.g.add(this.c);
                }
            }
        });
    }
    
    public static void z(final l80 l80, final Object o, final Object o2, final r8 r8, final boolean b, final a a) {
        final ArrayList p6 = a.p;
        if (p6 != null && !p6.isEmpty()) {
            ArrayList list;
            if (b) {
                list = a.q;
            }
            else {
                list = a.p;
            }
            final View view = (View)r8.get(list.get(0));
            l80.v(o, view);
            if (o2 != null) {
                l80.v(o2, view);
            }
        }
    }
    
    public interface g
    {
        void a(final Fragment p0, final jf p1);
        
        void b(final Fragment p0, final jf p1);
    }
    
    public static class h
    {
        public Fragment a;
        public boolean b;
        public a c;
        public Fragment d;
        public boolean e;
        public a f;
    }
}

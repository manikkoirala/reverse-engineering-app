// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.View;
import androidx.lifecycle.Lifecycle;
import java.lang.reflect.Modifier;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class q
{
    public final g a;
    public final ClassLoader b;
    public ArrayList c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public boolean i;
    public boolean j;
    public String k;
    public int l;
    public CharSequence m;
    public int n;
    public CharSequence o;
    public ArrayList p;
    public ArrayList q;
    public boolean r;
    public ArrayList s;
    
    public q(final g a, final ClassLoader b) {
        this.c = new ArrayList();
        this.j = true;
        this.r = false;
        this.a = a;
        this.b = b;
    }
    
    public q b(final int n, final Fragment fragment, final String s) {
        this.m(n, fragment, s, 1);
        return this;
    }
    
    public q c(final ViewGroup mContainer, final Fragment fragment, final String s) {
        fragment.mContainer = mContainer;
        return this.b(((View)mContainer).getId(), fragment, s);
    }
    
    public q d(final Fragment fragment, final String s) {
        this.m(0, fragment, s, 1);
        return this;
    }
    
    public void e(final a e) {
        this.c.add(e);
        e.c = this.d;
        e.d = this.e;
        e.e = this.f;
        e.f = this.g;
    }
    
    public q f(final Fragment fragment) {
        this.e(new a(7, fragment));
        return this;
    }
    
    public abstract int g();
    
    public abstract int h();
    
    public abstract void i();
    
    public abstract void j();
    
    public q k(final Fragment fragment) {
        this.e(new a(6, fragment));
        return this;
    }
    
    public q l() {
        if (!this.i) {
            this.j = false;
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }
    
    public void m(final int mContainerId, final Fragment obj, final String str, final int n) {
        final Class<? extends Fragment> class1 = obj.getClass();
        final int modifiers = class1.getModifiers();
        if (!class1.isAnonymousClass() && Modifier.isPublic(modifiers) && (!class1.isMemberClass() || Modifier.isStatic(modifiers))) {
            if (str != null) {
                final String mTag = obj.mTag;
                if (mTag != null && !str.equals(mTag)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Can't change tag of fragment ");
                    sb.append(obj);
                    sb.append(": was ");
                    sb.append(obj.mTag);
                    sb.append(" now ");
                    sb.append(str);
                    throw new IllegalStateException(sb.toString());
                }
                obj.mTag = str;
            }
            if (mContainerId != 0) {
                if (mContainerId == -1) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Can't add fragment ");
                    sb2.append(obj);
                    sb2.append(" with tag ");
                    sb2.append(str);
                    sb2.append(" to container view with no id");
                    throw new IllegalArgumentException(sb2.toString());
                }
                final int mFragmentId = obj.mFragmentId;
                if (mFragmentId != 0 && mFragmentId != mContainerId) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Can't change container ID of fragment ");
                    sb3.append(obj);
                    sb3.append(": was ");
                    sb3.append(obj.mFragmentId);
                    sb3.append(" now ");
                    sb3.append(mContainerId);
                    throw new IllegalStateException(sb3.toString());
                }
                obj.mFragmentId = mContainerId;
                obj.mContainerId = mContainerId;
            }
            this.e(new a(n, obj));
            return;
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("Fragment ");
        sb4.append(class1.getCanonicalName());
        sb4.append(" must be a public static class to be  properly recreated from instance state.");
        throw new IllegalStateException(sb4.toString());
    }
    
    public q n(final Fragment fragment) {
        this.e(new a(3, fragment));
        return this;
    }
    
    public q o(final int n, final Fragment fragment) {
        return this.p(n, fragment, null);
    }
    
    public q p(final int n, final Fragment fragment, final String s) {
        if (n != 0) {
            this.m(n, fragment, s, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }
    
    public q q(final Fragment fragment, final Lifecycle.State state) {
        this.e(new a(10, fragment, state));
        return this;
    }
    
    public q r(final boolean r) {
        this.r = r;
        return this;
    }
    
    public static final class a
    {
        public int a;
        public Fragment b;
        public int c;
        public int d;
        public int e;
        public int f;
        public Lifecycle.State g;
        public Lifecycle.State h;
        
        public a() {
        }
        
        public a(final int a, final Fragment b) {
            this.a = a;
            this.b = b;
            final Lifecycle.State resumed = Lifecycle.State.RESUMED;
            this.g = resumed;
            this.h = resumed;
        }
        
        public a(final int a, final Fragment b, final Lifecycle.State h) {
            this.a = a;
            this.b = b;
            this.g = b.mMaxState;
            this.h = h;
        }
    }
}

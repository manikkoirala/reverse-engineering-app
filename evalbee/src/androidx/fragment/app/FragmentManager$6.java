// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.f;

class FragmentManager$6 implements f
{
    public final String a;
    public final Lifecycle b;
    public final k c;
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        if (event == Lifecycle.Event.ON_START && k.a(this.c).get(this.a) != null) {
            throw null;
        }
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.b.c(this);
            k.b(this.c).remove(this.a);
        }
    }
}

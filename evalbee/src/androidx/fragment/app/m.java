// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import android.util.Log;
import java.util.HashMap;
import androidx.lifecycle.o;

public final class m extends y32
{
    public static final o.b k;
    public final HashMap d;
    public final HashMap e;
    public final HashMap f;
    public final boolean g;
    public boolean h;
    public boolean i;
    public boolean j;
    
    static {
        k = new o.b() {
            @Override
            public y32 b(final Class clazz) {
                return new m(true);
            }
        };
    }
    
    public m(final boolean g) {
        this.d = new HashMap();
        this.e = new HashMap();
        this.f = new HashMap();
        this.h = false;
        this.i = false;
        this.j = false;
        this.g = g;
    }
    
    public static m j(final b42 b42) {
        return (m)new o(b42, m.k).a(m.class);
    }
    
    @Override
    public void d() {
        if (androidx.fragment.app.k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onCleared called for ");
            sb.append(this);
            Log.d("FragmentManager", sb.toString());
        }
        this.h = true;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && m.class == o.getClass()) {
            final m m = (m)o;
            if (!this.d.equals(m.d) || !this.e.equals(m.e) || !this.f.equals(m.f)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public void f(final Fragment fragment) {
        if (this.j) {
            if (androidx.fragment.app.k.F0(2)) {
                Log.v("FragmentManager", "Ignoring addRetainedFragment as the state is already saved");
            }
            return;
        }
        if (this.d.containsKey(fragment.mWho)) {
            return;
        }
        this.d.put(fragment.mWho, fragment);
        if (androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Updating retained Fragments: Added ");
            sb.append(fragment);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    public void g(final Fragment obj) {
        if (androidx.fragment.app.k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Clearing non-config state for ");
            sb.append(obj);
            Log.d("FragmentManager", sb.toString());
        }
        final m m = this.e.get(obj.mWho);
        if (m != null) {
            m.d();
            this.e.remove(obj.mWho);
        }
        final b42 b42 = this.f.get(obj.mWho);
        if (b42 != null) {
            b42.a();
            this.f.remove(obj.mWho);
        }
    }
    
    public Fragment h(final String key) {
        return this.d.get(key);
    }
    
    @Override
    public int hashCode() {
        return (this.d.hashCode() * 31 + this.e.hashCode()) * 31 + this.f.hashCode();
    }
    
    public m i(final Fragment fragment) {
        m value;
        if ((value = this.e.get(fragment.mWho)) == null) {
            value = new m(this.g);
            this.e.put(fragment.mWho, value);
        }
        return value;
    }
    
    public Collection k() {
        return new ArrayList(this.d.values());
    }
    
    public b42 l(final Fragment fragment) {
        b42 value;
        if ((value = this.f.get(fragment.mWho)) == null) {
            value = new b42();
            this.f.put(fragment.mWho, value);
        }
        return value;
    }
    
    public boolean m() {
        return this.h;
    }
    
    public void n(final Fragment obj) {
        if (this.j) {
            if (androidx.fragment.app.k.F0(2)) {
                Log.v("FragmentManager", "Ignoring removeRetainedFragment as the state is already saved");
            }
            return;
        }
        if (this.d.remove(obj.mWho) != null && androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Updating retained Fragments: Removed ");
            sb.append(obj);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    public void o(final boolean j) {
        this.j = j;
    }
    
    public boolean p(final Fragment fragment) {
        if (!this.d.containsKey(fragment.mWho)) {
            return true;
        }
        if (this.g) {
            return this.h;
        }
        return this.i ^ true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        final Iterator iterator = this.d.values().iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next());
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        final Iterator iterator2 = this.e.keySet().iterator();
        while (iterator2.hasNext()) {
            sb.append((String)iterator2.next());
            if (iterator2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        final Iterator iterator3 = this.f.keySet().iterator();
        while (iterator3.hasNext()) {
            sb.append((String)iterator3.next());
            if (iterator3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}

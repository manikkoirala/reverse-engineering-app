// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.graphics.Rect;
import java.util.HashMap;
import android.content.Context;
import android.view.animation.Animation$AnimationListener;
import android.view.animation.Animation;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.util.Log;
import android.view.View;
import java.util.Map;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import android.view.ViewGroup;

public class c extends SpecialEffectsController
{
    public c(final ViewGroup viewGroup) {
        super(viewGroup);
    }
    
    @Override
    public void f(final List c, final boolean b) {
        final Iterator iterator = c.iterator();
        Operation operation = null;
        Operation operation2 = null;
        while (iterator.hasNext()) {
            final Operation operation3 = (Operation)iterator.next();
            final Operation.State from = Operation.State.from(operation3.f().mView);
            final int n = c$a.a[operation3.e().ordinal()];
            if (n != 1 && n != 2 && n != 3) {
                if (n != 4) {
                    continue;
                }
                if (from == State.VISIBLE) {
                    continue;
                }
                operation2 = operation3;
            }
            else {
                if (from != State.VISIBLE || operation != null) {
                    continue;
                }
                operation = operation3;
            }
        }
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final ArrayList list3 = new ArrayList(c);
        for (final Operation operation4 : c) {
            final jf jf = new jf();
            operation4.j(jf);
            list.add(new k(operation4, jf, b));
            final jf jf2 = new jf();
            operation4.j(jf2);
            boolean b2 = false;
            Label_0251: {
                if (b) {
                    if (operation4 != operation) {
                        break Label_0251;
                    }
                }
                else if (operation4 != operation2) {
                    break Label_0251;
                }
                b2 = true;
            }
            list2.add(new m(operation4, jf2, b, b2));
            operation4.a(new Runnable(this, list3, operation4) {
                public final List a;
                public final Operation b;
                public final c c;
                
                @Override
                public void run() {
                    if (this.a.contains(this.b)) {
                        this.a.remove(this.b);
                        this.c.s(this.b);
                    }
                }
            });
        }
        final Map x = this.x(list2, list3, b, operation, operation2);
        this.w(list, list3, x.containsValue(Boolean.TRUE), x);
        final Iterator iterator3 = list3.iterator();
        while (iterator3.hasNext()) {
            this.s((Operation)iterator3.next());
        }
        list3.clear();
    }
    
    public void s(final Operation operation) {
        operation.e().applyState(operation.f().mView);
    }
    
    public void t(final ArrayList list, View child) {
        if (child instanceof ViewGroup) {
            final ViewGroup e = (ViewGroup)child;
            if (s32.a(e)) {
                if (!list.contains(child)) {
                    list.add(e);
                }
            }
            else {
                for (int childCount = e.getChildCount(), i = 0; i < childCount; ++i) {
                    child = e.getChildAt(i);
                    if (child.getVisibility() == 0) {
                        this.t(list, child);
                    }
                }
            }
        }
        else if (!list.contains(child)) {
            list.add(child);
        }
    }
    
    public void u(final Map map, final View view) {
        final String j = o32.J(view);
        if (j != null) {
            map.put(j, view);
        }
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = viewGroup.getChildAt(i);
                if (child.getVisibility() == 0) {
                    this.u(map, child);
                }
            }
        }
    }
    
    public void v(final r8 r8, final Collection collection) {
        final Iterator iterator = r8.entrySet().iterator();
        while (iterator.hasNext()) {
            if (!collection.contains(o32.J(((Map.Entry<K, View>)iterator.next()).getValue()))) {
                iterator.remove();
            }
        }
    }
    
    public final void w(final List list, final List list2, final boolean b, final Map map) {
        final ViewGroup m = this.m();
        final Context context = ((View)m).getContext();
        final ArrayList list3 = new ArrayList();
        final Iterator iterator = list.iterator();
        boolean b2 = false;
        while (iterator.hasNext()) {
            final k e = (k)iterator.next();
            if (!((l)e).d()) {
                final f.d e2 = e.e(context);
                if (e2 != null) {
                    final Animator b3 = e2.b;
                    if (b3 == null) {
                        list3.add(e);
                        continue;
                    }
                    final Operation b4 = ((l)e).b();
                    final Fragment f = b4.f();
                    if (Boolean.TRUE.equals(map.get(b4))) {
                        if (androidx.fragment.app.k.F0(2)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Ignoring Animator set on ");
                            sb.append(f);
                            sb.append(" as this Fragment was involved in a Transition.");
                            Log.v("FragmentManager", sb.toString());
                        }
                        ((l)e).a();
                        continue;
                    }
                    final boolean b5 = b4.e() == State.GONE;
                    if (b5) {
                        list2.remove(b4);
                    }
                    final View mView = f.mView;
                    m.startViewTransition(mView);
                    b3.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, m, mView, b5, b4, e) {
                        public final ViewGroup a;
                        public final View b;
                        public final boolean c;
                        public final Operation d;
                        public final k e;
                        public final c f;
                        
                        public void onAnimationEnd(final Animator animator) {
                            this.a.endViewTransition(this.b);
                            if (this.c) {
                                this.d.e().applyState(this.b);
                            }
                            ((l)this.e).a();
                        }
                    });
                    b3.setTarget((Object)mView);
                    b3.start();
                    ((l)e).c().c((jf.b)new jf.b(this, b3) {
                        public final Animator a;
                        public final c b;
                        
                        @Override
                        public void a() {
                            this.a.end();
                        }
                    });
                    b2 = true;
                    continue;
                }
            }
            ((l)e).a();
        }
        for (final k k : list3) {
            final Operation b6 = ((l)k).b();
            final Fragment f2 = b6.f();
            Label_0397: {
                StringBuilder sb2;
                String str;
                if (b) {
                    if (!androidx.fragment.app.k.F0(2)) {
                        break Label_0397;
                    }
                    sb2 = new StringBuilder();
                    sb2.append("Ignoring Animation set on ");
                    sb2.append(f2);
                    str = " as Animations cannot run alongside Transitions.";
                }
                else {
                    if (!b2) {
                        final View mView2 = f2.mView;
                        final Animation animation = (Animation)l71.g(((f.d)l71.g(k.e(context))).a);
                        if (b6.e() != State.REMOVED) {
                            mView2.startAnimation(animation);
                            ((l)k).a();
                        }
                        else {
                            m.startViewTransition(mView2);
                            final f.e e3 = new f.e(animation, m, mView2);
                            ((Animation)e3).setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener(this, m, mView2, k) {
                                public final ViewGroup a;
                                public final View b;
                                public final k c;
                                public final c d;
                                
                                public void onAnimationEnd(final Animation animation) {
                                    ((View)this.a).post((Runnable)new Runnable(this) {
                                        public final c$e a;
                                        
                                        @Override
                                        public void run() {
                                            final Animation$AnimationListener a = (Animation$AnimationListener)this.a;
                                            a.a.endViewTransition(a.b);
                                            ((l)this.a.c).a();
                                        }
                                    });
                                }
                                
                                public void onAnimationRepeat(final Animation animation) {
                                }
                                
                                public void onAnimationStart(final Animation animation) {
                                }
                            });
                            mView2.startAnimation((Animation)e3);
                        }
                        ((l)k).c().c((jf.b)new jf.b(this, mView2, m, k) {
                            public final View a;
                            public final ViewGroup b;
                            public final k c;
                            public final c d;
                            
                            @Override
                            public void a() {
                                this.a.clearAnimation();
                                this.b.endViewTransition(this.a);
                                ((l)this.c).a();
                            }
                        });
                        continue;
                    }
                    if (!androidx.fragment.app.k.F0(2)) {
                        break Label_0397;
                    }
                    sb2 = new StringBuilder();
                    sb2.append("Ignoring Animation set on ");
                    sb2.append(f2);
                    str = " as Animations cannot run alongside Animators.";
                }
                sb2.append(str);
                Log.v("FragmentManager", sb2.toString());
            }
            ((l)k).a();
        }
    }
    
    public final Map x(final List list, final List list2, final boolean b, final Operation operation, final Operation operation2) {
        Operation operation3 = operation;
        Operation operation4 = operation2;
        final HashMap hashMap = new HashMap();
        final Iterator iterator = list.iterator();
        l80 l80 = null;
        while (iterator.hasNext()) {
            final m m = (m)iterator.next();
            if (((l)m).d()) {
                continue;
            }
            final l80 e = m.e();
            if (l80 == null) {
                l80 = e;
            }
            else {
                if (e == null) {
                    continue;
                }
                if (l80 == e) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Mixing framework transitions and AndroidX transitions is not allowed. Fragment ");
                sb.append(((l)m).b().f());
                sb.append(" returned Transition ");
                sb.append(m.h());
                sb.append(" which uses a different Transition  type than other Fragments.");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (l80 == null) {
            for (final m i : list) {
                hashMap.put(((l)i).b(), Boolean.FALSE);
                ((l)i).a();
            }
            return hashMap;
        }
        final View view = new View(((View)this.m()).getContext());
        final Rect rect = new Rect();
        final ArrayList list3 = new ArrayList();
        final ArrayList list4 = new ArrayList();
        final r8 r8 = new r8();
        final Iterator iterator3 = list.iterator();
        Object o = null;
        final View view2 = null;
        int n = 0;
        final l80 l81 = l80;
        View view3 = view2;
        while (iterator3.hasNext()) {
            final m j = (m)iterator3.next();
            if (j.i() && operation3 != null && operation4 != null) {
                final Object b2 = l81.B(l81.g(j.g()));
                final ArrayList<String> sharedElementSourceNames = operation2.f().getSharedElementSourceNames();
                final ArrayList<String> sharedElementSourceNames2 = operation.f().getSharedElementSourceNames();
                final ArrayList<String> sharedElementTargetNames = operation.f().getSharedElementTargetNames();
                for (int k = 0; k < sharedElementTargetNames.size(); ++k) {
                    final int index = sharedElementSourceNames.indexOf(sharedElementTargetNames.get(k));
                    if (index != -1) {
                        sharedElementSourceNames.set(index, sharedElementSourceNames2.get(k));
                    }
                }
                final ArrayList<String> sharedElementTargetNames2 = operation2.f().getSharedElementTargetNames();
                final Fragment f = operation.f();
                if (!b) {
                    f.getExitTransitionCallback();
                    operation2.f().getEnterTransitionCallback();
                }
                else {
                    f.getEnterTransitionCallback();
                    operation2.f().getExitTransitionCallback();
                }
                for (int size = sharedElementSourceNames.size(), n2 = 0; n2 < size; ++n2) {
                    r8.put(sharedElementSourceNames.get(n2), sharedElementTargetNames2.get(n2));
                }
                final r8 r9 = new r8();
                this.u(r9, operation.f().mView);
                r9.o(sharedElementSourceNames);
                r8.o(r9.keySet());
                final r8 r10 = new r8();
                this.u(r10, operation2.f().mView);
                r10.o(sharedElementTargetNames2);
                r10.o(r8.values());
                r.x(r8, r10);
                this.v(r9, r8.keySet());
                this.v(r10, r8.values());
                if (r8.isEmpty()) {
                    list3.clear();
                    list4.clear();
                    o = null;
                }
                else {
                    r.f(operation2.f(), operation.f(), b, r9, true);
                    s11.a((View)this.m(), new Runnable(this, operation2, operation, b, r10) {
                        public final Operation a;
                        public final Operation b;
                        public final boolean c;
                        public final r8 d;
                        public final c e;
                        
                        @Override
                        public void run() {
                            r.f(this.a.f(), this.b.f(), this.c, this.d, false);
                        }
                    });
                    list3.addAll(r9.values());
                    if (!sharedElementSourceNames.isEmpty()) {
                        view3 = (View)r9.get(sharedElementSourceNames.get(0));
                        l81.v(b2, view3);
                    }
                    list4.addAll(r10.values());
                    int n3 = n;
                    if (!sharedElementTargetNames2.isEmpty()) {
                        final View view4 = (View)r10.get(sharedElementTargetNames2.get(0));
                        n3 = n;
                        if (view4 != null) {
                            s11.a((View)this.m(), new Runnable(this, l81, view4, rect) {
                                public final l80 a;
                                public final View b;
                                public final Rect c;
                                public final c d;
                                
                                @Override
                                public void run() {
                                    this.a.k(this.b, this.c);
                                }
                            });
                            n3 = 1;
                        }
                    }
                    l81.z(b2, view, list3);
                    l81.t(b2, null, null, null, null, b2, list4);
                    final Boolean true = Boolean.TRUE;
                    operation3 = operation;
                    hashMap.put(operation3, true);
                    operation4 = operation2;
                    hashMap.put(operation4, true);
                    o = b2;
                    n = n3;
                }
            }
        }
        final View view5 = view3;
        final ArrayList c = list4;
        final ArrayList list5 = list3;
        final ArrayList list6 = new ArrayList();
        final Iterator iterator4 = list.iterator();
        final Object o2 = null;
        Object n4 = null;
        final View view6 = view5;
        Object n5 = o2;
        final View view7 = view;
        final ArrayList c2 = list5;
        while (iterator4.hasNext()) {
            final m m2 = (m)iterator4.next();
            if (((l)m2).d()) {
                hashMap.put(((l)m2).b(), Boolean.FALSE);
                ((l)m2).a();
            }
            else {
                final Object g = l81.g(m2.h());
                final Operation b3 = ((l)m2).b();
                final boolean b4 = o != null && (b3 == operation3 || b3 == operation4);
                if (g == null) {
                    if (b4) {
                        continue;
                    }
                    hashMap.put(b3, Boolean.FALSE);
                    ((l)m2).a();
                }
                else {
                    final ArrayList list7 = new ArrayList();
                    this.t(list7, b3.f().mView);
                    if (b4) {
                        if (b3 == operation3) {
                            list7.removeAll(c2);
                        }
                        else {
                            list7.removeAll(c);
                        }
                    }
                    if (list7.isEmpty()) {
                        l81.a(g, view7);
                    }
                    else {
                        l81.b(g, list7);
                        l81.t(g, g, list7, null, null, null, null);
                        if (b3.e() == State.GONE) {
                            list2.remove(b3);
                            final ArrayList list8 = new ArrayList(list7);
                            list8.remove(b3.f().mView);
                            l81.r(g, b3.f().mView, list8);
                            s11.a((View)this.m(), new Runnable(this, list7) {
                                public final ArrayList a;
                                public final c b;
                                
                                @Override
                                public void run() {
                                    r.A(this.a, 4);
                                }
                            });
                        }
                    }
                    if (b3.e() == State.VISIBLE) {
                        list6.addAll(list7);
                        if (n != 0) {
                            l81.u(g, rect);
                        }
                    }
                    else {
                        l81.v(g, view6);
                    }
                    hashMap.put(b3, Boolean.TRUE);
                    if (m2.j()) {
                        n4 = l81.n(n4, g, null);
                    }
                    else {
                        n5 = l81.n(n5, g, null);
                    }
                }
            }
        }
        final Object m3 = l81.m(n4, n5, o);
        for (final m m4 : list) {
            if (((l)m4).d()) {
                continue;
            }
            final Object h = m4.h();
            final Operation b5 = ((l)m4).b();
            final boolean b6 = o != null && (b5 == operation3 || b5 == operation4);
            if (h == null && !b6) {
                continue;
            }
            if (!o32.U((View)this.m())) {
                if (androidx.fragment.app.k.F0(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("SpecialEffectsController: Container ");
                    sb2.append(this.m());
                    sb2.append(" has not been laid out. Completing operation ");
                    sb2.append(b5);
                    Log.v("FragmentManager", sb2.toString());
                }
                ((l)m4).a();
            }
            else {
                l81.w(((l)m4).b().f(), m3, ((l)m4).c(), new Runnable(this, m4) {
                    public final m a;
                    public final c b;
                    
                    @Override
                    public void run() {
                        ((l)this.a).a();
                    }
                });
            }
        }
        if (!o32.U((View)this.m())) {
            return hashMap;
        }
        r.A(list6, 4);
        final ArrayList o3 = l81.o(c);
        l81.c(this.m(), m3);
        l81.y((View)this.m(), c2, c, o3, r8);
        r.A(list6, 0);
        l81.A(o, c2, c);
        return hashMap;
    }
    
    public static class k extends l
    {
        public boolean c;
        public boolean d;
        public f.d e;
        
        public k(final Operation operation, final jf jf, final boolean c) {
            super(operation, jf);
            this.d = false;
            this.c = c;
        }
        
        public f.d e(final Context context) {
            if (this.d) {
                return this.e;
            }
            final f.d c = f.c(context, ((l)this).b().f(), ((l)this).b().e() == State.VISIBLE, this.c);
            this.e = c;
            this.d = true;
            return c;
        }
    }
    
    public abstract static class l
    {
        public final Operation a;
        public final jf b;
        
        public l(final Operation a, final jf b) {
            this.a = a;
            this.b = b;
        }
        
        public void a() {
            this.a.d(this.b);
        }
        
        public Operation b() {
            return this.a;
        }
        
        public jf c() {
            return this.b;
        }
        
        public boolean d() {
            final Operation.State from = Operation.State.from(this.a.f().mView);
            final Operation.State e = this.a.e();
            if (from != e) {
                final Operation.State visible = State.VISIBLE;
                if (from == visible || e == visible) {
                    return false;
                }
            }
            return true;
        }
    }
    
    public static class m extends l
    {
        public final Object c;
        public final boolean d;
        public final Object e;
        
        public m(final Operation operation, final jf jf, final boolean b, final boolean b2) {
            super(operation, jf);
            boolean d;
            if (operation.e() == State.VISIBLE) {
                final Fragment f = operation.f();
                Object c;
                if (b) {
                    c = f.getReenterTransition();
                }
                else {
                    c = f.getEnterTransition();
                }
                this.c = c;
                final Fragment f2 = operation.f();
                if (b) {
                    d = f2.getAllowReturnTransitionOverlap();
                }
                else {
                    d = f2.getAllowEnterTransitionOverlap();
                }
            }
            else {
                final Fragment f3 = operation.f();
                Object c2;
                if (b) {
                    c2 = f3.getReturnTransition();
                }
                else {
                    c2 = f3.getExitTransition();
                }
                this.c = c2;
                d = true;
            }
            this.d = d;
            Object e;
            if (b2) {
                final Fragment f4 = operation.f();
                if (b) {
                    e = f4.getSharedElementReturnTransition();
                }
                else {
                    e = f4.getSharedElementEnterTransition();
                }
            }
            else {
                e = null;
            }
            this.e = e;
        }
        
        public l80 e() {
            l80 f = this.f(this.c);
            final l80 f2 = this.f(this.e);
            if (f != null && f2 != null && f != f2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Mixing framework transitions and AndroidX transitions is not allowed. Fragment ");
                sb.append(((l)this).b().f());
                sb.append(" returned Transition ");
                sb.append(this.c);
                sb.append(" which uses a different Transition  type than its shared element transition ");
                sb.append(this.e);
                throw new IllegalArgumentException(sb.toString());
            }
            if (f == null) {
                f = f2;
            }
            return f;
        }
        
        public final l80 f(final Object obj) {
            if (obj == null) {
                return null;
            }
            final l80 b = r.b;
            if (b != null && b.e(obj)) {
                return b;
            }
            final l80 c = r.c;
            if (c != null && c.e(obj)) {
                return c;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Transition ");
            sb.append(obj);
            sb.append(" for fragment ");
            sb.append(((l)this).b().f());
            sb.append(" is not a valid framework Transition or AndroidX Transition");
            throw new IllegalArgumentException(sb.toString());
        }
        
        public Object g() {
            return this.e;
        }
        
        public Object h() {
            return this.c;
        }
        
        public boolean i() {
            return this.e != null;
        }
        
        public boolean j() {
            return this.d;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.content.Context;
import android.view.LayoutInflater$Factory2;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.Set;
import androidx.lifecycle.Lifecycle;
import androidx.activity.result.ActivityResultRegistry;
import java.io.Writer;
import java.util.List;
import android.os.Parcelable;
import java.util.Collection;
import java.util.HashSet;
import android.os.Looper;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.view.animation.Animation;
import android.content.IntentSender;
import android.content.Intent;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import java.util.Iterator;
import android.content.res.Configuration;
import android.view.View;
import android.util.Log;
import android.view.ViewGroup;
import android.os.Bundle;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.activity.OnBackPressedDispatcher;
import java.util.ArrayList;
import java.util.ArrayDeque;

public abstract class k
{
    public static boolean O = false;
    public static boolean P = true;
    public r2 A;
    public r2 B;
    public ArrayDeque C;
    public boolean D;
    public boolean E;
    public boolean F;
    public boolean G;
    public boolean H;
    public ArrayList I;
    public ArrayList J;
    public ArrayList K;
    public ArrayList L;
    public androidx.fragment.app.m M;
    public Runnable N;
    public final ArrayList a;
    public boolean b;
    public final p c;
    public ArrayList d;
    public ArrayList e;
    public final i f;
    public OnBackPressedDispatcher g;
    public final h11 h;
    public final AtomicInteger i;
    public final Map j;
    public final Map k;
    public ArrayList l;
    public Map m;
    public final r.g n;
    public final j o;
    public final CopyOnWriteArrayList p;
    public int q;
    public h r;
    public f80 s;
    public Fragment t;
    public Fragment u;
    public g v;
    public g w;
    public xo1 x;
    public xo1 y;
    public r2 z;
    
    public k() {
        this.a = new ArrayList();
        this.c = new p();
        this.f = new i(this);
        this.h = new h11(this, false) {
            public final k d;
            
            @Override
            public void d() {
                this.d.B0();
            }
        };
        this.i = new AtomicInteger();
        this.j = Collections.synchronizedMap(new HashMap<Object, Object>());
        this.k = Collections.synchronizedMap(new HashMap<Object, Object>());
        this.m = Collections.synchronizedMap(new HashMap<Object, Object>());
        this.n = new r.g(this) {
            public final k a;
            
            @Override
            public void a(final Fragment fragment, final jf jf) {
                if (!jf.b()) {
                    this.a.b1(fragment, jf);
                }
            }
            
            @Override
            public void b(final Fragment fragment, final jf jf) {
                this.a.f(fragment, jf);
            }
        };
        this.o = new j(this);
        this.p = new CopyOnWriteArrayList();
        this.q = -1;
        this.v = null;
        this.w = new g(this) {
            public final k b;
            
            @Override
            public Fragment a(final ClassLoader classLoader, final String s) {
                return this.b.t0().b(this.b.t0().f(), s, null);
            }
        };
        this.x = null;
        this.y = new xo1(this) {
            public final k a;
            
            @Override
            public SpecialEffectsController a(final ViewGroup viewGroup) {
                return new c(viewGroup);
            }
        };
        this.C = new ArrayDeque();
        this.N = new Runnable(this) {
            public final k a;
            
            @Override
            public void run() {
                this.a.a0(true);
            }
        };
    }
    
    public static boolean F0(final int n) {
        return k.O || Log.isLoggable("FragmentManager", n);
    }
    
    public static /* synthetic */ p c(final k k) {
        return k.c;
    }
    
    public static void c0(final ArrayList list, final ArrayList list2, int i, final int n) {
        while (i < n) {
            final a a = list.get(i);
            final boolean booleanValue = list2.get(i);
            boolean b = true;
            if (booleanValue) {
                a.s(-1);
                if (i != n - 1) {
                    b = false;
                }
                a.x(b);
            }
            else {
                a.s(1);
                a.w();
            }
            ++i;
        }
    }
    
    public static int h1(final int n) {
        int n2 = 8194;
        if (n != 4097) {
            if (n != 4099) {
                if (n != 8194) {
                    n2 = 0;
                }
                else {
                    n2 = 4097;
                }
            }
            else {
                n2 = 4099;
            }
        }
        return n2;
    }
    
    public static Fragment z0(final View view) {
        final Object tag = view.getTag(gb1.a);
        if (tag instanceof Fragment) {
            return (Fragment)tag;
        }
        return null;
    }
    
    public void A(final Configuration configuration) {
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performConfigurationChanged(configuration);
            }
        }
    }
    
    public b42 A0(final Fragment fragment) {
        return this.M.l(fragment);
    }
    
    public boolean B(final MenuItem menuItem) {
        if (this.q < 1) {
            return false;
        }
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null && fragment.performContextItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    public void B0() {
        this.a0(true);
        if (this.h.g()) {
            this.X0();
        }
        else {
            this.g.k();
        }
    }
    
    public void C() {
        this.E = false;
        this.F = false;
        this.M.o(false);
        this.S(1);
    }
    
    public void C0(final Fragment obj) {
        if (F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("hide: ");
            sb.append(obj);
            Log.v("FragmentManager", sb.toString());
        }
        if (!obj.mHidden) {
            obj.mHidden = true;
            obj.mHiddenChanged ^= true;
            this.n1(obj);
        }
    }
    
    public boolean D(final Menu menu, final MenuInflater menuInflater) {
        final int q = this.q;
        int i = 0;
        if (q < 1) {
            return false;
        }
        final Iterator iterator = this.c.n().iterator();
        ArrayList e = null;
        boolean b = false;
        while (iterator.hasNext()) {
            final Fragment e2 = (Fragment)iterator.next();
            if (e2 != null && this.H0(e2) && e2.performCreateOptionsMenu(menu, menuInflater)) {
                ArrayList list;
                if ((list = e) == null) {
                    list = new ArrayList();
                }
                list.add(e2);
                b = true;
                e = list;
            }
        }
        if (this.e != null) {
            while (i < this.e.size()) {
                final Fragment o = this.e.get(i);
                if (e == null || !e.contains(o)) {
                    o.onDestroyOptionsMenu();
                }
                ++i;
            }
        }
        this.e = e;
        return b;
    }
    
    public void D0(final Fragment fragment) {
        if (fragment.mAdded && this.G0(fragment)) {
            this.D = true;
        }
    }
    
    public void E() {
        this.a0(this.G = true);
        this.X();
        this.S(-1);
        this.r = null;
        this.s = null;
        this.t = null;
        if (this.g != null) {
            this.h.h();
            this.g = null;
        }
        final r2 z = this.z;
        if (z != null) {
            z.c();
            this.A.c();
            this.B.c();
        }
    }
    
    public boolean E0() {
        return this.G;
    }
    
    public void F() {
        this.S(1);
    }
    
    public void G() {
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performLowMemory();
            }
        }
    }
    
    public final boolean G0(final Fragment fragment) {
        return (fragment.mHasMenu && fragment.mMenuVisible) || fragment.mChildFragmentManager.o();
    }
    
    public void H(final boolean b) {
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performMultiWindowModeChanged(b);
            }
        }
    }
    
    public boolean H0(final Fragment fragment) {
        return fragment == null || fragment.isMenuVisible();
    }
    
    public void I(final Fragment fragment) {
        final Iterator iterator = this.p.iterator();
        while (iterator.hasNext()) {
            ((i80)iterator.next()).a(this, fragment);
        }
    }
    
    public boolean I0(final Fragment fragment) {
        boolean b = true;
        if (fragment == null) {
            return true;
        }
        final k mFragmentManager = fragment.mFragmentManager;
        if (!fragment.equals(mFragmentManager.x0()) || !this.I0(mFragmentManager.t)) {
            b = false;
        }
        return b;
    }
    
    public boolean J(final MenuItem menuItem) {
        if (this.q < 1) {
            return false;
        }
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null && fragment.performOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean J0(final int n) {
        return this.q >= n;
    }
    
    public void K(final Menu menu) {
        if (this.q < 1) {
            return;
        }
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performOptionsMenuClosed(menu);
            }
        }
    }
    
    public boolean K0() {
        return this.E || this.F;
    }
    
    public final void L(final Fragment fragment) {
        if (fragment != null && fragment.equals(this.g0(fragment.mWho))) {
            fragment.performPrimaryNavigationFragmentChanged();
        }
    }
    
    public void L0(final Fragment fragment, final String[] array, final int n) {
        if (this.B != null) {
            this.C.addLast(new l(fragment.mWho, n));
            this.B.a(array);
        }
        else {
            this.r.j(fragment, array, n);
        }
    }
    
    public void M() {
        this.S(5);
    }
    
    public void M0(final Fragment fragment, final Intent intent, final int n, final Bundle bundle) {
        if (this.z != null) {
            this.C.addLast(new l(fragment.mWho, n));
            if (intent != null && bundle != null) {
                intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundle);
            }
            this.z.a(intent);
        }
        else {
            this.r.m(fragment, intent, n, bundle);
        }
    }
    
    public void N(final boolean b) {
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.performPictureInPictureModeChanged(b);
            }
        }
    }
    
    public void N0(final Fragment fragment, final IntentSender intentSender, final int n, Intent obj, final int n2, final int n3, final int n4, final Bundle obj2) {
        if (this.A != null) {
            if (obj2 != null) {
                if (obj == null) {
                    obj = new Intent();
                    obj.putExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", true);
                }
                if (F0(2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ActivityOptions ");
                    sb.append(obj2);
                    sb.append(" were added to fillInIntent ");
                    sb.append(obj);
                    sb.append(" for fragment ");
                    sb.append(fragment);
                    Log.v("FragmentManager", sb.toString());
                }
                obj.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", obj2);
            }
            final yf0 a = new yf0.a(intentSender).b(obj).c(n3, n2).a();
            this.C.addLast(new l(fragment.mWho, n));
            if (F0(2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(fragment);
                sb2.append("is launching an IntentSender for result ");
                Log.v("FragmentManager", sb2.toString());
            }
            this.A.a(a);
        }
        else {
            this.r.n(fragment, intentSender, n, obj, n2, n3, n4, obj2);
        }
    }
    
    public boolean O(final Menu menu) {
        final int q = this.q;
        boolean b = false;
        if (q < 1) {
            return false;
        }
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null && this.H0(fragment) && fragment.performPrepareOptionsMenu(menu)) {
                b = true;
            }
        }
        return b;
    }
    
    public final void O0(final s8 s8) {
        for (int size = s8.size(), i = 0; i < size; ++i) {
            final Fragment fragment = (Fragment)s8.n(i);
            if (!fragment.mAdded) {
                final View requireView = fragment.requireView();
                fragment.mPostponedAlpha = requireView.getAlpha();
                requireView.setAlpha(0.0f);
            }
        }
    }
    
    public void P() {
        this.q1();
        this.L(this.u);
    }
    
    public void P0(final Fragment obj) {
        if (!this.c.c(obj.mWho)) {
            if (F0(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignoring moving ");
                sb.append(obj);
                sb.append(" to state ");
                sb.append(this.q);
                sb.append("since it is not added to ");
                sb.append(this);
                Log.d("FragmentManager", sb.toString());
            }
            return;
        }
        this.R0(obj);
        final View mView = obj.mView;
        if (mView != null && obj.mIsNewlyAdded && obj.mContainer != null) {
            final float mPostponedAlpha = obj.mPostponedAlpha;
            if (mPostponedAlpha > 0.0f) {
                mView.setAlpha(mPostponedAlpha);
            }
            obj.mPostponedAlpha = 0.0f;
            obj.mIsNewlyAdded = false;
            final f.d c = androidx.fragment.app.f.c(this.r.f(), obj, true, obj.getPopDirection());
            if (c != null) {
                final Animation a = c.a;
                if (a != null) {
                    obj.mView.startAnimation(a);
                }
                else {
                    c.b.setTarget((Object)obj.mView);
                    c.b.start();
                }
            }
        }
        if (obj.mHiddenChanged) {
            this.u(obj);
        }
    }
    
    public void Q() {
        this.E = false;
        this.F = false;
        this.M.o(false);
        this.S(7);
    }
    
    public void Q0(int q, final boolean b) {
        if (this.r == null && q != -1) {
            throw new IllegalStateException("No activity");
        }
        if (!b && q == this.q) {
            return;
        }
        this.q = q;
        if (androidx.fragment.app.k.P) {
            this.c.r();
        }
        else {
            final Iterator iterator = this.c.n().iterator();
            while (iterator.hasNext()) {
                this.P0((Fragment)iterator.next());
            }
            for (final androidx.fragment.app.o o : this.c.k()) {
                final Fragment k = o.k();
                if (!k.mIsNewlyAdded) {
                    this.P0(k);
                }
                if (k.mRemoving && !k.isInBackStack()) {
                    q = 1;
                }
                else {
                    q = 0;
                }
                if (q != 0) {
                    this.c.q(o);
                }
            }
        }
        this.p1();
        if (this.D) {
            final h r = this.r;
            if (r != null && this.q == 7) {
                r.o();
                this.D = false;
            }
        }
    }
    
    public void R() {
        this.E = false;
        this.F = false;
        this.M.o(false);
        this.S(5);
    }
    
    public void R0(final Fragment fragment) {
        this.S0(fragment, this.q);
    }
    
    public final void S(final int n) {
        try {
            this.b = true;
            this.c.d(n);
            this.Q0(n, false);
            if (androidx.fragment.app.k.P) {
                final Iterator iterator = this.r().iterator();
                while (iterator.hasNext()) {
                    ((SpecialEffectsController)iterator.next()).j();
                }
            }
            this.b = false;
            this.a0(true);
        }
        finally {
            this.b = false;
        }
    }
    
    public void S0(final Fragment obj, int min) {
        final androidx.fragment.app.o m = this.c.m(obj.mWho);
        final int n = 1;
        androidx.fragment.app.o o = m;
        if (m == null) {
            o = new androidx.fragment.app.o(this.o, this.c, obj);
            o.t(1);
        }
        int max = min;
        if (obj.mFromLayout) {
            max = min;
            if (obj.mInLayout) {
                max = min;
                if (obj.mState == 2) {
                    max = Math.max(min, 2);
                }
            }
        }
        min = Math.min(max, o.d());
        final int mState = obj.mState;
        int n2 = 0;
        Label_0681: {
            if (mState <= min) {
                if (mState < min && !this.m.isEmpty()) {
                    this.n(obj);
                }
                final int mState2 = obj.mState;
                Label_0226: {
                    Label_0216: {
                        Label_0206: {
                            Label_0186: {
                                if (mState2 != -1) {
                                    if (mState2 != 0) {
                                        if (mState2 == 1) {
                                            break Label_0186;
                                        }
                                        if (mState2 == 2) {
                                            break Label_0206;
                                        }
                                        if (mState2 == 4) {
                                            break Label_0216;
                                        }
                                        if (mState2 != 5) {
                                            n2 = min;
                                            break Label_0681;
                                        }
                                        break Label_0226;
                                    }
                                }
                                else if (min > -1) {
                                    o.c();
                                }
                                if (min > 0) {
                                    o.e();
                                }
                            }
                            if (min > -1) {
                                o.j();
                            }
                            if (min > 1) {
                                o.f();
                            }
                        }
                        if (min > 2) {
                            o.a();
                        }
                    }
                    if (min > 4) {
                        o.u();
                    }
                }
                if ((n2 = min) > 5) {
                    o.p();
                    n2 = min;
                }
            }
            else if (mState > (n2 = min)) {
                if (mState != 0) {
                    if (mState != 1) {
                        if (mState != 2) {
                            if (mState != 4) {
                                if (mState != 5) {
                                    if (mState != 7) {
                                        n2 = min;
                                        break Label_0681;
                                    }
                                    if (min < 7) {
                                        o.n();
                                    }
                                }
                                if (min < 5) {
                                    o.v();
                                }
                            }
                            if (min < 4) {
                                if (F0(3)) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("movefrom ACTIVITY_CREATED: ");
                                    sb.append(obj);
                                    Log.d("FragmentManager", sb.toString());
                                }
                                if (obj.mView != null && this.r.k(obj) && obj.mSavedViewState == null) {
                                    o.s();
                                }
                            }
                        }
                        if (min < 2) {
                            final View mView = obj.mView;
                            if (mView != null) {
                                final ViewGroup mContainer = obj.mContainer;
                                if (mContainer != null) {
                                    mContainer.endViewTransition(mView);
                                    obj.mView.clearAnimation();
                                    if (!obj.isRemovingParent()) {
                                        f.d c;
                                        if (this.q > -1 && !this.G && obj.mView.getVisibility() == 0 && obj.mPostponedAlpha >= 0.0f) {
                                            c = androidx.fragment.app.f.c(this.r.f(), obj, false, obj.getPopDirection());
                                        }
                                        else {
                                            c = null;
                                        }
                                        obj.mPostponedAlpha = 0.0f;
                                        final ViewGroup mContainer2 = obj.mContainer;
                                        final View mView2 = obj.mView;
                                        if (c != null) {
                                            androidx.fragment.app.f.a(obj, c, this.n);
                                        }
                                        mContainer2.removeView(mView2);
                                        if (F0(2)) {
                                            final StringBuilder sb2 = new StringBuilder();
                                            sb2.append("Removing view ");
                                            sb2.append(mView2);
                                            sb2.append(" for fragment ");
                                            sb2.append(obj);
                                            sb2.append(" from container ");
                                            sb2.append(mContainer2);
                                            Log.v("FragmentManager", sb2.toString());
                                        }
                                        if (mContainer2 != obj.mContainer) {
                                            return;
                                        }
                                    }
                                }
                            }
                            if (this.m.get(obj) == null) {
                                o.h();
                            }
                        }
                    }
                    if (min < 1) {
                        if (this.m.get(obj) != null) {
                            min = n;
                        }
                        else {
                            o.g();
                        }
                    }
                }
                if (min < 0) {
                    o.i();
                }
                n2 = min;
            }
        }
        if (obj.mState != n2) {
            if (F0(3)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("moveToState: Fragment state for ");
                sb3.append(obj);
                sb3.append(" not updated inline; expected state ");
                sb3.append(n2);
                sb3.append(" found ");
                sb3.append(obj.mState);
                Log.d("FragmentManager", sb3.toString());
            }
            obj.mState = n2;
        }
    }
    
    public void T() {
        this.F = true;
        this.M.o(true);
        this.S(4);
    }
    
    public void T0() {
        if (this.r == null) {
            return;
        }
        this.E = false;
        this.F = false;
        this.M.o(false);
        for (final Fragment fragment : this.c.n()) {
            if (fragment != null) {
                fragment.noteStateNotSaved();
            }
        }
    }
    
    public void U() {
        this.S(2);
    }
    
    public void U0(final FragmentContainerView mContainer) {
        for (final androidx.fragment.app.o o : this.c.k()) {
            final Fragment k = o.k();
            if (k.mContainerId == ((View)mContainer).getId()) {
                final View mView = k.mView;
                if (mView == null || mView.getParent() != null) {
                    continue;
                }
                k.mContainer = (ViewGroup)mContainer;
                o.b();
            }
        }
    }
    
    public final void V() {
        if (this.H) {
            this.H = false;
            this.p1();
        }
    }
    
    public void V0(final androidx.fragment.app.o o) {
        final Fragment k = o.k();
        if (k.mDeferStart) {
            if (this.b) {
                this.H = true;
                return;
            }
            k.mDeferStart = false;
            if (androidx.fragment.app.k.P) {
                o.m();
            }
            else {
                this.R0(k);
            }
        }
    }
    
    public void W(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("    ");
        final String string = sb.toString();
        this.c.e(s, fileDescriptor, printWriter, array);
        final ArrayList e = this.e;
        final int n = 0;
        if (e != null) {
            final int size = e.size();
            if (size > 0) {
                printWriter.print(s);
                printWriter.println("Fragments Created Menus:");
                for (int i = 0; i < size; ++i) {
                    final Fragment fragment = this.e.get(i);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(i);
                    printWriter.print(": ");
                    printWriter.println(fragment.toString());
                }
            }
        }
        final ArrayList d = this.d;
        if (d != null) {
            final int size2 = d.size();
            if (size2 > 0) {
                printWriter.print(s);
                printWriter.println("Back Stack:");
                for (int j = 0; j < size2; ++j) {
                    final a a = this.d.get(j);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(j);
                    printWriter.print(": ");
                    printWriter.println(a.toString());
                    a.u(string, printWriter);
                }
            }
        }
        printWriter.print(s);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Back Stack Index: ");
        sb2.append(this.i.get());
        printWriter.println(sb2.toString());
        synchronized (this.a) {
            final int size3 = this.a.size();
            if (size3 > 0) {
                printWriter.print(s);
                printWriter.println("Pending Actions:");
                for (int k = n; k < size3; ++k) {
                    final m x = this.a.get(k);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(k);
                    printWriter.print(": ");
                    printWriter.println(x);
                }
            }
            monitorexit(this.a);
            printWriter.print(s);
            printWriter.println("FragmentManager misc state:");
            printWriter.print(s);
            printWriter.print("  mHost=");
            printWriter.println(this.r);
            printWriter.print(s);
            printWriter.print("  mContainer=");
            printWriter.println(this.s);
            if (this.t != null) {
                printWriter.print(s);
                printWriter.print("  mParent=");
                printWriter.println(this.t);
            }
            printWriter.print(s);
            printWriter.print("  mCurState=");
            printWriter.print(this.q);
            printWriter.print(" mStateSaved=");
            printWriter.print(this.E);
            printWriter.print(" mStopped=");
            printWriter.print(this.F);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.G);
            if (this.D) {
                printWriter.print(s);
                printWriter.print("  mNeedMenuInvalidate=");
                printWriter.println(this.D);
            }
        }
    }
    
    public void W0(final int i, final int n) {
        if (i >= 0) {
            this.Y((m)new n(null, i, n), false);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Bad id: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void X() {
        if (androidx.fragment.app.k.P) {
            final Iterator iterator = this.r().iterator();
            while (iterator.hasNext()) {
                ((SpecialEffectsController)iterator.next()).j();
            }
        }
        else if (!this.m.isEmpty()) {
            for (final Fragment fragment : this.m.keySet()) {
                this.n(fragment);
                this.R0(fragment);
            }
        }
    }
    
    public boolean X0() {
        return this.Y0(null, -1, 0);
    }
    
    public void Y(final m e, final boolean b) {
        if (!b) {
            if (this.r == null) {
                if (this.G) {
                    throw new IllegalStateException("FragmentManager has been destroyed");
                }
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            }
            else {
                this.p();
            }
        }
        synchronized (this.a) {
            if (this.r != null) {
                this.a.add(e);
                this.j1();
                return;
            }
            if (b) {
                return;
            }
            throw new IllegalStateException("Activity has been destroyed");
        }
    }
    
    public final boolean Y0(final String s, final int n, final int n2) {
        this.a0(false);
        this.Z(true);
        final Fragment u = this.u;
        if (u != null && n < 0 && s == null && u.getChildFragmentManager().X0()) {
            return true;
        }
        final boolean z0 = this.Z0(this.I, this.J, s, n, n2);
        if (z0) {
            this.b = true;
            try {
                this.d1(this.I, this.J);
            }
            finally {
                this.q();
            }
        }
        this.q1();
        this.V();
        this.c.b();
        return z0;
    }
    
    public final void Z(final boolean b) {
        if (this.b) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        }
        if (this.r != null) {
            if (Looper.myLooper() == this.r.g().getLooper()) {
                if (!b) {
                    this.p();
                }
                if (this.I == null) {
                    this.I = new ArrayList();
                    this.J = new ArrayList();
                }
                this.b = true;
                try {
                    this.f0(null, null);
                    return;
                }
                finally {
                    this.b = false;
                }
            }
            throw new IllegalStateException("Must be called from main thread of fragment host");
        }
        if (this.G) {
            throw new IllegalStateException("FragmentManager has been destroyed");
        }
        throw new IllegalStateException("FragmentManager has not been attached to a host.");
    }
    
    public boolean Z0(final ArrayList list, final ArrayList list2, final String s, int index, int i) {
        final ArrayList d = this.d;
        if (d == null) {
            return false;
        }
        if (s == null && index < 0 && (i & 0x1) == 0x0) {
            index = d.size() - 1;
            if (index < 0) {
                return false;
            }
            list.add(this.d.remove(index));
            list2.add(Boolean.TRUE);
        }
        else {
            if (s == null && index < 0) {
                index = -1;
            }
            else {
                int j;
                for (j = d.size() - 1; j >= 0; --j) {
                    final a a = this.d.get(j);
                    if (s != null && s.equals(a.z())) {
                        break;
                    }
                    if (index >= 0 && index == a.v) {
                        break;
                    }
                }
                if (j < 0) {
                    return false;
                }
                int n = j;
                if ((i & 0x1) != 0x0) {
                    while (true) {
                        i = j - 1;
                        if ((n = i) < 0) {
                            break;
                        }
                        final a a2 = this.d.get(i);
                        if (s != null) {
                            j = i;
                            if (s.equals(a2.z())) {
                                continue;
                            }
                        }
                        n = i;
                        if (index < 0) {
                            break;
                        }
                        n = i;
                        if (index != a2.v) {
                            break;
                        }
                        j = i;
                    }
                }
                index = n;
            }
            if (index == this.d.size() - 1) {
                return false;
            }
            for (i = this.d.size() - 1; i > index; --i) {
                list.add(this.d.remove(i));
                list2.add(Boolean.TRUE);
            }
        }
        return true;
    }
    
    public boolean a0(boolean b) {
        this.Z(b);
        b = false;
        while (this.l0(this.I, this.J)) {
            b = true;
            this.b = true;
            try {
                this.d1(this.I, this.J);
                continue;
            }
            finally {
                this.q();
            }
            break;
        }
        this.q1();
        this.V();
        this.c.b();
        return b;
    }
    
    public final int a1(final ArrayList list, final ArrayList list2, final int n, final int n2, final s8 s8) {
        int i = n2 - 1;
        int n3 = n2;
        while (i >= n) {
            final a element = list.get(i);
            final boolean booleanValue = list2.get(i);
            final boolean b = element.D() && !element.B(list, i + 1, n2);
            int index = n3;
            if (b) {
                if (this.L == null) {
                    this.L = new ArrayList();
                }
                final o e = new o(element, booleanValue);
                this.L.add(e);
                element.F(e);
                if (booleanValue) {
                    element.w();
                }
                else {
                    element.x(false);
                }
                index = n3 - 1;
                if (i != index) {
                    list.remove(i);
                    list.add(index, element);
                }
                this.d(s8);
            }
            --i;
            n3 = index;
        }
        return n3;
    }
    
    public void b0(final m m, final boolean b) {
        if (b && (this.r == null || this.G)) {
            return;
        }
        this.Z(b);
        if (m.a(this.I, this.J)) {
            this.b = true;
            try {
                this.d1(this.I, this.J);
            }
            finally {
                this.q();
            }
        }
        this.q1();
        this.V();
        this.c.b();
    }
    
    public void b1(final Fragment fragment, final jf o) {
        final HashSet set = this.m.get(fragment);
        if (set != null && set.remove(o) && set.isEmpty()) {
            this.m.remove(fragment);
            if (fragment.mState < 5) {
                this.w(fragment);
                this.R0(fragment);
            }
        }
    }
    
    public void c1(final Fragment obj) {
        if (F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("remove: ");
            sb.append(obj);
            sb.append(" nesting=");
            sb.append(obj.mBackStackNesting);
            Log.v("FragmentManager", sb.toString());
        }
        final boolean inBackStack = obj.isInBackStack();
        if (!obj.mDetached || (inBackStack ^ true)) {
            this.c.s(obj);
            if (this.G0(obj)) {
                this.D = true;
            }
            obj.mRemoving = true;
            this.n1(obj);
        }
    }
    
    public final void d(final s8 s8) {
        final int q = this.q;
        if (q < 1) {
            return;
        }
        final int min = Math.min(q, 5);
        for (final Fragment fragment : this.c.n()) {
            if (fragment.mState < min) {
                this.S0(fragment, min);
                if (fragment.mView == null || fragment.mHidden || !fragment.mIsNewlyAdded) {
                    continue;
                }
                s8.add(fragment);
            }
        }
    }
    
    public final void d0(final ArrayList list, final ArrayList list2, int i, final int n) {
        final boolean r = list.get(i).r;
        final ArrayList k = this.K;
        if (k == null) {
            this.K = new ArrayList();
        }
        else {
            k.clear();
        }
        this.K.addAll(this.c.n());
        Fragment fragment = this.x0();
        int n2 = 0;
        for (int j = i; j < n; ++j) {
            final a a = list.get(j);
            if (!(boolean)list2.get(j)) {
                fragment = a.y(this.K, fragment);
            }
            else {
                fragment = a.G(this.K, fragment);
            }
            if (n2 == 0 && !a.i) {
                n2 = 0;
            }
            else {
                n2 = 1;
            }
        }
        this.K.clear();
        if (!r && this.q >= 1) {
            if (androidx.fragment.app.k.P) {
                for (int l = i; l < n; ++l) {
                    final Iterator iterator = list.get(l).c.iterator();
                    while (iterator.hasNext()) {
                        final Fragment b = ((q.a)iterator.next()).b;
                        if (b != null && b.mFragmentManager != null) {
                            this.c.p(this.v(b));
                        }
                    }
                }
            }
            else {
                androidx.fragment.app.r.B(this.r.f(), this.s, list, list2, i, n, false, this.n);
            }
        }
        c0(list, list2, i, n);
        if (androidx.fragment.app.k.P) {
            final boolean booleanValue = list2.get(n - 1);
            for (int index = i; index < n; ++index) {
                final a a2 = list.get(index);
                if (booleanValue) {
                    for (int index2 = a2.c.size() - 1; index2 >= 0; --index2) {
                        final Fragment b2 = a2.c.get(index2).b;
                        if (b2 != null) {
                            this.v(b2).m();
                        }
                    }
                }
                else {
                    final Iterator iterator2 = a2.c.iterator();
                    while (iterator2.hasNext()) {
                        final Fragment b3 = ((q.a)iterator2.next()).b;
                        if (b3 != null) {
                            this.v(b3).m();
                        }
                    }
                }
            }
            this.Q0(this.q, true);
            for (final SpecialEffectsController specialEffectsController : this.s(list, i, n)) {
                specialEffectsController.r(booleanValue);
                specialEffectsController.p();
                specialEffectsController.g();
            }
        }
        else {
            int a3;
            if (r) {
                final s8 s8 = new s8();
                this.d(s8);
                a3 = this.a1(list, list2, i, n, s8);
                this.O0(s8);
            }
            else {
                a3 = n;
            }
            if (a3 != i && r) {
                if (this.q >= 1) {
                    androidx.fragment.app.r.B(this.r.f(), this.s, list, list2, i, a3, true, this.n);
                }
                this.Q0(this.q, true);
            }
        }
        while (i < n) {
            final a a4 = list.get(i);
            if (list2.get(i) && a4.v >= 0) {
                a4.v = -1;
            }
            a4.E();
            ++i;
        }
        if (n2 != 0) {
            this.f1();
        }
    }
    
    public final void d1(final ArrayList list, final ArrayList list2) {
        if (list.isEmpty()) {
            return;
        }
        if (list.size() == list2.size()) {
            this.f0(list, list2);
            final int size = list.size();
            int i = 0;
            int n = 0;
            while (i < size) {
                int n2 = i;
                int n3 = n;
                if (!((a)list.get(i)).r) {
                    if (n != i) {
                        this.d0(list, list2, n, i);
                    }
                    int n4 = n3 = i + 1;
                    if (list2.get(i)) {
                        while ((n3 = n4) < size) {
                            n3 = n4;
                            if (!(boolean)list2.get(n4)) {
                                break;
                            }
                            n3 = n4;
                            if (((a)list.get(n4)).r) {
                                break;
                            }
                            ++n4;
                        }
                    }
                    this.d0(list, list2, i, n3);
                    n2 = n3 - 1;
                }
                i = n2 + 1;
                n = n3;
            }
            if (n != size) {
                this.d0(list, list2, n, size);
            }
            return;
        }
        throw new IllegalStateException("Internal error with the back stack records");
    }
    
    public void e(final a e) {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        this.d.add(e);
    }
    
    public boolean e0() {
        final boolean a0 = this.a0(true);
        this.k0();
        return a0;
    }
    
    public void e1(final Fragment fragment) {
        this.M.n(fragment);
    }
    
    public void f(final Fragment fragment, final jf e) {
        if (this.m.get(fragment) == null) {
            this.m.put(fragment, new HashSet());
        }
        this.m.get(fragment).add(e);
    }
    
    public final void f0(final ArrayList list, final ArrayList list2) {
        final ArrayList l = this.L;
        int size;
        if (l == null) {
            size = 0;
        }
        else {
            size = l.size();
        }
        int n2;
        int n3;
    Label_0241:
        for (int i = 0, n = size; i < n; i = n2 + 1, n = n3) {
            final o o = this.L.get(i);
            while (true) {
                Label_0125: {
                    if (list == null || o.a) {
                        break Label_0125;
                    }
                    final int index = list.indexOf(o.b);
                    if (index == -1 || list2 == null || !(boolean)list2.get(index)) {
                        break Label_0125;
                    }
                    this.L.remove(i);
                    n2 = i - 1;
                    n3 = n - 1;
                    o.c();
                    continue Label_0241;
                }
                if (!o.e()) {
                    n3 = n;
                    n2 = i;
                    if (list == null) {
                        continue Label_0241;
                    }
                    n3 = n;
                    n2 = i;
                    if (!o.b.B(list, 0, list.size())) {
                        continue Label_0241;
                    }
                }
                this.L.remove(i);
                n2 = i - 1;
                n3 = n - 1;
                if (list != null && !o.a) {
                    final int index2 = list.indexOf(o.b);
                    if (index2 != -1 && list2 != null && (boolean)list2.get(index2)) {
                        continue;
                    }
                }
                break;
            }
            o.d();
        }
    }
    
    public final void f1() {
        final ArrayList l = this.l;
        if (l != null && l.size() > 0) {
            zu0.a(this.l.get(0));
            throw null;
        }
    }
    
    public androidx.fragment.app.o g(final Fragment obj) {
        if (F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("add: ");
            sb.append(obj);
            Log.v("FragmentManager", sb.toString());
        }
        final androidx.fragment.app.o v = this.v(obj);
        obj.mFragmentManager = this;
        this.c.p(v);
        if (!obj.mDetached) {
            this.c.a(obj);
            obj.mRemoving = false;
            if (obj.mView == null) {
                obj.mHiddenChanged = false;
            }
            if (this.G0(obj)) {
                this.D = true;
            }
        }
        return v;
    }
    
    public Fragment g0(final String s) {
        return this.c.f(s);
    }
    
    public void g1(final Parcelable parcelable) {
        if (parcelable == null) {
            return;
        }
        final androidx.fragment.app.l l = (androidx.fragment.app.l)parcelable;
        if (l.a == null) {
            return;
        }
        this.c.t();
        for (final androidx.fragment.app.n n : l.a) {
            if (n != null) {
                final Fragment h = this.M.h(n.b);
                androidx.fragment.app.o o;
                if (h != null) {
                    if (F0(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("restoreSaveState: re-attaching retained ");
                        sb.append(h);
                        Log.v("FragmentManager", sb.toString());
                    }
                    o = new androidx.fragment.app.o(this.o, this.c, h, n);
                }
                else {
                    o = new androidx.fragment.app.o(this.o, this.c, this.r.f().getClassLoader(), this.q0(), n);
                }
                final Fragment k = o.k();
                k.mFragmentManager = this;
                if (F0(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("restoreSaveState: active (");
                    sb2.append(k.mWho);
                    sb2.append("): ");
                    sb2.append(k);
                    Log.v("FragmentManager", sb2.toString());
                }
                o.o(this.r.f().getClassLoader());
                this.c.p(o);
                o.t(this.q);
            }
        }
        for (final Fragment obj : this.M.k()) {
            if (!this.c.c(obj.mWho)) {
                if (F0(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Discarding retained Fragment ");
                    sb3.append(obj);
                    sb3.append(" that was not found in the set of active Fragments ");
                    sb3.append(l.a);
                    Log.v("FragmentManager", sb3.toString());
                }
                this.M.n(obj);
                obj.mFragmentManager = this;
                final androidx.fragment.app.o o2 = new androidx.fragment.app.o(this.o, this.c, obj);
                o2.t(1);
                o2.m();
                obj.mRemoving = true;
                o2.m();
            }
        }
        this.c.u(l.b);
        final b[] c = l.c;
        final int n2 = 0;
        if (c != null) {
            this.d = new ArrayList(l.c.length);
            int i = 0;
            while (true) {
                final b[] c2 = l.c;
                if (i >= c2.length) {
                    break;
                }
                final a b = c2[i].b(this);
                if (F0(2)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("restoreAllState: back stack #");
                    sb4.append(i);
                    sb4.append(" (index ");
                    sb4.append(b.v);
                    sb4.append("): ");
                    sb4.append(b);
                    Log.v("FragmentManager", sb4.toString());
                    final PrintWriter printWriter = new PrintWriter(new wl0("FragmentManager"));
                    b.v("  ", printWriter, false);
                    printWriter.close();
                }
                this.d.add(b);
                ++i;
            }
        }
        else {
            this.d = null;
        }
        this.i.set(l.d);
        final String e = l.e;
        if (e != null) {
            this.L(this.u = this.g0(e));
        }
        final ArrayList f = l.f;
        if (f != null) {
            for (int j = n2; j < f.size(); ++j) {
                final Bundle bundle = l.g.get(j);
                bundle.setClassLoader(this.r.f().getClassLoader());
                this.j.put(f.get(j), bundle);
            }
        }
        this.C = new ArrayDeque(l.h);
    }
    
    public void h(final i80 e) {
        this.p.add(e);
    }
    
    public Fragment h0(final int n) {
        return this.c.g(n);
    }
    
    public void i(final Fragment fragment) {
        this.M.f(fragment);
    }
    
    public Fragment i0(final String s) {
        return this.c.h(s);
    }
    
    public Parcelable i1() {
        this.k0();
        this.X();
        this.a0(true);
        this.E = true;
        this.M.o(true);
        final ArrayList v = this.c.v();
        final boolean empty = v.isEmpty();
        final b[] array = null;
        if (empty) {
            if (F0(2)) {
                Log.v("FragmentManager", "saveAllState: no fragments!");
            }
            return null;
        }
        final ArrayList w = this.c.w();
        final ArrayList d = this.d;
        b[] c = array;
        if (d != null) {
            final int size = d.size();
            c = array;
            if (size > 0) {
                final b[] array2 = new b[size];
                int index = 0;
                while (true) {
                    c = array2;
                    if (index >= size) {
                        break;
                    }
                    array2[index] = new b((a)this.d.get(index));
                    if (F0(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("saveAllState: adding back stack #");
                        sb.append(index);
                        sb.append(": ");
                        sb.append(this.d.get(index));
                        Log.v("FragmentManager", sb.toString());
                    }
                    ++index;
                }
            }
        }
        final androidx.fragment.app.l l = new androidx.fragment.app.l();
        l.a = v;
        l.b = w;
        l.c = c;
        l.d = this.i.get();
        final Fragment u = this.u;
        if (u != null) {
            l.e = u.mWho;
        }
        l.f.addAll(this.j.keySet());
        l.g.addAll(this.j.values());
        l.h = new ArrayList(this.C);
        return (Parcelable)l;
    }
    
    public int j() {
        return this.i.getAndIncrement();
    }
    
    public Fragment j0(final String s) {
        return this.c.i(s);
    }
    
    public void j1() {
        synchronized (this.a) {
            final ArrayList l = this.L;
            boolean b = false;
            final boolean b2 = l != null && !l.isEmpty();
            if (this.a.size() == 1) {
                b = true;
            }
            if (b2 || b) {
                this.r.g().removeCallbacks(this.N);
                this.r.g().post(this.N);
                this.q1();
            }
        }
    }
    
    public void k(h r, final f80 s, final Fragment t) {
        if (this.r == null) {
            this.r = r;
            this.s = s;
            Label_0059: {
                i80 i80;
                if ((this.t = t) != null) {
                    i80 = new i80(this, t) {
                        public final Fragment a;
                        public final k b;
                        
                        @Override
                        public void a(final k k, final Fragment fragment) {
                            this.a.onAttachFragment(fragment);
                        }
                    };
                }
                else {
                    if (!(r instanceof i80)) {
                        break Label_0059;
                    }
                    i80 = (i80)r;
                }
                this.h(i80);
            }
            if (this.t != null) {
                this.q1();
            }
            if (r instanceof j11) {
                Object o = r;
                final OnBackPressedDispatcher onBackPressedDispatcher = ((j11)o).getOnBackPressedDispatcher();
                this.g = onBackPressedDispatcher;
                if (t != null) {
                    o = t;
                }
                onBackPressedDispatcher.h((qj0)o, this.h);
            }
            androidx.fragment.app.m m;
            if (t != null) {
                m = t.mFragmentManager.n0(t);
            }
            else if (r instanceof c42) {
                m = androidx.fragment.app.m.j(((c42)r).getViewModelStore());
            }
            else {
                m = new androidx.fragment.app.m(false);
            }
            (this.M = m).o(this.K0());
            this.c.x(this.M);
            r = this.r;
            if (r instanceof s2) {
                final ActivityResultRegistry activityResultRegistry = ((s2)r).getActivityResultRegistry();
                String string;
                if (t != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(t.mWho);
                    sb.append(":");
                    string = sb.toString();
                }
                else {
                    string = "";
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("FragmentManager:");
                sb2.append(string);
                final String string2 = sb2.toString();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append("StartActivityForResult");
                this.z = activityResultRegistry.i(sb3.toString(), new p2(), new m2(this) {
                    public final k a;
                    
                    public void b(final l2 l2) {
                        final l i = this.a.C.pollFirst();
                        if (i == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No Activities were started for result for ");
                            sb.append(this);
                            Log.w("FragmentManager", sb.toString());
                            return;
                        }
                        final String a = i.a;
                        final int b = i.b;
                        final Fragment j = androidx.fragment.app.k.c(this.a).i(a);
                        if (j == null) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Activity result delivered for unknown Fragment ");
                            sb2.append(a);
                            Log.w("FragmentManager", sb2.toString());
                            return;
                        }
                        j.onActivityResult(b, l2.c(), l2.b());
                    }
                });
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string2);
                sb4.append("StartIntentSenderForResult");
                this.A = activityResultRegistry.i(sb4.toString(), new k(), new m2(this) {
                    public final k a;
                    
                    public void b(final l2 l2) {
                        final l i = this.a.C.pollFirst();
                        if (i == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No IntentSenders were started for ");
                            sb.append(this);
                            Log.w("FragmentManager", sb.toString());
                            return;
                        }
                        final String a = i.a;
                        final int b = i.b;
                        final Fragment j = androidx.fragment.app.k.c(this.a).i(a);
                        if (j == null) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Intent Sender result delivered for unknown Fragment ");
                            sb2.append(a);
                            Log.w("FragmentManager", sb2.toString());
                            return;
                        }
                        j.onActivityResult(b, l2.c(), l2.b());
                    }
                });
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string2);
                sb5.append("RequestPermissions");
                this.B = activityResultRegistry.i(sb5.toString(), new o2(), new m2(this) {
                    public final k a;
                    
                    public void b(final Map map) {
                        final String[] array = (String[])map.keySet().toArray(new String[0]);
                        final ArrayList list = new ArrayList(map.values());
                        final int[] array2 = new int[list.size()];
                        for (int i = 0; i < list.size(); ++i) {
                            int n;
                            if (list.get(i)) {
                                n = 0;
                            }
                            else {
                                n = -1;
                            }
                            array2[i] = n;
                        }
                        final l l = this.a.C.pollFirst();
                        StringBuilder sb;
                        if (l == null) {
                            sb = new StringBuilder();
                            sb.append("No permissions were requested for ");
                            sb.append(this);
                        }
                        else {
                            final String a = l.a;
                            final int b = l.b;
                            final Fragment j = androidx.fragment.app.k.c(this.a).i(a);
                            if (j != null) {
                                j.onRequestPermissionsResult(b, array, array2);
                                return;
                            }
                            sb = new StringBuilder();
                            sb.append("Permission request result delivered for unknown Fragment ");
                            sb.append(a);
                        }
                        Log.w("FragmentManager", sb.toString());
                    }
                });
            }
            return;
        }
        throw new IllegalStateException("Already attached");
    }
    
    public final void k0() {
        if (androidx.fragment.app.k.P) {
            final Iterator iterator = this.r().iterator();
            while (iterator.hasNext()) {
                ((SpecialEffectsController)iterator.next()).k();
            }
        }
        else if (this.L != null) {
            while (!this.L.isEmpty()) {
                this.L.remove(0).d();
            }
        }
    }
    
    public void k1(final Fragment fragment, final boolean b) {
        final ViewGroup p2 = this.p0(fragment);
        if (p2 != null && p2 instanceof FragmentContainerView) {
            ((FragmentContainerView)p2).setDrawDisappearingViewsLast(b ^ true);
        }
    }
    
    public void l(final Fragment fragment) {
        if (F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("attach: ");
            sb.append(fragment);
            Log.v("FragmentManager", sb.toString());
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (!fragment.mAdded) {
                this.c.a(fragment);
                if (F0(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("add from attach: ");
                    sb2.append(fragment);
                    Log.v("FragmentManager", sb2.toString());
                }
                if (this.G0(fragment)) {
                    this.D = true;
                }
            }
        }
    }
    
    public final boolean l0(final ArrayList list, final ArrayList list2) {
        synchronized (this.a) {
            final boolean empty = this.a.isEmpty();
            int i = 0;
            if (empty) {
                return false;
            }
            final int size = this.a.size();
            boolean b = false;
            while (i < size) {
                b |= this.a.get(i).a(list, list2);
                ++i;
            }
            this.a.clear();
            this.r.g().removeCallbacks(this.N);
            return b;
        }
    }
    
    public void l1(final Fragment obj, final Lifecycle.State mMaxState) {
        if (obj.equals(this.g0(obj.mWho)) && (obj.mHost == null || obj.mFragmentManager == this)) {
            obj.mMaxState = mMaxState;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(obj);
        sb.append(" is not an active fragment of FragmentManager ");
        sb.append(this);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public q m() {
        return new a(this);
    }
    
    public int m0() {
        final ArrayList d = this.d;
        int size;
        if (d != null) {
            size = d.size();
        }
        else {
            size = 0;
        }
        return size;
    }
    
    public void m1(final Fragment fragment) {
        Label_0085: {
            if (fragment != null) {
                if (fragment.equals(this.g0(fragment.mWho))) {
                    if (fragment.mHost == null) {
                        break Label_0085;
                    }
                    if (fragment.mFragmentManager == this) {
                        break Label_0085;
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(fragment);
                sb.append(" is not an active fragment of FragmentManager ");
                sb.append(this);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        final Fragment u = this.u;
        this.u = fragment;
        this.L(u);
        this.L(this.u);
    }
    
    public final void n(final Fragment fragment) {
        final HashSet set = this.m.get(fragment);
        if (set != null) {
            final Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                ((jf)iterator.next()).a();
            }
            set.clear();
            this.w(fragment);
            this.m.remove(fragment);
        }
    }
    
    public final androidx.fragment.app.m n0(final Fragment fragment) {
        return this.M.i(fragment);
    }
    
    public final void n1(final Fragment fragment) {
        final ViewGroup p = this.p0(fragment);
        if (p != null && fragment.getEnterAnim() + fragment.getExitAnim() + fragment.getPopEnterAnim() + fragment.getPopExitAnim() > 0) {
            final int c = gb1.c;
            if (((View)p).getTag(c) == null) {
                ((View)p).setTag(c, (Object)fragment);
            }
            ((Fragment)((View)p).getTag(c)).setPopDirection(fragment.getPopDirection());
        }
    }
    
    public boolean o() {
        final Iterator iterator = this.c.l().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Fragment fragment = (Fragment)iterator.next();
            int g0 = n;
            if (fragment != null) {
                g0 = (this.G0(fragment) ? 1 : 0);
            }
            if ((n = g0) != 0) {
                return true;
            }
        }
        return false;
    }
    
    public f80 o0() {
        return this.s;
    }
    
    public void o1(final Fragment obj) {
        if (F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("show: ");
            sb.append(obj);
            Log.v("FragmentManager", sb.toString());
        }
        if (obj.mHidden) {
            obj.mHidden = false;
            obj.mHiddenChanged ^= true;
        }
    }
    
    public final void p() {
        if (!this.K0()) {
            return;
        }
        throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
    }
    
    public final ViewGroup p0(final Fragment fragment) {
        final ViewGroup mContainer = fragment.mContainer;
        if (mContainer != null) {
            return mContainer;
        }
        if (fragment.mContainerId <= 0) {
            return null;
        }
        if (this.s.d()) {
            final View c = this.s.c(fragment.mContainerId);
            if (c instanceof ViewGroup) {
                return (ViewGroup)c;
            }
        }
        return null;
    }
    
    public final void p1() {
        final Iterator iterator = this.c.k().iterator();
        while (iterator.hasNext()) {
            this.V0((androidx.fragment.app.o)iterator.next());
        }
    }
    
    public final void q() {
        this.b = false;
        this.J.clear();
        this.I.clear();
    }
    
    public g q0() {
        final g v = this.v;
        if (v != null) {
            return v;
        }
        final Fragment t = this.t;
        if (t != null) {
            return t.mFragmentManager.q0();
        }
        return this.w;
    }
    
    public final void q1() {
        Object o = this.a;
        synchronized (o) {
            final boolean empty = this.a.isEmpty();
            boolean b = true;
            if (!empty) {
                this.h.j(true);
                return;
            }
            monitorexit(o);
            o = this.h;
            if (this.m0() <= 0 || !this.I0(this.t)) {
                b = false;
            }
            ((h11)o).j(b);
        }
    }
    
    public final Set r() {
        final HashSet set = new HashSet();
        final Iterator iterator = this.c.k().iterator();
        while (iterator.hasNext()) {
            final ViewGroup mContainer = ((androidx.fragment.app.o)iterator.next()).k().mContainer;
            if (mContainer != null) {
                set.add(SpecialEffectsController.n(mContainer, this.y0()));
            }
        }
        return set;
    }
    
    public p r0() {
        return this.c;
    }
    
    public final Set s(final ArrayList list, int i, final int n) {
        final HashSet set = new HashSet();
        while (i < n) {
            final Iterator iterator = list.get(i).c.iterator();
            while (iterator.hasNext()) {
                final Fragment b = ((q.a)iterator.next()).b;
                if (b != null) {
                    final ViewGroup mContainer = b.mContainer;
                    if (mContainer == null) {
                        continue;
                    }
                    set.add(SpecialEffectsController.o(mContainer, this));
                }
            }
            ++i;
        }
        return set;
    }
    
    public List s0() {
        return this.c.n();
    }
    
    public void t(final a e, final boolean b, final boolean b2, final boolean b3) {
        if (b) {
            e.x(b3);
        }
        else {
            e.w();
        }
        final ArrayList list = new ArrayList(1);
        final ArrayList list2 = new ArrayList(1);
        list.add(e);
        list2.add(b);
        if (b2 && this.q >= 1) {
            androidx.fragment.app.r.B(this.r.f(), this.s, list, list2, 0, 1, true, this.n);
        }
        if (b3) {
            this.Q0(this.q, true);
        }
        for (final Fragment fragment : this.c.l()) {
            if (fragment != null && fragment.mView != null && fragment.mIsNewlyAdded && e.A(fragment.mContainerId)) {
                final float mPostponedAlpha = fragment.mPostponedAlpha;
                if (mPostponedAlpha > 0.0f) {
                    fragment.mView.setAlpha(mPostponedAlpha);
                }
                if (b3) {
                    fragment.mPostponedAlpha = 0.0f;
                }
                else {
                    fragment.mPostponedAlpha = -1.0f;
                    fragment.mIsNewlyAdded = false;
                }
            }
        }
    }
    
    public h t0() {
        return this.r;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        final Fragment t = this.t;
        Label_0141: {
            Object o;
            if (t != null) {
                sb.append(t.getClass().getSimpleName());
                sb.append("{");
                o = this.t;
            }
            else {
                final h r = this.r;
                if (r == null) {
                    sb.append("null");
                    break Label_0141;
                }
                sb.append(r.getClass().getSimpleName());
                sb.append("{");
                o = this.r;
            }
            sb.append(Integer.toHexString(System.identityHashCode(o)));
            sb.append("}");
        }
        sb.append("}}");
        return sb.toString();
    }
    
    public final void u(final Fragment fragment) {
        Label_0199: {
            if (fragment.mView != null) {
                final f.d c = androidx.fragment.app.f.c(this.r.f(), fragment, fragment.mHidden ^ true, fragment.getPopDirection());
                if (c != null) {
                    final Animator b = c.b;
                    if (b != null) {
                        b.setTarget((Object)fragment.mView);
                        if (fragment.mHidden) {
                            if (fragment.isHideReplaced()) {
                                fragment.setHideReplaced(false);
                            }
                            else {
                                final ViewGroup mContainer = fragment.mContainer;
                                final View mView = fragment.mView;
                                mContainer.startViewTransition(mView);
                                c.b.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, mContainer, mView, fragment) {
                                    public final ViewGroup a;
                                    public final View b;
                                    public final Fragment c;
                                    public final k d;
                                    
                                    public void onAnimationEnd(final Animator animator) {
                                        this.a.endViewTransition(this.b);
                                        animator.removeListener((Animator$AnimatorListener)this);
                                        final Fragment c = this.c;
                                        final View mView = c.mView;
                                        if (mView != null && c.mHidden) {
                                            mView.setVisibility(8);
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            fragment.mView.setVisibility(0);
                        }
                        c.b.start();
                        break Label_0199;
                    }
                }
                if (c != null) {
                    fragment.mView.startAnimation(c.a);
                    c.a.start();
                }
                int visibility;
                if (fragment.mHidden && !fragment.isHideReplaced()) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                fragment.mView.setVisibility(visibility);
                if (fragment.isHideReplaced()) {
                    fragment.setHideReplaced(false);
                }
            }
        }
        this.D0(fragment);
        fragment.mHiddenChanged = false;
        fragment.onHiddenChanged(fragment.mHidden);
    }
    
    public LayoutInflater$Factory2 u0() {
        return (LayoutInflater$Factory2)this.f;
    }
    
    public androidx.fragment.app.o v(final Fragment fragment) {
        final androidx.fragment.app.o m = this.c.m(fragment.mWho);
        if (m != null) {
            return m;
        }
        final androidx.fragment.app.o o = new androidx.fragment.app.o(this.o, this.c, fragment);
        o.o(this.r.f().getClassLoader());
        o.t(this.q);
        return o;
    }
    
    public j v0() {
        return this.o;
    }
    
    public final void w(final Fragment fragment) {
        fragment.performDestroyView();
        this.o.n(fragment, false);
        fragment.mContainer = null;
        fragment.mView = null;
        fragment.mViewLifecycleOwner = null;
        fragment.mViewLifecycleOwnerLiveData.n(null);
        fragment.mInLayout = false;
    }
    
    public Fragment w0() {
        return this.t;
    }
    
    public void x(final Fragment fragment) {
        if (F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("detach: ");
            sb.append(fragment);
            Log.v("FragmentManager", sb.toString());
        }
        if (!fragment.mDetached) {
            fragment.mDetached = true;
            if (fragment.mAdded) {
                if (F0(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("remove from detach: ");
                    sb2.append(fragment);
                    Log.v("FragmentManager", sb2.toString());
                }
                this.c.s(fragment);
                if (this.G0(fragment)) {
                    this.D = true;
                }
                this.n1(fragment);
            }
        }
    }
    
    public Fragment x0() {
        return this.u;
    }
    
    public void y() {
        this.E = false;
        this.F = false;
        this.M.o(false);
        this.S(4);
    }
    
    public xo1 y0() {
        final xo1 x = this.x;
        if (x != null) {
            return x;
        }
        final Fragment t = this.t;
        if (t != null) {
            return t.mFragmentManager.y0();
        }
        return this.y;
    }
    
    public void z() {
        this.E = false;
        this.F = false;
        this.M.o(false);
        this.S(0);
    }
    
    public static class k extends n2
    {
        public Intent d(final Context context, final yf0 yf0) {
            final Intent obj = new Intent("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST");
            final Intent b = yf0.b();
            yf0 a = yf0;
            if (b != null) {
                final Bundle bundleExtra = b.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                a = yf0;
                if (bundleExtra != null) {
                    obj.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundleExtra);
                    b.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                    a = yf0;
                    if (b.getBooleanExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", false)) {
                        a = new yf0.a(yf0.e()).b(null).c(yf0.d(), yf0.c()).a();
                    }
                }
            }
            obj.putExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST", (Parcelable)a);
            if (androidx.fragment.app.k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CreateIntent created the following intent: ");
                sb.append(obj);
                Log.v("FragmentManager", sb.toString());
            }
            return obj;
        }
        
        public l2 e(final int n, final Intent intent) {
            return new l2(n, intent);
        }
    }
    
    public static class l implements Parcelable
    {
        public static final Parcelable$Creator<l> CREATOR;
        public String a;
        public int b;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public l a(final Parcel parcel) {
                    return new l(parcel);
                }
                
                public l[] b(final int n) {
                    return new l[n];
                }
            };
        }
        
        public l(final Parcel parcel) {
            this.a = parcel.readString();
            this.b = parcel.readInt();
        }
        
        public l(final String a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeString(this.a);
            parcel.writeInt(this.b);
        }
    }
    
    public interface m
    {
        boolean a(final ArrayList p0, final ArrayList p1);
    }
    
    public class n implements m
    {
        public final String a;
        public final int b;
        public final int c;
        public final k d;
        
        public n(final k d, final String a, final int b, final int c) {
            this.d = d;
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public boolean a(final ArrayList list, final ArrayList list2) {
            final Fragment u = this.d.u;
            return (u == null || this.b >= 0 || this.a != null || !u.getChildFragmentManager().X0()) && this.d.Z0(list, list2, this.a, this.b, this.c);
        }
    }
    
    public static class o implements Fragment.k
    {
        public final boolean a;
        public final a b;
        public int c;
        
        public o(final a b, final boolean a) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void a() {
            ++this.c;
        }
        
        @Override
        public void b() {
            final int c = this.c - 1;
            this.c = c;
            if (c != 0) {
                return;
            }
            this.b.t.j1();
        }
        
        public void c() {
            final a b = this.b;
            b.t.t(b, this.a, false, false);
        }
        
        public void d() {
            final boolean b = this.c > 0;
            for (final Fragment fragment : this.b.t.s0()) {
                fragment.setOnStartEnterTransitionListener(null);
                if (b && fragment.isPostponed()) {
                    fragment.startPostponedEnterTransition();
                }
            }
            final a b2 = this.b;
            b2.t.t(b2, this.a, b ^ true, true);
        }
        
        public boolean e() {
            return this.c == 0;
        }
    }
}

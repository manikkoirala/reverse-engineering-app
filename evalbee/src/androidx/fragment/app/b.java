// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.List;
import androidx.lifecycle.Lifecycle;
import android.util.Log;
import android.text.TextUtils;
import android.os.Parcel;
import java.util.ArrayList;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class b implements Parcelable
{
    public static final Parcelable$Creator<b> CREATOR;
    public final int[] a;
    public final ArrayList b;
    public final int[] c;
    public final int[] d;
    public final int e;
    public final String f;
    public final int g;
    public final int h;
    public final CharSequence i;
    public final int j;
    public final CharSequence k;
    public final ArrayList l;
    public final ArrayList m;
    public final boolean n;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public b a(final Parcel parcel) {
                return new b(parcel);
            }
            
            public b[] b(final int n) {
                return new b[n];
            }
        };
    }
    
    public b(final Parcel parcel) {
        this.a = parcel.createIntArray();
        this.b = parcel.createStringArrayList();
        this.c = parcel.createIntArray();
        this.d = parcel.createIntArray();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.h = parcel.readInt();
        this.i = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.readInt();
        this.k = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.l = parcel.createStringArrayList();
        this.m = parcel.createStringArrayList();
        this.n = (parcel.readInt() != 0);
    }
    
    public b(final a a) {
        final int size = a.c.size();
        this.a = new int[size * 5];
        if (a.i) {
            this.b = new ArrayList(size);
            this.c = new int[size];
            this.d = new int[size];
            for (int i = 0, n = 0; i < size; ++i, ++n) {
                final q.a a2 = a.c.get(i);
                final int[] a3 = this.a;
                final int n2 = n + 1;
                a3[n] = a2.a;
                final ArrayList b = this.b;
                final Fragment b2 = a2.b;
                String mWho;
                if (b2 != null) {
                    mWho = b2.mWho;
                }
                else {
                    mWho = null;
                }
                b.add(mWho);
                final int[] a4 = this.a;
                final int n3 = n2 + 1;
                a4[n2] = a2.c;
                final int n4 = n3 + 1;
                a4[n3] = a2.d;
                n = n4 + 1;
                a4[n4] = a2.e;
                a4[n] = a2.f;
                this.c[i] = a2.g.ordinal();
                this.d[i] = a2.h.ordinal();
            }
            this.e = a.h;
            this.f = a.k;
            this.g = a.v;
            this.h = a.l;
            this.i = a.m;
            this.j = a.n;
            this.k = a.o;
            this.l = a.p;
            this.m = a.q;
            this.n = a.r;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }
    
    public a b(final k k) {
        final a obj = new a(k);
        int i = 0;
        int n = 0;
        while (i < this.a.length) {
            final q.a a = new q.a();
            final int[] a2 = this.a;
            final int n2 = i + 1;
            a.a = a2[i];
            if (k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Instantiate ");
                sb.append(obj);
                sb.append(" op #");
                sb.append(n);
                sb.append(" base fragment #");
                sb.append(this.a[n2]);
                Log.v("FragmentManager", sb.toString());
            }
            final String s = this.b.get(n);
            Fragment g0;
            if (s != null) {
                g0 = k.g0(s);
            }
            else {
                g0 = null;
            }
            a.b = g0;
            a.g = Lifecycle.State.values()[this.c[n]];
            a.h = Lifecycle.State.values()[this.d[n]];
            final int[] a3 = this.a;
            final int n3 = n2 + 1;
            final int n4 = a3[n2];
            a.c = n4;
            final int n5 = n3 + 1;
            final int n6 = a3[n3];
            a.d = n6;
            final int n7 = n5 + 1;
            final int n8 = a3[n5];
            a.e = n8;
            final int n9 = a3[n7];
            a.f = n9;
            obj.d = n4;
            obj.e = n6;
            obj.f = n8;
            obj.g = n9;
            obj.e(a);
            ++n;
            i = n7 + 1;
        }
        obj.h = this.e;
        obj.k = this.f;
        obj.v = this.g;
        obj.i = true;
        obj.l = this.h;
        obj.m = this.i;
        obj.n = this.j;
        obj.o = this.k;
        obj.p = this.l;
        obj.q = this.m;
        obj.r = this.n;
        obj.s(1);
        return obj;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeIntArray(this.a);
        parcel.writeStringList((List)this.b);
        parcel.writeIntArray(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeInt(this.j);
        TextUtils.writeToParcel(this.k, parcel, 0);
        parcel.writeStringList((List)this.l);
        parcel.writeStringList((List)this.m);
        parcel.writeInt((int)(this.n ? 1 : 0));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.View;
import java.util.Iterator;
import android.os.Bundle;
import java.util.concurrent.CopyOnWriteArrayList;

public class j
{
    public final CopyOnWriteArrayList a;
    public final k b;
    
    public j(final k b) {
        this.a = new CopyOnWriteArrayList();
        this.b = b;
    }
    
    public void a(final Fragment fragment, final Bundle bundle, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().a(fragment, bundle, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void b(final Fragment fragment, final boolean b) {
        this.b.t0().f();
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().b(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void c(final Fragment fragment, final Bundle bundle, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().c(fragment, bundle, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void d(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().d(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void e(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().e(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void f(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().f(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void g(final Fragment fragment, final boolean b) {
        this.b.t0().f();
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().g(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void h(final Fragment fragment, final Bundle bundle, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().h(fragment, bundle, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void i(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().i(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void j(final Fragment fragment, final Bundle bundle, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().j(fragment, bundle, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void k(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().k(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void l(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().l(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void m(final Fragment fragment, final View view, final Bundle bundle, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().m(fragment, view, bundle, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void n(final Fragment fragment, final boolean b) {
        final Fragment w0 = this.b.w0();
        if (w0 != null) {
            w0.getParentFragmentManager().v0().n(fragment, true);
        }
        final Iterator iterator = this.a.iterator();
        if (iterator.hasNext()) {
            zu0.a(iterator.next());
            throw null;
        }
    }
}

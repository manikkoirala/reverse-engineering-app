// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.BaseBundle;
import android.view.Window;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.util.Log;
import android.content.DialogInterface;
import android.os.Handler;
import android.app.Dialog;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnCancelListener;

public class d extends Fragment implements DialogInterface$OnCancelListener, DialogInterface$OnDismissListener
{
    private static final String SAVED_BACK_STACK_ID = "android:backStackId";
    private static final String SAVED_CANCELABLE = "android:cancelable";
    private static final String SAVED_DIALOG_STATE_TAG = "android:savedDialogState";
    private static final String SAVED_INTERNAL_DIALOG_SHOWING = "android:dialogShowing";
    private static final String SAVED_SHOWS_DIALOG = "android:showsDialog";
    private static final String SAVED_STYLE = "android:style";
    private static final String SAVED_THEME = "android:theme";
    public static final int STYLE_NORMAL = 0;
    public static final int STYLE_NO_FRAME = 2;
    public static final int STYLE_NO_INPUT = 3;
    public static final int STYLE_NO_TITLE = 1;
    private int mBackStackId;
    private boolean mCancelable;
    private boolean mCreatingDialog;
    private Dialog mDialog;
    private boolean mDialogCreated;
    private Runnable mDismissRunnable;
    private boolean mDismissed;
    private Handler mHandler;
    private d11 mObserver;
    private DialogInterface$OnCancelListener mOnCancelListener;
    private DialogInterface$OnDismissListener mOnDismissListener;
    private boolean mShownByMe;
    private boolean mShowsDialog;
    private int mStyle;
    private int mTheme;
    private boolean mViewDestroyed;
    
    public d() {
        this.mDismissRunnable = new Runnable(this) {
            public final d a;
            
            @Override
            public void run() {
                d.access$100(this.a).onDismiss((DialogInterface)d.access$000(this.a));
            }
        };
        this.mOnCancelListener = (DialogInterface$OnCancelListener)new DialogInterface$OnCancelListener(this) {
            public final d a;
            
            public void onCancel(final DialogInterface dialogInterface) {
                if (d.access$000(this.a) != null) {
                    final d a = this.a;
                    a.onCancel((DialogInterface)d.access$000(a));
                }
            }
        };
        this.mOnDismissListener = (DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener(this) {
            public final d a;
            
            public void onDismiss(final DialogInterface dialogInterface) {
                if (d.access$000(this.a) != null) {
                    final d a = this.a;
                    a.onDismiss((DialogInterface)d.access$000(a));
                }
            }
        };
        this.mStyle = 0;
        this.mTheme = 0;
        this.mCancelable = true;
        this.mShowsDialog = true;
        this.mBackStackId = -1;
        this.mObserver = new d11(this) {
            public final d a;
            
            public void b(final qj0 qj0) {
                if (qj0 != null && d.access$200(this.a)) {
                    final View requireView = this.a.requireView();
                    if (requireView.getParent() != null) {
                        throw new IllegalStateException("DialogFragment can not be attached to a container view");
                    }
                    if (d.access$000(this.a) != null) {
                        if (androidx.fragment.app.k.F0(3)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("DialogFragment ");
                            sb.append(this);
                            sb.append(" setting the content view on ");
                            sb.append(d.access$000(this.a));
                            Log.d("FragmentManager", sb.toString());
                        }
                        d.access$000(this.a).setContentView(requireView);
                    }
                }
            }
        };
        this.mDialogCreated = false;
    }
    
    public d(final int n) {
        super(n);
        this.mDismissRunnable = new Runnable(this) {
            public final d a;
            
            @Override
            public void run() {
                d.access$100(this.a).onDismiss((DialogInterface)d.access$000(this.a));
            }
        };
        this.mOnCancelListener = (DialogInterface$OnCancelListener)new DialogInterface$OnCancelListener(this) {
            public final d a;
            
            public void onCancel(final DialogInterface dialogInterface) {
                if (d.access$000(this.a) != null) {
                    final d a = this.a;
                    a.onCancel((DialogInterface)d.access$000(a));
                }
            }
        };
        this.mOnDismissListener = (DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener(this) {
            public final d a;
            
            public void onDismiss(final DialogInterface dialogInterface) {
                if (d.access$000(this.a) != null) {
                    final d a = this.a;
                    a.onDismiss((DialogInterface)d.access$000(a));
                }
            }
        };
        this.mStyle = 0;
        this.mTheme = 0;
        this.mCancelable = true;
        this.mShowsDialog = true;
        this.mBackStackId = -1;
        this.mObserver = new d11(this) {
            public final d a;
            
            public void b(final qj0 qj0) {
                if (qj0 != null && d.access$200(this.a)) {
                    final View requireView = this.a.requireView();
                    if (requireView.getParent() != null) {
                        throw new IllegalStateException("DialogFragment can not be attached to a container view");
                    }
                    if (d.access$000(this.a) != null) {
                        if (androidx.fragment.app.k.F0(3)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("DialogFragment ");
                            sb.append(this);
                            sb.append(" setting the content view on ");
                            sb.append(d.access$000(this.a));
                            Log.d("FragmentManager", sb.toString());
                        }
                        d.access$000(this.a).setContentView(requireView);
                    }
                }
            }
        };
        this.mDialogCreated = false;
    }
    
    public static /* synthetic */ Dialog access$000(final d d) {
        return d.mDialog;
    }
    
    public static /* synthetic */ DialogInterface$OnDismissListener access$100(final d d) {
        return d.mOnDismissListener;
    }
    
    public static /* synthetic */ boolean access$200(final d d) {
        return d.mShowsDialog;
    }
    
    @Override
    public f80 createFragmentContainer() {
        return new f80(this, super.createFragmentContainer()) {
            public final f80 a;
            public final d b;
            
            @Override
            public View c(final int n) {
                if (this.a.d()) {
                    return this.a.c(n);
                }
                return this.b.onFindViewById(n);
            }
            
            @Override
            public boolean d() {
                return this.a.d() || this.b.onHasView();
            }
        };
    }
    
    public void dismiss() {
        this.g(false, false);
    }
    
    public void dismissAllowingStateLoss() {
        this.g(true, false);
    }
    
    public final void g(final boolean b, final boolean b2) {
        if (this.mDismissed) {
            return;
        }
        this.mDismissed = true;
        this.mShownByMe = false;
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            mDialog.setOnDismissListener((DialogInterface$OnDismissListener)null);
            this.mDialog.dismiss();
            if (!b2) {
                if (Looper.myLooper() == this.mHandler.getLooper()) {
                    this.onDismiss((DialogInterface)this.mDialog);
                }
                else {
                    this.mHandler.post(this.mDismissRunnable);
                }
            }
        }
        this.mViewDestroyed = true;
        if (this.mBackStackId >= 0) {
            this.getParentFragmentManager().W0(this.mBackStackId, 1);
            this.mBackStackId = -1;
        }
        else {
            final q m = this.getParentFragmentManager().m();
            m.n(this);
            if (b) {
                m.h();
            }
            else {
                m.g();
            }
        }
    }
    
    public Dialog getDialog() {
        return this.mDialog;
    }
    
    public boolean getShowsDialog() {
        return this.mShowsDialog;
    }
    
    public int getTheme() {
        return this.mTheme;
    }
    
    public final void h(final Bundle bundle) {
        if (!this.mShowsDialog) {
            return;
        }
        if (!this.mDialogCreated) {
            try {
                this.mCreatingDialog = true;
                final Dialog onCreateDialog = this.onCreateDialog(bundle);
                this.mDialog = onCreateDialog;
                if (this.mShowsDialog) {
                    this.setupDialog(onCreateDialog, this.mStyle);
                    final Context context = this.getContext();
                    if (context instanceof Activity) {
                        this.mDialog.setOwnerActivity((Activity)context);
                    }
                    this.mDialog.setCancelable(this.mCancelable);
                    this.mDialog.setOnCancelListener(this.mOnCancelListener);
                    this.mDialog.setOnDismissListener(this.mOnDismissListener);
                    this.mDialogCreated = true;
                }
                else {
                    this.mDialog = null;
                }
            }
            finally {
                this.mCreatingDialog = false;
            }
        }
    }
    
    public boolean isCancelable() {
        return this.mCancelable;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.getViewLifecycleOwnerLiveData().i(this.mObserver);
        if (!this.mShownByMe) {
            this.mDismissed = false;
        }
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mHandler = new Handler();
        this.mShowsDialog = (super.mContainerId == 0);
        if (bundle != null) {
            this.mStyle = ((BaseBundle)bundle).getInt("android:style", 0);
            this.mTheme = ((BaseBundle)bundle).getInt("android:theme", 0);
            this.mCancelable = ((BaseBundle)bundle).getBoolean("android:cancelable", true);
            this.mShowsDialog = ((BaseBundle)bundle).getBoolean("android:showsDialog", this.mShowsDialog);
            this.mBackStackId = ((BaseBundle)bundle).getInt("android:backStackId", -1);
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        if (androidx.fragment.app.k.F0(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onCreateDialog called for DialogFragment ");
            sb.append(this);
            Log.d("FragmentManager", sb.toString());
        }
        return new Dialog(this.requireContext(), this.getTheme());
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            this.mViewDestroyed = true;
            mDialog.setOnDismissListener((DialogInterface$OnDismissListener)null);
            this.mDialog.dismiss();
            if (!this.mDismissed) {
                this.onDismiss((DialogInterface)this.mDialog);
            }
            this.mDialog = null;
            this.mDialogCreated = false;
        }
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        if (!this.mShownByMe && !this.mDismissed) {
            this.mDismissed = true;
        }
        this.getViewLifecycleOwnerLiveData().m(this.mObserver);
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        if (!this.mViewDestroyed) {
            if (androidx.fragment.app.k.F0(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("onDismiss called for DialogFragment ");
                sb.append(this);
                Log.d("FragmentManager", sb.toString());
            }
            this.g(true, true);
        }
    }
    
    public View onFindViewById(final int n) {
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            return mDialog.findViewById(n);
        }
        return null;
    }
    
    @Override
    public LayoutInflater onGetLayoutInflater(final Bundle bundle) {
        final LayoutInflater onGetLayoutInflater = super.onGetLayoutInflater(bundle);
        if (this.mShowsDialog && !this.mCreatingDialog) {
            this.h(bundle);
            if (androidx.fragment.app.k.F0(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("get layout inflater for DialogFragment ");
                sb.append(this);
                sb.append(" from dialog context");
                Log.d("FragmentManager", sb.toString());
            }
            final Dialog mDialog = this.mDialog;
            LayoutInflater cloneInContext = onGetLayoutInflater;
            if (mDialog != null) {
                cloneInContext = onGetLayoutInflater.cloneInContext(mDialog.getContext());
            }
            return cloneInContext;
        }
        if (androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("getting layout inflater for DialogFragment ");
            sb2.append(this);
            final String string = sb2.toString();
            StringBuilder sb3;
            String str;
            if (!this.mShowsDialog) {
                sb3 = new StringBuilder();
                str = "mShowsDialog = false: ";
            }
            else {
                sb3 = new StringBuilder();
                str = "mCreatingDialog = true: ";
            }
            sb3.append(str);
            sb3.append(string);
            Log.d("FragmentManager", sb3.toString());
        }
        return onGetLayoutInflater;
    }
    
    public boolean onHasView() {
        return this.mDialogCreated;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            final Bundle onSaveInstanceState = mDialog.onSaveInstanceState();
            ((BaseBundle)onSaveInstanceState).putBoolean("android:dialogShowing", false);
            bundle.putBundle("android:savedDialogState", onSaveInstanceState);
        }
        final int mStyle = this.mStyle;
        if (mStyle != 0) {
            ((BaseBundle)bundle).putInt("android:style", mStyle);
        }
        final int mTheme = this.mTheme;
        if (mTheme != 0) {
            ((BaseBundle)bundle).putInt("android:theme", mTheme);
        }
        final boolean mCancelable = this.mCancelable;
        if (!mCancelable) {
            ((BaseBundle)bundle).putBoolean("android:cancelable", mCancelable);
        }
        final boolean mShowsDialog = this.mShowsDialog;
        if (!mShowsDialog) {
            ((BaseBundle)bundle).putBoolean("android:showsDialog", mShowsDialog);
        }
        final int mBackStackId = this.mBackStackId;
        if (mBackStackId != -1) {
            ((BaseBundle)bundle).putInt("android:backStackId", mBackStackId);
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            this.mViewDestroyed = false;
            mDialog.show();
            final View decorView = this.mDialog.getWindow().getDecorView();
            o42.a(decorView, this);
            r42.a(decorView, this);
            q42.a(decorView, this);
        }
    }
    
    @Override
    public void onStop() {
        super.onStop();
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            mDialog.hide();
        }
    }
    
    @Override
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (this.mDialog != null && bundle != null) {
            bundle = bundle.getBundle("android:savedDialogState");
            if (bundle != null) {
                this.mDialog.onRestoreInstanceState(bundle);
            }
        }
    }
    
    @Override
    public void performCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        super.performCreateView(layoutInflater, viewGroup, bundle);
        if (super.mView == null && this.mDialog != null && bundle != null) {
            final Bundle bundle2 = bundle.getBundle("android:savedDialogState");
            if (bundle2 != null) {
                this.mDialog.onRestoreInstanceState(bundle2);
            }
        }
    }
    
    public final Dialog requireDialog() {
        final Dialog dialog = this.getDialog();
        if (dialog != null) {
            return dialog;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("DialogFragment ");
        sb.append(this);
        sb.append(" does not have a Dialog.");
        throw new IllegalStateException(sb.toString());
    }
    
    public void setCancelable(final boolean b) {
        this.mCancelable = b;
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            mDialog.setCancelable(b);
        }
    }
    
    public void setShowsDialog(final boolean mShowsDialog) {
        this.mShowsDialog = mShowsDialog;
    }
    
    public void setStyle(final int n, final int n2) {
        if (androidx.fragment.app.k.F0(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Setting style and theme for DialogFragment ");
            sb.append(this);
            sb.append(" to ");
            sb.append(n);
            sb.append(", ");
            sb.append(n2);
            Log.d("FragmentManager", sb.toString());
        }
        this.mStyle = n;
        if (n == 2 || n == 3) {
            this.mTheme = 16973913;
        }
        if (n2 != 0) {
            this.mTheme = n2;
        }
    }
    
    public void setupDialog(final Dialog dialog, final int n) {
        if (n != 1 && n != 2) {
            if (n != 3) {
                return;
            }
            final Window window = dialog.getWindow();
            if (window != null) {
                window.addFlags(24);
            }
        }
        dialog.requestWindowFeature(1);
    }
    
    public int show(final q q, final String s) {
        this.mDismissed = false;
        this.mShownByMe = true;
        q.d(this, s);
        this.mViewDestroyed = false;
        return this.mBackStackId = q.g();
    }
    
    public void show(final androidx.fragment.app.k k, final String s) {
        this.mDismissed = false;
        this.mShownByMe = true;
        final q m = k.m();
        m.d(this, s);
        m.g();
    }
    
    public void showNow(final androidx.fragment.app.k k, final String s) {
        this.mDismissed = false;
        this.mShownByMe = true;
        final q m = k.m();
        m.d(this, s);
        m.i();
    }
}

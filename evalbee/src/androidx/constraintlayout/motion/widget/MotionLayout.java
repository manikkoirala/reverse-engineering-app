// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import androidx.constraintlayout.widget.ConstraintLayout;

public abstract class MotionLayout extends ConstraintLayout implements oy0
{
    public static boolean a;
    
    public enum TransitionState
    {
        private static final TransitionState[] $VALUES;
        
        FINISHED, 
        MOVING, 
        SETUP, 
        UNDEFINED;
        
        private static /* synthetic */ TransitionState[] $values() {
            return new TransitionState[] { TransitionState.UNDEFINED, TransitionState.SETUP, TransitionState.MOVING, TransitionState.FINISHED };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import java.lang.reflect.Method;
import android.util.Log;
import android.graphics.drawable.Drawable;
import java.io.Serializable;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import java.util.Iterator;
import java.lang.reflect.InvocationTargetException;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import java.util.HashMap;

public class ConstraintAttribute
{
    public boolean a;
    public String b;
    public AttributeType c;
    public int d;
    public float e;
    public String f;
    public boolean g;
    public int h;
    
    public ConstraintAttribute(final ConstraintAttribute constraintAttribute, final Object o) {
        this.a = false;
        this.b = constraintAttribute.b;
        this.c = constraintAttribute.c;
        this.d(o);
    }
    
    public ConstraintAttribute(final String b, final AttributeType c, final Object o, final boolean a) {
        this.b = b;
        this.c = c;
        this.a = a;
        this.d(o);
    }
    
    public static HashMap a(final HashMap hashMap, final View obj) {
        final HashMap hashMap2 = new HashMap();
        final Class<? extends View> class1 = obj.getClass();
        for (final String key : hashMap.keySet()) {
            final ConstraintAttribute constraintAttribute = hashMap.get(key);
            Object value = null;
            try {
                if (key.equals("BackgroundColor")) {
                    value = new ConstraintAttribute(constraintAttribute, ((ColorDrawable)obj.getBackground()).getColor());
                }
                else {
                    value = new StringBuilder();
                    ((StringBuilder)value).append("getMap");
                    ((StringBuilder)value).append(key);
                    value = new ConstraintAttribute(constraintAttribute, class1.getMethod(((StringBuilder)value).toString(), (Class<?>[])new Class[0]).invoke(obj, new Object[0]));
                }
                hashMap2.put(key, value);
                continue;
            }
            catch (final InvocationTargetException value) {}
            catch (final IllegalAccessException value) {}
            catch (final NoSuchMethodException ex) {}
            ((Throwable)value).printStackTrace();
        }
        return hashMap2;
    }
    
    public static void b(final Context context, final XmlPullParser xmlPullParser, final HashMap hashMap) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), vb1.L4);
        final int indexCount = obtainStyledAttributes.getIndexCount();
        String key = null;
        Serializable s = null;
        AttributeType attributeType = null;
        int i = 0;
        int n = 0;
        while (i < indexCount) {
            final int index = obtainStyledAttributes.getIndex(i);
            String s3 = null;
            Serializable value = null;
            AttributeType boolean_TYPE = null;
            int n2 = 0;
            Label_0501: {
                if (index == vb1.M4) {
                    final String s2 = s3 = obtainStyledAttributes.getString(index);
                    value = s;
                    boolean_TYPE = attributeType;
                    n2 = n;
                    if (s2 != null) {
                        s3 = s2;
                        value = s;
                        boolean_TYPE = attributeType;
                        n2 = n;
                        if (s2.length() > 0) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append(Character.toUpperCase(s2.charAt(0)));
                            sb.append(s2.substring(1));
                            s3 = sb.toString();
                            value = s;
                            boolean_TYPE = attributeType;
                            n2 = n;
                        }
                    }
                }
                else if (index == vb1.W4) {
                    s3 = obtainStyledAttributes.getString(index);
                    n2 = 1;
                    value = s;
                    boolean_TYPE = attributeType;
                }
                else if (index == vb1.N4) {
                    value = obtainStyledAttributes.getBoolean(index, false);
                    boolean_TYPE = AttributeType.BOOLEAN_TYPE;
                    s3 = key;
                    n2 = n;
                }
                else {
                    AttributeType attributeType2 = null;
                    Serializable s4 = null;
                    Label_0259: {
                        int j = 0;
                        Label_0253: {
                            if (index == vb1.P4) {
                                attributeType2 = AttributeType.COLOR_TYPE;
                            }
                            else {
                                if (index != vb1.O4) {
                                    float f;
                                    if (index == vb1.T4) {
                                        attributeType2 = AttributeType.DIMENSION_TYPE;
                                        f = TypedValue.applyDimension(1, obtainStyledAttributes.getDimension(index, 0.0f), context.getResources().getDisplayMetrics());
                                    }
                                    else if (index == vb1.Q4) {
                                        attributeType2 = AttributeType.DIMENSION_TYPE;
                                        f = obtainStyledAttributes.getDimension(index, 0.0f);
                                    }
                                    else if (index == vb1.R4) {
                                        attributeType2 = AttributeType.FLOAT_TYPE;
                                        f = obtainStyledAttributes.getFloat(index, Float.NaN);
                                    }
                                    else {
                                        if (index == vb1.S4) {
                                            attributeType2 = AttributeType.INT_TYPE;
                                            j = obtainStyledAttributes.getInteger(index, -1);
                                            break Label_0253;
                                        }
                                        if (index == vb1.V4) {
                                            attributeType2 = AttributeType.STRING_TYPE;
                                            s4 = obtainStyledAttributes.getString(index);
                                            break Label_0259;
                                        }
                                        s3 = key;
                                        value = s;
                                        boolean_TYPE = attributeType;
                                        n2 = n;
                                        if (index != vb1.U4) {
                                            break Label_0501;
                                        }
                                        final AttributeType reference_TYPE = AttributeType.REFERENCE_TYPE;
                                        final int resourceId = obtainStyledAttributes.getResourceId(index, -1);
                                        attributeType2 = reference_TYPE;
                                        if ((j = resourceId) == -1) {
                                            j = obtainStyledAttributes.getInt(index, -1);
                                            attributeType2 = reference_TYPE;
                                        }
                                        break Label_0253;
                                    }
                                    s4 = f;
                                    break Label_0259;
                                }
                                attributeType2 = AttributeType.COLOR_DRAWABLE_TYPE;
                            }
                            j = obtainStyledAttributes.getColor(index, 0);
                        }
                        s4 = j;
                    }
                    boolean_TYPE = attributeType2;
                    value = s4;
                    s3 = key;
                    n2 = n;
                }
            }
            ++i;
            key = s3;
            s = value;
            attributeType = boolean_TYPE;
            n = n2;
        }
        if (key != null && s != null) {
            hashMap.put(key, new ConstraintAttribute(key, attributeType, s, (boolean)(n != 0)));
        }
        obtainStyledAttributes.recycle();
    }
    
    public static void c(final View view, final HashMap hashMap) {
        final Class<? extends View> class1 = view.getClass();
        for (final String str : hashMap.keySet()) {
            final ConstraintAttribute constraintAttribute = hashMap.get(str);
            String string;
            if (!constraintAttribute.a) {
                final StringBuilder sb = new StringBuilder();
                sb.append("set");
                sb.append(str);
                string = sb.toString();
            }
            else {
                string = str;
            }
            try {
                switch (ConstraintAttribute$a.a[constraintAttribute.c.ordinal()]) {
                    default: {
                        continue;
                    }
                    case 8: {
                        class1.getMethod(string, Float.TYPE).invoke(view, constraintAttribute.e);
                        continue;
                    }
                    case 7: {
                        class1.getMethod(string, Float.TYPE).invoke(view, constraintAttribute.e);
                        continue;
                    }
                    case 6: {
                        class1.getMethod(string, Integer.TYPE).invoke(view, constraintAttribute.d);
                        continue;
                    }
                    case 5: {
                        final Method method = class1.getMethod(string, Drawable.class);
                        final ColorDrawable colorDrawable = new ColorDrawable();
                        colorDrawable.setColor(constraintAttribute.h);
                        method.invoke(view, colorDrawable);
                        continue;
                    }
                    case 4: {
                        class1.getMethod(string, Integer.TYPE).invoke(view, constraintAttribute.h);
                        continue;
                    }
                    case 3: {
                        class1.getMethod(string, CharSequence.class).invoke(view, constraintAttribute.f);
                        continue;
                    }
                    case 2: {
                        class1.getMethod(string, Boolean.TYPE).invoke(view, constraintAttribute.g);
                        continue;
                    }
                    case 1: {
                        class1.getMethod(string, Integer.TYPE).invoke(view, constraintAttribute.d);
                        continue;
                    }
                }
            }
            catch (final InvocationTargetException ex) {
                final StringBuilder sb2 = new StringBuilder();
                goto Label_0491;
            }
            catch (final IllegalAccessException ex) {
                final StringBuilder sb2 = new StringBuilder();
            }
            catch (final NoSuchMethodException ex2) {
                Log.e("TransitionLayout", ex2.getMessage());
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" Custom Attribute \"");
                sb3.append(str);
                sb3.append("\" not found on ");
                sb3.append(class1.getName());
                Log.e("TransitionLayout", sb3.toString());
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(class1.getName());
                sb4.append(" must have a method ");
                sb4.append(string);
                Log.e("TransitionLayout", sb4.toString());
                continue;
            }
            break;
        }
    }
    
    public void d(final Object o) {
        switch (ConstraintAttribute$a.a[this.c.ordinal()]) {
            case 7:
            case 8: {
                this.e = (float)o;
                break;
            }
            case 4:
            case 5: {
                this.h = (int)o;
                break;
            }
            case 3: {
                this.f = (String)o;
                break;
            }
            case 2: {
                this.g = (boolean)o;
                break;
            }
            case 1:
            case 6: {
                this.d = (int)o;
                break;
            }
        }
    }
    
    public enum AttributeType
    {
        private static final AttributeType[] $VALUES;
        
        BOOLEAN_TYPE, 
        COLOR_DRAWABLE_TYPE, 
        COLOR_TYPE, 
        DIMENSION_TYPE, 
        FLOAT_TYPE, 
        INT_TYPE, 
        REFERENCE_TYPE, 
        STRING_TYPE;
        
        private static /* synthetic */ AttributeType[] $values() {
            return new AttributeType[] { AttributeType.INT_TYPE, AttributeType.FLOAT_TYPE, AttributeType.COLOR_TYPE, AttributeType.COLOR_DRAWABLE_TYPE, AttributeType.STRING_TYPE, AttributeType.BOOLEAN_TYPE, AttributeType.DIMENSION_TYPE, AttributeType.REFERENCE_TYPE };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import java.util.Locale;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.util.AttributeSet;
import java.util.Arrays;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.util.Iterator;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import java.util.Collection;
import java.util.HashSet;
import android.util.Log;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.content.Context;
import android.content.res.TypedArray;
import java.util.HashMap;
import android.util.SparseIntArray;

public class c
{
    public static final int[] f;
    public static SparseIntArray g;
    public static SparseIntArray h;
    public String a;
    public int b;
    public HashMap c;
    public boolean d;
    public HashMap e;
    
    static {
        f = new int[] { 0, 4, 8 };
        c.g = new SparseIntArray();
        c.h = new SparseIntArray();
        c.g.append(vb1.A0, 25);
        c.g.append(vb1.B0, 26);
        c.g.append(vb1.D0, 29);
        c.g.append(vb1.E0, 30);
        c.g.append(vb1.K0, 36);
        c.g.append(vb1.J0, 35);
        c.g.append(vb1.h0, 4);
        c.g.append(vb1.g0, 3);
        c.g.append(vb1.c0, 1);
        c.g.append(vb1.e0, 91);
        c.g.append(vb1.d0, 92);
        c.g.append(vb1.T0, 6);
        c.g.append(vb1.U0, 7);
        c.g.append(vb1.o0, 17);
        c.g.append(vb1.p0, 18);
        c.g.append(vb1.q0, 19);
        c.g.append(vb1.Y, 99);
        c.g.append(vb1.u, 27);
        c.g.append(vb1.F0, 32);
        c.g.append(vb1.G0, 33);
        c.g.append(vb1.n0, 10);
        c.g.append(vb1.m0, 9);
        c.g.append(vb1.X0, 13);
        c.g.append(vb1.a1, 16);
        c.g.append(vb1.Y0, 14);
        c.g.append(vb1.V0, 11);
        c.g.append(vb1.Z0, 15);
        c.g.append(vb1.W0, 12);
        c.g.append(vb1.N0, 40);
        c.g.append(vb1.y0, 39);
        c.g.append(vb1.x0, 41);
        c.g.append(vb1.M0, 42);
        c.g.append(vb1.w0, 20);
        c.g.append(vb1.L0, 37);
        c.g.append(vb1.l0, 5);
        c.g.append(vb1.z0, 87);
        c.g.append(vb1.I0, 87);
        c.g.append(vb1.C0, 87);
        c.g.append(vb1.f0, 87);
        c.g.append(vb1.b0, 87);
        c.g.append(vb1.z, 24);
        c.g.append(vb1.B, 28);
        c.g.append(vb1.N, 31);
        c.g.append(vb1.O, 8);
        c.g.append(vb1.A, 34);
        c.g.append(vb1.C, 2);
        c.g.append(vb1.x, 23);
        c.g.append(vb1.y, 21);
        c.g.append(vb1.O0, 95);
        c.g.append(vb1.r0, 96);
        c.g.append(vb1.w, 22);
        c.g.append(vb1.D, 43);
        c.g.append(vb1.Q, 44);
        c.g.append(vb1.L, 45);
        c.g.append(vb1.M, 46);
        c.g.append(vb1.K, 60);
        c.g.append(vb1.I, 47);
        c.g.append(vb1.J, 48);
        c.g.append(vb1.E, 49);
        c.g.append(vb1.F, 50);
        c.g.append(vb1.G, 51);
        c.g.append(vb1.H, 52);
        c.g.append(vb1.P, 53);
        c.g.append(vb1.P0, 54);
        c.g.append(vb1.s0, 55);
        c.g.append(vb1.Q0, 56);
        c.g.append(vb1.t0, 57);
        c.g.append(vb1.R0, 58);
        c.g.append(vb1.u0, 59);
        c.g.append(vb1.i0, 61);
        c.g.append(vb1.k0, 62);
        c.g.append(vb1.j0, 63);
        c.g.append(vb1.R, 64);
        c.g.append(vb1.k1, 65);
        c.g.append(vb1.X, 66);
        c.g.append(vb1.l1, 67);
        c.g.append(vb1.d1, 79);
        c.g.append(vb1.v, 38);
        c.g.append(vb1.c1, 68);
        c.g.append(vb1.S0, 69);
        c.g.append(vb1.v0, 70);
        c.g.append(vb1.b1, 97);
        c.g.append(vb1.V, 71);
        c.g.append(vb1.T, 72);
        c.g.append(vb1.U, 73);
        c.g.append(vb1.W, 74);
        c.g.append(vb1.S, 75);
        c.g.append(vb1.e1, 76);
        c.g.append(vb1.H0, 77);
        c.g.append(vb1.m1, 78);
        c.g.append(vb1.a0, 80);
        c.g.append(vb1.Z, 81);
        c.g.append(vb1.f1, 82);
        c.g.append(vb1.j1, 83);
        c.g.append(vb1.i1, 84);
        c.g.append(vb1.h1, 85);
        c.g.append(vb1.g1, 86);
        final SparseIntArray h = c.h;
        final int q4 = vb1.q4;
        h.append(q4, 6);
        c.h.append(q4, 7);
        c.h.append(vb1.l3, 27);
        c.h.append(vb1.t4, 13);
        c.h.append(vb1.w4, 16);
        c.h.append(vb1.u4, 14);
        c.h.append(vb1.r4, 11);
        c.h.append(vb1.v4, 15);
        c.h.append(vb1.s4, 12);
        c.h.append(vb1.k4, 40);
        c.h.append(vb1.d4, 39);
        c.h.append(vb1.c4, 41);
        c.h.append(vb1.j4, 42);
        c.h.append(vb1.b4, 20);
        c.h.append(vb1.i4, 37);
        c.h.append(vb1.V3, 5);
        c.h.append(vb1.e4, 87);
        c.h.append(vb1.h4, 87);
        c.h.append(vb1.f4, 87);
        c.h.append(vb1.S3, 87);
        c.h.append(vb1.R3, 87);
        c.h.append(vb1.q3, 24);
        c.h.append(vb1.s3, 28);
        c.h.append(vb1.E3, 31);
        c.h.append(vb1.F3, 8);
        c.h.append(vb1.r3, 34);
        c.h.append(vb1.t3, 2);
        c.h.append(vb1.o3, 23);
        c.h.append(vb1.p3, 21);
        c.h.append(vb1.l4, 95);
        c.h.append(vb1.W3, 96);
        c.h.append(vb1.n3, 22);
        c.h.append(vb1.u3, 43);
        c.h.append(vb1.H3, 44);
        c.h.append(vb1.C3, 45);
        c.h.append(vb1.D3, 46);
        c.h.append(vb1.B3, 60);
        c.h.append(vb1.z3, 47);
        c.h.append(vb1.A3, 48);
        c.h.append(vb1.v3, 49);
        c.h.append(vb1.w3, 50);
        c.h.append(vb1.x3, 51);
        c.h.append(vb1.y3, 52);
        c.h.append(vb1.G3, 53);
        c.h.append(vb1.m4, 54);
        c.h.append(vb1.X3, 55);
        c.h.append(vb1.n4, 56);
        c.h.append(vb1.Y3, 57);
        c.h.append(vb1.o4, 58);
        c.h.append(vb1.Z3, 59);
        c.h.append(vb1.U3, 62);
        c.h.append(vb1.T3, 63);
        c.h.append(vb1.I3, 64);
        c.h.append(vb1.H4, 65);
        c.h.append(vb1.O3, 66);
        c.h.append(vb1.I4, 67);
        c.h.append(vb1.z4, 79);
        c.h.append(vb1.m3, 38);
        c.h.append(vb1.A4, 98);
        c.h.append(vb1.y4, 68);
        c.h.append(vb1.p4, 69);
        c.h.append(vb1.a4, 70);
        c.h.append(vb1.M3, 71);
        c.h.append(vb1.K3, 72);
        c.h.append(vb1.L3, 73);
        c.h.append(vb1.N3, 74);
        c.h.append(vb1.J3, 75);
        c.h.append(vb1.B4, 76);
        c.h.append(vb1.g4, 77);
        c.h.append(vb1.J4, 78);
        c.h.append(vb1.Q3, 80);
        c.h.append(vb1.P3, 81);
        c.h.append(vb1.C4, 82);
        c.h.append(vb1.G4, 83);
        c.h.append(vb1.F4, 84);
        c.h.append(vb1.E4, 85);
        c.h.append(vb1.D4, 86);
        c.h.append(vb1.x4, 97);
    }
    
    public c() {
        this.a = "";
        this.b = 0;
        this.c = new HashMap();
        this.d = true;
        this.e = new HashMap();
    }
    
    public static /* synthetic */ int[] b() {
        return c.f;
    }
    
    public static int m(final TypedArray typedArray, final int n, int n2) {
        if ((n2 = typedArray.getResourceId(n, n2)) == -1) {
            n2 = typedArray.getInt(n, -1);
        }
        return n2;
    }
    
    public static void n(final Object o, final TypedArray typedArray, int dimensionPixelSize, final int n) {
        if (o == null) {
            return;
        }
        final int type = typedArray.peekValue(dimensionPixelSize).type;
        if (type != 3) {
            final int n2 = 0;
            boolean b = false;
            Label_0099: {
                Label_0096: {
                    if (type != 5) {
                        final int int1 = typedArray.getInt(dimensionPixelSize, 0);
                        if (int1 != -4) {
                            if (int1 != -3) {
                                dimensionPixelSize = int1;
                                if (int1 == -2 || (dimensionPixelSize = int1) == -1) {
                                    break Label_0096;
                                }
                            }
                            b = false;
                            dimensionPixelSize = n2;
                            break Label_0099;
                        }
                        b = true;
                        dimensionPixelSize = -2;
                        break Label_0099;
                    }
                    else {
                        dimensionPixelSize = typedArray.getDimensionPixelSize(dimensionPixelSize, 0);
                    }
                }
                b = false;
            }
            if (o instanceof ConstraintLayout.b) {
                final ConstraintLayout.b b2 = (ConstraintLayout.b)o;
                if (n == 0) {
                    b2.width = dimensionPixelSize;
                    b2.a0 = b;
                }
                else {
                    b2.height = dimensionPixelSize;
                    b2.b0 = b;
                }
            }
            else if (o instanceof b) {
                final b b3 = (b)o;
                if (n == 0) {
                    b3.d = dimensionPixelSize;
                    b3.n0 = b;
                }
                else {
                    b3.e = dimensionPixelSize;
                    b3.o0 = b;
                }
            }
            else if (o instanceof a.a) {
                final a.a a = (a.a)o;
                if (n == 0) {
                    a.b(23, dimensionPixelSize);
                    dimensionPixelSize = 80;
                }
                else {
                    a.b(21, dimensionPixelSize);
                    dimensionPixelSize = 81;
                }
                a.d(dimensionPixelSize, b);
            }
            return;
        }
        o(o, typedArray.getString(dimensionPixelSize), n);
    }
    
    public static void o(final Object o, String s, int n) {
        if (s == null) {
            return;
        }
        final int index = s.indexOf(61);
        final int length = s.length();
        if (index <= 0 || index >= length - 1) {
            return;
        }
        final String substring = s.substring(0, index);
        s = s.substring(index + 1);
        if (s.length() <= 0) {
            return;
        }
        final String trim = substring.trim();
        s = s.trim();
        Label_0303: {
            if ("ratio".equalsIgnoreCase(trim)) {
                if (o instanceof ConstraintLayout.b) {
                    final ConstraintLayout.b b = (ConstraintLayout.b)o;
                    if (n == 0) {
                        b.width = 0;
                    }
                    else {
                        b.height = 0;
                    }
                    p(b, s);
                    return;
                }
                if (o instanceof b) {
                    ((b)o).A = s;
                    return;
                }
                if (o instanceof a.a) {
                    ((a.a)o).c(5, s);
                }
                return;
            }
            else if (!"weight".equalsIgnoreCase(trim)) {
                break Label_0303;
            }
            try {
                final float float1 = Float.parseFloat(s);
                if (o instanceof ConstraintLayout.b) {
                    final ConstraintLayout.b b2 = (ConstraintLayout.b)o;
                    if (n == 0) {
                        b2.width = 0;
                        b2.L = float1;
                    }
                    else {
                        b2.height = 0;
                        b2.M = float1;
                    }
                }
                else if (o instanceof b) {
                    final b b3 = (b)o;
                    if (n == 0) {
                        b3.d = 0;
                        b3.W = float1;
                    }
                    else {
                        b3.e = 0;
                        b3.V = float1;
                    }
                }
                else if (o instanceof a.a) {
                    final a.a a = (a.a)o;
                    if (n == 0) {
                        a.b(23, 0);
                        n = 39;
                    }
                    else {
                        a.b(21, 0);
                        n = 40;
                    }
                    a.a(n, float1);
                }
                Label_0479: {
                    return;
                }
            Block_18_Outer:
                while (true) {
                    a.a a2 = null;
                    a2.b(n, 2);
                    return;
                    ConstraintLayout.b b4 = null;
                    Label_0361:
                    b4.height = 0;
                    float max = 0.0f;
                    b4.W = max;
                    b4.Q = 2;
                    return;
                    Block_24: {
                    Block_21_Outer:
                        while (true) {
                            max = Math.max(0.0f, Math.min(1.0f, Float.parseFloat(s)));
                            iftrue(Label_0379:)(!(o instanceof ConstraintLayout.b));
                            while (true) {
                                final b b5;
                                Block_22: {
                                    Block_19: {
                                        break Block_19;
                                        b5 = (b)o;
                                        iftrue(Label_0413:)(n != 0);
                                        break Block_22;
                                        Label_0431:
                                        iftrue(Label_0479:)(!(o instanceof a.a));
                                        Block_23: {
                                            break Block_23;
                                            Label_0413:
                                            b5.e = 0;
                                            b5.g0 = max;
                                            b5.a0 = 2;
                                            return;
                                        }
                                        a2 = (a.a)o;
                                        iftrue(Label_0466:)(n != 0);
                                        break Block_24;
                                    }
                                    b4 = (ConstraintLayout.b)o;
                                    iftrue(Label_0361:)(n != 0);
                                    b4.width = 0;
                                    b4.V = max;
                                    b4.P = 2;
                                    return;
                                    Label_0466:
                                    a2.b(21, 0);
                                    n = 55;
                                    continue Block_18_Outer;
                                }
                                b5.d = 0;
                                b5.f0 = max;
                                b5.Z = 2;
                                return;
                                Label_0379:
                                iftrue(Label_0431:)(!(o instanceof b));
                                continue;
                            }
                            iftrue(Label_0479:)(!"parent".equalsIgnoreCase(trim));
                            continue Block_21_Outer;
                        }
                    }
                    a2.b(23, 0);
                    n = 54;
                    continue Block_18_Outer;
                }
            }
            catch (final NumberFormatException ex) {}
        }
    }
    
    public static void p(final ConstraintLayout.b b, final String i) {
        final float n = Float.NaN;
        final int n2 = -1;
        float j = n;
        int k = n2;
        while (true) {
            if (i == null) {
                break Label_0294;
            }
            final int length = i.length();
            final int index = i.indexOf(44);
            final int n3 = 0;
            int n4 = n2;
            int n5 = n3;
            if (index > 0) {
                n4 = n2;
                n5 = n3;
                if (index < length - 1) {
                    final String substring = i.substring(0, index);
                    if (substring.equalsIgnoreCase("W")) {
                        n4 = 0;
                    }
                    else {
                        n4 = n2;
                        if (substring.equalsIgnoreCase("H")) {
                            n4 = 1;
                        }
                    }
                    n5 = index + 1;
                }
            }
            final int index2 = i.indexOf(58);
            Label_0262: {
                if (index2 < 0 || index2 >= length - 1) {
                    break Label_0262;
                }
                final String substring2 = i.substring(n5, index2);
                final String substring3 = i.substring(index2 + 1);
                j = n;
                k = n4;
                if (substring2.length() <= 0) {
                    break Label_0294;
                }
                j = n;
                k = n4;
                if (substring3.length() <= 0) {
                    break Label_0294;
                }
                try {
                    final float float1 = Float.parseFloat(substring2);
                    final float float2 = Float.parseFloat(substring3);
                    j = n;
                    k = n4;
                    if (float1 > 0.0f) {
                        j = n;
                        k = n4;
                        if (float2 > 0.0f) {
                            if (n4 == 1) {
                                j = Math.abs(float2 / float1);
                                k = n4;
                            }
                            else {
                                j = Math.abs(float1 / float2);
                                k = n4;
                            }
                        }
                    }
                    b.I = i;
                    b.J = j;
                    b.K = k;
                    return;
                    final String substring4 = i.substring(n5);
                    j = n;
                    k = n4;
                    iftrue(Label_0294:)(substring4.length() <= 0);
                    j = Float.parseFloat(substring4);
                    k = n4;
                    continue;
                }
                catch (final NumberFormatException ex) {
                    j = n;
                    k = n4;
                    continue;
                }
            }
            break;
        }
    }
    
    public static void r(final Context context, final a a, final TypedArray typedArray) {
        final int indexCount = typedArray.getIndexCount();
        final a.a h = new a.a();
        a.h = h;
        a.d.a = false;
        a.e.b = false;
        a.c.a = false;
        a.f.a = false;
        for (int i = 0; i < indexCount; ++i) {
            final int index = typedArray.getIndex(i);
            int a2 = 0;
            int n2 = 0;
            Label_2491: {
                int n5 = 0;
                Label_2457: {
                    float n3 = 0.0f;
                    int n4 = 0;
                    Label_2201: {
                        Label_1278: {
                            boolean b = false;
                            int n = 0;
                            Label_1191: {
                                StringBuilder sb = null;
                                String str = null;
                                switch (c.h.get(index)) {
                                    default: {
                                        sb = new StringBuilder();
                                        str = "Unknown attribute 0x";
                                        break;
                                    }
                                    case 99: {
                                        b = typedArray.getBoolean(index, a.e.i);
                                        n = 99;
                                        break Label_1191;
                                    }
                                    case 98: {
                                        if (MotionLayout.a) {
                                            if ((a.a = typedArray.getResourceId(index, a.a)) != -1) {
                                                continue;
                                            }
                                        }
                                        else if (typedArray.peekValue(index).type != 3) {
                                            a.a = typedArray.getResourceId(index, a.a);
                                            continue;
                                        }
                                        a.b = typedArray.getString(index);
                                        continue;
                                    }
                                    case 97: {
                                        a2 = typedArray.getInt(index, a.e.q0);
                                        n2 = 97;
                                        break Label_2491;
                                    }
                                    case 96: {
                                        n(h, typedArray, index, 1);
                                        continue;
                                    }
                                    case 95: {
                                        n(h, typedArray, index, 0);
                                        continue;
                                    }
                                    case 94: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.U);
                                        n2 = 94;
                                        break Label_2491;
                                    }
                                    case 93: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.N);
                                        n2 = 93;
                                        break Label_2491;
                                    }
                                    case 87: {
                                        sb = new StringBuilder();
                                        str = "unused attribute 0x";
                                        break;
                                    }
                                    case 86: {
                                        final int type = typedArray.peekValue(index).type;
                                        c c;
                                        if (type == 1) {
                                            h.b(89, a.d.n = typedArray.getResourceId(index, -1));
                                            c = a.d;
                                            if (c.n == -1) {
                                                continue;
                                            }
                                        }
                                        else {
                                            if (type != 3) {
                                                final c d = a.d;
                                                d.m = typedArray.getInteger(index, d.n);
                                                h.b(88, a.d.m);
                                                continue;
                                            }
                                            h.c(90, a.d.l = typedArray.getString(index));
                                            if (a.d.l.indexOf("/") <= 0) {
                                                h.b(88, a.d.m = -1);
                                                continue;
                                            }
                                            h.b(89, a.d.n = typedArray.getResourceId(index, -1));
                                            c = a.d;
                                        }
                                        h.b(88, c.m = -2);
                                        continue;
                                    }
                                    case 85: {
                                        n3 = typedArray.getFloat(index, a.d.j);
                                        n4 = 85;
                                        break Label_2201;
                                    }
                                    case 84: {
                                        a2 = typedArray.getInteger(index, a.d.k);
                                        n2 = 84;
                                        break Label_2491;
                                    }
                                    case 83: {
                                        a2 = m(typedArray, index, a.f.i);
                                        n2 = 83;
                                        break Label_2491;
                                    }
                                    case 82: {
                                        a2 = typedArray.getInteger(index, a.d.c);
                                        n2 = 82;
                                        break Label_2491;
                                    }
                                    case 81: {
                                        b = typedArray.getBoolean(index, a.e.o0);
                                        n = 81;
                                        break Label_1191;
                                    }
                                    case 80: {
                                        b = typedArray.getBoolean(index, a.e.n0);
                                        n = 80;
                                        break Label_1191;
                                    }
                                    case 79: {
                                        n3 = typedArray.getFloat(index, a.d.g);
                                        n4 = 79;
                                        break Label_2201;
                                    }
                                    case 78: {
                                        a2 = typedArray.getInt(index, a.c.c);
                                        n2 = 78;
                                        break Label_2491;
                                    }
                                    case 77: {
                                        n5 = 77;
                                        break Label_2457;
                                    }
                                    case 76: {
                                        a2 = typedArray.getInt(index, a.d.e);
                                        n2 = 76;
                                        break Label_2491;
                                    }
                                    case 75: {
                                        b = typedArray.getBoolean(index, a.e.p0);
                                        n = 75;
                                        break Label_1191;
                                    }
                                    case 74: {
                                        n5 = 74;
                                        break Label_2457;
                                    }
                                    case 73: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.i0);
                                        n2 = 73;
                                        break Label_2491;
                                    }
                                    case 72: {
                                        a2 = typedArray.getInt(index, a.e.h0);
                                        n2 = 72;
                                        break Label_2491;
                                    }
                                    case 71: {
                                        Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                                        continue;
                                    }
                                    case 70: {
                                        n4 = 70;
                                        break Label_1278;
                                    }
                                    case 69: {
                                        n4 = 69;
                                        break Label_1278;
                                    }
                                    case 68: {
                                        n3 = typedArray.getFloat(index, a.c.e);
                                        n4 = 68;
                                        break Label_2201;
                                    }
                                    case 67: {
                                        n3 = typedArray.getFloat(index, a.d.i);
                                        n4 = 67;
                                        break Label_2201;
                                    }
                                    case 66: {
                                        n2 = 66;
                                        a2 = typedArray.getInt(index, 0);
                                        break Label_2491;
                                    }
                                    case 65: {
                                        String string;
                                        if (typedArray.peekValue(index).type == 3) {
                                            string = typedArray.getString(index);
                                        }
                                        else {
                                            string = xv.c[typedArray.getInteger(index, 0)];
                                        }
                                        h.c(65, string);
                                        continue;
                                    }
                                    case 64: {
                                        a2 = m(typedArray, index, a.d.b);
                                        n2 = 64;
                                        break Label_2491;
                                    }
                                    case 63: {
                                        n3 = typedArray.getFloat(index, a.e.D);
                                        n4 = 63;
                                        break Label_2201;
                                    }
                                    case 62: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.C);
                                        n2 = 62;
                                        break Label_2491;
                                    }
                                    case 60: {
                                        n3 = typedArray.getFloat(index, a.f.b);
                                        n4 = 60;
                                        break Label_2201;
                                    }
                                    case 59: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.e0);
                                        n2 = 59;
                                        break Label_2491;
                                    }
                                    case 58: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.d0);
                                        n2 = 58;
                                        break Label_2491;
                                    }
                                    case 57: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.c0);
                                        n2 = 57;
                                        break Label_2491;
                                    }
                                    case 56: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.b0);
                                        n2 = 56;
                                        break Label_2491;
                                    }
                                    case 55: {
                                        a2 = typedArray.getInt(index, a.e.a0);
                                        n2 = 55;
                                        break Label_2491;
                                    }
                                    case 54: {
                                        a2 = typedArray.getInt(index, a.e.Z);
                                        n2 = 54;
                                        break Label_2491;
                                    }
                                    case 53: {
                                        n3 = typedArray.getDimension(index, a.f.l);
                                        n4 = 53;
                                        break Label_2201;
                                    }
                                    case 52: {
                                        n3 = typedArray.getDimension(index, a.f.k);
                                        n4 = 52;
                                        break Label_2201;
                                    }
                                    case 51: {
                                        n3 = typedArray.getDimension(index, a.f.j);
                                        n4 = 51;
                                        break Label_2201;
                                    }
                                    case 50: {
                                        n3 = typedArray.getDimension(index, a.f.h);
                                        n4 = 50;
                                        break Label_2201;
                                    }
                                    case 49: {
                                        n3 = typedArray.getDimension(index, a.f.g);
                                        n4 = 49;
                                        break Label_2201;
                                    }
                                    case 48: {
                                        n3 = typedArray.getFloat(index, a.f.f);
                                        n4 = 48;
                                        break Label_2201;
                                    }
                                    case 47: {
                                        n3 = typedArray.getFloat(index, a.f.e);
                                        n4 = 47;
                                        break Label_2201;
                                    }
                                    case 46: {
                                        n3 = typedArray.getFloat(index, a.f.d);
                                        n4 = 46;
                                        break Label_2201;
                                    }
                                    case 45: {
                                        n3 = typedArray.getFloat(index, a.f.c);
                                        n4 = 45;
                                        break Label_2201;
                                    }
                                    case 44: {
                                        n4 = 44;
                                        h.d(44, true);
                                        n3 = typedArray.getDimension(index, a.f.n);
                                        break Label_2201;
                                    }
                                    case 43: {
                                        n3 = typedArray.getFloat(index, a.c.d);
                                        n4 = 43;
                                        break Label_2201;
                                    }
                                    case 42: {
                                        a2 = typedArray.getInt(index, a.e.Y);
                                        n2 = 42;
                                        break Label_2491;
                                    }
                                    case 41: {
                                        a2 = typedArray.getInt(index, a.e.X);
                                        n2 = 41;
                                        break Label_2491;
                                    }
                                    case 40: {
                                        n3 = typedArray.getFloat(index, a.e.V);
                                        n4 = 40;
                                        break Label_2201;
                                    }
                                    case 39: {
                                        n3 = typedArray.getFloat(index, a.e.W);
                                        n4 = 39;
                                        break Label_2201;
                                    }
                                    case 38: {
                                        a2 = typedArray.getResourceId(index, a.a);
                                        a.a = a2;
                                        n2 = 38;
                                        break Label_2491;
                                    }
                                    case 37: {
                                        n3 = typedArray.getFloat(index, a.e.z);
                                        n4 = 37;
                                        break Label_2201;
                                    }
                                    case 34: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.J);
                                        n2 = 34;
                                        break Label_2491;
                                    }
                                    case 31: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.M);
                                        n2 = 31;
                                        break Label_2491;
                                    }
                                    case 28: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.I);
                                        n2 = 28;
                                        break Label_2491;
                                    }
                                    case 27: {
                                        a2 = typedArray.getInt(index, a.e.G);
                                        n2 = 27;
                                        break Label_2491;
                                    }
                                    case 24: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.H);
                                        n2 = 24;
                                        break Label_2491;
                                    }
                                    case 23: {
                                        a2 = typedArray.getLayoutDimension(index, a.e.d);
                                        n2 = 23;
                                        break Label_2491;
                                    }
                                    case 22: {
                                        a2 = c.f[typedArray.getInt(index, a.c.b)];
                                        n2 = 22;
                                        break Label_2491;
                                    }
                                    case 21: {
                                        a2 = typedArray.getLayoutDimension(index, a.e.e);
                                        n2 = 21;
                                        break Label_2491;
                                    }
                                    case 20: {
                                        n3 = typedArray.getFloat(index, a.e.y);
                                        n4 = 20;
                                        break Label_2201;
                                    }
                                    case 19: {
                                        n3 = typedArray.getFloat(index, a.e.h);
                                        n4 = 19;
                                        break Label_2201;
                                    }
                                    case 18: {
                                        a2 = typedArray.getDimensionPixelOffset(index, a.e.g);
                                        n2 = 18;
                                        break Label_2491;
                                    }
                                    case 17: {
                                        a2 = typedArray.getDimensionPixelOffset(index, a.e.f);
                                        n2 = 17;
                                        break Label_2491;
                                    }
                                    case 16: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.P);
                                        n2 = 16;
                                        break Label_2491;
                                    }
                                    case 15: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.T);
                                        n2 = 15;
                                        break Label_2491;
                                    }
                                    case 14: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.Q);
                                        n2 = 14;
                                        break Label_2491;
                                    }
                                    case 13: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.O);
                                        n2 = 13;
                                        break Label_2491;
                                    }
                                    case 12: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.S);
                                        n2 = 12;
                                        break Label_2491;
                                    }
                                    case 11: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.R);
                                        n2 = 11;
                                        break Label_2491;
                                    }
                                    case 8: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.L);
                                        n2 = 8;
                                        break Label_2491;
                                    }
                                    case 7: {
                                        a2 = typedArray.getDimensionPixelOffset(index, a.e.F);
                                        n2 = 7;
                                        break Label_2491;
                                    }
                                    case 6: {
                                        a2 = typedArray.getDimensionPixelOffset(index, a.e.E);
                                        n2 = 6;
                                        break Label_2491;
                                    }
                                    case 5: {
                                        n5 = 5;
                                        break Label_2457;
                                    }
                                    case 2: {
                                        a2 = typedArray.getDimensionPixelSize(index, a.e.K);
                                        n2 = 2;
                                        break Label_2491;
                                    }
                                }
                                sb.append(str);
                                sb.append(Integer.toHexString(index));
                                sb.append("   ");
                                sb.append(c.g.get(index));
                                Log.w("ConstraintSet", sb.toString());
                                continue;
                            }
                            h.d(n, b);
                            continue;
                        }
                        n3 = typedArray.getFloat(index, 1.0f);
                    }
                    h.a(n4, n3);
                    continue;
                }
                h.c(n5, typedArray.getString(index));
                continue;
            }
            h.b(n2, a2);
        }
    }
    
    public void c(final ConstraintLayout constraintLayout) {
        this.d(constraintLayout, true);
        constraintLayout.setConstraintSet(null);
        constraintLayout.requestLayout();
    }
    
    public void d(final ConstraintLayout constraintLayout, final boolean b) {
        final int childCount = constraintLayout.getChildCount();
        final HashSet set = new HashSet(this.e.keySet());
        final int n = 0;
        for (int i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            final int id = child.getId();
            if (!this.e.containsKey(id)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("id unknown ");
                sb.append(vp.a(child));
                Log.w("ConstraintSet", sb.toString());
            }
            else {
                if (this.d && id == -1) {
                    throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
                }
                if (id != -1) {
                    if (this.e.containsKey(id)) {
                        set.remove(id);
                        final a a = this.e.get(id);
                        if (a != null) {
                            if (child instanceof androidx.constraintlayout.widget.a) {
                                a.e.j0 = 1;
                                final androidx.constraintlayout.widget.a a2 = (androidx.constraintlayout.widget.a)child;
                                a2.setId(id);
                                a2.setType(a.e.h0);
                                a2.setMargin(a.e.i0);
                                a2.setAllowsGoneWidget(a.e.p0);
                                final b e = a.e;
                                final int[] k0 = e.k0;
                                if (k0 != null) {
                                    a2.setReferencedIds(k0);
                                }
                                else {
                                    final String l0 = e.l0;
                                    if (l0 != null) {
                                        e.k0 = this.h(a2, l0);
                                        a2.setReferencedIds(a.e.k0);
                                    }
                                }
                            }
                            final ConstraintLayout.b layoutParams = (ConstraintLayout.b)child.getLayoutParams();
                            layoutParams.a();
                            a.b(layoutParams);
                            if (b) {
                                ConstraintAttribute.c(child, a.g);
                            }
                            child.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
                            final d c = a.c;
                            if (c.c == 0) {
                                child.setVisibility(c.b);
                            }
                            child.setAlpha(a.c.d);
                            child.setRotation(a.f.b);
                            child.setRotationX(a.f.c);
                            child.setRotationY(a.f.d);
                            child.setScaleX(a.f.e);
                            child.setScaleY(a.f.f);
                            final e f = a.f;
                            if (f.i != -1) {
                                final View viewById = ((View)child.getParent()).findViewById(a.f.i);
                                if (viewById != null) {
                                    final float n2 = (viewById.getTop() + viewById.getBottom()) / 2.0f;
                                    final float n3 = (viewById.getLeft() + viewById.getRight()) / 2.0f;
                                    if (child.getRight() - child.getLeft() > 0 && child.getBottom() - child.getTop() > 0) {
                                        final float n4 = (float)child.getLeft();
                                        final float n5 = (float)child.getTop();
                                        child.setPivotX(n3 - n4);
                                        child.setPivotY(n2 - n5);
                                    }
                                }
                            }
                            else {
                                if (!Float.isNaN(f.g)) {
                                    child.setPivotX(a.f.g);
                                }
                                if (!Float.isNaN(a.f.h)) {
                                    child.setPivotY(a.f.h);
                                }
                            }
                            child.setTranslationX(a.f.j);
                            child.setTranslationY(a.f.k);
                            child.setTranslationZ(a.f.l);
                            final e f2 = a.f;
                            if (f2.m) {
                                child.setElevation(f2.n);
                            }
                        }
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("WARNING NO CONSTRAINTS for view ");
                        sb2.append(id);
                        Log.v("ConstraintSet", sb2.toString());
                    }
                }
            }
        }
        final Iterator iterator = set.iterator();
        int j;
        while (true) {
            j = n;
            if (!iterator.hasNext()) {
                break;
            }
            final Integer key = (Integer)iterator.next();
            final a a3 = this.e.get(key);
            if (a3 == null) {
                continue;
            }
            if (a3.e.j0 == 1) {
                final androidx.constraintlayout.widget.a a4 = new androidx.constraintlayout.widget.a(((View)constraintLayout).getContext());
                a4.setId((int)key);
                final b e2 = a3.e;
                final int[] k2 = e2.k0;
                if (k2 != null) {
                    a4.setReferencedIds(k2);
                }
                else {
                    final String l2 = e2.l0;
                    if (l2 != null) {
                        e2.k0 = this.h(a4, l2);
                        a4.setReferencedIds(a3.e.k0);
                    }
                }
                a4.setType(a3.e.h0);
                a4.setMargin(a3.e.i0);
                final ConstraintLayout.b generateDefaultLayoutParams = constraintLayout.generateDefaultLayoutParams();
                a4.o();
                a3.b(generateDefaultLayoutParams);
                constraintLayout.addView((View)a4, (ViewGroup$LayoutParams)generateDefaultLayoutParams);
            }
            if (!a3.e.a) {
                continue;
            }
            final androidx.constraintlayout.widget.d d = new androidx.constraintlayout.widget.d(((View)constraintLayout).getContext());
            d.setId((int)key);
            final ConstraintLayout.b generateDefaultLayoutParams2 = constraintLayout.generateDefaultLayoutParams();
            a3.b(generateDefaultLayoutParams2);
            constraintLayout.addView((View)d, (ViewGroup$LayoutParams)generateDefaultLayoutParams2);
        }
        while (j < childCount) {
            final View child2 = constraintLayout.getChildAt(j);
            if (child2 instanceof androidx.constraintlayout.widget.b) {
                ((androidx.constraintlayout.widget.b)child2).f(constraintLayout);
            }
            ++j;
        }
    }
    
    public void e(final Context context, final int n) {
        this.f((ConstraintLayout)LayoutInflater.from(context).inflate(n, (ViewGroup)null));
    }
    
    public void f(final ConstraintLayout constraintLayout) {
        final int childCount = constraintLayout.getChildCount();
        this.e.clear();
        for (int i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            final ConstraintLayout.b b = (ConstraintLayout.b)child.getLayoutParams();
            final int id = child.getId();
            if (this.d && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.e.containsKey(id)) {
                this.e.put(id, new a());
            }
            final a a = this.e.get(id);
            if (a != null) {
                a.g = ConstraintAttribute.a(this.c, child);
                a.d(id, b);
                a.c.b = child.getVisibility();
                a.c.d = child.getAlpha();
                a.f.b = child.getRotation();
                a.f.c = child.getRotationX();
                a.f.d = child.getRotationY();
                a.f.e = child.getScaleX();
                a.f.f = child.getScaleY();
                final float pivotX = child.getPivotX();
                final float pivotY = child.getPivotY();
                if (pivotX != 0.0 || pivotY != 0.0) {
                    final e f = a.f;
                    f.g = pivotX;
                    f.h = pivotY;
                }
                a.f.j = child.getTranslationX();
                a.f.k = child.getTranslationY();
                a.f.l = child.getTranslationZ();
                final e f2 = a.f;
                if (f2.m) {
                    f2.n = child.getElevation();
                }
                if (child instanceof androidx.constraintlayout.widget.a) {
                    final androidx.constraintlayout.widget.a a2 = (androidx.constraintlayout.widget.a)child;
                    a.e.p0 = a2.getAllowsGoneWidget();
                    a.e.k0 = a2.getReferencedIds();
                    a.e.h0 = a2.getType();
                    a.e.i0 = a2.getMargin();
                }
            }
        }
    }
    
    public void g(final int n, final int b, final int c, final float d) {
        final b e = this.j(n).e;
        e.B = b;
        e.C = c;
        e.D = d;
    }
    
    public final int[] h(final View view, String original) {
        final String[] split = original.split(",");
        final Context context = view.getContext();
        original = (String)(Object)new int[split.length];
        int i;
        int newLength;
        for (i = 0, newLength = 0; i < split.length; ++i, ++newLength) {
            final String trim = split[i].trim();
            int int1;
            try {
                int1 = eb1.class.getField(trim).getInt(null);
            }
            catch (final Exception ex) {
                int1 = 0;
            }
            int identifier = int1;
            if (int1 == 0) {
                identifier = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            int intValue;
            if ((intValue = identifier) == 0) {
                intValue = identifier;
                if (view.isInEditMode()) {
                    intValue = identifier;
                    if (view.getParent() instanceof ConstraintLayout) {
                        final Object designInformation = ((ConstraintLayout)view.getParent()).getDesignInformation(0, trim);
                        intValue = identifier;
                        if (designInformation != null) {
                            intValue = identifier;
                            if (designInformation instanceof Integer) {
                                intValue = (int)designInformation;
                            }
                        }
                    }
                }
            }
            original[newLength] = intValue;
        }
        Object copy = original;
        if (newLength != split.length) {
            copy = Arrays.copyOf((int[])(Object)original, newLength);
        }
        return (int[])copy;
    }
    
    public final a i(final Context context, final AttributeSet set, final boolean b) {
        final a a = new a();
        int[] array;
        if (b) {
            array = vb1.k3;
        }
        else {
            array = vb1.t;
        }
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, array);
        this.q(context, a, obtainStyledAttributes, b);
        obtainStyledAttributes.recycle();
        return a;
    }
    
    public final a j(final int i) {
        if (!this.e.containsKey(i)) {
            this.e.put(i, new a());
        }
        return this.e.get(i);
    }
    
    public void k(final Context context, int i) {
        final XmlResourceParser xml = context.getResources().getXml(i);
        try {
            String name;
            a j;
            for (i = ((XmlPullParser)xml).getEventType(); i != 1; i = ((XmlPullParser)xml).next()) {
                if (i != 0) {
                    if (i == 2) {
                        name = ((XmlPullParser)xml).getName();
                        j = this.i(context, Xml.asAttributeSet((XmlPullParser)xml), false);
                        if (name.equalsIgnoreCase("Guideline")) {
                            j.e.a = true;
                        }
                        this.e.put(j.a, j);
                    }
                }
                else {
                    ((XmlPullParser)xml).getName();
                }
            }
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public void l(final Context context, final XmlPullParser xmlPullParser) {
        try {
            int i = xmlPullParser.getEventType();
            a value = null;
            Label_0827: {
                Label_0760: {
                    Label_0695: {
                        Label_0628: {
                            Label_0561: {
                                while (i != 1) {
                                    if (i != 0) {
                                        int n = -1;
                                        if (i != 2) {
                                            if (i == 3) {
                                                final String lowerCase = xmlPullParser.getName().toLowerCase(Locale.ROOT);
                                                switch (lowerCase.hashCode()) {
                                                    case 2146106725: {
                                                        if (lowerCase.equals("constraintset")) {
                                                            n = 0;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    case 426575017: {
                                                        if (lowerCase.equals("constraintoverride")) {
                                                            n = 2;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    case -190376483: {
                                                        if (lowerCase.equals("constraint")) {
                                                            n = 1;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    case -2075718416: {
                                                        if (lowerCase.equals("guideline")) {
                                                            n = 3;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                }
                                                if (n == 0) {
                                                    return;
                                                }
                                                if (n == 1 || n == 2 || n == 3) {
                                                    this.e.put(value.a, value);
                                                    value = null;
                                                }
                                            }
                                        }
                                        else {
                                            final String name = xmlPullParser.getName();
                                            switch (name.hashCode()) {
                                                case 1803088381: {
                                                    if (name.equals("Constraint")) {
                                                        n = 0;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case 1791837707: {
                                                    if (name.equals("CustomAttribute")) {
                                                        n = 8;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case 1331510167: {
                                                    if (name.equals("Barrier")) {
                                                        n = 3;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case 366511058: {
                                                    if (name.equals("CustomMethod")) {
                                                        n = 9;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -71750448: {
                                                    if (name.equals("Guideline")) {
                                                        n = 2;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1238332596: {
                                                    if (name.equals("Transform")) {
                                                        n = 5;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1269513683: {
                                                    if (name.equals("PropertySet")) {
                                                        n = 4;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1962203927: {
                                                    if (name.equals("ConstraintOverride")) {
                                                        n = 1;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1984451626: {
                                                    if (name.equals("Motion")) {
                                                        n = 7;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -2025855158: {
                                                    if (name.equals("Layout")) {
                                                        n = 6;
                                                        break;
                                                    }
                                                    break;
                                                }
                                            }
                                            switch (n) {
                                                case 8:
                                                case 9: {
                                                    if (value != null) {
                                                        ConstraintAttribute.b(context, xmlPullParser, value.g);
                                                        break;
                                                    }
                                                    break Label_0561;
                                                }
                                                case 7: {
                                                    if (value != null) {
                                                        value.d.b(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0628;
                                                }
                                                case 6: {
                                                    if (value != null) {
                                                        value.e.b(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0695;
                                                }
                                                case 5: {
                                                    if (value != null) {
                                                        value.f.b(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0760;
                                                }
                                                case 4: {
                                                    if (value != null) {
                                                        value.c.b(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0827;
                                                }
                                                case 3: {
                                                    value = this.i(context, Xml.asAttributeSet(xmlPullParser), false);
                                                    value.e.j0 = 1;
                                                    break;
                                                }
                                                case 2: {
                                                    value = this.i(context, Xml.asAttributeSet(xmlPullParser), false);
                                                    final b e = value.e;
                                                    e.a = true;
                                                    e.b = true;
                                                    break;
                                                }
                                                case 1: {
                                                    value = this.i(context, Xml.asAttributeSet(xmlPullParser), true);
                                                    break;
                                                }
                                                case 0: {
                                                    value = this.i(context, Xml.asAttributeSet(xmlPullParser), false);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        xmlPullParser.getName();
                                    }
                                    i = xmlPullParser.next();
                                }
                                return;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("XML parser error must be within a Constraint ");
                            sb.append(xmlPullParser.getLineNumber());
                            throw new RuntimeException(sb.toString());
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("XML parser error must be within a Constraint ");
                        sb2.append(xmlPullParser.getLineNumber());
                        throw new RuntimeException(sb2.toString());
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("XML parser error must be within a Constraint ");
                    sb3.append(xmlPullParser.getLineNumber());
                    throw new RuntimeException(sb3.toString());
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("XML parser error must be within a Constraint ");
                sb4.append(xmlPullParser.getLineNumber());
                throw new RuntimeException(sb4.toString());
            }
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("XML parser error must be within a Constraint ");
            sb5.append(xmlPullParser.getLineNumber());
            throw new RuntimeException(sb5.toString());
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public final void q(final Context context, final a a, final TypedArray typedArray, final boolean b) {
        if (b) {
            r(context, a, typedArray);
            return;
        }
        for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
            final int index = typedArray.getIndex(i);
            if (index != vb1.v && vb1.N != index && vb1.O != index) {
                a.d.a = true;
                a.e.b = true;
                a.c.a = true;
                a.f.a = true;
            }
            StringBuilder sb = null;
            String str = null;
            switch (androidx.constraintlayout.widget.c.g.get(index)) {
                default: {
                    sb = new StringBuilder();
                    str = "Unknown attribute 0x";
                    break;
                }
                case 97: {
                    final b e = a.e;
                    e.q0 = typedArray.getInt(index, e.q0);
                    continue;
                }
                case 96: {
                    n(a.e, typedArray, index, 1);
                    continue;
                }
                case 95: {
                    n(a.e, typedArray, index, 0);
                    continue;
                }
                case 94: {
                    final b e2 = a.e;
                    e2.U = typedArray.getDimensionPixelSize(index, e2.U);
                    continue;
                }
                case 93: {
                    final b e3 = a.e;
                    e3.N = typedArray.getDimensionPixelSize(index, e3.N);
                    continue;
                }
                case 92: {
                    final b e4 = a.e;
                    e4.t = m(typedArray, index, e4.t);
                    continue;
                }
                case 91: {
                    final b e5 = a.e;
                    e5.s = m(typedArray, index, e5.s);
                    continue;
                }
                case 87: {
                    sb = new StringBuilder();
                    str = "unused attribute 0x";
                    break;
                }
                case 86: {
                    final int type = typedArray.peekValue(index).type;
                    c c;
                    if (type == 1) {
                        a.d.n = typedArray.getResourceId(index, -1);
                        c = a.d;
                        if (c.n == -1) {
                            continue;
                        }
                    }
                    else {
                        if (type != 3) {
                            final c d = a.d;
                            d.m = typedArray.getInteger(index, d.n);
                            continue;
                        }
                        a.d.l = typedArray.getString(index);
                        if (a.d.l.indexOf("/") <= 0) {
                            a.d.m = -1;
                            continue;
                        }
                        a.d.n = typedArray.getResourceId(index, -1);
                        c = a.d;
                    }
                    c.m = -2;
                    continue;
                }
                case 85: {
                    final c d2 = a.d;
                    d2.j = typedArray.getFloat(index, d2.j);
                    continue;
                }
                case 84: {
                    final c d3 = a.d;
                    d3.k = typedArray.getInteger(index, d3.k);
                    continue;
                }
                case 83: {
                    final e f = a.f;
                    f.i = m(typedArray, index, f.i);
                    continue;
                }
                case 82: {
                    final c d4 = a.d;
                    d4.c = typedArray.getInteger(index, d4.c);
                    continue;
                }
                case 81: {
                    final b e6 = a.e;
                    e6.o0 = typedArray.getBoolean(index, e6.o0);
                    continue;
                }
                case 80: {
                    final b e7 = a.e;
                    e7.n0 = typedArray.getBoolean(index, e7.n0);
                    continue;
                }
                case 79: {
                    final c d5 = a.d;
                    d5.g = typedArray.getFloat(index, d5.g);
                    continue;
                }
                case 78: {
                    final d c2 = a.c;
                    c2.c = typedArray.getInt(index, c2.c);
                    continue;
                }
                case 77: {
                    a.e.m0 = typedArray.getString(index);
                    continue;
                }
                case 76: {
                    final c d6 = a.d;
                    d6.e = typedArray.getInt(index, d6.e);
                    continue;
                }
                case 75: {
                    final b e8 = a.e;
                    e8.p0 = typedArray.getBoolean(index, e8.p0);
                    continue;
                }
                case 74: {
                    a.e.l0 = typedArray.getString(index);
                    continue;
                }
                case 73: {
                    final b e9 = a.e;
                    e9.i0 = typedArray.getDimensionPixelSize(index, e9.i0);
                    continue;
                }
                case 72: {
                    final b e10 = a.e;
                    e10.h0 = typedArray.getInt(index, e10.h0);
                    continue;
                }
                case 71: {
                    Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                    continue;
                }
                case 70: {
                    a.e.g0 = typedArray.getFloat(index, 1.0f);
                    continue;
                }
                case 69: {
                    a.e.f0 = typedArray.getFloat(index, 1.0f);
                    continue;
                }
                case 68: {
                    final d c3 = a.c;
                    c3.e = typedArray.getFloat(index, c3.e);
                    continue;
                }
                case 67: {
                    final c d7 = a.d;
                    d7.i = typedArray.getFloat(index, d7.i);
                    continue;
                }
                case 66: {
                    a.d.f = typedArray.getInt(index, 0);
                    continue;
                }
                case 65: {
                    c c4;
                    String string;
                    if (typedArray.peekValue(index).type == 3) {
                        c4 = a.d;
                        string = typedArray.getString(index);
                    }
                    else {
                        c4 = a.d;
                        string = xv.c[typedArray.getInteger(index, 0)];
                    }
                    c4.d = string;
                    continue;
                }
                case 64: {
                    final c d8 = a.d;
                    d8.b = m(typedArray, index, d8.b);
                    continue;
                }
                case 63: {
                    final b e11 = a.e;
                    e11.D = typedArray.getFloat(index, e11.D);
                    continue;
                }
                case 62: {
                    final b e12 = a.e;
                    e12.C = typedArray.getDimensionPixelSize(index, e12.C);
                    continue;
                }
                case 61: {
                    final b e13 = a.e;
                    e13.B = m(typedArray, index, e13.B);
                    continue;
                }
                case 60: {
                    final e f2 = a.f;
                    f2.b = typedArray.getFloat(index, f2.b);
                    continue;
                }
                case 59: {
                    final b e14 = a.e;
                    e14.e0 = typedArray.getDimensionPixelSize(index, e14.e0);
                    continue;
                }
                case 58: {
                    final b e15 = a.e;
                    e15.d0 = typedArray.getDimensionPixelSize(index, e15.d0);
                    continue;
                }
                case 57: {
                    final b e16 = a.e;
                    e16.c0 = typedArray.getDimensionPixelSize(index, e16.c0);
                    continue;
                }
                case 56: {
                    final b e17 = a.e;
                    e17.b0 = typedArray.getDimensionPixelSize(index, e17.b0);
                    continue;
                }
                case 55: {
                    final b e18 = a.e;
                    e18.a0 = typedArray.getInt(index, e18.a0);
                    continue;
                }
                case 54: {
                    final b e19 = a.e;
                    e19.Z = typedArray.getInt(index, e19.Z);
                    continue;
                }
                case 53: {
                    final e f3 = a.f;
                    f3.l = typedArray.getDimension(index, f3.l);
                    continue;
                }
                case 52: {
                    final e f4 = a.f;
                    f4.k = typedArray.getDimension(index, f4.k);
                    continue;
                }
                case 51: {
                    final e f5 = a.f;
                    f5.j = typedArray.getDimension(index, f5.j);
                    continue;
                }
                case 50: {
                    final e f6 = a.f;
                    f6.h = typedArray.getDimension(index, f6.h);
                    continue;
                }
                case 49: {
                    final e f7 = a.f;
                    f7.g = typedArray.getDimension(index, f7.g);
                    continue;
                }
                case 48: {
                    final e f8 = a.f;
                    f8.f = typedArray.getFloat(index, f8.f);
                    continue;
                }
                case 47: {
                    final e f9 = a.f;
                    f9.e = typedArray.getFloat(index, f9.e);
                    continue;
                }
                case 46: {
                    final e f10 = a.f;
                    f10.d = typedArray.getFloat(index, f10.d);
                    continue;
                }
                case 45: {
                    final e f11 = a.f;
                    f11.c = typedArray.getFloat(index, f11.c);
                    continue;
                }
                case 44: {
                    final e f12 = a.f;
                    f12.m = true;
                    f12.n = typedArray.getDimension(index, f12.n);
                    continue;
                }
                case 43: {
                    final d c5 = a.c;
                    c5.d = typedArray.getFloat(index, c5.d);
                    continue;
                }
                case 42: {
                    final b e20 = a.e;
                    e20.Y = typedArray.getInt(index, e20.Y);
                    continue;
                }
                case 41: {
                    final b e21 = a.e;
                    e21.X = typedArray.getInt(index, e21.X);
                    continue;
                }
                case 40: {
                    final b e22 = a.e;
                    e22.V = typedArray.getFloat(index, e22.V);
                    continue;
                }
                case 39: {
                    final b e23 = a.e;
                    e23.W = typedArray.getFloat(index, e23.W);
                    continue;
                }
                case 38: {
                    a.a = typedArray.getResourceId(index, a.a);
                    continue;
                }
                case 37: {
                    final b e24 = a.e;
                    e24.z = typedArray.getFloat(index, e24.z);
                    continue;
                }
                case 36: {
                    final b e25 = a.e;
                    e25.n = m(typedArray, index, e25.n);
                    continue;
                }
                case 35: {
                    final b e26 = a.e;
                    e26.o = m(typedArray, index, e26.o);
                    continue;
                }
                case 34: {
                    final b e27 = a.e;
                    e27.J = typedArray.getDimensionPixelSize(index, e27.J);
                    continue;
                }
                case 33: {
                    final b e28 = a.e;
                    e28.v = m(typedArray, index, e28.v);
                    continue;
                }
                case 32: {
                    final b e29 = a.e;
                    e29.u = m(typedArray, index, e29.u);
                    continue;
                }
                case 31: {
                    final b e30 = a.e;
                    e30.M = typedArray.getDimensionPixelSize(index, e30.M);
                    continue;
                }
                case 30: {
                    final b e31 = a.e;
                    e31.m = m(typedArray, index, e31.m);
                    continue;
                }
                case 29: {
                    final b e32 = a.e;
                    e32.l = m(typedArray, index, e32.l);
                    continue;
                }
                case 28: {
                    final b e33 = a.e;
                    e33.I = typedArray.getDimensionPixelSize(index, e33.I);
                    continue;
                }
                case 27: {
                    final b e34 = a.e;
                    e34.G = typedArray.getInt(index, e34.G);
                    continue;
                }
                case 26: {
                    final b e35 = a.e;
                    e35.k = m(typedArray, index, e35.k);
                    continue;
                }
                case 25: {
                    final b e36 = a.e;
                    e36.j = m(typedArray, index, e36.j);
                    continue;
                }
                case 24: {
                    final b e37 = a.e;
                    e37.H = typedArray.getDimensionPixelSize(index, e37.H);
                    continue;
                }
                case 23: {
                    final b e38 = a.e;
                    e38.d = typedArray.getLayoutDimension(index, e38.d);
                    continue;
                }
                case 22: {
                    final d c6 = a.c;
                    c6.b = typedArray.getInt(index, c6.b);
                    final d c7 = a.c;
                    c7.b = androidx.constraintlayout.widget.c.f[c7.b];
                    continue;
                }
                case 21: {
                    final b e39 = a.e;
                    e39.e = typedArray.getLayoutDimension(index, e39.e);
                    continue;
                }
                case 20: {
                    final b e40 = a.e;
                    e40.y = typedArray.getFloat(index, e40.y);
                    continue;
                }
                case 19: {
                    final b e41 = a.e;
                    e41.h = typedArray.getFloat(index, e41.h);
                    continue;
                }
                case 18: {
                    final b e42 = a.e;
                    e42.g = typedArray.getDimensionPixelOffset(index, e42.g);
                    continue;
                }
                case 17: {
                    final b e43 = a.e;
                    e43.f = typedArray.getDimensionPixelOffset(index, e43.f);
                    continue;
                }
                case 16: {
                    final b e44 = a.e;
                    e44.P = typedArray.getDimensionPixelSize(index, e44.P);
                    continue;
                }
                case 15: {
                    final b e45 = a.e;
                    e45.T = typedArray.getDimensionPixelSize(index, e45.T);
                    continue;
                }
                case 14: {
                    final b e46 = a.e;
                    e46.Q = typedArray.getDimensionPixelSize(index, e46.Q);
                    continue;
                }
                case 13: {
                    final b e47 = a.e;
                    e47.O = typedArray.getDimensionPixelSize(index, e47.O);
                    continue;
                }
                case 12: {
                    final b e48 = a.e;
                    e48.S = typedArray.getDimensionPixelSize(index, e48.S);
                    continue;
                }
                case 11: {
                    final b e49 = a.e;
                    e49.R = typedArray.getDimensionPixelSize(index, e49.R);
                    continue;
                }
                case 10: {
                    final b e50 = a.e;
                    e50.w = m(typedArray, index, e50.w);
                    continue;
                }
                case 9: {
                    final b e51 = a.e;
                    e51.x = m(typedArray, index, e51.x);
                    continue;
                }
                case 8: {
                    final b e52 = a.e;
                    e52.L = typedArray.getDimensionPixelSize(index, e52.L);
                    continue;
                }
                case 7: {
                    final b e53 = a.e;
                    e53.F = typedArray.getDimensionPixelOffset(index, e53.F);
                    continue;
                }
                case 6: {
                    final b e54 = a.e;
                    e54.E = typedArray.getDimensionPixelOffset(index, e54.E);
                    continue;
                }
                case 5: {
                    a.e.A = typedArray.getString(index);
                    continue;
                }
                case 4: {
                    final b e55 = a.e;
                    e55.p = m(typedArray, index, e55.p);
                    continue;
                }
                case 3: {
                    final b e56 = a.e;
                    e56.q = m(typedArray, index, e56.q);
                    continue;
                }
                case 2: {
                    final b e57 = a.e;
                    e57.K = typedArray.getDimensionPixelSize(index, e57.K);
                    continue;
                }
                case 1: {
                    final b e58 = a.e;
                    e58.r = m(typedArray, index, e58.r);
                    continue;
                }
            }
            sb.append(str);
            sb.append(Integer.toHexString(index));
            sb.append("   ");
            sb.append(androidx.constraintlayout.widget.c.g.get(index));
            Log.w("ConstraintSet", sb.toString());
        }
        final b e59 = a.e;
        if (e59.l0 != null) {
            e59.k0 = null;
        }
    }
    
    public static class a
    {
        public int a;
        public String b;
        public final d c;
        public final c d;
        public final b e;
        public final e f;
        public HashMap g;
        public a h;
        
        public a() {
            this.c = new d();
            this.d = new c();
            this.e = new b();
            this.f = new e();
            this.g = new HashMap();
        }
        
        public void b(final ConstraintLayout.b b) {
            final b e = this.e;
            b.e = e.j;
            b.f = e.k;
            b.g = e.l;
            b.h = e.m;
            b.i = e.n;
            b.j = e.o;
            b.k = e.p;
            b.l = e.q;
            b.m = e.r;
            b.n = e.s;
            b.o = e.t;
            b.s = e.u;
            b.t = e.v;
            b.u = e.w;
            b.v = e.x;
            b.leftMargin = e.H;
            b.rightMargin = e.I;
            b.topMargin = e.J;
            b.bottomMargin = e.K;
            b.A = e.T;
            b.B = e.S;
            b.x = e.P;
            b.z = e.R;
            b.G = e.y;
            b.H = e.z;
            b.p = e.B;
            b.q = e.C;
            b.r = e.D;
            b.I = e.A;
            b.X = e.E;
            b.Y = e.F;
            b.M = e.V;
            b.L = e.W;
            b.O = e.Y;
            b.N = e.X;
            b.a0 = e.n0;
            b.b0 = e.o0;
            b.P = e.Z;
            b.Q = e.a0;
            b.T = e.b0;
            b.U = e.c0;
            b.R = e.d0;
            b.S = e.e0;
            b.V = e.f0;
            b.W = e.g0;
            b.Z = e.G;
            b.c = e.h;
            b.a = e.f;
            b.b = e.g;
            b.width = e.d;
            b.height = e.e;
            final String m0 = e.m0;
            if (m0 != null) {
                b.c0 = m0;
            }
            b.d0 = e.q0;
            b.setMarginStart(e.M);
            b.setMarginEnd(this.e.L);
            b.a();
        }
        
        public a c() {
            final a a = new a();
            a.e.a(this.e);
            a.d.a(this.d);
            a.c.a(this.c);
            a.f.a(this.f);
            a.a = this.a;
            a.h = this.h;
            return a;
        }
        
        public final void d(final int a, final ConstraintLayout.b b) {
            this.a = a;
            final b e = this.e;
            e.j = b.e;
            e.k = b.f;
            e.l = b.g;
            e.m = b.h;
            e.n = b.i;
            e.o = b.j;
            e.p = b.k;
            e.q = b.l;
            e.r = b.m;
            e.s = b.n;
            e.t = b.o;
            e.u = b.s;
            e.v = b.t;
            e.w = b.u;
            e.x = b.v;
            e.y = b.G;
            e.z = b.H;
            e.A = b.I;
            e.B = b.p;
            e.C = b.q;
            e.D = b.r;
            e.E = b.X;
            e.F = b.Y;
            e.G = b.Z;
            e.h = b.c;
            e.f = b.a;
            e.g = b.b;
            e.d = b.width;
            e.e = b.height;
            e.H = b.leftMargin;
            e.I = b.rightMargin;
            e.J = b.topMargin;
            e.K = b.bottomMargin;
            e.N = b.D;
            e.V = b.M;
            e.W = b.L;
            e.Y = b.O;
            e.X = b.N;
            e.n0 = b.a0;
            e.o0 = b.b0;
            e.Z = b.P;
            e.a0 = b.Q;
            e.b0 = b.T;
            e.c0 = b.U;
            e.d0 = b.R;
            e.e0 = b.S;
            e.f0 = b.V;
            e.g0 = b.W;
            e.m0 = b.c0;
            e.P = b.x;
            e.R = b.z;
            e.O = b.w;
            e.Q = b.y;
            e.T = b.A;
            e.S = b.B;
            e.U = b.C;
            e.q0 = b.d0;
            e.L = b.getMarginEnd();
            this.e.M = b.getMarginStart();
        }
        
        public static class a
        {
            public int[] a;
            public int[] b;
            public int c;
            public int[] d;
            public float[] e;
            public int f;
            public int[] g;
            public String[] h;
            public int i;
            public int[] j;
            public boolean[] k;
            public int l;
            
            public a() {
                this.a = new int[10];
                this.b = new int[10];
                this.c = 0;
                this.d = new int[10];
                this.e = new float[10];
                this.f = 0;
                this.g = new int[5];
                this.h = new String[5];
                this.i = 0;
                this.j = new int[4];
                this.k = new boolean[4];
                this.l = 0;
            }
            
            public void a(final int n, final float n2) {
                final int f = this.f;
                final int[] d = this.d;
                if (f >= d.length) {
                    this.d = Arrays.copyOf(d, d.length * 2);
                    final float[] e = this.e;
                    this.e = Arrays.copyOf(e, e.length * 2);
                }
                final int[] d2 = this.d;
                final int f2 = this.f;
                d2[f2] = n;
                final float[] e2 = this.e;
                this.f = f2 + 1;
                e2[f2] = n2;
            }
            
            public void b(final int n, final int n2) {
                final int c = this.c;
                final int[] a = this.a;
                if (c >= a.length) {
                    this.a = Arrays.copyOf(a, a.length * 2);
                    final int[] b = this.b;
                    this.b = Arrays.copyOf(b, b.length * 2);
                }
                final int[] a2 = this.a;
                final int c2 = this.c;
                a2[c2] = n;
                final int[] b2 = this.b;
                this.c = c2 + 1;
                b2[c2] = n2;
            }
            
            public void c(final int n, final String s) {
                final int i = this.i;
                final int[] g = this.g;
                if (i >= g.length) {
                    this.g = Arrays.copyOf(g, g.length * 2);
                    final String[] h = this.h;
                    this.h = Arrays.copyOf(h, h.length * 2);
                }
                final int[] g2 = this.g;
                final int j = this.i;
                g2[j] = n;
                final String[] h2 = this.h;
                this.i = j + 1;
                h2[j] = s;
            }
            
            public void d(final int n, final boolean b) {
                final int l = this.l;
                final int[] j = this.j;
                if (l >= j.length) {
                    this.j = Arrays.copyOf(j, j.length * 2);
                    final boolean[] k = this.k;
                    this.k = Arrays.copyOf(k, k.length * 2);
                }
                final int[] i = this.j;
                final int m = this.l;
                i[m] = n;
                final boolean[] k2 = this.k;
                this.l = m + 1;
                k2[m] = b;
            }
        }
    }
    
    public static class b
    {
        public static SparseIntArray r0;
        public String A;
        public int B;
        public int C;
        public float D;
        public int E;
        public int F;
        public int G;
        public int H;
        public int I;
        public int J;
        public int K;
        public int L;
        public int M;
        public int N;
        public int O;
        public int P;
        public int Q;
        public int R;
        public int S;
        public int T;
        public int U;
        public float V;
        public float W;
        public int X;
        public int Y;
        public int Z;
        public boolean a;
        public int a0;
        public boolean b;
        public int b0;
        public boolean c;
        public int c0;
        public int d;
        public int d0;
        public int e;
        public int e0;
        public int f;
        public float f0;
        public int g;
        public float g0;
        public float h;
        public int h0;
        public boolean i;
        public int i0;
        public int j;
        public int j0;
        public int k;
        public int[] k0;
        public int l;
        public String l0;
        public int m;
        public String m0;
        public int n;
        public boolean n0;
        public int o;
        public boolean o0;
        public int p;
        public boolean p0;
        public int q;
        public int q0;
        public int r;
        public int s;
        public int t;
        public int u;
        public int v;
        public int w;
        public int x;
        public float y;
        public float z;
        
        static {
            (b.r0 = new SparseIntArray()).append(vb1.X5, 24);
            b.r0.append(vb1.Y5, 25);
            b.r0.append(vb1.a6, 28);
            b.r0.append(vb1.b6, 29);
            b.r0.append(vb1.g6, 35);
            b.r0.append(vb1.f6, 34);
            b.r0.append(vb1.H5, 4);
            b.r0.append(vb1.G5, 3);
            b.r0.append(vb1.E5, 1);
            b.r0.append(vb1.m6, 6);
            b.r0.append(vb1.n6, 7);
            b.r0.append(vb1.O5, 17);
            b.r0.append(vb1.P5, 18);
            b.r0.append(vb1.Q5, 19);
            b.r0.append(vb1.A5, 90);
            b.r0.append(vb1.m5, 26);
            b.r0.append(vb1.c6, 31);
            b.r0.append(vb1.d6, 32);
            b.r0.append(vb1.N5, 10);
            b.r0.append(vb1.M5, 9);
            b.r0.append(vb1.q6, 13);
            b.r0.append(vb1.t6, 16);
            b.r0.append(vb1.r6, 14);
            b.r0.append(vb1.o6, 11);
            b.r0.append(vb1.s6, 15);
            b.r0.append(vb1.p6, 12);
            b.r0.append(vb1.j6, 38);
            b.r0.append(vb1.V5, 37);
            b.r0.append(vb1.U5, 39);
            b.r0.append(vb1.i6, 40);
            b.r0.append(vb1.T5, 20);
            b.r0.append(vb1.h6, 36);
            b.r0.append(vb1.L5, 5);
            b.r0.append(vb1.W5, 91);
            b.r0.append(vb1.e6, 91);
            b.r0.append(vb1.Z5, 91);
            b.r0.append(vb1.F5, 91);
            b.r0.append(vb1.D5, 91);
            b.r0.append(vb1.p5, 23);
            b.r0.append(vb1.r5, 27);
            b.r0.append(vb1.t5, 30);
            b.r0.append(vb1.u5, 8);
            b.r0.append(vb1.q5, 33);
            b.r0.append(vb1.s5, 2);
            b.r0.append(vb1.n5, 22);
            b.r0.append(vb1.o5, 21);
            b.r0.append(vb1.k6, 41);
            b.r0.append(vb1.R5, 42);
            b.r0.append(vb1.C5, 41);
            b.r0.append(vb1.B5, 42);
            b.r0.append(vb1.u6, 76);
            b.r0.append(vb1.I5, 61);
            b.r0.append(vb1.K5, 62);
            b.r0.append(vb1.J5, 63);
            b.r0.append(vb1.l6, 69);
            b.r0.append(vb1.S5, 70);
            b.r0.append(vb1.y5, 71);
            b.r0.append(vb1.w5, 72);
            b.r0.append(vb1.x5, 73);
            b.r0.append(vb1.z5, 74);
            b.r0.append(vb1.v5, 75);
        }
        
        public b() {
            this.a = false;
            this.b = false;
            this.c = false;
            this.f = -1;
            this.g = -1;
            this.h = -1.0f;
            this.i = true;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = -1;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = -1;
            this.w = -1;
            this.x = -1;
            this.y = 0.5f;
            this.z = 0.5f;
            this.A = null;
            this.B = -1;
            this.C = 0;
            this.D = 0.0f;
            this.E = -1;
            this.F = -1;
            this.G = -1;
            this.H = 0;
            this.I = 0;
            this.J = 0;
            this.K = 0;
            this.L = 0;
            this.M = 0;
            this.N = 0;
            this.O = Integer.MIN_VALUE;
            this.P = Integer.MIN_VALUE;
            this.Q = Integer.MIN_VALUE;
            this.R = Integer.MIN_VALUE;
            this.S = Integer.MIN_VALUE;
            this.T = Integer.MIN_VALUE;
            this.U = Integer.MIN_VALUE;
            this.V = -1.0f;
            this.W = -1.0f;
            this.X = 0;
            this.Y = 0;
            this.Z = 0;
            this.a0 = 0;
            this.b0 = 0;
            this.c0 = 0;
            this.d0 = 0;
            this.e0 = 0;
            this.f0 = 1.0f;
            this.g0 = 1.0f;
            this.h0 = -1;
            this.i0 = 0;
            this.j0 = -1;
            this.n0 = false;
            this.o0 = false;
            this.p0 = true;
            this.q0 = 0;
        }
        
        public void a(final b b) {
            this.a = b.a;
            this.d = b.d;
            this.b = b.b;
            this.e = b.e;
            this.f = b.f;
            this.g = b.g;
            this.h = b.h;
            this.i = b.i;
            this.j = b.j;
            this.k = b.k;
            this.l = b.l;
            this.m = b.m;
            this.n = b.n;
            this.o = b.o;
            this.p = b.p;
            this.q = b.q;
            this.r = b.r;
            this.s = b.s;
            this.t = b.t;
            this.u = b.u;
            this.v = b.v;
            this.w = b.w;
            this.x = b.x;
            this.y = b.y;
            this.z = b.z;
            this.A = b.A;
            this.B = b.B;
            this.C = b.C;
            this.D = b.D;
            this.E = b.E;
            this.F = b.F;
            this.G = b.G;
            this.H = b.H;
            this.I = b.I;
            this.J = b.J;
            this.K = b.K;
            this.L = b.L;
            this.M = b.M;
            this.N = b.N;
            this.O = b.O;
            this.P = b.P;
            this.Q = b.Q;
            this.R = b.R;
            this.S = b.S;
            this.T = b.T;
            this.U = b.U;
            this.V = b.V;
            this.W = b.W;
            this.X = b.X;
            this.Y = b.Y;
            this.Z = b.Z;
            this.a0 = b.a0;
            this.b0 = b.b0;
            this.c0 = b.c0;
            this.d0 = b.d0;
            this.e0 = b.e0;
            this.f0 = b.f0;
            this.g0 = b.g0;
            this.h0 = b.h0;
            this.i0 = b.i0;
            this.j0 = b.j0;
            this.m0 = b.m0;
            final int[] k0 = b.k0;
            if (k0 != null && b.l0 == null) {
                this.k0 = Arrays.copyOf(k0, k0.length);
            }
            else {
                this.k0 = null;
            }
            this.l0 = b.l0;
            this.n0 = b.n0;
            this.o0 = b.o0;
            this.p0 = b.p0;
            this.q0 = b.q0;
        }
        
        public void b(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, vb1.l5);
            this.b = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                final int value = androidx.constraintlayout.widget.c.b.r0.get(index);
                switch (value) {
                    default: {
                        switch (value) {
                            default: {
                                StringBuilder sb = null;
                                String str = null;
                                switch (value) {
                                    default: {
                                        sb = new StringBuilder();
                                        str = "Unknown attribute 0x";
                                        break;
                                    }
                                    case 91: {
                                        sb = new StringBuilder();
                                        str = "unused attribute 0x";
                                        break;
                                    }
                                    case 90: {
                                        this.i = obtainStyledAttributes.getBoolean(index, this.i);
                                        continue;
                                    }
                                    case 89: {
                                        this.m0 = obtainStyledAttributes.getString(index);
                                        continue;
                                    }
                                    case 88: {
                                        this.o0 = obtainStyledAttributes.getBoolean(index, this.o0);
                                        continue;
                                    }
                                    case 87: {
                                        this.n0 = obtainStyledAttributes.getBoolean(index, this.n0);
                                        continue;
                                    }
                                    case 86: {
                                        this.d0 = obtainStyledAttributes.getDimensionPixelSize(index, this.d0);
                                        continue;
                                    }
                                    case 85: {
                                        this.e0 = obtainStyledAttributes.getDimensionPixelSize(index, this.e0);
                                        continue;
                                    }
                                    case 84: {
                                        this.b0 = obtainStyledAttributes.getDimensionPixelSize(index, this.b0);
                                        continue;
                                    }
                                    case 83: {
                                        this.c0 = obtainStyledAttributes.getDimensionPixelSize(index, this.c0);
                                        continue;
                                    }
                                    case 82: {
                                        this.a0 = obtainStyledAttributes.getInt(index, this.a0);
                                        continue;
                                    }
                                    case 81: {
                                        this.Z = obtainStyledAttributes.getInt(index, this.Z);
                                        continue;
                                    }
                                    case 80: {
                                        this.N = obtainStyledAttributes.getDimensionPixelSize(index, this.N);
                                        continue;
                                    }
                                    case 79: {
                                        this.U = obtainStyledAttributes.getDimensionPixelSize(index, this.U);
                                        continue;
                                    }
                                    case 78: {
                                        this.t = m(obtainStyledAttributes, index, this.t);
                                        continue;
                                    }
                                    case 77: {
                                        this.s = m(obtainStyledAttributes, index, this.s);
                                        continue;
                                    }
                                    case 76: {
                                        this.q0 = obtainStyledAttributes.getInt(index, this.q0);
                                        continue;
                                    }
                                    case 75: {
                                        this.p0 = obtainStyledAttributes.getBoolean(index, this.p0);
                                        continue;
                                    }
                                    case 74: {
                                        this.l0 = obtainStyledAttributes.getString(index);
                                        continue;
                                    }
                                    case 73: {
                                        this.i0 = obtainStyledAttributes.getDimensionPixelSize(index, this.i0);
                                        continue;
                                    }
                                    case 72: {
                                        this.h0 = obtainStyledAttributes.getInt(index, this.h0);
                                        continue;
                                    }
                                    case 71: {
                                        Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                                        continue;
                                    }
                                    case 70: {
                                        this.g0 = obtainStyledAttributes.getFloat(index, 1.0f);
                                        continue;
                                    }
                                    case 69: {
                                        this.f0 = obtainStyledAttributes.getFloat(index, 1.0f);
                                        continue;
                                    }
                                }
                                sb.append(str);
                                sb.append(Integer.toHexString(index));
                                sb.append("   ");
                                sb.append(androidx.constraintlayout.widget.c.b.r0.get(index));
                                Log.w("ConstraintSet", sb.toString());
                                continue;
                            }
                            case 63: {
                                this.D = obtainStyledAttributes.getFloat(index, this.D);
                                continue;
                            }
                            case 62: {
                                this.C = obtainStyledAttributes.getDimensionPixelSize(index, this.C);
                                continue;
                            }
                            case 61: {
                                this.B = m(obtainStyledAttributes, index, this.B);
                                continue;
                            }
                        }
                        break;
                    }
                    case 42: {
                        androidx.constraintlayout.widget.c.n(this, obtainStyledAttributes, index, 1);
                        break;
                    }
                    case 41: {
                        androidx.constraintlayout.widget.c.n(this, obtainStyledAttributes, index, 0);
                        break;
                    }
                    case 40: {
                        this.Y = obtainStyledAttributes.getInt(index, this.Y);
                        break;
                    }
                    case 39: {
                        this.X = obtainStyledAttributes.getInt(index, this.X);
                        break;
                    }
                    case 38: {
                        this.V = obtainStyledAttributes.getFloat(index, this.V);
                        break;
                    }
                    case 37: {
                        this.W = obtainStyledAttributes.getFloat(index, this.W);
                        break;
                    }
                    case 36: {
                        this.z = obtainStyledAttributes.getFloat(index, this.z);
                        break;
                    }
                    case 35: {
                        this.n = m(obtainStyledAttributes, index, this.n);
                        break;
                    }
                    case 34: {
                        this.o = m(obtainStyledAttributes, index, this.o);
                        break;
                    }
                    case 33: {
                        this.J = obtainStyledAttributes.getDimensionPixelSize(index, this.J);
                        break;
                    }
                    case 32: {
                        this.v = m(obtainStyledAttributes, index, this.v);
                        break;
                    }
                    case 31: {
                        this.u = m(obtainStyledAttributes, index, this.u);
                        break;
                    }
                    case 30: {
                        this.M = obtainStyledAttributes.getDimensionPixelSize(index, this.M);
                        break;
                    }
                    case 29: {
                        this.m = m(obtainStyledAttributes, index, this.m);
                        break;
                    }
                    case 28: {
                        this.l = m(obtainStyledAttributes, index, this.l);
                        break;
                    }
                    case 27: {
                        this.I = obtainStyledAttributes.getDimensionPixelSize(index, this.I);
                        break;
                    }
                    case 26: {
                        this.G = obtainStyledAttributes.getInt(index, this.G);
                        break;
                    }
                    case 25: {
                        this.k = m(obtainStyledAttributes, index, this.k);
                        break;
                    }
                    case 24: {
                        this.j = m(obtainStyledAttributes, index, this.j);
                        break;
                    }
                    case 23: {
                        this.H = obtainStyledAttributes.getDimensionPixelSize(index, this.H);
                        break;
                    }
                    case 22: {
                        this.d = obtainStyledAttributes.getLayoutDimension(index, this.d);
                        break;
                    }
                    case 21: {
                        this.e = obtainStyledAttributes.getLayoutDimension(index, this.e);
                        break;
                    }
                    case 20: {
                        this.y = obtainStyledAttributes.getFloat(index, this.y);
                        break;
                    }
                    case 19: {
                        this.h = obtainStyledAttributes.getFloat(index, this.h);
                        break;
                    }
                    case 18: {
                        this.g = obtainStyledAttributes.getDimensionPixelOffset(index, this.g);
                        break;
                    }
                    case 17: {
                        this.f = obtainStyledAttributes.getDimensionPixelOffset(index, this.f);
                        break;
                    }
                    case 16: {
                        this.P = obtainStyledAttributes.getDimensionPixelSize(index, this.P);
                        break;
                    }
                    case 15: {
                        this.T = obtainStyledAttributes.getDimensionPixelSize(index, this.T);
                        break;
                    }
                    case 14: {
                        this.Q = obtainStyledAttributes.getDimensionPixelSize(index, this.Q);
                        break;
                    }
                    case 13: {
                        this.O = obtainStyledAttributes.getDimensionPixelSize(index, this.O);
                        break;
                    }
                    case 12: {
                        this.S = obtainStyledAttributes.getDimensionPixelSize(index, this.S);
                        break;
                    }
                    case 11: {
                        this.R = obtainStyledAttributes.getDimensionPixelSize(index, this.R);
                        break;
                    }
                    case 10: {
                        this.w = m(obtainStyledAttributes, index, this.w);
                        break;
                    }
                    case 9: {
                        this.x = m(obtainStyledAttributes, index, this.x);
                        break;
                    }
                    case 8: {
                        this.L = obtainStyledAttributes.getDimensionPixelSize(index, this.L);
                        break;
                    }
                    case 7: {
                        this.F = obtainStyledAttributes.getDimensionPixelOffset(index, this.F);
                        break;
                    }
                    case 6: {
                        this.E = obtainStyledAttributes.getDimensionPixelOffset(index, this.E);
                        break;
                    }
                    case 5: {
                        this.A = obtainStyledAttributes.getString(index);
                        break;
                    }
                    case 4: {
                        this.p = m(obtainStyledAttributes, index, this.p);
                        break;
                    }
                    case 3: {
                        this.q = m(obtainStyledAttributes, index, this.q);
                        break;
                    }
                    case 2: {
                        this.K = obtainStyledAttributes.getDimensionPixelSize(index, this.K);
                        break;
                    }
                    case 1: {
                        this.r = m(obtainStyledAttributes, index, this.r);
                        break;
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static class c
    {
        public static SparseIntArray o;
        public boolean a;
        public int b;
        public int c;
        public String d;
        public int e;
        public int f;
        public float g;
        public int h;
        public float i;
        public float j;
        public int k;
        public String l;
        public int m;
        public int n;
        
        static {
            (c.o = new SparseIntArray()).append(vb1.G6, 1);
            c.o.append(vb1.I6, 2);
            c.o.append(vb1.M6, 3);
            c.o.append(vb1.F6, 4);
            c.o.append(vb1.E6, 5);
            c.o.append(vb1.D6, 6);
            c.o.append(vb1.H6, 7);
            c.o.append(vb1.L6, 8);
            c.o.append(vb1.K6, 9);
            c.o.append(vb1.J6, 10);
        }
        
        public c() {
            this.a = false;
            this.b = -1;
            this.c = 0;
            this.d = null;
            this.e = -1;
            this.f = 0;
            this.g = Float.NaN;
            this.h = -1;
            this.i = Float.NaN;
            this.j = Float.NaN;
            this.k = -1;
            this.l = null;
            this.m = -3;
            this.n = -1;
        }
        
        public void a(final c c) {
            this.a = c.a;
            this.b = c.b;
            this.d = c.d;
            this.e = c.e;
            this.f = c.f;
            this.i = c.i;
            this.g = c.g;
            this.h = c.h;
        }
        
        public void b(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, vb1.C6);
            this.a = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                switch (androidx.constraintlayout.widget.c.c.o.get(index)) {
                    case 10: {
                        final int type = obtainStyledAttributes.peekValue(index).type;
                        if (type == 1) {
                            if ((this.n = obtainStyledAttributes.getResourceId(index, -1)) == -1) {
                                break;
                            }
                        }
                        else {
                            if (type != 3) {
                                this.m = obtainStyledAttributes.getInteger(index, this.n);
                                break;
                            }
                            final String string = obtainStyledAttributes.getString(index);
                            this.l = string;
                            if (string.indexOf("/") <= 0) {
                                this.m = -1;
                                break;
                            }
                            this.n = obtainStyledAttributes.getResourceId(index, -1);
                        }
                        this.m = -2;
                        break;
                    }
                    case 9: {
                        this.j = obtainStyledAttributes.getFloat(index, this.j);
                        break;
                    }
                    case 8: {
                        this.k = obtainStyledAttributes.getInteger(index, this.k);
                        break;
                    }
                    case 7: {
                        this.g = obtainStyledAttributes.getFloat(index, this.g);
                        break;
                    }
                    case 6: {
                        this.c = obtainStyledAttributes.getInteger(index, this.c);
                        break;
                    }
                    case 5: {
                        this.b = m(obtainStyledAttributes, index, this.b);
                        break;
                    }
                    case 4: {
                        this.f = obtainStyledAttributes.getInt(index, 0);
                        break;
                    }
                    case 3: {
                        String string2;
                        if (obtainStyledAttributes.peekValue(index).type == 3) {
                            string2 = obtainStyledAttributes.getString(index);
                        }
                        else {
                            string2 = xv.c[obtainStyledAttributes.getInteger(index, 0)];
                        }
                        this.d = string2;
                        break;
                    }
                    case 2: {
                        this.e = obtainStyledAttributes.getInt(index, this.e);
                        break;
                    }
                    case 1: {
                        this.i = obtainStyledAttributes.getFloat(index, this.i);
                        break;
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static class d
    {
        public boolean a;
        public int b;
        public int c;
        public float d;
        public float e;
        
        public d() {
            this.a = false;
            this.b = 0;
            this.c = 0;
            this.d = 1.0f;
            this.e = Float.NaN;
        }
        
        public void a(final d d) {
            this.a = d.a;
            this.b = d.b;
            this.d = d.d;
            this.e = d.e;
            this.c = d.c;
        }
        
        public void b(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, vb1.X6);
            this.a = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == vb1.Z6) {
                    this.d = obtainStyledAttributes.getFloat(index, this.d);
                }
                else if (index == vb1.Y6) {
                    this.b = obtainStyledAttributes.getInt(index, this.b);
                    this.b = androidx.constraintlayout.widget.c.b()[this.b];
                }
                else if (index == vb1.b7) {
                    this.c = obtainStyledAttributes.getInt(index, this.c);
                }
                else if (index == vb1.a7) {
                    this.e = obtainStyledAttributes.getFloat(index, this.e);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static class e
    {
        public static SparseIntArray o;
        public boolean a;
        public float b;
        public float c;
        public float d;
        public float e;
        public float f;
        public float g;
        public float h;
        public int i;
        public float j;
        public float k;
        public float l;
        public boolean m;
        public float n;
        
        static {
            (e.o = new SparseIntArray()).append(vb1.w7, 1);
            e.o.append(vb1.x7, 2);
            e.o.append(vb1.y7, 3);
            e.o.append(vb1.u7, 4);
            e.o.append(vb1.v7, 5);
            e.o.append(vb1.q7, 6);
            e.o.append(vb1.r7, 7);
            e.o.append(vb1.s7, 8);
            e.o.append(vb1.t7, 9);
            e.o.append(vb1.z7, 10);
            e.o.append(vb1.A7, 11);
            e.o.append(vb1.B7, 12);
        }
        
        public e() {
            this.a = false;
            this.b = 0.0f;
            this.c = 0.0f;
            this.d = 0.0f;
            this.e = 1.0f;
            this.f = 1.0f;
            this.g = Float.NaN;
            this.h = Float.NaN;
            this.i = -1;
            this.j = 0.0f;
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = false;
            this.n = 0.0f;
        }
        
        public void a(final e e) {
            this.a = e.a;
            this.b = e.b;
            this.c = e.c;
            this.d = e.d;
            this.e = e.e;
            this.f = e.f;
            this.g = e.g;
            this.h = e.h;
            this.i = e.i;
            this.j = e.j;
            this.k = e.k;
            this.l = e.l;
            this.m = e.m;
            this.n = e.n;
        }
        
        public void b(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, vb1.p7);
            this.a = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                switch (androidx.constraintlayout.widget.c.e.o.get(index)) {
                    case 12: {
                        this.i = m(obtainStyledAttributes, index, this.i);
                        break;
                    }
                    case 11: {
                        this.m = true;
                        this.n = obtainStyledAttributes.getDimension(index, this.n);
                        break;
                    }
                    case 10: {
                        this.l = obtainStyledAttributes.getDimension(index, this.l);
                        break;
                    }
                    case 9: {
                        this.k = obtainStyledAttributes.getDimension(index, this.k);
                        break;
                    }
                    case 8: {
                        this.j = obtainStyledAttributes.getDimension(index, this.j);
                        break;
                    }
                    case 7: {
                        this.h = obtainStyledAttributes.getDimension(index, this.h);
                        break;
                    }
                    case 6: {
                        this.g = obtainStyledAttributes.getDimension(index, this.g);
                        break;
                    }
                    case 5: {
                        this.f = obtainStyledAttributes.getFloat(index, this.f);
                        break;
                    }
                    case 4: {
                        this.e = obtainStyledAttributes.getFloat(index, this.e);
                        break;
                    }
                    case 3: {
                        this.d = obtainStyledAttributes.getFloat(index, this.d);
                        break;
                    }
                    case 2: {
                        this.c = obtainStyledAttributes.getFloat(index, this.c);
                        break;
                    }
                    case 1: {
                        this.b = obtainStyledAttributes.getFloat(index, this.b);
                        break;
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
}

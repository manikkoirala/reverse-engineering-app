// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;

public class a extends b
{
    public int j;
    public int k;
    public androidx.constraintlayout.core.widgets.a l;
    
    public a(final Context context) {
        super(context);
        super.setVisibility(8);
    }
    
    public boolean getAllowsGoneWidget() {
        return this.l.u1();
    }
    
    public int getMargin() {
        return this.l.w1();
    }
    
    public int getType() {
        return this.j;
    }
    
    @Override
    public void i(final AttributeSet set) {
        super.i(set);
        this.l = new androidx.constraintlayout.core.widgets.a();
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, vb1.n1);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == vb1.D1) {
                    this.setType(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.C1) {
                    this.l.z1(obtainStyledAttributes.getBoolean(index, true));
                }
                else if (index == vb1.E1) {
                    this.l.B1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
            }
            obtainStyledAttributes.recycle();
        }
        super.d = this.l;
        this.o();
    }
    
    @Override
    public void j(final ConstraintWidget constraintWidget, final boolean b) {
        this.p(constraintWidget, this.j, b);
    }
    
    public final void p(final ConstraintWidget constraintWidget, int k, final boolean b) {
        this.k = k;
        Label_0063: {
            Label_0046: {
                if (b) {
                    k = this.j;
                    if (k != 5) {
                        if (k == 6) {
                            break Label_0046;
                        }
                        break Label_0063;
                    }
                }
                else {
                    k = this.j;
                    if (k == 5) {
                        break Label_0046;
                    }
                    if (k != 6) {
                        break Label_0063;
                    }
                }
                this.k = 1;
                break Label_0063;
            }
            this.k = 0;
        }
        if (constraintWidget instanceof androidx.constraintlayout.core.widgets.a) {
            ((androidx.constraintlayout.core.widgets.a)constraintWidget).A1(this.k);
        }
    }
    
    public void setAllowsGoneWidget(final boolean b) {
        this.l.z1(b);
    }
    
    public void setDpMargin(int n) {
        n = (int)(n * this.getResources().getDisplayMetrics().density + 0.5f);
        this.l.B1(n);
    }
    
    public void setMargin(final int n) {
        this.l.B1(n);
    }
    
    public void setType(final int j) {
        this.j = j;
    }
}

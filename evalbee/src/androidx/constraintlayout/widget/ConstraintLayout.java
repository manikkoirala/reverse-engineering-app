// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import androidx.constraintlayout.core.widgets.h;
import androidx.constraintlayout.core.widgets.g;
import android.util.SparseIntArray;
import android.content.res.TypedArray;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.View$MeasureSpec;
import java.util.Iterator;
import android.util.Log;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.view.ViewGroup$LayoutParams;
import android.content.res.Resources$NotFoundException;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.f;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import java.util.HashMap;
import java.util.ArrayList;
import android.view.View;
import android.util.SparseArray;
import android.view.ViewGroup;

public class ConstraintLayout extends ViewGroup
{
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_DRAW_CONSTRAINTS = false;
    public static final int DESIGN_INFO_ID = 0;
    private static final boolean MEASURE = false;
    private static final boolean OPTIMIZE_HEIGHT_CHANGE = false;
    private static final String TAG = "ConstraintLayout";
    private static final boolean USE_CONSTRAINTS_HELPER = true;
    public static final String VERSION = "ConstraintLayout-2.1.4";
    private static rn1 sSharedValues;
    SparseArray<View> mChildrenByIds;
    private ArrayList<androidx.constraintlayout.widget.b> mConstraintHelpers;
    protected qk mConstraintLayoutSpec;
    private androidx.constraintlayout.widget.c mConstraintSet;
    private int mConstraintSetId;
    private al mConstraintsChangedListener;
    private HashMap<String, Integer> mDesignIds;
    protected boolean mDirtyHierarchy;
    private int mLastMeasureHeight;
    int mLastMeasureHeightMode;
    int mLastMeasureHeightSize;
    private int mLastMeasureWidth;
    int mLastMeasureWidthMode;
    int mLastMeasureWidthSize;
    protected d mLayoutWidget;
    private int mMaxHeight;
    private int mMaxWidth;
    c mMeasurer;
    private hw0 mMetrics;
    private int mMinHeight;
    private int mMinWidth;
    private int mOnMeasureHeightMeasureSpec;
    private int mOnMeasureWidthMeasureSpec;
    private int mOptimizationLevel;
    private SparseArray<ConstraintWidget> mTempMapIdToWidget;
    
    public ConstraintLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<androidx.constraintlayout.widget.b>(4);
        this.mLayoutWidget = new d();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 257;
        this.mConstraintSet = null;
        this.mConstraintLayoutSpec = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.mTempMapIdToWidget = (SparseArray<ConstraintWidget>)new SparseArray();
        this.mMeasurer = new c(this);
        this.mOnMeasureWidthMeasureSpec = 0;
        this.b(set, this.mOnMeasureHeightMeasureSpec = 0, 0);
    }
    
    public ConstraintLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<androidx.constraintlayout.widget.b>(4);
        this.mLayoutWidget = new d();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 257;
        this.mConstraintSet = null;
        this.mConstraintLayoutSpec = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.mTempMapIdToWidget = (SparseArray<ConstraintWidget>)new SparseArray();
        this.mMeasurer = new c(this);
        this.mOnMeasureWidthMeasureSpec = 0;
        this.b(set, n, this.mOnMeasureHeightMeasureSpec = 0);
    }
    
    public static /* synthetic */ int access$000(final ConstraintLayout constraintLayout) {
        return constraintLayout.mOptimizationLevel;
    }
    
    public static /* synthetic */ ArrayList access$100(final ConstraintLayout constraintLayout) {
        return constraintLayout.mConstraintHelpers;
    }
    
    private int getPaddingWidth() {
        int n = Math.max(0, ((View)this).getPaddingLeft()) + Math.max(0, ((View)this).getPaddingRight());
        final int n2 = Math.max(0, ((View)this).getPaddingStart()) + Math.max(0, ((View)this).getPaddingEnd());
        if (n2 > 0) {
            n = n2;
        }
        return n;
    }
    
    public static rn1 getSharedValues() {
        if (ConstraintLayout.sSharedValues == null) {
            ConstraintLayout.sSharedValues = new rn1();
        }
        return ConstraintLayout.sSharedValues;
    }
    
    public final ConstraintWidget a(final int n) {
        if (n == 0) {
            return this.mLayoutWidget;
        }
        View view;
        if ((view = (View)this.mChildrenByIds.get(n)) == null) {
            final View viewById = ((View)this).findViewById(n);
            if ((view = viewById) != null && (view = viewById) != this) {
                view = viewById;
                if (viewById.getParent() == this) {
                    this.onViewAdded(viewById);
                    view = viewById;
                }
            }
        }
        if (view == this) {
            return this.mLayoutWidget;
        }
        ConstraintWidget v0;
        if (view == null) {
            v0 = null;
        }
        else {
            v0 = ((b)view.getLayoutParams()).v0;
        }
        return v0;
    }
    
    public void applyConstraintsFromLayoutParams(final boolean b, final View view, final ConstraintWidget constraintWidget, final b b2, final SparseArray<ConstraintWidget> sparseArray) {
        b2.a();
        b2.w0 = false;
        constraintWidget.j1(view.getVisibility());
        if (b2.j0) {
            constraintWidget.T0(true);
            constraintWidget.j1(8);
        }
        constraintWidget.B0(view);
        if (view instanceof androidx.constraintlayout.widget.b) {
            ((androidx.constraintlayout.widget.b)view).j(constraintWidget, this.mLayoutWidget.Q1());
        }
        if (b2.h0) {
            final f f = (f)constraintWidget;
            final int s0 = b2.s0;
            final int t0 = b2.t0;
            final float u0 = b2.u0;
            if (u0 != -1.0f) {
                f.z1(u0);
            }
            else if (s0 != -1) {
                f.x1(s0);
            }
            else if (t0 != -1) {
                f.y1(t0);
            }
        }
        else {
            final int l0 = b2.l0;
            final int m0 = b2.m0;
            final int n0 = b2.n0;
            final int o0 = b2.o0;
            final int p5 = b2.p0;
            final int q0 = b2.q0;
            final float r0 = b2.r0;
            final int p6 = b2.p;
            if (p6 != -1) {
                final ConstraintWidget constraintWidget2 = (ConstraintWidget)sparseArray.get(p6);
                if (constraintWidget2 != null) {
                    constraintWidget.l(constraintWidget2, b2.r, b2.q);
                }
            }
            else {
                Label_0338: {
                    ConstraintWidget constraintWidget3;
                    ConstraintAnchor.Type type;
                    int n2;
                    Enum<ConstraintAnchor.Type> left;
                    if (l0 != -1) {
                        constraintWidget3 = (ConstraintWidget)sparseArray.get(l0);
                        if (constraintWidget3 == null) {
                            break Label_0338;
                        }
                        type = ConstraintAnchor.Type.LEFT;
                        n2 = b2.leftMargin;
                        left = type;
                    }
                    else {
                        if (m0 == -1) {
                            break Label_0338;
                        }
                        constraintWidget3 = (ConstraintWidget)sparseArray.get(m0);
                        if (constraintWidget3 == null) {
                            break Label_0338;
                        }
                        left = ConstraintAnchor.Type.LEFT;
                        type = ConstraintAnchor.Type.RIGHT;
                        n2 = b2.leftMargin;
                    }
                    constraintWidget.e0((ConstraintAnchor.Type)left, constraintWidget3, type, n2, p5);
                }
                Label_0429: {
                    ConstraintWidget constraintWidget4;
                    Enum<ConstraintAnchor.Type> right;
                    ConstraintAnchor.Type type2;
                    int n3;
                    if (n0 != -1) {
                        constraintWidget4 = (ConstraintWidget)sparseArray.get(n0);
                        if (constraintWidget4 == null) {
                            break Label_0429;
                        }
                        right = ConstraintAnchor.Type.RIGHT;
                        type2 = ConstraintAnchor.Type.LEFT;
                        n3 = b2.rightMargin;
                    }
                    else {
                        if (o0 == -1) {
                            break Label_0429;
                        }
                        constraintWidget4 = (ConstraintWidget)sparseArray.get(o0);
                        if (constraintWidget4 == null) {
                            break Label_0429;
                        }
                        type2 = ConstraintAnchor.Type.RIGHT;
                        n3 = b2.rightMargin;
                        right = type2;
                    }
                    constraintWidget.e0((ConstraintAnchor.Type)right, constraintWidget4, type2, n3, q0);
                }
                final int i = b2.i;
                Label_0550: {
                    ConstraintWidget constraintWidget5;
                    ConstraintAnchor.Type type3;
                    int n4;
                    int n5;
                    Enum<ConstraintAnchor.Type> top;
                    if (i != -1) {
                        constraintWidget5 = (ConstraintWidget)sparseArray.get(i);
                        if (constraintWidget5 == null) {
                            break Label_0550;
                        }
                        type3 = ConstraintAnchor.Type.TOP;
                        n4 = b2.topMargin;
                        n5 = b2.x;
                        top = type3;
                    }
                    else {
                        final int j = b2.j;
                        if (j == -1) {
                            break Label_0550;
                        }
                        constraintWidget5 = (ConstraintWidget)sparseArray.get(j);
                        if (constraintWidget5 == null) {
                            break Label_0550;
                        }
                        top = ConstraintAnchor.Type.TOP;
                        type3 = ConstraintAnchor.Type.BOTTOM;
                        n4 = b2.topMargin;
                        n5 = b2.x;
                    }
                    constraintWidget.e0((ConstraintAnchor.Type)top, constraintWidget5, type3, n4, n5);
                }
                final int k = b2.k;
                Label_0671: {
                    ConstraintWidget constraintWidget6;
                    Enum<ConstraintAnchor.Type> bottom;
                    ConstraintAnchor.Type type4;
                    int n6;
                    int n7;
                    if (k != -1) {
                        constraintWidget6 = (ConstraintWidget)sparseArray.get(k);
                        if (constraintWidget6 == null) {
                            break Label_0671;
                        }
                        bottom = ConstraintAnchor.Type.BOTTOM;
                        type4 = ConstraintAnchor.Type.TOP;
                        n6 = b2.bottomMargin;
                        n7 = b2.z;
                    }
                    else {
                        final int l2 = b2.l;
                        if (l2 == -1) {
                            break Label_0671;
                        }
                        constraintWidget6 = (ConstraintWidget)sparseArray.get(l2);
                        if (constraintWidget6 == null) {
                            break Label_0671;
                        }
                        type4 = ConstraintAnchor.Type.BOTTOM;
                        n6 = b2.bottomMargin;
                        n7 = b2.z;
                        bottom = type4;
                    }
                    constraintWidget.e0((ConstraintAnchor.Type)bottom, constraintWidget6, type4, n6, n7);
                }
                int n8 = b2.m;
                Label_0743: {
                    ConstraintAnchor.Type type5;
                    if (n8 != -1) {
                        type5 = ConstraintAnchor.Type.BASELINE;
                    }
                    else {
                        n8 = b2.n;
                        if (n8 != -1) {
                            type5 = ConstraintAnchor.Type.TOP;
                        }
                        else {
                            n8 = b2.o;
                            if (n8 == -1) {
                                break Label_0743;
                            }
                            type5 = ConstraintAnchor.Type.BOTTOM;
                        }
                    }
                    this.e(constraintWidget, b2, sparseArray, n8, type5);
                }
                if (r0 >= 0.0f) {
                    constraintWidget.M0(r0);
                }
                final float h = b2.H;
                if (h >= 0.0f) {
                    constraintWidget.d1(h);
                }
            }
            if (b) {
                final int x = b2.X;
                if (x != -1 || b2.Y != -1) {
                    constraintWidget.b1(x, b2.Y);
                }
            }
            if (!b2.e0) {
                if (b2.width == -1) {
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour;
                    if (b2.a0) {
                        dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    }
                    else {
                        dimensionBehaviour = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                    }
                    constraintWidget.P0(dimensionBehaviour);
                    constraintWidget.o(ConstraintAnchor.Type.LEFT).g = b2.leftMargin;
                    constraintWidget.o(ConstraintAnchor.Type.RIGHT).g = b2.rightMargin;
                }
                else {
                    constraintWidget.P0(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                    constraintWidget.k1(0);
                }
            }
            else {
                constraintWidget.P0(ConstraintWidget.DimensionBehaviour.FIXED);
                constraintWidget.k1(b2.width);
                if (b2.width == -2) {
                    constraintWidget.P0(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
            }
            if (!b2.f0) {
                if (b2.height == -1) {
                    ConstraintWidget.DimensionBehaviour dimensionBehaviour2;
                    if (b2.b0) {
                        dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    }
                    else {
                        dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                    }
                    constraintWidget.g1(dimensionBehaviour2);
                    constraintWidget.o(ConstraintAnchor.Type.TOP).g = b2.topMargin;
                    constraintWidget.o(ConstraintAnchor.Type.BOTTOM).g = b2.bottomMargin;
                }
                else {
                    constraintWidget.g1(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                    constraintWidget.L0(0);
                }
            }
            else {
                constraintWidget.g1(ConstraintWidget.DimensionBehaviour.FIXED);
                constraintWidget.L0(b2.height);
                if (b2.height == -2) {
                    constraintWidget.g1(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
            }
            constraintWidget.D0(b2.I);
            constraintWidget.R0(b2.L);
            constraintWidget.i1(b2.M);
            constraintWidget.N0(b2.N);
            constraintWidget.e1(b2.O);
            constraintWidget.l1(b2.d0);
            constraintWidget.Q0(b2.P, b2.R, b2.T, b2.V);
            constraintWidget.h1(b2.Q, b2.S, b2.U, b2.W);
        }
    }
    
    public final void b(AttributeSet obtainStyledAttributes, int i, int indexCount) {
        this.mLayoutWidget.B0(this);
        this.mLayoutWidget.W1(this.mMeasurer);
        this.mChildrenByIds.put(((View)this).getId(), (Object)this);
        this.mConstraintSet = null;
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes = (AttributeSet)((View)this).getContext().obtainStyledAttributes(obtainStyledAttributes, vb1.n1, i, indexCount);
            int index;
            int resourceId;
            int resourceId2;
            for (indexCount = ((TypedArray)obtainStyledAttributes).getIndexCount(), i = 0; i < indexCount; ++i) {
                index = ((TypedArray)obtainStyledAttributes).getIndex(i);
                if (index == vb1.x1) {
                    this.mMinWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMinWidth);
                }
                else if (index == vb1.y1) {
                    this.mMinHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMinHeight);
                }
                else if (index == vb1.v1) {
                    this.mMaxWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMaxWidth);
                }
                else if (index == vb1.w1) {
                    this.mMaxHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMaxHeight);
                }
                else if (index == vb1.g3) {
                    this.mOptimizationLevel = ((TypedArray)obtainStyledAttributes).getInt(index, this.mOptimizationLevel);
                }
                else if (index == vb1.b2) {
                    resourceId = ((TypedArray)obtainStyledAttributes).getResourceId(index, 0);
                    if (resourceId != 0) {
                        try {
                            this.parseLayoutDescription(resourceId);
                        }
                        catch (final Resources$NotFoundException ex) {
                            this.mConstraintLayoutSpec = null;
                        }
                    }
                }
                else if (index == vb1.F1) {
                    resourceId2 = ((TypedArray)obtainStyledAttributes).getResourceId(index, 0);
                    try {
                        (this.mConstraintSet = new androidx.constraintlayout.widget.c()).k(((View)this).getContext(), resourceId2);
                    }
                    catch (final Resources$NotFoundException ex2) {
                        this.mConstraintSet = null;
                    }
                    this.mConstraintSetId = resourceId2;
                }
            }
            ((TypedArray)obtainStyledAttributes).recycle();
        }
        this.mLayoutWidget.X1(this.mOptimizationLevel);
    }
    
    public final void c() {
        this.mDirtyHierarchy = true;
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof b;
    }
    
    public final void d() {
        final boolean inEditMode = ((View)this).isInEditMode();
        final int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            final ConstraintWidget viewWidget = this.getViewWidget(this.getChildAt(i));
            if (viewWidget != null) {
                viewWidget.t0();
            }
        }
        Label_0143: {
            if (!inEditMode) {
                break Label_0143;
            }
            int i = 0;
        Label_0137_Outer:
            while (true) {
                if (i >= childCount) {
                    break Label_0143;
                }
                final View child = this.getChildAt(i);
                String resourceName;
                int index;
                String substring;
                androidx.constraintlayout.widget.c mConstraintSet;
                int size;
                int mConstraintSetId;
                View child2;
                ConstraintWidget viewWidget2;
                View child3;
                b b;
                Label_0217_Outer:Block_11_Outer:
                while (true) {
                    try {
                        resourceName = ((View)this).getResources().getResourceName(child.getId());
                        this.setDesignInformation(0, resourceName, child.getId());
                        index = resourceName.indexOf(47);
                        substring = resourceName;
                        if (index != -1) {
                            substring = resourceName.substring(index + 1);
                        }
                        this.a(child.getId()).C0(substring);
                        ++i;
                        continue Label_0137_Outer;
                        while (true) {
                        Label_0196:
                            while (true) {
                            Label_0217:
                                while (true) {
                                    Block_10: {
                                        while (true) {
                                            Label_0298: {
                                            Label_0153:
                                                while (true) {
                                                Label_0245_Outer:
                                                    while (true) {
                                                        iftrue(Label_0409:)(i >= childCount);
                                                    Label_0245:
                                                        while (true) {
                                                            while (true) {
                                                                Block_12: {
                                                                    Block_15: {
                                                                        break Block_15;
                                                                        Label_0178: {
                                                                            mConstraintSet = this.mConstraintSet;
                                                                        }
                                                                        iftrue(Label_0196:)(mConstraintSet == null);
                                                                        break Block_10;
                                                                        iftrue(Label_0243:)(i >= size);
                                                                        break Block_12;
                                                                        iftrue(Label_0178:)(i >= childCount);
                                                                        Block_9: {
                                                                            break Block_9;
                                                                            this.getChildAt(i);
                                                                            ++i;
                                                                            break Label_0245;
                                                                        }
                                                                        this.getChildAt(i).getId();
                                                                        mConstraintSetId = this.mConstraintSetId;
                                                                        ++i;
                                                                        continue Label_0153;
                                                                        iftrue(Label_0178:)(this.mConstraintSetId == -1);
                                                                        break Label_0245_Outer;
                                                                    }
                                                                    child2 = this.getChildAt(i);
                                                                    viewWidget2 = this.getViewWidget(child2);
                                                                    iftrue(Label_0368:)(viewWidget2 != null);
                                                                    break Label_0217;
                                                                }
                                                                this.mConstraintHelpers.get(i).n(this);
                                                                ++i;
                                                                continue Label_0217;
                                                                iftrue(Label_0262:)(i >= childCount);
                                                                continue Label_0245_Outer;
                                                            }
                                                            Label_0338: {
                                                                i = 0;
                                                            }
                                                            continue Label_0217_Outer;
                                                            Label_0409:
                                                            return;
                                                            Label_0243:
                                                            i = 0;
                                                            continue Label_0245;
                                                        }
                                                        Label_0262: {
                                                            this.mTempMapIdToWidget.clear();
                                                        }
                                                        this.mTempMapIdToWidget.put(0, (Object)this.mLayoutWidget);
                                                        this.mTempMapIdToWidget.put(((View)this).getId(), (Object)this.mLayoutWidget);
                                                        i = 0;
                                                        break Label_0298;
                                                        ++i;
                                                        continue Label_0217_Outer;
                                                    }
                                                    i = 0;
                                                    continue Label_0153;
                                                }
                                                child3 = this.getChildAt(i);
                                                this.mTempMapIdToWidget.put(child3.getId(), (Object)this.getViewWidget(child3));
                                                ++i;
                                            }
                                            iftrue(Label_0338:)(i >= childCount);
                                            continue Block_11_Outer;
                                        }
                                    }
                                    mConstraintSet.d(this, true);
                                    break Label_0196;
                                    i = 0;
                                    continue Label_0217;
                                }
                                continue Block_11_Outer;
                                Label_0368: {
                                    b = (b)child2.getLayoutParams();
                                }
                                this.mLayoutWidget.a(viewWidget2);
                                this.applyConstraintsFromLayoutParams(inEditMode, child2, viewWidget2, b, this.mTempMapIdToWidget);
                                continue Block_11_Outer;
                            }
                            this.mLayoutWidget.u1();
                            size = this.mConstraintHelpers.size();
                            iftrue(Label_0243:)(size <= 0);
                            continue;
                        }
                    }
                    catch (final Resources$NotFoundException ex) {
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    public void dispatchDraw(final Canvas canvas) {
        final ArrayList<androidx.constraintlayout.widget.b> mConstraintHelpers = this.mConstraintHelpers;
        if (mConstraintHelpers != null) {
            final int size = mConstraintHelpers.size();
            if (size > 0) {
                for (int i = 0; i < size; ++i) {
                    this.mConstraintHelpers.get(i).m(this);
                }
            }
        }
        super.dispatchDraw(canvas);
        if (((View)this).isInEditMode()) {
            final float n = (float)((View)this).getWidth();
            final float n2 = (float)((View)this).getHeight();
            for (int childCount = this.getChildCount(), j = 0; j < childCount; ++j) {
                final View child = this.getChildAt(j);
                if (child.getVisibility() != 8) {
                    final Object tag = child.getTag();
                    if (tag != null && tag instanceof String) {
                        final String[] split = ((String)tag).split(",");
                        if (split.length == 4) {
                            final int int1 = Integer.parseInt(split[0]);
                            final int int2 = Integer.parseInt(split[1]);
                            final int int3 = Integer.parseInt(split[2]);
                            final int int4 = Integer.parseInt(split[3]);
                            final int n3 = (int)(int1 / 1080.0f * n);
                            final int n4 = (int)(int2 / 1920.0f * n2);
                            final int n5 = (int)(int3 / 1080.0f * n);
                            final int n6 = (int)(int4 / 1920.0f * n2);
                            final Paint paint = new Paint();
                            paint.setColor(-65536);
                            final float n7 = (float)n3;
                            final float n8 = (float)n4;
                            final float n9 = (float)(n3 + n5);
                            canvas.drawLine(n7, n8, n9, n8, paint);
                            final float n10 = (float)(n4 + n6);
                            canvas.drawLine(n9, n8, n9, n10, paint);
                            canvas.drawLine(n9, n10, n7, n10, paint);
                            canvas.drawLine(n7, n10, n7, n8, paint);
                            paint.setColor(-16711936);
                            canvas.drawLine(n7, n8, n9, n10, paint);
                            canvas.drawLine(n7, n10, n9, n8, paint);
                        }
                    }
                }
            }
        }
    }
    
    public final void e(final ConstraintWidget constraintWidget, final b b, final SparseArray sparseArray, final int n, final ConstraintAnchor.Type type) {
        final View view = (View)this.mChildrenByIds.get(n);
        final ConstraintWidget constraintWidget2 = (ConstraintWidget)sparseArray.get(n);
        if (constraintWidget2 != null && view != null && view.getLayoutParams() instanceof b) {
            b.g0 = true;
            final ConstraintAnchor.Type baseline = ConstraintAnchor.Type.BASELINE;
            if (type == baseline) {
                final b b2 = (b)view.getLayoutParams();
                b2.g0 = true;
                b2.v0.K0(true);
            }
            constraintWidget.o(baseline).b(constraintWidget2.o(type), b.D, b.C, true);
            constraintWidget.K0(true);
            constraintWidget.o(ConstraintAnchor.Type.TOP).q();
            constraintWidget.o(ConstraintAnchor.Type.BOTTOM).q();
        }
    }
    
    public final boolean f() {
        final int childCount = this.getChildCount();
        final boolean b = false;
        int n = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n >= childCount) {
                break;
            }
            if (this.getChildAt(n).isLayoutRequested()) {
                b2 = true;
                break;
            }
            ++n;
        }
        if (b2) {
            this.d();
        }
        return b2;
    }
    
    public void fillMetrics(final hw0 hw0) {
        this.mLayoutWidget.I1(hw0);
    }
    
    public void forceLayout() {
        this.c();
        super.forceLayout();
    }
    
    public b generateDefaultLayoutParams() {
        return new b(-2, -2);
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return (ViewGroup$LayoutParams)new b(viewGroup$LayoutParams);
    }
    
    public b generateLayoutParams(final AttributeSet set) {
        return new b(((View)this).getContext(), set);
    }
    
    public Object getDesignInformation(final int n, final Object o) {
        if (n == 0 && o instanceof String) {
            final String s = (String)o;
            final HashMap<String, Integer> mDesignIds = this.mDesignIds;
            if (mDesignIds != null && mDesignIds.containsKey(s)) {
                return this.mDesignIds.get(s);
            }
        }
        return null;
    }
    
    public int getMaxHeight() {
        return this.mMaxHeight;
    }
    
    public int getMaxWidth() {
        return this.mMaxWidth;
    }
    
    public int getMinHeight() {
        return this.mMinHeight;
    }
    
    public int getMinWidth() {
        return this.mMinWidth;
    }
    
    public int getOptimizationLevel() {
        return this.mLayoutWidget.K1();
    }
    
    public String getSceneString() {
        final StringBuilder sb = new StringBuilder();
        if (this.mLayoutWidget.o == null) {
            final int id = ((View)this).getId();
            if (id != -1) {
                this.mLayoutWidget.o = ((View)this).getContext().getResources().getResourceEntryName(id);
            }
            else {
                this.mLayoutWidget.o = "parent";
            }
        }
        if (this.mLayoutWidget.t() == null) {
            final d mLayoutWidget = this.mLayoutWidget;
            mLayoutWidget.C0(mLayoutWidget.o);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" setDebugName ");
            sb2.append(this.mLayoutWidget.t());
            Log.v("ConstraintLayout", sb2.toString());
        }
        for (final ConstraintWidget constraintWidget : this.mLayoutWidget.r1()) {
            final View view = (View)constraintWidget.s();
            if (view != null) {
                if (constraintWidget.o == null) {
                    final int id2 = view.getId();
                    if (id2 != -1) {
                        constraintWidget.o = ((View)this).getContext().getResources().getResourceEntryName(id2);
                    }
                }
                if (constraintWidget.t() != null) {
                    continue;
                }
                constraintWidget.C0(constraintWidget.o);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" setDebugName ");
                sb3.append(constraintWidget.t());
                Log.v("ConstraintLayout", sb3.toString());
            }
        }
        this.mLayoutWidget.O(sb);
        return sb.toString();
    }
    
    public View getViewById(final int n) {
        return (View)this.mChildrenByIds.get(n);
    }
    
    public final ConstraintWidget getViewWidget(final View view) {
        if (view == this) {
            return this.mLayoutWidget;
        }
        if (view != null) {
            if (!(view.getLayoutParams() instanceof b)) {
                view.setLayoutParams(this.generateLayoutParams(view.getLayoutParams()));
                if (!(view.getLayoutParams() instanceof b)) {
                    return null;
                }
            }
            return ((b)view.getLayoutParams()).v0;
        }
        return null;
    }
    
    public boolean isRtl() {
        final int flags = ((View)this).getContext().getApplicationInfo().flags;
        final boolean b = false;
        final boolean b2 = (flags & 0x400000) != 0x0;
        boolean b3 = b;
        if (b2) {
            b3 = b;
            if (1 == ((View)this).getLayoutDirection()) {
                b3 = true;
            }
        }
        return b3;
    }
    
    public void loadLayoutDescription(final int n) {
        while (true) {
            if (n == 0) {
                break Label_0026;
            }
            try {
                this.mConstraintLayoutSpec = new qk(((View)this).getContext(), this, n);
                return;
                this.mConstraintLayoutSpec = null;
            }
            catch (final Resources$NotFoundException ex) {
                continue;
            }
            break;
        }
    }
    
    public void onLayout(final boolean b, int i, int n, int n2, int x) {
        n2 = this.getChildCount();
        final boolean inEditMode = ((View)this).isInEditMode();
        n = 0;
        View child;
        b b2;
        ConstraintWidget v0;
        int y;
        for (i = 0; i < n2; ++i) {
            child = this.getChildAt(i);
            b2 = (b)child.getLayoutParams();
            v0 = b2.v0;
            if (child.getVisibility() != 8 || b2.h0 || b2.i0 || b2.k0 || inEditMode) {
                if (!b2.j0) {
                    x = v0.X();
                    y = v0.Y();
                    child.layout(x, y, v0.W() + x, v0.x() + y);
                }
            }
        }
        n2 = this.mConstraintHelpers.size();
        if (n2 > 0) {
            for (i = n; i < n2; ++i) {
                this.mConstraintHelpers.get(i).k(this);
            }
        }
    }
    
    public void onMeasure(final int mOnMeasureWidthMeasureSpec, final int mOnMeasureHeightMeasureSpec) {
        if (this.mOnMeasureWidthMeasureSpec == mOnMeasureWidthMeasureSpec) {
            final int mOnMeasureHeightMeasureSpec2 = this.mOnMeasureHeightMeasureSpec;
        }
        if (!this.mDirtyHierarchy) {
            for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                if (this.getChildAt(i).isLayoutRequested()) {
                    this.mDirtyHierarchy = true;
                    break;
                }
            }
        }
        final boolean mDirtyHierarchy = this.mDirtyHierarchy;
        this.mOnMeasureWidthMeasureSpec = mOnMeasureWidthMeasureSpec;
        this.mOnMeasureHeightMeasureSpec = mOnMeasureHeightMeasureSpec;
        this.mLayoutWidget.Z1(this.isRtl());
        if (this.mDirtyHierarchy) {
            this.mDirtyHierarchy = false;
            if (this.f()) {
                this.mLayoutWidget.b2();
            }
        }
        this.resolveSystem(this.mLayoutWidget, this.mOptimizationLevel, mOnMeasureWidthMeasureSpec, mOnMeasureHeightMeasureSpec);
        this.resolveMeasuredDimension(mOnMeasureWidthMeasureSpec, mOnMeasureHeightMeasureSpec, this.mLayoutWidget.W(), this.mLayoutWidget.x(), this.mLayoutWidget.R1(), this.mLayoutWidget.P1());
    }
    
    public void onViewAdded(final View view) {
        super.onViewAdded(view);
        final ConstraintWidget viewWidget = this.getViewWidget(view);
        if (view instanceof androidx.constraintlayout.widget.d && !(viewWidget instanceof f)) {
            final b b = (b)view.getLayoutParams();
            final f v0 = new f();
            b.v0 = v0;
            b.h0 = true;
            v0.A1(b.Z);
        }
        if (view instanceof androidx.constraintlayout.widget.b) {
            final androidx.constraintlayout.widget.b b2 = (androidx.constraintlayout.widget.b)view;
            b2.o();
            ((b)view.getLayoutParams()).i0 = true;
            if (!this.mConstraintHelpers.contains(b2)) {
                this.mConstraintHelpers.add(b2);
            }
        }
        this.mChildrenByIds.put(view.getId(), (Object)view);
        this.mDirtyHierarchy = true;
    }
    
    public void onViewRemoved(final View o) {
        super.onViewRemoved(o);
        this.mChildrenByIds.remove(o.getId());
        this.mLayoutWidget.t1(this.getViewWidget(o));
        this.mConstraintHelpers.remove(o);
        this.mDirtyHierarchy = true;
    }
    
    public void parseLayoutDescription(final int n) {
        this.mConstraintLayoutSpec = new qk(((View)this).getContext(), this, n);
    }
    
    public void requestLayout() {
        this.c();
        super.requestLayout();
    }
    
    public void resolveMeasuredDimension(int resolveSizeAndState, int min, int n, final int n2, final boolean b, final boolean b2) {
        final c mMeasurer = this.mMeasurer;
        final int e = mMeasurer.e;
        resolveSizeAndState = View.resolveSizeAndState(n + mMeasurer.d, resolveSizeAndState, 0);
        n = View.resolveSizeAndState(n2 + e, min, 0);
        min = Math.min(this.mMaxWidth, resolveSizeAndState & 0xFFFFFF);
        n = Math.min(this.mMaxHeight, n & 0xFFFFFF);
        resolveSizeAndState = min;
        if (b) {
            resolveSizeAndState = (min | 0x1000000);
        }
        min = n;
        if (b2) {
            min = (n | 0x1000000);
        }
        ((View)this).setMeasuredDimension(resolveSizeAndState, min);
        this.mLastMeasureWidth = resolveSizeAndState;
        this.mLastMeasureHeight = min;
    }
    
    public void resolveSystem(final d d, final int n, int n2, int max) {
        final int mode = View$MeasureSpec.getMode(n2);
        final int size = View$MeasureSpec.getSize(n2);
        final int mode2 = View$MeasureSpec.getMode(max);
        final int size2 = View$MeasureSpec.getSize(max);
        final int max2 = Math.max(0, ((View)this).getPaddingTop());
        final int max3 = Math.max(0, ((View)this).getPaddingBottom());
        final int n3 = max2 + max3;
        final int paddingWidth = this.getPaddingWidth();
        this.mMeasurer.c(n2, max, max2, max3, paddingWidth, n3);
        n2 = Math.max(0, ((View)this).getPaddingStart());
        max = Math.max(0, ((View)this).getPaddingEnd());
        if (n2 <= 0 && max <= 0) {
            n2 = Math.max(0, ((View)this).getPaddingLeft());
        }
        else if (this.isRtl()) {
            n2 = max;
        }
        max = size - paddingWidth;
        final int n4 = size2 - n3;
        this.setSelfDimensionBehaviour(d, mode, max, mode2, n4);
        d.S1(n, mode, max, mode2, n4, this.mLastMeasureWidth, this.mLastMeasureHeight, n2, max2);
    }
    
    public void setConstraintSet(final androidx.constraintlayout.widget.c mConstraintSet) {
        this.mConstraintSet = mConstraintSet;
    }
    
    public void setDesignInformation(int i, final Object o, final Object o2) {
        if (i == 0 && o instanceof String && o2 instanceof Integer) {
            if (this.mDesignIds == null) {
                this.mDesignIds = new HashMap<String, Integer>();
            }
            final String s = (String)o;
            i = s.indexOf("/");
            String substring = s;
            if (i != -1) {
                substring = s.substring(i + 1);
            }
            i = (int)o2;
            this.mDesignIds.put(substring, i);
        }
    }
    
    public void setId(final int id) {
        this.mChildrenByIds.remove(((View)this).getId());
        super.setId(id);
        this.mChildrenByIds.put(((View)this).getId(), (Object)this);
    }
    
    public void setMaxHeight(final int mMaxHeight) {
        if (mMaxHeight == this.mMaxHeight) {
            return;
        }
        this.mMaxHeight = mMaxHeight;
        this.requestLayout();
    }
    
    public void setMaxWidth(final int mMaxWidth) {
        if (mMaxWidth == this.mMaxWidth) {
            return;
        }
        this.mMaxWidth = mMaxWidth;
        this.requestLayout();
    }
    
    public void setMinHeight(final int mMinHeight) {
        if (mMinHeight == this.mMinHeight) {
            return;
        }
        this.mMinHeight = mMinHeight;
        this.requestLayout();
    }
    
    public void setMinWidth(final int mMinWidth) {
        if (mMinWidth == this.mMinWidth) {
            return;
        }
        this.mMinWidth = mMinWidth;
        this.requestLayout();
    }
    
    public void setOnConstraintsChanged(final al al) {
        final qk mConstraintLayoutSpec = this.mConstraintLayoutSpec;
        if (mConstraintLayoutSpec != null) {
            mConstraintLayoutSpec.c(al);
        }
    }
    
    public void setOptimizationLevel(final int mOptimizationLevel) {
        this.mOptimizationLevel = mOptimizationLevel;
        this.mLayoutWidget.X1(mOptimizationLevel);
    }
    
    public void setSelfDimensionBehaviour(final d d, final int n, int b, final int n2, int b2) {
        final c mMeasurer = this.mMeasurer;
        final int e = mMeasurer.e;
        final int d2 = mMeasurer.d;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.FIXED;
        final int childCount = this.getChildCount();
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = null;
        Label_0125: {
            Label_0116: {
                if (n != Integer.MIN_VALUE) {
                    if (n != 0) {
                        if (n == 1073741824) {
                            b = Math.min(this.mMaxWidth - d2, b);
                            dimensionBehaviour2 = dimensionBehaviour;
                            break Label_0125;
                        }
                        dimensionBehaviour2 = dimensionBehaviour;
                    }
                    else {
                        final ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        if (childCount == 0) {
                            dimensionBehaviour2 = dimensionBehaviour3;
                            break Label_0116;
                        }
                    }
                    b = 0;
                    break Label_0125;
                }
                final ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (childCount != 0) {
                    break Label_0125;
                }
                dimensionBehaviour2 = dimensionBehaviour4;
            }
            b = Math.max(0, this.mMinWidth);
        }
        Label_0218: {
            Label_0208: {
                if (n2 != Integer.MIN_VALUE) {
                    if (n2 != 0) {
                        if (n2 == 1073741824) {
                            b2 = Math.min(this.mMaxHeight - e, b2);
                            break Label_0218;
                        }
                    }
                    else {
                        final ConstraintWidget.DimensionBehaviour dimensionBehaviour5 = dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        if (childCount == 0) {
                            dimensionBehaviour = dimensionBehaviour5;
                            break Label_0208;
                        }
                    }
                    b2 = 0;
                    break Label_0218;
                }
                final ConstraintWidget.DimensionBehaviour dimensionBehaviour6 = dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (childCount != 0) {
                    break Label_0218;
                }
                dimensionBehaviour = dimensionBehaviour6;
            }
            b2 = Math.max(0, this.mMinHeight);
        }
        if (b != d.W() || b2 != d.x()) {
            d.O1();
        }
        d.m1(0);
        d.n1(0);
        d.X0(this.mMaxWidth - d2);
        d.W0(this.mMaxHeight - e);
        d.a1(0);
        d.Z0(0);
        d.P0(dimensionBehaviour2);
        d.k1(b);
        d.g1(dimensionBehaviour);
        d.L0(b2);
        d.a1(this.mMinWidth - d2);
        d.Z0(this.mMinHeight - e);
    }
    
    public void setState(final int n, final int n2, final int n3) {
        final qk mConstraintLayoutSpec = this.mConstraintLayoutSpec;
        if (mConstraintLayoutSpec != null) {
            mConstraintLayoutSpec.d(n, (float)n2, (float)n3);
        }
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    public static class b extends ViewGroup$MarginLayoutParams
    {
        public int A;
        public int B;
        public int C;
        public int D;
        public boolean E;
        public boolean F;
        public float G;
        public float H;
        public String I;
        public float J;
        public int K;
        public float L;
        public float M;
        public int N;
        public int O;
        public int P;
        public int Q;
        public int R;
        public int S;
        public int T;
        public int U;
        public float V;
        public float W;
        public int X;
        public int Y;
        public int Z;
        public int a;
        public boolean a0;
        public int b;
        public boolean b0;
        public float c;
        public String c0;
        public boolean d;
        public int d0;
        public int e;
        public boolean e0;
        public int f;
        public boolean f0;
        public int g;
        public boolean g0;
        public int h;
        public boolean h0;
        public int i;
        public boolean i0;
        public int j;
        public boolean j0;
        public int k;
        public boolean k0;
        public int l;
        public int l0;
        public int m;
        public int m0;
        public int n;
        public int n0;
        public int o;
        public int o0;
        public int p;
        public int p0;
        public int q;
        public int q0;
        public float r;
        public float r0;
        public int s;
        public int s0;
        public int t;
        public int t0;
        public int u;
        public float u0;
        public int v;
        public ConstraintWidget v0;
        public int w;
        public boolean w0;
        public int x;
        public int y;
        public int z;
        
        public b(final int n, final int n2) {
            super(n, n2);
            this.a = -1;
            this.b = -1;
            this.c = -1.0f;
            this.d = true;
            this.e = -1;
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = 0;
            this.r = 0.0f;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = -1;
            this.w = Integer.MIN_VALUE;
            this.x = Integer.MIN_VALUE;
            this.y = Integer.MIN_VALUE;
            this.z = Integer.MIN_VALUE;
            this.A = Integer.MIN_VALUE;
            this.B = Integer.MIN_VALUE;
            this.C = Integer.MIN_VALUE;
            this.D = 0;
            this.E = true;
            this.F = true;
            this.G = 0.5f;
            this.H = 0.5f;
            this.I = null;
            this.J = 0.0f;
            this.K = 1;
            this.L = -1.0f;
            this.M = -1.0f;
            this.N = 0;
            this.O = 0;
            this.P = 0;
            this.Q = 0;
            this.R = 0;
            this.S = 0;
            this.T = 0;
            this.U = 0;
            this.V = 1.0f;
            this.W = 1.0f;
            this.X = -1;
            this.Y = -1;
            this.Z = -1;
            this.a0 = false;
            this.b0 = false;
            this.c0 = null;
            this.d0 = 0;
            this.e0 = true;
            this.f0 = true;
            this.g0 = false;
            this.h0 = false;
            this.i0 = false;
            this.j0 = false;
            this.k0 = false;
            this.l0 = -1;
            this.m0 = -1;
            this.n0 = -1;
            this.o0 = -1;
            this.p0 = Integer.MIN_VALUE;
            this.q0 = Integer.MIN_VALUE;
            this.r0 = 0.5f;
            this.v0 = new ConstraintWidget();
            this.w0 = false;
        }
        
        public b(final Context context, AttributeSet obtainStyledAttributes) {
            super(context, obtainStyledAttributes);
            this.a = -1;
            this.b = -1;
            this.c = -1.0f;
            this.d = true;
            this.e = -1;
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = 0;
            this.r = 0.0f;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = -1;
            this.w = Integer.MIN_VALUE;
            this.x = Integer.MIN_VALUE;
            this.y = Integer.MIN_VALUE;
            this.z = Integer.MIN_VALUE;
            this.A = Integer.MIN_VALUE;
            this.B = Integer.MIN_VALUE;
            this.C = Integer.MIN_VALUE;
            this.D = 0;
            this.E = true;
            this.F = true;
            this.G = 0.5f;
            this.H = 0.5f;
            this.I = null;
            this.J = 0.0f;
            this.K = 1;
            this.L = -1.0f;
            this.M = -1.0f;
            this.N = 0;
            this.O = 0;
            this.P = 0;
            this.Q = 0;
            this.R = 0;
            this.S = 0;
            this.T = 0;
            this.U = 0;
            this.V = 1.0f;
            this.W = 1.0f;
            this.X = -1;
            this.Y = -1;
            this.Z = -1;
            this.a0 = false;
            this.b0 = false;
            this.c0 = null;
            this.d0 = 0;
            this.e0 = true;
            this.f0 = true;
            this.g0 = false;
            this.h0 = false;
            this.i0 = false;
            this.j0 = false;
            this.k0 = false;
            this.l0 = -1;
            this.m0 = -1;
            this.n0 = -1;
            this.o0 = -1;
            this.p0 = Integer.MIN_VALUE;
            this.q0 = Integer.MIN_VALUE;
            this.r0 = 0.5f;
            this.v0 = new ConstraintWidget();
            this.w0 = false;
            obtainStyledAttributes = (AttributeSet)context.obtainStyledAttributes(obtainStyledAttributes, vb1.n1);
            for (int indexCount = ((TypedArray)obtainStyledAttributes).getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = ((TypedArray)obtainStyledAttributes).getIndex(i);
                final int value = a.a.get(index);
                String s = null;
                switch (value) {
                    default: {
                        switch (value) {
                            default: {
                                switch (value) {
                                    default: {
                                        continue;
                                    }
                                    case 67: {
                                        this.d = ((TypedArray)obtainStyledAttributes).getBoolean(index, this.d);
                                        continue;
                                    }
                                    case 66: {
                                        this.d0 = ((TypedArray)obtainStyledAttributes).getInt(index, this.d0);
                                        continue;
                                    }
                                    case 65: {
                                        androidx.constraintlayout.widget.c.n(this, (TypedArray)obtainStyledAttributes, index, 1);
                                        this.F = true;
                                        continue;
                                    }
                                    case 64: {
                                        androidx.constraintlayout.widget.c.n(this, (TypedArray)obtainStyledAttributes, index, 0);
                                        this.E = true;
                                        continue;
                                    }
                                }
                                break;
                            }
                            case 55: {
                                this.C = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.C);
                                continue;
                            }
                            case 54: {
                                this.D = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.D);
                                continue;
                            }
                            case 53: {
                                final int resourceId = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.o);
                                this.o = resourceId;
                                if (resourceId == -1) {
                                    this.o = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                                }
                                continue;
                            }
                            case 52: {
                                final int resourceId2 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.n);
                                this.n = resourceId2;
                                if (resourceId2 == -1) {
                                    this.n = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                                }
                                continue;
                            }
                            case 51: {
                                this.c0 = ((TypedArray)obtainStyledAttributes).getString(index);
                                continue;
                            }
                            case 50: {
                                this.Y = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.Y);
                                continue;
                            }
                            case 49: {
                                this.X = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.X);
                                continue;
                            }
                            case 48: {
                                this.O = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                                continue;
                            }
                            case 47: {
                                this.N = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                                continue;
                            }
                            case 46: {
                                this.M = ((TypedArray)obtainStyledAttributes).getFloat(index, this.M);
                                continue;
                            }
                            case 45: {
                                this.L = ((TypedArray)obtainStyledAttributes).getFloat(index, this.L);
                                continue;
                            }
                            case 44: {
                                androidx.constraintlayout.widget.c.p(this, ((TypedArray)obtainStyledAttributes).getString(index));
                                continue;
                            }
                        }
                        break;
                    }
                    case 38: {
                        this.W = Math.max(0.0f, ((TypedArray)obtainStyledAttributes).getFloat(index, this.W));
                        this.Q = 2;
                        continue;
                    }
                    case 37: {
                        try {
                            this.U = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.U);
                        }
                        catch (final Exception ex) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.U) == -2) {
                                this.U = -2;
                            }
                        }
                        continue;
                    }
                    case 36: {
                        try {
                            this.S = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.S);
                        }
                        catch (final Exception ex2) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.S) == -2) {
                                this.S = -2;
                            }
                        }
                        continue;
                    }
                    case 35: {
                        this.V = Math.max(0.0f, ((TypedArray)obtainStyledAttributes).getFloat(index, this.V));
                        this.P = 2;
                        continue;
                    }
                    case 34: {
                        try {
                            this.T = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.T);
                        }
                        catch (final Exception ex3) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.T) == -2) {
                                this.T = -2;
                            }
                        }
                        continue;
                    }
                    case 33: {
                        try {
                            this.R = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.R);
                        }
                        catch (final Exception ex4) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.R) == -2) {
                                this.R = -2;
                            }
                        }
                        continue;
                    }
                    case 32: {
                        final int int1 = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                        this.Q = int1;
                        if (int1 == 1) {
                            s = "layout_constraintHeight_default=\"wrap\" is deprecated.\nUse layout_height=\"WRAP_CONTENT\" and layout_constrainedHeight=\"true\" instead.";
                            break;
                        }
                        continue;
                    }
                    case 31: {
                        final int int2 = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                        this.P = int2;
                        if (int2 == 1) {
                            s = "layout_constraintWidth_default=\"wrap\" is deprecated.\nUse layout_width=\"WRAP_CONTENT\" and layout_constrainedWidth=\"true\" instead.";
                            break;
                        }
                        continue;
                    }
                    case 30: {
                        this.H = ((TypedArray)obtainStyledAttributes).getFloat(index, this.H);
                        continue;
                    }
                    case 29: {
                        this.G = ((TypedArray)obtainStyledAttributes).getFloat(index, this.G);
                        continue;
                    }
                    case 28: {
                        this.b0 = ((TypedArray)obtainStyledAttributes).getBoolean(index, this.b0);
                        continue;
                    }
                    case 27: {
                        this.a0 = ((TypedArray)obtainStyledAttributes).getBoolean(index, this.a0);
                        continue;
                    }
                    case 26: {
                        this.B = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.B);
                        continue;
                    }
                    case 25: {
                        this.A = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.A);
                        continue;
                    }
                    case 24: {
                        this.z = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.z);
                        continue;
                    }
                    case 23: {
                        this.y = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.y);
                        continue;
                    }
                    case 22: {
                        this.x = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.x);
                        continue;
                    }
                    case 21: {
                        this.w = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.w);
                        continue;
                    }
                    case 20: {
                        final int resourceId3 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.v);
                        this.v = resourceId3;
                        if (resourceId3 == -1) {
                            this.v = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 19: {
                        final int resourceId4 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.u);
                        this.u = resourceId4;
                        if (resourceId4 == -1) {
                            this.u = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 18: {
                        final int resourceId5 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.t);
                        this.t = resourceId5;
                        if (resourceId5 == -1) {
                            this.t = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 17: {
                        final int resourceId6 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.s);
                        this.s = resourceId6;
                        if (resourceId6 == -1) {
                            this.s = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 16: {
                        final int resourceId7 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.m);
                        this.m = resourceId7;
                        if (resourceId7 == -1) {
                            this.m = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 15: {
                        final int resourceId8 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.l);
                        this.l = resourceId8;
                        if (resourceId8 == -1) {
                            this.l = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 14: {
                        final int resourceId9 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.k);
                        this.k = resourceId9;
                        if (resourceId9 == -1) {
                            this.k = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 13: {
                        final int resourceId10 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.j);
                        this.j = resourceId10;
                        if (resourceId10 == -1) {
                            this.j = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 12: {
                        final int resourceId11 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.i);
                        this.i = resourceId11;
                        if (resourceId11 == -1) {
                            this.i = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 11: {
                        final int resourceId12 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.h);
                        this.h = resourceId12;
                        if (resourceId12 == -1) {
                            this.h = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 10: {
                        final int resourceId13 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.g);
                        this.g = resourceId13;
                        if (resourceId13 == -1) {
                            this.g = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 9: {
                        final int resourceId14 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.f);
                        this.f = resourceId14;
                        if (resourceId14 == -1) {
                            this.f = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 8: {
                        final int resourceId15 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.e);
                        this.e = resourceId15;
                        if (resourceId15 == -1) {
                            this.e = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 7: {
                        this.c = ((TypedArray)obtainStyledAttributes).getFloat(index, this.c);
                        continue;
                    }
                    case 6: {
                        this.b = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.b);
                        continue;
                    }
                    case 5: {
                        this.a = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.a);
                        continue;
                    }
                    case 4: {
                        final float r = ((TypedArray)obtainStyledAttributes).getFloat(index, this.r) % 360.0f;
                        this.r = r;
                        if (r < 0.0f) {
                            this.r = (360.0f - r) % 360.0f;
                        }
                        continue;
                    }
                    case 3: {
                        this.q = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.q);
                        continue;
                    }
                    case 2: {
                        final int resourceId16 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.p);
                        this.p = resourceId16;
                        if (resourceId16 == -1) {
                            this.p = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                        }
                        continue;
                    }
                    case 1: {
                        this.Z = ((TypedArray)obtainStyledAttributes).getInt(index, this.Z);
                        continue;
                    }
                }
                Log.e("ConstraintLayout", s);
            }
            ((TypedArray)obtainStyledAttributes).recycle();
            this.a();
        }
        
        public b(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.a = -1;
            this.b = -1;
            this.c = -1.0f;
            this.d = true;
            this.e = -1;
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = 0;
            this.r = 0.0f;
            this.s = -1;
            this.t = -1;
            this.u = -1;
            this.v = -1;
            this.w = Integer.MIN_VALUE;
            this.x = Integer.MIN_VALUE;
            this.y = Integer.MIN_VALUE;
            this.z = Integer.MIN_VALUE;
            this.A = Integer.MIN_VALUE;
            this.B = Integer.MIN_VALUE;
            this.C = Integer.MIN_VALUE;
            this.D = 0;
            this.E = true;
            this.F = true;
            this.G = 0.5f;
            this.H = 0.5f;
            this.I = null;
            this.J = 0.0f;
            this.K = 1;
            this.L = -1.0f;
            this.M = -1.0f;
            this.N = 0;
            this.O = 0;
            this.P = 0;
            this.Q = 0;
            this.R = 0;
            this.S = 0;
            this.T = 0;
            this.U = 0;
            this.V = 1.0f;
            this.W = 1.0f;
            this.X = -1;
            this.Y = -1;
            this.Z = -1;
            this.a0 = false;
            this.b0 = false;
            this.c0 = null;
            this.d0 = 0;
            this.e0 = true;
            this.f0 = true;
            this.g0 = false;
            this.h0 = false;
            this.i0 = false;
            this.j0 = false;
            this.k0 = false;
            this.l0 = -1;
            this.m0 = -1;
            this.n0 = -1;
            this.o0 = -1;
            this.p0 = Integer.MIN_VALUE;
            this.q0 = Integer.MIN_VALUE;
            this.r0 = 0.5f;
            this.v0 = new ConstraintWidget();
            this.w0 = false;
        }
        
        public void a() {
            this.h0 = false;
            this.e0 = true;
            this.f0 = true;
            final int width = super.width;
            if (width == -2 && this.a0) {
                this.e0 = false;
                if (this.P == 0) {
                    this.P = 1;
                }
            }
            final int height = super.height;
            if (height == -2 && this.b0) {
                this.f0 = false;
                if (this.Q == 0) {
                    this.Q = 1;
                }
            }
            if (width == 0 || width == -1) {
                this.e0 = false;
                if (width == 0 && this.P == 1) {
                    super.width = -2;
                    this.a0 = true;
                }
            }
            if (height == 0 || height == -1) {
                this.f0 = false;
                if (height == 0 && this.Q == 1) {
                    super.height = -2;
                    this.b0 = true;
                }
            }
            if (this.c != -1.0f || this.a != -1 || this.b != -1) {
                this.h0 = true;
                this.e0 = true;
                this.f0 = true;
                if (!(this.v0 instanceof f)) {
                    this.v0 = new f();
                }
                ((f)this.v0).A1(this.Z);
            }
        }
        
        public void resolveLayoutDirection(int m0) {
            final int leftMargin = super.leftMargin;
            final int rightMargin = super.rightMargin;
            super.resolveLayoutDirection(m0);
            m0 = this.getLayoutDirection();
            final int n = 0;
            if (1 == m0) {
                m0 = 1;
            }
            else {
                m0 = 0;
            }
            this.n0 = -1;
            this.o0 = -1;
            this.l0 = -1;
            this.m0 = -1;
            this.p0 = this.w;
            this.q0 = this.y;
            final float g = this.G;
            this.r0 = g;
            final int a = this.a;
            this.s0 = a;
            final int b = this.b;
            this.t0 = b;
            final float c = this.c;
            this.u0 = c;
            Label_0441: {
                if (m0 != 0) {
                    m0 = this.s;
                    Label_0165: {
                        if (m0 != -1) {
                            this.n0 = m0;
                        }
                        else {
                            final int t = this.t;
                            m0 = n;
                            if (t == -1) {
                                break Label_0165;
                            }
                            this.o0 = t;
                        }
                        m0 = 1;
                    }
                    final int u = this.u;
                    if (u != -1) {
                        this.m0 = u;
                        m0 = 1;
                    }
                    final int v = this.v;
                    if (v != -1) {
                        this.l0 = v;
                        m0 = 1;
                    }
                    final int a2 = this.A;
                    if (a2 != Integer.MIN_VALUE) {
                        this.q0 = a2;
                    }
                    final int b2 = this.B;
                    if (b2 != Integer.MIN_VALUE) {
                        this.p0 = b2;
                    }
                    if (m0 != 0) {
                        this.r0 = 1.0f - g;
                    }
                    if (this.h0 && this.Z == 1 && this.d) {
                        if (c != -1.0f) {
                            this.u0 = 1.0f - c;
                            this.s0 = -1;
                            this.t0 = -1;
                        }
                        else {
                            if (a != -1) {
                                this.t0 = a;
                                this.s0 = -1;
                            }
                            else {
                                if (b == -1) {
                                    break Label_0441;
                                }
                                this.s0 = b;
                                this.t0 = -1;
                            }
                            this.u0 = -1.0f;
                        }
                    }
                }
                else {
                    m0 = this.s;
                    if (m0 != -1) {
                        this.m0 = m0;
                    }
                    m0 = this.t;
                    if (m0 != -1) {
                        this.l0 = m0;
                    }
                    m0 = this.u;
                    if (m0 != -1) {
                        this.n0 = m0;
                    }
                    m0 = this.v;
                    if (m0 != -1) {
                        this.o0 = m0;
                    }
                    m0 = this.A;
                    if (m0 != Integer.MIN_VALUE) {
                        this.p0 = m0;
                    }
                    m0 = this.B;
                    if (m0 != Integer.MIN_VALUE) {
                        this.q0 = m0;
                    }
                }
            }
            if (this.u == -1 && this.v == -1 && this.t == -1 && this.s == -1) {
                m0 = this.g;
                Label_0539: {
                    if (m0 != -1) {
                        this.n0 = m0;
                        if (super.rightMargin > 0 || rightMargin <= 0) {
                            break Label_0539;
                        }
                    }
                    else {
                        m0 = this.h;
                        if (m0 == -1) {
                            break Label_0539;
                        }
                        this.o0 = m0;
                        if (super.rightMargin > 0 || rightMargin <= 0) {
                            break Label_0539;
                        }
                    }
                    super.rightMargin = rightMargin;
                }
                m0 = this.e;
                if (m0 != -1) {
                    this.l0 = m0;
                    if (super.leftMargin > 0 || leftMargin <= 0) {
                        return;
                    }
                }
                else {
                    m0 = this.f;
                    if (m0 == -1) {
                        return;
                    }
                    this.m0 = m0;
                    if (super.leftMargin > 0 || leftMargin <= 0) {
                        return;
                    }
                }
                super.leftMargin = leftMargin;
            }
        }
        
        public abstract static class a
        {
            public static final SparseIntArray a;
            
            static {
                final SparseIntArray a2 = new SparseIntArray();
                (a = a2).append(vb1.R2, 64);
                a2.append(vb1.u2, 65);
                a2.append(vb1.D2, 8);
                a2.append(vb1.E2, 9);
                a2.append(vb1.G2, 10);
                a2.append(vb1.H2, 11);
                a2.append(vb1.N2, 12);
                a2.append(vb1.M2, 13);
                a2.append(vb1.k2, 14);
                a2.append(vb1.j2, 15);
                a2.append(vb1.f2, 16);
                a2.append(vb1.h2, 52);
                a2.append(vb1.g2, 53);
                a2.append(vb1.l2, 2);
                a2.append(vb1.n2, 3);
                a2.append(vb1.m2, 4);
                a2.append(vb1.W2, 49);
                a2.append(vb1.X2, 50);
                a2.append(vb1.r2, 5);
                a2.append(vb1.s2, 6);
                a2.append(vb1.t2, 7);
                a2.append(vb1.a2, 67);
                a2.append(vb1.o1, 1);
                a2.append(vb1.I2, 17);
                a2.append(vb1.J2, 18);
                a2.append(vb1.q2, 19);
                a2.append(vb1.p2, 20);
                a2.append(vb1.b3, 21);
                a2.append(vb1.e3, 22);
                a2.append(vb1.c3, 23);
                a2.append(vb1.Z2, 24);
                a2.append(vb1.d3, 25);
                a2.append(vb1.a3, 26);
                a2.append(vb1.Y2, 55);
                a2.append(vb1.f3, 54);
                a2.append(vb1.z2, 29);
                a2.append(vb1.O2, 30);
                a2.append(vb1.o2, 44);
                a2.append(vb1.B2, 45);
                a2.append(vb1.Q2, 46);
                a2.append(vb1.A2, 47);
                a2.append(vb1.P2, 48);
                a2.append(vb1.d2, 27);
                a2.append(vb1.c2, 28);
                a2.append(vb1.S2, 31);
                a2.append(vb1.v2, 32);
                a2.append(vb1.U2, 33);
                a2.append(vb1.T2, 34);
                a2.append(vb1.V2, 35);
                a2.append(vb1.x2, 36);
                a2.append(vb1.w2, 37);
                a2.append(vb1.y2, 38);
                a2.append(vb1.C2, 39);
                a2.append(vb1.L2, 40);
                a2.append(vb1.F2, 41);
                a2.append(vb1.i2, 42);
                a2.append(vb1.e2, 43);
                a2.append(vb1.K2, 51);
                a2.append(vb1.h3, 66);
            }
        }
    }
    
    public class c implements lb.b
    {
        public ConstraintLayout a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public final ConstraintLayout h;
        
        public c(final ConstraintLayout h, final ConstraintLayout a) {
            this.h = h;
            this.a = a;
        }
        
        @Override
        public final void a(final ConstraintWidget constraintWidget, final lb.a a) {
            if (constraintWidget == null) {
                return;
            }
            if (constraintWidget.V() == 8 && !constraintWidget.j0()) {
                a.e = 0;
                a.f = 0;
                a.g = 0;
                return;
            }
            if (constraintWidget.K() == null) {
                return;
            }
            final ConstraintWidget.DimensionBehaviour a2 = a.a;
            final ConstraintWidget.DimensionBehaviour b = a.b;
            int n = a.c;
            final int d = a.d;
            final int n2 = this.b + this.c;
            final int d2 = this.d;
            final View view = (View)constraintWidget.s();
            final int[] a3 = ConstraintLayout$a.a;
            final int n3 = a3[a2.ordinal()];
            int n4 = 0;
            Label_0349: {
                Label_0340: {
                    if (n3 != 1) {
                        int n6;
                        int n7;
                        int n8;
                        if (n3 != 2) {
                            if (n3 != 3) {
                                if (n3 != 4) {
                                    n4 = 0;
                                    break Label_0349;
                                }
                                final int childMeasureSpec = ViewGroup.getChildMeasureSpec(this.f, d2, -2);
                                final boolean b2 = constraintWidget.w == 1;
                                final int j = a.j;
                                if (j != lb.a.l) {
                                    n4 = childMeasureSpec;
                                    if (j != lb.a.m) {
                                        break Label_0349;
                                    }
                                }
                                final boolean b3 = view.getMeasuredHeight() == constraintWidget.x();
                                final boolean b4 = a.j == lb.a.m || !b2 || (b2 && b3) || constraintWidget.n0();
                                n4 = childMeasureSpec;
                                if (b4) {
                                    n = constraintWidget.W();
                                    break Label_0340;
                                }
                                break Label_0349;
                            }
                            else {
                                final int f = this.f;
                                final int n5 = d2 + constraintWidget.B();
                                n6 = -1;
                                n7 = f;
                                n8 = n5;
                            }
                        }
                        else {
                            final int f2 = this.f;
                            n6 = -2;
                            n8 = d2;
                            n7 = f2;
                        }
                        n4 = ViewGroup.getChildMeasureSpec(n7, n8, n6);
                        break Label_0349;
                    }
                }
                n4 = View$MeasureSpec.makeMeasureSpec(n, 1073741824);
            }
            final int n9 = a3[b.ordinal()];
            int x = d;
            int n10 = 0;
            Label_0590: {
                Label_0581: {
                    if (n9 != 1) {
                        int n11;
                        int n12;
                        int n13;
                        if (n9 != 2) {
                            if (n9 != 3) {
                                if (n9 != 4) {
                                    n10 = 0;
                                    break Label_0590;
                                }
                                final int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(this.g, n2, -2);
                                final boolean b5 = constraintWidget.x == 1;
                                final int i = a.j;
                                if (i != lb.a.l) {
                                    n10 = childMeasureSpec2;
                                    if (i != lb.a.m) {
                                        break Label_0590;
                                    }
                                }
                                final boolean b6 = view.getMeasuredWidth() == constraintWidget.W();
                                final boolean b7 = a.j == lb.a.m || !b5 || (b5 && b6) || constraintWidget.o0();
                                n10 = childMeasureSpec2;
                                if (b7) {
                                    x = constraintWidget.x();
                                    break Label_0581;
                                }
                                break Label_0590;
                            }
                            else {
                                n11 = this.g;
                                n12 = n2 + constraintWidget.U();
                                n13 = -1;
                            }
                        }
                        else {
                            n11 = this.g;
                            n13 = -2;
                            n12 = n2;
                        }
                        n10 = ViewGroup.getChildMeasureSpec(n11, n12, n13);
                        break Label_0590;
                    }
                }
                n10 = View$MeasureSpec.makeMeasureSpec(x, 1073741824);
            }
            final d d3 = (d)constraintWidget.K();
            if (d3 != null && androidx.constraintlayout.core.widgets.g.b(ConstraintLayout.access$000(this.h), 256) && view.getMeasuredWidth() == constraintWidget.W() && view.getMeasuredWidth() < d3.W() && view.getMeasuredHeight() == constraintWidget.x() && view.getMeasuredHeight() < d3.x() && view.getBaseline() == constraintWidget.p() && !constraintWidget.m0() && (this.d(constraintWidget.C(), n4, constraintWidget.W()) && this.d(constraintWidget.D(), n10, constraintWidget.x()))) {
                a.e = constraintWidget.W();
                a.f = constraintWidget.x();
                a.g = constraintWidget.p();
                return;
            }
            final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            final boolean b8 = a2 == match_CONSTRAINT;
            final boolean b9 = b == match_CONSTRAINT;
            final ConstraintWidget.DimensionBehaviour match_PARENT = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
            final boolean b10 = b == match_PARENT || b == ConstraintWidget.DimensionBehaviour.FIXED;
            final boolean b11 = a2 == match_PARENT || a2 == ConstraintWidget.DimensionBehaviour.FIXED;
            final boolean b12 = b8 && constraintWidget.d0 > 0.0f;
            final boolean b13 = b9 && constraintWidget.d0 > 0.0f;
            if (view == null) {
                return;
            }
            final ConstraintLayout.b b14 = (ConstraintLayout.b)view.getLayoutParams();
            final int k = a.j;
            int measuredHeight = 0;
            int baseline = 0;
            int measuredWidth = 0;
            Label_1368: {
                if (k != lb.a.l && k != lb.a.m && b8 && constraintWidget.w == 0 && b9 && constraintWidget.x == 0) {
                    measuredHeight = 0;
                    baseline = 0;
                    measuredWidth = 0;
                }
                else {
                    if (view instanceof i52 && constraintWidget instanceof h) {
                        ((i52)view).p((h)constraintWidget, n4, n10);
                    }
                    else {
                        view.measure(n4, n10);
                    }
                    constraintWidget.V0(n4, n10);
                    final int measuredWidth2 = view.getMeasuredWidth();
                    final int measuredHeight2 = view.getMeasuredHeight();
                    final int baseline2 = view.getBaseline();
                    final int z = constraintWidget.z;
                    int max;
                    if (z > 0) {
                        max = Math.max(z, measuredWidth2);
                    }
                    else {
                        max = measuredWidth2;
                    }
                    final int a4 = constraintWidget.A;
                    int min = max;
                    if (a4 > 0) {
                        min = Math.min(a4, max);
                    }
                    final int c = constraintWidget.C;
                    int max2;
                    if (c > 0) {
                        max2 = Math.max(c, measuredHeight2);
                    }
                    else {
                        max2 = measuredHeight2;
                    }
                    final int d4 = constraintWidget.D;
                    int min2 = max2;
                    if (d4 > 0) {
                        min2 = Math.min(d4, max2);
                    }
                    int n14 = min2;
                    int n15 = min;
                    if (!androidx.constraintlayout.core.widgets.g.b(ConstraintLayout.access$000(this.h), 1)) {
                        if (b12 && b10) {
                            n15 = (int)(min2 * constraintWidget.d0 + 0.5f);
                            n14 = min2;
                        }
                        else {
                            n14 = min2;
                            n15 = min;
                            if (b13) {
                                n14 = min2;
                                n15 = min;
                                if (b11) {
                                    n14 = (int)(min / constraintWidget.d0 + 0.5f);
                                    n15 = min;
                                }
                            }
                        }
                    }
                    Label_1292: {
                        if (measuredWidth2 != n15) {
                            break Label_1292;
                        }
                        measuredHeight = n14;
                        baseline = baseline2;
                        measuredWidth = n15;
                        if (measuredHeight2 != n14) {
                            break Label_1292;
                        }
                        break Label_1368;
                    }
                    if (measuredWidth2 != n15) {
                        n4 = View$MeasureSpec.makeMeasureSpec(n15, 1073741824);
                    }
                    if (measuredHeight2 != n14) {
                        n10 = View$MeasureSpec.makeMeasureSpec(n14, 1073741824);
                    }
                    view.measure(n4, n10);
                    constraintWidget.V0(n4, n10);
                    measuredWidth = view.getMeasuredWidth();
                    measuredHeight = view.getMeasuredHeight();
                    baseline = view.getBaseline();
                }
            }
            boolean h = baseline != -1;
            a.i = (measuredWidth != a.c || measuredHeight != a.d);
            if (b14.g0) {
                h = true;
            }
            if (h && baseline != -1 && constraintWidget.p() != baseline) {
                a.i = true;
            }
            a.e = measuredWidth;
            a.f = measuredHeight;
            a.h = h;
            a.g = baseline;
        }
        
        @Override
        public final void b() {
            final int childCount = this.a.getChildCount();
            final int n = 0;
            for (int i = 0; i < childCount; ++i) {
                this.a.getChildAt(i);
            }
            final int size = ConstraintLayout.access$100(this.a).size();
            if (size > 0) {
                for (int j = n; j < size; ++j) {
                    ((b)ConstraintLayout.access$100(this.a).get(j)).l(this.a);
                }
            }
        }
        
        public void c(final int f, final int g, final int b, final int c, final int d, final int e) {
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
        }
        
        public final boolean d(int mode, int size, final int n) {
            if (mode == size) {
                return true;
            }
            final int mode2 = View$MeasureSpec.getMode(mode);
            View$MeasureSpec.getSize(mode);
            mode = View$MeasureSpec.getMode(size);
            size = View$MeasureSpec.getSize(size);
            return mode == 1073741824 && (mode2 == Integer.MIN_VALUE || mode2 == 0) && n == size;
        }
    }
}

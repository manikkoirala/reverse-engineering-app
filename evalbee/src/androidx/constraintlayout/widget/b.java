// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.graphics.Canvas;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.content.res.Resources$NotFoundException;
import android.view.ViewParent;
import android.view.ViewGroup$LayoutParams;
import java.util.Arrays;
import android.util.Log;
import android.util.AttributeSet;
import java.util.HashMap;
import android.content.Context;
import android.view.View;

public abstract class b extends View
{
    public int[] a;
    public int b;
    public Context c;
    public fd0 d;
    public boolean e;
    public String f;
    public String g;
    public View[] h;
    public HashMap i;
    
    public b(final Context c) {
        super(c);
        this.a = new int[32];
        this.e = false;
        this.h = null;
        this.i = new HashMap();
        this.c = c;
        this.i(null);
    }
    
    public b(final Context c, final AttributeSet set) {
        super(c, set);
        this.a = new int[32];
        this.e = false;
        this.h = null;
        this.i = new HashMap();
        this.c = c;
        this.i(set);
    }
    
    public final void a(String trim) {
        if (trim != null) {
            if (trim.length() != 0) {
                if (this.c == null) {
                    return;
                }
                trim = trim.trim();
                if (this.getParent() instanceof ConstraintLayout) {
                    final ConstraintLayout constraintLayout = (ConstraintLayout)this.getParent();
                }
                final int h = this.h(trim);
                if (h != 0) {
                    this.i.put(h, trim);
                    this.b(h);
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Could not find id of \"");
                    sb.append(trim);
                    sb.append("\"");
                    Log.w("ConstraintHelper", sb.toString());
                }
            }
        }
    }
    
    public final void b(final int n) {
        if (n == this.getId()) {
            return;
        }
        final int b = this.b;
        final int[] a = this.a;
        if (b + 1 > a.length) {
            this.a = Arrays.copyOf(a, a.length * 2);
        }
        final int[] a2 = this.a;
        final int b2 = this.b;
        a2[b2] = n;
        this.b = b2 + 1;
    }
    
    public final void c(final String s) {
        if (s != null) {
            if (s.length() != 0) {
                if (this.c == null) {
                    return;
                }
                final String trim = s.trim();
                ConstraintLayout constraintLayout;
                if (this.getParent() instanceof ConstraintLayout) {
                    constraintLayout = (ConstraintLayout)this.getParent();
                }
                else {
                    constraintLayout = null;
                }
                if (constraintLayout == null) {
                    Log.w("ConstraintHelper", "Parent not a ConstraintLayout");
                    return;
                }
                for (int childCount = constraintLayout.getChildCount(), i = 0; i < childCount; ++i) {
                    final View child = constraintLayout.getChildAt(i);
                    final ViewGroup$LayoutParams layoutParams = child.getLayoutParams();
                    if (layoutParams instanceof ConstraintLayout.b && trim.equals(((ConstraintLayout.b)layoutParams).c0)) {
                        if (child.getId() == -1) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("to use ConstraintTag view ");
                            sb.append(child.getClass().getSimpleName());
                            sb.append(" must have an ID");
                            Log.w("ConstraintHelper", sb.toString());
                        }
                        else {
                            this.b(child.getId());
                        }
                    }
                }
            }
        }
    }
    
    public void d() {
        final ViewParent parent = this.getParent();
        if (parent != null && parent instanceof ConstraintLayout) {
            this.e((ConstraintLayout)parent);
        }
    }
    
    public void e(final ConstraintLayout constraintLayout) {
        final int visibility = this.getVisibility();
        final float elevation = this.getElevation();
        for (int i = 0; i < this.b; ++i) {
            final View viewById = constraintLayout.getViewById(this.a[i]);
            if (viewById != null) {
                viewById.setVisibility(visibility);
                if (elevation > 0.0f) {
                    viewById.setTranslationZ(viewById.getTranslationZ() + elevation);
                }
            }
        }
    }
    
    public void f(final ConstraintLayout constraintLayout) {
    }
    
    public final int g(final ConstraintLayout constraintLayout, final String s) {
        if (s != null) {
            if (constraintLayout != null) {
                final Resources resources = this.c.getResources();
                if (resources == null) {
                    return 0;
                }
                for (int childCount = constraintLayout.getChildCount(), i = 0; i < childCount; ++i) {
                    final View child = constraintLayout.getChildAt(i);
                    if (child.getId() != -1) {
                        String resourceEntryName;
                        try {
                            resourceEntryName = resources.getResourceEntryName(child.getId());
                        }
                        catch (final Resources$NotFoundException ex) {
                            resourceEntryName = null;
                        }
                        if (s.equals(resourceEntryName)) {
                            return child.getId();
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    public int[] getReferencedIds() {
        return Arrays.copyOf(this.a, this.b);
    }
    
    public final int h(final String name) {
        ConstraintLayout constraintLayout;
        if (this.getParent() instanceof ConstraintLayout) {
            constraintLayout = (ConstraintLayout)this.getParent();
        }
        else {
            constraintLayout = null;
        }
        final boolean inEditMode = this.isInEditMode();
        int intValue;
        final int n = intValue = 0;
        if (inEditMode) {
            intValue = n;
            if (constraintLayout != null) {
                final Object designInformation = constraintLayout.getDesignInformation(0, name);
                intValue = n;
                if (designInformation instanceof Integer) {
                    intValue = (int)designInformation;
                }
            }
        }
        int n2;
        if ((n2 = intValue) == 0) {
            n2 = intValue;
            if (constraintLayout != null) {
                n2 = this.g(constraintLayout, name);
            }
        }
        while (true) {
            int int1;
            if ((int1 = n2) != 0) {
                break Label_0113;
            }
            try {
                int1 = eb1.class.getField(name).getInt(null);
                if ((n2 = int1) == 0) {
                    n2 = this.c.getResources().getIdentifier(name, "id", this.c.getPackageName());
                }
                return n2;
            }
            catch (final Exception ex) {
                int1 = n2;
                continue;
            }
            break;
        }
    }
    
    public void i(final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, vb1.n1);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == vb1.G1) {
                    this.setIds(this.f = obtainStyledAttributes.getString(index));
                }
                else if (index == vb1.H1) {
                    this.setReferenceTags(this.g = obtainStyledAttributes.getString(index));
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public abstract void j(final ConstraintWidget p0, final boolean p1);
    
    public void k(final ConstraintLayout constraintLayout) {
    }
    
    public void l(final ConstraintLayout constraintLayout) {
    }
    
    public void m(final ConstraintLayout constraintLayout) {
    }
    
    public void n(final ConstraintLayout constraintLayout) {
        if (this.isInEditMode()) {
            this.setIds(this.f);
        }
        final fd0 d = this.d;
        if (d == null) {
            return;
        }
        d.b();
        for (int i = 0; i < this.b; ++i) {
            final int j = this.a[i];
            final View viewById = constraintLayout.getViewById(j);
            View viewById2;
            if ((viewById2 = viewById) == null) {
                final String value = this.i.get(j);
                final int g = this.g(constraintLayout, value);
                viewById2 = viewById;
                if (g != 0) {
                    this.a[i] = g;
                    this.i.put(g, value);
                    viewById2 = constraintLayout.getViewById(g);
                }
            }
            if (viewById2 != null) {
                this.d.a(constraintLayout.getViewWidget(viewById2));
            }
        }
        this.d.c(constraintLayout.mLayoutWidget);
    }
    
    public void o() {
        if (this.d == null) {
            return;
        }
        final ViewGroup$LayoutParams layoutParams = this.getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.b) {
            ((ConstraintLayout.b)layoutParams).v0 = (ConstraintWidget)this.d;
        }
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        final String f = this.f;
        if (f != null) {
            this.setIds(f);
        }
        final String g = this.g;
        if (g != null) {
            this.setReferenceTags(g);
        }
    }
    
    public void onDraw(final Canvas canvas) {
    }
    
    public void onMeasure(final int n, final int n2) {
        if (this.e) {
            super.onMeasure(n, n2);
        }
        else {
            this.setMeasuredDimension(0, 0);
        }
    }
    
    public void setIds(final String f) {
        this.f = f;
        if (f == null) {
            return;
        }
        int beginIndex = 0;
        this.b = 0;
        while (true) {
            final int index = f.indexOf(44, beginIndex);
            if (index == -1) {
                break;
            }
            this.a(f.substring(beginIndex, index));
            beginIndex = index + 1;
        }
        this.a(f.substring(beginIndex));
    }
    
    public void setReferenceTags(final String g) {
        this.g = g;
        if (g == null) {
            return;
        }
        int beginIndex = 0;
        this.b = 0;
        while (true) {
            final int index = g.indexOf(44, beginIndex);
            if (index == -1) {
                break;
            }
            this.c(g.substring(beginIndex, index));
            beginIndex = index + 1;
        }
        this.c(g.substring(beginIndex));
    }
    
    public void setReferencedIds(final int[] array) {
        this.f = null;
        int i = 0;
        this.b = 0;
        while (i < array.length) {
            this.b(array[i]);
            ++i;
        }
    }
    
    public void setTag(final int n, final Object o) {
        super.setTag(n, o);
        if (o == null && this.f == null) {
            this.b(n);
        }
    }
}

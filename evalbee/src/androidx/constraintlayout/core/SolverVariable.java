// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.util.Arrays;
import java.util.HashSet;

public class SolverVariable implements Comparable
{
    public static int v = 1;
    public boolean a;
    public String b;
    public int c;
    public int d;
    public int e;
    public float f;
    public boolean g;
    public float[] h;
    public float[] i;
    public Type j;
    public b[] k;
    public int l;
    public int m;
    public boolean n;
    public int p;
    public float q;
    public HashSet t;
    
    public SolverVariable(final Type j, final String s) {
        this.c = -1;
        this.d = -1;
        this.e = 0;
        this.g = false;
        this.h = new float[9];
        this.i = new float[9];
        this.k = new b[16];
        this.l = 0;
        this.m = 0;
        this.n = false;
        this.p = -1;
        this.q = 0.0f;
        this.t = null;
        this.j = j;
    }
    
    public static void d() {
        ++SolverVariable.v;
    }
    
    public final void a(final b b) {
        int n = 0;
        while (true) {
            final int l = this.l;
            if (n >= l) {
                final b[] k = this.k;
                if (l >= k.length) {
                    this.k = Arrays.copyOf(k, k.length * 2);
                }
                final b[] i = this.k;
                final int j = this.l;
                i[j] = b;
                this.l = j + 1;
                return;
            }
            if (this.k[n] == b) {
                return;
            }
            ++n;
        }
    }
    
    public int c(final SolverVariable solverVariable) {
        return this.c - solverVariable.c;
    }
    
    public final void e(final b b) {
        for (int l = this.l, i = 0; i < l; ++i) {
            if (this.k[i] == b) {
                while (i < l - 1) {
                    final b[] k = this.k;
                    final int n = i + 1;
                    k[i] = k[n];
                    i = n;
                }
                --this.l;
                return;
            }
        }
    }
    
    public void f() {
        this.b = null;
        this.j = Type.UNKNOWN;
        this.e = 0;
        this.c = -1;
        this.d = -1;
        this.f = 0.0f;
        this.g = false;
        this.n = false;
        this.p = -1;
        this.q = 0.0f;
        for (int l = this.l, i = 0; i < l; ++i) {
            this.k[i] = null;
        }
        this.l = 0;
        this.m = 0;
        this.a = false;
        Arrays.fill(this.i, 0.0f);
    }
    
    public void g(final c c, final float f) {
        this.f = f;
        this.g = true;
        this.n = false;
        this.p = -1;
        this.q = 0.0f;
        final int l = this.l;
        this.d = -1;
        for (int i = 0; i < l; ++i) {
            this.k[i].A(c, this, false);
        }
        this.l = 0;
    }
    
    public void h(final Type j, final String s) {
        this.j = j;
    }
    
    public final void j(final c c, final b b) {
        for (int l = this.l, i = 0; i < l; ++i) {
            this.k[i].B(c, b, false);
        }
        this.l = 0;
    }
    
    @Override
    public String toString() {
        StringBuilder sb;
        if (this.b != null) {
            sb = new StringBuilder();
            sb.append("");
            sb.append(this.b);
        }
        else {
            sb = new StringBuilder();
            sb.append("");
            sb.append(this.c);
        }
        return sb.toString();
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        CONSTANT, 
        ERROR, 
        SLACK, 
        UNKNOWN, 
        UNRESTRICTED;
    }
}

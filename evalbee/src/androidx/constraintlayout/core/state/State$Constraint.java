// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

public enum State$Constraint
{
    private static final State$Constraint[] $VALUES;
    
    BASELINE_TO_BASELINE, 
    BASELINE_TO_BOTTOM, 
    BASELINE_TO_TOP, 
    BOTTOM_TO_BOTTOM, 
    BOTTOM_TO_TOP, 
    CENTER_HORIZONTALLY, 
    CENTER_VERTICALLY, 
    CIRCULAR_CONSTRAINT, 
    END_TO_END, 
    END_TO_START, 
    LEFT_TO_LEFT, 
    LEFT_TO_RIGHT, 
    RIGHT_TO_LEFT, 
    RIGHT_TO_RIGHT, 
    START_TO_END, 
    START_TO_START, 
    TOP_TO_BOTTOM, 
    TOP_TO_TOP;
}

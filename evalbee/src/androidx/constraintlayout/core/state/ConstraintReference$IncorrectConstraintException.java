// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import java.util.ArrayList;

class ConstraintReference$IncorrectConstraintException extends Exception
{
    private final ArrayList<String> mErrors;
    
    public ConstraintReference$IncorrectConstraintException(final ArrayList<String> mErrors) {
        this.mErrors = mErrors;
    }
    
    public ArrayList<String> getErrors() {
        return this.mErrors;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IncorrectConstraintException: ");
        sb.append(this.mErrors.toString());
        return sb.toString();
    }
}

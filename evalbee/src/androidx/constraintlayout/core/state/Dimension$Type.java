// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

public enum Dimension$Type
{
    private static final Dimension$Type[] $VALUES;
    
    FIXED, 
    MATCH_CONSTRAINT, 
    MATCH_PARENT, 
    WRAP;
}

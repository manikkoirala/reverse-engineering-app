// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

public enum CustomAttribute$AttributeType
{
    private static final CustomAttribute$AttributeType[] $VALUES;
    
    BOOLEAN_TYPE, 
    COLOR_DRAWABLE_TYPE, 
    COLOR_TYPE, 
    DIMENSION_TYPE, 
    FLOAT_TYPE, 
    INT_TYPE, 
    REFERENCE_TYPE, 
    STRING_TYPE;
}

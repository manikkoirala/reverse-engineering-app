// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.util.ArrayList;

public class b implements c.a
{
    public SolverVariable a;
    public float b;
    public boolean c;
    public ArrayList d;
    public a e;
    public boolean f;
    
    public b() {
        this.a = null;
        this.b = 0.0f;
        this.c = false;
        this.d = new ArrayList();
        this.f = false;
    }
    
    public b(final oe oe) {
        this.a = null;
        this.b = 0.0f;
        this.c = false;
        this.d = new ArrayList();
        this.f = false;
        this.e = (a)new androidx.constraintlayout.core.a(this, oe);
    }
    
    public void A(final c c, final SolverVariable solverVariable, final boolean b) {
        if (solverVariable != null) {
            if (solverVariable.g) {
                this.b += solverVariable.f * this.e.d(solverVariable);
                this.e.g(solverVariable, b);
                if (b) {
                    solverVariable.e(this);
                }
                if (c.t && this.e.h() == 0) {
                    this.f = true;
                    c.a = true;
                }
            }
        }
    }
    
    public void B(final c c, final b b, final boolean b2) {
        this.b += b.b * this.e.i(b, b2);
        if (b2) {
            b.a.e(this);
        }
        if (c.t && this.a != null && this.e.h() == 0) {
            this.f = true;
            c.a = true;
        }
    }
    
    public void C(final c c, final SolverVariable solverVariable, final boolean b) {
        if (solverVariable != null) {
            if (solverVariable.n) {
                final float d = this.e.d(solverVariable);
                this.b += solverVariable.q * d;
                this.e.g(solverVariable, b);
                if (b) {
                    solverVariable.e(this);
                }
                this.e.f(c.n.d[solverVariable.p], d, b);
                if (c.t && this.e.h() == 0) {
                    this.f = true;
                    c.a = true;
                }
            }
        }
    }
    
    public void D(final c c) {
        if (c.g.length == 0) {
            return;
        }
        int i = 0;
        while (i == 0) {
            for (int h = this.e.h(), j = 0; j < h; ++j) {
                final SolverVariable b = this.e.b(j);
                if (b.d != -1 || b.g || b.n) {
                    this.d.add(b);
                }
            }
            final int size = this.d.size();
            if (size > 0) {
                for (int k = 0; k < size; ++k) {
                    final SolverVariable solverVariable = this.d.get(k);
                    if (solverVariable.g) {
                        this.A(c, solverVariable, true);
                    }
                    else if (solverVariable.n) {
                        this.C(c, solverVariable, true);
                    }
                    else {
                        this.B(c, c.g[solverVariable.d], true);
                    }
                }
                this.d.clear();
            }
            else {
                i = 1;
            }
        }
        if (c.t && this.a != null && this.e.h() == 0) {
            this.f = true;
            c.a = true;
        }
    }
    
    @Override
    public SolverVariable a(final c c, final boolean[] array) {
        return this.w(array, null);
    }
    
    @Override
    public void b(final c.a a) {
        if (a instanceof b) {
            final b b = (b)a;
            this.a = null;
            this.e.clear();
            for (int i = 0; i < b.e.h(); ++i) {
                this.e.f(b.e.b(i), b.e.j(i), true);
            }
        }
    }
    
    @Override
    public void c(final SolverVariable solverVariable) {
        final int e = solverVariable.e;
        float n = 1.0f;
        if (e != 1) {
            if (e == 2) {
                n = 1000.0f;
            }
            else if (e == 3) {
                n = 1000000.0f;
            }
            else if (e == 4) {
                n = 1.0E9f;
            }
            else if (e == 5) {
                n = 1.0E12f;
            }
        }
        this.e.c(solverVariable, n);
    }
    
    @Override
    public void clear() {
        this.e.clear();
        this.a = null;
        this.b = 0.0f;
    }
    
    public b d(final c c, final int n) {
        this.e.c(c.o(n, "ep"), 1.0f);
        this.e.c(c.o(n, "em"), -1.0f);
        return this;
    }
    
    public b e(final SolverVariable solverVariable, final int n) {
        this.e.c(solverVariable, (float)n);
        return this;
    }
    
    public boolean f(final c c) {
        final SolverVariable g = this.g(c);
        boolean b;
        if (g == null) {
            b = true;
        }
        else {
            this.x(g);
            b = false;
        }
        if (this.e.h() == 0) {
            this.f = true;
        }
        return b;
    }
    
    public SolverVariable g(final c c) {
        final int h = this.e.h();
        SolverVariable solverVariable = null;
        float n = 0.0f;
        float n2 = 0.0f;
        int i = 0;
        int n4;
        int n3 = n4 = 0;
        SolverVariable solverVariable2 = null;
        while (i < h) {
            final float j = this.e.j(i);
            final SolverVariable b = this.e.b(i);
            SolverVariable solverVariable3;
            SolverVariable solverVariable4;
            int u;
            int u2;
            float n5;
            float n6;
            if (b.j == SolverVariable.Type.UNRESTRICTED) {
                if (solverVariable != null && n <= j) {
                    solverVariable3 = solverVariable;
                    solverVariable4 = solverVariable2;
                    u = n3;
                    u2 = n4;
                    n5 = n;
                    n6 = n2;
                    if (n3 == 0) {
                        solverVariable3 = solverVariable;
                        solverVariable4 = solverVariable2;
                        u = n3;
                        u2 = n4;
                        n5 = n;
                        n6 = n2;
                        if (this.u(b, c)) {
                            u = 1;
                            solverVariable3 = b;
                            solverVariable4 = solverVariable2;
                            u2 = n4;
                            n5 = j;
                            n6 = n2;
                        }
                    }
                }
                else {
                    u = (this.u(b, c) ? 1 : 0);
                    solverVariable3 = b;
                    solverVariable4 = solverVariable2;
                    u2 = n4;
                    n5 = j;
                    n6 = n2;
                }
            }
            else {
                solverVariable3 = solverVariable;
                solverVariable4 = solverVariable2;
                u = n3;
                u2 = n4;
                n5 = n;
                n6 = n2;
                if (solverVariable == null) {
                    solverVariable3 = solverVariable;
                    solverVariable4 = solverVariable2;
                    u = n3;
                    u2 = n4;
                    n5 = n;
                    n6 = n2;
                    if (j < 0.0f) {
                        if (solverVariable2 != null && n2 <= j) {
                            solverVariable3 = solverVariable;
                            solverVariable4 = solverVariable2;
                            u = n3;
                            u2 = n4;
                            n5 = n;
                            n6 = n2;
                            if (n4 == 0) {
                                solverVariable3 = solverVariable;
                                solverVariable4 = solverVariable2;
                                u = n3;
                                u2 = n4;
                                n5 = n;
                                n6 = n2;
                                if (this.u(b, c)) {
                                    u2 = 1;
                                    n6 = j;
                                    n5 = n;
                                    u = n3;
                                    solverVariable4 = b;
                                    solverVariable3 = solverVariable;
                                }
                            }
                        }
                        else {
                            u2 = (this.u(b, c) ? 1 : 0);
                            solverVariable3 = solverVariable;
                            solverVariable4 = b;
                            u = n3;
                            n5 = n;
                            n6 = j;
                        }
                    }
                }
            }
            ++i;
            solverVariable = solverVariable3;
            solverVariable2 = solverVariable4;
            n3 = u;
            n4 = u2;
            n = n5;
            n2 = n6;
        }
        if (solverVariable != null) {
            return solverVariable;
        }
        return solverVariable2;
    }
    
    @Override
    public SolverVariable getKey() {
        return this.a;
    }
    
    public b h(final SolverVariable solverVariable, final SolverVariable solverVariable2, int n, float b, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n2) {
        if (solverVariable2 == solverVariable3) {
            this.e.c(solverVariable, 1.0f);
            this.e.c(solverVariable4, 1.0f);
            this.e.c(solverVariable2, -2.0f);
            return this;
        }
        Label_0155: {
            if (b == 0.5f) {
                this.e.c(solverVariable, 1.0f);
                this.e.c(solverVariable2, -1.0f);
                this.e.c(solverVariable3, -1.0f);
                this.e.c(solverVariable4, 1.0f);
                if (n <= 0 && n2 <= 0) {
                    return this;
                }
                n = -n + n2;
            }
            else {
                if (b <= 0.0f) {
                    this.e.c(solverVariable, -1.0f);
                    this.e.c(solverVariable2, 1.0f);
                    b = (float)n;
                    break Label_0155;
                }
                if (b >= 1.0f) {
                    this.e.c(solverVariable4, -1.0f);
                    this.e.c(solverVariable3, 1.0f);
                    n = -n2;
                }
                else {
                    final a e = this.e;
                    final float n3 = 1.0f - b;
                    e.c(solverVariable, n3 * 1.0f);
                    this.e.c(solverVariable2, n3 * -1.0f);
                    this.e.c(solverVariable3, -1.0f * b);
                    this.e.c(solverVariable4, 1.0f * b);
                    if (n > 0 || n2 > 0) {
                        b = -n * n3 + n2 * b;
                        break Label_0155;
                    }
                    return this;
                }
            }
            b = (float)n;
        }
        this.b = b;
        return this;
    }
    
    public b i(final SolverVariable a, final int n) {
        this.a = a;
        final float n2 = (float)n;
        a.f = n2;
        this.b = n2;
        this.f = true;
        return this;
    }
    
    @Override
    public boolean isEmpty() {
        return this.a == null && this.b == 0.0f && this.e.h() == 0;
    }
    
    public b j(final SolverVariable solverVariable, final SolverVariable solverVariable2, final float n) {
        this.e.c(solverVariable, -1.0f);
        this.e.c(solverVariable2, n);
        return this;
    }
    
    public b k(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n) {
        this.e.c(solverVariable, -1.0f);
        this.e.c(solverVariable2, 1.0f);
        this.e.c(solverVariable3, n);
        this.e.c(solverVariable4, -n);
        return this;
    }
    
    public b l(float n, final float n2, final float n3, final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4) {
        this.b = 0.0f;
        if (n2 != 0.0f && n != n3) {
            if (n == 0.0f) {
                this.e.c(solverVariable, 1.0f);
                this.e.c(solverVariable2, -1.0f);
            }
            else if (n3 == 0.0f) {
                this.e.c(solverVariable3, 1.0f);
                this.e.c(solverVariable4, -1.0f);
            }
            else {
                n = n / n2 / (n3 / n2);
                this.e.c(solverVariable, 1.0f);
                this.e.c(solverVariable2, -1.0f);
                this.e.c(solverVariable4, n);
                this.e.c(solverVariable3, -n);
            }
        }
        else {
            this.e.c(solverVariable, 1.0f);
            this.e.c(solverVariable2, -1.0f);
            this.e.c(solverVariable4, 1.0f);
            this.e.c(solverVariable3, -1.0f);
        }
        return this;
    }
    
    public b m(final SolverVariable solverVariable, final int n) {
        a a;
        float n2;
        if (n < 0) {
            this.b = (float)(n * -1);
            a = this.e;
            n2 = 1.0f;
        }
        else {
            this.b = (float)n;
            a = this.e;
            n2 = -1.0f;
        }
        a.c(solverVariable, n2);
        return this;
    }
    
    public b n(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n) {
        int n2 = 0;
        final int n3 = 0;
        if (n != 0) {
            n2 = n3;
            int n4;
            if ((n4 = n) < 0) {
                n4 = n * -1;
                n2 = 1;
            }
            this.b = (float)n4;
        }
        if (n2 == 0) {
            this.e.c(solverVariable, -1.0f);
            this.e.c(solverVariable2, 1.0f);
        }
        else {
            this.e.c(solverVariable, 1.0f);
            this.e.c(solverVariable2, -1.0f);
        }
        return this;
    }
    
    public b o(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final int n) {
        int n2 = 0;
        final int n3 = 0;
        if (n != 0) {
            n2 = n3;
            int n4;
            if ((n4 = n) < 0) {
                n4 = n * -1;
                n2 = 1;
            }
            this.b = (float)n4;
        }
        if (n2 == 0) {
            this.e.c(solverVariable, -1.0f);
            this.e.c(solverVariable2, 1.0f);
            this.e.c(solverVariable3, 1.0f);
        }
        else {
            this.e.c(solverVariable, 1.0f);
            this.e.c(solverVariable2, -1.0f);
            this.e.c(solverVariable3, -1.0f);
        }
        return this;
    }
    
    public b p(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final int n) {
        int n2 = 0;
        final int n3 = 0;
        if (n != 0) {
            n2 = n3;
            int n4;
            if ((n4 = n) < 0) {
                n4 = n * -1;
                n2 = 1;
            }
            this.b = (float)n4;
        }
        if (n2 == 0) {
            this.e.c(solverVariable, -1.0f);
            this.e.c(solverVariable2, 1.0f);
            this.e.c(solverVariable3, -1.0f);
        }
        else {
            this.e.c(solverVariable, 1.0f);
            this.e.c(solverVariable2, -1.0f);
            this.e.c(solverVariable3, 1.0f);
        }
        return this;
    }
    
    public b q(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n) {
        this.e.c(solverVariable3, 0.5f);
        this.e.c(solverVariable4, 0.5f);
        this.e.c(solverVariable, -0.5f);
        this.e.c(solverVariable2, -0.5f);
        this.b = -n;
        return this;
    }
    
    public void r() {
        final float b = this.b;
        if (b < 0.0f) {
            this.b = b * -1.0f;
            this.e.invert();
        }
    }
    
    public boolean s() {
        final SolverVariable a = this.a;
        return a != null && (a.j == SolverVariable.Type.UNRESTRICTED || this.b >= 0.0f);
    }
    
    public boolean t(final SolverVariable solverVariable) {
        return this.e.a(solverVariable);
    }
    
    @Override
    public String toString() {
        return this.z();
    }
    
    public final boolean u(final SolverVariable solverVariable, final c c) {
        final int m = solverVariable.m;
        boolean b = true;
        if (m > 1) {
            b = false;
        }
        return b;
    }
    
    public SolverVariable v(final SolverVariable solverVariable) {
        return this.w(null, solverVariable);
    }
    
    public final SolverVariable w(final boolean[] array, final SolverVariable solverVariable) {
        final int h = this.e.h();
        SolverVariable solverVariable2 = null;
        int i = 0;
        float n = 0.0f;
        while (i < h) {
            final float j = this.e.j(i);
            SolverVariable solverVariable3 = solverVariable2;
            float n2 = n;
            Label_0152: {
                if (j < 0.0f) {
                    final SolverVariable b = this.e.b(i);
                    if (array != null) {
                        solverVariable3 = solverVariable2;
                        n2 = n;
                        if (array[b.c]) {
                            break Label_0152;
                        }
                    }
                    solverVariable3 = solverVariable2;
                    n2 = n;
                    if (b != solverVariable) {
                        final SolverVariable.Type k = b.j;
                        if (k != SolverVariable.Type.SLACK) {
                            solverVariable3 = solverVariable2;
                            n2 = n;
                            if (k != SolverVariable.Type.ERROR) {
                                break Label_0152;
                            }
                        }
                        solverVariable3 = solverVariable2;
                        n2 = n;
                        if (j < n) {
                            n2 = j;
                            solverVariable3 = b;
                        }
                    }
                }
            }
            ++i;
            solverVariable2 = solverVariable3;
            n = n2;
        }
        return solverVariable2;
    }
    
    public void x(final SolverVariable a) {
        final SolverVariable a2 = this.a;
        if (a2 != null) {
            this.e.c(a2, -1.0f);
            this.a.d = -1;
            this.a = null;
        }
        final float n = this.e.g(a, true) * -1.0f;
        this.a = a;
        if (n == 1.0f) {
            return;
        }
        this.b /= n;
        this.e.e(n);
    }
    
    public void y() {
        this.a = null;
        this.e.clear();
        this.b = 0.0f;
        this.f = false;
    }
    
    public String z() {
        StringBuilder sb;
        if (this.a == null) {
            sb = new StringBuilder();
            sb.append("");
            sb.append("0");
        }
        else {
            sb = new StringBuilder();
            sb.append("");
            sb.append(this.a);
        }
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(" = ");
        String str = sb2.toString();
        final float b = this.b;
        int i = 0;
        int n;
        if (b != 0.0f) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(this.b);
            str = sb3.toString();
            n = 1;
        }
        else {
            n = 0;
        }
        while (i < this.e.h()) {
            final SolverVariable b2 = this.e.b(i);
            if (b2 != null) {
                final float j = this.e.j(i);
                final float n2 = fcmpl(j, 0.0f);
                if (n2 != 0) {
                    final String string2 = b2.toString();
                    String s = null;
                    float f = 0.0f;
                    Label_0348: {
                        StringBuilder sb4;
                        String str2;
                        if (n == 0) {
                            s = str;
                            f = j;
                            if (j >= 0.0f) {
                                break Label_0348;
                            }
                            sb4 = new StringBuilder();
                            sb4.append(str);
                            str2 = "- ";
                        }
                        else {
                            sb4 = new(java.lang.StringBuilder.class)();
                            if (n2 > 0) {
                                new StringBuilder();
                                sb4.append(str);
                                sb4.append(" + ");
                                s = sb4.toString();
                                f = j;
                                break Label_0348;
                            }
                            new StringBuilder();
                            sb4.append(str);
                            str2 = " - ";
                        }
                        sb4.append(str2);
                        s = sb4.toString();
                        f = j * -1.0f;
                    }
                    StringBuilder sb5;
                    if (f == 1.0f) {
                        sb5 = new StringBuilder();
                    }
                    else {
                        sb5 = new StringBuilder();
                        sb5.append(s);
                        sb5.append(f);
                        s = " ";
                    }
                    sb5.append(s);
                    sb5.append(string2);
                    str = sb5.toString();
                    n = 1;
                }
            }
            ++i;
        }
        String string3 = str;
        if (n == 0) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(str);
            sb6.append("0.0");
            string3 = sb6.toString();
        }
        return string3;
    }
    
    public interface a
    {
        boolean a(final SolverVariable p0);
        
        SolverVariable b(final int p0);
        
        void c(final SolverVariable p0, final float p1);
        
        void clear();
        
        float d(final SolverVariable p0);
        
        void e(final float p0);
        
        void f(final SolverVariable p0, final float p1, final boolean p2);
        
        float g(final SolverVariable p0, final boolean p1);
        
        int h();
        
        float i(final b p0, final boolean p1);
        
        void invert();
        
        float j(final int p0);
    }
}

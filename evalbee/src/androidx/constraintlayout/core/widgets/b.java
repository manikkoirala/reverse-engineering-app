// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.ArrayList;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.c;

public abstract class b
{
    public static void a(final d d, final c c, int n, int f, final androidx.constraintlayout.core.widgets.c c2) {
        final ConstraintWidget a = c2.a;
        final ConstraintWidget c3 = c2.c;
        final ConstraintWidget b = c2.b;
        final ConstraintWidget d2 = c2.d;
        final ConstraintWidget e = c2.e;
        final float k = c2.k;
        final boolean b2 = d.Z[n] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean b3 = false;
        int n6 = 0;
        int n7 = 0;
        Label_0195: {
            int n4 = 0;
            int n5 = 0;
            Label_0184: {
                int n2;
                int n3;
                if (n == 0) {
                    final int z0 = e.z0;
                    if (z0 == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (z0 == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (z0 != 2) {
                        break Label_0184;
                    }
                }
                else {
                    final int a2 = e.A0;
                    if (a2 == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (a2 == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (a2 != 2) {
                        break Label_0184;
                    }
                }
                b3 = true;
                n6 = n2;
                n7 = n3;
                break Label_0195;
            }
            b3 = false;
            n7 = n5;
            n6 = n4;
        }
        int n8 = 0;
        ConstraintWidget constraintWidget = a;
        SolverVariable solverVariable;
        while (true) {
            solverVariable = null;
            final ConstraintWidget constraintWidget2 = null;
            if (n8 != 0) {
                break;
            }
            final ConstraintAnchor constraintAnchor = constraintWidget.W[f];
            int n9;
            if (b3) {
                n9 = 1;
            }
            else {
                n9 = 4;
            }
            final int f2 = constraintAnchor.f();
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour = constraintWidget.Z[n];
            final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            final boolean b4 = dimensionBehaviour == match_CONSTRAINT && constraintWidget.y[n] == 0;
            final ConstraintAnchor f3 = constraintAnchor.f;
            int n10 = f2;
            if (f3 != null) {
                n10 = f2;
                if (constraintWidget != a) {
                    n10 = f2 + f3.f();
                }
            }
            int n11;
            if (b3 && constraintWidget != a && constraintWidget != b) {
                n11 = 8;
            }
            else {
                n11 = n9;
            }
            final ConstraintAnchor f4 = constraintAnchor.f;
            if (f4 != null) {
                if (constraintWidget == b) {
                    c.h(constraintAnchor.i, f4.i, n10, 6);
                }
                else {
                    c.h(constraintAnchor.i, f4.i, n10, 8);
                }
                int n12 = n11;
                if (b4) {
                    n12 = n11;
                    if (!b3) {
                        n12 = 5;
                    }
                }
                if (constraintWidget == b && b3 && constraintWidget.h0(n)) {
                    n12 = 5;
                }
                c.e(constraintAnchor.i, constraintAnchor.f.i, n10, n12);
            }
            if (b2) {
                if (constraintWidget.V() != 8 && constraintWidget.Z[n] == match_CONSTRAINT) {
                    final ConstraintAnchor[] w = constraintWidget.W;
                    c.h(w[f + 1].i, w[f].i, 0, 5);
                }
                c.h(constraintWidget.W[f].i, d.W[f].i, 0, 8);
            }
            final ConstraintAnchor f5 = constraintWidget.W[f + 1].f;
            ConstraintWidget constraintWidget3 = constraintWidget2;
            if (f5 != null) {
                final ConstraintWidget d3 = f5.d;
                final ConstraintAnchor f6 = d3.W[f].f;
                constraintWidget3 = constraintWidget2;
                if (f6 != null) {
                    if (f6.d != constraintWidget) {
                        constraintWidget3 = constraintWidget2;
                    }
                    else {
                        constraintWidget3 = d3;
                    }
                }
            }
            if (constraintWidget3 != null) {
                constraintWidget = constraintWidget3;
            }
            else {
                n8 = 1;
            }
        }
        if (d2 != null) {
            final ConstraintAnchor[] w2 = c3.W;
            final int n13 = f + 1;
            if (w2[n13].f != null) {
                final ConstraintAnchor constraintAnchor2 = d2.W[n13];
                Label_0817: {
                    if (d2.Z[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && d2.y[n] == 0 && !b3) {
                        final ConstraintAnchor f7 = constraintAnchor2.f;
                        if (f7.d == d) {
                            c.e(constraintAnchor2.i, f7.i, -constraintAnchor2.f(), 5);
                            break Label_0817;
                        }
                    }
                    if (b3) {
                        final ConstraintAnchor f8 = constraintAnchor2.f;
                        if (f8.d == d) {
                            c.e(constraintAnchor2.i, f8.i, -constraintAnchor2.f(), 4);
                        }
                    }
                }
                c.j(constraintAnchor2.i, c3.W[n13].f.i, -constraintAnchor2.f(), 6);
            }
        }
        if (b2) {
            final ConstraintAnchor[] w3 = d.W;
            final int n14 = f + 1;
            final SolverVariable i = w3[n14].i;
            final ConstraintAnchor constraintAnchor3 = c3.W[n14];
            c.h(i, constraintAnchor3.i, constraintAnchor3.f(), 8);
        }
        final ArrayList h = c2.h;
        if (h != null) {
            final int size = h.size();
            if (size > 1) {
                float n15;
                if (c2.r && !c2.t) {
                    n15 = (float)c2.j;
                }
                else {
                    n15 = k;
                }
                float n16 = 0.0f;
                ConstraintWidget constraintWidget4 = null;
                float n17;
                for (int j = 0; j < size; ++j, n16 = n17) {
                    final ConstraintWidget constraintWidget5 = h.get(j);
                    n17 = constraintWidget5.D0[n];
                    Label_1082: {
                        if (n17 < 0.0f) {
                            if (c2.t) {
                                final ConstraintAnchor[] w4 = constraintWidget5.W;
                                c.e(w4[f + 1].i, w4[f].i, 0, 4);
                                break Label_1082;
                            }
                            n17 = 1.0f;
                        }
                        if (n17 != 0.0f) {
                            if (constraintWidget4 != null) {
                                final ConstraintAnchor[] w5 = constraintWidget4.W;
                                final SolverVariable l = w5[f].i;
                                final int n18 = f + 1;
                                final SolverVariable m = w5[n18].i;
                                final ConstraintAnchor[] w6 = constraintWidget5.W;
                                final SolverVariable i2 = w6[f].i;
                                final SolverVariable i3 = w6[n18].i;
                                final androidx.constraintlayout.core.b r = c.r();
                                r.l(n16, n15, n17, l, m, i2, i3);
                                c.d(r);
                            }
                            constraintWidget4 = constraintWidget5;
                            continue;
                        }
                        final ConstraintAnchor[] w7 = constraintWidget5.W;
                        c.e(w7[f + 1].i, w7[f].i, 0, 8);
                    }
                    n17 = n16;
                }
            }
        }
        if (b != null && (b == d2 || b3)) {
            final ConstraintAnchor constraintAnchor4 = a.W[f];
            final ConstraintAnchor[] w8 = c3.W;
            final int n19 = f + 1;
            ConstraintAnchor constraintAnchor5 = w8[n19];
            final ConstraintAnchor f9 = constraintAnchor4.f;
            SolverVariable i4;
            if (f9 != null) {
                i4 = f9.i;
            }
            else {
                i4 = null;
            }
            final ConstraintAnchor f10 = constraintAnchor5.f;
            SolverVariable i5;
            if (f10 != null) {
                i5 = f10.i;
            }
            else {
                i5 = null;
            }
            final ConstraintAnchor constraintAnchor6 = b.W[f];
            if (d2 != null) {
                constraintAnchor5 = d2.W[n19];
            }
            if (i4 != null && i5 != null) {
                float n20;
                if (n == 0) {
                    n20 = e.o0;
                }
                else {
                    n20 = e.p0;
                }
                final int f11 = constraintAnchor6.f();
                n = constraintAnchor5.f();
                c.c(constraintAnchor6.i, i4, f11, n20, i5, constraintAnchor5.i, n, 7);
            }
        }
        else if (n6 != 0 && b != null) {
            final int j2 = c2.j;
            final boolean b5 = j2 > 0 && c2.i == j2;
            ConstraintWidget constraintWidget7;
            ConstraintWidget constraintWidget8;
            for (ConstraintWidget constraintWidget6 = constraintWidget7 = b; constraintWidget7 != null; constraintWidget7 = constraintWidget8) {
                for (constraintWidget8 = constraintWidget7.F0[n]; constraintWidget8 != null && constraintWidget8.V() == 8; constraintWidget8 = constraintWidget8.F0[n]) {}
                if (constraintWidget8 != null || constraintWidget7 == d2) {
                    final ConstraintAnchor constraintAnchor7 = constraintWidget7.W[f];
                    final SolverVariable i6 = constraintAnchor7.i;
                    final ConstraintAnchor f12 = constraintAnchor7.f;
                    SolverVariable solverVariable2;
                    if (f12 != null) {
                        solverVariable2 = f12.i;
                    }
                    else {
                        solverVariable2 = null;
                    }
                    Label_1589: {
                        ConstraintAnchor f13;
                        if (constraintWidget6 != constraintWidget7) {
                            f13 = constraintWidget6.W[f + 1];
                        }
                        else {
                            if (constraintWidget7 != b) {
                                break Label_1589;
                            }
                            f13 = a.W[f].f;
                            if (f13 == null) {
                                solverVariable2 = null;
                                break Label_1589;
                            }
                        }
                        solverVariable2 = f13.i;
                    }
                    final int f14 = constraintAnchor7.f();
                    final ConstraintAnchor[] w9 = constraintWidget7.W;
                    final int n21 = f + 1;
                    final int f15 = w9[n21].f();
                    ConstraintAnchor f16 = null;
                    SolverVariable solverVariable3 = null;
                    Label_1676: {
                        ConstraintAnchor constraintAnchor8;
                        if (constraintWidget8 != null) {
                            constraintAnchor8 = constraintWidget8.W[f];
                        }
                        else {
                            f16 = c3.W[n21].f;
                            if (f16 == null) {
                                solverVariable3 = null;
                                break Label_1676;
                            }
                            constraintAnchor8 = f16;
                        }
                        final SolverVariable i7 = constraintAnchor8.i;
                        f16 = constraintAnchor8;
                        solverVariable3 = i7;
                    }
                    final SolverVariable i8 = constraintWidget7.W[n21].i;
                    int f17 = f15;
                    if (f16 != null) {
                        f17 = f15 + f16.f();
                    }
                    int f18 = f14 + constraintWidget6.W[n21].f();
                    if (i6 != null && solverVariable2 != null && solverVariable3 != null && i8 != null) {
                        if (constraintWidget7 == b) {
                            f18 = b.W[f].f();
                        }
                        if (constraintWidget7 == d2) {
                            f17 = d2.W[n21].f();
                        }
                        int n22;
                        if (b5) {
                            n22 = 8;
                        }
                        else {
                            n22 = 5;
                        }
                        c.c(i6, solverVariable2, f18, 0.5f, solverVariable3, i8, f17, n22);
                    }
                }
                if (constraintWidget7.V() == 8) {
                    constraintWidget7 = constraintWidget6;
                }
                constraintWidget6 = constraintWidget7;
            }
        }
        else {
            int n23 = 8;
            if (n7 != 0 && b != null) {
                final int j3 = c2.j;
                final boolean b6 = j3 > 0 && c2.i == j3;
                ConstraintWidget constraintWidget10;
                ConstraintWidget constraintWidget11;
                for (ConstraintWidget constraintWidget9 = constraintWidget10 = b; constraintWidget10 != null; constraintWidget10 = constraintWidget11) {
                    for (constraintWidget11 = constraintWidget10.F0[n]; constraintWidget11 != null && constraintWidget11.V() == n23; constraintWidget11 = constraintWidget11.F0[n]) {}
                    if (constraintWidget10 != b && constraintWidget10 != d2 && constraintWidget11 != null) {
                        if (constraintWidget11 == d2) {
                            constraintWidget11 = null;
                        }
                        final ConstraintAnchor constraintAnchor9 = constraintWidget10.W[f];
                        final SolverVariable i9 = constraintAnchor9.i;
                        final ConstraintAnchor f19 = constraintAnchor9.f;
                        if (f19 != null) {
                            final SolverVariable i10 = f19.i;
                        }
                        final ConstraintAnchor[] w10 = constraintWidget9.W;
                        final int n24 = f + 1;
                        final SolverVariable i11 = w10[n24].i;
                        final int f20 = constraintAnchor9.f();
                        final int f21 = constraintWidget10.W[n24].f();
                        ConstraintAnchor constraintAnchor10;
                        SolverVariable i12;
                        SolverVariable i14;
                        if (constraintWidget11 != null) {
                            constraintAnchor10 = constraintWidget11.W[f];
                            i12 = constraintAnchor10.i;
                            final ConstraintAnchor f22 = constraintAnchor10.f;
                            SolverVariable i13;
                            if (f22 != null) {
                                i13 = f22.i;
                            }
                            else {
                                i13 = null;
                            }
                            i14 = i13;
                        }
                        else {
                            constraintAnchor10 = d2.W[f];
                            SolverVariable i15;
                            if (constraintAnchor10 != null) {
                                i15 = constraintAnchor10.i;
                            }
                            else {
                                i15 = null;
                            }
                            i14 = constraintWidget10.W[n24].i;
                            i12 = i15;
                        }
                        int n25 = f21;
                        if (constraintAnchor10 != null) {
                            n25 = f21 + constraintAnchor10.f();
                        }
                        final int f23 = constraintWidget9.W[n24].f();
                        int n26;
                        if (b6) {
                            n26 = 8;
                        }
                        else {
                            n26 = 4;
                        }
                        if (i9 != null && i11 != null && i12 != null && i14 != null) {
                            c.c(i9, i11, f23 + f20, 0.5f, i12, i14, n25, n26);
                        }
                        n23 = 8;
                    }
                    if (constraintWidget10.V() != n23) {
                        constraintWidget9 = constraintWidget10;
                    }
                }
                final ConstraintAnchor constraintAnchor11 = b.W[f];
                final ConstraintAnchor f24 = a.W[f].f;
                final ConstraintAnchor[] w11 = d2.W;
                n = f + 1;
                final ConstraintAnchor constraintAnchor12 = w11[n];
                final ConstraintAnchor f25 = c3.W[n].f;
                if (f24 != null) {
                    if (b != d2) {
                        c.e(constraintAnchor11.i, f24.i, constraintAnchor11.f(), 5);
                    }
                    else if (f25 != null) {
                        c.c(constraintAnchor11.i, f24.i, constraintAnchor11.f(), 0.5f, constraintAnchor12.i, f25.i, constraintAnchor12.f(), 5);
                    }
                }
                if (f25 != null && b != d2) {
                    c.e(constraintAnchor12.i, f25.i, -constraintAnchor12.f(), 5);
                }
            }
        }
        if ((n6 != 0 || n7 != 0) && b != null && b != d2) {
            final ConstraintAnchor[] w12 = b.W;
            final ConstraintAnchor constraintAnchor13 = w12[f];
            ConstraintWidget constraintWidget12;
            if ((constraintWidget12 = d2) == null) {
                constraintWidget12 = b;
            }
            final ConstraintAnchor[] w13 = constraintWidget12.W;
            ++f;
            ConstraintAnchor constraintAnchor14 = w13[f];
            final ConstraintAnchor f26 = constraintAnchor13.f;
            SolverVariable i16;
            if (f26 != null) {
                i16 = f26.i;
            }
            else {
                i16 = null;
            }
            final ConstraintAnchor f27 = constraintAnchor14.f;
            SolverVariable solverVariable4;
            if (f27 != null) {
                solverVariable4 = f27.i;
            }
            else {
                solverVariable4 = null;
            }
            if (c3 != constraintWidget12) {
                final ConstraintAnchor f28 = c3.W[f].f;
                solverVariable4 = solverVariable;
                if (f28 != null) {
                    solverVariable4 = f28.i;
                }
            }
            if (b == constraintWidget12) {
                constraintAnchor14 = w12[f];
            }
            if (i16 != null && solverVariable4 != null) {
                n = constraintAnchor13.f();
                f = constraintWidget12.W[f].f();
                c.c(constraintAnchor13.i, i16, n, 0.5f, solverVariable4, constraintAnchor14.i, f, 5);
            }
        }
    }
    
    public static void b(final d d, final c c, final ArrayList list, final int n) {
        int i = 0;
        int n2;
        androidx.constraintlayout.core.widgets.c[] array;
        int n3;
        if (n == 0) {
            n2 = d.W0;
            array = d.Z0;
            n3 = 0;
        }
        else {
            n2 = d.X0;
            array = d.Y0;
            n3 = 2;
        }
        while (i < n2) {
            final androidx.constraintlayout.core.widgets.c c2 = array[i];
            c2.a();
            if (list == null || list.contains(c2.a)) {
                a(d, c, n, n3, c2);
            }
            ++i;
        }
    }
}

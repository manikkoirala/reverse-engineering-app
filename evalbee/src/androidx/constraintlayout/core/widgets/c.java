// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.ArrayList;

public class c
{
    public ConstraintWidget a;
    public ConstraintWidget b;
    public ConstraintWidget c;
    public ConstraintWidget d;
    public ConstraintWidget e;
    public ConstraintWidget f;
    public ConstraintWidget g;
    public ArrayList h;
    public int i;
    public int j;
    public float k;
    public int l;
    public int m;
    public int n;
    public boolean o;
    public int p;
    public boolean q;
    public boolean r;
    public boolean s;
    public boolean t;
    public boolean u;
    public boolean v;
    
    public c(final ConstraintWidget a, final int p3, final boolean q) {
        this.k = 0.0f;
        this.a = a;
        this.p = p3;
        this.q = q;
    }
    
    public static boolean c(final ConstraintWidget constraintWidget, int n) {
        if (constraintWidget.V() != 8 && constraintWidget.Z[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            n = constraintWidget.y[n];
            if (n == 0 || n == 3) {
                return true;
            }
        }
        return false;
    }
    
    public void a() {
        if (!this.v) {
            this.b();
        }
        this.v = true;
    }
    
    public final void b() {
        final int n = this.p * 2;
        ConstraintWidget a = this.a;
        boolean t = true;
        this.o = true;
        ConstraintWidget constraintWidget = a;
        int i = 0;
        while (i == 0) {
            ++this.i;
            final ConstraintWidget[] f0 = a.F0;
            final int p = this.p;
            final ConstraintWidget constraintWidget2 = null;
            f0[p] = null;
            a.E0[p] = null;
            if (a.V() != 8) {
                ++this.l;
                final ConstraintWidget.DimensionBehaviour u = a.u(this.p);
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (u != match_CONSTRAINT) {
                    this.m += a.E(this.p);
                }
                final int m = this.m + a.W[n].f();
                this.m = m;
                final ConstraintAnchor[] w = a.W;
                final int n2 = n + 1;
                this.m = m + w[n2].f();
                final int n3 = this.n + a.W[n].f();
                this.n = n3;
                this.n = n3 + a.W[n2].f();
                if (this.b == null) {
                    this.b = a;
                }
                this.d = a;
                final ConstraintWidget.DimensionBehaviour[] z = a.Z;
                final int p2 = this.p;
                if (z[p2] == match_CONSTRAINT) {
                    final int n4 = a.y[p2];
                    if (n4 == 0 || n4 == 3 || n4 == 2) {
                        ++this.j;
                        final float n5 = a.D0[p2];
                        if (n5 > 0.0f) {
                            this.k += n5;
                        }
                        if (c(a, p2)) {
                            if (n5 < 0.0f) {
                                this.r = true;
                            }
                            else {
                                this.s = true;
                            }
                            if (this.h == null) {
                                this.h = new ArrayList();
                            }
                            this.h.add(a);
                        }
                        if (this.f == null) {
                            this.f = a;
                        }
                        final ConstraintWidget g = this.g;
                        if (g != null) {
                            g.E0[this.p] = a;
                        }
                        this.g = a;
                    }
                    Label_0499: {
                        if (this.p == 0) {
                            if (a.w == 0) {
                                if (a.z == 0) {
                                    if (a.A == 0) {
                                        break Label_0499;
                                    }
                                }
                            }
                        }
                        else if (a.x == 0 && a.C == 0) {
                            if (a.D == 0) {
                                break Label_0499;
                            }
                        }
                        this.o = false;
                    }
                    if (a.d0 != 0.0f) {
                        this.o = false;
                        this.u = true;
                    }
                }
            }
            if (constraintWidget != a) {
                constraintWidget.F0[this.p] = a;
            }
            final ConstraintAnchor f2 = a.W[n + 1].f;
            ConstraintWidget constraintWidget3 = constraintWidget2;
            if (f2 != null) {
                final ConstraintWidget d = f2.d;
                final ConstraintAnchor f3 = d.W[n].f;
                constraintWidget3 = constraintWidget2;
                if (f3 != null) {
                    if (f3.d != a) {
                        constraintWidget3 = constraintWidget2;
                    }
                    else {
                        constraintWidget3 = d;
                    }
                }
            }
            if (constraintWidget3 == null) {
                constraintWidget3 = a;
                i = 1;
            }
            final ConstraintWidget constraintWidget4 = a;
            a = constraintWidget3;
            constraintWidget = constraintWidget4;
        }
        final ConstraintWidget b = this.b;
        if (b != null) {
            this.m -= b.W[n].f();
        }
        final ConstraintWidget d2 = this.d;
        if (d2 != null) {
            this.m -= d2.W[n + 1].f();
        }
        this.c = a;
        if (this.p == 0 && this.q) {
            this.e = a;
        }
        else {
            this.e = this.a;
        }
        if (!this.s || !this.r) {
            t = false;
        }
        this.t = t;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Arrays;
import androidx.constraintlayout.core.SolverVariable;
import java.util.HashSet;
import java.lang.ref.WeakReference;
import androidx.constraintlayout.core.c;

public class d extends z52
{
    public lb M0;
    public ss N0;
    public int O0;
    public lb.b P0;
    public boolean Q0;
    public c R0;
    public int S0;
    public int T0;
    public int U0;
    public int V0;
    public int W0;
    public int X0;
    public androidx.constraintlayout.core.widgets.c[] Y0;
    public androidx.constraintlayout.core.widgets.c[] Z0;
    public boolean a1;
    public boolean b1;
    public boolean c1;
    public int d1;
    public int e1;
    public int f1;
    public boolean g1;
    public boolean h1;
    public boolean i1;
    public int j1;
    public WeakReference k1;
    public WeakReference l1;
    public WeakReference m1;
    public WeakReference n1;
    public HashSet o1;
    public lb.a p1;
    
    public d() {
        this.M0 = new lb(this);
        this.N0 = new ss(this);
        this.P0 = null;
        this.Q0 = false;
        this.R0 = new c();
        this.W0 = 0;
        this.X0 = 0;
        this.Y0 = new androidx.constraintlayout.core.widgets.c[4];
        this.Z0 = new androidx.constraintlayout.core.widgets.c[4];
        this.a1 = false;
        this.b1 = false;
        this.c1 = false;
        this.d1 = 0;
        this.e1 = 0;
        this.f1 = 257;
        this.g1 = false;
        this.h1 = false;
        this.i1 = false;
        this.j1 = 0;
        this.k1 = null;
        this.l1 = null;
        this.m1 = null;
        this.n1 = null;
        this.o1 = new HashSet();
        this.p1 = new lb.a();
    }
    
    public static boolean T1(int d, final ConstraintWidget constraintWidget, final lb.b b, final lb.a a, int j) {
        if (b == null) {
            return false;
        }
        if (constraintWidget.V() != 8 && !(constraintWidget instanceof f) && !(constraintWidget instanceof a)) {
            a.a = constraintWidget.A();
            a.b = constraintWidget.T();
            a.c = constraintWidget.W();
            a.d = constraintWidget.x();
            a.i = false;
            a.j = j;
            final DimensionBehaviour a2 = a.a;
            final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
            if (a2 == match_CONSTRAINT) {
                d = 1;
            }
            else {
                d = 0;
            }
            if (a.b == match_CONSTRAINT) {
                j = 1;
            }
            else {
                j = 0;
            }
            final boolean b2 = d != 0 && constraintWidget.d0 > 0.0f;
            final boolean b3 = j != 0 && constraintWidget.d0 > 0.0f;
            int n = d;
            if (d != 0) {
                n = d;
                if (constraintWidget.a0(0)) {
                    n = d;
                    if (constraintWidget.w == 0) {
                        n = d;
                        if (!b2) {
                            a.a = DimensionBehaviour.WRAP_CONTENT;
                            if (j != 0 && constraintWidget.x == 0) {
                                a.a = DimensionBehaviour.FIXED;
                            }
                            n = 0;
                        }
                    }
                }
            }
            if ((d = j) != 0) {
                d = j;
                if (constraintWidget.a0(1)) {
                    d = j;
                    if (constraintWidget.x == 0) {
                        d = j;
                        if (!b3) {
                            a.b = DimensionBehaviour.WRAP_CONTENT;
                            if (n != 0 && constraintWidget.w == 0) {
                                a.b = DimensionBehaviour.FIXED;
                            }
                            d = 0;
                        }
                    }
                }
            }
            if (constraintWidget.n0()) {
                a.a = DimensionBehaviour.FIXED;
                n = 0;
            }
            if (constraintWidget.o0()) {
                a.b = DimensionBehaviour.FIXED;
                d = 0;
            }
            if (b2) {
                if (constraintWidget.y[0] == 4) {
                    a.a = DimensionBehaviour.FIXED;
                }
                else if (d == 0) {
                    final DimensionBehaviour b4 = a.b;
                    final DimensionBehaviour fixed = DimensionBehaviour.FIXED;
                    if (b4 == fixed) {
                        d = a.d;
                    }
                    else {
                        a.a = DimensionBehaviour.WRAP_CONTENT;
                        b.a(constraintWidget, a);
                        d = a.f;
                    }
                    a.a = fixed;
                    a.c = (int)(constraintWidget.v() * d);
                }
            }
            if (b3) {
                if (constraintWidget.y[1] == 4) {
                    a.b = DimensionBehaviour.FIXED;
                }
                else if (n == 0) {
                    final DimensionBehaviour a3 = a.a;
                    final DimensionBehaviour fixed2 = DimensionBehaviour.FIXED;
                    if (a3 == fixed2) {
                        d = a.c;
                    }
                    else {
                        a.b = DimensionBehaviour.WRAP_CONTENT;
                        b.a(constraintWidget, a);
                        d = a.e;
                    }
                    a.b = fixed2;
                    if (constraintWidget.w() == -1) {
                        d /= (int)constraintWidget.v();
                    }
                    else {
                        d *= (int)constraintWidget.v();
                    }
                    a.d = d;
                }
            }
            b.a(constraintWidget, a);
            constraintWidget.k1(a.e);
            constraintWidget.L0(a.f);
            constraintWidget.K0(a.h);
            constraintWidget.A0(a.g);
            a.j = lb.a.k;
            return a.i;
        }
        a.e = 0;
        a.f = 0;
        return false;
    }
    
    public final void A1(final ConstraintAnchor constraintAnchor, final SolverVariable solverVariable) {
        this.R0.h(solverVariable, this.R0.q(constraintAnchor), 0, 5);
    }
    
    public final void B1(final ConstraintAnchor constraintAnchor, final SolverVariable solverVariable) {
        this.R0.h(this.R0.q(constraintAnchor), solverVariable, 0, 5);
    }
    
    public final void C1(final ConstraintWidget constraintWidget) {
        final int x0 = this.X0;
        final androidx.constraintlayout.core.widgets.c[] y0 = this.Y0;
        if (x0 + 1 >= y0.length) {
            this.Y0 = Arrays.copyOf(y0, y0.length * 2);
        }
        this.Y0[this.X0] = new androidx.constraintlayout.core.widgets.c(constraintWidget, 1, this.Q1());
        ++this.X0;
    }
    
    public void D1(final ConstraintAnchor referent) {
        final WeakReference m1 = this.m1;
        if (m1 == null || m1.get() == null || referent.e() > ((ConstraintAnchor)this.m1.get()).e()) {
            this.m1 = new WeakReference((T)referent);
        }
    }
    
    public void E1(final ConstraintAnchor referent) {
        final WeakReference k1 = this.k1;
        if (k1 == null || k1.get() == null || referent.e() > ((ConstraintAnchor)this.k1.get()).e()) {
            this.k1 = new WeakReference((T)referent);
        }
    }
    
    public boolean F1(final boolean b) {
        return this.N0.f(b);
    }
    
    public boolean G1(final boolean b) {
        return this.N0.g(b);
    }
    
    public boolean H1(final boolean b, final int n) {
        return this.N0.h(b, n);
    }
    
    public void I1(final hw0 hw0) {
        this.R0.v(hw0);
    }
    
    public lb.b J1() {
        return this.P0;
    }
    
    public int K1() {
        return this.f1;
    }
    
    public c L1() {
        return this.R0;
    }
    
    public boolean M1() {
        return false;
    }
    
    public void N1() {
        this.N0.j();
    }
    
    @Override
    public void O(final StringBuilder sb) {
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(super.o);
        sb2.append(":{\n");
        sb.append(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("  actualWidth:");
        sb3.append(super.b0);
        sb.append(sb3.toString());
        sb.append("\n");
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("  actualHeight:");
        sb4.append(super.c0);
        sb.append(sb4.toString());
        sb.append("\n");
        final Iterator iterator = this.r1().iterator();
        while (iterator.hasNext()) {
            ((ConstraintWidget)iterator.next()).O(sb);
            sb.append(",\n");
        }
        sb.append("}");
    }
    
    public void O1() {
        this.N0.k();
    }
    
    public boolean P1() {
        return this.i1;
    }
    
    public boolean Q1() {
        return this.Q0;
    }
    
    public boolean R1() {
        return this.h1;
    }
    
    public long S1(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int s0, final int t0) {
        this.S0 = s0;
        this.T0 = t0;
        return this.M0.d(this, n, s0, t0, n2, n3, n4, n5, n6, n7);
    }
    
    public boolean U1(final int n) {
        return (this.f1 & n) == n;
    }
    
    public final void V1() {
        this.W0 = 0;
        this.X0 = 0;
    }
    
    public void W1(final lb.b p) {
        this.P0 = p;
        this.N0.n(p);
    }
    
    public void X1(final int f1) {
        this.f1 = f1;
        androidx.constraintlayout.core.c.r = this.U1(512);
    }
    
    public void Y1(final int o0) {
        this.O0 = o0;
    }
    
    public void Z1(final boolean q0) {
        this.Q0 = q0;
    }
    
    public boolean a2(final c c, final boolean[] array) {
        int i = 0;
        array[2] = false;
        final boolean u1 = this.U1(64);
        this.q1(c, u1);
        final int size = super.L0.size();
        boolean b = false;
        while (i < size) {
            final ConstraintWidget constraintWidget = super.L0.get(i);
            constraintWidget.q1(c, u1);
            if (constraintWidget.c0()) {
                b = true;
            }
            ++i;
        }
        return b;
    }
    
    public void b2() {
        this.M0.e(this);
    }
    
    @Override
    public void p1(final boolean b, final boolean b2) {
        super.p1(b, b2);
        for (int size = super.L0.size(), i = 0; i < size; ++i) {
            ((ConstraintWidget)super.L0.get(i)).p1(b, b2);
        }
    }
    
    @Override
    public void s1() {
        super.f0 = 0;
        super.g0 = 0;
        this.h1 = false;
        this.i1 = false;
        final int size = super.L0.size();
        final int max = Math.max(0, this.W());
        final int max2 = Math.max(0, this.x());
        final DimensionBehaviour[] z = super.Z;
        final DimensionBehaviour dimensionBehaviour = z[1];
        final DimensionBehaviour dimensionBehaviour2 = z[0];
        if (this.O0 == 0 && androidx.constraintlayout.core.widgets.g.b(this.f1, 1)) {
            ys.h(this, this.J1());
            for (int i = 0; i < size; ++i) {
                final ConstraintWidget constraintWidget = super.L0.get(i);
                if (constraintWidget.m0() && !(constraintWidget instanceof f) && !(constraintWidget instanceof a) && !(constraintWidget instanceof h) && !constraintWidget.l0()) {
                    final DimensionBehaviour u = constraintWidget.u(0);
                    final DimensionBehaviour u2 = constraintWidget.u(1);
                    final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
                    if (u != match_CONSTRAINT || constraintWidget.w == 1 || u2 != match_CONSTRAINT || constraintWidget.x == 1) {
                        T1(0, constraintWidget, this.P0, new lb.a(), lb.a.k);
                    }
                }
            }
        }
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        Label_0400: {
            if (size > 2) {
                final DimensionBehaviour wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
                if ((dimensionBehaviour2 == wrap_CONTENT || dimensionBehaviour == wrap_CONTENT) && androidx.constraintlayout.core.widgets.g.b(this.f1, 1024) && vb0.c(this, this.J1())) {
                    int w = max;
                    if (dimensionBehaviour2 == wrap_CONTENT) {
                        if (max < this.W() && max > 0) {
                            this.k1(max);
                            this.h1 = true;
                            w = max;
                        }
                        else {
                            w = this.W();
                        }
                    }
                    int x = max2;
                    if (dimensionBehaviour == wrap_CONTENT) {
                        if (max2 < this.x() && max2 > 0) {
                            this.L0(max2);
                            this.i1 = true;
                            x = max2;
                        }
                        else {
                            x = this.x();
                        }
                    }
                    n = w;
                    n2 = 1;
                    n3 = x;
                    break Label_0400;
                }
            }
            n2 = 0;
            n3 = max2;
            n = max;
        }
        final boolean b = this.U1(64) || this.U1(128);
        final c r0 = this.R0;
        r0.h = false;
        r0.i = false;
        if (this.f1 != 0 && b) {
            r0.i = true;
        }
        final ArrayList l0 = super.L0;
        final DimensionBehaviour a = this.A();
        final DimensionBehaviour wrap_CONTENT2 = DimensionBehaviour.WRAP_CONTENT;
        final boolean b2 = a == wrap_CONTENT2 || this.T() == wrap_CONTENT2;
        this.V1();
        for (int j = 0; j < size; ++j) {
            final ConstraintWidget constraintWidget2 = super.L0.get(j);
            if (constraintWidget2 instanceof z52) {
                ((z52)constraintWidget2).s1();
            }
        }
        final boolean u3 = this.U1(64);
        int n4 = 0;
        int k = 1;
        while (k != 0) {
            final int n5 = n4 + 1;
            int w2 = k;
            try {
                this.R0.E();
                w2 = k;
                this.V1();
                w2 = k;
                this.m(this.R0);
                for (int index = 0; index < size; ++index) {
                    w2 = k;
                    ((ConstraintWidget)super.L0.get(index)).m(this.R0);
                }
                w2 = k;
                final boolean b3 = (w2 = (this.w1(this.R0) ? 1 : 0)) != 0;
                final WeakReference k2 = this.k1;
                if (k2 != null) {
                    w2 = (b3 ? 1 : 0);
                    if (k2.get() != null) {
                        w2 = (b3 ? 1 : 0);
                        this.B1((ConstraintAnchor)this.k1.get(), this.R0.q(super.P));
                        w2 = (b3 ? 1 : 0);
                        this.k1 = null;
                    }
                }
                w2 = (b3 ? 1 : 0);
                final WeakReference m1 = this.m1;
                if (m1 != null) {
                    w2 = (b3 ? 1 : 0);
                    if (m1.get() != null) {
                        w2 = (b3 ? 1 : 0);
                        this.A1((ConstraintAnchor)this.m1.get(), this.R0.q(super.R));
                        w2 = (b3 ? 1 : 0);
                        this.m1 = null;
                    }
                }
                w2 = (b3 ? 1 : 0);
                final WeakReference l2 = this.l1;
                if (l2 != null) {
                    w2 = (b3 ? 1 : 0);
                    if (l2.get() != null) {
                        w2 = (b3 ? 1 : 0);
                        this.B1((ConstraintAnchor)this.l1.get(), this.R0.q(super.O));
                        w2 = (b3 ? 1 : 0);
                        this.l1 = null;
                    }
                }
                w2 = (b3 ? 1 : 0);
                final WeakReference n6 = this.n1;
                if (n6 != null) {
                    w2 = (b3 ? 1 : 0);
                    if (n6.get() != null) {
                        w2 = (b3 ? 1 : 0);
                        this.A1((ConstraintAnchor)this.n1.get(), this.R0.q(super.Q));
                        w2 = (b3 ? 1 : 0);
                        this.n1 = null;
                    }
                }
                if ((w2 = (b3 ? 1 : 0)) != 0) {
                    w2 = (b3 ? 1 : 0);
                    this.R0.A();
                    w2 = (b3 ? 1 : 0);
                }
            }
            catch (final Exception obj) {
                obj.printStackTrace();
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append("EXCEPTION : ");
                sb.append(obj);
                out.println(sb.toString());
            }
            final c r2 = this.R0;
            int a2;
            if (w2 != 0) {
                a2 = (this.a2(r2, androidx.constraintlayout.core.widgets.g.a) ? 1 : 0);
            }
            else {
                this.q1(r2, u3);
                for (int index2 = 0; index2 < size; ++index2) {
                    ((ConstraintWidget)super.L0.get(index2)).q1(this.R0, u3);
                }
                a2 = 0;
            }
            if (b2 && n5 < 8 && androidx.constraintlayout.core.widgets.g.a[2]) {
                int index3 = 0;
                int max3 = 0;
                int max4 = 0;
                while (index3 < size) {
                    final ConstraintWidget constraintWidget3 = super.L0.get(index3);
                    max4 = Math.max(max4, constraintWidget3.f0 + constraintWidget3.W());
                    max3 = Math.max(max3, constraintWidget3.g0 + constraintWidget3.x());
                    ++index3;
                }
                final int max5 = Math.max(super.m0, max4);
                final int max6 = Math.max(super.n0, max3);
                final DimensionBehaviour wrap_CONTENT3 = DimensionBehaviour.WRAP_CONTENT;
                int n7 = n2;
                int n8 = a2;
                if (dimensionBehaviour2 == wrap_CONTENT3) {
                    n7 = n2;
                    n8 = a2;
                    if (this.W() < max5) {
                        this.k1(max5);
                        super.Z[0] = wrap_CONTENT3;
                        n7 = 1;
                        n8 = 1;
                    }
                }
                n2 = n7;
                a2 = n8;
                if (dimensionBehaviour == wrap_CONTENT3) {
                    n2 = n7;
                    a2 = n8;
                    if (this.x() < max6) {
                        this.L0(max6);
                        super.Z[1] = wrap_CONTENT3;
                        n2 = 1;
                        a2 = 1;
                    }
                }
            }
            final int max7 = Math.max(super.m0, this.W());
            if (max7 > this.W()) {
                this.k1(max7);
                super.Z[0] = DimensionBehaviour.FIXED;
                n2 = 1;
                a2 = 1;
            }
            final int max8 = Math.max(super.n0, this.x());
            if (max8 > this.x()) {
                this.L0(max8);
                super.Z[1] = DimensionBehaviour.FIXED;
                n2 = 1;
                a2 = 1;
            }
            int n9 = n2;
            int n10 = a2;
            int n13 = 0;
            Label_1516: {
                if (n2 == 0) {
                    final DimensionBehaviour dimensionBehaviour3 = super.Z[0];
                    final DimensionBehaviour wrap_CONTENT4 = DimensionBehaviour.WRAP_CONTENT;
                    int n11 = n2;
                    int n12 = a2;
                    if (dimensionBehaviour3 == wrap_CONTENT4) {
                        n11 = n2;
                        n12 = a2;
                        if (n > 0) {
                            n11 = n2;
                            n12 = a2;
                            if (this.W() > n) {
                                this.h1 = true;
                                super.Z[0] = DimensionBehaviour.FIXED;
                                this.k1(n);
                                n11 = 1;
                                n12 = 1;
                            }
                        }
                    }
                    n9 = n11;
                    n10 = n12;
                    if (super.Z[1] == wrap_CONTENT4) {
                        n9 = n11;
                        n10 = n12;
                        if (n3 > 0) {
                            n9 = n11;
                            n10 = n12;
                            if (this.x() > n3) {
                                this.i1 = true;
                                super.Z[1] = DimensionBehaviour.FIXED;
                                this.L0(n3);
                                n13 = 1;
                                n2 = 1;
                                break Label_1516;
                            }
                        }
                    }
                }
                n2 = n9;
                n13 = n10;
            }
            if (n5 > 8) {
                k = 0;
            }
            else {
                k = n13;
            }
            n4 = n5;
        }
        super.L0 = l0;
        if (n2 != 0) {
            final DimensionBehaviour[] z2 = super.Z;
            z2[0] = dimensionBehaviour2;
            z2[1] = dimensionBehaviour;
        }
        this.w0(this.R0.w());
    }
    
    @Override
    public void t0() {
        this.R0.E();
        this.S0 = 0;
        this.U0 = 0;
        this.T0 = 0;
        this.V0 = 0;
        this.g1 = false;
        super.t0();
    }
    
    public void v1(final ConstraintWidget constraintWidget, final int n) {
        if (n == 0) {
            this.x1(constraintWidget);
        }
        else if (n == 1) {
            this.C1(constraintWidget);
        }
    }
    
    public boolean w1(final c c) {
        final boolean u1 = this.U1(64);
        this.g(c, u1);
        final int size = super.L0.size();
        int i = 0;
        boolean b = false;
        while (i < size) {
            final ConstraintWidget constraintWidget = super.L0.get(i);
            constraintWidget.S0(0, false);
            constraintWidget.S0(1, false);
            if (constraintWidget instanceof a) {
                b = true;
            }
            ++i;
        }
        if (b) {
            for (int j = 0; j < size; ++j) {
                final ConstraintWidget constraintWidget2 = super.L0.get(j);
                if (constraintWidget2 instanceof a) {
                    ((a)constraintWidget2).y1();
                }
            }
        }
        this.o1.clear();
        for (int k = 0; k < size; ++k) {
            final ConstraintWidget e = super.L0.get(k);
            if (e.f()) {
                if (e instanceof h) {
                    this.o1.add(e);
                }
                else {
                    e.g(c, u1);
                }
            }
        }
        while (this.o1.size() > 0) {
            final int size2 = this.o1.size();
            for (final h o : this.o1) {
                if (o.v1(this.o1)) {
                    o.g(c, u1);
                    this.o1.remove(o);
                    break;
                }
            }
            if (size2 == this.o1.size()) {
                final Iterator iterator2 = this.o1.iterator();
                while (iterator2.hasNext()) {
                    ((ConstraintWidget)iterator2.next()).g(c, u1);
                }
                this.o1.clear();
            }
        }
        if (c.r) {
            final HashSet<ConstraintWidget> set = new HashSet<ConstraintWidget>();
            for (int l = 0; l < size; ++l) {
                final ConstraintWidget e2 = super.L0.get(l);
                if (!e2.f()) {
                    set.add(e2);
                }
            }
            int n;
            if (this.A() == DimensionBehaviour.WRAP_CONTENT) {
                n = 0;
            }
            else {
                n = 1;
            }
            this.e(this, c, set, n, false);
            for (final ConstraintWidget constraintWidget3 : set) {
                androidx.constraintlayout.core.widgets.g.a(this, c, constraintWidget3);
                constraintWidget3.g(c, u1);
            }
        }
        else {
            for (int index = 0; index < size; ++index) {
                final ConstraintWidget constraintWidget4 = super.L0.get(index);
                if (constraintWidget4 instanceof d) {
                    final DimensionBehaviour[] z = constraintWidget4.Z;
                    final DimensionBehaviour dimensionBehaviour = z[0];
                    final DimensionBehaviour dimensionBehaviour2 = z[1];
                    final DimensionBehaviour wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
                    if (dimensionBehaviour == wrap_CONTENT) {
                        constraintWidget4.P0(DimensionBehaviour.FIXED);
                    }
                    if (dimensionBehaviour2 == wrap_CONTENT) {
                        constraintWidget4.g1(DimensionBehaviour.FIXED);
                    }
                    constraintWidget4.g(c, u1);
                    if (dimensionBehaviour == wrap_CONTENT) {
                        constraintWidget4.P0(dimensionBehaviour);
                    }
                    if (dimensionBehaviour2 == wrap_CONTENT) {
                        constraintWidget4.g1(dimensionBehaviour2);
                    }
                }
                else {
                    androidx.constraintlayout.core.widgets.g.a(this, c, constraintWidget4);
                    if (!constraintWidget4.f()) {
                        constraintWidget4.g(c, u1);
                    }
                }
            }
        }
        if (this.W0 > 0) {
            androidx.constraintlayout.core.widgets.b.b(this, c, null, 0);
        }
        if (this.X0 > 0) {
            androidx.constraintlayout.core.widgets.b.b(this, c, null, 1);
        }
        return true;
    }
    
    public final void x1(final ConstraintWidget constraintWidget) {
        final int w0 = this.W0;
        final androidx.constraintlayout.core.widgets.c[] z0 = this.Z0;
        if (w0 + 1 >= z0.length) {
            this.Z0 = Arrays.copyOf(z0, z0.length * 2);
        }
        this.Z0[this.W0] = new androidx.constraintlayout.core.widgets.c(constraintWidget, 0, this.Q1());
        ++this.W0;
    }
    
    public void y1(final ConstraintAnchor referent) {
        final WeakReference n1 = this.n1;
        if (n1 == null || n1.get() == null || referent.e() > ((ConstraintAnchor)this.n1.get()).e()) {
            this.n1 = new WeakReference((T)referent);
        }
    }
    
    public void z1(final ConstraintAnchor referent) {
        final WeakReference l1 = this.l1;
        if (l1 == null || l1.get() == null || referent.e() > ((ConstraintAnchor)this.l1.get()).e()) {
            this.l1 = new WeakReference((T)referent);
        }
    }
}

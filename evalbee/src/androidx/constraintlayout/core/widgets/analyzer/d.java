// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class d extends WidgetRun
{
    public DependencyNode k;
    public a l;
    
    public d(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        final DependencyNode k = new DependencyNode(this);
        this.k = k;
        this.l = null;
        super.h.e = DependencyNode.Type.TOP;
        super.i.e = DependencyNode.Type.BOTTOM;
        k.e = DependencyNode.Type.BASELINE;
        super.f = 1;
    }
    
    @Override
    public void a(final ps ps) {
        final int n = d$a.a[super.j.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    final ConstraintWidget b = super.b;
                    this.n(ps, b.P, b.R, 1);
                    return;
                }
            }
            else {
                this.o(ps);
            }
        }
        else {
            this.p(ps);
        }
        final a e = super.e;
        Label_0266: {
            if (e.c && !e.j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final ConstraintWidget b2 = super.b;
                final int x = b2.x;
                int n3 = 0;
                Label_0208: {
                    if (x != 2) {
                        if (x != 3) {
                            break Label_0266;
                        }
                        if (!b2.e.e.j) {
                            break Label_0266;
                        }
                        final int w = b2.w();
                        float n2 = 0.0f;
                        Label_0202: {
                            if (w != -1) {
                                if (w == 0) {
                                    final ConstraintWidget b3 = super.b;
                                    n2 = b3.e.e.g * b3.v();
                                    break Label_0202;
                                }
                                if (w != 1) {
                                    n3 = 0;
                                    break Label_0208;
                                }
                            }
                            final ConstraintWidget b4 = super.b;
                            n2 = b4.e.e.g / b4.v();
                        }
                        n3 = (int)(n2 + 0.5f);
                    }
                    else {
                        final ConstraintWidget k = b2.K();
                        if (k == null) {
                            break Label_0266;
                        }
                        final a e2 = k.f.e;
                        if (!e2.j) {
                            break Label_0266;
                        }
                        n3 = (int)(e2.g * super.b.E + 0.5f);
                    }
                }
                super.e.d(n3);
            }
        }
        final DependencyNode h = super.h;
        if (h.c) {
            final DependencyNode i = super.i;
            if (i.c) {
                if (h.j && i.j && super.e.j) {
                    return;
                }
                if (!super.e.j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    final ConstraintWidget b5 = super.b;
                    if (b5.w == 0 && !b5.k0()) {
                        final DependencyNode dependencyNode = super.h.l.get(0);
                        final DependencyNode dependencyNode2 = super.i.l.get(0);
                        final int g = dependencyNode.g;
                        final DependencyNode h2 = super.h;
                        final int n4 = g + h2.f;
                        final int n5 = dependencyNode2.g + super.i.f;
                        h2.d(n4);
                        super.i.d(n5);
                        super.e.d(n5 - n4);
                        return;
                    }
                }
                if (!super.e.j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.a == 1 && super.h.l.size() > 0 && super.i.l.size() > 0) {
                    final int n6 = super.i.l.get(0).g + super.i.f - (super.h.l.get(0).g + super.h.f);
                    final a e3 = super.e;
                    final int m = e3.m;
                    if (n6 < m) {
                        e3.d(n6);
                    }
                    else {
                        e3.d(m);
                    }
                }
                if (!super.e.j) {
                    return;
                }
                if (super.h.l.size() > 0 && super.i.l.size() > 0) {
                    final DependencyNode dependencyNode3 = super.h.l.get(0);
                    final DependencyNode dependencyNode4 = super.i.l.get(0);
                    int g2 = dependencyNode3.g + super.h.f;
                    int g3 = dependencyNode4.g + super.i.f;
                    float r = super.b.R();
                    if (dependencyNode3 == dependencyNode4) {
                        g2 = dependencyNode3.g;
                        g3 = dependencyNode4.g;
                        r = 0.5f;
                    }
                    super.h.d((int)(g2 + 0.5f + (g3 - g2 - super.e.g) * r));
                    super.i.d(super.h.g + super.e.g);
                }
            }
        }
    }
    
    @Override
    public void d() {
        final ConstraintWidget b = super.b;
        if (b.a) {
            super.e.d(b.x());
        }
        if (!super.e.j) {
            super.d = super.b.T();
            if (super.b.Z()) {
                this.l = new kb(this);
            }
            final ConstraintWidget.DimensionBehaviour d = super.d;
            if (d != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                if (d == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                    final ConstraintWidget k = super.b.K();
                    if (k != null && k.T() == ConstraintWidget.DimensionBehaviour.FIXED) {
                        final int x = k.x();
                        final int f = super.b.P.f();
                        final int f2 = super.b.R.f();
                        this.b(super.h, k.f.h, super.b.P.f());
                        this.b(super.i, k.f.i, -super.b.R.f());
                        super.e.d(x - f - f2);
                        return;
                    }
                }
                if (super.d == ConstraintWidget.DimensionBehaviour.FIXED) {
                    super.e.d(super.b.x());
                }
            }
        }
        else if (super.d == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final ConstraintWidget i = super.b.K();
            if (i != null && i.T() == ConstraintWidget.DimensionBehaviour.FIXED) {
                this.b(super.h, i.f.h, super.b.P.f());
                this.b(super.i, i.f.i, -super.b.R.f());
                return;
            }
        }
        final a e = super.e;
        final boolean j = e.j;
        if (j) {
            final ConstraintWidget b2 = super.b;
            if (b2.a) {
                final ConstraintAnchor[] w = b2.W;
                final ConstraintAnchor constraintAnchor = w[2];
                final ConstraintAnchor f3 = constraintAnchor.f;
                DependencyNode dependencyNode = null;
                DependencyNode dependencyNode2 = null;
                int n = 0;
                Label_0574: {
                    if (f3 != null && w[3].f != null) {
                        if (b2.k0()) {
                            super.h.f = super.b.W[2].f();
                            super.i.f = -super.b.W[3].f();
                        }
                        else {
                            final DependencyNode h = this.h(super.b.W[2]);
                            if (h != null) {
                                this.b(super.h, h, super.b.W[2].f());
                            }
                            final DependencyNode h2 = this.h(super.b.W[3]);
                            if (h2 != null) {
                                this.b(super.i, h2, -super.b.W[3].f());
                            }
                            super.h.b = true;
                            super.i.b = true;
                        }
                        if (!super.b.Z()) {
                            return;
                        }
                    }
                    else if (f3 != null) {
                        final DependencyNode h3 = this.h(constraintAnchor);
                        if (h3 == null) {
                            return;
                        }
                        this.b(super.h, h3, super.b.W[2].f());
                        this.b(super.i, super.h, super.e.g);
                        if (!super.b.Z()) {
                            return;
                        }
                    }
                    else {
                        final ConstraintAnchor constraintAnchor2 = w[3];
                        if (constraintAnchor2.f != null) {
                            final DependencyNode h4 = this.h(constraintAnchor2);
                            if (h4 != null) {
                                this.b(super.i, h4, -super.b.W[3].f());
                                this.b(super.h, super.i, -super.e.g);
                            }
                            if (!super.b.Z()) {
                                return;
                            }
                        }
                        else {
                            final ConstraintAnchor constraintAnchor3 = w[4];
                            if (constraintAnchor3.f != null) {
                                final DependencyNode h5 = this.h(constraintAnchor3);
                                if (h5 != null) {
                                    this.b(this.k, h5, 0);
                                    this.b(super.h, this.k, -super.b.p());
                                    dependencyNode = super.i;
                                    dependencyNode2 = super.h;
                                    n = super.e.g;
                                    break Label_0574;
                                }
                                return;
                            }
                            else {
                                if (b2 instanceof fd0 || b2.K() == null || super.b.o(ConstraintAnchor.Type.CENTER).f != null) {
                                    return;
                                }
                                this.b(super.h, super.b.K().f.h, super.b.Y());
                                this.b(super.i, super.h, super.e.g);
                                if (!super.b.Z()) {
                                    return;
                                }
                            }
                        }
                    }
                    dependencyNode = this.k;
                    dependencyNode2 = super.h;
                    n = super.b.p();
                }
                this.b(dependencyNode, dependencyNode2, n);
                return;
            }
        }
        Label_1106: {
            if (!j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final ConstraintWidget b3 = super.b;
                final int x2 = b3.x;
                WidgetRun widgetRun;
                if (x2 != 2) {
                    if (x2 != 3) {
                        break Label_1106;
                    }
                    if (b3.k0()) {
                        break Label_1106;
                    }
                    final ConstraintWidget b4 = super.b;
                    if (b4.w == 3) {
                        break Label_1106;
                    }
                    widgetRun = b4.e;
                }
                else {
                    final ConstraintWidget l = b3.K();
                    if (l == null) {
                        break Label_1106;
                    }
                    widgetRun = l.f;
                }
                final a e2 = widgetRun.e;
                super.e.l.add(e2);
                e2.k.add(super.e);
                final a e3 = super.e;
                e3.b = true;
                e3.k.add(super.h);
                super.e.k.add(super.i);
            }
            else {
                e.b(this);
            }
        }
        final ConstraintWidget b5 = super.b;
        final ConstraintAnchor[] w2 = b5.W;
        final ConstraintAnchor constraintAnchor4 = w2[2];
        final ConstraintAnchor f4 = constraintAnchor4.f;
        Label_1780: {
            DependencyNode dependencyNode3 = null;
            DependencyNode dependencyNode4 = null;
            a a = null;
            Label_1284: {
                Label_1266: {
                    if (f4 == null || w2[3].f == null) {
                        c c;
                        if (f4 != null) {
                            final DependencyNode h6 = this.h(constraintAnchor4);
                            if (h6 == null) {
                                break Label_1780;
                            }
                            this.b(super.h, h6, super.b.W[2].f());
                            this.c(super.i, super.h, 1, super.e);
                            if (super.b.Z()) {
                                this.c(this.k, super.h, 1, this.l);
                            }
                            final ConstraintWidget.DimensionBehaviour d2 = super.d;
                            final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                            if (d2 != match_CONSTRAINT || super.b.v() <= 0.0f) {
                                break Label_1780;
                            }
                            c = super.b.e;
                            if (c.d != match_CONSTRAINT) {
                                break Label_1780;
                            }
                        }
                        else {
                            final ConstraintAnchor constraintAnchor5 = w2[3];
                            if (constraintAnchor5.f != null) {
                                final DependencyNode h7 = this.h(constraintAnchor5);
                                if (h7 == null) {
                                    break Label_1780;
                                }
                                this.b(super.i, h7, -super.b.W[3].f());
                                this.c(super.h, super.i, -1, super.e);
                                if (super.b.Z()) {
                                    break Label_1266;
                                }
                                break Label_1780;
                            }
                            else {
                                final ConstraintAnchor constraintAnchor6 = w2[4];
                                if (constraintAnchor6.f != null) {
                                    final DependencyNode h8 = this.h(constraintAnchor6);
                                    if (h8 != null) {
                                        this.b(this.k, h8, 0);
                                        this.c(super.h, this.k, -1, this.l);
                                        dependencyNode3 = super.i;
                                        dependencyNode4 = super.h;
                                        a = super.e;
                                        break Label_1284;
                                    }
                                    break Label_1780;
                                }
                                else {
                                    if (b5 instanceof fd0 || b5.K() == null) {
                                        break Label_1780;
                                    }
                                    this.b(super.h, super.b.K().f.h, super.b.Y());
                                    this.c(super.i, super.h, 1, super.e);
                                    if (super.b.Z()) {
                                        this.c(this.k, super.h, 1, this.l);
                                    }
                                    final ConstraintWidget.DimensionBehaviour d3 = super.d;
                                    final ConstraintWidget.DimensionBehaviour match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                                    if (d3 != match_CONSTRAINT2 || super.b.v() <= 0.0f) {
                                        break Label_1780;
                                    }
                                    c = super.b.e;
                                    if (c.d != match_CONSTRAINT2) {
                                        break Label_1780;
                                    }
                                }
                            }
                        }
                        c.e.k.add(super.e);
                        super.e.l.add(super.b.e.e);
                        super.e.a = this;
                        break Label_1780;
                    }
                    if (b5.k0()) {
                        super.h.f = super.b.W[2].f();
                        super.i.f = -super.b.W[3].f();
                    }
                    else {
                        final DependencyNode h9 = this.h(super.b.W[2]);
                        final DependencyNode h10 = this.h(super.b.W[3]);
                        if (h9 != null) {
                            h9.b(this);
                        }
                        if (h10 != null) {
                            h10.b(this);
                        }
                        super.j = RunType.CENTER;
                    }
                    if (!super.b.Z()) {
                        break Label_1780;
                    }
                }
                dependencyNode3 = this.k;
                dependencyNode4 = super.h;
                a = this.l;
            }
            this.c(dependencyNode3, dependencyNode4, 1, a);
        }
        if (super.e.l.size() == 0) {
            super.e.c = true;
        }
    }
    
    @Override
    public void e() {
        final DependencyNode h = super.h;
        if (h.j) {
            super.b.n1(h.g);
        }
    }
    
    @Override
    public void f() {
        super.c = null;
        super.h.c();
        super.i.c();
        this.k.c();
        super.e.c();
        super.g = false;
    }
    
    @Override
    public boolean m() {
        return super.d != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.b.x == 0;
    }
    
    public void q() {
        super.g = false;
        super.h.c();
        super.h.j = false;
        super.i.c();
        super.i.j = false;
        this.k.c();
        this.k.j = false;
        super.e.j = false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VerticalRun ");
        sb.append(super.b.t());
        return sb.toString();
    }
}

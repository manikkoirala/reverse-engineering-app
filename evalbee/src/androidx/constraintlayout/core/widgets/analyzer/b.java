// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.Iterator;
import androidx.constraintlayout.core.widgets.a;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class b extends WidgetRun
{
    public b(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
    }
    
    @Override
    public void a(final ps ps) {
        final a a = (a)super.b;
        final int v1 = a.v1();
        final Iterator iterator = super.h.l.iterator();
        int n = 0;
        int n2 = -1;
        while (iterator.hasNext()) {
            final int g = ((DependencyNode)iterator.next()).g;
            int n3;
            if (n2 == -1 || g < (n3 = n2)) {
                n3 = g;
            }
            n2 = n3;
            if (n < g) {
                n = g;
                n2 = n3;
            }
        }
        if (v1 != 0 && v1 != 2) {
            super.h.d(n + a.w1());
        }
        else {
            super.h.d(n2 + a.w1());
        }
    }
    
    @Override
    public void d() {
        final ConstraintWidget b = super.b;
        if (b instanceof a) {
            super.h.b = true;
            final a a = (a)b;
            final int v1 = a.v1();
            final boolean u1 = a.u1();
            final int n = 0;
            int i = 0;
            final int n2 = 0;
            final int n3 = 0;
            WidgetRun widgetRun = null;
            Label_0502: {
                if (v1 != 0) {
                    if (v1 != 1) {
                        if (v1 != 2) {
                            if (v1 != 3) {
                                return;
                            }
                            super.h.e = DependencyNode.Type.BOTTOM;
                            for (int j = n3; j < a.M0; ++j) {
                                final ConstraintWidget constraintWidget = a.L0[j];
                                if (u1 || constraintWidget.V() != 8) {
                                    final DependencyNode k = constraintWidget.f.i;
                                    k.k.add(super.h);
                                    super.h.l.add(k);
                                }
                            }
                        }
                        else {
                            super.h.e = DependencyNode.Type.TOP;
                            for (int l = n; l < a.M0; ++l) {
                                final ConstraintWidget constraintWidget2 = a.L0[l];
                                if (u1 || constraintWidget2.V() != 8) {
                                    final DependencyNode h = constraintWidget2.f.h;
                                    h.k.add(super.h);
                                    super.h.l.add(h);
                                }
                            }
                        }
                        this.q(super.b.f.h);
                        widgetRun = super.b.f;
                        break Label_0502;
                    }
                    super.h.e = DependencyNode.Type.RIGHT;
                    while (i < a.M0) {
                        final ConstraintWidget constraintWidget3 = a.L0[i];
                        if (u1 || constraintWidget3.V() != 8) {
                            final DependencyNode m = constraintWidget3.e.i;
                            m.k.add(super.h);
                            super.h.l.add(m);
                        }
                        ++i;
                    }
                }
                else {
                    super.h.e = DependencyNode.Type.LEFT;
                    for (int n4 = n2; n4 < a.M0; ++n4) {
                        final ConstraintWidget constraintWidget4 = a.L0[n4];
                        if (u1 || constraintWidget4.V() != 8) {
                            final DependencyNode h2 = constraintWidget4.e.h;
                            h2.k.add(super.h);
                            super.h.l.add(h2);
                        }
                    }
                }
                this.q(super.b.e.h);
                widgetRun = super.b.e;
            }
            this.q(widgetRun.i);
        }
    }
    
    @Override
    public void e() {
        final ConstraintWidget b = super.b;
        if (b instanceof a) {
            final int v1 = ((a)b).v1();
            if (v1 != 0 && v1 != 1) {
                super.b.n1(super.h.g);
            }
            else {
                super.b.m1(super.h.g);
            }
        }
    }
    
    @Override
    public void f() {
        super.c = null;
        super.h.c();
    }
    
    @Override
    public boolean m() {
        return false;
    }
    
    public final void q(final DependencyNode dependencyNode) {
        super.h.k.add(dependencyNode);
        dependencyNode.l.add(super.h);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.Iterator;

public class a extends DependencyNode
{
    public int m;
    
    public a(final WidgetRun widgetRun) {
        super(widgetRun);
        Type e;
        if (widgetRun instanceof c) {
            e = Type.HORIZONTAL_DIMENSION;
        }
        else {
            e = Type.VERTICAL_DIMENSION;
        }
        super.e = e;
    }
    
    @Override
    public void d(final int g) {
        if (super.j) {
            return;
        }
        super.j = true;
        super.g = g;
        for (final ps ps : super.k) {
            ps.a(ps);
        }
    }
}

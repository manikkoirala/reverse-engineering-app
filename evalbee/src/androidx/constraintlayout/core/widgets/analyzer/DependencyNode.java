// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.io.Serializable;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class DependencyNode implements ps
{
    public ps a;
    public boolean b;
    public boolean c;
    public WidgetRun d;
    public Type e;
    public int f;
    public int g;
    public int h;
    public a i;
    public boolean j;
    public List k;
    public List l;
    
    public DependencyNode(final WidgetRun d) {
        this.a = null;
        this.b = false;
        this.c = false;
        this.e = Type.UNKNOWN;
        this.h = 1;
        this.i = null;
        this.j = false;
        this.k = new ArrayList();
        this.l = new ArrayList();
        this.d = d;
    }
    
    @Override
    public void a(ps ps) {
        final Iterator iterator = this.l.iterator();
        while (iterator.hasNext()) {
            if (!((DependencyNode)iterator.next()).j) {
                return;
            }
        }
        this.c = true;
        ps = this.a;
        if (ps != null) {
            ps.a(this);
        }
        if (this.b) {
            this.d.a(this);
            return;
        }
        final Iterator iterator2 = this.l.iterator();
        DependencyNode dependencyNode = null;
        int n = 0;
        while (iterator2.hasNext()) {
            final DependencyNode dependencyNode2 = (DependencyNode)iterator2.next();
            if (dependencyNode2 instanceof a) {
                continue;
            }
            ++n;
            dependencyNode = dependencyNode2;
        }
        if (dependencyNode != null && n == 1 && dependencyNode.j) {
            final a i = this.i;
            if (i != null) {
                if (!i.j) {
                    return;
                }
                this.f = this.h * i.g;
            }
            this.d(dependencyNode.g + this.f);
        }
        ps = this.a;
        if (ps != null) {
            ps.a(this);
        }
    }
    
    public void b(final ps ps) {
        this.k.add(ps);
        if (this.j) {
            ps.a(ps);
        }
    }
    
    public void c() {
        this.l.clear();
        this.k.clear();
        this.j = false;
        this.g = 0;
        this.c = false;
        this.b = false;
    }
    
    public void d(final int g) {
        if (this.j) {
            return;
        }
        this.j = true;
        this.g = g;
        for (final ps ps : this.k) {
            ps.a(ps);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.d.b.t());
        sb.append(":");
        sb.append(this.e);
        sb.append("(");
        Serializable value;
        if (this.j) {
            value = this.g;
        }
        else {
            value = "unresolved";
        }
        sb.append(value);
        sb.append(") <t=");
        sb.append(this.l.size());
        sb.append(":d=");
        sb.append(this.k.size());
        sb.append(">");
        return sb.toString();
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        BASELINE, 
        BOTTOM, 
        HORIZONTAL_DIMENSION, 
        LEFT, 
        RIGHT, 
        TOP, 
        UNKNOWN, 
        VERTICAL_DIMENSION;
    }
}

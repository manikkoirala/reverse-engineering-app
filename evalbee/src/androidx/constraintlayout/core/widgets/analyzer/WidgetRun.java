// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public abstract class WidgetRun implements ps
{
    public int a;
    public ConstraintWidget b;
    public zf1 c;
    public ConstraintWidget.DimensionBehaviour d;
    public a e;
    public int f;
    public boolean g;
    public DependencyNode h;
    public DependencyNode i;
    public RunType j;
    
    public WidgetRun(final ConstraintWidget b) {
        this.e = new a(this);
        this.f = 0;
        this.g = false;
        this.h = new DependencyNode(this);
        this.i = new DependencyNode(this);
        this.j = RunType.NONE;
        this.b = b;
    }
    
    @Override
    public abstract void a(final ps p0);
    
    public final void b(final DependencyNode dependencyNode, final DependencyNode dependencyNode2, final int f) {
        dependencyNode.l.add(dependencyNode2);
        dependencyNode.f = f;
        dependencyNode2.k.add(dependencyNode);
    }
    
    public final void c(final DependencyNode dependencyNode, final DependencyNode dependencyNode2, final int h, final a i) {
        dependencyNode.l.add(dependencyNode2);
        dependencyNode.l.add(this.e);
        dependencyNode.h = h;
        dependencyNode.i = i;
        dependencyNode2.k.add(dependencyNode);
        i.k.add(dependencyNode);
    }
    
    public abstract void d();
    
    public abstract void e();
    
    public abstract void f();
    
    public final int g(final int n, int n2) {
        if (n2 == 0) {
            final ConstraintWidget b = this.b;
            final int a = b.A;
            n2 = Math.max(b.z, n);
            if (a > 0) {
                n2 = Math.min(a, n);
            }
            final int n3;
            if (n2 == (n3 = n)) {
                return n3;
            }
        }
        else {
            final ConstraintWidget b2 = this.b;
            final int d = b2.D;
            n2 = Math.max(b2.C, n);
            if (d > 0) {
                n2 = Math.min(d, n);
            }
            final int n3;
            if (n2 == (n3 = n)) {
                return n3;
            }
        }
        return n2;
    }
    
    public final DependencyNode h(final ConstraintAnchor constraintAnchor) {
        final ConstraintAnchor f = constraintAnchor.f;
        DependencyNode dependencyNode = null;
        if (f == null) {
            return null;
        }
        final ConstraintWidget d = f.d;
        final int n = WidgetRun$a.a[f.e.ordinal()];
        if (n != 1) {
            WidgetRun widgetRun2;
            if (n != 2) {
                if (n == 3) {
                    final WidgetRun widgetRun = d.f;
                    return widgetRun.h;
                }
                if (n == 4) {
                    dependencyNode = d.f.k;
                    return dependencyNode;
                }
                if (n != 5) {
                    return dependencyNode;
                }
                widgetRun2 = d.f;
            }
            else {
                widgetRun2 = d.e;
            }
            dependencyNode = widgetRun2.i;
            return dependencyNode;
        }
        final WidgetRun widgetRun = d.e;
        dependencyNode = widgetRun.h;
        return dependencyNode;
    }
    
    public final DependencyNode i(final ConstraintAnchor constraintAnchor, int n) {
        final ConstraintAnchor f = constraintAnchor.f;
        final DependencyNode dependencyNode = null;
        if (f == null) {
            return null;
        }
        final ConstraintWidget d = f.d;
        WidgetRun widgetRun;
        if (n == 0) {
            widgetRun = d.e;
        }
        else {
            widgetRun = d.f;
        }
        n = WidgetRun$a.a[f.e.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    return widgetRun.h;
                }
                if (n != 5) {
                    return dependencyNode;
                }
            }
            return widgetRun.i;
        }
        return widgetRun.h;
    }
    
    public long j() {
        final a e = this.e;
        if (e.j) {
            return e.g;
        }
        return 0L;
    }
    
    public boolean k() {
        return this.g;
    }
    
    public final void l(int a, final int b) {
        final int a2 = this.a;
        int n = b;
        a a3 = null;
        Label_0318: {
            if (a2 != 0) {
                if (a2 == 1) {
                    a = this.g(this.e.m, a);
                    a3 = this.e;
                    a = Math.min(a, b);
                    break Label_0318;
                }
                if (a2 != 2) {
                    if (a2 != 3) {
                        return;
                    }
                    final ConstraintWidget b2 = this.b;
                    WidgetRun widgetRun = b2.e;
                    final ConstraintWidget.DimensionBehaviour d = widgetRun.d;
                    final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    if (d == match_CONSTRAINT && widgetRun.a == 3) {
                        final d f = b2.f;
                        if (f.d == match_CONSTRAINT && f.a == 3) {
                            return;
                        }
                    }
                    if (a == 0) {
                        widgetRun = b2.f;
                    }
                    if (widgetRun.e.j) {
                        final float v = b2.v();
                        if (a == 1) {
                            a = (int)(widgetRun.e.g / v + 0.5f);
                        }
                        else {
                            a = (int)(v * widgetRun.e.g + 0.5f);
                        }
                        this.e.d(a);
                    }
                    return;
                }
                else {
                    final ConstraintWidget k = this.b.K();
                    if (k == null) {
                        return;
                    }
                    WidgetRun widgetRun2;
                    if (a == 0) {
                        widgetRun2 = k.e;
                    }
                    else {
                        widgetRun2 = k.f;
                    }
                    final a e = widgetRun2.e;
                    if (!e.j) {
                        return;
                    }
                    final ConstraintWidget b3 = this.b;
                    float n2;
                    if (a == 0) {
                        n2 = b3.B;
                    }
                    else {
                        n2 = b3.E;
                    }
                    n = (int)(e.g * n2 + 0.5f);
                }
            }
            a3 = this.e;
            a = this.g(n, a);
        }
        a3.d(a);
    }
    
    public abstract boolean m();
    
    public void n(final ps ps, final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, int g) {
        final DependencyNode h = this.h(constraintAnchor);
        final DependencyNode h2 = this.h(constraintAnchor2);
        if (!h.j || !h2.j) {
            return;
        }
        final int n = h.g + constraintAnchor.f();
        int g2 = h2.g - constraintAnchor2.f();
        final int n2 = g2 - n;
        if (!this.e.j && this.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            this.l(g, n2);
        }
        final a e = this.e;
        if (!e.j) {
            return;
        }
        DependencyNode dependencyNode;
        if (e.g == n2) {
            this.h.d(n);
            dependencyNode = this.i;
        }
        else {
            final ConstraintWidget b = this.b;
            float n3;
            if (g == 0) {
                n3 = b.y();
            }
            else {
                n3 = b.R();
            }
            g = n;
            if (h == h2) {
                g = h.g;
                g2 = h2.g;
                n3 = 0.5f;
            }
            this.h.d((int)(g + 0.5f + (g2 - g - this.e.g) * n3));
            dependencyNode = this.i;
            g2 = this.h.g + this.e.g;
        }
        dependencyNode.d(g2);
    }
    
    public void o(final ps ps) {
    }
    
    public void p(final ps ps) {
    }
    
    public enum RunType
    {
        private static final RunType[] $VALUES;
        
        CENTER, 
        END, 
        NONE, 
        START;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.List;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class c extends WidgetRun
{
    public static int[] k;
    
    static {
        c.k = new int[2];
    }
    
    public c(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        super.h.e = DependencyNode.Type.LEFT;
        super.i.e = DependencyNode.Type.RIGHT;
        super.f = 0;
    }
    
    @Override
    public void a(final ps ps) {
        final int n = c$a.a[super.j.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    final ConstraintWidget b = super.b;
                    this.n(ps, b.O, b.Q, 0);
                    return;
                }
            }
            else {
                this.o(ps);
            }
        }
        else {
            this.p(ps);
        }
        Label_1475: {
            if (!super.e.j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final ConstraintWidget b2 = super.b;
                final int w = b2.w;
                int n17 = 0;
                Label_0207: {
                    if (w != 2) {
                        if (w != 3) {
                            break Label_1475;
                        }
                        final int x = b2.x;
                        if (x == 0 || x == 3) {
                            final d f = b2.f;
                            final DependencyNode h = f.h;
                            final DependencyNode i = f.i;
                            final boolean b3 = b2.O.f != null;
                            final boolean b4 = b2.P.f != null;
                            final boolean b5 = b2.Q.f != null;
                            final boolean b6 = b2.R.f != null;
                            final int w2 = b2.w();
                            a a;
                            int n2 = 0;
                            if (b3 && b4 && b5 && b6) {
                                final float v = super.b.v();
                                if (h.j && i.j) {
                                    final DependencyNode h2 = super.h;
                                    if (h2.c) {
                                        if (super.i.c) {
                                            this.q(c.k, ((DependencyNode)h2.l.get(0)).g + super.h.f, super.i.l.get(0).g - super.i.f, h.g + h.f, i.g - i.f, v, w2);
                                            super.e.d(c.k[0]);
                                            super.b.f.e.d(c.k[1]);
                                        }
                                    }
                                    return;
                                }
                                final DependencyNode h3 = super.h;
                                if (h3.j) {
                                    final DependencyNode j = super.i;
                                    if (j.j) {
                                        if (!h.c || !i.c) {
                                            return;
                                        }
                                        this.q(c.k, h3.g + h3.f, j.g - j.f, h.l.get(0).g + h.f, i.l.get(0).g - i.f, v, w2);
                                        super.e.d(c.k[0]);
                                        super.b.f.e.d(c.k[1]);
                                    }
                                }
                                final DependencyNode h4 = super.h;
                                if (!h4.c || !super.i.c || !h.c || !i.c) {
                                    return;
                                }
                                this.q(c.k, ((DependencyNode)h4.l.get(0)).g + super.h.f, super.i.l.get(0).g - super.i.f, h.l.get(0).g + h.f, i.l.get(0).g - i.f, v, w2);
                                super.e.d(c.k[0]);
                                a = super.b.f.e;
                                n2 = c.k[1];
                            }
                            else if (b3 && b5) {
                                if (super.h.c && super.i.c) {
                                    final float v2 = super.b.v();
                                    final int n3 = super.h.l.get(0).g + super.h.f;
                                    final int n4 = super.i.l.get(0).g - super.i.f;
                                    int n5 = 0;
                                    int n7 = 0;
                                    Label_1159: {
                                        float n8;
                                        if (w2 != -1 && w2 != 0) {
                                            if (w2 != 1) {
                                                break Label_1475;
                                            }
                                            n5 = this.g(n4 - n3, 0);
                                            final int n6 = (int)(n5 / v2 + 0.5f);
                                            final int g = this.g(n6, 1);
                                            if (n6 == (n7 = g)) {
                                                break Label_1159;
                                            }
                                            n8 = g * v2;
                                            n7 = g;
                                        }
                                        else {
                                            n5 = this.g(n4 - n3, 0);
                                            final int n9 = (int)(n5 * v2 + 0.5f);
                                            final int g2 = this.g(n9, 1);
                                            if (n9 == (n7 = g2)) {
                                                break Label_1159;
                                            }
                                            n8 = g2 / v2;
                                            n7 = g2;
                                        }
                                        n5 = (int)(n8 + 0.5f);
                                    }
                                    super.e.d(n5);
                                    super.b.f.e.d(n7);
                                    break Label_1475;
                                }
                                return;
                            }
                            else {
                                if (!b4 || !b6) {
                                    break Label_1475;
                                }
                                if (!h.c || !i.c) {
                                    return;
                                }
                                final float v3 = super.b.v();
                                final int n10 = h.l.get(0).g + h.f;
                                final int n11 = i.l.get(0).g - i.f;
                                int n13 = 0;
                                Label_1398: {
                                    float n14 = 0.0f;
                                    Label_1392: {
                                        if (w2 != -1) {
                                            if (w2 != 0) {
                                                if (w2 != 1) {
                                                    break Label_1475;
                                                }
                                            }
                                            else {
                                                n2 = this.g(n11 - n10, 1);
                                                final int n12 = (int)(n2 * v3 + 0.5f);
                                                final int g3 = this.g(n12, 0);
                                                if (n12 != (n13 = g3)) {
                                                    n14 = g3 / v3;
                                                    n13 = g3;
                                                    break Label_1392;
                                                }
                                                break Label_1398;
                                            }
                                        }
                                        n2 = this.g(n11 - n10, 1);
                                        final int n15 = (int)(n2 / v3 + 0.5f);
                                        final int g4 = this.g(n15, 0);
                                        if (n15 == (n13 = g4)) {
                                            break Label_1398;
                                        }
                                        n14 = g4 * v3;
                                        n13 = g4;
                                    }
                                    n2 = (int)(n14 + 0.5f);
                                }
                                super.e.d(n13);
                                a = super.b.f.e;
                            }
                            a.d(n2);
                            break Label_1475;
                        }
                        final int w3 = b2.w();
                        float n16 = 0.0f;
                        Label_0201: {
                            if (w3 != -1) {
                                if (w3 == 0) {
                                    final ConstraintWidget b7 = super.b;
                                    n16 = b7.f.e.g / b7.v();
                                    break Label_0201;
                                }
                                if (w3 != 1) {
                                    n17 = 0;
                                    break Label_0207;
                                }
                            }
                            final ConstraintWidget b8 = super.b;
                            n16 = b8.f.e.g * b8.v();
                        }
                        n17 = (int)(n16 + 0.5f);
                    }
                    else {
                        final ConstraintWidget k = b2.K();
                        if (k == null) {
                            break Label_1475;
                        }
                        final a e = k.e.e;
                        if (!e.j) {
                            break Label_1475;
                        }
                        n17 = (int)(e.g * super.b.B + 0.5f);
                    }
                }
                super.e.d(n17);
            }
        }
        final DependencyNode h5 = super.h;
        if (h5.c) {
            final DependencyNode l = super.i;
            if (l.c) {
                if (h5.j && l.j && super.e.j) {
                    return;
                }
                if (!super.e.j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    final ConstraintWidget b9 = super.b;
                    if (b9.w == 0 && !b9.i0()) {
                        final DependencyNode dependencyNode = super.h.l.get(0);
                        final DependencyNode dependencyNode2 = super.i.l.get(0);
                        final int g5 = dependencyNode.g;
                        final DependencyNode h6 = super.h;
                        final int n18 = g5 + h6.f;
                        final int n19 = dependencyNode2.g + super.i.f;
                        h6.d(n18);
                        super.i.d(n19);
                        super.e.d(n19 - n18);
                        return;
                    }
                }
                if (!super.e.j && super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.a == 1 && super.h.l.size() > 0 && super.i.l.size() > 0) {
                    final int min = Math.min(super.i.l.get(0).g + super.i.f - (super.h.l.get(0).g + super.h.f), super.e.m);
                    final ConstraintWidget b10 = super.b;
                    final int a2 = b10.A;
                    int b11 = Math.max(b10.z, min);
                    if (a2 > 0) {
                        b11 = Math.min(a2, b11);
                    }
                    super.e.d(b11);
                }
                if (!super.e.j) {
                    return;
                }
                final DependencyNode dependencyNode3 = super.h.l.get(0);
                final DependencyNode dependencyNode4 = super.i.l.get(0);
                int g6 = dependencyNode3.g + super.h.f;
                int g7 = dependencyNode4.g + super.i.f;
                float y = super.b.y();
                if (dependencyNode3 == dependencyNode4) {
                    g6 = dependencyNode3.g;
                    g7 = dependencyNode4.g;
                    y = 0.5f;
                }
                super.h.d((int)(g6 + 0.5f + (g7 - g6 - super.e.g) * y));
                super.i.d(super.h.g + super.e.g);
            }
        }
    }
    
    @Override
    public void d() {
        final ConstraintWidget b = super.b;
        if (b.a) {
            super.e.d(b.W());
        }
        if (!super.e.j) {
            final ConstraintWidget.DimensionBehaviour a = super.b.A();
            if ((super.d = a) != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final ConstraintWidget.DimensionBehaviour match_PARENT = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                if (a == match_PARENT) {
                    final ConstraintWidget k = super.b.K();
                    if (k != null && (k.A() == ConstraintWidget.DimensionBehaviour.FIXED || k.A() == match_PARENT)) {
                        final int w = k.W();
                        final int f = super.b.O.f();
                        final int f2 = super.b.Q.f();
                        this.b(super.h, k.e.h, super.b.O.f());
                        this.b(super.i, k.e.i, -super.b.Q.f());
                        super.e.d(w - f - f2);
                        return;
                    }
                }
                if (super.d == ConstraintWidget.DimensionBehaviour.FIXED) {
                    super.e.d(super.b.W());
                }
            }
        }
        else {
            final ConstraintWidget.DimensionBehaviour d = super.d;
            final ConstraintWidget.DimensionBehaviour match_PARENT2 = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
            if (d == match_PARENT2) {
                final ConstraintWidget i = super.b.K();
                if (i != null && (i.A() == ConstraintWidget.DimensionBehaviour.FIXED || i.A() == match_PARENT2)) {
                    this.b(super.h, i.e.h, super.b.O.f());
                    this.b(super.i, i.e.i, -super.b.Q.f());
                    return;
                }
            }
        }
        final a e = super.e;
        DependencyNode dependencyNode5 = null;
        ConstraintAnchor constraintAnchor3 = null;
        Label_1440: {
            if (e.j) {
                final ConstraintWidget b2 = super.b;
                if (b2.a) {
                    final ConstraintAnchor[] w2 = b2.W;
                    final ConstraintAnchor constraintAnchor = w2[0];
                    final ConstraintAnchor f3 = constraintAnchor.f;
                    if (f3 == null || w2[1].f == null) {
                        DependencyNode dependencyNode3 = null;
                        DependencyNode dependencyNode4 = null;
                        int g = 0;
                        Label_0752: {
                            DependencyNode dependencyNode;
                            DependencyNode dependencyNode2;
                            int n;
                            if (f3 != null) {
                                dependencyNode = this.h(constraintAnchor);
                                if (dependencyNode == null) {
                                    return;
                                }
                                dependencyNode2 = super.h;
                                n = super.b.W[0].f();
                            }
                            else {
                                final ConstraintAnchor constraintAnchor2 = w2[1];
                                if (constraintAnchor2.f != null) {
                                    final DependencyNode h = this.h(constraintAnchor2);
                                    if (h != null) {
                                        this.b(super.i, h, -super.b.W[1].f());
                                        dependencyNode3 = super.h;
                                        dependencyNode4 = super.i;
                                        g = -super.e.g;
                                        break Label_0752;
                                    }
                                    return;
                                }
                                else {
                                    if (b2 instanceof fd0 || b2.K() == null || super.b.o(ConstraintAnchor.Type.CENTER).f != null) {
                                        return;
                                    }
                                    dependencyNode = super.b.K().e.h;
                                    dependencyNode2 = super.h;
                                    n = super.b.X();
                                }
                            }
                            this.b(dependencyNode2, dependencyNode, n);
                            dependencyNode3 = super.i;
                            dependencyNode4 = super.h;
                            g = super.e.g;
                        }
                        this.b(dependencyNode3, dependencyNode4, g);
                        return;
                    }
                    if (b2.i0()) {
                        super.h.f = super.b.W[0].f();
                        dependencyNode5 = super.i;
                        constraintAnchor3 = super.b.W[1];
                        break Label_1440;
                    }
                    final DependencyNode h2 = this.h(super.b.W[0]);
                    if (h2 != null) {
                        this.b(super.h, h2, super.b.W[0].f());
                    }
                    final DependencyNode h3 = this.h(super.b.W[1]);
                    if (h3 != null) {
                        this.b(super.i, h3, -super.b.W[1].f());
                    }
                    super.h.b = true;
                    super.i.b = true;
                    return;
                }
            }
            Label_1355: {
                if (super.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    final ConstraintWidget b3 = super.b;
                    final int w3 = b3.w;
                    List list = null;
                    DependencyNode dependencyNode6 = null;
                    Label_1345: {
                        if (w3 != 2) {
                            if (w3 != 3) {
                                break Label_1355;
                            }
                            Label_1239: {
                                DependencyNode dependencyNode7;
                                if (b3.x == 3) {
                                    super.h.a = this;
                                    super.i.a = this;
                                    final d f4 = b3.f;
                                    f4.h.a = this;
                                    f4.i.a = this;
                                    e.a = this;
                                    if (b3.k0()) {
                                        super.e.l.add(super.b.f.e);
                                        super.b.f.e.k.add(super.e);
                                        final d f5 = super.b.f;
                                        f5.e.a = this;
                                        super.e.l.add(f5.h);
                                        super.e.l.add(super.b.f.i);
                                        super.b.f.h.k.add(super.e);
                                        list = super.b.f.i.k;
                                        break Label_1239;
                                    }
                                    if (super.b.i0()) {
                                        super.b.f.e.l.add(super.e);
                                        list = super.e.k;
                                        dependencyNode6 = super.b.f.e;
                                        break Label_1345;
                                    }
                                    dependencyNode7 = super.b.f.e;
                                }
                                else {
                                    final a e2 = b3.f.e;
                                    e.l.add(e2);
                                    e2.k.add(super.e);
                                    super.b.f.h.k.add(super.e);
                                    super.b.f.i.k.add(super.e);
                                    final a e3 = super.e;
                                    e3.b = true;
                                    e3.k.add(super.h);
                                    super.e.k.add(super.i);
                                    super.h.l.add(super.e);
                                    dependencyNode7 = super.i;
                                }
                                list = dependencyNode7.l;
                            }
                            dependencyNode6 = super.e;
                        }
                        else {
                            final ConstraintWidget j = b3.K();
                            if (j == null) {
                                break Label_1355;
                            }
                            final a e4 = j.f.e;
                            super.e.l.add(e4);
                            e4.k.add(super.e);
                            final a e5 = super.e;
                            e5.b = true;
                            e5.k.add(super.h);
                            list = super.e.k;
                            dependencyNode6 = super.i;
                        }
                    }
                    list.add(dependencyNode6);
                }
            }
            final ConstraintWidget b4 = super.b;
            final ConstraintAnchor[] w4 = b4.W;
            final ConstraintAnchor constraintAnchor4 = w4[0];
            final ConstraintAnchor f6 = constraintAnchor4.f;
            if (f6 == null || w4[1].f == null) {
                DependencyNode dependencyNode8;
                DependencyNode dependencyNode9;
                int n2;
                if (f6 != null) {
                    dependencyNode8 = this.h(constraintAnchor4);
                    if (dependencyNode8 == null) {
                        return;
                    }
                    dependencyNode9 = super.h;
                    n2 = super.b.W[0].f();
                }
                else {
                    final ConstraintAnchor constraintAnchor5 = w4[1];
                    if (constraintAnchor5.f != null) {
                        final DependencyNode h4 = this.h(constraintAnchor5);
                        if (h4 != null) {
                            this.b(super.i, h4, -super.b.W[1].f());
                            this.c(super.h, super.i, -1, super.e);
                        }
                        return;
                    }
                    else {
                        if (b4 instanceof fd0 || b4.K() == null) {
                            return;
                        }
                        dependencyNode8 = super.b.K().e.h;
                        dependencyNode9 = super.h;
                        n2 = super.b.X();
                    }
                }
                this.b(dependencyNode9, dependencyNode8, n2);
                this.c(super.i, super.h, 1, super.e);
                return;
            }
            if (!b4.i0()) {
                final DependencyNode h5 = this.h(super.b.W[0]);
                final DependencyNode h6 = this.h(super.b.W[1]);
                if (h5 != null) {
                    h5.b(this);
                }
                if (h6 != null) {
                    h6.b(this);
                }
                super.j = RunType.CENTER;
                return;
            }
            super.h.f = super.b.W[0].f();
            dependencyNode5 = super.i;
            constraintAnchor3 = super.b.W[1];
        }
        dependencyNode5.f = -constraintAnchor3.f();
    }
    
    @Override
    public void e() {
        final DependencyNode h = super.h;
        if (h.j) {
            super.b.m1(h.g);
        }
    }
    
    @Override
    public void f() {
        super.c = null;
        super.h.c();
        super.i.c();
        super.e.c();
        super.g = false;
    }
    
    @Override
    public boolean m() {
        return super.d != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.b.w == 0;
    }
    
    public final void q(final int[] array, int n, int n2, int n3, int n4, final float n5, final int n6) {
        n = n2 - n;
        n2 = n4 - n3;
        if (n6 != -1) {
            if (n6 != 0) {
                if (n6 == 1) {
                    n2 = (int)(n * n5 + 0.5f);
                    array[0] = n;
                    array[1] = n2;
                }
            }
            else {
                array[0] = (int)(n2 * n5 + 0.5f);
                array[1] = n2;
            }
        }
        else {
            n3 = (int)(n2 * n5 + 0.5f);
            n4 = (int)(n / n5 + 0.5f);
            if (n3 <= n) {
                array[0] = n3;
                array[1] = n2;
            }
            else if (n4 <= n2) {
                array[0] = n;
                array[1] = n4;
            }
        }
    }
    
    public void r() {
        super.g = false;
        super.h.c();
        super.h.j = false;
        super.i.c();
        super.i.j = false;
        super.e.j = false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("HorizontalRun ");
        sb.append(super.b.t());
        return sb.toString();
    }
}

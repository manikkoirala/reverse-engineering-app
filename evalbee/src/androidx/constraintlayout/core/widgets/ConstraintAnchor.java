// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.Iterator;
import java.util.ArrayList;
import androidx.constraintlayout.core.SolverVariable;
import java.util.HashSet;

public class ConstraintAnchor
{
    public HashSet a;
    public int b;
    public boolean c;
    public final ConstraintWidget d;
    public final Type e;
    public ConstraintAnchor f;
    public int g;
    public int h;
    public SolverVariable i;
    
    public ConstraintAnchor(final ConstraintWidget d, final Type e) {
        this.a = null;
        this.g = 0;
        this.h = Integer.MIN_VALUE;
        this.d = d;
        this.e = e;
    }
    
    public boolean a(final ConstraintAnchor constraintAnchor, final int n) {
        return this.b(constraintAnchor, n, Integer.MIN_VALUE, false);
    }
    
    public boolean b(final ConstraintAnchor f, final int g, final int h, final boolean b) {
        if (f == null) {
            this.q();
            return true;
        }
        if (!b && !this.p(f)) {
            return false;
        }
        this.f = f;
        if (f.a == null) {
            f.a = new HashSet();
        }
        final HashSet a = this.f.a;
        if (a != null) {
            a.add(this);
        }
        this.g = g;
        this.h = h;
        return true;
    }
    
    public void c(final int n, final ArrayList list, final b62 b62) {
        final HashSet a = this.a;
        if (a != null) {
            final Iterator iterator = a.iterator();
            while (iterator.hasNext()) {
                vb0.a(((ConstraintAnchor)iterator.next()).d, n, list, b62);
            }
        }
    }
    
    public HashSet d() {
        return this.a;
    }
    
    public int e() {
        if (!this.c) {
            return 0;
        }
        return this.b;
    }
    
    public int f() {
        if (this.d.V() == 8) {
            return 0;
        }
        if (this.h != Integer.MIN_VALUE) {
            final ConstraintAnchor f = this.f;
            if (f != null && f.d.V() == 8) {
                return this.h;
            }
        }
        return this.g;
    }
    
    public final ConstraintAnchor g() {
        switch (ConstraintAnchor$a.a[this.e.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.e.name());
            }
            case 5: {
                return this.d.P;
            }
            case 4: {
                return this.d.R;
            }
            case 3: {
                return this.d.O;
            }
            case 2: {
                return this.d.Q;
            }
            case 1:
            case 6:
            case 7:
            case 8:
            case 9: {
                return null;
            }
        }
    }
    
    public ConstraintWidget h() {
        return this.d;
    }
    
    public SolverVariable i() {
        return this.i;
    }
    
    public ConstraintAnchor j() {
        return this.f;
    }
    
    public Type k() {
        return this.e;
    }
    
    public boolean l() {
        final HashSet a = this.a;
        if (a == null) {
            return false;
        }
        final Iterator iterator = a.iterator();
        while (iterator.hasNext()) {
            if (((ConstraintAnchor)iterator.next()).g().o()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean m() {
        final HashSet a = this.a;
        boolean b = false;
        if (a == null) {
            return false;
        }
        if (a.size() > 0) {
            b = true;
        }
        return b;
    }
    
    public boolean n() {
        return this.c;
    }
    
    public boolean o() {
        return this.f != null;
    }
    
    public boolean p(final ConstraintAnchor constraintAnchor) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        if (constraintAnchor == null) {
            return false;
        }
        final Type k = constraintAnchor.k();
        final Type e = this.e;
        if (k == e) {
            return e != Type.BASELINE || (constraintAnchor.h().Z() && this.h().Z());
        }
        switch (ConstraintAnchor$a.a[e.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.e.name());
            }
            case 7:
            case 8:
            case 9: {
                return false;
            }
            case 6: {
                return k != Type.LEFT && k != Type.RIGHT;
            }
            case 4:
            case 5: {
                boolean b4 = k == Type.TOP || k == Type.BOTTOM;
                if (constraintAnchor.h() instanceof f) {
                    boolean b5 = false;
                    Label_0219: {
                        if (!b4) {
                            b5 = b3;
                            if (k != Type.CENTER_Y) {
                                break Label_0219;
                            }
                        }
                        b5 = true;
                    }
                    b4 = b5;
                }
                return b4;
            }
            case 2:
            case 3: {
                boolean b6 = k == Type.LEFT || k == Type.RIGHT;
                if (constraintAnchor.h() instanceof f) {
                    boolean b7 = false;
                    Label_0278: {
                        if (!b6) {
                            b7 = b;
                            if (k != Type.CENTER_X) {
                                break Label_0278;
                            }
                        }
                        b7 = true;
                    }
                    b6 = b7;
                }
                return b6;
            }
            case 1: {
                boolean b8 = b2;
                if (k != Type.BASELINE) {
                    b8 = b2;
                    if (k != Type.CENTER_X) {
                        b8 = b2;
                        if (k != Type.CENTER_Y) {
                            b8 = true;
                        }
                    }
                }
                return b8;
            }
        }
    }
    
    public void q() {
        final ConstraintAnchor f = this.f;
        if (f != null) {
            final HashSet a = f.a;
            if (a != null) {
                a.remove(this);
                if (this.f.a.size() == 0) {
                    this.f.a = null;
                }
            }
        }
        this.a = null;
        this.f = null;
        this.g = 0;
        this.h = Integer.MIN_VALUE;
        this.c = false;
        this.b = 0;
    }
    
    public void r() {
        this.c = false;
        this.b = 0;
    }
    
    public void s(final oe oe) {
        final SolverVariable i = this.i;
        if (i == null) {
            this.i = new SolverVariable(SolverVariable.Type.UNRESTRICTED, null);
        }
        else {
            i.f();
        }
    }
    
    public void t(final int b) {
        this.b = b;
        this.c = true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.d.t());
        sb.append(":");
        sb.append(this.e.toString());
        return sb.toString();
    }
    
    public void u(final int h) {
        if (this.o()) {
            this.h = h;
        }
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        BASELINE, 
        BOTTOM, 
        CENTER, 
        CENTER_X, 
        CENTER_Y, 
        LEFT, 
        NONE, 
        RIGHT, 
        TOP;
    }
}

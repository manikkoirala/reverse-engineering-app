// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.SolverVariable;
import java.util.Iterator;
import java.util.HashSet;
import androidx.constraintlayout.core.widgets.analyzer.d;
import androidx.constraintlayout.core.widgets.analyzer.c;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import java.util.ArrayList;

public class ConstraintWidget
{
    public static float K0 = 0.5f;
    public int A;
    public int A0;
    public float B;
    public boolean B0;
    public int C;
    public boolean C0;
    public int D;
    public float[] D0;
    public float E;
    public ConstraintWidget[] E0;
    public int F;
    public ConstraintWidget[] F0;
    public float G;
    public ConstraintWidget G0;
    public int[] H;
    public ConstraintWidget H0;
    public float I;
    public int I0;
    public boolean J;
    public int J0;
    public boolean K;
    public boolean L;
    public int M;
    public int N;
    public ConstraintAnchor O;
    public ConstraintAnchor P;
    public ConstraintAnchor Q;
    public ConstraintAnchor R;
    public ConstraintAnchor S;
    public ConstraintAnchor T;
    public ConstraintAnchor U;
    public ConstraintAnchor V;
    public ConstraintAnchor[] W;
    public ArrayList X;
    public boolean[] Y;
    public DimensionBehaviour[] Z;
    public boolean a;
    public ConstraintWidget a0;
    public WidgetRun[] b;
    public int b0;
    public tf c;
    public int c0;
    public tf d;
    public float d0;
    public c e;
    public int e0;
    public d f;
    public int f0;
    public boolean[] g;
    public int g0;
    public boolean h;
    public int h0;
    public boolean i;
    public int i0;
    public boolean j;
    public int j0;
    public boolean k;
    public int k0;
    public int l;
    public int l0;
    public int m;
    public int m0;
    public a62 n;
    public int n0;
    public String o;
    public float o0;
    public boolean p;
    public float p0;
    public boolean q;
    public Object q0;
    public boolean r;
    public int r0;
    public boolean s;
    public int s0;
    public int t;
    public boolean t0;
    public int u;
    public String u0;
    public int v;
    public String v0;
    public int w;
    public boolean w0;
    public int x;
    public boolean x0;
    public int[] y;
    public boolean y0;
    public int z;
    public int z0;
    
    public ConstraintWidget() {
        this.a = false;
        this.b = new WidgetRun[2];
        this.e = null;
        this.f = null;
        this.g = new boolean[] { true, true };
        this.h = false;
        this.i = true;
        this.j = false;
        this.k = true;
        this.l = -1;
        this.m = -1;
        this.n = new a62(this);
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = false;
        this.t = -1;
        this.u = -1;
        this.v = 0;
        this.w = 0;
        this.x = 0;
        this.y = new int[2];
        this.z = 0;
        this.A = 0;
        this.B = 1.0f;
        this.C = 0;
        this.D = 0;
        this.E = 1.0f;
        this.F = -1;
        this.G = 1.0f;
        this.H = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE };
        this.I = 0.0f;
        this.J = false;
        this.L = false;
        this.M = 0;
        this.N = 0;
        this.O = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.P = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.Q = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.R = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.S = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.T = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.U = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        final ConstraintAnchor v = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.V = v;
        this.W = new ConstraintAnchor[] { this.O, this.Q, this.P, this.R, this.S, v };
        this.X = new ArrayList();
        this.Y = new boolean[2];
        final DimensionBehaviour fixed = DimensionBehaviour.FIXED;
        this.Z = new DimensionBehaviour[] { fixed, fixed };
        this.a0 = null;
        this.b0 = 0;
        this.c0 = 0;
        this.d0 = 0.0f;
        this.e0 = -1;
        this.f0 = 0;
        this.g0 = 0;
        this.h0 = 0;
        this.i0 = 0;
        this.j0 = 0;
        this.k0 = 0;
        this.l0 = 0;
        final float k0 = ConstraintWidget.K0;
        this.o0 = k0;
        this.p0 = k0;
        this.r0 = 0;
        this.s0 = 0;
        this.t0 = false;
        this.u0 = null;
        this.v0 = null;
        this.y0 = false;
        this.z0 = 0;
        this.A0 = 0;
        this.D0 = new float[] { -1.0f, -1.0f };
        this.E0 = new ConstraintWidget[] { null, null };
        this.F0 = new ConstraintWidget[] { null, null };
        this.G0 = null;
        this.H0 = null;
        this.I0 = -1;
        this.J0 = -1;
        this.d();
    }
    
    public DimensionBehaviour A() {
        return this.Z[0];
    }
    
    public void A0(final int l0) {
        this.l0 = l0;
        this.J = (l0 > 0);
    }
    
    public int B() {
        final ConstraintAnchor o = this.O;
        int n = 0;
        if (o != null) {
            n = 0 + o.g;
        }
        final ConstraintAnchor q = this.Q;
        int n2 = n;
        if (q != null) {
            n2 = n + q.g;
        }
        return n2;
    }
    
    public void B0(final Object q0) {
        this.q0 = q0;
    }
    
    public int C() {
        return this.M;
    }
    
    public void C0(final String u0) {
        this.u0 = u0;
    }
    
    public int D() {
        return this.N;
    }
    
    public void D0(String s) {
        Label_0267: {
            if (s == null || s.length() == 0) {
                break Label_0267;
            }
            final int length = s.length();
            final int index = s.indexOf(44);
            final int n = 0;
            final int n2 = 0;
            final int n3 = -1;
            int n4 = n;
            int e0 = n3;
            if (index > 0) {
                n4 = n;
                e0 = n3;
                if (index < length - 1) {
                    final String substring = s.substring(0, index);
                    if (substring.equalsIgnoreCase("W")) {
                        e0 = n2;
                    }
                    else if (substring.equalsIgnoreCase("H")) {
                        e0 = 1;
                    }
                    else {
                        e0 = -1;
                    }
                    n4 = index + 1;
                }
            }
            final int index2 = s.indexOf(58);
            Label_0225: {
                if (index2 < 0 || index2 >= length - 1) {
                    break Label_0225;
                }
                final String substring2 = s.substring(n4, index2);
                s = s.substring(index2 + 1);
                while (true) {
                    if (substring2.length() <= 0 || s.length() <= 0) {
                        break Label_0247;
                    }
                    try {
                        final float float1 = Float.parseFloat(substring2);
                        final float float2 = Float.parseFloat(s);
                        while (true) {
                            if (float1 > 0.0f && float2 > 0.0f) {
                                if (e0 == 1) {
                                    final float d0 = Math.abs(float2 / float1);
                                    break Label_0249;
                                }
                                final float d0 = Math.abs(float1 / float2);
                                break Label_0249;
                            }
                            float d0 = 0.0f;
                            if (d0 > 0.0f) {
                                this.d0 = d0;
                                this.e0 = e0;
                            }
                            return;
                            this.d0 = 0.0f;
                            return;
                            s = s.substring(n4);
                            iftrue(Label_0247:)(s.length() <= 0);
                            d0 = Float.parseFloat(s);
                            continue;
                        }
                    }
                    catch (final NumberFormatException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    public int E(final int n) {
        if (n == 0) {
            return this.W();
        }
        if (n == 1) {
            return this.x();
        }
        return 0;
    }
    
    public void E0(final int n) {
        if (!this.J) {
            return;
        }
        final int g0 = n - this.l0;
        final int c0 = this.c0;
        this.g0 = g0;
        this.P.t(g0);
        this.R.t(c0 + g0);
        this.S.t(n);
        this.q = true;
    }
    
    public int F() {
        return this.H[1];
    }
    
    public void F0(final int f0, final int n) {
        if (this.p) {
            return;
        }
        this.O.t(f0);
        this.Q.t(n);
        this.f0 = f0;
        this.b0 = n - f0;
        this.p = true;
    }
    
    public int G() {
        return this.H[0];
    }
    
    public void G0(final int f0) {
        this.O.t(f0);
        this.f0 = f0;
    }
    
    public int H() {
        return this.n0;
    }
    
    public void H0(final int g0) {
        this.P.t(g0);
        this.g0 = g0;
    }
    
    public int I() {
        return this.m0;
    }
    
    public void I0(final int g0, final int n) {
        if (this.q) {
            return;
        }
        this.P.t(g0);
        this.R.t(n);
        this.g0 = g0;
        this.c0 = n - g0;
        if (this.J) {
            this.S.t(g0 + this.l0);
        }
        this.q = true;
    }
    
    public ConstraintWidget J(final int n) {
        if (n == 0) {
            final ConstraintAnchor q = this.Q;
            final ConstraintAnchor f = q.f;
            if (f != null && f.f == q) {
                return f.d;
            }
        }
        else if (n == 1) {
            final ConstraintAnchor r = this.R;
            final ConstraintAnchor f2 = r.f;
            if (f2 != null && f2.f == r) {
                return f2.d;
            }
        }
        return null;
    }
    
    public void J0(int c0, int b0, int l, int c2) {
        final int n = l - c0;
        l = c2 - b0;
        this.f0 = c0;
        this.g0 = b0;
        if (this.s0 == 8) {
            this.b0 = 0;
            this.c0 = 0;
            return;
        }
        final DimensionBehaviour[] z = this.Z;
        final DimensionBehaviour dimensionBehaviour = z[0];
        final DimensionBehaviour fixed = DimensionBehaviour.FIXED;
        c0 = n;
        if (dimensionBehaviour == fixed) {
            b0 = this.b0;
            if ((c0 = n) < b0) {
                c0 = b0;
            }
        }
        b0 = l;
        if (z[1] == fixed) {
            c2 = this.c0;
            if ((b0 = l) < c2) {
                b0 = c2;
            }
        }
        this.b0 = c0;
        this.c0 = b0;
        l = this.n0;
        if (b0 < l) {
            this.c0 = l;
        }
        l = this.m0;
        if (c0 < l) {
            this.b0 = l;
        }
        l = this.A;
        if (l > 0 && dimensionBehaviour == DimensionBehaviour.MATCH_CONSTRAINT) {
            this.b0 = Math.min(this.b0, l);
        }
        l = this.D;
        if (l > 0 && this.Z[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
            this.c0 = Math.min(this.c0, l);
        }
        l = this.b0;
        if (c0 != l) {
            this.l = l;
        }
        c0 = this.c0;
        if (b0 != c0) {
            this.m = c0;
        }
    }
    
    public ConstraintWidget K() {
        return this.a0;
    }
    
    public void K0(final boolean j) {
        this.J = j;
    }
    
    public ConstraintWidget L(final int n) {
        if (n == 0) {
            final ConstraintAnchor o = this.O;
            final ConstraintAnchor f = o.f;
            if (f != null && f.f == o) {
                return f.d;
            }
        }
        else if (n == 1) {
            final ConstraintAnchor p = this.P;
            final ConstraintAnchor f2 = p.f;
            if (f2 != null && f2.f == p) {
                return f2.d;
            }
        }
        return null;
    }
    
    public void L0(final int c0) {
        this.c0 = c0;
        final int n0 = this.n0;
        if (c0 < n0) {
            this.c0 = n0;
        }
    }
    
    public int M() {
        return this.X() + this.b0;
    }
    
    public void M0(final float o0) {
        this.o0 = o0;
    }
    
    public WidgetRun N(final int n) {
        if (n == 0) {
            return this.e;
        }
        if (n == 1) {
            return this.f;
        }
        return null;
    }
    
    public void N0(final int z0) {
        this.z0 = z0;
    }
    
    public void O(final StringBuilder sb) {
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("  ");
        sb2.append(this.o);
        sb2.append(":{\n");
        sb.append(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("    actualWidth:");
        sb3.append(this.b0);
        sb.append(sb3.toString());
        sb.append("\n");
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("    actualHeight:");
        sb4.append(this.c0);
        sb.append(sb4.toString());
        sb.append("\n");
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("    actualLeft:");
        sb5.append(this.f0);
        sb.append(sb5.toString());
        sb.append("\n");
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("    actualTop:");
        sb6.append(this.g0);
        sb.append(sb6.toString());
        sb.append("\n");
        this.Q(sb, "left", this.O);
        this.Q(sb, "top", this.P);
        this.Q(sb, "right", this.Q);
        this.Q(sb, "bottom", this.R);
        this.Q(sb, "baseline", this.S);
        this.Q(sb, "centerX", this.T);
        this.Q(sb, "centerY", this.U);
        this.P(sb, "    width", this.b0, this.m0, this.H[0], this.l, this.z, this.w, this.B, this.D0[0]);
        this.P(sb, "    height", this.c0, this.n0, this.H[1], this.m, this.C, this.x, this.E, this.D0[1]);
        this.z0(sb, "    dimensionRatio", this.d0, this.e0);
        this.x0(sb, "    horizontalBias", this.o0, ConstraintWidget.K0);
        this.x0(sb, "    verticalBias", this.p0, ConstraintWidget.K0);
        this.y0(sb, "    horizontalChainStyle", this.z0, 0);
        this.y0(sb, "    verticalChainStyle", this.A0, 0);
        sb.append("  }");
    }
    
    public void O0(int n, int m0) {
        this.f0 = n;
        n = m0 - n;
        this.b0 = n;
        m0 = this.m0;
        if (n < m0) {
            this.b0 = m0;
        }
    }
    
    public final void P(final StringBuilder sb, final String str, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float n7, final float n8) {
        sb.append(str);
        sb.append(" :  {\n");
        this.y0(sb, "      size", n, 0);
        this.y0(sb, "      min", n2, 0);
        this.y0(sb, "      max", n3, Integer.MAX_VALUE);
        this.y0(sb, "      matchMin", n5, 0);
        this.y0(sb, "      matchDef", n6, 0);
        this.x0(sb, "      matchPercent", n7, 1.0f);
        sb.append("    },\n");
    }
    
    public void P0(final DimensionBehaviour dimensionBehaviour) {
        this.Z[0] = dimensionBehaviour;
    }
    
    public final void Q(final StringBuilder sb, final String str, final ConstraintAnchor constraintAnchor) {
        if (constraintAnchor.f == null) {
            return;
        }
        sb.append("    ");
        sb.append(str);
        sb.append(" : [ '");
        sb.append(constraintAnchor.f);
        sb.append("'");
        if (constraintAnchor.h != Integer.MIN_VALUE || constraintAnchor.g != 0) {
            sb.append(",");
            sb.append(constraintAnchor.g);
            if (constraintAnchor.h != Integer.MIN_VALUE) {
                sb.append(",");
                sb.append(constraintAnchor.h);
                sb.append(",");
            }
        }
        sb.append(" ] ,\n");
    }
    
    public void Q0(final int w, int n, final int n2, final float b) {
        this.w = w;
        this.z = n;
        n = n2;
        if (n2 == Integer.MAX_VALUE) {
            n = 0;
        }
        this.A = n;
        this.B = b;
        if (b > 0.0f && b < 1.0f && w == 0) {
            this.w = 2;
        }
    }
    
    public float R() {
        return this.p0;
    }
    
    public void R0(final float n) {
        this.D0[0] = n;
    }
    
    public int S() {
        return this.A0;
    }
    
    public void S0(final int n, final boolean b) {
        this.Y[n] = b;
    }
    
    public DimensionBehaviour T() {
        return this.Z[1];
    }
    
    public void T0(final boolean k) {
        this.K = k;
    }
    
    public int U() {
        final ConstraintAnchor o = this.O;
        int n = 0;
        if (o != null) {
            n = 0 + this.P.g;
        }
        int n2 = n;
        if (this.Q != null) {
            n2 = n + this.R.g;
        }
        return n2;
    }
    
    public void U0(final boolean l) {
        this.L = l;
    }
    
    public int V() {
        return this.s0;
    }
    
    public void V0(final int m, final int n) {
        this.M = m;
        this.N = n;
        this.Y0(false);
    }
    
    public int W() {
        if (this.s0 == 8) {
            return 0;
        }
        return this.b0;
    }
    
    public void W0(final int n) {
        this.H[1] = n;
    }
    
    public int X() {
        final ConstraintWidget a0 = this.a0;
        if (a0 != null && a0 instanceof androidx.constraintlayout.core.widgets.d) {
            return ((androidx.constraintlayout.core.widgets.d)a0).S0 + this.f0;
        }
        return this.f0;
    }
    
    public void X0(final int n) {
        this.H[0] = n;
    }
    
    public int Y() {
        final ConstraintWidget a0 = this.a0;
        if (a0 != null && a0 instanceof androidx.constraintlayout.core.widgets.d) {
            return ((androidx.constraintlayout.core.widgets.d)a0).T0 + this.g0;
        }
        return this.g0;
    }
    
    public void Y0(final boolean i) {
        this.i = i;
    }
    
    public boolean Z() {
        return this.J;
    }
    
    public void Z0(final int n) {
        int n2 = n;
        if (n < 0) {
            n2 = 0;
        }
        this.n0 = n2;
    }
    
    public boolean a0(int n) {
        boolean b = true;
        final boolean b2 = true;
        if (n == 0) {
            if (this.O.f != null) {
                n = 1;
            }
            else {
                n = 0;
            }
            int n2;
            if (this.Q.f != null) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            return n + n2 < 2 && b2;
        }
        if (this.P.f != null) {
            n = 1;
        }
        else {
            n = 0;
        }
        int n3;
        if (this.R.f != null) {
            n3 = 1;
        }
        else {
            n3 = 0;
        }
        int n4;
        if (this.S.f != null) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        if (n + n3 + n4 >= 2) {
            b = false;
        }
        return b;
    }
    
    public void a1(final int n) {
        int m0 = n;
        if (n < 0) {
            m0 = 0;
        }
        this.m0 = m0;
    }
    
    public boolean b0() {
        for (int size = this.X.size(), i = 0; i < size; ++i) {
            if (((ConstraintAnchor)this.X.get(i)).m()) {
                return true;
            }
        }
        return false;
    }
    
    public void b1(final int f0, final int g0) {
        this.f0 = f0;
        this.g0 = g0;
    }
    
    public boolean c0() {
        return this.l != -1 || this.m != -1;
    }
    
    public void c1(final ConstraintWidget a0) {
        this.a0 = a0;
    }
    
    public final void d() {
        this.X.add(this.O);
        this.X.add(this.P);
        this.X.add(this.Q);
        this.X.add(this.R);
        this.X.add(this.T);
        this.X.add(this.U);
        this.X.add(this.V);
        this.X.add(this.S);
    }
    
    public boolean d0(final int n, final int n2) {
        boolean b = true;
        final boolean b2 = true;
        if (n == 0) {
            final ConstraintAnchor f = this.O.f;
            if (f != null && f.n()) {
                final ConstraintAnchor f2 = this.Q.f;
                if (f2 != null && f2.n()) {
                    return this.Q.f.e() - this.Q.f() - (this.O.f.e() + this.O.f()) >= n2 && b2;
                }
            }
        }
        else {
            final ConstraintAnchor f3 = this.P.f;
            if (f3 != null && f3.n()) {
                final ConstraintAnchor f4 = this.R.f;
                if (f4 != null && f4.n()) {
                    if (this.R.f.e() - this.R.f() - (this.P.f.e() + this.P.f()) < n2) {
                        b = false;
                    }
                    return b;
                }
            }
        }
        return false;
    }
    
    public void d1(final float p) {
        this.p0 = p;
    }
    
    public void e(final androidx.constraintlayout.core.widgets.d d, final androidx.constraintlayout.core.c c, final HashSet set, final int n, final boolean b) {
        if (b) {
            if (!set.contains(this)) {
                return;
            }
            androidx.constraintlayout.core.widgets.g.a(d, c, this);
            set.remove(this);
            this.g(c, d.U1(64));
        }
        if (n == 0) {
            final HashSet d2 = this.O.d();
            if (d2 != null) {
                final Iterator iterator = d2.iterator();
                while (iterator.hasNext()) {
                    ((ConstraintAnchor)iterator.next()).d.e(d, c, set, n, true);
                }
            }
            final HashSet d3 = this.Q.d();
            if (d3 != null) {
                final Iterator iterator2 = d3.iterator();
                while (iterator2.hasNext()) {
                    ((ConstraintAnchor)iterator2.next()).d.e(d, c, set, n, true);
                }
            }
        }
        else {
            final HashSet d4 = this.P.d();
            if (d4 != null) {
                final Iterator iterator3 = d4.iterator();
                while (iterator3.hasNext()) {
                    ((ConstraintAnchor)iterator3.next()).d.e(d, c, set, n, true);
                }
            }
            final HashSet d5 = this.R.d();
            if (d5 != null) {
                final Iterator iterator4 = d5.iterator();
                while (iterator4.hasNext()) {
                    ((ConstraintAnchor)iterator4.next()).d.e(d, c, set, n, true);
                }
            }
            final HashSet d6 = this.S.d();
            if (d6 != null) {
                final Iterator iterator5 = d6.iterator();
                while (iterator5.hasNext()) {
                    ((ConstraintAnchor)iterator5.next()).d.e(d, c, set, n, true);
                }
            }
        }
    }
    
    public void e0(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2, final int n, final int n2) {
        this.o(type).b(constraintWidget.o(type2), n, n2, true);
    }
    
    public void e1(final int a0) {
        this.A0 = a0;
    }
    
    public boolean f() {
        return this instanceof h || this instanceof f;
    }
    
    public final boolean f0(int n) {
        n *= 2;
        final ConstraintAnchor[] w = this.W;
        final ConstraintAnchor constraintAnchor = w[n];
        final ConstraintAnchor f = constraintAnchor.f;
        if (f != null && f.f != constraintAnchor) {
            final boolean b = true;
            final ConstraintAnchor constraintAnchor2 = w[n + 1];
            final ConstraintAnchor f2 = constraintAnchor2.f;
            if (f2 != null && f2.f == constraintAnchor2) {
                return b;
            }
        }
        return false;
    }
    
    public void f1(int n, int n2) {
        this.g0 = n;
        n = n2 - n;
        this.c0 = n;
        n2 = this.n0;
        if (n < n2) {
            this.c0 = n2;
        }
    }
    
    public void g(final androidx.constraintlayout.core.c c, final boolean b) {
        final SolverVariable q = c.q(this.O);
        final SolverVariable q2 = c.q(this.Q);
        SolverVariable q3 = c.q(this.P);
        SolverVariable q4 = c.q(this.R);
        final SolverVariable q5 = c.q(this.S);
        final ConstraintWidget a0 = this.a0;
        boolean b2 = false;
        boolean b3 = false;
        Label_0160: {
            if (a0 != null) {
                b2 = (a0 != null && a0.Z[0] == DimensionBehaviour.WRAP_CONTENT);
                b3 = (a0 != null && a0.Z[1] == DimensionBehaviour.WRAP_CONTENT);
                final int v = this.v;
                if (v == 1) {
                    b3 = false;
                    break Label_0160;
                }
                if (v == 2) {
                    b2 = false;
                    break Label_0160;
                }
                if (v != 3) {
                    break Label_0160;
                }
            }
            b2 = false;
            b3 = false;
        }
        if (this.s0 == 8 && !this.t0 && !this.b0()) {
            final boolean[] y = this.Y;
            if (!y[0] && !y[1]) {
                return;
            }
        }
        final boolean p2 = this.p;
        if (p2 || this.q) {
            if (p2) {
                c.f(q, this.f0);
                c.f(q2, this.f0 + this.b0);
                if (b2) {
                    final ConstraintWidget a2 = this.a0;
                    if (a2 != null) {
                        if (this.k) {
                            final androidx.constraintlayout.core.widgets.d d = (androidx.constraintlayout.core.widgets.d)a2;
                            d.z1(this.O);
                            d.y1(this.Q);
                        }
                        else {
                            c.h(c.q(a2.Q), q2, 0, 5);
                        }
                    }
                }
            }
            if (this.q) {
                c.f(q3, this.g0);
                c.f(q4, this.g0 + this.c0);
                if (this.S.m()) {
                    c.f(q5, this.g0 + this.l0);
                }
                if (b3) {
                    final ConstraintWidget a3 = this.a0;
                    if (a3 != null) {
                        if (this.k) {
                            final androidx.constraintlayout.core.widgets.d d2 = (androidx.constraintlayout.core.widgets.d)a3;
                            d2.E1(this.P);
                            d2.D1(this.R);
                        }
                        else {
                            c.h(c.q(a3.R), q4, 0, 5);
                        }
                    }
                }
            }
            if (this.p && this.q) {
                this.p = false;
                this.q = false;
                return;
            }
        }
        final boolean r = androidx.constraintlayout.core.c.r;
        if (b) {
            final c e = this.e;
            if (e != null) {
                final d f = this.f;
                if (f != null) {
                    final DependencyNode h = e.h;
                    if (h.j && e.i.j && f.h.j && f.i.j) {
                        c.f(q, h.g);
                        c.f(q2, this.e.i.g);
                        c.f(q3, this.f.h.g);
                        c.f(q4, this.f.i.g);
                        c.f(q5, this.f.k.g);
                        if (this.a0 != null) {
                            if (b2 && this.g[0] && !this.i0()) {
                                c.h(c.q(this.a0.Q), q2, 0, 8);
                            }
                            if (b3 && this.g[1] && !this.k0()) {
                                c.h(c.q(this.a0.R), q4, 0, 8);
                            }
                        }
                        this.p = false;
                        this.q = false;
                        return;
                    }
                }
            }
        }
        int n;
        int n2;
        if (this.a0 != null) {
            int i0;
            if (this.f0(0)) {
                ((androidx.constraintlayout.core.widgets.d)this.a0).v1(this, 0);
                i0 = 1;
            }
            else {
                i0 = (this.i0() ? 1 : 0);
            }
            int k0;
            if (this.f0(1)) {
                ((androidx.constraintlayout.core.widgets.d)this.a0).v1(this, 1);
                k0 = 1;
            }
            else {
                k0 = (this.k0() ? 1 : 0);
            }
            if (i0 == 0 && b2 && this.s0 != 8 && this.O.f == null && this.Q.f == null) {
                c.h(c.q(this.a0.Q), q2, 0, 1);
            }
            if (k0 == 0 && b3 && this.s0 != 8 && this.P.f == null && this.R.f == null && this.S == null) {
                c.h(c.q(this.a0.R), q4, 0, 1);
            }
            n = k0;
            n2 = i0;
        }
        else {
            n = 0;
            n2 = 0;
        }
        final int b4 = this.b0;
        int m0 = this.m0;
        if (b4 >= m0) {
            m0 = b4;
        }
        final int c2 = this.c0;
        int n3 = this.n0;
        if (c2 >= n3) {
            n3 = c2;
        }
        final DimensionBehaviour[] z = this.Z;
        final DimensionBehaviour dimensionBehaviour = z[0];
        final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
        final boolean b5 = dimensionBehaviour != match_CONSTRAINT;
        final DimensionBehaviour dimensionBehaviour2 = z[1];
        final boolean b6 = dimensionBehaviour2 != match_CONSTRAINT;
        final int e2 = this.e0;
        this.F = e2;
        final float d3 = this.d0;
        this.G = d3;
        int w = this.w;
        final int x = this.x;
        int n8 = 0;
        boolean h2 = false;
        int n10 = 0;
        int n11 = 0;
        Label_1374: {
            Label_1359: {
                if (d3 > 0.0f && this.s0 != 8) {
                    int n4 = w;
                    if (dimensionBehaviour == match_CONSTRAINT && (n4 = w) == 0) {
                        n4 = 3;
                    }
                    int n5 = x;
                    if (dimensionBehaviour2 == match_CONSTRAINT && (n5 = x) == 0) {
                        n5 = 3;
                    }
                    Label_1337: {
                        int n12 = 0;
                        Label_1329: {
                            if (dimensionBehaviour == match_CONSTRAINT && dimensionBehaviour2 == match_CONSTRAINT && n4 == 3 && n5 == 3) {
                                this.o1(b2, b3, b5, b6);
                            }
                            else if (dimensionBehaviour == match_CONSTRAINT && n4 == 3) {
                                this.F = 0;
                                final int n6 = (int)(d3 * c2);
                                if (dimensionBehaviour2 != match_CONSTRAINT) {
                                    final int n7 = 4;
                                    n8 = n5;
                                    final int n9 = n3;
                                    h2 = false;
                                    n10 = n6;
                                    n11 = n9;
                                    w = n7;
                                    break Label_1374;
                                }
                                n12 = n6;
                                break Label_1329;
                            }
                            else if (dimensionBehaviour2 == match_CONSTRAINT && n5 == 3) {
                                this.F = 1;
                                if (e2 == -1) {
                                    this.G = 1.0f / d3;
                                }
                                n3 = (int)(this.G * b4);
                                if (dimensionBehaviour != match_CONSTRAINT) {
                                    final int n13 = 4;
                                    w = n4;
                                    n8 = n13;
                                    break Label_1359;
                                }
                                final int n14 = n3;
                                n10 = m0;
                                n11 = n14;
                                break Label_1337;
                            }
                            n12 = m0;
                        }
                        n11 = n3;
                        n10 = n12;
                    }
                    final int n15 = n4;
                    n8 = n5;
                    h2 = true;
                    w = n15;
                    break Label_1374;
                }
                n8 = x;
            }
            final int n16 = m0;
            h2 = false;
            n11 = n3;
            n10 = n16;
        }
        final int[] y2 = this.y;
        y2[0] = w;
        y2[1] = n8;
        this.h = h2;
        boolean b7 = false;
        Label_1429: {
            if (h2) {
                final int f2 = this.F;
                if (f2 == 0 || f2 == -1) {
                    b7 = true;
                    break Label_1429;
                }
            }
            b7 = false;
        }
        boolean b8 = false;
        Label_1461: {
            if (h2) {
                final int f3 = this.F;
                if (f3 == 1 || f3 == -1) {
                    b8 = true;
                    break Label_1461;
                }
            }
            b8 = false;
        }
        final DimensionBehaviour dimensionBehaviour3 = this.Z[0];
        final DimensionBehaviour wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
        final boolean b9 = dimensionBehaviour3 == wrap_CONTENT && this instanceof androidx.constraintlayout.core.widgets.d;
        if (b9) {
            n10 = 0;
        }
        final boolean b10 = this.V.o() ^ true;
        final boolean[] y3 = this.Y;
        final boolean b11 = y3[0];
        final boolean b12 = y3[1];
        Label_1876: {
            if (this.t != 2 && !this.p) {
                if (b) {
                    final c e3 = this.e;
                    if (e3 != null) {
                        final DependencyNode h3 = e3.h;
                        if (h3.j) {
                            if (e3.i.j) {
                                if (b) {
                                    c.f(q, h3.g);
                                    c.f(q2, this.e.i.g);
                                    if (this.a0 != null && b2 && this.g[0] && !this.i0()) {
                                        c.h(c.q(this.a0.Q), q2, 0, 8);
                                    }
                                }
                                break Label_1876;
                            }
                        }
                    }
                }
                final ConstraintWidget a4 = this.a0;
                SolverVariable q6;
                if (a4 != null) {
                    q6 = c.q(a4.Q);
                }
                else {
                    q6 = null;
                }
                final ConstraintWidget a5 = this.a0;
                SolverVariable q7;
                if (a5 != null) {
                    q7 = c.q(a5.O);
                }
                else {
                    q7 = null;
                }
                final boolean b13 = this.g[0];
                final DimensionBehaviour[] z2 = this.Z;
                this.i(c, true, b2, b3, b13, q7, q6, z2[0], b9, this.O, this.Q, this.f0, n10, this.m0, this.H[0], this.o0, b7, z2[1] == match_CONSTRAINT, (boolean)(n2 != 0), (boolean)(n != 0), b11, w, n8, this.z, this.A, this.B, b10);
            }
        }
        boolean b14 = false;
        Label_2034: {
            if (b) {
                final d f4 = this.f;
                if (f4 != null) {
                    final DependencyNode h4 = f4.h;
                    if (h4.j && f4.i.j) {
                        c.f(q3, h4.g);
                        c.f(q4, this.f.i.g);
                        c.f(q5, this.f.k.g);
                        final ConstraintWidget a6 = this.a0;
                        if (a6 != null && n == 0 && b3) {
                            if (this.g[1]) {
                                c.h(c.q(a6.R), q4, 0, 8);
                            }
                        }
                        b14 = false;
                        break Label_2034;
                    }
                }
            }
            b14 = true;
        }
        if (this.u == 2) {
            b14 = false;
        }
        if (b14 && !this.q) {
            final boolean b15 = this.Z[1] == wrap_CONTENT && this instanceof androidx.constraintlayout.core.widgets.d;
            if (b15) {
                n11 = 0;
            }
            final ConstraintWidget a7 = this.a0;
            SolverVariable q8;
            if (a7 != null) {
                q8 = c.q(a7.R);
            }
            else {
                q8 = null;
            }
            final ConstraintWidget a8 = this.a0;
            SolverVariable q9;
            if (a8 != null) {
                q9 = c.q(a8.P);
            }
            else {
                q9 = null;
            }
            boolean b16 = false;
            Label_2289: {
                if (this.l0 > 0 || this.s0 == 8) {
                    final ConstraintAnchor s = this.S;
                    if (s.f != null) {
                        c.e(q5, q3, this.p(), 8);
                        c.e(q5, c.q(this.S.f), this.S.f(), 8);
                        if (b3) {
                            c.h(q8, c.q(this.R), 0, 5);
                        }
                        b16 = false;
                        break Label_2289;
                    }
                    int n17;
                    if (this.s0 == 8) {
                        n17 = s.f();
                    }
                    else {
                        n17 = this.p();
                    }
                    c.e(q5, q3, n17, 8);
                }
                b16 = b10;
            }
            final boolean b17 = this.g[1];
            final DimensionBehaviour[] z3 = this.Z;
            this.i(c, false, b3, b2, b17, q9, q8, z3[1], b15, this.P, this.R, this.g0, n11, this.n0, this.H[1], this.p0, b8, z3[0] == match_CONSTRAINT, (boolean)(n != 0), (boolean)(n2 != 0), b12, n8, w, this.C, this.D, this.E, b16);
        }
        if (h2) {
            final int f5 = this.F;
            final float g = this.G;
            SolverVariable solverVariable;
            SolverVariable solverVariable2;
            if (f5 == 1) {
                solverVariable = q3;
                solverVariable2 = q2;
                q3 = q;
            }
            else {
                solverVariable2 = q4;
                solverVariable = q;
                q4 = q2;
            }
            c.k(q4, solverVariable, solverVariable2, q3, g, 8);
        }
        if (this.V.o()) {
            c.b(this, this.V.j().h(), (float)Math.toRadians(this.I + 90.0f), this.V.f());
        }
        this.p = false;
        this.q = false;
    }
    
    public boolean g0() {
        return this.r;
    }
    
    public void g1(final DimensionBehaviour dimensionBehaviour) {
        this.Z[1] = dimensionBehaviour;
    }
    
    public boolean h() {
        return this.s0 != 8;
    }
    
    public boolean h0(final int n) {
        return this.Y[n];
    }
    
    public void h1(final int x, int n, final int n2, final float e) {
        this.x = x;
        this.C = n;
        n = n2;
        if (n2 == Integer.MAX_VALUE) {
            n = 0;
        }
        this.D = n;
        this.E = e;
        if (e > 0.0f && e < 1.0f && x == 0) {
            this.x = 2;
        }
    }
    
    public final void i(final androidx.constraintlayout.core.c c, final boolean b, boolean b2, final boolean b3, boolean b4, final SolverVariable solverVariable, final SolverVariable solverVariable2, final DimensionBehaviour dimensionBehaviour, final boolean b5, final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, int n, int b6, final int n2, int n3, final float n4, final boolean b7, final boolean b8, final boolean b9, final boolean b10, final boolean b11, int n5, int n6, int n7, int a, final float n8, final boolean b12) {
        final SolverVariable q = c.q(constraintAnchor);
        final SolverVariable q2 = c.q(constraintAnchor2);
        final SolverVariable q3 = c.q(constraintAnchor.j());
        final SolverVariable q4 = c.q(constraintAnchor2.j());
        androidx.constraintlayout.core.c.x();
        final int o = constraintAnchor.o() ? 1 : 0;
        final boolean o2 = constraintAnchor2.o();
        final boolean o3 = this.V.o();
        int n9;
        if (o2) {
            n9 = o + 1;
        }
        else {
            n9 = o;
        }
        int n10 = n9;
        if (o3) {
            n10 = n9 + 1;
        }
        int n11;
        if (b7) {
            n11 = 3;
        }
        else {
            n11 = n5;
        }
        n5 = ConstraintWidget$a.b[dimensionBehaviour.ordinal()];
        int n12;
        if (n5 != 1 && n5 != 2 && n5 != 3 && n5 == 4 && n11 != 4) {
            n12 = 1;
        }
        else {
            n12 = 0;
        }
        final int l = this.l;
        n5 = n12;
        int n13 = b6;
        if (l != -1) {
            n5 = n12;
            n13 = b6;
            if (b) {
                this.l = -1;
                n13 = l;
                n5 = 0;
            }
        }
        b6 = this.m;
        if (b6 != -1 && !b) {
            this.m = -1;
            final int n14 = 0;
            n5 = b6;
            b6 = n14;
        }
        else {
            b6 = n5;
            n5 = n13;
        }
        if (this.s0 == 8) {
            n5 = 0;
            b6 = 0;
        }
        if (b12) {
            if (o == 0 && !o2 && !o3) {
                c.f(q, n);
            }
            else if (o != 0 && !o2) {
                c.e(q, q3, constraintAnchor.f(), 8);
            }
        }
        int n15 = 0;
        Label_0862: {
            if (b6 == 0) {
                if (b5) {
                    c.e(q2, q, 0, 3);
                    if (n2 > 0) {
                        c.h(q2, q, n2, 8);
                    }
                    if (n3 < Integer.MAX_VALUE) {
                        c.j(q2, q, n3, 8);
                    }
                }
                else {
                    c.e(q2, q, n5, 8);
                }
                n15 = b6;
            }
            else if (n10 != 2 && !b7 && (n11 == 1 || n11 == 0)) {
                b6 = (n = Math.max(n7, n5));
                if (a > 0) {
                    n = Math.min(a, b6);
                }
                c.e(q2, q, n, 8);
                n15 = 0;
            }
            else {
                if (n7 == -2) {
                    n = n5;
                }
                else {
                    n = n7;
                }
                if (a == -2) {
                    n3 = n5;
                }
                else {
                    n3 = a;
                }
                n7 = n5;
                if (n5 > 0) {
                    n7 = n5;
                    if (n11 != 1) {
                        n7 = 0;
                    }
                }
                n5 = n7;
                if (n > 0) {
                    c.h(q2, q, n, 8);
                    n5 = Math.max(n7, n);
                }
                if (n3 > 0) {
                    if (b2 && n11 == 1) {
                        n7 = 0;
                    }
                    else {
                        n7 = 1;
                    }
                    if (n7 != 0) {
                        c.j(q2, q, n3, 8);
                    }
                    n5 = Math.min(n5, n3);
                }
                if (n11 == 1) {
                    if (b2) {
                        c.e(q2, q, n5, 8);
                    }
                    else {
                        c.e(q2, q, n5, 5);
                        c.j(q2, q, n5, 8);
                    }
                    a = n;
                    n = n3;
                    n15 = b6;
                    break Label_0862;
                }
                if (n11 == 2) {
                    final ConstraintAnchor.Type k = constraintAnchor.k();
                    final ConstraintAnchor.Type top = ConstraintAnchor.Type.TOP;
                    SolverVariable solverVariable3;
                    ConstraintWidget constraintWidget;
                    ConstraintAnchor.Type type;
                    if (k != top && constraintAnchor.k() != ConstraintAnchor.Type.BOTTOM) {
                        solverVariable3 = c.q(this.a0.o(ConstraintAnchor.Type.LEFT));
                        constraintWidget = this.a0;
                        type = ConstraintAnchor.Type.RIGHT;
                    }
                    else {
                        solverVariable3 = c.q(this.a0.o(top));
                        constraintWidget = this.a0;
                        type = ConstraintAnchor.Type.BOTTOM;
                    }
                    c.d(c.r().k(q2, q, c.q(constraintWidget.o(type)), solverVariable3, n8));
                    if (b2) {
                        b6 = 0;
                    }
                    a = n;
                    n = n3;
                    n15 = b6;
                    break Label_0862;
                }
                b4 = true;
                n15 = b6;
                a = n;
                n = n3;
                break Label_0862;
            }
            n = a;
            a = n7;
        }
        if (b12 && !b9) {
            Label_2271: {
                Label_0890: {
                    if (o != 0 || o2 || o3) {
                        boolean b13;
                        if (o != 0 && !o2) {
                            final ConstraintWidget d = constraintAnchor.f.d;
                            if (b2 && d instanceof a) {
                                n = 8;
                            }
                            else {
                                n = 5;
                            }
                            b13 = b2;
                            b6 = n;
                        }
                        else if (o == 0 && o2) {
                            c.e(q2, q4, -constraintAnchor2.f(), 8);
                            if (b2) {
                                if (this.j && q.g) {
                                    final ConstraintWidget a2 = this.a0;
                                    if (a2 != null) {
                                        final androidx.constraintlayout.core.widgets.d d2 = (androidx.constraintlayout.core.widgets.d)a2;
                                        if (b) {
                                            d2.z1(constraintAnchor);
                                            break Label_0890;
                                        }
                                        d2.E1(constraintAnchor);
                                        break Label_0890;
                                    }
                                }
                                c.h(q, solverVariable, 0, 5);
                            }
                            break Label_0890;
                        }
                        else {
                            if (o == 0 || !o2) {
                                break Label_0890;
                            }
                            final ConstraintWidget d3 = constraintAnchor.f.d;
                            final ConstraintWidget d4 = constraintAnchor2.f.d;
                            final ConstraintWidget i = this.K();
                            Label_1673: {
                                Label_1660: {
                                    if (n15 != 0) {
                                        if (n11 == 0) {
                                            if (n == 0 && a == 0) {
                                                if (q3.g && q4.g) {
                                                    c.e(q, q3, constraintAnchor.f(), 8);
                                                    c.e(q2, q4, -constraintAnchor2.f(), 8);
                                                    return;
                                                }
                                                n = 8;
                                                n6 = 8;
                                                n3 = 0;
                                                b6 = 0;
                                                n5 = 1;
                                            }
                                            else {
                                                n5 = 0;
                                                n = 5;
                                                n6 = 5;
                                                n3 = 1;
                                                b6 = 1;
                                            }
                                            if (d3 instanceof a || d4 instanceof a) {
                                                n6 = 4;
                                            }
                                            final int n16 = n5;
                                            n7 = n3;
                                            n5 = b6;
                                            n3 = 6;
                                            b6 = n6;
                                            n6 = n16;
                                            break Label_1673;
                                        }
                                        if (n11 == 2) {
                                            if (!(d3 instanceof a) && !(d4 instanceof a)) {
                                                b6 = 5;
                                            }
                                            else {
                                                b6 = 4;
                                            }
                                            n = 5;
                                        }
                                        else if (n11 == 1) {
                                            n = 8;
                                            b6 = 4;
                                        }
                                        else {
                                            if (n11 != 3) {
                                                n5 = 0;
                                                n7 = 0;
                                                break Label_1660;
                                            }
                                            if (this.F == -1) {
                                                if (b10) {
                                                    if (b2) {
                                                        n3 = 5;
                                                    }
                                                    else {
                                                        n3 = 4;
                                                    }
                                                }
                                                else {
                                                    n3 = 8;
                                                }
                                                n = 8;
                                                b6 = 5;
                                                n5 = 1;
                                                n7 = 1;
                                                n6 = 1;
                                                break Label_1673;
                                            }
                                            if (b7) {
                                                if (n6 != 2 && n6 != 1) {
                                                    n = 0;
                                                }
                                                else {
                                                    n = 1;
                                                }
                                                if (n == 0) {
                                                    b6 = 8;
                                                    n = 5;
                                                }
                                                else {
                                                    b6 = 5;
                                                    n = 4;
                                                }
                                                n5 = b6;
                                                final int n17 = 1;
                                                n7 = (n6 = 1);
                                                n3 = 6;
                                                b6 = n;
                                                n = n5;
                                                n5 = n17;
                                                break Label_1673;
                                            }
                                            if (n > 0) {
                                                b6 = 5;
                                            }
                                            else if (n == 0 && a == 0) {
                                                if (b10) {
                                                    if (d3 != i && d4 != i) {
                                                        n = 4;
                                                    }
                                                    else {
                                                        n = 5;
                                                    }
                                                    n5 = 1;
                                                    n7 = (n6 = 1);
                                                    n3 = 6;
                                                    b6 = 4;
                                                    break Label_1673;
                                                }
                                                b6 = 8;
                                            }
                                            else {
                                                b6 = 4;
                                            }
                                            n6 = 1;
                                            n7 = 1;
                                            n5 = 1;
                                            n3 = 6;
                                            n = 5;
                                            break Label_1673;
                                        }
                                        n6 = 0;
                                        n3 = 6;
                                        n5 = 1;
                                        n7 = 1;
                                        break Label_1673;
                                    }
                                    else {
                                        if (q3.g && q4.g) {
                                            c.c(q, q3, constraintAnchor.f(), n4, q4, q2, constraintAnchor2.f(), 8);
                                            if (b2 && b4) {
                                                if (constraintAnchor2.f != null) {
                                                    n = constraintAnchor2.f();
                                                }
                                                else {
                                                    n = 0;
                                                }
                                                if (q4 != solverVariable2) {
                                                    c.h(solverVariable2, q2, n, 5);
                                                }
                                            }
                                            return;
                                        }
                                        n5 = 1;
                                        n7 = 1;
                                    }
                                }
                                n = 5;
                                b6 = 4;
                                n3 = 6;
                                n6 = 0;
                            }
                            boolean b14;
                            if (n5 != 0 && q3 == q4 && d3 != i) {
                                n5 = 0;
                                b14 = false;
                            }
                            else {
                                b14 = true;
                            }
                            if (n7 != 0) {
                                if (n15 == 0 && !b8 && !b10 && q3 == solverVariable && q4 == solverVariable2) {
                                    n = 8;
                                    n3 = 8;
                                    b2 = false;
                                    b14 = false;
                                }
                                c.c(q, q3, constraintAnchor.f(), n4, q4, q2, constraintAnchor2.f(), n3);
                            }
                            if (this.s0 == 8 && !constraintAnchor2.m()) {
                                return;
                            }
                            if (n5 != 0) {
                                if (b2 && q3 != q4 && n15 == 0 && (d3 instanceof a || d4 instanceof a)) {
                                    n = 6;
                                }
                                c.h(q, q3, constraintAnchor.f(), n);
                                c.j(q2, q4, -constraintAnchor2.f(), n);
                            }
                            if (b2 && b11 && !(d3 instanceof a) && !(d4 instanceof a) && d4 != i) {
                                n = 6;
                                n3 = 6;
                                b14 = true;
                            }
                            else {
                                n3 = n;
                                n = b6;
                            }
                            if (b14) {
                                if (n6 != 0 && (!b10 || b3)) {
                                    if (d3 != i && d4 != i) {
                                        b6 = n;
                                    }
                                    else {
                                        b6 = 6;
                                    }
                                    if (d3 instanceof f || d4 instanceof f) {
                                        b6 = 5;
                                    }
                                    if (d3 instanceof a || d4 instanceof a) {
                                        b6 = 5;
                                    }
                                    if (b10) {
                                        b6 = 5;
                                    }
                                    b6 = Math.max(b6, n);
                                }
                                else {
                                    b6 = n;
                                }
                                n = b6;
                                Label_2102: {
                                    if (b2) {
                                        b6 = (n = Math.min(n3, b6));
                                        if (b7) {
                                            n = b6;
                                            if (!b10) {
                                                if (d3 != i) {
                                                    n = b6;
                                                    if (d4 != i) {
                                                        break Label_2102;
                                                    }
                                                }
                                                n = 4;
                                            }
                                        }
                                    }
                                }
                                c.e(q, q3, constraintAnchor.f(), n);
                                c.e(q2, q4, -constraintAnchor2.f(), n);
                            }
                            if (b2) {
                                if (solverVariable == q3) {
                                    n = constraintAnchor.f();
                                }
                                else {
                                    n = 0;
                                }
                                if (q3 != solverVariable) {
                                    c.h(q, solverVariable, n, 5);
                                }
                            }
                            n = (b6 = 5);
                            if (b13 = b2) {
                                b6 = n;
                                b13 = b2;
                                if (n15 != 0) {
                                    b6 = n;
                                    b13 = b2;
                                    if (n2 == 0) {
                                        b6 = n;
                                        b13 = b2;
                                        if (a == 0) {
                                            if (n15 != 0 && n11 == 3) {
                                                c.h(q2, q, 0, 8);
                                                break Label_2271;
                                            }
                                            c.h(q2, q, 0, 5);
                                            break Label_2271;
                                        }
                                    }
                                }
                            }
                        }
                        n = b6;
                        b2 = b13;
                        break Label_2271;
                    }
                }
                n = 5;
            }
            b6 = 0;
            if (b2 && b4) {
                if (constraintAnchor2.f != null) {
                    b6 = constraintAnchor2.f();
                }
                if (q4 != solverVariable2) {
                    if (this.j && q2.g) {
                        final ConstraintWidget a3 = this.a0;
                        if (a3 != null) {
                            final androidx.constraintlayout.core.widgets.d d5 = (androidx.constraintlayout.core.widgets.d)a3;
                            if (b) {
                                d5.y1(constraintAnchor2);
                            }
                            else {
                                d5.D1(constraintAnchor2);
                            }
                            return;
                        }
                    }
                    c.h(solverVariable2, q2, b6, n);
                }
            }
            return;
        }
        if (n10 < 2 && b2 && b4) {
            c.h(q, solverVariable, 0, 8);
            if (!b && this.S.f != null) {
                b6 = 0;
            }
            else {
                b6 = 1;
            }
            n = b6;
            Label_2508: {
                if (!b) {
                    final ConstraintAnchor f = this.S.f;
                    n = b6;
                    if (f != null) {
                        final ConstraintWidget d6 = f.d;
                        if (d6.d0 != 0.0f) {
                            final DimensionBehaviour[] z = d6.Z;
                            final DimensionBehaviour dimensionBehaviour2 = z[0];
                            final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
                            if (dimensionBehaviour2 == match_CONSTRAINT && z[1] == match_CONSTRAINT) {
                                n = 1;
                                break Label_2508;
                            }
                        }
                        n = 0;
                    }
                }
            }
            if (n != 0) {
                c.h(solverVariable2, q2, 0, 8);
            }
        }
    }
    
    public boolean i0() {
        final ConstraintAnchor o = this.O;
        final ConstraintAnchor f = o.f;
        if (f == null || f.f != o) {
            final ConstraintAnchor q = this.Q;
            final ConstraintAnchor f2 = q.f;
            if (f2 == null || f2.f != q) {
                return false;
            }
        }
        return true;
    }
    
    public void i1(final float n) {
        this.D0[1] = n;
    }
    
    public void j(ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2, int n) {
        final ConstraintAnchor.Type center = ConstraintAnchor.Type.CENTER;
        ConstraintAnchor constraintAnchor = null;
        ConstraintAnchor constraintAnchor2 = null;
        Label_0391: {
            if (type == center) {
                type = ConstraintAnchor.Type.LEFT;
                if (type2 == center) {
                    final ConstraintAnchor o = this.o(type);
                    final ConstraintAnchor.Type right = ConstraintAnchor.Type.RIGHT;
                    final ConstraintAnchor o2 = this.o(right);
                    final ConstraintAnchor.Type top = ConstraintAnchor.Type.TOP;
                    final ConstraintAnchor o3 = this.o(top);
                    final ConstraintAnchor.Type bottom = ConstraintAnchor.Type.BOTTOM;
                    final ConstraintAnchor o4 = this.o(bottom);
                    boolean b = true;
                    if ((o != null && o.o()) || (o2 != null && o2.o())) {
                        n = 0;
                    }
                    else {
                        this.j(type, constraintWidget, type, 0);
                        this.j(right, constraintWidget, right, 0);
                        n = 1;
                    }
                    if ((o3 != null && o3.o()) || (o4 != null && o4.o())) {
                        b = false;
                    }
                    else {
                        this.j(top, constraintWidget, top, 0);
                        this.j(bottom, constraintWidget, bottom, 0);
                    }
                    if (n != 0 && b) {
                        constraintAnchor = this.o(center);
                        constraintAnchor2 = constraintWidget.o(center);
                        break Label_0391;
                    }
                    if (n != 0) {
                        type = ConstraintAnchor.Type.CENTER_X;
                    }
                    else {
                        if (!b) {
                            return;
                        }
                        type = ConstraintAnchor.Type.CENTER_Y;
                    }
                    this.o(type).a(constraintWidget.o(type), 0);
                    return;
                }
                else {
                    if (type2 != type && type2 != ConstraintAnchor.Type.RIGHT) {
                        type = ConstraintAnchor.Type.TOP;
                        if (type2 != type && type2 != ConstraintAnchor.Type.BOTTOM) {
                            return;
                        }
                        this.j(type, constraintWidget, type2, 0);
                        type = ConstraintAnchor.Type.BOTTOM;
                    }
                    else {
                        this.j(type, constraintWidget, type2, 0);
                        type = ConstraintAnchor.Type.RIGHT;
                    }
                    this.j(type, constraintWidget, type2, 0);
                    constraintAnchor = this.o(center);
                }
            }
            else {
                final ConstraintAnchor.Type center_X = ConstraintAnchor.Type.CENTER_X;
                if (type == center_X) {
                    final ConstraintAnchor.Type left = ConstraintAnchor.Type.LEFT;
                    if (type2 == left || type2 == ConstraintAnchor.Type.RIGHT) {
                        final ConstraintAnchor o5 = this.o(left);
                        constraintAnchor2 = constraintWidget.o(type2);
                        final ConstraintAnchor o6 = this.o(ConstraintAnchor.Type.RIGHT);
                        o5.a(constraintAnchor2, 0);
                        o6.a(constraintAnchor2, 0);
                        constraintAnchor = this.o(center_X);
                        break Label_0391;
                    }
                }
                final ConstraintAnchor.Type center_Y = ConstraintAnchor.Type.CENTER_Y;
                if (type == center_Y) {
                    final ConstraintAnchor.Type top2 = ConstraintAnchor.Type.TOP;
                    if (type2 == top2 || type2 == ConstraintAnchor.Type.BOTTOM) {
                        final ConstraintAnchor o7 = constraintWidget.o(type2);
                        this.o(top2).a(o7, 0);
                        this.o(ConstraintAnchor.Type.BOTTOM).a(o7, 0);
                        this.o(center_Y).a(o7, 0);
                        return;
                    }
                }
                if (type == center_X && type2 == center_X) {
                    type = ConstraintAnchor.Type.LEFT;
                    this.o(type).a(constraintWidget.o(type), 0);
                    type = ConstraintAnchor.Type.RIGHT;
                    this.o(type).a(constraintWidget.o(type), 0);
                    constraintAnchor = this.o(center_X);
                }
                else if (type == center_Y && type2 == center_Y) {
                    type = ConstraintAnchor.Type.TOP;
                    this.o(type).a(constraintWidget.o(type), 0);
                    type = ConstraintAnchor.Type.BOTTOM;
                    this.o(type).a(constraintWidget.o(type), 0);
                    constraintAnchor = this.o(center_Y);
                }
                else {
                    final ConstraintAnchor o8 = this.o(type);
                    final ConstraintAnchor o9 = constraintWidget.o(type2);
                    if (o8.p(o9)) {
                        final ConstraintAnchor.Type baseline = ConstraintAnchor.Type.BASELINE;
                        Label_0801: {
                            ConstraintAnchor constraintAnchor3;
                            if (type == baseline) {
                                final ConstraintAnchor o10 = this.o(ConstraintAnchor.Type.TOP);
                                constraintAnchor3 = this.o(ConstraintAnchor.Type.BOTTOM);
                                if (o10 != null) {
                                    o10.q();
                                }
                                if (constraintAnchor3 == null) {
                                    break Label_0801;
                                }
                            }
                            else {
                                ConstraintAnchor constraintAnchor4;
                                if (type != ConstraintAnchor.Type.TOP && type != ConstraintAnchor.Type.BOTTOM) {
                                    if (type != ConstraintAnchor.Type.LEFT && type != ConstraintAnchor.Type.RIGHT) {
                                        break Label_0801;
                                    }
                                    final ConstraintAnchor o11 = this.o(center);
                                    if (o11.j() != o9) {
                                        o11.q();
                                    }
                                    constraintAnchor4 = this.o(type).g();
                                    constraintAnchor3 = this.o(center_X);
                                    if (!constraintAnchor3.o()) {
                                        break Label_0801;
                                    }
                                }
                                else {
                                    final ConstraintAnchor o12 = this.o(baseline);
                                    if (o12 != null) {
                                        o12.q();
                                    }
                                    final ConstraintAnchor o13 = this.o(center);
                                    if (o13.j() != o9) {
                                        o13.q();
                                    }
                                    constraintAnchor4 = this.o(type).g();
                                    constraintAnchor3 = this.o(center_Y);
                                    if (!constraintAnchor3.o()) {
                                        break Label_0801;
                                    }
                                }
                                constraintAnchor4.q();
                            }
                            constraintAnchor3.q();
                        }
                        o8.a(o9, n);
                    }
                    return;
                }
            }
            constraintAnchor2 = constraintWidget.o(type2);
        }
        constraintAnchor.a(constraintAnchor2, 0);
    }
    
    public boolean j0() {
        return this.K;
    }
    
    public void j1(final int s0) {
        this.s0 = s0;
    }
    
    public void k(final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, final int n) {
        if (constraintAnchor.h() == this) {
            this.j(constraintAnchor.k(), constraintAnchor2.h(), constraintAnchor2.k(), n);
        }
    }
    
    public boolean k0() {
        final ConstraintAnchor p = this.P;
        final ConstraintAnchor f = p.f;
        if (f == null || f.f != p) {
            final ConstraintAnchor r = this.R;
            final ConstraintAnchor f2 = r.f;
            if (f2 == null || f2.f != r) {
                return false;
            }
        }
        return true;
    }
    
    public void k1(final int b0) {
        this.b0 = b0;
        final int m0 = this.m0;
        if (b0 < m0) {
            this.b0 = m0;
        }
    }
    
    public void l(final ConstraintWidget constraintWidget, final float i, final int n) {
        final ConstraintAnchor.Type center = ConstraintAnchor.Type.CENTER;
        this.e0(center, constraintWidget, center, n, 0);
        this.I = i;
    }
    
    public boolean l0() {
        return this.L;
    }
    
    public void l1(final int v) {
        if (v >= 0 && v <= 3) {
            this.v = v;
        }
    }
    
    public void m(final androidx.constraintlayout.core.c c) {
        c.q(this.O);
        c.q(this.P);
        c.q(this.Q);
        c.q(this.R);
        if (this.l0 > 0) {
            c.q(this.S);
        }
    }
    
    public boolean m0() {
        return this.i && this.s0 != 8;
    }
    
    public void m1(final int f0) {
        this.f0 = f0;
    }
    
    public void n() {
        if (this.e == null) {
            this.e = new c(this);
        }
        if (this.f == null) {
            this.f = new d(this);
        }
    }
    
    public boolean n0() {
        return this.p || (this.O.n() && this.Q.n());
    }
    
    public void n1(final int g0) {
        this.g0 = g0;
    }
    
    public ConstraintAnchor o(final ConstraintAnchor.Type type) {
        switch (ConstraintWidget$a.a[type.ordinal()]) {
            default: {
                throw new AssertionError((Object)type.name());
            }
            case 9: {
                return null;
            }
            case 8: {
                return this.U;
            }
            case 7: {
                return this.T;
            }
            case 6: {
                return this.V;
            }
            case 5: {
                return this.S;
            }
            case 4: {
                return this.R;
            }
            case 3: {
                return this.Q;
            }
            case 2: {
                return this.P;
            }
            case 1: {
                return this.O;
            }
        }
    }
    
    public boolean o0() {
        return this.q || (this.P.n() && this.R.n());
    }
    
    public void o1(final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        if (this.F == -1) {
            if (b3 && !b4) {
                this.F = 0;
            }
            else if (!b3 && b4) {
                this.F = 1;
                if (this.e0 == -1) {
                    this.G = 1.0f / this.G;
                }
            }
        }
        if (this.F == 0 && (!this.P.o() || !this.R.o())) {
            this.F = 1;
        }
        else if (this.F == 1 && (!this.O.o() || !this.Q.o())) {
            this.F = 0;
        }
        if (this.F == -1 && (!this.P.o() || !this.R.o() || !this.O.o() || !this.Q.o())) {
            if (this.P.o() && this.R.o()) {
                this.F = 0;
            }
            else if (this.O.o() && this.Q.o()) {
                this.G = 1.0f / this.G;
                this.F = 1;
            }
        }
        if (this.F == -1) {
            final int z = this.z;
            if (z > 0 && this.C == 0) {
                this.F = 0;
            }
            else if (z == 0 && this.C > 0) {
                this.G = 1.0f / this.G;
                this.F = 1;
            }
        }
    }
    
    public int p() {
        return this.l0;
    }
    
    public boolean p0() {
        return this.s;
    }
    
    public void p1(final boolean b, final boolean b2) {
        final boolean b3 = b & this.e.k();
        final boolean b4 = b2 & this.f.k();
        final c e = this.e;
        int g = e.h.g;
        final d f = this.f;
        int g2 = f.h.g;
        int g3 = e.i.g;
        final int g4 = f.i.g;
        int n;
        if (g3 - g < 0 || g4 - g2 < 0 || g == Integer.MIN_VALUE || g == Integer.MAX_VALUE || g2 == Integer.MIN_VALUE || g2 == Integer.MAX_VALUE || g3 == Integer.MIN_VALUE || g3 == Integer.MAX_VALUE || g4 == Integer.MIN_VALUE || (n = g4) == Integer.MAX_VALUE) {
            g3 = 0;
            g = 0;
            n = (g2 = g);
        }
        final int n2 = g3 - g;
        final int n3 = n - g2;
        if (b3) {
            this.f0 = g;
        }
        if (b4) {
            this.g0 = g2;
        }
        if (this.s0 == 8) {
            this.b0 = 0;
            this.c0 = 0;
            return;
        }
        if (b3) {
            int b5 = n2;
            if (this.Z[0] == DimensionBehaviour.FIXED) {
                final int b6 = this.b0;
                if ((b5 = n2) < b6) {
                    b5 = b6;
                }
            }
            this.b0 = b5;
            final int m0 = this.m0;
            if (b5 < m0) {
                this.b0 = m0;
            }
        }
        if (b4) {
            int c0 = n3;
            if (this.Z[1] == DimensionBehaviour.FIXED) {
                final int c2 = this.c0;
                if ((c0 = n3) < c2) {
                    c0 = c2;
                }
            }
            this.c0 = c0;
            final int n4 = this.n0;
            if (c0 < n4) {
                this.c0 = n4;
            }
        }
    }
    
    public float q(final int n) {
        if (n == 0) {
            return this.o0;
        }
        if (n == 1) {
            return this.p0;
        }
        return -1.0f;
    }
    
    public void q0() {
        this.r = true;
    }
    
    public void q1(final androidx.constraintlayout.core.c c, final boolean b) {
        final int y = c.y(this.O);
        final int y2 = c.y(this.P);
        final int y3 = c.y(this.Q);
        final int y4 = c.y(this.R);
        int g = y;
        int g2 = y3;
        if (b) {
            final c e = this.e;
            g = y;
            g2 = y3;
            if (e != null) {
                final DependencyNode h = e.h;
                g = y;
                g2 = y3;
                if (h.j) {
                    final DependencyNode i = e.i;
                    g = y;
                    g2 = y3;
                    if (i.j) {
                        g = h.g;
                        g2 = i.g;
                    }
                }
            }
        }
        int g3 = y2;
        int g4 = y4;
        if (b) {
            final d f = this.f;
            g3 = y2;
            g4 = y4;
            if (f != null) {
                final DependencyNode h2 = f.h;
                g3 = y2;
                g4 = y4;
                if (h2.j) {
                    final DependencyNode j = f.i;
                    g3 = y2;
                    g4 = y4;
                    if (j.j) {
                        g3 = h2.g;
                        g4 = j.g;
                    }
                }
            }
        }
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        Label_0306: {
            if (g2 - g >= 0 && g4 - g3 >= 0 && g != Integer.MIN_VALUE && g != Integer.MAX_VALUE && g3 != Integer.MIN_VALUE && g3 != Integer.MAX_VALUE && g2 != Integer.MIN_VALUE && g2 != Integer.MAX_VALUE && g4 != Integer.MIN_VALUE) {
                n = g;
                n2 = g2;
                if ((n3 = g4) != Integer.MAX_VALUE) {
                    break Label_0306;
                }
            }
            n = 0;
            n3 = 0;
            g3 = (n2 = 0);
        }
        this.J0(n, g3, n2, n3);
    }
    
    public int r() {
        return this.Y() + this.c0;
    }
    
    public void r0() {
        this.s = true;
    }
    
    public Object s() {
        return this.q0;
    }
    
    public boolean s0() {
        final DimensionBehaviour[] z = this.Z;
        final boolean b = false;
        final DimensionBehaviour dimensionBehaviour = z[0];
        final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
        boolean b2 = b;
        if (dimensionBehaviour == match_CONSTRAINT) {
            b2 = b;
            if (z[1] == match_CONSTRAINT) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public String t() {
        return this.u0;
    }
    
    public void t0() {
        this.O.q();
        this.P.q();
        this.Q.q();
        this.R.q();
        this.S.q();
        this.T.q();
        this.U.q();
        this.V.q();
        this.a0 = null;
        this.I = 0.0f;
        this.b0 = 0;
        this.c0 = 0;
        this.d0 = 0.0f;
        this.e0 = -1;
        this.f0 = 0;
        this.g0 = 0;
        this.j0 = 0;
        this.k0 = 0;
        this.l0 = 0;
        this.m0 = 0;
        this.n0 = 0;
        final float k0 = ConstraintWidget.K0;
        this.o0 = k0;
        this.p0 = k0;
        final DimensionBehaviour[] z = this.Z;
        z[1] = (z[0] = DimensionBehaviour.FIXED);
        this.q0 = null;
        this.r0 = 0;
        this.s0 = 0;
        this.v0 = null;
        this.w0 = false;
        this.x0 = false;
        this.z0 = 0;
        this.A0 = 0;
        this.B0 = false;
        this.C0 = false;
        final float[] d0 = this.D0;
        d0[1] = (d0[0] = -1.0f);
        this.t = -1;
        this.u = -1;
        final int[] h = this.H;
        h[1] = (h[0] = Integer.MAX_VALUE);
        this.w = 0;
        this.x = 0;
        this.B = 1.0f;
        this.E = 1.0f;
        this.A = Integer.MAX_VALUE;
        this.D = Integer.MAX_VALUE;
        this.z = 0;
        this.C = 0;
        this.h = false;
        this.F = -1;
        this.G = 1.0f;
        this.y0 = false;
        final boolean[] g = this.g;
        g[1] = (g[0] = true);
        this.L = false;
        final boolean[] y = this.Y;
        y[1] = (y[0] = false);
        this.i = true;
        final int[] y2 = this.y;
        y2[1] = (y2[0] = 0);
        this.l = -1;
        this.m = -1;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final String v0 = this.v0;
        final String s = "";
        String string;
        if (v0 != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("type: ");
            sb2.append(this.v0);
            sb2.append(" ");
            string = sb2.toString();
        }
        else {
            string = "";
        }
        sb.append(string);
        String string2 = s;
        if (this.u0 != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("id: ");
            sb3.append(this.u0);
            sb3.append(" ");
            string2 = sb3.toString();
        }
        sb.append(string2);
        sb.append("(");
        sb.append(this.f0);
        sb.append(", ");
        sb.append(this.g0);
        sb.append(") - (");
        sb.append(this.b0);
        sb.append(" x ");
        sb.append(this.c0);
        sb.append(")");
        return sb.toString();
    }
    
    public DimensionBehaviour u(final int n) {
        if (n == 0) {
            return this.A();
        }
        if (n == 1) {
            return this.T();
        }
        return null;
    }
    
    public void u0() {
        final ConstraintWidget k = this.K();
        if (k != null && k instanceof androidx.constraintlayout.core.widgets.d && ((androidx.constraintlayout.core.widgets.d)this.K()).M1()) {
            return;
        }
        for (int size = this.X.size(), i = 0; i < size; ++i) {
            ((ConstraintAnchor)this.X.get(i)).q();
        }
    }
    
    public float v() {
        return this.d0;
    }
    
    public void v0() {
        int i = 0;
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = false;
        while (i < this.X.size()) {
            ((ConstraintAnchor)this.X.get(i)).r();
            ++i;
        }
    }
    
    public int w() {
        return this.e0;
    }
    
    public void w0(final oe oe) {
        this.O.s(oe);
        this.P.s(oe);
        this.Q.s(oe);
        this.R.s(oe);
        this.S.s(oe);
        this.V.s(oe);
        this.T.s(oe);
        this.U.s(oe);
    }
    
    public int x() {
        if (this.s0 == 8) {
            return 0;
        }
        return this.c0;
    }
    
    public final void x0(final StringBuilder sb, final String str, final float f, final float n) {
        if (f == n) {
            return;
        }
        sb.append(str);
        sb.append(" :   ");
        sb.append(f);
        sb.append(",\n");
    }
    
    public float y() {
        return this.o0;
    }
    
    public final void y0(final StringBuilder sb, final String str, final int i, final int n) {
        if (i == n) {
            return;
        }
        sb.append(str);
        sb.append(" :   ");
        sb.append(i);
        sb.append(",\n");
    }
    
    public int z() {
        return this.z0;
    }
    
    public final void z0(final StringBuilder sb, final String str, final float f, final int i) {
        if (f == 0.0f) {
            return;
        }
        sb.append(str);
        sb.append(" :  [");
        sb.append(f);
        sb.append(",");
        sb.append(i);
        sb.append("");
        sb.append("],\n");
    }
    
    public enum DimensionBehaviour
    {
        private static final DimensionBehaviour[] $VALUES;
        
        FIXED, 
        MATCH_CONSTRAINT, 
        MATCH_PARENT, 
        WRAP_CONTENT;
    }
}

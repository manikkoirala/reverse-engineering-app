// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashSet;

public abstract class h extends gd0
{
    public int N0;
    public int O0;
    public int P0;
    public int Q0;
    public int R0;
    public int S0;
    public int T0;
    public int U0;
    public boolean V0;
    public int W0;
    public int X0;
    public lb.a Y0;
    public lb.b Z0;
    
    public h() {
        this.N0 = 0;
        this.O0 = 0;
        this.P0 = 0;
        this.Q0 = 0;
        this.R0 = 0;
        this.S0 = 0;
        this.T0 = 0;
        this.U0 = 0;
        this.V0 = false;
        this.W0 = 0;
        this.X0 = 0;
        this.Y0 = new lb.a();
        this.Z0 = null;
    }
    
    public int A1() {
        return this.U0;
    }
    
    public int B1() {
        return this.N0;
    }
    
    public abstract void C1(final int p0, final int p1, final int p2, final int p3);
    
    public void D1(final ConstraintWidget constraintWidget, final DimensionBehaviour a, final int c, final DimensionBehaviour b, final int d) {
        while (this.Z0 == null && this.K() != null) {
            this.Z0 = ((d)this.K()).J1();
        }
        final lb.a y0 = this.Y0;
        y0.a = a;
        y0.b = b;
        y0.c = c;
        y0.d = d;
        this.Z0.a(constraintWidget, y0);
        constraintWidget.k1(this.Y0.e);
        constraintWidget.L0(this.Y0.f);
        constraintWidget.K0(this.Y0.h);
        constraintWidget.A0(this.Y0.g);
    }
    
    public boolean E1() {
        final ConstraintWidget a0 = super.a0;
        lb.b j1;
        if (a0 != null) {
            j1 = ((d)a0).J1();
        }
        else {
            j1 = null;
        }
        if (j1 == null) {
            return false;
        }
        int n = 0;
        while (true) {
            final int m0 = super.M0;
            boolean b = true;
            if (n >= m0) {
                break;
            }
            final ConstraintWidget constraintWidget = super.L0[n];
            if (constraintWidget != null) {
                if (!(constraintWidget instanceof f)) {
                    final DimensionBehaviour u = constraintWidget.u(0);
                    final DimensionBehaviour u2 = constraintWidget.u(1);
                    final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
                    if (u != match_CONSTRAINT || constraintWidget.w == 1 || u2 != match_CONSTRAINT || constraintWidget.x == 1) {
                        b = false;
                    }
                    if (!b) {
                        Enum<DimensionBehaviour> wrap_CONTENT;
                        if ((wrap_CONTENT = u) == match_CONSTRAINT) {
                            wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
                        }
                        Enum<DimensionBehaviour> wrap_CONTENT2;
                        if ((wrap_CONTENT2 = u2) == match_CONSTRAINT) {
                            wrap_CONTENT2 = DimensionBehaviour.WRAP_CONTENT;
                        }
                        final lb.a y0 = this.Y0;
                        y0.a = (DimensionBehaviour)wrap_CONTENT;
                        y0.b = (DimensionBehaviour)wrap_CONTENT2;
                        y0.c = constraintWidget.W();
                        this.Y0.d = constraintWidget.x();
                        j1.a(constraintWidget, this.Y0);
                        constraintWidget.k1(this.Y0.e);
                        constraintWidget.L0(this.Y0.f);
                        constraintWidget.A0(this.Y0.g);
                    }
                }
            }
            ++n;
        }
        return true;
    }
    
    public boolean F1() {
        return this.V0;
    }
    
    public void G1(final boolean v0) {
        this.V0 = v0;
    }
    
    public void H1(final int w0, final int x0) {
        this.W0 = w0;
        this.X0 = x0;
    }
    
    public void I1(final int n) {
        this.P0 = n;
        this.N0 = n;
        this.Q0 = n;
        this.O0 = n;
        this.R0 = n;
        this.S0 = n;
    }
    
    public void J1(final int o0) {
        this.O0 = o0;
    }
    
    public void K1(final int s0) {
        this.S0 = s0;
    }
    
    public void L1(final int n) {
        this.P0 = n;
        this.T0 = n;
    }
    
    public void M1(final int n) {
        this.Q0 = n;
        this.U0 = n;
    }
    
    public void N1(final int u0) {
        this.R0 = u0;
        this.T0 = u0;
        this.U0 = u0;
    }
    
    public void O1(final int n0) {
        this.N0 = n0;
    }
    
    @Override
    public void c(final d d) {
        this.u1();
    }
    
    public void t1(final boolean b) {
        final int r0 = this.R0;
        if (r0 > 0 || this.S0 > 0) {
            if (b) {
                this.T0 = this.S0;
                this.U0 = r0;
            }
            else {
                this.T0 = r0;
                this.U0 = this.S0;
            }
        }
    }
    
    public void u1() {
        for (int i = 0; i < super.M0; ++i) {
            final ConstraintWidget constraintWidget = super.L0[i];
            if (constraintWidget != null) {
                constraintWidget.U0(true);
            }
        }
    }
    
    public boolean v1(final HashSet set) {
        for (int i = 0; i < super.M0; ++i) {
            if (set.contains(super.L0[i])) {
                return true;
            }
        }
        return false;
    }
    
    public int w1() {
        return this.X0;
    }
    
    public int x1() {
        return this.W0;
    }
    
    public int y1() {
        return this.O0;
    }
    
    public int z1() {
        return this.T0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.Arrays;
import androidx.constraintlayout.core.c;
import java.util.ArrayList;

public class e extends h
{
    public int a1;
    public int b1;
    public int c1;
    public int d1;
    public int e1;
    public int f1;
    public float g1;
    public float h1;
    public float i1;
    public float j1;
    public float k1;
    public float l1;
    public int m1;
    public int n1;
    public int o1;
    public int p1;
    public int q1;
    public int r1;
    public int s1;
    public ArrayList t1;
    public ConstraintWidget[] u1;
    public ConstraintWidget[] v1;
    public int[] w1;
    public ConstraintWidget[] x1;
    public int y1;
    
    public e() {
        this.a1 = -1;
        this.b1 = -1;
        this.c1 = -1;
        this.d1 = -1;
        this.e1 = -1;
        this.f1 = -1;
        this.g1 = 0.5f;
        this.h1 = 0.5f;
        this.i1 = 0.5f;
        this.j1 = 0.5f;
        this.k1 = 0.5f;
        this.l1 = 0.5f;
        this.m1 = 0;
        this.n1 = 0;
        this.o1 = 2;
        this.p1 = 2;
        this.q1 = 0;
        this.r1 = -1;
        this.s1 = 0;
        this.t1 = new ArrayList();
        this.u1 = null;
        this.v1 = null;
        this.w1 = null;
        this.y1 = 0;
    }
    
    public static /* synthetic */ int P1(final e e) {
        return e.m1;
    }
    
    public static /* synthetic */ int Q1(final e e) {
        return e.n1;
    }
    
    public static /* synthetic */ int R1(final e e) {
        return e.c1;
    }
    
    public static /* synthetic */ float S1(final e e) {
        return e.i1;
    }
    
    public static /* synthetic */ int T1(final e e) {
        return e.e1;
    }
    
    public static /* synthetic */ float U1(final e e) {
        return e.k1;
    }
    
    public static /* synthetic */ float V1(final e e) {
        return e.h1;
    }
    
    public static /* synthetic */ int W1(final e e) {
        return e.d1;
    }
    
    public static /* synthetic */ float X1(final e e) {
        return e.j1;
    }
    
    public static /* synthetic */ int Y1(final e e) {
        return e.f1;
    }
    
    public static /* synthetic */ float Z1(final e e) {
        return e.l1;
    }
    
    public static /* synthetic */ int a2(final e e) {
        return e.o1;
    }
    
    public static /* synthetic */ int d2(final e e) {
        return e.y1;
    }
    
    public static /* synthetic */ ConstraintWidget[] e2(final e e) {
        return e.x1;
    }
    
    public static /* synthetic */ int f2(final e e) {
        return e.b1;
    }
    
    public static /* synthetic */ int g2(final e e) {
        return e.p1;
    }
    
    public static /* synthetic */ int h2(final e e) {
        return e.a1;
    }
    
    public static /* synthetic */ float i2(final e e) {
        return e.g1;
    }
    
    public void A2(final float l1) {
        this.l1 = l1;
    }
    
    public void B2(final int f1) {
        this.f1 = f1;
    }
    
    @Override
    public void C1(int min, int min2, final int n, final int b) {
        if (super.M0 > 0 && !this.E1()) {
            this.H1(0, 0);
            this.G1(false);
            return;
        }
        final int z1 = this.z1();
        final int a1 = this.A1();
        final int b2 = this.B1();
        final int y1 = this.y1();
        final int[] array = new int[2];
        int n2 = min2 - z1 - a1;
        final int s1 = this.s1;
        if (s1 == 1) {
            n2 = b - b2 - y1;
        }
        Label_0141: {
            if (s1 == 0) {
                if (this.a1 == -1) {
                    this.a1 = 0;
                }
                if (this.b1 != -1) {
                    break Label_0141;
                }
            }
            else {
                if (this.a1 == -1) {
                    this.a1 = 0;
                }
                if (this.b1 != -1) {
                    break Label_0141;
                }
            }
            this.b1 = 0;
        }
        ConstraintWidget[] l0 = super.L0;
        int n3 = 0;
        int n4 = 0;
        int m0;
        while (true) {
            m0 = super.M0;
            if (n3 >= m0) {
                break;
            }
            int n5 = n4;
            if (super.L0[n3].V() == 8) {
                n5 = n4 + 1;
            }
            ++n3;
            n4 = n5;
        }
        int y2;
        if (n4 > 0) {
            l0 = new ConstraintWidget[m0 - n4];
            int i = 0;
            y2 = 0;
            while (i < super.M0) {
                final ConstraintWidget constraintWidget = super.L0[i];
                int n6 = y2;
                if (constraintWidget.V() != 8) {
                    l0[y2] = constraintWidget;
                    n6 = y2 + 1;
                }
                ++i;
                y2 = n6;
            }
        }
        else {
            y2 = m0;
        }
        this.x1 = l0;
        this.y1 = y2;
        final int q1 = this.q1;
        if (q1 != 0) {
            if (q1 != 1) {
                if (q1 != 2) {
                    if (q1 == 3) {
                        this.o2(l0, y2, this.s1, n2, array);
                    }
                }
                else {
                    this.m2(l0, y2, this.s1, n2, array);
                }
            }
            else {
                this.n2(l0, y2, this.s1, n2, array);
            }
        }
        else {
            this.p2(l0, y2, this.s1, n2, array);
        }
        boolean b3 = true;
        final int a2 = array[0] + z1 + a1;
        final int a3 = array[1] + b2 + y1;
        if (min == 1073741824) {
            min = min2;
        }
        else if (min == Integer.MIN_VALUE) {
            min = Math.min(a2, min2);
        }
        else if (min == 0) {
            min = a2;
        }
        else {
            min = 0;
        }
        if (n == 1073741824) {
            min2 = b;
        }
        else if (n == Integer.MIN_VALUE) {
            min2 = Math.min(a3, b);
        }
        else if (n == 0) {
            min2 = a3;
        }
        else {
            min2 = 0;
        }
        this.H1(min, min2);
        this.k1(min);
        this.L0(min2);
        if (super.M0 <= 0) {
            b3 = false;
        }
        this.G1(b3);
    }
    
    public void C2(final int r1) {
        this.r1 = r1;
    }
    
    public void D2(final int s1) {
        this.s1 = s1;
    }
    
    public void E2(final int p) {
        this.p1 = p;
    }
    
    public void F2(final float h1) {
        this.h1 = h1;
    }
    
    public void G2(final int n1) {
        this.n1 = n1;
    }
    
    public void H2(final int b1) {
        this.b1 = b1;
    }
    
    public void I2(final int q1) {
        this.q1 = q1;
    }
    
    @Override
    public void g(final c c, final boolean b) {
        super.g(c, b);
        final boolean b2 = this.K() != null && ((d)this.K()).Q1();
        final int q1 = this.q1;
        if (q1 != 0) {
            if (q1 != 1) {
                if (q1 != 2) {
                    if (q1 == 3) {
                        for (int size = this.t1.size(), i = 0; i < size; ++i) {
                            ((a)this.t1.get(i)).d(b2, i, i == size - 1);
                        }
                    }
                }
                else {
                    this.j2(b2);
                }
            }
            else {
                for (int size2 = this.t1.size(), j = 0; j < size2; ++j) {
                    ((a)this.t1.get(j)).d(b2, j, j == size2 - 1);
                }
            }
        }
        else if (this.t1.size() > 0) {
            this.t1.get(0).d(b2, 0, true);
        }
        this.G1(false);
    }
    
    public final void j2(final boolean b) {
        if (this.w1 != null && this.v1 != null) {
            if (this.u1 != null) {
                for (int i = 0; i < this.y1; ++i) {
                    this.x1[i].u0();
                }
                final int[] w1 = this.w1;
                final int n = w1[0];
                final int n2 = w1[1];
                float g1 = this.g1;
                ConstraintWidget constraintWidget = null;
                ConstraintWidget constraintWidget3;
                for (int j = 0; j < n; ++j, constraintWidget = constraintWidget3) {
                    int n3;
                    if (b) {
                        n3 = n - j - 1;
                        g1 = 1.0f - this.g1;
                    }
                    else {
                        n3 = j;
                    }
                    final ConstraintWidget constraintWidget2 = this.v1[n3];
                    constraintWidget3 = constraintWidget;
                    if (constraintWidget2 != null) {
                        if (constraintWidget2.V() == 8) {
                            constraintWidget3 = constraintWidget;
                        }
                        else {
                            if (j == 0) {
                                constraintWidget2.k(constraintWidget2.O, super.O, this.z1());
                                constraintWidget2.N0(this.a1);
                                constraintWidget2.M0(g1);
                            }
                            if (j == n - 1) {
                                constraintWidget2.k(constraintWidget2.Q, super.Q, this.A1());
                            }
                            if (j > 0 && constraintWidget != null) {
                                constraintWidget2.k(constraintWidget2.O, constraintWidget.Q, this.m1);
                                constraintWidget.k(constraintWidget.Q, constraintWidget2.O, 0);
                            }
                            constraintWidget3 = constraintWidget2;
                        }
                    }
                }
                ConstraintWidget constraintWidget5;
                for (int k = 0; k < n2; ++k, constraintWidget = constraintWidget5) {
                    final ConstraintWidget constraintWidget4 = this.u1[k];
                    constraintWidget5 = constraintWidget;
                    if (constraintWidget4 != null) {
                        if (constraintWidget4.V() == 8) {
                            constraintWidget5 = constraintWidget;
                        }
                        else {
                            if (k == 0) {
                                constraintWidget4.k(constraintWidget4.P, super.P, this.B1());
                                constraintWidget4.e1(this.b1);
                                constraintWidget4.d1(this.h1);
                            }
                            if (k == n2 - 1) {
                                constraintWidget4.k(constraintWidget4.R, super.R, this.y1());
                            }
                            if (k > 0 && constraintWidget != null) {
                                constraintWidget4.k(constraintWidget4.P, constraintWidget.R, this.n1);
                                constraintWidget.k(constraintWidget.R, constraintWidget4.P, 0);
                            }
                            constraintWidget5 = constraintWidget4;
                        }
                    }
                }
                for (int l = 0; l < n; ++l) {
                    for (int n4 = 0; n4 < n2; ++n4) {
                        int n5 = n4 * n + l;
                        if (this.s1 == 1) {
                            n5 = l * n2 + n4;
                        }
                        final ConstraintWidget[] x1 = this.x1;
                        if (n5 < x1.length) {
                            final ConstraintWidget constraintWidget6 = x1[n5];
                            if (constraintWidget6 != null) {
                                if (constraintWidget6.V() != 8) {
                                    final ConstraintWidget constraintWidget7 = this.v1[l];
                                    final ConstraintWidget constraintWidget8 = this.u1[n4];
                                    if (constraintWidget6 != constraintWidget7) {
                                        constraintWidget6.k(constraintWidget6.O, constraintWidget7.O, 0);
                                        constraintWidget6.k(constraintWidget6.Q, constraintWidget7.Q, 0);
                                    }
                                    if (constraintWidget6 != constraintWidget8) {
                                        constraintWidget6.k(constraintWidget6.P, constraintWidget8.P, 0);
                                        constraintWidget6.k(constraintWidget6.R, constraintWidget8.R, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public final int k2(final ConstraintWidget constraintWidget, int n) {
        if (constraintWidget == null) {
            return 0;
        }
        if (constraintWidget.T() == DimensionBehaviour.MATCH_CONSTRAINT) {
            final int x = constraintWidget.x;
            if (x == 0) {
                return 0;
            }
            if (x == 2) {
                n *= (int)constraintWidget.E;
                if (n != constraintWidget.x()) {
                    constraintWidget.Y0(true);
                    this.D1(constraintWidget, constraintWidget.A(), constraintWidget.W(), DimensionBehaviour.FIXED, n);
                }
                return n;
            }
            if (x == 1) {
                return constraintWidget.x();
            }
            if (x == 3) {
                return (int)(constraintWidget.W() * constraintWidget.d0 + 0.5f);
            }
        }
        return constraintWidget.x();
    }
    
    public final int l2(final ConstraintWidget constraintWidget, int n) {
        if (constraintWidget == null) {
            return 0;
        }
        if (constraintWidget.A() == DimensionBehaviour.MATCH_CONSTRAINT) {
            final int w = constraintWidget.w;
            if (w == 0) {
                return 0;
            }
            if (w == 2) {
                n *= (int)constraintWidget.B;
                if (n != constraintWidget.W()) {
                    constraintWidget.Y0(true);
                    this.D1(constraintWidget, DimensionBehaviour.FIXED, n, constraintWidget.T(), constraintWidget.x());
                }
                return n;
            }
            if (w == 1) {
                return constraintWidget.W();
            }
            if (w == 3) {
                return (int)(constraintWidget.x() * constraintWidget.d0 + 0.5f);
            }
        }
        return constraintWidget.W();
    }
    
    public final void m2(final ConstraintWidget[] array, final int n, final int n2, final int n3, final int[] array2) {
        final int r1 = this.r1;
        int n9;
        int n10;
        if (n2 == 0) {
            int n4;
            if ((n4 = r1) <= 0) {
                int n5 = 0;
                int n7;
                int n6 = n7 = 0;
                while (true) {
                    n4 = n5;
                    if (n6 >= n) {
                        break;
                    }
                    int n8 = n7;
                    if (n6 > 0) {
                        n8 = n7 + this.m1;
                    }
                    final ConstraintWidget constraintWidget = array[n6];
                    if (constraintWidget == null) {
                        n7 = n8;
                    }
                    else {
                        n7 = n8 + this.l2(constraintWidget, n3);
                        if (n7 > n3) {
                            n4 = n5;
                            break;
                        }
                        ++n5;
                    }
                    ++n6;
                }
            }
            n9 = n4;
            n10 = 0;
        }
        else {
            int n11;
            if ((n11 = r1) <= 0) {
                int n12 = 0;
                int n14;
                int n13 = n14 = 0;
                while (true) {
                    n11 = n12;
                    if (n13 >= n) {
                        break;
                    }
                    int n15 = n14;
                    if (n13 > 0) {
                        n15 = n14 + this.n1;
                    }
                    final ConstraintWidget constraintWidget2 = array[n13];
                    if (constraintWidget2 == null) {
                        n14 = n15;
                    }
                    else {
                        n14 = n15 + this.k2(constraintWidget2, n3);
                        if (n14 > n3) {
                            n11 = n12;
                            break;
                        }
                        ++n12;
                    }
                    ++n13;
                }
            }
            n9 = 0;
            n10 = n11;
        }
        if (this.w1 == null) {
            this.w1 = new int[2];
        }
        while (true) {
            int n16 = 0;
            int n17 = 0;
            Label_0278: {
                Label_0261: {
                    if (n10 != 0) {
                        break Label_0261;
                    }
                    n16 = n10;
                    n17 = n9;
                    if (n2 != 1) {
                        break Label_0261;
                    }
                    break Label_0278;
                    final boolean b;
                    while (!b) {
                        if (n2 == 0) {
                            n10 = (int)Math.ceil(n / (float)n9);
                        }
                        else {
                            n9 = (int)Math.ceil(n / (float)n10);
                        }
                        final ConstraintWidget[] v1 = this.v1;
                        if (v1 != null && v1.length >= n9) {
                            Arrays.fill(v1, null);
                        }
                        else {
                            this.v1 = new ConstraintWidget[n9];
                        }
                        final ConstraintWidget[] u1 = this.u1;
                        if (u1 != null && u1.length >= n10) {
                            Arrays.fill(u1, null);
                        }
                        else {
                            this.u1 = new ConstraintWidget[n10];
                        }
                        for (int i = 0; i < n9; ++i) {
                            for (int j = 0; j < n10; ++j) {
                                int n18 = j * n9 + i;
                                if (n2 == 1) {
                                    n18 = i * n10 + j;
                                }
                                if (n18 < array.length) {
                                    final ConstraintWidget constraintWidget3 = array[n18];
                                    if (constraintWidget3 != null) {
                                        final int l2 = this.l2(constraintWidget3, n3);
                                        final ConstraintWidget constraintWidget4 = this.v1[i];
                                        if (constraintWidget4 == null || constraintWidget4.W() < l2) {
                                            this.v1[i] = constraintWidget3;
                                        }
                                        final int k2 = this.k2(constraintWidget3, n3);
                                        final ConstraintWidget constraintWidget5 = this.u1[j];
                                        if (constraintWidget5 == null || constraintWidget5.x() < k2) {
                                            this.u1[j] = constraintWidget3;
                                        }
                                    }
                                }
                            }
                        }
                        int n19 = 0;
                        int n20 = 0;
                        while (n19 < n9) {
                            final ConstraintWidget constraintWidget6 = this.v1[n19];
                            int n21 = n20;
                            if (constraintWidget6 != null) {
                                int n22 = n20;
                                if (n19 > 0) {
                                    n22 = n20 + this.m1;
                                }
                                n21 = n22 + this.l2(constraintWidget6, n3);
                            }
                            ++n19;
                            n20 = n21;
                        }
                        int n23 = 0;
                        int n24 = 0;
                        while (n23 < n10) {
                            final ConstraintWidget constraintWidget7 = this.u1[n23];
                            int n25 = n24;
                            if (constraintWidget7 != null) {
                                int n26 = n24;
                                if (n23 > 0) {
                                    n26 = n24 + this.n1;
                                }
                                n25 = n26 + this.k2(constraintWidget7, n3);
                            }
                            ++n23;
                            n24 = n25;
                        }
                        array2[0] = n20;
                        array2[1] = n24;
                        if (n2 == 0) {
                            n16 = n10;
                            n17 = n9;
                            if (n20 <= n3) {
                                break Label_0278;
                            }
                            n16 = n10;
                            if ((n17 = n9) <= 1) {
                                break Label_0278;
                            }
                            --n9;
                        }
                        else {
                            n16 = n10;
                            n17 = n9;
                            if (n24 <= n3) {
                                break Label_0278;
                            }
                            n16 = n10;
                            n17 = n9;
                            if (n10 <= 1) {
                                break Label_0278;
                            }
                            --n10;
                        }
                    }
                    final int[] w1 = this.w1;
                    w1[0] = n9;
                    w1[1] = n10;
                    return;
                }
                if (n9 != 0 || n2 != 0) {
                    final boolean b = false;
                    continue;
                }
                n17 = n9;
                n16 = n10;
            }
            final boolean b = true;
            n10 = n16;
            n9 = n17;
            continue;
        }
    }
    
    public final void n2(final ConstraintWidget[] array, int i, final int n, final int n2, final int[] array2) {
        if (i == 0) {
            return;
        }
        this.t1.clear();
        a e = new a(n, super.O, super.P, super.Q, super.R, n2);
        this.t1.add(e);
        int n3 = 0;
        final int n4 = 0;
        final int n7;
        int n6;
        final int n5 = n6 = (n7 = 0);
        a a = e;
        int n8 = n7;
        int n12;
        if (n == 0) {
            int n9 = n7;
            int n10 = n5;
            int n11 = n4;
            while (true) {
                n12 = n11;
                if (n9 >= i) {
                    break;
                }
                final ConstraintWidget constraintWidget = array[n9];
                final int l2 = this.l2(constraintWidget, n2);
                int n13 = n11;
                if (constraintWidget.A() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n13 = n11 + 1;
                }
                boolean b2;
                final boolean b = b2 = ((n10 == n2 || this.m1 + n10 + l2 > n2) && androidx.constraintlayout.core.widgets.e.a.a(e) != null);
                if (!b) {
                    b2 = b;
                    if (n9 > 0) {
                        final int r1 = this.r1;
                        b2 = b;
                        if (r1 > 0) {
                            b2 = b;
                            if (n9 % r1 == 0) {
                                b2 = true;
                            }
                        }
                    }
                }
                a e2 = null;
                Label_0309: {
                    if (b2) {
                        e2 = new a(n, super.O, super.P, super.Q, super.R, n2);
                        e2.i(n9);
                        this.t1.add(e2);
                    }
                    else {
                        e2 = e;
                        if (n9 > 0) {
                            n10 += this.m1 + l2;
                            e2 = e;
                            break Label_0309;
                        }
                    }
                    n10 = l2;
                }
                e2.b(constraintWidget);
                ++n9;
                n11 = n13;
                e = e2;
            }
        }
        else {
            while (true) {
                n12 = n3;
                if (n8 >= i) {
                    break;
                }
                final ConstraintWidget constraintWidget2 = array[n8];
                final int k2 = this.k2(constraintWidget2, n2);
                int n14 = n3;
                if (constraintWidget2.T() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n14 = n3 + 1;
                }
                boolean b4;
                final boolean b3 = b4 = ((n6 == n2 || this.n1 + n6 + k2 > n2) && androidx.constraintlayout.core.widgets.e.a.a(a) != null);
                if (!b3) {
                    b4 = b3;
                    if (n8 > 0) {
                        final int r2 = this.r1;
                        b4 = b3;
                        if (r2 > 0) {
                            b4 = b3;
                            if (n8 % r2 == 0) {
                                b4 = true;
                            }
                        }
                    }
                }
                int n15 = 0;
                Label_0547: {
                    a e3;
                    if (b4) {
                        e3 = new a(n, super.O, super.P, super.Q, super.R, n2);
                        e3.i(n8);
                        this.t1.add(e3);
                    }
                    else {
                        e3 = a;
                        if (n8 > 0) {
                            n15 = n6 + (this.n1 + k2);
                            break Label_0547;
                        }
                    }
                    n15 = k2;
                    a = e3;
                }
                a.b(constraintWidget2);
                ++n8;
                n3 = n14;
                n6 = n15;
            }
        }
        final int size = this.t1.size();
        ConstraintAnchor o = super.O;
        ConstraintAnchor p5 = super.P;
        ConstraintAnchor constraintAnchor = super.Q;
        ConstraintAnchor constraintAnchor2 = super.R;
        int z1 = this.z1();
        int b5 = this.B1();
        int a2 = this.A1();
        int y1 = this.y1();
        final DimensionBehaviour a3 = this.A();
        final DimensionBehaviour wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
        if (a3 != wrap_CONTENT && this.T() != wrap_CONTENT) {
            i = 0;
        }
        else {
            i = 1;
        }
        if (n12 > 0 && i != 0) {
            a a4;
            int n16;
            for (i = 0; i < size; ++i) {
                a4 = this.t1.get(i);
                if (n == 0) {
                    n16 = a4.f();
                }
                else {
                    n16 = a4.e();
                }
                a4.g(n2 - n16);
            }
        }
        int a5 = 0;
        int j;
        int a6;
        for (a6 = (j = 0); j < size; ++j, a6 = i) {
            final a a7 = this.t1.get(j);
            if (n == 0) {
                if (j < size - 1) {
                    constraintAnchor2 = androidx.constraintlayout.core.widgets.e.a.a((a)this.t1.get(j + 1)).P;
                    i = 0;
                }
                else {
                    constraintAnchor2 = super.R;
                    i = this.y1();
                }
                final ConstraintAnchor r3 = androidx.constraintlayout.core.widgets.e.a.a(a7).R;
                a7.j(n, o, p5, constraintAnchor, constraintAnchor2, z1, b5, a2, i, n2);
                final int max = Math.max(a6, a7.f());
                final int n17 = a5 += a7.e();
                if (j > 0) {
                    a5 = n17 + this.n1;
                }
                final int n18 = 0;
                p5 = r3;
                y1 = i;
                i = max;
                b5 = n18;
            }
            else {
                if (j < size - 1) {
                    constraintAnchor = androidx.constraintlayout.core.widgets.e.a.a((a)this.t1.get(j + 1)).O;
                    i = 0;
                }
                else {
                    constraintAnchor = super.Q;
                    i = this.A1();
                }
                final ConstraintAnchor q = androidx.constraintlayout.core.widgets.e.a.a(a7).Q;
                a7.j(n, o, p5, constraintAnchor, constraintAnchor2, z1, b5, i, y1, n2);
                final int n19 = a6 + a7.f();
                final int max2 = Math.max(a5, a7.e());
                int n20 = n19;
                if (j > 0) {
                    n20 = n19 + this.m1;
                }
                final int n21 = max2;
                final int n22 = 0;
                a2 = i;
                o = q;
                i = n20;
                a5 = n21;
                z1 = n22;
            }
        }
        array2[0] = a6;
        array2[1] = a5;
    }
    
    public final void o2(final ConstraintWidget[] array, int i, final int n, final int n2, final int[] array2) {
        if (i == 0) {
            return;
        }
        this.t1.clear();
        a a = new a(n, super.O, super.P, super.Q, super.R, n2);
        this.t1.add(a);
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        int n8;
        if (n == 0) {
            int n7;
            int n6 = n7 = n5;
            while (true) {
                n8 = n5;
                if (n6 >= i) {
                    break;
                }
                final int n9 = n4 + 1;
                final ConstraintWidget constraintWidget = array[n6];
                final int l2 = this.l2(constraintWidget, n2);
                int n10 = n5;
                if (constraintWidget.A() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n10 = n5 + 1;
                }
                boolean b2;
                final boolean b = b2 = ((n7 == n2 || this.m1 + n7 + l2 > n2) && e.a.a(a) != null);
                if (!b) {
                    b2 = b;
                    if (n6 > 0) {
                        final int r1 = this.r1;
                        b2 = b;
                        if (r1 > 0) {
                            b2 = b;
                            if (n9 > r1) {
                                b2 = true;
                            }
                        }
                    }
                }
                int n11;
                if (b2) {
                    a = new a(n, super.O, super.P, super.Q, super.R, n2);
                    a.i(n6);
                    this.t1.add(a);
                    n11 = n9;
                    n7 = l2;
                }
                else {
                    if (n6 > 0) {
                        n7 += this.m1 + l2;
                    }
                    else {
                        n7 = l2;
                    }
                    n11 = 0;
                }
                a.b(constraintWidget);
                ++n6;
                n4 = n11;
                n5 = n10;
            }
        }
        else {
            int n12 = n5;
            a a2 = a;
            while (true) {
                n8 = n5;
                if (n12 >= i) {
                    break;
                }
                final ConstraintWidget constraintWidget2 = array[n12];
                final int k2 = this.k2(constraintWidget2, n2);
                int n13 = n5;
                if (constraintWidget2.T() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n13 = n5 + 1;
                }
                boolean b4;
                final boolean b3 = b4 = ((n3 == n2 || this.n1 + n3 + k2 > n2) && e.a.a(a2) != null);
                if (!b3) {
                    b4 = b3;
                    if (n12 > 0) {
                        final int r2 = this.r1;
                        b4 = b3;
                        if (r2 > 0) {
                            b4 = b3;
                            if (r2 < 0) {
                                b4 = true;
                            }
                        }
                    }
                }
                a e = null;
                int n14 = 0;
                Label_0547: {
                    if (b4) {
                        e = new a(n, super.O, super.P, super.Q, super.R, n2);
                        e.i(n12);
                        this.t1.add(e);
                    }
                    else {
                        e = a2;
                        if (n12 > 0) {
                            n14 = n3 + (this.n1 + k2);
                            e = a2;
                            break Label_0547;
                        }
                    }
                    n14 = k2;
                }
                e.b(constraintWidget2);
                ++n12;
                n3 = n14;
                n5 = n13;
                a2 = e;
            }
        }
        final int size = this.t1.size();
        ConstraintAnchor o = super.O;
        ConstraintAnchor p5 = super.P;
        ConstraintAnchor constraintAnchor = super.Q;
        ConstraintAnchor constraintAnchor2 = super.R;
        int z1 = this.z1();
        int b5 = this.B1();
        int a3 = this.A1();
        int y1 = this.y1();
        final DimensionBehaviour a4 = this.A();
        final DimensionBehaviour wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
        if (a4 != wrap_CONTENT && this.T() != wrap_CONTENT) {
            i = 0;
        }
        else {
            i = 1;
        }
        if (n8 > 0 && i != 0) {
            a a5;
            int n15;
            for (i = 0; i < size; ++i) {
                a5 = this.t1.get(i);
                if (n == 0) {
                    n15 = a5.f();
                }
                else {
                    n15 = a5.e();
                }
                a5.g(n2 - n15);
            }
        }
        int a6 = 0;
        int j;
        int a7;
        for (a7 = (j = 0); j < size; ++j, a7 = i) {
            final a a8 = this.t1.get(j);
            if (n == 0) {
                if (j < size - 1) {
                    constraintAnchor2 = e.a.a((a)this.t1.get(j + 1)).P;
                    i = 0;
                }
                else {
                    constraintAnchor2 = super.R;
                    i = this.y1();
                }
                final ConstraintAnchor r3 = e.a.a(a8).R;
                a8.j(n, o, p5, constraintAnchor, constraintAnchor2, z1, b5, a3, i, n2);
                final int max = Math.max(a7, a8.f());
                final int n16 = a6 += a8.e();
                if (j > 0) {
                    a6 = n16 + this.n1;
                }
                final int n17 = 0;
                p5 = r3;
                y1 = i;
                i = max;
                b5 = n17;
            }
            else {
                if (j < size - 1) {
                    constraintAnchor = e.a.a((a)this.t1.get(j + 1)).O;
                    i = 0;
                }
                else {
                    constraintAnchor = super.Q;
                    i = this.A1();
                }
                final ConstraintAnchor q = e.a.a(a8).Q;
                a8.j(n, o, p5, constraintAnchor, constraintAnchor2, z1, b5, i, y1, n2);
                final int n18 = a7 + a8.f();
                final int max2 = Math.max(a6, a8.e());
                int n19 = n18;
                if (j > 0) {
                    n19 = n18 + this.m1;
                }
                final int n20 = max2;
                final int n21 = 0;
                a3 = i;
                o = q;
                i = n19;
                a6 = n20;
                z1 = n21;
            }
        }
        array2[0] = a7;
        array2[1] = a6;
    }
    
    public final void p2(final ConstraintWidget[] array, final int n, int i, final int n2, final int[] array2) {
        if (n == 0) {
            return;
        }
        a e;
        if (this.t1.size() == 0) {
            e = new a(i, super.O, super.P, super.Q, super.R, n2);
            this.t1.add(e);
        }
        else {
            e = this.t1.get(0);
            e.c();
            e.j(i, super.O, super.P, super.Q, super.R, this.z1(), this.B1(), this.A1(), this.y1(), n2);
        }
        for (i = 0; i < n; ++i) {
            e.b(array[i]);
        }
        array2[0] = e.f();
        array2[1] = e.e();
    }
    
    public void q2(final float i1) {
        this.i1 = i1;
    }
    
    public void r2(final int c1) {
        this.c1 = c1;
    }
    
    public void s2(final float j1) {
        this.j1 = j1;
    }
    
    public void t2(final int d1) {
        this.d1 = d1;
    }
    
    public void u2(final int o1) {
        this.o1 = o1;
    }
    
    public void v2(final float g1) {
        this.g1 = g1;
    }
    
    public void w2(final int m1) {
        this.m1 = m1;
    }
    
    public void x2(final int a1) {
        this.a1 = a1;
    }
    
    public void y2(final float k1) {
        this.k1 = k1;
    }
    
    public void z2(final int e1) {
        this.e1 = e1;
    }
    
    public class a
    {
        public int a;
        public ConstraintWidget b;
        public int c;
        public ConstraintAnchor d;
        public ConstraintAnchor e;
        public ConstraintAnchor f;
        public ConstraintAnchor g;
        public int h;
        public int i;
        public int j;
        public int k;
        public int l;
        public int m;
        public int n;
        public int o;
        public int p;
        public int q;
        public final e r;
        
        public a(final e r, final int a, final ConstraintAnchor d, final ConstraintAnchor e, final ConstraintAnchor f, final ConstraintAnchor g, final int q) {
            this.r = r;
            this.b = null;
            this.c = 0;
            this.h = 0;
            this.i = 0;
            this.j = 0;
            this.k = 0;
            this.l = 0;
            this.m = 0;
            this.n = 0;
            this.o = 0;
            this.p = 0;
            this.q = 0;
            this.a = a;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = r.z1();
            this.i = r.B1();
            this.j = r.A1();
            this.k = r.y1();
            this.q = q;
        }
        
        public static /* synthetic */ ConstraintWidget a(final a a) {
            return a.b;
        }
        
        public void b(final ConstraintWidget constraintWidget) {
            final int a = this.a;
            int n = 0;
            final int n2 = 0;
            if (a == 0) {
                int b2 = this.r.l2(constraintWidget, this.q);
                if (constraintWidget.A() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    ++this.p;
                    b2 = 0;
                }
                int p = androidx.constraintlayout.core.widgets.e.P1(this.r);
                if (constraintWidget.V() == 8) {
                    p = n2;
                }
                this.l += b2 + p;
                final int c2 = this.r.k2(constraintWidget, this.q);
                if (this.b == null || this.c < c2) {
                    this.b = constraintWidget;
                    this.c = c2;
                    this.m = c2;
                }
            }
            else {
                final int b3 = this.r.l2(constraintWidget, this.q);
                int c3 = this.r.k2(constraintWidget, this.q);
                if (constraintWidget.T() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    ++this.p;
                    c3 = 0;
                }
                final int q1 = androidx.constraintlayout.core.widgets.e.Q1(this.r);
                if (constraintWidget.V() != 8) {
                    n = q1;
                }
                this.m += c3 + n;
                if (this.b == null || this.c < b3) {
                    this.b = constraintWidget;
                    this.c = b3;
                    this.l = b3;
                }
            }
            ++this.o;
        }
        
        public void c() {
            this.c = 0;
            this.b = null;
            this.l = 0;
            this.m = 0;
            this.n = 0;
            this.o = 0;
            this.p = 0;
        }
        
        public void d(final boolean b, int i, final boolean b2) {
            final int o = this.o;
            for (int n = 0; n < o && this.n + n < androidx.constraintlayout.core.widgets.e.d2(this.r); ++n) {
                final ConstraintWidget constraintWidget = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n];
                if (constraintWidget != null) {
                    constraintWidget.u0();
                }
            }
            if (o != 0) {
                if (this.b != null) {
                    final boolean b3 = b2 && i == 0;
                    int j = 0;
                    int n2 = -1;
                    int n3 = -1;
                    while (j < o) {
                        int n4;
                        if (b) {
                            n4 = o - 1 - j;
                        }
                        else {
                            n4 = j;
                        }
                        if (this.n + n4 >= androidx.constraintlayout.core.widgets.e.d2(this.r)) {
                            break;
                        }
                        final ConstraintWidget constraintWidget2 = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n4];
                        int n5 = n2;
                        int n6 = n3;
                        if (constraintWidget2 != null) {
                            n5 = n2;
                            n6 = n3;
                            if (constraintWidget2.V() == 0) {
                                int n7;
                                if ((n7 = n2) == -1) {
                                    n7 = j;
                                }
                                n6 = j;
                                n5 = n7;
                            }
                        }
                        ++j;
                        n2 = n5;
                        n3 = n6;
                    }
                    final int a = this.a;
                    final ConstraintWidget constraintWidget3 = null;
                    ConstraintWidget constraintWidget4 = null;
                    if (a == 0) {
                        final ConstraintWidget b4 = this.b;
                        b4.e1(androidx.constraintlayout.core.widgets.e.f2(this.r));
                        int k = this.i;
                        if (i > 0) {
                            k += androidx.constraintlayout.core.widgets.e.Q1(this.r);
                        }
                        b4.P.a(this.e, k);
                        if (b2) {
                            b4.R.a(this.g, this.k);
                        }
                        if (i > 0) {
                            this.e.d.R.a(b4.P, 0);
                        }
                        i = androidx.constraintlayout.core.widgets.e.g2(this.r);
                        final int n8 = 3;
                        ConstraintWidget constraintWidget5 = null;
                        Label_0468: {
                            if (i == 3 && !b4.Z()) {
                                int n9;
                                for (i = 0; i < o; ++i) {
                                    if (b) {
                                        n9 = o - 1 - i;
                                    }
                                    else {
                                        n9 = i;
                                    }
                                    if (this.n + n9 >= androidx.constraintlayout.core.widgets.e.d2(this.r)) {
                                        break;
                                    }
                                    constraintWidget5 = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n9];
                                    if (constraintWidget5.Z()) {
                                        break Label_0468;
                                    }
                                }
                            }
                            constraintWidget5 = b4;
                        }
                        int l = 0;
                        i = n8;
                        while (l < o) {
                            int n10;
                            if (b) {
                                n10 = o - 1 - l;
                            }
                            else {
                                n10 = l;
                            }
                            if (this.n + n10 >= androidx.constraintlayout.core.widgets.e.d2(this.r)) {
                                break;
                            }
                            final ConstraintWidget constraintWidget6 = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n10];
                            Label_1055: {
                                if (constraintWidget6 != null) {
                                    if (l == 0) {
                                        constraintWidget6.k(constraintWidget6.O, this.d, this.h);
                                    }
                                    if (n10 == 0) {
                                        final int h2 = androidx.constraintlayout.core.widgets.e.h2(this.r);
                                        float i2;
                                        final float n11 = i2 = androidx.constraintlayout.core.widgets.e.i2(this.r);
                                        if (b) {
                                            i2 = 1.0f - n11;
                                        }
                                        float n14 = 0.0f;
                                        Label_0735: {
                                            float n12 = 0.0f;
                                            Label_0663: {
                                                float n13;
                                                if (this.n == 0 && androidx.constraintlayout.core.widgets.e.R1(this.r) != -1) {
                                                    i = androidx.constraintlayout.core.widgets.e.R1(this.r);
                                                    if (!b) {
                                                        n12 = androidx.constraintlayout.core.widgets.e.S1(this.r);
                                                        break Label_0663;
                                                    }
                                                    n13 = androidx.constraintlayout.core.widgets.e.S1(this.r);
                                                }
                                                else {
                                                    i = h2;
                                                    n14 = i2;
                                                    if (!b2) {
                                                        break Label_0735;
                                                    }
                                                    i = h2;
                                                    n14 = i2;
                                                    if (androidx.constraintlayout.core.widgets.e.T1(this.r) == -1) {
                                                        break Label_0735;
                                                    }
                                                    i = androidx.constraintlayout.core.widgets.e.T1(this.r);
                                                    if (!b) {
                                                        n12 = androidx.constraintlayout.core.widgets.e.U1(this.r);
                                                        break Label_0663;
                                                    }
                                                    n13 = androidx.constraintlayout.core.widgets.e.U1(this.r);
                                                }
                                                n12 = 1.0f - n13;
                                            }
                                            n14 = n12;
                                        }
                                        constraintWidget6.N0(i);
                                        constraintWidget6.M0(n14);
                                    }
                                    if (l == o - 1) {
                                        constraintWidget6.k(constraintWidget6.Q, this.f, this.j);
                                    }
                                    if (constraintWidget4 != null) {
                                        constraintWidget6.O.a(constraintWidget4.Q, androidx.constraintlayout.core.widgets.e.P1(this.r));
                                        if (l == n2) {
                                            constraintWidget6.O.u(this.h);
                                        }
                                        constraintWidget4.Q.a(constraintWidget6.O, 0);
                                        if (l == n3 + 1) {
                                            constraintWidget4.Q.u(this.j);
                                        }
                                    }
                                    if (constraintWidget6 != b4) {
                                        final int g2 = androidx.constraintlayout.core.widgets.e.g2(this.r);
                                        i = 3;
                                        ConstraintAnchor constraintAnchor;
                                        ConstraintAnchor constraintAnchor2;
                                        if (g2 == 3 && constraintWidget5.Z() && constraintWidget6 != constraintWidget5 && constraintWidget6.Z()) {
                                            constraintAnchor = constraintWidget6.S;
                                            constraintAnchor2 = constraintWidget5.S;
                                        }
                                        else {
                                            final int g3 = androidx.constraintlayout.core.widgets.e.g2(this.r);
                                            if (g3 != 0) {
                                                if (g3 != 1) {
                                                    final ConstraintAnchor p3 = constraintWidget6.P;
                                                    if (b3) {
                                                        p3.a(this.e, this.i);
                                                        constraintWidget6.R.a(this.g, this.k);
                                                        constraintWidget4 = constraintWidget6;
                                                        break Label_1055;
                                                    }
                                                    p3.a(b4.P, 0);
                                                }
                                                constraintAnchor = constraintWidget6.R;
                                                constraintAnchor2 = b4.R;
                                            }
                                            else {
                                                constraintAnchor = constraintWidget6.P;
                                                constraintAnchor2 = b4.P;
                                            }
                                        }
                                        constraintAnchor.a(constraintAnchor2, 0);
                                        constraintWidget4 = constraintWidget6;
                                    }
                                    else {
                                        i = 3;
                                        constraintWidget4 = constraintWidget6;
                                    }
                                }
                            }
                            ++l;
                        }
                    }
                    else {
                        final ConstraintWidget b5 = this.b;
                        b5.N0(androidx.constraintlayout.core.widgets.e.h2(this.r));
                        int h3;
                        final int n15 = h3 = this.h;
                        if (i > 0) {
                            h3 = n15 + androidx.constraintlayout.core.widgets.e.P1(this.r);
                        }
                        Label_1239: {
                            ConstraintAnchor constraintAnchor3;
                            ConstraintAnchor constraintAnchor4;
                            if (b) {
                                b5.Q.a(this.f, h3);
                                if (b2) {
                                    b5.O.a(this.d, this.j);
                                }
                                if (i <= 0) {
                                    break Label_1239;
                                }
                                constraintAnchor3 = this.f.d.O;
                                constraintAnchor4 = b5.Q;
                            }
                            else {
                                b5.O.a(this.d, h3);
                                if (b2) {
                                    b5.Q.a(this.f, this.j);
                                }
                                if (i <= 0) {
                                    break Label_1239;
                                }
                                constraintAnchor3 = this.d.d.Q;
                                constraintAnchor4 = b5.O;
                            }
                            constraintAnchor3.a(constraintAnchor4, 0);
                        }
                        int n16 = 0;
                        ConstraintWidget constraintWidget7 = constraintWidget3;
                        while (n16 < o) {
                            if (this.n + n16 >= androidx.constraintlayout.core.widgets.e.d2(this.r)) {
                                break;
                            }
                            final ConstraintWidget constraintWidget8 = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n16];
                            if (constraintWidget8 != null) {
                                if (n16 == 0) {
                                    constraintWidget8.k(constraintWidget8.P, this.e, this.i);
                                    final int f2 = androidx.constraintlayout.core.widgets.e.f2(this.r);
                                    final float v1 = androidx.constraintlayout.core.widgets.e.V1(this.r);
                                    float n17;
                                    if (this.n == 0 && androidx.constraintlayout.core.widgets.e.W1(this.r) != -1) {
                                        i = androidx.constraintlayout.core.widgets.e.W1(this.r);
                                        n17 = androidx.constraintlayout.core.widgets.e.X1(this.r);
                                    }
                                    else {
                                        i = f2;
                                        n17 = v1;
                                        if (b2) {
                                            i = f2;
                                            n17 = v1;
                                            if (androidx.constraintlayout.core.widgets.e.Y1(this.r) != -1) {
                                                i = androidx.constraintlayout.core.widgets.e.Y1(this.r);
                                                n17 = androidx.constraintlayout.core.widgets.e.Z1(this.r);
                                            }
                                        }
                                    }
                                    constraintWidget8.e1(i);
                                    constraintWidget8.d1(n17);
                                }
                                if (n16 == o - 1) {
                                    constraintWidget8.k(constraintWidget8.R, this.g, this.k);
                                }
                                if (constraintWidget7 != null) {
                                    constraintWidget8.P.a(constraintWidget7.R, androidx.constraintlayout.core.widgets.e.Q1(this.r));
                                    if (n16 == n2) {
                                        constraintWidget8.P.u(this.i);
                                    }
                                    constraintWidget7.R.a(constraintWidget8.P, 0);
                                    if (n16 == n3 + 1) {
                                        constraintWidget7.R.u(this.k);
                                    }
                                }
                                Label_1759: {
                                    if (constraintWidget8 != b5) {
                                        i = androidx.constraintlayout.core.widgets.e.a2(this.r);
                                        if (b) {
                                            ConstraintAnchor constraintAnchor5 = null;
                                            ConstraintAnchor constraintAnchor6 = null;
                                            Label_1629: {
                                                if (i != 0) {
                                                    if (i == 1) {
                                                        constraintAnchor5 = constraintWidget8.O;
                                                        constraintAnchor6 = b5.O;
                                                        break Label_1629;
                                                    }
                                                    if (i != 2) {
                                                        break Label_1759;
                                                    }
                                                    constraintWidget8.O.a(b5.O, 0);
                                                }
                                                constraintAnchor5 = constraintWidget8.Q;
                                                constraintAnchor6 = b5.Q;
                                            }
                                            constraintAnchor5.a(constraintAnchor6, 0);
                                        }
                                        else {
                                            ConstraintAnchor constraintAnchor7;
                                            ConstraintAnchor constraintAnchor8;
                                            if (i != 0) {
                                                if (i != 1) {
                                                    if (i != 2) {
                                                        break Label_1759;
                                                    }
                                                    final ConstraintAnchor o2 = constraintWidget8.O;
                                                    if (b3) {
                                                        o2.a(this.d, this.h);
                                                        constraintWidget8.Q.a(this.f, this.j);
                                                        break Label_1759;
                                                    }
                                                    o2.a(b5.O, 0);
                                                }
                                                constraintAnchor7 = constraintWidget8.Q;
                                                constraintAnchor8 = b5.Q;
                                            }
                                            else {
                                                constraintAnchor7 = constraintWidget8.O;
                                                constraintAnchor8 = b5.O;
                                            }
                                            constraintAnchor7.a(constraintAnchor8, 0);
                                        }
                                    }
                                }
                                constraintWidget7 = constraintWidget8;
                            }
                            ++n16;
                        }
                    }
                }
            }
        }
        
        public int e() {
            if (this.a == 1) {
                return this.m - androidx.constraintlayout.core.widgets.e.Q1(this.r);
            }
            return this.m;
        }
        
        public int f() {
            if (this.a == 0) {
                return this.l - androidx.constraintlayout.core.widgets.e.P1(this.r);
            }
            return this.l;
        }
        
        public void g(int n) {
            final int p = this.p;
            if (p == 0) {
                return;
            }
            final int o = this.o;
            n /= p;
            for (int n2 = 0; n2 < o && this.n + n2 < androidx.constraintlayout.core.widgets.e.d2(this.r); ++n2) {
                final ConstraintWidget constraintWidget = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n2];
                e e;
                DimensionBehaviour dimensionBehaviour;
                DimensionBehaviour dimensionBehaviour2;
                int x;
                int w;
                if (this.a == 0) {
                    if (constraintWidget == null || constraintWidget.A() != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.w != 0) {
                        continue;
                    }
                    e = this.r;
                    dimensionBehaviour = DimensionBehaviour.FIXED;
                    dimensionBehaviour2 = constraintWidget.T();
                    x = constraintWidget.x();
                    w = n;
                }
                else {
                    if (constraintWidget == null || constraintWidget.T() != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.x != 0) {
                        continue;
                    }
                    e = this.r;
                    dimensionBehaviour = constraintWidget.A();
                    w = constraintWidget.W();
                    dimensionBehaviour2 = DimensionBehaviour.FIXED;
                    x = n;
                }
                e.D1(constraintWidget, dimensionBehaviour, w, dimensionBehaviour2, x);
            }
            this.h();
        }
        
        public final void h() {
            this.l = 0;
            this.m = 0;
            this.b = null;
            this.c = 0;
            for (int o = this.o, n = 0; n < o && this.n + n < androidx.constraintlayout.core.widgets.e.d2(this.r); ++n) {
                final ConstraintWidget constraintWidget = androidx.constraintlayout.core.widgets.e.e2(this.r)[this.n + n];
                if (this.a == 0) {
                    final int w = constraintWidget.W();
                    int p1 = androidx.constraintlayout.core.widgets.e.P1(this.r);
                    if (constraintWidget.V() == 8) {
                        p1 = 0;
                    }
                    this.l += w + p1;
                    final int c2 = this.r.k2(constraintWidget, this.q);
                    if (this.b == null || this.c < c2) {
                        this.b = constraintWidget;
                        this.c = c2;
                        this.m = c2;
                    }
                }
                else {
                    final int b2 = this.r.l2(constraintWidget, this.q);
                    final int c3 = this.r.k2(constraintWidget, this.q);
                    int q1 = androidx.constraintlayout.core.widgets.e.Q1(this.r);
                    if (constraintWidget.V() == 8) {
                        q1 = 0;
                    }
                    this.m += c3 + q1;
                    if (this.b == null || this.c < b2) {
                        this.b = constraintWidget;
                        this.c = b2;
                        this.l = b2;
                    }
                }
            }
        }
        
        public void i(final int n) {
            this.n = n;
        }
        
        public void j(final int a, final ConstraintAnchor d, final ConstraintAnchor e, final ConstraintAnchor f, final ConstraintAnchor g, final int h, final int i, final int j, final int k, final int q) {
            this.a = a;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
            this.i = i;
            this.j = j;
            this.k = k;
            this.q = q;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.c;

public class f extends ConstraintWidget
{
    public float L0;
    public int M0;
    public int N0;
    public boolean O0;
    public ConstraintAnchor P0;
    public int Q0;
    public int R0;
    public boolean S0;
    
    public f() {
        this.L0 = -1.0f;
        this.M0 = -1;
        this.N0 = -1;
        this.O0 = true;
        this.P0 = super.P;
        int i = 0;
        this.Q0 = 0;
        this.R0 = 0;
        super.X.clear();
        super.X.add(this.P0);
        while (i < super.W.length) {
            super.W[i] = this.P0;
            ++i;
        }
    }
    
    public void A1(int i) {
        if (this.Q0 == i) {
            return;
        }
        this.Q0 = i;
        super.X.clear();
        ConstraintAnchor p;
        if (this.Q0 == 1) {
            p = super.O;
        }
        else {
            p = super.P;
        }
        this.P0 = p;
        super.X.add(this.P0);
        int length;
        for (length = super.W.length, i = 0; i < length; ++i) {
            super.W[i] = this.P0;
        }
    }
    
    @Override
    public void g(final c c, final boolean b) {
        final d d = (d)this.K();
        if (d == null) {
            return;
        }
        ConstraintAnchor constraintAnchor = d.o(ConstraintAnchor.Type.LEFT);
        ConstraintAnchor constraintAnchor2 = d.o(ConstraintAnchor.Type.RIGHT);
        final ConstraintWidget a0 = super.a0;
        final int n = 1;
        int n2;
        if (a0 != null && a0.Z[0] == DimensionBehaviour.WRAP_CONTENT) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if (this.Q0 == 0) {
            constraintAnchor = d.o(ConstraintAnchor.Type.TOP);
            constraintAnchor2 = d.o(ConstraintAnchor.Type.BOTTOM);
            final ConstraintWidget a2 = super.a0;
            if (a2 != null && a2.Z[1] == DimensionBehaviour.WRAP_CONTENT) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
        }
        if (this.S0 && this.P0.n()) {
            final SolverVariable q = c.q(this.P0);
            c.f(q, this.P0.e());
            if (this.M0 != -1) {
                if (n2 != 0) {
                    c.h(c.q(constraintAnchor2), q, 0, 5);
                }
            }
            else if (this.N0 != -1 && n2 != 0) {
                final SolverVariable q2 = c.q(constraintAnchor2);
                c.h(q, c.q(constraintAnchor), 0, 5);
                c.h(q2, q, 0, 5);
            }
            this.S0 = false;
            return;
        }
        if (this.M0 != -1) {
            final SolverVariable q3 = c.q(this.P0);
            c.e(q3, c.q(constraintAnchor), this.M0, 8);
            if (n2 != 0) {
                c.h(c.q(constraintAnchor2), q3, 0, 5);
            }
        }
        else if (this.N0 != -1) {
            final SolverVariable q4 = c.q(this.P0);
            final SolverVariable q5 = c.q(constraintAnchor2);
            c.e(q4, q5, -this.N0, 8);
            if (n2 != 0) {
                c.h(q4, c.q(constraintAnchor), 0, 5);
                c.h(q5, q4, 0, 5);
            }
        }
        else if (this.L0 != -1.0f) {
            c.d(c.s(c, c.q(this.P0), c.q(constraintAnchor2), this.L0));
        }
    }
    
    @Override
    public boolean h() {
        return true;
    }
    
    @Override
    public boolean n0() {
        return this.S0;
    }
    
    @Override
    public ConstraintAnchor o(final ConstraintAnchor.Type type) {
        final int n = f$a.a[type.ordinal()];
        if (n != 1 && n != 2) {
            if (n == 3 || n == 4) {
                if (this.Q0 == 0) {
                    return this.P0;
                }
            }
        }
        else if (this.Q0 == 1) {
            return this.P0;
        }
        return null;
    }
    
    @Override
    public boolean o0() {
        return this.S0;
    }
    
    @Override
    public void q1(final c c, final boolean b) {
        if (this.K() == null) {
            return;
        }
        final int y = c.y(this.P0);
        if (this.Q0 == 1) {
            this.m1(y);
            this.n1(0);
            this.L0(this.K().x());
            this.k1(0);
        }
        else {
            this.m1(0);
            this.n1(y);
            this.k1(this.K().W());
            this.L0(0);
        }
    }
    
    public ConstraintAnchor r1() {
        return this.P0;
    }
    
    public int s1() {
        return this.Q0;
    }
    
    public int t1() {
        return this.M0;
    }
    
    public int u1() {
        return this.N0;
    }
    
    public float v1() {
        return this.L0;
    }
    
    public void w1(final int n) {
        this.P0.t(n);
        this.S0 = true;
    }
    
    public void x1(final int m0) {
        if (m0 > -1) {
            this.L0 = -1.0f;
            this.M0 = m0;
            this.N0 = -1;
        }
    }
    
    public void y1(final int n0) {
        if (n0 > -1) {
            this.L0 = -1.0f;
            this.M0 = -1;
            this.N0 = n0;
        }
    }
    
    public void z1(final float l0) {
        if (l0 > -1.0f) {
            this.L0 = l0;
            this.M0 = -1;
            this.N0 = -1;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.c;

public abstract class g
{
    public static boolean[] a;
    
    static {
        g.a = new boolean[3];
    }
    
    public static void a(final d d, final c c, final ConstraintWidget constraintWidget) {
        constraintWidget.t = -1;
        constraintWidget.u = -1;
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = d.Z[0];
        final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (dimensionBehaviour != wrap_CONTENT && constraintWidget.Z[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final int g = constraintWidget.O.g;
            final int n = d.W() - constraintWidget.Q.g;
            final ConstraintAnchor o = constraintWidget.O;
            o.i = c.q(o);
            final ConstraintAnchor q = constraintWidget.Q;
            q.i = c.q(q);
            c.f(constraintWidget.O.i, g);
            c.f(constraintWidget.Q.i, n);
            constraintWidget.t = 2;
            constraintWidget.O0(g, n);
        }
        if (d.Z[1] != wrap_CONTENT && constraintWidget.Z[1] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final int g2 = constraintWidget.P.g;
            final int n2 = d.x() - constraintWidget.R.g;
            final ConstraintAnchor p3 = constraintWidget.P;
            p3.i = c.q(p3);
            final ConstraintAnchor r = constraintWidget.R;
            r.i = c.q(r);
            c.f(constraintWidget.P.i, g2);
            c.f(constraintWidget.R.i, n2);
            if (constraintWidget.l0 > 0 || constraintWidget.V() == 8) {
                final ConstraintAnchor s = constraintWidget.S;
                s.i = c.q(s);
                c.f(constraintWidget.S.i, constraintWidget.l0 + g2);
            }
            constraintWidget.u = 2;
            constraintWidget.f1(g2, n2);
        }
    }
    
    public static final boolean b(final int n, final int n2) {
        return (n & n2) == n2;
    }
}

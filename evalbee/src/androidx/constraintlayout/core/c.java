// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Arrays;
import java.util.HashMap;

public class c
{
    public static boolean r = false;
    public static boolean s = true;
    public static boolean t = true;
    public static boolean u = true;
    public static boolean v = false;
    public static int w = 1000;
    public static long x;
    public static long y;
    public boolean a;
    public int b;
    public HashMap c;
    public a d;
    public int e;
    public int f;
    public androidx.constraintlayout.core.b[] g;
    public boolean h;
    public boolean i;
    public boolean[] j;
    public int k;
    public int l;
    public int m;
    public final oe n;
    public SolverVariable[] o;
    public int p;
    public a q;
    
    public c() {
        this.a = false;
        this.b = 0;
        this.c = null;
        this.e = 32;
        this.f = 32;
        this.g = null;
        this.h = false;
        this.i = false;
        this.j = new boolean[32];
        this.k = 1;
        this.l = 0;
        this.m = 32;
        this.o = new SolverVariable[androidx.constraintlayout.core.c.w];
        this.p = 0;
        this.g = new androidx.constraintlayout.core.b[32];
        this.D();
        final oe n = new oe();
        this.n = n;
        this.d = (a)new d(n);
        androidx.constraintlayout.core.b q;
        if (androidx.constraintlayout.core.c.v) {
            q = new b(n);
        }
        else {
            q = new androidx.constraintlayout.core.b(n);
        }
        this.q = (a)q;
    }
    
    public static androidx.constraintlayout.core.b s(final c c, final SolverVariable solverVariable, final SolverVariable solverVariable2, final float n) {
        return c.r().j(solverVariable, solverVariable2, n);
    }
    
    public static hw0 x() {
        return null;
    }
    
    public void A() {
        if (this.d.isEmpty()) {
            this.n();
            return;
        }
        Label_0034: {
            if (this.h || this.i) {
                final int n = 0;
                int i = 0;
                while (true) {
                    while (i < this.l) {
                        if (!this.g[i].f) {
                            final int n2 = n;
                            if (n2 == 0) {
                                break Label_0034;
                            }
                            this.n();
                            return;
                        }
                        else {
                            ++i;
                        }
                    }
                    final int n2 = 1;
                    continue;
                }
            }
        }
        this.B(this.d);
    }
    
    public void B(final a a) {
        this.u(a);
        this.C(a, false);
        this.n();
    }
    
    public final int C(final a a, final boolean b) {
        for (int i = 0; i < this.k; ++i) {
            this.j[i] = false;
        }
        int j = 0;
        int n = 0;
        while (j == 0) {
            final int n2 = n + 1;
            if (n2 >= this.k * 2) {
                return n2;
            }
            if (a.getKey() != null) {
                this.j[a.getKey().c] = true;
            }
            final SolverVariable a2 = a.a(this, this.j);
            if (a2 != null) {
                final boolean[] k = this.j;
                final int c = a2.c;
                if (k[c]) {
                    return n2;
                }
                k[c] = true;
            }
            if (a2 != null) {
                float n3 = Float.MAX_VALUE;
                int l = 0;
                int d = -1;
                while (l < this.l) {
                    final androidx.constraintlayout.core.b b2 = this.g[l];
                    float n4;
                    int n5;
                    if (b2.a.j == SolverVariable.Type.UNRESTRICTED) {
                        n4 = n3;
                        n5 = d;
                    }
                    else if (b2.f) {
                        n4 = n3;
                        n5 = d;
                    }
                    else {
                        n4 = n3;
                        n5 = d;
                        if (b2.t(a2)) {
                            final float d2 = b2.e.d(a2);
                            n4 = n3;
                            n5 = d;
                            if (d2 < 0.0f) {
                                final float n6 = -b2.b / d2;
                                n4 = n3;
                                n5 = d;
                                if (n6 < n3) {
                                    n5 = l;
                                    n4 = n6;
                                }
                            }
                        }
                    }
                    ++l;
                    n3 = n4;
                    d = n5;
                }
                n = n2;
                if (d <= -1) {
                    continue;
                }
                final androidx.constraintlayout.core.b b3 = this.g[d];
                b3.a.d = -1;
                b3.x(a2);
                final SolverVariable a3 = b3.a;
                a3.d = d;
                a3.j(this, b3);
                n = n2;
            }
            else {
                j = 1;
                n = n2;
            }
        }
        return n;
    }
    
    public final void D() {
        final boolean v = androidx.constraintlayout.core.c.v;
        int i = 0;
        final int n = 0;
        if (v) {
            for (int j = n; j < this.l; ++j) {
                final androidx.constraintlayout.core.b b = this.g[j];
                if (b != null) {
                    this.n.a.b(b);
                }
                this.g[j] = null;
            }
        }
        else {
            while (i < this.l) {
                final androidx.constraintlayout.core.b b2 = this.g[i];
                if (b2 != null) {
                    this.n.b.b(b2);
                }
                this.g[i] = null;
                ++i;
            }
        }
    }
    
    public void E() {
        int n = 0;
        oe n2;
        while (true) {
            n2 = this.n;
            final SolverVariable[] d = n2.d;
            if (n >= d.length) {
                break;
            }
            final SolverVariable solverVariable = d[n];
            if (solverVariable != null) {
                solverVariable.f();
            }
            ++n;
        }
        n2.c.c(this.o, this.p);
        this.p = 0;
        Arrays.fill(this.n.d, null);
        final HashMap c = this.c;
        if (c != null) {
            c.clear();
        }
        this.b = 0;
        this.d.clear();
        this.k = 1;
        for (int i = 0; i < this.l; ++i) {
            final androidx.constraintlayout.core.b b = this.g[i];
            if (b != null) {
                b.c = false;
            }
        }
        this.D();
        this.l = 0;
        androidx.constraintlayout.core.b q;
        if (androidx.constraintlayout.core.c.v) {
            q = new b(this.n);
        }
        else {
            q = new androidx.constraintlayout.core.b(this.n);
        }
        this.q = (a)q;
    }
    
    public final SolverVariable a(final SolverVariable.Type type, final String s) {
        SolverVariable solverVariable = (SolverVariable)this.n.c.a();
        if (solverVariable == null) {
            solverVariable = new SolverVariable(type, s);
        }
        else {
            solverVariable.f();
        }
        solverVariable.h(type, s);
        final int p2 = this.p;
        final int w = androidx.constraintlayout.core.c.w;
        if (p2 >= w) {
            this.o = Arrays.copyOf(this.o, androidx.constraintlayout.core.c.w = w * 2);
        }
        return this.o[this.p++] = solverVariable;
    }
    
    public void b(final ConstraintWidget constraintWidget, final ConstraintWidget constraintWidget2, final float n, final int n2) {
        final ConstraintAnchor.Type left = ConstraintAnchor.Type.LEFT;
        final SolverVariable q = this.q(constraintWidget.o(left));
        final ConstraintAnchor.Type top = ConstraintAnchor.Type.TOP;
        final SolverVariable q2 = this.q(constraintWidget.o(top));
        final ConstraintAnchor.Type right = ConstraintAnchor.Type.RIGHT;
        final SolverVariable q3 = this.q(constraintWidget.o(right));
        final ConstraintAnchor.Type bottom = ConstraintAnchor.Type.BOTTOM;
        final SolverVariable q4 = this.q(constraintWidget.o(bottom));
        final SolverVariable q5 = this.q(constraintWidget2.o(left));
        final SolverVariable q6 = this.q(constraintWidget2.o(top));
        final SolverVariable q7 = this.q(constraintWidget2.o(right));
        final SolverVariable q8 = this.q(constraintWidget2.o(bottom));
        final androidx.constraintlayout.core.b r = this.r();
        final double n3 = n;
        final double sin = Math.sin(n3);
        final double n4 = n2;
        r.q(q2, q4, q6, q8, (float)(sin * n4));
        this.d(r);
        final androidx.constraintlayout.core.b r2 = this.r();
        r2.q(q, q3, q5, q7, (float)(Math.cos(n3) * n4));
        this.d(r2);
    }
    
    public void c(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final float n2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n3, final int n4) {
        final androidx.constraintlayout.core.b r = this.r();
        r.h(solverVariable, solverVariable2, n, n2, solverVariable3, solverVariable4, n3);
        if (n4 != 8) {
            r.d(this, n4);
        }
        this.d(r);
    }
    
    public void d(final androidx.constraintlayout.core.b b) {
        if (b == null) {
            return;
        }
        final int l = this.l;
        final boolean b2 = true;
        if (l + 1 >= this.m || this.k + 1 >= this.f) {
            this.z();
        }
        final boolean f = b.f;
        int n = 0;
        if (!f) {
            b.D(this);
            if (b.isEmpty()) {
                return;
            }
            b.r();
            Label_0237: {
                if (b.f(this)) {
                    final SolverVariable p = this.p();
                    b.a = p;
                    final int i = this.l;
                    this.l(b);
                    if (this.l == i + 1) {
                        this.q.b((a)b);
                        this.C(this.q, true);
                        n = (b2 ? 1 : 0);
                        if (p.d == -1) {
                            if (b.a == p) {
                                final SolverVariable v = b.v(p);
                                if (v != null) {
                                    b.x(v);
                                }
                            }
                            if (!b.f) {
                                b.a.j(this, b);
                            }
                            x51 x51;
                            if (androidx.constraintlayout.core.c.v) {
                                x51 = this.n.a;
                            }
                            else {
                                x51 = this.n.b;
                            }
                            x51.b(b);
                            --this.l;
                            n = (b2 ? 1 : 0);
                        }
                        break Label_0237;
                    }
                }
                n = 0;
            }
            if (!b.s()) {
                return;
            }
        }
        if (n == 0) {
            this.l(b);
        }
    }
    
    public androidx.constraintlayout.core.b e(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        if (androidx.constraintlayout.core.c.s && n2 == 8 && solverVariable2.g && solverVariable.d == -1) {
            solverVariable.g(this, solverVariable2.f + n);
            return null;
        }
        final androidx.constraintlayout.core.b r = this.r();
        r.n(solverVariable, solverVariable2, n);
        if (n2 != 8) {
            r.d(this, n2);
        }
        this.d(r);
        return r;
    }
    
    public void f(final SolverVariable solverVariable, int i) {
        if (androidx.constraintlayout.core.c.s && solverVariable.d == -1) {
            final float n = (float)i;
            solverVariable.g(this, n);
            SolverVariable solverVariable2;
            for (i = 0; i < this.b + 1; ++i) {
                solverVariable2 = this.n.d[i];
                if (solverVariable2 != null && solverVariable2.n && solverVariable2.p == solverVariable.c) {
                    solverVariable2.g(this, solverVariable2.q + n);
                }
            }
            return;
        }
        final int d = solverVariable.d;
        androidx.constraintlayout.core.b b2 = null;
        Label_0189: {
            if (d != -1) {
                final androidx.constraintlayout.core.b b = this.g[d];
                if (!b.f) {
                    if (b.e.h() != 0) {
                        final androidx.constraintlayout.core.b r = this.r();
                        r.m(solverVariable, i);
                        b2 = r;
                        break Label_0189;
                    }
                    b.f = true;
                }
                b.b = (float)i;
                return;
            }
            final androidx.constraintlayout.core.b r2 = this.r();
            r2.i(solverVariable, i);
            b2 = r2;
        }
        this.d(b2);
    }
    
    public void g(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final androidx.constraintlayout.core.b r = this.r();
        final SolverVariable t = this.t();
        t.e = 0;
        r.o(solverVariable, solverVariable2, t, n);
        this.d(r);
    }
    
    public void h(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final androidx.constraintlayout.core.b r = this.r();
        final SolverVariable t = this.t();
        t.e = 0;
        r.o(solverVariable, solverVariable2, t, n);
        if (n2 != 8) {
            this.m(r, (int)(r.e.d(t) * -1.0f), n2);
        }
        this.d(r);
    }
    
    public void i(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final androidx.constraintlayout.core.b r = this.r();
        final SolverVariable t = this.t();
        t.e = 0;
        r.p(solverVariable, solverVariable2, t, n);
        this.d(r);
    }
    
    public void j(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final androidx.constraintlayout.core.b r = this.r();
        final SolverVariable t = this.t();
        t.e = 0;
        r.p(solverVariable, solverVariable2, t, n);
        if (n2 != 8) {
            this.m(r, (int)(r.e.d(t) * -1.0f), n2);
        }
        this.d(r);
    }
    
    public void k(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n, final int n2) {
        final androidx.constraintlayout.core.b r = this.r();
        r.k(solverVariable, solverVariable2, solverVariable3, solverVariable4, n);
        if (n2 != 8) {
            r.d(this, n2);
        }
        this.d(r);
    }
    
    public final void l(androidx.constraintlayout.core.b b) {
        if (androidx.constraintlayout.core.c.t && b.f) {
            b.a.g(this, b.b);
        }
        else {
            final androidx.constraintlayout.core.b[] g = this.g;
            final int l = this.l;
            g[l] = b;
            final SolverVariable a = b.a;
            a.d = l;
            this.l = l + 1;
            a.j(this, b);
        }
        if (androidx.constraintlayout.core.c.t && this.a) {
            int n;
            for (int i = 0; i < this.l; i = n + 1) {
                if (this.g[i] == null) {
                    System.out.println("WTF");
                }
                final androidx.constraintlayout.core.b b2 = this.g[i];
                n = i;
                if (b2 != null) {
                    n = i;
                    if (b2.f) {
                        b2.a.g(this, b2.b);
                        x51 x51;
                        if (androidx.constraintlayout.core.c.v) {
                            x51 = this.n.a;
                        }
                        else {
                            x51 = this.n.b;
                        }
                        x51.b(b2);
                        this.g[i] = null;
                        int n3;
                        int n2 = n3 = i + 1;
                        int j;
                        while (true) {
                            j = this.l;
                            if (n2 >= j) {
                                break;
                            }
                            final androidx.constraintlayout.core.b[] g2 = this.g;
                            final int d = n2 - 1;
                            b = g2[n2];
                            g2[d] = b;
                            final SolverVariable a2 = b.a;
                            if (a2.d == n2) {
                                a2.d = d;
                            }
                            n3 = n2;
                            ++n2;
                        }
                        if (n3 < j) {
                            this.g[n3] = null;
                        }
                        this.l = j - 1;
                        n = i - 1;
                    }
                }
            }
            this.a = false;
        }
    }
    
    public void m(final androidx.constraintlayout.core.b b, final int n, final int n2) {
        b.e(this.o(n2, null), n);
    }
    
    public final void n() {
        for (int i = 0; i < this.l; ++i) {
            final androidx.constraintlayout.core.b b = this.g[i];
            b.a.f = b.b;
        }
    }
    
    public SolverVariable o(final int e, final String s) {
        if (this.k + 1 >= this.f) {
            this.z();
        }
        final SolverVariable a = this.a(SolverVariable.Type.ERROR, s);
        final int n = this.b + 1;
        this.b = n;
        ++this.k;
        a.c = n;
        a.e = e;
        this.n.d[n] = a;
        this.d.c(a);
        return a;
    }
    
    public SolverVariable p() {
        if (this.k + 1 >= this.f) {
            this.z();
        }
        final SolverVariable a = this.a(SolverVariable.Type.SLACK, null);
        final int n = this.b + 1;
        this.b = n;
        ++this.k;
        a.c = n;
        return this.n.d[n] = a;
    }
    
    public SolverVariable q(final Object o) {
        SolverVariable solverVariable = null;
        if (o == null) {
            return null;
        }
        if (this.k + 1 >= this.f) {
            this.z();
        }
        if (o instanceof ConstraintAnchor) {
            final ConstraintAnchor constraintAnchor = (ConstraintAnchor)o;
            SolverVariable solverVariable2;
            if ((solverVariable2 = constraintAnchor.i()) == null) {
                constraintAnchor.s(this.n);
                solverVariable2 = constraintAnchor.i();
            }
            final int c = solverVariable2.c;
            if (c != -1 && c <= this.b) {
                solverVariable = solverVariable2;
                if (this.n.d[c] != null) {
                    return solverVariable;
                }
            }
            if (c != -1) {
                solverVariable2.f();
            }
            final int n = this.b + 1;
            this.b = n;
            ++this.k;
            solverVariable2.c = n;
            solverVariable2.j = SolverVariable.Type.UNRESTRICTED;
            this.n.d[n] = solverVariable2;
            solverVariable = solverVariable2;
        }
        return solverVariable;
    }
    
    public androidx.constraintlayout.core.b r() {
        androidx.constraintlayout.core.b b = null;
        Label_0101: {
            if (androidx.constraintlayout.core.c.v) {
                if ((b = (androidx.constraintlayout.core.b)this.n.a.a()) == null) {
                    b = new b(this.n);
                    ++androidx.constraintlayout.core.c.y;
                    break Label_0101;
                }
            }
            else if ((b = (androidx.constraintlayout.core.b)this.n.b.a()) == null) {
                b = new androidx.constraintlayout.core.b(this.n);
                ++androidx.constraintlayout.core.c.x;
                break Label_0101;
            }
            b.y();
        }
        SolverVariable.d();
        return b;
    }
    
    public SolverVariable t() {
        if (this.k + 1 >= this.f) {
            this.z();
        }
        final SolverVariable a = this.a(SolverVariable.Type.SLACK, null);
        final int n = this.b + 1;
        this.b = n;
        ++this.k;
        a.c = n;
        return this.n.d[n] = a;
    }
    
    public final int u(final a a) {
        while (true) {
            for (int i = 0; i < this.l; ++i) {
                final androidx.constraintlayout.core.b b = this.g[i];
                if (b.a.j != SolverVariable.Type.UNRESTRICTED) {
                    if (b.b < 0.0f) {
                        final boolean b2 = true;
                        int n;
                        if (b2) {
                            int j = 0;
                            n = 0;
                            while (j == 0) {
                                final int n2 = n + 1;
                                float n3 = Float.MAX_VALUE;
                                int k = 0;
                                int d = -1;
                                int c = -1;
                                int n4 = 0;
                                while (k < this.l) {
                                    final androidx.constraintlayout.core.b b3 = this.g[k];
                                    float n5;
                                    int n6;
                                    int n7;
                                    int n8;
                                    if (b3.a.j == SolverVariable.Type.UNRESTRICTED) {
                                        n5 = n3;
                                        n6 = d;
                                        n7 = c;
                                        n8 = n4;
                                    }
                                    else if (b3.f) {
                                        n5 = n3;
                                        n6 = d;
                                        n7 = c;
                                        n8 = n4;
                                    }
                                    else {
                                        n5 = n3;
                                        n6 = d;
                                        n7 = c;
                                        n8 = n4;
                                        if (b3.b < 0.0f) {
                                            if (androidx.constraintlayout.core.c.u) {
                                                final int h = b3.e.h();
                                                int n9 = 0;
                                                while (true) {
                                                    n5 = n3;
                                                    n6 = d;
                                                    n7 = c;
                                                    n8 = n4;
                                                    if (n9 >= h) {
                                                        break;
                                                    }
                                                    final SolverVariable b4 = b3.e.b(n9);
                                                    final float d2 = b3.e.d(b4);
                                                    float n10;
                                                    int n11;
                                                    int n12;
                                                    int n13;
                                                    if (d2 <= 0.0f) {
                                                        n10 = n3;
                                                        n11 = d;
                                                        n12 = c;
                                                        n13 = n4;
                                                    }
                                                    else {
                                                        final int n14 = 0;
                                                        int n15 = d;
                                                        int n16 = n14;
                                                        while (true) {
                                                            n10 = n3;
                                                            n11 = n15;
                                                            n12 = c;
                                                            n13 = n4;
                                                            if (n16 >= 9) {
                                                                break;
                                                            }
                                                            final float n17 = b4.h[n16] / d2;
                                                            int n18;
                                                            if ((n17 < n3 && n16 == n4) || n16 > (n18 = n4)) {
                                                                c = b4.c;
                                                                n18 = n16;
                                                                n15 = k;
                                                                n3 = n17;
                                                            }
                                                            ++n16;
                                                            n4 = n18;
                                                        }
                                                    }
                                                    ++n9;
                                                    n3 = n10;
                                                    d = n11;
                                                    c = n12;
                                                    n4 = n13;
                                                }
                                            }
                                            else {
                                                int n19 = 1;
                                                while (true) {
                                                    n5 = n3;
                                                    n6 = d;
                                                    n7 = c;
                                                    n8 = n4;
                                                    if (n19 >= this.k) {
                                                        break;
                                                    }
                                                    final SolverVariable solverVariable = this.n.d[n19];
                                                    final float d3 = b3.e.d(solverVariable);
                                                    float n20;
                                                    int n21;
                                                    int n22;
                                                    int n23;
                                                    if (d3 <= 0.0f) {
                                                        n20 = n3;
                                                        n21 = d;
                                                        n22 = c;
                                                        n23 = n4;
                                                    }
                                                    else {
                                                        int n24 = 0;
                                                        while (true) {
                                                            n20 = n3;
                                                            n21 = d;
                                                            n22 = c;
                                                            n23 = n4;
                                                            if (n24 >= 9) {
                                                                break;
                                                            }
                                                            final float n25 = solverVariable.h[n24] / d3;
                                                            int n26;
                                                            if ((n25 < n3 && n24 == n4) || n24 > (n26 = n4)) {
                                                                c = n19;
                                                                d = k;
                                                                n26 = n24;
                                                                n3 = n25;
                                                            }
                                                            ++n24;
                                                            n4 = n26;
                                                        }
                                                    }
                                                    ++n19;
                                                    n3 = n20;
                                                    d = n21;
                                                    c = n22;
                                                    n4 = n23;
                                                }
                                            }
                                        }
                                    }
                                    ++k;
                                    n3 = n5;
                                    d = n6;
                                    c = n7;
                                    n4 = n8;
                                }
                                if (d != -1) {
                                    final androidx.constraintlayout.core.b b5 = this.g[d];
                                    b5.a.d = -1;
                                    b5.x(this.n.d[c]);
                                    final SolverVariable a2 = b5.a;
                                    a2.d = d;
                                    a2.j(this, b5);
                                }
                                else {
                                    j = 1;
                                }
                                n = n2;
                                if (n2 > this.k / 2) {
                                    j = 1;
                                    n = n2;
                                }
                            }
                        }
                        else {
                            n = 0;
                        }
                        return n;
                    }
                }
            }
            final boolean b2 = false;
            continue;
        }
    }
    
    public void v(final hw0 hw0) {
    }
    
    public oe w() {
        return this.n;
    }
    
    public int y(final Object o) {
        final SolverVariable i = ((ConstraintAnchor)o).i();
        if (i != null) {
            return (int)(i.f + 0.5f);
        }
        return 0;
    }
    
    public final void z() {
        final int n = this.e * 2;
        this.e = n;
        this.g = Arrays.copyOf(this.g, n);
        final oe n2 = this.n;
        n2.d = Arrays.copyOf(n2.d, this.e);
        final int e = this.e;
        this.j = new boolean[e];
        this.f = e;
        this.m = e;
    }
    
    public interface a
    {
        SolverVariable a(final c p0, final boolean[] p1);
        
        void b(final a p0);
        
        void c(final SolverVariable p0);
        
        void clear();
        
        SolverVariable getKey();
        
        boolean isEmpty();
    }
    
    public class b extends androidx.constraintlayout.core.b
    {
        public final c g;
        
        public b(final c g, final oe oe) {
            this.g = g;
            super.e = new e(this, oe);
        }
    }
}

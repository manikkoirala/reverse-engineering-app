// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.util.Arrays;

public class e implements a
{
    public static float n = 0.001f;
    public final int a;
    public int b;
    public int c;
    public int[] d;
    public int[] e;
    public int[] f;
    public float[] g;
    public int[] h;
    public int[] i;
    public int j;
    public int k;
    public final b l;
    public final oe m;
    
    public e(final b l, final oe m) {
        this.a = -1;
        this.b = 16;
        this.c = 16;
        this.d = new int[16];
        this.e = new int[16];
        this.f = new int[16];
        this.g = new float[16];
        this.h = new int[16];
        this.i = new int[16];
        this.j = 0;
        this.k = -1;
        this.l = l;
        this.m = m;
        this.clear();
    }
    
    @Override
    public boolean a(final SolverVariable solverVariable) {
        return this.o(solverVariable) != -1;
    }
    
    @Override
    public SolverVariable b(final int n) {
        final int j = this.j;
        if (j == 0) {
            return null;
        }
        int k = this.k;
        for (int i = 0; i < j; ++i) {
            if (i == n && k != -1) {
                return this.m.d[this.f[k]];
            }
            k = this.i[k];
            if (k == -1) {
                break;
            }
        }
        return null;
    }
    
    @Override
    public void c(final SolverVariable solverVariable, final float n) {
        final float n2 = androidx.constraintlayout.core.e.n;
        if (n > -n2 && n < n2) {
            this.g(solverVariable, true);
            return;
        }
        final int j = this.j;
        int n3 = 0;
        if (j == 0) {
            this.l(0, solverVariable, n);
            this.k(solverVariable, 0);
            this.k = 0;
        }
        else {
            final int o = this.o(solverVariable);
            if (o != -1) {
                this.g[o] = n;
            }
            else {
                if (this.j + 1 >= this.b) {
                    this.n();
                }
                final int i = this.j;
                int k = this.k;
                int n4 = -1;
                int n5;
                while (true) {
                    n5 = n4;
                    if (n3 >= i) {
                        break;
                    }
                    final int n6 = this.f[k];
                    final int c = solverVariable.c;
                    if (n6 == c) {
                        this.g[k] = n;
                        return;
                    }
                    if (n6 < c) {
                        n4 = k;
                    }
                    k = this.i[k];
                    if (k == -1) {
                        n5 = n4;
                        break;
                    }
                    ++n3;
                }
                this.p(n5, solverVariable, n);
            }
        }
    }
    
    @Override
    public void clear() {
        for (int j = this.j, i = 0; i < j; ++i) {
            final SolverVariable b = this.b(i);
            if (b != null) {
                b.e(this.l);
            }
        }
        for (int k = 0; k < this.b; ++k) {
            this.f[k] = -1;
            this.e[k] = -1;
        }
        for (int l = 0; l < this.c; ++l) {
            this.d[l] = -1;
        }
        this.j = 0;
        this.k = -1;
    }
    
    @Override
    public float d(final SolverVariable solverVariable) {
        final int o = this.o(solverVariable);
        if (o != -1) {
            return this.g[o];
        }
        return 0.0f;
    }
    
    @Override
    public void e(final float n) {
        final int j = this.j;
        int k = this.k;
        for (int i = 0; i < j; ++i) {
            final float[] g = this.g;
            g[k] /= n;
            k = this.i[k];
            if (k == -1) {
                break;
            }
        }
    }
    
    @Override
    public void f(final SolverVariable solverVariable, float n, final boolean b) {
        final float n2 = androidx.constraintlayout.core.e.n;
        if (n > -n2 && n < n2) {
            return;
        }
        final int o = this.o(solverVariable);
        if (o == -1) {
            this.c(solverVariable, n);
        }
        else {
            final float[] g = this.g;
            final float n3 = g[o] + n;
            g[o] = n3;
            n = androidx.constraintlayout.core.e.n;
            if (n3 > -n && n3 < n) {
                g[o] = 0.0f;
                this.g(solverVariable, b);
            }
        }
    }
    
    @Override
    public float g(final SolverVariable solverVariable, final boolean b) {
        final int o = this.o(solverVariable);
        if (o == -1) {
            return 0.0f;
        }
        this.q(solverVariable);
        final float n = this.g[o];
        if (this.k == o) {
            this.k = this.i[o];
        }
        this.f[o] = -1;
        final int[] h = this.h;
        final int n2 = h[o];
        if (n2 != -1) {
            final int[] i = this.i;
            i[n2] = i[o];
        }
        final int n3 = this.i[o];
        if (n3 != -1) {
            h[n3] = h[o];
        }
        --this.j;
        --solverVariable.m;
        if (b) {
            solverVariable.e(this.l);
        }
        return n;
    }
    
    @Override
    public int h() {
        return this.j;
    }
    
    @Override
    public float i(final b b, final boolean b2) {
        final float d = this.d(b.a);
        this.g(b.a, b2);
        final e e = (e)b.e;
        final int h = e.h();
        int i = 0;
        int n = 0;
        while (i < h) {
            final int n2 = e.f[n];
            int n3 = i;
            if (n2 != -1) {
                this.f(this.m.d[n2], e.g[n] * d, b2);
                n3 = i + 1;
            }
            ++n;
            i = n3;
        }
        return d;
    }
    
    @Override
    public void invert() {
        final int j = this.j;
        int k = this.k;
        for (int i = 0; i < j; ++i) {
            final float[] g = this.g;
            g[k] *= -1.0f;
            k = this.i[k];
            if (k == -1) {
                break;
            }
        }
    }
    
    @Override
    public float j(final int n) {
        final int j = this.j;
        int k = this.k;
        for (int i = 0; i < j; ++i) {
            if (i == n) {
                return this.g[k];
            }
            k = this.i[k];
            if (k == -1) {
                break;
            }
        }
        return 0.0f;
    }
    
    public final void k(final SolverVariable solverVariable, final int n) {
        final int n2 = solverVariable.c % this.c;
        final int[] d = this.d;
        int n3;
        if ((n3 = d[n2]) == -1) {
            d[n2] = n;
        }
        else {
            int[] e;
            while (true) {
                e = this.e;
                final int n4 = e[n3];
                if (n4 == -1) {
                    break;
                }
                n3 = n4;
            }
            e[n3] = n;
        }
        this.e[n] = -1;
    }
    
    public final void l(final int n, final SolverVariable solverVariable, final float n2) {
        this.f[n] = solverVariable.c;
        this.g[n] = n2;
        this.h[n] = -1;
        this.i[n] = -1;
        solverVariable.a(this.l);
        ++solverVariable.m;
        ++this.j;
    }
    
    public final int m() {
        for (int i = 0; i < this.b; ++i) {
            if (this.f[i] == -1) {
                return i;
            }
        }
        return -1;
    }
    
    public final void n() {
        final int n = this.b * 2;
        this.f = Arrays.copyOf(this.f, n);
        this.g = Arrays.copyOf(this.g, n);
        this.h = Arrays.copyOf(this.h, n);
        this.i = Arrays.copyOf(this.i, n);
        this.e = Arrays.copyOf(this.e, n);
        for (int i = this.b; i < n; ++i) {
            this.f[i] = -1;
            this.e[i] = -1;
        }
        this.b = n;
    }
    
    public int o(final SolverVariable solverVariable) {
        if (this.j != 0) {
            if (solverVariable != null) {
                final int c = solverVariable.c;
                final int n = this.d[c % this.c];
                if (n == -1) {
                    return -1;
                }
                int n2 = n;
                if (this.f[n] == c) {
                    return n;
                }
                do {
                    n2 = this.e[n2];
                } while (n2 != -1 && this.f[n2] != c);
                if (n2 == -1) {
                    return -1;
                }
                if (this.f[n2] == c) {
                    return n2;
                }
            }
        }
        return -1;
    }
    
    public final void p(int n, final SolverVariable solverVariable, final float n2) {
        final int m = this.m();
        this.l(m, solverVariable, n2);
        if (n != -1) {
            this.h[m] = n;
            final int[] i = this.i;
            i[m] = i[n];
            i[n] = m;
        }
        else {
            this.h[m] = -1;
            if (this.j > 0) {
                this.i[m] = this.k;
                this.k = m;
            }
            else {
                this.i[m] = -1;
            }
        }
        n = this.i[m];
        if (n != -1) {
            this.h[n] = m;
        }
        this.k(solverVariable, m);
    }
    
    public final void q(final SolverVariable solverVariable) {
        final int c = solverVariable.c;
        final int n = c % this.c;
        final int[] d = this.d;
        final int n2 = d[n];
        if (n2 == -1) {
            return;
        }
        int n3 = n2;
        if (this.f[n2] == c) {
            final int[] e = this.e;
            d[n] = e[n2];
            e[n2] = -1;
        }
        else {
            int[] e2;
            int n4;
            while (true) {
                e2 = this.e;
                n4 = e2[n3];
                if (n4 == -1 || this.f[n4] == c) {
                    break;
                }
                n3 = n4;
            }
            if (n4 != -1 && this.f[n4] == c) {
                e2[n3] = e2[n4];
                e2[n4] = -1;
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.hashCode());
        sb.append(" { ");
        String s = sb.toString();
        for (int j = this.j, i = 0; i < j; ++i) {
            final SolverVariable b = this.b(i);
            if (b != null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s);
                sb2.append(b);
                sb2.append(" = ");
                sb2.append(this.j(i));
                sb2.append(" ");
                final String string = sb2.toString();
                final int o = this.o(b);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string);
                sb3.append("[p: ");
                final String string2 = sb3.toString();
                StringBuilder sb4;
                if (this.h[o] != -1) {
                    sb4 = new StringBuilder();
                    sb4.append(string2);
                    sb4.append(this.m.d[this.f[this.h[o]]]);
                }
                else {
                    sb4 = new StringBuilder();
                    sb4.append(string2);
                    sb4.append("none");
                }
                final String string3 = sb4.toString();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string3);
                sb5.append(", n: ");
                final String string4 = sb5.toString();
                String str;
                if (this.i[o] != -1) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append(string4);
                    sb6.append(this.m.d[this.f[this.i[o]]]);
                    str = sb6.toString();
                }
                else {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append(string4);
                    sb7.append("none");
                    str = sb7.toString();
                }
                final StringBuilder sb8 = new StringBuilder();
                sb8.append(str);
                sb8.append("]");
                s = sb8.toString();
            }
        }
        final StringBuilder sb9 = new StringBuilder();
        sb9.append(s);
        sb9.append(" }");
        return sb9.toString();
    }
}

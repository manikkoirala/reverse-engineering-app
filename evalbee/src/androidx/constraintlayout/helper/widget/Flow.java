// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import android.view.View$MeasureSpec;
import androidx.constraintlayout.core.widgets.h;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.core.widgets.e;

public class Flow extends i52
{
    public e l;
    
    public Flow(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public void i(final AttributeSet set) {
        super.i(set);
        this.l = new e();
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, vb1.n1);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == vb1.o1) {
                    this.l.D2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.p1) {
                    this.l.I1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.z1) {
                    this.l.N1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.A1) {
                    this.l.K1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.q1) {
                    this.l.L1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.r1) {
                    this.l.O1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.s1) {
                    this.l.M1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.t1) {
                    this.l.J1(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.Z1) {
                    this.l.I2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.P1) {
                    this.l.x2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.Y1) {
                    this.l.H2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.J1) {
                    this.l.r2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.R1) {
                    this.l.z2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.L1) {
                    this.l.t2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.T1) {
                    this.l.B2(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == vb1.N1) {
                    this.l.v2(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == vb1.I1) {
                    this.l.q2(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == vb1.Q1) {
                    this.l.y2(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == vb1.K1) {
                    this.l.s2(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == vb1.S1) {
                    this.l.A2(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == vb1.W1) {
                    this.l.F2(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == vb1.M1) {
                    this.l.u2(obtainStyledAttributes.getInt(index, 2));
                }
                else if (index == vb1.V1) {
                    this.l.E2(obtainStyledAttributes.getInt(index, 2));
                }
                else if (index == vb1.O1) {
                    this.l.w2(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.X1) {
                    this.l.G2(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == vb1.U1) {
                    this.l.C2(obtainStyledAttributes.getInt(index, -1));
                }
            }
            obtainStyledAttributes.recycle();
        }
        super.d = this.l;
        this.o();
    }
    
    @Override
    public void j(final ConstraintWidget constraintWidget, final boolean b) {
        this.l.t1(b);
    }
    
    @Override
    public void onMeasure(final int n, final int n2) {
        this.p(this.l, n, n2);
    }
    
    @Override
    public void p(final h h, int mode, int size) {
        final int mode2 = View$MeasureSpec.getMode(mode);
        final int size2 = View$MeasureSpec.getSize(mode);
        mode = View$MeasureSpec.getMode(size);
        size = View$MeasureSpec.getSize(size);
        if (h != null) {
            h.C1(mode2, size2, mode, size);
            this.setMeasuredDimension(h.x1(), h.w1());
        }
        else {
            this.setMeasuredDimension(0, 0);
        }
    }
    
    public void setFirstHorizontalBias(final float n) {
        this.l.q2(n);
        this.requestLayout();
    }
    
    public void setFirstHorizontalStyle(final int n) {
        this.l.r2(n);
        this.requestLayout();
    }
    
    public void setFirstVerticalBias(final float n) {
        this.l.s2(n);
        this.requestLayout();
    }
    
    public void setFirstVerticalStyle(final int n) {
        this.l.t2(n);
        this.requestLayout();
    }
    
    public void setHorizontalAlign(final int n) {
        this.l.u2(n);
        this.requestLayout();
    }
    
    public void setHorizontalBias(final float n) {
        this.l.v2(n);
        this.requestLayout();
    }
    
    public void setHorizontalGap(final int n) {
        this.l.w2(n);
        this.requestLayout();
    }
    
    public void setHorizontalStyle(final int n) {
        this.l.x2(n);
        this.requestLayout();
    }
    
    public void setLastHorizontalBias(final float n) {
        this.l.y2(n);
        this.requestLayout();
    }
    
    public void setLastHorizontalStyle(final int n) {
        this.l.z2(n);
        this.requestLayout();
    }
    
    public void setLastVerticalBias(final float n) {
        this.l.A2(n);
        this.requestLayout();
    }
    
    public void setLastVerticalStyle(final int n) {
        this.l.B2(n);
        this.requestLayout();
    }
    
    public void setMaxElementsWrap(final int n) {
        this.l.C2(n);
        this.requestLayout();
    }
    
    public void setOrientation(final int n) {
        this.l.D2(n);
        this.requestLayout();
    }
    
    public void setPadding(final int n) {
        this.l.I1(n);
        this.requestLayout();
    }
    
    public void setPaddingBottom(final int n) {
        this.l.J1(n);
        this.requestLayout();
    }
    
    public void setPaddingLeft(final int n) {
        this.l.L1(n);
        this.requestLayout();
    }
    
    public void setPaddingRight(final int n) {
        this.l.M1(n);
        this.requestLayout();
    }
    
    public void setPaddingTop(final int n) {
        this.l.O1(n);
        this.requestLayout();
    }
    
    public void setVerticalAlign(final int n) {
        this.l.E2(n);
        this.requestLayout();
    }
    
    public void setVerticalBias(final float n) {
        this.l.F2(n);
        this.requestLayout();
    }
    
    public void setVerticalGap(final int n) {
        this.l.G2(n);
        this.requestLayout();
    }
    
    public void setVerticalStyle(final int n) {
        this.l.H2(n);
        this.requestLayout();
    }
    
    public void setWrapMode(final int n) {
        this.l.I2(n);
        this.requestLayout();
    }
}

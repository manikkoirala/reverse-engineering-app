// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

public final class StartupException extends RuntimeException
{
    public StartupException(final String message) {
        super(message);
    }
    
    public StartupException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public StartupException(final Throwable cause) {
        super(cause);
    }
}

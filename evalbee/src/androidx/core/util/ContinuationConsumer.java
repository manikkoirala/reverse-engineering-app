// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.Result;
import org.jetbrains.annotations.NotNull;
import java.util.function.Consumer;
import java.util.concurrent.atomic.AtomicBoolean;

final class ContinuationConsumer<T> extends AtomicBoolean implements Consumer<T>
{
    @NotNull
    private final vl continuation;
    
    public ContinuationConsumer(@NotNull final vl continuation) {
        fg0.e((Object)continuation, "continuation");
        super(false);
        this.continuation = continuation;
    }
    
    @Override
    public void accept(final T t) {
        if (this.compareAndSet(false, true)) {
            this.continuation.resumeWith(Result.constructor-impl((Object)t));
        }
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationConsumer(resultAccepted = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

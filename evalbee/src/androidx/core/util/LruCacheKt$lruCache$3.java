// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Lambda;

public final class LruCacheKt$lruCache$3 extends Lambda implements u90
{
    public static final LruCacheKt$lruCache$3 INSTANCE;
    
    static {
        INSTANCE = new LruCacheKt$lruCache$3();
    }
    
    public LruCacheKt$lruCache$3() {
        super(4);
    }
    
    public final void invoke(final boolean b, @NotNull final Object o, @NotNull final Object o2, @Nullable final Object o3) {
        fg0.e(o, "<anonymous parameter 1>");
        fg0.e(o2, "<anonymous parameter 2>");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Lambda;

public final class LruCacheKt$lruCache$2 extends Lambda implements c90
{
    public static final LruCacheKt$lruCache$2 INSTANCE;
    
    static {
        INSTANCE = new LruCacheKt$lruCache$2();
    }
    
    public LruCacheKt$lruCache$2() {
        super(1);
    }
    
    @Nullable
    public final Object invoke(@NotNull final Object o) {
        fg0.e(o, "it");
        return null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Lambda;

public final class LruCacheKt$lruCache$1 extends Lambda implements q90
{
    public static final LruCacheKt$lruCache$1 INSTANCE;
    
    static {
        INSTANCE = new LruCacheKt$lruCache$1();
    }
    
    public LruCacheKt$lruCache$1() {
        super(2);
    }
    
    @NotNull
    public final Integer invoke(@NotNull final Object o, @NotNull final Object o2) {
        fg0.e(o, "<anonymous parameter 0>");
        fg0.e(o2, "<anonymous parameter 1>");
        return 1;
    }
}

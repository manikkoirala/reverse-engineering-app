// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.Result$a;
import kotlin.Result;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicBoolean;

final class ContinuationRunnable extends AtomicBoolean implements Runnable
{
    @NotNull
    private final vl continuation;
    
    public ContinuationRunnable(@NotNull final vl continuation) {
        fg0.e((Object)continuation, "continuation");
        super(false);
        this.continuation = continuation;
    }
    
    @Override
    public void run() {
        if (this.compareAndSet(false, true)) {
            final vl continuation = this.continuation;
            final Result$a companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl((Object)u02.a));
        }
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationRunnable(ran = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

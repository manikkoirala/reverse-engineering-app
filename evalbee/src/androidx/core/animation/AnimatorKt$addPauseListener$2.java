// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.animation;

import org.jetbrains.annotations.NotNull;
import android.animation.Animator;
import kotlin.jvm.internal.Lambda;

final class AnimatorKt$addPauseListener$2 extends Lambda implements c90
{
    public static final AnimatorKt$addPauseListener$2 INSTANCE;
    
    static {
        INSTANCE = new AnimatorKt$addPauseListener$2();
    }
    
    public AnimatorKt$addPauseListener$2() {
        super(1);
    }
    
    public final void invoke(@NotNull final Animator animator) {
        fg0.e((Object)animator, "it");
    }
}

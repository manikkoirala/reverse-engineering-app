// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.AbsSavedState;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import android.os.Bundle;
import android.view.accessibility.AccessibilityRecord;
import android.widget.ScrollView;
import android.view.accessibility.AccessibilityEvent;
import android.view.ViewConfiguration;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.View$MeasureSpec;
import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.FocusFinder;
import android.view.ViewGroup$LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout$LayoutParams;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.util.TypedValue;
import android.content.res.TypedArray;
import android.view.ViewGroup;
import android.util.AttributeSet;
import android.content.Context;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import android.graphics.Rect;
import android.widget.FrameLayout;

public class NestedScrollView extends FrameLayout implements oy0, ly0
{
    public static final float H;
    public static final a I;
    public static final int[] J;
    public d A;
    public final py0 C;
    public final my0 D;
    public float F;
    public c G;
    public final float a;
    public long b;
    public final Rect c;
    public OverScroller d;
    public EdgeEffect e;
    public EdgeEffect f;
    public int g;
    public boolean h;
    public boolean i;
    public View j;
    public boolean k;
    public VelocityTracker l;
    public boolean m;
    public boolean n;
    public int p;
    public int q;
    public int t;
    public int v;
    public final int[] w;
    public final int[] x;
    public int y;
    public int z;
    
    static {
        H = (float)(Math.log(0.78) / Math.log(0.9));
        I = new a();
        J = new int[] { 16843130 };
    }
    
    public NestedScrollView(final Context context, final AttributeSet set) {
        this(context, set, qa1.c);
    }
    
    public NestedScrollView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.c = new Rect();
        this.h = true;
        this.i = false;
        this.j = null;
        this.k = false;
        this.n = true;
        this.v = -1;
        this.w = new int[2];
        this.x = new int[2];
        this.e = yv.a(context, set);
        this.f = yv.a(context, set);
        this.a = context.getResources().getDisplayMetrics().density * 160.0f * 386.0878f * 0.84f;
        this.u();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, NestedScrollView.J, n, 0);
        this.setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.C = new py0((ViewGroup)this);
        this.D = new my0((View)this);
        this.setNestedScrollingEnabled(true);
        o32.q0((View)this, NestedScrollView.I);
    }
    
    public static int e(final int n, final int n2, final int n3) {
        if (n2 >= n3 || n < 0) {
            return 0;
        }
        if (n2 + n > n3) {
            return n3 - n2;
        }
        return n;
    }
    
    private float getVerticalScrollFactorCompat() {
        if (this.F == 0.0f) {
            final TypedValue typedValue = new TypedValue();
            final Context context = ((View)this).getContext();
            if (!context.getTheme().resolveAttribute(16842829, typedValue, true)) {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
            this.F = typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return this.F;
    }
    
    public static boolean x(final View view, final View view2) {
        boolean b = true;
        if (view == view2) {
            return true;
        }
        final ViewParent parent = view.getParent();
        if (!(parent instanceof ViewGroup) || !x((View)parent, view2)) {
            b = false;
        }
        return b;
    }
    
    public final void A(final MotionEvent motionEvent) {
        final int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.v) {
            int n;
            if (actionIndex == 0) {
                n = 1;
            }
            else {
                n = 0;
            }
            this.g = (int)motionEvent.getY(n);
            this.v = motionEvent.getPointerId(n);
            final VelocityTracker l = this.l;
            if (l != null) {
                l.clear();
            }
        }
    }
    
    public boolean B(int n, int n2, int n3, int n4, int n5, final int n6, int n7, final int n8, final boolean b) {
        final int overScrollMode = ((View)this).getOverScrollMode();
        final int computeHorizontalScrollRange = this.computeHorizontalScrollRange();
        final int computeHorizontalScrollExtent = this.computeHorizontalScrollExtent();
        final boolean b2 = false;
        final boolean b3 = computeHorizontalScrollRange > computeHorizontalScrollExtent;
        final boolean b4 = this.computeVerticalScrollRange() > this.computeVerticalScrollExtent();
        final boolean b5 = overScrollMode == 0 || (overScrollMode == 1 && b3);
        final boolean b6 = overScrollMode == 0 || (overScrollMode == 1 && b4);
        n3 += n;
        if (!b5) {
            n = 0;
        }
        else {
            n = n7;
        }
        n4 += n2;
        if (!b6) {
            n2 = 0;
        }
        else {
            n2 = n8;
        }
        n7 = -n;
        n += n5;
        n5 = -n2;
        n2 += n6;
        boolean b7;
        if (n3 > n) {
            b7 = true;
        }
        else if (n3 < n7) {
            b7 = true;
            n = n7;
        }
        else {
            b7 = false;
            n = n3;
        }
        boolean b8;
        if (n4 > n2) {
            b8 = true;
        }
        else if (n4 < n5) {
            b8 = true;
            n2 = n5;
        }
        else {
            b8 = false;
            n2 = n4;
        }
        if (b8 && !this.r(1)) {
            this.d.springBack(n, n2, 0, 0, 0, this.getScrollRange());
        }
        this.onOverScrolled(n, n2, b7, b8);
        if (!b7) {
            final boolean b9 = b2;
            if (!b8) {
                return b9;
            }
        }
        return true;
    }
    
    public boolean C(final int n) {
        final boolean b = n == 130;
        final int height = ((View)this).getHeight();
        if (b) {
            this.c.top = ((View)this).getScrollY() + height;
            final int childCount = ((ViewGroup)this).getChildCount();
            if (childCount > 0) {
                final View child = ((ViewGroup)this).getChildAt(childCount - 1);
                final int n2 = child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin + ((View)this).getPaddingBottom();
                final Rect c = this.c;
                if (c.top + height > n2) {
                    c.top = n2 - height;
                }
            }
        }
        else {
            this.c.top = ((View)this).getScrollY() - height;
            final Rect c2 = this.c;
            if (c2.top < 0) {
                c2.top = 0;
            }
        }
        final Rect c3 = this.c;
        final int top = c3.top;
        final int bottom = height + top;
        c3.bottom = bottom;
        return this.G(n, top, bottom);
    }
    
    public final void D() {
        final VelocityTracker l = this.l;
        if (l != null) {
            l.recycle();
            this.l = null;
        }
    }
    
    public final int E(int round, float d) {
        final float n = d / ((View)this).getWidth();
        final float n2 = round / (float)((View)this).getHeight();
        final float b = yv.b(this.e);
        d = 0.0f;
        Label_0076: {
            EdgeEffect edgeEffect;
            if (b != 0.0f) {
                final float n3 = d = -yv.d(this.e, -n2, n);
                if (yv.b(this.e) != 0.0f) {
                    break Label_0076;
                }
                edgeEffect = this.e;
                d = n3;
            }
            else {
                if (yv.b(this.f) == 0.0f) {
                    break Label_0076;
                }
                final float n4 = d = yv.d(this.f, n2, 1.0f - n);
                if (yv.b(this.f) != 0.0f) {
                    break Label_0076;
                }
                edgeEffect = this.f;
                d = n4;
            }
            edgeEffect.onRelease();
        }
        round = Math.round(d * ((View)this).getHeight());
        if (round != 0) {
            ((View)this).invalidate();
        }
        return round;
    }
    
    public final void F(final boolean b) {
        if (b) {
            this.O(2, 1);
        }
        else {
            this.Q(1);
        }
        this.z = ((View)this).getScrollY();
        o32.h0((View)this);
    }
    
    public final boolean G(final int n, int n2, final int n3) {
        final int height = ((View)this).getHeight();
        final int scrollY = ((View)this).getScrollY();
        final int n4 = height + scrollY;
        final boolean b = false;
        final boolean b2 = n == 33;
        Object n5;
        if ((n5 = this.n(b2, n2, n3)) == null) {
            n5 = this;
        }
        boolean b3;
        if (n2 >= scrollY && n3 <= n4) {
            b3 = b;
        }
        else {
            if (b2) {
                n2 -= scrollY;
            }
            else {
                n2 = n3 - n4;
            }
            this.j(n2);
            b3 = true;
        }
        if (n5 != ((View)this).findFocus()) {
            ((View)n5).requestFocus(n);
        }
        return b3;
    }
    
    public final void H(final View view) {
        view.getDrawingRect(this.c);
        ((ViewGroup)this).offsetDescendantRectToMyCoords(view, this.c);
        final int f = this.f(this.c);
        if (f != 0) {
            ((View)this).scrollBy(0, f);
        }
    }
    
    public final boolean I(final Rect rect, final boolean b) {
        final int f = this.f(rect);
        final boolean b2 = f != 0;
        if (b2) {
            if (b) {
                ((View)this).scrollBy(0, f);
            }
            else {
                this.K(0, f);
            }
        }
        return b2;
    }
    
    public final boolean J(final EdgeEffect edgeEffect, final int n) {
        boolean b = true;
        if (n > 0) {
            return true;
        }
        if (this.q(-n) >= yv.b(edgeEffect) * ((View)this).getHeight()) {
            b = false;
        }
        return b;
    }
    
    public final void K(final int n, final int n2) {
        this.L(n, n2, 250, false);
    }
    
    public final void L(int scrollY, int max, final int n, final boolean b) {
        if (((ViewGroup)this).getChildCount() == 0) {
            return;
        }
        if (AnimationUtils.currentAnimationTimeMillis() - this.b > 250L) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            final int height = child.getHeight();
            final int topMargin = frameLayout$LayoutParams.topMargin;
            final int bottomMargin = frameLayout$LayoutParams.bottomMargin;
            final int height2 = ((View)this).getHeight();
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            scrollY = ((View)this).getScrollY();
            max = Math.max(0, Math.min(max + scrollY, Math.max(0, height + topMargin + bottomMargin - (height2 - paddingTop - paddingBottom))));
            this.d.startScroll(((View)this).getScrollX(), scrollY, 0, max - scrollY, n);
            this.F(b);
        }
        else {
            if (!this.d.isFinished()) {
                this.a();
            }
            ((View)this).scrollBy(scrollY, max);
        }
        this.b = AnimationUtils.currentAnimationTimeMillis();
    }
    
    public void M(final int n, final int n2, final int n3, final boolean b) {
        this.L(n - ((View)this).getScrollX(), n2 - ((View)this).getScrollY(), n3, b);
    }
    
    public void N(final int n, final int n2, final boolean b) {
        this.M(n, n2, 250, b);
    }
    
    public boolean O(final int n, final int n2) {
        return this.D.q(n, n2);
    }
    
    public final boolean P(final MotionEvent motionEvent) {
        final float b = yv.b(this.e);
        final boolean b2 = true;
        boolean b3;
        if (b != 0.0f) {
            yv.d(this.e, 0.0f, motionEvent.getX() / ((View)this).getWidth());
            b3 = true;
        }
        else {
            b3 = false;
        }
        if (yv.b(this.f) != 0.0f) {
            yv.d(this.f, 0.0f, 1.0f - motionEvent.getX() / ((View)this).getWidth());
            b3 = b2;
        }
        return b3;
    }
    
    public void Q(final int n) {
        this.D.s(n);
    }
    
    public final void a() {
        this.d.abortAnimation();
        this.Q(1);
    }
    
    public void addView(final View view) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public void addView(final View view, final int n) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view, n);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view, n, viewGroup$LayoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public void addView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view, viewGroup$LayoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public boolean b(int descendantFocusability) {
        View focus;
        if ((focus = ((View)this).findFocus()) == this) {
            focus = null;
        }
        final View nextFocus = FocusFinder.getInstance().findNextFocus((ViewGroup)this, focus, descendantFocusability);
        final int maxScrollAmount = this.getMaxScrollAmount();
        if (nextFocus != null && this.y(nextFocus, maxScrollAmount, ((View)this).getHeight())) {
            nextFocus.getDrawingRect(this.c);
            ((ViewGroup)this).offsetDescendantRectToMyCoords(nextFocus, this.c);
            this.j(this.f(this.c));
            nextFocus.requestFocus(descendantFocusability);
        }
        else {
            int n;
            if (descendantFocusability == 33 && ((View)this).getScrollY() < maxScrollAmount) {
                n = ((View)this).getScrollY();
            }
            else {
                n = maxScrollAmount;
                if (descendantFocusability == 130) {
                    n = maxScrollAmount;
                    if (((ViewGroup)this).getChildCount() > 0) {
                        final View child = ((ViewGroup)this).getChildAt(0);
                        n = Math.min(child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin - (((View)this).getScrollY() + ((View)this).getHeight() - ((View)this).getPaddingBottom()), maxScrollAmount);
                    }
                }
            }
            if (n == 0) {
                return false;
            }
            if (descendantFocusability != 130) {
                n = -n;
            }
            this.j(n);
        }
        if (focus != null && focus.isFocused() && this.w(focus)) {
            descendantFocusability = ((ViewGroup)this).getDescendantFocusability();
            ((ViewGroup)this).setDescendantFocusability(131072);
            ((View)this).requestFocus();
            ((ViewGroup)this).setDescendantFocusability(descendantFocusability);
        }
        return true;
    }
    
    public final boolean c() {
        final int overScrollMode = ((View)this).getOverScrollMode();
        boolean b = true;
        if (overScrollMode != 0) {
            b = (overScrollMode == 1 && this.getScrollRange() > 0 && b);
        }
        return b;
    }
    
    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }
    
    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }
    
    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }
    
    public void computeScroll() {
        if (this.d.isFinished()) {
            return;
        }
        this.d.computeScrollOffset();
        final int currY = this.d.getCurrY();
        final int g = this.g(currY - this.z);
        this.z = currY;
        final int[] x = this.x;
        final boolean b = false;
        this.h(x[1] = 0, g, x, null, 1);
        final int n = g - this.x[1];
        final int scrollRange = this.getScrollRange();
        int n2;
        if ((n2 = n) != 0) {
            final int scrollY = ((View)this).getScrollY();
            this.B(0, n, ((View)this).getScrollX(), scrollY, 0, scrollRange, 0, 0, false);
            final int n3 = ((View)this).getScrollY() - scrollY;
            final int n4 = n - n3;
            final int[] x2 = this.x;
            this.i(x2[1] = 0, n3, 0, n4, this.w, 1, x2);
            n2 = n4 - this.x[1];
        }
        if (n2 != 0) {
            final int overScrollMode = ((View)this).getOverScrollMode();
            int n5 = 0;
            Label_0189: {
                if (overScrollMode != 0) {
                    n5 = (b ? 1 : 0);
                    if (overScrollMode != 1) {
                        break Label_0189;
                    }
                    n5 = (b ? 1 : 0);
                    if (scrollRange <= 0) {
                        break Label_0189;
                    }
                }
                n5 = 1;
            }
            Label_0245: {
                if (n5 != 0) {
                    EdgeEffect edgeEffect;
                    if (n2 < 0) {
                        if (!this.e.isFinished()) {
                            break Label_0245;
                        }
                        edgeEffect = this.e;
                    }
                    else {
                        if (!this.f.isFinished()) {
                            break Label_0245;
                        }
                        edgeEffect = this.f;
                    }
                    edgeEffect.onAbsorb((int)this.d.getCurrVelocity());
                }
            }
            this.a();
        }
        if (!this.d.isFinished()) {
            o32.h0((View)this);
        }
        else {
            this.Q(1);
        }
    }
    
    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }
    
    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }
    
    public int computeVerticalScrollRange() {
        final int childCount = ((ViewGroup)this).getChildCount();
        final int n = ((View)this).getHeight() - ((View)this).getPaddingBottom() - ((View)this).getPaddingTop();
        if (childCount == 0) {
            return n;
        }
        final View child = ((ViewGroup)this).getChildAt(0);
        final int n2 = child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin;
        final int scrollY = ((View)this).getScrollY();
        final int max = Math.max(0, n2 - n);
        int n3;
        if (scrollY < 0) {
            n3 = n2 - scrollY;
        }
        else {
            n3 = n2;
            if (scrollY > max) {
                n3 = n2 + (scrollY - max);
            }
        }
        return n3;
    }
    
    public final boolean d() {
        final int childCount = ((ViewGroup)this).getChildCount();
        boolean b = false;
        if (childCount > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            b = b;
            if (child.getHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin > ((View)this).getHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom()) {
                b = true;
            }
        }
        return b;
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || this.m(keyEvent);
    }
    
    public boolean dispatchNestedFling(final float n, final float n2, final boolean b) {
        return this.D.a(n, n2, b);
    }
    
    public boolean dispatchNestedPreFling(final float n, final float n2) {
        return this.D.b(n, n2);
    }
    
    public boolean dispatchNestedPreScroll(final int n, final int n2, final int[] array, final int[] array2) {
        return this.h(n, n2, array, array2, 0);
    }
    
    public boolean dispatchNestedScroll(final int n, final int n2, final int n3, final int n4, final int[] array) {
        return this.D.f(n, n2, n3, n4, array);
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final int scrollY = ((View)this).getScrollY();
        final boolean finished = this.e.isFinished();
        final int n = 0;
        if (!finished) {
            final int save = canvas.save();
            int width = ((View)this).getWidth();
            final int height = ((View)this).getHeight();
            final int min = Math.min(0, scrollY);
            int n2;
            if (NestedScrollView.b.a((ViewGroup)this)) {
                width -= ((View)this).getPaddingLeft() + ((View)this).getPaddingRight();
                n2 = ((View)this).getPaddingLeft() + 0;
            }
            else {
                n2 = 0;
            }
            int n3 = height;
            int n4 = min;
            if (NestedScrollView.b.a((ViewGroup)this)) {
                n3 = height - (((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
                n4 = min + ((View)this).getPaddingTop();
            }
            canvas.translate((float)n2, (float)n4);
            this.e.setSize(width, n3);
            if (this.e.draw(canvas)) {
                o32.h0((View)this);
            }
            canvas.restoreToCount(save);
        }
        if (!this.f.isFinished()) {
            final int save2 = canvas.save();
            final int width2 = ((View)this).getWidth();
            final int height2 = ((View)this).getHeight();
            final int n5 = Math.max(this.getScrollRange(), scrollY) + height2;
            int n6 = n;
            int n7 = width2;
            if (NestedScrollView.b.a((ViewGroup)this)) {
                n7 = width2 - (((View)this).getPaddingLeft() + ((View)this).getPaddingRight());
                n6 = 0 + ((View)this).getPaddingLeft();
            }
            int n8 = n5;
            int n9 = height2;
            if (NestedScrollView.b.a((ViewGroup)this)) {
                n9 = height2 - (((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
                n8 = n5 - ((View)this).getPaddingBottom();
            }
            canvas.translate((float)(n6 - n7), (float)n8);
            canvas.rotate(180.0f, (float)n7, 0.0f);
            this.f.setSize(n7, n9);
            if (this.f.draw(canvas)) {
                o32.h0((View)this);
            }
            canvas.restoreToCount(save2);
        }
    }
    
    public int f(final Rect rect) {
        final int childCount = ((ViewGroup)this).getChildCount();
        final boolean b = false;
        if (childCount == 0) {
            return 0;
        }
        final int height = ((View)this).getHeight();
        final int scrollY = ((View)this).getScrollY();
        final int n = scrollY + height;
        final int verticalFadingEdgeLength = ((View)this).getVerticalFadingEdgeLength();
        int n2 = scrollY;
        if (rect.top > 0) {
            n2 = scrollY + verticalFadingEdgeLength;
        }
        final View child = ((ViewGroup)this).getChildAt(0);
        final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
        int n3;
        if (rect.bottom < child.getHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin) {
            n3 = n - verticalFadingEdgeLength;
        }
        else {
            n3 = n;
        }
        final int bottom = rect.bottom;
        int n5;
        if (bottom > n3 && rect.top > n2) {
            int n4;
            if (rect.height() > height) {
                n4 = rect.top - n2;
            }
            else {
                n4 = rect.bottom - n3;
            }
            n5 = Math.min(n4 + 0, child.getBottom() + frameLayout$LayoutParams.bottomMargin - n);
        }
        else {
            n5 = (b ? 1 : 0);
            if (rect.top < n2) {
                n5 = (b ? 1 : 0);
                if (bottom < n3) {
                    int a;
                    if (rect.height() > height) {
                        a = 0 - (n3 - rect.bottom);
                    }
                    else {
                        a = 0 - (n2 - rect.top);
                    }
                    n5 = Math.max(a, -((View)this).getScrollY());
                }
            }
        }
        return n5;
    }
    
    public int g(final int n) {
        final int height = ((View)this).getHeight();
        if (n > 0 && yv.b(this.e) != 0.0f) {
            final int round = Math.round(-height / 4.0f * yv.d(this.e, -n * 4.0f / height, 0.5f));
            if (round != n) {
                this.e.finish();
            }
            return n - round;
        }
        int n2;
        if ((n2 = n) < 0) {
            n2 = n;
            if (yv.b(this.f) != 0.0f) {
                final float n3 = (float)n;
                final float n4 = (float)height;
                final int round2 = Math.round(n4 / 4.0f * yv.d(this.f, n3 * 4.0f / n4, 0.5f));
                if (round2 != n) {
                    this.f.finish();
                }
                n2 = n - round2;
            }
        }
        return n2;
    }
    
    public float getBottomFadingEdgeStrength() {
        if (((ViewGroup)this).getChildCount() == 0) {
            return 0.0f;
        }
        final View child = ((ViewGroup)this).getChildAt(0);
        final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
        final int verticalFadingEdgeLength = ((View)this).getVerticalFadingEdgeLength();
        final int n = child.getBottom() + frameLayout$LayoutParams.bottomMargin - ((View)this).getScrollY() - (((View)this).getHeight() - ((View)this).getPaddingBottom());
        if (n < verticalFadingEdgeLength) {
            return n / (float)verticalFadingEdgeLength;
        }
        return 1.0f;
    }
    
    public int getMaxScrollAmount() {
        return (int)(((View)this).getHeight() * 0.5f);
    }
    
    public int getNestedScrollAxes() {
        return this.C.a();
    }
    
    public int getScrollRange() {
        final int childCount = ((ViewGroup)this).getChildCount();
        int max = 0;
        if (childCount > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            max = Math.max(0, child.getHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin - (((View)this).getHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom()));
        }
        return max;
    }
    
    public float getTopFadingEdgeStrength() {
        if (((ViewGroup)this).getChildCount() == 0) {
            return 0.0f;
        }
        final int verticalFadingEdgeLength = ((View)this).getVerticalFadingEdgeLength();
        final int scrollY = ((View)this).getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return scrollY / (float)verticalFadingEdgeLength;
        }
        return 1.0f;
    }
    
    public boolean h(final int n, final int n2, final int[] array, final int[] array2, final int n3) {
        return this.D.d(n, n2, array, array2, n3);
    }
    
    public boolean hasNestedScrollingParent() {
        return this.r(0);
    }
    
    public void i(final int n, final int n2, final int n3, final int n4, final int[] array, final int n5, final int[] array2) {
        this.D.e(n, n2, n3, n4, array, n5, array2);
    }
    
    public boolean isNestedScrollingEnabled() {
        return this.D.m();
    }
    
    public final void j(final int n) {
        if (n != 0) {
            if (this.n) {
                this.K(0, n);
            }
            else {
                ((View)this).scrollBy(0, n);
            }
        }
    }
    
    public final boolean k(int n) {
        Label_0042: {
            EdgeEffect edgeEffect;
            if (yv.b(this.e) != 0.0f) {
                if (!this.J(this.e, n)) {
                    n = -n;
                    break Label_0042;
                }
                edgeEffect = this.e;
            }
            else {
                if (yv.b(this.f) == 0.0f) {
                    return false;
                }
                final EdgeEffect f = this.f;
                final int n2 = n = -n;
                if (!this.J(f, n2)) {
                    break Label_0042;
                }
                edgeEffect = this.f;
                n = n2;
            }
            edgeEffect.onAbsorb(n);
            return true;
        }
        this.o(n);
        return true;
    }
    
    public final void l() {
        this.k = false;
        this.D();
        this.Q(0);
        this.e.onRelease();
        this.f.onRelease();
    }
    
    public boolean m(final KeyEvent keyEvent) {
        this.c.setEmpty();
        final boolean d = this.d();
        final boolean b = false;
        final boolean b2 = false;
        int n = 130;
        if (!d) {
            boolean b3 = b2;
            if (((View)this).isFocused()) {
                b3 = b2;
                if (keyEvent.getKeyCode() != 4) {
                    View focus;
                    if ((focus = ((View)this).findFocus()) == this) {
                        focus = null;
                    }
                    final View nextFocus = FocusFinder.getInstance().findNextFocus((ViewGroup)this, focus, 130);
                    b3 = b2;
                    if (nextFocus != null) {
                        b3 = b2;
                        if (nextFocus != this) {
                            b3 = b2;
                            if (nextFocus.requestFocus(130)) {
                                b3 = true;
                            }
                        }
                    }
                }
            }
            return b3;
        }
        boolean b4 = b;
        if (keyEvent.getAction() == 0) {
            final int keyCode = keyEvent.getKeyCode();
            if (keyCode != 19) {
                if (keyCode != 20) {
                    if (keyCode != 62) {
                        b4 = b;
                    }
                    else {
                        if (keyEvent.isShiftPressed()) {
                            n = 33;
                        }
                        this.C(n);
                        b4 = b;
                    }
                }
                else if (!keyEvent.isAltPressed()) {
                    b4 = this.b(130);
                }
                else {
                    b4 = this.p(130);
                }
            }
            else if (!keyEvent.isAltPressed()) {
                b4 = this.b(33);
            }
            else {
                b4 = this.p(33);
            }
        }
        return b4;
    }
    
    public void measureChild(final View view, final int n, final int n2) {
        view.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight(), view.getLayoutParams().width), View$MeasureSpec.makeMeasureSpec(0, 0));
    }
    
    public void measureChildWithMargins(final View view, final int n, final int n2, final int n3, final int n4) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        view.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight() + viewGroup$MarginLayoutParams.leftMargin + viewGroup$MarginLayoutParams.rightMargin + n2, viewGroup$MarginLayoutParams.width), View$MeasureSpec.makeMeasureSpec(viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin, 0));
    }
    
    public final View n(final boolean b, final int n, final int n2) {
        final ArrayList focusables = ((View)this).getFocusables(2);
        final int size = focusables.size();
        View view = null;
        int i = 0;
        int n3 = 0;
        while (i < size) {
            final View view2 = (View)focusables.get(i);
            final int top = view2.getTop();
            final int bottom = view2.getBottom();
            View view3 = view;
            int n4 = n3;
            Label_0232: {
                if (n < bottom) {
                    view3 = view;
                    n4 = n3;
                    if (top < n2) {
                        final boolean b2 = n < top && bottom < n2;
                        if (view == null) {
                            view3 = view2;
                            n4 = (b2 ? 1 : 0);
                        }
                        else {
                            final boolean b3 = (b && top < view.getTop()) || (!b && bottom > view.getBottom());
                            if (n3 != 0) {
                                view3 = view;
                                n4 = n3;
                                if (!b2) {
                                    break Label_0232;
                                }
                                view3 = view;
                                n4 = n3;
                                if (!b3) {
                                    break Label_0232;
                                }
                            }
                            else {
                                if (b2) {
                                    view3 = view2;
                                    n4 = 1;
                                    break Label_0232;
                                }
                                view3 = view;
                                n4 = n3;
                                if (!b3) {
                                    break Label_0232;
                                }
                            }
                            view3 = view2;
                            n4 = n3;
                        }
                    }
                }
            }
            ++i;
            view = view3;
            n3 = n4;
        }
        return view;
    }
    
    public void o(final int n) {
        if (((ViewGroup)this).getChildCount() > 0) {
            this.d.fling(((View)this).getScrollX(), ((View)this).getScrollY(), 0, n, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 0);
            this.F(true);
        }
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.i = false;
    }
    
    public boolean onGenericMotionEvent(final MotionEvent motionEvent) {
        final int action = motionEvent.getAction();
        final int n = 0;
        boolean b = false;
        if (action == 8 && !this.k) {
            float axisValue = 0.0f;
            Label_0062: {
                int n2;
                if (ax0.a(motionEvent, 2)) {
                    n2 = 9;
                }
                else {
                    if (!ax0.a(motionEvent, 4194304)) {
                        axisValue = 0.0f;
                        break Label_0062;
                    }
                    n2 = 26;
                }
                axisValue = motionEvent.getAxisValue(n2);
            }
            if (axisValue != 0.0f) {
                final int n3 = (int)(axisValue * this.getVerticalScrollFactorCompat());
                final int scrollRange = this.getScrollRange();
                final int scrollY = ((View)this).getScrollY();
                final int n4 = scrollY - n3;
                int n5;
                if (n4 < 0) {
                    if (this.c() && !ax0.a(motionEvent, 8194)) {
                        yv.d(this.e, -(float)n4 / ((View)this).getHeight(), 0.5f);
                        this.e.onRelease();
                        ((View)this).invalidate();
                        b = true;
                        n5 = n;
                    }
                    else {
                        b = false;
                        n5 = n;
                    }
                }
                else if (n4 > scrollRange) {
                    if (this.c() && !ax0.a(motionEvent, 8194)) {
                        yv.d(this.f, (n4 - scrollRange) / (float)((View)this).getHeight(), 0.5f);
                        this.f.onRelease();
                        ((View)this).invalidate();
                        b = true;
                    }
                    n5 = scrollRange;
                }
                else {
                    b = false;
                    n5 = n4;
                }
                if (n5 != scrollY) {
                    super.scrollTo(((View)this).getScrollX(), n5);
                    return true;
                }
                return b;
            }
        }
        return false;
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final int action = motionEvent.getAction();
        final boolean b = true;
        final boolean b2 = true;
        if (action == 2 && this.k) {
            return true;
        }
        final int n = action & 0xFF;
        if (n != 0) {
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 6) {
                            return this.k;
                        }
                        this.A(motionEvent);
                        return this.k;
                    }
                }
                else {
                    final int v = this.v;
                    if (v == -1) {
                        return this.k;
                    }
                    final int pointerIndex = motionEvent.findPointerIndex(v);
                    if (pointerIndex == -1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid pointerId=");
                        sb.append(v);
                        sb.append(" in onInterceptTouchEvent");
                        Log.e("NestedScrollView", sb.toString());
                        return this.k;
                    }
                    final int g = (int)motionEvent.getY(pointerIndex);
                    if (Math.abs(g - this.g) <= this.p || (0x2 & this.getNestedScrollAxes()) != 0x0) {
                        return this.k;
                    }
                    this.k = true;
                    this.g = g;
                    this.v();
                    this.l.addMovement(motionEvent);
                    this.y = 0;
                    final ViewParent parent = ((View)this).getParent();
                    if (parent != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                        return this.k;
                    }
                    return this.k;
                }
            }
            this.k = false;
            this.v = -1;
            this.D();
            if (this.d.springBack(((View)this).getScrollX(), ((View)this).getScrollY(), 0, 0, 0, this.getScrollRange())) {
                o32.h0((View)this);
            }
            this.Q(0);
        }
        else {
            final int g2 = (int)motionEvent.getY();
            if (!this.s((int)motionEvent.getX(), g2)) {
                boolean k = b2;
                if (!this.P(motionEvent)) {
                    k = (!this.d.isFinished() && b2);
                }
                this.k = k;
                this.D();
            }
            else {
                this.g = g2;
                this.v = motionEvent.getPointerId(0);
                this.t();
                this.l.addMovement(motionEvent);
                this.d.computeScrollOffset();
                boolean i = b;
                if (!this.P(motionEvent)) {
                    i = (!this.d.isFinished() && b);
                }
                this.k = i;
                this.O(2, 0);
            }
        }
        return this.k;
    }
    
    public void onLayout(final boolean b, int e, final int n, int scrollY, final int n2) {
        super.onLayout(b, e, n, scrollY, n2);
        e = 0;
        this.h = false;
        final View j = this.j;
        if (j != null && x(j, (View)this)) {
            this.H(this.j);
        }
        this.j = null;
        if (!this.i) {
            if (this.A != null) {
                this.scrollTo(((View)this).getScrollX(), this.A.a);
                this.A = null;
            }
            if (((ViewGroup)this).getChildCount() > 0) {
                final View child = ((ViewGroup)this).getChildAt(0);
                final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
                e = child.getMeasuredHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin;
            }
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            scrollY = ((View)this).getScrollY();
            e = e(scrollY, n2 - n - paddingTop - paddingBottom, e);
            if (e != scrollY) {
                this.scrollTo(((View)this).getScrollX(), e);
            }
        }
        this.scrollTo(((View)this).getScrollX(), ((View)this).getScrollY());
        this.i = true;
    }
    
    public void onMeasure(final int n, int measuredHeight) {
        super.onMeasure(n, measuredHeight);
        if (!this.m) {
            return;
        }
        if (View$MeasureSpec.getMode(measuredHeight) == 0) {
            return;
        }
        if (((ViewGroup)this).getChildCount() > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            measuredHeight = child.getMeasuredHeight();
            final int n2 = ((View)this).getMeasuredHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom() - frameLayout$LayoutParams.topMargin - frameLayout$LayoutParams.bottomMargin;
            if (measuredHeight < n2) {
                child.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight() + frameLayout$LayoutParams.leftMargin + frameLayout$LayoutParams.rightMargin, frameLayout$LayoutParams.width), View$MeasureSpec.makeMeasureSpec(n2, 1073741824));
            }
        }
    }
    
    public boolean onNestedFling(final View view, final float n, final float n2, final boolean b) {
        if (!b) {
            this.dispatchNestedFling(0.0f, n2, true);
            this.o((int)n2);
            return true;
        }
        return false;
    }
    
    public boolean onNestedPreFling(final View view, final float n, final float n2) {
        return this.dispatchNestedPreFling(n, n2);
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array) {
        this.onNestedPreScroll(view, n, n2, array, 0);
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int n3) {
        this.h(n, n2, array, null, n3);
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4) {
        this.z(n4, 0, null);
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        this.z(n4, n5, null);
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int[] array) {
        this.z(n4, n5, array);
    }
    
    public void onNestedScrollAccepted(final View view, final View view2, final int n) {
        this.onNestedScrollAccepted(view, view2, n, 0);
    }
    
    public void onNestedScrollAccepted(final View view, final View view2, final int n, final int n2) {
        this.C.c(view, view2, n, n2);
        this.O(2, n2);
    }
    
    public void onOverScrolled(final int n, final int n2, final boolean b, final boolean b2) {
        super.scrollTo(n, n2);
    }
    
    public boolean onRequestFocusInDescendants(final int n, final Rect rect) {
        int n2;
        if (n == 2) {
            n2 = 130;
        }
        else if ((n2 = n) == 1) {
            n2 = 33;
        }
        final FocusFinder instance = FocusFinder.getInstance();
        View view;
        if (rect == null) {
            view = instance.findNextFocus((ViewGroup)this, (View)null, n2);
        }
        else {
            view = instance.findNextFocusFromRect((ViewGroup)this, rect, n2);
        }
        return view != null && !this.w(view) && view.requestFocus(n2, rect);
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof d)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final d a = (d)parcelable;
        super.onRestoreInstanceState(((AbsSavedState)a).getSuperState());
        this.A = a;
        this.requestLayout();
    }
    
    public Parcelable onSaveInstanceState() {
        final d d = new d(super.onSaveInstanceState());
        d.a = ((View)this).getScrollY();
        return (Parcelable)d;
    }
    
    public void onScrollChanged(final int n, final int n2, final int n3, final int n4) {
        super.onScrollChanged(n, n2, n3, n4);
        final c g = this.G;
        if (g != null) {
            g.a(this, n, n2, n3, n4);
        }
    }
    
    public void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        final View focus = ((View)this).findFocus();
        if (focus != null) {
            if (this != focus) {
                if (this.y(focus, 0, n4)) {
                    focus.getDrawingRect(this.c);
                    ((ViewGroup)this).offsetDescendantRectToMyCoords(focus, this.c);
                    this.j(this.f(this.c));
                }
            }
        }
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n) {
        return this.onStartNestedScroll(view, view2, n, 0);
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n, final int n2) {
        return (n & 0x2) != 0x0;
    }
    
    public void onStopNestedScroll(final View view) {
        this.onStopNestedScroll(view, 0);
    }
    
    public void onStopNestedScroll(final View view, final int n) {
        this.C.e(view, n);
        this.Q(n);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        this.v();
        final int actionMasked = motionEvent.getActionMasked();
        final int n = 0;
        if (actionMasked == 0) {
            this.y = 0;
        }
        final MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.offsetLocation(0.0f, (float)this.y);
        Label_0923: {
            if (actionMasked != 0) {
                Label_0166: {
                    if (actionMasked != 1) {
                        if (actionMasked != 2) {
                            if (actionMasked != 3) {
                                if (actionMasked == 5) {
                                    final int actionIndex = motionEvent.getActionIndex();
                                    this.g = (int)motionEvent.getY(actionIndex);
                                    this.v = motionEvent.getPointerId(actionIndex);
                                    break Label_0923;
                                }
                                if (actionMasked != 6) {
                                    break Label_0923;
                                }
                                this.A(motionEvent);
                                this.g = (int)motionEvent.getY(motionEvent.findPointerIndex(this.v));
                                break Label_0923;
                            }
                            else if (!this.k || ((ViewGroup)this).getChildCount() <= 0 || !this.d.springBack(((View)this).getScrollX(), ((View)this).getScrollY(), 0, 0, 0, this.getScrollRange())) {
                                break Label_0166;
                            }
                        }
                        else {
                            final int pointerIndex = motionEvent.findPointerIndex(this.v);
                            if (pointerIndex == -1) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Invalid pointerId=");
                                sb.append(this.v);
                                sb.append(" in onTouchEvent");
                                Log.e("NestedScrollView", sb.toString());
                                break Label_0923;
                            }
                            final int n2 = (int)motionEvent.getY(pointerIndex);
                            final int n3 = this.g - n2;
                            int n4;
                            final int a = n4 = n3 - this.E(n3, motionEvent.getX(pointerIndex));
                            if (!this.k) {
                                n4 = a;
                                if (Math.abs(a) > this.p) {
                                    final ViewParent parent = ((View)this).getParent();
                                    if (parent != null) {
                                        parent.requestDisallowInterceptTouchEvent(true);
                                    }
                                    this.k = true;
                                    final int p = this.p;
                                    if (a > 0) {
                                        n4 = a - p;
                                    }
                                    else {
                                        n4 = a + p;
                                    }
                                }
                            }
                            if (!this.k) {
                                break Label_0923;
                            }
                            int n5 = n4;
                            if (this.h(0, n4, this.x, this.w, 0)) {
                                n5 = n4 - this.x[1];
                                this.y += this.w[1];
                            }
                            this.g = n2 - this.w[1];
                            final int scrollY = ((View)this).getScrollY();
                            final int scrollRange = this.getScrollRange();
                            final int overScrollMode = ((View)this).getOverScrollMode();
                            final boolean b = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                            int n6;
                            if (this.B(0, n5, 0, ((View)this).getScrollY(), 0, scrollRange, 0, 0, true) && !this.r(0)) {
                                n6 = 1;
                            }
                            else {
                                n6 = 0;
                            }
                            final int n7 = ((View)this).getScrollY() - scrollY;
                            final int[] x = this.x;
                            this.i(x[1] = 0, n7, 0, n5 - n7, this.w, 0, x);
                            final int g = this.g;
                            final int n8 = this.w[1];
                            this.g = g - n8;
                            this.y += n8;
                            if (b) {
                                final int n9 = n5 - this.x[1];
                                final int n10 = scrollY + n9;
                                Label_0700: {
                                    EdgeEffect edgeEffect;
                                    if (n10 < 0) {
                                        yv.d(this.e, -n9 / (float)((View)this).getHeight(), motionEvent.getX(pointerIndex) / ((View)this).getWidth());
                                        if (this.f.isFinished()) {
                                            break Label_0700;
                                        }
                                        edgeEffect = this.f;
                                    }
                                    else {
                                        if (n10 <= scrollRange) {
                                            break Label_0700;
                                        }
                                        yv.d(this.f, n9 / (float)((View)this).getHeight(), 1.0f - motionEvent.getX(pointerIndex) / ((View)this).getWidth());
                                        if (this.e.isFinished()) {
                                            break Label_0700;
                                        }
                                        edgeEffect = this.e;
                                    }
                                    edgeEffect.onRelease();
                                }
                                if (!this.e.isFinished() || !this.f.isFinished()) {
                                    o32.h0((View)this);
                                    n6 = n;
                                }
                            }
                            if (n6 != 0) {
                                this.l.clear();
                            }
                            break Label_0923;
                        }
                    }
                    else {
                        final VelocityTracker l = this.l;
                        l.computeCurrentVelocity(1000, (float)this.t);
                        final int a2 = (int)l.getYVelocity(this.v);
                        if (Math.abs(a2) >= this.q) {
                            if (this.k(a2)) {
                                break Label_0166;
                            }
                            final int n11 = -a2;
                            final float n12 = (float)n11;
                            if (!this.dispatchNestedPreFling(0.0f, n12)) {
                                this.dispatchNestedFling(0.0f, n12, true);
                                this.o(n11);
                            }
                            break Label_0166;
                        }
                        else if (!this.d.springBack(((View)this).getScrollX(), ((View)this).getScrollY(), 0, 0, 0, this.getScrollRange())) {
                            break Label_0166;
                        }
                    }
                    o32.h0((View)this);
                }
                this.v = -1;
                this.l();
            }
            else {
                if (((ViewGroup)this).getChildCount() == 0) {
                    return false;
                }
                if (this.k) {
                    final ViewParent parent2 = ((View)this).getParent();
                    if (parent2 != null) {
                        parent2.requestDisallowInterceptTouchEvent(true);
                    }
                }
                if (!this.d.isFinished()) {
                    this.a();
                }
                this.g = (int)motionEvent.getY();
                this.v = motionEvent.getPointerId(0);
                this.O(2, 0);
            }
        }
        final VelocityTracker i = this.l;
        if (i != null) {
            i.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }
    
    public boolean p(final int n) {
        final boolean b = n == 130;
        final int height = ((View)this).getHeight();
        final Rect c = this.c;
        c.top = 0;
        c.bottom = height;
        if (b) {
            final int childCount = ((ViewGroup)this).getChildCount();
            if (childCount > 0) {
                final View child = ((ViewGroup)this).getChildAt(childCount - 1);
                this.c.bottom = child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin + ((View)this).getPaddingBottom();
                final Rect c2 = this.c;
                c2.top = c2.bottom - height;
            }
        }
        final Rect c3 = this.c;
        return this.G(n, c3.top, c3.bottom);
    }
    
    public final float q(final int a) {
        final double log = Math.log(Math.abs(a) * 0.35f / (this.a * 0.015f));
        final float h = NestedScrollView.H;
        return (float)(this.a * 0.015f * Math.exp(h / (h - 1.0) * log));
    }
    
    public boolean r(final int n) {
        return this.D.l(n);
    }
    
    public void requestChildFocus(final View view, final View j) {
        if (!this.h) {
            this.H(j);
        }
        else {
            this.j = j;
        }
        super.requestChildFocus(view, j);
    }
    
    public boolean requestChildRectangleOnScreen(final View view, final Rect rect, final boolean b) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return this.I(rect, b);
    }
    
    public void requestDisallowInterceptTouchEvent(final boolean b) {
        if (b) {
            this.D();
        }
        super.requestDisallowInterceptTouchEvent(b);
    }
    
    public void requestLayout() {
        this.h = true;
        super.requestLayout();
    }
    
    public final boolean s(final int n, final int n2) {
        final int childCount = ((ViewGroup)this).getChildCount();
        boolean b2;
        final boolean b = b2 = false;
        if (childCount > 0) {
            final int scrollY = ((View)this).getScrollY();
            final View child = ((ViewGroup)this).getChildAt(0);
            b2 = b;
            if (n2 >= child.getTop() - scrollY) {
                b2 = b;
                if (n2 < child.getBottom() - scrollY) {
                    b2 = b;
                    if (n >= child.getLeft()) {
                        b2 = b;
                        if (n < child.getRight()) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public void scrollTo(int e, int e2) {
        if (((ViewGroup)this).getChildCount() > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            final int width = ((View)this).getWidth();
            final int paddingLeft = ((View)this).getPaddingLeft();
            final int paddingRight = ((View)this).getPaddingRight();
            final int width2 = child.getWidth();
            final int leftMargin = frameLayout$LayoutParams.leftMargin;
            final int rightMargin = frameLayout$LayoutParams.rightMargin;
            final int height = ((View)this).getHeight();
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            final int height2 = child.getHeight();
            final int topMargin = frameLayout$LayoutParams.topMargin;
            final int bottomMargin = frameLayout$LayoutParams.bottomMargin;
            e = e(e, width - paddingLeft - paddingRight, width2 + leftMargin + rightMargin);
            e2 = e(e2, height - paddingTop - paddingBottom, height2 + topMargin + bottomMargin);
            if (e != ((View)this).getScrollX() || e2 != ((View)this).getScrollY()) {
                super.scrollTo(e, e2);
            }
        }
    }
    
    public void setFillViewport(final boolean m) {
        if (m != this.m) {
            this.m = m;
            this.requestLayout();
        }
    }
    
    public void setNestedScrollingEnabled(final boolean b) {
        this.D.n(b);
    }
    
    public void setOnScrollChangeListener(final c g) {
        this.G = g;
    }
    
    public void setSmoothScrollingEnabled(final boolean n) {
        this.n = n;
    }
    
    public boolean shouldDelayChildPressedState() {
        return true;
    }
    
    public boolean startNestedScroll(final int n) {
        return this.O(n, 0);
    }
    
    public void stopNestedScroll() {
        this.Q(0);
    }
    
    public final void t() {
        final VelocityTracker l = this.l;
        if (l == null) {
            this.l = VelocityTracker.obtain();
        }
        else {
            l.clear();
        }
    }
    
    public final void u() {
        this.d = new OverScroller(((View)this).getContext());
        ((View)this).setFocusable(true);
        ((ViewGroup)this).setDescendantFocusability(262144);
        ((View)this).setWillNotDraw(false);
        final ViewConfiguration value = ViewConfiguration.get(((View)this).getContext());
        this.p = value.getScaledTouchSlop();
        this.q = value.getScaledMinimumFlingVelocity();
        this.t = value.getScaledMaximumFlingVelocity();
    }
    
    public final void v() {
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
    }
    
    public final boolean w(final View view) {
        return this.y(view, 0, ((View)this).getHeight()) ^ true;
    }
    
    public final boolean y(final View view, final int n, final int n2) {
        view.getDrawingRect(this.c);
        ((ViewGroup)this).offsetDescendantRectToMyCoords(view, this.c);
        return this.c.bottom + n >= ((View)this).getScrollY() && this.c.top - n <= ((View)this).getScrollY() + n2;
    }
    
    public final void z(final int n, final int n2, final int[] array) {
        final int scrollY = ((View)this).getScrollY();
        ((View)this).scrollBy(0, n);
        final int n3 = ((View)this).getScrollY() - scrollY;
        if (array != null) {
            array[1] += n3;
        }
        this.D.e(0, n3, 0, n - n3, null, n2, array);
    }
    
    public static class a extends p0
    {
        @Override
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            final NestedScrollView nestedScrollView = (NestedScrollView)view;
            ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)ScrollView.class.getName());
            ((AccessibilityRecord)accessibilityEvent).setScrollable(nestedScrollView.getScrollRange() > 0);
            ((AccessibilityRecord)accessibilityEvent).setScrollX(((View)nestedScrollView).getScrollX());
            ((AccessibilityRecord)accessibilityEvent).setScrollY(((View)nestedScrollView).getScrollY());
            p1.a((AccessibilityRecord)accessibilityEvent, ((View)nestedScrollView).getScrollX());
            p1.b((AccessibilityRecord)accessibilityEvent, nestedScrollView.getScrollRange());
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(final View view, final n1 n1) {
            super.onInitializeAccessibilityNodeInfo(view, n1);
            final NestedScrollView nestedScrollView = (NestedScrollView)view;
            n1.Y(ScrollView.class.getName());
            if (((View)nestedScrollView).isEnabled()) {
                final int scrollRange = nestedScrollView.getScrollRange();
                if (scrollRange > 0) {
                    n1.t0(true);
                    if (((View)nestedScrollView).getScrollY() > 0) {
                        n1.b(n1.a.r);
                        n1.b(n1.a.C);
                    }
                    if (((View)nestedScrollView).getScrollY() < scrollRange) {
                        n1.b(n1.a.q);
                        n1.b(n1.a.E);
                    }
                }
            }
        }
        
        @Override
        public boolean performAccessibilityAction(final View view, int n, final Bundle bundle) {
            if (super.performAccessibilityAction(view, n, bundle)) {
                return true;
            }
            final NestedScrollView nestedScrollView = (NestedScrollView)view;
            if (!((View)nestedScrollView).isEnabled()) {
                return false;
            }
            final int height = ((View)nestedScrollView).getHeight();
            final Rect rect = new Rect();
            int height2 = height;
            if (((View)nestedScrollView).getMatrix().isIdentity()) {
                height2 = height;
                if (((View)nestedScrollView).getGlobalVisibleRect(rect)) {
                    height2 = rect.height();
                }
            }
            if (n != 4096) {
                if (n != 8192 && n != 16908344) {
                    if (n != 16908346) {
                        return false;
                    }
                }
                else {
                    final int paddingBottom = ((View)nestedScrollView).getPaddingBottom();
                    n = ((View)nestedScrollView).getPaddingTop();
                    n = Math.max(((View)nestedScrollView).getScrollY() - (height2 - paddingBottom - n), 0);
                    if (n != ((View)nestedScrollView).getScrollY()) {
                        nestedScrollView.N(0, n, true);
                        return true;
                    }
                    return false;
                }
            }
            n = ((View)nestedScrollView).getPaddingBottom();
            n = Math.min(((View)nestedScrollView).getScrollY() + (height2 - n - ((View)nestedScrollView).getPaddingTop()), nestedScrollView.getScrollRange());
            if (n != ((View)nestedScrollView).getScrollY()) {
                nestedScrollView.N(0, n, true);
                return true;
            }
            return false;
        }
    }
    
    public abstract static class b
    {
        public static boolean a(final ViewGroup viewGroup) {
            return viewGroup.getClipToPadding();
        }
    }
    
    public interface c
    {
        void a(final NestedScrollView p0, final int p1, final int p2, final int p3, final int p4);
    }
    
    public static class d extends View$BaseSavedState
    {
        public static final Parcelable$Creator<d> CREATOR;
        public int a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public d a(final Parcel parcel) {
                    return new d(parcel);
                }
                
                public d[] b(final int n) {
                    return new d[n];
                }
            };
        }
        
        public d(final Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
        }
        
        public d(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("HorizontalScrollView.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" scrollPosition=");
            sb.append(this.a);
            sb.append("}");
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.a);
        }
    }
}

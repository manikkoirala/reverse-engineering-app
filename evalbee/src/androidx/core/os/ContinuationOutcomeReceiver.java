// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.Result$a;
import kotlin.Result;
import org.jetbrains.annotations.NotNull;
import android.os.OutcomeReceiver;
import java.util.concurrent.atomic.AtomicBoolean;

final class ContinuationOutcomeReceiver<R, E extends Throwable> extends AtomicBoolean implements OutcomeReceiver
{
    @NotNull
    private final vl continuation;
    
    public ContinuationOutcomeReceiver(@NotNull final vl continuation) {
        fg0.e((Object)continuation, "continuation");
        super(false);
        this.continuation = continuation;
    }
    
    public void onError(@NotNull final E e) {
        fg0.e((Object)e, "error");
        if (this.compareAndSet(false, true)) {
            final vl continuation = this.continuation;
            final Result$a companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(xe1.a((Throwable)e)));
        }
    }
    
    public void onResult(final R r) {
        if (this.compareAndSet(false, true)) {
            this.continuation.resumeWith(Result.constructor-impl((Object)r));
        }
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationOutcomeReceiver(outcomeReceived = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

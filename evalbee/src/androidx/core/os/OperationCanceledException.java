// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

public class OperationCanceledException extends RuntimeException
{
    public OperationCanceledException() {
        this((String)null);
    }
    
    public OperationCanceledException(final String s) {
        super(c11.e(s, "The operation has been canceled."));
    }
}

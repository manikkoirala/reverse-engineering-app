// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import android.content.pm.PackageItemInfo;
import android.os.BaseBundle;
import android.text.TextUtils;
import java.util.Iterator;
import android.net.Uri$Builder;
import android.database.MatrixCursor;
import android.database.Cursor;
import android.os.ParcelFileDescriptor;
import android.content.ContentValues;
import android.webkit.MimeTypeMap;
import android.os.Environment;
import android.net.Uri;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.os.Bundle;
import android.content.res.XmlResourceParser;
import android.content.pm.ProviderInfo;
import android.content.Context;
import java.util.HashMap;
import java.io.File;
import android.content.ContentProvider;

public class FileProvider extends ContentProvider
{
    public static final String[] c;
    public static final File d;
    public static final HashMap e;
    public b a;
    public int b;
    
    static {
        c = new String[] { "_display_name", "_size" };
        d = new File("/");
        e = new HashMap();
    }
    
    public FileProvider() {
        this.b = 0;
    }
    
    public static File a(final File file, final String... array) {
        final int length = array.length;
        int i = 0;
        File parent = file;
        while (i < length) {
            final String child = array[i];
            File file2 = parent;
            if (child != null) {
                file2 = new File(parent, child);
            }
            ++i;
            parent = file2;
        }
        return parent;
    }
    
    public static Object[] b(final Object[] array, final int n) {
        final Object[] array2 = new Object[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    public static String[] c(final String[] array, final int n) {
        final String[] array2 = new String[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    public static XmlResourceParser d(final Context context, final String str, final ProviderInfo providerInfo, final int n) {
        if (providerInfo == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Couldn't find meta-data for provider with authority ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
        if (providerInfo.metaData == null && n != 0) {
            ((BaseBundle)(providerInfo.metaData = new Bundle(1))).putInt("android.support.FILE_PROVIDER_PATHS", n);
        }
        final XmlResourceParser loadXmlMetaData = ((PackageItemInfo)providerInfo).loadXmlMetaData(context.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
        if (loadXmlMetaData != null) {
            return loadXmlMetaData;
        }
        throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
    }
    
    public static b e(final Context context, final String s, final int n) {
        final HashMap e = FileProvider.e;
        synchronized (e) {
            b h;
            if ((h = e.get(s)) == null) {
                try {
                    h = h(context, s, n);
                    e.put(s, h);
                }
                catch (final XmlPullParserException cause) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", (Throwable)cause);
                }
                catch (final IOException cause2) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", cause2);
                }
            }
            return h;
        }
    }
    
    public static Uri f(final Context context, final String s, final File file) {
        return e(context, s, 0).a(file);
    }
    
    public static int g(final String str) {
        int n;
        if ("r".equals(str)) {
            n = 268435456;
        }
        else if (!"w".equals(str) && !"wt".equals(str)) {
            if ("wa".equals(str)) {
                n = 704643072;
            }
            else if ("rw".equals(str)) {
                n = 939524096;
            }
            else {
                if (!"rwt".equals(str)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid mode: ");
                    sb.append(str);
                    throw new IllegalArgumentException(sb.toString());
                }
                n = 1006632960;
            }
        }
        else {
            n = 738197504;
        }
        return n;
    }
    
    public static b h(final Context context, final String s, int next) {
        final c c = new c(s);
        final XmlResourceParser d = d(context, s, context.getPackageManager().resolveContentProvider(s, 128), next);
        while (true) {
            next = ((XmlPullParser)d).next();
            if (next == 1) {
                break;
            }
            if (next != 2) {
                continue;
            }
            final String name = ((XmlPullParser)d).getName();
            final File file = null;
            final String attributeValue = ((XmlPullParser)d).getAttributeValue((String)null, "name");
            final String attributeValue2 = ((XmlPullParser)d).getAttributeValue((String)null, "path");
            File file2;
            if ("root-path".equals(name)) {
                file2 = FileProvider.d;
            }
            else if ("files-path".equals(name)) {
                file2 = context.getFilesDir();
            }
            else if ("cache-path".equals(name)) {
                file2 = context.getCacheDir();
            }
            else if ("external-path".equals(name)) {
                file2 = Environment.getExternalStorageDirectory();
            }
            else if ("external-files-path".equals(name)) {
                final File[] externalFilesDirs = sl.getExternalFilesDirs(context, null);
                file2 = file;
                if (externalFilesDirs.length > 0) {
                    file2 = externalFilesDirs[0];
                }
            }
            else if ("external-cache-path".equals(name)) {
                final File[] externalCacheDirs = sl.getExternalCacheDirs(context);
                file2 = file;
                if (externalCacheDirs.length > 0) {
                    file2 = externalCacheDirs[0];
                }
            }
            else {
                file2 = file;
                if ("external-media-path".equals(name)) {
                    final File[] a = FileProvider.a.a(context);
                    file2 = file;
                    if (a.length > 0) {
                        file2 = a[0];
                    }
                }
            }
            if (file2 == null) {
                continue;
            }
            c.c(attributeValue, a(file2, attributeValue2));
        }
        return (b)c;
    }
    
    public void attachInfo(final Context context, ProviderInfo e) {
        super.attachInfo(context, e);
        if (!e.exported) {
            if (e.grantUriPermissions) {
                final String key = e.authority.split(";")[0];
                e = (ProviderInfo)FileProvider.e;
                synchronized (e) {
                    ((HashMap<Object, Object>)e).remove(key);
                    monitorexit(e);
                    this.a = e(context, key, this.b);
                    return;
                }
            }
            throw new SecurityException("Provider must grant uri permissions");
        }
        throw new SecurityException("Provider must not be exported");
    }
    
    public int delete(final Uri uri, final String s, final String[] array) {
        return this.a.b(uri).delete() ? 1 : 0;
    }
    
    public String getType(final Uri uri) {
        final File b = this.a.b(uri);
        final int lastIndex = b.getName().lastIndexOf(46);
        if (lastIndex >= 0) {
            final String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(b.getName().substring(lastIndex + 1));
            if (mimeTypeFromExtension != null) {
                return mimeTypeFromExtension;
            }
        }
        return "application/octet-stream";
    }
    
    public Uri insert(final Uri uri, final ContentValues contentValues) {
        throw new UnsupportedOperationException("No external inserts");
    }
    
    public boolean onCreate() {
        return true;
    }
    
    public ParcelFileDescriptor openFile(final Uri uri, final String s) {
        return ParcelFileDescriptor.open(this.a.b(uri), g(s));
    }
    
    public Cursor query(final Uri uri, String[] c, String queryParameter, final String[] array, final String s) {
        final File b = this.a.b(uri);
        queryParameter = uri.getQueryParameter("displayName");
        String[] c2 = c;
        if (c == null) {
            c2 = FileProvider.c;
        }
        final String[] array2 = new String[c2.length];
        final Object[] array3 = new Object[c2.length];
        final int length = c2.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final String s2 = c2[i];
            int n4 = 0;
            Label_0163: {
                int n3;
                if ("_display_name".equals(s2)) {
                    array2[n] = "_display_name";
                    final int n2 = n + 1;
                    String name;
                    if (queryParameter == null) {
                        name = b.getName();
                    }
                    else {
                        name = queryParameter;
                    }
                    array3[n] = name;
                    n3 = n2;
                }
                else {
                    n4 = n;
                    if (!"_size".equals(s2)) {
                        break Label_0163;
                    }
                    array2[n] = "_size";
                    final int n5 = n + 1;
                    array3[n] = b.length();
                    n3 = n5;
                }
                n4 = n3;
            }
            ++i;
            n = n4;
        }
        c = c(array2, n);
        final Object[] b2 = b(array3, n);
        final MatrixCursor matrixCursor = new MatrixCursor(c, 1);
        matrixCursor.addRow(b2);
        return (Cursor)matrixCursor;
    }
    
    public int update(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        throw new UnsupportedOperationException("No external updates");
    }
    
    public abstract static class a
    {
        public static File[] a(final Context context) {
            return context.getExternalMediaDirs();
        }
    }
    
    public interface b
    {
        Uri a(final File p0);
        
        File b(final Uri p0);
    }
    
    public static class c implements b
    {
        public final String a;
        public final HashMap b;
        
        public c(final String a) {
            this.b = new HashMap();
            this.a = a;
        }
        
        @Override
        public Uri a(File string) {
            try {
                final String canonicalPath = string.getCanonicalPath();
                final Iterator iterator = this.b.entrySet().iterator();
                string = null;
                while (iterator.hasNext()) {
                    final Object o = iterator.next();
                    final String path = ((Map.Entry<K, File>)o).getValue().getPath();
                    if (canonicalPath.startsWith(path) && (string == null || path.length() > ((Map.Entry<K, File>)string).getValue().getPath().length())) {
                        string = (File)o;
                    }
                }
                if (string != null) {
                    final String path2 = ((Map.Entry<K, File>)string).getValue().getPath();
                    final boolean endsWith = path2.endsWith("/");
                    int length = path2.length();
                    if (!endsWith) {
                        ++length;
                    }
                    final String substring = canonicalPath.substring(length);
                    final StringBuilder sb = new StringBuilder();
                    sb.append(Uri.encode((String)((Map.Entry<String, V>)string).getKey()));
                    sb.append('/');
                    sb.append(Uri.encode(substring, "/"));
                    string = (File)sb.toString();
                    return new Uri$Builder().scheme("content").authority(this.a).encodedPath((String)string).build();
                }
                string = (File)new StringBuilder();
                ((StringBuilder)string).append("Failed to find configured root that contains ");
                ((StringBuilder)string).append(canonicalPath);
                throw new IllegalArgumentException(((StringBuilder)string).toString());
            }
            catch (final IOException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to resolve canonical path for ");
                sb2.append(string);
                throw new IllegalArgumentException(sb2.toString());
            }
        }
        
        @Override
        public File b(Uri uri) {
            final String encodedPath = uri.getEncodedPath();
            final int index = encodedPath.indexOf(47, 1);
            final String decode = Uri.decode(encodedPath.substring(1, index));
            final String decode2 = Uri.decode(encodedPath.substring(index + 1));
            final File parent = this.b.get(decode);
            if (parent != null) {
                uri = (Uri)new File(parent, decode2);
                try {
                    final File canonicalFile = ((File)uri).getCanonicalFile();
                    if (canonicalFile.getPath().startsWith(parent.getPath())) {
                        return canonicalFile;
                    }
                    throw new SecurityException("Resolved path jumped beyond configured root");
                }
                catch (final IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to resolve canonical path for ");
                    sb.append(uri);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to find configured root for ");
            sb2.append(uri);
            throw new IllegalArgumentException(sb2.toString());
        }
        
        public void c(final String key, final File obj) {
            if (!TextUtils.isEmpty((CharSequence)key)) {
                try {
                    this.b.put(key, obj.getCanonicalFile());
                    return;
                }
                catch (final IOException cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to resolve canonical path for ");
                    sb.append(obj);
                    throw new IllegalArgumentException(sb.toString(), cause);
                }
            }
            throw new IllegalArgumentException("Name must not be empty");
        }
    }
}

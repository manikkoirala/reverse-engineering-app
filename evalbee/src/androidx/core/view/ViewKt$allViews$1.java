// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.ViewGroup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import android.view.View;
import kotlin.coroutines.jvm.internal.RestrictedSuspendLambda;

@xp(c = "androidx.core.view.ViewKt$allViews$1", f = "View.kt", l = { 414, 416 }, m = "invokeSuspend")
final class ViewKt$allViews$1 extends RestrictedSuspendLambda implements q90
{
    final View $this_allViews;
    private Object L$0;
    int label;
    
    public ViewKt$allViews$1(final View $this_allViews, final vl vl) {
        this.$this_allViews = $this_allViews;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
        final ViewKt$allViews$1 viewKt$allViews$1 = new ViewKt$allViews$1(this.$this_allViews, vl);
        viewKt$allViews$1.L$0 = l$0;
        return (vl)viewKt$allViews$1;
    }
    
    @Nullable
    public final Object invoke(@NotNull final el1 el1, @Nullable final vl vl) {
        return ((ViewKt$allViews$1)this.create(el1, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        final Object d = gg0.d();
        final int label = this.label;
        el1 el2;
        if (label != 0) {
            if (label != 1) {
                if (label == 2) {
                    xe1.b(o);
                    return u02.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            else {
                final el1 el1 = (el1)this.L$0;
                xe1.b(o);
                el2 = el1;
            }
        }
        else {
            xe1.b(o);
            final el1 l$0 = (el1)this.L$0;
            final View $this_allViews = this.$this_allViews;
            this.L$0 = l$0;
            this.label = 1;
            el2 = l$0;
            if (l$0.b((Object)$this_allViews, (vl)this) == d) {
                return d;
            }
        }
        final View $this_allViews2 = this.$this_allViews;
        if ($this_allViews2 instanceof ViewGroup) {
            final cl1 a = ViewGroupKt.a((ViewGroup)$this_allViews2);
            this.L$0 = null;
            this.label = 2;
            if (el2.c(a, (vl)this) == d) {
                return d;
            }
        }
        return u02.a;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.transition;

import org.jetbrains.annotations.NotNull;
import android.transition.Transition;
import kotlin.jvm.internal.Lambda;

public final class TransitionKt$addListener$1 extends Lambda implements c90
{
    public static final TransitionKt$addListener$1 INSTANCE;
    
    static {
        INSTANCE = new TransitionKt$addListener$1();
    }
    
    public TransitionKt$addListener$1() {
        super(1);
    }
    
    public final void invoke(@NotNull final Transition transition) {
        fg0.e((Object)transition, "it");
    }
}

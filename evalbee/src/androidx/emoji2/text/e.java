// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.content.pm.PackageManager$NameNotFoundException;
import java.nio.ByteBuffer;
import java.util.concurrent.ThreadPoolExecutor;
import android.os.Handler;
import android.database.ContentObserver;
import android.os.CancellationSignal;
import android.graphics.Typeface;
import java.util.concurrent.Executor;
import android.content.Context;

public class e extends c
{
    public static final a j;
    
    static {
        j = new a();
    }
    
    public e(final Context context, final g70 g70) {
        super(new b(context, g70, e.j));
    }
    
    public e c(final Executor executor) {
        ((b)((c)this).a()).f(executor);
        return this;
    }
    
    public static class a
    {
        public Typeface a(final Context context, final k70.b b) {
            return k70.a(context, null, new k70.b[] { b });
        }
        
        public k70.a b(final Context context, final g70 g70) {
            return k70.b(context, null, g70);
        }
        
        public void c(final Context context, final ContentObserver contentObserver) {
            context.getContentResolver().unregisterContentObserver(contentObserver);
        }
    }
    
    public static class b implements g
    {
        public final Context a;
        public final g70 b;
        public final a c;
        public final Object d;
        public Handler e;
        public Executor f;
        public ThreadPoolExecutor g;
        public h h;
        public ContentObserver i;
        public Runnable j;
        
        public b(final Context context, final g70 b, final a c) {
            this.d = new Object();
            l71.h(context, "Context cannot be null");
            l71.h(b, "FontRequest cannot be null");
            this.a = context.getApplicationContext();
            this.b = b;
            this.c = c;
        }
        
        @Override
        public void a(final h h) {
            l71.h(h, "LoaderCallback cannot be null");
            synchronized (this.d) {
                this.h = h;
                monitorexit(this.d);
                this.d();
            }
        }
        
        public final void b() {
            synchronized (this.d) {
                this.h = null;
                final ContentObserver i = this.i;
                if (i != null) {
                    this.c.c(this.a, i);
                    this.i = null;
                }
                final Handler e = this.e;
                if (e != null) {
                    e.removeCallbacks(this.j);
                }
                this.e = null;
                final ThreadPoolExecutor g = this.g;
                if (g != null) {
                    g.shutdown();
                }
                this.f = null;
                this.g = null;
            }
        }
        
        public void c() {
            Object o = this.d;
            synchronized (o) {
                if (this.h == null) {
                    return;
                }
                monitorexit(o);
                try {
                    final k70.b e = this.e();
                    final int b = e.b();
                    Label_0054: {
                        if (b == 2) {
                            o = this.d;
                            monitorenter(o);
                            while (true) {
                                try {
                                    monitorexit(o);
                                    break Label_0054;
                                    monitorexit(o);
                                    throw e;
                                }
                                finally {
                                    continue;
                                }
                                break;
                            }
                        }
                    }
                    if (b == 0) {
                        try {
                            jy1.a("EmojiCompat.FontRequestEmojiCompatConfig.buildTypeface");
                            final Typeface a = this.c.a(this.a, e);
                            final ByteBuffer f = zz1.f(this.a, null, e.d());
                            if (f != null && a != null) {
                                final androidx.emoji2.text.f b2 = androidx.emoji2.text.f.b(a, f);
                                jy1.b();
                                synchronized (this.d) {
                                    final h h = this.h;
                                    if (h != null) {
                                        h.b(b2);
                                    }
                                    monitorexit(this.d);
                                    this.b();
                                    return;
                                }
                            }
                            throw new RuntimeException("Unable to open file.");
                        }
                        finally {
                            jy1.b();
                        }
                    }
                    o = new(java.lang.RuntimeException.class)();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("fetchFonts result is not OK. (");
                    sb.append(b);
                    sb.append(")");
                    new RuntimeException(sb.toString());
                    throw o;
                }
                finally {
                    synchronized (this.d) {
                        final h h2 = this.h;
                        if (h2 != null) {
                            final Throwable t;
                            h2.a(t);
                        }
                        monitorexit(this.d);
                        this.b();
                    }
                }
            }
        }
        
        public void d() {
            synchronized (this.d) {
                if (this.h == null) {
                    return;
                }
                if (this.f == null) {
                    final ThreadPoolExecutor b = bk.b("emojiCompat");
                    this.g = b;
                    this.f = b;
                }
                this.f.execute(new h70(this));
            }
        }
        
        public final k70.b e() {
            try {
                final k70.a b = this.c.b(this.a, this.b);
                if (b.c() != 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("fetchFonts failed (");
                    sb.append(b.c());
                    sb.append(")");
                    throw new RuntimeException(sb.toString());
                }
                final k70.b[] b2 = b.b();
                if (b2 != null && b2.length != 0) {
                    return b2[0];
                }
                throw new RuntimeException("fetchFonts failed (empty result)");
            }
            catch (final PackageManager$NameNotFoundException cause) {
                throw new RuntimeException("provider not found", (Throwable)cause);
            }
        }
        
        public void f(final Executor f) {
            synchronized (this.d) {
                this.f = f;
            }
        }
    }
}

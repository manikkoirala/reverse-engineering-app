// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.os.BaseBundle;
import java.util.Arrays;
import java.util.List;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import java.util.ArrayList;
import android.view.KeyEvent;
import android.text.Editable;
import android.view.inputmethod.InputConnection;
import java.util.Collection;
import android.os.Looper;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import android.os.Handler;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;

public class c
{
    public static final Object n;
    public static final Object o;
    public static volatile c p;
    public final ReadWriteLock a;
    public final Set b;
    public volatile int c;
    public final Handler d;
    public final b e;
    public final g f;
    public final boolean g;
    public final boolean h;
    public final int[] i;
    public final boolean j;
    public final int k;
    public final int l;
    public final d m;
    
    static {
        n = new Object();
        o = new Object();
    }
    
    public c(final c c) {
        this.a = new ReentrantReadWriteLock();
        this.c = 3;
        this.g = c.b;
        this.h = c.c;
        this.i = c.d;
        this.j = c.f;
        this.k = c.g;
        this.f = c.a;
        this.l = c.h;
        this.m = c.i;
        this.d = new Handler(Looper.getMainLooper());
        final s8 b = new s8();
        this.b = b;
        final Set e = c.e;
        if (e != null && !e.isEmpty()) {
            b.addAll(c.e);
        }
        this.e = (b)new a(this);
        this.l();
    }
    
    public static /* synthetic */ d a(final c c) {
        return c.m;
    }
    
    public static c b() {
        synchronized (c.n) {
            final c p = c.p;
            l71.i(p != null, "EmojiCompat is not initialized.\n\nYou must initialize EmojiCompat prior to referencing the EmojiCompat instance.\n\nThe most likely cause of this error is disabling the EmojiCompatInitializer\neither explicitly in AndroidManifest.xml, or by including\nandroidx.emoji2:emoji2-bundled.\n\nAutomatic initialization is typically performed by EmojiCompatInitializer. If\nyou are not expecting to initialize EmojiCompat manually in your application,\nplease check to ensure it has not been removed from your APK's manifest. You can\ndo this in Android Studio using Build > Analyze APK.\n\nIn the APK Analyzer, ensure that the startup entry for\nEmojiCompatInitializer and InitializationProvider is present in\n AndroidManifest.xml. If it is missing or contains tools:node=\"remove\", and you\nintend to use automatic configuration, verify:\n\n  1. Your application does not include emoji2-bundled\n  2. All modules do not contain an exclusion manifest rule for\n     EmojiCompatInitializer or InitializationProvider. For more information\n     about manifest exclusions see the documentation for the androidx startup\n     library.\n\nIf you intend to use emoji2-bundled, please call EmojiCompat.init. You can\nlearn more in the documentation for BundledEmojiCompatConfig.\n\nIf you intended to perform manual configuration, it is recommended that you call\nEmojiCompat.init immediately on application startup.\n\nIf you still cannot resolve this issue, please open a bug with your specific\nconfiguration to help improve error message.");
            return p;
        }
    }
    
    public static boolean e(final InputConnection inputConnection, final Editable editable, final int n, final int n2, final boolean b) {
        return androidx.emoji2.text.d.c(inputConnection, editable, n, n2, b);
    }
    
    public static boolean f(final Editable editable, final int n, final KeyEvent keyEvent) {
        return androidx.emoji2.text.d.d(editable, n, keyEvent);
    }
    
    public static c g(final c c) {
        c c2;
        if ((c2 = c.p) == null) {
            synchronized (c.n) {
                if ((c2 = c.p) == null) {
                    c2 = (c.p = new c(c));
                }
            }
        }
        return c2;
    }
    
    public static boolean h() {
        return c.p != null;
    }
    
    public int c() {
        return this.k;
    }
    
    public int d() {
        this.a.readLock().lock();
        try {
            return this.c;
        }
        finally {
            this.a.readLock().unlock();
        }
    }
    
    public boolean i() {
        return this.j;
    }
    
    public final boolean j() {
        final int d = this.d();
        boolean b = true;
        if (d != 1) {
            b = false;
        }
        return b;
    }
    
    public void k() {
        final int l = this.l;
        boolean b = true;
        if (l != 1) {
            b = false;
        }
        l71.i(b, "Set metadataLoadStrategy to LOAD_STRATEGY_MANUAL to execute manual loading");
        if (this.j()) {
            return;
        }
        this.a.writeLock().lock();
        try {
            if (this.c == 0) {
                return;
            }
            this.c = 0;
            this.a.writeLock().unlock();
            this.e.a();
        }
        finally {
            this.a.writeLock().unlock();
        }
    }
    
    public final void l() {
        this.a.writeLock().lock();
        try {
            if (this.l == 0) {
                this.c = 0;
            }
            this.a.writeLock().unlock();
            if (this.d() == 0) {
                this.e.a();
            }
        }
        finally {
            this.a.writeLock().unlock();
        }
    }
    
    public void m(final Throwable t) {
        final ArrayList list = new ArrayList();
        this.a.writeLock().lock();
        try {
            this.c = 2;
            list.addAll(this.b);
            this.b.clear();
            this.a.writeLock().unlock();
            this.d.post((Runnable)new f(list, this.c, t));
        }
        finally {
            this.a.writeLock().unlock();
        }
    }
    
    public void n() {
        final ArrayList list = new ArrayList();
        this.a.writeLock().lock();
        try {
            this.c = 1;
            list.addAll(this.b);
            this.b.clear();
            this.a.writeLock().unlock();
            this.d.post((Runnable)new f(list, this.c));
        }
        finally {
            this.a.writeLock().unlock();
        }
    }
    
    public CharSequence o(final CharSequence charSequence) {
        int length;
        if (charSequence == null) {
            length = 0;
        }
        else {
            length = charSequence.length();
        }
        return this.p(charSequence, 0, length);
    }
    
    public CharSequence p(final CharSequence charSequence, final int n, final int n2) {
        return this.q(charSequence, n, n2, Integer.MAX_VALUE);
    }
    
    public CharSequence q(final CharSequence charSequence, final int n, final int n2, final int n3) {
        return this.r(charSequence, n, n2, n3, 0);
    }
    
    public CharSequence r(final CharSequence charSequence, final int n, final int n2, final int n3, final int n4) {
        l71.i(this.j(), "Not initialized yet");
        l71.e(n, "start cannot be negative");
        l71.e(n2, "end cannot be negative");
        l71.e(n3, "maxEmojiCount cannot be negative");
        final boolean b = false;
        l71.b(n <= n2, "start should be <= than end");
        if (charSequence == null) {
            return null;
        }
        l71.b(n <= charSequence.length(), "start should be < than charSequence length");
        l71.b(n2 <= charSequence.length(), "end should be < than charSequence length");
        CharSequence b2 = charSequence;
        if (charSequence.length() != 0) {
            if (n == n2) {
                b2 = charSequence;
            }
            else {
                boolean g;
                if (n4 != 1) {
                    g = b;
                    if (n4 != 2) {
                        g = this.g;
                    }
                }
                else {
                    g = true;
                }
                b2 = this.e.b(charSequence, n, n2, n3, g);
            }
        }
        return b2;
    }
    
    public void s(final e e) {
        l71.h(e, "initCallback cannot be null");
        this.a.writeLock().lock();
        try {
            if (this.c != 1 && this.c != 2) {
                this.b.add(e);
            }
            else {
                this.d.post((Runnable)new f(e, this.c));
            }
        }
        finally {
            this.a.writeLock().unlock();
        }
    }
    
    public void t(final e e) {
        l71.h(e, "initCallback cannot be null");
        this.a.writeLock().lock();
        try {
            this.b.remove(e);
        }
        finally {
            this.a.writeLock().unlock();
        }
    }
    
    public void u(final EditorInfo editorInfo) {
        if (this.j()) {
            if (editorInfo != null) {
                if (editorInfo.extras == null) {
                    editorInfo.extras = new Bundle();
                }
                this.e.c(editorInfo);
            }
        }
    }
    
    public static final class a extends b
    {
        public volatile androidx.emoji2.text.d b;
        public volatile androidx.emoji2.text.f c;
        
        public a(final c c) {
            super(c);
        }
        
        @Override
        public void a() {
            try {
                super.a.f.a(new h(this) {
                    public final a a;
                    
                    @Override
                    public void a(final Throwable t) {
                        this.a.a.m(t);
                    }
                    
                    @Override
                    public void b(final androidx.emoji2.text.f f) {
                        this.a.d(f);
                    }
                });
            }
            finally {
                final Throwable t;
                super.a.m(t);
            }
        }
        
        @Override
        public CharSequence b(final CharSequence charSequence, final int n, final int n2, final int n3, final boolean b) {
            return this.b.h(charSequence, n, n2, n3, b);
        }
        
        @Override
        public void c(final EditorInfo editorInfo) {
            ((BaseBundle)editorInfo.extras).putInt("android.support.text.emoji.emojiCompat_metadataVersion", this.c.e());
            ((BaseBundle)editorInfo.extras).putBoolean("android.support.text.emoji.emojiCompat_replaceAll", super.a.g);
        }
        
        public void d(final androidx.emoji2.text.f c) {
            if (c == null) {
                super.a.m(new IllegalArgumentException("metadataRepo cannot be null"));
                return;
            }
            this.c = c;
            final androidx.emoji2.text.f c2 = this.c;
            final i i = new i();
            final d a = c.a(super.a);
            final c a2 = super.a;
            this.b = new androidx.emoji2.text.d(c2, i, a, a2.h, a2.i);
            super.a.n();
        }
    }
    
    public abstract static class b
    {
        public final c a;
        
        public b(final c a) {
            this.a = a;
        }
        
        public abstract void a();
        
        public abstract CharSequence b(final CharSequence p0, final int p1, final int p2, final int p3, final boolean p4);
        
        public abstract void c(final EditorInfo p0);
    }
    
    public abstract static class c
    {
        public final g a;
        public boolean b;
        public boolean c;
        public int[] d;
        public Set e;
        public boolean f;
        public int g;
        public int h;
        public d i;
        
        public c(final g a) {
            this.g = -16711936;
            this.h = 0;
            this.i = new androidx.emoji2.text.b();
            l71.h(a, "metadataLoader cannot be null.");
            this.a = a;
        }
        
        public final g a() {
            return this.a;
        }
        
        public c b(final int h) {
            this.h = h;
            return this;
        }
    }
    
    public interface d
    {
        boolean a(final CharSequence p0, final int p1, final int p2, final int p3);
    }
    
    public abstract static class e
    {
        public void a(final Throwable t) {
        }
        
        public void b() {
        }
    }
    
    public static class f implements Runnable
    {
        public final List a;
        public final Throwable b;
        public final int c;
        
        public f(final e e, final int n) {
            this(Arrays.asList((e)l71.h(e, "initCallback cannot be null")), n, null);
        }
        
        public f(final Collection collection, final int n) {
            this(collection, n, null);
        }
        
        public f(final Collection c, final int c2, final Throwable b) {
            l71.h(c, "initCallbacks cannot be null");
            this.a = new ArrayList(c);
            this.c = c2;
            this.b = b;
        }
        
        @Override
        public void run() {
            final int size = this.a.size();
            final int c = this.c;
            int i = 0;
            final int n = 0;
            if (c != 1) {
                for (int j = n; j < size; ++j) {
                    ((e)this.a.get(j)).a(this.b);
                }
            }
            else {
                while (i < size) {
                    ((e)this.a.get(i)).b();
                    ++i;
                }
            }
        }
    }
    
    public interface g
    {
        void a(final h p0);
    }
    
    public abstract static class h
    {
        public abstract void a(final Throwable p0);
        
        public abstract void b(final androidx.emoji2.text.f p0);
    }
    
    public static class i
    {
        public ow a(final nw nw) {
            return new a02(nw);
        }
    }
}

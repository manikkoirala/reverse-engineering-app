// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.graphics.Paint;
import android.text.TextPaint;

public class b implements d
{
    public static final ThreadLocal b;
    public final TextPaint a;
    
    static {
        b = new ThreadLocal();
    }
    
    public b() {
        ((Paint)(this.a = new TextPaint())).setTextSize(10.0f);
    }
    
    public static StringBuilder b() {
        final ThreadLocal b = androidx.emoji2.text.b.b;
        if (b.get() == null) {
            b.set(new StringBuilder());
        }
        return (StringBuilder)b.get();
    }
    
    @Override
    public boolean a(final CharSequence charSequence, int i, final int n, final int n2) {
        final StringBuilder b = b();
        b.setLength(0);
        while (i < n) {
            b.append(charSequence.charAt(i));
            ++i;
        }
        return x21.a((Paint)this.a, b.toString());
    }
}

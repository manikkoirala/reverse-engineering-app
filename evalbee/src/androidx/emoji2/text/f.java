// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.util.SparseArray;
import java.nio.ByteBuffer;
import android.graphics.Typeface;

public final class f
{
    public final ew0 a;
    public final char[] b;
    public final a c;
    public final Typeface d;
    
    public f(final Typeface d, final ew0 a) {
        this.d = d;
        this.a = a;
        this.c = new a(1024);
        this.b = new char[a.k() * 2];
        this.a(a);
    }
    
    public static f b(final Typeface typeface, final ByteBuffer byteBuffer) {
        try {
            jy1.a("EmojiCompat.MetadataRepo.create");
            return new f(typeface, fw0.b(byteBuffer));
        }
        finally {
            jy1.b();
        }
    }
    
    public final void a(final ew0 ew0) {
        for (int k = ew0.k(), i = 0; i < k; ++i) {
            final nw nw = new nw(this, i);
            Character.toChars(nw.f(), this.b, i * 2);
            this.h(nw);
        }
    }
    
    public char[] c() {
        return this.b;
    }
    
    public ew0 d() {
        return this.a;
    }
    
    public int e() {
        return this.a.l();
    }
    
    public a f() {
        return this.c;
    }
    
    public Typeface g() {
        return this.d;
    }
    
    public void h(final nw nw) {
        l71.h(nw, "emoji metadata cannot be null");
        l71.b(nw.c() > 0, "invalid metadata codepoint length");
        this.c.c(nw, 0, nw.c() - 1);
    }
    
    public static class a
    {
        public final SparseArray a;
        public nw b;
        
        public a() {
            this(1);
        }
        
        public a(final int n) {
            this.a = new SparseArray(n);
        }
        
        public a a(final int n) {
            final SparseArray a = this.a;
            a a2;
            if (a == null) {
                a2 = null;
            }
            else {
                a2 = (a)a.get(n);
            }
            return a2;
        }
        
        public final nw b() {
            return this.b;
        }
        
        public void c(final nw b, final int n, final int n2) {
            a a;
            if ((a = this.a(b.b(n))) == null) {
                a = new a();
                this.a.put(b.b(n), (Object)a);
            }
            if (n2 > n) {
                a.c(b, n + 1, n2);
            }
            else {
                a.b = b;
            }
        }
    }
}

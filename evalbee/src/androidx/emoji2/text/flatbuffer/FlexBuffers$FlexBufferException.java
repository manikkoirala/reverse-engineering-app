// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

public class FlexBuffers$FlexBufferException extends RuntimeException
{
    public FlexBuffers$FlexBufferException(final String message) {
        super(message);
    }
}

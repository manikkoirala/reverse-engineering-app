// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

public final class Utf8Safe extends Utf8
{
    public static class UnpairedSurrogateException extends IllegalArgumentException
    {
        public UnpairedSurrogateException(final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unpaired surrogate at index ");
            sb.append(i);
            sb.append(" of ");
            sb.append(j);
            super(sb.toString());
        }
    }
}

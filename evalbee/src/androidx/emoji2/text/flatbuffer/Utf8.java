// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

public abstract class Utf8
{
    public static Utf8 a;
    
    public static Utf8 a() {
        if (Utf8.a == null) {
            Utf8.a = new Utf8Safe();
        }
        return Utf8.a;
    }
    
    public static class UnpairedSurrogateException extends IllegalArgumentException
    {
        public UnpairedSurrogateException(final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unpaired surrogate at index ");
            sb.append(i);
            sb.append(" of ");
            sb.append(j);
            super(sb.toString());
        }
    }
}

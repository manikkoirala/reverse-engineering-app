// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.util.Arrays;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.Spannable;
import android.text.method.MetaKeyKeyListener;
import android.view.inputmethod.InputConnection;
import android.text.Selection;
import android.view.KeyEvent;
import android.text.Editable;

public final class d
{
    public final c.i a;
    public final f b;
    public c.d c;
    public final boolean d;
    public final int[] e;
    
    public d(final f b, final c.i a, final c.d c, final boolean d, final int[] e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static boolean b(final Editable editable, final KeyEvent keyEvent, final boolean b) {
        if (g(keyEvent)) {
            return false;
        }
        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
        if (f(selectionStart, selectionEnd)) {
            return false;
        }
        final ow[] array = (ow[])((Spanned)editable).getSpans(selectionStart, selectionEnd, (Class)ow.class);
        if (array != null && array.length > 0) {
            for (final ow ow : array) {
                final int spanStart = ((Spanned)editable).getSpanStart((Object)ow);
                final int spanEnd = ((Spanned)editable).getSpanEnd((Object)ow);
                if ((b && spanStart == selectionStart) || (!b && spanEnd == selectionStart) || (selectionStart > spanStart && selectionStart < spanEnd)) {
                    editable.delete(spanStart, spanEnd);
                    return true;
                }
            }
        }
        return false;
    }
    
    public static boolean c(final InputConnection inputConnection, final Editable editable, int a, int a2, final boolean b) {
        if (editable != null) {
            if (inputConnection != null) {
                if (a >= 0) {
                    if (a2 >= 0) {
                        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
                        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
                        if (f(selectionStart, selectionEnd)) {
                            return false;
                        }
                        Label_0121: {
                            if (b) {
                                a = d.a.a((CharSequence)editable, selectionStart, Math.max(a, 0));
                                final int b2 = d.a.b((CharSequence)editable, selectionEnd, Math.max(a2, 0));
                                if (a != -1) {
                                    a2 = a;
                                    if ((a = b2) != -1) {
                                        break Label_0121;
                                    }
                                }
                                return false;
                            }
                            final int max = Math.max(selectionStart - a, 0);
                            a = Math.min(selectionEnd + a2, ((CharSequence)editable).length());
                            a2 = max;
                        }
                        final ow[] array = (ow[])((Spanned)editable).getSpans(a2, a, (Class)ow.class);
                        if (array != null && array.length > 0) {
                            for (final ow ow : array) {
                                final int spanStart = ((Spanned)editable).getSpanStart((Object)ow);
                                final int spanEnd = ((Spanned)editable).getSpanEnd((Object)ow);
                                a2 = Math.min(spanStart, a2);
                                a = Math.max(spanEnd, a);
                            }
                            a2 = Math.max(a2, 0);
                            a = Math.min(a, ((CharSequence)editable).length());
                            inputConnection.beginBatchEdit();
                            editable.delete(a2, a);
                            inputConnection.endBatchEdit();
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    public static boolean d(final Editable editable, final int n, final KeyEvent keyEvent) {
        boolean b;
        if (n != 67) {
            b = (n == 112 && b(editable, keyEvent, true));
        }
        else {
            b = b(editable, keyEvent, false);
        }
        if (b) {
            MetaKeyKeyListener.adjustMetaAfterKeypress((Spannable)editable);
            return true;
        }
        return false;
    }
    
    public static boolean f(final int n, final int n2) {
        return n == -1 || n2 == -1 || n != n2;
    }
    
    public static boolean g(final KeyEvent keyEvent) {
        return KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) ^ true;
    }
    
    public final void a(final Spannable spannable, final nw nw, final int n, final int n2) {
        spannable.setSpan((Object)this.a.a(nw), n, n2, 33);
    }
    
    public final boolean e(final CharSequence charSequence, final int n, final int n2, final nw nw) {
        if (nw.d() == 0) {
            nw.k(this.c.a(charSequence, n, n2, nw.h()));
        }
        return nw.d() == 2;
    }
    
    public CharSequence h(final CharSequence charSequence, int n, int max, int n2, final boolean b) {
        final boolean b2 = charSequence instanceof vo1;
        if (b2) {
            ((vo1)charSequence).a();
        }
        Label_0080: {
            if (b2) {
                break Label_0080;
            }
            try {
                Object o;
                if (charSequence instanceof Spannable) {
                    o = new a12((Spannable)charSequence);
                }
                else if (charSequence instanceof Spanned && ((Spanned)charSequence).nextSpanTransition(n - 1, max + 1, (Class)ow.class) <= max) {
                    o = new a12(charSequence);
                }
                else {
                    o = null;
                }
                int index = n;
                int n3 = max;
                if (o != null) {
                    final ow[] array = (ow[])((a12)o).getSpans(n, max, ow.class);
                    index = n;
                    n3 = max;
                    if (array != null) {
                        index = n;
                        n3 = max;
                        if (array.length > 0) {
                            final int length = array.length;
                            int n4 = 0;
                            while (true) {
                                index = n;
                                n3 = max;
                                if (n4 >= length) {
                                    break;
                                }
                                final ow ow = array[n4];
                                final int spanStart = ((a12)o).getSpanStart(ow);
                                final int spanEnd = ((a12)o).getSpanEnd(ow);
                                if (spanStart != max) {
                                    ((a12)o).removeSpan(ow);
                                }
                                n = Math.min(spanStart, n);
                                max = Math.max(spanEnd, max);
                                ++n4;
                            }
                        }
                    }
                }
                if (index == n3 || index >= charSequence.length()) {
                    return charSequence;
                }
                int n5;
                if ((n5 = n2) != Integer.MAX_VALUE) {
                    n5 = n2;
                    if (o != null) {
                        n5 = n2 - ((ow[])((a12)o).getSpans(0, ((a12)o).length(), ow.class)).length;
                    }
                }
                final b b3 = new b(this.b.f(), this.d, this.e);
                int codePoint = Character.codePointAt(charSequence, index);
                n2 = 0;
                int n6 = 0;
            Label_0320:
                while (true) {
                    n6 = index;
                    max = index;
                    n = codePoint;
                    while (max < n3 && n2 < n5) {
                        final int a = b3.a(n);
                        if (a != 1) {
                            if (a != 2) {
                                if (a != 3) {
                                    continue;
                                }
                                if (!b) {
                                    codePoint = n;
                                    index = max;
                                    if (this.e(charSequence, n6, max, b3.c())) {
                                        continue Label_0320;
                                    }
                                }
                                a12 a2;
                                if ((a2 = (a12)o) == null) {
                                    a2 = new a12((Spannable)new SpannableString(charSequence));
                                }
                                this.a((Spannable)a2, b3.c(), n6, max);
                                ++n2;
                                o = a2;
                                codePoint = n;
                                index = max;
                                continue Label_0320;
                            }
                            else {
                                final int index2 = max + Character.charCount(n);
                                if ((max = index2) >= n3) {
                                    continue;
                                }
                                n = Character.codePointAt(charSequence, index2);
                                max = index2;
                            }
                        }
                        else {
                            n6 += Character.charCount(Character.codePointAt(charSequence, n6));
                            if (n6 < n3) {
                                n = Character.codePointAt(charSequence, n6);
                            }
                            max = n6;
                        }
                    }
                    break;
                }
                a12 a3 = (a12)o;
                Label_0608: {
                    if (b3.e()) {
                        a3 = (a12)o;
                        if (n2 < n5) {
                            if (!b) {
                                a3 = (a12)o;
                                if (this.e(charSequence, n6, max, b3.b())) {
                                    break Label_0608;
                                }
                            }
                            if ((a3 = (a12)o) == null) {
                                a3 = new a12(charSequence);
                            }
                            this.a((Spannable)a3, b3.b(), n6, max);
                        }
                    }
                }
                if (a3 != null) {
                    return (CharSequence)a3.b();
                }
                return charSequence;
            }
            finally {
                if (b2) {
                    ((vo1)charSequence).d();
                }
            }
        }
    }
    
    public abstract static final class a
    {
        public static int a(final CharSequence charSequence, int n, int i) {
            final int length = charSequence.length();
            if (n < 0 || length < n) {
                return -1;
            }
            if (i < 0) {
                return -1;
            }
        Label_0027:
            while (true) {
                int n2 = 0;
                while (i != 0) {
                    if (--n < 0) {
                        if (n2 != 0) {
                            return -1;
                        }
                        return 0;
                    }
                    else {
                        final char char1 = charSequence.charAt(n);
                        if (n2 != 0) {
                            if (!Character.isHighSurrogate(char1)) {
                                return -1;
                            }
                            --i;
                            continue Label_0027;
                        }
                        else if (!Character.isSurrogate(char1)) {
                            --i;
                        }
                        else {
                            if (Character.isHighSurrogate(char1)) {
                                return -1;
                            }
                            n2 = 1;
                        }
                    }
                }
                return n;
            }
        }
        
        public static int b(final CharSequence charSequence, int n, int i) {
            final int length = charSequence.length();
            if (n < 0 || length < n) {
                return -1;
            }
            if (i < 0) {
                return -1;
            }
        Label_0027:
            while (true) {
                int n2 = 0;
                while (i != 0) {
                    if (n >= length) {
                        if (n2 != 0) {
                            return -1;
                        }
                        return length;
                    }
                    else {
                        final char char1 = charSequence.charAt(n);
                        if (n2 != 0) {
                            if (!Character.isLowSurrogate(char1)) {
                                return -1;
                            }
                            --i;
                            ++n;
                            continue Label_0027;
                        }
                        else if (!Character.isSurrogate(char1)) {
                            --i;
                            ++n;
                        }
                        else {
                            if (Character.isLowSurrogate(char1)) {
                                return -1;
                            }
                            ++n;
                            n2 = 1;
                        }
                    }
                }
                return n;
            }
        }
    }
    
    public static final class b
    {
        public int a;
        public final f.a b;
        public f.a c;
        public f.a d;
        public int e;
        public int f;
        public final boolean g;
        public final int[] h;
        
        public b(final f.a a, final boolean g, final int[] h) {
            this.a = 1;
            this.b = a;
            this.c = a;
            this.g = g;
            this.h = h;
        }
        
        public static boolean d(final int n) {
            return n == 65039;
        }
        
        public static boolean f(final int n) {
            return n == 65038;
        }
        
        public int a(final int e) {
            final f.a a = this.c.a(e);
            final int a2 = this.a;
            int g = 2;
            Label_0140: {
                if (a2 != 2) {
                    if (a != null) {
                        this.a = 2;
                        this.c = a;
                        this.f = 1;
                        break Label_0140;
                    }
                }
                else {
                    if (a != null) {
                        this.c = a;
                        ++this.f;
                        break Label_0140;
                    }
                    if (!f(e)) {
                        if (d(e)) {
                            break Label_0140;
                        }
                        if (this.c.b() != null) {
                            final int f = this.f;
                            g = 3;
                            if (f != 1 || this.h()) {
                                this.d = this.c;
                                this.g();
                                break Label_0140;
                            }
                        }
                    }
                }
                g = this.g();
            }
            this.e = e;
            return g;
        }
        
        public nw b() {
            return this.c.b();
        }
        
        public nw c() {
            return this.d.b();
        }
        
        public boolean e() {
            if (this.a == 2 && this.c.b() != null) {
                final int f = this.f;
                boolean b = true;
                if (f > 1) {
                    return b;
                }
                if (this.h()) {
                    b = b;
                    return b;
                }
            }
            return false;
        }
        
        public final int g() {
            this.a = 1;
            this.c = this.b;
            this.f = 0;
            return 1;
        }
        
        public final boolean h() {
            if (this.c.b().j()) {
                return true;
            }
            if (d(this.e)) {
                return true;
            }
            if (this.g) {
                if (this.h == null) {
                    return true;
                }
                if (Arrays.binarySearch(this.h, this.c.b().b(0)) < 0) {
                    return true;
                }
            }
            return false;
        }
    }
}

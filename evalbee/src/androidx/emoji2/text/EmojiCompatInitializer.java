// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.Collections;
import java.util.List;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ProcessLifecycleInitializer;
import android.content.Context;

public class EmojiCompatInitializer implements af0
{
    public Boolean b(final Context context) {
        androidx.emoji2.text.c.g((androidx.emoji2.text.c.c)new a(context));
        this.c(context);
        return Boolean.TRUE;
    }
    
    public void c(final Context context) {
        final Lifecycle lifecycle = ((qj0)t7.e(context).f(ProcessLifecycleInitializer.class)).getLifecycle();
        lifecycle.a(new vq(this, lifecycle) {
            public final Lifecycle a;
            public final EmojiCompatInitializer b;
            
            @Override
            public void b(final qj0 qj0) {
                this.b.d();
                this.a.c(this);
            }
        });
    }
    
    public void d() {
        bk.d().postDelayed((Runnable)new c(), 500L);
    }
    
    @Override
    public List dependencies() {
        return Collections.singletonList(ProcessLifecycleInitializer.class);
    }
    
    public static class a extends c.c
    {
        public a(final Context context) {
            super(new EmojiCompatInitializer.b(context));
            ((c.c)this).b(1);
        }
    }
    
    public static class b implements g
    {
        public final Context a;
        
        public b(final Context context) {
            this.a = context.getApplicationContext();
        }
        
        @Override
        public void a(final h h) {
            final ThreadPoolExecutor b = bk.b("EmojiCompatInitializer");
            b.execute(new hw(this, h, b));
        }
        
        public void c(final h h, final ThreadPoolExecutor threadPoolExecutor) {
            try {
                final androidx.emoji2.text.e a = androidx.emoji2.text.a.a(this.a);
                if (a == null) {
                    throw new RuntimeException("EmojiCompat font provider not available on this device.");
                }
                a.c(threadPoolExecutor);
                ((c.c)a).a().a(new h(this, h, threadPoolExecutor) {
                    public final h a;
                    public final ThreadPoolExecutor b;
                    public final EmojiCompatInitializer.b c;
                    
                    @Override
                    public void a(final Throwable t) {
                        try {
                            this.a.a(t);
                        }
                        finally {
                            this.b.shutdown();
                        }
                    }
                    
                    @Override
                    public void b(final androidx.emoji2.text.f f) {
                        try {
                            this.a.b(f);
                        }
                        finally {
                            this.b.shutdown();
                        }
                    }
                });
            }
            finally {
                final Throwable t;
                h.a(t);
                threadPoolExecutor.shutdown();
            }
        }
    }
    
    public static class c implements Runnable
    {
        @Override
        public void run() {
            try {
                jy1.a("EmojiCompat.EmojiCompatInitializer.run");
                if (androidx.emoji2.text.c.h()) {
                    androidx.emoji2.text.c.b().k();
                }
            }
            finally {
                jy1.b();
            }
        }
    }
}

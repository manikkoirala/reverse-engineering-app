// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.Signature;
import android.os.Build$VERSION;
import android.content.Context;

public abstract class a
{
    public static e a(final Context context) {
        return (e)new a(null).c(context);
    }
    
    public static class a
    {
        public final b a;
        
        public a(b e) {
            if (e == null) {
                e = e();
            }
            this.a = e;
        }
        
        public static b e() {
            if (Build$VERSION.SDK_INT >= 28) {
                return new d();
            }
            return new c();
        }
        
        public final androidx.emoji2.text.c.c a(final Context context, final g70 g70) {
            if (g70 == null) {
                return null;
            }
            return new e(context, g70);
        }
        
        public final List b(final Signature[] array) {
            final ArrayList o = new ArrayList();
            for (int length = array.length, i = 0; i < length; ++i) {
                o.add(array[i].toByteArray());
            }
            return Collections.singletonList(o);
        }
        
        public androidx.emoji2.text.c.c c(final Context context) {
            return this.a(context, this.h(context));
        }
        
        public final g70 d(final ProviderInfo providerInfo, final PackageManager packageManager) {
            final String authority = providerInfo.authority;
            final String packageName = providerInfo.packageName;
            return new g70(authority, packageName, "emojicompat-emoji-font", this.b(this.a.b(packageManager, packageName)));
        }
        
        public final boolean f(final ProviderInfo providerInfo) {
            if (providerInfo != null) {
                final ApplicationInfo applicationInfo = providerInfo.applicationInfo;
                if (applicationInfo != null) {
                    final int flags = applicationInfo.flags;
                    final boolean b = true;
                    if ((flags & 0x1) == 0x1) {
                        return b;
                    }
                }
            }
            return false;
        }
        
        public final ProviderInfo g(final PackageManager packageManager) {
            final Iterator iterator = this.a.c(packageManager, new Intent("androidx.content.action.LOAD_EMOJI_FONT"), 0).iterator();
            while (iterator.hasNext()) {
                final ProviderInfo a = this.a.a((ResolveInfo)iterator.next());
                if (this.f(a)) {
                    return a;
                }
            }
            return null;
        }
        
        public g70 h(final Context context) {
            final PackageManager packageManager = context.getPackageManager();
            l71.h(packageManager, "Package manager required to locate emoji font provider");
            final ProviderInfo g = this.g(packageManager);
            if (g == null) {
                return null;
            }
            try {
                return this.d(g, packageManager);
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.wtf("emoji2.text.DefaultEmojiConfig", (Throwable)ex);
                return null;
            }
        }
    }
    
    public abstract static class b
    {
        public abstract ProviderInfo a(final ResolveInfo p0);
        
        public Signature[] b(final PackageManager packageManager, final String s) {
            return packageManager.getPackageInfo(s, 64).signatures;
        }
        
        public abstract List c(final PackageManager p0, final Intent p1, final int p2);
    }
    
    public static class c extends b
    {
        @Override
        public ProviderInfo a(final ResolveInfo resolveInfo) {
            return resolveInfo.providerInfo;
        }
        
        @Override
        public List c(final PackageManager packageManager, final Intent intent, final int n) {
            return packageManager.queryIntentContentProviders(intent, n);
        }
    }
    
    public static class d extends c
    {
        @Override
        public Signature[] b(final PackageManager packageManager, final String s) {
            return packageManager.getPackageInfo(s, 64).signatures;
        }
    }
}

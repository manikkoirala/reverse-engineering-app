// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import android.view.View;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

@Deprecated
public class BrowserActionsFallbackMenuView extends LinearLayout
{
    public final int a;
    public final int b;
    
    public BrowserActionsFallbackMenuView(final Context context, final AttributeSet set) {
        super(context, set);
        this.a = ((View)this).getResources().getDimensionPixelOffset(xa1.b);
        this.b = ((View)this).getResources().getDimensionPixelOffset(xa1.a);
    }
    
    public void onMeasure(final int n, final int n2) {
        super.onMeasure(View$MeasureSpec.makeMeasureSpec(Math.min(((View)this).getResources().getDisplayMetrics().widthPixels - this.a * 2, this.b), 1073741824), n2);
    }
}

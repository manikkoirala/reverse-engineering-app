// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager.widget;

import android.view.accessibility.AccessibilityRecord;
import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.os.Bundle;
import android.content.res.TypedArray;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.ViewConfiguration;
import android.database.DataSetObserver;
import android.view.View$MeasureSpec;
import android.view.MotionEvent;
import android.graphics.Canvas;
import android.view.accessibility.AccessibilityEvent;
import android.view.KeyEvent;
import android.view.SoundEffectConstants;
import android.view.FocusFinder;
import android.util.Log;
import android.view.ViewGroup$LayoutParams;
import java.util.Collections;
import android.view.ViewParent;
import android.content.res.Resources$NotFoundException;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.List;
import android.widget.Scroller;
import android.os.Parcelable;
import android.graphics.Rect;
import java.util.ArrayList;
import android.view.VelocityTracker;
import android.widget.EdgeEffect;
import java.util.Comparator;
import android.view.animation.Interpolator;
import android.view.ViewGroup;

public class ViewPager extends ViewGroup
{
    public static final Interpolator A1;
    public static final m B1;
    public static final int[] y1;
    public static final Comparator z1;
    public boolean A;
    public EdgeEffect A0;
    public int C;
    public EdgeEffect C0;
    public boolean D;
    public boolean F;
    public int G;
    public int H;
    public int I;
    public float J;
    public float K;
    public float M;
    public float O;
    public int P;
    public VelocityTracker Q;
    public int U;
    public int V;
    public int W;
    public int a;
    public final ArrayList b;
    public boolean b1;
    public final f c;
    public int c0;
    public boolean c1;
    public final Rect d;
    public boolean d1;
    public u21 e;
    public int f;
    public int g;
    public int g1;
    public Parcelable h;
    public ClassLoader i;
    public Scroller j;
    public boolean k;
    public boolean k0;
    public List k1;
    public k l;
    public int m;
    public Drawable n;
    public int p;
    public j p1;
    public int q;
    public j s1;
    public float t;
    public List t1;
    public int u1;
    public float v;
    public ArrayList v1;
    public int w;
    public final Runnable w1;
    public int x;
    public int x1;
    public boolean y;
    public boolean z;
    
    static {
        y1 = new int[] { 16842931 };
        z1 = new Comparator() {
            public int a(final f f, final f f2) {
                return f.b - f2.b;
            }
        };
        A1 = (Interpolator)new Interpolator() {
            public float getInterpolation(float n) {
                --n;
                return n * n * n * n * n + 1.0f;
            }
        };
        B1 = new m();
    }
    
    public ViewPager(final Context context, final AttributeSet set) {
        super(context, set);
        this.b = new ArrayList();
        this.c = new f();
        this.d = new Rect();
        this.g = -1;
        this.h = null;
        this.i = null;
        this.t = -3.4028235E38f;
        this.v = Float.MAX_VALUE;
        this.C = 1;
        this.P = -1;
        this.b1 = true;
        this.c1 = false;
        this.w1 = new Runnable(this) {
            public final ViewPager a;
            
            @Override
            public void run() {
                this.a.setScrollState(0);
                this.a.D();
            }
        };
        this.x1 = 0;
        this.u();
    }
    
    private int getClientWidth() {
        return ((View)this).getMeasuredWidth() - ((View)this).getPaddingLeft() - ((View)this).getPaddingRight();
    }
    
    private void setScrollingCacheEnabled(final boolean z) {
        if (this.z != z) {
            this.z = z;
        }
    }
    
    public static boolean v(final View view) {
        return view.getClass().getAnnotation(e.class) != null;
    }
    
    public boolean A() {
        final u21 e = this.e;
        if (e != null && this.f < e.c() - 1) {
            this.M(this.f + 1, true);
            return true;
        }
        return false;
    }
    
    public final boolean B(int n) {
        if (this.b.size() == 0) {
            if (this.b1) {
                return false;
            }
            this.d1 = false;
            this.x(0, 0.0f, 0);
            if (this.d1) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        else {
            final f s = this.s();
            final int clientWidth = this.getClientWidth();
            final int m = this.m;
            final float n2 = (float)m;
            final float n3 = (float)clientWidth;
            final float n4 = n2 / n3;
            final int b = s.b;
            final float n5 = (n / n3 - s.e) / (s.d + n4);
            n = (int)((clientWidth + m) * n5);
            this.d1 = false;
            this.x(b, n5, n);
            if (this.d1) {
                return true;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
    }
    
    public final boolean C(float j) {
        final float i = this.J;
        this.J = j;
        final float n = ((View)this).getScrollX() + (i - j);
        final float n2 = (float)this.getClientWidth();
        j = this.t * n2;
        float n3 = this.v * n2;
        final ArrayList b = this.b;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = false;
        final f f = b.get(0);
        final ArrayList b5 = this.b;
        final f f2 = b5.get(b5.size() - 1);
        boolean b6;
        if (f.b != 0) {
            j = f.e * n2;
            b6 = false;
        }
        else {
            b6 = true;
        }
        boolean b7;
        if (f2.b != this.e.c() - 1) {
            n3 = f2.e * n2;
            b7 = false;
        }
        else {
            b7 = true;
        }
        if (n < j) {
            if (b6) {
                this.A0.onPull(Math.abs(j - n) / n2);
                b4 = true;
            }
        }
        else {
            b4 = b3;
            j = n;
            if (n > n3) {
                b4 = b2;
                if (b7) {
                    this.C0.onPull(Math.abs(n - n3) / n2);
                    b4 = true;
                }
                j = n3;
            }
        }
        final float k = this.J;
        final int n4 = (int)j;
        this.J = k + (j - n4);
        ((View)this).scrollTo(n4, ((View)this).getScrollY());
        this.B(n4);
        return b4;
    }
    
    public void D() {
        this.E(this.f);
    }
    
    public void E(int i) {
        final int f = this.f;
        f t;
        if (f != i) {
            t = this.t(f);
            this.f = i;
        }
        else {
            t = null;
        }
        if (this.e == null) {
            this.Q();
            return;
        }
        if (this.A) {
            this.Q();
            return;
        }
        if (((View)this).getWindowToken() == null) {
            return;
        }
        this.e.n(this);
        i = this.C;
        final int max = Math.max(0, this.f - i);
        final int c = this.e.c();
        final int min = Math.min(c - 1, this.f + i);
        if (c == this.a) {
            i = 0;
            while (true) {
                while (i < this.b.size()) {
                    final f f2 = this.b.get(i);
                    final int b = f2.b;
                    final int f3 = this.f;
                    if (b >= f3) {
                        if (b == f3) {
                            f a = f2;
                            if (f2 == null) {
                                a = f2;
                                if (c > 0) {
                                    a = this.a(this.f, i);
                                }
                            }
                            if (a != null) {
                                int index = i - 1;
                                f f4;
                                if (index >= 0) {
                                    f4 = this.b.get(index);
                                }
                                else {
                                    f4 = null;
                                }
                                final int clientWidth = this.getClientWidth();
                                float n;
                                if (clientWidth <= 0) {
                                    n = 0.0f;
                                }
                                else {
                                    n = 2.0f - a.d + ((View)this).getPaddingLeft() / (float)clientWidth;
                                }
                                int j = this.f - 1;
                                float n2 = 0.0f;
                                f f5 = f4;
                                while (j >= 0) {
                                    int n3 = 0;
                                    int n4 = 0;
                                    f f6 = null;
                                    float n5 = 0.0f;
                                    Label_0531: {
                                        Label_0524: {
                                            int n6 = 0;
                                            int n7 = 0;
                                            Label_0514: {
                                                if (n2 >= n && j < max) {
                                                    if (f5 == null) {
                                                        break;
                                                    }
                                                    n3 = i;
                                                    n4 = index;
                                                    f6 = f5;
                                                    n5 = n2;
                                                    if (j != f5.b) {
                                                        break Label_0531;
                                                    }
                                                    n3 = i;
                                                    n4 = index;
                                                    f6 = f5;
                                                    n5 = n2;
                                                    if (f5.c) {
                                                        break Label_0531;
                                                    }
                                                    this.b.remove(index);
                                                    this.e.a(this, j, f5.a);
                                                    --index;
                                                    n6 = --i;
                                                    n7 = index;
                                                    n5 = n2;
                                                    if (index < 0) {
                                                        break Label_0514;
                                                    }
                                                }
                                                else if (f5 != null && j == f5.b) {
                                                    n2 += f5.d;
                                                    --index;
                                                    n6 = i;
                                                    n7 = index;
                                                    n5 = n2;
                                                    if (index < 0) {
                                                        break Label_0514;
                                                    }
                                                }
                                                else {
                                                    n2 += this.a(j, index + 1).d;
                                                    n6 = ++i;
                                                    n7 = index;
                                                    n5 = n2;
                                                    if (index < 0) {
                                                        break Label_0514;
                                                    }
                                                }
                                                f6 = this.b.get(index);
                                                n5 = n2;
                                                break Label_0524;
                                            }
                                            f6 = null;
                                            index = n7;
                                            i = n6;
                                        }
                                        n4 = index;
                                        n3 = i;
                                    }
                                    --j;
                                    i = n3;
                                    index = n4;
                                    f5 = f6;
                                    n2 = n5;
                                }
                                float d = a.d;
                                int index2 = i + 1;
                                if (d < 2.0f) {
                                    f f7;
                                    if (index2 < this.b.size()) {
                                        f7 = this.b.get(index2);
                                    }
                                    else {
                                        f7 = null;
                                    }
                                    float n8;
                                    if (clientWidth <= 0) {
                                        n8 = 0.0f;
                                    }
                                    else {
                                        n8 = ((View)this).getPaddingRight() / (float)clientWidth + 2.0f;
                                    }
                                    int f8 = this.f;
                                    f f9 = f7;
                                    while (true) {
                                        final int n9 = f8 + 1;
                                        if (n9 >= c) {
                                            break;
                                        }
                                        float n10 = 0.0f;
                                        int n11 = 0;
                                        f f10 = null;
                                        Label_0867: {
                                            Label_0772: {
                                                if (d >= n8 && n9 > min) {
                                                    if (f9 == null) {
                                                        break;
                                                    }
                                                    n10 = d;
                                                    n11 = index2;
                                                    f10 = f9;
                                                    if (n9 != f9.b) {
                                                        break Label_0867;
                                                    }
                                                    n10 = d;
                                                    n11 = index2;
                                                    f10 = f9;
                                                    if (f9.c) {
                                                        break Label_0867;
                                                    }
                                                    this.b.remove(index2);
                                                    this.e.a(this, n9, f9.a);
                                                    n10 = d;
                                                    if ((n11 = index2) >= this.b.size()) {
                                                        break Label_0772;
                                                    }
                                                }
                                                else if (f9 != null && n9 == f9.b) {
                                                    d += f9.d;
                                                    ++index2;
                                                    n10 = d;
                                                    if ((n11 = index2) >= this.b.size()) {
                                                        break Label_0772;
                                                    }
                                                }
                                                else {
                                                    final f a2 = this.a(n9, index2);
                                                    ++index2;
                                                    d = (n10 = d + a2.d);
                                                    if ((n11 = index2) >= this.b.size()) {
                                                        break Label_0772;
                                                    }
                                                }
                                                f10 = this.b.get(index2);
                                                n10 = d;
                                                n11 = index2;
                                                break Label_0867;
                                            }
                                            f10 = null;
                                        }
                                        d = n10;
                                        index2 = n11;
                                        f9 = f10;
                                        f8 = n9;
                                    }
                                }
                                this.e(a, i, t);
                                this.e.l(this, this.f, a.a);
                            }
                            this.e.b(this);
                            int childCount;
                            View child;
                            g g;
                            f r;
                            for (childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                                child = this.getChildAt(i);
                                g = (g)child.getLayoutParams();
                                g.f = i;
                                if (!g.a && g.c == 0.0f) {
                                    r = this.r(child);
                                    if (r != null) {
                                        g.c = r.d;
                                        g.e = r.b;
                                    }
                                }
                            }
                            this.Q();
                            if (((View)this).hasFocus()) {
                                final View focus = ((View)this).findFocus();
                                f q;
                                if (focus != null) {
                                    q = this.q(focus);
                                }
                                else {
                                    q = null;
                                }
                                if (q == null || q.b != this.f) {
                                    View child2;
                                    f r2;
                                    for (i = 0; i < this.getChildCount(); ++i) {
                                        child2 = this.getChildAt(i);
                                        r2 = this.r(child2);
                                        if (r2 != null && r2.b == this.f && child2.requestFocus(2)) {
                                            break;
                                        }
                                    }
                                }
                            }
                            return;
                        }
                        break;
                    }
                    else {
                        ++i;
                    }
                }
                final f f2 = null;
                continue;
            }
        }
        String str;
        try {
            str = ((View)this).getResources().getResourceName(((View)this).getId());
        }
        catch (final Resources$NotFoundException ex) {
            str = Integer.toHexString(((View)this).getId());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: ");
        sb.append(this.a);
        sb.append(", found: ");
        sb.append(c);
        sb.append(" Pager id: ");
        sb.append(str);
        sb.append(" Pager class: ");
        sb.append(this.getClass());
        sb.append(" Problematic adapter: ");
        sb.append(this.e.getClass());
        throw new IllegalStateException(sb.toString());
    }
    
    public final void F(int n, final int n2, final int n3, final int n4) {
        if (n2 > 0 && !this.b.isEmpty()) {
            if (!this.j.isFinished()) {
                this.j.setFinalX(this.getCurrentItem() * this.getClientWidth());
                return;
            }
            n = (int)(((View)this).getScrollX() / (float)(n2 - ((View)this).getPaddingLeft() - ((View)this).getPaddingRight() + n4) * (n - ((View)this).getPaddingLeft() - ((View)this).getPaddingRight() + n3));
        }
        else {
            final f t = this.t(this.f);
            float min;
            if (t != null) {
                min = Math.min(t.e, this.v);
            }
            else {
                min = 0.0f;
            }
            n = (int)(min * (n - ((View)this).getPaddingLeft() - ((View)this).getPaddingRight()));
            if (n == ((View)this).getScrollX()) {
                return;
            }
            this.g(false);
        }
        ((View)this).scrollTo(n, ((View)this).getScrollY());
    }
    
    public final void G() {
        int n;
        for (int i = 0; i < this.getChildCount(); i = n + 1) {
            n = i;
            if (!((g)this.getChildAt(i).getLayoutParams()).a) {
                this.removeViewAt(i);
                n = i - 1;
            }
        }
    }
    
    public void H(final i i) {
        final List t1 = this.t1;
        if (t1 != null) {
            t1.remove(i);
        }
    }
    
    public void I(final j j) {
        final List k1 = this.k1;
        if (k1 != null) {
            k1.remove(j);
        }
    }
    
    public final void J(final boolean b) {
        final ViewParent parent = ((View)this).getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(b);
        }
    }
    
    public final boolean K() {
        this.P = -1;
        this.n();
        this.A0.onRelease();
        this.C0.onRelease();
        return this.A0.isFinished() || this.C0.isFinished();
    }
    
    public final void L(final int n, final boolean b, final int n2, final boolean b2) {
        final f t = this.t(n);
        int n3;
        if (t != null) {
            n3 = (int)(this.getClientWidth() * Math.max(this.t, Math.min(t.e, this.v)));
        }
        else {
            n3 = 0;
        }
        if (b) {
            this.P(n3, 0, n2);
            if (b2) {
                this.k(n);
            }
        }
        else {
            if (b2) {
                this.k(n);
            }
            this.g(false);
            ((View)this).scrollTo(n3, 0);
            this.B(n3);
        }
    }
    
    public void M(final int n, final boolean b) {
        this.N(n, b, this.A = false);
    }
    
    public void N(final int n, final boolean b, final boolean b2) {
        this.O(n, b, b2, 0);
    }
    
    public void O(int i, final boolean b, final boolean b2, final int n) {
        final u21 e = this.e;
        final boolean b3 = false;
        if (e == null || e.c() <= 0) {
            this.setScrollingCacheEnabled(false);
            return;
        }
        if (!b2 && this.f == i && this.b.size() != 0) {
            this.setScrollingCacheEnabled(false);
            return;
        }
        int f;
        if (i < 0) {
            f = 0;
        }
        else if ((f = i) >= this.e.c()) {
            f = this.e.c() - 1;
        }
        i = this.C;
        final int f2 = this.f;
        if (f > f2 + i || f < f2 - i) {
            for (i = 0; i < this.b.size(); ++i) {
                this.b.get(i).c = true;
            }
        }
        boolean b4 = b3;
        if (this.f != f) {
            b4 = true;
        }
        if (this.b1) {
            this.f = f;
            if (b4) {
                this.k(f);
            }
            ((View)this).requestLayout();
        }
        else {
            this.E(f);
            this.L(f, b, n, b4);
        }
    }
    
    public void P(int a, int n, final int a2) {
        if (this.getChildCount() == 0) {
            this.setScrollingCacheEnabled(false);
            return;
        }
        final Scroller j = this.j;
        int n2;
        if (j != null && !j.isFinished()) {
            if (this.k) {
                n2 = this.j.getCurrX();
            }
            else {
                n2 = this.j.getStartX();
            }
            this.j.abortAnimation();
            this.setScrollingCacheEnabled(false);
        }
        else {
            n2 = ((View)this).getScrollX();
        }
        final int scrollY = ((View)this).getScrollY();
        final int n3 = a - n2;
        n -= scrollY;
        if (n3 == 0 && n == 0) {
            this.g(false);
            this.D();
            this.setScrollState(0);
            return;
        }
        this.setScrollingCacheEnabled(true);
        this.setScrollState(2);
        final int clientWidth = this.getClientWidth();
        a = clientWidth / 2;
        final float n4 = (float)Math.abs(n3);
        final float n5 = (float)clientWidth;
        final float min = Math.min(1.0f, n4 * 1.0f / n5);
        final float n6 = (float)a;
        final float m = this.m(min);
        a = Math.abs(a2);
        if (a > 0) {
            a = Math.round(Math.abs((n6 + m * n6) / a) * 1000.0f) * 4;
        }
        else {
            a = (int)((Math.abs(n3) / (n5 * this.e.f(this.f) + this.m) + 1.0f) * 100.0f);
        }
        a = Math.min(a, 600);
        this.k = false;
        this.j.startScroll(n2, scrollY, n3, n, a);
        o32.h0((View)this);
    }
    
    public final void Q() {
        if (this.u1 != 0) {
            final ArrayList v1 = this.v1;
            if (v1 == null) {
                this.v1 = new ArrayList();
            }
            else {
                v1.clear();
            }
            for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                this.v1.add(this.getChildAt(i));
            }
            Collections.sort((List<Object>)this.v1, ViewPager.B1);
        }
    }
    
    public f a(final int b, final int index) {
        final f f = new f();
        f.b = b;
        f.a = this.e.g(this, b);
        f.d = this.e.f(b);
        if (index >= 0 && index < this.b.size()) {
            this.b.add(index, f);
        }
        else {
            this.b.add(f);
        }
        return f;
    }
    
    public void addFocusables(final ArrayList list, final int n, final int n2) {
        final int size = list.size();
        final int descendantFocusability = this.getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i = 0; i < this.getChildCount(); ++i) {
                final View child = this.getChildAt(i);
                if (child.getVisibility() == 0) {
                    final f r = this.r(child);
                    if (r != null && r.b == this.f) {
                        child.addFocusables(list, n, n2);
                    }
                }
            }
        }
        if (descendantFocusability != 262144 || size == list.size()) {
            if (!((View)this).isFocusable()) {
                return;
            }
            if ((n2 & 0x1) == 0x1 && ((View)this).isInTouchMode() && !((View)this).isFocusableInTouchMode()) {
                return;
            }
            list.add(this);
        }
    }
    
    public void addTouchables(final ArrayList list) {
        for (int i = 0; i < this.getChildCount(); ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() == 0) {
                final f r = this.r(child);
                if (r != null && r.b == this.f) {
                    child.addTouchables(list);
                }
            }
        }
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        ViewGroup$LayoutParams generateLayoutParams = viewGroup$LayoutParams;
        if (!this.checkLayoutParams(viewGroup$LayoutParams)) {
            generateLayoutParams = this.generateLayoutParams(viewGroup$LayoutParams);
        }
        final g g = (g)generateLayoutParams;
        final boolean a = g.a | v(view);
        g.a = a;
        if (this.y) {
            if (a) {
                throw new IllegalStateException("Cannot add pager decor view during layout");
            }
            g.d = true;
            this.addViewInLayout(view, n, generateLayoutParams);
        }
        else {
            super.addView(view, n, generateLayoutParams);
        }
    }
    
    public void b(final i i) {
        if (this.t1 == null) {
            this.t1 = new ArrayList();
        }
        this.t1.add(i);
    }
    
    public void c(final j j) {
        if (this.k1 == null) {
            this.k1 = new ArrayList();
        }
        this.k1.add(j);
    }
    
    public boolean canScrollHorizontally(final int n) {
        final u21 e = this.e;
        final boolean b = false;
        boolean b2 = false;
        if (e == null) {
            return false;
        }
        final int clientWidth = this.getClientWidth();
        final int scrollX = ((View)this).getScrollX();
        if (n < 0) {
            if (scrollX > (int)(clientWidth * this.t)) {
                b2 = true;
            }
            return b2;
        }
        boolean b3 = b;
        if (n > 0) {
            b3 = b;
            if (scrollX < (int)(clientWidth * this.v)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof g && super.checkLayoutParams(viewGroup$LayoutParams);
    }
    
    public void computeScroll() {
        this.k = true;
        if (!this.j.isFinished() && this.j.computeScrollOffset()) {
            final int scrollX = ((View)this).getScrollX();
            final int scrollY = ((View)this).getScrollY();
            final int currX = this.j.getCurrX();
            final int currY = this.j.getCurrY();
            if (scrollX != currX || scrollY != currY) {
                ((View)this).scrollTo(currX, currY);
                if (!this.B(currX)) {
                    this.j.abortAnimation();
                    ((View)this).scrollTo(0, currY);
                }
            }
            o32.h0((View)this);
            return;
        }
        this.g(true);
    }
    
    public boolean d(final int n) {
        final View focus = ((View)this).findFocus();
        boolean b = false;
        View view = null;
        Label_0195: {
            Label_0015: {
                if (focus != this) {
                    if ((view = focus) != null) {
                        ViewParent viewParent = focus.getParent();
                        while (true) {
                            while (viewParent instanceof ViewGroup) {
                                if (viewParent == this) {
                                    final boolean b2 = true;
                                    view = focus;
                                    if (!b2) {
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append(focus.getClass().getSimpleName());
                                        for (ViewParent viewParent2 = focus.getParent(); viewParent2 instanceof ViewGroup; viewParent2 = viewParent2.getParent()) {
                                            sb.append(" => ");
                                            sb.append(viewParent2.getClass().getSimpleName());
                                        }
                                        final StringBuilder sb2 = new StringBuilder();
                                        sb2.append("arrowScroll tried to find focus based on non-child current focused view ");
                                        sb2.append(sb.toString());
                                        Log.e("ViewPager", sb2.toString());
                                        break Label_0015;
                                    }
                                    break Label_0195;
                                }
                                else {
                                    viewParent = viewParent.getParent();
                                }
                            }
                            final boolean b2 = false;
                            continue;
                        }
                    }
                    break Label_0195;
                }
            }
            view = null;
        }
        final View nextFocus = FocusFinder.getInstance().findNextFocus((ViewGroup)this, view, n);
        Label_0375: {
            if (nextFocus != null && nextFocus != view) {
                Label_0279: {
                    if (n == 17) {
                        final int left = this.p(this.d, nextFocus).left;
                        final int left2 = this.p(this.d, view).left;
                        if (view != null && left >= left2) {
                            b = this.z();
                            break Label_0279;
                        }
                    }
                    else {
                        if (n != 66) {
                            break Label_0375;
                        }
                        final int left3 = this.p(this.d, nextFocus).left;
                        final int left4 = this.p(this.d, view).left;
                        if (view != null && left3 <= left4) {
                            b = this.A();
                            break Label_0279;
                        }
                    }
                    b = nextFocus.requestFocus();
                }
            }
            else if (n != 17 && n != 1) {
                if (n == 66 || n == 2) {
                    b = this.A();
                }
            }
            else {
                b = this.z();
            }
        }
        if (b) {
            ((View)this).playSoundEffect(SoundEffectConstants.getContantForFocusDirection(n));
        }
        return b;
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || this.o(keyEvent);
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() == 0) {
                final f r = this.r(child);
                if (r != null && r.b == this.f && child.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final int overScrollMode = ((View)this).getOverScrollMode();
        int n = 0;
        int n2 = 0;
        Label_0257: {
            Label_0066: {
                if (overScrollMode != 0) {
                    if (overScrollMode == 1) {
                        final u21 e = this.e;
                        if (e != null && e.c() > 1) {
                            break Label_0066;
                        }
                    }
                    this.A0.finish();
                    this.C0.finish();
                    break Label_0257;
                }
            }
            if (!this.A0.isFinished()) {
                final int save = canvas.save();
                final int n3 = ((View)this).getHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom();
                final int width = ((View)this).getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float)(-n3 + ((View)this).getPaddingTop()), this.t * width);
                this.A0.setSize(n3, width);
                n2 = ((false | this.A0.draw(canvas)) ? 1 : 0);
                canvas.restoreToCount(save);
            }
            n = n2;
            if (!this.C0.isFinished()) {
                final int save2 = canvas.save();
                final int width2 = ((View)this).getWidth();
                final int height = ((View)this).getHeight();
                final int paddingTop = ((View)this).getPaddingTop();
                final int paddingBottom = ((View)this).getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float)(-((View)this).getPaddingTop()), -(this.v + 1.0f) * width2);
                this.C0.setSize(height - paddingTop - paddingBottom, width2);
                n = (n2 | (this.C0.draw(canvas) ? 1 : 0));
                canvas.restoreToCount(save2);
            }
        }
        if (n != 0) {
            o32.h0((View)this);
        }
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final Drawable n = this.n;
        if (n != null && n.isStateful()) {
            n.setState(((View)this).getDrawableState());
        }
    }
    
    public final void e(f f, int n, f f2) {
        int c = this.e.c();
        final int clientWidth = this.getClientWidth();
        float n2;
        if (clientWidth > 0) {
            n2 = this.m / (float)clientWidth;
        }
        else {
            n2 = 0.0f;
        }
        if (f2 != null) {
            int b = f2.b;
            final int b2 = f.b;
            if (b < b2) {
                float n3 = f2.e + f2.d + n2;
                ++b;
                int i;
                for (int index = 0; b <= f.b && index < this.b.size(); b = i + 1) {
                    float e;
                    while (true) {
                        f2 = (f)this.b.get(index);
                        i = b;
                        e = n3;
                        if (b <= f2.b) {
                            break;
                        }
                        i = b;
                        e = n3;
                        if (index >= this.b.size() - 1) {
                            break;
                        }
                        ++index;
                    }
                    while (i < f2.b) {
                        e += this.e.f(i) + n2;
                        ++i;
                    }
                    f2.e = e;
                    n3 = e + (f2.d + n2);
                }
            }
            else if (b > b2) {
                int index2 = this.b.size() - 1;
                float e2 = f2.e;
                --b;
                while (b >= f.b && index2 >= 0) {
                    int j;
                    float n4;
                    while (true) {
                        f2 = (f)this.b.get(index2);
                        j = b;
                        n4 = e2;
                        if (b >= f2.b) {
                            break;
                        }
                        j = b;
                        n4 = e2;
                        if (index2 <= 0) {
                            break;
                        }
                        --index2;
                    }
                    while (j > f2.b) {
                        n4 -= this.e.f(j) + n2;
                        --j;
                    }
                    e2 = n4 - (f2.d + n2);
                    f2.e = e2;
                    b = j - 1;
                }
            }
        }
        final int size = this.b.size();
        float e3 = f.e;
        final int b3 = f.b;
        int n5 = b3 - 1;
        float t;
        if (b3 == 0) {
            t = e3;
        }
        else {
            t = -3.4028235E38f;
        }
        this.t = t;
        --c;
        float v;
        if (b3 == c) {
            v = f.d + e3 - 1.0f;
        }
        else {
            v = Float.MAX_VALUE;
        }
        this.v = v;
        for (int k = n - 1; k >= 0; --k, --n5) {
            f2 = (f)this.b.get(k);
            int b4;
            while (true) {
                b4 = f2.b;
                if (n5 <= b4) {
                    break;
                }
                e3 -= this.e.f(n5) + n2;
                --n5;
            }
            e3 -= f2.d + n2;
            f2.e = e3;
            if (b4 == 0) {
                this.t = e3;
            }
        }
        float e4 = f.e + f.d + n2;
        final int n6 = f.b + 1;
        int l;
        int b5;
        for (l = n + 1, n = n6; l < size; ++l, ++n) {
            f = (f)this.b.get(l);
            while (true) {
                b5 = f.b;
                if (n >= b5) {
                    break;
                }
                e4 += this.e.f(n) + n2;
                ++n;
            }
            if (b5 == c) {
                this.v = f.d + e4 - 1.0f;
            }
            f.e = e4;
            e4 += f.d + n2;
        }
        this.c1 = false;
    }
    
    public boolean f(final View view, final boolean b, final int n, final int n2, final int n3) {
        final boolean b2 = view instanceof ViewGroup;
        final boolean b3 = true;
        if (b2) {
            final ViewGroup viewGroup = (ViewGroup)view;
            final int scrollX = view.getScrollX();
            final int scrollY = view.getScrollY();
            for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                final View child = viewGroup.getChildAt(i);
                final int n4 = n2 + scrollX;
                if (n4 >= child.getLeft() && n4 < child.getRight()) {
                    final int n5 = n3 + scrollY;
                    if (n5 >= child.getTop() && n5 < child.getBottom() && this.f(child, true, n, n4 - child.getLeft(), n5 - child.getTop())) {
                        return true;
                    }
                }
            }
        }
        return b && view.canScrollHorizontally(-n) && b3;
    }
    
    public final void g(final boolean b) {
        int n;
        if (this.x1 == 2) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            this.setScrollingCacheEnabled(false);
            if (this.j.isFinished() ^ true) {
                this.j.abortAnimation();
                final int scrollX = ((View)this).getScrollX();
                final int scrollY = ((View)this).getScrollY();
                final int currX = this.j.getCurrX();
                final int currY = this.j.getCurrY();
                if (scrollX != currX || scrollY != currY) {
                    ((View)this).scrollTo(currX, currY);
                    if (currX != scrollX) {
                        this.B(currX);
                    }
                }
            }
        }
        this.A = false;
        for (int i = 0; i < this.b.size(); ++i) {
            final f f = this.b.get(i);
            if (f.c) {
                f.c = false;
                n = 1;
            }
        }
        if (n != 0) {
            if (b) {
                o32.i0((View)this, this.w1);
            }
            else {
                this.w1.run();
            }
        }
    }
    
    public ViewGroup$LayoutParams generateDefaultLayoutParams() {
        return new g();
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final AttributeSet set) {
        return new g(((View)this).getContext(), set);
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return this.generateDefaultLayoutParams();
    }
    
    public u21 getAdapter() {
        return this.e;
    }
    
    public int getChildDrawingOrder(final int n, final int n2) {
        int index = n2;
        if (this.u1 == 2) {
            index = n - 1 - n2;
        }
        return ((g)((View)this.v1.get(index)).getLayoutParams()).f;
    }
    
    public int getCurrentItem() {
        return this.f;
    }
    
    public int getOffscreenPageLimit() {
        return this.C;
    }
    
    public int getPageMargin() {
        return this.m;
    }
    
    public void h() {
        final int c = this.e.c();
        this.a = c;
        final boolean b = this.b.size() < this.C * 2 + 1 && this.b.size() < c;
        int n = this.f;
        int i = 0;
        int n2 = 0;
        boolean b2 = b;
        while (i < this.b.size()) {
            final f f = this.b.get(i);
            final int d = this.e.d(f.a);
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            Label_0282: {
                if (d == -1) {
                    n3 = n;
                    n4 = i;
                    n5 = n2;
                }
                else {
                    if (d == -2) {
                        this.b.remove(i);
                        final int n6 = i - 1;
                        int n7;
                        if ((n7 = n2) == 0) {
                            this.e.n(this);
                            n7 = 1;
                        }
                        this.e.a(this, f.b, f.a);
                        final int f2 = this.f;
                        i = n6;
                        n2 = n7;
                        if (f2 == f.b) {
                            n = Math.max(0, Math.min(f2, c - 1));
                            n2 = n7;
                            i = n6;
                        }
                    }
                    else {
                        final int b3 = f.b;
                        n3 = n;
                        n4 = i;
                        n5 = n2;
                        if (b3 == d) {
                            break Label_0282;
                        }
                        if (b3 == this.f) {
                            n = d;
                        }
                        f.b = d;
                    }
                    b2 = true;
                    n3 = n;
                    n4 = i;
                    n5 = n2;
                }
            }
            i = n4 + 1;
            n = n3;
            n2 = n5;
        }
        if (n2 != 0) {
            this.e.b(this);
        }
        Collections.sort((List<Object>)this.b, ViewPager.z1);
        if (b2) {
            for (int childCount = this.getChildCount(), j = 0; j < childCount; ++j) {
                final g g = (g)this.getChildAt(j).getLayoutParams();
                if (!g.a) {
                    g.c = 0.0f;
                }
            }
            this.N(n, false, true);
            ((View)this).requestLayout();
        }
    }
    
    public final int i(int a, final float n, int max, final int a2) {
        if (Math.abs(a2) > this.W && Math.abs(max) > this.U) {
            if (max <= 0) {
                ++a;
            }
        }
        else {
            float n2;
            if (a >= this.f) {
                n2 = 0.4f;
            }
            else {
                n2 = 0.6f;
            }
            a += (int)(n + n2);
        }
        max = a;
        if (this.b.size() > 0) {
            final f f = this.b.get(0);
            final ArrayList b = this.b;
            max = Math.max(f.b, Math.min(a, ((f)b.get(b.size() - 1)).b));
        }
        return max;
    }
    
    public final void j(final int n, final float n2, final int n3) {
        final j p3 = this.p1;
        if (p3 != null) {
            p3.onPageScrolled(n, n2, n3);
        }
        final List k1 = this.k1;
        if (k1 != null) {
            for (int size = k1.size(), i = 0; i < size; ++i) {
                final j j = this.k1.get(i);
                if (j != null) {
                    j.onPageScrolled(n, n2, n3);
                }
            }
        }
        final j s1 = this.s1;
        if (s1 != null) {
            s1.onPageScrolled(n, n2, n3);
        }
    }
    
    public final void k(final int n) {
        final j p = this.p1;
        if (p != null) {
            p.onPageSelected(n);
        }
        final List k1 = this.k1;
        if (k1 != null) {
            for (int size = k1.size(), i = 0; i < size; ++i) {
                final j j = this.k1.get(i);
                if (j != null) {
                    j.onPageSelected(n);
                }
            }
        }
        final j s1 = this.s1;
        if (s1 != null) {
            s1.onPageSelected(n);
        }
    }
    
    public final void l(final int n) {
        final j p = this.p1;
        if (p != null) {
            p.onPageScrollStateChanged(n);
        }
        final List k1 = this.k1;
        if (k1 != null) {
            for (int size = k1.size(), i = 0; i < size; ++i) {
                final j j = this.k1.get(i);
                if (j != null) {
                    j.onPageScrollStateChanged(n);
                }
            }
        }
        final j s1 = this.s1;
        if (s1 != null) {
            s1.onPageScrollStateChanged(n);
        }
    }
    
    public float m(final float n) {
        return (float)Math.sin((n - 0.5f) * 0.47123894f);
    }
    
    public final void n() {
        this.D = false;
        this.F = false;
        final VelocityTracker q = this.Q;
        if (q != null) {
            q.recycle();
            this.Q = null;
        }
    }
    
    public boolean o(final KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            final int keyCode = keyEvent.getKeyCode();
            int n;
            if (keyCode != 21) {
                if (keyCode != 22) {
                    if (keyCode != 61) {
                        return false;
                    }
                    if (keyEvent.hasNoModifiers()) {
                        return this.d(2);
                    }
                    if (keyEvent.hasModifiers(1)) {
                        return this.d(1);
                    }
                    return false;
                }
                else {
                    if (keyEvent.hasModifiers(2)) {
                        return this.A();
                    }
                    n = 66;
                }
            }
            else {
                if (keyEvent.hasModifiers(2)) {
                    return this.z();
                }
                n = 17;
            }
            return this.d(n);
        }
        return false;
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.b1 = true;
    }
    
    public void onDetachedFromWindow() {
        ((View)this).removeCallbacks(this.w1);
        final Scroller j = this.j;
        if (j != null && !j.isFinished()) {
            this.j.abortAnimation();
        }
        super.onDetachedFromWindow();
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.m > 0 && this.n != null && this.b.size() > 0 && this.e != null) {
            final int scrollX = ((View)this).getScrollX();
            final int width = ((View)this).getWidth();
            final float n = (float)this.m;
            final float n2 = (float)width;
            final float n3 = n / n2;
            final ArrayList b = this.b;
            int index = 0;
            f f = b.get(0);
            float e = f.e;
            for (int size = this.b.size(), i = f.b; i < this.b.get(size - 1).b; ++i) {
                int b2;
                while (true) {
                    b2 = f.b;
                    if (i <= b2 || index >= size) {
                        break;
                    }
                    final ArrayList b3 = this.b;
                    ++index;
                    f = (f)b3.get(index);
                }
                float a;
                if (i == b2) {
                    final float e2 = f.e;
                    final float d = f.d;
                    a = (e2 + d) * n2;
                    e = e2 + d + n3;
                }
                else {
                    final float f2 = this.e.f(i);
                    a = (e + f2) * n2;
                    e += f2 + n3;
                }
                if (this.m + a > scrollX) {
                    this.n.setBounds(Math.round(a), this.p, Math.round(this.m + a), this.q);
                    this.n.draw(canvas);
                }
                if (a > scrollX + width) {
                    break;
                }
            }
        }
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final int n = motionEvent.getAction() & 0xFF;
        if (n != 3 && n != 1) {
            if (n != 0) {
                if (this.D) {
                    return true;
                }
                if (this.F) {
                    return false;
                }
            }
            if (n != 0) {
                if (n != 2) {
                    if (n == 6) {
                        this.y(motionEvent);
                    }
                }
                else {
                    final int p = this.P;
                    if (p != -1) {
                        final int pointerIndex = motionEvent.findPointerIndex(p);
                        final float x = motionEvent.getX(pointerIndex);
                        final float a = x - this.J;
                        final float abs = Math.abs(a);
                        final float y = motionEvent.getY(pointerIndex);
                        final float abs2 = Math.abs(y - this.O);
                        final float n2 = fcmpl(a, 0.0f);
                        if (n2 != 0 && !this.w(this.J, a) && this.f((View)this, false, (int)a, (int)x, (int)y)) {
                            this.J = x;
                            this.K = y;
                            this.F = true;
                            return false;
                        }
                        final int i = this.I;
                        if (abs > i && abs * 0.5f > abs2) {
                            this.J(this.D = true);
                            this.setScrollState(1);
                            final float m = this.M;
                            final float n3 = (float)this.I;
                            float j;
                            if (n2 > 0) {
                                j = m + n3;
                            }
                            else {
                                j = m - n3;
                            }
                            this.J = j;
                            this.K = y;
                            this.setScrollingCacheEnabled(true);
                        }
                        else if (abs2 > i) {
                            this.F = true;
                        }
                        if (this.D && this.C(x)) {
                            o32.h0((View)this);
                        }
                    }
                }
            }
            else {
                final float x2 = motionEvent.getX();
                this.M = x2;
                this.J = x2;
                final float y2 = motionEvent.getY();
                this.O = y2;
                this.K = y2;
                this.P = motionEvent.getPointerId(0);
                this.F = false;
                this.k = true;
                this.j.computeScrollOffset();
                if (this.x1 == 2 && Math.abs(this.j.getFinalX() - this.j.getCurrX()) > this.c0) {
                    this.j.abortAnimation();
                    this.A = false;
                    this.D();
                    this.J(this.D = true);
                    this.setScrollState(1);
                }
                else {
                    this.g(false);
                    this.D = false;
                }
            }
            if (this.Q == null) {
                this.Q = VelocityTracker.obtain();
            }
            this.Q.addMovement(motionEvent);
            return this.D;
        }
        this.K();
        return false;
    }
    
    public void onLayout(final boolean b, int paddingTop, int paddingLeft, int paddingBottom, int paddingRight) {
        final int childCount = this.getChildCount();
        final int n = paddingBottom - paddingTop;
        final int n2 = paddingRight - paddingLeft;
        paddingLeft = ((View)this).getPaddingLeft();
        paddingTop = ((View)this).getPaddingTop();
        paddingRight = ((View)this).getPaddingRight();
        paddingBottom = ((View)this).getPaddingBottom();
        final int scrollX = ((View)this).getScrollX();
        int i = 0;
        int g1 = 0;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            int n3 = paddingLeft;
            int n4 = paddingTop;
            int n5 = paddingRight;
            int n6 = paddingBottom;
            int n7 = g1;
            if (child.getVisibility() != 8) {
                final g g2 = (g)child.getLayoutParams();
                n3 = paddingLeft;
                n4 = paddingTop;
                n5 = paddingRight;
                n6 = paddingBottom;
                n7 = g1;
                if (g2.a) {
                    final int b2 = g2.b;
                    final int n8 = b2 & 0x7;
                    final int n9 = b2 & 0x70;
                    int n11 = 0;
                    Label_0246: {
                        int max;
                        if (n8 != 1) {
                            if (n8 == 3) {
                                final int n10 = child.getMeasuredWidth() + paddingLeft;
                                n11 = paddingLeft;
                                paddingLeft = n10;
                                break Label_0246;
                            }
                            if (n8 != 5) {
                                final int n12 = paddingLeft;
                                n11 = paddingLeft;
                                paddingLeft = n12;
                                break Label_0246;
                            }
                            max = n - paddingRight - child.getMeasuredWidth();
                            paddingRight += child.getMeasuredWidth();
                        }
                        else {
                            max = Math.max((n - child.getMeasuredWidth()) / 2, paddingLeft);
                        }
                        n11 = max;
                    }
                    int max2;
                    if (n9 != 16) {
                        if (n9 != 48) {
                            if (n9 != 80) {
                                final int n13 = paddingTop;
                                max2 = paddingTop;
                                paddingTop = n13;
                            }
                            else {
                                max2 = n2 - paddingBottom - child.getMeasuredHeight();
                                paddingBottom += child.getMeasuredHeight();
                            }
                        }
                        else {
                            final int n14 = child.getMeasuredHeight() + paddingTop;
                            max2 = paddingTop;
                            paddingTop = n14;
                        }
                    }
                    else {
                        max2 = Math.max((n2 - child.getMeasuredHeight()) / 2, paddingTop);
                    }
                    final int n15 = n11 + scrollX;
                    child.layout(n15, max2, child.getMeasuredWidth() + n15, max2 + child.getMeasuredHeight());
                    n7 = g1 + 1;
                    n6 = paddingBottom;
                    n5 = paddingRight;
                    n4 = paddingTop;
                    n3 = paddingLeft;
                }
            }
            ++i;
            paddingLeft = n3;
            paddingTop = n4;
            paddingRight = n5;
            paddingBottom = n6;
            g1 = n7;
        }
        for (int j = 0; j < childCount; ++j) {
            final View child2 = this.getChildAt(j);
            if (child2.getVisibility() != 8) {
                final g g3 = (g)child2.getLayoutParams();
                if (!g3.a) {
                    final f r = this.r(child2);
                    if (r != null) {
                        final float n16 = (float)(n - paddingLeft - paddingRight);
                        final int n17 = (int)(r.e * n16) + paddingLeft;
                        if (g3.d) {
                            g3.d = false;
                            child2.measure(View$MeasureSpec.makeMeasureSpec((int)(n16 * g3.c), 1073741824), View$MeasureSpec.makeMeasureSpec(n2 - paddingTop - paddingBottom, 1073741824));
                        }
                        child2.layout(n17, paddingTop, child2.getMeasuredWidth() + n17, child2.getMeasuredHeight() + paddingTop);
                    }
                }
            }
        }
        this.p = paddingTop;
        this.q = n2 - paddingBottom;
        this.g1 = g1;
        if (this.b1) {
            this.L(this.f, false, 0, false);
        }
        this.b1 = false;
    }
    
    public void onMeasure(int measuredWidth, int i) {
        final int n = 0;
        ((View)this).setMeasuredDimension(View.getDefaultSize(0, measuredWidth), View.getDefaultSize(0, i));
        measuredWidth = ((View)this).getMeasuredWidth();
        this.H = Math.min(measuredWidth / 10, this.G);
        measuredWidth = measuredWidth - ((View)this).getPaddingLeft() - ((View)this).getPaddingRight();
        i = ((View)this).getMeasuredHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom();
        final int childCount = this.getChildCount();
        int n2 = 0;
        while (true) {
            final int n3 = 1;
            int n4 = 1073741824;
            if (n2 >= childCount) {
                break;
            }
            final View child = this.getChildAt(n2);
            int n5 = measuredWidth;
            int n6 = i;
            if (child.getVisibility() != 8) {
                final g g = (g)child.getLayoutParams();
                n5 = measuredWidth;
                n6 = i;
                if (g != null) {
                    n5 = measuredWidth;
                    n6 = i;
                    if (g.a) {
                        final int b = g.b;
                        final int n7 = b & 0x7;
                        final int n8 = b & 0x70;
                        final boolean b2 = n8 == 48 || n8 == 80;
                        int n9 = n3;
                        if (n7 != 3) {
                            if (n7 == 5) {
                                n9 = n3;
                            }
                            else {
                                n9 = 0;
                            }
                        }
                        int n10 = Integer.MIN_VALUE;
                        int n11;
                        if (b2) {
                            n11 = Integer.MIN_VALUE;
                            n10 = 1073741824;
                        }
                        else if (n9 != 0) {
                            n11 = 1073741824;
                        }
                        else {
                            n11 = Integer.MIN_VALUE;
                        }
                        final int width = g.width;
                        int n13;
                        int n14;
                        if (width != -2) {
                            int n12;
                            if (width != -1) {
                                n12 = width;
                            }
                            else {
                                n12 = measuredWidth;
                            }
                            n13 = 1073741824;
                            n14 = n12;
                        }
                        else {
                            n14 = measuredWidth;
                            n13 = n10;
                        }
                        final int height = g.height;
                        int n15;
                        if (height != -2) {
                            if (height != -1) {
                                n15 = height;
                            }
                            else {
                                n15 = i;
                            }
                        }
                        else {
                            final int n16 = i;
                            n4 = n11;
                            n15 = n16;
                        }
                        child.measure(View$MeasureSpec.makeMeasureSpec(n14, n13), View$MeasureSpec.makeMeasureSpec(n15, n4));
                        if (b2) {
                            n6 = i - child.getMeasuredHeight();
                            n5 = measuredWidth;
                        }
                        else {
                            n5 = measuredWidth;
                            n6 = i;
                            if (n9 != 0) {
                                n5 = measuredWidth - child.getMeasuredWidth();
                                n6 = i;
                            }
                        }
                    }
                }
            }
            ++n2;
            measuredWidth = n5;
            i = n6;
        }
        this.w = View$MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824);
        this.x = View$MeasureSpec.makeMeasureSpec(i, 1073741824);
        this.y = true;
        this.D();
        this.y = false;
        int childCount2;
        View child2;
        g g2;
        for (childCount2 = this.getChildCount(), i = n; i < childCount2; ++i) {
            child2 = this.getChildAt(i);
            if (child2.getVisibility() != 8) {
                g2 = (g)child2.getLayoutParams();
                if (g2 == null || !g2.a) {
                    child2.measure(View$MeasureSpec.makeMeasureSpec((int)(measuredWidth * g2.c), 1073741824), this.x);
                }
            }
        }
    }
    
    public boolean onRequestFocusInDescendants(final int n, final Rect rect) {
        int childCount = this.getChildCount();
        int i;
        int n2;
        if ((n & 0x2) != 0x0) {
            i = 0;
            n2 = 1;
        }
        else {
            i = childCount - 1;
            childCount = -1;
            n2 = -1;
        }
        while (i != childCount) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() == 0) {
                final f r = this.r(child);
                if (r != null && r.b == this.f && child.requestFocus(n, rect)) {
                    return true;
                }
            }
            i += n2;
        }
        return false;
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof l)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final l l = (l)parcelable;
        super.onRestoreInstanceState(l.getSuperState());
        final u21 e = this.e;
        if (e != null) {
            e.j(l.b, l.c);
            this.N(l.a, false, true);
        }
        else {
            this.g = l.a;
            this.h = l.b;
            this.i = l.c;
        }
    }
    
    public Parcelable onSaveInstanceState() {
        final l l = new l(super.onSaveInstanceState());
        l.a = this.f;
        final u21 e = this.e;
        if (e != null) {
            l.b = e.k();
        }
        return (Parcelable)l;
    }
    
    public void onSizeChanged(final int n, int m, final int n2, final int n3) {
        super.onSizeChanged(n, m, n2, n3);
        if (n != n2) {
            m = this.m;
            this.F(n, n2, m, m);
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.k0) {
            return true;
        }
        final int action = motionEvent.getAction();
        boolean k = false;
        if (action == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        final u21 e = this.e;
        if (e != null && e.c() != 0) {
            if (this.Q == null) {
                this.Q = VelocityTracker.obtain();
            }
            this.Q.addMovement(motionEvent);
            final int n = motionEvent.getAction() & 0xFF;
            Label_0592: {
                int p = 0;
                Label_0586: {
                    if (n != 0) {
                        Label_0524: {
                            if (n != 1) {
                                if (n != 2) {
                                    if (n != 3) {
                                        if (n == 5) {
                                            final int actionIndex = motionEvent.getActionIndex();
                                            this.J = motionEvent.getX(actionIndex);
                                            p = motionEvent.getPointerId(actionIndex);
                                            break Label_0586;
                                        }
                                        if (n != 6) {
                                            break Label_0592;
                                        }
                                        this.y(motionEvent);
                                        this.J = motionEvent.getX(motionEvent.findPointerIndex(this.P));
                                        break Label_0592;
                                    }
                                    else {
                                        if (!this.D) {
                                            break Label_0592;
                                        }
                                        this.L(this.f, true, 0, false);
                                    }
                                }
                                else {
                                    if (!this.D) {
                                        final int pointerIndex = motionEvent.findPointerIndex(this.P);
                                        if (pointerIndex == -1) {
                                            break Label_0524;
                                        }
                                        final float x = motionEvent.getX(pointerIndex);
                                        final float abs = Math.abs(x - this.J);
                                        final float y = motionEvent.getY(pointerIndex);
                                        final float abs2 = Math.abs(y - this.K);
                                        if (abs > this.I && abs > abs2) {
                                            this.J(this.D = true);
                                            final float m = this.M;
                                            float j;
                                            if (x - m > 0.0f) {
                                                j = m + this.I;
                                            }
                                            else {
                                                j = m - this.I;
                                            }
                                            this.J = j;
                                            this.K = y;
                                            this.setScrollState(1);
                                            this.setScrollingCacheEnabled(true);
                                            final ViewParent parent = ((View)this).getParent();
                                            if (parent != null) {
                                                parent.requestDisallowInterceptTouchEvent(true);
                                            }
                                        }
                                    }
                                    if (this.D) {
                                        k = (false | this.C(motionEvent.getX(motionEvent.findPointerIndex(this.P))));
                                    }
                                    break Label_0592;
                                }
                            }
                            else {
                                if (!this.D) {
                                    break Label_0592;
                                }
                                final VelocityTracker q = this.Q;
                                q.computeCurrentVelocity(1000, (float)this.V);
                                final int n2 = (int)q.getXVelocity(this.P);
                                this.A = true;
                                final int clientWidth = this.getClientWidth();
                                final int scrollX = ((View)this).getScrollX();
                                final f s = this.s();
                                final float n3 = (float)this.m;
                                final float n4 = (float)clientWidth;
                                this.O(this.i(s.b, (scrollX / n4 - s.e) / (s.d + n3 / n4), n2, (int)(motionEvent.getX(motionEvent.findPointerIndex(this.P)) - this.M)), true, true, n2);
                            }
                        }
                        k = this.K();
                        break Label_0592;
                    }
                    this.j.abortAnimation();
                    this.A = false;
                    this.D();
                    final float x2 = motionEvent.getX();
                    this.M = x2;
                    this.J = x2;
                    final float y2 = motionEvent.getY();
                    this.O = y2;
                    this.K = y2;
                    p = motionEvent.getPointerId(0);
                }
                this.P = p;
            }
            if (k) {
                o32.h0((View)this);
            }
            return true;
        }
        return false;
    }
    
    public final Rect p(final Rect rect, View view) {
        Rect rect2 = rect;
        if (rect == null) {
            rect2 = new Rect();
        }
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        int bottom = view.getBottom();
        while (true) {
            rect2.bottom = bottom;
            final ViewParent parent = view.getParent();
            if (!(parent instanceof ViewGroup) || parent == this) {
                break;
            }
            view = (View)parent;
            rect2.left += view.getLeft();
            rect2.right += view.getRight();
            rect2.top += view.getTop();
            bottom = rect2.bottom + view.getBottom();
        }
        return rect2;
    }
    
    public f q(View view) {
        while (true) {
            final ViewParent parent = view.getParent();
            if (parent == this) {
                return this.r(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View)parent;
        }
    }
    
    public f r(final View view) {
        for (int i = 0; i < this.b.size(); ++i) {
            final f f = this.b.get(i);
            if (this.e.h(view, f.a)) {
                return f;
            }
        }
        return null;
    }
    
    public void removeView(final View view) {
        if (this.y) {
            this.removeViewInLayout(view);
        }
        else {
            super.removeView(view);
        }
    }
    
    public final f s() {
        final int clientWidth = this.getClientWidth();
        float e = 0.0f;
        float n;
        if (clientWidth > 0) {
            n = ((View)this).getScrollX() / (float)clientWidth;
        }
        else {
            n = 0.0f;
        }
        float n2;
        if (clientWidth > 0) {
            n2 = this.m / (float)clientWidth;
        }
        else {
            n2 = 0.0f;
        }
        int i = 0;
        int n3 = 1;
        f f = null;
        int b = -1;
        float d = 0.0f;
        while (i < this.b.size()) {
            final f f2 = this.b.get(i);
            int n4 = i;
            f c = f2;
            if (n3 == 0) {
                final int b2 = f2.b;
                ++b;
                n4 = i;
                c = f2;
                if (b2 != b) {
                    c = this.c;
                    c.e = e + d + n2;
                    c.b = b;
                    c.d = this.e.f(b);
                    n4 = i - 1;
                }
            }
            e = c.e;
            final float d2 = c.d;
            if (n3 == 0 && n < e) {
                return f;
            }
            if (n < d2 + e + n2 || n4 == this.b.size() - 1) {
                return c;
            }
            b = c.b;
            d = c.d;
            i = n4 + 1;
            n3 = 0;
            f = c;
        }
        return f;
    }
    
    public void setAdapter(final u21 e) {
        final u21 e2 = this.e;
        final int n = 0;
        if (e2 != null) {
            e2.m(null);
            this.e.n(this);
            for (int i = 0; i < this.b.size(); ++i) {
                final f f = this.b.get(i);
                this.e.a(this, f.b, f.a);
            }
            this.e.b(this);
            this.b.clear();
            this.G();
            ((View)this).scrollTo(this.f = 0, 0);
        }
        final u21 e3 = this.e;
        this.e = e;
        this.a = 0;
        if (e != null) {
            if (this.l == null) {
                this.l = new k();
            }
            this.e.m(this.l);
            this.A = false;
            final boolean b1 = this.b1;
            this.b1 = true;
            this.a = this.e.c();
            if (this.g >= 0) {
                this.e.j(this.h, this.i);
                this.N(this.g, false, true);
                this.g = -1;
                this.h = null;
                this.i = null;
            }
            else if (!b1) {
                this.D();
            }
            else {
                ((View)this).requestLayout();
            }
        }
        final List t1 = this.t1;
        if (t1 != null && !t1.isEmpty()) {
            for (int size = this.t1.size(), j = n; j < size; ++j) {
                ((i)this.t1.get(j)).onAdapterChanged(this, e3, e);
            }
        }
    }
    
    public void setCurrentItem(final int n) {
        this.A = false;
        this.N(n, this.b1 ^ true, false);
    }
    
    public void setOffscreenPageLimit(final int i) {
        int c = i;
        if (i < 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Requested offscreen page limit ");
            sb.append(i);
            sb.append(" too small; defaulting to ");
            sb.append(1);
            Log.w("ViewPager", sb.toString());
            c = 1;
        }
        if (c != this.C) {
            this.C = c;
            this.D();
        }
    }
    
    @Deprecated
    public void setOnPageChangeListener(final j p) {
        this.p1 = p;
    }
    
    public void setPageMargin(final int m) {
        final int i = this.m;
        this.m = m;
        final int width = ((View)this).getWidth();
        this.F(width, width, m, i);
        ((View)this).requestLayout();
    }
    
    public void setPageMarginDrawable(final int n) {
        this.setPageMarginDrawable(sl.getDrawable(((View)this).getContext(), n));
    }
    
    public void setPageMarginDrawable(final Drawable n) {
        this.n = n;
        if (n != null) {
            ((View)this).refreshDrawableState();
        }
        ((View)this).setWillNotDraw(n == null);
        ((View)this).invalidate();
    }
    
    public void setScrollState(final int x1) {
        if (this.x1 == x1) {
            return;
        }
        this.l(this.x1 = x1);
    }
    
    public f t(final int n) {
        for (int i = 0; i < this.b.size(); ++i) {
            final f f = this.b.get(i);
            if (f.b == n) {
                return f;
            }
        }
        return null;
    }
    
    public void u() {
        ((View)this).setWillNotDraw(false);
        this.setDescendantFocusability(262144);
        ((View)this).setFocusable(true);
        final Context context = ((View)this).getContext();
        this.j = new Scroller(context, ViewPager.A1);
        final ViewConfiguration value = ViewConfiguration.get(context);
        final float density = context.getResources().getDisplayMetrics().density;
        this.I = value.getScaledPagingTouchSlop();
        this.U = (int)(400.0f * density);
        this.V = value.getScaledMaximumFlingVelocity();
        this.A0 = new EdgeEffect(context);
        this.C0 = new EdgeEffect(context);
        this.W = (int)(25.0f * density);
        this.c0 = (int)(2.0f * density);
        this.G = (int)(density * 16.0f);
        o32.q0((View)this, new h());
        if (o32.z((View)this) == 0) {
            o32.B0((View)this, 1);
        }
        o32.E0((View)this, new g11(this) {
            public final Rect a = new Rect();
            public final ViewPager b;
            
            @Override
            public u62 onApplyWindowInsets(final View view, u62 c0) {
                c0 = o32.c0(view, c0);
                if (c0.o()) {
                    return c0;
                }
                final Rect a = this.a;
                a.left = c0.i();
                a.top = c0.k();
                a.right = c0.j();
                a.bottom = c0.h();
                for (int childCount = this.b.getChildCount(), i = 0; i < childCount; ++i) {
                    final u62 g = o32.g(this.b.getChildAt(i), c0);
                    a.left = Math.min(g.i(), a.left);
                    a.top = Math.min(g.k(), a.top);
                    a.right = Math.min(g.j(), a.right);
                    a.bottom = Math.min(g.h(), a.bottom);
                }
                return c0.p(a.left, a.top, a.right, a.bottom);
            }
        });
    }
    
    public boolean verifyDrawable(final Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.n;
    }
    
    public final boolean w(final float n, final float n2) {
        return (n < this.H && n2 > 0.0f) || (n > ((View)this).getWidth() - this.H && n2 < 0.0f);
    }
    
    public void x(final int n, final float n2, final int n3) {
        if (this.g1 > 0) {
            final int scrollX = ((View)this).getScrollX();
            int paddingLeft = ((View)this).getPaddingLeft();
            int paddingRight = ((View)this).getPaddingRight();
            final int width = ((View)this).getWidth();
            for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = this.getChildAt(i);
                final g g = (g)child.getLayoutParams();
                if (g.a) {
                    final int n4 = g.b & 0x7;
                    int max;
                    if (n4 != 1) {
                        if (n4 != 3) {
                            if (n4 != 5) {
                                final int n5 = paddingLeft;
                                max = paddingLeft;
                                paddingLeft = n5;
                            }
                            else {
                                max = width - paddingRight - child.getMeasuredWidth();
                                paddingRight += child.getMeasuredWidth();
                            }
                        }
                        else {
                            final int n6 = child.getWidth() + paddingLeft;
                            max = paddingLeft;
                            paddingLeft = n6;
                        }
                    }
                    else {
                        max = Math.max((width - child.getMeasuredWidth()) / 2, paddingLeft);
                    }
                    final int n7 = max + scrollX - child.getLeft();
                    if (n7 != 0) {
                        child.offsetLeftAndRight(n7);
                    }
                }
            }
        }
        this.j(n, n2, n3);
        this.d1 = true;
    }
    
    public final void y(final MotionEvent motionEvent) {
        final int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.P) {
            int n;
            if (actionIndex == 0) {
                n = 1;
            }
            else {
                n = 0;
            }
            this.J = motionEvent.getX(n);
            this.P = motionEvent.getPointerId(n);
            final VelocityTracker q = this.Q;
            if (q != null) {
                q.clear();
            }
        }
    }
    
    public boolean z() {
        final int f = this.f;
        if (f > 0) {
            this.M(f - 1, true);
            return true;
        }
        return false;
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    public @interface e {
    }
    
    public static class f
    {
        public Object a;
        public int b;
        public boolean c;
        public float d;
        public float e;
    }
    
    public static class g extends ViewGroup$LayoutParams
    {
        public boolean a;
        public int b;
        public float c;
        public boolean d;
        public int e;
        public int f;
        
        public g() {
            super(-1, -1);
            this.c = 0.0f;
        }
        
        public g(final Context context, final AttributeSet set) {
            super(context, set);
            this.c = 0.0f;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, ViewPager.y1);
            this.b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }
    
    public class h extends p0
    {
        public final ViewPager a;
        
        public h(final ViewPager a) {
            this.a = a;
        }
        
        public final boolean c() {
            final u21 e = this.a.e;
            if (e != null) {
                final int c = e.c();
                final boolean b = true;
                if (c > 1) {
                    return b;
                }
            }
            return false;
        }
        
        @Override
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)ViewPager.class.getName());
            ((AccessibilityRecord)accessibilityEvent).setScrollable(this.c());
            if (accessibilityEvent.getEventType() == 4096) {
                final u21 e = this.a.e;
                if (e != null) {
                    ((AccessibilityRecord)accessibilityEvent).setItemCount(e.c());
                    ((AccessibilityRecord)accessibilityEvent).setFromIndex(this.a.f);
                    ((AccessibilityRecord)accessibilityEvent).setToIndex(this.a.f);
                }
            }
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(final View view, final n1 n1) {
            super.onInitializeAccessibilityNodeInfo(view, n1);
            n1.Y(ViewPager.class.getName());
            n1.t0(this.c());
            if (this.a.canScrollHorizontally(1)) {
                n1.a(4096);
            }
            if (this.a.canScrollHorizontally(-1)) {
                n1.a(8192);
            }
        }
        
        @Override
        public boolean performAccessibilityAction(final View view, int currentItem, final Bundle bundle) {
            if (super.performAccessibilityAction(view, currentItem, bundle)) {
                return true;
            }
            ViewPager viewPager;
            if (currentItem != 4096) {
                if (currentItem != 8192) {
                    return false;
                }
                if (!this.a.canScrollHorizontally(-1)) {
                    return false;
                }
                viewPager = this.a;
                currentItem = viewPager.f - 1;
            }
            else {
                if (!this.a.canScrollHorizontally(1)) {
                    return false;
                }
                viewPager = this.a;
                currentItem = viewPager.f + 1;
            }
            viewPager.setCurrentItem(currentItem);
            return true;
        }
    }
    
    public interface i
    {
        void onAdapterChanged(final ViewPager p0, final u21 p1, final u21 p2);
    }
    
    public interface j
    {
        void onPageScrollStateChanged(final int p0);
        
        void onPageScrolled(final int p0, final float p1, final int p2);
        
        void onPageSelected(final int p0);
    }
    
    public class k extends DataSetObserver
    {
        public final ViewPager a;
        
        public k(final ViewPager a) {
            this.a = a;
        }
        
        public void onChanged() {
            this.a.h();
        }
        
        public void onInvalidated() {
            this.a.h();
        }
    }
    
    public static class l extends e
    {
        public static final Parcelable$Creator<l> CREATOR;
        public int a;
        public Parcelable b;
        public ClassLoader c;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator() {
                public l a(final Parcel parcel) {
                    return new l(parcel, null);
                }
                
                public l b(final Parcel parcel, final ClassLoader classLoader) {
                    return new l(parcel, classLoader);
                }
                
                public l[] c(final int n) {
                    return new l[n];
                }
            };
        }
        
        public l(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            ClassLoader classLoader2 = classLoader;
            if (classLoader == null) {
                classLoader2 = this.getClass().getClassLoader();
            }
            this.a = parcel.readInt();
            this.b = parcel.readParcelable(classLoader2);
            this.c = classLoader2;
        }
        
        public l(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("FragmentPager.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" position=");
            sb.append(this.a);
            sb.append("}");
            return sb.toString();
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.b, n);
        }
    }
    
    public static class m implements Comparator
    {
        public int a(final View view, final View view2) {
            final g g = (g)view.getLayoutParams();
            final g g2 = (g)view2.getLayoutParams();
            final boolean a = g.a;
            if (a != g2.a) {
                int n;
                if (a) {
                    n = 1;
                }
                else {
                    n = -1;
                }
                return n;
            }
            return g.e - g2.e;
        }
    }
}

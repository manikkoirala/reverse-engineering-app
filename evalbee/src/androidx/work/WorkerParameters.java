// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import java.util.List;
import android.net.Network;
import java.util.HashSet;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.Set;
import java.util.UUID;

public final class WorkerParameters
{
    public UUID a;
    public b b;
    public Set c;
    public a d;
    public int e;
    public Executor f;
    public hu1 g;
    public aa2 h;
    public e91 i;
    public r70 j;
    public int k;
    
    public WorkerParameters(final UUID a, final b b, final Collection c, final a d, final int e, final int k, final Executor f, final hu1 g, final aa2 h, final e91 i, final r70 j) {
        this.a = a;
        this.b = b;
        this.c = new HashSet(c);
        this.d = d;
        this.e = e;
        this.k = k;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }
    
    public Executor a() {
        return this.f;
    }
    
    public r70 b() {
        return this.j;
    }
    
    public UUID c() {
        return this.a;
    }
    
    public b d() {
        return this.b;
    }
    
    public Network e() {
        return this.d.c;
    }
    
    public e91 f() {
        return this.i;
    }
    
    public int g() {
        return this.e;
    }
    
    public Set h() {
        return this.c;
    }
    
    public hu1 i() {
        return this.g;
    }
    
    public List j() {
        return this.d.a;
    }
    
    public List k() {
        return this.d.b;
    }
    
    public aa2 l() {
        return this.h;
    }
    
    public static class a
    {
        public List a;
        public List b;
        public Network c;
        
        public a() {
            this.a = Collections.emptyList();
            this.b = Collections.emptyList();
        }
    }
}

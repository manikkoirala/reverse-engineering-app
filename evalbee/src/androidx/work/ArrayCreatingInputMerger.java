// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

public final class ArrayCreatingInputMerger extends kf0
{
    @Override
    public b a(final List list) {
        fg0.e((Object)list, "inputs");
        final b.a a = new b.a();
        final HashMap hashMap = new HashMap();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            final Map h = ((b)iterator.next()).h();
            fg0.d((Object)h, "input.keyValueMap");
            for (final Map.Entry<String, V> entry : h.entrySet()) {
                final String s = entry.getKey();
                final V value = entry.getValue();
                Class<?> class1;
                if (value != null) {
                    class1 = value.getClass();
                }
                else {
                    class1 = String.class;
                }
                final Object value2 = hashMap.get(s);
                fg0.d((Object)s, "key");
                Object o;
                if (value2 == null) {
                    if (class1.isArray()) {
                        o = value;
                    }
                    else {
                        o = this.d(value, class1);
                    }
                }
                else {
                    final Class<?> class2 = value2.getClass();
                    if (fg0.a((Object)class2, (Object)class1)) {
                        fg0.d((Object)value, "value");
                        o = this.c(value2, value);
                    }
                    else {
                        if (!fg0.a((Object)class2.getComponentType(), (Object)class1)) {
                            throw new IllegalArgumentException();
                        }
                        o = this.b(value2, value, class1);
                    }
                }
                fg0.d(o, "if (existingValue == nul\u2026      }\n                }");
                hashMap.put(s, o);
            }
        }
        a.d(hashMap);
        final b a2 = a.a();
        fg0.d((Object)a2, "output.build()");
        return a2;
    }
    
    public final Object b(final Object o, final Object o2, final Class componentType) {
        final int length = Array.getLength(o);
        final Object instance = Array.newInstance(componentType, length + 1);
        System.arraycopy(o, 0, instance, 0, length);
        Array.set(instance, length, o2);
        fg0.d(instance, "newArray");
        return instance;
    }
    
    public final Object c(final Object o, final Object o2) {
        final int length = Array.getLength(o);
        final int length2 = Array.getLength(o2);
        final Class<?> componentType = o.getClass().getComponentType();
        fg0.b((Object)componentType);
        final Object instance = Array.newInstance(componentType, length + length2);
        System.arraycopy(o, 0, instance, 0, length);
        System.arraycopy(o2, 0, instance, length, length2);
        fg0.d(instance, "newArray");
        return instance;
    }
    
    public final Object d(final Object o, final Class componentType) {
        final Object instance = Array.newInstance(componentType, 1);
        Array.set(instance, 0, o);
        fg0.d(instance, "newArray");
        return instance;
    }
}

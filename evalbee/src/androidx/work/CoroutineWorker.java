// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.f;
import kotlin.coroutines.CoroutineContext;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n$a;
import java.util.concurrent.Executor;
import kotlinx.coroutines.n;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlinx.coroutines.CoroutineDispatcher;

public abstract class CoroutineWorker extends c
{
    public final pi a;
    public final um1 b;
    public final CoroutineDispatcher c;
    
    public CoroutineWorker(@NotNull final Context context, @NotNull final WorkerParameters workerParameters) {
        fg0.e((Object)context, "appContext");
        fg0.e((Object)workerParameters, "params");
        super(context, workerParameters);
        this.a = ah0.b((n)null, 1, (Object)null);
        final um1 s = um1.s();
        fg0.d((Object)s, "create()");
        (this.b = s).addListener(new nm(this), this.getTaskExecutor().d());
        this.c = pt.a();
    }
    
    public static final void b(final CoroutineWorker coroutineWorker) {
        fg0.e((Object)coroutineWorker, "this$0");
        if (coroutineWorker.b.isCancelled()) {
            n$a.a((n)coroutineWorker.a, (CancellationException)null, 1, (Object)null);
        }
    }
    
    public static /* synthetic */ Object g(final CoroutineWorker coroutineWorker, final vl vl) {
        throw new IllegalStateException("Not implemented");
    }
    
    public abstract Object d(final vl p0);
    
    public CoroutineDispatcher e() {
        return this.c;
    }
    
    public Object f(final vl vl) {
        return g(this, vl);
    }
    
    @Override
    public final ik0 getForegroundInfoAsync() {
        final pi b = ah0.b((n)null, 1, (Object)null);
        final lm a = f.a(((k)this.e()).plus((CoroutineContext)b));
        final JobListenableFuture jobListenableFuture = new JobListenableFuture((n)b, null, 2, null);
        ad.d(a, (CoroutineContext)null, (CoroutineStart)null, (q90)new CoroutineWorker$getForegroundInfoAsync.CoroutineWorker$getForegroundInfoAsync$1(jobListenableFuture, this, (vl)null), 3, (Object)null);
        return jobListenableFuture;
    }
    
    public final um1 h() {
        return this.b;
    }
    
    @Override
    public final void onStopped() {
        super.onStopped();
        this.b.cancel(false);
    }
    
    @Override
    public final ik0 startWork() {
        ad.d(f.a(((k)this.e()).plus((CoroutineContext)this.a)), (CoroutineContext)null, (CoroutineStart)null, (q90)new CoroutineWorker$startWork.CoroutineWorker$startWork$1(this, (vl)null), 3, (Object)null);
        return this.b;
    }
}

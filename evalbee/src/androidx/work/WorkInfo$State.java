// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum WorkInfo$State
{
    private static final WorkInfo$State[] $VALUES;
    
    BLOCKED, 
    CANCELLED, 
    ENQUEUED, 
    FAILED, 
    RUNNING, 
    SUCCEEDED;
    
    private static final /* synthetic */ WorkInfo$State[] $values() {
        return new WorkInfo$State[] { WorkInfo$State.ENQUEUED, WorkInfo$State.RUNNING, WorkInfo$State.SUCCEEDED, WorkInfo$State.FAILED, WorkInfo$State.BLOCKED, WorkInfo$State.CANCELLED };
    }
    
    static {
        $VALUES = $values();
    }
    
    public final boolean isFinished() {
        return this == WorkInfo$State.SUCCEEDED || this == WorkInfo$State.FAILED || this == WorkInfo$State.CANCELLED;
    }
}

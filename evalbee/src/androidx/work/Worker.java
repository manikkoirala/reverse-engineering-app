// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.content.Context;

public abstract class Worker extends c
{
    um1 mFuture;
    
    public Worker(final Context context, final WorkerParameters workerParameters) {
        super(context, workerParameters);
    }
    
    public abstract a doWork();
    
    public p70 getForegroundInfo() {
        throw new IllegalStateException("Expedited WorkRequests require a Worker to provide an implementation for \n `getForegroundInfo()`");
    }
    
    @Override
    public ik0 getForegroundInfoAsync() {
        final um1 s = um1.s();
        this.getBackgroundExecutor().execute(new Runnable(this, s) {
            public final um1 a;
            public final Worker b;
            
            @Override
            public void run() {
                try {
                    this.a.o(this.b.getForegroundInfo());
                }
                finally {
                    final Throwable t;
                    this.a.p(t);
                }
            }
        });
        return s;
    }
    
    @Override
    public final ik0 startWork() {
        this.mFuture = um1.s();
        this.getBackgroundExecutor().execute(new Runnable(this) {
            public final Worker a;
            
            @Override
            public void run() {
                try {
                    this.a.mFuture.o(this.a.doWork());
                }
                finally {
                    final Throwable t;
                    this.a.mFuture.p(t);
                }
            }
        });
        return this.mFuture;
    }
}

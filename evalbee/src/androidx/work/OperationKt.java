// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.concurrent.Executor;
import kotlinx.coroutines.c;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import java.util.concurrent.ExecutionException;

public abstract class OperationKt
{
    public static final Object a(final d d, final vl vl) {
        Object o = null;
        Label_0046: {
            if (vl instanceof OperationKt$await.OperationKt$await$1) {
                final OperationKt$await.OperationKt$await$1 operationKt$await$1 = (OperationKt$await.OperationKt$await$1)vl;
                final int label = operationKt$await$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    operationKt$await$1.label = label + Integer.MIN_VALUE;
                    o = operationKt$await$1;
                    break Label_0046;
                }
            }
            o = new OperationKt$await.OperationKt$await$1(vl);
        }
        final Object result = ((OperationKt$await.OperationKt$await$1)o).result;
        final Object d2 = gg0.d();
        final int label2 = ((OperationKt$await.OperationKt$await$1)o).label;
        Object value = null;
        Label_0236: {
            if (label2 != 0) {
                if (label2 != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                final ik0 ik0 = (ik0)((OperationKt$await.OperationKt$await$1)o).L$0;
                xe1.b(result);
                value = result;
            }
            else {
                xe1.b(result);
                final ik0 result2 = d.getResult();
                fg0.d((Object)result2, "result");
                if (result2.isDone()) {
                    try {
                        value = result2.get();
                        break Label_0236;
                    }
                    catch (final ExecutionException ex) {
                        final Throwable cause = ex.getCause();
                        if (cause != null) {
                            ex = (ExecutionException)cause;
                        }
                        throw ex;
                    }
                }
                ((OperationKt$await.OperationKt$await$1)o).L$0 = result2;
                ((OperationKt$await.OperationKt$await$1)o).label = 1;
                final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c((vl)o), 1);
                c.z();
                result2.addListener(new jk0((df)c, result2), DirectExecutor.INSTANCE);
                ((df)c).y((c90)new ListenableFutureKt$await$2$2(result2));
                final Object v = c.v();
                if (v == gg0.d()) {
                    zp.c((vl)o);
                }
                if ((value = v) == d2) {
                    return d2;
                }
            }
        }
        fg0.d(value, "result.await()");
        return value;
    }
}

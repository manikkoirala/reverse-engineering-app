// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executor;
import java.util.concurrent.CancellationException;
import org.jetbrains.annotations.Nullable;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.n;

public final class JobListenableFuture implements ik0
{
    public final n a;
    public final um1 b;
    
    public JobListenableFuture(final n a, final um1 b) {
        fg0.e((Object)a, "job");
        fg0.e((Object)b, "underlying");
        this.a = a;
        this.b = b;
        a.q((c90)new c90(this) {
            final JobListenableFuture this$0;
            
            public final void invoke(@Nullable Throwable t) {
                if (t == null) {
                    if (!JobListenableFuture.a(this.this$0).isDone()) {
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                }
                else if (t instanceof CancellationException) {
                    JobListenableFuture.a(this.this$0).cancel(true);
                }
                else {
                    final um1 a = JobListenableFuture.a(this.this$0);
                    final Throwable cause = t.getCause();
                    if (cause != null) {
                        t = cause;
                    }
                    a.p(t);
                }
            }
        });
    }
    
    public static final /* synthetic */ um1 a(final JobListenableFuture jobListenableFuture) {
        return jobListenableFuture.b;
    }
    
    @Override
    public void addListener(final Runnable runnable, final Executor executor) {
        this.b.addListener(runnable, executor);
    }
    
    public final void b(final Object o) {
        this.b.o(o);
    }
    
    @Override
    public boolean cancel(final boolean b) {
        return this.b.cancel(b);
    }
    
    @Override
    public Object get() {
        return this.b.get();
    }
    
    @Override
    public Object get(final long n, final TimeUnit timeUnit) {
        return this.b.get(n, timeUnit);
    }
    
    @Override
    public boolean isCancelled() {
        return this.b.isCancelled();
    }
    
    @Override
    public boolean isDone() {
        return this.b.isDone();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import androidx.work.c;
import androidx.work.WorkerParameters;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import androidx.work.Worker;

public final class CombineContinuationsWorker extends Worker
{
    public CombineContinuationsWorker(@NotNull final Context context, @NotNull final WorkerParameters workerParameters) {
        fg0.e((Object)context, "context");
        fg0.e((Object)workerParameters, "workerParams");
        super(context, workerParameters);
    }
    
    @Override
    public a doWork() {
        final a d = a.d(this.getInputData());
        fg0.d((Object)d, "success(inputData)");
        return d;
    }
}

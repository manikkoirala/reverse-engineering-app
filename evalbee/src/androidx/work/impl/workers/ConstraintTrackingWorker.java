// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import android.os.Build$VERSION;
import kotlinx.coroutines.CoroutineDispatcher;
import java.util.concurrent.Executor;
import androidx.work.impl.constraints.WorkConstraintsTrackerKt;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import androidx.work.impl.constraints.a;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import androidx.work.WorkerParameters;
import androidx.work.c;

public final class ConstraintTrackingWorker extends c implements k11
{
    public final WorkerParameters a;
    public final Object b;
    public volatile boolean c;
    public final um1 d;
    public c e;
    
    public ConstraintTrackingWorker(@NotNull final Context context, @NotNull final WorkerParameters a) {
        fg0.e((Object)context, "appContext");
        fg0.e((Object)a, "workerParameters");
        super(context, a);
        this.a = a;
        this.b = new Object();
        this.d = um1.s();
    }
    
    public static final void f(final n n) {
        fg0.e((Object)n, "$job");
        n.b((CancellationException)null);
    }
    
    public static final void g(final ConstraintTrackingWorker constraintTrackingWorker, final ik0 ik0) {
        fg0.e((Object)constraintTrackingWorker, "this$0");
        fg0.e((Object)ik0, "$innerFuture");
        synchronized (constraintTrackingWorker.b) {
            if (constraintTrackingWorker.c) {
                final um1 d = constraintTrackingWorker.d;
                fg0.d((Object)d, "future");
                yk.c(d);
            }
            else {
                constraintTrackingWorker.d.q(ik0);
            }
            final u02 a = u02.a;
        }
    }
    
    public static final void h(final ConstraintTrackingWorker constraintTrackingWorker) {
        fg0.e((Object)constraintTrackingWorker, "this$0");
        constraintTrackingWorker.e();
    }
    
    @Override
    public void c(final p92 obj, final a a) {
        fg0.e((Object)obj, "workSpec");
        fg0.e((Object)a, "state");
        final xl0 e = xl0.e();
        final String a2 = yk.a();
        final StringBuilder sb = new StringBuilder();
        sb.append("Constraints changed for ");
        sb.append(obj);
        e.a(a2, sb.toString());
        if (a instanceof androidx.work.impl.constraints.a.b) {
            synchronized (this.b) {
                this.c = true;
                final u02 a3 = u02.a;
            }
        }
    }
    
    public final void e() {
        if (this.d.isCancelled()) {
            return;
        }
        final String i = this.getInputData().i("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME");
        final xl0 e = xl0.e();
        fg0.d((Object)e, "get()");
        if (i == null || i.length() == 0) {
            e.c(yk.a(), "No worker to delegate to.");
        }
        else if ((this.e = this.getWorkerFactory().b(this.getApplicationContext(), i, this.a)) == null) {
            e.a(yk.a(), "No worker to delegate to.");
        }
        else {
            final c92 j = c92.j(this.getApplicationContext());
            fg0.d((Object)j, "getInstance(applicationContext)");
            final q92 k = j.o().K();
            final String string = this.getId().toString();
            fg0.d((Object)string, "id.toString()");
            final p92 s = k.s(string);
            if (s != null) {
                final ky1 n = j.n();
                fg0.d((Object)n, "workManagerImpl.trackers");
                final WorkConstraintsTracker workConstraintsTracker = new WorkConstraintsTracker(n);
                final CoroutineDispatcher a = j.p().a();
                fg0.d((Object)a, "workManagerImpl.workTask\u2026r.taskCoroutineDispatcher");
                this.d.addListener(new wk(WorkConstraintsTrackerKt.b(workConstraintsTracker, s, a, this)), new bt1());
                if (workConstraintsTracker.a(s)) {
                    final String a2 = yk.a();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Constraints met for delegate ");
                    sb.append(i);
                    e.a(a2, sb.toString());
                    try {
                        final c e2 = this.e;
                        fg0.b((Object)e2);
                        final ik0 startWork = e2.startWork();
                        fg0.d((Object)startWork, "delegate!!.startWork()");
                        startWork.addListener(new xk(this, startWork), this.getBackgroundExecutor());
                        return;
                    }
                    finally {
                        final String a3 = yk.a();
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Delegated worker ");
                        sb2.append(i);
                        sb2.append(" threw exception in startWork.");
                        final Throwable t;
                        e.b(a3, sb2.toString(), t);
                        synchronized (this.b) {
                            if (this.c) {
                                e.a(yk.a(), "Constraints were unmet, Retrying.");
                                final um1 d = this.d;
                                fg0.d((Object)d, "future");
                                yk.c(d);
                            }
                            else {
                                final um1 d2 = this.d;
                                fg0.d((Object)d2, "future");
                                yk.b(d2);
                            }
                        }
                    }
                }
                final String a4 = yk.a();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Constraints not met for delegate ");
                sb3.append(i);
                sb3.append(". Requesting retry.");
                e.a(a4, sb3.toString());
                final um1 d3 = this.d;
                fg0.d((Object)d3, "future");
                yk.c(d3);
                return;
            }
        }
        final um1 d4 = this.d;
        fg0.d((Object)d4, "future");
        yk.b(d4);
    }
    
    @Override
    public void onStopped() {
        super.onStopped();
        final c e = this.e;
        if (e != null && !e.isStopped()) {
            int stopReason;
            if (Build$VERSION.SDK_INT >= 31) {
                stopReason = this.getStopReason();
            }
            else {
                stopReason = 0;
            }
            e.stop(stopReason);
        }
    }
    
    @Override
    public ik0 startWork() {
        this.getBackgroundExecutor().execute(new vk(this));
        final um1 d = this.d;
        fg0.d((Object)d, "future");
        return d;
    }
}

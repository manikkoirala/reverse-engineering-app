// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import java.util.List;
import androidx.work.impl.WorkDatabase;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import androidx.work.c;
import androidx.work.WorkerParameters;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import androidx.work.Worker;

public final class DiagnosticsWorker extends Worker
{
    public DiagnosticsWorker(@NotNull final Context context, @NotNull final WorkerParameters workerParameters) {
        fg0.e((Object)context, "context");
        fg0.e((Object)workerParameters, "parameters");
        super(context, workerParameters);
    }
    
    @Override
    public a doWork() {
        final c92 j = c92.j(this.getApplicationContext());
        fg0.d((Object)j, "getInstance(applicationContext)");
        final WorkDatabase o = j.o();
        fg0.d((Object)o, "workManager.workDatabase");
        final q92 k = o.K();
        final g92 i = o.I();
        final w92 l = o.L();
        final gt1 h = o.H();
        final List q = k.q(j.h().a().currentTimeMillis() - TimeUnit.DAYS.toMillis(1L));
        final List z = k.z();
        final List h2 = k.h(200);
        if (q.isEmpty() ^ true) {
            xl0.e().f(ws.a(), "Recently completed work:\n\n");
            xl0.e().f(ws.a(), ws.b(i, l, h, q));
        }
        if (z.isEmpty() ^ true) {
            xl0.e().f(ws.a(), "Running work:\n\n");
            xl0.e().f(ws.a(), ws.b(i, l, h, z));
        }
        if (h2.isEmpty() ^ true) {
            xl0.e().f(ws.a(), "Enqueued work:\n\n");
            xl0.e().f(ws.a(), ws.b(i, l, h, h2));
        }
        final a c = androidx.work.c.a.c();
        fg0.d((Object)c, "success()");
        return c;
    }
}

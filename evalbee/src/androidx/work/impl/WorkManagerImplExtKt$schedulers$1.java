// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import androidx.work.a;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

final class WorkManagerImplExtKt$schedulers$1 extends Lambda implements x90
{
    final ij1[] $schedulers;
    
    public WorkManagerImplExtKt$schedulers$1(final ij1[] $schedulers) {
        this.$schedulers = $schedulers;
        super(6);
    }
    
    @NotNull
    public final List<ij1> invoke(@NotNull final Context context, @NotNull final a a, @NotNull final hu1 hu1, @NotNull final WorkDatabase workDatabase, @NotNull final ky1 ky1, @NotNull final q81 q81) {
        fg0.e((Object)context, "<anonymous parameter 0>");
        fg0.e((Object)a, "<anonymous parameter 1>");
        fg0.e((Object)hu1, "<anonymous parameter 2>");
        fg0.e((Object)workDatabase, "<anonymous parameter 3>");
        fg0.e((Object)ky1, "<anonymous parameter 4>");
        fg0.e((Object)q81, "<anonymous parameter 5>");
        return a9.T((Object[])this.$schedulers);
    }
}

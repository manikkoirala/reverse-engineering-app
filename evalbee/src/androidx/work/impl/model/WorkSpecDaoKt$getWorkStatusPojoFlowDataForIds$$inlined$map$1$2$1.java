// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "androidx.work.impl.model.WorkSpecDaoKt$getWorkStatusPojoFlowDataForIds$$inlined$map$1$2", f = "WorkSpecDao.kt", l = { 223 }, m = "emit")
public final class WorkSpecDaoKt$getWorkStatusPojoFlowDataForIds$$inlined$map$1$2$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final s92 this$0;
    
    public WorkSpecDaoKt$getWorkStatusPojoFlowDataForIds$$inlined$map$1$2$1(final s92 s92, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "androidx.work.impl.model.WorkSpecDaoKt$dedup$$inlined$map$1$2", f = "WorkSpecDao.kt", l = { 223 }, m = "emit")
public final class WorkSpecDaoKt$dedup$$inlined$map$1$2$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final r92 this$0;
    
    public WorkSpecDaoKt$dedup$$inlined$map$1$2$1(final r92 r92, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.diagnostics;

import androidx.work.impl.workers.DiagnosticsWorker;
import androidx.work.WorkManager;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class DiagnosticsReceiver extends BroadcastReceiver
{
    public static final String a;
    
    static {
        a = xl0.i("DiagnosticsRcvr");
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        xl0.e().a(DiagnosticsReceiver.a, "Requesting diagnostics");
        try {
            WorkManager.d(context).b(t11.e(DiagnosticsWorker.class));
        }
        catch (final IllegalStateException ex) {
            xl0.e().d(DiagnosticsReceiver.a, "WorkManager is not initialized", ex);
        }
    }
}

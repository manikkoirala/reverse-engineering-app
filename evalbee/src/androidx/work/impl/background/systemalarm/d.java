// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import java.util.concurrent.Executor;
import android.os.PowerManager$WakeLock;
import java.util.Iterator;
import android.os.Looper;
import android.text.TextUtils;
import java.util.ArrayList;
import android.content.Intent;
import java.util.List;
import android.content.Context;

public class d implements qy
{
    public static final String l;
    public final Context a;
    public final hu1 b;
    public final y92 c;
    public final q81 d;
    public final c92 e;
    public final a f;
    public final List g;
    public Intent h;
    public c i;
    public pp1 j;
    public final a92 k;
    
    static {
        l = xl0.i("SystemAlarmDispatcher");
    }
    
    public d(final Context context) {
        this(context, null, null, null);
    }
    
    public d(final Context context, q81 l, c92 j, a92 k) {
        final Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.j = new pp1();
        if (j == null) {
            j = c92.j(context);
        }
        this.e = j;
        this.f = new a(applicationContext, j.h().a(), this.j);
        this.c = new y92(j.h().k());
        if (l == null) {
            l = j.l();
        }
        this.d = l;
        final hu1 p3 = j.p();
        this.b = p3;
        if (k == null) {
            k = new b92(l, p3);
        }
        this.k = k;
        l.e(this);
        this.g = new ArrayList();
        this.h = null;
    }
    
    public boolean a(final Intent obj, int i) {
        final xl0 e = xl0.e();
        final String l = androidx.work.impl.background.systemalarm.d.l;
        final StringBuilder sb = new StringBuilder();
        sb.append("Adding command ");
        sb.append(obj);
        sb.append(" (");
        sb.append(i);
        sb.append(")");
        e.a(l, sb.toString());
        this.b();
        final String action = obj.getAction();
        final boolean empty = TextUtils.isEmpty((CharSequence)action);
        final int n = 0;
        if (empty) {
            xl0.e().k(l, "Unknown command. Ignoring");
            return false;
        }
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && this.j("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        }
        obj.putExtra("KEY_START_ID", i);
        final List g = this.g;
        monitorenter(g);
        i = n;
        try {
            if (!this.g.isEmpty()) {
                i = 1;
            }
            this.g.add(obj);
            if (i == 0) {
                this.l();
            }
            return true;
        }
        finally {
            monitorexit(g);
        }
    }
    
    public final void b() {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            return;
        }
        throw new IllegalStateException("Needs to be invoked on the main thread.");
    }
    
    public void c() {
        final xl0 e = xl0.e();
        final String l = androidx.work.impl.background.systemalarm.d.l;
        e.a(l, "Checking if commands are complete.");
        this.b();
        synchronized (this.g) {
            if (this.h != null) {
                final xl0 e2 = xl0.e();
                final StringBuilder sb = new StringBuilder();
                sb.append("Removing command ");
                sb.append(this.h);
                e2.a(l, sb.toString());
                if (!this.g.remove(0).equals(this.h)) {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
                this.h = null;
            }
            final jl1 d = this.b.d();
            if (!this.f.n() && this.g.isEmpty() && !d.E()) {
                xl0.e().a(l, "No more commands & intents.");
                final c i = this.i;
                if (i != null) {
                    i.b();
                }
            }
            else if (!this.g.isEmpty()) {
                this.l();
            }
        }
    }
    
    @Override
    public void d(final x82 x82, final boolean b) {
        this.b.c().execute(new b(this, androidx.work.impl.background.systemalarm.a.c(this.a, x82, b), 0));
    }
    
    public q81 e() {
        return this.d;
    }
    
    public hu1 f() {
        return this.b;
    }
    
    public c92 g() {
        return this.e;
    }
    
    public y92 h() {
        return this.c;
    }
    
    public a92 i() {
        return this.k;
    }
    
    public final boolean j(final String s) {
        this.b();
        synchronized (this.g) {
            final Iterator iterator = this.g.iterator();
            while (iterator.hasNext()) {
                if (s.equals(((Intent)iterator.next()).getAction())) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public void k() {
        xl0.e().a(androidx.work.impl.background.systemalarm.d.l, "Destroying SystemAlarmDispatcher");
        this.d.p(this);
        this.i = null;
    }
    
    public final void l() {
        this.b();
        final PowerManager$WakeLock b = s52.b(this.a, "ProcessCommand");
        try {
            b.acquire();
            this.e.p().b(new Runnable(this) {
                public final d a;
                
                @Override
                public void run() {
                    Object o = this.a.g;
                    synchronized (o) {
                        final d a = this.a;
                        a.h = (Intent)a.g.get(0);
                        monitorexit(o);
                        o = this.a.h;
                        if (o != null) {
                            o = ((Intent)o).getAction();
                            final int intExtra = this.a.h.getIntExtra("KEY_START_ID", 0);
                            final xl0 e = xl0.e();
                            final String l = androidx.work.impl.background.systemalarm.d.l;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Processing command ");
                            sb.append(this.a.h);
                            sb.append(", ");
                            sb.append(intExtra);
                            e.a(l, sb.toString());
                            final Context a2 = this.a.a;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append((String)o);
                            sb2.append(" (");
                            sb2.append(intExtra);
                            sb2.append(")");
                            Object b = s52.b(a2, sb2.toString());
                            Label_0462: {
                                final Throwable t2;
                                try {
                                    final xl0 e2 = xl0.e();
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Acquiring operation wake lock (");
                                    sb3.append((String)o);
                                    sb3.append(") ");
                                    sb3.append(b);
                                    e2.a(l, sb3.toString());
                                    ((PowerManager$WakeLock)b).acquire();
                                    final d a3 = this.a;
                                    a3.f.o(a3.h, intExtra, a3);
                                    final xl0 e3 = xl0.e();
                                    final StringBuilder sb4 = new StringBuilder();
                                    sb4.append("Releasing operation wake lock (");
                                    sb4.append((String)o);
                                    sb4.append(") ");
                                    sb4.append(b);
                                    e3.a(l, sb4.toString());
                                    ((PowerManager$WakeLock)b).release();
                                    o = this.a.b.c();
                                    b = new d(this.a);
                                    break Label_0462;
                                }
                                finally {
                                    final xl0 xl0 = xl0.e();
                                    final String s = androidx.work.impl.background.systemalarm.d.l;
                                    final xl0 xl2 = xl0;
                                    final String s2 = s;
                                    final String s3 = "Unexpected error in onHandleIntent";
                                    final Throwable t = t2;
                                    xl2.d(s2, s3, t);
                                    final xl0 xl3 = xl0.e();
                                    final StringBuilder sb5 = new StringBuilder();
                                    final StringBuilder sb7;
                                    final StringBuilder sb6 = sb7 = sb5;
                                    final String s4 = "Releasing operation wake lock (";
                                    sb7.append(s4);
                                    final StringBuilder sb8 = sb6;
                                    sb8.append((String)o);
                                    final StringBuilder sb9 = sb6;
                                    final String s5 = ") ";
                                    sb9.append(s5);
                                    final StringBuilder sb10 = sb6;
                                    final Object o2 = b;
                                    sb10.append(o2);
                                    final xl0 xl4 = xl3;
                                    final String s6 = s;
                                    final StringBuilder sb11 = sb6;
                                    final String s7 = sb11.toString();
                                    xl4.a(s6, s7);
                                    final Object o3 = b;
                                    ((PowerManager$WakeLock)o3).release();
                                    final Runnable runnable = this;
                                    final d d = runnable.a;
                                    final hu1 hu1 = d.b;
                                    o = hu1.c();
                                    final Runnable runnable2 = this;
                                    final d d2 = runnable2.a;
                                    final d d3 = (d)(b = new d(d2));
                                }
                                try {
                                    final xl0 xl0 = xl0.e();
                                    final String s = androidx.work.impl.background.systemalarm.d.l;
                                    final xl0 xl2 = xl0;
                                    final String s2 = s;
                                    final String s3 = "Unexpected error in onHandleIntent";
                                    final Throwable t = t2;
                                    xl2.d(s2, s3, t);
                                    final xl0 xl3 = xl0.e();
                                    final StringBuilder sb5 = new StringBuilder();
                                    final StringBuilder sb7;
                                    final StringBuilder sb6 = sb7 = sb5;
                                    final String s4 = "Releasing operation wake lock (";
                                    sb7.append(s4);
                                    final StringBuilder sb8 = sb6;
                                    sb8.append((String)o);
                                    final StringBuilder sb9 = sb6;
                                    final String s5 = ") ";
                                    sb9.append(s5);
                                    final StringBuilder sb10 = sb6;
                                    final Object o2 = b;
                                    sb10.append(o2);
                                    final xl0 xl4 = xl3;
                                    final String s6 = s;
                                    final StringBuilder sb11 = sb6;
                                    final String s7 = sb11.toString();
                                    xl4.a(s6, s7);
                                    final Object o3 = b;
                                    ((PowerManager$WakeLock)o3).release();
                                    final Runnable runnable = this;
                                    final d d = runnable.a;
                                    final hu1 hu1 = d.b;
                                    o = hu1.c();
                                    final Runnable runnable2 = this;
                                    final d d2 = runnable2.a;
                                    b = new d(d2);
                                    ((Executor)o).execute((Runnable)b);
                                }
                                finally {
                                    final xl0 e4 = xl0.e();
                                    final String i = androidx.work.impl.background.systemalarm.d.l;
                                    final StringBuilder sb12 = new StringBuilder();
                                    sb12.append("Releasing operation wake lock (");
                                    sb12.append((String)o);
                                    sb12.append(") ");
                                    sb12.append(b);
                                    e4.a(i, sb12.toString());
                                    ((PowerManager$WakeLock)b).release();
                                    this.a.b.c().execute(new d(this.a));
                                }
                            }
                        }
                    }
                }
            });
        }
        finally {
            b.release();
        }
    }
    
    public void m(final c i) {
        if (this.i != null) {
            xl0.e().c(androidx.work.impl.background.systemalarm.d.l, "A completion listener for SystemAlarmDispatcher already exists.");
            return;
        }
        this.i = i;
    }
    
    public static class b implements Runnable
    {
        public final d a;
        public final Intent b;
        public final int c;
        
        public b(final d a, final Intent b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public void run() {
            this.a.a(this.b, this.c);
        }
    }
    
    public interface c
    {
        void b();
    }
    
    public static class d implements Runnable
    {
        public final androidx.work.impl.background.systemalarm.d a;
        
        public d(final androidx.work.impl.background.systemalarm.d a) {
            this.a = a;
        }
        
        @Override
        public void run() {
            this.a.c();
        }
    }
}

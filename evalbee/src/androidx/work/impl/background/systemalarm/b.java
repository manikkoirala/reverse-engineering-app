// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import android.content.Context;

public class b
{
    public static final String f;
    public final Context a;
    public final ch b;
    public final int c;
    public final d d;
    public final WorkConstraintsTracker e;
    
    static {
        f = xl0.i("ConstraintsCmdHandler");
    }
    
    public b(final Context a, final ch b, final int c, final d d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = new WorkConstraintsTracker(d.g().n());
    }
    
    public void a() {
        final List r = this.d.g().o().K().r();
        ConstraintProxy.a(this.a, r);
        final ArrayList list = new ArrayList(r.size());
        final long currentTimeMillis = this.b.currentTimeMillis();
        for (final p92 p92 : r) {
            if (currentTimeMillis >= p92.c() && (!p92.k() || this.e.a(p92))) {
                list.add((Object)p92);
            }
        }
        for (final p92 p93 : list) {
            final String a = p93.a;
            final Intent b = androidx.work.impl.background.systemalarm.a.b(this.a, u92.a(p93));
            final xl0 e = xl0.e();
            final String f = androidx.work.impl.background.systemalarm.b.f;
            final StringBuilder sb = new StringBuilder();
            sb.append("Creating a delay_met command for workSpec with id (");
            sb.append(a);
            sb.append(")");
            e.a(f, sb.toString());
            this.d.f().c().execute(new d.b(this.d, b, this.c));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class RescheduleReceiver extends BroadcastReceiver
{
    public static final String a;
    
    static {
        a = xl0.i("RescheduleReceiver");
    }
    
    public void onReceive(final Context context, final Intent obj) {
        final xl0 e = xl0.e();
        final String a = RescheduleReceiver.a;
        final StringBuilder sb = new StringBuilder();
        sb.append("Received intent ");
        sb.append(obj);
        e.a(a, sb.toString());
        try {
            c92.j(context).s(this.goAsync());
        }
        catch (final IllegalStateException ex) {
            xl0.e().d(RescheduleReceiver.a, "Cannot reschedule jobs. WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().", ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import android.content.Context;

public class SystemAlarmService extends rj0 implements c
{
    public static final String d;
    public d b;
    public boolean c;
    
    static {
        d = xl0.i("SystemAlarmService");
    }
    
    @Override
    public void b() {
        this.c = true;
        xl0.e().a(SystemAlarmService.d, "All commands completed in dispatcher");
        s52.a();
        this.stopSelf();
    }
    
    public final void e() {
        (this.b = new d((Context)this)).m((c)this);
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        this.e();
        this.c = false;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.c = true;
        this.b.k();
    }
    
    @Override
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.c) {
            xl0.e().f(SystemAlarmService.d, "Re-initializing SystemAlarmDispatcher after a request to shut-down.");
            this.b.k();
            this.e();
            this.c = false;
        }
        if (intent != null) {
            this.b.a(intent, n2);
        }
        return 3;
    }
}

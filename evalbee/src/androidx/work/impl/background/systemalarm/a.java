// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.room.RoomDatabase;
import android.os.BaseBundle;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import androidx.work.impl.WorkDatabase;
import android.os.Bundle;
import android.content.Intent;
import java.util.HashMap;
import java.util.Map;
import android.content.Context;

public class a implements qy
{
    public static final String f;
    public final Context a;
    public final Map b;
    public final Object c;
    public final ch d;
    public final pp1 e;
    
    static {
        f = xl0.i("CommandHandler");
    }
    
    public a(final Context a, final ch d, final pp1 e) {
        this.a = a;
        this.d = d;
        this.e = e;
        this.b = new HashMap();
        this.c = new Object();
    }
    
    public static Intent a(final Context context) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }
    
    public static Intent b(final Context context, final x82 x82) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        return q(intent, x82);
    }
    
    public static Intent c(final Context context, final x82 x82, final boolean b) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_NEEDS_RESCHEDULE", b);
        return q(intent, x82);
    }
    
    public static Intent e(final Context context, final x82 x82) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        return q(intent, x82);
    }
    
    public static Intent f(final Context context, final x82 x82) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        return q(intent, x82);
    }
    
    public static boolean m(final Bundle bundle, final String... array) {
        if (bundle != null && !((BaseBundle)bundle).isEmpty()) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (((BaseBundle)bundle).get(array[i]) == null) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public static x82 p(final Intent intent) {
        return new x82(intent.getStringExtra("KEY_WORKSPEC_ID"), intent.getIntExtra("KEY_WORKSPEC_GENERATION", 0));
    }
    
    public static Intent q(final Intent intent, final x82 x82) {
        intent.putExtra("KEY_WORKSPEC_ID", x82.b());
        intent.putExtra("KEY_WORKSPEC_GENERATION", x82.a());
        return intent;
    }
    
    @Override
    public void d(final x82 x82, final boolean b) {
        synchronized (this.c) {
            final c c = this.b.remove(x82);
            this.e.b(x82);
            if (c != null) {
                c.g(b);
            }
        }
    }
    
    public final void g(final Intent obj, final int n, final d d) {
        final xl0 e = xl0.e();
        final String f = androidx.work.impl.background.systemalarm.a.f;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling constraints changed ");
        sb.append(obj);
        e.a(f, sb.toString());
        new b(this.a, this.d, n, d).a();
    }
    
    public final void h(final Intent intent, final int n, final d d) {
        synchronized (this.c) {
            final x82 p3 = p(intent);
            final xl0 e = xl0.e();
            final String f = androidx.work.impl.background.systemalarm.a.f;
            final StringBuilder sb = new StringBuilder();
            sb.append("Handing delay met for ");
            sb.append(p3);
            e.a(f, sb.toString());
            if (!this.b.containsKey(p3)) {
                final c c = new c(this.a, n, d, this.e.d(p3));
                this.b.put(p3, c);
                c.f();
            }
            else {
                final xl0 e2 = xl0.e();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("WorkSpec ");
                sb2.append(p3);
                sb2.append(" is is already being handled for ACTION_DELAY_MET");
                e2.a(f, sb2.toString());
            }
        }
    }
    
    public final void i(final Intent obj, final int i) {
        final x82 p2 = p(obj);
        final boolean boolean1 = ((BaseBundle)obj.getExtras()).getBoolean("KEY_NEEDS_RESCHEDULE");
        final xl0 e = xl0.e();
        final String f = androidx.work.impl.background.systemalarm.a.f;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling onExecutionCompleted ");
        sb.append(obj);
        sb.append(", ");
        sb.append(i);
        e.a(f, sb.toString());
        this.d(p2, boolean1);
    }
    
    public final void j(final Intent obj, final int i, final d d) {
        final xl0 e = xl0.e();
        final String f = androidx.work.impl.background.systemalarm.a.f;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling reschedule ");
        sb.append(obj);
        sb.append(", ");
        sb.append(i);
        e.a(f, sb.toString());
        d.g().r();
    }
    
    public final void k(Intent o, final int n, final d d) {
        final x82 p3 = p(o);
        final xl0 e = xl0.e();
        final String f = androidx.work.impl.background.systemalarm.a.f;
        final StringBuilder sb = new StringBuilder();
        sb.append("Handling schedule work for ");
        sb.append(p3);
        e.a(f, sb.toString());
        o = (Intent)d.g().o();
        ((RoomDatabase)o).e();
        try {
            final p92 s = ((WorkDatabase)o).K().s(p3.b());
            if (s == null) {
                final xl0 e2 = xl0.e();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Skipping scheduling ");
                sb2.append(p3);
                sb2.append(" because it's no longer in the DB");
                e2.k(f, sb2.toString());
                return;
            }
            if (s.b.isFinished()) {
                final xl0 e3 = xl0.e();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Skipping scheduling ");
                sb3.append(p3);
                sb3.append("because it is finished.");
                e3.k(f, sb3.toString());
                return;
            }
            final long c = s.c();
            if (!s.k()) {
                final xl0 e4 = xl0.e();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Setting up Alarms for ");
                sb4.append(p3);
                sb4.append("at ");
                sb4.append(c);
                e4.a(f, sb4.toString());
                b4.c(this.a, (WorkDatabase)o, p3, c);
            }
            else {
                final xl0 e5 = xl0.e();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Opportunistically setting an alarm for ");
                sb5.append(p3);
                sb5.append("at ");
                sb5.append(c);
                e5.a(f, sb5.toString());
                b4.c(this.a, (WorkDatabase)o, p3, c);
                d.f().c().execute(new d.b(d, a(this.a), n));
            }
            ((RoomDatabase)o).D();
        }
        finally {
            ((RoomDatabase)o).i();
        }
    }
    
    public final void l(final Intent intent, final d d) {
        final Bundle extras = intent.getExtras();
        final String string = ((BaseBundle)extras).getString("KEY_WORKSPEC_ID");
        List c;
        if (((BaseBundle)extras).containsKey("KEY_WORKSPEC_GENERATION")) {
            final int int1 = ((BaseBundle)extras).getInt("KEY_WORKSPEC_GENERATION");
            final ArrayList list = new ArrayList(1);
            final op1 b = this.e.b(new x82(string, int1));
            c = list;
            if (b != null) {
                list.add(b);
                c = list;
            }
        }
        else {
            c = this.e.c(string);
        }
        for (final op1 op1 : c) {
            final xl0 e = xl0.e();
            final String f = androidx.work.impl.background.systemalarm.a.f;
            final StringBuilder sb = new StringBuilder();
            sb.append("Handing stopWork work for ");
            sb.append(string);
            e.a(f, sb.toString());
            d.i().b(op1);
            b4.a(this.a, d.g().o(), op1.a());
            d.d(op1.a(), false);
        }
    }
    
    public boolean n() {
        synchronized (this.c) {
            return !this.b.isEmpty();
        }
    }
    
    public void o(final Intent obj, final int n, final d d) {
        final String action = obj.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            this.g(obj, n, d);
        }
        else if ("ACTION_RESCHEDULE".equals(action)) {
            this.j(obj, n, d);
        }
        else if (!m(obj.getExtras(), "KEY_WORKSPEC_ID")) {
            final xl0 e = xl0.e();
            final String f = androidx.work.impl.background.systemalarm.a.f;
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid request for ");
            sb.append(action);
            sb.append(" , requires ");
            sb.append("KEY_WORKSPEC_ID");
            sb.append(" .");
            e.c(f, sb.toString());
        }
        else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            this.k(obj, n, d);
        }
        else if ("ACTION_DELAY_MET".equals(action)) {
            this.h(obj, n, d);
        }
        else if ("ACTION_STOP_WORK".equals(action)) {
            this.l(obj, d);
        }
        else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            this.i(obj, n);
        }
        else {
            final xl0 e2 = xl0.e();
            final String f2 = androidx.work.impl.background.systemalarm.a.f;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring intent ");
            sb2.append(obj);
            e2.k(f2, sb2.toString());
        }
    }
}

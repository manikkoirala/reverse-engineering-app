// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.BroadcastReceiver$PendingResult;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class ConstraintProxyUpdateReceiver extends BroadcastReceiver
{
    public static final String a;
    
    static {
        a = xl0.i("ConstrntProxyUpdtRecvr");
    }
    
    public static Intent a(final Context context, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        final Intent intent = new Intent("androidx.work.impl.background.systemalarm.UpdateProxies");
        intent.setComponent(new ComponentName(context, (Class)ConstraintProxyUpdateReceiver.class));
        intent.putExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", b).putExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", b2).putExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", b3).putExtra("KEY_NETWORK_STATE_PROXY_ENABLED", b4);
        return intent;
    }
    
    public void onReceive(final Context context, final Intent intent) {
        String action;
        if (intent != null) {
            action = intent.getAction();
        }
        else {
            action = null;
        }
        if (!"androidx.work.impl.background.systemalarm.UpdateProxies".equals(action)) {
            final xl0 e = xl0.e();
            final String a = ConstraintProxyUpdateReceiver.a;
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring unknown action ");
            sb.append(action);
            e.a(a, sb.toString());
        }
        else {
            c92.j(context).p().b(new Runnable(this, intent, context, this.goAsync()) {
                public final Intent a;
                public final Context b;
                public final BroadcastReceiver$PendingResult c;
                public final ConstraintProxyUpdateReceiver d;
                
                @Override
                public void run() {
                    try {
                        final boolean booleanExtra = this.a.getBooleanExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", false);
                        final boolean booleanExtra2 = this.a.getBooleanExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", false);
                        final boolean booleanExtra3 = this.a.getBooleanExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", false);
                        final boolean booleanExtra4 = this.a.getBooleanExtra("KEY_NETWORK_STATE_PROXY_ENABLED", false);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Updating proxies: (BatteryNotLowProxy (");
                        sb.append(booleanExtra);
                        sb.append("), BatteryChargingProxy (");
                        sb.append(booleanExtra2);
                        sb.append("), StorageNotLowProxy (");
                        sb.append(booleanExtra3);
                        sb.append("), NetworkStateProxy (");
                        sb.append(booleanExtra4);
                        sb.append("), ");
                        xl0.e().a(ConstraintProxyUpdateReceiver.a, sb.toString());
                        n21.c(this.b, ConstraintProxy.BatteryNotLowProxy.class, booleanExtra);
                        n21.c(this.b, ConstraintProxy.BatteryChargingProxy.class, booleanExtra2);
                        n21.c(this.b, ConstraintProxy.StorageNotLowProxy.class, booleanExtra3);
                        n21.c(this.b, ConstraintProxy.NetworkStateProxy.class, booleanExtra4);
                    }
                    finally {
                        this.c.finish();
                    }
                }
            });
        }
    }
}

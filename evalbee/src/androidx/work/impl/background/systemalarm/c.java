// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.constraints.WorkConstraintsTrackerKt;
import java.util.concurrent.CancellationException;
import androidx.work.impl.constraints.a;
import kotlinx.coroutines.n;
import kotlinx.coroutines.CoroutineDispatcher;
import android.os.PowerManager$WakeLock;
import java.util.concurrent.Executor;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import android.content.Context;

public class c implements k11, a
{
    public static final String p;
    public final Context a;
    public final int b;
    public final x82 c;
    public final d d;
    public final WorkConstraintsTracker e;
    public final Object f;
    public int g;
    public final Executor h;
    public final Executor i;
    public PowerManager$WakeLock j;
    public boolean k;
    public final op1 l;
    public final CoroutineDispatcher m;
    public volatile n n;
    
    static {
        p = xl0.i("DelayMetCommandHandler");
    }
    
    public c(final Context a, final int b, final d d, final op1 l) {
        this.a = a;
        this.b = b;
        this.d = d;
        this.c = l.a();
        this.l = l;
        final ky1 n = d.g().n();
        this.h = d.f().d();
        this.i = d.f().c();
        this.m = d.f().a();
        this.e = new WorkConstraintsTracker(n);
        this.k = false;
        this.g = 0;
        this.f = new Object();
    }
    
    @Override
    public void a(final x82 obj) {
        final xl0 e = xl0.e();
        final String p = androidx.work.impl.background.systemalarm.c.p;
        final StringBuilder sb = new StringBuilder();
        sb.append("Exceeded time limits on execution for ");
        sb.append(obj);
        e.a(p, sb.toString());
        this.h.execute(new mr(this));
    }
    
    @Override
    public void c(final p92 p2, final androidx.work.impl.constraints.a a) {
        Executor executor;
        Runnable runnable;
        if (a instanceof androidx.work.impl.constraints.a.a) {
            executor = this.h;
            runnable = new nr(this);
        }
        else {
            executor = this.h;
            runnable = new mr(this);
        }
        executor.execute(runnable);
    }
    
    public final void e() {
        synchronized (this.f) {
            if (this.n != null) {
                this.n.b((CancellationException)null);
            }
            this.d.h().b(this.c);
            final PowerManager$WakeLock j = this.j;
            if (j != null && j.isHeld()) {
                final xl0 e = xl0.e();
                final String p = androidx.work.impl.background.systemalarm.c.p;
                final StringBuilder sb = new StringBuilder();
                sb.append("Releasing wakelock ");
                sb.append(this.j);
                sb.append("for WorkSpec ");
                sb.append(this.c);
                e.a(p, sb.toString());
                this.j.release();
            }
        }
    }
    
    public void f() {
        final String b = this.c.b();
        final Context a = this.a;
        final StringBuilder sb = new StringBuilder();
        sb.append(b);
        sb.append(" (");
        sb.append(this.b);
        sb.append(")");
        this.j = s52.b(a, sb.toString());
        final xl0 e = xl0.e();
        final String p = androidx.work.impl.background.systemalarm.c.p;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Acquiring wakelock ");
        sb2.append(this.j);
        sb2.append("for WorkSpec ");
        sb2.append(b);
        e.a(p, sb2.toString());
        this.j.acquire();
        final p92 s = this.d.g().o().K().s(b);
        if (s == null) {
            this.h.execute(new mr(this));
            return;
        }
        if (!(this.k = s.k())) {
            final xl0 e2 = xl0.e();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("No constraints for ");
            sb3.append(b);
            e2.a(p, sb3.toString());
            this.h.execute(new nr(this));
        }
        else {
            this.n = WorkConstraintsTrackerKt.b(this.e, s, this.m, this);
        }
    }
    
    public void g(final boolean b) {
        final xl0 e = xl0.e();
        final String p = androidx.work.impl.background.systemalarm.c.p;
        final StringBuilder sb = new StringBuilder();
        sb.append("onExecuted ");
        sb.append(this.c);
        sb.append(", ");
        sb.append(b);
        e.a(p, sb.toString());
        this.e();
        if (b) {
            this.i.execute(new d.b(this.d, androidx.work.impl.background.systemalarm.a.e(this.a, this.c), this.b));
        }
        if (this.k) {
            this.i.execute(new d.b(this.d, androidx.work.impl.background.systemalarm.a.a(this.a), this.b));
        }
    }
    
    public final void h() {
        if (this.g == 0) {
            this.g = 1;
            final xl0 e = xl0.e();
            final String p = androidx.work.impl.background.systemalarm.c.p;
            final StringBuilder sb = new StringBuilder();
            sb.append("onAllConstraintsMet for ");
            sb.append(this.c);
            e.a(p, sb.toString());
            if (this.d.e().r(this.l)) {
                this.d.h().a(this.c, 600000L, (y92.a)this);
            }
            else {
                this.e();
            }
        }
        else {
            final xl0 e2 = xl0.e();
            final String p2 = androidx.work.impl.background.systemalarm.c.p;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Already started work for ");
            sb2.append(this.c);
            e2.a(p2, sb2.toString());
        }
    }
    
    public final void i() {
        String b = this.c.b();
        String s;
        xl0 xl0;
        StringBuilder sb3;
        if (this.g < 2) {
            this.g = 2;
            final xl0 e = xl0.e();
            s = androidx.work.impl.background.systemalarm.c.p;
            final StringBuilder sb = new StringBuilder();
            sb.append("Stopping work for WorkSpec ");
            sb.append(b);
            e.a(s, sb.toString());
            this.i.execute(new d.b(this.d, androidx.work.impl.background.systemalarm.a.f(this.a, this.c), this.b));
            if (this.d.e().k(this.c.b())) {
                final xl0 e2 = xl0.e();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("WorkSpec ");
                sb2.append(b);
                sb2.append(" needs to be rescheduled");
                e2.a(s, sb2.toString());
                this.i.execute(new d.b(this.d, androidx.work.impl.background.systemalarm.a.e(this.a, this.c), this.b));
                return;
            }
            xl0 = xl0.e();
            sb3 = new StringBuilder();
            sb3.append("Processor does not have WorkSpec ");
            sb3.append(b);
            b = ". No need to reschedule";
        }
        else {
            xl0 = xl0.e();
            s = androidx.work.impl.background.systemalarm.c.p;
            sb3 = new StringBuilder();
            sb3.append("Already stopped work for ");
        }
        sb3.append(b);
        xl0.a(s, sb3.toString());
    }
}

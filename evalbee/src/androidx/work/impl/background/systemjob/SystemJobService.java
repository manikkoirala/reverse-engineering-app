// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import android.app.Service;
import android.content.Context;
import android.os.BaseBundle;
import android.net.Network;
import android.net.Uri;
import java.util.Arrays;
import androidx.work.WorkerParameters;
import android.os.Build$VERSION;
import android.app.Application;
import android.os.PersistableBundle;
import android.app.job.JobParameters;
import java.util.HashMap;
import java.util.Map;
import android.app.job.JobService;

public class SystemJobService extends JobService implements qy
{
    public static final String e;
    public c92 a;
    public final Map b;
    public final pp1 c;
    public a92 d;
    
    static {
        e = xl0.i("SystemJobService");
    }
    
    public SystemJobService() {
        this.b = new HashMap();
        this.c = new pp1();
    }
    
    public static int a(final int n) {
        int n2 = n;
        switch (n) {
            default: {
                n2 = -512;
                return n2;
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15: {
                return n2;
            }
        }
    }
    
    public static x82 b(final JobParameters jobParameters) {
        try {
            final PersistableBundle extras = jobParameters.getExtras();
            if (extras != null && ((BaseBundle)extras).containsKey("EXTRA_WORK_SPEC_ID")) {
                return new x82(((BaseBundle)extras).getString("EXTRA_WORK_SPEC_ID"), ((BaseBundle)extras).getInt("EXTRA_WORK_SPEC_GENERATION"));
            }
            return null;
        }
        catch (final NullPointerException ex) {
            return null;
        }
    }
    
    public void d(final x82 x82, final boolean b) {
        final xl0 e = xl0.e();
        final String e2 = SystemJobService.e;
        final StringBuilder sb = new StringBuilder();
        sb.append(x82.b());
        sb.append(" executed on JobScheduler");
        e.a(e2, sb.toString());
        synchronized (this.b) {
            final JobParameters jobParameters = this.b.remove(x82);
            monitorexit(this.b);
            this.c.b(x82);
            if (jobParameters != null) {
                this.jobFinished(jobParameters, b);
            }
        }
    }
    
    public void onCreate() {
        super.onCreate();
        try {
            final c92 j = c92.j(((Context)this).getApplicationContext());
            this.a = j;
            final q81 l = j.l();
            this.d = new b92(l, this.a.p());
            l.e(this);
        }
        catch (final IllegalStateException cause) {
            if (!Application.class.equals(((Service)this).getApplication().getClass())) {
                throw new IllegalStateException("WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().", cause);
            }
            xl0.e().k(SystemJobService.e, "Could not find WorkManager instance; this may be because an auto-backup is in progress. Ignoring JobScheduler commands for now. Please make sure that you are initializing WorkManager if you have manually disabled WorkManagerInitializer.");
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        final c92 a = this.a;
        if (a != null) {
            a.l().p(this);
        }
    }
    
    public boolean onStartJob(final JobParameters jobParameters) {
        if (this.a == null) {
            xl0.e().a(SystemJobService.e, "WorkManager is not initialized; requesting retry.");
            this.jobFinished(jobParameters, true);
            return false;
        }
        final x82 b = b(jobParameters);
        if (b == null) {
            xl0.e().c(SystemJobService.e, "WorkSpec id not found!");
            return false;
        }
        Object b2 = this.b;
        synchronized (b2) {
            if (this.b.containsKey(b)) {
                final xl0 e = xl0.e();
                final String e2 = SystemJobService.e;
                final StringBuilder sb = new StringBuilder();
                sb.append("Job is already being executed by SystemJobService: ");
                sb.append(b);
                e.a(e2, sb.toString());
                return false;
            }
            final xl0 e3 = xl0.e();
            final String e4 = SystemJobService.e;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("onStartJob for ");
            sb2.append(b);
            e3.a(e4, sb2.toString());
            this.b.put(b, jobParameters);
            monitorexit(b2);
            final int sdk_INT = Build$VERSION.SDK_INT;
            b2 = new WorkerParameters.a();
            if (SystemJobService.a.b(jobParameters) != null) {
                ((WorkerParameters.a)b2).b = Arrays.asList(SystemJobService.a.b(jobParameters));
            }
            if (SystemJobService.a.a(jobParameters) != null) {
                ((WorkerParameters.a)b2).a = Arrays.asList(SystemJobService.a.a(jobParameters));
            }
            if (sdk_INT >= 28) {
                ((WorkerParameters.a)b2).c = SystemJobService.b.a(jobParameters);
            }
            this.d.a(this.c.d(b), (WorkerParameters.a)b2);
            return true;
        }
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        if (this.a == null) {
            xl0.e().a(SystemJobService.e, "WorkManager is not initialized; requesting retry.");
            return true;
        }
        final x82 b = b(jobParameters);
        if (b == null) {
            xl0.e().c(SystemJobService.e, "WorkSpec id not found!");
            return false;
        }
        final xl0 e = xl0.e();
        final String e2 = SystemJobService.e;
        final StringBuilder sb = new StringBuilder();
        sb.append("onStopJob for ");
        sb.append(b);
        e.a(e2, sb.toString());
        Object o = this.b;
        synchronized (o) {
            this.b.remove(b);
            monitorexit(o);
            o = this.c.b(b);
            if (o != null) {
                int a;
                if (Build$VERSION.SDK_INT >= 31) {
                    a = SystemJobService.c.a(jobParameters);
                }
                else {
                    a = -512;
                }
                this.d.c((op1)o, a);
            }
            return this.a.l().j(b.b()) ^ true;
        }
    }
    
    public abstract static class a
    {
        public static String[] a(final JobParameters jobParameters) {
            return jobParameters.getTriggeredContentAuthorities();
        }
        
        public static Uri[] b(final JobParameters jobParameters) {
            return jobParameters.getTriggeredContentUris();
        }
    }
    
    public abstract static class b
    {
        public static Network a(final JobParameters jobParameters) {
            return jobParameters.getNetwork();
        }
    }
    
    public abstract static class c
    {
        public static int a(final JobParameters jobParameters) {
            return SystemJobService.a(jobParameters.getStopReason());
        }
    }
}

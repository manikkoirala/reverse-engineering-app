// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.ExistingWorkPolicy;
import kotlin.jvm.internal.Lambda;

final class WorkerUpdater$enqueueUniquelyNamedPeriodic$enqueueNew$1 extends Lambda implements a90
{
    final String $name;
    final x11 $operation;
    final c92 $this_enqueueUniquelyNamedPeriodic;
    final n92 $workRequest;
    
    public WorkerUpdater$enqueueUniquelyNamedPeriodic$enqueueNew$1(final n92 $workRequest, final c92 $this_enqueueUniquelyNamedPeriodic, final String $name, final x11 $operation) {
        this.$workRequest = $workRequest;
        this.$this_enqueueUniquelyNamedPeriodic = $this_enqueueUniquelyNamedPeriodic;
        this.$name = $name;
        this.$operation = $operation;
        super(0);
    }
    
    public final void invoke() {
        new fx(new j82(this.$this_enqueueUniquelyNamedPeriodic, this.$name, ExistingWorkPolicy.KEEP, mh.e((Object)this.$workRequest)), this.$operation).run();
    }
}

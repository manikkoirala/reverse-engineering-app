// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.concurrent.Executor;
import java.util.List;
import androidx.work.a;
import android.content.Context;

public abstract class WorkManagerImplExtKt
{
    public static final List b(final Context context, final a a, final hu1 hu1, final WorkDatabase workDatabase, final ky1 ky1, final q81 q81) {
        final ij1 c = nj1.c(context, workDatabase, a);
        fg0.d((Object)c, "createBestAvailableBackg\u2026kDatabase, configuration)");
        return nh.j((Object[])new ij1[] { c, new qb0(context, a, ky1, q81, new b92(q81, hu1), hu1) });
    }
    
    public static final c92 c(final Context context, final a a) {
        fg0.e((Object)context, "context");
        fg0.e((Object)a, "configuration");
        return e(context, a, null, null, null, null, null, 124, null);
    }
    
    public static final c92 d(final Context context, final a a, final hu1 hu1, final WorkDatabase workDatabase, final ky1 ky1, final q81 q81, final x90 x90) {
        fg0.e((Object)context, "context");
        fg0.e((Object)a, "configuration");
        fg0.e((Object)hu1, "workTaskExecutor");
        fg0.e((Object)workDatabase, "workDatabase");
        fg0.e((Object)ky1, "trackers");
        fg0.e((Object)q81, "processor");
        fg0.e((Object)x90, "schedulersCreator");
        return new c92(context.getApplicationContext(), a, hu1, workDatabase, (List)x90.invoke((Object)context, (Object)a, (Object)hu1, (Object)workDatabase, (Object)ky1, (Object)q81), q81, ky1);
    }
    
    public static /* synthetic */ c92 e(final Context context, final a a, hu1 hu1, WorkDatabase b, ky1 ky1, q81 q81, x90 instance, final int n, final Object o) {
        if ((n & 0x4) != 0x0) {
            hu1 = new d92(a.m());
        }
        if ((n & 0x8) != 0x0) {
            final WorkDatabase.a p9 = WorkDatabase.p;
            final Context applicationContext = context.getApplicationContext();
            fg0.d((Object)applicationContext, "context.applicationContext");
            final jl1 d = hu1.d();
            fg0.d((Object)d, "workTaskExecutor.serialTaskExecutor");
            b = p9.b(applicationContext, d, a.a(), context.getResources().getBoolean(ta1.a));
        }
        if ((n & 0x10) != 0x0) {
            final Context applicationContext2 = context.getApplicationContext();
            fg0.d((Object)applicationContext2, "context.applicationContext");
            ky1 = new ky1(applicationContext2, hu1, null, null, null, null, 60, null);
        }
        if ((n & 0x20) != 0x0) {
            q81 = new q81(context.getApplicationContext(), a, hu1, b);
        }
        if ((n & 0x40) != 0x0) {
            instance = (x90)WorkManagerImplExtKt$WorkManagerImpl.WorkManagerImplExtKt$WorkManagerImpl$1.INSTANCE;
        }
        return d(context, a, hu1, b, ky1, q81, instance);
    }
}

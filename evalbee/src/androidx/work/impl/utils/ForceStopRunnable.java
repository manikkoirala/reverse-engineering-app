// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.content.BroadcastReceiver;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteTableLockedException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteAccessPermException;
import androidx.work.a;
import android.text.TextUtils;
import android.app.ApplicationExitInfo;
import android.app.ActivityManager;
import java.util.Iterator;
import java.util.List;
import androidx.work.impl.WorkDatabase;
import androidx.work.WorkInfo$State;
import android.os.Build$VERSION;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import android.content.Context;

public class ForceStopRunnable implements Runnable
{
    public static final String e;
    public static final long f;
    public final Context a;
    public final c92 b;
    public final r71 c;
    public int d;
    
    static {
        e = xl0.i("ForceStopRunnable");
        f = TimeUnit.DAYS.toMillis(3650L);
    }
    
    public ForceStopRunnable(final Context context, final c92 b) {
        this.a = context.getApplicationContext();
        this.b = b;
        this.c = b.k();
        this.d = 0;
    }
    
    public static Intent c(final Context context) {
        final Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, (Class)BroadcastReceiver.class));
        intent.setAction("ACTION_FORCE_STOP_RESCHEDULE");
        return intent;
    }
    
    public static PendingIntent d(final Context context, final int n) {
        return PendingIntent.getBroadcast(context, -1, c(context), n);
    }
    
    public static void g(final Context context) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        int n;
        if (Build$VERSION.SDK_INT >= 31) {
            n = 167772160;
        }
        else {
            n = 134217728;
        }
        final PendingIntent d = d(context, n);
        final long currentTimeMillis = System.currentTimeMillis();
        final long f = ForceStopRunnable.f;
        if (alarmManager != null) {
            alarmManager.setExact(0, currentTimeMillis + f, d);
        }
    }
    
    public boolean a() {
        final boolean i = pt1.i(this.a, this.b.o());
        final WorkDatabase o = this.b.o();
        final q92 k = o.K();
        final j92 j = o.J();
        o.e();
        try {
            final List z = k.z();
            final boolean b = true;
            final boolean b2 = z != null && !z.isEmpty();
            if (b2) {
                for (final p92 p92 : z) {
                    k.i(WorkInfo$State.ENQUEUED, p92.a);
                    k.b(p92.a, -512);
                    k.v(p92.a, -1L);
                }
            }
            j.c();
            o.D();
            o.i();
            boolean b3 = b;
            if (!b2) {
                b3 = (i && b);
            }
            return b3;
        }
        finally {
            o.i();
        }
    }
    
    public void b() {
        final boolean a = this.a();
        if (this.h()) {
            xl0.e().a(ForceStopRunnable.e, "Rescheduling Workers.");
            this.b.r();
            this.b.k().e(false);
        }
        else if (this.e()) {
            xl0.e().a(ForceStopRunnable.e, "Application was force-stopped, rescheduling.");
            this.b.r();
            this.c.d(this.b.h().a().currentTimeMillis());
        }
        else if (a) {
            xl0.e().a(ForceStopRunnable.e, "Found unfinished work, scheduling it.");
            nj1.h(this.b.h(), this.b.o(), this.b.m());
        }
    }
    
    public boolean e() {
        Object o = null;
        try {
            final int sdk_INT = Build$VERSION.SDK_INT;
            int n;
            if (sdk_INT >= 31) {
                n = 570425344;
            }
            else {
                n = 536870912;
            }
            o = d(this.a, n);
            if (sdk_INT >= 30) {
                if (o != null) {
                    ((PendingIntent)o).cancel();
                }
                final List a = l70.a((ActivityManager)this.a.getSystemService("activity"), (String)null, 0, 0);
                if (a != null && !a.isEmpty()) {
                    final long a2 = this.c.a();
                    for (int i = 0; i < a.size(); ++i) {
                        o = m70.a(a.get(i));
                        if (n70.a((ApplicationExitInfo)o) == 10 && o70.a((ApplicationExitInfo)o) >= a2) {
                            return true;
                        }
                    }
                }
            }
            else if (o == null) {
                g(this.a);
                return true;
            }
            return false;
        }
        catch (final IllegalArgumentException o) {}
        catch (final SecurityException ex) {}
        xl0.e().l(ForceStopRunnable.e, "Ignoring exception", (Throwable)o);
        return true;
    }
    
    public boolean f() {
        final a h = this.b.h();
        if (TextUtils.isEmpty((CharSequence)h.c())) {
            xl0.e().a(ForceStopRunnable.e, "The default process name was not specified.");
            return true;
        }
        final boolean b = m81.b(this.a, h);
        final xl0 e = xl0.e();
        final String e2 = ForceStopRunnable.e;
        final StringBuilder sb = new StringBuilder();
        sb.append("Is default app process = ");
        sb.append(b);
        e.a(e2, sb.toString());
        return b;
    }
    
    public boolean h() {
        return this.b.k().b();
    }
    
    public void i(final long n) {
        try {
            Thread.sleep(n);
        }
        catch (final InterruptedException ex) {}
    }
    
    @Override
    public void run() {
        try {
            if (!this.f()) {
                return;
            }
            Label_0017: {
                break Label_0017;
                while (true) {
                    try {
                        final SQLiteAccessPermException cause;
                        while (true) {
                            m82.d(this.a);
                            xl0.e().a(ForceStopRunnable.e, "Performing cleanup operations.");
                            try {
                                this.b();
                                return;
                            }
                            catch (final SQLiteAccessPermException cause) {}
                            catch (final SQLiteConstraintException cause) {}
                            catch (final SQLiteTableLockedException cause) {}
                            catch (final SQLiteDatabaseLockedException cause) {}
                            catch (final SQLiteDatabaseCorruptException cause) {}
                            catch (final SQLiteDiskIOException cause) {}
                            catch (final SQLiteCantOpenDatabaseException ex) {}
                            final int d = this.d + 1;
                            this.d = d;
                            if (d >= 3) {
                                break;
                            }
                            final long n = d;
                            final xl0 e = xl0.e();
                            final String e2 = ForceStopRunnable.e;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Retrying after ");
                            sb.append(n * 300L);
                            e.b(e2, sb.toString(), (Throwable)cause);
                            this.i(this.d * 300L);
                        }
                        String message;
                        if (d22.a(this.a)) {
                            message = "The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.";
                        }
                        else {
                            message = "WorkManager can't be accessed from direct boot, because credential encrypted storage isn't accessible.\nDon't access or initialise WorkManager from directAware components. See https://developer.android.com/training/articles/direct-boot";
                        }
                        final xl0 e3 = xl0.e();
                        final String e4 = ForceStopRunnable.e;
                        e3.d(e4, message, (Throwable)cause);
                        final IllegalStateException ex2 = new IllegalStateException(message, (Throwable)cause);
                        final dl dl = this.b.h().e();
                        if (dl == null) {
                            throw ex2;
                        }
                        xl0.e().b(e4, "Routing exception to the specified exception handler", ex2);
                        final IllegalStateException ex3 = ex2;
                        dl.accept(ex3);
                    }
                    catch (final SQLiteException cause2) {
                        xl0.e().c(ForceStopRunnable.e, "Unexpected SQLite exception during migrations");
                        final IllegalStateException ex3 = new IllegalStateException("Unexpected SQLite exception during migrations", (Throwable)cause2);
                        final dl dl = this.b.h().e();
                        if (dl != null) {
                            continue;
                        }
                        throw ex3;
                    }
                    break;
                }
            }
        }
        finally {
            this.b.q();
        }
    }
    
    public static class BroadcastReceiver extends android.content.BroadcastReceiver
    {
        public static final String a;
        
        static {
            a = xl0.i("ForceStopRunnable$Rcvr");
        }
        
        public void onReceive(final Context context, final Intent intent) {
            if (intent != null && "ACTION_FORCE_STOP_RESCHEDULE".equals(intent.getAction())) {
                xl0.e().j(BroadcastReceiver.a, "Rescheduling alarm that keeps track of force-stops.");
                ForceStopRunnable.g(context);
            }
        }
    }
}

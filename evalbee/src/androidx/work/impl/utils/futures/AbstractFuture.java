// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils.futures;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeoutException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Executor;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.logging.Logger;

public abstract class AbstractFuture implements ik0
{
    public static final boolean d;
    public static final Logger e;
    public static final b f;
    public static final Object g;
    public volatile Object a;
    public volatile d b;
    public volatile h c;
    
    static {
        d = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
        e = Logger.getLogger(AbstractFuture.class.getName());
        g f2;
        try {
            final e e2 = new e(AtomicReferenceFieldUpdater.newUpdater(h.class, Thread.class, "a"), AtomicReferenceFieldUpdater.newUpdater(h.class, h.class, "b"), AtomicReferenceFieldUpdater.newUpdater(AbstractFuture.class, h.class, "c"), AtomicReferenceFieldUpdater.newUpdater(AbstractFuture.class, d.class, "b"), AtomicReferenceFieldUpdater.newUpdater(AbstractFuture.class, Object.class, "a"));
        }
        finally {
            f2 = new g();
        }
        f = (b)f2;
        final Throwable thrown;
        if (thrown != null) {
            AbstractFuture.e.log(Level.SEVERE, "SafeAtomicHelper is broken!", thrown);
        }
        g = new Object();
    }
    
    public static CancellationException c(final String message, final Throwable cause) {
        final CancellationException ex = new CancellationException(message);
        ex.initCause(cause);
        return ex;
    }
    
    public static Object d(final Object o) {
        o.getClass();
        return o;
    }
    
    public static void f(AbstractFuture abstractFuture) {
        d d = null;
    Label_0002:
        while (true) {
            abstractFuture.m();
            abstractFuture.b();
            d c;
            for (d e = abstractFuture.e(d); e != null; e = c) {
                c = e.c;
                final Runnable a = e.a;
                if (a instanceof f) {
                    final f f = (f)a;
                    final AbstractFuture a2 = f.a;
                    if (a2.a == f && AbstractFuture.f.b(a2, f, i(f.b))) {
                        d = c;
                        abstractFuture = a2;
                        continue Label_0002;
                    }
                }
                else {
                    g(a, e.b);
                }
            }
            break;
        }
    }
    
    public static void g(final Runnable obj, final Executor obj2) {
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException thrown) {
            final Logger e = AbstractFuture.e;
            final Level severe = Level.SEVERE;
            final StringBuilder sb = new StringBuilder();
            sb.append("RuntimeException while executing runnable ");
            sb.append(obj);
            sb.append(" with executor ");
            sb.append(obj2);
            e.log(severe, sb.toString(), thrown);
        }
    }
    
    public static Object i(final ik0 obj) {
        if (obj instanceof AbstractFuture) {
            Object o2;
            final Object o = o2 = ((AbstractFuture)obj).a;
            if (o instanceof c) {
                final c c = (c)o;
                o2 = o;
                if (c.a) {
                    if (c.b != null) {
                        o2 = new c(false, c.b);
                    }
                    else {
                        o2 = AbstractFuture.c.d;
                    }
                }
            }
            return o2;
        }
        final boolean cancelled = obj.isCancelled();
        if ((AbstractFuture.d ^ true) & cancelled) {
            return c.d;
        }
        try {
            Object o3;
            if ((o3 = j(obj)) == null) {
                o3 = AbstractFuture.g;
            }
            return o3;
        }
        catch (final CancellationException cause) {
            if (!cancelled) {
                final StringBuilder sb = new StringBuilder();
                sb.append("get() threw CancellationException, despite reporting isCancelled() == false: ");
                sb.append(obj);
                return new Failure(new IllegalArgumentException(sb.toString(), cause));
            }
            return new c(false, cause);
        }
        catch (final ExecutionException ex) {
            return new Failure(ex.getCause());
        }
        finally {
            final Throwable t;
            return new Failure(t);
        }
    }
    
    public static Object j(final Future future) {
        boolean b = false;
        try {
            return future.get();
        }
        catch (final InterruptedException ex) {
            b = true;
            return future.get();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    public final void a(final StringBuilder sb) {
        String str = "]";
        try {
            final Object j = j(this);
            sb.append("SUCCESS, result=[");
            sb.append(this.r(j));
            sb.append("]");
            return;
        }
        catch (final RuntimeException ex) {
            sb.append("UNKNOWN, cause=[");
            sb.append(ex.getClass());
            str = " thrown from get()]";
        }
        catch (final CancellationException ex2) {
            str = "CANCELLED";
        }
        catch (final ExecutionException ex3) {
            sb.append("FAILURE, cause=[");
            sb.append(ex3.getCause());
        }
        sb.append(str);
    }
    
    @Override
    public final void addListener(final Runnable runnable, final Executor executor) {
        d(runnable);
        d(executor);
        d c = this.b;
        if (c != AbstractFuture.d.d) {
            final d d = new d(runnable, executor);
            do {
                d.c = c;
                if (AbstractFuture.f.a(this, c, d)) {
                    return;
                }
            } while ((c = this.b) != AbstractFuture.d.d);
        }
        g(runnable, executor);
    }
    
    public void b() {
    }
    
    @Override
    public final boolean cancel(final boolean b) {
        Object o = this.a;
        final boolean b2 = true;
        boolean b3;
        if (o == null | o instanceof f) {
            c c;
            if (AbstractFuture.d) {
                c = new c(b, new CancellationException("Future.cancel() was called."));
            }
            else if (b) {
                c = AbstractFuture.c.c;
            }
            else {
                c = AbstractFuture.c.d;
            }
            AbstractFuture abstractFuture = this;
            b3 = false;
            while (true) {
                if (AbstractFuture.f.b(abstractFuture, o, c)) {
                    if (b) {
                        abstractFuture.k();
                    }
                    f(abstractFuture);
                    b3 = b2;
                    if (!(o instanceof f)) {
                        break;
                    }
                    final ik0 b4 = ((f)o).b;
                    if (!(b4 instanceof AbstractFuture)) {
                        b4.cancel(b);
                        b3 = b2;
                        break;
                    }
                    abstractFuture = (AbstractFuture)b4;
                    o = abstractFuture.a;
                    final boolean b5 = o == null;
                    b3 = b2;
                    if (!(b5 | o instanceof f)) {
                        break;
                    }
                    b3 = true;
                }
                else {
                    if (!((o = abstractFuture.a) instanceof f)) {
                        break;
                    }
                    continue;
                }
            }
        }
        else {
            b3 = false;
        }
        return b3;
    }
    
    public final d e(d d) {
        d b;
        do {
            b = this.b;
        } while (!AbstractFuture.f.a(this, b, AbstractFuture.d.d));
        d c = d;
        d c2;
        for (d = b; d != null; d = c2) {
            c2 = d.c;
            d.c = c;
            c = d;
        }
        return c;
    }
    
    @Override
    public final Object get() {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object a = this.a;
        if (a != null & (a instanceof f ^ true)) {
            return this.h(a);
        }
        h h = this.c;
        if (h != AbstractFuture.h.c) {
            final h h2 = new h();
            do {
                h2.a(h);
                if (AbstractFuture.f.c(this, h, h2)) {
                    Object a2;
                    do {
                        LockSupport.park(this);
                        if (Thread.interrupted()) {
                            this.n(h2);
                            throw new InterruptedException();
                        }
                        a2 = this.a;
                    } while (!(a2 != null & (a2 instanceof f ^ true)));
                    return this.h(a2);
                }
            } while ((h = this.c) != AbstractFuture.h.c);
        }
        return this.h(this.a);
    }
    
    @Override
    public final Object get(long convert, final TimeUnit timeUnit) {
        long nanos = timeUnit.toNanos(convert);
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object a = this.a;
        if (a != null & (a instanceof f ^ true)) {
            return this.h(a);
        }
        long n;
        if (nanos > 0L) {
            n = System.nanoTime() + nanos;
        }
        else {
            n = 0L;
        }
        long n2 = nanos;
        Label_0254: {
            if (nanos >= 1000L) {
                h h = this.c;
                if (h != AbstractFuture.h.c) {
                    final h h2 = new h();
                    do {
                        h2.a(h);
                        if (AbstractFuture.f.c(this, h, h2)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (Thread.interrupted()) {
                                    this.n(h2);
                                    throw new InterruptedException();
                                }
                                final Object a2 = this.a;
                                if (a2 != null & (a2 instanceof f ^ true)) {
                                    return this.h(a2);
                                }
                                n2 = (nanos = n - System.nanoTime());
                            } while (n2 >= 1000L);
                            this.n(h2);
                            break Label_0254;
                        }
                    } while ((h = this.c) != AbstractFuture.h.c);
                }
                return this.h(this.a);
            }
        }
        while (n2 > 0L) {
            final Object a3 = this.a;
            if (a3 != null & (a3 instanceof f ^ true)) {
                return this.h(a3);
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            n2 = n - System.nanoTime();
        }
        final String string = this.toString();
        final String string2 = timeUnit.toString();
        final Locale root = Locale.ROOT;
        final String lowerCase = string2.toLowerCase(root);
        final StringBuilder sb = new StringBuilder();
        sb.append("Waited ");
        sb.append(convert);
        sb.append(" ");
        sb.append(timeUnit.toString().toLowerCase(root));
        String s;
        final String str = s = sb.toString();
        if (n2 + 1000L < 0L) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(" (plus ");
            final String string3 = sb2.toString();
            final long sourceDuration = -n2;
            convert = timeUnit.convert(sourceDuration, TimeUnit.NANOSECONDS);
            final long lng = sourceDuration - timeUnit.toNanos(convert);
            final long n3 = lcmp(convert, 0L);
            final boolean b = n3 == 0 || lng > 1000L;
            String string4 = string3;
            if (n3 > 0) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string3);
                sb3.append(convert);
                sb3.append(" ");
                sb3.append(lowerCase);
                String s2 = sb3.toString();
                if (b) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(s2);
                    sb4.append(",");
                    s2 = sb4.toString();
                }
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(s2);
                sb5.append(" ");
                string4 = sb5.toString();
            }
            String string5 = string4;
            if (b) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string4);
                sb6.append(lng);
                sb6.append(" nanoseconds ");
                string5 = sb6.toString();
            }
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string5);
            sb7.append("delay)");
            s = sb7.toString();
        }
        if (this.isDone()) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(s);
            sb8.append(" but future completed as timeout expired");
            throw new TimeoutException(sb8.toString());
        }
        final StringBuilder sb9 = new StringBuilder();
        sb9.append(s);
        sb9.append(" for ");
        sb9.append(string);
        throw new TimeoutException(sb9.toString());
    }
    
    public final Object h(final Object o) {
        if (o instanceof c) {
            throw c("Task was cancelled.", ((c)o).b);
        }
        if (!(o instanceof Failure)) {
            Object o2;
            if ((o2 = o) == AbstractFuture.g) {
                o2 = null;
            }
            return o2;
        }
        throw new ExecutionException(((Failure)o).a);
    }
    
    @Override
    public final boolean isCancelled() {
        return this.a instanceof c;
    }
    
    @Override
    public final boolean isDone() {
        final Object a = this.a;
        return (a instanceof f ^ true) & a != null;
    }
    
    public void k() {
    }
    
    public String l() {
        final Object a = this.a;
        if (a instanceof f) {
            final StringBuilder sb = new StringBuilder();
            sb.append("setFuture=[");
            sb.append(this.r(((f)a).b));
            sb.append("]");
            return sb.toString();
        }
        if (this instanceof ScheduledFuture) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("remaining delay=[");
            sb2.append(((ScheduledFuture)this).getDelay(TimeUnit.MILLISECONDS));
            sb2.append(" ms]");
            return sb2.toString();
        }
        return null;
    }
    
    public final void m() {
        h h;
        do {
            h = this.c;
        } while (!AbstractFuture.f.c(this, h, AbstractFuture.h.c));
        while (h != null) {
            h.b();
            h = h.b;
        }
    }
    
    public final void n(h c) {
        c.a = null;
    Label_0005:
        while (true) {
            c = this.c;
            if (c == h.c) {
                return;
            }
            h h = null;
            while (c != null) {
                final h b = c.b;
                h h2;
                if (c.a != null) {
                    h2 = c;
                }
                else if (h != null) {
                    h.b = b;
                    h2 = h;
                    if (h.a == null) {
                        continue Label_0005;
                    }
                }
                else {
                    h2 = h;
                    if (!AbstractFuture.f.c(this, c, b)) {
                        continue Label_0005;
                    }
                }
                c = b;
                h = h2;
            }
        }
    }
    
    public boolean o(final Object o) {
        Object g = o;
        if (o == null) {
            g = AbstractFuture.g;
        }
        if (AbstractFuture.f.b(this, null, g)) {
            f(this);
            return true;
        }
        return false;
    }
    
    public boolean p(final Throwable t) {
        if (AbstractFuture.f.b(this, null, new Failure((Throwable)d(t)))) {
            f(this);
            return true;
        }
        return false;
    }
    
    public boolean q(final ik0 ik0) {
        d(ik0);
        Object o;
        if ((o = this.a) == null) {
            if (ik0.isDone()) {
                if (AbstractFuture.f.b(this, null, i(ik0))) {
                    f(this);
                    return true;
                }
                return false;
            }
            else {
                final f f = new f(this, ik0);
                if (AbstractFuture.f.b(this, null, f)) {
                    try {
                        ik0.addListener(f, DirectExecutor.INSTANCE);
                    }
                    finally {
                        Failure b = null;
                        try {
                            final Throwable t;
                            final Failure failure = new Failure(t);
                        }
                        finally {
                            b = Failure.b;
                        }
                        AbstractFuture.f.b(this, f, b);
                    }
                    return true;
                }
                o = this.a;
            }
        }
        if (o instanceof c) {
            ik0.cancel(((c)o).a);
        }
        return false;
    }
    
    public final String r(final Object obj) {
        if (obj == this) {
            return "this future";
        }
        return String.valueOf(obj);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        Label_0150: {
            String str2 = null;
            Label_0035: {
                if (!this.isCancelled()) {
                    if (!this.isDone()) {
                        String str;
                        try {
                            str = this.l();
                        }
                        catch (final RuntimeException ex) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Exception thrown from implementation: ");
                            sb2.append(ex.getClass());
                            str = sb2.toString();
                        }
                        if (str != null && !str.isEmpty()) {
                            sb.append("PENDING, info=[");
                            sb.append(str);
                            sb.append("]");
                            break Label_0150;
                        }
                        if (!this.isDone()) {
                            str2 = "PENDING";
                            break Label_0035;
                        }
                    }
                    this.a(sb);
                    break Label_0150;
                }
                str2 = "CANCELLED";
            }
            sb.append(str2);
        }
        sb.append("]");
        return sb.toString();
    }
    
    public static final class Failure
    {
        public static final Failure b;
        public final Throwable a;
        
        static {
            b = new Failure(new Throwable("Failure occurred while trying to finish a future.") {
                @Override
                public Throwable fillInStackTrace() {
                    monitorenter(this);
                    monitorexit(this);
                    return this;
                }
            });
        }
        
        public Failure(final Throwable t) {
            this.a = (Throwable)AbstractFuture.d(t);
        }
    }
    
    public abstract static class b
    {
        public abstract boolean a(final AbstractFuture p0, final d p1, final d p2);
        
        public abstract boolean b(final AbstractFuture p0, final Object p1, final Object p2);
        
        public abstract boolean c(final AbstractFuture p0, final h p1, final h p2);
        
        public abstract void d(final h p0, final h p1);
        
        public abstract void e(final h p0, final Thread p1);
    }
    
    public static final class c
    {
        public static final c c;
        public static final c d;
        public final boolean a;
        public final Throwable b;
        
        static {
            if (AbstractFuture.d) {
                d = null;
                c = null;
            }
            else {
                d = new c(false, null);
                c = new c(true, null);
            }
        }
        
        public c(final boolean a, final Throwable b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class d
    {
        public static final d d;
        public final Runnable a;
        public final Executor b;
        public d c;
        
        static {
            d = new d(null, null);
        }
        
        public d(final Runnable a, final Executor b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class e extends b
    {
        public final AtomicReferenceFieldUpdater a;
        public final AtomicReferenceFieldUpdater b;
        public final AtomicReferenceFieldUpdater c;
        public final AtomicReferenceFieldUpdater d;
        public final AtomicReferenceFieldUpdater e;
        
        public e(final AtomicReferenceFieldUpdater a, final AtomicReferenceFieldUpdater b, final AtomicReferenceFieldUpdater c, final AtomicReferenceFieldUpdater d, final AtomicReferenceFieldUpdater e) {
            super(null);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        @Override
        public boolean a(final AbstractFuture abstractFuture, final d d, final d d2) {
            return z.a(this.d, abstractFuture, d, d2);
        }
        
        @Override
        public boolean b(final AbstractFuture abstractFuture, final Object o, final Object o2) {
            return z.a(this.e, abstractFuture, o, o2);
        }
        
        @Override
        public boolean c(final AbstractFuture abstractFuture, final h h, final h h2) {
            return z.a(this.c, abstractFuture, h, h2);
        }
        
        @Override
        public void d(final h h, final h h2) {
            this.b.lazySet(h, h2);
        }
        
        @Override
        public void e(final h h, final Thread thread) {
            this.a.lazySet(h, thread);
        }
    }
    
    public static final class f implements Runnable
    {
        public final AbstractFuture a;
        public final ik0 b;
        
        public f(final AbstractFuture a, final ik0 b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            if (this.a.a != this) {
                return;
            }
            if (AbstractFuture.f.b(this.a, this, AbstractFuture.i(this.b))) {
                AbstractFuture.f(this.a);
            }
        }
    }
    
    public static final class g extends b
    {
        public g() {
            super(null);
        }
        
        @Override
        public boolean a(final AbstractFuture abstractFuture, final d d, final d b) {
            synchronized (abstractFuture) {
                if (abstractFuture.b == d) {
                    abstractFuture.b = b;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public boolean b(final AbstractFuture abstractFuture, final Object o, final Object a) {
            synchronized (abstractFuture) {
                if (abstractFuture.a == o) {
                    abstractFuture.a = a;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public boolean c(final AbstractFuture abstractFuture, final h h, final h c) {
            synchronized (abstractFuture) {
                if (abstractFuture.c == h) {
                    abstractFuture.c = c;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public void d(final h h, final h b) {
            h.b = b;
        }
        
        @Override
        public void e(final h h, final Thread a) {
            h.a = a;
        }
    }
    
    public static final class h
    {
        public static final h c;
        public volatile Thread a;
        public volatile h b;
        
        static {
            c = new h(false);
        }
        
        public h() {
            AbstractFuture.f.e(this, Thread.currentThread());
        }
        
        public h(final boolean b) {
        }
        
        public void a(final h h) {
            AbstractFuture.f.d(this, h);
        }
        
        public void b() {
            final Thread a = this.a;
            if (a != null) {
                this.a = null;
                LockSupport.unpark(a);
            }
        }
    }
}

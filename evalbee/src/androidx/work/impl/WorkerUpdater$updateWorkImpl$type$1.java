// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Lambda;

final class WorkerUpdater$updateWorkImpl$type$1 extends Lambda implements c90
{
    public static final WorkerUpdater$updateWorkImpl$type$1 INSTANCE;
    
    static {
        INSTANCE = new WorkerUpdater$updateWorkImpl$type$1();
    }
    
    public WorkerUpdater$updateWorkImpl$type$1() {
        super(1);
    }
    
    @NotNull
    public final String invoke(@NotNull final p92 p) {
        fg0.e((Object)p, "spec");
        String s;
        if (p.m()) {
            s = "Periodic";
        }
        else {
            s = "OneTime";
        }
        return s;
    }
}

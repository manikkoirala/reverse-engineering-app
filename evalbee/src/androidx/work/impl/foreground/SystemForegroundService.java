// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import android.content.Context;
import android.app.ForegroundServiceStartNotAllowedException;
import android.content.Intent;
import android.os.Looper;
import android.app.Service;
import android.os.Build$VERSION;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Handler;

public class SystemForegroundService extends rj0 implements b
{
    public static final String f;
    public static SystemForegroundService g;
    public Handler b;
    public boolean c;
    public a d;
    public NotificationManager e;
    
    static {
        f = xl0.i("SystemFgService");
        SystemForegroundService.g = null;
    }
    
    public static /* synthetic */ String e() {
        return SystemForegroundService.f;
    }
    
    @Override
    public void a(final int n, final Notification notification) {
        this.b.post((Runnable)new Runnable(this, n, notification) {
            public final int a;
            public final Notification b;
            public final SystemForegroundService c;
            
            @Override
            public void run() {
                this.c.e.notify(this.a, this.b);
            }
        });
    }
    
    @Override
    public void c(final int n, final int n2, final Notification notification) {
        this.b.post((Runnable)new Runnable(this, n, notification, n2) {
            public final int a;
            public final Notification b;
            public final int c;
            public final SystemForegroundService d;
            
            @Override
            public void run() {
                final int sdk_INT = Build$VERSION.SDK_INT;
                if (sdk_INT >= 31) {
                    SystemForegroundService.e.a(this.d, this.a, this.b, this.c);
                }
                else if (sdk_INT >= 29) {
                    SystemForegroundService.d.a(this.d, this.a, this.b, this.c);
                }
                else {
                    this.d.startForeground(this.a, this.b);
                }
            }
        });
    }
    
    @Override
    public void d(final int n) {
        this.b.post((Runnable)new Runnable(this, n) {
            public final int a;
            public final SystemForegroundService b;
            
            @Override
            public void run() {
                this.b.e.cancel(this.a);
            }
        });
    }
    
    public final void f() {
        this.b = new Handler(Looper.getMainLooper());
        this.e = (NotificationManager)((Context)this).getApplicationContext().getSystemService("notification");
        (this.d = new a(((Context)this).getApplicationContext())).n((a.b)this);
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        (SystemForegroundService.g = this).f();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.d.l();
    }
    
    @Override
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.c) {
            xl0.e().f(SystemForegroundService.f, "Re-initializing SystemForegroundService after a request to shut-down.");
            this.d.l();
            this.f();
            this.c = false;
        }
        if (intent != null) {
            this.d.m(intent);
        }
        return 3;
    }
    
    @Override
    public void stop() {
        this.c = true;
        xl0.e().a(SystemForegroundService.f, "All commands completed.");
        if (Build$VERSION.SDK_INT >= 26) {
            this.stopForeground(true);
        }
        SystemForegroundService.g = null;
        this.stopSelf();
    }
    
    public abstract static class d
    {
        public static void a(final Service service, final int n, final Notification notification, final int n2) {
            service.startForeground(n, notification, n2);
        }
    }
    
    public abstract static class e
    {
        public static void a(final Service service, final int n, final Notification notification, final int n2) {
            try {
                service.startForeground(n, notification, n2);
            }
            catch (final ForegroundServiceStartNotAllowedException ex) {
                xl0.e().l(SystemForegroundService.e(), "Unable to start foreground service", ex);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import androidx.work.impl.constraints.WorkConstraintsTrackerKt;
import android.os.Build$VERSION;
import android.app.Notification;
import java.util.UUID;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n;
import android.os.Parcelable;
import android.content.Intent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import java.util.Map;
import android.content.Context;

public class a implements k11, qy
{
    public static final String k;
    public Context a;
    public c92 b;
    public final hu1 c;
    public final Object d;
    public x82 e;
    public final Map f;
    public final Map g;
    public final Map h;
    public final WorkConstraintsTracker i;
    public b j;
    
    static {
        k = xl0.i("SystemFgDispatcher");
    }
    
    public a(final Context a) {
        this.a = a;
        this.d = new Object();
        final c92 j = c92.j(a);
        this.b = j;
        this.c = j.p();
        this.e = null;
        this.f = new LinkedHashMap();
        this.h = new HashMap();
        this.g = new HashMap();
        this.i = new WorkConstraintsTracker(this.b.n());
        this.b.l().e(this);
    }
    
    public static /* synthetic */ c92 a(final a a) {
        return a.b;
    }
    
    public static /* synthetic */ hu1 b(final a a) {
        return a.c;
    }
    
    public static Intent e(final Context context, final x82 x82, final p70 p3) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_NOTIFY");
        intent.putExtra("KEY_NOTIFICATION_ID", p3.c());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", p3.a());
        intent.putExtra("KEY_NOTIFICATION", (Parcelable)p3.b());
        intent.putExtra("KEY_WORKSPEC_ID", x82.b());
        intent.putExtra("KEY_GENERATION", x82.a());
        return intent;
    }
    
    public static Intent f(final Context context, final x82 x82, final p70 p3) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_START_FOREGROUND");
        intent.putExtra("KEY_WORKSPEC_ID", x82.b());
        intent.putExtra("KEY_GENERATION", x82.a());
        intent.putExtra("KEY_NOTIFICATION_ID", p3.c());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", p3.a());
        intent.putExtra("KEY_NOTIFICATION", (Parcelable)p3.b());
        return intent;
    }
    
    public static Intent g(final Context context) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_STOP_FOREGROUND");
        return intent;
    }
    
    @Override
    public void c(final p92 p2, final androidx.work.impl.constraints.a a) {
        if (a instanceof androidx.work.impl.constraints.a.b) {
            final String a2 = p2.a;
            final xl0 e = xl0.e();
            final String k = a.k;
            final StringBuilder sb = new StringBuilder();
            sb.append("Constraints unmet for WorkSpec ");
            sb.append(a2);
            e.a(k, sb.toString());
            this.b.t(u92.a(p2));
        }
    }
    
    @Override
    public void d(final x82 obj, final boolean b) {
        Object o = this.d;
        synchronized (o) {
            n n;
            if (this.g.remove(obj) != null) {
                n = this.h.remove(obj);
            }
            else {
                n = null;
            }
            if (n != null) {
                n.b((CancellationException)null);
            }
            monitorexit(o);
            final p70 p2 = this.f.remove(obj);
            if (obj.equals(this.e)) {
                if (this.f.size() > 0) {
                    final Iterator iterator = this.f.entrySet().iterator();
                    do {
                        o = iterator.next();
                    } while (iterator.hasNext());
                    this.e = ((Map.Entry<x82, p70>)o).getKey();
                    if (this.j != null) {
                        o = ((Map.Entry<x82, p70>)o).getValue();
                        this.j.c(((p70)o).c(), ((p70)o).a(), ((p70)o).b());
                        this.j.d(((p70)o).c());
                    }
                }
                else {
                    this.e = null;
                }
            }
            o = this.j;
            if (p2 != null && o != null) {
                final xl0 e = xl0.e();
                final String k = androidx.work.impl.foreground.a.k;
                final StringBuilder sb = new StringBuilder();
                sb.append("Removing Notification (id: ");
                sb.append(p2.c());
                sb.append(", workSpecId: ");
                sb.append(obj);
                sb.append(", notificationType: ");
                sb.append(p2.a());
                e.a(k, sb.toString());
                ((b)o).d(p2.c());
            }
        }
    }
    
    public final void h(final Intent obj) {
        final xl0 e = xl0.e();
        final String k = androidx.work.impl.foreground.a.k;
        final StringBuilder sb = new StringBuilder();
        sb.append("Stopping foreground work for ");
        sb.append(obj);
        e.f(k, sb.toString());
        final String stringExtra = obj.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty((CharSequence)stringExtra)) {
            this.b.f(UUID.fromString(stringExtra));
        }
    }
    
    public final void i(final Intent intent) {
        int n = 0;
        final int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        final int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        final String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        final x82 e = new x82(stringExtra, intent.getIntExtra("KEY_GENERATION", 0));
        final Notification notification = (Notification)intent.getParcelableExtra("KEY_NOTIFICATION");
        final xl0 e2 = xl0.e();
        final String k = androidx.work.impl.foreground.a.k;
        final StringBuilder sb = new StringBuilder();
        sb.append("Notifying with (id:");
        sb.append(intExtra);
        sb.append(", workSpecId: ");
        sb.append(stringExtra);
        sb.append(", notificationType :");
        sb.append(intExtra2);
        sb.append(")");
        e2.a(k, sb.toString());
        if (notification != null && this.j != null) {
            this.f.put(e, new p70(intExtra, notification, intExtra2));
            if (this.e == null) {
                this.e = e;
                this.j.c(intExtra, intExtra2, notification);
            }
            else {
                this.j.a(intExtra, notification);
                if (intExtra2 != 0 && Build$VERSION.SDK_INT >= 29) {
                    final Iterator iterator = this.f.entrySet().iterator();
                    while (iterator.hasNext()) {
                        n |= ((Map.Entry<K, p70>)iterator.next()).getValue().a();
                    }
                    final p70 p = this.f.get(this.e);
                    if (p != null) {
                        this.j.c(p.c(), n, p.b());
                    }
                }
            }
        }
    }
    
    public final void j(final Intent obj) {
        final xl0 e = xl0.e();
        final String k = androidx.work.impl.foreground.a.k;
        final StringBuilder sb = new StringBuilder();
        sb.append("Started foreground service ");
        sb.append(obj);
        e.f(k, sb.toString());
        this.c.b(new Runnable(this, obj.getStringExtra("KEY_WORKSPEC_ID")) {
            public final String a;
            public final a b;
            
            @Override
            public void run() {
                final p92 g = androidx.work.impl.foreground.a.a(this.b).l().g(this.a);
                if (g != null && g.k()) {
                    synchronized (this.b.d) {
                        this.b.g.put(u92.a(g), g);
                        final a b = this.b;
                        this.b.h.put(u92.a(g), WorkConstraintsTrackerKt.b(b.i, g, androidx.work.impl.foreground.a.b(b).a(), this.b));
                    }
                }
            }
        });
    }
    
    public void k(final Intent intent) {
        xl0.e().f(androidx.work.impl.foreground.a.k, "Stopping foreground service");
        final b j = this.j;
        if (j != null) {
            j.stop();
        }
    }
    
    public void l() {
        this.j = null;
        synchronized (this.d) {
            final Iterator iterator = this.h.values().iterator();
            while (iterator.hasNext()) {
                ((n)iterator.next()).b((CancellationException)null);
            }
            monitorexit(this.d);
            this.b.l().p(this);
        }
    }
    
    public void m(final Intent intent) {
        final String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            this.j(intent);
        }
        else if (!"ACTION_NOTIFY".equals(action)) {
            if ("ACTION_CANCEL_WORK".equals(action)) {
                this.h(intent);
                return;
            }
            if ("ACTION_STOP_FOREGROUND".equals(action)) {
                this.k(intent);
            }
            return;
        }
        this.i(intent);
    }
    
    public void n(final b j) {
        if (this.j != null) {
            xl0.e().c(androidx.work.impl.foreground.a.k, "A callback already exists.");
            return;
        }
        this.j = j;
    }
    
    public interface b
    {
        void a(final int p0, final Notification p1);
        
        void c(final int p0, final int p1, final Notification p2);
        
        void d(final int p0);
        
        void stop();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.controllers;

public abstract class ConstraintController
{
    public final tk a;
    
    public ConstraintController(final tk a) {
        fg0.e((Object)a, "tracker");
        this.a = a;
    }
    
    public abstract int b();
    
    public abstract boolean c(final p92 p0);
    
    public final boolean d(final p92 p) {
        fg0.e((Object)p, "workSpec");
        return this.c(p) && this.e(this.a.e());
    }
    
    public abstract boolean e(final Object p0);
    
    public final y40 f() {
        return d50.c((q90)new ConstraintController$track.ConstraintController$track$1(this, (vl)null));
    }
}

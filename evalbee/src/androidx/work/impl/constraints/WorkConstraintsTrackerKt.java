// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.f;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.n;
import kotlinx.coroutines.CoroutineDispatcher;

public abstract class WorkConstraintsTrackerKt
{
    public static final String a;
    
    static {
        final String i = xl0.i("WorkConstraintsTracker");
        fg0.d((Object)i, "tagWithPrefix(\"WorkConstraintsTracker\")");
        a = i;
    }
    
    public static final n b(final WorkConstraintsTracker workConstraintsTracker, final p92 p4, final CoroutineDispatcher coroutineDispatcher, final k11 k11) {
        fg0.e((Object)workConstraintsTracker, "<this>");
        fg0.e((Object)p4, "spec");
        fg0.e((Object)coroutineDispatcher, "dispatcher");
        fg0.e((Object)k11, "listener");
        final pi b = ah0.b((n)null, 1, (Object)null);
        ad.d(f.a(((k)coroutineDispatcher).plus((CoroutineContext)b)), (CoroutineContext)null, (CoroutineStart)null, (q90)new WorkConstraintsTrackerKt$listen.WorkConstraintsTrackerKt$listen$1(workConstraintsTracker, p4, k11, (vl)null), 3, (Object)null);
        return (n)b;
    }
}

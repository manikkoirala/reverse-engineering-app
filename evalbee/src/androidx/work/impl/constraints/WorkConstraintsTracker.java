// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.work.impl.constraints.controllers.ConstraintController;
import java.util.List;

public final class WorkConstraintsTracker
{
    public final List a;
    
    public WorkConstraintsTracker(final List a) {
        fg0.e((Object)a, "controllers");
        this.a = a;
    }
    
    public WorkConstraintsTracker(final ky1 ky1) {
        fg0.e((Object)ky1, "trackers");
        this(nh.j((Object[])new ConstraintController[] { new nb(ky1.a()), new qb(ky1.b()), new gq1(ky1.d()), new uy0(ky1.c()), new cz0(ky1.c()), new wy0(ky1.c()), new vy0(ky1.c()) }));
    }
    
    public final boolean a(final p92 p) {
        fg0.e((Object)p, "workSpec");
        final Iterable iterable = this.a;
        final ArrayList list = new ArrayList();
        for (final Object next : iterable) {
            if (((ConstraintController)next).d(p)) {
                list.add(next);
            }
        }
        if (list.isEmpty() ^ true) {
            final xl0 e = xl0.e();
            final String a = WorkConstraintsTrackerKt.a();
            final StringBuilder sb = new StringBuilder();
            sb.append("Work ");
            sb.append(p.a);
            sb.append(" constrained by ");
            sb.append(vh.D((Iterable)list, (CharSequence)null, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (c90)WorkConstraintsTracker$areAllConstraintsMet.WorkConstraintsTracker$areAllConstraintsMet$1.INSTANCE, 31, (Object)null));
            e.a(a, sb.toString());
        }
        return list.isEmpty();
    }
    
    public final y40 b(final p92 p) {
        fg0.e((Object)p, "spec");
        final Iterable iterable = this.a;
        final ArrayList list = new ArrayList();
        for (final Object next : iterable) {
            if (((ConstraintController)next).c(p)) {
                list.add(next);
            }
        }
        final ArrayList list2 = new ArrayList(oh.o((Iterable)list, 10));
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            list2.add((Object)((ConstraintController)iterator2.next()).f());
        }
        return d50.j((y40)new WorkConstraintsTracker$track$$inlined$combine.WorkConstraintsTracker$track$$inlined$combine$1((y40[])vh.P((Iterable)list2).toArray(new y40[0])));
    }
}

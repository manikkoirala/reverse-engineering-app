// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

public abstract class a
{
    public static final class a extends androidx.work.impl.constraints.a
    {
        public static final a a;
        
        static {
            a = new a();
        }
        
        public a() {
            super(null);
        }
    }
    
    public static final class b extends a
    {
        public final int a;
        
        public b(final int a) {
            super(null);
            this.a = a;
        }
        
        public final int a() {
            return this.a;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof b && this.a == ((b)o).a);
        }
        
        @Override
        public int hashCode() {
            return Integer.hashCode(this.a);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ConstraintsNotMet(reason=");
            sb.append(this.a);
            sb.append(')');
            return sb.toString();
        }
    }
}

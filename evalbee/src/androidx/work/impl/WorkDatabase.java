// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.concurrent.Executor;
import android.content.Context;
import androidx.room.RoomDatabase;

public abstract class WorkDatabase extends RoomDatabase
{
    public static final a p;
    
    static {
        p = new a(null);
    }
    
    public abstract qs F();
    
    public abstract o71 G();
    
    public abstract gt1 H();
    
    public abstract g92 I();
    
    public abstract j92 J();
    
    public abstract q92 K();
    
    public abstract w92 L();
    
    public static final class a
    {
        public static final ts1 c(final Context context, final ts1.b b) {
            fg0.e((Object)context, "$context");
            fg0.e((Object)b, "configuration");
            final ts1.b.a a = ts1.b.f.a(context);
            a.d(b.b).c(b.c).e(true).a(true);
            return new r80().a(a.b());
        }
        
        public final WorkDatabase b(final Context context, final Executor executor, final ch ch, final boolean b) {
            fg0.e((Object)context, "context");
            fg0.e((Object)executor, "queryExecutor");
            fg0.e((Object)ch, "clock");
            RoomDatabase.a a;
            if (b) {
                a = pf1.c(context, WorkDatabase.class).c();
            }
            else {
                a = pf1.a(context, WorkDatabase.class, "androidx.work.workdb").f(new k82(context));
            }
            return (WorkDatabase)a.g(executor).a(new yg(ch)).b(pw0.c).b(new fe1(context, 2, 3)).b(qw0.c).b(rw0.c).b(new fe1(context, 5, 6)).b(sw0.c).b(tw0.c).b(uw0.c).b(new e92(context)).b(new fe1(context, 10, 11)).b(lw0.c).b(mw0.c).b(nw0.c).b(ow0.c).e().d();
        }
    }
}

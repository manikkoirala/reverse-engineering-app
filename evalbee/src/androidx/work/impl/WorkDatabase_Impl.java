// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import androidx.room.RoomDatabase;
import java.util.List;

public final class WorkDatabase_Impl extends WorkDatabase
{
    public volatile q92 q;
    public volatile qs r;
    public volatile w92 s;
    public volatile gt1 t;
    public volatile g92 u;
    public volatile j92 v;
    public volatile o71 w;
    
    public static /* synthetic */ List M(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List N(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List O(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List P(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List Q(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List R(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List S(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ ss1 T(final WorkDatabase_Impl workDatabase_Impl, final ss1 a) {
        return workDatabase_Impl.a = a;
    }
    
    public static /* synthetic */ List V(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    public static /* synthetic */ List W(final WorkDatabase_Impl workDatabase_Impl) {
        return workDatabase_Impl.h;
    }
    
    @Override
    public qs F() {
        if (this.r != null) {
            return this.r;
        }
        synchronized (this) {
            if (this.r == null) {
                this.r = new rs(this);
            }
            return this.r;
        }
    }
    
    @Override
    public o71 G() {
        if (this.w != null) {
            return this.w;
        }
        synchronized (this) {
            if (this.w == null) {
                this.w = new p71(this);
            }
            return this.w;
        }
    }
    
    @Override
    public gt1 H() {
        if (this.t != null) {
            return this.t;
        }
        synchronized (this) {
            if (this.t == null) {
                this.t = new ht1(this);
            }
            return this.t;
        }
    }
    
    @Override
    public g92 I() {
        if (this.u != null) {
            return this.u;
        }
        synchronized (this) {
            if (this.u == null) {
                this.u = new h92(this);
            }
            return this.u;
        }
    }
    
    @Override
    public j92 J() {
        if (this.v != null) {
            return this.v;
        }
        synchronized (this) {
            if (this.v == null) {
                this.v = new k92(this);
            }
            return this.v;
        }
    }
    
    @Override
    public q92 K() {
        if (this.q != null) {
            return this.q;
        }
        synchronized (this) {
            if (this.q == null) {
                this.q = new t92(this);
            }
            return this.q;
        }
    }
    
    @Override
    public w92 L() {
        if (this.s != null) {
            return this.s;
        }
        synchronized (this) {
            if (this.s == null) {
                this.s = new x92(this);
            }
            return this.s;
        }
    }
    
    @Override
    public lg0 g() {
        return new lg0(this, new HashMap(0), new HashMap(0), new String[] { "Dependency", "WorkSpec", "WorkTag", "SystemIdInfo", "WorkName", "WorkProgress", "Preference" });
    }
    
    @Override
    public ts1 h(final pp pp) {
        return pp.c.a(ts1.b.a(pp.a).d(pp.b).c(new rf1(pp, (rf1.b)new rf1.b(this, 20) {
            public final WorkDatabase_Impl b;
            
            @Override
            public void a(final ss1 ss1) {
                ss1.M("CREATE TABLE IF NOT EXISTS `Dependency` (`work_spec_id` TEXT NOT NULL, `prerequisite_id` TEXT NOT NULL, PRIMARY KEY(`work_spec_id`, `prerequisite_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`prerequisite_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
                ss1.M("CREATE INDEX IF NOT EXISTS `index_Dependency_work_spec_id` ON `Dependency` (`work_spec_id`)");
                ss1.M("CREATE INDEX IF NOT EXISTS `index_Dependency_prerequisite_id` ON `Dependency` (`prerequisite_id`)");
                ss1.M("CREATE TABLE IF NOT EXISTS `WorkSpec` (`id` TEXT NOT NULL, `state` INTEGER NOT NULL, `worker_class_name` TEXT NOT NULL, `input_merger_class_name` TEXT NOT NULL, `input` BLOB NOT NULL, `output` BLOB NOT NULL, `initial_delay` INTEGER NOT NULL, `interval_duration` INTEGER NOT NULL, `flex_duration` INTEGER NOT NULL, `run_attempt_count` INTEGER NOT NULL, `backoff_policy` INTEGER NOT NULL, `backoff_delay_duration` INTEGER NOT NULL, `last_enqueue_time` INTEGER NOT NULL DEFAULT -1, `minimum_retention_duration` INTEGER NOT NULL, `schedule_requested_at` INTEGER NOT NULL, `run_in_foreground` INTEGER NOT NULL, `out_of_quota_policy` INTEGER NOT NULL, `period_count` INTEGER NOT NULL DEFAULT 0, `generation` INTEGER NOT NULL DEFAULT 0, `next_schedule_time_override` INTEGER NOT NULL DEFAULT 9223372036854775807, `next_schedule_time_override_generation` INTEGER NOT NULL DEFAULT 0, `stop_reason` INTEGER NOT NULL DEFAULT -256, `required_network_type` INTEGER NOT NULL, `requires_charging` INTEGER NOT NULL, `requires_device_idle` INTEGER NOT NULL, `requires_battery_not_low` INTEGER NOT NULL, `requires_storage_not_low` INTEGER NOT NULL, `trigger_content_update_delay` INTEGER NOT NULL, `trigger_max_content_delay` INTEGER NOT NULL, `content_uri_triggers` BLOB NOT NULL, PRIMARY KEY(`id`))");
                ss1.M("CREATE INDEX IF NOT EXISTS `index_WorkSpec_schedule_requested_at` ON `WorkSpec` (`schedule_requested_at`)");
                ss1.M("CREATE INDEX IF NOT EXISTS `index_WorkSpec_last_enqueue_time` ON `WorkSpec` (`last_enqueue_time`)");
                ss1.M("CREATE TABLE IF NOT EXISTS `WorkTag` (`tag` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`tag`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
                ss1.M("CREATE INDEX IF NOT EXISTS `index_WorkTag_work_spec_id` ON `WorkTag` (`work_spec_id`)");
                ss1.M("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `generation` INTEGER NOT NULL DEFAULT 0, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`, `generation`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
                ss1.M("CREATE TABLE IF NOT EXISTS `WorkName` (`name` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`name`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
                ss1.M("CREATE INDEX IF NOT EXISTS `index_WorkName_work_spec_id` ON `WorkName` (`work_spec_id`)");
                ss1.M("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
                ss1.M("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
                ss1.M("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
                ss1.M("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '7d73d21f1bd82c9e5268b6dcf9fde2cb')");
            }
            
            @Override
            public void b(final ss1 ss1) {
                ss1.M("DROP TABLE IF EXISTS `Dependency`");
                ss1.M("DROP TABLE IF EXISTS `WorkSpec`");
                ss1.M("DROP TABLE IF EXISTS `WorkTag`");
                ss1.M("DROP TABLE IF EXISTS `SystemIdInfo`");
                ss1.M("DROP TABLE IF EXISTS `WorkName`");
                ss1.M("DROP TABLE IF EXISTS `WorkProgress`");
                ss1.M("DROP TABLE IF EXISTS `Preference`");
                if (WorkDatabase_Impl.M(this.b) != null) {
                    for (int size = WorkDatabase_Impl.N(this.b).size(), i = 0; i < size; ++i) {
                        ((RoomDatabase.b)WorkDatabase_Impl.P(this.b).get(i)).b(ss1);
                    }
                }
            }
            
            @Override
            public void c(final ss1 ss1) {
                if (WorkDatabase_Impl.Q(this.b) != null) {
                    for (int size = WorkDatabase_Impl.R(this.b).size(), i = 0; i < size; ++i) {
                        ((RoomDatabase.b)WorkDatabase_Impl.S(this.b).get(i)).a(ss1);
                    }
                }
            }
            
            @Override
            public void d(final ss1 ss1) {
                WorkDatabase_Impl.T(this.b, ss1);
                ss1.M("PRAGMA foreign_keys = ON");
                this.b.x(ss1);
                if (WorkDatabase_Impl.V(this.b) != null) {
                    for (int size = WorkDatabase_Impl.W(this.b).size(), i = 0; i < size; ++i) {
                        ((RoomDatabase.b)WorkDatabase_Impl.O(this.b).get(i)).c(ss1);
                    }
                }
            }
            
            @Override
            public void e(final ss1 ss1) {
            }
            
            @Override
            public void f(final ss1 ss1) {
                bp.a(ss1);
            }
            
            @Override
            public rf1.c g(final ss1 ss1) {
                final HashMap hashMap = new HashMap(2);
                hashMap.put("work_spec_id", new ut1.a("work_spec_id", "TEXT", true, 1, null, 1));
                hashMap.put("prerequisite_id", new ut1.a("prerequisite_id", "TEXT", true, 2, null, 1));
                final HashSet set = new HashSet(2);
                set.add(new ut1.c("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                set.add(new ut1.c("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("prerequisite_id"), Arrays.asList("id")));
                final HashSet set2 = new HashSet(2);
                set2.add(new ut1.e("index_Dependency_work_spec_id", false, Arrays.asList("work_spec_id"), Arrays.asList("ASC")));
                set2.add(new ut1.e("index_Dependency_prerequisite_id", false, Arrays.asList("prerequisite_id"), Arrays.asList("ASC")));
                final ut1 obj = new ut1("Dependency", hashMap, set, set2);
                final ut1 a = ut1.a(ss1, "Dependency");
                if (!obj.equals(a)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Dependency(androidx.work.impl.model.Dependency).\n Expected:\n");
                    sb.append(obj);
                    sb.append("\n Found:\n");
                    sb.append(a);
                    return new rf1.c(false, sb.toString());
                }
                final HashMap hashMap2 = new HashMap(30);
                hashMap2.put("id", new ut1.a("id", "TEXT", true, 1, null, 1));
                hashMap2.put("state", new ut1.a("state", "INTEGER", true, 0, null, 1));
                hashMap2.put("worker_class_name", new ut1.a("worker_class_name", "TEXT", true, 0, null, 1));
                hashMap2.put("input_merger_class_name", new ut1.a("input_merger_class_name", "TEXT", true, 0, null, 1));
                hashMap2.put("input", new ut1.a("input", "BLOB", true, 0, null, 1));
                hashMap2.put("output", new ut1.a("output", "BLOB", true, 0, null, 1));
                hashMap2.put("initial_delay", new ut1.a("initial_delay", "INTEGER", true, 0, null, 1));
                hashMap2.put("interval_duration", new ut1.a("interval_duration", "INTEGER", true, 0, null, 1));
                hashMap2.put("flex_duration", new ut1.a("flex_duration", "INTEGER", true, 0, null, 1));
                hashMap2.put("run_attempt_count", new ut1.a("run_attempt_count", "INTEGER", true, 0, null, 1));
                hashMap2.put("backoff_policy", new ut1.a("backoff_policy", "INTEGER", true, 0, null, 1));
                hashMap2.put("backoff_delay_duration", new ut1.a("backoff_delay_duration", "INTEGER", true, 0, null, 1));
                hashMap2.put("last_enqueue_time", new ut1.a("last_enqueue_time", "INTEGER", true, 0, "-1", 1));
                hashMap2.put("minimum_retention_duration", new ut1.a("minimum_retention_duration", "INTEGER", true, 0, null, 1));
                hashMap2.put("schedule_requested_at", new ut1.a("schedule_requested_at", "INTEGER", true, 0, null, 1));
                hashMap2.put("run_in_foreground", new ut1.a("run_in_foreground", "INTEGER", true, 0, null, 1));
                hashMap2.put("out_of_quota_policy", new ut1.a("out_of_quota_policy", "INTEGER", true, 0, null, 1));
                hashMap2.put("period_count", new ut1.a("period_count", "INTEGER", true, 0, "0", 1));
                hashMap2.put("generation", new ut1.a("generation", "INTEGER", true, 0, "0", 1));
                hashMap2.put("next_schedule_time_override", new ut1.a("next_schedule_time_override", "INTEGER", true, 0, "9223372036854775807", 1));
                hashMap2.put("next_schedule_time_override_generation", new ut1.a("next_schedule_time_override_generation", "INTEGER", true, 0, "0", 1));
                hashMap2.put("stop_reason", new ut1.a("stop_reason", "INTEGER", true, 0, "-256", 1));
                hashMap2.put("required_network_type", new ut1.a("required_network_type", "INTEGER", true, 0, null, 1));
                hashMap2.put("requires_charging", new ut1.a("requires_charging", "INTEGER", true, 0, null, 1));
                hashMap2.put("requires_device_idle", new ut1.a("requires_device_idle", "INTEGER", true, 0, null, 1));
                hashMap2.put("requires_battery_not_low", new ut1.a("requires_battery_not_low", "INTEGER", true, 0, null, 1));
                hashMap2.put("requires_storage_not_low", new ut1.a("requires_storage_not_low", "INTEGER", true, 0, null, 1));
                hashMap2.put("trigger_content_update_delay", new ut1.a("trigger_content_update_delay", "INTEGER", true, 0, null, 1));
                hashMap2.put("trigger_max_content_delay", new ut1.a("trigger_max_content_delay", "INTEGER", true, 0, null, 1));
                hashMap2.put("content_uri_triggers", new ut1.a("content_uri_triggers", "BLOB", true, 0, null, 1));
                final HashSet set3 = new HashSet(0);
                final HashSet set4 = new HashSet(2);
                set4.add(new ut1.e("index_WorkSpec_schedule_requested_at", false, Arrays.asList("schedule_requested_at"), Arrays.asList("ASC")));
                set4.add(new ut1.e("index_WorkSpec_last_enqueue_time", false, Arrays.asList("last_enqueue_time"), Arrays.asList("ASC")));
                final ut1 obj2 = new ut1("WorkSpec", hashMap2, set3, set4);
                final ut1 a2 = ut1.a(ss1, "WorkSpec");
                if (!obj2.equals(a2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("WorkSpec(androidx.work.impl.model.WorkSpec).\n Expected:\n");
                    sb2.append(obj2);
                    sb2.append("\n Found:\n");
                    sb2.append(a2);
                    return new rf1.c(false, sb2.toString());
                }
                final HashMap hashMap3 = new HashMap(2);
                hashMap3.put("tag", new ut1.a("tag", "TEXT", true, 1, null, 1));
                hashMap3.put("work_spec_id", new ut1.a("work_spec_id", "TEXT", true, 2, null, 1));
                final HashSet set5 = new HashSet(1);
                set5.add(new ut1.c("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                final HashSet set6 = new HashSet(1);
                set6.add(new ut1.e("index_WorkTag_work_spec_id", false, Arrays.asList("work_spec_id"), Arrays.asList("ASC")));
                final ut1 obj3 = new ut1("WorkTag", hashMap3, set5, set6);
                final ut1 a3 = ut1.a(ss1, "WorkTag");
                if (!obj3.equals(a3)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("WorkTag(androidx.work.impl.model.WorkTag).\n Expected:\n");
                    sb3.append(obj3);
                    sb3.append("\n Found:\n");
                    sb3.append(a3);
                    return new rf1.c(false, sb3.toString());
                }
                final HashMap hashMap4 = new HashMap(3);
                hashMap4.put("work_spec_id", new ut1.a("work_spec_id", "TEXT", true, 1, null, 1));
                hashMap4.put("generation", new ut1.a("generation", "INTEGER", true, 2, "0", 1));
                hashMap4.put("system_id", new ut1.a("system_id", "INTEGER", true, 0, null, 1));
                final HashSet set7 = new HashSet(1);
                set7.add(new ut1.c("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                final ut1 obj4 = new ut1("SystemIdInfo", hashMap4, set7, new HashSet(0));
                final ut1 a4 = ut1.a(ss1, "SystemIdInfo");
                if (!obj4.equals(a4)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("SystemIdInfo(androidx.work.impl.model.SystemIdInfo).\n Expected:\n");
                    sb4.append(obj4);
                    sb4.append("\n Found:\n");
                    sb4.append(a4);
                    return new rf1.c(false, sb4.toString());
                }
                final HashMap hashMap5 = new HashMap(2);
                hashMap5.put("name", new ut1.a("name", "TEXT", true, 1, null, 1));
                hashMap5.put("work_spec_id", new ut1.a("work_spec_id", "TEXT", true, 2, null, 1));
                final HashSet set8 = new HashSet(1);
                set8.add(new ut1.c("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                final HashSet set9 = new HashSet(1);
                set9.add(new ut1.e("index_WorkName_work_spec_id", false, Arrays.asList("work_spec_id"), Arrays.asList("ASC")));
                final ut1 obj5 = new ut1("WorkName", hashMap5, set8, set9);
                final ut1 a5 = ut1.a(ss1, "WorkName");
                if (!obj5.equals(a5)) {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("WorkName(androidx.work.impl.model.WorkName).\n Expected:\n");
                    sb5.append(obj5);
                    sb5.append("\n Found:\n");
                    sb5.append(a5);
                    return new rf1.c(false, sb5.toString());
                }
                final HashMap hashMap6 = new HashMap(2);
                hashMap6.put("work_spec_id", new ut1.a("work_spec_id", "TEXT", true, 1, null, 1));
                hashMap6.put("progress", new ut1.a("progress", "BLOB", true, 0, null, 1));
                final HashSet set10 = new HashSet(1);
                set10.add(new ut1.c("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                final ut1 obj6 = new ut1("WorkProgress", hashMap6, set10, new HashSet(0));
                final ut1 a6 = ut1.a(ss1, "WorkProgress");
                if (!obj6.equals(a6)) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("WorkProgress(androidx.work.impl.model.WorkProgress).\n Expected:\n");
                    sb6.append(obj6);
                    sb6.append("\n Found:\n");
                    sb6.append(a6);
                    return new rf1.c(false, sb6.toString());
                }
                final HashMap hashMap7 = new HashMap(2);
                hashMap7.put("key", new ut1.a("key", "TEXT", true, 1, null, 1));
                hashMap7.put("long_value", new ut1.a("long_value", "INTEGER", false, 0, null, 1));
                final ut1 obj7 = new ut1("Preference", hashMap7, new HashSet(0), new HashSet(0));
                final ut1 a7 = ut1.a(ss1, "Preference");
                if (!obj7.equals(a7)) {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append("Preference(androidx.work.impl.model.Preference).\n Expected:\n");
                    sb7.append(obj7);
                    sb7.append("\n Found:\n");
                    sb7.append(a7);
                    return new rf1.c(false, sb7.toString());
                }
                return new rf1.c(true, null);
            }
        }, "7d73d21f1bd82c9e5268b6dcf9fde2cb", "3071c8717539de5d5353f4c8cd59a032")).b());
    }
    
    @Override
    public List j(final Map map) {
        return Arrays.asList(new o82(), new p82(), new q82(), new r82(), new s82(), new t82());
    }
    
    @Override
    public Set p() {
        return new HashSet();
    }
    
    @Override
    public Map q() {
        final HashMap hashMap = new HashMap();
        hashMap.put(q92.class, t92.B());
        hashMap.put(qs.class, rs.e());
        hashMap.put(w92.class, x92.e());
        hashMap.put(gt1.class, ht1.j());
        hashMap.put(g92.class, h92.c());
        hashMap.put(j92.class, k92.d());
        hashMap.put(o71.class, p71.c());
        hashMap.put(mc1.class, nc1.a());
        return hashMap;
    }
}

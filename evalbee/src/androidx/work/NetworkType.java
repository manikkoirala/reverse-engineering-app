// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum NetworkType
{
    private static final NetworkType[] $VALUES;
    
    CONNECTED, 
    METERED, 
    NOT_REQUIRED, 
    NOT_ROAMING, 
    TEMPORARILY_UNMETERED, 
    UNMETERED;
    
    private static final /* synthetic */ NetworkType[] $values() {
        return new NetworkType[] { NetworkType.NOT_REQUIRED, NetworkType.CONNECTED, NetworkType.UNMETERED, NetworkType.NOT_ROAMING, NetworkType.METERED, NetworkType.TEMPORARILY_UNMETERED };
    }
    
    static {
        $VALUES = $values();
    }
}

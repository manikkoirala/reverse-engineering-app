// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.concurrent.Executor;

public final class a
{
    public static final b p;
    public final Executor a;
    public final Executor b;
    public final ch c;
    public final aa2 d;
    public final lf0 e;
    public final ag1 f;
    public final dl g;
    public final dl h;
    public final String i;
    public final int j;
    public final int k;
    public final int l;
    public final int m;
    public final int n;
    public final boolean o;
    
    static {
        p = new b(null);
    }
    
    public a(final a a) {
        fg0.e((Object)a, "builder");
        final Executor e = a.e();
        boolean o = false;
        Executor a2 = e;
        if (e == null) {
            a2 = ik.a(false);
        }
        this.a = a2;
        if (a.n() == null) {
            o = true;
        }
        this.o = o;
        Executor b;
        if ((b = a.n()) == null) {
            b = ik.a(true);
        }
        this.b = b;
        ch b2;
        if ((b2 = a.b()) == null) {
            b2 = new dt1();
        }
        this.c = b2;
        aa2 d;
        if ((d = a.o()) == null) {
            d = aa2.c();
            fg0.d((Object)d, "getDefaultWorkerFactory()");
        }
        this.d = d;
        lf0 e2;
        if ((e2 = a.g()) == null) {
            e2 = lz0.a;
        }
        this.e = e2;
        ag1 l;
        if ((l = a.l()) == null) {
            l = new xq();
        }
        this.f = l;
        this.j = a.h();
        this.k = a.k();
        this.l = a.i();
        this.n = a.j();
        this.g = a.f();
        this.h = a.m();
        this.i = a.d();
        this.m = a.c();
    }
    
    public final ch a() {
        return this.c;
    }
    
    public final int b() {
        return this.m;
    }
    
    public final String c() {
        return this.i;
    }
    
    public final Executor d() {
        return this.a;
    }
    
    public final dl e() {
        return this.g;
    }
    
    public final lf0 f() {
        return this.e;
    }
    
    public final int g() {
        return this.l;
    }
    
    public final int h() {
        return this.n;
    }
    
    public final int i() {
        return this.k;
    }
    
    public final int j() {
        return this.j;
    }
    
    public final ag1 k() {
        return this.f;
    }
    
    public final dl l() {
        return this.h;
    }
    
    public final Executor m() {
        return this.b;
    }
    
    public final aa2 n() {
        return this.d;
    }
    
    public static final class a
    {
        public Executor a;
        public aa2 b;
        public lf0 c;
        public Executor d;
        public ch e;
        public ag1 f;
        public dl g;
        public dl h;
        public String i;
        public int j;
        public int k;
        public int l;
        public int m;
        public int n;
        
        public a() {
            this.j = 4;
            this.l = Integer.MAX_VALUE;
            this.m = 20;
            this.n = ik.c();
        }
        
        public final androidx.work.a a() {
            return new androidx.work.a(this);
        }
        
        public final ch b() {
            return this.e;
        }
        
        public final int c() {
            return this.n;
        }
        
        public final String d() {
            return this.i;
        }
        
        public final Executor e() {
            return this.a;
        }
        
        public final dl f() {
            return this.g;
        }
        
        public final lf0 g() {
            return this.c;
        }
        
        public final int h() {
            return this.j;
        }
        
        public final int i() {
            return this.l;
        }
        
        public final int j() {
            return this.m;
        }
        
        public final int k() {
            return this.k;
        }
        
        public final ag1 l() {
            return this.f;
        }
        
        public final dl m() {
            return this.h;
        }
        
        public final Executor n() {
            return this.d;
        }
        
        public final aa2 o() {
            return this.b;
        }
    }
    
    public static final class b
    {
    }
}

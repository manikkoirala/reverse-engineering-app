// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import org.jetbrains.annotations.Nullable;
import kotlin.jvm.internal.Lambda;

public final class ListenableFutureKt$await$2$2 extends Lambda implements c90
{
    final ik0 $this_await;
    
    public ListenableFutureKt$await$2$2(final ik0 $this_await) {
        this.$this_await = $this_await;
        super(1);
    }
    
    public final void invoke(@Nullable final Throwable t) {
        this.$this_await.cancel(false);
    }
}

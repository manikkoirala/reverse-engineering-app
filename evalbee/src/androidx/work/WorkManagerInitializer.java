// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import java.util.List;
import android.content.Context;

public final class WorkManagerInitializer implements af0
{
    public static final String a;
    
    static {
        a = xl0.i("WrkMgrInitializer");
    }
    
    public WorkManager b(final Context context) {
        xl0.e().a(WorkManagerInitializer.a, "Initializing WorkManager with default configuration.");
        WorkManager.e(context, new a.a().a());
        return WorkManager.d(context);
    }
    
    @Override
    public List dependencies() {
        return Collections.emptyList();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;

public final class OverwritingInputMerger extends kf0
{
    @Override
    public b a(final List list) {
        fg0.e((Object)list, "inputs");
        final b.a a = new b.a();
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            final Map h = ((b)iterator.next()).h();
            fg0.d((Object)h, "input.keyValueMap");
            linkedHashMap.putAll(h);
        }
        a.d(linkedHashMap);
        final b a2 = a.a();
        fg0.d((Object)a2, "output.build()");
        return a2;
    }
}

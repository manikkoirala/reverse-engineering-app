// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum ExistingWorkPolicy
{
    private static final ExistingWorkPolicy[] $VALUES;
    
    APPEND, 
    APPEND_OR_REPLACE, 
    KEEP, 
    REPLACE;
    
    private static final /* synthetic */ ExistingWorkPolicy[] $values() {
        return new ExistingWorkPolicy[] { ExistingWorkPolicy.REPLACE, ExistingWorkPolicy.KEEP, ExistingWorkPolicy.APPEND, ExistingWorkPolicy.APPEND_OR_REPLACE };
    }
    
    static {
        $VALUES = $values();
    }
}

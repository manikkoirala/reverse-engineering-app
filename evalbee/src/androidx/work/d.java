// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public interface d
{
    public static final c a = new c(null);
    public static final b.b b = new b.b(null);
    
    ik0 getResult();
    
    public abstract static class b
    {
        public static final class a extends d.b
        {
            public final Throwable a;
            
            public a(final Throwable a) {
                this.a = a;
            }
            
            public Throwable a() {
                return this.a;
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("FAILURE (");
                sb.append(this.a.getMessage());
                sb.append(")");
                return sb.toString();
            }
        }
        
        public static final class b extends d.b
        {
            @Override
            public String toString() {
                return "IN_PROGRESS";
            }
        }
        
        public static final class c extends d.b
        {
            @Override
            public String toString() {
                return "SUCCESS";
            }
        }
    }
}

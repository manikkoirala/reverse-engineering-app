// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum ExistingPeriodicWorkPolicy
{
    private static final ExistingPeriodicWorkPolicy[] $VALUES;
    
    CANCEL_AND_REENQUEUE, 
    KEEP, 
    REPLACE, 
    UPDATE;
    
    private static final /* synthetic */ ExistingPeriodicWorkPolicy[] $values() {
        return new ExistingPeriodicWorkPolicy[] { ExistingPeriodicWorkPolicy.REPLACE, ExistingPeriodicWorkPolicy.KEEP, ExistingPeriodicWorkPolicy.UPDATE, ExistingPeriodicWorkPolicy.CANCEL_AND_REENQUEUE };
    }
    
    static {
        $VALUES = $values();
    }
}

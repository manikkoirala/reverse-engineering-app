// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.net.Uri;
import java.util.List;
import java.util.Set;
import android.net.Network;
import java.util.UUID;
import java.util.concurrent.Executor;
import android.content.Context;

public abstract class c
{
    private Context mAppContext;
    private volatile int mStopReason;
    private boolean mUsed;
    private WorkerParameters mWorkerParams;
    
    public c(final Context mAppContext, final WorkerParameters mWorkerParams) {
        this.mStopReason = -256;
        if (mAppContext == null) {
            throw new IllegalArgumentException("Application Context is null");
        }
        if (mWorkerParams != null) {
            this.mAppContext = mAppContext;
            this.mWorkerParams = mWorkerParams;
            return;
        }
        throw new IllegalArgumentException("WorkerParameters is null");
    }
    
    public final Context getApplicationContext() {
        return this.mAppContext;
    }
    
    public Executor getBackgroundExecutor() {
        return this.mWorkerParams.a();
    }
    
    public ik0 getForegroundInfoAsync() {
        final um1 s = um1.s();
        s.p(new IllegalStateException("Expedited WorkRequests require a ListenableWorker to provide an implementation for `getForegroundInfoAsync()`"));
        return s;
    }
    
    public final UUID getId() {
        return this.mWorkerParams.c();
    }
    
    public final androidx.work.b getInputData() {
        return this.mWorkerParams.d();
    }
    
    public final Network getNetwork() {
        return this.mWorkerParams.e();
    }
    
    public final int getRunAttemptCount() {
        return this.mWorkerParams.g();
    }
    
    public final int getStopReason() {
        return this.mStopReason;
    }
    
    public final Set<String> getTags() {
        return this.mWorkerParams.h();
    }
    
    public hu1 getTaskExecutor() {
        return this.mWorkerParams.i();
    }
    
    public final List<String> getTriggeredContentAuthorities() {
        return this.mWorkerParams.j();
    }
    
    public final List<Uri> getTriggeredContentUris() {
        return this.mWorkerParams.k();
    }
    
    public aa2 getWorkerFactory() {
        return this.mWorkerParams.l();
    }
    
    public final boolean isStopped() {
        return this.mStopReason != -256;
    }
    
    public final boolean isUsed() {
        return this.mUsed;
    }
    
    public void onStopped() {
    }
    
    public final ik0 setForegroundAsync(final p70 p) {
        return this.mWorkerParams.b().a(this.getApplicationContext(), this.getId(), p);
    }
    
    public ik0 setProgressAsync(final androidx.work.b b) {
        return this.mWorkerParams.f().a(this.getApplicationContext(), this.getId(), b);
    }
    
    public final void setUsed() {
        this.mUsed = true;
    }
    
    public abstract ik0 startWork();
    
    public final void stop(final int mStopReason) {
        this.mStopReason = mStopReason;
        this.onStopped();
    }
    
    public abstract static class a
    {
        public static androidx.work.c.a a() {
            return new androidx.work.c.a.a();
        }
        
        public static androidx.work.c.a b() {
            return new b();
        }
        
        public static androidx.work.c.a c() {
            return new c();
        }
        
        public static androidx.work.c.a d(final androidx.work.b b) {
            return new c(b);
        }
        
        public static final class a extends androidx.work.c.a
        {
            public final androidx.work.b a;
            
            public a() {
                this(androidx.work.b.c);
            }
            
            public a(final androidx.work.b a) {
                this.a = a;
            }
            
            public androidx.work.b e() {
                return this.a;
            }
            
            @Override
            public boolean equals(final Object o) {
                return this == o || (o != null && a.class == o.getClass() && this.a.equals(((a)o).a));
            }
            
            @Override
            public int hashCode() {
                return a.class.getName().hashCode() * 31 + this.a.hashCode();
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failure {mOutputData=");
                sb.append(this.a);
                sb.append('}');
                return sb.toString();
            }
        }
        
        public static final class b extends androidx.work.c.a
        {
            @Override
            public boolean equals(final Object o) {
                boolean b = true;
                if (this == o) {
                    return true;
                }
                if (o == null || b.class != o.getClass()) {
                    b = false;
                }
                return b;
            }
            
            @Override
            public int hashCode() {
                return b.class.getName().hashCode();
            }
            
            @Override
            public String toString() {
                return "Retry";
            }
        }
        
        public static final class c extends androidx.work.c.a
        {
            public final androidx.work.b a;
            
            public c() {
                this(androidx.work.b.c);
            }
            
            public c(final androidx.work.b a) {
                this.a = a;
            }
            
            public androidx.work.b e() {
                return this.a;
            }
            
            @Override
            public boolean equals(final Object o) {
                return this == o || (o != null && c.class == o.getClass() && this.a.equals(((c)o).a));
            }
            
            @Override
            public int hashCode() {
                return c.class.getName().hashCode() * 31 + this.a.hashCode();
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Success {mOutputData=");
                sb.append(this.a);
                sb.append('}');
                return sb.toString();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import org.jetbrains.annotations.NotNull;
import java.util.concurrent.Executor;

public enum DirectExecutor implements Executor
{
    private static final DirectExecutor[] $VALUES;
    
    INSTANCE;
    
    private static final /* synthetic */ DirectExecutor[] $values() {
        return new DirectExecutor[] { DirectExecutor.INSTANCE };
    }
    
    static {
        $VALUES = $values();
    }
    
    @Override
    public void execute(@NotNull final Runnable runnable) {
        fg0.e((Object)runnable, "command");
        runnable.run();
    }
    
    @NotNull
    @Override
    public String toString() {
        return "DirectExecutor";
    }
}

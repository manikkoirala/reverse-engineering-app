// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum OutOfQuotaPolicy
{
    private static final OutOfQuotaPolicy[] $VALUES;
    
    DROP_WORK_REQUEST, 
    RUN_AS_NON_EXPEDITED_WORK_REQUEST;
    
    private static final /* synthetic */ OutOfQuotaPolicy[] $values() {
        return new OutOfQuotaPolicy[] { OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST, OutOfQuotaPolicy.DROP_WORK_REQUEST };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum BackoffPolicy
{
    private static final BackoffPolicy[] $VALUES;
    
    EXPONENTIAL, 
    LINEAR;
    
    private static final /* synthetic */ BackoffPolicy[] $values() {
        return new BackoffPolicy[] { BackoffPolicy.EXPONENTIAL, BackoffPolicy.LINEAR };
    }
    
    static {
        $VALUES = $values();
    }
}

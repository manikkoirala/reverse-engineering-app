// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.List;
import java.util.Collections;
import android.content.Context;

public abstract class WorkManager
{
    public static WorkManager d(final Context context) {
        return c92.j(context);
    }
    
    public static void e(final Context context, final a a) {
        c92.e(context, a);
    }
    
    public abstract d a(final String p0);
    
    public final d b(final n92 o) {
        return this.c(Collections.singletonList(o));
    }
    
    public abstract d c(final List p0);
    
    public enum UpdateResult
    {
        private static final UpdateResult[] $VALUES;
        
        APPLIED_FOR_NEXT_RUN, 
        APPLIED_IMMEDIATELY, 
        NOT_APPLIED;
        
        private static /* synthetic */ UpdateResult[] $values() {
            return new UpdateResult[] { UpdateResult.NOT_APPLIED, UpdateResult.APPLIED_IMMEDIATELY, UpdateResult.APPLIED_FOR_NEXT_RUN };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

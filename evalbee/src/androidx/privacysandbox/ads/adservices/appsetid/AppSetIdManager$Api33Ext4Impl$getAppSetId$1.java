// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.appsetid;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "androidx.privacysandbox.ads.adservices.appsetid.AppSetIdManager$Api33Ext4Impl", f = "AppSetIdManager.kt", l = { 55 }, m = "getAppSetId")
final class AppSetIdManager$Api33Ext4Impl$getAppSetId$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final a.a this$0;
    
    public AppSetIdManager$Api33Ext4Impl$getAppSetId$1(final a.a a, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

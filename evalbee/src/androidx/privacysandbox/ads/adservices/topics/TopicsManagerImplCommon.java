// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import java.util.concurrent.Executor;
import androidx.core.os.a;
import kotlinx.coroutines.c;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import android.adservices.topics.Topic;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import android.adservices.topics.GetTopicsResponse;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.adservices.topics.GetTopicsRequest;
import android.adservices.topics.TopicsManager;

public abstract class TopicsManagerImplCommon extends kx1
{
    public final TopicsManager b;
    
    public TopicsManagerImplCommon(final TopicsManager b) {
        fg0.e((Object)b, "mTopicsManager");
        this.b = b;
    }
    
    public static final /* synthetic */ TopicsManager b(final TopicsManagerImplCommon topicsManagerImplCommon) {
        return topicsManagerImplCommon.b;
    }
    
    public static /* synthetic */ Object e(TopicsManagerImplCommon l$0, final za0 za0, final vl vl) {
        TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1 topicsManagerImplCommon$getTopics$2 = null;
        Label_0051: {
            if (vl instanceof TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1) {
                final TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1 topicsManagerImplCommon$getTopics$1 = (TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)vl;
                final int label = topicsManagerImplCommon$getTopics$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    topicsManagerImplCommon$getTopics$1.label = label + Integer.MIN_VALUE;
                    topicsManagerImplCommon$getTopics$2 = topicsManagerImplCommon$getTopics$1;
                    break Label_0051;
                }
            }
            topicsManagerImplCommon$getTopics$2 = new TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1(l$0, vl);
        }
        final Object result = topicsManagerImplCommon$getTopics$2.result;
        final Object d = gg0.d();
        final int label2 = topicsManagerImplCommon$getTopics$2.label;
        Object f;
        if (label2 != 0) {
            if (label2 != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            l$0 = (TopicsManagerImplCommon)topicsManagerImplCommon$getTopics$2.L$0;
            xe1.b(result);
            f = result;
        }
        else {
            xe1.b(result);
            final GetTopicsRequest c = l$0.c(za0);
            topicsManagerImplCommon$getTopics$2.L$0 = l$0;
            topicsManagerImplCommon$getTopics$2.label = 1;
            if ((f = l$0.f(c, (vl)topicsManagerImplCommon$getTopics$2)) == d) {
                return d;
            }
        }
        return l$0.d(tx1.a(f));
    }
    
    @Nullable
    @Override
    public Object a(@NotNull final za0 za0, @NotNull final vl vl) {
        return e(this, za0, vl);
    }
    
    public GetTopicsRequest c(final za0 za0) {
        fg0.e((Object)za0, "request");
        final GetTopicsRequest a = rx1.a(px1.a(ox1.a(), za0.a()));
        fg0.d((Object)a, "Builder()\n            .s\u2026ame)\n            .build()");
        return a;
    }
    
    public final ab0 d(final GetTopicsResponse getTopicsResponse) {
        fg0.e((Object)getTopicsResponse, "response");
        final ArrayList list = new ArrayList();
        final Iterator iterator = vx1.a(getTopicsResponse).iterator();
        while (iterator.hasNext()) {
            final Topic a = wx1.a(iterator.next());
            list.add(new ix1(xx1.a(a), yx1.a(a), zx1.a(a)));
        }
        return new ab0(list);
    }
    
    public final Object f(final GetTopicsRequest getTopicsRequest, final vl vl) {
        final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
        c.z();
        ux1.a(b(this), getTopicsRequest, (Executor)new xu0(), androidx.core.os.a.a((vl)c));
        final Object v = c.v();
        if (v == gg0.d()) {
            zp.c(vl);
        }
        return v;
    }
}

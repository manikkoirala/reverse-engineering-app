// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adselection;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "androidx.privacysandbox.ads.adservices.adselection.AdSelectionManager$Api33Ext4Impl", f = "AdSelectionManager.kt", l = { 92 }, m = "selectAds")
final class AdSelectionManager$Api33Ext4Impl$selectAds$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final a.a this$0;
    
    public AdSelectionManager$Api33Ext4Impl$selectAds$1(final a.a a, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

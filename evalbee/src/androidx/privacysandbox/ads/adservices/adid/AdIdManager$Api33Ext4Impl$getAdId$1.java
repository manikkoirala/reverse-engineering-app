// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adid;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "androidx.privacysandbox.ads.adservices.adid.AdIdManager$Api33Ext4Impl", f = "AdIdManager.kt", l = { 62 }, m = "getAdId")
final class AdIdManager$Api33Ext4Impl$getAdId$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final a.a this$0;
    
    public AdIdManager$Api33Ext4Impl$getAdId$1(final a.a a, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

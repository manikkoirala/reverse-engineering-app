// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.appsetid;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.privacysandbox.ads.adservices.java.appsetid.AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync$1", f = "AppSetIdManagerFutures.kt", l = { 50 }, m = "invokeSuspend")
final class AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync$1 extends SuspendLambda implements q90
{
    int label;
    final a.a this$0;
    
    public AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync$1(final a.a a, final vl vl) {
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync$1(null, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        final int label = this.label;
        if (label == 0) {
            xe1.b(o);
            a.a.a(null);
            this.label = 1;
            throw null;
        }
        if (label == 1) {
            xe1.b(o);
            return o;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

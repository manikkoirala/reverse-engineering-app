// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.adselection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.privacysandbox.ads.adservices.java.adselection.AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync$1", f = "AdSelectionManagerFutures.kt", l = { 94 }, m = "invokeSuspend")
final class AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync$1 extends SuspendLambda implements q90
{
    final y2 $adSelectionConfig;
    int label;
    final a.a this$0;
    
    public AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync$1(final a.a a, final y2 y2, final vl vl) {
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync$1(null, null, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        final int label = this.label;
        if (label == 0) {
            xe1.b(o);
            a.a.a(null);
            fg0.b((Object)null);
            this.label = 1;
            throw null;
        }
        if (label == 1) {
            xe1.b(o);
            return o;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

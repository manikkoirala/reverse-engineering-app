// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.topics;

import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.f;
import org.jetbrains.annotations.NotNull;
import android.content.Context;

public abstract class TopicsManagerFutures
{
    public static final a a;
    
    static {
        a = new a(null);
    }
    
    public static final TopicsManagerFutures a(final Context context) {
        return TopicsManagerFutures.a.a(context);
    }
    
    public abstract ik0 b(final za0 p0);
    
    public static final class Api33Ext4JavaImpl extends TopicsManagerFutures
    {
        public final kx1 b;
        
        public Api33Ext4JavaImpl(final kx1 b) {
            fg0.e((Object)b, "mTopicsManager");
            this.b = b;
        }
        
        @NotNull
        @Override
        public ik0 b(@NotNull final za0 za0) {
            fg0.e((Object)za0, "request");
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.c()), (CoroutineContext)null, (CoroutineStart)null, (q90)new TopicsManagerFutures$Api33Ext4JavaImpl$getTopicsAsync.TopicsManagerFutures$Api33Ext4JavaImpl$getTopicsAsync$1(this, za0, (vl)null), 3, (Object)null), null, 1, null);
        }
    }
    
    public static final class a
    {
        public final TopicsManagerFutures a(final Context context) {
            fg0.e((Object)context, "context");
            final kx1 a = kx1.a.a(context);
            TopicsManagerFutures topicsManagerFutures;
            if (a != null) {
                topicsManagerFutures = new Api33Ext4JavaImpl(a);
            }
            else {
                topicsManagerFutures = null;
            }
            return topicsManagerFutures;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.measurement;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.f;
import android.view.InputEvent;
import android.net.Uri;
import android.content.Context;

public abstract class MeasurementManagerFutures
{
    public static final a a;
    
    static {
        a = new a(null);
    }
    
    public static final MeasurementManagerFutures a(final Context context) {
        return MeasurementManagerFutures.a.a(context);
    }
    
    public abstract ik0 b();
    
    public abstract ik0 c(final Uri p0, final InputEvent p1);
    
    public abstract ik0 d(final Uri p0);
    
    public static final class Api33Ext5JavaImpl extends MeasurementManagerFutures
    {
        public final yu0 b;
        
        public Api33Ext5JavaImpl(final yu0 b) {
            fg0.e((Object)b, "mMeasurementManager");
            this.b = b;
        }
        
        @NotNull
        @Override
        public ik0 b() {
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.a()), (CoroutineContext)null, (CoroutineStart)null, (q90)new MeasurementManagerFutures$Api33Ext5JavaImpl$getMeasurementApiStatusAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$getMeasurementApiStatusAsync$1(this, (vl)null), 3, (Object)null), null, 1, null);
        }
        
        @NotNull
        @Override
        public ik0 c(@NotNull final Uri uri, @Nullable final InputEvent inputEvent) {
            fg0.e((Object)uri, "attributionSource");
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.a()), (CoroutineContext)null, (CoroutineStart)null, (q90)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerSourceAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerSourceAsync$1(this, uri, inputEvent, (vl)null), 3, (Object)null), null, 1, null);
        }
        
        @NotNull
        @Override
        public ik0 d(@NotNull final Uri uri) {
            fg0.e((Object)uri, "trigger");
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.a()), (CoroutineContext)null, (CoroutineStart)null, (q90)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerTriggerAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerTriggerAsync$1(this, uri, (vl)null), 3, (Object)null), null, 1, null);
        }
        
        @NotNull
        public ik0 f(@NotNull final ls ls) {
            fg0.e((Object)ls, "deletionRequest");
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.a()), (CoroutineContext)null, (CoroutineStart)null, (q90)new MeasurementManagerFutures$Api33Ext5JavaImpl$deleteRegistrationsAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$deleteRegistrationsAsync$1(this, ls, (vl)null), 3, (Object)null), null, 1, null);
        }
        
        @NotNull
        public ik0 g(@NotNull final w52 w52) {
            fg0.e((Object)w52, "request");
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.a()), (CoroutineContext)null, (CoroutineStart)null, (q90)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebSourceAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebSourceAsync$1(this, w52, (vl)null), 3, (Object)null), null, 1, null);
        }
        
        @NotNull
        public ik0 h(@NotNull final x52 x52) {
            fg0.e((Object)x52, "request");
            return CoroutineAdapterKt.c(ad.b(f.a((CoroutineContext)pt.a()), (CoroutineContext)null, (CoroutineStart)null, (q90)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebTriggerAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebTriggerAsync$1(this, x52, (vl)null), 3, (Object)null), null, 1, null);
        }
    }
    
    public static final class a
    {
        public final MeasurementManagerFutures a(final Context context) {
            fg0.e((Object)context, "context");
            final yu0 a = yu0.a.a(context);
            MeasurementManagerFutures measurementManagerFutures;
            if (a != null) {
                measurementManagerFutures = new Api33Ext5JavaImpl(a);
            }
            else {
                measurementManagerFutures = null;
            }
            return measurementManagerFutures;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.internal;

import kotlinx.coroutines.n;
import androidx.concurrent.futures.CallbackToFutureAdapter;

public abstract class CoroutineAdapterKt
{
    public static final ik0 b(final kr kr, final Object o) {
        fg0.e((Object)kr, "<this>");
        final ik0 a = CallbackToFutureAdapter.a((CallbackToFutureAdapter.b)new fm(kr, o));
        fg0.d((Object)a, "getFuture { completer ->\u2026        }\n    }\n    tag\n}");
        return a;
    }
    
    public static final Object d(final kr kr, final Object o, final CallbackToFutureAdapter.a a) {
        fg0.e((Object)kr, "$this_asListenableFuture");
        fg0.e((Object)a, "completer");
        ((n)kr).q((c90)new CoroutineAdapterKt$asListenableFuture$1.CoroutineAdapterKt$asListenableFuture$1$1(a, kr));
        return o;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.customaudience;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.privacysandbox.ads.adservices.java.customaudience.CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync$1", f = "CustomAudienceManagerFutures.kt", l = { 123 }, m = "invokeSuspend")
final class CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync$1 extends SuspendLambda implements q90
{
    final hj0 $request;
    int label;
    final a.a this$0;
    
    public CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync$1(final a.a a, final hj0 hj0, final vl vl) {
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync$1(null, null, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        final int label = this.label;
        if (label == 0) {
            xe1.b(o);
            a.a.a(null);
            fg0.b((Object)null);
            this.label = 1;
            throw null;
        }
        if (label == 1) {
            xe1.b(o);
            return u02.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.migrations;

import org.jetbrains.annotations.NotNull;
import android.content.SharedPreferences;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

final class SharedPreferencesMigration$4 extends Lambda implements a90
{
    final Context $context;
    final String $sharedPreferencesName;
    
    public SharedPreferencesMigration$4(final Context $context, final String $sharedPreferencesName) {
        this.$context = $context;
        this.$sharedPreferencesName = $sharedPreferencesName;
        super(0);
    }
    
    @NotNull
    public final SharedPreferences invoke() {
        final SharedPreferences sharedPreferences = this.$context.getSharedPreferences(this.$sharedPreferencesName, 0);
        fg0.d((Object)sharedPreferences, "context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE)");
        return sharedPreferences;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.migrations;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.datastore.migrations.SharedPreferencesMigration$2", f = "SharedPreferencesMigration.kt", l = {}, m = "invokeSuspend")
final class SharedPreferencesMigration$2 extends SuspendLambda implements q90
{
    int label;
    
    public SharedPreferencesMigration$2(final vl vl) {
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new SharedPreferencesMigration$2(vl);
    }
    
    @Nullable
    public final Object invoke(final Object o, @Nullable final vl vl) {
        return ((SharedPreferencesMigration$2)this.create(o, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        if (this.label == 0) {
            xe1.b(o);
            return pc.a(true);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.migrations;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "androidx.datastore.migrations.SharedPreferencesMigration", f = "SharedPreferencesMigration.kt", l = { 147 }, m = "shouldMigrate")
final class SharedPreferencesMigration$shouldMigrate$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final mn1 this$0;
    
    public SharedPreferencesMigration$shouldMigrate$1(final mn1 mn1, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

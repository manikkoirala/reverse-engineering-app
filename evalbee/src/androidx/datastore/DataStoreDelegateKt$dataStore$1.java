// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

final class DataStoreDelegateKt$dataStore$1 extends Lambda implements c90
{
    public static final DataStoreDelegateKt$dataStore$1 INSTANCE;
    
    static {
        INSTANCE = new DataStoreDelegateKt$dataStore$1();
    }
    
    public DataStoreDelegateKt$dataStore$1() {
        super(1);
    }
    
    @NotNull
    public final List invoke(@NotNull final Context context) {
        fg0.e((Object)context, "it");
        return nh.g();
    }
}

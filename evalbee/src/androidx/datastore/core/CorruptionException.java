// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.core;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;

public final class CorruptionException extends IOException
{
    public CorruptionException(@NotNull final String message, @Nullable final Throwable cause) {
        fg0.e((Object)message, "message");
        super(message, cause);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.core;

import java.util.ArrayList;
import kotlin.jvm.internal.Ref$ObjectRef;
import java.util.Iterator;
import java.util.List;

public abstract class DataMigrationInitializer
{
    public static final Companion a;
    
    static {
        a = new Companion(null);
    }
    
    public static final class Companion
    {
        public final q90 b(final List list) {
            fg0.e((Object)list, "migrations");
            return (q90)new DataMigrationInitializer$Companion$getInitializer.DataMigrationInitializer$Companion$getInitializer$1(list, (vl)null);
        }
        
        public final Object c(List o, final bf0 bf0, final vl vl) {
            Object o2 = null;
            Label_0054: {
                if (vl instanceof DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1) {
                    final DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1 dataMigrationInitializer$Companion$runMigrations$1 = (DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)vl;
                    final int label = dataMigrationInitializer$Companion$runMigrations$1.label;
                    if ((label & Integer.MIN_VALUE) != 0x0) {
                        dataMigrationInitializer$Companion$runMigrations$1.label = label + Integer.MIN_VALUE;
                        o2 = dataMigrationInitializer$Companion$runMigrations$1;
                        break Label_0054;
                    }
                }
                o2 = new DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1(this, vl);
            }
            final Object result = ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).result;
            final Object d = gg0.d();
            final int label2 = ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).label;
            Iterator iterator;
            Object l$0 = null;
            DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1 dataMigrationInitializer$Companion$runMigrations$2;
            Iterator iterator2;
            Ref$ObjectRef ref$ObjectRef;
            Object element;
            final Throwable element2;
            c90 c90;
            ArrayList l$2;
            DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$2 dataMigrationInitializer$Companion$runMigrations$3;
            Label_0372:Label_0207_Outer:
            while (true) {
                Label_0151: {
                    if (label2 == 0) {
                        break Label_0151;
                    }
                    Label_0135: {
                        if (label2 == 1) {
                            break Label_0135;
                        }
                        Label_0125: {
                            if (label2 != 2) {
                                break Label_0125;
                            }
                            iterator = (Iterator)((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).L$1;
                            l$0 = ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).L$0;
                            dataMigrationInitializer$Companion$runMigrations$2 = (DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2;
                            iterator2 = iterator;
                            ref$ObjectRef = (Ref$ObjectRef)l$0;
                            try {
                                xe1.b(result);
                                break Label_0225;
                            }
                            finally {
                                element = ref$ObjectRef.element;
                                if (element == null) {
                                    ref$ObjectRef.element = element2;
                                    o2 = dataMigrationInitializer$Companion$runMigrations$2;
                                    iterator = iterator2;
                                    l$0 = ref$ObjectRef;
                                }
                                else {
                                    fg0.b(element);
                                    my.a((Throwable)ref$ObjectRef.element, element2);
                                    o2 = dataMigrationInitializer$Companion$runMigrations$2;
                                    iterator = iterator2;
                                    l$0 = ref$ObjectRef;
                                }
                                while (iterator.hasNext()) {
                                    c90 = iterator.next();
                                    ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).L$0 = l$0;
                                    ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).L$1 = iterator;
                                    ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).label = 2;
                                    if (c90.invoke(o2) == d) {
                                        return d;
                                    }
                                }
                                break Label_0372;
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                                Label_0204: {
                                    o = l$2;
                                }
                                while (true) {
                                    l$0 = new Ref$ObjectRef();
                                    iterator = ((Iterable)o).iterator();
                                    continue Label_0207_Outer;
                                    o = ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).L$0;
                                    xe1.b(result);
                                    continue;
                                }
                                xe1.b(result);
                                l$2 = new ArrayList();
                                dataMigrationInitializer$Companion$runMigrations$3 = new DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$2((List)o, (List)l$2, (vl)null);
                                ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).L$0 = l$2;
                                ((DataMigrationInitializer$Companion$runMigrations.DataMigrationInitializer$Companion$runMigrations$1)o2).label = 1;
                                iftrue(Label_0204:)(bf0.a((q90)dataMigrationInitializer$Companion$runMigrations$3, (vl)o2) != d);
                                return d;
                            }
                        }
                    }
                }
                continue;
            }
            final Throwable t = (Throwable)((Ref$ObjectRef)l$0).element;
            if (t == null) {
                return u02.a;
            }
            throw t;
        }
    }
}

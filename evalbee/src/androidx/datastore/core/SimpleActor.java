// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.core;

import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.ClosedSendChannelException;
import kotlinx.coroutines.channels.b$a;
import kotlinx.coroutines.channels.b;
import org.jetbrains.annotations.Nullable;
import kotlin.jvm.internal.Lambda;
import kotlin.coroutines.CoroutineContext$b;
import kotlinx.coroutines.n;
import kotlinx.coroutines.channels.BufferOverflow;
import java.util.concurrent.atomic.AtomicInteger;

public final class SimpleActor
{
    public final lm a;
    public final q90 b;
    public final vf c;
    public final AtomicInteger d;
    
    public SimpleActor(final lm a, final c90 c90, final q90 q90, final q90 b) {
        fg0.e((Object)a, "scope");
        fg0.e((Object)c90, "onComplete");
        fg0.e((Object)q90, "onUndeliveredElement");
        fg0.e((Object)b, "consumeMessage");
        this.a = a;
        this.b = b;
        this.c = bg.b(Integer.MAX_VALUE, (BufferOverflow)null, (c90)null, 6, (Object)null);
        this.d = new AtomicInteger(0);
        final n n = (n)a.C().get((CoroutineContext$b)kotlinx.coroutines.n.r1);
        if (n != null) {
            n.q((c90)new c90(c90, this, q90) {
                final c90 $onComplete;
                final q90 $onUndeliveredElement;
                final SimpleActor this$0;
                
                public final void invoke(@Nullable final Throwable t) {
                    this.$onComplete.invoke((Object)t);
                    ((zk1)SimpleActor.b(this.this$0)).B(t);
                    u02 a;
                    do {
                        final Object f = kotlinx.coroutines.channels.b.f(((ReceiveChannel)SimpleActor.b(this.this$0)).p());
                        if (f == null) {
                            a = null;
                        }
                        else {
                            this.$onUndeliveredElement.invoke(f, (Object)t);
                            a = u02.a;
                        }
                    } while (a != null);
                }
            });
        }
    }
    
    public static final /* synthetic */ vf b(final SimpleActor simpleActor) {
        return simpleActor.c;
    }
    
    public final void e(Object m) {
        m = ((zk1)this.c).m(m);
        if (m instanceof b$a) {
            Object e;
            if ((e = kotlinx.coroutines.channels.b.e(m)) == null) {
                e = new ClosedSendChannelException("Channel was closed normally");
            }
            throw e;
        }
        if (kotlinx.coroutines.channels.b.i(m)) {
            if (this.d.getAndIncrement() == 0) {
                ad.d(this.a, (CoroutineContext)null, (CoroutineStart)null, (q90)new SimpleActor$offer.SimpleActor$offer$2(this, (vl)null), 3, (Object)null);
            }
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }
}

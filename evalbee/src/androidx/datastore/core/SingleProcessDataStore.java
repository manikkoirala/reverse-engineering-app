// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.core;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.io.Closeable;
import java.io.InputStream;
import java.io.FileInputStream;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlinx.coroutines.sync.MutexKt;
import java.util.Iterator;
import kotlin.Result$a;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Result;
import java.io.IOException;
import kotlinx.coroutines.n;
import kotlin.coroutines.CoroutineContext;
import java.io.File;
import kotlin.a;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class SingleProcessDataStore implements jp
{
    public static final a k;
    public static final Set l;
    public static final Object m;
    public final a90 a;
    public final nl1 b;
    public final pm c;
    public final lm d;
    public final y40 e;
    public final String f;
    public final xi0 g;
    public final vx0 h;
    public List i;
    public final SimpleActor j;
    
    static {
        k = new a(null);
        l = new LinkedHashSet();
        m = new Object();
    }
    
    public SingleProcessDataStore(final a90 a, final nl1 b, final List list, final pm c, final lm d) {
        fg0.e((Object)a, "produceFile");
        fg0.e((Object)b, "serializer");
        fg0.e((Object)list, "initTasksList");
        fg0.e((Object)c, "corruptionHandler");
        fg0.e((Object)d, "scope");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = d50.u((q90)new SingleProcessDataStore$data.SingleProcessDataStore$data$1(this, (vl)null));
        this.f = ".tmp";
        this.g = kotlin.a.a((a90)new SingleProcessDataStore$file.SingleProcessDataStore$file$2(this));
        this.h = wp1.a((Object)o02.a);
        this.i = vh.P((Iterable)list);
        this.j = new SimpleActor(d, (c90)new SingleProcessDataStore$actor.SingleProcessDataStore$actor$1(this), (q90)SingleProcessDataStore$actor.SingleProcessDataStore$actor$2.INSTANCE, (q90)new SingleProcessDataStore$actor.SingleProcessDataStore$actor$3(this, (vl)null));
    }
    
    public static final /* synthetic */ Set b() {
        return SingleProcessDataStore.l;
    }
    
    public static final /* synthetic */ Object c() {
        return SingleProcessDataStore.m;
    }
    
    @Override
    public Object a(final q90 q90, final vl vl) {
        final mi b = oi.b((n)null, 1, (Object)null);
        this.j.e(new b.b(q90, b, (up1)this.h.getValue(), vl.getContext()));
        return ((kr)b).i0(vl);
    }
    
    @Override
    public y40 getData() {
        return this.e;
    }
    
    public final void p(final File file) {
        final File parentFile = file.getCanonicalFile().getParentFile();
        if (parentFile != null) {
            parentFile.mkdirs();
            if (!parentFile.isDirectory()) {
                throw new IOException(fg0.m("Unable to create parent directories of ", (Object)file));
            }
        }
    }
    
    public final File q() {
        return (File)this.g.getValue();
    }
    
    public final Object r(final b.a a, final vl vl) {
        final up1 up1 = (up1)this.h.getValue();
        if (!(up1 instanceof cp)) {
            if (up1 instanceof oc1) {
                if (up1 == a.a()) {
                    final Object v = this.v(vl);
                    if (v == gg0.d()) {
                        return v;
                    }
                    return u02.a;
                }
            }
            else if (fg0.a((Object)up1, (Object)o02.a)) {
                final Object v2 = this.v(vl);
                if (v2 == gg0.d()) {
                    return v2;
                }
                return u02.a;
            }
            else if (up1 instanceof n10) {
                throw new IllegalStateException("Can't read in final state.".toString());
            }
        }
        return u02.a;
    }
    
    public final Object s(b.b l$0, final vl vl) {
        SingleProcessDataStore$handleUpdate.SingleProcessDataStore$handleUpdate$1 singleProcessDataStore$handleUpdate$1 = null;
        Label_0051: {
            if (vl instanceof SingleProcessDataStore$handleUpdate.SingleProcessDataStore$handleUpdate$1) {
                singleProcessDataStore$handleUpdate$1 = (SingleProcessDataStore$handleUpdate.SingleProcessDataStore$handleUpdate$1)vl;
                final int label = singleProcessDataStore$handleUpdate$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    singleProcessDataStore$handleUpdate$1.label = label + Integer.MIN_VALUE;
                    break Label_0051;
                }
            }
            singleProcessDataStore$handleUpdate$1 = new SingleProcessDataStore$handleUpdate.SingleProcessDataStore$handleUpdate$1(this, vl);
        }
        Object o = singleProcessDataStore$handleUpdate$1.result;
        final Object d = gg0.d();
        final int label2 = singleProcessDataStore$handleUpdate$1.label;
        boolean b = true;
        Object constructor-impl = null;
        Label_0568: {
            Label_0480: {
                if (label2 != 0) {
                    Label_0156: {
                        if (label2 == 1) {
                            break Label_0156;
                        }
                        Label_0165: {
                            if (label2 != 2) {
                                if (label2 == 3) {
                                    final Object o2 = singleProcessDataStore$handleUpdate$1.L$0;
                                    break Label_0165;
                                }
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                            }
                            final Object o3 = singleProcessDataStore$handleUpdate$1.L$2;
                            final Object o4 = singleProcessDataStore$handleUpdate$1.L$1;
                            final Object o5 = singleProcessDataStore$handleUpdate$1.L$0;
                            l$0 = (b.b)o3;
                            try {
                                xe1.b(o);
                                l$0 = (b.b)o4;
                                break Label_0480;
                                final Object o2 = singleProcessDataStore$handleUpdate$1.L$0;
                                l$0 = (b.b)o2;
                                xe1.b(o);
                                break Label_0480;
                            }
                            finally {
                                break Label_0480;
                            }
                        }
                    }
                }
                xe1.b(o);
                constructor-impl = (o = l$0.a());
                try {
                    final Result$a companion = Result.Companion;
                    o = constructor-impl;
                    final up1 up1 = (up1)this.h.getValue();
                    o = constructor-impl;
                    Object o6;
                    if (up1 instanceof cp) {
                        o = constructor-impl;
                        final q90 d2 = l$0.d();
                        o = constructor-impl;
                        final CoroutineContext b2 = l$0.b();
                        o = constructor-impl;
                        singleProcessDataStore$handleUpdate$1.L$0 = constructor-impl;
                        o = constructor-impl;
                        singleProcessDataStore$handleUpdate$1.label = 1;
                        o = constructor-impl;
                        if ((o6 = this.y(d2, b2, (vl)singleProcessDataStore$handleUpdate$1)) == d) {
                            return d;
                        }
                    }
                    else {
                        o = constructor-impl;
                        if (!(up1 instanceof oc1)) {
                            o = constructor-impl;
                            b = (up1 instanceof o02);
                        }
                        if (b) {
                            o = constructor-impl;
                            if (up1 != l$0.c()) {
                                o = constructor-impl;
                                throw ((oc1)up1).a();
                            }
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.L$0 = l$0;
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.L$1 = this;
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.L$2 = constructor-impl;
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.label = 2;
                            o = constructor-impl;
                            if (this.u((vl)singleProcessDataStore$handleUpdate$1) == d) {
                                return d;
                            }
                            o = this;
                            final Object o5 = l$0;
                            l$0 = (b.b)o;
                            o = constructor-impl;
                            final q90 d3 = ((b.b)o5).d();
                            o = constructor-impl;
                            final CoroutineContext b3 = ((b.b)o5).b();
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.L$0 = constructor-impl;
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.L$1 = null;
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.L$2 = null;
                            o = constructor-impl;
                            singleProcessDataStore$handleUpdate$1.label = 3;
                            o = constructor-impl;
                            if ((o6 = ((SingleProcessDataStore)l$0).y(d3, b3, (vl)singleProcessDataStore$handleUpdate$1)) == d) {
                                return d;
                            }
                        }
                        else {
                            o = constructor-impl;
                            if (up1 instanceof n10) {
                                o = constructor-impl;
                                throw ((n10)up1).a();
                            }
                            o = constructor-impl;
                            o = constructor-impl;
                            final NoWhenBranchMatchedException ex = new NoWhenBranchMatchedException();
                            o = constructor-impl;
                            throw ex;
                        }
                    }
                    o = o6;
                    o = Result.constructor-impl(o);
                    break Label_0568;
                }
                finally {
                    l$0 = (b.b)o;
                }
            }
            final Result$a companion2 = Result.Companion;
            constructor-impl = Result.constructor-impl(xe1.a((Throwable)constructor-impl));
        }
        oi.c((mi)l$0, constructor-impl);
        return u02.a;
    }
    
    public final Object t(final vl vl) {
        SingleProcessDataStore$readAndInit.SingleProcessDataStore$readAndInit$1 singleProcessDataStore$readAndInit$1 = null;
        Label_0051: {
            if (vl instanceof SingleProcessDataStore$readAndInit.SingleProcessDataStore$readAndInit$1) {
                singleProcessDataStore$readAndInit$1 = (SingleProcessDataStore$readAndInit.SingleProcessDataStore$readAndInit$1)vl;
                final int label = singleProcessDataStore$readAndInit$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    singleProcessDataStore$readAndInit$1.label = label + Integer.MIN_VALUE;
                    break Label_0051;
                }
            }
            singleProcessDataStore$readAndInit$1 = new SingleProcessDataStore$readAndInit.SingleProcessDataStore$readAndInit$1(this, vl);
        }
        Object element = singleProcessDataStore$readAndInit$1.result;
        final Object d = gg0.d();
        final int label2 = singleProcessDataStore$readAndInit$1.label;
        final int n = 0;
        Label_0662: {
            by0 by0 = null;
            SingleProcessDataStore singleProcessDataStore2 = null;
            Object o5 = null;
            Object o6 = null;
            Label_0583: {
                Object o2 = null;
                by0 b = null;
                SingleProcessDataStore singleProcessDataStore = null;
                Ref$BooleanRef ref$BooleanRef;
                while (true) {
                    Object o = null;
                    Label_0392: {
                        Object l$4;
                        Iterator iterator2;
                        if (label2 != 0) {
                            if (label2 == 1) {
                                o = singleProcessDataStore$readAndInit$1.L$3;
                                o2 = singleProcessDataStore$readAndInit$1.L$2;
                                b = (by0)singleProcessDataStore$readAndInit$1.L$1;
                                singleProcessDataStore = (SingleProcessDataStore)singleProcessDataStore$readAndInit$1.L$0;
                                xe1.b(element);
                                break Label_0392;
                            }
                            if (label2 != 2) {
                                if (label2 == 3) {
                                    by0 = (by0)singleProcessDataStore$readAndInit$1.L$3;
                                    final Object o3 = singleProcessDataStore$readAndInit$1.L$2;
                                    final Object o4 = singleProcessDataStore$readAndInit$1.L$1;
                                    singleProcessDataStore2 = (SingleProcessDataStore)singleProcessDataStore$readAndInit$1.L$0;
                                    xe1.b(element);
                                    break Label_0662;
                                }
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                            }
                            else {
                                final Iterator iterator = (Iterator)singleProcessDataStore$readAndInit$1.L$5;
                                l$4 = singleProcessDataStore$readAndInit$1.L$4;
                                o5 = singleProcessDataStore$readAndInit$1.L$3;
                                o6 = singleProcessDataStore$readAndInit$1.L$2;
                                by0 = (by0)singleProcessDataStore$readAndInit$1.L$1;
                                singleProcessDataStore2 = (SingleProcessDataStore)singleProcessDataStore$readAndInit$1.L$0;
                                xe1.b(element);
                                iterator2 = iterator;
                            }
                        }
                        else {
                            xe1.b(element);
                            if (!fg0.a(this.h.getValue(), (Object)o02.a) && !(this.h.getValue() instanceof oc1)) {
                                throw new IllegalStateException("Check failed.".toString());
                            }
                            b = MutexKt.b(false, 1, (Object)null);
                            o = new Ref$ObjectRef();
                            singleProcessDataStore$readAndInit$1.L$0 = this;
                            singleProcessDataStore$readAndInit$1.L$1 = b;
                            singleProcessDataStore$readAndInit$1.L$2 = o;
                            singleProcessDataStore$readAndInit$1.L$3 = o;
                            singleProcessDataStore$readAndInit$1.label = 1;
                            element = this.x((vl)singleProcessDataStore$readAndInit$1);
                            if (element == d) {
                                return d;
                            }
                            singleProcessDataStore = this;
                            o2 = o;
                            break Label_0392;
                        }
                        while (iterator2.hasNext()) {
                            final q90 q90 = iterator2.next();
                            singleProcessDataStore$readAndInit$1.L$0 = singleProcessDataStore2;
                            singleProcessDataStore$readAndInit$1.L$1 = by0;
                            singleProcessDataStore$readAndInit$1.L$2 = o6;
                            singleProcessDataStore$readAndInit$1.L$3 = o5;
                            singleProcessDataStore$readAndInit$1.L$4 = l$4;
                            singleProcessDataStore$readAndInit$1.L$5 = iterator2;
                            singleProcessDataStore$readAndInit$1.label = 2;
                            if (q90.invoke(l$4, (Object)singleProcessDataStore$readAndInit$1) == d) {
                                return d;
                            }
                        }
                        break Label_0583;
                    }
                    ((Ref$ObjectRef)o).element = element;
                    ref$BooleanRef = new Ref$BooleanRef();
                    Object l$4 = new SingleProcessDataStore$readAndInit$api.SingleProcessDataStore$readAndInit$api$1(b, ref$BooleanRef, (Ref$ObjectRef)o2, singleProcessDataStore);
                    final List i = singleProcessDataStore.i;
                    if (i != null) {
                        final Iterator iterator2 = i.iterator();
                        final by0 by2 = b;
                        final Ref$BooleanRef ref$BooleanRef2 = ref$BooleanRef;
                        singleProcessDataStore2 = singleProcessDataStore;
                        by0 = by2;
                        o6 = o2;
                        o5 = ref$BooleanRef2;
                        continue;
                    }
                    break;
                }
                final by0 by3 = b;
                final Ref$BooleanRef ref$BooleanRef3 = ref$BooleanRef;
                singleProcessDataStore2 = singleProcessDataStore;
                by0 = by3;
                o6 = o2;
                o5 = ref$BooleanRef3;
            }
            singleProcessDataStore2.i = null;
            singleProcessDataStore$readAndInit$1.L$0 = singleProcessDataStore2;
            singleProcessDataStore$readAndInit$1.L$1 = o6;
            singleProcessDataStore$readAndInit$1.L$2 = o5;
            singleProcessDataStore$readAndInit$1.L$3 = by0;
            singleProcessDataStore$readAndInit$1.L$4 = null;
            singleProcessDataStore$readAndInit$1.L$5 = null;
            singleProcessDataStore$readAndInit$1.label = 3;
            if (by0.d((Object)null, (vl)singleProcessDataStore$readAndInit$1) == d) {
                return d;
            }
            final Object o7 = o5;
            final Object o4 = o6;
            final Object o3 = o7;
            try {
                ((Ref$BooleanRef)o3).element = true;
                final u02 a = u02.a;
                by0.c((Object)null);
                final vx0 h = singleProcessDataStore2.h;
                final Object element2 = ((Ref$ObjectRef)o4).element;
                int hashCode = n;
                if (element2 != null) {
                    hashCode = element2.hashCode();
                }
                h.setValue((Object)new cp(element2, hashCode));
                return u02.a;
            }
            finally {
                by0.c((Object)null);
            }
        }
        throw new IllegalStateException("Check failed.".toString());
    }
    
    public final Object u(final vl vl) {
        SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure.SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1 singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2 = null;
        Label_0049: {
            if (vl instanceof SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure.SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1) {
                final SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure.SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1 singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1 = (SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure.SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1)vl;
                final int label = singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1.label = label + Integer.MIN_VALUE;
                    singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2 = singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1;
                    break Label_0049;
                }
            }
            singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2 = new SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure.SingleProcessDataStore$readAndInitOrPropagateAndThrowFailure$1(this, vl);
        }
        final Object result = singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2.result;
        final Object d = gg0.d();
        final int label2 = singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2.label;
        SingleProcessDataStore singleProcessDataStore = null;
        Label_0132: {
            if (label2 != 0) {
                if (label2 == 1) {
                    singleProcessDataStore = (SingleProcessDataStore)singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2.L$0;
                    try {
                        xe1.b(result);
                        break Label_0132;
                    }
                    finally {
                        break Label_0132;
                    }
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(result);
            try {
                singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2.L$0 = this;
                singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2.label = 1;
                if (this.t((vl)singleProcessDataStore$readAndInitOrPropagateAndThrowFailure$2) == d) {
                    return d;
                }
                return u02.a;
            }
            finally {
                singleProcessDataStore = this;
            }
        }
        final Throwable t;
        singleProcessDataStore.h.setValue((Object)new oc1(t));
        throw t;
    }
    
    public final Object v(vl vl) {
        SingleProcessDataStore$readAndInitOrPropagateFailure.SingleProcessDataStore$readAndInitOrPropagateFailure$1 singleProcessDataStore$readAndInitOrPropagateFailure$2 = null;
        Label_0049: {
            if (vl instanceof SingleProcessDataStore$readAndInitOrPropagateFailure.SingleProcessDataStore$readAndInitOrPropagateFailure$1) {
                final SingleProcessDataStore$readAndInitOrPropagateFailure.SingleProcessDataStore$readAndInitOrPropagateFailure$1 singleProcessDataStore$readAndInitOrPropagateFailure$1 = (SingleProcessDataStore$readAndInitOrPropagateFailure.SingleProcessDataStore$readAndInitOrPropagateFailure$1)vl;
                final int label = singleProcessDataStore$readAndInitOrPropagateFailure$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    singleProcessDataStore$readAndInitOrPropagateFailure$1.label = label + Integer.MIN_VALUE;
                    singleProcessDataStore$readAndInitOrPropagateFailure$2 = singleProcessDataStore$readAndInitOrPropagateFailure$1;
                    break Label_0049;
                }
            }
            singleProcessDataStore$readAndInitOrPropagateFailure$2 = new SingleProcessDataStore$readAndInitOrPropagateFailure.SingleProcessDataStore$readAndInitOrPropagateFailure$1(this, vl);
        }
        final Object result = singleProcessDataStore$readAndInitOrPropagateFailure$2.result;
        final Object d = gg0.d();
        final int label2 = singleProcessDataStore$readAndInitOrPropagateFailure$2.label;
        Label_0135: {
            if (label2 != 0) {
                if (label2 == 1) {
                    vl = (vl)singleProcessDataStore$readAndInitOrPropagateFailure$2.L$0;
                    try {
                        xe1.b(result);
                        return u02.a;
                    }
                    finally {
                        break Label_0135;
                    }
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(result);
            try {
                singleProcessDataStore$readAndInitOrPropagateFailure$2.L$0 = this;
                singleProcessDataStore$readAndInitOrPropagateFailure$2.label = 1;
                if (this.t((vl)singleProcessDataStore$readAndInitOrPropagateFailure$2) == d) {
                    return d;
                }
                return u02.a;
            }
            finally {
                vl = (vl)this;
            }
        }
        final Throwable t;
        ((SingleProcessDataStore)vl).h.setValue((Object)new oc1(t));
        return u02.a;
    }
    
    public final Object w(vl l$1) {
        Object o = null;
        Label_0047: {
            if (l$1 instanceof SingleProcessDataStore$readData.SingleProcessDataStore$readData$1) {
                o = l$1;
                final int label = ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).label = label + Integer.MIN_VALUE;
                    break Label_0047;
                }
            }
            o = new SingleProcessDataStore$readData.SingleProcessDataStore$readData$1(this, l$1);
        }
        final Object result = ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).label;
        Object o3 = null;
        Label_0238: {
            if (label2 != 0) {
                if (label2 == 1) {
                    final Throwable t = (Throwable)((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).L$2;
                    final Object o2 = ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).L$1;
                    l$1 = (vl)((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).L$0;
                    try {
                        xe1.b(result);
                    }
                    finally {
                        o3 = l$1;
                        final Object o4 = o2;
                        final Throwable t2;
                        final Object a = t2;
                        break Label_0238;
                    }
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(result);
            try {
                l$1 = (vl)new FileInputStream(this.q());
                Object o4;
                Object a;
                try {
                    final nl1 b = this.b;
                    ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).L$0 = this;
                    ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).L$1 = l$1;
                    ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).L$2 = null;
                    ((SingleProcessDataStore$readData.SingleProcessDataStore$readData$1)o).label = 1;
                    a = b.a((InputStream)l$1, (vl)o);
                    if (a == d) {
                        return d;
                    }
                    o3 = null;
                    o4 = l$1;
                    l$1 = (vl)this;
                    try {
                        dh.a((Closeable)o4, (Throwable)o3);
                        return a;
                    }
                    catch (final FileNotFoundException ex) {}
                }
                finally {
                    o3 = this;
                    o4 = l$1;
                }
                try {
                    throw a;
                }
                finally {
                    dh.a((Closeable)o4, (Throwable)a);
                }
            }
            catch (final FileNotFoundException o3) {
                l$1 = (vl)this;
            }
        }
        if (!((SingleProcessDataStore)l$1).q().exists()) {
            return ((SingleProcessDataStore)l$1).b.getDefaultValue();
        }
        throw o3;
    }
    
    public final Object x(vl l$1) {
        Object o = null;
        Label_0051: {
            if (l$1 instanceof SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1) {
                o = l$1;
                final int label = ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).label = label + Integer.MIN_VALUE;
                    break Label_0051;
                }
            }
            o = new SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1(this, l$1);
        }
        Object l$2 = ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).label;
        Object o2 = null;
        IOException ex = null;
        Label_0268: {
            Label_0210: {
                if (label2 != 0) {
                    if (label2 != 1) {
                        if (label2 != 2) {
                            if (label2 == 3) {
                                l$1 = (vl)((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$1;
                                o2 = ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$0;
                                try {
                                    xe1.b(l$2);
                                    return l$1;
                                }
                                catch (final IOException ex) {
                                    break Label_0268;
                                }
                            }
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        l$1 = (vl)((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$1;
                        final Object o3 = ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$0;
                        xe1.b(l$2);
                        break Label_0268;
                    }
                    else {
                        l$1 = (vl)((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$0;
                        try {
                            xe1.b(l$2);
                            return l$2;
                        }
                        catch (final CorruptionException l$3) {
                            break Label_0210;
                        }
                    }
                }
                xe1.b(l$2);
                try {
                    ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$0 = this;
                    ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).label = 1;
                    if ((l$2 = this.w((vl)o)) == d) {
                        return d;
                    }
                    return l$2;
                }
                catch (final CorruptionException l$3) {
                    l$1 = (vl)this;
                }
            }
            final pm c = ((SingleProcessDataStore)l$1).c;
            ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$0 = l$1;
            final CorruptionException l$3;
            ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$1 = l$3;
            ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).label = 2;
            final Object a = c.a(l$3, (vl)o);
            if (a == d) {
                return d;
            }
            final vl vl = (vl)l$3;
            l$2 = a;
            final Object o3 = l$1;
            l$1 = vl;
            try {
                ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$0 = l$1;
                ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).L$1 = l$2;
                ((SingleProcessDataStore$readDataOrHandleCorruption.SingleProcessDataStore$readDataOrHandleCorruption$1)o).label = 3;
                if (((SingleProcessDataStore)o3).z(l$2, (vl)o) == d) {
                    return d;
                }
                l$1 = (vl)l$2;
                return l$1;
            }
            catch (final IOException ex2) {
                o2 = l$1;
                ex = ex2;
            }
        }
        my.a((Throwable)o2, (Throwable)ex);
        throw o2;
    }
    
    public final Object y(final q90 q90, final CoroutineContext coroutineContext, final vl vl) {
        SingleProcessDataStore$transformAndWrite.SingleProcessDataStore$transformAndWrite$1 singleProcessDataStore$transformAndWrite$1 = null;
        Label_0054: {
            if (vl instanceof SingleProcessDataStore$transformAndWrite.SingleProcessDataStore$transformAndWrite$1) {
                singleProcessDataStore$transformAndWrite$1 = (SingleProcessDataStore$transformAndWrite.SingleProcessDataStore$transformAndWrite$1)vl;
                final int label = singleProcessDataStore$transformAndWrite$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    singleProcessDataStore$transformAndWrite$1.label = label + Integer.MIN_VALUE;
                    break Label_0054;
                }
            }
            singleProcessDataStore$transformAndWrite$1 = new SingleProcessDataStore$transformAndWrite.SingleProcessDataStore$transformAndWrite$1(this, vl);
        }
        final Object result = singleProcessDataStore$transformAndWrite$1.result;
        final Object d = gg0.d();
        final int label2 = singleProcessDataStore$transformAndWrite$1.label;
        Object l$1 = null;
        SingleProcessDataStore singleProcessDataStore = null;
        Label_0306: {
            Object l$2;
            cp l$3;
            SingleProcessDataStore l$4;
            Object g;
            if (label2 != 0) {
                if (label2 != 1) {
                    if (label2 == 2) {
                        l$1 = singleProcessDataStore$transformAndWrite$1.L$1;
                        singleProcessDataStore = (SingleProcessDataStore)singleProcessDataStore$transformAndWrite$1.L$0;
                        xe1.b(result);
                        break Label_0306;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                else {
                    l$2 = singleProcessDataStore$transformAndWrite$1.L$2;
                    l$3 = (cp)singleProcessDataStore$transformAndWrite$1.L$1;
                    l$4 = (SingleProcessDataStore)singleProcessDataStore$transformAndWrite$1.L$0;
                    xe1.b(result);
                    g = result;
                }
            }
            else {
                xe1.b(result);
                l$3 = (cp)this.h.getValue();
                l$3.a();
                l$2 = l$3.b();
                final SingleProcessDataStore$transformAndWrite$newData.SingleProcessDataStore$transformAndWrite$newData$1 singleProcessDataStore$transformAndWrite$newData$1 = new SingleProcessDataStore$transformAndWrite$newData.SingleProcessDataStore$transformAndWrite$newData$1(q90, l$2, (vl)null);
                singleProcessDataStore$transformAndWrite$1.L$0 = this;
                singleProcessDataStore$transformAndWrite$1.L$1 = l$3;
                singleProcessDataStore$transformAndWrite$1.L$2 = l$2;
                singleProcessDataStore$transformAndWrite$1.label = 1;
                g = ad.g(coroutineContext, (q90)singleProcessDataStore$transformAndWrite$newData$1, (vl)singleProcessDataStore$transformAndWrite$1);
                if (g == d) {
                    return d;
                }
                l$4 = this;
            }
            l$3.a();
            if (fg0.a(l$2, g)) {
                l$1 = l$2;
                return l$1;
            }
            singleProcessDataStore$transformAndWrite$1.L$0 = l$4;
            singleProcessDataStore$transformAndWrite$1.L$1 = g;
            singleProcessDataStore$transformAndWrite$1.L$2 = null;
            singleProcessDataStore$transformAndWrite$1.label = 2;
            if (l$4.z(g, (vl)singleProcessDataStore$transformAndWrite$1) == d) {
                return d;
            }
            singleProcessDataStore = l$4;
            l$1 = g;
        }
        final vx0 h = singleProcessDataStore.h;
        int hashCode;
        if (l$1 != null) {
            hashCode = l$1.hashCode();
        }
        else {
            hashCode = 0;
        }
        h.setValue((Object)new cp(l$1, hashCode));
        return l$1;
    }
    
    public final Object z(final Object p0, final vl p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: instanceof      Landroidx/datastore/core/SingleProcessDataStore$writeData$1;
        //     4: ifeq            40
        //     7: aload_2        
        //     8: checkcast       Landroidx/datastore/core/SingleProcessDataStore$writeData$1;
        //    11: astore          6
        //    13: aload           6
        //    15: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.label:I
        //    18: istore_3       
        //    19: iload_3        
        //    20: ldc_w           -2147483648
        //    23: iand           
        //    24: ifeq            40
        //    27: aload           6
        //    29: iload_3        
        //    30: ldc_w           -2147483648
        //    33: iadd           
        //    34: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.label:I
        //    37: goto            51
        //    40: new             Landroidx/datastore/core/SingleProcessDataStore$writeData$1;
        //    43: dup            
        //    44: aload_0        
        //    45: aload_2        
        //    46: invokespecial   androidx/datastore/core/SingleProcessDataStore$writeData$1.<init>:(Landroidx/datastore/core/SingleProcessDataStore;Lvl;)V
        //    49: astore          6
        //    51: aload           6
        //    53: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.result:Ljava/lang/Object;
        //    56: astore          11
        //    58: invokestatic    gg0.d:()Ljava/lang/Object;
        //    61: astore          7
        //    63: aload           6
        //    65: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.label:I
        //    68: istore_3       
        //    69: iload_3        
        //    70: ifeq            175
        //    73: iload_3        
        //    74: iconst_1       
        //    75: if_icmpne       164
        //    78: aload           6
        //    80: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$4:Ljava/lang/Object;
        //    83: checkcast       Ljava/io/FileOutputStream;
        //    86: astore          10
        //    88: aload           6
        //    90: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$3:Ljava/lang/Object;
        //    93: checkcast       Ljava/lang/Throwable;
        //    96: astore          8
        //    98: aload           6
        //   100: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$2:Ljava/lang/Object;
        //   103: checkcast       Ljava/io/Closeable;
        //   106: astore          9
        //   108: aload           6
        //   110: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$1:Ljava/lang/Object;
        //   113: checkcast       Ljava/io/File;
        //   116: astore_1       
        //   117: aload           6
        //   119: getfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$0:Ljava/lang/Object;
        //   122: checkcast       Landroidx/datastore/core/SingleProcessDataStore;
        //   125: astore          7
        //   127: aload           9
        //   129: astore          5
        //   131: aload_1        
        //   132: astore_2       
        //   133: aload           11
        //   135: invokestatic    xe1.b:(Ljava/lang/Object;)V
        //   138: aload           9
        //   140: astore          6
        //   142: aload           10
        //   144: astore          9
        //   146: goto            312
        //   149: astore          7
        //   151: aload           5
        //   153: astore          6
        //   155: aload_2        
        //   156: astore_1       
        //   157: aload           7
        //   159: astore          5
        //   161: goto            451
        //   164: new             Ljava/lang/IllegalStateException;
        //   167: dup            
        //   168: ldc_w           "call to 'resume' before 'invoke' with coroutine"
        //   171: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   174: athrow         
        //   175: aload           11
        //   177: invokestatic    xe1.b:(Ljava/lang/Object;)V
        //   180: aload_0        
        //   181: aload_0        
        //   182: invokevirtual   androidx/datastore/core/SingleProcessDataStore.q:()Ljava/io/File;
        //   185: invokevirtual   androidx/datastore/core/SingleProcessDataStore.p:(Ljava/io/File;)V
        //   188: new             Ljava/io/File;
        //   191: dup            
        //   192: aload_0        
        //   193: invokevirtual   androidx/datastore/core/SingleProcessDataStore.q:()Ljava/io/File;
        //   196: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   199: aload_0        
        //   200: getfield        androidx/datastore/core/SingleProcessDataStore.f:Ljava/lang/String;
        //   203: invokestatic    fg0.m:(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;
        //   206: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   209: astore          5
        //   211: new             Ljava/io/FileOutputStream;
        //   214: dup            
        //   215: aload           5
        //   217: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   220: astore_2       
        //   221: aload_0        
        //   222: getfield        androidx/datastore/core/SingleProcessDataStore.b:Lnl1;
        //   225: astore          9
        //   227: new             Landroidx/datastore/core/SingleProcessDataStore$c;
        //   230: astore          8
        //   232: aload           8
        //   234: aload_2        
        //   235: invokespecial   androidx/datastore/core/SingleProcessDataStore$c.<init>:(Ljava/io/FileOutputStream;)V
        //   238: aload           6
        //   240: aload_0        
        //   241: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$0:Ljava/lang/Object;
        //   244: aload           6
        //   246: aload           5
        //   248: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$1:Ljava/lang/Object;
        //   251: aload           6
        //   253: aload_2        
        //   254: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$2:Ljava/lang/Object;
        //   257: aload           6
        //   259: aconst_null    
        //   260: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$3:Ljava/lang/Object;
        //   263: aload           6
        //   265: aload_2        
        //   266: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.L$4:Ljava/lang/Object;
        //   269: aload           6
        //   271: iconst_1       
        //   272: putfield        androidx/datastore/core/SingleProcessDataStore$writeData$1.label:I
        //   275: aload           9
        //   277: aload_1        
        //   278: aload           8
        //   280: aload           6
        //   282: invokeinterface nl1.b:(Ljava/lang/Object;Ljava/io/OutputStream;Lvl;)Ljava/lang/Object;
        //   287: astore_1       
        //   288: aload_1        
        //   289: aload           7
        //   291: if_acmpne       297
        //   294: aload           7
        //   296: areturn        
        //   297: aload_0        
        //   298: astore          7
        //   300: aload           5
        //   302: astore_1       
        //   303: aload_2        
        //   304: astore          9
        //   306: aconst_null    
        //   307: astore          8
        //   309: aload_2        
        //   310: astore          6
        //   312: aload           6
        //   314: astore          5
        //   316: aload_1        
        //   317: astore_2       
        //   318: aload           9
        //   320: invokevirtual   java/io/FileOutputStream.getFD:()Ljava/io/FileDescriptor;
        //   323: invokevirtual   java/io/FileDescriptor.sync:()V
        //   326: aload           6
        //   328: astore          5
        //   330: aload_1        
        //   331: astore_2       
        //   332: getstatic       u02.a:Lu02;
        //   335: astore          9
        //   337: aload_1        
        //   338: astore_2       
        //   339: aload           6
        //   341: aload           8
        //   343: invokestatic    dh.a:(Ljava/io/Closeable;Ljava/lang/Throwable;)V
        //   346: aload_1        
        //   347: astore_2       
        //   348: aload_1        
        //   349: aload           7
        //   351: invokevirtual   androidx/datastore/core/SingleProcessDataStore.q:()Ljava/io/File;
        //   354: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   357: istore          4
        //   359: iload           4
        //   361: ifeq            368
        //   364: getstatic       u02.a:Lu02;
        //   367: areturn        
        //   368: aload_1        
        //   369: astore_2       
        //   370: new             Ljava/io/IOException;
        //   373: astore          5
        //   375: aload_1        
        //   376: astore_2       
        //   377: new             Ljava/lang/StringBuilder;
        //   380: astore          6
        //   382: aload_1        
        //   383: astore_2       
        //   384: aload           6
        //   386: invokespecial   java/lang/StringBuilder.<init>:()V
        //   389: aload_1        
        //   390: astore_2       
        //   391: aload           6
        //   393: ldc_w           "Unable to rename "
        //   396: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   399: pop            
        //   400: aload_1        
        //   401: astore_2       
        //   402: aload           6
        //   404: aload_1        
        //   405: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   408: pop            
        //   409: aload_1        
        //   410: astore_2       
        //   411: aload           6
        //   413: ldc_w           ".This likely means that there are multiple instances of DataStore for this file. Ensure that you are only creating a single instance of datastore for this file."
        //   416: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   419: pop            
        //   420: aload_1        
        //   421: astore_2       
        //   422: aload           5
        //   424: aload           6
        //   426: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   429: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   432: aload_1        
        //   433: astore_2       
        //   434: aload           5
        //   436: athrow         
        //   437: astore_1       
        //   438: aload           5
        //   440: astore          6
        //   442: aload_1        
        //   443: astore          5
        //   445: aload           6
        //   447: astore_1       
        //   448: aload_2        
        //   449: astore          6
        //   451: aload           5
        //   453: athrow         
        //   454: astore          7
        //   456: aload_1        
        //   457: astore_2       
        //   458: aload           6
        //   460: aload           5
        //   462: invokestatic    dh.a:(Ljava/io/Closeable;Ljava/lang/Throwable;)V
        //   465: aload_1        
        //   466: astore_2       
        //   467: aload           7
        //   469: athrow         
        //   470: astore_1       
        //   471: aload_2        
        //   472: astore          5
        //   474: goto            478
        //   477: astore_1       
        //   478: aload           5
        //   480: invokevirtual   java/io/File.exists:()Z
        //   483: ifeq            492
        //   486: aload           5
        //   488: invokevirtual   java/io/File.delete:()Z
        //   491: pop            
        //   492: aload_1        
        //   493: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  133    138    149    164    Any
        //  211    221    477    478    Ljava/io/IOException;
        //  221    288    437    451    Any
        //  318    326    149    164    Any
        //  332    337    149    164    Any
        //  339    346    470    477    Ljava/io/IOException;
        //  348    359    470    477    Ljava/io/IOException;
        //  370    375    470    477    Ljava/io/IOException;
        //  377    382    470    477    Ljava/io/IOException;
        //  384    389    470    477    Ljava/io/IOException;
        //  391    400    470    477    Ljava/io/IOException;
        //  402    409    470    477    Ljava/io/IOException;
        //  411    420    470    477    Ljava/io/IOException;
        //  422    432    470    477    Ljava/io/IOException;
        //  434    437    470    477    Ljava/io/IOException;
        //  451    454    454    470    Any
        //  458    465    470    477    Ljava/io/IOException;
        //  467    470    470    477    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0368:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static final class a
    {
        public final Set a() {
            return SingleProcessDataStore.b();
        }
        
        public final Object b() {
            return SingleProcessDataStore.c();
        }
    }
    
    public abstract static class b
    {
        public static final class a extends SingleProcessDataStore.b
        {
            public final up1 a;
            
            public a(final up1 a) {
                super(null);
                this.a = a;
            }
            
            public up1 a() {
                return this.a;
            }
        }
        
        public static final class b extends SingleProcessDataStore.b
        {
            public final q90 a;
            public final mi b;
            public final up1 c;
            public final CoroutineContext d;
            
            public b(final q90 a, final mi b, final up1 c, final CoroutineContext d) {
                fg0.e((Object)a, "transform");
                fg0.e((Object)b, "ack");
                fg0.e((Object)d, "callerContext");
                super(null);
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
            }
            
            public final mi a() {
                return this.b;
            }
            
            public final CoroutineContext b() {
                return this.d;
            }
            
            public up1 c() {
                return this.c;
            }
            
            public final q90 d() {
                return this.a;
            }
        }
    }
    
    public static final class c extends OutputStream
    {
        public final FileOutputStream a;
        
        public c(final FileOutputStream a) {
            fg0.e((Object)a, "fileOutputStream");
            this.a = a;
        }
        
        @Override
        public void close() {
        }
        
        @Override
        public void flush() {
            this.a.flush();
        }
        
        @Override
        public void write(final int b) {
            this.a.write(b);
        }
        
        @Override
        public void write(final byte[] b) {
            fg0.e((Object)b, "b");
            this.a.write(b);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) {
            fg0.e((Object)b, "bytes");
            this.a.write(b, off, len);
        }
    }
}

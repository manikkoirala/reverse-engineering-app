// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences;

import androidx.datastore.preferences.protobuf.y;
import androidx.datastore.preferences.protobuf.GeneratedMessageLite;

public final class PreferencesProto$Value extends GeneratedMessageLite implements yv0
{
    public static final int BOOLEAN_FIELD_NUMBER = 1;
    private static final PreferencesProto$Value DEFAULT_INSTANCE;
    public static final int DOUBLE_FIELD_NUMBER = 7;
    public static final int FLOAT_FIELD_NUMBER = 2;
    public static final int INTEGER_FIELD_NUMBER = 3;
    public static final int LONG_FIELD_NUMBER = 4;
    private static volatile c31 PARSER;
    public static final int STRING_FIELD_NUMBER = 5;
    public static final int STRING_SET_FIELD_NUMBER = 6;
    private int bitField0_;
    private int valueCase_;
    private Object value_;
    
    static {
        GeneratedMessageLite.H(PreferencesProto$Value.class, DEFAULT_INSTANCE = new PreferencesProto$Value());
    }
    
    public PreferencesProto$Value() {
        this.valueCase_ = 0;
    }
    
    public static /* synthetic */ PreferencesProto$Value N() {
        return PreferencesProto$Value.DEFAULT_INSTANCE;
    }
    
    public static PreferencesProto$Value S() {
        return PreferencesProto$Value.DEFAULT_INSTANCE;
    }
    
    public static a a0() {
        return (a)PreferencesProto$Value.DEFAULT_INSTANCE.r();
    }
    
    public boolean R() {
        return this.valueCase_ == 1 && (boolean)this.value_;
    }
    
    public double T() {
        if (this.valueCase_ == 7) {
            return (double)this.value_;
        }
        return 0.0;
    }
    
    public float U() {
        if (this.valueCase_ == 2) {
            return (float)this.value_;
        }
        return 0.0f;
    }
    
    public int V() {
        if (this.valueCase_ == 3) {
            return (int)this.value_;
        }
        return 0;
    }
    
    public long W() {
        if (this.valueCase_ == 4) {
            return (long)this.value_;
        }
        return 0L;
    }
    
    public String X() {
        String s;
        if (this.valueCase_ == 5) {
            s = (String)this.value_;
        }
        else {
            s = "";
        }
        return s;
    }
    
    public androidx.datastore.preferences.a Y() {
        if (this.valueCase_ == 6) {
            return (androidx.datastore.preferences.a)this.value_;
        }
        return androidx.datastore.preferences.a.N();
    }
    
    public ValueCase Z() {
        return ValueCase.forNumber(this.valueCase_);
    }
    
    public final void b0(final boolean b) {
        this.valueCase_ = 1;
        this.value_ = b;
    }
    
    public final void c0(final double d) {
        this.valueCase_ = 7;
        this.value_ = d;
    }
    
    public final void d0(final float f) {
        this.valueCase_ = 2;
        this.value_ = f;
    }
    
    public final void e0(final int i) {
        this.valueCase_ = 3;
        this.value_ = i;
    }
    
    public final void f0(final long l) {
        this.valueCase_ = 4;
        this.value_ = l;
    }
    
    public final void g0(final String value_) {
        value_.getClass();
        this.valueCase_ = 5;
        this.value_ = value_;
    }
    
    public final void h0(final androidx.datastore.preferences.a.a a) {
        this.value_ = ((GeneratedMessageLite.a)a).p();
        this.valueCase_ = 6;
    }
    
    @Override
    public final Object u(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (w71.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final c31 parser;
                if ((parser = PreferencesProto$Value.PARSER) == null) {
                    synchronized (PreferencesProto$Value.class) {
                        if (PreferencesProto$Value.PARSER == null) {
                            PreferencesProto$Value.PARSER = new b(PreferencesProto$Value.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return PreferencesProto$Value.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.E(PreferencesProto$Value.DEFAULT_INSTANCE, "\u0001\u0007\u0001\u0001\u0001\u0007\u0007\u0000\u0000\u0000\u0001:\u0000\u00024\u0000\u00037\u0000\u00045\u0000\u0005;\u0000\u0006<\u0000\u00073\u0000", new Object[] { "value_", "valueCase_", "bitField0_", androidx.datastore.preferences.a.class });
            }
            case 2: {
                return new a((w71)null);
            }
            case 1: {
                return new PreferencesProto$Value();
            }
        }
    }
    
    public enum ValueCase
    {
        private static final ValueCase[] $VALUES;
        
        BOOLEAN(1), 
        DOUBLE(7), 
        FLOAT(2), 
        INTEGER(3), 
        LONG(4), 
        STRING(5), 
        STRING_SET(6), 
        VALUE_NOT_SET(0);
        
        private final int value;
        
        private ValueCase(final int value) {
            this.value = value;
        }
        
        public static ValueCase forNumber(final int n) {
            switch (n) {
                default: {
                    return null;
                }
                case 7: {
                    return ValueCase.DOUBLE;
                }
                case 6: {
                    return ValueCase.STRING_SET;
                }
                case 5: {
                    return ValueCase.STRING;
                }
                case 4: {
                    return ValueCase.LONG;
                }
                case 3: {
                    return ValueCase.INTEGER;
                }
                case 2: {
                    return ValueCase.FLOAT;
                }
                case 1: {
                    return ValueCase.BOOLEAN;
                }
                case 0: {
                    return ValueCase.VALUE_NOT_SET;
                }
            }
        }
        
        @Deprecated
        public static ValueCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class a extends GeneratedMessageLite.a implements yv0
    {
        public a() {
            super(PreferencesProto$Value.N());
        }
        
        public a A(final double n) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).c0(n);
            return this;
        }
        
        public a B(final float n) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).d0(n);
            return this;
        }
        
        public a C(final int n) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).e0(n);
            return this;
        }
        
        public a D(final long n) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).f0(n);
            return this;
        }
        
        public a E(final String s) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).g0(s);
            return this;
        }
        
        public a F(final androidx.datastore.preferences.a.a a) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).h0(a);
            return this;
        }
        
        public a z(final boolean b) {
            ((GeneratedMessageLite.a)this).s();
            ((PreferencesProto$Value)super.b).b0(b);
            return this;
        }
    }
}

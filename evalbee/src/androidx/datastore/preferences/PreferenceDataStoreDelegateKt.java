// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences;

import kotlinx.coroutines.f;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.n;

public abstract class PreferenceDataStoreDelegateKt
{
    public static final pc1 a(final String s, final sd1 sd1, final c90 c90, final lm lm) {
        fg0.e((Object)s, "name");
        fg0.e((Object)c90, "produceMigrations");
        fg0.e((Object)lm, "scope");
        return (pc1)new PreferenceDataStoreSingletonDelegate(s, sd1, c90, lm);
    }
}

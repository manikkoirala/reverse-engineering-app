// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences;

import java.util.Iterator;
import java.util.ArrayList;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.datastore.preferences.SharedPreferencesMigrationKt$getMigrationFunction$1", f = "SharedPreferencesMigration.kt", l = {}, m = "invokeSuspend")
final class SharedPreferencesMigrationKt$getMigrationFunction$1 extends SuspendLambda implements s90
{
    Object L$0;
    Object L$1;
    int label;
    
    public SharedPreferencesMigrationKt$getMigrationFunction$1(final vl vl) {
        super(3, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final qn1 l$0, @NotNull final s71 l$2, @Nullable final vl vl) {
        final SharedPreferencesMigrationKt$getMigrationFunction$1 sharedPreferencesMigrationKt$getMigrationFunction$1 = new SharedPreferencesMigrationKt$getMigrationFunction$1(vl);
        sharedPreferencesMigrationKt$getMigrationFunction$1.L$0 = l$0;
        sharedPreferencesMigrationKt$getMigrationFunction$1.L$1 = l$2;
        return sharedPreferencesMigrationKt$getMigrationFunction$1.invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        if (this.label == 0) {
            xe1.b(o);
            zu0.a(this.L$0);
            final Iterable iterable = ((s71)this.L$1).a().keySet();
            final ArrayList list = new ArrayList(oh.o(iterable, 10));
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                list.add((Object)((s71.a)iterator.next()).a());
            }
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

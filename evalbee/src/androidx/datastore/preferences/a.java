// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences;

import androidx.datastore.preferences.protobuf.y;
import java.util.List;
import androidx.datastore.preferences.protobuf.r;
import androidx.datastore.preferences.protobuf.GeneratedMessageLite;

public final class a extends GeneratedMessageLite implements yv0
{
    private static final a DEFAULT_INSTANCE;
    private static volatile c31 PARSER;
    public static final int STRINGS_FIELD_NUMBER = 1;
    private r.d strings_;
    
    static {
        GeneratedMessageLite.H(a.class, DEFAULT_INSTANCE = new a());
    }
    
    public a() {
        this.strings_ = GeneratedMessageLite.v();
    }
    
    public static /* synthetic */ a J() {
        return a.DEFAULT_INSTANCE;
    }
    
    public static a N() {
        return a.DEFAULT_INSTANCE;
    }
    
    public static a P() {
        return (a)a.DEFAULT_INSTANCE.r();
    }
    
    public final void L(final Iterable iterable) {
        this.M();
        a.h(iterable, this.strings_);
    }
    
    public final void M() {
        if (!this.strings_.h()) {
            this.strings_ = GeneratedMessageLite.C(this.strings_);
        }
    }
    
    public List O() {
        return this.strings_;
    }
    
    @Override
    public final Object u(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (w71.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final c31 parser;
                if ((parser = a.PARSER) == null) {
                    synchronized (a.class) {
                        if (a.PARSER == null) {
                            a.PARSER = new b(a.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return a.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.E(a.DEFAULT_INSTANCE, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001a", new Object[] { "strings_" });
            }
            case 2: {
                return new a((w71)null);
            }
            case 1: {
                return new a();
            }
        }
    }
    
    public static final class a extends GeneratedMessageLite.a implements yv0
    {
        public a() {
            super(androidx.datastore.preferences.a.J());
        }
        
        public a z(final Iterable iterable) {
            ((GeneratedMessageLite.a)this).s();
            ((androidx.datastore.preferences.a)super.b).L(iterable);
            return this;
        }
    }
}

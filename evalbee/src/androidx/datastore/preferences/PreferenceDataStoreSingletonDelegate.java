// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences;

import java.util.List;
import androidx.datastore.preferences.core.PreferenceDataStoreFactory;
import android.content.Context;

public final class PreferenceDataStoreSingletonDelegate implements pc1
{
    public final String a;
    public final c90 b;
    public final lm c;
    public final Object d;
    public volatile jp e;
    
    public PreferenceDataStoreSingletonDelegate(final String a, final sd1 sd1, final c90 b, final lm c) {
        fg0.e((Object)a, "name");
        fg0.e((Object)b, "produceMigrations");
        fg0.e((Object)c, "scope");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = new Object();
    }
    
    public jp c(final Context context, gi0 gi0) {
        fg0.e((Object)context, "thisRef");
        fg0.e((Object)gi0, "property");
        if ((gi0 = (gi0)this.e) == null) {
            synchronized (this.d) {
                if (this.e == null) {
                    gi0 = (gi0)context.getApplicationContext();
                    final PreferenceDataStoreFactory a = PreferenceDataStoreFactory.a;
                    final c90 b = this.b;
                    fg0.d((Object)gi0, "applicationContext");
                    this.e = a.a(null, (List)b.invoke((Object)gi0), this.c, (a90)new PreferenceDataStoreSingletonDelegate$getValue$1.PreferenceDataStoreSingletonDelegate$getValue$1$1((Context)gi0, this));
                }
                gi0 = (gi0)this.e;
                fg0.b((Object)gi0);
            }
        }
        return (jp)gi0;
    }
}

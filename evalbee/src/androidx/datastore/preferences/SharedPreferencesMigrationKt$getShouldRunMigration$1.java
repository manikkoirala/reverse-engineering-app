// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Set;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.datastore.preferences.SharedPreferencesMigrationKt$getShouldRunMigration$1", f = "SharedPreferencesMigration.kt", l = {}, m = "invokeSuspend")
final class SharedPreferencesMigrationKt$getShouldRunMigration$1 extends SuspendLambda implements q90
{
    final Set<String> $keysToMigrate;
    Object L$0;
    int label;
    
    public SharedPreferencesMigrationKt$getShouldRunMigration$1(final Set<String> $keysToMigrate, final vl vl) {
        this.$keysToMigrate = $keysToMigrate;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
        final SharedPreferencesMigrationKt$getShouldRunMigration$1 sharedPreferencesMigrationKt$getShouldRunMigration$1 = new SharedPreferencesMigrationKt$getShouldRunMigration$1(this.$keysToMigrate, vl);
        sharedPreferencesMigrationKt$getShouldRunMigration$1.L$0 = l$0;
        return (vl)sharedPreferencesMigrationKt$getShouldRunMigration$1;
    }
    
    @Nullable
    public final Object invoke(@NotNull final s71 s71, @Nullable final vl vl) {
        return ((SharedPreferencesMigrationKt$getShouldRunMigration$1)this.create(s71, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        if (this.label == 0) {
            xe1.b(o);
            final Iterable iterable = ((s71)this.L$0).a().keySet();
            final ArrayList list = new ArrayList(oh.o(iterable, 10));
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                list.add((Object)((s71.a)iterator.next()).a());
            }
            final Set<String> $keysToMigrate = this.$keysToMigrate;
            final Set a = nn1.a();
            boolean b = true;
            if ($keysToMigrate != a) {
                final Iterable iterable2 = this.$keysToMigrate;
                if (!(iterable2 instanceof Collection) || !((Collection)iterable2).isEmpty()) {
                    final Iterator iterator2 = iterable2.iterator();
                    while (iterator2.hasNext()) {
                        if (pc.a(list.contains(iterator2.next()) ^ true)) {
                            return pc.a(b);
                        }
                    }
                }
                b = false;
            }
            return pc.a(b);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

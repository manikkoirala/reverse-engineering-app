// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.core;

public final class PreferenceDataStore implements jp
{
    public final jp a;
    
    public PreferenceDataStore(final jp a) {
        fg0.e((Object)a, "delegate");
        this.a = a;
    }
    
    @Override
    public Object a(final q90 q90, final vl vl) {
        return this.a.a((q90)new PreferenceDataStore$updateData.PreferenceDataStore$updateData$2(q90, (vl)null), vl);
    }
    
    @Override
    public y40 getData() {
        return this.a.getData();
    }
}

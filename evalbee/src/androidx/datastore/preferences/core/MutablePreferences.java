// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.core;

import java.util.Set;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Map;

public final class MutablePreferences extends s71
{
    public final Map a;
    public final AtomicBoolean b;
    
    public MutablePreferences(final Map a, final boolean initialValue) {
        fg0.e((Object)a, "preferencesMap");
        this.a = a;
        this.b = new AtomicBoolean(initialValue);
    }
    
    @Override
    public Map a() {
        final Map<Object, Object> unmodifiableMap = Collections.unmodifiableMap((Map<?, ?>)this.a);
        fg0.d((Object)unmodifiableMap, "unmodifiableMap(preferencesMap)");
        return unmodifiableMap;
    }
    
    @Override
    public Object b(final a a) {
        fg0.e((Object)a, "key");
        return this.a.get(a);
    }
    
    public final void e() {
        if (this.b.get() ^ true) {
            return;
        }
        throw new IllegalStateException("Do mutate preferences once returned to DataStore.".toString());
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof MutablePreferences && fg0.a((Object)this.a, (Object)((MutablePreferences)o).a);
    }
    
    public final void f() {
        this.e();
        this.a.clear();
    }
    
    public final void g() {
        this.b.set(true);
    }
    
    public final void h(final b... array) {
        fg0.e((Object)array, "pairs");
        this.e();
        if (array.length <= 0) {
            return;
        }
        final b b = array[0];
        throw null;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    public final Object i(final a a) {
        fg0.e((Object)a, "key");
        this.e();
        return this.a.remove(a);
    }
    
    public final void j(final a a, final Object o) {
        fg0.e((Object)a, "key");
        this.k(a, o);
    }
    
    public final void k(final a a, final Object o) {
        fg0.e((Object)a, "key");
        this.e();
        if (o == null) {
            this.i(a);
        }
        else {
            Map map;
            Object o2;
            if (o instanceof Set) {
                final Map a2 = this.a;
                final Set<Object> unmodifiableSet = Collections.unmodifiableSet((Set<?>)vh.S((Iterable)o));
                fg0.d((Object)unmodifiableSet, "unmodifiableSet(value.toSet())");
                map = a2;
                o2 = unmodifiableSet;
            }
            else {
                final Map a3 = this.a;
                o2 = o;
                map = a3;
            }
            map.put(a, o2);
        }
    }
    
    @Override
    public String toString() {
        return vh.D((Iterable)this.a.entrySet(), (CharSequence)",\n", (CharSequence)"{\n", (CharSequence)"\n}", 0, (CharSequence)null, (c90)MutablePreferences$toString.MutablePreferences$toString$1.INSTANCE, 24, (Object)null);
    }
}

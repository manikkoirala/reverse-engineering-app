// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.core;

import java.util.List;

public final class PreferenceDataStoreFactory
{
    public static final PreferenceDataStoreFactory a;
    
    static {
        a = new PreferenceDataStoreFactory();
    }
    
    public final jp a(final sd1 sd1, final List list, final lm lm, final a90 a90) {
        fg0.e((Object)list, "migrations");
        fg0.e((Object)lm, "scope");
        fg0.e((Object)a90, "produceFile");
        return new PreferenceDataStore(kp.a.a(y71.a, sd1, list, lm, (a90)new PreferenceDataStoreFactory$create$delegate.PreferenceDataStoreFactory$create$delegate$1(a90)));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Arrays;

public final class j0
{
    public static final j0 f;
    public int a;
    public int[] b;
    public Object[] c;
    public int d;
    public boolean e;
    
    static {
        f = new j0(0, new int[0], new Object[0], false);
    }
    
    public j0() {
        this(0, new int[8], new Object[8], true);
    }
    
    public j0(final int a, final int[] b, final Object[] c, final boolean e) {
        this.d = -1;
        this.a = a;
        this.b = b;
        this.c = c;
        this.e = e;
    }
    
    public static boolean c(final int[] array, final int[] array2, final int n) {
        for (int i = 0; i < n; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean d(final Object[] array, final Object[] array2, final int n) {
        for (int i = 0; i < n; ++i) {
            if (!array[i].equals(array2[i])) {
                return false;
            }
        }
        return true;
    }
    
    public static j0 e() {
        return j0.f;
    }
    
    public static int h(final int[] array, final int n) {
        int n2 = 17;
        for (int i = 0; i < n; ++i) {
            n2 = n2 * 31 + array[i];
        }
        return n2;
    }
    
    public static int i(final Object[] array, final int n) {
        int n2 = 17;
        for (int i = 0; i < n; ++i) {
            n2 = n2 * 31 + array[i].hashCode();
        }
        return n2;
    }
    
    public static j0 k(final j0 j0, final j0 j2) {
        final int n = j0.a + j2.a;
        final int[] copy = Arrays.copyOf(j0.b, n);
        System.arraycopy(j2.b, 0, copy, j0.a, j2.a);
        final Object[] copy2 = Arrays.copyOf(j0.c, n);
        System.arraycopy(j2.c, 0, copy2, j0.a, j2.a);
        return new j0(n, copy, copy2, true);
    }
    
    public static j0 l() {
        return new j0();
    }
    
    public static void p(int b, final Object o, final Writer writer) {
        final int a = WireFormat.a(b);
        b = WireFormat.b(b);
        if (b != 0) {
            if (b != 1) {
                if (b != 2) {
                    if (b != 3) {
                        if (b != 5) {
                            throw new RuntimeException(InvalidProtocolBufferException.invalidWireType());
                        }
                        writer.c(a, (int)o);
                    }
                    else if (writer.B() == Writer.FieldOrder.ASCENDING) {
                        writer.p(a);
                        ((j0)o).q(writer);
                        writer.r(a);
                    }
                    else {
                        writer.r(a);
                        ((j0)o).q(writer);
                        writer.p(a);
                    }
                }
                else {
                    writer.J(a, (ByteString)o);
                }
            }
            else {
                writer.m(a, (long)o);
            }
        }
        else {
            writer.C(a, (long)o);
        }
    }
    
    public void a() {
        if (this.e) {
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    public final void b() {
        final int a = this.a;
        final int[] b = this.b;
        if (a == b.length) {
            int n;
            if (a < 4) {
                n = 8;
            }
            else {
                n = a >> 1;
            }
            final int n2 = a + n;
            this.b = Arrays.copyOf(b, n2);
            this.c = Arrays.copyOf(this.c, n2);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof j0)) {
            return false;
        }
        final j0 j0 = (j0)o;
        final int a = this.a;
        return a == j0.a && c(this.b, j0.b, a) && d(this.c, j0.c, this.a);
    }
    
    public int f() {
        final int d = this.d;
        if (d != -1) {
            return d;
        }
        int i = 0;
        int d2 = 0;
        while (i < this.a) {
            final int n = this.b[i];
            final int a = WireFormat.a(n);
            final int b = WireFormat.b(n);
            int n2;
            if (b != 0) {
                if (b != 1) {
                    if (b != 2) {
                        if (b != 3) {
                            if (b != 5) {
                                throw new IllegalStateException(InvalidProtocolBufferException.invalidWireType());
                            }
                            n2 = CodedOutputStream.n(a, (int)this.c[i]);
                        }
                        else {
                            n2 = CodedOutputStream.R(a) * 2 + ((j0)this.c[i]).f();
                        }
                    }
                    else {
                        n2 = CodedOutputStream.h(a, (ByteString)this.c[i]);
                    }
                }
                else {
                    n2 = CodedOutputStream.p(a, (long)this.c[i]);
                }
            }
            else {
                n2 = CodedOutputStream.U(a, (long)this.c[i]);
            }
            d2 += n2;
            ++i;
        }
        return this.d = d2;
    }
    
    public int g() {
        final int d = this.d;
        if (d != -1) {
            return d;
        }
        int i = 0;
        int d2 = 0;
        while (i < this.a) {
            d2 += CodedOutputStream.F(WireFormat.a(this.b[i]), (ByteString)this.c[i]);
            ++i;
        }
        return this.d = d2;
    }
    
    @Override
    public int hashCode() {
        final int a = this.a;
        return ((527 + a) * 31 + h(this.b, a)) * 31 + i(this.c, this.a);
    }
    
    public void j() {
        this.e = false;
    }
    
    public final void m(final StringBuilder sb, final int n) {
        for (int i = 0; i < this.a; ++i) {
            z.c(sb, n, String.valueOf(WireFormat.a(this.b[i])), this.c[i]);
        }
    }
    
    public void n(final int n, final Object o) {
        this.a();
        this.b();
        final int[] b = this.b;
        final int a = this.a;
        b[a] = n;
        this.c[a] = o;
        this.a = a + 1;
    }
    
    public void o(final Writer writer) {
        if (writer.B() == Writer.FieldOrder.DESCENDING) {
            for (int i = this.a - 1; i >= 0; --i) {
                writer.b(WireFormat.a(this.b[i]), this.c[i]);
            }
        }
        else {
            for (int j = 0; j < this.a; ++j) {
                writer.b(WireFormat.a(this.b[j]), this.c[j]);
            }
        }
    }
    
    public void q(final Writer writer) {
        if (this.a == 0) {
            return;
        }
        if (writer.B() == Writer.FieldOrder.ASCENDING) {
            for (int i = 0; i < this.a; ++i) {
                p(this.b[i], this.c[i], writer);
            }
        }
        else {
            for (int j = this.a - 1; j >= 0; --j) {
                p(this.b[j], this.c[j], writer);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Map;
import java.util.List;

public interface Writer
{
    void A(final int p0, final List p1, final boolean p2);
    
    FieldOrder B();
    
    void C(final int p0, final long p1);
    
    void D(final int p0, final List p1, final boolean p2);
    
    void E(final int p0, final List p1, final boolean p2);
    
    void F(final int p0, final float p1);
    
    void G(final int p0, final int p1);
    
    void H(final int p0, final List p1, final boolean p2);
    
    void I(final int p0, final int p1);
    
    void J(final int p0, final ByteString p1);
    
    void K(final int p0, final Object p1, final f0 p2);
    
    void L(final int p0, final v.a p1, final Map p2);
    
    void M(final int p0, final List p1, final f0 p2);
    
    void N(final int p0, final Object p1, final f0 p2);
    
    void O(final int p0, final List p1, final f0 p2);
    
    void a(final int p0, final List p1, final boolean p2);
    
    void b(final int p0, final Object p1);
    
    void c(final int p0, final int p1);
    
    void d(final int p0, final String p1);
    
    void e(final int p0, final long p1);
    
    void f(final int p0, final List p1, final boolean p2);
    
    void g(final int p0, final int p1);
    
    void h(final int p0, final List p1, final boolean p2);
    
    void i(final int p0, final List p1, final boolean p2);
    
    void j(final int p0, final long p1);
    
    void k(final int p0, final int p1);
    
    void l(final int p0, final List p1, final boolean p2);
    
    void m(final int p0, final long p1);
    
    void n(final int p0, final boolean p1);
    
    void o(final int p0, final int p1);
    
    void p(final int p0);
    
    void q(final int p0, final List p1, final boolean p2);
    
    void r(final int p0);
    
    void s(final int p0, final List p1, final boolean p2);
    
    void t(final int p0, final List p1, final boolean p2);
    
    void u(final int p0, final List p1);
    
    void v(final int p0, final List p1);
    
    void w(final int p0, final long p1);
    
    void x(final int p0, final List p1, final boolean p2);
    
    void y(final int p0, final List p1, final boolean p2);
    
    void z(final int p0, final double p1);
    
    public enum FieldOrder
    {
        private static final FieldOrder[] $VALUES;
        
        ASCENDING, 
        DESCENDING;
    }
}

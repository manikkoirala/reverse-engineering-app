// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Map;
import java.util.List;

public final class g implements e0
{
    public final f a;
    public int b;
    public int c;
    public int d;
    
    public g(f a) {
        this.d = 0;
        a = (f)r.b(a, "input");
        this.a = a;
        a.d = this;
    }
    
    public static g N(final f f) {
        final g d = f.d;
        if (d != null) {
            return d;
        }
        return new g(f);
    }
    
    @Override
    public int A() {
        this.U(0);
        return this.a.u();
    }
    
    @Override
    public void B(final List list) {
        if (list instanceof u) {
            final u u = (u)list;
            final int b = WireFormat.b(this.b);
            if (b == 1) {
                int i;
                do {
                    u.b(this.a.s());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int d = this.a.D();
            this.W(d);
            do {
                u.b(this.a.s());
            } while (this.a.d() < this.a.d() + d);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 1) {
                int j;
                do {
                    list.add(this.a.s());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int d2 = this.a.D();
            this.W(d2);
            do {
                list.add(this.a.s());
            } while (this.a.d() < this.a.d() + d2);
        }
    }
    
    @Override
    public void C(final List list) {
        int n;
        if (list instanceof q) {
            final q q = (q)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    q.b(this.a.D());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                q.b(this.a.D());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.D());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.D());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public int D() {
        this.U(5);
        return this.a.r();
    }
    
    @Override
    public long E() {
        this.U(0);
        return this.a.z();
    }
    
    @Override
    public String F() {
        this.U(2);
        return this.a.A();
    }
    
    @Override
    public int G() {
        this.U(5);
        return this.a.w();
    }
    
    @Override
    public String H() {
        this.U(2);
        return this.a.B();
    }
    
    @Override
    public Object I(final f0 f0, final k k) {
        this.U(3);
        return this.P(f0, k);
    }
    
    @Override
    public Object J(final f0 f0, final k k) {
        this.U(2);
        return this.Q(f0, k);
    }
    
    @Override
    public void K(final List list, final f0 f0, final k k) {
        if (WireFormat.b(this.b) == 2) {
            int i;
            do {
                list.add(this.Q(f0, k));
                if (!this.a.e()) {
                    if (this.d == 0) {
                        i = this.a.C();
                        continue;
                    }
                }
                return;
            } while (i == this.b);
            this.d = i;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    @Override
    public void L(final Map map, final v.a a, final k k) {
        this.U(2);
        final int m = this.a.m(this.a.D());
        Object o = a.b;
        Object o2 = a.d;
        try {
            while (true) {
                final int i = this.m();
                if (i == Integer.MAX_VALUE) {
                    break;
                }
                if (this.a.e()) {
                    break;
                }
                Label_0129: {
                    if (i == 1) {
                        break Label_0129;
                    }
                    Label_0104: {
                        if (i == 2) {
                            break Label_0104;
                        }
                        try {
                            if (this.p()) {
                                continue;
                            }
                            throw new InvalidProtocolBufferException("Unable to parse map entry.");
                            o2 = this.O(a.c, a.d.getClass(), k);
                            continue;
                            o = this.O(a.a, null, null);
                            continue;
                        }
                        catch (final InvalidProtocolBufferException.InvalidWireTypeException ex) {
                            if (this.p()) {
                                continue;
                            }
                            throw new InvalidProtocolBufferException("Unable to parse map entry.");
                        }
                    }
                }
                break;
            }
            map.put(o, o2);
        }
        finally {
            this.a.l(m);
        }
    }
    
    @Override
    public void M(final List list, final f0 f0, final k k) {
        if (WireFormat.b(this.b) == 3) {
            int i;
            do {
                list.add(this.P(f0, k));
                if (!this.a.e()) {
                    if (this.d == 0) {
                        i = this.a.C();
                        continue;
                    }
                }
                return;
            } while (i == this.b);
            this.d = i;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public final Object O(final WireFormat.FieldType fieldType, final Class clazz, final k k) {
        switch (g$a.a[fieldType.ordinal()]) {
            default: {
                throw new RuntimeException("unsupported field type.");
            }
            case 17: {
                return this.i();
            }
            case 16: {
                return this.c();
            }
            case 15: {
                return this.H();
            }
            case 14: {
                return this.E();
            }
            case 13: {
                return this.e();
            }
            case 12: {
                return this.b();
            }
            case 11: {
                return this.G();
            }
            case 10: {
                return this.R(clazz, k);
            }
            case 9: {
                return this.s();
            }
            case 8: {
                return this.A();
            }
            case 7: {
                return this.readFloat();
            }
            case 6: {
                return this.t();
            }
            case 5: {
                return this.D();
            }
            case 4: {
                return this.d();
            }
            case 3: {
                return this.readDouble();
            }
            case 2: {
                return this.g();
            }
            case 1: {
                return this.v();
            }
        }
    }
    
    public final Object P(final f0 f0, final k k) {
        final int c = this.c;
        this.c = WireFormat.c(WireFormat.a(this.b), 4);
        try {
            final Object instance = f0.newInstance();
            f0.h(instance, this, k);
            f0.d(instance);
            if (this.b == this.c) {
                return instance;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }
        finally {
            this.c = c;
        }
    }
    
    public final Object Q(final f0 f0, final k k) {
        final int d = this.a.D();
        final f a = this.a;
        if (a.a < a.b) {
            final int m = a.m(d);
            final Object instance = f0.newInstance();
            final f a2 = this.a;
            ++a2.a;
            f0.h(instance, this, k);
            f0.d(instance);
            this.a.a(0);
            final f a3 = this.a;
            --a3.a;
            a3.l(m);
            return instance;
        }
        throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public Object R(final Class clazz, final k k) {
        this.U(2);
        return this.Q(l91.a().c(clazz), k);
    }
    
    public void S(final List list, final boolean b) {
        if (WireFormat.b(this.b) != 2) {
            throw InvalidProtocolBufferException.invalidWireType();
        }
        if (list instanceof gj0 && !b) {
            final gj0 gj0 = (gj0)list;
            int i;
            do {
                gj0.x(this.g());
                if (this.a.e()) {
                    return;
                }
                i = this.a.C();
            } while (i == this.b);
            this.d = i;
            return;
        }
        int j;
        do {
            String s;
            if (b) {
                s = this.H();
            }
            else {
                s = this.F();
            }
            list.add(s);
            if (this.a.e()) {
                return;
            }
            j = this.a.C();
        } while (j == this.b);
        this.d = j;
    }
    
    public final void T(final int n) {
        if (this.a.d() == n) {
            return;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public final void U(final int n) {
        if (WireFormat.b(this.b) == n) {
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public final void V(final int n) {
        if ((n & 0x3) == 0x0) {
            return;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    public final void W(final int n) {
        if ((n & 0x7) == 0x0) {
            return;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    @Override
    public void a(final List list) {
        int n;
        if (list instanceof u) {
            final u u = (u)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    u.b(this.a.z());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                u.b(this.a.z());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.z());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.z());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public long b() {
        this.U(1);
        return this.a.x();
    }
    
    @Override
    public int c() {
        this.U(0);
        return this.a.D();
    }
    
    @Override
    public int d() {
        this.U(0);
        return this.a.q();
    }
    
    @Override
    public int e() {
        this.U(0);
        return this.a.y();
    }
    
    @Override
    public void f(final List list) {
        int n;
        if (list instanceof e) {
            final e e = (e)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    e.b(this.a.n());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                e.b(this.a.n());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.n());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.n());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public ByteString g() {
        this.U(2);
        return this.a.o();
    }
    
    @Override
    public int getTag() {
        return this.b;
    }
    
    @Override
    public void h(final List list) {
        int n;
        if (list instanceof q) {
            final q q = (q)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    q.b(this.a.y());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                q.b(this.a.y());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.y());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.y());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public long i() {
        this.U(0);
        return this.a.E();
    }
    
    @Override
    public void j(final List list) {
        if (list instanceof u) {
            final u u = (u)list;
            final int b = WireFormat.b(this.b);
            if (b == 1) {
                int i;
                do {
                    u.b(this.a.x());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int d = this.a.D();
            this.W(d);
            do {
                u.b(this.a.x());
            } while (this.a.d() < this.a.d() + d);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 1) {
                int j;
                do {
                    list.add(this.a.x());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int d2 = this.a.D();
            this.W(d2);
            do {
                list.add(this.a.x());
            } while (this.a.d() < this.a.d() + d2);
        }
    }
    
    @Override
    public void k(final List list) {
        int n;
        if (list instanceof q) {
            final q q = (q)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    q.b(this.a.u());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                q.b(this.a.u());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.u());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.u());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public void l(final List list) {
        if (list instanceof q) {
            final q q = (q)list;
            final int b = WireFormat.b(this.b);
            if (b != 2) {
                if (b == 5) {
                    int i;
                    do {
                        q.b(this.a.r());
                        if (this.a.e()) {
                            return;
                        }
                        i = this.a.C();
                    } while (i == this.b);
                    this.d = i;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int d = this.a.D();
                this.V(d);
                do {
                    q.b(this.a.r());
                } while (this.a.d() < this.a.d() + d);
            }
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 != 2) {
                if (b2 == 5) {
                    int j;
                    do {
                        list.add(this.a.r());
                        if (this.a.e()) {
                            return;
                        }
                        j = this.a.C();
                    } while (j == this.b);
                    this.d = j;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int d2 = this.a.D();
                this.V(d2);
                do {
                    list.add(this.a.r());
                } while (this.a.d() < this.a.d() + d2);
            }
        }
    }
    
    @Override
    public int m() {
        final int d = this.d;
        if (d != 0) {
            this.b = d;
            this.d = 0;
        }
        else {
            this.b = this.a.C();
        }
        final int b = this.b;
        if (b != 0 && b != this.c) {
            return WireFormat.a(b);
        }
        return Integer.MAX_VALUE;
    }
    
    @Override
    public void n(final List list) {
        this.S(list, false);
    }
    
    @Override
    public void o(final List list) {
        if (list instanceof o) {
            final o o = (o)list;
            final int b = WireFormat.b(this.b);
            if (b != 2) {
                if (b == 5) {
                    int i;
                    do {
                        o.b(this.a.t());
                        if (this.a.e()) {
                            return;
                        }
                        i = this.a.C();
                    } while (i == this.b);
                    this.d = i;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int d = this.a.D();
                this.V(d);
                do {
                    o.b(this.a.t());
                } while (this.a.d() < this.a.d() + d);
            }
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 != 2) {
                if (b2 == 5) {
                    int j;
                    do {
                        list.add(this.a.t());
                        if (this.a.e()) {
                            return;
                        }
                        j = this.a.C();
                    } while (j == this.b);
                    this.d = j;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int d2 = this.a.D();
                this.V(d2);
                do {
                    list.add(this.a.t());
                } while (this.a.d() < this.a.d() + d2);
            }
        }
    }
    
    @Override
    public boolean p() {
        if (!this.a.e()) {
            final int b = this.b;
            if (b != this.c) {
                return this.a.F(b);
            }
        }
        return false;
    }
    
    @Override
    public void q(final List list) {
        if (WireFormat.b(this.b) == 2) {
            int i;
            do {
                list.add(this.g());
                if (this.a.e()) {
                    return;
                }
                i = this.a.C();
            } while (i == this.b);
            this.d = i;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    @Override
    public void r(final List list) {
        if (list instanceof i) {
            final i i = (i)list;
            final int b = WireFormat.b(this.b);
            if (b == 1) {
                int j;
                do {
                    i.b(this.a.p());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int d = this.a.D();
            this.W(d);
            do {
                i.b(this.a.p());
            } while (this.a.d() < this.a.d() + d);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 1) {
                int k;
                do {
                    list.add(this.a.p());
                    if (this.a.e()) {
                        return;
                    }
                    k = this.a.C();
                } while (k == this.b);
                this.d = k;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int d2 = this.a.D();
            this.W(d2);
            do {
                list.add(this.a.p());
            } while (this.a.d() < this.a.d() + d2);
        }
    }
    
    @Override
    public double readDouble() {
        this.U(1);
        return this.a.p();
    }
    
    @Override
    public float readFloat() {
        this.U(5);
        return this.a.t();
    }
    
    @Override
    public long s() {
        this.U(0);
        return this.a.v();
    }
    
    @Override
    public long t() {
        this.U(1);
        return this.a.s();
    }
    
    @Override
    public void u(final List list) {
        if (list instanceof q) {
            final q q = (q)list;
            final int b = WireFormat.b(this.b);
            if (b != 2) {
                if (b == 5) {
                    int i;
                    do {
                        q.b(this.a.w());
                        if (this.a.e()) {
                            return;
                        }
                        i = this.a.C();
                    } while (i == this.b);
                    this.d = i;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int d = this.a.D();
                this.V(d);
                do {
                    q.b(this.a.w());
                } while (this.a.d() < this.a.d() + d);
            }
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 != 2) {
                if (b2 == 5) {
                    int j;
                    do {
                        list.add(this.a.w());
                        if (this.a.e()) {
                            return;
                        }
                        j = this.a.C();
                    } while (j == this.b);
                    this.d = j;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int d2 = this.a.D();
                this.V(d2);
                do {
                    list.add(this.a.w());
                } while (this.a.d() < this.a.d() + d2);
            }
        }
    }
    
    @Override
    public boolean v() {
        this.U(0);
        return this.a.n();
    }
    
    @Override
    public void w(final List list) {
        int n;
        if (list instanceof u) {
            final u u = (u)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    u.b(this.a.E());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                u.b(this.a.E());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.E());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.E());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public void x(final List list) {
        int n;
        if (list instanceof u) {
            final u u = (u)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    u.b(this.a.v());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                u.b(this.a.v());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.v());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.v());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public void y(final List list) {
        int n;
        if (list instanceof q) {
            final q q = (q)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    q.b(this.a.q());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.C();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                q.b(this.a.q());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.q());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.C();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.D();
            do {
                list.add(this.a.q());
            } while (this.a.d() < n);
        }
        this.T(n);
    }
    
    @Override
    public void z(final List list) {
        this.S(list, true);
    }
}

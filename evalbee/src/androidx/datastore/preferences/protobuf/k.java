// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Collections;
import java.util.Map;

public class k
{
    public static boolean b = true;
    public static final Class c;
    public static volatile k d;
    public static final k e;
    public final Map a;
    
    static {
        c = c();
        e = new k(true);
    }
    
    public k(final boolean b) {
        this.a = Collections.emptyMap();
    }
    
    public static k b() {
        final k d;
        if ((d = k.d) == null) {
            synchronized (k.class) {
                if (k.d == null) {
                    k d2;
                    if (k.b) {
                        d2 = rz.a();
                    }
                    else {
                        d2 = k.e;
                    }
                    k.d = d2;
                }
            }
        }
        return d;
    }
    
    public static Class c() {
        try {
            return Class.forName("androidx.datastore.preferences.protobuf.Extension");
        }
        catch (final ClassNotFoundException ex) {
            return null;
        }
    }
    
    public GeneratedMessageLite.c a(final y y, final int n) {
        zu0.a(this.a.get(new a(y, n)));
        return null;
    }
    
    public static final class a
    {
        public final Object a;
        public final int b;
        
        public a(final Object a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof a;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final a a = (a)o;
            boolean b3 = b2;
            if (this.a == a.a) {
                b3 = b2;
                if (this.b == a.b) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return System.identityHashCode(this.a) * 65535 + this.b;
        }
    }
}

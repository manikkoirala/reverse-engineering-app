// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;
import java.lang.reflect.Field;
import java.util.List;
import sun.misc.Unsafe;

public final class a0 implements f0
{
    public static final int[] r;
    public static final Unsafe s;
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final y e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final int[] j;
    public final int k;
    public final int l;
    public final fz0 m;
    public final t n;
    public final i0 o;
    public final l p;
    public final w q;
    
    static {
        r = new int[0];
        s = e12.F();
    }
    
    public a0(final int[] a, final Object[] b, final int c, final int d, final y e, final boolean h, final boolean i, final int[] j, final int k, final int l, final fz0 m, final t n, final i0 o, final l p15, final w q) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.g = (e instanceof GeneratedMessageLite);
        this.h = h;
        this.f = (p15 != null && p15.e(e));
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
        this.p = p15;
        this.e = e;
        this.q = q;
    }
    
    public static boolean A(final int n) {
        return (n & 0x20000000) != 0x0;
    }
    
    public static boolean D(final Object o, final int n, final f0 f0) {
        return f0.b(e12.E(o, U(n)));
    }
    
    public static boolean I(final int n) {
        return (n & 0x10000000) != 0x0;
    }
    
    public static List J(final Object o, final long n) {
        return (List)e12.E(o, n);
    }
    
    public static long K(final Object o, final long n) {
        return e12.C(o, n);
    }
    
    public static a0 Q(final Class clazz, final uv0 uv0, final fz0 fz0, final t t, final i0 i0, final l l, final w w) {
        if (uv0 instanceof lc1) {
            return S((lc1)uv0, fz0, t, i0, l, w);
        }
        zu0.a(uv0);
        return R(null, fz0, t, i0, l, w);
    }
    
    public static a0 R(final ur1 ur1, final fz0 fz0, final t t, final i0 i0, final l l, final w w) {
        throw null;
    }
    
    public static a0 S(final lc1 lc1, final fz0 fz0, final t t, final i0 i0, final l l, final w w) {
        final ProtoSyntax c = lc1.c();
        final ProtoSyntax proto3 = ProtoSyntax.PROTO3;
        int n = 0;
        final boolean b = c == proto3;
        final String e = lc1.e();
        final int length = e.length();
        int char1 = e.charAt(0);
        int index2;
        if (char1 >= 55296) {
            int n2 = char1 & 0x1FFF;
            int index = 1;
            int n3 = 13;
            int n4;
            char char2;
            while (true) {
                n4 = index + 1;
                char2 = e.charAt(index);
                if (char2 < '\ud800') {
                    break;
                }
                n2 |= (char2 & '\u1fff') << n3;
                n3 += 13;
                index = n4;
            }
            char1 = (n2 | char2 << n3);
            index2 = n4;
        }
        else {
            index2 = 1;
        }
        final int n5 = index2 + 1;
        final char char3 = e.charAt(index2);
        int j = n5;
        int n6 = char3;
        if (char3 >= '\ud800') {
            int n7 = char3 & '\u1fff';
            int n8 = 13;
            int index3 = n5;
            int n9;
            char char4;
            while (true) {
                n9 = index3 + 1;
                char4 = e.charAt(index3);
                if (char4 < '\ud800') {
                    break;
                }
                n7 |= (char4 & '\u1fff') << n8;
                n8 += 13;
                index3 = n9;
            }
            n6 = (n7 | char4 << n8);
            j = n9;
        }
        int[] r;
        int n10;
        int n11;
        int n13;
        int n15;
        int n14;
        int n16;
        if (n6 == 0) {
            r = a0.r;
            n10 = 0;
            n11 = 0;
            final int n12 = n13 = n11;
            n14 = (n15 = n13);
            n16 = n12;
        }
        else {
            final int n17 = j + 1;
            int char5;
            final char c2 = (char)(char5 = e.charAt(j));
            int index4 = n17;
            if (c2 >= '\ud800') {
                final int n18 = c2 & '\u1fff';
                int n19 = 13;
                int index5 = n17;
                int n20 = n18;
                int n21;
                char char6;
                while (true) {
                    n21 = index5 + 1;
                    char6 = e.charAt(index5);
                    if (char6 < '\ud800') {
                        break;
                    }
                    n20 |= (char6 & '\u1fff') << n19;
                    n19 += 13;
                    index5 = n21;
                }
                final int n22 = n20 | char6 << n19;
                index4 = n21;
                char5 = n22;
            }
            final int n23 = index4 + 1;
            int char7;
            final char c3 = (char)(char7 = e.charAt(index4));
            int index6 = n23;
            if (c3 >= '\ud800') {
                int n24 = c3 & '\u1fff';
                int n25 = 13;
                int index7 = n23;
                int n26;
                char char8;
                while (true) {
                    n26 = index7 + 1;
                    char8 = e.charAt(index7);
                    if (char8 < '\ud800') {
                        break;
                    }
                    n24 |= (char8 & '\u1fff') << n25;
                    n25 += 13;
                    index7 = n26;
                }
                char7 = (n24 | char8 << n25);
                index6 = n26;
            }
            final int n27 = index6 + 1;
            int char9;
            final char c4 = (char)(char9 = e.charAt(index6));
            int index8 = n27;
            if (c4 >= '\ud800') {
                int n28 = c4 & '\u1fff';
                int n29 = 13;
                int index9 = n27;
                int n30;
                char char10;
                while (true) {
                    n30 = index9 + 1;
                    char10 = e.charAt(index9);
                    if (char10 < '\ud800') {
                        break;
                    }
                    n28 |= (char10 & '\u1fff') << n29;
                    n29 += 13;
                    index9 = n30;
                }
                char9 = (n28 | char10 << n29);
                index8 = n30;
            }
            final int n31 = index8 + 1;
            int char11;
            final char c5 = (char)(char11 = e.charAt(index8));
            int index10 = n31;
            if (c5 >= '\ud800') {
                final int n32 = c5 & '\u1fff';
                int n33 = 13;
                int index11 = n31;
                int n34 = n32;
                int n35;
                char char12;
                while (true) {
                    n35 = index11 + 1;
                    char12 = e.charAt(index11);
                    if (char12 < '\ud800') {
                        break;
                    }
                    n34 |= (char12 & '\u1fff') << n33;
                    n33 += 13;
                    index11 = n35;
                }
                final int n36 = n34 | char12 << n33;
                index10 = n35;
                char11 = n36;
            }
            final int n37 = index10 + 1;
            int char13;
            final char c6 = (char)(char13 = e.charAt(index10));
            int index12 = n37;
            if (c6 >= '\ud800') {
                final int n38 = c6 & '\u1fff';
                int n39 = 13;
                int index13 = n37;
                int n40 = n38;
                int n41;
                char char14;
                while (true) {
                    n41 = index13 + 1;
                    char14 = e.charAt(index13);
                    if (char14 < '\ud800') {
                        break;
                    }
                    n40 |= (char14 & '\u1fff') << n39;
                    n39 += 13;
                    index13 = n41;
                }
                final int n42 = n40 | char14 << n39;
                index12 = n41;
                char13 = n42;
            }
            final int n43 = index12 + 1;
            int char15;
            final char c7 = (char)(char15 = e.charAt(index12));
            int index14 = n43;
            if (c7 >= '\ud800') {
                int n44 = c7 & '\u1fff';
                int n45 = 13;
                int index15 = n43;
                int n46;
                char char16;
                while (true) {
                    n46 = index15 + 1;
                    char16 = e.charAt(index15);
                    if (char16 < '\ud800') {
                        break;
                    }
                    n44 |= (char16 & '\u1fff') << n45;
                    n45 += 13;
                    index15 = n46;
                }
                char15 = (n44 | char16 << n45);
                index14 = n46;
            }
            final int n47 = index14 + 1;
            int char17;
            final char c8 = (char)(char17 = e.charAt(index14));
            int index16 = n47;
            if (c8 >= '\ud800') {
                int n48 = c8 & '\u1fff';
                int n49 = 13;
                int index17 = n47;
                int n50;
                char char18;
                while (true) {
                    n50 = index17 + 1;
                    char18 = e.charAt(index17);
                    if (char18 < '\ud800') {
                        break;
                    }
                    n48 |= (char18 & '\u1fff') << n49;
                    n49 += 13;
                    index17 = n50;
                }
                char17 = (n48 | char18 << n49);
                index16 = n50;
            }
            final int n51 = index16 + 1;
            int char19;
            final char c9 = (char)(char19 = e.charAt(index16));
            int n52 = n51;
            if (c9 >= '\ud800') {
                final int n53 = c9 & '\u1fff';
                int index18 = n51;
                int n54 = 13;
                int n55 = n53;
                char char20;
                while (true) {
                    n52 = index18 + 1;
                    char20 = e.charAt(index18);
                    if (char20 < '\ud800') {
                        break;
                    }
                    n55 |= (char20 & '\u1fff') << n54;
                    n54 += 13;
                    index18 = n52;
                }
                char19 = (n55 | char20 << n54);
            }
            r = new int[char19 + char15 + char17];
            final int n56 = char5 * 2 + char7;
            n15 = char5;
            final int n57 = n52;
            n = char9;
            final int n58 = char15;
            n14 = char19;
            n13 = n56;
            n16 = char13;
            n11 = char11;
            n10 = n58;
            j = n57;
        }
        final Unsafe s = a0.s;
        final Object[] d = lc1.d();
        final Class<? extends y> class1 = lc1.b().getClass();
        final int[] array = new int[n16 * 3];
        final Object[] array2 = new Object[n16 * 2];
        final int n59 = n14 + n10;
        int n60 = n14;
        int n61 = n59;
        int n62 = 0;
        int n63 = 0;
        final int n64 = n11;
        final int n65 = char1;
        while (j < length) {
            final int n66 = j + 1;
            int char21 = e.charAt(j);
            int n70;
            int index20;
            if (char21 >= 55296) {
                int n67 = char21 & 0x1FFF;
                int index19 = n66;
                int n68 = 13;
                int n69;
                char char22;
                while (true) {
                    n69 = index19 + 1;
                    char22 = e.charAt(index19);
                    n70 = n14;
                    if (char22 < '\ud800') {
                        break;
                    }
                    n67 |= (char22 & '\u1fff') << n68;
                    n68 += 13;
                    n14 = n70;
                    index19 = n69;
                }
                char21 = (n67 | char22 << n68);
                index20 = n69;
            }
            else {
                n70 = n14;
                index20 = n66;
            }
            int index21 = index20 + 1;
            int char23 = e.charAt(index20);
            int n74;
            if (char23 >= 55296) {
                int n71 = char23 & 0x1FFF;
                int n72 = 13;
                int n73;
                char char24;
                while (true) {
                    n73 = index21 + 1;
                    char24 = e.charAt(index21);
                    if (char24 < '\ud800') {
                        break;
                    }
                    n71 |= (char24 & '\u1fff') << n72;
                    n72 += 13;
                    index21 = n73;
                }
                char23 = (n71 | char24 << n72);
                n74 = n73;
            }
            else {
                n74 = index21;
            }
            final int n75 = char23 & 0xFF;
            int n76 = n62;
            if ((char23 & 0x400) != 0x0) {
                r[n62] = n63;
                n76 = n62 + 1;
            }
            int n77;
            int n86;
            int n87;
            int n88;
            if (n75 >= 51) {
                int index22 = n74 + 1;
                final char char25 = e.charAt(n74);
                n77 = index22;
                int n78;
                if ((n78 = char25) >= 55296) {
                    int n79 = char25 & '\u1fff';
                    int n80 = 13;
                    char char26;
                    while (true) {
                        n77 = index22 + 1;
                        char26 = e.charAt(index22);
                        if (char26 < '\ud800') {
                            break;
                        }
                        n79 |= (char26 & '\u1fff') << n80;
                        n80 += 13;
                        index22 = n77;
                    }
                    n78 = (n79 | char26 << n80);
                }
                final int n81 = n75 - 51;
                int n82;
                if (n81 != 9 && n81 != 17) {
                    n82 = n13;
                    if (n81 == 12) {
                        n82 = n13;
                        if ((n65 & 0x1) == 0x1) {
                            final int n83 = n63 / 3;
                            n82 = n13 + 1;
                            array2[n83 * 2 + 1] = d[n13];
                        }
                    }
                }
                else {
                    final int n84 = n63 / 3;
                    n82 = n13 + 1;
                    array2[n84 * 2 + 1] = d[n13];
                }
                int n85 = n78 * 2;
                final Object o = d[n85];
                Field m0;
                if (o instanceof Field) {
                    m0 = (Field)o;
                }
                else {
                    m0 = m0(class1, (String)o);
                    d[n85] = m0;
                }
                n86 = (int)s.objectFieldOffset(m0);
                ++n85;
                final Object o2 = d[n85];
                Field m2;
                if (o2 instanceof Field) {
                    m2 = (Field)o2;
                }
                else {
                    m2 = m0(class1, (String)o2);
                    d[n85] = m2;
                }
                n87 = (int)s.objectFieldOffset(m2);
                n88 = 0;
                n13 = n82;
            }
            else {
                final int n89 = n13 + 1;
                final Field m3 = m0(class1, (String)d[n13]);
                int n90 = 0;
                Label_2165: {
                    int n91 = 0;
                    Label_2161: {
                        if (n75 != 9 && n75 != 17) {
                            if (n75 != 27 && n75 != 49) {
                                if (n75 != 12 && n75 != 30 && n75 != 44) {
                                    if (n75 != 50) {
                                        n90 = n89;
                                        n91 = n60;
                                        break Label_2161;
                                    }
                                    final int n92 = n60 + 1;
                                    r[n60] = n63;
                                    final int n93 = n63 / 3 * 2;
                                    final int n94 = n89 + 1;
                                    array2[n93] = d[n89];
                                    if ((char23 & 0x800) != 0x0) {
                                        final int n95 = n94 + 1;
                                        array2[n93 + 1] = d[n94];
                                        n91 = n92;
                                        n90 = n95;
                                        break Label_2161;
                                    }
                                    n91 = n92;
                                    n90 = n94;
                                    break Label_2161;
                                }
                                else {
                                    n90 = n89;
                                    n91 = n60;
                                    if ((n65 & 0x1) != 0x1) {
                                        break Label_2161;
                                    }
                                    final int n96 = n63 / 3;
                                    n90 = n89 + 1;
                                    array2[n96 * 2 + 1] = d[n89];
                                }
                            }
                            else {
                                final int n97 = n63 / 3;
                                n90 = n89 + 1;
                                array2[n97 * 2 + 1] = d[n89];
                            }
                            break Label_2165;
                        }
                        array2[n63 / 3 * 2 + 1] = m3.getType();
                        n91 = n60;
                        n90 = n89;
                    }
                    n60 = n91;
                }
                final int n98 = (int)s.objectFieldOffset(m3);
                int n100;
                int n106;
                int n107;
                if ((n65 & 0x1) == 0x1 && n75 <= 17) {
                    final int n99 = n74 + 1;
                    final char char27 = e.charAt(n74);
                    n100 = n99;
                    int n101;
                    if ((n101 = char27) >= 55296) {
                        int n102 = char27 & '\u1fff';
                        int n103 = 13;
                        int index23 = n99;
                        int n104;
                        char char28;
                        while (true) {
                            n104 = index23 + 1;
                            char28 = e.charAt(index23);
                            if (char28 < '\ud800') {
                                break;
                            }
                            n102 |= (char28 & '\u1fff') << n103;
                            n103 += 13;
                            index23 = n104;
                        }
                        n101 = (n102 | char28 << n103);
                        n100 = n104;
                    }
                    final int n105 = n15 * 2 + n101 / 32;
                    final Object o3 = d[n105];
                    Field m4;
                    if (o3 instanceof Field) {
                        m4 = (Field)o3;
                    }
                    else {
                        m4 = m0(class1, (String)o3);
                        d[n105] = m4;
                    }
                    n106 = (int)s.objectFieldOffset(m4);
                    n107 = n101 % 32;
                }
                else {
                    n100 = n74;
                    n106 = 0;
                    n107 = 0;
                }
                final int n108 = n75;
                int n109 = n61;
                if (n108 >= 18) {
                    n109 = n61;
                    if (n108 <= 49) {
                        r[n61] = n98;
                        n109 = n61 + 1;
                    }
                }
                n61 = n109;
                n88 = n107;
                n13 = n90;
                n77 = n100;
                n86 = n98;
                n87 = n106;
            }
            final int n110 = n75;
            final int n111 = n63 + 1;
            array[n63] = char21;
            n63 = n111 + 1;
            int n112;
            if ((char23 & 0x200) != 0x0) {
                n112 = 536870912;
            }
            else {
                n112 = 0;
            }
            int n113;
            if ((char23 & 0x100) != 0x0) {
                n113 = 268435456;
            }
            else {
                n113 = 0;
            }
            array[n111] = (n112 | n113 | n110 << 20 | n86);
            array[n63] = (n88 << 20 | n87);
            final int n114 = n70;
            ++n63;
            j = n77;
            n62 = n76;
            n14 = n114;
        }
        return new a0(array, array2, n, n64, lc1.b(), b, false, r, n14, n59, fz0, t, i0, l, w);
    }
    
    public static long U(final int n) {
        return n & 0xFFFFF;
    }
    
    public static boolean V(final Object o, final long n) {
        return (boolean)e12.E(o, n);
    }
    
    public static double W(final Object o, final long n) {
        return (double)e12.E(o, n);
    }
    
    public static float X(final Object o, final long n) {
        return (float)e12.E(o, n);
    }
    
    public static int Y(final Object o, final long n) {
        return (int)e12.E(o, n);
    }
    
    public static long Z(final Object o, final long n) {
        return (long)e12.E(o, n);
    }
    
    public static boolean k(final Object o, final long n) {
        return e12.r(o, n);
    }
    
    public static Field m0(final Class clazz, final String s) {
        try {
            return clazz.getDeclaredField(s);
        }
        catch (final NoSuchFieldException ex) {
            final Field[] declaredFields = clazz.getDeclaredFields();
            for (final Field field : declaredFields) {
                if (s.equals(field.getName())) {
                    return field;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Field ");
            sb.append(s);
            sb.append(" for ");
            sb.append(clazz.getName());
            sb.append(" not found. Known fields are ");
            sb.append(Arrays.toString(declaredFields));
            throw new RuntimeException(sb.toString());
        }
    }
    
    public static double n(final Object o, final long n) {
        return e12.y(o, n);
    }
    
    public static int q0(final int n) {
        return (n & 0xFF00000) >>> 20;
    }
    
    public static float r(final Object o, final long n) {
        return e12.z(o, n);
    }
    
    public static j0 v(final Object o) {
        final GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)o;
        j0 unknownFields;
        if ((unknownFields = generatedMessageLite.unknownFields) == j0.e()) {
            unknownFields = j0.l();
            generatedMessageLite.unknownFields = unknownFields;
        }
        return unknownFields;
    }
    
    public static int z(final Object o, final long n) {
        return e12.A(o, n);
    }
    
    public final boolean B(Object e, int n) {
        final boolean h = this.h;
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        boolean b7 = false;
        final boolean b8 = false;
        final boolean b9 = false;
        final boolean b10 = false;
        final boolean b11 = false;
        final boolean b12 = false;
        final boolean b13 = false;
        final boolean b14 = false;
        final boolean b15 = false;
        final boolean b16 = false;
        if (!h) {
            n = this.h0(n);
            boolean b17 = b15;
            if ((e12.A(e, n & 0xFFFFF) & 1 << (n >>> 20)) != 0x0) {
                b17 = true;
            }
            return b17;
        }
        n = this.r0(n);
        final long u = U(n);
        switch (q0(n)) {
            default: {
                throw new IllegalArgumentException();
            }
            case 17: {
                boolean b18 = b16;
                if (e12.E(e, u) != null) {
                    b18 = true;
                }
                return b18;
            }
            case 16: {
                boolean b19 = b;
                if (e12.C(e, u) != 0L) {
                    b19 = true;
                }
                return b19;
            }
            case 15: {
                boolean b20 = b2;
                if (e12.A(e, u) != 0) {
                    b20 = true;
                }
                return b20;
            }
            case 14: {
                boolean b21 = b3;
                if (e12.C(e, u) != 0L) {
                    b21 = true;
                }
                return b21;
            }
            case 13: {
                boolean b22 = b4;
                if (e12.A(e, u) != 0) {
                    b22 = true;
                }
                return b22;
            }
            case 12: {
                boolean b23 = b5;
                if (e12.A(e, u) != 0) {
                    b23 = true;
                }
                return b23;
            }
            case 11: {
                boolean b24 = b6;
                if (e12.A(e, u) != 0) {
                    b24 = true;
                }
                return b24;
            }
            case 10: {
                return ByteString.EMPTY.equals(e12.E(e, u)) ^ true;
            }
            case 9: {
                if (e12.E(e, u) != null) {
                    b7 = true;
                }
                return b7;
            }
            case 8: {
                e = e12.E(e, u);
                if (e instanceof String) {
                    return ((String)e).isEmpty() ^ true;
                }
                if (e instanceof ByteString) {
                    return ByteString.EMPTY.equals(e) ^ true;
                }
                throw new IllegalArgumentException();
            }
            case 7: {
                return e12.r(e, u);
            }
            case 6: {
                boolean b25 = b8;
                if (e12.A(e, u) != 0) {
                    b25 = true;
                }
                return b25;
            }
            case 5: {
                boolean b26 = b9;
                if (e12.C(e, u) != 0L) {
                    b26 = true;
                }
                return b26;
            }
            case 4: {
                boolean b27 = b10;
                if (e12.A(e, u) != 0) {
                    b27 = true;
                }
                return b27;
            }
            case 3: {
                boolean b28 = b11;
                if (e12.C(e, u) != 0L) {
                    b28 = true;
                }
                return b28;
            }
            case 2: {
                boolean b29 = b12;
                if (e12.C(e, u) != 0L) {
                    b29 = true;
                }
                return b29;
            }
            case 1: {
                boolean b30 = b13;
                if (e12.z(e, u) != 0.0f) {
                    b30 = true;
                }
                return b30;
            }
            case 0: {
                boolean b31 = b14;
                if (e12.y(e, u) != 0.0) {
                    b31 = true;
                }
                return b31;
            }
        }
    }
    
    public final boolean C(final Object o, final int n, final int n2, final int n3) {
        if (this.h) {
            return this.B(o, n);
        }
        return (n2 & n3) != 0x0;
    }
    
    public final boolean E(final Object o, int i, final int n) {
        final List list = (List)e12.E(o, U(i));
        if (list.isEmpty()) {
            return true;
        }
        final f0 u = this.u(n);
        for (i = 0; i < list.size(); ++i) {
            if (!u.b(list.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean F(final Object o, final int n, final int n2) {
        final Map g = this.q.g(e12.E(o, U(n)));
        if (g.isEmpty()) {
            return true;
        }
        if (this.q.b(this.t(n2)).c.getJavaType() != WireFormat.JavaType.MESSAGE) {
            return true;
        }
        final Iterator iterator = g.values().iterator();
        f0 f0 = null;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            f0 c;
            if ((c = f0) == null) {
                c = l91.a().c(next.getClass());
            }
            f0 = c;
            if (!c.b(next)) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean G(final Object o, final Object o2, final int n) {
        final long n2 = this.h0(n) & 0xFFFFF;
        return e12.A(o, n2) == e12.A(o2, n2);
    }
    
    public final boolean H(final Object o, final int n, final int n2) {
        return e12.A(o, this.h0(n2) & 0xFFFFF) == n;
    }
    
    public final void L(final i0 i0, final l l, final Object o, final e0 e0, final k k) {
        Object o2 = null;
        n n = null;
        while (true) {
            Object p5 = o2;
            try {
                final int m = e0.m();
                p5 = o2;
                final int f0 = this.f0(m);
                if (f0 < 0) {
                    if (m == Integer.MAX_VALUE) {
                        return;
                    }
                    p5 = o2;
                    Object b;
                    if (!this.f) {
                        b = null;
                    }
                    else {
                        p5 = o2;
                        b = l.b(k, this.e, m);
                    }
                    if (b == null) {
                        p5 = o2;
                        if (i0.q(e0)) {
                            p5 = o2;
                            if (e0.p()) {
                                continue;
                            }
                        }
                        else {
                            Object f2;
                            if ((f2 = o2) == null) {
                                p5 = o2;
                                f2 = i0.f(o);
                            }
                            p5 = f2;
                            if (i0.m(f2, e0)) {
                                o2 = f2;
                                continue;
                            }
                        }
                        return;
                    }
                    n d;
                    if ((d = n) == null) {
                        p5 = o2;
                        d = l.d(o);
                    }
                    p5 = o2;
                    o2 = l.g(e0, b, k, d, o2, i0);
                    n = d;
                }
                else {
                    p5 = o2;
                    final int r0 = this.r0(f0);
                    Object o3 = o2;
                    p5 = o2;
                    try {
                        Label_2685: {
                            Object o4 = null;
                            long n4 = 0L;
                            Label_2634: {
                                List list14 = null;
                                Label_2068: {
                                    List list13 = null;
                                    Label_2025: {
                                        List list12 = null;
                                        Label_1982: {
                                            List list11 = null;
                                            Label_1939: {
                                                List list10 = null;
                                                Label_1896: {
                                                    List list9 = null;
                                                    Label_1853: {
                                                        List list8 = null;
                                                        Label_1810: {
                                                            List list7 = null;
                                                            Label_1767: {
                                                                List list6 = null;
                                                                Label_1724: {
                                                                    List list5 = null;
                                                                    r.c c = null;
                                                                    Label_1676: {
                                                                        List list4 = null;
                                                                        Label_1600: {
                                                                            List list3 = null;
                                                                            Label_1557: {
                                                                                List list2 = null;
                                                                                Label_1514: {
                                                                                    List list = null;
                                                                                    Label_1471: {
                                                                                        int n3 = 0;
                                                                                        Label_0895: {
                                                                                            switch (q0(r0)) {
                                                                                                default: {
                                                                                                    Object n2 = o2;
                                                                                                    if (o2 == null) {
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        n2 = i0.n();
                                                                                                    }
                                                                                                    o3 = n2;
                                                                                                    p5 = n2;
                                                                                                    final boolean j = i0.m(n2, e0);
                                                                                                    o2 = n2;
                                                                                                    if (!j) {
                                                                                                        return;
                                                                                                    }
                                                                                                    continue;
                                                                                                }
                                                                                                case 68: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.I(this.u(f0), k));
                                                                                                    break;
                                                                                                }
                                                                                                case 67: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.E());
                                                                                                    break;
                                                                                                }
                                                                                                case 66: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.e());
                                                                                                    break;
                                                                                                }
                                                                                                case 65: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.b());
                                                                                                    break;
                                                                                                }
                                                                                                case 64: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.G());
                                                                                                    break;
                                                                                                }
                                                                                                case 63: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    final int d2 = e0.d();
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    final r.c s = this.s(f0);
                                                                                                    if (s != null) {
                                                                                                        n3 = d2;
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        if (!s.a(d2)) {
                                                                                                            break Label_0895;
                                                                                                        }
                                                                                                    }
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), d2);
                                                                                                    break;
                                                                                                }
                                                                                                case 62: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.c());
                                                                                                    break;
                                                                                                }
                                                                                                case 61: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.g());
                                                                                                    break;
                                                                                                }
                                                                                                case 60: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    if (this.H(o, m, f0)) {
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        final Object h = androidx.datastore.preferences.protobuf.r.h(e12.E(o, U(r0)), e0.J(this.u(f0), k));
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        e12.T(o, U(r0), h);
                                                                                                        break;
                                                                                                    }
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.J(this.u(f0), k));
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.n0(o, f0);
                                                                                                    break;
                                                                                                }
                                                                                                case 59: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.k0(o, r0, e0);
                                                                                                    break;
                                                                                                }
                                                                                                case 58: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.v());
                                                                                                    break;
                                                                                                }
                                                                                                case 57: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.D());
                                                                                                    break;
                                                                                                }
                                                                                                case 56: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.t());
                                                                                                    break;
                                                                                                }
                                                                                                case 55: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.A());
                                                                                                    break;
                                                                                                }
                                                                                                case 54: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.i());
                                                                                                    break;
                                                                                                }
                                                                                                case 53: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.s());
                                                                                                    break;
                                                                                                }
                                                                                                case 52: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.readFloat());
                                                                                                    break;
                                                                                                }
                                                                                                case 51: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.readDouble());
                                                                                                    break;
                                                                                                }
                                                                                                case 50: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.M(o, f0, this.t(f0), k, e0);
                                                                                                    continue;
                                                                                                }
                                                                                                case 49: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.i0(o, U(r0), e0, this.u(f0), k);
                                                                                                    continue;
                                                                                                }
                                                                                                case 48: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list = this.n.e(o, U(r0));
                                                                                                    break Label_1471;
                                                                                                }
                                                                                                case 47: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list2 = this.n.e(o, U(r0));
                                                                                                    break Label_1514;
                                                                                                }
                                                                                                case 46: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list3 = this.n.e(o, U(r0));
                                                                                                    break Label_1557;
                                                                                                }
                                                                                                case 45: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list4 = this.n.e(o, U(r0));
                                                                                                    break Label_1600;
                                                                                                }
                                                                                                case 44: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list5 = this.n.e(o, U(r0));
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e0.y(list5);
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    c = this.s(f0);
                                                                                                    break Label_1676;
                                                                                                }
                                                                                                case 43: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list6 = this.n.e(o, U(r0));
                                                                                                    break Label_1724;
                                                                                                }
                                                                                                case 42: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list7 = this.n.e(o, U(r0));
                                                                                                    break Label_1767;
                                                                                                }
                                                                                                case 41: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list8 = this.n.e(o, U(r0));
                                                                                                    break Label_1810;
                                                                                                }
                                                                                                case 40: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list9 = this.n.e(o, U(r0));
                                                                                                    break Label_1853;
                                                                                                }
                                                                                                case 39: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list10 = this.n.e(o, U(r0));
                                                                                                    break Label_1896;
                                                                                                }
                                                                                                case 38: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list11 = this.n.e(o, U(r0));
                                                                                                    break Label_1939;
                                                                                                }
                                                                                                case 37: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list12 = this.n.e(o, U(r0));
                                                                                                    break Label_1982;
                                                                                                }
                                                                                                case 36: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list13 = this.n.e(o, U(r0));
                                                                                                    break Label_2025;
                                                                                                }
                                                                                                case 35: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list14 = this.n.e(o, U(r0));
                                                                                                    break Label_2068;
                                                                                                }
                                                                                                case 34: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list = this.n.e(o, U(r0));
                                                                                                    break Label_1471;
                                                                                                }
                                                                                                case 33: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list2 = this.n.e(o, U(r0));
                                                                                                    break Label_1514;
                                                                                                }
                                                                                                case 32: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list3 = this.n.e(o, U(r0));
                                                                                                    break Label_1557;
                                                                                                }
                                                                                                case 31: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list4 = this.n.e(o, U(r0));
                                                                                                    break Label_1600;
                                                                                                }
                                                                                                case 30: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list5 = this.n.e(o, U(r0));
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e0.y(list5);
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    c = this.s(f0);
                                                                                                    break Label_1676;
                                                                                                }
                                                                                                case 29: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list6 = this.n.e(o, U(r0));
                                                                                                    break Label_1724;
                                                                                                }
                                                                                                case 28: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e0.q(this.n.e(o, U(r0)));
                                                                                                    continue;
                                                                                                }
                                                                                                case 27: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.j0(o, r0, e0, this.u(f0), k);
                                                                                                    continue;
                                                                                                }
                                                                                                case 26: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.l0(o, r0, e0);
                                                                                                    continue;
                                                                                                }
                                                                                                case 25: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list7 = this.n.e(o, U(r0));
                                                                                                    break Label_1767;
                                                                                                }
                                                                                                case 24: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list8 = this.n.e(o, U(r0));
                                                                                                    break Label_1810;
                                                                                                }
                                                                                                case 23: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list9 = this.n.e(o, U(r0));
                                                                                                    break Label_1853;
                                                                                                }
                                                                                                case 22: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list10 = this.n.e(o, U(r0));
                                                                                                    break Label_1896;
                                                                                                }
                                                                                                case 21: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list11 = this.n.e(o, U(r0));
                                                                                                    break Label_1939;
                                                                                                }
                                                                                                case 20: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list12 = this.n.e(o, U(r0));
                                                                                                    break Label_1982;
                                                                                                }
                                                                                                case 19: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list13 = this.n.e(o, U(r0));
                                                                                                    break Label_2025;
                                                                                                }
                                                                                                case 18: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    list14 = this.n.e(o, U(r0));
                                                                                                    break Label_2068;
                                                                                                }
                                                                                                case 17: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    if (this.B(o, f0)) {
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        o4 = androidx.datastore.preferences.protobuf.r.h(e12.E(o, U(r0)), e0.I(this.u(f0), k));
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        n4 = U(r0);
                                                                                                        break Label_2634;
                                                                                                    }
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.I(this.u(f0), k));
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 16: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.S(o, U(r0), e0.E());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 15: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.R(o, U(r0), e0.e());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 14: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.S(o, U(r0), e0.b());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 13: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.R(o, U(r0), e0.G());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 12: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    final int d3 = e0.d();
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    final r.c s2 = this.s(f0);
                                                                                                    if (s2 != null) {
                                                                                                        n3 = d3;
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        if (!s2.a(d3)) {
                                                                                                            break Label_0895;
                                                                                                        }
                                                                                                    }
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.R(o, U(r0), d3);
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 11: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.R(o, U(r0), e0.c());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 10: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.g());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 9: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    if (this.B(o, f0)) {
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        o4 = androidx.datastore.preferences.protobuf.r.h(e12.E(o, U(r0)), e0.J(this.u(f0), k));
                                                                                                        o3 = o2;
                                                                                                        p5 = o2;
                                                                                                        n4 = U(r0);
                                                                                                        break Label_2634;
                                                                                                    }
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.T(o, U(r0), e0.J(this.u(f0), k));
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 8: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    this.k0(o, r0, e0);
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 7: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.J(o, U(r0), e0.v());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 6: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.R(o, U(r0), e0.D());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 5: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.S(o, U(r0), e0.t());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 4: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.R(o, U(r0), e0.A());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 3: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.S(o, U(r0), e0.i());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 2: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.S(o, U(r0), e0.s());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 1: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.Q(o, U(r0), e0.readFloat());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                                case 0: {
                                                                                                    o3 = o2;
                                                                                                    p5 = o2;
                                                                                                    e12.P(o, U(r0), e0.readDouble());
                                                                                                    break Label_2685;
                                                                                                }
                                                                                            }
                                                                                            o3 = o2;
                                                                                            p5 = o2;
                                                                                            this.o0(o, m, f0);
                                                                                            continue;
                                                                                        }
                                                                                        o3 = o2;
                                                                                        p5 = o2;
                                                                                        o2 = g0.K(m, n3, o2, i0);
                                                                                        continue;
                                                                                    }
                                                                                    o3 = o2;
                                                                                    p5 = o2;
                                                                                    e0.a(list);
                                                                                    continue;
                                                                                }
                                                                                o3 = o2;
                                                                                p5 = o2;
                                                                                e0.h(list2);
                                                                                continue;
                                                                            }
                                                                            o3 = o2;
                                                                            p5 = o2;
                                                                            e0.j(list3);
                                                                            continue;
                                                                        }
                                                                        o3 = o2;
                                                                        p5 = o2;
                                                                        e0.u(list4);
                                                                        continue;
                                                                    }
                                                                    o3 = o2;
                                                                    p5 = o2;
                                                                    o2 = g0.z(m, list5, c, o2, i0);
                                                                    continue;
                                                                }
                                                                o3 = o2;
                                                                p5 = o2;
                                                                e0.C(list6);
                                                                continue;
                                                            }
                                                            o3 = o2;
                                                            p5 = o2;
                                                            e0.f(list7);
                                                            continue;
                                                        }
                                                        o3 = o2;
                                                        p5 = o2;
                                                        e0.l(list8);
                                                        continue;
                                                    }
                                                    o3 = o2;
                                                    p5 = o2;
                                                    e0.B(list9);
                                                    continue;
                                                }
                                                o3 = o2;
                                                p5 = o2;
                                                e0.k(list10);
                                                continue;
                                            }
                                            o3 = o2;
                                            p5 = o2;
                                            e0.w(list11);
                                            continue;
                                        }
                                        o3 = o2;
                                        p5 = o2;
                                        e0.x(list12);
                                        continue;
                                    }
                                    o3 = o2;
                                    p5 = o2;
                                    e0.o(list13);
                                    continue;
                                }
                                o3 = o2;
                                p5 = o2;
                                e0.r(list14);
                                continue;
                            }
                            o3 = o2;
                            p5 = o2;
                            e12.T(o, n4, o4);
                            continue;
                        }
                        o3 = o2;
                        p5 = o2;
                        this.n0(o, f0);
                    }
                    catch (final InvalidProtocolBufferException.InvalidWireTypeException o2) {
                        p5 = o3;
                        if (i0.q(e0)) {
                            p5 = o3;
                            final boolean p6 = e0.p();
                            o2 = o3;
                            if (!p6) {
                                return;
                            }
                            continue;
                        }
                        else {
                            Object f3;
                            if ((f3 = o3) == null) {
                                p5 = o3;
                                f3 = i0.f(o);
                            }
                            p5 = f3;
                            final boolean m2 = i0.m(f3, e0);
                            o2 = f3;
                            if (!m2) {
                                return;
                            }
                            continue;
                        }
                    }
                }
            }
            finally {
                for (int k2 = this.k; k2 < this.l; ++k2) {
                    p5 = this.p(o, this.j[k2], p5, i0);
                }
                if (p5 != null) {
                    i0.o(o, p5);
                }
            }
        }
    }
    
    public final void M(final Object o, final int n, final Object o2, final k k, final e0 e0) {
        final long u = U(this.r0(n));
        final Object e2 = e12.E(o, u);
        Object o3;
        if (e2 == null) {
            o3 = this.q.f(o2);
            e12.T(o, u, o3);
        }
        else {
            o3 = e2;
            if (this.q.h(e2)) {
                o3 = this.q.f(o2);
                this.q.a(o3, e2);
                e12.T(o, u, o3);
            }
        }
        e0.L(this.q.e(o3), this.q.b(o2), k);
    }
    
    public final void N(final Object o, Object o2, final int n) {
        final long u = U(this.r0(n));
        if (!this.B(o2, n)) {
            return;
        }
        final Object e = e12.E(o, u);
        o2 = e12.E(o2, u);
        if (e != null && o2 != null) {
            o2 = androidx.datastore.preferences.protobuf.r.h(e, o2);
        }
        else if (o2 == null) {
            return;
        }
        e12.T(o, u, o2);
        this.n0(o, n);
    }
    
    public final void O(final Object o, Object o2, final int n) {
        final int r0 = this.r0(n);
        final int t = this.T(n);
        final long u = U(r0);
        if (!this.H(o2, t, n)) {
            return;
        }
        final Object e = e12.E(o, u);
        o2 = e12.E(o2, u);
        if (e != null && o2 != null) {
            o2 = androidx.datastore.preferences.protobuf.r.h(e, o2);
        }
        else if (o2 == null) {
            return;
        }
        e12.T(o, u, o2);
        this.o0(o, t, n);
    }
    
    public final void P(final Object o, final Object o2, final int n) {
        final int r0 = this.r0(n);
        final long u = U(r0);
        final int t = this.T(n);
        Label_0676: {
            Label_0616: {
                Label_0580: {
                    Label_0508: {
                        Label_0462: {
                            switch (q0(r0)) {
                                default: {
                                    return;
                                }
                                case 61:
                                case 62:
                                case 63:
                                case 64:
                                case 65:
                                case 66:
                                case 67: {
                                    if (this.H(o2, t, n)) {
                                        break;
                                    }
                                    return;
                                }
                                case 60:
                                case 68: {
                                    this.O(o, o2, n);
                                    return;
                                }
                                case 51:
                                case 52:
                                case 53:
                                case 54:
                                case 55:
                                case 56:
                                case 57:
                                case 58:
                                case 59: {
                                    if (this.H(o2, t, n)) {
                                        break;
                                    }
                                    return;
                                }
                                case 50: {
                                    g0.E(this.q, o, o2, u);
                                    return;
                                }
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                case 41:
                                case 42:
                                case 43:
                                case 44:
                                case 45:
                                case 46:
                                case 47:
                                case 48:
                                case 49: {
                                    this.n.d(o, o2, u);
                                    return;
                                }
                                case 16: {
                                    if (this.B(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 15: {
                                    if (this.B(o2, n)) {
                                        break Label_0462;
                                    }
                                    return;
                                }
                                case 14: {
                                    if (this.B(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 13: {
                                    if (this.B(o2, n)) {
                                        break Label_0462;
                                    }
                                    return;
                                }
                                case 12: {
                                    if (this.B(o2, n)) {
                                        break Label_0462;
                                    }
                                    return;
                                }
                                case 11: {
                                    if (this.B(o2, n)) {
                                        break Label_0580;
                                    }
                                    return;
                                }
                                case 10: {
                                    if (this.B(o2, n)) {
                                        break Label_0508;
                                    }
                                    return;
                                }
                                case 9:
                                case 17: {
                                    this.N(o, o2, n);
                                    return;
                                }
                                case 8: {
                                    if (this.B(o2, n)) {
                                        break Label_0508;
                                    }
                                    return;
                                }
                                case 7: {
                                    if (this.B(o2, n)) {
                                        e12.J(o, u, e12.r(o2, u));
                                        break Label_0676;
                                    }
                                    return;
                                }
                                case 6: {
                                    if (this.B(o2, n)) {
                                        break Label_0580;
                                    }
                                    return;
                                }
                                case 5: {
                                    if (this.B(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 4: {
                                    if (this.B(o2, n)) {
                                        break Label_0580;
                                    }
                                    return;
                                }
                                case 3: {
                                    if (this.B(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 2: {
                                    if (this.B(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 1: {
                                    if (this.B(o2, n)) {
                                        e12.Q(o, u, e12.z(o2, u));
                                        break Label_0676;
                                    }
                                    return;
                                }
                                case 0: {
                                    if (this.B(o2, n)) {
                                        e12.P(o, u, e12.y(o2, u));
                                        break Label_0676;
                                    }
                                    return;
                                }
                            }
                            e12.T(o, u, e12.E(o2, u));
                            this.o0(o, t, n);
                            return;
                        }
                        break Label_0580;
                    }
                    e12.T(o, u, e12.E(o2, u));
                    break Label_0676;
                }
                e12.R(o, u, e12.A(o2, u));
                break Label_0676;
            }
            e12.S(o, u, e12.C(o2, u));
        }
        this.n0(o, n);
    }
    
    public final int T(final int n) {
        return this.a[n];
    }
    
    @Override
    public void a(final Object o, final Object o2) {
        o2.getClass();
        for (int i = 0; i < this.a.length; i += 3) {
            this.P(o, o2, i);
        }
        if (!this.h) {
            g0.F(this.o, o, o2);
            if (this.f) {
                g0.D(this.p, o, o2);
            }
        }
    }
    
    public final int a0(final Object o, final byte[] array, final int n, final int n2, final int n3, final long n4, final d.a a) {
        final Unsafe s = a0.s;
        final Object t = this.t(n3);
        Object x;
        final Object o2 = x = s.getObject(o, n4);
        if (this.q.h(o2)) {
            x = this.q.f(t);
            this.q.a(x, o2);
            s.putObject(o, n4, x);
        }
        return this.l(array, n, n2, this.q.b(t), this.q.e(x), a);
    }
    
    @Override
    public final boolean b(final Object o) {
        int n = -1;
        int i = 0;
        int int1 = 0;
        while (i < this.k) {
            final int n2 = this.j[i];
            final int t = this.T(n2);
            final int r0 = this.r0(n2);
            int n6;
            int n7;
            if (!this.h) {
                final int n3 = this.a[n2 + 2];
                final int n4 = 0xFFFFF & n3;
                final int n5 = 1 << (n3 >>> 20);
                n6 = n;
                n7 = n5;
                if (n4 != n) {
                    int1 = a0.s.getInt(o, n4);
                    n6 = n4;
                    n7 = n5;
                }
            }
            else {
                n7 = 0;
                n6 = n;
            }
            if (I(r0) && !this.C(o, n2, int1, n7)) {
                return false;
            }
            final int q0 = q0(r0);
            Label_0278: {
                if (q0 != 9 && q0 != 17) {
                    if (q0 != 27) {
                        if (q0 != 60 && q0 != 68) {
                            if (q0 != 49) {
                                if (q0 != 50) {
                                    break Label_0278;
                                }
                                if (!this.F(o, r0, n2)) {
                                    return false;
                                }
                                break Label_0278;
                            }
                        }
                        else {
                            if (this.H(o, t, n2) && !D(o, r0, this.u(n2))) {
                                return false;
                            }
                            break Label_0278;
                        }
                    }
                    if (!this.E(o, r0, n2)) {
                        return false;
                    }
                }
                else if (this.C(o, n2, int1, n7) && !D(o, r0, this.u(n2))) {
                    return false;
                }
            }
            ++i;
            n = n6;
        }
        return !this.f || this.p.c(o).k();
    }
    
    public final int b0(final Object o, final byte[] bytes, int offset, int i, final int n, final int n2, final int n3, final int n4, final int n5, final long offset2, final int n6, final d.a a) {
        final Unsafe s = a0.s;
        final long n7 = this.a[n6 + 2] & 0xFFFFF;
        Label_0725: {
            Number x3 = null;
            Label_0713: {
                Number x2 = null;
                Label_0683: {
                    Object x = null;
                    Label_0656: {
                        long l = 0L;
                        Label_0650: {
                            switch (n5) {
                                default: {
                                    return offset;
                                }
                                case 68: {
                                    if (n3 != 3) {
                                        return offset;
                                    }
                                    offset = androidx.datastore.preferences.protobuf.d.m(this.u(n6), bytes, offset, i, (n & 0xFFFFFFF8) | 0x4, a);
                                    Object object;
                                    if (s.getInt(o, n7) == n2) {
                                        object = s.getObject(o, offset2);
                                    }
                                    else {
                                        object = null;
                                    }
                                    final Object c = a.c;
                                    if (object == null) {
                                        x = c;
                                        break Label_0656;
                                    }
                                    x = androidx.datastore.preferences.protobuf.r.h(object, c);
                                    break Label_0656;
                                }
                                case 67: {
                                    if (n3 == 0) {
                                        offset = androidx.datastore.preferences.protobuf.d.K(bytes, offset, a);
                                        l = androidx.datastore.preferences.protobuf.f.c(a.b);
                                        break Label_0650;
                                    }
                                    return offset;
                                }
                                case 66: {
                                    if (n3 == 0) {
                                        offset = androidx.datastore.preferences.protobuf.d.H(bytes, offset, a);
                                        i = androidx.datastore.preferences.protobuf.f.b(a.a);
                                        break;
                                    }
                                    return offset;
                                }
                                case 63: {
                                    if (n3 == 0) {
                                        offset = androidx.datastore.preferences.protobuf.d.H(bytes, offset, a);
                                        i = a.a;
                                        final r.c s2 = this.s(n6);
                                        if (s2 != null && !s2.a(i)) {
                                            v(o).n(n, (long)i);
                                        }
                                        else {
                                            s.putObject(o, offset2, i);
                                            s.putInt(o, n7, n2);
                                        }
                                        return offset;
                                    }
                                    return offset;
                                }
                                case 61: {
                                    if (n3 == 2) {
                                        offset = androidx.datastore.preferences.protobuf.d.b(bytes, offset, a);
                                        x = a.c;
                                        break Label_0656;
                                    }
                                    return offset;
                                }
                                case 60: {
                                    if (n3 != 2) {
                                        return offset;
                                    }
                                    offset = androidx.datastore.preferences.protobuf.d.o(this.u(n6), bytes, offset, i, a);
                                    Object object2;
                                    if (s.getInt(o, n7) == n2) {
                                        object2 = s.getObject(o, offset2);
                                    }
                                    else {
                                        object2 = null;
                                    }
                                    final Object c2 = a.c;
                                    if (object2 == null) {
                                        x = c2;
                                        break Label_0656;
                                    }
                                    x = androidx.datastore.preferences.protobuf.r.h(object2, c2);
                                    break Label_0656;
                                }
                                case 59: {
                                    if (n3 != 2) {
                                        return offset;
                                    }
                                    offset = androidx.datastore.preferences.protobuf.d.H(bytes, offset, a);
                                    i = a.a;
                                    if (i == 0) {
                                        x = "";
                                        break Label_0656;
                                    }
                                    if ((n4 & 0x20000000) != 0x0 && !Utf8.t(bytes, offset, offset + i)) {
                                        throw InvalidProtocolBufferException.invalidUtf8();
                                    }
                                    s.putObject(o, offset2, new String(bytes, offset, i, androidx.datastore.preferences.protobuf.r.a));
                                    offset += i;
                                    break Label_0725;
                                }
                                case 58: {
                                    if (n3 == 0) {
                                        offset = androidx.datastore.preferences.protobuf.d.K(bytes, offset, a);
                                        x = (a.b != 0L);
                                        break Label_0656;
                                    }
                                    return offset;
                                }
                                case 57:
                                case 64: {
                                    if (n3 == 5) {
                                        x2 = androidx.datastore.preferences.protobuf.d.g(bytes, offset);
                                        break Label_0683;
                                    }
                                    return offset;
                                }
                                case 56:
                                case 65: {
                                    if (n3 == 1) {
                                        x3 = androidx.datastore.preferences.protobuf.d.i(bytes, offset);
                                        break Label_0713;
                                    }
                                    return offset;
                                }
                                case 55:
                                case 62: {
                                    if (n3 == 0) {
                                        offset = androidx.datastore.preferences.protobuf.d.H(bytes, offset, a);
                                        i = a.a;
                                        break;
                                    }
                                    return offset;
                                }
                                case 53:
                                case 54: {
                                    if (n3 == 0) {
                                        offset = androidx.datastore.preferences.protobuf.d.K(bytes, offset, a);
                                        l = a.b;
                                        break Label_0650;
                                    }
                                    return offset;
                                }
                                case 52: {
                                    if (n3 == 5) {
                                        x2 = androidx.datastore.preferences.protobuf.d.k(bytes, offset);
                                        break Label_0683;
                                    }
                                    return offset;
                                }
                                case 51: {
                                    if (n3 == 1) {
                                        x3 = androidx.datastore.preferences.protobuf.d.d(bytes, offset);
                                        break Label_0713;
                                    }
                                    return offset;
                                }
                            }
                            x = i;
                            break Label_0656;
                        }
                        x = l;
                    }
                    s.putObject(o, offset2, x);
                    break Label_0725;
                }
                s.putObject(o, offset2, x2);
                offset += 4;
                break Label_0725;
            }
            s.putObject(o, offset2, x3);
            offset += 8;
        }
        s.putInt(o, n7, n2);
        return offset;
    }
    
    @Override
    public boolean c(final Object o, final Object o2) {
        for (int length = this.a.length, i = 0; i < length; i += 3) {
            if (!this.o(o, o2, i)) {
                return false;
            }
        }
        return this.o.g(o).equals(this.o.g(o2)) && (!this.f || this.p.c(o).equals(this.p.c(o2)));
    }
    
    public int c0(final Object o, final byte[] array, int i, final int n, int n2, final d.a a) {
        int n3 = n2;
        final Unsafe s = a0.s;
        int n4 = 0;
        int n6;
        int n5 = n6 = 0;
        int n7 = -1;
        int n8 = -1;
        while (true) {
        Label_1342_Outer:
            while (i < n) {
                int g = i + 1;
                i = array[i];
                if (i < 0) {
                    g = androidx.datastore.preferences.protobuf.d.G(i, array, g, a);
                    i = a.a;
                }
                final int n9 = i >>> 3;
                final int n10 = i & 0x7;
                int n11;
                if (n9 > n7) {
                    n11 = this.g0(n9, n4 / 3);
                }
                else {
                    n11 = this.f0(n9);
                }
                while (true) {
                    int n13 = 0;
                    int n14 = 0;
                    int n15 = 0;
                    int n16 = 0;
                    Label_1496: {
                        if (n11 == -1) {
                            final int n12 = n6;
                            n13 = n8;
                            n14 = n3;
                            n15 = 0;
                            n16 = n12;
                            break Label_1496;
                        }
                        final int n17 = this.a[n11 + 1];
                        final int q0 = q0(n17);
                        final long u = U(n17);
                        int p6 = 0;
                        Label_1227: {
                            if (q0 <= 17) {
                                final int n18 = this.a[n11 + 2];
                                final int n19 = 1 << (n18 >>> 20);
                                final int n20 = n18 & 0xFFFFF;
                                int n21;
                                if (n20 != n8) {
                                    if (n8 != -1) {
                                        s.putInt(o, n8, n6);
                                    }
                                    final int int1 = s.getInt(o, n20);
                                    n8 = n20;
                                    n21 = int1;
                                }
                                else {
                                    n21 = n6;
                                }
                                Label_1077: {
                                    int n22 = 0;
                                    Label_1055: {
                                        Label_1048: {
                                            Label_1041: {
                                                int n26 = 0;
                                                Label_0963: {
                                                    int n23 = 0;
                                                    long x2 = 0L;
                                                    Label_0923: {
                                                        int x3 = 0;
                                                        Label_0888: {
                                                            int n24 = 0;
                                                            Label_0807: {
                                                                Object x4 = null;
                                                                Label_0723: {
                                                                    switch (q0) {
                                                                        case 17: {
                                                                            if (n10 == 3) {
                                                                                n22 = androidx.datastore.preferences.protobuf.d.m(this.u(n11), array, g, n, n9 << 3 | 0x4, a);
                                                                                Object x;
                                                                                if ((n21 & n19) == 0x0) {
                                                                                    x = a.c;
                                                                                }
                                                                                else {
                                                                                    x = androidx.datastore.preferences.protobuf.r.h(s.getObject(o, u), a.c);
                                                                                }
                                                                                s.putObject(o, u, x);
                                                                                n21 |= n19;
                                                                                break Label_1055;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 16: {
                                                                            if (n10 == 0) {
                                                                                n23 = androidx.datastore.preferences.protobuf.d.K(array, g, a);
                                                                                x2 = androidx.datastore.preferences.protobuf.f.c(a.b);
                                                                                break Label_0923;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 15: {
                                                                            if (n10 == 0) {
                                                                                n22 = androidx.datastore.preferences.protobuf.d.H(array, g, a);
                                                                                x3 = androidx.datastore.preferences.protobuf.f.b(a.a);
                                                                                break Label_0888;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 12: {
                                                                            if (n10 != 0) {
                                                                                break;
                                                                            }
                                                                            final int h = androidx.datastore.preferences.protobuf.d.H(array, g, a);
                                                                            final int a2 = a.a;
                                                                            final r.c s2 = this.s(n11);
                                                                            n22 = h;
                                                                            x3 = a2;
                                                                            if (s2 == null) {
                                                                                break Label_0888;
                                                                            }
                                                                            if (s2.a(a2)) {
                                                                                n22 = h;
                                                                                x3 = a2;
                                                                                break Label_0888;
                                                                            }
                                                                            v(o).n(i, (long)a2);
                                                                            n22 = h;
                                                                            break Label_1055;
                                                                        }
                                                                        case 10: {
                                                                            if (n10 == 2) {
                                                                                n22 = androidx.datastore.preferences.protobuf.d.b(array, g, a);
                                                                                s.putObject(o, u, a.c);
                                                                                break Label_1048;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 9: {
                                                                            if (n10 != 2) {
                                                                                break;
                                                                            }
                                                                            n24 = androidx.datastore.preferences.protobuf.d.o(this.u(n11), array, g, n, a);
                                                                            if ((n21 & n19) == 0x0) {
                                                                                x4 = a.c;
                                                                                break Label_0723;
                                                                            }
                                                                            x4 = androidx.datastore.preferences.protobuf.r.h(s.getObject(o, u), a.c);
                                                                            break Label_0723;
                                                                        }
                                                                        case 8: {
                                                                            if (n10 == 2) {
                                                                                if ((n17 & 0x20000000) == 0x0) {
                                                                                    n24 = androidx.datastore.preferences.protobuf.d.B(array, g, a);
                                                                                }
                                                                                else {
                                                                                    n24 = androidx.datastore.preferences.protobuf.d.E(array, g, a);
                                                                                }
                                                                                x4 = a.c;
                                                                                break Label_0723;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 7: {
                                                                            if (n10 == 0) {
                                                                                n24 = androidx.datastore.preferences.protobuf.d.K(array, g, a);
                                                                                e12.J(o, u, a.b != 0L);
                                                                                break Label_0807;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 6:
                                                                        case 13: {
                                                                            if (n10 == 5) {
                                                                                s.putInt(o, u, androidx.datastore.preferences.protobuf.d.g(array, g));
                                                                                n24 = g + 4;
                                                                                break Label_0807;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 5:
                                                                        case 14: {
                                                                            if (n10 == 1) {
                                                                                s.putLong(o, u, androidx.datastore.preferences.protobuf.d.i(array, g));
                                                                                break Label_1041;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 4:
                                                                        case 11: {
                                                                            if (n10 == 0) {
                                                                                n22 = androidx.datastore.preferences.protobuf.d.H(array, g, a);
                                                                                x3 = a.a;
                                                                                break Label_0888;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 2:
                                                                        case 3: {
                                                                            if (n10 == 0) {
                                                                                n23 = androidx.datastore.preferences.protobuf.d.K(array, g, a);
                                                                                x2 = a.b;
                                                                                break Label_0923;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 1: {
                                                                            if (n10 == 5) {
                                                                                e12.Q(o, u, androidx.datastore.preferences.protobuf.d.k(array, g));
                                                                                n22 = g + 4;
                                                                                break Label_1048;
                                                                            }
                                                                            break;
                                                                        }
                                                                        case 0: {
                                                                            if (n10 == 1) {
                                                                                e12.P(o, u, androidx.datastore.preferences.protobuf.d.d(array, g));
                                                                                break Label_1041;
                                                                            }
                                                                            break;
                                                                        }
                                                                    }
                                                                    break Label_1077;
                                                                }
                                                                s.putObject(o, u, x4);
                                                            }
                                                            final int n25 = n21 | n19;
                                                            n26 = n24;
                                                            n6 = n25;
                                                            break Label_0963;
                                                            break Label_1077;
                                                        }
                                                        s.putInt(o, u, x3);
                                                        break Label_1048;
                                                    }
                                                    s.putLong(o, u, x2);
                                                    final int n27 = n21 | n19;
                                                    n26 = n23;
                                                    n6 = n27;
                                                }
                                                final int n28 = i;
                                                n7 = n9;
                                                final int n29 = n2;
                                                i = n26;
                                                n4 = n11;
                                                n5 = n28;
                                                n3 = n29;
                                                continue Label_1342_Outer;
                                            }
                                            n22 = g + 8;
                                        }
                                        n21 |= n19;
                                    }
                                    p6 = n22;
                                    n6 = n21;
                                    break Label_1227;
                                }
                                final int n30 = n2;
                                final int n31 = n21;
                                n13 = n8;
                                n15 = n11;
                                n14 = n30;
                                n16 = n31;
                                break Label_1496;
                            }
                            Label_1406: {
                                if (q0 != 27) {
                                    n15 = n11;
                                    final int n32 = n6;
                                    int n33 = 0;
                                    Label_1477: {
                                        int n34;
                                        if (q0 <= 49) {
                                            final int e0 = this.e0(o, array, g, n, i, n9, n10, n15, n17, q0, u, a);
                                            if ((n33 = e0) == g) {
                                                break Label_1477;
                                            }
                                            n34 = e0;
                                        }
                                        else if (q0 == 50) {
                                            if (n10 != 2) {
                                                break Label_1406;
                                            }
                                            final int a3 = this.a0(o, array, g, n, n15, u, a);
                                            if ((n33 = a3) == g) {
                                                break Label_1477;
                                            }
                                            n34 = a3;
                                        }
                                        else {
                                            final int b0 = this.b0(o, array, g, n, i, n9, n10, n17, q0, u, n15, a);
                                            if ((n33 = b0) == g) {
                                                break Label_1477;
                                            }
                                            n34 = b0;
                                        }
                                        final int n35 = n2;
                                        n4 = n15;
                                        n3 = n35;
                                        n6 = n32;
                                        final int n36 = i;
                                        i = n34;
                                        break Label_1342;
                                    }
                                    n13 = n8;
                                    g = n33;
                                    n14 = n2;
                                    n16 = n32;
                                    break Label_1496;
                                }
                                if (n10 == 2) {
                                    Object j;
                                    final Object o2 = j = s.getObject(o, u);
                                    if (!((r.d)o2).h()) {
                                        final int size = ((List)o2).size();
                                        int n37;
                                        if (size == 0) {
                                            n37 = 10;
                                        }
                                        else {
                                            n37 = size * 2;
                                        }
                                        j = ((r.d)o2).j(n37);
                                        s.putObject(o, u, j);
                                    }
                                    p6 = androidx.datastore.preferences.protobuf.d.p(this.u(n11), i, array, g, n, (r.d)j, a);
                                    break Label_1227;
                                }
                            }
                            final int n38 = n6;
                            n15 = n11;
                            n13 = n8;
                            n14 = n2;
                            n16 = n38;
                            break Label_1496;
                        }
                        final int n39 = i;
                        n3 = n2;
                        i = n;
                        i = p6;
                        n7 = n9;
                        n4 = n11;
                        n5 = n39;
                        continue Label_1342_Outer;
                        n7 = n9;
                        int n36 = 0;
                        n5 = n36;
                        continue Label_1342_Outer;
                    }
                    final int n40 = i;
                    if (n40 == n14 && n14 != 0) {
                        i = g;
                        n2 = n40;
                        final int n41 = n13;
                        n6 = n16;
                        n8 = n41;
                        final int n42 = n14;
                        if (n8 != -1) {
                            s.putInt(o, n8, n6);
                        }
                        int k = this.k;
                        Object o3 = null;
                        while (k < this.l) {
                            o3 = this.p(o, this.j[k], o3, this.o);
                            ++k;
                        }
                        if (o3 != null) {
                            this.o.o(o, o3);
                        }
                        if (n42 == 0) {
                            if (i != n) {
                                throw InvalidProtocolBufferException.parseFailure();
                            }
                        }
                        else if (i > n || n2 != n42) {
                            throw InvalidProtocolBufferException.parseFailure();
                        }
                        return i;
                    }
                    if (this.f && a.d != androidx.datastore.preferences.protobuf.k.b()) {
                        i = androidx.datastore.preferences.protobuf.d.f(n40, array, g, n, o, this.e, this.o, a);
                    }
                    else {
                        i = androidx.datastore.preferences.protobuf.d.F(n40, array, g, n, v(o), a);
                    }
                    final int n36 = n40;
                    final int n43 = n13;
                    n6 = n16;
                    n4 = n15;
                    n8 = n43;
                    n3 = n14;
                    continue;
                }
            }
            final int n42 = n3;
            n2 = n5;
            continue;
        }
    }
    
    @Override
    public void d(final Object o) {
        int k = this.k;
        int l;
        while (true) {
            l = this.l;
            if (k >= l) {
                break;
            }
            final long u = U(this.r0(this.j[k]));
            final Object e = e12.E(o, u);
            if (e != null) {
                e12.T(o, u, this.q.c(e));
            }
            ++k;
        }
        for (int length = this.j.length, i = l; i < length; ++i) {
            this.n.c(o, this.j[i]);
        }
        this.o.j(o);
        if (this.f) {
            this.p.f(o);
        }
    }
    
    public final int d0(final Object o, final byte[] array, int n, final int n2, final d.a a) {
        final Unsafe s = a0.s;
        int n3 = -1;
        int i = n;
        n = -1;
        int n4 = 0;
        while (i < n2) {
            final int n5 = i + 1;
            int a2 = array[i];
            if (a2 < 0) {
                i = androidx.datastore.preferences.protobuf.d.G(a2, array, n5, a);
                a2 = a.a;
            }
            else {
                i = n5;
            }
            final int n6 = a2 >>> 3;
            final int n7 = a2 & 0x7;
            if (n6 > n) {
                n = this.g0(n6, n4 / 3);
            }
            else {
                n = this.f0(n6);
            }
            int n8 = 0;
            int n12 = 0;
            Label_1012: {
                int n10 = 0;
                Label_0988: {
                    if (n == n3) {
                        n = i;
                        n8 = n3;
                        final int n9 = 0;
                        n10 = n;
                        n = n9;
                    }
                    else {
                        final int n11 = this.a[n + 1];
                        final int q0 = q0(n11);
                        final long u = U(n11);
                        Label_0803: {
                            Label_0666: {
                                if (q0 <= 17) {
                                    boolean b = true;
                                    Label_0678: {
                                        Label_0663: {
                                            Label_0660: {
                                                long x = 0L;
                                                Label_0605: {
                                                    int x2 = 0;
                                                    Label_0570: {
                                                        Label_0553: {
                                                            Label_0519: {
                                                                Object x3 = null;
                                                                Label_0338: {
                                                                    switch (q0) {
                                                                        default: {
                                                                            break Label_0803;
                                                                        }
                                                                        case 16: {
                                                                            if (n7 == 0) {
                                                                                i = androidx.datastore.preferences.protobuf.d.K(array, i, a);
                                                                                x = androidx.datastore.preferences.protobuf.f.c(a.b);
                                                                                break Label_0605;
                                                                            }
                                                                            break Label_0803;
                                                                        }
                                                                        case 15: {
                                                                            if (n7 == 0) {
                                                                                i = androidx.datastore.preferences.protobuf.d.H(array, i, a);
                                                                                x2 = androidx.datastore.preferences.protobuf.f.b(a.a);
                                                                                break Label_0570;
                                                                            }
                                                                            break Label_0678;
                                                                        }
                                                                        case 12: {
                                                                            if (n7 == 0) {
                                                                                break Label_0553;
                                                                            }
                                                                            break Label_0678;
                                                                        }
                                                                        case 10: {
                                                                            if (n7 == 2) {
                                                                                i = androidx.datastore.preferences.protobuf.d.b(array, i, a);
                                                                                break;
                                                                            }
                                                                            break Label_0803;
                                                                        }
                                                                        case 9: {
                                                                            if (n7 != 2) {
                                                                                break Label_0803;
                                                                            }
                                                                            i = androidx.datastore.preferences.protobuf.d.o(this.u(n), array, i, n2, a);
                                                                            final Object object = s.getObject(o, u);
                                                                            if (object == null) {
                                                                                x3 = a.c;
                                                                                break Label_0338;
                                                                            }
                                                                            x3 = androidx.datastore.preferences.protobuf.r.h(object, a.c);
                                                                            break Label_0338;
                                                                        }
                                                                        case 8: {
                                                                            if (n7 != 2) {
                                                                                break Label_0803;
                                                                            }
                                                                            if ((0x20000000 & n11) == 0x0) {
                                                                                i = androidx.datastore.preferences.protobuf.d.B(array, i, a);
                                                                                break;
                                                                            }
                                                                            i = androidx.datastore.preferences.protobuf.d.E(array, i, a);
                                                                            break;
                                                                        }
                                                                        case 7: {
                                                                            if (n7 == 0) {
                                                                                i = androidx.datastore.preferences.protobuf.d.K(array, i, a);
                                                                                if (a.b == 0L) {
                                                                                    b = false;
                                                                                }
                                                                                e12.J(o, u, b);
                                                                                break Label_0519;
                                                                            }
                                                                            break Label_0803;
                                                                        }
                                                                        case 6:
                                                                        case 13: {
                                                                            if (n7 == 5) {
                                                                                s.putInt(o, u, androidx.datastore.preferences.protobuf.d.g(array, i));
                                                                                i += 4;
                                                                                break Label_0519;
                                                                            }
                                                                            break Label_0803;
                                                                        }
                                                                        case 5:
                                                                        case 14: {
                                                                            if (n7 == 1) {
                                                                                s.putLong(o, u, androidx.datastore.preferences.protobuf.d.i(array, i));
                                                                                break Label_0660;
                                                                            }
                                                                            break Label_0803;
                                                                        }
                                                                        case 4:
                                                                        case 11: {
                                                                            if (n7 == 0) {
                                                                                break Label_0553;
                                                                            }
                                                                            break Label_0678;
                                                                        }
                                                                        case 2:
                                                                        case 3: {
                                                                            if (n7 == 0) {
                                                                                i = androidx.datastore.preferences.protobuf.d.K(array, i, a);
                                                                                x = a.b;
                                                                                break Label_0605;
                                                                            }
                                                                            break Label_0678;
                                                                        }
                                                                        case 1: {
                                                                            if (n7 == 5) {
                                                                                e12.Q(o, u, androidx.datastore.preferences.protobuf.d.k(array, i));
                                                                                i += 4;
                                                                                break Label_0663;
                                                                            }
                                                                            break Label_0678;
                                                                        }
                                                                        case 0: {
                                                                            if (n7 == 1) {
                                                                                e12.P(o, u, androidx.datastore.preferences.protobuf.d.d(array, i));
                                                                                break Label_0660;
                                                                            }
                                                                            break Label_0678;
                                                                        }
                                                                    }
                                                                    x3 = a.c;
                                                                }
                                                                s.putObject(o, u, x3);
                                                            }
                                                            break Label_0666;
                                                        }
                                                        i = androidx.datastore.preferences.protobuf.d.H(array, i, a);
                                                        x2 = a.a;
                                                    }
                                                    s.putInt(o, u, x2);
                                                    break Label_0663;
                                                }
                                                s.putLong(o, u, x);
                                                break Label_0663;
                                            }
                                            i += 8;
                                        }
                                        break Label_0666;
                                    }
                                    break Label_0803;
                                }
                                if (q0 != 27) {
                                    n12 = n;
                                    Label_0975: {
                                        if (q0 <= 49) {
                                            final int e0 = this.e0(o, array, i, n2, a2, n6, n7, n12, n11, q0, u, a);
                                            if ((n = e0) == i) {
                                                break Label_0975;
                                            }
                                            n = e0;
                                        }
                                        else if (q0 == 50) {
                                            if (n7 != 2) {
                                                break Label_0803;
                                            }
                                            final int a3 = this.a0(o, array, i, n2, n12, u, a);
                                            if ((n = a3) == i) {
                                                break Label_0975;
                                            }
                                            n = a3;
                                        }
                                        else {
                                            final int b2 = this.b0(o, array, i, n2, a2, n6, n7, n11, q0, u, n12, a);
                                            if ((n = b2) == i) {
                                                break Label_0975;
                                            }
                                            n = b2;
                                        }
                                        n8 = -1;
                                        break Label_1012;
                                    }
                                    n8 = -1;
                                    final int n13 = n;
                                    n = n12;
                                    n10 = n13;
                                    break Label_0988;
                                }
                                if (n7 != 2) {
                                    break Label_0803;
                                }
                                Object j;
                                final Object o2 = j = s.getObject(o, u);
                                if (!((r.d)o2).h()) {
                                    final int size = ((List)o2).size();
                                    int n14;
                                    if (size == 0) {
                                        n14 = 10;
                                    }
                                    else {
                                        n14 = size * 2;
                                    }
                                    j = ((r.d)o2).j(n14);
                                    s.putObject(o, u, j);
                                }
                                i = androidx.datastore.preferences.protobuf.d.p(this.u(n), a2, array, i, n2, (r.d)j, a);
                            }
                            n4 = n;
                            n = n6;
                            n3 = -1;
                            continue;
                        }
                        final int n15 = -1;
                        n10 = i;
                        n8 = n15;
                    }
                }
                final int f = androidx.datastore.preferences.protobuf.d.F(a2, array, n10, n2, v(o), a);
                n12 = n;
                n = f;
            }
            final int n16 = n6;
            final int n17 = n8;
            i = n;
            n = n16;
            n4 = n12;
            n3 = n17;
        }
        if (i == n2) {
            return i;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    @Override
    public int e(final Object o) {
        int n;
        if (this.h) {
            n = this.x(o);
        }
        else {
            n = this.w(o);
        }
        return n;
    }
    
    public final int e0(final Object o, final byte[] array, int n, final int n2, final int n3, final int n4, final int n5, final int n6, final long n7, final int n8, final long n9, final d.a a) {
        final Unsafe s = a0.s;
        Object j;
        final Object o2 = j = s.getObject(o, n9);
        if (!((r.d)o2).h()) {
            final int size = ((List)o2).size();
            int n10;
            if (size == 0) {
                n10 = 10;
            }
            else {
                n10 = size * 2;
            }
            j = ((r.d)o2).j(n10);
            s.putObject(o, n9, j);
        }
        switch (n8) {
            case 49: {
                if (n5 == 3) {
                    n = androidx.datastore.preferences.protobuf.d.n(this.u(n6), n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 34:
            case 48: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.w(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = androidx.datastore.preferences.protobuf.d.A(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 33:
            case 47: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.v(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = androidx.datastore.preferences.protobuf.d.z(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 30:
            case 44: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.x(array, n, (r.d)j, a);
                }
                else {
                    if (n5 != 0) {
                        break;
                    }
                    n = androidx.datastore.preferences.protobuf.d.I(n3, array, n, n2, (r.d)j, a);
                }
                final GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)o;
                j0 unknownFields;
                if ((unknownFields = generatedMessageLite.unknownFields) == j0.e()) {
                    unknownFields = null;
                }
                final j0 unknownFields2 = (j0)g0.z(n4, (List)j, this.s(n6), unknownFields, this.o);
                if (unknownFields2 != null) {
                    generatedMessageLite.unknownFields = unknownFields2;
                }
                break;
            }
            case 28: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.c(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 27: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.p(this.u(n6), n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 26: {
                if (n5 != 2) {
                    break;
                }
                if ((n7 & 0x20000000L) == 0x0L) {
                    n = androidx.datastore.preferences.protobuf.d.C(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                n = androidx.datastore.preferences.protobuf.d.D(n3, array, n, n2, (r.d)j, a);
                break;
            }
            case 25:
            case 42: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.q(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = androidx.datastore.preferences.protobuf.d.a(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 24:
            case 31:
            case 41:
            case 45: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.s(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 5) {
                    n = androidx.datastore.preferences.protobuf.d.h(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 23:
            case 32:
            case 40:
            case 46: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.t(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 1) {
                    n = androidx.datastore.preferences.protobuf.d.j(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 22:
            case 29:
            case 39:
            case 43: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.x(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = androidx.datastore.preferences.protobuf.d.I(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 20:
            case 21:
            case 37:
            case 38: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.y(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = androidx.datastore.preferences.protobuf.d.L(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 19:
            case 36: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.u(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 5) {
                    n = androidx.datastore.preferences.protobuf.d.l(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
            case 18:
            case 35: {
                if (n5 == 2) {
                    n = androidx.datastore.preferences.protobuf.d.r(array, n, (r.d)j, a);
                    break;
                }
                if (n5 == 1) {
                    n = androidx.datastore.preferences.protobuf.d.e(n3, array, n, n2, (r.d)j, a);
                    break;
                }
                break;
            }
        }
        return n;
    }
    
    @Override
    public int f(final Object o) {
        final int length = this.a.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final int r0 = this.r0(i);
            final int t = this.T(i);
            final long u = U(r0);
            final int q0 = q0(r0);
            int hashCode = 37;
            int n2 = 0;
            Label_1004: {
                int n3 = 0;
                int n4 = 0;
                Label_0997: {
                    long n5 = 0L;
                    Label_0990: {
                        double value2 = 0.0;
                        Label_0984: {
                            float value = 0.0f;
                            Label_0960: {
                                boolean b = false;
                                Label_0899: {
                                    Label_0847: {
                                        Object o2 = null;
                                        Label_0840: {
                                            Object o3 = null;
                                            Label_0817: {
                                                Label_0701: {
                                                    Label_0648: {
                                                        Label_0523: {
                                                            switch (q0) {
                                                                default: {
                                                                    n2 = n;
                                                                    break Label_1004;
                                                                }
                                                                case 68: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0523;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 67: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 66: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 65: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 64: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 63: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 62: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 60: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0523;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 58: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        n3 = n * 53;
                                                                        b = V(o, u);
                                                                        break Label_0899;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 57: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 56: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 55: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 54: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 53: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 52: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        n3 = n * 53;
                                                                        value = X(o, u);
                                                                        break Label_0960;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 51: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i)) {
                                                                        n3 = n * 53;
                                                                        value2 = W(o, u);
                                                                        break Label_0984;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 17: {
                                                                    o2 = e12.E(o, u);
                                                                    if (o2 != null) {
                                                                        break Label_0840;
                                                                    }
                                                                    break Label_0847;
                                                                }
                                                                case 61: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i))
                                                                    break Label_1004;
                                                                }
                                                                case 10:
                                                                case 18:
                                                                case 19:
                                                                case 20:
                                                                case 21:
                                                                case 22:
                                                                case 23:
                                                                case 24:
                                                                case 25:
                                                                case 26:
                                                                case 27:
                                                                case 28:
                                                                case 29:
                                                                case 30:
                                                                case 31:
                                                                case 32:
                                                                case 33:
                                                                case 34:
                                                                case 35:
                                                                case 36:
                                                                case 37:
                                                                case 38:
                                                                case 39:
                                                                case 40:
                                                                case 41:
                                                                case 42:
                                                                case 43:
                                                                case 44:
                                                                case 45:
                                                                case 46:
                                                                case 47:
                                                                case 48:
                                                                case 49:
                                                                case 50: {
                                                                    n3 = n * 53;
                                                                    o3 = e12.E(o, u);
                                                                    break Label_0817;
                                                                }
                                                                case 9: {
                                                                    o2 = e12.E(o, u);
                                                                    if (o2 != null) {
                                                                        break Label_0840;
                                                                    }
                                                                    break Label_0847;
                                                                }
                                                                case 59: {
                                                                    n2 = n;
                                                                    if (this.H(o, t, i))
                                                                    break Label_1004;
                                                                }
                                                                case 8: {
                                                                    n3 = n * 53;
                                                                    n4 = ((String)e12.E(o, u)).hashCode();
                                                                    break Label_0997;
                                                                }
                                                                case 7: {
                                                                    n3 = n * 53;
                                                                    b = e12.r(o, u);
                                                                    break Label_0899;
                                                                }
                                                                case 4:
                                                                case 6:
                                                                case 11:
                                                                case 12:
                                                                case 13:
                                                                case 15: {
                                                                    n3 = n * 53;
                                                                    n4 = e12.A(o, u);
                                                                    break Label_0997;
                                                                }
                                                                case 2:
                                                                case 3:
                                                                case 5:
                                                                case 14:
                                                                case 16: {
                                                                    n3 = n * 53;
                                                                    n5 = e12.C(o, u);
                                                                    break Label_0990;
                                                                }
                                                                case 1: {
                                                                    n3 = n * 53;
                                                                    value = e12.z(o, u);
                                                                    break Label_0960;
                                                                }
                                                                case 0: {
                                                                    n3 = n * 53;
                                                                    value2 = e12.y(o, u);
                                                                    break Label_0984;
                                                                }
                                                            }
                                                            break Label_0648;
                                                        }
                                                        o3 = e12.E(o, u);
                                                        n3 = n * 53;
                                                        break Label_0817;
                                                    }
                                                    n3 = n * 53;
                                                    n4 = Y(o, u);
                                                    break Label_0997;
                                                }
                                                n3 = n * 53;
                                                n5 = Z(o, u);
                                                break Label_0990;
                                            }
                                            n4 = o3.hashCode();
                                            break Label_0997;
                                        }
                                        hashCode = o2.hashCode();
                                    }
                                    n2 = n * 53 + hashCode;
                                    break Label_1004;
                                }
                                n4 = androidx.datastore.preferences.protobuf.r.c(b);
                                break Label_0997;
                            }
                            n4 = Float.floatToIntBits(value);
                            break Label_0997;
                        }
                        n5 = Double.doubleToLongBits(value2);
                    }
                    n4 = androidx.datastore.preferences.protobuf.r.f(n5);
                }
                n2 = n3 + n4;
            }
            i += 3;
            n = n2;
        }
        int n6 = n * 53 + this.o.g(o).hashCode();
        if (this.f) {
            n6 = n6 * 53 + this.p.c(o).hashCode();
        }
        return n6;
    }
    
    public final int f0(final int n) {
        if (n >= this.c && n <= this.d) {
            return this.p0(n, 0);
        }
        return -1;
    }
    
    @Override
    public void g(final Object o, final Writer writer) {
        if (writer.B() == Writer.FieldOrder.DESCENDING) {
            this.u0(o, writer);
        }
        else if (this.h) {
            this.t0(o, writer);
        }
        else {
            this.s0(o, writer);
        }
    }
    
    public final int g0(final int n, final int n2) {
        if (n >= this.c && n <= this.d) {
            return this.p0(n, n2);
        }
        return -1;
    }
    
    @Override
    public void h(final Object o, final e0 e0, final k k) {
        k.getClass();
        this.L(this.o, this.p, o, e0, k);
    }
    
    public final int h0(final int n) {
        return this.a[n + 2];
    }
    
    @Override
    public void i(final Object o, final byte[] array, final int n, final int n2, final d.a a) {
        if (this.h) {
            this.d0(o, array, n, n2, a);
        }
        else {
            this.c0(o, array, n, n2, 0, a);
        }
    }
    
    public final void i0(final Object o, final long n, final e0 e0, final f0 f0, final k k) {
        e0.M(this.n.e(o, n), f0, k);
    }
    
    public final boolean j(final Object o, final Object o2, final int n) {
        return this.B(o, n) == this.B(o2, n);
    }
    
    public final void j0(final Object o, final int n, final e0 e0, final f0 f0, final k k) {
        e0.K(this.n.e(o, U(n)), f0, k);
    }
    
    public final void k0(final Object o, final int n, final e0 e0) {
        long n2;
        Serializable s;
        if (A(n)) {
            n2 = U(n);
            s = e0.H();
        }
        else if (this.g) {
            n2 = U(n);
            s = e0.F();
        }
        else {
            n2 = U(n);
            s = e0.g();
        }
        e12.T(o, n2, s);
    }
    
    public final int l(final byte[] array, int i, final int n, final v.a a, final Map map, final d.a a2) {
        i = androidx.datastore.preferences.protobuf.d.H(array, i, a2);
        final int a3 = a2.a;
        if (a3 < 0 || a3 > n - i) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        final int n2 = i + a3;
        Object o = a.b;
        Object o2 = a.d;
        while (i < n2) {
            final int n3 = i + 1;
            int a4 = array[i];
            if (a4 < 0) {
                i = androidx.datastore.preferences.protobuf.d.G(a4, array, n3, a2);
                a4 = a2.a;
            }
            else {
                i = n3;
            }
            final int n4 = a4 >>> 3;
            final int n5 = a4 & 0x7;
            if (n4 != 1) {
                if (n4 == 2) {
                    if (n5 == a.c.getWireType()) {
                        i = this.m(array, i, n, a.c, a.d.getClass(), a2);
                        o2 = a2.c;
                        continue;
                    }
                }
            }
            else if (n5 == a.a.getWireType()) {
                i = this.m(array, i, n, a.a, null, a2);
                o = a2.c;
                continue;
            }
            i = androidx.datastore.preferences.protobuf.d.M(a4, array, i, n, a2);
        }
        if (i == n2) {
            map.put(o, o2);
            return n2;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    public final void l0(final Object o, final int n, final e0 e0) {
        if (A(n)) {
            e0.z(this.n.e(o, U(n)));
        }
        else {
            e0.n(this.n.e(o, U(n)));
        }
    }
    
    public final int m(final byte[] array, int n, int i, final WireFormat.FieldType fieldType, final Class clazz, final d.a a) {
        Number c2 = null;
        Label_0281: {
            Number c = null;
            Label_0260: {
                Serializable c3 = null;
                Label_0218: {
                    Label_0213: {
                        long l = 0L;
                        switch (a0$a.a[fieldType.ordinal()]) {
                            default: {
                                throw new RuntimeException("unsupported field type.");
                            }
                            case 17: {
                                n = androidx.datastore.preferences.protobuf.d.E(array, n, a);
                                return n;
                            }
                            case 16: {
                                n = androidx.datastore.preferences.protobuf.d.K(array, n, a);
                                l = androidx.datastore.preferences.protobuf.f.c(a.b);
                                break;
                            }
                            case 15: {
                                n = androidx.datastore.preferences.protobuf.d.H(array, n, a);
                                i = androidx.datastore.preferences.protobuf.f.b(a.a);
                                break Label_0213;
                            }
                            case 14: {
                                n = androidx.datastore.preferences.protobuf.d.o(l91.a().c(clazz), array, n, i, a);
                                return n;
                            }
                            case 12:
                            case 13: {
                                n = androidx.datastore.preferences.protobuf.d.K(array, n, a);
                                l = a.b;
                                break;
                            }
                            case 9:
                            case 10:
                            case 11: {
                                n = androidx.datastore.preferences.protobuf.d.H(array, n, a);
                                i = a.a;
                                break Label_0213;
                            }
                            case 8: {
                                c = androidx.datastore.preferences.protobuf.d.k(array, n);
                                break Label_0260;
                            }
                            case 6:
                            case 7: {
                                c2 = androidx.datastore.preferences.protobuf.d.i(array, n);
                                break Label_0281;
                            }
                            case 4:
                            case 5: {
                                c = androidx.datastore.preferences.protobuf.d.g(array, n);
                                break Label_0260;
                            }
                            case 3: {
                                c2 = androidx.datastore.preferences.protobuf.d.d(array, n);
                                break Label_0281;
                            }
                            case 2: {
                                n = androidx.datastore.preferences.protobuf.d.b(array, n, a);
                                return n;
                            }
                            case 1: {
                                n = androidx.datastore.preferences.protobuf.d.K(array, n, a);
                                c3 = (a.b != 0L);
                                break Label_0218;
                            }
                        }
                        c3 = l;
                        break Label_0218;
                    }
                    c3 = i;
                }
                a.c = c3;
                return n;
            }
            a.c = c;
            n += 4;
            return n;
        }
        a.c = c2;
        n += 8;
        return n;
    }
    
    public final void n0(final Object o, int h0) {
        if (this.h) {
            return;
        }
        h0 = this.h0(h0);
        final long n = h0 & 0xFFFFF;
        e12.R(o, n, e12.A(o, n) | 1 << (h0 >>> 20));
    }
    
    @Override
    public Object newInstance() {
        return this.m.a(this.e);
    }
    
    public final boolean o(final Object o, final Object o2, final int n) {
        final int r0 = this.r0(n);
        final long u = U(r0);
        final int q0 = q0(r0);
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        final boolean b7 = false;
        final boolean b8 = false;
        final boolean b9 = false;
        final boolean b10 = false;
        final boolean b11 = false;
        final boolean b12 = false;
        final boolean b13 = false;
        final boolean b14 = false;
        final boolean b15 = false;
        final boolean b16 = false;
        final boolean b17 = false;
        final boolean b18 = false;
        final boolean b19 = false;
        switch (q0) {
            default: {
                return true;
            }
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68: {
                boolean b20 = b19;
                if (this.G(o, o2, n)) {
                    b20 = b19;
                    if (g0.J(e12.E(o, u), e12.E(o2, u))) {
                        b20 = true;
                    }
                }
                return b20;
            }
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50: {
                return g0.J(e12.E(o, u), e12.E(o2, u));
            }
            case 17: {
                boolean b21 = b;
                if (this.j(o, o2, n)) {
                    b21 = b;
                    if (g0.J(e12.E(o, u), e12.E(o2, u))) {
                        b21 = true;
                    }
                }
                return b21;
            }
            case 16: {
                boolean b22 = b2;
                if (this.j(o, o2, n)) {
                    b22 = b2;
                    if (e12.C(o, u) == e12.C(o2, u)) {
                        b22 = true;
                    }
                }
                return b22;
            }
            case 15: {
                boolean b23 = b3;
                if (this.j(o, o2, n)) {
                    b23 = b3;
                    if (e12.A(o, u) == e12.A(o2, u)) {
                        b23 = true;
                    }
                }
                return b23;
            }
            case 14: {
                boolean b24 = b4;
                if (this.j(o, o2, n)) {
                    b24 = b4;
                    if (e12.C(o, u) == e12.C(o2, u)) {
                        b24 = true;
                    }
                }
                return b24;
            }
            case 13: {
                boolean b25 = b5;
                if (this.j(o, o2, n)) {
                    b25 = b5;
                    if (e12.A(o, u) == e12.A(o2, u)) {
                        b25 = true;
                    }
                }
                return b25;
            }
            case 12: {
                boolean b26 = b6;
                if (this.j(o, o2, n)) {
                    b26 = b6;
                    if (e12.A(o, u) == e12.A(o2, u)) {
                        b26 = true;
                    }
                }
                return b26;
            }
            case 11: {
                boolean b27 = b7;
                if (this.j(o, o2, n)) {
                    b27 = b7;
                    if (e12.A(o, u) == e12.A(o2, u)) {
                        b27 = true;
                    }
                }
                return b27;
            }
            case 10: {
                boolean b28 = b8;
                if (this.j(o, o2, n)) {
                    b28 = b8;
                    if (g0.J(e12.E(o, u), e12.E(o2, u))) {
                        b28 = true;
                    }
                }
                return b28;
            }
            case 9: {
                boolean b29 = b9;
                if (this.j(o, o2, n)) {
                    b29 = b9;
                    if (g0.J(e12.E(o, u), e12.E(o2, u))) {
                        b29 = true;
                    }
                }
                return b29;
            }
            case 8: {
                boolean b30 = b10;
                if (this.j(o, o2, n)) {
                    b30 = b10;
                    if (g0.J(e12.E(o, u), e12.E(o2, u))) {
                        b30 = true;
                    }
                }
                return b30;
            }
            case 7: {
                boolean b31 = b11;
                if (this.j(o, o2, n)) {
                    b31 = b11;
                    if (e12.r(o, u) == e12.r(o2, u)) {
                        b31 = true;
                    }
                }
                return b31;
            }
            case 6: {
                boolean b32 = b12;
                if (this.j(o, o2, n)) {
                    b32 = b12;
                    if (e12.A(o, u) == e12.A(o2, u)) {
                        b32 = true;
                    }
                }
                return b32;
            }
            case 5: {
                boolean b33 = b13;
                if (this.j(o, o2, n)) {
                    b33 = b13;
                    if (e12.C(o, u) == e12.C(o2, u)) {
                        b33 = true;
                    }
                }
                return b33;
            }
            case 4: {
                boolean b34 = b14;
                if (this.j(o, o2, n)) {
                    b34 = b14;
                    if (e12.A(o, u) == e12.A(o2, u)) {
                        b34 = true;
                    }
                }
                return b34;
            }
            case 3: {
                boolean b35 = b15;
                if (this.j(o, o2, n)) {
                    b35 = b15;
                    if (e12.C(o, u) == e12.C(o2, u)) {
                        b35 = true;
                    }
                }
                return b35;
            }
            case 2: {
                boolean b36 = b16;
                if (this.j(o, o2, n)) {
                    b36 = b16;
                    if (e12.C(o, u) == e12.C(o2, u)) {
                        b36 = true;
                    }
                }
                return b36;
            }
            case 1: {
                boolean b37 = b17;
                if (this.j(o, o2, n)) {
                    b37 = b17;
                    if (Float.floatToIntBits(e12.z(o, u)) == Float.floatToIntBits(e12.z(o2, u))) {
                        b37 = true;
                    }
                }
                return b37;
            }
            case 0: {
                boolean b38 = b18;
                if (this.j(o, o2, n)) {
                    b38 = b18;
                    if (Double.doubleToLongBits(e12.y(o, u)) == Double.doubleToLongBits(e12.y(o2, u))) {
                        b38 = true;
                    }
                }
                return b38;
            }
        }
    }
    
    public final void o0(final Object o, final int n, final int n2) {
        e12.R(o, this.h0(n2) & 0xFFFFF, n);
    }
    
    public final Object p(Object e, final int n, final Object o, final i0 i0) {
        final int t = this.T(n);
        e = e12.E(e, U(this.r0(n)));
        if (e == null) {
            return o;
        }
        final r.c s = this.s(n);
        if (s == null) {
            return o;
        }
        return this.q(n, t, this.q.e(e), s, o, i0);
    }
    
    public final int p0(final int n, int i) {
        int n2 = this.a.length / 3 - 1;
        while (i <= n2) {
            final int n3 = n2 + i >>> 1;
            final int n4 = n3 * 3;
            final int t = this.T(n4);
            if (n == t) {
                return n4;
            }
            if (n < t) {
                n2 = n3 - 1;
            }
            else {
                i = n3 + 1;
            }
        }
        return -1;
    }
    
    public final Object q(final int n, final int n2, final Map map, final r.c c, Object o, final i0 i0) {
        final v.a b = this.q.b(this.t(n));
        final Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<K, Integer> entry = (Map.Entry<K, Integer>)iterator.next();
            if (!c.a(entry.getValue())) {
                Object n3;
                if ((n3 = o) == null) {
                    n3 = i0.n();
                }
                final ByteString.g codedBuilder = ByteString.newCodedBuilder(v.b(b, entry.getKey(), entry.getValue()));
                final CodedOutputStream b2 = codedBuilder.b();
                try {
                    v.e(b2, b, entry.getKey(), entry.getValue());
                    i0.d(n3, n2, codedBuilder.a());
                    iterator.remove();
                    o = n3;
                    continue;
                }
                catch (final IOException cause) {
                    throw new RuntimeException(cause);
                }
                break;
            }
        }
        return o;
    }
    
    public final int r0(final int n) {
        return this.a[n + 1];
    }
    
    public final r.c s(final int n) {
        return (r.c)this.b[n / 3 * 2 + 1];
    }
    
    public final void s0(final Object o, final Writer writer) {
        Iterator n = null;
        Map.Entry entry = null;
        Label_0053: {
            if (this.f) {
                final n c = this.p.c(o);
                if (!c.j()) {
                    n = c.n();
                    entry = (Map.Entry)n.next();
                    break Label_0053;
                }
            }
            n = null;
            entry = null;
        }
        final int length = this.a.length;
        final Unsafe s = a0.s;
        int n2 = -1;
        int i = 0;
        int int1 = 0;
        while (i < length) {
            final int r0 = this.r0(i);
            final int t = this.T(i);
            final int q0 = q0(r0);
            int n6;
            if (!this.h && q0 <= 17) {
                final int n3 = this.a[i + 2];
                final int n4 = n3 & 0xFFFFF;
                int n5;
                if (n4 != (n5 = n2)) {
                    int1 = s.getInt(o, n4);
                    n5 = n4;
                }
                n6 = 1 << (n3 >>> 20);
                n2 = n5;
            }
            else {
                n6 = 0;
            }
            while (entry != null && this.p.a(entry) <= t) {
                this.p.j(writer, entry);
                if (n.hasNext()) {
                    entry = (Map.Entry)n.next();
                }
                else {
                    entry = null;
                }
            }
            final long u = U(r0);
            switch (q0) {
                case 68: {
                    if (this.H(o, t, i)) {
                        writer.N(t, s.getObject(o, u), this.u(i));
                        break;
                    }
                    break;
                }
                case 67: {
                    if (this.H(o, t, i)) {
                        writer.j(t, Z(o, u));
                        break;
                    }
                    break;
                }
                case 66: {
                    if (this.H(o, t, i)) {
                        writer.I(t, Y(o, u));
                        break;
                    }
                    break;
                }
                case 65: {
                    if (this.H(o, t, i)) {
                        writer.w(t, Z(o, u));
                        break;
                    }
                    break;
                }
                case 64: {
                    if (this.H(o, t, i)) {
                        writer.o(t, Y(o, u));
                        break;
                    }
                    break;
                }
                case 63: {
                    if (this.H(o, t, i)) {
                        writer.G(t, Y(o, u));
                        break;
                    }
                    break;
                }
                case 62: {
                    if (this.H(o, t, i)) {
                        writer.k(t, Y(o, u));
                        break;
                    }
                    break;
                }
                case 61: {
                    if (this.H(o, t, i)) {
                        writer.J(t, (ByteString)s.getObject(o, u));
                        break;
                    }
                    break;
                }
                case 60: {
                    if (this.H(o, t, i)) {
                        writer.K(t, s.getObject(o, u), this.u(i));
                        break;
                    }
                    break;
                }
                case 59: {
                    if (this.H(o, t, i)) {
                        this.w0(t, s.getObject(o, u), writer);
                        break;
                    }
                    break;
                }
                case 58: {
                    if (this.H(o, t, i)) {
                        writer.n(t, V(o, u));
                        break;
                    }
                    break;
                }
                case 57: {
                    if (this.H(o, t, i)) {
                        writer.c(t, Y(o, u));
                        break;
                    }
                    break;
                }
                case 56: {
                    if (this.H(o, t, i)) {
                        writer.m(t, Z(o, u));
                        break;
                    }
                    break;
                }
                case 55: {
                    if (this.H(o, t, i)) {
                        writer.g(t, Y(o, u));
                        break;
                    }
                    break;
                }
                case 54: {
                    if (this.H(o, t, i)) {
                        writer.e(t, Z(o, u));
                        break;
                    }
                    break;
                }
                case 53: {
                    if (this.H(o, t, i)) {
                        writer.C(t, Z(o, u));
                        break;
                    }
                    break;
                }
                case 52: {
                    if (this.H(o, t, i)) {
                        writer.F(t, X(o, u));
                        break;
                    }
                    break;
                }
                case 51: {
                    if (this.H(o, t, i)) {
                        writer.z(t, W(o, u));
                        break;
                    }
                    break;
                }
                case 50: {
                    this.v0(writer, t, s.getObject(o, u), i);
                    break;
                }
                case 49: {
                    g0.T(this.T(i), (List)s.getObject(o, u), writer, this.u(i));
                    break;
                }
                case 48: {
                    g0.a0(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 47: {
                    g0.Z(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 46: {
                    g0.Y(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 45: {
                    g0.X(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 44: {
                    g0.P(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 43: {
                    g0.c0(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 42: {
                    g0.M(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 41: {
                    g0.Q(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 40: {
                    g0.R(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 39: {
                    g0.U(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 38: {
                    g0.d0(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 37: {
                    g0.V(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 36: {
                    g0.S(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 35: {
                    g0.O(this.T(i), (List)s.getObject(o, u), writer, true);
                    break;
                }
                case 34: {
                    g0.a0(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 33: {
                    g0.Z(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 32: {
                    g0.Y(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 31: {
                    g0.X(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 30: {
                    g0.P(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 29: {
                    g0.c0(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 28: {
                    g0.N(this.T(i), (List)s.getObject(o, u), writer);
                    break;
                }
                case 27: {
                    g0.W(this.T(i), (List)s.getObject(o, u), writer, this.u(i));
                    break;
                }
                case 26: {
                    g0.b0(this.T(i), (List)s.getObject(o, u), writer);
                    break;
                }
                case 25: {
                    g0.M(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 24: {
                    g0.Q(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 23: {
                    g0.R(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 22: {
                    g0.U(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 21: {
                    g0.d0(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 20: {
                    g0.V(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 19: {
                    g0.S(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 18: {
                    g0.O(this.T(i), (List)s.getObject(o, u), writer, false);
                    break;
                }
                case 17: {
                    if ((n6 & int1) != 0x0) {
                        writer.N(t, s.getObject(o, u), this.u(i));
                        break;
                    }
                    break;
                }
                case 16: {
                    if ((n6 & int1) != 0x0) {
                        writer.j(t, s.getLong(o, u));
                        break;
                    }
                    break;
                }
                case 15: {
                    if ((n6 & int1) != 0x0) {
                        writer.I(t, s.getInt(o, u));
                        break;
                    }
                    break;
                }
                case 14: {
                    if ((n6 & int1) != 0x0) {
                        writer.w(t, s.getLong(o, u));
                        break;
                    }
                    break;
                }
                case 13: {
                    if ((n6 & int1) != 0x0) {
                        writer.o(t, s.getInt(o, u));
                        break;
                    }
                    break;
                }
                case 12: {
                    if ((n6 & int1) != 0x0) {
                        writer.G(t, s.getInt(o, u));
                        break;
                    }
                    break;
                }
                case 11: {
                    if ((n6 & int1) != 0x0) {
                        writer.k(t, s.getInt(o, u));
                        break;
                    }
                    break;
                }
                case 10: {
                    if ((n6 & int1) != 0x0) {
                        writer.J(t, (ByteString)s.getObject(o, u));
                        break;
                    }
                    break;
                }
                case 9: {
                    if ((n6 & int1) != 0x0) {
                        writer.K(t, s.getObject(o, u), this.u(i));
                        break;
                    }
                    break;
                }
                case 8: {
                    if ((n6 & int1) != 0x0) {
                        this.w0(t, s.getObject(o, u), writer);
                        break;
                    }
                    break;
                }
                case 7: {
                    if ((n6 & int1) != 0x0) {
                        writer.n(t, k(o, u));
                        break;
                    }
                    break;
                }
                case 6: {
                    if ((n6 & int1) != 0x0) {
                        writer.c(t, s.getInt(o, u));
                        break;
                    }
                    break;
                }
                case 5: {
                    if ((n6 & int1) != 0x0) {
                        writer.m(t, s.getLong(o, u));
                        break;
                    }
                    break;
                }
                case 4: {
                    if ((n6 & int1) != 0x0) {
                        writer.g(t, s.getInt(o, u));
                        break;
                    }
                    break;
                }
                case 3: {
                    if ((n6 & int1) != 0x0) {
                        writer.e(t, s.getLong(o, u));
                        break;
                    }
                    break;
                }
                case 2: {
                    if ((n6 & int1) != 0x0) {
                        writer.C(t, s.getLong(o, u));
                        break;
                    }
                    break;
                }
                case 1: {
                    if ((n6 & int1) != 0x0) {
                        writer.F(t, r(o, u));
                        break;
                    }
                    break;
                }
                case 0: {
                    if ((n6 & int1) != 0x0) {
                        writer.z(t, n(o, u));
                        break;
                    }
                    break;
                }
            }
            i += 3;
        }
        while (entry != null) {
            this.p.j(writer, entry);
            if (n.hasNext()) {
                entry = (Map.Entry)n.next();
            }
            else {
                entry = null;
            }
        }
        this.x0(this.o, o, writer);
    }
    
    public final Object t(final int n) {
        return this.b[n / 3 * 2];
    }
    
    public final void t0(final Object o, final Writer writer) {
        Iterator n = null;
        Map.Entry entry = null;
        Label_0053: {
            if (this.f) {
                final n c = this.p.c(o);
                if (!c.j()) {
                    n = c.n();
                    entry = (Map.Entry)n.next();
                    break Label_0053;
                }
            }
            n = null;
            entry = null;
        }
        final int length = this.a.length;
        int n2 = 0;
        Map.Entry entry2 = entry;
        Map.Entry entry3;
        while (true) {
            entry3 = entry2;
            if (n2 >= length) {
                break;
            }
            final int r0 = this.r0(n2);
            final int t = this.T(n2);
            while (entry2 != null && this.p.a(entry2) <= t) {
                this.p.j(writer, entry2);
                if (n.hasNext()) {
                    entry2 = (Map.Entry)n.next();
                }
                else {
                    entry2 = null;
                }
            }
            Label_2340: {
                double n15 = 0.0;
                Label_2331: {
                    float n14 = 0.0f;
                    Label_2298: {
                        long n13 = 0L;
                        Label_2264: {
                            long n12 = 0L;
                            Label_2230: {
                                int n11 = 0;
                                Label_2196: {
                                    long n10 = 0L;
                                    Label_2162: {
                                        int n9 = 0;
                                        Label_2128: {
                                            boolean b = false;
                                            Label_2094: {
                                                Label_2054: {
                                                    Label_2018: {
                                                        Label_1985: {
                                                            int n8 = 0;
                                                            Label_1962: {
                                                                int n7 = 0;
                                                                Label_1928: {
                                                                    int n6 = 0;
                                                                    Label_1894: {
                                                                        long n5 = 0L;
                                                                        Label_1860: {
                                                                            int n4 = 0;
                                                                            Label_1826: {
                                                                                long n3 = 0L;
                                                                                Label_1792: {
                                                                                    switch (q0(r0)) {
                                                                                        default: {
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 68: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 67: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n3 = Z(o, U(r0));
                                                                                                break Label_1792;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 66: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n4 = Y(o, U(r0));
                                                                                                break Label_1826;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 65: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n5 = Z(o, U(r0));
                                                                                                break Label_1860;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 64: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n6 = Y(o, U(r0));
                                                                                                break Label_1894;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 63: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n7 = Y(o, U(r0));
                                                                                                break Label_1928;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 62: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n8 = Y(o, U(r0));
                                                                                                break Label_1962;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 61: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                break Label_1985;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 60: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                break Label_2018;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 59: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                break Label_2054;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 58: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                b = V(o, U(r0));
                                                                                                break Label_2094;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 57: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n9 = Y(o, U(r0));
                                                                                                break Label_2128;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 56: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n10 = Z(o, U(r0));
                                                                                                break Label_2162;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 55: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n11 = Y(o, U(r0));
                                                                                                break Label_2196;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 54: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n12 = Z(o, U(r0));
                                                                                                break Label_2230;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 53: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n13 = Z(o, U(r0));
                                                                                                break Label_2264;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 52: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n14 = X(o, U(r0));
                                                                                                break Label_2298;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 51: {
                                                                                            if (this.H(o, t, n2)) {
                                                                                                n15 = W(o, U(r0));
                                                                                                break Label_2331;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 50: {
                                                                                            this.v0(writer, t, e12.E(o, U(r0)), n2);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 49: {
                                                                                            g0.T(this.T(n2), (List)e12.E(o, U(r0)), writer, this.u(n2));
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 48: {
                                                                                            g0.a0(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 47: {
                                                                                            g0.Z(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 46: {
                                                                                            g0.Y(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 45: {
                                                                                            g0.X(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 44: {
                                                                                            g0.P(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 43: {
                                                                                            g0.c0(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 42: {
                                                                                            g0.M(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 41: {
                                                                                            g0.Q(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 40: {
                                                                                            g0.R(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 39: {
                                                                                            g0.U(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 38: {
                                                                                            g0.d0(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 37: {
                                                                                            g0.V(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 36: {
                                                                                            g0.S(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 35: {
                                                                                            g0.O(this.T(n2), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 34: {
                                                                                            g0.a0(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 33: {
                                                                                            g0.Z(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 32: {
                                                                                            g0.Y(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 31: {
                                                                                            g0.X(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 30: {
                                                                                            g0.P(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 29: {
                                                                                            g0.c0(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 28: {
                                                                                            g0.N(this.T(n2), (List)e12.E(o, U(r0)), writer);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 27: {
                                                                                            g0.W(this.T(n2), (List)e12.E(o, U(r0)), writer, this.u(n2));
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 26: {
                                                                                            g0.b0(this.T(n2), (List)e12.E(o, U(r0)), writer);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 25: {
                                                                                            g0.M(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 24: {
                                                                                            g0.Q(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 23: {
                                                                                            g0.R(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 22: {
                                                                                            g0.U(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 21: {
                                                                                            g0.d0(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 20: {
                                                                                            g0.V(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 19: {
                                                                                            g0.S(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 18: {
                                                                                            g0.O(this.T(n2), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 17: {
                                                                                            if (this.B(o, n2)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 16: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n3 = K(o, U(r0));
                                                                                                break Label_1792;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 15: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n4 = z(o, U(r0));
                                                                                                break Label_1826;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 14: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n5 = K(o, U(r0));
                                                                                                break Label_1860;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 13: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n6 = z(o, U(r0));
                                                                                                break Label_1894;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 12: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n7 = z(o, U(r0));
                                                                                                break Label_1928;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 11: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n8 = z(o, U(r0));
                                                                                                break Label_1962;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 10: {
                                                                                            if (this.B(o, n2)) {
                                                                                                break Label_1985;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 9: {
                                                                                            if (this.B(o, n2)) {
                                                                                                break Label_2018;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 8: {
                                                                                            if (this.B(o, n2)) {
                                                                                                break Label_2054;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 7: {
                                                                                            if (this.B(o, n2)) {
                                                                                                b = k(o, U(r0));
                                                                                                break Label_2094;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 6: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n9 = z(o, U(r0));
                                                                                                break Label_2128;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 5: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n10 = K(o, U(r0));
                                                                                                break Label_2162;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 4: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n11 = z(o, U(r0));
                                                                                                break Label_2196;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 3: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n12 = K(o, U(r0));
                                                                                                break Label_2230;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 2: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n13 = K(o, U(r0));
                                                                                                break Label_2264;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 1: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n14 = r(o, U(r0));
                                                                                                break Label_2298;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 0: {
                                                                                            if (this.B(o, n2)) {
                                                                                                n15 = n(o, U(r0));
                                                                                                break Label_2331;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                    }
                                                                                    writer.N(t, e12.E(o, U(r0)), this.u(n2));
                                                                                    break Label_2340;
                                                                                }
                                                                                writer.j(t, n3);
                                                                                break Label_2340;
                                                                            }
                                                                            writer.I(t, n4);
                                                                            break Label_2340;
                                                                        }
                                                                        writer.w(t, n5);
                                                                        break Label_2340;
                                                                    }
                                                                    writer.o(t, n6);
                                                                    break Label_2340;
                                                                }
                                                                writer.G(t, n7);
                                                                break Label_2340;
                                                            }
                                                            writer.k(t, n8);
                                                            break Label_2340;
                                                        }
                                                        writer.J(t, (ByteString)e12.E(o, U(r0)));
                                                        break Label_2340;
                                                    }
                                                    writer.K(t, e12.E(o, U(r0)), this.u(n2));
                                                    break Label_2340;
                                                }
                                                this.w0(t, e12.E(o, U(r0)), writer);
                                                break Label_2340;
                                            }
                                            writer.n(t, b);
                                            break Label_2340;
                                        }
                                        writer.c(t, n9);
                                        break Label_2340;
                                    }
                                    writer.m(t, n10);
                                    break Label_2340;
                                }
                                writer.g(t, n11);
                                break Label_2340;
                            }
                            writer.e(t, n12);
                            break Label_2340;
                        }
                        writer.C(t, n13);
                        break Label_2340;
                    }
                    writer.F(t, n14);
                    break Label_2340;
                }
                writer.z(t, n15);
            }
            n2 += 3;
        }
        while (entry3 != null) {
            this.p.j(writer, entry3);
            if (n.hasNext()) {
                entry3 = (Map.Entry)n.next();
            }
            else {
                entry3 = null;
            }
        }
        this.x0(this.o, o, writer);
    }
    
    public final f0 u(int n) {
        n = n / 3 * 2;
        final f0 f0 = (f0)this.b[n];
        if (f0 != null) {
            return f0;
        }
        return (f0)(this.b[n] = l91.a().c((Class)this.b[n + 1]));
    }
    
    public final void u0(final Object o, final Writer writer) {
        this.x0(this.o, o, writer);
        Iterator e = null;
        Map.Entry entry = null;
        Label_0063: {
            if (this.f) {
                final n c = this.p.c(o);
                if (!c.j()) {
                    e = c.e();
                    entry = (Map.Entry)e.next();
                    break Label_0063;
                }
            }
            e = null;
            entry = null;
        }
        int n = this.a.length - 3;
        Map.Entry entry2 = entry;
        Map.Entry entry3;
        while (true) {
            entry3 = entry2;
            if (n < 0) {
                break;
            }
            final int r0 = this.r0(n);
            final int t = this.T(n);
            while (entry2 != null && this.p.a(entry2) > t) {
                this.p.j(writer, entry2);
                if (e.hasNext()) {
                    entry2 = (Map.Entry)e.next();
                }
                else {
                    entry2 = null;
                }
            }
            Label_2348: {
                double n14 = 0.0;
                Label_2339: {
                    float n13 = 0.0f;
                    Label_2306: {
                        long n12 = 0L;
                        Label_2272: {
                            long n11 = 0L;
                            Label_2238: {
                                int n10 = 0;
                                Label_2204: {
                                    long n9 = 0L;
                                    Label_2170: {
                                        int n8 = 0;
                                        Label_2136: {
                                            boolean b = false;
                                            Label_2102: {
                                                Label_2062: {
                                                    Label_2026: {
                                                        Label_1993: {
                                                            int n7 = 0;
                                                            Label_1970: {
                                                                int n6 = 0;
                                                                Label_1936: {
                                                                    int n5 = 0;
                                                                    Label_1902: {
                                                                        long n4 = 0L;
                                                                        Label_1868: {
                                                                            int n3 = 0;
                                                                            Label_1834: {
                                                                                long n2 = 0L;
                                                                                Label_1800: {
                                                                                    switch (q0(r0)) {
                                                                                        default: {
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 68: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 67: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n2 = Z(o, U(r0));
                                                                                                break Label_1800;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 66: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n3 = Y(o, U(r0));
                                                                                                break Label_1834;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 65: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n4 = Z(o, U(r0));
                                                                                                break Label_1868;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 64: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n5 = Y(o, U(r0));
                                                                                                break Label_1902;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 63: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n6 = Y(o, U(r0));
                                                                                                break Label_1936;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 62: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n7 = Y(o, U(r0));
                                                                                                break Label_1970;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 61: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                break Label_1993;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 60: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                break Label_2026;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 59: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                break Label_2062;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 58: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                b = V(o, U(r0));
                                                                                                break Label_2102;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 57: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n8 = Y(o, U(r0));
                                                                                                break Label_2136;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 56: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n9 = Z(o, U(r0));
                                                                                                break Label_2170;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 55: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n10 = Y(o, U(r0));
                                                                                                break Label_2204;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 54: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n11 = Z(o, U(r0));
                                                                                                break Label_2238;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 53: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n12 = Z(o, U(r0));
                                                                                                break Label_2272;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 52: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n13 = X(o, U(r0));
                                                                                                break Label_2306;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 51: {
                                                                                            if (this.H(o, t, n)) {
                                                                                                n14 = W(o, U(r0));
                                                                                                break Label_2339;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 50: {
                                                                                            this.v0(writer, t, e12.E(o, U(r0)), n);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 49: {
                                                                                            g0.T(this.T(n), (List)e12.E(o, U(r0)), writer, this.u(n));
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 48: {
                                                                                            g0.a0(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 47: {
                                                                                            g0.Z(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 46: {
                                                                                            g0.Y(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 45: {
                                                                                            g0.X(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 44: {
                                                                                            g0.P(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 43: {
                                                                                            g0.c0(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 42: {
                                                                                            g0.M(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 41: {
                                                                                            g0.Q(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 40: {
                                                                                            g0.R(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 39: {
                                                                                            g0.U(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 38: {
                                                                                            g0.d0(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 37: {
                                                                                            g0.V(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 36: {
                                                                                            g0.S(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 35: {
                                                                                            g0.O(this.T(n), (List)e12.E(o, U(r0)), writer, true);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 34: {
                                                                                            g0.a0(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 33: {
                                                                                            g0.Z(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 32: {
                                                                                            g0.Y(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 31: {
                                                                                            g0.X(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 30: {
                                                                                            g0.P(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 29: {
                                                                                            g0.c0(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 28: {
                                                                                            g0.N(this.T(n), (List)e12.E(o, U(r0)), writer);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 27: {
                                                                                            g0.W(this.T(n), (List)e12.E(o, U(r0)), writer, this.u(n));
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 26: {
                                                                                            g0.b0(this.T(n), (List)e12.E(o, U(r0)), writer);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 25: {
                                                                                            g0.M(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 24: {
                                                                                            g0.Q(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 23: {
                                                                                            g0.R(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 22: {
                                                                                            g0.U(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 21: {
                                                                                            g0.d0(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 20: {
                                                                                            g0.V(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 19: {
                                                                                            g0.S(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 18: {
                                                                                            g0.O(this.T(n), (List)e12.E(o, U(r0)), writer, false);
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 17: {
                                                                                            if (this.B(o, n)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 16: {
                                                                                            if (this.B(o, n)) {
                                                                                                n2 = K(o, U(r0));
                                                                                                break Label_1800;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 15: {
                                                                                            if (this.B(o, n)) {
                                                                                                n3 = z(o, U(r0));
                                                                                                break Label_1834;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 14: {
                                                                                            if (this.B(o, n)) {
                                                                                                n4 = K(o, U(r0));
                                                                                                break Label_1868;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 13: {
                                                                                            if (this.B(o, n)) {
                                                                                                n5 = z(o, U(r0));
                                                                                                break Label_1902;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 12: {
                                                                                            if (this.B(o, n)) {
                                                                                                n6 = z(o, U(r0));
                                                                                                break Label_1936;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 11: {
                                                                                            if (this.B(o, n)) {
                                                                                                n7 = z(o, U(r0));
                                                                                                break Label_1970;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 10: {
                                                                                            if (this.B(o, n)) {
                                                                                                break Label_1993;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 9: {
                                                                                            if (this.B(o, n)) {
                                                                                                break Label_2026;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 8: {
                                                                                            if (this.B(o, n)) {
                                                                                                break Label_2062;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 7: {
                                                                                            if (this.B(o, n)) {
                                                                                                b = k(o, U(r0));
                                                                                                break Label_2102;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 6: {
                                                                                            if (this.B(o, n)) {
                                                                                                n8 = z(o, U(r0));
                                                                                                break Label_2136;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 5: {
                                                                                            if (this.B(o, n)) {
                                                                                                n9 = K(o, U(r0));
                                                                                                break Label_2170;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 4: {
                                                                                            if (this.B(o, n)) {
                                                                                                n10 = z(o, U(r0));
                                                                                                break Label_2204;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 3: {
                                                                                            if (this.B(o, n)) {
                                                                                                n11 = K(o, U(r0));
                                                                                                break Label_2238;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 2: {
                                                                                            if (this.B(o, n)) {
                                                                                                n12 = K(o, U(r0));
                                                                                                break Label_2272;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 1: {
                                                                                            if (this.B(o, n)) {
                                                                                                n13 = r(o, U(r0));
                                                                                                break Label_2306;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                        case 0: {
                                                                                            if (this.B(o, n)) {
                                                                                                n14 = n(o, U(r0));
                                                                                                break Label_2339;
                                                                                            }
                                                                                            break Label_2348;
                                                                                        }
                                                                                    }
                                                                                    writer.N(t, e12.E(o, U(r0)), this.u(n));
                                                                                    break Label_2348;
                                                                                }
                                                                                writer.j(t, n2);
                                                                                break Label_2348;
                                                                            }
                                                                            writer.I(t, n3);
                                                                            break Label_2348;
                                                                        }
                                                                        writer.w(t, n4);
                                                                        break Label_2348;
                                                                    }
                                                                    writer.o(t, n5);
                                                                    break Label_2348;
                                                                }
                                                                writer.G(t, n6);
                                                                break Label_2348;
                                                            }
                                                            writer.k(t, n7);
                                                            break Label_2348;
                                                        }
                                                        writer.J(t, (ByteString)e12.E(o, U(r0)));
                                                        break Label_2348;
                                                    }
                                                    writer.K(t, e12.E(o, U(r0)), this.u(n));
                                                    break Label_2348;
                                                }
                                                this.w0(t, e12.E(o, U(r0)), writer);
                                                break Label_2348;
                                            }
                                            writer.n(t, b);
                                            break Label_2348;
                                        }
                                        writer.c(t, n8);
                                        break Label_2348;
                                    }
                                    writer.m(t, n9);
                                    break Label_2348;
                                }
                                writer.g(t, n10);
                                break Label_2348;
                            }
                            writer.e(t, n11);
                            break Label_2348;
                        }
                        writer.C(t, n12);
                        break Label_2348;
                    }
                    writer.F(t, n13);
                    break Label_2348;
                }
                writer.z(t, n14);
            }
            n -= 3;
        }
        while (entry3 != null) {
            this.p.j(writer, entry3);
            if (e.hasNext()) {
                entry3 = (Map.Entry)e.next();
            }
            else {
                entry3 = null;
            }
        }
    }
    
    public final void v0(final Writer writer, final int n, final Object o, final int n2) {
        if (o != null) {
            writer.L(n, this.q.b(this.t(n2)), this.q.g(o));
        }
    }
    
    public final int w(final Object o) {
        final Unsafe s = a0.s;
        int n = -1;
        int i = 0;
        int n2 = 0;
        int int1 = 0;
    Label_1819_Outer:
        while (i < this.a.length) {
            final int r0 = this.r0(i);
            final int t = this.T(i);
            final int q0 = q0(r0);
            int n3;
            int n5;
            int n6;
            int n7;
            if (q0 <= 17) {
                n3 = this.a[i + 2];
                final int n4 = 0xFFFFF & n3;
                if (n4 != (n5 = n)) {
                    int1 = s.getInt(o, n4);
                    n5 = n4;
                }
                n6 = 1 << (n3 >>> 20);
                n7 = int1;
            }
            else {
                int n8;
                if (this.i && q0 >= FieldType.DOUBLE_LIST_PACKED.id() && q0 <= FieldType.SINT64_LIST_PACKED.id()) {
                    n8 = (this.a[i + 2] & 0xFFFFF);
                }
                else {
                    n8 = 0;
                }
                n6 = 0;
                n3 = n8;
                n7 = int1;
                n5 = n;
            }
            final long u = U(r0);
            int n19 = 0;
            Label_2348: {
                int n18 = 0;
            Label_1819:
                while (true) {
                    Label_2168: {
                        Label_2077: {
                            Label_2046: {
                                int n13 = 0;
                                Label_2025: {
                                    int n12 = 0;
                                Label_1995:
                                    while (true) {
                                        int n15 = 0;
                                        Label_1967: {
                                            Label_1960: {
                                                Label_1939: {
                                                    int n11 = 0;
                                                    Label_1918: {
                                                        long n10 = 0L;
                                                        Label_1887: {
                                                            Label_1840: {
                                                                int n17 = 0;
                                                                Label_1793: {
                                                                    int x = 0;
                                                                    Label_1504: {
                                                                        switch (q0) {
                                                                            default: {
                                                                                final int n9 = n2;
                                                                                break Label_1824;
                                                                            }
                                                                            case 68: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    break Label_1840;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 67: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    n10 = Z(o, u);
                                                                                    break Label_1887;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 66: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    n11 = Y(o, u);
                                                                                    break Label_1918;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 65: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    break Label_1939;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 64: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    break Label_1960;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 63: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    n12 = Y(o, u);
                                                                                    break Label_1995;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 62: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    n13 = Y(o, u);
                                                                                    break Label_2025;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 61: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    break Label_2046;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 60: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    break Label_2077;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 59: {
                                                                                final int n9 = n2;
                                                                                if (!this.H(o, t, i)) {
                                                                                    break Label_1824;
                                                                                }
                                                                                final Object object = s.getObject(o, u);
                                                                                if (object instanceof ByteString) {
                                                                                    final int n14 = CodedOutputStream.h(t, (ByteString)object);
                                                                                    break Label_1819;
                                                                                }
                                                                                final int n14 = CodedOutputStream.P(t, (String)object);
                                                                                break Label_1819;
                                                                            }
                                                                            case 58: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    break Label_2168;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 57: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    n15 = CodedOutputStream.n(t, 0);
                                                                                    break Label_1967;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 56: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    final int n14 = CodedOutputStream.p(t, 0L);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 55: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    final int n14 = CodedOutputStream.w(t, Y(o, u));
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 54: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    final int n14 = CodedOutputStream.U(t, Z(o, u));
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 53: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    final int n14 = CodedOutputStream.y(t, Z(o, u));
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 52: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    final int n14 = CodedOutputStream.r(t, 0.0f);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 51: {
                                                                                final int n9 = n2;
                                                                                if (this.H(o, t, i)) {
                                                                                    final int n14 = CodedOutputStream.j(t, 0.0);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 50: {
                                                                                final int n14 = this.q.d(t, s.getObject(o, u), this.t(i));
                                                                                break Label_1819;
                                                                            }
                                                                            case 49: {
                                                                                final int n14 = g0.j(t, (List)s.getObject(o, u), this.u(i));
                                                                                break Label_1819;
                                                                            }
                                                                            case 48: {
                                                                                final int t2 = g0.t((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (t2 <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = t2;
                                                                                if (this.i) {
                                                                                    x = t2;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 47: {
                                                                                final int r2 = g0.r((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (r2 <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = r2;
                                                                                if (this.i) {
                                                                                    x = r2;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 46: {
                                                                                final int j = g0.i((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (j <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = j;
                                                                                if (this.i) {
                                                                                    x = j;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 45: {
                                                                                final int g = g0.g((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (g <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = g;
                                                                                if (this.i) {
                                                                                    x = g;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 44: {
                                                                                final int e = g0.e((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (e <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = e;
                                                                                if (this.i) {
                                                                                    x = e;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 43: {
                                                                                final int w = g0.w((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (w <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = w;
                                                                                if (this.i) {
                                                                                    x = w;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 42: {
                                                                                final int b = g0.b((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (b <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = b;
                                                                                if (this.i) {
                                                                                    x = b;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 41: {
                                                                                final int g2 = g0.g((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (g2 <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = g2;
                                                                                if (this.i) {
                                                                                    x = g2;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 40: {
                                                                                final int k = g0.i((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (k <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = k;
                                                                                if (this.i) {
                                                                                    x = k;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 39: {
                                                                                final int l = g0.l((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (l <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = l;
                                                                                if (this.i) {
                                                                                    x = l;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 38: {
                                                                                final int y = g0.y((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (y <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = y;
                                                                                if (this.i) {
                                                                                    x = y;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 37: {
                                                                                final int n16 = g0.n((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (n16 <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = n16;
                                                                                if (this.i) {
                                                                                    x = n16;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 36: {
                                                                                final int g3 = g0.g((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (g3 <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = g3;
                                                                                if (this.i) {
                                                                                    x = g3;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 35: {
                                                                                final int m = g0.i((List)s.getObject(o, u));
                                                                                final int n9 = n2;
                                                                                if (m <= 0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                x = m;
                                                                                if (this.i) {
                                                                                    x = m;
                                                                                    break;
                                                                                }
                                                                                break Label_1504;
                                                                            }
                                                                            case 34: {
                                                                                n17 = g0.s(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 33: {
                                                                                n17 = g0.q(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 30: {
                                                                                n17 = g0.d(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 29: {
                                                                                final int n14 = g0.v(t, (List)s.getObject(o, u), false);
                                                                                break Label_1819;
                                                                            }
                                                                            case 28: {
                                                                                final int n14 = g0.c(t, (List)s.getObject(o, u));
                                                                                break Label_1819;
                                                                            }
                                                                            case 27: {
                                                                                final int n14 = g0.p(t, (List)s.getObject(o, u), this.u(i));
                                                                                break Label_1819;
                                                                            }
                                                                            case 26: {
                                                                                final int n14 = g0.u(t, (List)s.getObject(o, u));
                                                                                break Label_1819;
                                                                            }
                                                                            case 25: {
                                                                                n17 = g0.a(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 23:
                                                                            case 32: {
                                                                                n17 = g0.h(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 22: {
                                                                                n17 = g0.k(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 21: {
                                                                                n17 = g0.x(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 20: {
                                                                                n17 = g0.m(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 19:
                                                                            case 24:
                                                                            case 31: {
                                                                                n17 = g0.f(t, (List)s.getObject(o, u), false);
                                                                                break Label_1793;
                                                                            }
                                                                            case 18: {
                                                                                final int n14 = g0.h(t, (List)s.getObject(o, u), false);
                                                                                break Label_1819;
                                                                            }
                                                                            case 17: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    break Label_1840;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 16: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n10 = s.getLong(o, u);
                                                                                    break Label_1887;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 15: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n11 = s.getInt(o, u);
                                                                                    break Label_1918;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 14: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    break Label_1939;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 13: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    break Label_1960;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 12: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n12 = s.getInt(o, u);
                                                                                    break Label_1995;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 11: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n13 = s.getInt(o, u);
                                                                                    break Label_2025;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 10: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    break Label_2046;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 9: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    break Label_2077;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 8: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) == 0x0) {
                                                                                    break Label_1824;
                                                                                }
                                                                                final Object object2 = s.getObject(o, u);
                                                                                if (object2 instanceof ByteString) {
                                                                                    final int n14 = CodedOutputStream.h(t, (ByteString)object2);
                                                                                    break Label_1819;
                                                                                }
                                                                                final int n14 = CodedOutputStream.P(t, (String)object2);
                                                                                break Label_1819;
                                                                            }
                                                                            case 7: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    break Label_2168;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 6: {
                                                                                final int n9 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.n(t, 0);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_1824;
                                                                            }
                                                                            case 5: {
                                                                                n19 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.p(t, 0L);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_2348;
                                                                            }
                                                                            case 4: {
                                                                                n19 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.w(t, s.getInt(o, u));
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_2348;
                                                                            }
                                                                            case 3: {
                                                                                n19 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.U(t, s.getLong(o, u));
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_2348;
                                                                            }
                                                                            case 2: {
                                                                                n19 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.y(t, s.getLong(o, u));
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_2348;
                                                                            }
                                                                            case 1: {
                                                                                n19 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.r(t, 0.0f);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_2348;
                                                                            }
                                                                            case 0: {
                                                                                n19 = n2;
                                                                                if ((n7 & n6) != 0x0) {
                                                                                    n18 = CodedOutputStream.j(t, 0.0);
                                                                                    break Label_1819;
                                                                                }
                                                                                break Label_2348;
                                                                            }
                                                                        }
                                                                        s.putInt(o, n3, x);
                                                                    }
                                                                    n15 = CodedOutputStream.R(t) + CodedOutputStream.T(x) + x;
                                                                    break Label_1967;
                                                                }
                                                                n19 = n2 + n17;
                                                                break Label_2348;
                                                                int n14 = 0;
                                                                final int n9 = n2 + n14;
                                                                n19 = n9;
                                                                break Label_2348;
                                                            }
                                                            final int n14 = CodedOutputStream.t(t, (y)s.getObject(o, u), this.u(i));
                                                            continue Label_1819;
                                                        }
                                                        final int n14 = CodedOutputStream.N(t, n10);
                                                        continue Label_1819;
                                                    }
                                                    final int n14 = CodedOutputStream.L(t, n11);
                                                    continue Label_1819;
                                                }
                                                final int n14 = CodedOutputStream.J(t, 0L);
                                                continue Label_1819;
                                            }
                                            n15 = CodedOutputStream.H(t, 0);
                                        }
                                        final int n9 = n2 + n15;
                                        continue Label_1819_Outer;
                                    }
                                    final int n14 = CodedOutputStream.l(t, n12);
                                    continue Label_1819;
                                }
                                final int n14 = CodedOutputStream.S(t, n13);
                                continue Label_1819;
                            }
                            final int n14 = CodedOutputStream.h(t, (ByteString)s.getObject(o, u));
                            continue Label_1819;
                        }
                        final int n14 = g0.o(t, s.getObject(o, u), this.u(i));
                        continue Label_1819;
                    }
                    final int n14 = CodedOutputStream.e(t, true);
                    continue Label_1819;
                }
                n19 = n2 + n18;
            }
            i += 3;
            n = n5;
            n2 = n19;
            int1 = n7;
        }
        int n20 = n2 + this.y(this.o, o);
        if (this.f) {
            n20 += this.p.c(o).h();
        }
        return n20;
    }
    
    public final void w0(final int n, final Object o, final Writer writer) {
        if (o instanceof String) {
            writer.d(n, (String)o);
        }
        else {
            writer.J(n, (ByteString)o);
        }
    }
    
    public final int x(final Object o) {
        final Unsafe s = a0.s;
        int i = 0;
        int n = 0;
        while (i < this.a.length) {
            final int r0 = this.r0(i);
            final int q0 = q0(r0);
            final int t = this.T(i);
            final long u = U(r0);
            int n2;
            if (q0 >= FieldType.DOUBLE_LIST_PACKED.id() && q0 <= FieldType.SINT64_LIST_PACKED.id()) {
                n2 = (this.a[i + 2] & 0xFFFFF);
            }
            else {
                n2 = 0;
            }
            int n3 = 0;
            Label_2125: {
                while (true) {
                    Label_2115: {
                        Label_2093: {
                            long n10 = 0L;
                            Label_2070: {
                                long n9 = 0L;
                                Label_2039: {
                                    int n8 = 0;
                                    Label_2009: {
                                        Label_1980: {
                                            Label_1958: {
                                                Label_1936: {
                                                    Label_1865: {
                                                        Object e = null;
                                                        Label_1839: {
                                                            Label_1831: {
                                                                int n7 = 0;
                                                                Label_1809: {
                                                                    int n6 = 0;
                                                                    Label_1780: {
                                                                        Label_1751: {
                                                                            Label_1729: {
                                                                                int n5 = 0;
                                                                                Label_1707: {
                                                                                    long n4 = 0L;
                                                                                    Label_1677: {
                                                                                        Label_1634: {
                                                                                            int x = 0;
                                                                                            Label_1391: {
                                                                                                Label_1381: {
                                                                                                    Object o3 = null;
                                                                                                    Label_0605: {
                                                                                                        switch (q0) {
                                                                                                            default: {
                                                                                                                n3 = n;
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 68: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1634;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 67: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n4 = Z(o, u);
                                                                                                                    break Label_1677;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 66: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n5 = Y(o, u);
                                                                                                                    break Label_1707;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 65: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1729;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 64: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1751;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 63: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n6 = Y(o, u);
                                                                                                                    break Label_1780;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 62: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n7 = Y(o, u);
                                                                                                                    break Label_1809;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 61: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1831;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 60: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1865;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 59: {
                                                                                                                n3 = n;
                                                                                                                if (!this.H(o, t, i)) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                final Object o2 = o3 = e12.E(o, u);
                                                                                                                if (o2 instanceof ByteString) {
                                                                                                                    e = o2;
                                                                                                                    break;
                                                                                                                }
                                                                                                                break Label_0605;
                                                                                                            }
                                                                                                            case 58: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1936;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 57: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1958;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 56: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_1980;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 55: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n8 = Y(o, u);
                                                                                                                    break Label_2009;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 54: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n9 = Z(o, u);
                                                                                                                    break Label_2039;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 53: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    n10 = Z(o, u);
                                                                                                                    break Label_2070;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 52: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_2093;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 51: {
                                                                                                                n3 = n;
                                                                                                                if (this.H(o, t, i)) {
                                                                                                                    break Label_2115;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 50: {
                                                                                                                final int n11 = this.q.d(t, e12.E(o, u), this.t(i));
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 49: {
                                                                                                                final int n11 = g0.j(t, J(o, u), this.u(i));
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 48: {
                                                                                                                final int t2 = g0.t((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (t2 <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = t2;
                                                                                                                if (this.i) {
                                                                                                                    x = t2;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 47: {
                                                                                                                final int r2 = g0.r((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (r2 <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = r2;
                                                                                                                if (this.i) {
                                                                                                                    x = r2;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 46: {
                                                                                                                final int j = g0.i((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (j <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = j;
                                                                                                                if (this.i) {
                                                                                                                    x = j;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 45: {
                                                                                                                final int g = g0.g((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (g <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = g;
                                                                                                                if (this.i) {
                                                                                                                    x = g;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 44: {
                                                                                                                final int e2 = g0.e((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (e2 <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = e2;
                                                                                                                if (this.i) {
                                                                                                                    x = e2;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 43: {
                                                                                                                final int w = g0.w((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (w <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = w;
                                                                                                                if (this.i) {
                                                                                                                    x = w;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 42: {
                                                                                                                final int b = g0.b((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (b <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = b;
                                                                                                                if (this.i) {
                                                                                                                    x = b;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 41: {
                                                                                                                final int g2 = g0.g((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (g2 <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = g2;
                                                                                                                if (this.i) {
                                                                                                                    x = g2;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 40: {
                                                                                                                final int k = g0.i((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (k <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = k;
                                                                                                                if (this.i) {
                                                                                                                    x = k;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 39: {
                                                                                                                final int l = g0.l((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (l <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = l;
                                                                                                                if (this.i) {
                                                                                                                    x = l;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 38: {
                                                                                                                final int y = g0.y((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (y <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = y;
                                                                                                                if (this.i) {
                                                                                                                    x = y;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 37: {
                                                                                                                final int n12 = g0.n((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (n12 <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = n12;
                                                                                                                if (this.i) {
                                                                                                                    x = n12;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 36: {
                                                                                                                final int g3 = g0.g((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (g3 <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = g3;
                                                                                                                if (this.i) {
                                                                                                                    x = g3;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 35: {
                                                                                                                final int m = g0.i((List)s.getObject(o, u));
                                                                                                                n3 = n;
                                                                                                                if (m <= 0) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                x = m;
                                                                                                                if (this.i) {
                                                                                                                    x = m;
                                                                                                                    break Label_1381;
                                                                                                                }
                                                                                                                break Label_1391;
                                                                                                            }
                                                                                                            case 34: {
                                                                                                                final int n11 = g0.s(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 33: {
                                                                                                                final int n11 = g0.q(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 30: {
                                                                                                                final int n11 = g0.d(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 29: {
                                                                                                                final int n11 = g0.v(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 28: {
                                                                                                                final int n11 = g0.c(t, J(o, u));
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 27: {
                                                                                                                final int n11 = g0.p(t, J(o, u), this.u(i));
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 26: {
                                                                                                                final int n11 = g0.u(t, J(o, u));
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 25: {
                                                                                                                final int n11 = g0.a(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 22: {
                                                                                                                final int n11 = g0.k(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 21: {
                                                                                                                final int n11 = g0.x(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 20: {
                                                                                                                final int n11 = g0.m(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 19:
                                                                                                            case 24:
                                                                                                            case 31: {
                                                                                                                final int n11 = g0.f(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 18:
                                                                                                            case 23:
                                                                                                            case 32: {
                                                                                                                final int n11 = g0.h(t, J(o, u), false);
                                                                                                                break Label_1614;
                                                                                                            }
                                                                                                            case 17: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1634;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 16: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n4 = e12.C(o, u);
                                                                                                                    break Label_1677;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 15: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n5 = e12.A(o, u);
                                                                                                                    break Label_1707;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 14: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1729;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 13: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1751;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 12: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n6 = e12.A(o, u);
                                                                                                                    break Label_1780;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 11: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n7 = e12.A(o, u);
                                                                                                                    break Label_1809;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 10: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1831;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 9: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1865;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 8: {
                                                                                                                n3 = n;
                                                                                                                if (!this.B(o, i)) {
                                                                                                                    break Label_2125;
                                                                                                                }
                                                                                                                final Object o4 = o3 = e12.E(o, u);
                                                                                                                if (o4 instanceof ByteString) {
                                                                                                                    e = o4;
                                                                                                                    break;
                                                                                                                }
                                                                                                                break Label_0605;
                                                                                                            }
                                                                                                            case 7: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1936;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 6: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1958;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 5: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_1980;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 4: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n8 = e12.A(o, u);
                                                                                                                    break Label_2009;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 3: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n9 = e12.C(o, u);
                                                                                                                    break Label_2039;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 2: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    n10 = e12.C(o, u);
                                                                                                                    break Label_2070;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 1: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_2093;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                            case 0: {
                                                                                                                n3 = n;
                                                                                                                if (this.B(o, i)) {
                                                                                                                    break Label_2115;
                                                                                                                }
                                                                                                                break Label_2125;
                                                                                                            }
                                                                                                        }
                                                                                                        break Label_1839;
                                                                                                    }
                                                                                                    final int n11 = CodedOutputStream.P(t, (String)o3);
                                                                                                    break Label_1614;
                                                                                                }
                                                                                                s.putInt(o, n2, x);
                                                                                            }
                                                                                            final int n11 = CodedOutputStream.R(t) + CodedOutputStream.T(x) + x;
                                                                                            n3 = n + n11;
                                                                                            break Label_2125;
                                                                                        }
                                                                                        final int n11 = CodedOutputStream.t(t, (y)e12.E(o, u), this.u(i));
                                                                                        continue;
                                                                                    }
                                                                                    final int n11 = CodedOutputStream.N(t, n4);
                                                                                    continue;
                                                                                }
                                                                                final int n11 = CodedOutputStream.L(t, n5);
                                                                                continue;
                                                                            }
                                                                            final int n11 = CodedOutputStream.J(t, 0L);
                                                                            continue;
                                                                        }
                                                                        final int n11 = CodedOutputStream.H(t, 0);
                                                                        continue;
                                                                    }
                                                                    final int n11 = CodedOutputStream.l(t, n6);
                                                                    continue;
                                                                }
                                                                final int n11 = CodedOutputStream.S(t, n7);
                                                                continue;
                                                            }
                                                            e = e12.E(o, u);
                                                        }
                                                        final int n11 = CodedOutputStream.h(t, (ByteString)e);
                                                        continue;
                                                    }
                                                    final int n11 = g0.o(t, e12.E(o, u), this.u(i));
                                                    continue;
                                                }
                                                final int n11 = CodedOutputStream.e(t, true);
                                                continue;
                                            }
                                            final int n11 = CodedOutputStream.n(t, 0);
                                            continue;
                                        }
                                        final int n11 = CodedOutputStream.p(t, 0L);
                                        continue;
                                    }
                                    final int n11 = CodedOutputStream.w(t, n8);
                                    continue;
                                }
                                final int n11 = CodedOutputStream.U(t, n9);
                                continue;
                            }
                            final int n11 = CodedOutputStream.y(t, n10);
                            continue;
                        }
                        final int n11 = CodedOutputStream.r(t, 0.0f);
                        continue;
                    }
                    final int n11 = CodedOutputStream.j(t, 0.0);
                    continue;
                }
            }
            i += 3;
            n = n3;
        }
        return n + this.y(this.o, o);
    }
    
    public final void x0(final i0 i0, final Object o, final Writer writer) {
        i0.t(i0.g(o), writer);
    }
    
    public final int y(final i0 i0, final Object o) {
        return i0.h(i0.g(o));
    }
}

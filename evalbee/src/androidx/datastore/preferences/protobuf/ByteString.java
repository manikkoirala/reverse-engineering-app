// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Arrays;
import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.nio.charset.UnsupportedCharsetException;
import java.io.UnsupportedEncodingException;
import java.util.NoSuchElementException;
import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Iterator;
import java.util.Comparator;
import java.io.Serializable;

public abstract class ByteString implements Iterable<Byte>, Serializable
{
    static final int CONCATENATE_BY_COPY_SIZE = 128;
    public static final ByteString EMPTY;
    static final int MAX_READ_FROM_CHUNK_SIZE = 8192;
    static final int MIN_READ_FROM_CHUNK_SIZE = 256;
    private static final int UNSIGNED_BYTE_MASK = 255;
    private static final Comparator<ByteString> UNSIGNED_LEXICOGRAPHICAL_COMPARATOR;
    private static final e byteArrayCopier;
    private int hash;
    
    static {
        EMPTY = new LiteralByteString(r.c);
        e byteArrayCopier2;
        if (q4.c()) {
            byteArrayCopier2 = new i(null);
        }
        else {
            byteArrayCopier2 = new d(null);
        }
        byteArrayCopier = byteArrayCopier2;
        UNSIGNED_LEXICOGRAPHICAL_COMPARATOR = new Comparator() {
            public int a(final ByteString byteString, final ByteString byteString2) {
                final f iterator = byteString.iterator();
                final f iterator2 = byteString2.iterator();
                while (iterator.hasNext() && iterator2.hasNext()) {
                    final int compare = Integer.compare(toInt(iterator.a()), toInt(iterator2.a()));
                    if (compare != 0) {
                        return compare;
                    }
                }
                return Integer.compare(byteString.size(), byteString2.size());
            }
        };
    }
    
    public ByteString() {
        this.hash = 0;
    }
    
    private static ByteString balancedConcat(final Iterator<ByteString> iterator, final int i) {
        if (i >= 1) {
            ByteString concat;
            if (i == 1) {
                concat = iterator.next();
            }
            else {
                final int n = i >>> 1;
                concat = balancedConcat(iterator, n).concat(balancedConcat(iterator, i - n));
            }
            return concat;
        }
        throw new IllegalArgumentException(String.format("length (%s) must be >= 1", i));
    }
    
    public static void checkIndex(final int n, final int i) {
        if ((i - (n + 1) | n) >= 0) {
            return;
        }
        if (n < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index < 0: ");
            sb.append(n);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Index > length: ");
        sb2.append(n);
        sb2.append(", ");
        sb2.append(i);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }
    
    public static int checkRange(final int n, final int n2, final int i) {
        final int n3 = n2 - n;
        if ((n | n2 | n3 | i - n2) >= 0) {
            return n3;
        }
        if (n < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Beginning index: ");
            sb.append(n);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        }
        if (n2 < n) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(n);
            sb2.append(", ");
            sb2.append(n2);
            throw new IndexOutOfBoundsException(sb2.toString());
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("End index: ");
        sb3.append(n2);
        sb3.append(" >= ");
        sb3.append(i);
        throw new IndexOutOfBoundsException(sb3.toString());
    }
    
    public static ByteString copyFrom(final Iterable<ByteString> iterable) {
        int size;
        if (!(iterable instanceof Collection)) {
            final Iterator iterator = iterable.iterator();
            int n = 0;
            while (true) {
                size = n;
                if (!iterator.hasNext()) {
                    break;
                }
                iterator.next();
                ++n;
            }
        }
        else {
            size = ((Collection)iterable).size();
        }
        if (size == 0) {
            return ByteString.EMPTY;
        }
        return balancedConcat(iterable.iterator(), size);
    }
    
    public static ByteString copyFrom(final String s, final String charsetName) {
        return new LiteralByteString(s.getBytes(charsetName));
    }
    
    public static ByteString copyFrom(final String s, final Charset charset) {
        return new LiteralByteString(s.getBytes(charset));
    }
    
    public static ByteString copyFrom(final ByteBuffer byteBuffer) {
        return copyFrom(byteBuffer, byteBuffer.remaining());
    }
    
    public static ByteString copyFrom(final ByteBuffer byteBuffer, final int n) {
        checkRange(0, n, byteBuffer.remaining());
        final byte[] dst = new byte[n];
        byteBuffer.get(dst);
        return new LiteralByteString(dst);
    }
    
    public static ByteString copyFrom(final byte[] array) {
        return copyFrom(array, 0, array.length);
    }
    
    public static ByteString copyFrom(final byte[] array, final int n, final int n2) {
        checkRange(n, n + n2, array.length);
        return new LiteralByteString(ByteString.byteArrayCopier.a(array, n, n2));
    }
    
    public static ByteString copyFromUtf8(final String s) {
        return new LiteralByteString(s.getBytes(r.a));
    }
    
    public static g newCodedBuilder(final int n) {
        return new g(n, null);
    }
    
    public static h newOutput() {
        return new h(128);
    }
    
    public static h newOutput(final int n) {
        return new h(n);
    }
    
    private static ByteString readChunk(final InputStream inputStream, final int n) {
        final byte[] b = new byte[n];
        int i;
        int read;
        for (i = 0; i < n; i += read) {
            read = inputStream.read(b, i, n - i);
            if (read == -1) {
                break;
            }
        }
        if (i == 0) {
            return null;
        }
        return copyFrom(b, 0, i);
    }
    
    public static ByteString readFrom(final InputStream inputStream) {
        return readFrom(inputStream, 256, 8192);
    }
    
    public static ByteString readFrom(final InputStream inputStream, final int n) {
        return readFrom(inputStream, n, n);
    }
    
    public static ByteString readFrom(final InputStream inputStream, int min, final int b) {
        final ArrayList list = new ArrayList();
        while (true) {
            final ByteString chunk = readChunk(inputStream, min);
            if (chunk == null) {
                break;
            }
            list.add(chunk);
            min = Math.min(min * 2, b);
        }
        return copyFrom(list);
    }
    
    private static int toInt(final byte b) {
        return b & 0xFF;
    }
    
    public static Comparator<ByteString> unsignedLexicographicalComparator() {
        return ByteString.UNSIGNED_LEXICOGRAPHICAL_COMPARATOR;
    }
    
    public static ByteString wrap(final ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            return wrap(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
        }
        return new NioByteString(byteBuffer);
    }
    
    public static ByteString wrap(final byte[] array) {
        return new LiteralByteString(array);
    }
    
    public static ByteString wrap(final byte[] array, final int n, final int n2) {
        return new BoundedByteString(array, n, n2);
    }
    
    public abstract ByteBuffer asReadOnlyByteBuffer();
    
    public abstract List<ByteBuffer> asReadOnlyByteBufferList();
    
    public abstract byte byteAt(final int p0);
    
    public final ByteString concat(final ByteString byteString) {
        if (Integer.MAX_VALUE - this.size() >= byteString.size()) {
            return RopeByteString.concatenate(this, byteString);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ByteString would be too long: ");
        sb.append(this.size());
        sb.append("+");
        sb.append(byteString.size());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public abstract void copyTo(final ByteBuffer p0);
    
    public void copyTo(final byte[] array, final int n) {
        this.copyTo(array, 0, n, this.size());
    }
    
    @Deprecated
    public final void copyTo(final byte[] array, final int n, final int n2, final int n3) {
        checkRange(n, n + n3, this.size());
        checkRange(n2, n2 + n3, array.length);
        if (n3 > 0) {
            this.copyToInternal(array, n, n2, n3);
        }
    }
    
    public abstract void copyToInternal(final byte[] p0, final int p1, final int p2, final int p3);
    
    public final boolean endsWith(final ByteString byteString) {
        return this.size() >= byteString.size() && this.substring(this.size() - byteString.size()).equals(byteString);
    }
    
    @Override
    public abstract boolean equals(final Object p0);
    
    public abstract int getTreeDepth();
    
    @Override
    public final int hashCode() {
        int hash;
        if ((hash = this.hash) == 0) {
            final int size = this.size();
            if ((hash = this.partialHash(size, 0, size)) == 0) {
                hash = 1;
            }
            this.hash = hash;
        }
        return hash;
    }
    
    public abstract byte internalByteAt(final int p0);
    
    public abstract boolean isBalanced();
    
    public final boolean isEmpty() {
        return this.size() == 0;
    }
    
    public abstract boolean isValidUtf8();
    
    @Override
    public f iterator() {
        return (f)new c(this) {
            public int a = 0;
            public final int b = c.size();
            public final ByteString c;
            
            @Override
            public byte a() {
                final int a = this.a;
                if (a < this.b) {
                    this.a = a + 1;
                    return this.c.internalByteAt(a);
                }
                throw new NoSuchElementException();
            }
            
            @Override
            public boolean hasNext() {
                return this.a < this.b;
            }
        };
    }
    
    public abstract androidx.datastore.preferences.protobuf.f newCodedInput();
    
    public abstract InputStream newInput();
    
    public abstract int partialHash(final int p0, final int p1, final int p2);
    
    public abstract int partialIsValidUtf8(final int p0, final int p1, final int p2);
    
    public final int peekCachedHashCode() {
        return this.hash;
    }
    
    public abstract int size();
    
    public final boolean startsWith(final ByteString byteString) {
        final int size = this.size();
        final int size2 = byteString.size();
        boolean b = false;
        if (size >= size2) {
            b = b;
            if (this.substring(0, byteString.size()).equals(byteString)) {
                b = true;
            }
        }
        return b;
    }
    
    public final ByteString substring(final int n) {
        return this.substring(n, this.size());
    }
    
    public abstract ByteString substring(final int p0, final int p1);
    
    public final byte[] toByteArray() {
        final int size = this.size();
        if (size == 0) {
            return r.c;
        }
        final byte[] array = new byte[size];
        this.copyToInternal(array, 0, 0, size);
        return array;
    }
    
    @Override
    public final String toString() {
        return String.format("<ByteString@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), this.size());
    }
    
    public final String toString(final String s) {
        try {
            return this.toString(Charset.forName(s));
        }
        catch (final UnsupportedCharsetException cause) {
            final UnsupportedEncodingException ex = new UnsupportedEncodingException(s);
            ex.initCause(cause);
            throw ex;
        }
    }
    
    public final String toString(final Charset charset) {
        String stringInternal;
        if (this.size() == 0) {
            stringInternal = "";
        }
        else {
            stringInternal = this.toStringInternal(charset);
        }
        return stringInternal;
    }
    
    public abstract String toStringInternal(final Charset p0);
    
    public final String toStringUtf8() {
        return this.toString(r.a);
    }
    
    public abstract void writeTo(final OutputStream p0);
    
    public final void writeTo(final OutputStream outputStream, final int n, final int n2) {
        checkRange(n, n + n2, this.size());
        if (n2 > 0) {
            this.writeToInternal(outputStream, n, n2);
        }
    }
    
    public abstract void writeTo(final qd p0);
    
    public abstract void writeToInternal(final OutputStream p0, final int p1, final int p2);
    
    public abstract void writeToReverse(final qd p0);
    
    public static final class BoundedByteString extends LiteralByteString
    {
        private static final long serialVersionUID = 1L;
        private final int bytesLength;
        private final int bytesOffset;
        
        public BoundedByteString(final byte[] array, final int bytesOffset, final int bytesLength) {
            super(array);
            ByteString.checkRange(bytesOffset, bytesOffset + bytesLength, array.length);
            this.bytesOffset = bytesOffset;
            this.bytesLength = bytesLength;
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            throw new InvalidObjectException("BoundedByteStream instances are not to be serialized directly");
        }
        
        @Override
        public byte byteAt(final int n) {
            ByteString.checkIndex(n, this.size());
            return super.bytes[this.bytesOffset + n];
        }
        
        @Override
        public void copyToInternal(final byte[] array, final int n, final int n2, final int n3) {
            System.arraycopy(super.bytes, this.getOffsetIntoBytes() + n, array, n2, n3);
        }
        
        @Override
        public int getOffsetIntoBytes() {
            return this.bytesOffset;
        }
        
        @Override
        public byte internalByteAt(final int n) {
            return super.bytes[this.bytesOffset + n];
        }
        
        @Override
        public int size() {
            return this.bytesLength;
        }
        
        public Object writeReplace() {
            return ByteString.wrap(this.toByteArray());
        }
    }
    
    public abstract static class LeafByteString extends ByteString
    {
        public abstract boolean equalsRange(final ByteString p0, final int p1, final int p2);
        
        @Override
        public final int getTreeDepth() {
            return 0;
        }
        
        @Override
        public final boolean isBalanced() {
            return true;
        }
        
        @Override
        public void writeToReverse(final qd qd) {
            this.writeTo(qd);
        }
    }
    
    public static class LiteralByteString extends LeafByteString
    {
        private static final long serialVersionUID = 1L;
        protected final byte[] bytes;
        
        public LiteralByteString(final byte[] bytes) {
            bytes.getClass();
            this.bytes = bytes;
        }
        
        @Override
        public final ByteBuffer asReadOnlyByteBuffer() {
            return ByteBuffer.wrap(this.bytes, this.getOffsetIntoBytes(), this.size()).asReadOnlyBuffer();
        }
        
        @Override
        public final List<ByteBuffer> asReadOnlyByteBufferList() {
            return Collections.singletonList(this.asReadOnlyByteBuffer());
        }
        
        @Override
        public byte byteAt(final int n) {
            return this.bytes[n];
        }
        
        @Override
        public final void copyTo(final ByteBuffer byteBuffer) {
            byteBuffer.put(this.bytes, this.getOffsetIntoBytes(), this.size());
        }
        
        @Override
        public void copyToInternal(final byte[] array, final int n, final int n2, final int n3) {
            System.arraycopy(this.bytes, n, array, n2, n3);
        }
        
        @Override
        public final boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof ByteString)) {
                return false;
            }
            if (this.size() != ((ByteString)o).size()) {
                return false;
            }
            if (this.size() == 0) {
                return true;
            }
            if (o instanceof LiteralByteString) {
                final LiteralByteString literalByteString = (LiteralByteString)o;
                final int peekCachedHashCode = this.peekCachedHashCode();
                final int peekCachedHashCode2 = literalByteString.peekCachedHashCode();
                return (peekCachedHashCode == 0 || peekCachedHashCode2 == 0 || peekCachedHashCode == peekCachedHashCode2) && this.equalsRange(literalByteString, 0, this.size());
            }
            return o.equals(this);
        }
        
        @Override
        public final boolean equalsRange(final ByteString byteString, int i, final int n) {
            if (n > byteString.size()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Length too large: ");
                sb.append(n);
                sb.append(this.size());
                throw new IllegalArgumentException(sb.toString());
            }
            final int n2 = i + n;
            if (n2 > byteString.size()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Ran off end of other: ");
                sb2.append(i);
                sb2.append(", ");
                sb2.append(n);
                sb2.append(", ");
                sb2.append(byteString.size());
                throw new IllegalArgumentException(sb2.toString());
            }
            if (byteString instanceof LiteralByteString) {
                final LiteralByteString literalByteString = (LiteralByteString)byteString;
                final byte[] bytes = this.bytes;
                final byte[] bytes2 = literalByteString.bytes;
                int offsetIntoBytes;
                int j;
                for (offsetIntoBytes = this.getOffsetIntoBytes(), j = this.getOffsetIntoBytes(), i += literalByteString.getOffsetIntoBytes(); j < offsetIntoBytes + n; ++j, ++i) {
                    if (bytes[j] != bytes2[i]) {
                        return false;
                    }
                }
                return true;
            }
            return byteString.substring(i, n2).equals(this.substring(0, n));
        }
        
        public int getOffsetIntoBytes() {
            return 0;
        }
        
        @Override
        public byte internalByteAt(final int n) {
            return this.bytes[n];
        }
        
        @Override
        public final boolean isValidUtf8() {
            final int offsetIntoBytes = this.getOffsetIntoBytes();
            return Utf8.t(this.bytes, offsetIntoBytes, this.size() + offsetIntoBytes);
        }
        
        @Override
        public final androidx.datastore.preferences.protobuf.f newCodedInput() {
            return androidx.datastore.preferences.protobuf.f.k(this.bytes, this.getOffsetIntoBytes(), this.size(), true);
        }
        
        @Override
        public final InputStream newInput() {
            return new ByteArrayInputStream(this.bytes, this.getOffsetIntoBytes(), this.size());
        }
        
        @Override
        public final int partialHash(final int n, final int n2, final int n3) {
            return r.i(n, this.bytes, this.getOffsetIntoBytes() + n2, n3);
        }
        
        @Override
        public final int partialIsValidUtf8(final int n, int n2, final int n3) {
            n2 += this.getOffsetIntoBytes();
            return Utf8.v(n, this.bytes, n2, n3 + n2);
        }
        
        @Override
        public int size() {
            return this.bytes.length;
        }
        
        @Override
        public final ByteString substring(final int n, int checkRange) {
            checkRange = ByteString.checkRange(n, checkRange, this.size());
            if (checkRange == 0) {
                return ByteString.EMPTY;
            }
            return new BoundedByteString(this.bytes, this.getOffsetIntoBytes() + n, checkRange);
        }
        
        @Override
        public final String toStringInternal(final Charset charset) {
            return new String(this.bytes, this.getOffsetIntoBytes(), this.size(), charset);
        }
        
        @Override
        public final void writeTo(final OutputStream outputStream) {
            outputStream.write(this.toByteArray());
        }
        
        @Override
        public final void writeTo(final qd qd) {
            qd.b(this.bytes, this.getOffsetIntoBytes(), this.size());
        }
        
        @Override
        public final void writeToInternal(final OutputStream outputStream, final int n, final int len) {
            outputStream.write(this.bytes, this.getOffsetIntoBytes() + n, len);
        }
    }
    
    public abstract static class c implements f
    {
        public final Byte b() {
            return ((f)this).a();
        }
        
        @Override
        public final void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    public interface f extends Iterator
    {
        byte a();
    }
    
    public static final class d implements e
    {
        @Override
        public byte[] a(final byte[] original, final int from, final int n) {
            return Arrays.copyOfRange(original, from, n + from);
        }
    }
    
    public interface e
    {
        byte[] a(final byte[] p0, final int p1, final int p2);
    }
    
    public static final class g
    {
        public final CodedOutputStream a;
        public final byte[] b;
        
        public g(final int n) {
            final byte[] b = new byte[n];
            this.b = b;
            this.a = CodedOutputStream.c0(b);
        }
        
        public ByteString a() {
            this.a.d();
            return new LiteralByteString(this.b);
        }
        
        public CodedOutputStream b() {
            return this.a;
        }
    }
    
    public static final class h extends OutputStream
    {
        public static final byte[] f;
        public final int a;
        public final ArrayList b;
        public int c;
        public byte[] d;
        public int e;
        
        static {
            f = new byte[0];
        }
        
        public h(final int a) {
            if (a >= 0) {
                this.a = a;
                this.b = new ArrayList();
                this.d = new byte[a];
                return;
            }
            throw new IllegalArgumentException("Buffer size < 0");
        }
        
        public final void a(final int a) {
            this.b.add(new LiteralByteString(this.d));
            final int c = this.c + this.d.length;
            this.c = c;
            this.d = new byte[Math.max(this.a, Math.max(a, c >>> 1))];
            this.e = 0;
        }
        
        public int b() {
            synchronized (this) {
                return this.c + this.e;
            }
        }
        
        @Override
        public String toString() {
            return String.format("<ByteString.Output@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), this.b());
        }
        
        @Override
        public void write(final int n) {
            synchronized (this) {
                if (this.e == this.d.length) {
                    this.a(1);
                }
                this.d[this.e++] = (byte)n;
            }
        }
        
        @Override
        public void write(final byte[] array, final int n, int e) {
            synchronized (this) {
                final byte[] d = this.d;
                final int length = d.length;
                final int e2 = this.e;
                if (e <= length - e2) {
                    System.arraycopy(array, n, d, e2, e);
                    this.e += e;
                }
                else {
                    final int n2 = d.length - e2;
                    System.arraycopy(array, n, d, e2, n2);
                    e -= n2;
                    this.a(e);
                    System.arraycopy(array, n + n2, this.d, 0, e);
                    this.e = e;
                }
            }
        }
    }
    
    public static final class i implements e
    {
        @Override
        public byte[] a(final byte[] array, final int n, final int n2) {
            final byte[] array2 = new byte[n2];
            System.arraycopy(array, n, array2, 0, n2);
            return array2;
        }
    }
}

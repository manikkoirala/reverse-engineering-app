// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public enum NullValue implements a
{
    private static final NullValue[] $VALUES;
    
    NULL_VALUE(0);
    
    public static final int NULL_VALUE_VALUE = 0;
    
    UNRECOGNIZED(-1);
    
    private static final r.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new r.b() {};
    }
    
    private NullValue(final int value) {
        this.value = value;
    }
    
    public static NullValue forNumber(final int n) {
        if (n != 0) {
            return null;
        }
        return NullValue.NULL_VALUE;
    }
    
    public static r.b internalGetValueMap() {
        return NullValue.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static NullValue valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != NullValue.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return NullValue.forNumber(n) != null;
        }
    }
}

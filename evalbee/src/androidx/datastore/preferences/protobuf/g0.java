// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Iterator;
import java.util.RandomAccess;
import java.util.List;

public abstract class g0
{
    public static final Class a;
    public static final i0 b;
    public static final i0 c;
    public static final i0 d;
    
    static {
        a = A();
        b = B(false);
        c = B(true);
        d = new k0();
    }
    
    public static Class A() {
        try {
            return Class.forName("androidx.datastore.preferences.protobuf.GeneratedMessageV3");
        }
        finally {
            return null;
        }
    }
    
    public static i0 B(final boolean b) {
        try {
            final Class c = C();
            if (c == null) {}
            return c.getConstructor(Boolean.TYPE).newInstance(b);
        }
        finally {
            return null;
        }
    }
    
    public static Class C() {
        try {
            return Class.forName("androidx.datastore.preferences.protobuf.UnknownFieldSetSchema");
        }
        finally {
            return null;
        }
    }
    
    public static void D(final l l, final Object o, final Object o2) {
        final n c = l.c(o2);
        if (!c.j()) {
            l.d(o).p(c);
        }
    }
    
    public static void E(final w w, final Object o, final Object o2, final long n) {
        e12.T(o, n, w.a(e12.E(o, n), e12.E(o2, n)));
    }
    
    public static void F(final i0 i0, final Object o, final Object o2) {
        i0.p(o, i0.k(i0.g(o), i0.g(o2)));
    }
    
    public static i0 G() {
        return g0.b;
    }
    
    public static i0 H() {
        return g0.c;
    }
    
    public static void I(final Class clazz) {
        if (!GeneratedMessageLite.class.isAssignableFrom(clazz)) {
            final Class a = g0.a;
            if (a != null) {
                if (!a.isAssignableFrom(clazz)) {
                    throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
                }
            }
        }
    }
    
    public static boolean J(final Object o, final Object obj) {
        return o == obj || (o != null && o.equals(obj));
    }
    
    public static Object K(final int n, final int n2, final Object o, final i0 i0) {
        Object n3 = o;
        if (o == null) {
            n3 = i0.n();
        }
        i0.e(n3, n, n2);
        return n3;
    }
    
    public static i0 L() {
        return g0.d;
    }
    
    public static void M(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.E(n, list, b);
        }
    }
    
    public static void N(final int n, final List list, final Writer writer) {
        if (list != null && !list.isEmpty()) {
            writer.u(n, list);
        }
    }
    
    public static void O(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.t(n, list, b);
        }
    }
    
    public static void P(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.y(n, list, b);
        }
    }
    
    public static void Q(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.h(n, list, b);
        }
    }
    
    public static void R(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.q(n, list, b);
        }
    }
    
    public static void S(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.a(n, list, b);
        }
    }
    
    public static void T(final int n, final List list, final Writer writer, final f0 f0) {
        if (list != null && !list.isEmpty()) {
            writer.M(n, list, f0);
        }
    }
    
    public static void U(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.f(n, list, b);
        }
    }
    
    public static void V(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.H(n, list, b);
        }
    }
    
    public static void W(final int n, final List list, final Writer writer, final f0 f0) {
        if (list != null && !list.isEmpty()) {
            writer.O(n, list, f0);
        }
    }
    
    public static void X(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.D(n, list, b);
        }
    }
    
    public static void Y(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.A(n, list, b);
        }
    }
    
    public static void Z(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.s(n, list, b);
        }
    }
    
    public static int a(final int n, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return CodedOutputStream.R(n) + CodedOutputStream.A(size);
        }
        return size * CodedOutputStream.e(n, true);
    }
    
    public static void a0(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.x(n, list, b);
        }
    }
    
    public static int b(final List list) {
        return list.size();
    }
    
    public static void b0(final int n, final List list, final Writer writer) {
        if (list != null && !list.isEmpty()) {
            writer.v(n, list);
        }
    }
    
    public static int c(int i, final List list) {
        final int size = list.size();
        final int n = 0;
        if (size == 0) {
            return 0;
        }
        final int n2 = size * CodedOutputStream.R(i);
        i = n;
        int n3 = n2;
        while (i < list.size()) {
            n3 += CodedOutputStream.i(list.get(i));
            ++i;
        }
        return n3;
    }
    
    public static void c0(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.i(n, list, b);
        }
    }
    
    public static int d(int r, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int e = e(list);
        r = CodedOutputStream.R(r);
        if (b) {
            return r + CodedOutputStream.A(e);
        }
        return e + size * r;
    }
    
    public static void d0(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.l(n, list, b);
        }
    }
    
    public static int e(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof q) {
            final q q = (q)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.m(q.c(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.m((int)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int f(final int n, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return CodedOutputStream.R(n) + CodedOutputStream.A(size * 4);
        }
        return size * CodedOutputStream.n(n, 0);
    }
    
    public static int g(final List list) {
        return list.size() * 4;
    }
    
    public static int h(final int n, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return CodedOutputStream.R(n) + CodedOutputStream.A(size * 8);
        }
        return size * CodedOutputStream.p(n, 0L);
    }
    
    public static int i(final List list) {
        return list.size() * 8;
    }
    
    public static int j(final int n, final List list, final f0 f0) {
        final int size = list.size();
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n2 = 0;
        while (i < size) {
            n2 += CodedOutputStream.t(n, list.get(i), f0);
            ++i;
        }
        return n2;
    }
    
    public static int k(int r, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int l = l(list);
        r = CodedOutputStream.R(r);
        if (b) {
            return r + CodedOutputStream.A(l);
        }
        return l + size * r;
    }
    
    public static int l(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof q) {
            final q q = (q)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.x(q.c(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.x((int)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int m(final int n, final List list, final boolean b) {
        if (list.size() == 0) {
            return 0;
        }
        final int n2 = n(list);
        if (b) {
            return CodedOutputStream.R(n) + CodedOutputStream.A(n2);
        }
        return n2 + list.size() * CodedOutputStream.R(n);
    }
    
    public static int n(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof u) {
            final u u = (u)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.z(u.c(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.z((long)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int o(final int n, final Object o, final f0 f0) {
        return CodedOutputStream.B(n, (y)o, f0);
    }
    
    public static int p(int n, final List list, final f0 f0) {
        final int size = list.size();
        int i = 0;
        if (size == 0) {
            return 0;
        }
        n = CodedOutputStream.R(n) * size;
        while (i < size) {
            n += CodedOutputStream.D((y)list.get(i), f0);
            ++i;
        }
        return n;
    }
    
    public static int q(int r, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int r2 = r(list);
        r = CodedOutputStream.R(r);
        if (b) {
            return r + CodedOutputStream.A(r2);
        }
        return r2 + size * r;
    }
    
    public static int r(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof q) {
            final q q = (q)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.M(q.c(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.M(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static int s(int r, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int t = t(list);
        r = CodedOutputStream.R(r);
        if (b) {
            return r + CodedOutputStream.A(t);
        }
        return t + size * r;
    }
    
    public static int t(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof u) {
            final u u = (u)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.O(u.c(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.O(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static int u(int n, final List list) {
        final int size = list.size();
        int n2 = 0;
        final int n3 = 0;
        if (size == 0) {
            return 0;
        }
        final int n4 = n = CodedOutputStream.R(n) * size;
        int n6;
        if (list instanceof gj0) {
            final gj0 gj0 = (gj0)list;
            n = n4;
            int n5 = n3;
            while (true) {
                n6 = n;
                if (n5 >= size) {
                    break;
                }
                final Object k = gj0.k(n5);
                int n7;
                if (k instanceof ByteString) {
                    n7 = CodedOutputStream.i((ByteString)k);
                }
                else {
                    n7 = CodedOutputStream.Q((String)k);
                }
                n += n7;
                ++n5;
            }
        }
        else {
            while (true) {
                n6 = n;
                if (n2 >= size) {
                    break;
                }
                final Object value = list.get(n2);
                int n8;
                if (value instanceof ByteString) {
                    n8 = CodedOutputStream.i((ByteString)value);
                }
                else {
                    n8 = CodedOutputStream.Q((String)value);
                }
                n += n8;
                ++n2;
            }
        }
        return n6;
    }
    
    public static int v(int r, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int w = w(list);
        r = CodedOutputStream.R(r);
        if (b) {
            return r + CodedOutputStream.A(w);
        }
        return w + size * r;
    }
    
    public static int w(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof q) {
            final q q = (q)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.T(q.c(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.T(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static int x(int r, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int y = y(list);
        r = CodedOutputStream.R(r);
        if (b) {
            return r + CodedOutputStream.A(y);
        }
        return y + size * r;
    }
    
    public static int y(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof u) {
            final u u = (u)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.V(u.c(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.V(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static Object z(final int n, final List list, final r.c c, Object o, final i0 i0) {
        if (c == null) {
            return o;
        }
        Object o2;
        if (list instanceof RandomAccess) {
            final int size = list.size();
            int j = 0;
            int n2 = 0;
            while (j < size) {
                final int intValue = list.get(j);
                if (c.a(intValue)) {
                    if (j != n2) {
                        list.set(n2, intValue);
                    }
                    ++n2;
                }
                else {
                    o = K(n, intValue, o, i0);
                }
                ++j;
            }
            o2 = o;
            if (n2 != size) {
                list.subList(n2, size).clear();
                o2 = o;
            }
        }
        else {
            final Iterator iterator = list.iterator();
            while (true) {
                o2 = o;
                if (!iterator.hasNext()) {
                    break;
                }
                final int intValue2 = (int)iterator.next();
                if (c.a(intValue2)) {
                    continue;
                }
                o = K(n, intValue2, o, i0);
                iterator.remove();
            }
        }
        return o2;
    }
}

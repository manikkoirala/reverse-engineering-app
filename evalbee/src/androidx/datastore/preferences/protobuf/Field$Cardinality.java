// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public enum Field$Cardinality implements a
{
    private static final Field$Cardinality[] $VALUES;
    
    CARDINALITY_OPTIONAL(1);
    
    public static final int CARDINALITY_OPTIONAL_VALUE = 1;
    
    CARDINALITY_REPEATED(3);
    
    public static final int CARDINALITY_REPEATED_VALUE = 3;
    
    CARDINALITY_REQUIRED(2);
    
    public static final int CARDINALITY_REQUIRED_VALUE = 2;
    
    CARDINALITY_UNKNOWN(0);
    
    public static final int CARDINALITY_UNKNOWN_VALUE = 0;
    
    UNRECOGNIZED(-1);
    
    private static final r.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new r.b() {};
    }
    
    private Field$Cardinality(final int value) {
        this.value = value;
    }
    
    public static Field$Cardinality forNumber(final int n) {
        if (n == 0) {
            return Field$Cardinality.CARDINALITY_UNKNOWN;
        }
        if (n == 1) {
            return Field$Cardinality.CARDINALITY_OPTIONAL;
        }
        if (n == 2) {
            return Field$Cardinality.CARDINALITY_REQUIRED;
        }
        if (n != 3) {
            return null;
        }
        return Field$Cardinality.CARDINALITY_REPEATED;
    }
    
    public static r.b internalGetValueMap() {
        return Field$Cardinality.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static Field$Cardinality valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != Field$Cardinality.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return Field$Cardinality.forNumber(n) != null;
        }
    }
}

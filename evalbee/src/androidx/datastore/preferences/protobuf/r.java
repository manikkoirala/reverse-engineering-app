// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.RandomAccess;
import java.util.List;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public abstract class r
{
    public static final Charset a;
    public static final Charset b;
    public static final byte[] c;
    public static final ByteBuffer d;
    public static final f e;
    
    static {
        a = Charset.forName("UTF-8");
        b = Charset.forName("ISO-8859-1");
        final byte[] array = c = new byte[0];
        d = ByteBuffer.wrap(array);
        e = f.i(array);
    }
    
    public static Object a(final Object o) {
        o.getClass();
        return o;
    }
    
    public static Object b(final Object o, final String s) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(s);
    }
    
    public static int c(final boolean b) {
        int n;
        if (b) {
            n = 1231;
        }
        else {
            n = 1237;
        }
        return n;
    }
    
    public static int d(final byte[] array) {
        return e(array, 0, array.length);
    }
    
    public static int e(final byte[] array, int i, int n) {
        n = (i = i(n, array, i, n));
        if (n == 0) {
            i = 1;
        }
        return i;
    }
    
    public static int f(final long n) {
        return (int)(n ^ n >>> 32);
    }
    
    public static boolean g(final byte[] array) {
        return Utf8.s(array);
    }
    
    public static Object h(final Object o, final Object o2) {
        return ((y)o).f().o((y)o2).g();
    }
    
    public static int i(int n, final byte[] array, final int n2, final int n3) {
        for (int i = n2; i < n2 + n3; ++i) {
            n = n * 31 + array[i];
        }
        return n;
    }
    
    public static String j(final byte[] bytes) {
        return new String(bytes, r.a);
    }
    
    public interface a
    {
        int getNumber();
    }
    
    public interface b
    {
    }
    
    public interface c
    {
        boolean a(final int p0);
    }
    
    public interface d extends List, RandomAccess
    {
        void e();
        
        boolean h();
        
        d j(final int p0);
    }
}

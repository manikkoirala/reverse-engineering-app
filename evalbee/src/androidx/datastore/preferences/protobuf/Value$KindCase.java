// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public enum Value$KindCase
{
    private static final Value$KindCase[] $VALUES;
    
    BOOL_VALUE(4), 
    KIND_NOT_SET(0), 
    LIST_VALUE(6), 
    NULL_VALUE(1), 
    NUMBER_VALUE(2), 
    STRING_VALUE(3), 
    STRUCT_VALUE(5);
    
    private final int value;
    
    private Value$KindCase(final int value) {
        this.value = value;
    }
    
    public static Value$KindCase forNumber(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 6: {
                return Value$KindCase.LIST_VALUE;
            }
            case 5: {
                return Value$KindCase.STRUCT_VALUE;
            }
            case 4: {
                return Value$KindCase.BOOL_VALUE;
            }
            case 3: {
                return Value$KindCase.STRING_VALUE;
            }
            case 2: {
                return Value$KindCase.NUMBER_VALUE;
            }
            case 1: {
                return Value$KindCase.NULL_VALUE;
            }
            case 0: {
                return Value$KindCase.KIND_NOT_SET;
            }
        }
    }
    
    @Deprecated
    public static Value$KindCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

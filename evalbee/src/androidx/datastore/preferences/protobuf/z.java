// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.lang.reflect.Modifier;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.io.Serializable;

public abstract class z
{
    public static final String a(final String s) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (Character.isUpperCase(char1)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(char1));
        }
        return sb.toString();
    }
    
    public static boolean b(final Object o) {
        final boolean b = o instanceof Boolean;
        final boolean b2 = true;
        boolean b3 = true;
        final boolean b4 = true;
        final boolean b5 = true;
        final boolean b6 = true;
        if (b) {
            return (boolean)o ^ true;
        }
        if (o instanceof Integer) {
            return (int)o == 0 && b6;
        }
        if (o instanceof Float) {
            return (float)o == 0.0f && b2;
        }
        if (o instanceof Double) {
            if ((double)o != 0.0) {
                b3 = false;
            }
            return b3;
        }
        Serializable empty;
        if (o instanceof String) {
            empty = "";
        }
        else if (o instanceof ByteString) {
            empty = ByteString.EMPTY;
        }
        else {
            if (o instanceof y) {
                return o == ((y)o).a() && b4;
            }
            return o instanceof Enum && ((Enum)o).ordinal() == 0 && b5;
        }
        return o.equals(empty);
    }
    
    public static final void c(final StringBuilder sb, final int n, String s, final Object o) {
        if (o instanceof List) {
            final Iterator iterator = ((List)o).iterator();
            while (iterator.hasNext()) {
                c(sb, n, s, iterator.next());
            }
            return;
        }
        if (o instanceof Map) {
            final Iterator iterator2 = ((Map)o).entrySet().iterator();
            while (iterator2.hasNext()) {
                c(sb, n, s, iterator2.next());
            }
            return;
        }
        sb.append('\n');
        final int n2 = 0;
        final int n3 = 0;
        for (int i = 0; i < n; ++i) {
            sb.append(' ');
        }
        sb.append(s);
        if (o instanceof String) {
            sb.append(": \"");
            s = cv1.c((String)o);
        }
        else {
            if (!(o instanceof ByteString)) {
                if (o instanceof GeneratedMessageLite) {
                    sb.append(" {");
                    d((y)o, sb, n + 2);
                    sb.append("\n");
                    for (int j = n3; j < n; ++j) {
                        sb.append(' ');
                    }
                }
                else {
                    if (!(o instanceof Map.Entry)) {
                        sb.append(": ");
                        sb.append(o.toString());
                        return;
                    }
                    sb.append(" {");
                    final Map.Entry entry = (Map.Entry)o;
                    final int n4 = n + 2;
                    c(sb, n4, "key", entry.getKey());
                    c(sb, n4, "value", entry.getValue());
                    sb.append("\n");
                    for (int k = n2; k < n; ++k) {
                        sb.append(' ');
                    }
                }
                sb.append("}");
                return;
            }
            sb.append(": \"");
            s = cv1.b((ByteString)o);
        }
        sb.append(s);
        sb.append('\"');
    }
    
    public static void d(final y y, final StringBuilder sb, final int n) {
        final HashMap hashMap = new HashMap();
        final HashMap hashMap2 = new HashMap();
        final TreeSet set = new TreeSet();
        for (final Method method : y.getClass().getDeclaredMethods()) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    set.add(method.getName());
                }
            }
        }
        for (final String s : set) {
            final String replaceFirst = s.replaceFirst("get", "");
            final boolean endsWith = replaceFirst.endsWith("List");
            boolean booleanValue = true;
            if (endsWith && !replaceFirst.endsWith("OrBuilderList") && !replaceFirst.equals("List")) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(replaceFirst.substring(0, 1).toLowerCase());
                sb2.append(replaceFirst.substring(1, replaceFirst.length() - 4));
                final String string = sb2.toString();
                final Method method2 = (Method)hashMap.get(s);
                if (method2 != null && method2.getReturnType().equals(List.class)) {
                    c(sb, n, a(string), GeneratedMessageLite.y(method2, y, new Object[0]));
                    continue;
                }
            }
            if (replaceFirst.endsWith("Map") && !replaceFirst.equals("Map")) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(replaceFirst.substring(0, 1).toLowerCase());
                sb3.append(replaceFirst.substring(1, replaceFirst.length() - 3));
                final String string2 = sb3.toString();
                final Method method3 = (Method)hashMap.get(s);
                if (method3 != null && method3.getReturnType().equals(Map.class) && !method3.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method3.getModifiers())) {
                    c(sb, n, a(string2), GeneratedMessageLite.y(method3, y, new Object[0]));
                    continue;
                }
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("set");
            sb4.append(replaceFirst);
            if (hashMap2.get(sb4.toString()) == null) {
                continue;
            }
            if (replaceFirst.endsWith("Bytes")) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("get");
                sb5.append(replaceFirst.substring(0, replaceFirst.length() - 5));
                if (hashMap.containsKey(sb5.toString())) {
                    continue;
                }
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(replaceFirst.substring(0, 1).toLowerCase());
            sb6.append(replaceFirst.substring(1));
            final String string3 = sb6.toString();
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("get");
            sb7.append(replaceFirst);
            final Method method4 = (Method)hashMap.get(sb7.toString());
            final StringBuilder sb8 = new StringBuilder();
            sb8.append("has");
            sb8.append(replaceFirst);
            final Method method5 = (Method)hashMap.get(sb8.toString());
            if (method4 == null) {
                continue;
            }
            final Object y2 = GeneratedMessageLite.y(method4, y, new Object[0]);
            if (method5 == null) {
                if (b(y2)) {
                    booleanValue = false;
                }
            }
            else {
                booleanValue = (boolean)GeneratedMessageLite.y(method5, y, new Object[0]);
            }
            if (!booleanValue) {
                continue;
            }
            c(sb, n, a(string3), y2);
        }
        final j0 unknownFields = ((GeneratedMessageLite)y).unknownFields;
        if (unknownFields != null) {
            unknownFields.m(sb, n);
        }
    }
    
    public static String e(final y y, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(str);
        d(y, sb, 0);
        return sb.toString();
    }
}

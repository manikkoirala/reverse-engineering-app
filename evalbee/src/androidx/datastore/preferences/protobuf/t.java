// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class t
{
    public static final t a;
    public static final t b;
    
    static {
        a = new b(null);
        b = new c(null);
    }
    
    public static t a() {
        return t.a;
    }
    
    public static t b() {
        return t.b;
    }
    
    public abstract void c(final Object p0, final long p1);
    
    public abstract void d(final Object p0, final Object p1, final long p2);
    
    public abstract List e(final Object p0, final long p1);
    
    public static final class b extends t
    {
        public static final Class c;
        
        static {
            c = Collections.unmodifiableList(Collections.emptyList()).getClass();
        }
        
        public b() {
            super(null);
        }
        
        public static List f(final Object o, final long n) {
            return (List)e12.E(o, n);
        }
        
        public static List g(final Object o, final long n, final int initialCapacity) {
            final List f = f(o, n);
            List<E> list = null;
            Label_0080: {
                if (!f.isEmpty()) {
                    if (b.c.isAssignableFrom(f.getClass())) {
                        list = (List<E>)new ArrayList<Object>(f.size() + initialCapacity);
                        ((ArrayList<Object>)list).addAll(f);
                    }
                    else if (f instanceof y02) {
                        list = new s(f.size() + initialCapacity);
                        ((s)list).addAll(f);
                    }
                    else {
                        list = f;
                        if (!(f instanceof b81)) {
                            return list;
                        }
                        list = f;
                        if (!(f instanceof r.d)) {
                            return list;
                        }
                        final r.d d = (r.d)f;
                        list = f;
                        if (!d.h()) {
                            list = d.j(f.size() + initialCapacity);
                            break Label_0080;
                        }
                        return list;
                    }
                    e12.T(o, n, list);
                    return list;
                }
                if (f instanceof gj0) {
                    list = new s(initialCapacity);
                }
                else if (f instanceof b81 && f instanceof r.d) {
                    list = ((r.d)f).j(initialCapacity);
                }
                else {
                    list = (List<E>)new ArrayList<Object>(initialCapacity);
                }
            }
            e12.T(o, n, list);
            return list;
        }
        
        @Override
        public void c(final Object o, final long n) {
            final List list = (List)e12.E(o, n);
            List<Object> list2;
            if (list instanceof gj0) {
                list2 = ((gj0)list).d();
            }
            else {
                if (b.c.isAssignableFrom(((gj0)list).getClass())) {
                    return;
                }
                if (list instanceof b81 && list instanceof r.d) {
                    final r.d d = (r.d)list;
                    if (d.h()) {
                        d.e();
                    }
                    return;
                }
                list2 = Collections.unmodifiableList((List<?>)list);
            }
            e12.T(o, n, list2);
        }
        
        @Override
        public void d(final Object o, final Object o2, final long n) {
            List f = f(o2, n);
            final List g = g(o, n, f.size());
            final int size = g.size();
            final int size2 = f.size();
            if (size > 0 && size2 > 0) {
                g.addAll(f);
            }
            if (size > 0) {
                f = g;
            }
            e12.T(o, n, f);
        }
        
        @Override
        public List e(final Object o, final long n) {
            return g(o, n, 10);
        }
    }
    
    public static final class c extends t
    {
        public c() {
            super(null);
        }
        
        public static r.d f(final Object o, final long n) {
            return (r.d)e12.E(o, n);
        }
        
        @Override
        public void c(final Object o, final long n) {
            f(o, n).e();
        }
        
        @Override
        public void d(final Object o, final Object o2, final long n) {
            final r.d f = f(o, n);
            final r.d f2 = f(o2, n);
            final int size = f.size();
            final int size2 = f2.size();
            Object j = f;
            if (size > 0) {
                j = f;
                if (size2 > 0) {
                    j = f;
                    if (!f.h()) {
                        j = f.j(size2 + size);
                    }
                    ((List<Object>)j).addAll(f2);
                }
            }
            List<Object> list = f2;
            if (size > 0) {
                list = (List<Object>)j;
            }
            e12.T(o, n, list);
        }
        
        @Override
        public List e(final Object o, final long n) {
            Object o2;
            final r.d d = (r.d)(o2 = f(o, n));
            if (!d.h()) {
                final int size = d.size();
                int n2;
                if (size == 0) {
                    n2 = 10;
                }
                else {
                    n2 = size * 2;
                }
                o2 = d.j(n2);
                e12.T(o, n, o2);
            }
            return (List)o2;
        }
    }
}

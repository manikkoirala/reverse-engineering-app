// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public abstract class d
{
    public static int A(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final u u = (u)d;
        n2 = K(array, n2, a);
        while (true) {
            u.b(f.c(a.b));
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = K(array, h, a);
        }
        return n2;
    }
    
    public static int B(final byte[] bytes, int h, final a a) {
        h = H(bytes, h, a);
        final int a2 = a.a;
        if (a2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a2 == 0) {
            a.c = "";
            return h;
        }
        a.c = new String(bytes, h, a2, r.a);
        return h + a2;
    }
    
    public static int C(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        n2 = H(array, n2, a);
        int n4 = a.a;
        if (n4 >= 0) {
        Label_0025_Outer:
            while (true) {
                if (n4 != 0) {
                    final String s = new String(array, n2, n4, r.a);
                    break Label_0054;
                }
                while (true) {
                    d.add("");
                    Label_0069: {
                        break Label_0069;
                        final String s;
                        d.add(s);
                        n2 += n4;
                    }
                    if (n2 < n3) {
                        final int h = H(array, n2, a);
                        if (n == a.a) {
                            n2 = H(array, h, a);
                            n4 = a.a;
                            if (n4 < 0) {
                                throw InvalidProtocolBufferException.negativeSize();
                            }
                            if (n4 == 0) {
                                continue;
                            }
                            final String s = new String(array, n2, n4, r.a);
                            continue Label_0025_Outer;
                        }
                    }
                    break;
                }
                break;
            }
            return n2;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }
    
    public static int D(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        n2 = H(array, n2, a);
        final int a2 = a.a;
        if (a2 >= 0) {
        Label_0025_Outer:
            while (true) {
                if (a2 != 0) {
                    final int n4 = n2 + a2;
                    if (Utf8.t(array, n2, n4)) {
                        final String s = new String(array, n2, a2, r.a);
                        n2 = n4;
                        break Label_0073;
                    }
                    throw InvalidProtocolBufferException.invalidUtf8();
                }
                while (true) {
                    d.add("");
                    Label_0083: {
                        break Label_0083;
                        final String s;
                        d.add(s);
                    }
                    if (n2 < n3) {
                        final int h = H(array, n2, a);
                        if (n == a.a) {
                            n2 = H(array, h, a);
                            final int a3 = a.a;
                            if (a3 < 0) {
                                throw InvalidProtocolBufferException.negativeSize();
                            }
                            if (a3 == 0) {
                                continue;
                            }
                            final int n5 = n2 + a3;
                            if (Utf8.t(array, n2, n5)) {
                                final String s = new String(array, n2, a3, r.a);
                                n2 = n5;
                                continue Label_0025_Outer;
                            }
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                    }
                    break;
                }
                break;
            }
            return n2;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }
    
    public static int E(final byte[] array, int h, final a a) {
        h = H(array, h, a);
        final int a2 = a.a;
        if (a2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a2 == 0) {
            a.c = "";
            return h;
        }
        a.c = Utf8.h(array, h, a2);
        return h + a2;
    }
    
    public static int F(final int n, final byte[] array, int n2, int a, final j0 j0, final a a2) {
        if (WireFormat.a(n) == 0) {
            throw InvalidProtocolBufferException.invalidTag();
        }
        final int b = WireFormat.b(n);
        if (b == 0) {
            n2 = K(array, n2, a2);
            j0.n(n, a2.b);
            return n2;
        }
        if (b == 1) {
            j0.n(n, i(array, n2));
            return n2 + 8;
        }
        if (b != 2) {
            if (b != 3) {
                if (b == 5) {
                    j0.n(n, g(array, n2));
                    return n2 + 4;
                }
                throw InvalidProtocolBufferException.invalidTag();
            }
            else {
                final j0 l = j0.l();
                final int n3 = (n & 0xFFFFFFF8) | 0x4;
                int a3 = 0;
                int h;
                while (true) {
                    h = n2;
                    if (n2 >= a) {
                        break;
                    }
                    h = H(array, n2, a2);
                    a3 = a2.a;
                    if ((n2 = a3) == n3) {
                        a3 = n2;
                        break;
                    }
                    n2 = F(n2, array, h, a, l, a2);
                }
                if (h <= a && a3 == n3) {
                    j0.n(n, l);
                    return h;
                }
                throw InvalidProtocolBufferException.parseFailure();
            }
        }
        else {
            n2 = H(array, n2, a2);
            a = a2.a;
            if (a < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            if (a <= array.length - n2) {
                ByteString byteString;
                if (a == 0) {
                    byteString = ByteString.EMPTY;
                }
                else {
                    byteString = ByteString.copyFrom(array, n2, a);
                }
                j0.n(n, byteString);
                return n2 + a;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
    }
    
    public static int G(int n, final byte[] array, int n2, final a a) {
        n &= 0x7F;
        final int n3 = n2 + 1;
        n2 = array[n2];
        int n7 = 0;
        Label_0027: {
            if (n2 < 0) {
                int n4 = n | (n2 & 0x7F) << 7;
                n = n3 + 1;
                n2 = array[n3];
                if (n2 >= 0) {
                    n2 <<= 14;
                }
                else {
                    final int n5 = n4 | (n2 & 0x7F) << 14;
                    n2 = n + 1;
                    n = array[n];
                    if (n >= 0) {
                        final int n6 = n << 21;
                        n = n5;
                        n7 = n6;
                        break Label_0027;
                    }
                    n4 = (n5 | (n & 0x7F) << 21);
                    n = n2 + 1;
                    final byte b = array[n2];
                    if (b < 0) {
                        while (true) {
                            n2 = n + 1;
                            if (array[n] >= 0) {
                                break;
                            }
                            n = n2;
                        }
                        a.a = (n4 | (b & 0x7F) << 28);
                        return n2;
                    }
                    n2 = b << 28;
                }
                a.a = (n4 | n2);
                return n;
            }
            n7 = n2 << 7;
            n2 = n3;
        }
        a.a = (n | n7);
        return n2;
    }
    
    public static int H(final byte[] array, int a, final a a2) {
        final int n = a + 1;
        a = array[a];
        if (a >= 0) {
            a2.a = a;
            return n;
        }
        return G(a, array, n, a2);
    }
    
    public static int I(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final q q = (q)d;
        n2 = H(array, n2, a);
        while (true) {
            q.b(a.a);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = H(array, h, a);
        }
        return n2;
    }
    
    public static int J(long b, final byte[] array, int n, final a a) {
        final int n2 = n + 1;
        byte b2;
        int n3;
        for (b2 = array[n], b = ((b & 0x7FL) | (long)(b2 & 0x7F) << 7), n3 = 7, n = n2; b2 < 0; b2 = array[n], n3 += 7, b |= (long)(b2 & 0x7F) << n3, ++n) {}
        a.b = b;
        return n;
    }
    
    public static int K(final byte[] array, final int n, final a a) {
        final int n2 = n + 1;
        final long b = array[n];
        if (b >= 0L) {
            a.b = b;
            return n2;
        }
        return J(b, array, n2, a);
    }
    
    public static int L(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final u u = (u)d;
        n2 = K(array, n2, a);
        while (true) {
            u.b(a.b);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = K(array, h, a);
        }
        return n2;
    }
    
    public static int M(int a, final byte[] array, int m, final int n, final a a2) {
        if (WireFormat.a(a) == 0) {
            throw InvalidProtocolBufferException.invalidTag();
        }
        final int b = WireFormat.b(a);
        if (b == 0) {
            return K(array, m, a2);
        }
        if (b == 1) {
            return m + 8;
        }
        if (b == 2) {
            return H(array, m, a2) + a2.a;
        }
        if (b != 3) {
            if (b == 5) {
                return m + 4;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }
        else {
            final int n2 = (a & 0xFFFFFFF8) | 0x4;
            a = 0;
            int h;
            while (true) {
                h = m;
                if (m >= n) {
                    break;
                }
                h = H(array, m, a2);
                a = a2.a;
                if (a == n2) {
                    break;
                }
                m = M(a, array, h, n, a2);
            }
            if (h <= n && a == n2) {
                return h;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }
    }
    
    public static int a(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final e e = (e)d;
        final int n4 = n2 = K(array, n2, a);
    Label_0032_Outer:
        while (true) {
            if (a.b == 0L) {
                break Label_0038;
            }
            n2 = n4;
            while (true) {
                boolean b = true;
                Label_0041: {
                    break Label_0041;
                    b = false;
                }
                e.b(b);
                if (n2 < n3) {
                    final int h = H(array, n2, a);
                    if (n == a.a) {
                        final int n5 = n2 = K(array, h, a);
                        if (a.b != 0L) {
                            n2 = n5;
                            continue;
                        }
                        continue Label_0032_Outer;
                    }
                }
                break;
            }
            break;
        }
        return n2;
    }
    
    public static int b(final byte[] array, int h, final a a) {
        h = H(array, h, a);
        final int a2 = a.a;
        if (a2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a2 > array.length - h) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        if (a2 == 0) {
            a.c = ByteString.EMPTY;
            return h;
        }
        a.c = ByteString.copyFrom(array, h, a2);
        return h + a2;
    }
    
    public static int c(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final int h = H(array, n2, a);
        final int a2 = a.a;
        if (a2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a2 <= array.length - h) {
            n2 = a2;
            int n4 = h;
        Label_0046_Outer:
            while (true) {
                if (a2 != 0) {
                    break Label_0060;
                }
                n2 = h;
                while (true) {
                    d.add(ByteString.EMPTY);
                    Label_0080: {
                        break Label_0080;
                        d.add(ByteString.copyFrom(array, n4, n2));
                        n2 += n4;
                    }
                    if (n2 < n3) {
                        final int h2 = H(array, n2, a);
                        if (n == a.a) {
                            final int h3 = H(array, h2, a);
                            final int a3 = a.a;
                            if (a3 < 0) {
                                throw InvalidProtocolBufferException.negativeSize();
                            }
                            if (a3 > array.length - h3) {
                                throw InvalidProtocolBufferException.truncatedMessage();
                            }
                            n2 = a3;
                            n4 = h3;
                            if (a3 == 0) {
                                n2 = h3;
                                continue;
                            }
                            continue Label_0046_Outer;
                        }
                    }
                    break;
                }
                break;
            }
            return n2;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static double d(final byte[] array, final int n) {
        return Double.longBitsToDouble(i(array, n));
    }
    
    public static int e(final int n, final byte[] array, int i, final int n2, final r.d d, final a a) {
        final i j = (i)d;
        j.b(d(array, i));
        int h;
        for (i += 8; i < n2; i = h + 8) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            j.b(d(array, h));
        }
        return i;
    }
    
    public static int f(final int n, final byte[] array, final int n2, final int n3, final Object o, final y y, final i0 i0, final a a) {
        a.d.a(y, n >>> 3);
        return F(n, array, n2, n3, a0.v(o), a);
    }
    
    public static int g(final byte[] array, final int n) {
        return (array[n + 3] & 0xFF) << 24 | ((array[n] & 0xFF) | (array[n + 1] & 0xFF) << 8 | (array[n + 2] & 0xFF) << 16);
    }
    
    public static int h(final int n, final byte[] array, int i, final int n2, final r.d d, final a a) {
        final q q = (q)d;
        q.b(g(array, i));
        int h;
        for (i += 4; i < n2; i = h + 4) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            q.b(g(array, h));
        }
        return i;
    }
    
    public static long i(final byte[] array, final int n) {
        return ((long)array[n + 7] & 0xFFL) << 56 | (((long)array[n] & 0xFFL) | ((long)array[n + 1] & 0xFFL) << 8 | ((long)array[n + 2] & 0xFFL) << 16 | ((long)array[n + 3] & 0xFFL) << 24 | ((long)array[n + 4] & 0xFFL) << 32 | ((long)array[n + 5] & 0xFFL) << 40 | ((long)array[n + 6] & 0xFFL) << 48);
    }
    
    public static int j(final int n, final byte[] array, int i, final int n2, final r.d d, final a a) {
        final u u = (u)d;
        u.b(i(array, i));
        int h;
        for (i += 8; i < n2; i = h + 8) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            u.b(i(array, h));
        }
        return i;
    }
    
    public static float k(final byte[] array, final int n) {
        return Float.intBitsToFloat(g(array, n));
    }
    
    public static int l(final int n, final byte[] array, int i, final int n2, final r.d d, final a a) {
        final o o = (o)d;
        o.b(k(array, i));
        int h;
        for (i += 4; i < n2; i = h + 4) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            o.b(k(array, h));
        }
        return i;
    }
    
    public static int m(final f0 f0, final byte[] array, int c0, final int n, final int n2, final a a) {
        final a0 a2 = (a0)f0;
        final Object instance = a2.newInstance();
        c0 = a2.c0(instance, array, c0, n, n2, a);
        a2.d(instance);
        a.c = instance;
        return c0;
    }
    
    public static int n(final f0 f0, final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final int n4 = (n & 0xFFFFFFF8) | 0x4;
        n2 = m(f0, array, n2, n3, n4, a);
        while (true) {
            d.add(a.c);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = m(f0, array, h, n3, n4, a);
        }
        return n2;
    }
    
    public static int o(final f0 f0, final byte[] array, int g, int n, final a a) {
        final int n2 = g + 1;
        final byte b = array[g];
        g = n2;
        int a2 = b;
        if (b < 0) {
            g = G(b, array, n2, a);
            a2 = a.a;
        }
        if (a2 >= 0 && a2 <= n - g) {
            final Object instance = f0.newInstance();
            n = a2 + g;
            f0.i(instance, array, g, n, a);
            f0.d(instance);
            a.c = instance;
            return n;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int p(final f0 f0, final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        n2 = o(f0, array, n2, n3, a);
        while (true) {
            d.add(a.c);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = o(f0, array, h, n3, a);
        }
        return n2;
    }
    
    public static int q(final byte[] array, int i, final r.d d, final a a) {
        final e e = (e)d;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = K(array, i, a);
            e.b(a.b != 0L);
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int r(final byte[] array, int i, final r.d d, final a a) {
        final i j = (i)d;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 8) {
            j.b(d(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int s(final byte[] array, int i, final r.d d, final a a) {
        final q q = (q)d;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 4) {
            q.b(g(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int t(final byte[] array, int i, final r.d d, final a a) {
        final u u = (u)d;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 8) {
            u.b(i(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int u(final byte[] array, int i, final r.d d, final a a) {
        final o o = (o)d;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 4) {
            o.b(k(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int v(final byte[] array, int i, final r.d d, final a a) {
        final q q = (q)d;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = H(array, i, a);
            q.b(f.b(a.a));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int w(final byte[] array, int i, final r.d d, final a a) {
        final u u = (u)d;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = K(array, i, a);
            u.b(f.c(a.b));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int x(final byte[] array, int i, final r.d d, final a a) {
        final q q = (q)d;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = H(array, i, a);
            q.b(a.a);
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int y(final byte[] array, int i, final r.d d, final a a) {
        final u u = (u)d;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = K(array, i, a);
            u.b(a.b);
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int z(final int n, final byte[] array, int n2, final int n3, final r.d d, final a a) {
        final q q = (q)d;
        n2 = H(array, n2, a);
        while (true) {
            q.b(f.b(a.a));
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = H(array, h, a);
        }
        return n2;
    }
    
    public static final class a
    {
        public int a;
        public long b;
        public Object c;
        public final k d;
        
        public a(final k d) {
            d.getClass();
            this.d = d;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Map;

public abstract class l
{
    public abstract int a(final Map.Entry p0);
    
    public abstract Object b(final k p0, final y p1, final int p2);
    
    public abstract n c(final Object p0);
    
    public abstract n d(final Object p0);
    
    public abstract boolean e(final y p0);
    
    public abstract void f(final Object p0);
    
    public abstract Object g(final e0 p0, final Object p1, final k p2, final n p3, final Object p4, final i0 p5);
    
    public abstract void h(final e0 p0, final Object p1, final k p2, final n p3);
    
    public abstract void i(final ByteString p0, final Object p1, final k p2, final n p3);
    
    public abstract void j(final Writer p0, final Map.Entry p1);
}

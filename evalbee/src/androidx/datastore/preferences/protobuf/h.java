// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

public final class h implements Writer
{
    public final CodedOutputStream a;
    
    public h(CodedOutputStream a) {
        a = (CodedOutputStream)r.b(a, "output");
        this.a = a;
        a.a = this;
    }
    
    public static h P(final CodedOutputStream codedOutputStream) {
        final h a = codedOutputStream.a;
        if (a != null) {
            return a;
        }
        return new h(codedOutputStream);
    }
    
    @Override
    public void A(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.K(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.L0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.K0(i, (long)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public FieldOrder B() {
        return FieldOrder.ASCENDING;
    }
    
    @Override
    public void C(final int n, final long n2) {
        this.a.C0(n, n2);
    }
    
    @Override
    public void D(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            int k = 0;
            i = 0;
            while (k < list.size()) {
                i += CodedOutputStream.I((int)list.get(k));
                ++k;
            }
            this.a.U0(i);
            for (i = n; i < list.size(); ++i) {
                this.a.J0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.I0(i, (int)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void E(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.f(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.h0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.g0(i, (boolean)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void F(final int n, final float n2) {
        this.a.u0(n, n2);
    }
    
    @Override
    public void G(final int n, final int n2) {
        this.a.o0(n, n2);
    }
    
    @Override
    public void H(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.z(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.D0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.C0(i, (long)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void I(final int n, final int n2) {
        this.a.M0(n, n2);
    }
    
    @Override
    public void J(final int n, final ByteString byteString) {
        this.a.k0(n, byteString);
    }
    
    @Override
    public void K(final int n, final Object o, final f0 f0) {
        this.a.E0(n, (y)o, f0);
    }
    
    @Override
    public void L(final int n, final v.a a, final Map map) {
        if (this.a.a0()) {
            this.T(n, a, map);
            return;
        }
        for (final Map.Entry<Object, V> entry : map.entrySet()) {
            this.a.S0(n, 2);
            this.a.U0(v.b(a, entry.getKey(), entry.getValue()));
            v.e(this.a, a, entry.getKey(), entry.getValue());
        }
    }
    
    @Override
    public void M(final int n, final List list, final f0 f0) {
        for (int i = 0; i < list.size(); ++i) {
            this.N(n, list.get(i), f0);
        }
    }
    
    @Override
    public void N(final int n, final Object o, final f0 f0) {
        this.a.x0(n, (y)o, f0);
    }
    
    @Override
    public void O(final int n, final List list, final f0 f0) {
        for (int i = 0; i < list.size(); ++i) {
            this.K(n, list.get(i), f0);
        }
    }
    
    public final void Q(final int n, final boolean b, final Object o, final v.a a) {
        this.a.S0(n, 2);
        this.a.U0(v.b(a, b, o));
        v.e(this.a, a, b, o);
    }
    
    public final void R(final int n, final v.a a, final Map map) {
        final int size = map.size();
        final int[] a2 = new int[size];
        final Iterator iterator = map.keySet().iterator();
        final int n2 = 0;
        int n3 = 0;
        while (iterator.hasNext()) {
            a2[n3] = (int)iterator.next();
            ++n3;
        }
        Arrays.sort(a2);
        for (int i = n2; i < size; ++i) {
            final int j = a2[i];
            final Object value = map.get(j);
            this.a.S0(n, 2);
            this.a.U0(v.b(a, j, value));
            v.e(this.a, a, j, value);
        }
    }
    
    public final void S(final int n, final v.a a, final Map map) {
        final int size = map.size();
        final long[] a2 = new long[size];
        final Iterator iterator = map.keySet().iterator();
        final int n2 = 0;
        int n3 = 0;
        while (iterator.hasNext()) {
            a2[n3] = (long)iterator.next();
            ++n3;
        }
        Arrays.sort(a2);
        for (int i = n2; i < size; ++i) {
            final long l = a2[i];
            final Object value = map.get(l);
            this.a.S0(n, 2);
            this.a.U0(v.b(a, l, value));
            v.e(this.a, a, l, value);
        }
    }
    
    public final void T(final int n, final v.a a, final Map map) {
        switch (h$a.a[a.a.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("does not support key type: ");
                sb.append(a.a);
                throw new IllegalArgumentException(sb.toString());
            }
            case 12: {
                this.U(n, a, map);
                break;
            }
            case 7:
            case 8:
            case 9:
            case 10:
            case 11: {
                this.S(n, a, map);
                break;
            }
            case 2:
            case 3:
            case 4:
            case 5:
            case 6: {
                this.R(n, a, map);
                break;
            }
            case 1: {
                final Object value = map.get(Boolean.FALSE);
                if (value != null) {
                    this.Q(n, false, value, a);
                }
                final Object value2 = map.get(Boolean.TRUE);
                if (value2 != null) {
                    this.Q(n, true, value2, a);
                    break;
                }
                break;
            }
        }
    }
    
    public final void U(final int n, final v.a a, final Map map) {
        final int size = map.size();
        final String[] a2 = new String[size];
        final Iterator iterator = map.keySet().iterator();
        final int n2 = 0;
        int n3 = 0;
        while (iterator.hasNext()) {
            a2[n3] = (String)iterator.next();
            ++n3;
        }
        Arrays.sort(a2);
        for (int i = n2; i < size; ++i) {
            final String s = a2[i];
            final Object value = map.get(s);
            this.a.S0(n, 2);
            this.a.U0(v.b(a, s, value));
            v.e(this.a, a, s, value);
        }
    }
    
    public final void V(final int n, final Object o) {
        if (o instanceof String) {
            this.a.Q0(n, (String)o);
        }
        else {
            this.a.k0(n, (ByteString)o);
        }
    }
    
    @Override
    public void a(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.s(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.v0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.u0(i, (float)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public final void b(final int n, final Object o) {
        if (o instanceof ByteString) {
            this.a.H0(n, (ByteString)o);
        }
        else {
            this.a.G0(n, (y)o);
        }
    }
    
    @Override
    public void c(final int n, final int n2) {
        this.a.q0(n, n2);
    }
    
    @Override
    public void d(final int n, final String s) {
        this.a.Q0(n, s);
    }
    
    @Override
    public void e(final int n, final long n2) {
        this.a.V0(n, n2);
    }
    
    @Override
    public void f(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            int k = 0;
            i = 0;
            while (k < list.size()) {
                i += CodedOutputStream.x((int)list.get(k));
                ++k;
            }
            this.a.U0(i);
            for (i = n; i < list.size(); ++i) {
                this.a.B0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.A0(i, (int)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void g(final int n, final int n2) {
        this.a.A0(n, n2);
    }
    
    @Override
    public void h(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.o(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.r0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.q0(i, (int)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void i(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.T(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.U0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.T0(i, (int)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void j(final int n, final long n2) {
        this.a.O0(n, n2);
    }
    
    @Override
    public void k(final int n, final int n2) {
        this.a.T0(n, n2);
    }
    
    @Override
    public void l(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            int k = 0;
            i = 0;
            while (k < list.size()) {
                i += CodedOutputStream.V((long)list.get(k));
                ++k;
            }
            this.a.U0(i);
            for (i = n; i < list.size(); ++i) {
                this.a.W0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.V0(i, (long)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void m(final int n, final long n2) {
        this.a.s0(n, n2);
    }
    
    @Override
    public void n(final int n, final boolean b) {
        this.a.g0(n, b);
    }
    
    @Override
    public void o(final int n, final int n2) {
        this.a.I0(n, n2);
    }
    
    @Override
    public void p(final int n) {
        this.a.S0(n, 3);
    }
    
    @Override
    public void q(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.q(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.t0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.s0(i, (long)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void r(final int n) {
        this.a.S0(n, 4);
    }
    
    @Override
    public void s(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            int k = 0;
            i = 0;
            while (k < list.size()) {
                i += CodedOutputStream.M((int)list.get(k));
                ++k;
            }
            this.a.U0(i);
            for (i = n; i < list.size(); ++i) {
                this.a.N0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.M0(i, (int)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void t(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.k(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.n0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.m0(i, (double)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void u(final int n, final List list) {
        for (int i = 0; i < list.size(); ++i) {
            this.a.k0(n, (ByteString)list.get(i));
        }
    }
    
    @Override
    public void v(final int n, final List list) {
        final boolean b = list instanceof gj0;
        int i = 0;
        final int n2 = 0;
        if (b) {
            final gj0 gj0 = (gj0)list;
            for (int j = n2; j < list.size(); ++j) {
                this.V(n, gj0.k(j));
            }
        }
        else {
            while (i < list.size()) {
                this.a.Q0(n, (String)list.get(i));
                ++i;
            }
        }
    }
    
    @Override
    public void w(final int n, final long n2) {
        this.a.K0(n, n2);
    }
    
    @Override
    public void x(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            i = 0;
            int n2 = 0;
            while (i < list.size()) {
                n2 += CodedOutputStream.O(list.get(i));
                ++i;
            }
            this.a.U0(n2);
            for (i = n; i < list.size(); ++i) {
                this.a.P0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.O0(i, (long)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void y(int i, final List list, final boolean b) {
        int j = 0;
        final int n = 0;
        if (b) {
            this.a.S0(i, 2);
            int k = 0;
            i = 0;
            while (k < list.size()) {
                i += CodedOutputStream.m((int)list.get(k));
                ++k;
            }
            this.a.U0(i);
            for (i = n; i < list.size(); ++i) {
                this.a.p0(list.get(i));
            }
        }
        else {
            while (j < list.size()) {
                this.a.o0(i, (int)list.get(j));
                ++j;
            }
        }
    }
    
    @Override
    public void z(final int n, final double n2) {
        this.a.m0(n, n2);
    }
}

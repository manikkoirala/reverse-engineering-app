// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public class k0 extends i0
{
    public j0 A(final Object o) {
        return ((GeneratedMessageLite)o).unknownFields;
    }
    
    public int B(final j0 j0) {
        return j0.f();
    }
    
    public int C(final j0 j0) {
        return j0.g();
    }
    
    public j0 D(j0 k, final j0 j0) {
        if (!j0.equals(j0.e())) {
            k = j0.k(k, j0);
        }
        return k;
    }
    
    public j0 E() {
        return j0.l();
    }
    
    public void F(final Object o, final j0 j0) {
        this.G(o, j0);
    }
    
    public void G(final Object o, final j0 unknownFields) {
        ((GeneratedMessageLite)o).unknownFields = unknownFields;
    }
    
    public j0 H(final j0 j0) {
        j0.j();
        return j0;
    }
    
    public void I(final j0 j0, final Writer writer) {
        j0.o(writer);
    }
    
    public void J(final j0 j0, final Writer writer) {
        j0.q(writer);
    }
    
    @Override
    public void j(final Object o) {
        this.A(o).j();
    }
    
    @Override
    public boolean q(final e0 e0) {
        return false;
    }
    
    public void u(final j0 j0, final int n, final int i) {
        j0.n(WireFormat.c(n, 5), i);
    }
    
    public void v(final j0 j0, final int n, final long l) {
        j0.n(WireFormat.c(n, 1), l);
    }
    
    public void w(final j0 j0, final int n, final j0 j2) {
        j0.n(WireFormat.c(n, 3), j2);
    }
    
    public void x(final j0 j0, final int n, final ByteString byteString) {
        j0.n(WireFormat.c(n, 2), byteString);
    }
    
    public void y(final j0 j0, final int n, final long l) {
        j0.n(WireFormat.c(n, 0), l);
    }
    
    public j0 z(final Object o) {
        j0 j0;
        if ((j0 = this.A(o)) == androidx.datastore.preferences.protobuf.j0.e()) {
            j0 = androidx.datastore.preferences.protobuf.j0.l();
            this.G(o, j0);
        }
        return j0;
    }
}

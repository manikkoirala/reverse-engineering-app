// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public class p implements wv0
{
    public static final p a;
    
    static {
        a = new p();
    }
    
    public static p c() {
        return p.a;
    }
    
    @Override
    public uv0 a(final Class clazz) {
        if (GeneratedMessageLite.class.isAssignableFrom(clazz)) {
            try {
                return (uv0)GeneratedMessageLite.w(clazz.asSubclass(GeneratedMessageLite.class)).p();
            }
            catch (final Exception cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to get message info for ");
                sb.append(clazz.getName());
                throw new RuntimeException(sb.toString(), cause);
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unsupported message type: ");
        sb2.append(clazz.getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    @Override
    public boolean b(final Class clazz) {
        return GeneratedMessageLite.class.isAssignableFrom(clazz);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import java.util.List;

public final class n
{
    public static final n d;
    public final h0 a;
    public boolean b;
    public boolean c;
    
    static {
        d = new n(true);
    }
    
    public n() {
        this.a = h0.q(16);
    }
    
    public n(final h0 a) {
        this.a = a;
        this.o();
    }
    
    public n(final boolean b) {
        this(h0.q(0));
        this.o();
    }
    
    public static int b(final WireFormat.FieldType fieldType, int r, final Object o) {
        final int n = r = CodedOutputStream.R(r);
        if (fieldType == WireFormat.FieldType.GROUP) {
            r = n * 2;
        }
        return r + c(fieldType, o);
    }
    
    public static int c(final WireFormat.FieldType fieldType, final Object o) {
        switch (n$a.b[fieldType.ordinal()]) {
            default: {
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
            }
            case 18: {
                if (o instanceof r.a) {
                    return CodedOutputStream.m(((r.a)o).getNumber());
                }
                return CodedOutputStream.m((int)o);
            }
            case 17: {
                return CodedOutputStream.O((long)o);
            }
            case 16: {
                return CodedOutputStream.M((int)o);
            }
            case 15: {
                return CodedOutputStream.K((long)o);
            }
            case 14: {
                return CodedOutputStream.I((int)o);
            }
            case 13: {
                return CodedOutputStream.T((int)o);
            }
            case 12: {
                if (o instanceof ByteString) {
                    return CodedOutputStream.i((ByteString)o);
                }
                return CodedOutputStream.g((byte[])o);
            }
            case 11: {
                if (o instanceof ByteString) {
                    return CodedOutputStream.i((ByteString)o);
                }
                return CodedOutputStream.Q((String)o);
            }
            case 10: {
                return CodedOutputStream.C((y)o);
            }
            case 9: {
                return CodedOutputStream.u((y)o);
            }
            case 8: {
                return CodedOutputStream.f((boolean)o);
            }
            case 7: {
                return CodedOutputStream.o((int)o);
            }
            case 6: {
                return CodedOutputStream.q((long)o);
            }
            case 5: {
                return CodedOutputStream.x((int)o);
            }
            case 4: {
                return CodedOutputStream.V((long)o);
            }
            case 3: {
                return CodedOutputStream.z((long)o);
            }
            case 2: {
                return CodedOutputStream.s((float)o);
            }
            case 1: {
                return CodedOutputStream.k((double)o);
            }
        }
    }
    
    public static int d(final b b, final Object o) {
        final WireFormat.FieldType b2 = b.b();
        final int number = b.getNumber();
        if (!b.i()) {
            return b(b2, number, o);
        }
        final boolean packed = b.isPacked();
        int n = 0;
        final int n2 = 0;
        final List list = (List)o;
        if (packed) {
            final Iterator iterator = list.iterator();
            int n3 = n2;
            while (iterator.hasNext()) {
                n3 += c(b2, iterator.next());
            }
            return CodedOutputStream.R(number) + n3 + CodedOutputStream.G(n3);
        }
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            n += b(b2, number, iterator2.next());
        }
        return n;
    }
    
    public static int i(final WireFormat.FieldType fieldType, final boolean b) {
        if (b) {
            return 2;
        }
        return fieldType.getWireType();
    }
    
    public static boolean l(final Map.Entry entry) {
        zu0.a(entry.getKey());
        throw null;
    }
    
    public static boolean m(final WireFormat.FieldType fieldType, final Object o) {
        r.a(o);
        final int n = n$a.a[fieldType.getJavaType().ordinal()];
        final boolean b = true;
        final boolean b2 = true;
        boolean b3 = true;
        switch (n) {
            default: {
                return false;
            }
            case 9: {
                if (!(o instanceof y)) {
                    b3 = false;
                }
                return b3;
            }
            case 8: {
                boolean b4 = b;
                if (!(o instanceof Integer)) {
                    b4 = (o instanceof r.a && b);
                }
                return b4;
            }
            case 7: {
                boolean b5 = b2;
                if (!(o instanceof ByteString)) {
                    b5 = (o instanceof byte[] && b2);
                }
                return b5;
            }
            case 6: {
                return o instanceof String;
            }
            case 5: {
                return o instanceof Boolean;
            }
            case 4: {
                return o instanceof Double;
            }
            case 3: {
                return o instanceof Float;
            }
            case 2: {
                return o instanceof Long;
            }
            case 1: {
                return o instanceof Integer;
            }
        }
    }
    
    public static n r() {
        return new n();
    }
    
    public static void u(final CodedOutputStream codedOutputStream, final WireFormat.FieldType fieldType, final int n, final Object o) {
        if (fieldType == WireFormat.FieldType.GROUP) {
            codedOutputStream.w0(n, (y)o);
        }
        else {
            codedOutputStream.S0(n, i(fieldType, false));
            v(codedOutputStream, fieldType, o);
        }
    }
    
    public static void v(final CodedOutputStream codedOutputStream, final WireFormat.FieldType fieldType, final Object o) {
        switch (n$a.b[fieldType.ordinal()]) {
            default: {
                return;
            }
            case 18: {
                int n;
                if (o instanceof r.a) {
                    n = ((r.a)o).getNumber();
                }
                else {
                    n = (int)o;
                }
                codedOutputStream.p0(n);
                return;
            }
            case 17: {
                codedOutputStream.P0((long)o);
                return;
            }
            case 16: {
                codedOutputStream.N0((int)o);
                return;
            }
            case 15: {
                codedOutputStream.L0((long)o);
                return;
            }
            case 14: {
                codedOutputStream.J0((int)o);
                return;
            }
            case 13: {
                codedOutputStream.U0((int)o);
                return;
            }
            case 12: {
                if (o instanceof ByteString) {
                    break;
                }
                codedOutputStream.i0((byte[])o);
                return;
            }
            case 11: {
                if (o instanceof ByteString) {
                    break;
                }
                codedOutputStream.R0((String)o);
                return;
            }
            case 10: {
                codedOutputStream.F0((y)o);
                return;
            }
            case 9: {
                codedOutputStream.y0((y)o);
                return;
            }
            case 8: {
                codedOutputStream.h0((boolean)o);
                return;
            }
            case 7: {
                codedOutputStream.r0((int)o);
                return;
            }
            case 6: {
                codedOutputStream.t0((long)o);
                return;
            }
            case 5: {
                codedOutputStream.B0((int)o);
                return;
            }
            case 4: {
                codedOutputStream.W0((long)o);
                return;
            }
            case 3: {
                codedOutputStream.D0((long)o);
                return;
            }
            case 2: {
                codedOutputStream.v0((float)o);
                return;
            }
            case 1: {
                codedOutputStream.n0((double)o);
                return;
            }
        }
        codedOutputStream.l0((ByteString)o);
    }
    
    public n a() {
        final n r = r();
        for (int i = 0; i < this.a.k(); ++i) {
            final Map.Entry j = this.a.j(i);
            zu0.a(j.getKey());
            r.s(null, j.getValue());
        }
        for (final Map.Entry<Object, V> entry : this.a.m()) {
            zu0.a(entry.getKey());
            r.s(null, entry.getValue());
        }
        r.c = this.c;
        return r;
    }
    
    public Iterator e() {
        if (this.c) {
            return new cj0(this.a.h().iterator());
        }
        return this.a.h().iterator();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof n && this.a.equals(((n)o).a));
    }
    
    public int f() {
        int i = 0;
        int n = 0;
        while (i < this.a.k()) {
            n += this.g(this.a.j(i));
            ++i;
        }
        final Iterator iterator = this.a.m().iterator();
        while (iterator.hasNext()) {
            n += this.g((Map.Entry)iterator.next());
        }
        return n;
    }
    
    public final int g(final Map.Entry entry) {
        zu0.a(entry.getKey());
        entry.getValue();
        throw null;
    }
    
    public int h() {
        int i = 0;
        int n = 0;
        while (i < this.a.k()) {
            final Map.Entry j = this.a.j(i);
            zu0.a(j.getKey());
            n += d(null, j.getValue());
            ++i;
        }
        for (final Map.Entry<Object, V> entry : this.a.m()) {
            zu0.a(entry.getKey());
            n += d(null, entry.getValue());
        }
        return n;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    public boolean j() {
        return this.a.isEmpty();
    }
    
    public boolean k() {
        for (int i = 0; i < this.a.k(); ++i) {
            if (!l(this.a.j(i))) {
                return false;
            }
        }
        final Iterator iterator = this.a.m().iterator();
        while (iterator.hasNext()) {
            if (!l((Map.Entry)iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public Iterator n() {
        if (this.c) {
            return new cj0(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }
    
    public void o() {
        if (this.b) {
            return;
        }
        this.a.p();
        this.b = true;
    }
    
    public void p(final n n) {
        for (int i = 0; i < n.a.k(); ++i) {
            this.q(n.a.j(i));
        }
        final Iterator iterator = n.a.m().iterator();
        while (iterator.hasNext()) {
            this.q((Map.Entry)iterator.next());
        }
    }
    
    public final void q(final Map.Entry entry) {
        zu0.a(entry.getKey());
        entry.getValue();
        throw null;
    }
    
    public void s(final b b, Object o) {
        if (b.i()) {
            if (!(o instanceof List)) {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
            final ArrayList list = new ArrayList();
            list.addAll((Collection)o);
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                this.t(b.b(), iterator.next());
            }
            o = list;
        }
        else {
            this.t(b.b(), o);
        }
        this.a.r(b, o);
    }
    
    public final void t(final WireFormat.FieldType fieldType, final Object o) {
        if (m(fieldType, o)) {
            return;
        }
        throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
    }
    
    public interface b extends Comparable
    {
        WireFormat.FieldType b();
        
        int getNumber();
        
        boolean i();
        
        boolean isPacked();
    }
}

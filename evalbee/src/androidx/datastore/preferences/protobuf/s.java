// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

public class s extends c implements gj0, RandomAccess
{
    public static final s c;
    public static final gj0 d;
    public final List b;
    
    static {
        final s s = new s();
        (c = s).e();
        d = s;
    }
    
    public s() {
        this(10);
    }
    
    public s(final int initialCapacity) {
        this(new ArrayList(initialCapacity));
    }
    
    public s(final ArrayList b) {
        this.b = b;
    }
    
    public static String c(final Object o) {
        if (o instanceof String) {
            return (String)o;
        }
        if (o instanceof ByteString) {
            return ((ByteString)o).toStringUtf8();
        }
        return r.j((byte[])o);
    }
    
    @Override
    public boolean addAll(final int n, final Collection collection) {
        this.a();
        List f = (List)collection;
        if (collection instanceof gj0) {
            f = ((gj0)collection).f();
        }
        final boolean addAll = this.b.addAll(n, f);
        ++super.modCount;
        return addAll;
    }
    
    @Override
    public boolean addAll(final Collection collection) {
        return this.addAll(this.size(), collection);
    }
    
    public void b(final int n, final String s) {
        this.a();
        this.b.add(n, s);
        ++super.modCount;
    }
    
    @Override
    public void clear() {
        this.a();
        this.b.clear();
        ++super.modCount;
    }
    
    @Override
    public gj0 d() {
        if (this.h()) {
            return new y02(this);
        }
        return this;
    }
    
    @Override
    public List f() {
        return Collections.unmodifiableList((List<?>)this.b);
    }
    
    public String g(final int n) {
        final byte[] value = this.b.get(n);
        if (value instanceof String) {
            return (String)(Object)value;
        }
        if (value instanceof ByteString) {
            final ByteString byteString = (Object)value;
            final String stringUtf8 = byteString.toStringUtf8();
            if (byteString.isValidUtf8()) {
                this.b.set(n, stringUtf8);
            }
            return stringUtf8;
        }
        final byte[] array = value;
        final String j = r.j(array);
        if (r.g(array)) {
            this.b.set(n, j);
        }
        return j;
    }
    
    public s i(final int initialCapacity) {
        if (initialCapacity >= this.size()) {
            final ArrayList list = new ArrayList(initialCapacity);
            list.addAll(this.b);
            return new s(list);
        }
        throw new IllegalArgumentException();
    }
    
    @Override
    public Object k(final int n) {
        return this.b.get(n);
    }
    
    public String l(final int n) {
        this.a();
        final Object remove = this.b.remove(n);
        ++super.modCount;
        return c(remove);
    }
    
    public String m(final int n, final String s) {
        this.a();
        return c(this.b.set(n, s));
    }
    
    @Override
    public int size() {
        return this.b.size();
    }
    
    @Override
    public void x(final ByteString byteString) {
        this.a();
        this.b.add(byteString);
        ++super.modCount;
    }
}

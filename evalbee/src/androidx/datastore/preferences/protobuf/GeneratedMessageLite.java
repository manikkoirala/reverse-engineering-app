// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.lang.reflect.Field;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public abstract class GeneratedMessageLite extends androidx.datastore.preferences.protobuf.a
{
    private static Map<Object, GeneratedMessageLite> defaultInstanceMap;
    protected int memoizedSerializedSize;
    protected j0 unknownFields;
    
    static {
        GeneratedMessageLite.defaultInstanceMap = new ConcurrentHashMap<Object, GeneratedMessageLite>();
    }
    
    public GeneratedMessageLite() {
        this.unknownFields = j0.e();
        this.memoizedSerializedSize = -1;
    }
    
    public static final boolean A(final GeneratedMessageLite generatedMessageLite, final boolean b) {
        final byte byteValue = (byte)generatedMessageLite.s(MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED);
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        final boolean b2 = l91.a().d(generatedMessageLite).b(generatedMessageLite);
        if (b) {
            final MethodToInvoke set_MEMOIZED_IS_INITIALIZED = MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED;
            GeneratedMessageLite generatedMessageLite2;
            if (b2) {
                generatedMessageLite2 = generatedMessageLite;
            }
            else {
                generatedMessageLite2 = null;
            }
            generatedMessageLite.t(set_MEMOIZED_IS_INITIALIZED, generatedMessageLite2);
        }
        return b2;
    }
    
    public static r.d C(final r.d d) {
        final int size = d.size();
        int n;
        if (size == 0) {
            n = 10;
        }
        else {
            n = size * 2;
        }
        return d.j(n);
    }
    
    public static Object E(final y y, final String s, final Object[] array) {
        return new lc1(y, s, array);
    }
    
    public static GeneratedMessageLite F(final GeneratedMessageLite generatedMessageLite, final InputStream inputStream) {
        return q(G(generatedMessageLite, f.f(inputStream), k.b()));
    }
    
    public static GeneratedMessageLite G(GeneratedMessageLite unfinishedMessage, final f f, final k k) {
        unfinishedMessage = (GeneratedMessageLite)unfinishedMessage.s(MethodToInvoke.NEW_MUTABLE_INSTANCE);
        try {
            final f0 d = l91.a().d(unfinishedMessage);
            d.h(unfinishedMessage, g.N(f), k);
            d.d(unfinishedMessage);
            return unfinishedMessage;
        }
        catch (final RuntimeException ex) {
            if (ex.getCause() instanceof InvalidProtocolBufferException) {
                throw (InvalidProtocolBufferException)ex.getCause();
            }
            throw ex;
        }
        catch (final IOException ex2) {
            if (ex2.getCause() instanceof InvalidProtocolBufferException) {
                throw (InvalidProtocolBufferException)ex2.getCause();
            }
            throw new InvalidProtocolBufferException(ex2.getMessage()).setUnfinishedMessage(unfinishedMessage);
        }
    }
    
    public static void H(final Class clazz, final GeneratedMessageLite generatedMessageLite) {
        GeneratedMessageLite.defaultInstanceMap.put(clazz, generatedMessageLite);
    }
    
    public static GeneratedMessageLite q(final GeneratedMessageLite unfinishedMessage) {
        if (unfinishedMessage != null && !unfinishedMessage.z()) {
            throw unfinishedMessage.l().asInvalidProtocolBufferException().setUnfinishedMessage(unfinishedMessage);
        }
        return unfinishedMessage;
    }
    
    public static r.d v() {
        return d0.c();
    }
    
    public static GeneratedMessageLite w(final Class clazz) {
        GeneratedMessageLite generatedMessageLite;
        if ((generatedMessageLite = GeneratedMessageLite.defaultInstanceMap.get(clazz)) == null) {
            try {
                Class.forName(clazz.getName(), true, clazz.getClassLoader());
                generatedMessageLite = GeneratedMessageLite.defaultInstanceMap.get(clazz);
            }
            catch (final ClassNotFoundException cause) {
                throw new IllegalStateException("Class initialization cannot fail.", cause);
            }
        }
        GeneratedMessageLite x;
        if ((x = generatedMessageLite) == null) {
            x = ((GeneratedMessageLite)e12.j(clazz)).x();
            if (x == null) {
                throw new IllegalStateException();
            }
            GeneratedMessageLite.defaultInstanceMap.put(clazz, x);
        }
        return x;
    }
    
    static Object y(final Method method, final Object obj, final Object... args) {
        try {
            return method.invoke(obj, args);
        }
        catch (final InvocationTargetException ex) {
            final Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            if (cause instanceof Error) {
                throw (Error)cause;
            }
            throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", cause2);
        }
    }
    
    public void B() {
        l91.a().d(this).d(this);
    }
    
    public final a D() {
        return (a)this.s(MethodToInvoke.NEW_BUILDER);
    }
    
    public final a I() {
        final a a = (a)this.s(MethodToInvoke.NEW_BUILDER);
        a.v(this);
        return a;
    }
    
    @Override
    public int b() {
        if (this.memoizedSerializedSize == -1) {
            this.memoizedSerializedSize = l91.a().d(this).e(this);
        }
        return this.memoizedSerializedSize;
    }
    
    @Override
    public void e(final CodedOutputStream codedOutputStream) {
        l91.a().d(this).g(this, h.P(codedOutputStream));
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (this.x().getClass().isInstance(o) && l91.a().d(this).c(this, o));
    }
    
    @Override
    public int hashCode() {
        final int memoizedHashCode = super.memoizedHashCode;
        if (memoizedHashCode != 0) {
            return memoizedHashCode;
        }
        return super.memoizedHashCode = l91.a().d(this).f(this);
    }
    
    @Override
    int i() {
        return this.memoizedSerializedSize;
    }
    
    @Override
    void m(final int memoizedSerializedSize) {
        this.memoizedSerializedSize = memoizedSerializedSize;
    }
    
    Object p() {
        return this.s(MethodToInvoke.BUILD_MESSAGE_INFO);
    }
    
    public final a r() {
        return (a)this.s(MethodToInvoke.NEW_BUILDER);
    }
    
    public Object s(final MethodToInvoke methodToInvoke) {
        return this.u(methodToInvoke, null, null);
    }
    
    public Object t(final MethodToInvoke methodToInvoke, final Object o) {
        return this.u(methodToInvoke, o, null);
    }
    
    @Override
    public String toString() {
        return z.e(this, super.toString());
    }
    
    public abstract Object u(final MethodToInvoke p0, final Object p1, final Object p2);
    
    public final GeneratedMessageLite x() {
        return (GeneratedMessageLite)this.s(MethodToInvoke.GET_DEFAULT_INSTANCE);
    }
    
    public final boolean z() {
        return A(this, true);
    }
    
    public enum MethodToInvoke
    {
        private static final MethodToInvoke[] $VALUES;
        
        BUILD_MESSAGE_INFO, 
        GET_DEFAULT_INSTANCE, 
        GET_MEMOIZED_IS_INITIALIZED, 
        GET_PARSER, 
        NEW_BUILDER, 
        NEW_MUTABLE_INSTANCE, 
        SET_MEMOIZED_IS_INITIALIZED;
    }
    
    public static final class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final byte[] asBytes;
        private final Class<?> messageClass;
        private final String messageClassName;
        
        public SerializedForm(final y y) {
            final Class<? extends y> class1 = y.getClass();
            this.messageClass = class1;
            this.messageClassName = class1.getName();
            this.asBytes = y.toByteArray();
        }
        
        public static SerializedForm of(final y y) {
            return new SerializedForm(y);
        }
        
        @Deprecated
        private Object readResolveFallback() {
            try {
                final Field declaredField = this.resolveMessageClass().getDeclaredField("defaultInstance");
                declaredField.setAccessible(true);
                return ((y)declaredField.get(null)).d().c(this.asBytes).g();
            }
            catch (final InvalidProtocolBufferException cause) {
                throw new RuntimeException("Unable to understand proto buffer", cause);
            }
            catch (final IllegalAccessException cause2) {
                throw new RuntimeException("Unable to call parsePartialFrom", cause2);
            }
            catch (final SecurityException cause3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to call defaultInstance in ");
                sb.append(this.messageClassName);
                throw new RuntimeException(sb.toString(), cause3);
            }
            catch (final NoSuchFieldException cause4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to find defaultInstance in ");
                sb2.append(this.messageClassName);
                throw new RuntimeException(sb2.toString(), cause4);
            }
            catch (final ClassNotFoundException cause5) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to find proto buffer class: ");
                sb3.append(this.messageClassName);
                throw new RuntimeException(sb3.toString(), cause5);
            }
        }
        
        private Class<?> resolveMessageClass() {
            Class<?> clazz = this.messageClass;
            if (clazz == null) {
                clazz = Class.forName(this.messageClassName);
            }
            return clazz;
        }
        
        public Object readResolve() {
            try {
                final Field declaredField = this.resolveMessageClass().getDeclaredField("DEFAULT_INSTANCE");
                declaredField.setAccessible(true);
                return ((y)declaredField.get(null)).d().c(this.asBytes).g();
            }
            catch (final InvalidProtocolBufferException cause) {
                throw new RuntimeException("Unable to understand proto buffer", cause);
            }
            catch (final IllegalAccessException cause2) {
                throw new RuntimeException("Unable to call parsePartialFrom", cause2);
            }
            catch (final SecurityException cause3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to call DEFAULT_INSTANCE in ");
                sb.append(this.messageClassName);
                throw new RuntimeException(sb.toString(), cause3);
            }
            catch (final NoSuchFieldException ex) {
                return this.readResolveFallback();
            }
            catch (final ClassNotFoundException cause4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to find proto buffer class: ");
                sb2.append(this.messageClassName);
                throw new RuntimeException(sb2.toString(), cause4);
            }
        }
    }
    
    public abstract static class a extends androidx.datastore.preferences.protobuf.a.a
    {
        public final GeneratedMessageLite a;
        public GeneratedMessageLite b;
        public boolean c;
        
        public a(final GeneratedMessageLite a) {
            this.a = a;
            this.b = (GeneratedMessageLite)a.s(MethodToInvoke.NEW_MUTABLE_INSTANCE);
            this.c = false;
        }
        
        public final GeneratedMessageLite p() {
            final GeneratedMessageLite q = this.q();
            if (q.z()) {
                return q;
            }
            throw androidx.datastore.preferences.protobuf.a.a.n(q);
        }
        
        public GeneratedMessageLite q() {
            if (this.c) {
                return this.b;
            }
            this.b.B();
            this.c = true;
            return this.b;
        }
        
        public a r() {
            final a d = this.t().D();
            d.v(this.q());
            return d;
        }
        
        public void s() {
            if (this.c) {
                final GeneratedMessageLite b = (GeneratedMessageLite)this.b.s(MethodToInvoke.NEW_MUTABLE_INSTANCE);
                this.y(b, this.b);
                this.b = b;
                this.c = false;
            }
        }
        
        public GeneratedMessageLite t() {
            return this.a;
        }
        
        public a u(final GeneratedMessageLite generatedMessageLite) {
            return this.v(generatedMessageLite);
        }
        
        public a v(final GeneratedMessageLite generatedMessageLite) {
            this.s();
            this.y(this.b, generatedMessageLite);
            return this;
        }
        
        public a w(final byte[] array, final int n, final int n2) {
            return this.x(array, n, n2, k.b());
        }
        
        public a x(final byte[] array, final int n, final int n2, final k k) {
            this.s();
            try {
                l91.a().d(this.b).i(this.b, array, n, n + n2, new d.a(k));
                return this;
            }
            catch (final IOException cause) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", cause);
            }
            catch (final IndexOutOfBoundsException ex) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            catch (final InvalidProtocolBufferException ex2) {
                throw ex2;
            }
        }
        
        public final void y(final GeneratedMessageLite generatedMessageLite, final GeneratedMessageLite generatedMessageLite2) {
            l91.a().d(generatedMessageLite).a(generatedMessageLite, generatedMessageLite2);
        }
    }
    
    public static class b extends androidx.datastore.preferences.protobuf.b
    {
        public final GeneratedMessageLite b;
        
        public b(final GeneratedMessageLite b) {
            this.b = b;
        }
    }
    
    public abstract static class c extends j
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public abstract class a implements y
{
    protected int memoizedHashCode;
    
    public a() {
        this.memoizedHashCode = 0;
    }
    
    public static void h(final Iterable iterable, final List list) {
        a.h(iterable, list);
    }
    
    abstract int i();
    
    public int j(final f0 f0) {
        int n;
        if ((n = this.i()) == -1) {
            n = f0.e(this);
            this.m(n);
        }
        return n;
    }
    
    public final String k(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Serializing ");
        sb.append(this.getClass().getName());
        sb.append(" to a ");
        sb.append(str);
        sb.append(" threw an IOException (should never happen).");
        return sb.toString();
    }
    
    public UninitializedMessageException l() {
        return new UninitializedMessageException(this);
    }
    
    abstract void m(final int p0);
    
    public void n(final OutputStream outputStream) {
        final CodedOutputStream b0 = CodedOutputStream.b0(outputStream, CodedOutputStream.E(this.b()));
        this.e(b0);
        b0.Y();
    }
    
    @Override
    public byte[] toByteArray() {
        try {
            final byte[] array = new byte[this.b()];
            final CodedOutputStream c0 = CodedOutputStream.c0(array);
            this.e(c0);
            c0.d();
            return array;
        }
        catch (final IOException cause) {
            throw new RuntimeException(this.k("byte array"), cause);
        }
    }
    
    public abstract static class a implements y.a
    {
        public static void h(final Iterable iterable, final List list) {
            r.a(iterable);
            if (iterable instanceof gj0) {
                final List f = ((gj0)iterable).f();
                final gj0 gj0 = (gj0)list;
                final int size = list.size();
                for (final Object next : f) {
                    if (next == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Element at index ");
                        sb.append(gj0.size() - size);
                        sb.append(" is null.");
                        final String string = sb.toString();
                        for (int i = gj0.size() - 1; i >= size; --i) {
                            gj0.remove(i);
                        }
                        throw new NullPointerException(string);
                    }
                    if (next instanceof ByteString) {
                        gj0.x((ByteString)next);
                    }
                    else {
                        gj0.add((String)next);
                    }
                }
            }
            else if (iterable instanceof b81) {
                list.addAll((Collection<?>)iterable);
            }
            else {
                i(iterable, list);
            }
        }
        
        public static void i(final Iterable iterable, final List list) {
            if (list instanceof ArrayList && iterable instanceof Collection) {
                ((ArrayList)list).ensureCapacity(list.size() + ((Collection)iterable).size());
            }
            final int size = list.size();
            for (final Object next : iterable) {
                if (next == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Element at index ");
                    sb.append(list.size() - size);
                    sb.append(" is null.");
                    final String string = sb.toString();
                    for (int i = list.size() - 1; i >= size; --i) {
                        list.remove(i);
                    }
                    throw new NullPointerException(string);
                }
                list.add(next);
            }
        }
        
        public static UninitializedMessageException n(final y y) {
            return new UninitializedMessageException(y);
        }
        
        public abstract a j(final androidx.datastore.preferences.protobuf.a p0);
        
        public a k(final y y) {
            if (this.a().getClass().isInstance(y)) {
                return this.j((androidx.datastore.preferences.protobuf.a)y);
            }
            throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
        }
        
        public a l(final byte[] array) {
            return this.m(array, 0, array.length);
        }
        
        public abstract a m(final byte[] p0, final int p1, final int p2);
    }
}

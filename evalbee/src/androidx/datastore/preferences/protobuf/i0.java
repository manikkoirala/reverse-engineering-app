// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public abstract class i0
{
    public abstract void a(final Object p0, final int p1, final int p2);
    
    public abstract void b(final Object p0, final int p1, final long p2);
    
    public abstract void c(final Object p0, final int p1, final Object p2);
    
    public abstract void d(final Object p0, final int p1, final ByteString p2);
    
    public abstract void e(final Object p0, final int p1, final long p2);
    
    public abstract Object f(final Object p0);
    
    public abstract Object g(final Object p0);
    
    public abstract int h(final Object p0);
    
    public abstract int i(final Object p0);
    
    public abstract void j(final Object p0);
    
    public abstract Object k(final Object p0, final Object p1);
    
    public final void l(final Object o, final e0 e0) {
        while (e0.m() != Integer.MAX_VALUE && this.m(o, e0)) {}
    }
    
    public final boolean m(final Object o, final e0 e0) {
        final int tag = e0.getTag();
        final int a = WireFormat.a(tag);
        final int b = WireFormat.b(tag);
        if (b == 0) {
            this.e(o, a, e0.s());
            return true;
        }
        if (b == 1) {
            this.b(o, a, e0.t());
            return true;
        }
        if (b == 2) {
            this.d(o, a, e0.g());
            return true;
        }
        if (b != 3) {
            if (b == 4) {
                return false;
            }
            if (b == 5) {
                this.a(o, a, e0.D());
                return true;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }
        else {
            final Object n = this.n();
            final int c = WireFormat.c(a, 4);
            this.l(n, e0);
            if (c == e0.getTag()) {
                this.c(o, a, this.r(n));
                return true;
            }
            throw InvalidProtocolBufferException.invalidEndTag();
        }
    }
    
    public abstract Object n();
    
    public abstract void o(final Object p0, final Object p1);
    
    public abstract void p(final Object p0, final Object p1);
    
    public abstract boolean q(final e0 p0);
    
    public abstract Object r(final Object p0);
    
    public abstract void s(final Object p0, final Writer p1);
    
    public abstract void t(final Object p0, final Writer p1);
}

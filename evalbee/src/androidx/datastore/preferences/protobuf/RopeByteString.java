// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Arrays;
import java.util.ArrayDeque;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.io.InputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ArrayList;
import java.util.List;
import java.nio.ByteBuffer;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;

final class RopeByteString extends ByteString
{
    static final int[] minLengthByDepth;
    private static final long serialVersionUID = 1L;
    private final ByteString left;
    private final int leftLength;
    private final ByteString right;
    private final int totalLength;
    private final int treeDepth;
    
    static {
        minLengthByDepth = new int[] { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903, Integer.MAX_VALUE };
    }
    
    private RopeByteString(final ByteString left, final ByteString right) {
        this.left = left;
        this.right = right;
        final int size = left.size();
        this.leftLength = size;
        this.totalLength = size + right.size();
        this.treeDepth = Math.max(left.getTreeDepth(), right.getTreeDepth()) + 1;
    }
    
    public static /* synthetic */ ByteString access$400(final RopeByteString ropeByteString) {
        return ropeByteString.left;
    }
    
    public static /* synthetic */ ByteString access$500(final RopeByteString ropeByteString) {
        return ropeByteString.right;
    }
    
    public static ByteString concatenate(ByteString concatenateBytes, final ByteString byteString) {
        if (byteString.size() == 0) {
            return concatenateBytes;
        }
        if (concatenateBytes.size() == 0) {
            return byteString;
        }
        final int n = concatenateBytes.size() + byteString.size();
        if (n < 128) {
            return concatenateBytes(concatenateBytes, byteString);
        }
        if (concatenateBytes instanceof RopeByteString) {
            final RopeByteString ropeByteString = (RopeByteString)concatenateBytes;
            if (ropeByteString.right.size() + byteString.size() < 128) {
                concatenateBytes = concatenateBytes(ropeByteString.right, byteString);
                return new RopeByteString(ropeByteString.left, concatenateBytes);
            }
            if (ropeByteString.left.getTreeDepth() > ropeByteString.right.getTreeDepth() && ropeByteString.getTreeDepth() > byteString.getTreeDepth()) {
                return new RopeByteString(ropeByteString.left, new RopeByteString(ropeByteString.right, byteString));
            }
        }
        if (n >= RopeByteString.minLengthByDepth[Math.max(concatenateBytes.getTreeDepth(), byteString.getTreeDepth()) + 1]) {
            return new RopeByteString(concatenateBytes, byteString);
        }
        return new b(null).b(concatenateBytes, byteString);
    }
    
    private static ByteString concatenateBytes(final ByteString byteString, final ByteString byteString2) {
        final int size = byteString.size();
        final int size2 = byteString2.size();
        final byte[] array = new byte[size + size2];
        byteString.copyTo(array, 0, 0, size);
        byteString2.copyTo(array, 0, size, size2);
        return ByteString.wrap(array);
    }
    
    private boolean equalsFragments(final ByteString byteString) {
        final c c = new c(this, null);
        LeafByteString leafByteString = c.next();
        final c c2 = new c(byteString, null);
        LeafByteString leafByteString2 = c2.next();
        int n = 0;
        int n3;
        int n2 = n3 = 0;
        while (true) {
            final int a = leafByteString.size() - n;
            final int b = leafByteString2.size() - n2;
            final int min = Math.min(a, b);
            boolean b2;
            if (n == 0) {
                b2 = leafByteString.equalsRange(leafByteString2, n2, min);
            }
            else {
                b2 = leafByteString2.equalsRange(leafByteString, n, min);
            }
            if (!b2) {
                return false;
            }
            n3 += min;
            final int totalLength = this.totalLength;
            if (n3 >= totalLength) {
                if (n3 == totalLength) {
                    return true;
                }
                throw new IllegalStateException();
            }
            else {
                if (min == a) {
                    leafByteString = (LeafByteString)c.next();
                    n = 0;
                }
                else {
                    n += min;
                }
                if (min == b) {
                    leafByteString2 = (LeafByteString)c2.next();
                    n2 = 0;
                }
                else {
                    n2 += min;
                }
            }
        }
    }
    
    public static RopeByteString newInstanceForTest(final ByteString byteString, final ByteString byteString2) {
        return new RopeByteString(byteString, byteString2);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("RopeByteStream instances are not to be serialized directly");
    }
    
    @Override
    public ByteBuffer asReadOnlyByteBuffer() {
        return ByteBuffer.wrap(this.toByteArray()).asReadOnlyBuffer();
    }
    
    @Override
    public List<ByteBuffer> asReadOnlyByteBufferList() {
        final ArrayList list = new ArrayList();
        final c c = new c(this, null);
        while (c.hasNext()) {
            list.add(c.d().asReadOnlyByteBuffer());
        }
        return list;
    }
    
    @Override
    public byte byteAt(final int n) {
        ByteString.checkIndex(n, this.totalLength);
        return this.internalByteAt(n);
    }
    
    @Override
    public void copyTo(final ByteBuffer byteBuffer) {
        this.left.copyTo(byteBuffer);
        this.right.copyTo(byteBuffer);
    }
    
    @Override
    public void copyToInternal(final byte[] array, int n, final int n2, final int n3) {
        final int leftLength = this.leftLength;
        ByteString byteString;
        if (n + n3 <= leftLength) {
            byteString = this.left;
        }
        else {
            if (n < leftLength) {
                final int n4 = leftLength - n;
                this.left.copyToInternal(array, n, n2, n4);
                this.right.copyToInternal(array, 0, n2 + n4, n3 - n4);
                return;
            }
            byteString = this.right;
            n -= leftLength;
        }
        byteString.copyToInternal(array, n, n2, n3);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ByteString)) {
            return false;
        }
        final ByteString byteString = (ByteString)o;
        if (this.totalLength != byteString.size()) {
            return false;
        }
        if (this.totalLength == 0) {
            return true;
        }
        final int peekCachedHashCode = this.peekCachedHashCode();
        final int peekCachedHashCode2 = byteString.peekCachedHashCode();
        return (peekCachedHashCode == 0 || peekCachedHashCode2 == 0 || peekCachedHashCode == peekCachedHashCode2) && this.equalsFragments(byteString);
    }
    
    @Override
    public int getTreeDepth() {
        return this.treeDepth;
    }
    
    @Override
    public byte internalByteAt(final int n) {
        final int leftLength = this.leftLength;
        if (n < leftLength) {
            return this.left.internalByteAt(n);
        }
        return this.right.internalByteAt(n - leftLength);
    }
    
    @Override
    public boolean isBalanced() {
        return this.totalLength >= RopeByteString.minLengthByDepth[this.treeDepth];
    }
    
    @Override
    public boolean isValidUtf8() {
        final ByteString left = this.left;
        final int leftLength = this.leftLength;
        boolean b = false;
        final int partialIsValidUtf8 = left.partialIsValidUtf8(0, 0, leftLength);
        final ByteString right = this.right;
        if (right.partialIsValidUtf8(partialIsValidUtf8, 0, right.size()) == 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public f iterator() {
        return new ByteString.c(this) {
            public final RopeByteString.c a = new RopeByteString.c(c, null);
            public f b = this.c();
            public final RopeByteString c;
            
            @Override
            public byte a() {
                final f b = this.b;
                if (b != null) {
                    final byte a = b.a();
                    if (!this.b.hasNext()) {
                        this.b = this.c();
                    }
                    return a;
                }
                throw new NoSuchElementException();
            }
            
            public final f c() {
                Object iterator;
                if (this.a.hasNext()) {
                    iterator = this.a.d().iterator();
                }
                else {
                    iterator = null;
                }
                return (f)iterator;
            }
            
            @Override
            public boolean hasNext() {
                return this.b != null;
            }
        };
    }
    
    @Override
    public androidx.datastore.preferences.protobuf.f newCodedInput() {
        return androidx.datastore.preferences.protobuf.f.f(new d());
    }
    
    @Override
    public InputStream newInput() {
        return new d();
    }
    
    @Override
    public int partialHash(int partialHash, final int n, final int n2) {
        final int leftLength = this.leftLength;
        if (n + n2 <= leftLength) {
            return this.left.partialHash(partialHash, n, n2);
        }
        if (n >= leftLength) {
            return this.right.partialHash(partialHash, n - leftLength, n2);
        }
        final int n3 = leftLength - n;
        partialHash = this.left.partialHash(partialHash, n, n3);
        return this.right.partialHash(partialHash, 0, n2 - n3);
    }
    
    @Override
    public int partialIsValidUtf8(int partialIsValidUtf8, final int n, final int n2) {
        final int leftLength = this.leftLength;
        if (n + n2 <= leftLength) {
            return this.left.partialIsValidUtf8(partialIsValidUtf8, n, n2);
        }
        if (n >= leftLength) {
            return this.right.partialIsValidUtf8(partialIsValidUtf8, n - leftLength, n2);
        }
        final int n3 = leftLength - n;
        partialIsValidUtf8 = this.left.partialIsValidUtf8(partialIsValidUtf8, n, n3);
        return this.right.partialIsValidUtf8(partialIsValidUtf8, 0, n2 - n3);
    }
    
    @Override
    public int size() {
        return this.totalLength;
    }
    
    @Override
    public ByteString substring(final int n, final int n2) {
        final int checkRange = ByteString.checkRange(n, n2, this.totalLength);
        if (checkRange == 0) {
            return ByteString.EMPTY;
        }
        if (checkRange == this.totalLength) {
            return this;
        }
        final int leftLength = this.leftLength;
        if (n2 <= leftLength) {
            return this.left.substring(n, n2);
        }
        if (n >= leftLength) {
            return this.right.substring(n - leftLength, n2 - leftLength);
        }
        return new RopeByteString(this.left.substring(n), this.right.substring(0, n2 - this.leftLength));
    }
    
    @Override
    public String toStringInternal(final Charset charset) {
        return new String(this.toByteArray(), charset);
    }
    
    public Object writeReplace() {
        return ByteString.wrap(this.toByteArray());
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) {
        this.left.writeTo(outputStream);
        this.right.writeTo(outputStream);
    }
    
    @Override
    public void writeTo(final qd qd) {
        this.left.writeTo(qd);
        this.right.writeTo(qd);
    }
    
    @Override
    public void writeToInternal(final OutputStream outputStream, int n, final int n2) {
        final int leftLength = this.leftLength;
        ByteString byteString;
        if (n + n2 <= leftLength) {
            byteString = this.left;
        }
        else {
            if (n < leftLength) {
                final int n3 = leftLength - n;
                this.left.writeToInternal(outputStream, n, n3);
                this.right.writeToInternal(outputStream, 0, n2 - n3);
                return;
            }
            byteString = this.right;
            n -= leftLength;
        }
        byteString.writeToInternal(outputStream, n, n2);
    }
    
    @Override
    public void writeToReverse(final qd qd) {
        this.right.writeToReverse(qd);
        this.left.writeToReverse(qd);
    }
    
    public static class b
    {
        public final ArrayDeque a;
        
        public b() {
            this.a = new ArrayDeque();
        }
        
        public final ByteString b(ByteString o, final ByteString byteString) {
            this.c((ByteString)o);
            this.c(byteString);
            o = this.a.pop();
            while (!this.a.isEmpty()) {
                o = new RopeByteString(this.a.pop(), (ByteString)o, null);
            }
            return (ByteString)o;
        }
        
        public final void c(final ByteString byteString) {
            if (byteString.isBalanced()) {
                this.e(byteString);
            }
            else {
                if (!(byteString instanceof RopeByteString)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Has a new type of ByteString been created? Found ");
                    sb.append(byteString.getClass());
                    throw new IllegalArgumentException(sb.toString());
                }
                final RopeByteString ropeByteString = (RopeByteString)byteString;
                this.c(RopeByteString.access$400(ropeByteString));
                this.c(RopeByteString.access$500(ropeByteString));
            }
        }
        
        public final int d(int binarySearch) {
            final int n = binarySearch = Arrays.binarySearch(RopeByteString.minLengthByDepth, binarySearch);
            if (n < 0) {
                binarySearch = -(n + 1) - 1;
            }
            return binarySearch;
        }
        
        public final void e(final ByteString e) {
            final int d = this.d(e.size());
            final int[] minLengthByDepth = RopeByteString.minLengthByDepth;
            final int n = minLengthByDepth[d + 1];
            if (!this.a.isEmpty() && this.a.peek().size() < n) {
                final int n2 = minLengthByDepth[d];
                ByteString byteString = this.a.pop();
                while (!this.a.isEmpty() && this.a.peek().size() < n2) {
                    byteString = new RopeByteString(this.a.pop(), byteString, null);
                }
                RopeByteString e2;
                for (e2 = new RopeByteString(byteString, e, null); !this.a.isEmpty() && this.a.peek().size() < RopeByteString.minLengthByDepth[this.d(e2.size()) + 1]; e2 = new RopeByteString(this.a.pop(), e2, null)) {}
                this.a.push(e2);
            }
            else {
                this.a.push(e);
            }
        }
    }
    
    public static final class c implements Iterator
    {
        public final ArrayDeque a;
        public LeafByteString b;
        
        public c(final ByteString byteString) {
            LeafByteString b;
            if (byteString instanceof RopeByteString) {
                final RopeByteString e = (RopeByteString)byteString;
                (this.a = new ArrayDeque(e.getTreeDepth())).push(e);
                b = this.b(RopeByteString.access$400(e));
            }
            else {
                this.a = null;
                b = (LeafByteString)byteString;
            }
            this.b = b;
        }
        
        public final LeafByteString b(ByteString access$400) {
            while (access$400 instanceof RopeByteString) {
                final RopeByteString e = (RopeByteString)access$400;
                this.a.push(e);
                access$400 = RopeByteString.access$400(e);
            }
            return (LeafByteString)access$400;
        }
        
        public final LeafByteString c() {
            LeafByteString b;
            do {
                final ArrayDeque a = this.a;
                if (a == null || a.isEmpty()) {
                    return null;
                }
                b = this.b(RopeByteString.access$500(this.a.pop()));
            } while (b.isEmpty());
            return b;
        }
        
        public LeafByteString d() {
            final LeafByteString b = this.b;
            if (b != null) {
                this.b = this.c();
                return b;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public boolean hasNext() {
            return this.b != null;
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    public class d extends InputStream
    {
        public c a;
        public LeafByteString b;
        public int c;
        public int d;
        public int e;
        public int f;
        public final RopeByteString g;
        
        public d(final RopeByteString g) {
            this.g = g;
            this.b();
        }
        
        public final void a() {
            if (this.b != null) {
                final int d = this.d;
                final int c = this.c;
                if (d == c) {
                    this.e += c;
                    int size = 0;
                    this.d = 0;
                    if (this.a.hasNext()) {
                        final LeafByteString d2 = this.a.d();
                        this.b = d2;
                        size = d2.size();
                    }
                    else {
                        this.b = null;
                    }
                    this.c = size;
                }
            }
        }
        
        @Override
        public int available() {
            return this.g.size() - (this.e + this.d);
        }
        
        public final void b() {
            final c a = new c(this.g, null);
            this.a = a;
            final LeafByteString d = a.d();
            this.b = d;
            this.c = d.size();
            this.d = 0;
            this.e = 0;
        }
        
        public final int c(final byte[] array, int i, final int n) {
            int n2 = i;
            i = n;
            while (i > 0) {
                this.a();
                if (this.b == null) {
                    if (i == n) {
                        return -1;
                    }
                    break;
                }
                else {
                    final int min = Math.min(this.c - this.d, i);
                    int n3 = n2;
                    if (array != null) {
                        this.b.copyTo(array, this.d, n2, min);
                        n3 = n2 + min;
                    }
                    this.d += min;
                    i -= min;
                    n2 = n3;
                }
            }
            return n - i;
        }
        
        @Override
        public void mark(final int n) {
            this.f = this.e + this.d;
        }
        
        @Override
        public boolean markSupported() {
            return true;
        }
        
        @Override
        public int read() {
            this.a();
            final LeafByteString b = this.b;
            if (b == null) {
                return -1;
            }
            return b.byteAt(this.d++) & 0xFF;
        }
        
        @Override
        public int read(final byte[] array, final int n, final int n2) {
            array.getClass();
            if (n >= 0 && n2 >= 0 && n2 <= array.length - n) {
                return this.c(array, n, n2);
            }
            throw new IndexOutOfBoundsException();
        }
        
        @Override
        public void reset() {
            synchronized (this) {
                this.b();
                this.c(null, 0, this.f);
            }
        }
        
        @Override
        public long skip(final long n) {
            if (n >= 0L) {
                long n2 = n;
                if (n > 2147483647L) {
                    n2 = 2147483647L;
                }
                return this.c(null, 0, (int)n2);
            }
            throw new IndexOutOfBoundsException();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

public enum ProtoSyntax
{
    private static final ProtoSyntax[] $VALUES;
    
    PROTO2, 
    PROTO3;
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Arrays;
import java.util.RandomAccess;

public final class d0 extends c implements RandomAccess
{
    public static final d0 d;
    public Object[] b;
    public int c;
    
    static {
        (d = new d0(new Object[0], 0)).e();
    }
    
    public d0(final Object[] b, final int c) {
        this.b = b;
        this.c = c;
    }
    
    public static Object[] b(final int n) {
        return new Object[n];
    }
    
    public static d0 c() {
        return d0.d;
    }
    
    @Override
    public void add(final int n, final Object o) {
        this.a();
        if (n >= 0) {
            final int c = this.c;
            if (n <= c) {
                final Object[] b = this.b;
                if (c < b.length) {
                    System.arraycopy(b, n, b, n + 1, c - n);
                }
                else {
                    final Object[] b2 = b(c * 3 / 2 + 1);
                    System.arraycopy(this.b, 0, b2, 0, n);
                    System.arraycopy(this.b, n, b2, n + 1, this.c - n);
                    this.b = b2;
                }
                this.b[n] = o;
                ++this.c;
                ++super.modCount;
                return;
            }
        }
        throw new IndexOutOfBoundsException(this.i(n));
    }
    
    @Override
    public boolean add(final Object o) {
        this.a();
        final int c = this.c;
        final Object[] b = this.b;
        if (c == b.length) {
            this.b = Arrays.copyOf(b, c * 3 / 2 + 1);
        }
        this.b[this.c++] = o;
        ++super.modCount;
        return true;
    }
    
    public final void g(final int n) {
        if (n >= 0 && n < this.c) {
            return;
        }
        throw new IndexOutOfBoundsException(this.i(n));
    }
    
    @Override
    public Object get(final int n) {
        this.g(n);
        return this.b[n];
    }
    
    public final String i(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(this.c);
        return sb.toString();
    }
    
    public d0 l(final int newLength) {
        if (newLength >= this.c) {
            return new d0(Arrays.copyOf(this.b, newLength), this.c);
        }
        throw new IllegalArgumentException();
    }
    
    @Override
    public Object remove(final int n) {
        this.a();
        this.g(n);
        final Object[] b = this.b;
        final Object o = b[n];
        final int c = this.c;
        if (n < c - 1) {
            System.arraycopy(b, n + 1, b, n, c - n - 1);
        }
        --this.c;
        ++super.modCount;
        return o;
    }
    
    @Override
    public Object set(final int n, final Object o) {
        this.a();
        this.g(n);
        final Object[] b = this.b;
        final Object o2 = b[n];
        b[n] = o;
        ++super.modCount;
        return o2;
    }
    
    @Override
    public int size() {
        return this.c;
    }
}

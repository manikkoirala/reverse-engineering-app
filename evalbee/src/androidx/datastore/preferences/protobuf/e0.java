// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Map;
import java.util.List;

public interface e0
{
    int A();
    
    void B(final List p0);
    
    void C(final List p0);
    
    int D();
    
    long E();
    
    String F();
    
    int G();
    
    String H();
    
    Object I(final f0 p0, final k p1);
    
    Object J(final f0 p0, final k p1);
    
    void K(final List p0, final f0 p1, final k p2);
    
    void L(final Map p0, final v.a p1, final k p2);
    
    void M(final List p0, final f0 p1, final k p2);
    
    void a(final List p0);
    
    long b();
    
    int c();
    
    int d();
    
    int e();
    
    void f(final List p0);
    
    ByteString g();
    
    int getTag();
    
    void h(final List p0);
    
    long i();
    
    void j(final List p0);
    
    void k(final List p0);
    
    void l(final List p0);
    
    int m();
    
    void n(final List p0);
    
    void o(final List p0);
    
    boolean p();
    
    void q(final List p0);
    
    void r(final List p0);
    
    double readDouble();
    
    float readFloat();
    
    long s();
    
    long t();
    
    void u(final List p0);
    
    boolean v();
    
    void w(final List p0);
    
    void x(final List p0);
    
    void y(final List p0);
    
    void z(final List p0);
}

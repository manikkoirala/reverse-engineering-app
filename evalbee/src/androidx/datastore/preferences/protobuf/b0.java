// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.util.Iterator;
import java.util.Map;

public final class b0 implements f0
{
    public final y a;
    public final i0 b;
    public final boolean c;
    public final l d;
    
    public b0(final i0 b, final l d, final y a) {
        this.b = b;
        this.c = d.e(a);
        this.d = d;
        this.a = a;
    }
    
    public static b0 l(final i0 i0, final l l, final y y) {
        return new b0(i0, l, y);
    }
    
    @Override
    public void a(final Object o, final Object o2) {
        g0.F(this.b, o, o2);
        if (this.c) {
            g0.D(this.d, o, o2);
        }
    }
    
    @Override
    public final boolean b(final Object o) {
        return this.d.c(o).k();
    }
    
    @Override
    public boolean c(final Object o, final Object o2) {
        return this.b.g(o).equals(this.b.g(o2)) && (!this.c || this.d.c(o).equals(this.d.c(o2)));
    }
    
    @Override
    public void d(final Object o) {
        this.b.j(o);
        this.d.f(o);
    }
    
    @Override
    public int e(final Object o) {
        int n = this.j(this.b, o) + 0;
        if (this.c) {
            n += this.d.c(o).f();
        }
        return n;
    }
    
    @Override
    public int f(final Object o) {
        int hashCode = this.b.g(o).hashCode();
        if (this.c) {
            hashCode = hashCode * 53 + this.d.c(o).hashCode();
        }
        return hashCode;
    }
    
    @Override
    public void g(final Object o, final Writer writer) {
        final Iterator n = this.d.c(o).n();
        if (!n.hasNext()) {
            this.n(this.b, o, writer);
            return;
        }
        zu0.a(((Map.Entry<Object, V>)n.next()).getKey());
        throw null;
    }
    
    @Override
    public void h(final Object o, final e0 e0, final k k) {
        this.k(this.b, this.d, o, e0, k);
    }
    
    @Override
    public void i(final Object o, final byte[] array, final int n, final int n2, final d.a a) {
        final GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)o;
        if (generatedMessageLite.unknownFields == j0.e()) {
            generatedMessageLite.unknownFields = j0.l();
        }
        zu0.a(o);
        throw null;
    }
    
    public final int j(final i0 i0, final Object o) {
        return i0.i(i0.g(o));
    }
    
    public final void k(final i0 i0, final l l, final Object o, final e0 e0, final k k) {
        final Object f = i0.f(o);
        final n d = l.d(o);
        try {
            while (e0.m() != Integer.MAX_VALUE) {
                if (this.m(e0, k, l, d, i0, f)) {
                    continue;
                }
            }
        }
        finally {
            i0.o(o, f);
        }
    }
    
    public final boolean m(final e0 e0, final k k, final l l, final n n, final i0 i0, final Object o) {
        final int tag = e0.getTag();
        if (tag == WireFormat.a) {
            Object b = null;
            int c = 0;
            ByteString g = null;
            while (true) {
                while (e0.m() != Integer.MAX_VALUE) {
                    final int tag2 = e0.getTag();
                    if (tag2 == WireFormat.c) {
                        c = e0.c();
                        b = l.b(k, this.a, c);
                    }
                    else if (tag2 == WireFormat.d) {
                        if (b != null) {
                            l.h(e0, b, k, n);
                        }
                        else {
                            g = e0.g();
                        }
                    }
                    else {
                        if (e0.p()) {
                            continue;
                        }
                        if (e0.getTag() == WireFormat.b) {
                            if (g != null) {
                                if (b != null) {
                                    l.i(g, b, k, n);
                                }
                                else {
                                    i0.d(o, c, g);
                                }
                            }
                            return true;
                        }
                        throw InvalidProtocolBufferException.invalidEndTag();
                    }
                }
                continue;
            }
        }
        if (WireFormat.b(tag) != 2) {
            return e0.p();
        }
        final Object b2 = l.b(k, this.a, WireFormat.a(tag));
        if (b2 != null) {
            l.h(e0, b2, k, n);
            return true;
        }
        return i0.m(o, e0);
    }
    
    public final void n(final i0 i0, final Object o, final Writer writer) {
        i0.s(i0.g(o), writer);
    }
    
    @Override
    public Object newInstance() {
        return this.a.d().g();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore.preferences.protobuf;

import java.nio.ByteBuffer;
import java.io.IOException;
import java.util.logging.Level;
import java.io.OutputStream;
import java.util.logging.Logger;

public abstract class CodedOutputStream extends qd
{
    public static final Logger c;
    public static final boolean d;
    public h a;
    public boolean b;
    
    static {
        c = Logger.getLogger(CodedOutputStream.class.getName());
        d = e12.G();
    }
    
    public static int A(final int n) {
        return T(n) + n;
    }
    
    public static int B(final int n, final y y, final f0 f0) {
        return R(n) + D(y, f0);
    }
    
    public static int C(final y y) {
        return A(y.b());
    }
    
    public static int D(final y y, final f0 f0) {
        return A(((a)y).j(f0));
    }
    
    public static int E(final int n) {
        if (n > 4096) {
            return 4096;
        }
        return n;
    }
    
    public static int F(final int n, final ByteString byteString) {
        return R(1) * 2 + S(2, n) + h(3, byteString);
    }
    
    public static int G(final int n) {
        return T(n);
    }
    
    public static int H(final int n, final int n2) {
        return R(n) + I(n2);
    }
    
    public static int I(final int n) {
        return 4;
    }
    
    public static int J(final int n, final long n2) {
        return R(n) + K(n2);
    }
    
    public static int K(final long n) {
        return 8;
    }
    
    public static int L(final int n, final int n2) {
        return R(n) + M(n2);
    }
    
    public static int M(final int n) {
        return T(W(n));
    }
    
    public static int N(final int n, final long n2) {
        return R(n) + O(n2);
    }
    
    public static int O(final long n) {
        return V(X(n));
    }
    
    public static int P(final int n, final String s) {
        return R(n) + Q(s);
    }
    
    public static int Q(final String s) {
        int n;
        try {
            n = Utf8.j(s);
        }
        catch (final Utf8.UnpairedSurrogateException ex) {
            n = s.getBytes(r.a).length;
        }
        return A(n);
    }
    
    public static int R(final int n) {
        return T(WireFormat.c(n, 0));
    }
    
    public static int S(final int n, final int n2) {
        return R(n) + T(n2);
    }
    
    public static int T(final int n) {
        if ((n & 0xFFFFFF80) == 0x0) {
            return 1;
        }
        if ((n & 0xFFFFC000) == 0x0) {
            return 2;
        }
        if ((0xFFE00000 & n) == 0x0) {
            return 3;
        }
        if ((n & 0xF0000000) == 0x0) {
            return 4;
        }
        return 5;
    }
    
    public static int U(final int n, final long n2) {
        return R(n) + V(n2);
    }
    
    public static int V(long n) {
        if ((0xFFFFFFFFFFFFFF80L & n) == 0x0L) {
            return 1;
        }
        if (n < 0L) {
            return 10;
        }
        int n2;
        if ((0xFFFFFFF800000000L & n) != 0x0L) {
            n >>>= 28;
            n2 = 6;
        }
        else {
            n2 = 2;
        }
        int n3 = n2;
        long n4 = n;
        if ((0xFFFFFFFFFFE00000L & n) != 0x0L) {
            n3 = n2 + 2;
            n4 = n >>> 14;
        }
        int n5 = n3;
        if ((n4 & 0xFFFFFFFFFFFFC000L) != 0x0L) {
            n5 = n3 + 1;
        }
        return n5;
    }
    
    public static int W(final int n) {
        return n >> 31 ^ n << 1;
    }
    
    public static long X(final long n) {
        return n >> 63 ^ n << 1;
    }
    
    public static CodedOutputStream b0(final OutputStream outputStream, final int n) {
        return new d(outputStream, n);
    }
    
    public static /* synthetic */ boolean c() {
        return CodedOutputStream.d;
    }
    
    public static CodedOutputStream c0(final byte[] array) {
        return d0(array, 0, array.length);
    }
    
    public static CodedOutputStream d0(final byte[] array, final int n, final int n2) {
        return new c(array, n, n2);
    }
    
    public static int e(final int n, final boolean b) {
        return R(n) + f(b);
    }
    
    public static int f(final boolean b) {
        return 1;
    }
    
    public static int g(final byte[] array) {
        return A(array.length);
    }
    
    public static int h(final int n, final ByteString byteString) {
        return R(n) + i(byteString);
    }
    
    public static int i(final ByteString byteString) {
        return A(byteString.size());
    }
    
    public static int j(final int n, final double n2) {
        return R(n) + k(n2);
    }
    
    public static int k(final double n) {
        return 8;
    }
    
    public static int l(final int n, final int n2) {
        return R(n) + m(n2);
    }
    
    public static int m(final int n) {
        return x(n);
    }
    
    public static int n(final int n, final int n2) {
        return R(n) + o(n2);
    }
    
    public static int o(final int n) {
        return 4;
    }
    
    public static int p(final int n, final long n2) {
        return R(n) + q(n2);
    }
    
    public static int q(final long n) {
        return 8;
    }
    
    public static int r(final int n, final float n2) {
        return R(n) + s(n2);
    }
    
    public static int s(final float n) {
        return 4;
    }
    
    public static int t(final int n, final y y, final f0 f0) {
        return R(n) * 2 + v(y, f0);
    }
    
    public static int u(final y y) {
        return y.b();
    }
    
    public static int v(final y y, final f0 f0) {
        return ((a)y).j(f0);
    }
    
    public static int w(final int n, final int n2) {
        return R(n) + x(n2);
    }
    
    public static int x(final int n) {
        if (n >= 0) {
            return T(n);
        }
        return 10;
    }
    
    public static int y(final int n, final long n2) {
        return R(n) + z(n2);
    }
    
    public static int z(final long n) {
        return V(n);
    }
    
    public abstract void A0(final int p0, final int p1);
    
    public abstract void B0(final int p0);
    
    public final void C0(final int n, final long n2) {
        this.V0(n, n2);
    }
    
    public final void D0(final long n) {
        this.W0(n);
    }
    
    public abstract void E0(final int p0, final y p1, final f0 p2);
    
    public abstract void F0(final y p0);
    
    public abstract void G0(final int p0, final y p1);
    
    public abstract void H0(final int p0, final ByteString p1);
    
    public final void I0(final int n, final int n2) {
        this.q0(n, n2);
    }
    
    public final void J0(final int n) {
        this.r0(n);
    }
    
    public final void K0(final int n, final long n2) {
        this.s0(n, n2);
    }
    
    public final void L0(final long n) {
        this.t0(n);
    }
    
    public final void M0(final int n, final int n2) {
        this.T0(n, W(n2));
    }
    
    public final void N0(final int n) {
        this.U0(W(n));
    }
    
    public final void O0(final int n, final long n2) {
        this.V0(n, X(n2));
    }
    
    public final void P0(final long n) {
        this.W0(X(n));
    }
    
    public abstract void Q0(final int p0, final String p1);
    
    public abstract void R0(final String p0);
    
    public abstract void S0(final int p0, final int p1);
    
    public abstract void T0(final int p0, final int p1);
    
    public abstract void U0(final int p0);
    
    public abstract void V0(final int p0, final long p1);
    
    public abstract void W0(final long p0);
    
    public abstract void Y();
    
    public final void Z(final String s, final Utf8.UnpairedSurrogateException thrown) {
        CodedOutputStream.c.log(Level.WARNING, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", thrown);
        final byte[] bytes = s.getBytes(r.a);
        try {
            this.U0(bytes.length);
            this.b(bytes, 0, bytes.length);
        }
        catch (final OutOfSpaceException ex) {
            throw ex;
        }
        catch (final IndexOutOfBoundsException ex2) {
            throw new OutOfSpaceException(ex2);
        }
    }
    
    public boolean a0() {
        return this.b;
    }
    
    @Override
    public abstract void b(final byte[] p0, final int p1, final int p2);
    
    public final void d() {
        if (this.e0() == 0) {
            return;
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }
    
    public abstract int e0();
    
    public abstract void f0(final byte p0);
    
    public abstract void g0(final int p0, final boolean p1);
    
    public final void h0(final boolean b) {
        this.f0((byte)(b ? 1 : 0));
    }
    
    public final void i0(final byte[] array) {
        this.j0(array, 0, array.length);
    }
    
    public abstract void j0(final byte[] p0, final int p1, final int p2);
    
    public abstract void k0(final int p0, final ByteString p1);
    
    public abstract void l0(final ByteString p0);
    
    public final void m0(final int n, final double n2) {
        this.s0(n, Double.doubleToRawLongBits(n2));
    }
    
    public final void n0(final double n) {
        this.t0(Double.doubleToRawLongBits(n));
    }
    
    public final void o0(final int n, final int n2) {
        this.A0(n, n2);
    }
    
    public final void p0(final int n) {
        this.B0(n);
    }
    
    public abstract void q0(final int p0, final int p1);
    
    public abstract void r0(final int p0);
    
    public abstract void s0(final int p0, final long p1);
    
    public abstract void t0(final long p0);
    
    public final void u0(final int n, final float n2) {
        this.q0(n, Float.floatToRawIntBits(n2));
    }
    
    public final void v0(final float n) {
        this.r0(Float.floatToRawIntBits(n));
    }
    
    public final void w0(final int n, final y y) {
        this.S0(n, 3);
        this.y0(y);
        this.S0(n, 4);
    }
    
    public final void x0(final int n, final y y, final f0 f0) {
        this.S0(n, 3);
        this.z0(y, f0);
        this.S0(n, 4);
    }
    
    public final void y0(final y y) {
        y.e(this);
    }
    
    public final void z0(final y y, final f0 f0) {
        f0.g(y, this.a);
    }
    
    public static class OutOfSpaceException extends IOException
    {
        private static final String MESSAGE = "CodedOutputStream was writing to a flat byte array and ran out of space.";
        private static final long serialVersionUID = -6947486886997889499L;
        
        public OutOfSpaceException() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
        
        public OutOfSpaceException(final String str) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            sb.append(str);
            super(sb.toString());
        }
        
        public OutOfSpaceException(final String str, final Throwable cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            sb.append(str);
            super(sb.toString(), cause);
        }
        
        public OutOfSpaceException(final Throwable cause) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", cause);
        }
    }
    
    public abstract static class b extends CodedOutputStream
    {
        public final byte[] e;
        public final int f;
        public int g;
        public int h;
        
        public b(final int a) {
            super(null);
            if (a >= 0) {
                final byte[] e = new byte[Math.max(a, 20)];
                this.e = e;
                this.f = e.length;
                return;
            }
            throw new IllegalArgumentException("bufferSize must be >= 0");
        }
        
        public final void X0(final byte b) {
            this.e[this.g++] = b;
            ++this.h;
        }
        
        public final void Y0(final int n) {
            final byte[] e = this.e;
            final int g = this.g;
            final int n2 = g + 1;
            e[g] = (byte)(n & 0xFF);
            final int n3 = n2 + 1;
            e[n2] = (byte)(n >> 8 & 0xFF);
            final int n4 = n3 + 1;
            e[n3] = (byte)(n >> 16 & 0xFF);
            this.g = n4 + 1;
            e[n4] = (byte)(n >> 24 & 0xFF);
            this.h += 4;
        }
        
        public final void Z0(final long n) {
            final byte[] e = this.e;
            final int g = this.g;
            final int n2 = g + 1;
            e[g] = (byte)(n & 0xFFL);
            final int n3 = n2 + 1;
            e[n2] = (byte)(n >> 8 & 0xFFL);
            final int n4 = n3 + 1;
            e[n3] = (byte)(n >> 16 & 0xFFL);
            final int n5 = n4 + 1;
            e[n4] = (byte)(0xFFL & n >> 24);
            final int n6 = n5 + 1;
            e[n5] = (byte)((int)(n >> 32) & 0xFF);
            final int n7 = n6 + 1;
            e[n6] = (byte)((int)(n >> 40) & 0xFF);
            final int n8 = n7 + 1;
            e[n7] = (byte)((int)(n >> 48) & 0xFF);
            this.g = n8 + 1;
            e[n8] = (byte)((int)(n >> 56) & 0xFF);
            this.h += 8;
        }
        
        public final void a1(final int n) {
            if (n >= 0) {
                this.c1(n);
            }
            else {
                this.d1(n);
            }
        }
        
        public final void b1(final int n, final int n2) {
            this.c1(WireFormat.c(n, n2));
        }
        
        public final void c1(int n) {
            int n2 = n;
            if (CodedOutputStream.c()) {
                final long n3 = this.g;
                while ((n & 0xFFFFFF80) != 0x0) {
                    e12.M(this.e, this.g++, (byte)((n & 0x7F) | 0x80));
                    n >>>= 7;
                }
                e12.M(this.e, this.g++, (byte)n);
                n = (int)(this.g - n3);
                this.h += n;
                return;
            }
            while ((n2 & 0xFFFFFF80) != 0x0) {
                final byte[] e = this.e;
                n = this.g++;
                e[n] = (byte)((n2 & 0x7F) | 0x80);
                ++this.h;
                n2 >>>= 7;
            }
            final byte[] e2 = this.e;
            n = this.g++;
            e2[n] = (byte)n2;
            ++this.h;
        }
        
        public final void d1(long n) {
            long n2 = n;
            if (CodedOutputStream.c()) {
                final long n3 = this.g;
                while ((n & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                    e12.M(this.e, this.g++, (byte)(((int)n & 0x7F) | 0x80));
                    n >>>= 7;
                }
                e12.M(this.e, this.g++, (byte)n);
                this.h += (int)(this.g - n3);
                return;
            }
            while ((n2 & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                this.e[this.g++] = (byte)(((int)n2 & 0x7F) | 0x80);
                ++this.h;
                n2 >>>= 7;
            }
            this.e[this.g++] = (byte)n2;
            ++this.h;
        }
        
        @Override
        public final int e0() {
            throw new UnsupportedOperationException("spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array or ByteBuffer.");
        }
    }
    
    public static class c extends CodedOutputStream
    {
        public final byte[] e;
        public final int f;
        public final int g;
        public int h;
        
        public c(final byte[] e, final int i, final int j) {
            super(null);
            if (e == null) {
                throw new NullPointerException("buffer");
            }
            final int length = e.length;
            final int g = i + j;
            if ((i | j | length - g) >= 0) {
                this.e = e;
                this.f = i;
                this.h = i;
                this.g = g;
                return;
            }
            throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", e.length, i, j));
        }
        
        @Override
        public final void A0(final int n, final int n2) {
            this.S0(n, 0);
            this.B0(n2);
        }
        
        @Override
        public final void B0(final int n) {
            if (n >= 0) {
                this.U0(n);
            }
            else {
                this.W0(n);
            }
        }
        
        @Override
        public final void E0(final int n, final y y, final f0 f0) {
            this.S0(n, 2);
            this.U0(((a)y).j(f0));
            f0.g(y, super.a);
        }
        
        @Override
        public final void F0(final y y) {
            this.U0(y.b());
            y.e(this);
        }
        
        @Override
        public final void G0(final int n, final y y) {
            this.S0(1, 3);
            this.T0(2, n);
            this.Z0(3, y);
            this.S0(1, 4);
        }
        
        @Override
        public final void H0(final int n, final ByteString byteString) {
            this.S0(1, 3);
            this.T0(2, n);
            this.k0(3, byteString);
            this.S0(1, 4);
        }
        
        @Override
        public final void Q0(final int n, final String s) {
            this.S0(n, 2);
            this.R0(s);
        }
        
        @Override
        public final void R0(final String s) {
            final int h = this.h;
            try {
                final int t = CodedOutputStream.T(s.length() * 3);
                final int t2 = CodedOutputStream.T(s.length());
                int h3;
                if (t2 == t) {
                    final int h2 = h + t2;
                    this.h = h2;
                    h3 = Utf8.i(s, this.e, h2, this.e0());
                    this.U0(h3 - (this.h = h) - t2);
                }
                else {
                    this.U0(Utf8.j(s));
                    h3 = Utf8.i(s, this.e, this.h, this.e0());
                }
                this.h = h3;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(ex);
            }
            catch (final Utf8.UnpairedSurrogateException ex2) {
                this.h = h;
                this.Z(s, ex2);
            }
        }
        
        @Override
        public final void S0(final int n, final int n2) {
            this.U0(WireFormat.c(n, n2));
        }
        
        @Override
        public final void T0(final int n, final int n2) {
            this.S0(n, 0);
            this.U0(n2);
        }
        
        @Override
        public final void U0(int n) {
            int n2 = n;
            if (CodedOutputStream.c()) {
                n2 = n;
                if (!q4.c()) {
                    n2 = n;
                    if (this.e0() >= 5) {
                        if ((n & 0xFFFFFF80) == 0x0) {
                            e12.M(this.e, this.h++, (byte)n);
                            return;
                        }
                        e12.M(this.e, this.h++, (byte)(n | 0x80));
                        n >>>= 7;
                        if ((n & 0xFFFFFF80) == 0x0) {
                            e12.M(this.e, this.h++, (byte)n);
                            return;
                        }
                        e12.M(this.e, this.h++, (byte)(n | 0x80));
                        n >>>= 7;
                        if ((n & 0xFFFFFF80) == 0x0) {
                            e12.M(this.e, this.h++, (byte)n);
                            return;
                        }
                        e12.M(this.e, this.h++, (byte)(n | 0x80));
                        n >>>= 7;
                        if ((n & 0xFFFFFF80) == 0x0) {
                            e12.M(this.e, this.h++, (byte)n);
                            return;
                        }
                        e12.M(this.e, this.h++, (byte)(n | 0x80));
                        e12.M(this.e, this.h++, (byte)(n >>> 7));
                        return;
                    }
                }
            }
            while (true) {
                Label_0348: {
                    if ((n2 & 0xFFFFFF80) != 0x0) {
                        break Label_0348;
                    }
                    try {
                        final byte[] e = this.e;
                        n = this.h++;
                        e[n] = (byte)n2;
                        return;
                        final byte[] e2 = this.e;
                        n = this.h++;
                        e2[n] = (byte)((n2 & 0x7F) | 0x80);
                        n2 >>>= 7;
                    }
                    catch (final IndexOutOfBoundsException ex) {
                        throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
                    }
                }
            }
        }
        
        @Override
        public final void V0(final int n, final long n2) {
            this.S0(n, 0);
            this.W0(n2);
        }
        
        @Override
        public final void W0(long n) {
            long n2 = n;
            if (CodedOutputStream.c()) {
                n2 = n;
                if (this.e0() >= 10) {
                    while ((n & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                        e12.M(this.e, this.h++, (byte)(((int)n & 0x7F) | 0x80));
                        n >>>= 7;
                    }
                    e12.M(this.e, this.h++, (byte)n);
                    return;
                }
            }
            while (true) {
                Label_0141: {
                    if ((n2 & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                        break Label_0141;
                    }
                    try {
                        this.e[this.h++] = (byte)n2;
                        return;
                        this.e[this.h++] = (byte)(((int)n2 & 0x7F) | 0x80);
                        n2 >>>= 7;
                    }
                    catch (final IndexOutOfBoundsException ex) {
                        throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
                    }
                }
            }
        }
        
        public final void X0(final ByteBuffer byteBuffer) {
            final int remaining = byteBuffer.remaining();
            try {
                byteBuffer.get(this.e, this.h, remaining);
                this.h += remaining;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, remaining), ex);
            }
        }
        
        @Override
        public void Y() {
        }
        
        public final void Y0(final byte[] array, final int n, final int i) {
            try {
                System.arraycopy(array, n, this.e, this.h, i);
                this.h += i;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, i), ex);
            }
        }
        
        public final void Z0(final int n, final y y) {
            this.S0(n, 2);
            this.F0(y);
        }
        
        @Override
        public final void a(final ByteBuffer byteBuffer) {
            this.X0(byteBuffer);
        }
        
        @Override
        public final void b(final byte[] array, final int n, final int n2) {
            this.Y0(array, n, n2);
        }
        
        @Override
        public final int e0() {
            return this.g - this.h;
        }
        
        @Override
        public final void f0(final byte b) {
            try {
                this.e[this.h++] = b;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
            }
        }
        
        @Override
        public final void g0(final int n, final boolean b) {
            this.S0(n, 0);
            this.f0((byte)(b ? 1 : 0));
        }
        
        @Override
        public final void j0(final byte[] array, final int n, final int n2) {
            this.U0(n2);
            this.Y0(array, n, n2);
        }
        
        @Override
        public final void k0(final int n, final ByteString byteString) {
            this.S0(n, 2);
            this.l0(byteString);
        }
        
        @Override
        public final void l0(final ByteString byteString) {
            this.U0(byteString.size());
            byteString.writeTo(this);
        }
        
        @Override
        public final void q0(final int n, final int n2) {
            this.S0(n, 5);
            this.r0(n2);
        }
        
        @Override
        public final void r0(final int n) {
            try {
                final byte[] e = this.e;
                final int h = this.h;
                final int n2 = h + 1;
                e[h] = (byte)(n & 0xFF);
                final int n3 = n2 + 1;
                e[n2] = (byte)(n >> 8 & 0xFF);
                final int n4 = n3 + 1;
                e[n3] = (byte)(n >> 16 & 0xFF);
                this.h = n4 + 1;
                e[n4] = (byte)(n >> 24 & 0xFF);
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
            }
        }
        
        @Override
        public final void s0(final int n, final long n2) {
            this.S0(n, 1);
            this.t0(n2);
        }
        
        @Override
        public final void t0(final long n) {
            try {
                final byte[] e = this.e;
                final int h = this.h;
                final int n2 = h + 1;
                e[h] = (byte)((int)n & 0xFF);
                final int n3 = n2 + 1;
                e[n2] = (byte)((int)(n >> 8) & 0xFF);
                final int n4 = n3 + 1;
                e[n3] = (byte)((int)(n >> 16) & 0xFF);
                final int n5 = n4 + 1;
                e[n4] = (byte)((int)(n >> 24) & 0xFF);
                final int n6 = n5 + 1;
                e[n5] = (byte)((int)(n >> 32) & 0xFF);
                final int n7 = n6 + 1;
                e[n6] = (byte)((int)(n >> 40) & 0xFF);
                final int n8 = n7 + 1;
                e[n7] = (byte)((int)(n >> 48) & 0xFF);
                this.h = n8 + 1;
                e[n8] = (byte)((int)(n >> 56) & 0xFF);
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
            }
        }
    }
    
    public static final class d extends b
    {
        public final OutputStream i;
        
        public d(final OutputStream i, final int n) {
            super(n);
            if (i != null) {
                this.i = i;
                return;
            }
            throw new NullPointerException("out");
        }
        
        @Override
        public void A0(final int n, final int n2) {
            this.f1(20);
            ((b)this).b1(n, 0);
            ((b)this).a1(n2);
        }
        
        @Override
        public void B0(final int n) {
            if (n >= 0) {
                this.U0(n);
            }
            else {
                this.W0(n);
            }
        }
        
        @Override
        public void E0(final int n, final y y, final f0 f0) {
            this.S0(n, 2);
            this.j1(y, f0);
        }
        
        @Override
        public void F0(final y y) {
            this.U0(y.b());
            y.e(this);
        }
        
        @Override
        public void G0(final int n, final y y) {
            this.S0(1, 3);
            this.T0(2, n);
            this.i1(3, y);
            this.S0(1, 4);
        }
        
        @Override
        public void H0(final int n, final ByteString byteString) {
            this.S0(1, 3);
            this.T0(2, n);
            this.k0(3, byteString);
            this.S0(1, 4);
        }
        
        @Override
        public void Q0(final int n, final String s) {
            this.S0(n, 2);
            this.R0(s);
        }
        
        @Override
        public void R0(final String s) {
            try {
                final int n = s.length() * 3;
                final int t = CodedOutputStream.T(n);
                final int n2 = t + n;
                final int f = super.f;
                if (n2 > f) {
                    final byte[] array = new byte[n];
                    final int i = Utf8.i(s, array, 0, n);
                    this.U0(i);
                    this.b(array, 0, i);
                    return;
                }
                if (n2 > f - super.g) {
                    this.e1();
                }
                final int t2 = CodedOutputStream.T(s.length());
                final int g = super.g;
                Label_0150: {
                    if (t2 != t) {
                        break Label_0150;
                    }
                    final int g2 = g + t2;
                    try {
                        super.g = g2;
                        final int j = Utf8.i(s, super.e, g2, super.f - g2);
                        super.g = g;
                        int k = j - g - t2;
                        ((b)this).c1(k);
                        super.g = j;
                        super.h += k;
                        return;
                        k = Utf8.j(s);
                        ((b)this).c1(k);
                        super.g = Utf8.i(s, super.e, super.g, k);
                    }
                    catch (final ArrayIndexOutOfBoundsException ex) {
                        throw new OutOfSpaceException(ex);
                    }
                    catch (final Utf8.UnpairedSurrogateException ex2) {
                        try {
                            super.h -= super.g - g;
                            super.g = g;
                            throw ex2;
                        }
                        catch (final Utf8.UnpairedSurrogateException ex3) {
                            this.Z(s, ex3);
                        }
                    }
                }
            }
            catch (final Utf8.UnpairedSurrogateException ex4) {}
        }
        
        @Override
        public void S0(final int n, final int n2) {
            this.U0(WireFormat.c(n, n2));
        }
        
        @Override
        public void T0(final int n, final int n2) {
            this.f1(20);
            ((b)this).b1(n, 0);
            ((b)this).c1(n2);
        }
        
        @Override
        public void U0(final int n) {
            this.f1(5);
            ((b)this).c1(n);
        }
        
        @Override
        public void V0(final int n, final long n2) {
            this.f1(20);
            ((b)this).b1(n, 0);
            ((b)this).d1(n2);
        }
        
        @Override
        public void W0(final long n) {
            this.f1(10);
            ((b)this).d1(n);
        }
        
        @Override
        public void Y() {
            if (super.g > 0) {
                this.e1();
            }
        }
        
        @Override
        public void a(final ByteBuffer byteBuffer) {
            this.g1(byteBuffer);
        }
        
        @Override
        public void b(final byte[] array, final int n, final int n2) {
            this.h1(array, n, n2);
        }
        
        public final void e1() {
            this.i.write(super.e, 0, super.g);
            super.g = 0;
        }
        
        @Override
        public void f0(final byte b) {
            if (super.g == super.f) {
                this.e1();
            }
            ((b)this).X0(b);
        }
        
        public final void f1(final int n) {
            if (super.f - super.g < n) {
                this.e1();
            }
        }
        
        @Override
        public void g0(final int n, final boolean b) {
            this.f1(11);
            ((b)this).b1(n, 0);
            ((b)this).X0((byte)(b ? 1 : 0));
        }
        
        public void g1(final ByteBuffer byteBuffer) {
            int remaining = byteBuffer.remaining();
            final int f = super.f;
            final int g = super.g;
            if (f - g >= remaining) {
                byteBuffer.get(super.e, g, remaining);
                super.g += remaining;
            }
            else {
                final int length = f - g;
                byteBuffer.get(super.e, g, length);
                remaining -= length;
                super.g = super.f;
                super.h += length;
                this.e1();
                while (true) {
                    final int f2 = super.f;
                    if (remaining <= f2) {
                        break;
                    }
                    byteBuffer.get(super.e, 0, f2);
                    this.i.write(super.e, 0, super.f);
                    final int f3 = super.f;
                    remaining -= f3;
                    super.h += f3;
                }
                byteBuffer.get(super.e, 0, remaining);
                super.g = remaining;
            }
            super.h += remaining;
        }
        
        public void h1(final byte[] b, int off, int n) {
            final int f = super.f;
            final int g = super.g;
            if (f - g >= n) {
                System.arraycopy(b, off, super.e, g, n);
                super.g += n;
            }
            else {
                final int n2 = f - g;
                System.arraycopy(b, off, super.e, g, n2);
                off += n2;
                n -= n2;
                super.g = super.f;
                super.h += n2;
                this.e1();
                if (n <= super.f) {
                    System.arraycopy(b, off, super.e, 0, n);
                    super.g = n;
                }
                else {
                    this.i.write(b, off, n);
                }
            }
            super.h += n;
        }
        
        public void i1(final int n, final y y) {
            this.S0(n, 2);
            this.F0(y);
        }
        
        @Override
        public void j0(final byte[] array, final int n, final int n2) {
            this.U0(n2);
            this.h1(array, n, n2);
        }
        
        public void j1(final y y, final f0 f0) {
            this.U0(((a)y).j(f0));
            f0.g(y, super.a);
        }
        
        @Override
        public void k0(final int n, final ByteString byteString) {
            this.S0(n, 2);
            this.l0(byteString);
        }
        
        @Override
        public void l0(final ByteString byteString) {
            this.U0(byteString.size());
            byteString.writeTo(this);
        }
        
        @Override
        public void q0(final int n, final int n2) {
            this.f1(14);
            ((b)this).b1(n, 5);
            ((b)this).Y0(n2);
        }
        
        @Override
        public void r0(final int n) {
            this.f1(4);
            ((b)this).Y0(n);
        }
        
        @Override
        public void s0(final int n, final long n2) {
            this.f1(18);
            ((b)this).b1(n, 1);
            ((b)this).Z0(n2);
        }
        
        @Override
        public void t0(final long n) {
            this.f1(8);
            ((b)this).Z0(n);
        }
    }
}

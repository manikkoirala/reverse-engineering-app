// 
// Decompiled by Procyon v0.6.0
// 

package androidx.datastore;

import org.jetbrains.annotations.NotNull;
import java.io.File;
import android.content.Context;
import kotlin.jvm.internal.Lambda;

final class DataStoreSingletonDelegate$getValue$1$1 extends Lambda implements a90
{
    final Context $applicationContext;
    final mp this$0;
    
    public DataStoreSingletonDelegate$getValue$1$1(final Context $applicationContext, final mp mp) {
        this.$applicationContext = $applicationContext;
        super(0);
    }
    
    @NotNull
    public final File invoke() {
        final Context $applicationContext = this.$applicationContext;
        fg0.d((Object)$applicationContext, "applicationContext");
        return lp.a($applicationContext, mp.b(null));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import android.os.BaseBundle;
import java.util.Iterator;
import android.util.Log;
import androidx.lifecycle.f;
import androidx.lifecycle.Lifecycle;
import java.util.Collection;
import kotlin.random.Random;
import android.os.Parcelable;
import android.content.Intent;
import java.util.HashMap;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Map;

public abstract class ActivityResultRegistry
{
    public final Map a;
    public final Map b;
    public final Map c;
    public ArrayList d;
    public final transient Map e;
    public final Map f;
    public final Bundle g;
    
    public ActivityResultRegistry() {
        this.a = new HashMap();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new ArrayList();
        this.e = new HashMap();
        this.f = new HashMap();
        this.g = new Bundle();
    }
    
    public final void a(final int n, final String s) {
        this.a.put(n, s);
        this.b.put(s, n);
    }
    
    public final boolean b(final int i, final int n, final Intent intent) {
        final String s = this.a.get(i);
        if (s == null) {
            return false;
        }
        this.d(s, n, intent, (c)this.e.get(s));
        return true;
    }
    
    public final boolean c(final int i, final Object o) {
        final String o2 = this.a.get(i);
        if (o2 == null) {
            return false;
        }
        final c c = this.e.get(o2);
        if (c != null) {
            final m2 a = c.a;
            if (a != null) {
                if (this.d.remove(o2)) {
                    a.a(o);
                    return true;
                }
                return true;
            }
        }
        this.g.remove(o2);
        this.f.put(o2, o);
        return true;
    }
    
    public final void d(final String s, final int n, final Intent intent, final c c) {
        if (c != null && c.a != null && this.d.contains(s)) {
            c.a.a(c.b.c(n, intent));
            this.d.remove(s);
        }
        else {
            this.f.remove(s);
            this.g.putParcelable(s, (Parcelable)new l2(n, intent));
        }
    }
    
    public final int e() {
        int n = Random.Default.nextInt(2147418112);
        int i;
        while (true) {
            i = n + 65536;
            if (!this.a.containsKey(i)) {
                break;
            }
            n = Random.Default.nextInt(2147418112);
        }
        return i;
    }
    
    public abstract void f(final int p0, final n2 p1, final Object p2, final j2 p3);
    
    public final void g(final Bundle bundle) {
        if (bundle == null) {
            return;
        }
        final ArrayList integerArrayList = bundle.getIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS");
        final ArrayList stringArrayList = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS");
        if (stringArrayList != null) {
            if (integerArrayList != null) {
                this.d = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS");
                this.g.putAll(bundle.getBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT"));
                for (int i = 0; i < stringArrayList.size(); ++i) {
                    final String s = stringArrayList.get(i);
                    if (this.b.containsKey(s)) {
                        final Integer n = this.b.remove(s);
                        if (!((BaseBundle)this.g).containsKey(s)) {
                            this.a.remove(n);
                        }
                    }
                    this.a((int)integerArrayList.get(i), (String)stringArrayList.get(i));
                }
            }
        }
    }
    
    public final void h(final Bundle bundle) {
        bundle.putIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS", new ArrayList(this.b.values()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS", new ArrayList(this.b.keySet()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS", new ArrayList(this.d));
        bundle.putBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT", (Bundle)this.g.clone());
    }
    
    public final r2 i(final String s, final n2 n2, final m2 m2) {
        this.k(s);
        this.e.put(s, new c(m2, n2));
        if (this.f.containsKey(s)) {
            final Object value = this.f.get(s);
            this.f.remove(s);
            m2.a(value);
        }
        final l2 l2 = (l2)this.g.getParcelable(s);
        if (l2 != null) {
            this.g.remove(s);
            m2.a(n2.c(l2.c(), l2.b()));
        }
        return new r2(this, s, n2) {
            public final String a;
            public final n2 b;
            public final ActivityResultRegistry c;
            
            @Override
            public void b(final Object obj, final j2 j2) {
                final Integer n = this.c.b.get(this.a);
                if (n != null) {
                    this.c.d.add(this.a);
                    try {
                        this.c.f(n, this.b, obj, j2);
                        return;
                    }
                    catch (final Exception ex) {
                        this.c.d.remove(this.a);
                        throw ex;
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Attempting to launch an unregistered ActivityResultLauncher with contract ");
                sb.append(this.b);
                sb.append(" and input ");
                sb.append(obj);
                sb.append(". You must ensure the ActivityResultLauncher is registered before calling launch().");
                throw new IllegalStateException(sb.toString());
            }
            
            @Override
            public void c() {
                this.c.l(this.a);
            }
        };
    }
    
    public final r2 j(final String s, final qj0 obj, final n2 n2, final m2 m2) {
        final Lifecycle lifecycle = obj.getLifecycle();
        if (!lifecycle.b().isAtLeast(Lifecycle.State.STARTED)) {
            this.k(s);
            d d;
            if ((d = this.c.get(s)) == null) {
                d = new d(lifecycle);
            }
            d.a(new f(this, s, m2, n2) {
                public final String a;
                public final m2 b;
                public final n2 c;
                public final ActivityResultRegistry d;
                
                @Override
                public void d(final qj0 qj0, final Lifecycle.Event obj) {
                    if (Lifecycle.Event.ON_START.equals(obj)) {
                        this.d.e.put(this.a, new c(this.b, this.c));
                        if (this.d.f.containsKey(this.a)) {
                            final Object value = this.d.f.get(this.a);
                            this.d.f.remove(this.a);
                            this.b.a(value);
                        }
                        final l2 l2 = (l2)this.d.g.getParcelable(this.a);
                        if (l2 != null) {
                            this.d.g.remove(this.a);
                            this.b.a(this.c.c(l2.c(), l2.b()));
                        }
                    }
                    else if (Lifecycle.Event.ON_STOP.equals(obj)) {
                        this.d.e.remove(this.a);
                    }
                    else if (Lifecycle.Event.ON_DESTROY.equals(obj)) {
                        this.d.l(this.a);
                    }
                }
            });
            this.c.put(s, d);
            return new r2(this, s, n2) {
                public final String a;
                public final n2 b;
                public final ActivityResultRegistry c;
                
                @Override
                public void b(final Object obj, final j2 j2) {
                    final Integer n = this.c.b.get(this.a);
                    if (n != null) {
                        this.c.d.add(this.a);
                        try {
                            this.c.f(n, this.b, obj, j2);
                            return;
                        }
                        catch (final Exception ex) {
                            this.c.d.remove(this.a);
                            throw ex;
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Attempting to launch an unregistered ActivityResultLauncher with contract ");
                    sb.append(this.b);
                    sb.append(" and input ");
                    sb.append(obj);
                    sb.append(". You must ensure the ActivityResultLauncher is registered before calling launch().");
                    throw new IllegalStateException(sb.toString());
                }
                
                @Override
                public void c() {
                    this.c.l(this.a);
                }
            };
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("LifecycleOwner ");
        sb.append(obj);
        sb.append(" is attempting to register while current state is ");
        sb.append(lifecycle.b());
        sb.append(". LifecycleOwners must call register before they are STARTED.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final void k(final String s) {
        if (this.b.get(s) != null) {
            return;
        }
        this.a(this.e(), s);
    }
    
    public final void l(final String str) {
        if (!this.d.contains(str)) {
            final Integer n = this.b.remove(str);
            if (n != null) {
                this.a.remove(n);
            }
        }
        this.e.remove(str);
        if (this.f.containsKey(str)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Dropping pending result for request ");
            sb.append(str);
            sb.append(": ");
            sb.append(this.f.get(str));
            Log.w("ActivityResultRegistry", sb.toString());
            this.f.remove(str);
        }
        if (((BaseBundle)this.g).containsKey(str)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Dropping pending result for request ");
            sb2.append(str);
            sb2.append(": ");
            sb2.append(this.g.getParcelable(str));
            Log.w("ActivityResultRegistry", sb2.toString());
            this.g.remove(str);
        }
        final d d = this.c.get(str);
        if (d != null) {
            d.b();
            this.c.remove(str);
        }
    }
    
    public static class c
    {
        public final m2 a;
        public final n2 b;
        
        public c(final m2 a, final n2 b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static class d
    {
        public final Lifecycle a;
        public final ArrayList b;
        
        public d(final Lifecycle a) {
            this.a = a;
            this.b = new ArrayList();
        }
        
        public void a(final f e) {
            this.a.a(e);
            this.b.add(e);
        }
        
        public void b() {
            final Iterator iterator = this.b.iterator();
            while (iterator.hasNext()) {
                this.a.c((pj0)iterator.next());
            }
            this.b.clear();
        }
    }
}

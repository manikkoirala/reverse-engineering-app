// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.os.SystemClock;
import android.view.ViewTreeObserver$OnDrawListener;
import android.window.OnBackInvokedDispatcher;
import android.content.IntentSender;
import android.view.MenuItem;
import android.view.Menu;
import androidx.lifecycle.k;
import java.util.Iterator;
import android.content.res.Configuration;
import android.os.Build$VERSION;
import android.text.TextUtils;
import android.app.Application;
import androidx.lifecycle.m;
import android.view.ViewGroup$LayoutParams;
import androidx.savedstate.a;
import androidx.lifecycle.SavedStateHandleSupport;
import android.view.View;
import android.view.Window;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.f;
import android.os.Bundle;
import java.io.Serializable;
import android.content.Intent;
import android.content.IntentSender$SendIntentException;
import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.content.Context;
import java.util.concurrent.Executor;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.lifecycle.g;
import androidx.lifecycle.o;
import androidx.activity.result.ActivityResultRegistry;
import androidx.lifecycle.c;

public abstract class ComponentActivity extends ej implements pl, c42, androidx.lifecycle.c, aj1, j11, s2, x80
{
    private static final String ACTIVITY_RESULT_TAG = "android:support:activity-result";
    private final ActivityResultRegistry mActivityResultRegistry;
    private int mContentLayoutId;
    final ql mContextAwareHelper;
    private o.b mDefaultFactory;
    private boolean mDispatchingOnMultiWindowModeChanged;
    private boolean mDispatchingOnPictureInPictureModeChanged;
    final w80 mFullyDrawnReporter;
    private final androidx.lifecycle.g mLifecycleRegistry;
    private final lv0 mMenuHostHelper;
    private final AtomicInteger mNextLocalRequestCode;
    private OnBackPressedDispatcher mOnBackPressedDispatcher;
    private final CopyOnWriteArrayList<dl> mOnConfigurationChangedListeners;
    private final CopyOnWriteArrayList<dl> mOnMultiWindowModeChangedListeners;
    private final CopyOnWriteArrayList<dl> mOnNewIntentListeners;
    private final CopyOnWriteArrayList<dl> mOnPictureInPictureModeChangedListeners;
    private final CopyOnWriteArrayList<dl> mOnTrimMemoryListeners;
    final f mReportFullyDrawnExecutor;
    final zi1 mSavedStateRegistryController;
    private b42 mViewModelStore;
    
    public ComponentActivity() {
        this.mContextAwareHelper = new ql();
        this.mMenuHostHelper = new lv0(new aj(this));
        this.mLifecycleRegistry = new androidx.lifecycle.g(this);
        final zi1 a = zi1.a(this);
        this.mSavedStateRegistryController = a;
        this.mOnBackPressedDispatcher = null;
        final f g = this.g();
        this.mReportFullyDrawnExecutor = g;
        this.mFullyDrawnReporter = new w80(g, (a90)new bj(this));
        this.mNextLocalRequestCode = new AtomicInteger();
        this.mActivityResultRegistry = new ActivityResultRegistry(this) {
            public final ComponentActivity h;
            
            @Override
            public void f(final int n, final n2 n2, final Object o, final j2 j2) {
                final ComponentActivity h = this.h;
                final n2.a b = n2.b((Context)h, o);
                if (b != null) {
                    new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, b) {
                        public final int a;
                        public final n2.a b;
                        public final ComponentActivity$a c;
                        
                        @Override
                        public void run() {
                            this.c.c(this.a, this.b.a());
                        }
                    });
                    return;
                }
                final Intent a = n2.a((Context)h, o);
                if (a.getExtras() != null && a.getExtras().getClassLoader() == null) {
                    a.setExtrasClassLoader(((Context)h).getClassLoader());
                }
                Bundle bundleExtra;
                if (a.hasExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE")) {
                    bundleExtra = a.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                    a.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                }
                else {
                    bundleExtra = null;
                }
                if ("androidx.activity.result.contract.action.REQUEST_PERMISSIONS".equals(a.getAction())) {
                    String[] stringArrayExtra;
                    if ((stringArrayExtra = a.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS")) == null) {
                        stringArrayExtra = new String[0];
                    }
                    h2.g(h, stringArrayExtra, n);
                }
                else if ("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST".equals(a.getAction())) {
                    final yf0 yf0 = (yf0)a.getParcelableExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST");
                    try {
                        h2.l(h, yf0.e(), n, yf0.b(), yf0.c(), yf0.d(), 0, bundleExtra);
                    }
                    catch (final IntentSender$SendIntentException ex) {
                        new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, ex) {
                            public final int a;
                            public final IntentSender$SendIntentException b;
                            public final ComponentActivity$a c;
                            
                            @Override
                            public void run() {
                                this.c.b(this.a, 0, new Intent().setAction("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST").putExtra("androidx.activity.result.contract.extra.SEND_INTENT_EXCEPTION", (Serializable)this.b));
                            }
                        });
                    }
                }
                else {
                    h2.k(h, a, n, bundleExtra);
                }
            }
        };
        this.mOnConfigurationChangedListeners = new CopyOnWriteArrayList<dl>();
        this.mOnTrimMemoryListeners = new CopyOnWriteArrayList<dl>();
        this.mOnNewIntentListeners = new CopyOnWriteArrayList<dl>();
        this.mOnMultiWindowModeChangedListeners = new CopyOnWriteArrayList<dl>();
        this.mOnPictureInPictureModeChangedListeners = new CopyOnWriteArrayList<dl>();
        this.mDispatchingOnMultiWindowModeChanged = false;
        this.mDispatchingOnPictureInPictureModeChanged = false;
        if (this.getLifecycle() != null) {
            this.getLifecycle().a(new androidx.lifecycle.f(this) {
                public final ComponentActivity a;
                
                @Override
                public void d(final qj0 qj0, final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_STOP) {
                        final Window window = this.a.getWindow();
                        View peekDecorView;
                        if (window != null) {
                            peekDecorView = window.peekDecorView();
                        }
                        else {
                            peekDecorView = null;
                        }
                        if (peekDecorView != null) {
                            c.a(peekDecorView);
                        }
                    }
                }
            });
            this.getLifecycle().a(new androidx.lifecycle.f(this) {
                public final ComponentActivity a;
                
                @Override
                public void d(final qj0 qj0, final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_DESTROY) {
                        this.a.mContextAwareHelper.b();
                        if (!this.a.isChangingConfigurations()) {
                            this.a.getViewModelStore().a();
                        }
                        this.a.mReportFullyDrawnExecutor.K();
                    }
                }
            });
            this.getLifecycle().a(new androidx.lifecycle.f(this) {
                public final ComponentActivity a;
                
                @Override
                public void d(final qj0 qj0, final Lifecycle.Event event) {
                    this.a.ensureViewModelStore();
                    this.a.getLifecycle().c(this);
                }
            });
            a.c();
            SavedStateHandleSupport.c(this);
            this.getSavedStateRegistry().h("android:support:activity-result", (androidx.savedstate.a.c)new cj(this));
            this.addOnContextAvailableListener(new dj(this));
            return;
        }
        throw new IllegalStateException("getLifecycle() returned null in ComponentActivity's constructor. Please make sure you are lazily constructing your Lifecycle in the first call to getLifecycle() rather than relying on field initialization.");
    }
    
    public static /* synthetic */ void access$001(final ComponentActivity componentActivity) {
        componentActivity.onBackPressed();
    }
    
    public static /* synthetic */ OnBackPressedDispatcher access$100(final ComponentActivity componentActivity) {
        return componentActivity.mOnBackPressedDispatcher;
    }
    
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.g(this.getWindow().getDecorView());
        super.addContentView(view, viewGroup$LayoutParams);
    }
    
    public void addMenuProvider(final rv0 rv0) {
        this.mMenuHostHelper.c(rv0);
    }
    
    public void addMenuProvider(final rv0 rv0, final qj0 qj0) {
        this.mMenuHostHelper.d(rv0, qj0);
    }
    
    public void addMenuProvider(final rv0 rv0, final qj0 qj0, final Lifecycle.State state) {
        this.mMenuHostHelper.e(rv0, qj0, state);
    }
    
    public final void addOnConfigurationChangedListener(final dl e) {
        this.mOnConfigurationChangedListeners.add(e);
    }
    
    public final void addOnContextAvailableListener(final l11 l11) {
        this.mContextAwareHelper.a(l11);
    }
    
    public final void addOnMultiWindowModeChangedListener(final dl e) {
        this.mOnMultiWindowModeChangedListeners.add(e);
    }
    
    public final void addOnNewIntentListener(final dl e) {
        this.mOnNewIntentListeners.add(e);
    }
    
    public final void addOnPictureInPictureModeChangedListener(final dl e) {
        this.mOnPictureInPictureModeChangedListeners.add(e);
    }
    
    public final void addOnTrimMemoryListener(final dl e) {
        this.mOnTrimMemoryListeners.add(e);
    }
    
    public void ensureViewModelStore() {
        if (this.mViewModelStore == null) {
            final e e = (e)this.getLastNonConfigurationInstance();
            if (e != null) {
                this.mViewModelStore = e.b;
            }
            if (this.mViewModelStore == null) {
                this.mViewModelStore = new b42();
            }
        }
    }
    
    public final f g() {
        return (f)new g();
    }
    
    @Override
    public final ActivityResultRegistry getActivityResultRegistry() {
        return this.mActivityResultRegistry;
    }
    
    @Override
    public do getDefaultViewModelCreationExtras() {
        final sx0 sx0 = new sx0();
        if (this.getApplication() != null) {
            sx0.c(o.a.h, this.getApplication());
        }
        sx0.c(SavedStateHandleSupport.a, this);
        sx0.c(SavedStateHandleSupport.b, this);
        if (this.getIntent() != null && this.getIntent().getExtras() != null) {
            sx0.c(SavedStateHandleSupport.c, this.getIntent().getExtras());
        }
        return sx0;
    }
    
    public o.b getDefaultViewModelProviderFactory() {
        if (this.mDefaultFactory == null) {
            final Application application = this.getApplication();
            Bundle extras;
            if (this.getIntent() != null) {
                extras = this.getIntent().getExtras();
            }
            else {
                extras = null;
            }
            this.mDefaultFactory = new m(application, this, extras);
        }
        return this.mDefaultFactory;
    }
    
    public w80 getFullyDrawnReporter() {
        return this.mFullyDrawnReporter;
    }
    
    @Deprecated
    public Object getLastCustomNonConfigurationInstance() {
        final e e = (e)this.getLastNonConfigurationInstance();
        Object a;
        if (e != null) {
            a = e.a;
        }
        else {
            a = null;
        }
        return a;
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    @Override
    public final OnBackPressedDispatcher getOnBackPressedDispatcher() {
        if (this.mOnBackPressedDispatcher == null) {
            this.mOnBackPressedDispatcher = new OnBackPressedDispatcher(new Runnable(this) {
                public final ComponentActivity a;
                
                @Override
                public void run() {
                    try {
                        ComponentActivity.access$001(this.a);
                    }
                    catch (final NullPointerException ex) {
                        if (!TextUtils.equals((CharSequence)ex.getMessage(), (CharSequence)"Attempt to invoke virtual method 'android.os.Handler android.app.FragmentHostCallback.getHandler()' on a null object reference")) {
                            throw ex;
                        }
                    }
                    catch (final IllegalStateException ex2) {
                        if (!TextUtils.equals((CharSequence)ex2.getMessage(), (CharSequence)"Can not perform this action after onSaveInstanceState")) {
                            throw ex2;
                        }
                    }
                }
            });
            this.getLifecycle().a(new androidx.lifecycle.f(this) {
                public final ComponentActivity a;
                
                @Override
                public void d(final qj0 qj0, final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_CREATE && Build$VERSION.SDK_INT >= 33) {
                        ComponentActivity.access$100(this.a).n(d.a((Activity)qj0));
                    }
                }
            });
        }
        return this.mOnBackPressedDispatcher;
    }
    
    @Override
    public final androidx.savedstate.a getSavedStateRegistry() {
        return this.mSavedStateRegistryController.b();
    }
    
    @Override
    public b42 getViewModelStore() {
        if (this.getApplication() != null) {
            this.ensureViewModelStore();
            return this.mViewModelStore;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }
    
    public void initializeViewTreeOwners() {
        o42.a(this.getWindow().getDecorView(), this);
        r42.a(this.getWindow().getDecorView(), this);
        q42.a(this.getWindow().getDecorView(), this);
        p42.a(this.getWindow().getDecorView(), this);
        n42.a(this.getWindow().getDecorView(), this);
    }
    
    public void invalidateMenu() {
        this.invalidateOptionsMenu();
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (!this.mActivityResultRegistry.b(n, n2, intent)) {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Deprecated
    public void onBackPressed() {
        this.getOnBackPressedDispatcher().k();
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final Iterator<dl> iterator = this.mOnConfigurationChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(configuration);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.mSavedStateRegistryController.d(bundle);
        this.mContextAwareHelper.c((Context)this);
        super.onCreate(bundle);
        k.e(this);
        final int mContentLayoutId = this.mContentLayoutId;
        if (mContentLayoutId != 0) {
            this.setContentView(mContentLayoutId);
        }
    }
    
    public boolean onCreatePanelMenu(final int n, final Menu menu) {
        if (n == 0) {
            super.onCreatePanelMenu(n, menu);
            this.mMenuHostHelper.h(menu, this.getMenuInflater());
        }
        return true;
    }
    
    public boolean onMenuItemSelected(final int n, final MenuItem menuItem) {
        return super.onMenuItemSelected(n, menuItem) || (n == 0 && this.mMenuHostHelper.j(menuItem));
    }
    
    public void onMultiWindowModeChanged(final boolean b) {
        if (this.mDispatchingOnMultiWindowModeChanged) {
            return;
        }
        final Iterator<dl> iterator = this.mOnMultiWindowModeChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(new qx0(b));
        }
    }
    
    public void onMultiWindowModeChanged(final boolean b, final Configuration configuration) {
        this.mDispatchingOnMultiWindowModeChanged = true;
        try {
            super.onMultiWindowModeChanged(b, configuration);
            this.mDispatchingOnMultiWindowModeChanged = false;
            final Iterator<dl> iterator = this.mOnMultiWindowModeChangedListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().accept(new qx0(b, configuration));
            }
        }
        finally {
            this.mDispatchingOnMultiWindowModeChanged = false;
        }
    }
    
    public void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        final Iterator<dl> iterator = this.mOnNewIntentListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(intent);
        }
    }
    
    public void onPanelClosed(final int n, final Menu menu) {
        this.mMenuHostHelper.i(menu);
        super.onPanelClosed(n, menu);
    }
    
    public void onPictureInPictureModeChanged(final boolean b) {
        if (this.mDispatchingOnPictureInPictureModeChanged) {
            return;
        }
        final Iterator<dl> iterator = this.mOnPictureInPictureModeChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(new k51(b));
        }
    }
    
    public void onPictureInPictureModeChanged(final boolean b, final Configuration configuration) {
        this.mDispatchingOnPictureInPictureModeChanged = true;
        try {
            super.onPictureInPictureModeChanged(b, configuration);
            this.mDispatchingOnPictureInPictureModeChanged = false;
            final Iterator<dl> iterator = this.mOnPictureInPictureModeChangedListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().accept(new k51(b, configuration));
            }
        }
        finally {
            this.mDispatchingOnPictureInPictureModeChanged = false;
        }
    }
    
    public boolean onPreparePanel(final int n, final View view, final Menu menu) {
        if (n == 0) {
            super.onPreparePanel(n, view, menu);
            this.mMenuHostHelper.k(menu);
        }
        return true;
    }
    
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        if (!this.mActivityResultRegistry.b(n, -1, new Intent().putExtra("androidx.activity.result.contract.extra.PERMISSIONS", array).putExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS", array2))) {
            super.onRequestPermissionsResult(n, array, array2);
        }
    }
    
    @Deprecated
    public Object onRetainCustomNonConfigurationInstance() {
        return null;
    }
    
    public final Object onRetainNonConfigurationInstance() {
        final Object onRetainCustomNonConfigurationInstance = this.onRetainCustomNonConfigurationInstance();
        b42 b43;
        final b42 b42 = b43 = this.mViewModelStore;
        if (b42 == null) {
            final e e = (e)this.getLastNonConfigurationInstance();
            b43 = b42;
            if (e != null) {
                b43 = e.b;
            }
        }
        if (b43 == null && onRetainCustomNonConfigurationInstance == null) {
            return null;
        }
        final e e2 = new e();
        e2.a = onRetainCustomNonConfigurationInstance;
        e2.b = b43;
        return e2;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        final Lifecycle lifecycle = this.getLifecycle();
        if (lifecycle instanceof androidx.lifecycle.g) {
            ((androidx.lifecycle.g)lifecycle).n(Lifecycle.State.CREATED);
        }
        super.onSaveInstanceState(bundle);
        this.mSavedStateRegistryController.e(bundle);
    }
    
    public void onTrimMemory(final int i) {
        super.onTrimMemory(i);
        final Iterator<dl> iterator = this.mOnTrimMemoryListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(i);
        }
    }
    
    public Context peekAvailableContext() {
        return this.mContextAwareHelper.d();
    }
    
    public final <I, O> r2 registerForActivityResult(final n2 n2, final ActivityResultRegistry activityResultRegistry, final m2 m2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("activity_rq#");
        sb.append(this.mNextLocalRequestCode.getAndIncrement());
        return activityResultRegistry.j(sb.toString(), this, n2, m2);
    }
    
    public final <I, O> r2 registerForActivityResult(final n2 n2, final m2 m2) {
        return this.registerForActivityResult(n2, this.mActivityResultRegistry, m2);
    }
    
    public void removeMenuProvider(final rv0 rv0) {
        this.mMenuHostHelper.l(rv0);
    }
    
    public final void removeOnConfigurationChangedListener(final dl o) {
        this.mOnConfigurationChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnContextAvailableListener(final l11 l11) {
        this.mContextAwareHelper.e(l11);
    }
    
    public final void removeOnMultiWindowModeChangedListener(final dl o) {
        this.mOnMultiWindowModeChangedListeners.remove(o);
    }
    
    public final void removeOnNewIntentListener(final dl o) {
        this.mOnNewIntentListeners.remove(o);
    }
    
    public final void removeOnPictureInPictureModeChangedListener(final dl o) {
        this.mOnPictureInPictureModeChangedListeners.remove(o);
    }
    
    public final void removeOnTrimMemoryListener(final dl o) {
        this.mOnTrimMemoryListeners.remove(o);
    }
    
    public void reportFullyDrawn() {
        try {
            if (hy1.d()) {
                hy1.a("reportFullyDrawn() for ComponentActivity");
            }
            super.reportFullyDrawn();
            this.mFullyDrawnReporter.c();
        }
        finally {
            hy1.b();
        }
    }
    
    public void setContentView(final int contentView) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.g(this.getWindow().getDecorView());
        super.setContentView(contentView);
    }
    
    public void setContentView(final View contentView) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.g(this.getWindow().getDecorView());
        super.setContentView(contentView);
    }
    
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.g(this.getWindow().getDecorView());
        super.setContentView(view, viewGroup$LayoutParams);
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n) {
        super.startActivityForResult(intent, n);
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n, final Bundle bundle) {
        super.startActivityForResult(intent, n, bundle);
    }
    
    @Deprecated
    public void startIntentSenderForResult(final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4) {
        super.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4);
    }
    
    @Deprecated
    public void startIntentSenderForResult(final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) {
        super.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    public abstract static class c
    {
        public static void a(final View view) {
            view.cancelPendingInputEvents();
        }
    }
    
    public abstract static class d
    {
        public static OnBackInvokedDispatcher a(final Activity activity) {
            return activity.getOnBackInvokedDispatcher();
        }
    }
    
    public static final class e
    {
        public Object a;
        public b42 b;
    }
    
    public interface f extends Executor
    {
        void K();
        
        void g(final View p0);
    }
    
    public class g implements f, ViewTreeObserver$OnDrawListener, Runnable
    {
        public final long a;
        public Runnable b;
        public boolean c;
        public final ComponentActivity d;
        
        public g(final ComponentActivity d) {
            this.d = d;
            this.a = SystemClock.uptimeMillis() + 10000L;
            this.c = false;
        }
        
        @Override
        public void K() {
            this.d.getWindow().getDecorView().removeCallbacks((Runnable)this);
            this.d.getWindow().getDecorView().getViewTreeObserver().removeOnDrawListener((ViewTreeObserver$OnDrawListener)this);
        }
        
        public void execute(final Runnable b) {
            this.b = b;
            final View decorView = this.d.getWindow().getDecorView();
            if (this.c) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    decorView.invalidate();
                }
                else {
                    decorView.postInvalidate();
                }
            }
            else {
                decorView.postOnAnimation((Runnable)new fj(this));
            }
        }
        
        @Override
        public void g(final View view) {
            if (!this.c) {
                this.c = true;
                view.getViewTreeObserver().addOnDrawListener((ViewTreeObserver$OnDrawListener)this);
            }
        }
        
        public void onDraw() {
            final Runnable b = this.b;
            if (b != null) {
                b.run();
                this.b = null;
                if (!this.d.mFullyDrawnReporter.d()) {
                    return;
                }
            }
            else if (SystemClock.uptimeMillis() <= this.a) {
                return;
            }
            this.c = false;
            this.d.getWindow().getDecorView().post((Runnable)this);
        }
        
        public void run() {
            this.d.getWindow().getDecorView().getViewTreeObserver().removeOnDrawListener((ViewTreeObserver$OnDrawListener)this);
        }
    }
}

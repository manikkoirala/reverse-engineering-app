// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import org.jetbrains.annotations.Nullable;
import kotlin.jvm.internal.Lambda;

public final class ContextAwareKt$withContextAvailable$2$1 extends Lambda implements c90
{
    final rl $listener;
    final pl $this_withContextAvailable;
    
    public ContextAwareKt$withContextAvailable$2$1(final pl $this_withContextAvailable, final rl rl) {
        this.$this_withContextAvailable = $this_withContextAvailable;
        super(1);
    }
    
    public final void invoke(@Nullable final Throwable t) {
        this.$this_withContextAvailable.removeOnContextAvailableListener(null);
    }
}

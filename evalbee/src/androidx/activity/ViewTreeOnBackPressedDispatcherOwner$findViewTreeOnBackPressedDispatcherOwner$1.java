// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import org.jetbrains.annotations.Nullable;
import android.view.ViewParent;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.jvm.internal.Lambda;

final class ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$1 extends Lambda implements c90
{
    public static final ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$1 INSTANCE;
    
    static {
        INSTANCE = new ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$1();
    }
    
    public ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$1() {
        super(1);
    }
    
    @Nullable
    public final View invoke(@NotNull View view) {
        fg0.e((Object)view, "it");
        final ViewParent parent = view.getParent();
        if (parent instanceof View) {
            view = (View)parent;
        }
        else {
            view = null;
        }
        return view;
    }
}

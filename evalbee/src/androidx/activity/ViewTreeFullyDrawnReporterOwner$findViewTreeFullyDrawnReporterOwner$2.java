// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.jvm.internal.Lambda;

final class ViewTreeFullyDrawnReporterOwner$findViewTreeFullyDrawnReporterOwner$2 extends Lambda implements c90
{
    public static final ViewTreeFullyDrawnReporterOwner$findViewTreeFullyDrawnReporterOwner$2 INSTANCE;
    
    static {
        INSTANCE = new ViewTreeFullyDrawnReporterOwner$findViewTreeFullyDrawnReporterOwner$2();
    }
    
    public ViewTreeFullyDrawnReporterOwner$findViewTreeFullyDrawnReporterOwner$2() {
        super(1);
    }
    
    @Nullable
    public final x80 invoke(@NotNull final View view) {
        fg0.e((Object)view, "it");
        final Object tag = view.getTag(mb1.a);
        x80 x80;
        if (tag instanceof x80) {
            x80 = (x80)tag;
        }
        else {
            x80 = null;
        }
        return x80;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.jvm.internal.Lambda;

final class ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$2 extends Lambda implements c90
{
    public static final ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$2 INSTANCE;
    
    static {
        INSTANCE = new ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$2();
    }
    
    public ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$2() {
        super(1);
    }
    
    @Nullable
    public final j11 invoke(@NotNull final View view) {
        fg0.e((Object)view, "it");
        final Object tag = view.getTag(mb1.b);
        j11 j11;
        if (tag instanceof j11) {
            j11 = (j11)tag;
        }
        else {
            j11 = null;
        }
        return j11;
    }
}

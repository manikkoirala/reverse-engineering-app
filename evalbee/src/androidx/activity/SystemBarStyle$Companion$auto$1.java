// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import org.jetbrains.annotations.NotNull;
import android.content.res.Resources;
import kotlin.jvm.internal.Lambda;

final class SystemBarStyle$Companion$auto$1 extends Lambda implements c90
{
    public static final SystemBarStyle$Companion$auto$1 INSTANCE;
    
    static {
        INSTANCE = new SystemBarStyle$Companion$auto$1();
    }
    
    public SystemBarStyle$Companion$auto$1() {
        super(1);
    }
    
    @NotNull
    public final Boolean invoke(@NotNull final Resources resources) {
        fg0.e((Object)resources, "resources");
        return (resources.getConfiguration().uiMode & 0x30) == 0x20;
    }
}

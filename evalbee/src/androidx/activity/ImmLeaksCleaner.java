// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.view.View;
import androidx.lifecycle.Lifecycle;
import android.view.inputmethod.InputMethodManager;
import android.app.Activity;
import java.lang.reflect.Field;
import androidx.lifecycle.f;

final class ImmLeaksCleaner implements f
{
    public static int b;
    public static Field c;
    public static Field d;
    public static Field e;
    public Activity a;
    
    public static void f() {
        try {
            ImmLeaksCleaner.b = 2;
            (ImmLeaksCleaner.d = InputMethodManager.class.getDeclaredField("mServedView")).setAccessible(true);
            (ImmLeaksCleaner.e = InputMethodManager.class.getDeclaredField("mNextServedView")).setAccessible(true);
            (ImmLeaksCleaner.c = InputMethodManager.class.getDeclaredField("mH")).setAccessible(true);
            ImmLeaksCleaner.b = 1;
        }
        catch (final NoSuchFieldException ex) {}
    }
    
    @Override
    public void d(qj0 value, final Lifecycle.Event event) {
        if (event != Lifecycle.Event.ON_DESTROY) {
            return;
        }
        if (ImmLeaksCleaner.b == 0) {
            f();
        }
        if (ImmLeaksCleaner.b != 1) {
            return;
        }
        final InputMethodManager obj = (InputMethodManager)this.a.getSystemService("input_method");
        try {
            value = (qj0)ImmLeaksCleaner.c.get(obj);
            if (value == null) {
                return;
            }
            monitorenter(value);
            try {
                try {
                    final View view = (View)ImmLeaksCleaner.d.get(obj);
                    if (view == null) {
                        monitorexit(value);
                        return;
                    }
                    if (view.isAttachedToWindow()) {
                        monitorexit(value);
                        return;
                    }
                    try {
                        ImmLeaksCleaner.e.set(obj, null);
                        monitorexit(value);
                        obj.isActive();
                    }
                    catch (final IllegalAccessException ex) {
                        monitorexit(value);
                    }
                }
                finally {
                    monitorexit(value);
                }
            }
            catch (final ClassCastException ex2) {}
            catch (final IllegalAccessException ex3) {}
        }
        catch (final IllegalAccessException ex4) {}
    }
}

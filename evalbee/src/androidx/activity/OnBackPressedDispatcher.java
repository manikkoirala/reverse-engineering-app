// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import java.util.List;
import android.window.BackEvent;
import android.window.OnBackAnimationCallback;
import androidx.lifecycle.f;
import java.util.Iterator;
import java.util.Collection;
import java.util.ListIterator;
import androidx.lifecycle.Lifecycle;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Lambda;
import android.os.Build$VERSION;
import android.window.OnBackInvokedDispatcher;
import android.window.OnBackInvokedCallback;

public final class OnBackPressedDispatcher
{
    public final Runnable a;
    public final dl b;
    public final j8 c;
    public h11 d;
    public OnBackInvokedCallback e;
    public OnBackInvokedDispatcher f;
    public boolean g;
    public boolean h;
    
    public OnBackPressedDispatcher(final Runnable runnable) {
        this(runnable, null);
    }
    
    public OnBackPressedDispatcher(final Runnable a, final dl b) {
        this.a = a;
        this.b = b;
        this.c = new j8();
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 33) {
            OnBackInvokedCallback e;
            if (sdk_INT >= 34) {
                e = OnBackPressedDispatcher.b.a.a((c90)new c90(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke(@NotNull final xa xa) {
                        fg0.e((Object)xa, "backEvent");
                        this.this$0.m(xa);
                    }
                }, (c90)new c90(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke(@NotNull final xa xa) {
                        fg0.e((Object)xa, "backEvent");
                        this.this$0.l(xa);
                    }
                }, (a90)new a90(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke() {
                        this.this$0.k();
                    }
                }, (a90)new a90(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke() {
                        this.this$0.j();
                    }
                });
            }
            else {
                e = OnBackPressedDispatcher.a.a.b((a90)new a90(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke() {
                        this.this$0.k();
                    }
                });
            }
            this.e = e;
        }
    }
    
    public static final /* synthetic */ h11 a(final OnBackPressedDispatcher onBackPressedDispatcher) {
        return onBackPressedDispatcher.d;
    }
    
    public static final /* synthetic */ j8 b(final OnBackPressedDispatcher onBackPressedDispatcher) {
        return onBackPressedDispatcher.c;
    }
    
    public static final /* synthetic */ void f(final OnBackPressedDispatcher onBackPressedDispatcher, final h11 d) {
        onBackPressedDispatcher.d = d;
    }
    
    public final void h(final qj0 qj0, final h11 h11) {
        fg0.e((Object)qj0, "owner");
        fg0.e((Object)h11, "onBackPressedCallback");
        final Lifecycle lifecycle = qj0.getLifecycle();
        if (lifecycle.b() == Lifecycle.State.DESTROYED) {
            return;
        }
        h11.a(new LifecycleOnBackPressedCancellable(lifecycle, h11));
        this.p();
        h11.k((a90)new OnBackPressedDispatcher$addCallback.OnBackPressedDispatcher$addCallback$1((Object)this));
    }
    
    public final cf i(final h11 h11) {
        fg0.e((Object)h11, "onBackPressedCallback");
        this.c.add((Object)h11);
        final c c = new c(h11);
        h11.a(c);
        this.p();
        h11.k((a90)new OnBackPressedDispatcher$addCancellableCallback.OnBackPressedDispatcher$addCancellableCallback$1((Object)this));
        return c;
    }
    
    public final void j() {
        final j8 c = this.c;
        final ListIterator listIterator = ((List)c).listIterator(((List)c).size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((h11)previous).g()) {
                    final h11 h11 = (h11)previous;
                    this.d = null;
                    if (h11 != null) {
                        h11.c();
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    public final void k() {
        final j8 c = this.c;
        final ListIterator listIterator = ((List)c).listIterator(((List)c).size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((h11)previous).g()) {
                    final h11 h11 = (h11)previous;
                    this.d = null;
                    if (h11 != null) {
                        h11.d();
                        return;
                    }
                    final Runnable a = this.a;
                    if (a != null) {
                        a.run();
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    public final void l(final xa xa) {
        final j8 c = this.c;
        final ListIterator listIterator = ((List)c).listIterator(((List)c).size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((h11)previous).g()) {
                    final h11 h11 = (h11)previous;
                    if (h11 != null) {
                        h11.e(xa);
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    public final void m(final xa xa) {
        final j8 c = this.c;
        final ListIterator listIterator = ((List)c).listIterator(((List)c).size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((h11)previous).g()) {
                    final h11 d = (h11)previous;
                    this.d = d;
                    if (d != null) {
                        d.f(xa);
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    public final void n(final OnBackInvokedDispatcher f) {
        fg0.e((Object)f, "invoker");
        this.f = f;
        this.o(this.h);
    }
    
    public final void o(final boolean b) {
        final OnBackInvokedDispatcher f = this.f;
        final OnBackInvokedCallback e = this.e;
        if (f != null && e != null) {
            if (b && !this.g) {
                OnBackPressedDispatcher.a.a.d(f, 0, e);
                this.g = true;
            }
            else if (!b && this.g) {
                OnBackPressedDispatcher.a.a.e(f, e);
                this.g = false;
            }
        }
    }
    
    public final void p() {
        final boolean h = this.h;
        final j8 c = this.c;
        final boolean b = c instanceof Collection;
        final boolean b2 = false;
        boolean b3 = false;
        Label_0077: {
            if (b && ((Collection)c).isEmpty()) {
                b3 = b2;
            }
            else {
                final Iterator iterator = ((Iterable)c).iterator();
                do {
                    b3 = b2;
                    if (iterator.hasNext()) {
                        continue;
                    }
                    break Label_0077;
                } while (!((h11)iterator.next()).g());
                b3 = true;
            }
        }
        if ((this.h = b3) != h) {
            final dl b4 = this.b;
            if (b4 != null) {
                b4.accept(b3);
            }
            if (Build$VERSION.SDK_INT >= 33) {
                this.o(b3);
            }
        }
    }
    
    public final class LifecycleOnBackPressedCancellable implements f, cf
    {
        public final Lifecycle a;
        public final h11 b;
        public cf c;
        public final OnBackPressedDispatcher d;
        
        public LifecycleOnBackPressedCancellable(final OnBackPressedDispatcher d, final Lifecycle a, final h11 b) {
            fg0.e((Object)a, "lifecycle");
            fg0.e((Object)b, "onBackPressedCallback");
            this.d = d;
            this.a = a;
            this.b = b;
            a.a(this);
        }
        
        @Override
        public void cancel() {
            this.a.c(this);
            this.b.i(this);
            final cf c = this.c;
            if (c != null) {
                c.cancel();
            }
            this.c = null;
        }
        
        @Override
        public void d(final qj0 qj0, final Lifecycle.Event event) {
            fg0.e((Object)qj0, "source");
            fg0.e((Object)event, "event");
            if (event == Lifecycle.Event.ON_START) {
                this.c = this.d.i(this.b);
            }
            else if (event == Lifecycle.Event.ON_STOP) {
                final cf c = this.c;
                if (c != null) {
                    c.cancel();
                }
            }
            else if (event == Lifecycle.Event.ON_DESTROY) {
                this.cancel();
            }
        }
    }
    
    public static final class a
    {
        public static final a a;
        
        static {
            a = new a();
        }
        
        public static final void c(final a90 a90) {
            fg0.e((Object)a90, "$onBackInvoked");
            a90.invoke();
        }
        
        @NotNull
        public final OnBackInvokedCallback b(@NotNull final a90 a90) {
            fg0.e((Object)a90, "onBackInvoked");
            return new i11(a90);
        }
        
        public final void d(@NotNull final Object o, final int n, @NotNull final Object o2) {
            fg0.e(o, "dispatcher");
            fg0.e(o2, "callback");
            ((OnBackInvokedDispatcher)o).registerOnBackInvokedCallback(n, (OnBackInvokedCallback)o2);
        }
        
        public final void e(@NotNull final Object o, @NotNull final Object o2) {
            fg0.e(o, "dispatcher");
            fg0.e(o2, "callback");
            ((OnBackInvokedDispatcher)o).unregisterOnBackInvokedCallback((OnBackInvokedCallback)o2);
        }
    }
    
    public static final class b
    {
        public static final b a;
        
        static {
            a = new b();
        }
        
        @NotNull
        public final OnBackInvokedCallback a(@NotNull final c90 c90, @NotNull final c90 c91, @NotNull final a90 a90, @NotNull final a90 a91) {
            fg0.e((Object)c90, "onBackStarted");
            fg0.e((Object)c91, "onBackProgressed");
            fg0.e((Object)a90, "onBackInvoked");
            fg0.e((Object)a91, "onBackCancelled");
            return new OnBackAnimationCallback(c90, c91, a90, a91) {
                public final c90 a;
                public final c90 b;
                public final a90 c;
                public final a90 d;
                
                public void onBackCancelled() {
                    this.d.invoke();
                }
                
                public void onBackInvoked() {
                    this.c.invoke();
                }
                
                public void onBackProgressed(final BackEvent backEvent) {
                    fg0.e((Object)backEvent, "backEvent");
                    this.b.invoke((Object)new xa(backEvent));
                }
                
                public void onBackStarted(final BackEvent backEvent) {
                    fg0.e((Object)backEvent, "backEvent");
                    this.a.invoke((Object)new xa(backEvent));
                }
            };
        }
    }
    
    public final class c implements cf
    {
        public final h11 a;
        public final OnBackPressedDispatcher b;
        
        public c(final OnBackPressedDispatcher b, final h11 a) {
            fg0.e((Object)a, "onBackPressedCallback");
            this.b = b;
            this.a = a;
        }
        
        @Override
        public void cancel() {
            OnBackPressedDispatcher.b(this.b).remove((Object)this.a);
            if (fg0.a((Object)OnBackPressedDispatcher.a(this.b), (Object)this.a)) {
                this.a.c();
                OnBackPressedDispatcher.f(this.b, null);
            }
            this.a.i(this);
            final a90 b = this.a.b();
            if (b != null) {
                b.invoke();
            }
            this.a.k(null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.res.AssetManager;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import android.content.pm.PackageInfo;
import java.util.concurrent.Executor;
import android.content.Context;
import java.io.File;
import android.util.Log;

public abstract class c
{
    public static final c a;
    public static final c b;
    
    static {
        a = (c)new c() {
            @Override
            public void a(final int n, final Object o) {
            }
            
            @Override
            public void b(final int n, final Object o) {
            }
        };
        b = (c)new c() {
            @Override
            public void a(final int n, final Object o) {
                String s = null;
                switch (n) {
                    default: {
                        s = "";
                        break;
                    }
                    case 11: {
                        s = "RESULT_DELETE_SKIP_FILE_SUCCESS";
                        break;
                    }
                    case 10: {
                        s = "RESULT_INSTALL_SKIP_FILE_SUCCESS";
                        break;
                    }
                    case 8: {
                        s = "RESULT_PARSE_EXCEPTION";
                        break;
                    }
                    case 7: {
                        s = "RESULT_IO_EXCEPTION";
                        break;
                    }
                    case 6: {
                        s = "RESULT_BASELINE_PROFILE_NOT_FOUND";
                        break;
                    }
                    case 5: {
                        s = "RESULT_DESIRED_FORMAT_UNSUPPORTED";
                        break;
                    }
                    case 4: {
                        s = "RESULT_NOT_WRITABLE";
                        break;
                    }
                    case 3: {
                        s = "RESULT_UNSUPPORTED_ART_VERSION";
                        break;
                    }
                    case 2: {
                        s = "RESULT_ALREADY_INSTALLED";
                        break;
                    }
                    case 1: {
                        s = "RESULT_INSTALL_SUCCESS";
                        break;
                    }
                }
                if (n != 6 && n != 7 && n != 8) {
                    Log.d("ProfileInstaller", s);
                }
                else {
                    Log.e("ProfileInstaller", s, (Throwable)o);
                }
            }
            
            @Override
            public void b(final int n, final Object o) {
                String s;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            if (n != 4) {
                                if (n != 5) {
                                    s = "";
                                }
                                else {
                                    s = "DIAGNOSTIC_PROFILE_IS_COMPRESSED";
                                }
                            }
                            else {
                                s = "DIAGNOSTIC_REF_PROFILE_DOES_NOT_EXIST";
                            }
                        }
                        else {
                            s = "DIAGNOSTIC_REF_PROFILE_EXISTS";
                        }
                    }
                    else {
                        s = "DIAGNOSTIC_CURRENT_PROFILE_DOES_NOT_EXIST";
                    }
                }
                else {
                    s = "DIAGNOSTIC_CURRENT_PROFILE_EXISTS";
                }
                Log.d("ProfileInstaller", s);
            }
        };
    }
    
    public static boolean b(final File parent) {
        return new File(parent, "profileinstaller_profileWrittenFor_lastUpdateTime.dat").delete();
    }
    
    public static void c(final Context context, final Executor executor, final c c) {
        b(context.getFilesDir());
        g(executor, c, 11, null);
    }
    
    public static boolean d(final PackageInfo packageInfo, File parent, final c c) {
        final File file = new File(parent, "profileinstaller_profileWrittenFor_lastUpdateTime.dat");
        final boolean exists = file.exists();
        boolean b = false;
        if (!exists) {
            return false;
        }
        try {
            parent = (File)new DataInputStream(new FileInputStream(file));
            try {
                final long long1 = ((DataInputStream)parent).readLong();
                ((InputStream)parent).close();
                if (long1 == packageInfo.lastUpdateTime) {
                    b = true;
                }
                if (b) {
                    c.a(2, null);
                }
                return b;
            }
            finally {
                try {
                    ((InputStream)parent).close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)packageInfo).addSuppressed(exception);
                }
            }
        }
        catch (final IOException ex) {
            return false;
        }
    }
    
    public static void f(final PackageInfo packageInfo, File parent) {
        final File file = new File(parent, "profileinstaller_profileWrittenFor_lastUpdateTime.dat");
        try {
            parent = (File)new DataOutputStream(new FileOutputStream(file));
            try {
                ((DataOutputStream)parent).writeLong(packageInfo.lastUpdateTime);
                ((OutputStream)parent).close();
            }
            finally {
                try {
                    ((OutputStream)parent).close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)packageInfo).addSuppressed(exception);
                }
            }
        }
        catch (final IOException ex) {}
    }
    
    public static void g(final Executor executor, final c c, final int n, final Object o) {
        executor.execute(new v81(c, n, o));
    }
    
    public static boolean h(final AssetManager assetManager, final String child, final PackageInfo packageInfo, final File file, final String s, final Executor executor, final c c) {
        final b b = new b(assetManager, executor, c, s, "dexopt/baseline.prof", "dexopt/baseline.profm", new File(new File("/data/misc/profiles/cur/0", child), "primary.prof"));
        if (!b.e()) {
            return false;
        }
        final boolean n = b.i().m().n();
        if (n) {
            f(packageInfo, file);
        }
        return n;
    }
    
    public static void i(final Context context) {
        j(context, new xu0(), c.a);
    }
    
    public static void j(final Context context, final Executor executor, final c c) {
        k(context, executor, c, false);
    }
    
    public static void k(final Context context, final Executor executor, final c c, final boolean b) {
        final Context applicationContext = context.getApplicationContext();
        final String packageName = applicationContext.getPackageName();
        final ApplicationInfo applicationInfo = applicationContext.getApplicationInfo();
        final AssetManager assets = applicationContext.getAssets();
        final String name = new File(applicationInfo.sourceDir).getName();
        final PackageManager packageManager = context.getPackageManager();
        final boolean b2 = false;
        try {
            final PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            final File filesDir = context.getFilesDir();
            boolean b3;
            if (!b && d(packageInfo, filesDir, c)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping profile installation for ");
                sb.append(context.getPackageName());
                Log.d("ProfileInstaller", sb.toString());
                b3 = b2;
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Installing profile for ");
                sb2.append(context.getPackageName());
                Log.d("ProfileInstaller", sb2.toString());
                b3 = b2;
                if (h(assets, packageName, packageInfo, filesDir, name, executor, c)) {
                    b3 = b2;
                    if (b) {
                        b3 = true;
                    }
                }
            }
            e.c(context, b3);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            c.a(7, ex);
            e.c(context, false);
        }
    }
    
    public static void l(final Context context, final Executor executor, final c c) {
        final String packageName = context.getApplicationContext().getPackageName();
        final PackageManager packageManager = context.getPackageManager();
        try {
            f(packageManager.getPackageInfo(packageName, 0), context.getFilesDir());
            g(executor, c, 10, null);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            g(executor, c, 7, ex);
        }
    }
    
    public interface c
    {
        void a(final int p0, final Object p1);
        
        void b(final int p0, final Object p1);
    }
}

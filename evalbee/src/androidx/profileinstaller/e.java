// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.util.Objects;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import android.content.pm.PackageManager$PackageInfoFlags;
import android.content.pm.PackageManager$NameNotFoundException;
import java.io.IOException;
import java.io.File;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build$VERSION;
import android.content.Context;

public abstract class e
{
    public static final he1 a;
    public static final Object b;
    public static c c;
    
    static {
        a = he1.t();
        b = new Object();
        e.c = null;
    }
    
    public static long a(final Context context) {
        final PackageManager packageManager = context.getApplicationContext().getPackageManager();
        PackageInfo packageInfo;
        if (Build$VERSION.SDK_INT >= 33) {
            packageInfo = e.a.a(packageManager, context);
        }
        else {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
        }
        return packageInfo.lastUpdateTime;
    }
    
    public static c b(final int n, final boolean b, final boolean b2) {
        e.a.p(e.c = new c(n, b, b2));
        return e.c;
    }
    
    public static c c(final Context context, final boolean b) {
        if (!b) {
            final c c = e.c;
            if (c != null) {
                return c;
            }
        }
        final Object b2 = e.b;
        monitorenter(b2);
        Label_0045: {
            if (b) {
                break Label_0045;
            }
            try {
                final c c2 = e.c;
                if (c2 != null) {
                    return c2;
                }
                final int sdk_INT = Build$VERSION.SDK_INT;
                int n = 0;
                if (sdk_INT >= 28) {
                    if (sdk_INT != 30) {
                        final File file = new File(new File("/data/misc/profiles/ref/", context.getPackageName()), "primary.prof");
                        final long length = file.length();
                        final boolean b3 = file.exists() && length > 0L;
                        final File file2 = new File(new File("/data/misc/profiles/cur/0/", context.getPackageName()), "primary.prof");
                        final long length2 = file2.length();
                        boolean b4;
                        if (file2.exists() && length2 > 0L) {
                            b4 = true;
                        }
                        else {
                            b4 = false;
                        }
                        try {
                            final long a = a(context);
                            final File file3 = new File(context.getFilesDir(), "profileInstalled");
                            b a2 = null;
                            Label_0253: {
                                if (file3.exists()) {
                                    try {
                                        a2 = e.b.a(file3);
                                        break Label_0253;
                                    }
                                    catch (final IOException ex) {
                                        return b(131072, b3, b4);
                                    }
                                }
                                a2 = null;
                            }
                            Label_0302: {
                                if (a2 != null && a2.c == a) {
                                    final int b5 = a2.b;
                                    if (b5 != 2) {
                                        n = b5;
                                        break Label_0302;
                                    }
                                }
                                if (b3) {
                                    n = 1;
                                }
                                else if (b4) {
                                    n = 2;
                                }
                            }
                            int n2 = n;
                            if (b) {
                                n2 = n;
                                if (b4 && (n2 = n) != 1) {
                                    n2 = 2;
                                }
                            }
                            int n3 = n2;
                            if (a2 != null) {
                                n3 = n2;
                                if (a2.b == 2 && (n3 = n2) == 1) {
                                    n3 = n2;
                                    if (length < a2.d) {
                                        n3 = 3;
                                    }
                                }
                            }
                            final b b6 = new b(1, n3, a, length2);
                            if (a2 != null) {
                                final boolean equals = a2.equals(b6);
                                final int n4 = n3;
                                if (equals) {
                                    return b(n4, b3, b4);
                                }
                            }
                            int n4;
                            try {
                                b6.b(file3);
                                n4 = n3;
                            }
                            catch (final IOException ex2) {
                                n4 = 196608;
                            }
                            return b(n4, b3, b4);
                        }
                        catch (final PackageManager$NameNotFoundException ex3) {
                            return b(65536, b3, b4);
                        }
                    }
                }
                return b(262144, false, false);
            }
            finally {
                monitorexit(b2);
            }
        }
    }
    
    public abstract static class a
    {
        public static PackageInfo a(final PackageManager packageManager, final Context context) {
            return packageManager.getPackageInfo(context.getPackageName(), PackageManager$PackageInfoFlags.of(0L));
        }
    }
    
    public static class b
    {
        public final int a;
        public final int b;
        public final long c;
        public final long d;
        
        public b(final int a, final int b, final long c, final long d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        public static b a(final File file) {
            final DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
            try {
                final b b = new b(dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readLong(), dataInputStream.readLong());
                dataInputStream.close();
                return b;
            }
            finally {
                try {
                    dataInputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)file).addSuppressed(exception);
                }
            }
        }
        
        public void b(final File file) {
            file.delete();
            final DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
            try {
                dataOutputStream.writeInt(this.a);
                dataOutputStream.writeInt(this.b);
                dataOutputStream.writeLong(this.c);
                dataOutputStream.writeLong(this.d);
                dataOutputStream.close();
            }
            finally {
                try {
                    dataOutputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)file).addSuppressed(exception);
                }
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && o instanceof b) {
                final b b2 = (b)o;
                if (this.b != b2.b || this.c != b2.c || this.a != b2.a || this.d != b2.d) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(this.b, this.c, this.a, this.d);
        }
    }
    
    public static class c
    {
        public final int a;
        public final boolean b;
        public final boolean c;
        
        public c(final int a, final boolean b, final boolean c) {
            this.a = a;
            this.c = c;
            this.b = b;
        }
    }
}

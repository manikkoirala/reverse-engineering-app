// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.view.Choreographer$FrameCallback;
import android.view.Choreographer;
import java.util.Random;
import android.os.Handler;
import android.os.Looper;
import android.os.Build$VERSION;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import android.content.Context;

public class ProfileInstallerInitializer implements af0
{
    public static void k(final Context context) {
        new ThreadPoolExecutor(0, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()).execute(new y81(context));
    }
    
    @Override
    public List dependencies() {
        return Collections.emptyList();
    }
    
    public c e(final Context context) {
        this.f(context.getApplicationContext());
        return new c();
    }
    
    public void f(final Context context) {
        a.c(new w81(this, context));
    }
    
    public void g(final Context context) {
        Handler a;
        if (Build$VERSION.SDK_INT >= 28) {
            a = b.a(Looper.getMainLooper());
        }
        else {
            a = new Handler(Looper.getMainLooper());
        }
        a.postDelayed((Runnable)new x81(context), (long)(new Random().nextInt(Math.max(1000, 1)) + 5000));
    }
    
    public abstract static class a
    {
        public static void c(final Runnable runnable) {
            Choreographer.getInstance().postFrameCallback((Choreographer$FrameCallback)new z81(runnable));
        }
    }
    
    public abstract static class b
    {
        public static Handler a(final Looper looper) {
            return ak.a(looper);
        }
    }
    
    public static class c
    {
    }
}

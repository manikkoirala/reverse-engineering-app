// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.content.Context;
import java.io.File;

public abstract class a
{
    public static boolean a(final File file) {
        if (!file.isDirectory()) {
            file.delete();
            return true;
        }
        final File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return false;
        }
        final int length = listFiles.length;
        int i = 0;
        boolean b = true;
        while (i < length) {
            b = (a(listFiles[i]) && b);
            ++i;
        }
        return b;
    }
    
    public static void b(final Context context, final ProfileInstallReceiver.a a) {
        int n;
        if (a(a.a(context))) {
            n = 14;
        }
        else {
            n = 15;
        }
        a.a(n, null);
    }
    
    public abstract static class a
    {
        public static File a(final Context context) {
            return context.createDeviceProtectedStorageContext().getCodeCacheDir();
        }
    }
}

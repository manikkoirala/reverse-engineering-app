// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import android.os.BaseBundle;
import android.os.Bundle;
import java.util.concurrent.Executor;
import android.content.Intent;
import android.content.Context;
import android.os.Process;
import android.content.BroadcastReceiver;

public class ProfileInstallReceiver extends BroadcastReceiver
{
    public static void a(final c.c c) {
        Process.sendSignal(Process.myPid(), 10);
        c.a(12, null);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        final String action = intent.getAction();
        if ("androidx.profileinstaller.action.INSTALL_PROFILE".equals(action)) {
            c.k(context, new xu0(), (c.c)new a(), true);
        }
        else if ("androidx.profileinstaller.action.SKIP_FILE".equals(action)) {
            final Bundle extras = intent.getExtras();
            if (extras != null) {
                final String string = ((BaseBundle)extras).getString("EXTRA_SKIP_FILE_OPERATION");
                if ("WRITE_SKIP_FILE".equals(string)) {
                    c.l(context, new xu0(), (c.c)new a());
                }
                else if ("DELETE_SKIP_FILE".equals(string)) {
                    c.c(context, new xu0(), (c.c)new a());
                }
            }
        }
        else if ("androidx.profileinstaller.action.SAVE_PROFILE".equals(action)) {
            a(new a());
        }
        else if ("androidx.profileinstaller.action.BENCHMARK_OPERATION".equals(action)) {
            final Bundle extras2 = intent.getExtras();
            if (extras2 != null) {
                final String string2 = ((BaseBundle)extras2).getString("EXTRA_BENCHMARK_OPERATION");
                final a a = new a();
                if ("DROP_SHADER_CACHE".equals(string2)) {
                    androidx.profileinstaller.a.b(context, a);
                }
                else {
                    a.a(16, null);
                }
            }
        }
    }
    
    public class a implements c
    {
        public final ProfileInstallReceiver a;
        
        public a(final ProfileInstallReceiver a) {
            this.a = a;
        }
        
        @Override
        public void a(final int resultCode, final Object o) {
            androidx.profileinstaller.c.b.a(resultCode, o);
            this.a.setResultCode(resultCode);
        }
        
        @Override
        public void b(final int n, final Object o) {
            androidx.profileinstaller.c.b.b(n, o);
        }
    }
}

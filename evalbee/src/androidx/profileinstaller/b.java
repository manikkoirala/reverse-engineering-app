// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.io.FileOutputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.os.Build$VERSION;
import java.io.File;
import java.util.concurrent.Executor;
import android.content.res.AssetManager;

public class b
{
    public final AssetManager a;
    public final Executor b;
    public final c.c c;
    public final byte[] d;
    public final File e;
    public final String f;
    public final String g;
    public final String h;
    public boolean i;
    public vs[] j;
    public byte[] k;
    
    public b(final AssetManager a, final Executor b, final c.c c, final String f, final String g, final String h, final File e) {
        this.i = false;
        this.a = a;
        this.b = b;
        this.c = c;
        this.f = f;
        this.g = g;
        this.h = h;
        this.e = e;
        this.d = d();
    }
    
    public static byte[] d() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT > 33) {
            return null;
        }
        switch (sdk_INT) {
            default: {
                return null;
            }
            case 31:
            case 32:
            case 33: {
                return b91.a;
            }
            case 28:
            case 29:
            case 30: {
                return b91.b;
            }
            case 27: {
                return b91.c;
            }
            case 26: {
                return b91.d;
            }
            case 24:
            case 25: {
                return b91.e;
            }
        }
    }
    
    public static boolean k() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT > 33) {
            return false;
        }
        if (sdk_INT != 24 && sdk_INT != 25) {
            switch (sdk_INT) {
                default: {
                    return false;
                }
                case 31:
                case 32:
                case 33: {
                    break;
                }
            }
        }
        return true;
    }
    
    public final b b(final vs[] array, final byte[] array2) {
        c.c c;
        int n;
        try {
            final InputStream h = this.h(this.a, this.h);
            if (h != null) {
                try {
                    this.j = androidx.profileinstaller.d.q(h, androidx.profileinstaller.d.o(h, androidx.profileinstaller.d.b), array2, array);
                    h.close();
                    return this;
                }
                finally {
                    try {
                        h.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)(Object)array).addSuppressed(exception);
                    }
                }
            }
            if (h != null) {
                h.close();
                return null;
            }
            return null;
        }
        catch (final IllegalStateException ex) {
            this.j = null;
            c = this.c;
            n = 8;
        }
        catch (final IOException ex) {
            c = this.c;
            n = 7;
        }
        catch (final FileNotFoundException ex) {
            c = this.c;
            n = 9;
        }
        final IllegalStateException ex;
        c.a(n, ex);
        return null;
    }
    
    public final void c() {
        if (this.i) {
            return;
        }
        throw new IllegalStateException("This device doesn't support aot. Did you call deviceSupportsAotProfile()?");
    }
    
    public boolean e() {
        if (this.d == null) {
            this.l(3, Build$VERSION.SDK_INT);
            return false;
        }
        if (!this.e.canWrite()) {
            this.l(4, null);
            return false;
        }
        return this.i = true;
    }
    
    public final InputStream f(final AssetManager assetManager) {
        c.c c;
        int n;
        try {
            return this.h(assetManager, this.g);
        }
        catch (final IOException ex) {
            c = this.c;
            n = 7;
        }
        catch (final FileNotFoundException ex) {
            c = this.c;
            n = 6;
        }
        final IOException ex;
        c.a(n, ex);
        return null;
    }
    
    public final InputStream h(final AssetManager assetManager, final String s) {
        FileInputStream inputStream;
        try {
            inputStream = assetManager.openFd(s).createInputStream();
        }
        catch (final FileNotFoundException ex) {
            final String message = ex.getMessage();
            if (message != null && message.contains("compressed")) {
                this.c.b(5, null);
            }
            inputStream = null;
        }
        return inputStream;
    }
    
    public b i() {
        this.c();
        if (this.d == null) {
            return this;
        }
        final InputStream f = this.f(this.a);
        if (f != null) {
            this.j = this.j(f);
        }
        final vs[] j = this.j;
        if (j != null && k()) {
            final b b = this.b(j, this.d);
            if (b != null) {
                return b;
            }
        }
        return this;
    }
    
    public final vs[] j(final InputStream p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: getstatic       androidx/profileinstaller/d.a:[B
        //     5: invokestatic    androidx/profileinstaller/d.o:(Ljava/io/InputStream;[B)[B
        //     8: aload_0        
        //     9: getfield        androidx/profileinstaller/b.f:Ljava/lang/String;
        //    12: invokestatic    androidx/profileinstaller/d.w:(Ljava/io/InputStream;[BLjava/lang/String;)[Lvs;
        //    15: astore_2       
        //    16: aload_1        
        //    17: invokevirtual   java/io/InputStream.close:()V
        //    20: aload_2        
        //    21: astore_1       
        //    22: goto            102
        //    25: astore_1       
        //    26: aload_0        
        //    27: getfield        androidx/profileinstaller/b.c:Landroidx/profileinstaller/c$c;
        //    30: bipush          7
        //    32: aload_1        
        //    33: invokeinterface androidx/profileinstaller/c$c.a:(ILjava/lang/Object;)V
        //    38: aload_2        
        //    39: astore_1       
        //    40: goto            102
        //    43: astore_2       
        //    44: goto            104
        //    47: astore_2       
        //    48: aload_0        
        //    49: getfield        androidx/profileinstaller/b.c:Landroidx/profileinstaller/c$c;
        //    52: bipush          8
        //    54: aload_2        
        //    55: invokeinterface androidx/profileinstaller/c$c.a:(ILjava/lang/Object;)V
        //    60: aload_1        
        //    61: invokevirtual   java/io/InputStream.close:()V
        //    64: goto            100
        //    67: astore_2       
        //    68: aload_0        
        //    69: getfield        androidx/profileinstaller/b.c:Landroidx/profileinstaller/c$c;
        //    72: bipush          7
        //    74: aload_2        
        //    75: invokeinterface androidx/profileinstaller/c$c.a:(ILjava/lang/Object;)V
        //    80: aload_1        
        //    81: invokevirtual   java/io/InputStream.close:()V
        //    84: goto            100
        //    87: astore_1       
        //    88: aload_0        
        //    89: getfield        androidx/profileinstaller/b.c:Landroidx/profileinstaller/c$c;
        //    92: bipush          7
        //    94: aload_1        
        //    95: invokeinterface androidx/profileinstaller/c$c.a:(ILjava/lang/Object;)V
        //   100: aconst_null    
        //   101: astore_1       
        //   102: aload_1        
        //   103: areturn        
        //   104: aload_1        
        //   105: invokevirtual   java/io/InputStream.close:()V
        //   108: goto            124
        //   111: astore_1       
        //   112: aload_0        
        //   113: getfield        androidx/profileinstaller/b.c:Landroidx/profileinstaller/c$c;
        //   116: bipush          7
        //   118: aload_1        
        //   119: invokeinterface androidx/profileinstaller/c$c.a:(ILjava/lang/Object;)V
        //   124: aload_2        
        //   125: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  0      16     67     87     Ljava/io/IOException;
        //  0      16     47     67     Ljava/lang/IllegalStateException;
        //  0      16     43     126    Any
        //  16     20     25     43     Ljava/io/IOException;
        //  48     60     43     126    Any
        //  60     64     87     100    Ljava/io/IOException;
        //  68     80     43     126    Any
        //  80     84     87     100    Ljava/io/IOException;
        //  104    108    111    124    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 63 out of bounds for length 63
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3611)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public final void l(final int n, final Object o) {
        this.b.execute(new us(this, n, o));
    }
    
    public b m() {
        final vs[] j = this.j;
        final byte[] d = this.d;
        if (j != null) {
            if (d != null) {
                this.c();
                Label_0132: {
                    c.c c;
                    int n;
                    try {
                        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        try {
                            androidx.profileinstaller.d.E(byteArrayOutputStream, d);
                            if (!androidx.profileinstaller.d.B(byteArrayOutputStream, d, j)) {
                                this.c.a(5, null);
                                this.j = null;
                                byteArrayOutputStream.close();
                                return this;
                            }
                            this.k = byteArrayOutputStream.toByteArray();
                            byteArrayOutputStream.close();
                            break Label_0132;
                        }
                        finally {
                            try {
                                byteArrayOutputStream.close();
                            }
                            finally {
                                final Throwable t;
                                final Throwable exception;
                                t.addSuppressed(exception);
                            }
                        }
                    }
                    catch (final IllegalStateException ex) {
                        c = this.c;
                        n = 8;
                    }
                    catch (final IOException ex) {
                        c = this.c;
                        n = 7;
                    }
                    final IllegalStateException ex;
                    c.a(n, ex);
                }
                this.j = null;
            }
        }
        return this;
    }
    
    public boolean n() {
        final byte[] k = this.k;
        if (k == null) {
            return false;
        }
        this.c();
        Label_0134: {
            try {
                final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(k);
                try {
                    final FileOutputStream fileOutputStream = new FileOutputStream(this.e);
                    try {
                        ax.l(byteArrayInputStream, fileOutputStream);
                        this.l(1, null);
                        fileOutputStream.close();
                        byteArrayInputStream.close();
                        this.k = null;
                        this.j = null;
                        return true;
                    }
                    finally {
                        try {
                            fileOutputStream.close();
                        }
                        finally {
                            final Throwable t;
                            final Throwable exception;
                            t.addSuppressed(exception);
                        }
                    }
                }
                finally {
                    try {
                        byteArrayInputStream.close();
                    }
                    finally {
                        final Throwable t2;
                        final Throwable exception2;
                        t2.addSuppressed(exception2);
                    }
                }
            }
            catch (final IOException ex) {
                this.l(7, ex);
            }
            catch (final FileNotFoundException ex2) {
                this.l(6, ex2);
            }
            finally {
                break Label_0134;
            }
            this.k = null;
            this.j = null;
            return false;
        }
        this.k = null;
        this.j = null;
    }
}

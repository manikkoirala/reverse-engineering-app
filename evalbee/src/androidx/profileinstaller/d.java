// 
// Decompiled by Procyon v0.6.0
// 

package androidx.profileinstaller;

import java.util.TreeMap;
import java.io.ByteArrayInputStream;
import java.util.BitSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.io.OutputStream;
import java.io.InputStream;

public abstract class d
{
    public static final byte[] a;
    public static final byte[] b;
    
    static {
        a = new byte[] { 112, 114, 111, 0 };
        b = new byte[] { 112, 114, 109, 0 };
    }
    
    public static void A(final InputStream inputStream) {
        ax.h(inputStream);
        final int j = ax.j(inputStream);
        if (j == 6) {
            return;
        }
        int i;
        if ((i = j) == 7) {
            return;
        }
        while (i > 0) {
            ax.j(inputStream);
            for (int k = ax.j(inputStream); k > 0; --k) {
                ax.h(inputStream);
            }
            --i;
        }
    }
    
    public static boolean B(final OutputStream outputStream, final byte[] a, final vs[] array) {
        if (Arrays.equals(a, b91.a)) {
            N(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, b91.b)) {
            M(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, b91.d)) {
            K(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, b91.c)) {
            L(outputStream, array);
            return true;
        }
        if (Arrays.equals(a, b91.e)) {
            J(outputStream, array);
            return true;
        }
        return false;
    }
    
    public static void C(final OutputStream outputStream, final vs vs) {
        final int[] h = vs.h;
        final int length = h.length;
        int i = 0;
        int intValue = 0;
        while (i < length) {
            final Integer value = h[i];
            ax.p(outputStream, value - intValue);
            intValue = value;
            ++i;
        }
    }
    
    public static f D(final vs[] array) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ax.p(byteArrayOutputStream, array.length);
            int i = 0;
            int j = 2;
            while (i < array.length) {
                final vs vs = array[i];
                ax.q(byteArrayOutputStream, vs.c);
                ax.q(byteArrayOutputStream, vs.d);
                ax.q(byteArrayOutputStream, vs.g);
                final String k = j(vs.a, vs.b, b91.a);
                final int l = ax.k(k);
                ax.p(byteArrayOutputStream, l);
                j = j + 4 + 4 + 4 + 2 + l * 1;
                ax.n(byteArrayOutputStream, k);
                ++i;
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (j == byteArray.length) {
                final f f = new f(FileSectionType.DEX_FILES, j, byteArray, false);
                byteArrayOutputStream.close();
                return f;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected size ");
            sb.append(j);
            sb.append(", does not match actual size ");
            sb.append(byteArray.length);
            throw ax.c(sb.toString());
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)(Object)array).addSuppressed(exception);
            }
        }
    }
    
    public static void E(final OutputStream outputStream, final byte[] b) {
        outputStream.write(d.a);
        outputStream.write(b);
    }
    
    public static void F(final OutputStream outputStream, final vs vs) {
        I(outputStream, vs);
        C(outputStream, vs);
        H(outputStream, vs);
    }
    
    public static void G(final OutputStream outputStream, final vs vs, final String s) {
        ax.p(outputStream, ax.k(s));
        ax.p(outputStream, vs.e);
        ax.q(outputStream, vs.f);
        ax.q(outputStream, vs.c);
        ax.q(outputStream, vs.g);
        ax.n(outputStream, s);
    }
    
    public static void H(final OutputStream outputStream, final vs vs) {
        final byte[] b = new byte[k(vs.g)];
        for (final Map.Entry<Integer, V> entry : vs.i.entrySet()) {
            final int intValue = entry.getKey();
            final int intValue2 = (int)entry.getValue();
            if ((intValue2 & 0x2) != 0x0) {
                z(b, 2, intValue, vs);
            }
            if ((intValue2 & 0x4) != 0x0) {
                z(b, 4, intValue, vs);
            }
        }
        outputStream.write(b);
    }
    
    public static void I(final OutputStream outputStream, final vs vs) {
        final Iterator iterator = vs.i.entrySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry<Integer, V> entry = (Map.Entry<Integer, V>)iterator.next();
            final int intValue = entry.getKey();
            if (((int)entry.getValue() & 0x1) == 0x0) {
                continue;
            }
            ax.p(outputStream, intValue - n);
            ax.p(outputStream, 0);
            n = intValue;
        }
    }
    
    public static void J(final OutputStream outputStream, final vs[] array) {
        ax.p(outputStream, array.length);
        for (final vs vs : array) {
            final String j = j(vs.a, vs.b, b91.e);
            ax.p(outputStream, ax.k(j));
            ax.p(outputStream, vs.i.size());
            ax.p(outputStream, vs.h.length);
            ax.q(outputStream, vs.c);
            ax.n(outputStream, j);
            final Iterator iterator = vs.i.keySet().iterator();
            while (iterator.hasNext()) {
                ax.p(outputStream, (int)iterator.next());
            }
            final int[] h = vs.h;
            for (int length2 = h.length, k = 0; k < length2; ++k) {
                ax.p(outputStream, h[k]);
            }
        }
    }
    
    public static void K(final OutputStream outputStream, final vs[] array) {
        ax.r(outputStream, array.length);
        for (final vs vs : array) {
            final int size = vs.i.size();
            final String j = j(vs.a, vs.b, b91.d);
            ax.p(outputStream, ax.k(j));
            ax.p(outputStream, vs.h.length);
            ax.q(outputStream, size * 4);
            ax.q(outputStream, vs.c);
            ax.n(outputStream, j);
            final Iterator iterator = vs.i.keySet().iterator();
            while (iterator.hasNext()) {
                ax.p(outputStream, (int)iterator.next());
                ax.p(outputStream, 0);
            }
            final int[] h = vs.h;
            for (int length2 = h.length, k = 0; k < length2; ++k) {
                ax.p(outputStream, h[k]);
            }
        }
    }
    
    public static void L(final OutputStream outputStream, final vs[] array) {
        final byte[] b = b(array, b91.c);
        ax.r(outputStream, array.length);
        ax.m(outputStream, b);
    }
    
    public static void M(final OutputStream outputStream, final vs[] array) {
        final byte[] b = b(array, b91.b);
        ax.r(outputStream, array.length);
        ax.m(outputStream, b);
    }
    
    public static void N(final OutputStream outputStream, final vs[] array) {
        O(outputStream, array);
    }
    
    public static void O(final OutputStream outputStream, final vs[] array) {
        final ArrayList list = new ArrayList(3);
        final ArrayList list2 = new ArrayList(3);
        list.add(D(array));
        list.add(c(array));
        list.add(d(array));
        long n = b91.a.length + (long)d.a.length + 4L + list.size() * 16;
        ax.q(outputStream, list.size());
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= list.size()) {
                break;
            }
            final f f = (f)list.get(n3);
            ax.q(outputStream, f.a.getValue());
            ax.q(outputStream, n);
            int n5;
            if (f.d) {
                final byte[] c = f.c;
                final long n4 = c.length;
                final byte[] b = ax.b(c);
                list2.add(b);
                ax.q(outputStream, b.length);
                ax.q(outputStream, n4);
                n5 = b.length;
            }
            else {
                list2.add(f.c);
                ax.q(outputStream, f.c.length);
                ax.q(outputStream, 0L);
                n5 = f.c.length;
            }
            n += n5;
            ++n3;
        }
        while (i < list2.size()) {
            outputStream.write((byte[])list2.get(i));
            ++i;
        }
    }
    
    public static int a(final vs vs) {
        final Iterator iterator = vs.i.entrySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            n |= ((Map.Entry<K, Integer>)iterator.next()).getValue();
        }
        return n;
    }
    
    public static byte[] b(final vs[] array, final byte[] a) {
        final int length = array.length;
        final int n = 0;
        final int n2 = 0;
        int i = 0;
        int n3 = 0;
        while (i < length) {
            final vs vs = array[i];
            n3 += ax.k(j(vs.a, vs.b, a)) + 16 + vs.e * 2 + vs.f + k(vs.g);
            ++i;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(n3);
        if (Arrays.equals(a, b91.c)) {
            for (int length2 = array.length, j = n2; j < length2; ++j) {
                final vs vs2 = array[j];
                G(byteArrayOutputStream, vs2, j(vs2.a, vs2.b, a));
                F(byteArrayOutputStream, vs2);
            }
        }
        else {
            for (final vs vs3 : array) {
                G(byteArrayOutputStream, vs3, j(vs3.a, vs3.b, a));
            }
            for (int length4 = array.length, l = n; l < length4; ++l) {
                F(byteArrayOutputStream, array[l]);
            }
        }
        if (byteArrayOutputStream.size() == n3) {
            return byteArrayOutputStream.toByteArray();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("The bytes saved do not match expectation. actual=");
        sb.append(byteArrayOutputStream.size());
        sb.append(" expected=");
        sb.append(n3);
        throw ax.c(sb.toString());
    }
    
    public static f c(final vs[] array) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int j = 0;
        try {
            while (i < array.length) {
                final vs vs = array[i];
                ax.p(byteArrayOutputStream, i);
                ax.p(byteArrayOutputStream, vs.e);
                j = j + 2 + 2 + vs.e * 2;
                C(byteArrayOutputStream, vs);
                ++i;
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (j == byteArray.length) {
                final f f = new f(FileSectionType.CLASSES, j, byteArray, true);
                byteArrayOutputStream.close();
                return f;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected size ");
            sb.append(j);
            sb.append(", does not match actual size ");
            sb.append(byteArray.length);
            throw ax.c(sb.toString());
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)(Object)array).addSuppressed(exception);
            }
        }
    }
    
    public static f d(final vs[] array) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        int j = 0;
        try {
            while (i < array.length) {
                final vs vs = array[i];
                final int a = a(vs);
                final byte[] e = e(vs);
                final byte[] f = f(vs);
                ax.p(byteArrayOutputStream, i);
                final int n = e.length + 2 + f.length;
                ax.q(byteArrayOutputStream, n);
                ax.p(byteArrayOutputStream, a);
                byteArrayOutputStream.write(e);
                byteArrayOutputStream.write(f);
                j = j + 2 + 4 + n;
                ++i;
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (j == byteArray.length) {
                final f f2 = new f(FileSectionType.METHODS, j, byteArray, true);
                byteArrayOutputStream.close();
                return f2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected size ");
            sb.append(j);
            sb.append(", does not match actual size ");
            sb.append(byteArray.length);
            throw ax.c(sb.toString());
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)(Object)array).addSuppressed(exception);
            }
        }
    }
    
    public static byte[] e(final vs vs) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            H(byteArrayOutputStream, vs);
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)vs).addSuppressed(exception);
            }
        }
    }
    
    public static byte[] f(final vs vs) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            I(byteArrayOutputStream, vs);
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)vs).addSuppressed(exception);
            }
        }
    }
    
    public static String g(final String s, final String s2) {
        if ("!".equals(s2)) {
            return s.replace(":", "!");
        }
        String replace = s;
        if (":".equals(s2)) {
            replace = s.replace("!", ":");
        }
        return replace;
    }
    
    public static String h(final String s) {
        int n;
        if ((n = s.indexOf("!")) < 0) {
            n = s.indexOf(":");
        }
        String substring = s;
        if (n > 0) {
            substring = s.substring(n + 1);
        }
        return substring;
    }
    
    public static vs i(final vs[] array, String h) {
        if (array.length <= 0) {
            return null;
        }
        h = h(h);
        for (int i = 0; i < array.length; ++i) {
            if (array[i].b.equals(h)) {
                return array[i];
            }
        }
        return null;
    }
    
    public static String j(final String str, final String str2, final byte[] array) {
        final String a = b91.a(array);
        if (str.length() <= 0) {
            return g(str2, a);
        }
        if (str2.equals("classes.dex")) {
            return str;
        }
        if (str2.contains("!") || str2.contains(":")) {
            return g(str2, a);
        }
        if (str2.endsWith(".apk")) {
            return str2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(b91.a(array));
        sb.append(str2);
        return sb.toString();
    }
    
    public static int k(final int n) {
        return y(n * 2) / 8;
    }
    
    public static int l(final int i, final int n, final int n2) {
        if (i == 1) {
            throw ax.c("HOT methods are not stored in the bitmap");
        }
        if (i == 2) {
            return n;
        }
        if (i == 4) {
            return n + n2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected flag: ");
        sb.append(i);
        throw ax.c(sb.toString());
    }
    
    public static int[] m(final InputStream inputStream, final int n) {
        final int[] array = new int[n];
        int i = 0;
        int n2 = 0;
        while (i < n) {
            n2 += ax.h(inputStream);
            array[i] = n2;
            ++i;
        }
        return array;
    }
    
    public static int n(final BitSet set, final int n, final int n2) {
        int n3 = 2;
        if (!set.get(l(2, n, n2))) {
            n3 = 0;
        }
        int n4 = n3;
        if (set.get(l(4, n, n2))) {
            n4 = (n3 | 0x4);
        }
        return n4;
    }
    
    public static byte[] o(final InputStream inputStream, final byte[] a) {
        if (Arrays.equals(a, ax.d(inputStream, a.length))) {
            return ax.d(inputStream, b91.b.length);
        }
        throw ax.c("Invalid magic");
    }
    
    public static void p(final InputStream inputStream, final vs vs) {
        final int n = inputStream.available() - vs.f;
        int n2 = 0;
        while (inputStream.available() > n) {
            final int i = n2 + ax.h(inputStream);
            vs.i.put(i, 1);
            int h = ax.h(inputStream);
            while (true) {
                n2 = i;
                if (h <= 0) {
                    break;
                }
                A(inputStream);
                --h;
            }
        }
        if (inputStream.available() == n) {
            return;
        }
        throw ax.c("Read too much data during profile line parse");
    }
    
    public static vs[] q(final InputStream inputStream, final byte[] array, final byte[] a2, final vs[] array2) {
        if (Arrays.equals(array, b91.f)) {
            if (!Arrays.equals(b91.a, a2)) {
                return r(inputStream, array, array2);
            }
            throw ax.c("Requires new Baseline Profile Metadata. Please rebuild the APK with Android Gradle Plugin 7.2 Canary 7 or higher");
        }
        else {
            if (Arrays.equals(array, b91.g)) {
                return t(inputStream, a2, array2);
            }
            throw ax.c("Unsupported meta version");
        }
    }
    
    public static vs[] r(InputStream inputStream, byte[] e, final vs[] array) {
        if (Arrays.equals((byte[])e, b91.f)) {
            final int j = ax.j(inputStream);
            e = ax.e(inputStream, (int)ax.i(inputStream), (int)ax.i(inputStream));
            if (inputStream.read() <= 0) {
                inputStream = new ByteArrayInputStream((byte[])e);
                try {
                    final vs[] s = s(inputStream, j, array);
                    inputStream.close();
                    return s;
                }
                finally {
                    try {
                        inputStream.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)e).addSuppressed(exception);
                    }
                }
            }
            throw ax.c("Content found after the end of file");
        }
        throw ax.c("Unsupported meta version");
    }
    
    public static vs[] s(final InputStream inputStream, final int n, final vs[] array) {
        final int available = inputStream.available();
        final int n2 = 0;
        if (available == 0) {
            return new vs[0];
        }
        if (n == array.length) {
            final String[] array2 = new String[n];
            final int[] array3 = new int[n];
            int n3 = 0;
            int i;
            while (true) {
                i = n2;
                if (n3 >= n) {
                    break;
                }
                final int h = ax.h(inputStream);
                array3[n3] = ax.h(inputStream);
                array2[n3] = ax.f(inputStream, h);
                ++n3;
            }
            while (i < n) {
                final vs vs = array[i];
                if (!vs.b.equals(array2[i])) {
                    throw ax.c("Order of dexfiles in metadata did not match baseline");
                }
                final int e = array3[i];
                vs.e = e;
                vs.h = m(inputStream, e);
                ++i;
            }
            return array;
        }
        throw ax.c("Mismatched number of dex files found in metadata");
    }
    
    public static vs[] t(InputStream inputStream, final byte[] array, final vs[] array2) {
        final int h = ax.h(inputStream);
        final byte[] e = ax.e(inputStream, (int)ax.i(inputStream), (int)ax.i(inputStream));
        if (inputStream.read() <= 0) {
            inputStream = new ByteArrayInputStream(e);
            try {
                final vs[] u = u(inputStream, array, h, array2);
                inputStream.close();
                return u;
            }
            finally {
                try {
                    inputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)(Object)array).addSuppressed(exception);
                }
            }
        }
        throw ax.c("Content found after the end of file");
    }
    
    public static vs[] u(final InputStream inputStream, final byte[] a, final int n, final vs[] array) {
        final int available = inputStream.available();
        int i = 0;
        if (available == 0) {
            return new vs[0];
        }
        if (n == array.length) {
            while (i < n) {
                ax.h(inputStream);
                final String f = ax.f(inputStream, ax.h(inputStream));
                final long j = ax.i(inputStream);
                final int h = ax.h(inputStream);
                final vs k = i(array, f);
                if (k == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Missing profile key: ");
                    sb.append(f);
                    throw ax.c(sb.toString());
                }
                k.d = j;
                final int[] m = m(inputStream, h);
                if (Arrays.equals(a, b91.e)) {
                    k.e = h;
                    k.h = m;
                }
                ++i;
            }
            return array;
        }
        throw ax.c("Mismatched number of dex files found in metadata");
    }
    
    public static void v(final InputStream inputStream, final vs vs) {
        final BitSet value = BitSet.valueOf(ax.d(inputStream, ax.a(vs.g * 2)));
        int n = 0;
        while (true) {
            final int g = vs.g;
            if (n >= g) {
                break;
            }
            final int n2 = n(value, n, g);
            if (n2 != 0) {
                Integer value2;
                if ((value2 = vs.i.get(n)) == null) {
                    value2 = 0;
                }
                vs.i.put(n, n2 | value2);
            }
            ++n;
        }
    }
    
    public static vs[] w(InputStream inputStream, byte[] e, final String s) {
        if (Arrays.equals((byte[])e, b91.b)) {
            final int j = ax.j(inputStream);
            e = ax.e(inputStream, (int)ax.i(inputStream), (int)ax.i(inputStream));
            if (inputStream.read() <= 0) {
                inputStream = new ByteArrayInputStream((byte[])e);
                try {
                    final vs[] x = x(inputStream, s, j);
                    inputStream.close();
                    return x;
                }
                finally {
                    try {
                        inputStream.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)e).addSuppressed(exception);
                    }
                }
            }
            throw ax.c("Content found after the end of file");
        }
        throw ax.c("Unsupported version");
    }
    
    public static vs[] x(final InputStream inputStream, final String s, final int n) {
        final int available = inputStream.available();
        final int n2 = 0;
        if (available == 0) {
            return new vs[0];
        }
        final vs[] array = new vs[n];
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= n) {
                break;
            }
            final int h = ax.h(inputStream);
            final int h2 = ax.h(inputStream);
            array[n3] = new vs(s, ax.f(inputStream, h), ax.i(inputStream), 0L, h2, (int)ax.i(inputStream), (int)ax.i(inputStream), new int[h2], new TreeMap());
            ++n3;
        }
        while (i < n) {
            final vs vs = array[i];
            p(inputStream, vs);
            vs.h = m(inputStream, vs.e);
            v(inputStream, vs);
            ++i;
        }
        return array;
    }
    
    public static int y(final int n) {
        return n + 8 - 1 & 0xFFFFFFF8;
    }
    
    public static void z(final byte[] array, int l, int n, final vs vs) {
        l = l(l, n, vs.g);
        n = l / 8;
        array[n] |= (byte)(1 << l % 8);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import java.util.Arrays;

class AudioAttributesImplBase implements AudioAttributesImpl
{
    public int a;
    public int b;
    public int c;
    public int d;
    
    public AudioAttributesImplBase() {
        this.a = 0;
        this.b = 0;
        this.c = 0;
        this.d = -1;
    }
    
    public int a() {
        return this.b;
    }
    
    public int b() {
        final int c = this.c;
        final int c2 = this.c();
        int n;
        if (c2 == 6) {
            n = (c | 0x4);
        }
        else {
            n = c;
            if (c2 == 7) {
                n = (c | 0x1);
            }
        }
        return n & 0x111;
    }
    
    public int c() {
        final int d = this.d;
        if (d != -1) {
            return d;
        }
        return AudioAttributesCompat.a(false, this.c, this.a);
    }
    
    public int d() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof AudioAttributesImplBase;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final AudioAttributesImplBase audioAttributesImplBase = (AudioAttributesImplBase)o;
        boolean b3 = b2;
        if (this.b == audioAttributesImplBase.a()) {
            b3 = b2;
            if (this.c == audioAttributesImplBase.b()) {
                b3 = b2;
                if (this.a == audioAttributesImplBase.d()) {
                    b3 = b2;
                    if (this.d == audioAttributesImplBase.d) {
                        b3 = true;
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.b, this.c, this.a, this.d });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AudioAttributesCompat:");
        if (this.d != -1) {
            sb.append(" stream=");
            sb.append(this.d);
            sb.append(" derived");
        }
        sb.append(" usage=");
        sb.append(AudioAttributesCompat.b(this.a));
        sb.append(" content=");
        sb.append(this.b);
        sb.append(" flags=0x");
        sb.append(Integer.toHexString(this.c).toUpperCase());
        return sb.toString();
    }
}

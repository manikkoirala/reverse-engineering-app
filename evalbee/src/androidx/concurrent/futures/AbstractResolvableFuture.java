// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeoutException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Executor;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.logging.Logger;

public abstract class AbstractResolvableFuture implements ik0
{
    public static final boolean d;
    public static final Logger e;
    public static final b f;
    public static final Object g;
    public volatile Object a;
    public volatile d b;
    public volatile g c;
    
    static {
        d = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
        e = Logger.getLogger(AbstractResolvableFuture.class.getName());
        f f2;
        try {
            final e e2 = new e(AtomicReferenceFieldUpdater.newUpdater(g.class, Thread.class, "a"), AtomicReferenceFieldUpdater.newUpdater(g.class, g.class, "b"), AtomicReferenceFieldUpdater.newUpdater(AbstractResolvableFuture.class, g.class, "c"), AtomicReferenceFieldUpdater.newUpdater(AbstractResolvableFuture.class, d.class, "b"), AtomicReferenceFieldUpdater.newUpdater(AbstractResolvableFuture.class, Object.class, "a"));
        }
        finally {
            f2 = new f();
        }
        f = (b)f2;
        final Throwable thrown;
        if (thrown != null) {
            AbstractResolvableFuture.e.log(Level.SEVERE, "SafeAtomicHelper is broken!", thrown);
        }
        g = new Object();
    }
    
    public static CancellationException d(final String message, final Throwable cause) {
        final CancellationException ex = new CancellationException(message);
        ex.initCause(cause);
        return ex;
    }
    
    static Object e(final Object o) {
        o.getClass();
        return o;
    }
    
    public static void g(final AbstractResolvableFuture abstractResolvableFuture) {
        abstractResolvableFuture.n();
        abstractResolvableFuture.c();
        d c;
        for (d f = abstractResolvableFuture.f(null); f != null; f = c) {
            c = f.c;
            h(f.a, f.b);
        }
    }
    
    public static void h(final Runnable obj, final Executor obj2) {
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException thrown) {
            final Logger e = AbstractResolvableFuture.e;
            final Level severe = Level.SEVERE;
            final StringBuilder sb = new StringBuilder();
            sb.append("RuntimeException while executing runnable ");
            sb.append(obj);
            sb.append(" with executor ");
            sb.append(obj2);
            e.log(severe, sb.toString(), thrown);
        }
    }
    
    static Object k(final Future future) {
        boolean b = false;
        try {
            return future.get();
        }
        catch (final InterruptedException ex) {
            b = true;
            return future.get();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    public final void a(final StringBuilder sb) {
        String str = "]";
        try {
            final Object k = k(this);
            sb.append("SUCCESS, result=[");
            sb.append(this.r(k));
            sb.append("]");
            return;
        }
        catch (final RuntimeException ex) {
            sb.append("UNKNOWN, cause=[");
            sb.append(ex.getClass());
            str = " thrown from get()]";
        }
        catch (final CancellationException ex2) {
            str = "CANCELLED";
        }
        catch (final ExecutionException ex3) {
            sb.append("FAILURE, cause=[");
            sb.append(ex3.getCause());
        }
        sb.append(str);
    }
    
    @Override
    public final void addListener(final Runnable runnable, final Executor executor) {
        e(runnable);
        e(executor);
        d c = this.b;
        if (c != AbstractResolvableFuture.d.d) {
            final d d = new d(runnable, executor);
            do {
                d.c = c;
                if (AbstractResolvableFuture.f.a(this, c, d)) {
                    return;
                }
            } while ((c = this.b) != AbstractResolvableFuture.d.d);
        }
        h(runnable, executor);
    }
    
    public void c() {
    }
    
    @Override
    public final boolean cancel(final boolean b) {
        final Object a = this.a;
        final boolean b2 = true;
        if (a == null | false) {
            c c;
            if (AbstractResolvableFuture.d) {
                c = new c(b, new CancellationException("Future.cancel() was called."));
            }
            else if (b) {
                c = AbstractResolvableFuture.c.c;
            }
            else {
                c = AbstractResolvableFuture.c.d;
            }
            if (AbstractResolvableFuture.f.b(this, a, c)) {
                if (b) {
                    this.l();
                }
                g(this);
                return b2;
            }
        }
        return false;
    }
    
    public final d f(d d) {
        d b;
        do {
            b = this.b;
        } while (!AbstractResolvableFuture.f.a(this, b, AbstractResolvableFuture.d.d));
        d c = d;
        d c2;
        for (d = b; d != null; d = c2) {
            c2 = d.c;
            d.c = c;
            c = d;
        }
        return c;
    }
    
    @Override
    public final Object get() {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object a = this.a;
        if (a != null & true) {
            return this.j(a);
        }
        g g = this.c;
        if (g != AbstractResolvableFuture.g.c) {
            final g g2 = new g();
            do {
                g2.a(g);
                if (AbstractResolvableFuture.f.c(this, g, g2)) {
                    Object a2;
                    do {
                        LockSupport.park(this);
                        if (Thread.interrupted()) {
                            this.o(g2);
                            throw new InterruptedException();
                        }
                        a2 = this.a;
                    } while (!(a2 != null & true));
                    return this.j(a2);
                }
            } while ((g = this.c) != AbstractResolvableFuture.g.c);
        }
        return this.j(this.a);
    }
    
    @Override
    public final Object get(long convert, final TimeUnit timeUnit) {
        long nanos = timeUnit.toNanos(convert);
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object a = this.a;
        if (a != null & true) {
            return this.j(a);
        }
        long n;
        if (nanos > 0L) {
            n = System.nanoTime() + nanos;
        }
        else {
            n = 0L;
        }
        long n2 = nanos;
        Label_0242: {
            if (nanos >= 1000L) {
                g g = this.c;
                if (g != AbstractResolvableFuture.g.c) {
                    final g g2 = new g();
                    do {
                        g2.a(g);
                        if (AbstractResolvableFuture.f.c(this, g, g2)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (Thread.interrupted()) {
                                    this.o(g2);
                                    throw new InterruptedException();
                                }
                                final Object a2 = this.a;
                                if (a2 != null & true) {
                                    return this.j(a2);
                                }
                                n2 = (nanos = n - System.nanoTime());
                            } while (n2 >= 1000L);
                            this.o(g2);
                            break Label_0242;
                        }
                    } while ((g = this.c) != AbstractResolvableFuture.g.c);
                }
                return this.j(this.a);
            }
        }
        while (n2 > 0L) {
            final Object a3 = this.a;
            if (a3 != null & true) {
                return this.j(a3);
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            n2 = n - System.nanoTime();
        }
        final String string = this.toString();
        final String string2 = timeUnit.toString();
        final Locale root = Locale.ROOT;
        final String lowerCase = string2.toLowerCase(root);
        final StringBuilder sb = new StringBuilder();
        sb.append("Waited ");
        sb.append(convert);
        sb.append(" ");
        sb.append(timeUnit.toString().toLowerCase(root));
        String s;
        final String str = s = sb.toString();
        if (n2 + 1000L < 0L) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(" (plus ");
            final String string3 = sb2.toString();
            final long sourceDuration = -n2;
            convert = timeUnit.convert(sourceDuration, TimeUnit.NANOSECONDS);
            final long lng = sourceDuration - timeUnit.toNanos(convert);
            final long n3 = lcmp(convert, 0L);
            final boolean b = n3 == 0 || lng > 1000L;
            String string4 = string3;
            if (n3 > 0) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string3);
                sb3.append(convert);
                sb3.append(" ");
                sb3.append(lowerCase);
                String s2 = sb3.toString();
                if (b) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(s2);
                    sb4.append(",");
                    s2 = sb4.toString();
                }
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(s2);
                sb5.append(" ");
                string4 = sb5.toString();
            }
            String string5 = string4;
            if (b) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string4);
                sb6.append(lng);
                sb6.append(" nanoseconds ");
                string5 = sb6.toString();
            }
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string5);
            sb7.append("delay)");
            s = sb7.toString();
        }
        if (this.isDone()) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(s);
            sb8.append(" but future completed as timeout expired");
            throw new TimeoutException(sb8.toString());
        }
        final StringBuilder sb9 = new StringBuilder();
        sb9.append(s);
        sb9.append(" for ");
        sb9.append(string);
        throw new TimeoutException(sb9.toString());
    }
    
    @Override
    public final boolean isCancelled() {
        return this.a instanceof c;
    }
    
    @Override
    public final boolean isDone() {
        return this.a != null & true;
    }
    
    public final Object j(final Object o) {
        if (o instanceof c) {
            throw d("Task was cancelled.", ((c)o).b);
        }
        if (!(o instanceof Failure)) {
            Object o2;
            if ((o2 = o) == AbstractResolvableFuture.g) {
                o2 = null;
            }
            return o2;
        }
        throw new ExecutionException(((Failure)o).a);
    }
    
    public void l() {
    }
    
    public String m() {
        if (this instanceof ScheduledFuture) {
            final StringBuilder sb = new StringBuilder();
            sb.append("remaining delay=[");
            sb.append(((ScheduledFuture)this).getDelay(TimeUnit.MILLISECONDS));
            sb.append(" ms]");
            return sb.toString();
        }
        return null;
    }
    
    public final void n() {
        g g;
        do {
            g = this.c;
        } while (!AbstractResolvableFuture.f.c(this, g, AbstractResolvableFuture.g.c));
        while (g != null) {
            g.b();
            g = g.b;
        }
    }
    
    public final void o(g c) {
        c.a = null;
    Label_0005:
        while (true) {
            c = this.c;
            if (c == AbstractResolvableFuture.g.c) {
                return;
            }
            g g = null;
            while (c != null) {
                final g b = c.b;
                g g2;
                if (c.a != null) {
                    g2 = c;
                }
                else if (g != null) {
                    g.b = b;
                    g2 = g;
                    if (g.a == null) {
                        continue Label_0005;
                    }
                }
                else {
                    g2 = g;
                    if (!AbstractResolvableFuture.f.c(this, c, b)) {
                        continue Label_0005;
                    }
                }
                c = b;
                g = g2;
            }
        }
    }
    
    public boolean p(final Object o) {
        Object g = o;
        if (o == null) {
            g = AbstractResolvableFuture.g;
        }
        if (AbstractResolvableFuture.f.b(this, null, g)) {
            g(this);
            return true;
        }
        return false;
    }
    
    public boolean q(final Throwable t) {
        if (AbstractResolvableFuture.f.b(this, null, new Failure((Throwable)e(t)))) {
            g(this);
            return true;
        }
        return false;
    }
    
    public final String r(final Object obj) {
        if (obj == this) {
            return "this future";
        }
        return String.valueOf(obj);
    }
    
    public final boolean s() {
        final Object a = this.a;
        return a instanceof c && ((c)a).a;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        Label_0150: {
            String str2 = null;
            Label_0035: {
                if (!this.isCancelled()) {
                    if (!this.isDone()) {
                        String str;
                        try {
                            str = this.m();
                        }
                        catch (final RuntimeException ex) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Exception thrown from implementation: ");
                            sb2.append(ex.getClass());
                            str = sb2.toString();
                        }
                        if (str != null && !str.isEmpty()) {
                            sb.append("PENDING, info=[");
                            sb.append(str);
                            sb.append("]");
                            break Label_0150;
                        }
                        if (!this.isDone()) {
                            str2 = "PENDING";
                            break Label_0035;
                        }
                    }
                    this.a(sb);
                    break Label_0150;
                }
                str2 = "CANCELLED";
            }
            sb.append(str2);
        }
        sb.append("]");
        return sb.toString();
    }
    
    public static final class Failure
    {
        public static final Failure b;
        public final Throwable a;
        
        static {
            b = new Failure(new Throwable("Failure occurred while trying to finish a future.") {
                @Override
                public Throwable fillInStackTrace() {
                    monitorenter(this);
                    monitorexit(this);
                    return this;
                }
            });
        }
        
        public Failure(final Throwable t) {
            this.a = (Throwable)AbstractResolvableFuture.e(t);
        }
    }
    
    public abstract static class b
    {
        public abstract boolean a(final AbstractResolvableFuture p0, final d p1, final d p2);
        
        public abstract boolean b(final AbstractResolvableFuture p0, final Object p1, final Object p2);
        
        public abstract boolean c(final AbstractResolvableFuture p0, final g p1, final g p2);
        
        public abstract void d(final g p0, final g p1);
        
        public abstract void e(final g p0, final Thread p1);
    }
    
    public static final class c
    {
        public static final c c;
        public static final c d;
        public final boolean a;
        public final Throwable b;
        
        static {
            if (AbstractResolvableFuture.d) {
                d = null;
                c = null;
            }
            else {
                d = new c(false, null);
                c = new c(true, null);
            }
        }
        
        public c(final boolean a, final Throwable b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class d
    {
        public static final d d;
        public final Runnable a;
        public final Executor b;
        public d c;
        
        static {
            d = new d(null, null);
        }
        
        public d(final Runnable a, final Executor b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class e extends b
    {
        public final AtomicReferenceFieldUpdater a;
        public final AtomicReferenceFieldUpdater b;
        public final AtomicReferenceFieldUpdater c;
        public final AtomicReferenceFieldUpdater d;
        public final AtomicReferenceFieldUpdater e;
        
        public e(final AtomicReferenceFieldUpdater a, final AtomicReferenceFieldUpdater b, final AtomicReferenceFieldUpdater c, final AtomicReferenceFieldUpdater d, final AtomicReferenceFieldUpdater e) {
            super(null);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        @Override
        public boolean a(final AbstractResolvableFuture abstractResolvableFuture, final d d, final d d2) {
            return z.a(this.d, abstractResolvableFuture, d, d2);
        }
        
        @Override
        public boolean b(final AbstractResolvableFuture abstractResolvableFuture, final Object o, final Object o2) {
            return z.a(this.e, abstractResolvableFuture, o, o2);
        }
        
        @Override
        public boolean c(final AbstractResolvableFuture abstractResolvableFuture, final g g, final g g2) {
            return z.a(this.c, abstractResolvableFuture, g, g2);
        }
        
        @Override
        public void d(final g g, final g g2) {
            this.b.lazySet(g, g2);
        }
        
        @Override
        public void e(final g g, final Thread thread) {
            this.a.lazySet(g, thread);
        }
    }
    
    public static final class f extends b
    {
        public f() {
            super(null);
        }
        
        @Override
        public boolean a(final AbstractResolvableFuture abstractResolvableFuture, final d d, final d b) {
            synchronized (abstractResolvableFuture) {
                if (abstractResolvableFuture.b == d) {
                    abstractResolvableFuture.b = b;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public boolean b(final AbstractResolvableFuture abstractResolvableFuture, final Object o, final Object a) {
            synchronized (abstractResolvableFuture) {
                if (abstractResolvableFuture.a == o) {
                    abstractResolvableFuture.a = a;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public boolean c(final AbstractResolvableFuture abstractResolvableFuture, final g g, final g c) {
            synchronized (abstractResolvableFuture) {
                if (abstractResolvableFuture.c == g) {
                    abstractResolvableFuture.c = c;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public void d(final g g, final g b) {
            g.b = b;
        }
        
        @Override
        public void e(final g g, final Thread a) {
            g.a = a;
        }
    }
    
    public static final class g
    {
        public static final g c;
        public volatile Thread a;
        public volatile g b;
        
        static {
            c = new g(false);
        }
        
        public g() {
            AbstractResolvableFuture.f.e(this, Thread.currentThread());
        }
        
        public g(final boolean b) {
        }
        
        public void a(final g g) {
            AbstractResolvableFuture.f.d(this, g);
        }
        
        public void b() {
            final Thread a = this.a;
            if (a != null) {
                this.a = null;
                LockSupport.unpark(a);
            }
        }
    }
}

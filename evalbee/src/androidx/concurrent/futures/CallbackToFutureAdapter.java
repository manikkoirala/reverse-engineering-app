// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executor;
import java.lang.ref.WeakReference;

public abstract class CallbackToFutureAdapter
{
    public static ik0 a(final b b) {
        final a a = new a();
        final c b2 = new c(a);
        a.b = b2;
        a.a = b.getClass();
        try {
            final Object a2 = b.a(a);
            if (a2 != null) {
                a.a = a2;
            }
        }
        catch (final Exception ex) {
            b2.c(ex);
        }
        return b2;
    }
    
    public static final class FutureGarbageCollectedException extends Throwable
    {
        public FutureGarbageCollectedException(final String message) {
            super(message);
        }
        
        @Override
        public Throwable fillInStackTrace() {
            monitorenter(this);
            monitorexit(this);
            return this;
        }
    }
    
    public static final class a
    {
        public Object a;
        public c b;
        public he1 c;
        public boolean d;
        
        public a() {
            this.c = he1.t();
        }
        
        public void a() {
            this.a = null;
            this.b = null;
            this.c.p(null);
        }
        
        public boolean b(final Object o) {
            boolean b = true;
            this.d = true;
            final c b2 = this.b;
            if (b2 == null || !b2.b(o)) {
                b = false;
            }
            if (b) {
                this.d();
            }
            return b;
        }
        
        public boolean c() {
            boolean b = true;
            this.d = true;
            final c b2 = this.b;
            if (b2 == null || !b2.a(true)) {
                b = false;
            }
            if (b) {
                this.d();
            }
            return b;
        }
        
        public final void d() {
            this.a = null;
            this.b = null;
            this.c = null;
        }
        
        public boolean e(final Throwable t) {
            boolean b = true;
            this.d = true;
            final c b2 = this.b;
            if (b2 == null || !b2.c(t)) {
                b = false;
            }
            if (b) {
                this.d();
            }
            return b;
        }
        
        public void finalize() {
            final c b = this.b;
            if (b != null && !b.isDone()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("The completer object was garbage collected - this future would otherwise never complete. The tag was: ");
                sb.append(this.a);
                b.c(new FutureGarbageCollectedException(sb.toString()));
            }
            if (!this.d) {
                final he1 c = this.c;
                if (c != null) {
                    c.p(null);
                }
            }
        }
    }
    
    public interface b
    {
        Object a(final a p0);
    }
    
    public static final class c implements ik0
    {
        public final WeakReference a;
        public final AbstractResolvableFuture b;
        
        public c(final a referent) {
            this.b = new AbstractResolvableFuture(this) {
                public final CallbackToFutureAdapter.c h;
                
                @Override
                public String m() {
                    final a a = (a)this.h.a.get();
                    if (a == null) {
                        return "Completer object has been garbage collected, future will fail soon";
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("tag=[");
                    sb.append(a.a);
                    sb.append("]");
                    return sb.toString();
                }
            };
            this.a = new WeakReference((T)referent);
        }
        
        public boolean a(final boolean b) {
            return this.b.cancel(b);
        }
        
        @Override
        public void addListener(final Runnable runnable, final Executor executor) {
            this.b.addListener(runnable, executor);
        }
        
        public boolean b(final Object o) {
            return this.b.p(o);
        }
        
        public boolean c(final Throwable t) {
            return this.b.q(t);
        }
        
        @Override
        public boolean cancel(final boolean b) {
            final a a = (a)this.a.get();
            final boolean cancel = this.b.cancel(b);
            if (cancel && a != null) {
                a.a();
            }
            return cancel;
        }
        
        @Override
        public Object get() {
            return this.b.get();
        }
        
        @Override
        public Object get(final long n, final TimeUnit timeUnit) {
            return this.b.get(n, timeUnit);
        }
        
        @Override
        public boolean isCancelled() {
            return this.b.isCancelled();
        }
        
        @Override
        public boolean isDone() {
            return this.b.isDone();
        }
        
        @Override
        public String toString() {
            return this.b.toString();
        }
    }
}

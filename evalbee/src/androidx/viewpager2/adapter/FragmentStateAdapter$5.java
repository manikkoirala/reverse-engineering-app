// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.adapter;

import androidx.lifecycle.Lifecycle;
import android.os.Handler;
import androidx.lifecycle.f;

class FragmentStateAdapter$5 implements f
{
    public final Handler a;
    public final Runnable b;
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.a.removeCallbacks(this.b);
            qj0.getLifecycle().c(this);
        }
    }
}

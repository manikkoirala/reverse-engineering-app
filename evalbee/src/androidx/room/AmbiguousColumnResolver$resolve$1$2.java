// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.jvm.internal.Lambda;

final class AmbiguousColumnResolver$resolve$1$2 extends Lambda implements c90
{
    final int $mappingIndex;
    final List<List<e4>> $mappingMatches;
    
    public AmbiguousColumnResolver$resolve$1$2(final List<? extends List<e4>> $mappingMatches, final int $mappingIndex) {
        this.$mappingMatches = (List<List<e4>>)$mappingMatches;
        this.$mappingIndex = $mappingIndex;
        super(1);
    }
    
    public final void invoke(@NotNull final List<Integer> list) {
        fg0.e((Object)list, "indices");
        final Iterable iterable = list;
        final Iterator iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        int intValue = ((Number)iterator.next()).intValue();
        while (iterator.hasNext()) {
            final int intValue2 = ((Number)iterator.next()).intValue();
            if (intValue > intValue2) {
                intValue = intValue2;
            }
        }
        final Iterator iterator2 = iterable.iterator();
        if (iterator2.hasNext()) {
            int intValue3 = ((Number)iterator2.next()).intValue();
            while (iterator2.hasNext()) {
                final int intValue4 = ((Number)iterator2.next()).intValue();
                if (intValue3 < intValue4) {
                    intValue3 = intValue4;
                }
            }
            this.$mappingMatches.get(this.$mappingIndex).add(new e4(new xf0(intValue, intValue3), list));
            return;
        }
        throw new NoSuchElementException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

public enum FtsOptions$MatchInfo
{
    private static final FtsOptions$MatchInfo[] $VALUES;
    
    FTS3, 
    FTS4;
    
    private static final /* synthetic */ FtsOptions$MatchInfo[] $values() {
        return new FtsOptions$MatchInfo[] { FtsOptions$MatchInfo.FTS3, FtsOptions$MatchInfo.FTS4 };
    }
    
    static {
        $VALUES = $values();
    }
}

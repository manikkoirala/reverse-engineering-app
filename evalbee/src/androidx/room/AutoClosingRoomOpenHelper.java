// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.content.ContentResolver;
import android.database.DataSetObserver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.database.CharArrayBuffer;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Locale;
import java.util.List;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.CancellationSignal;

public final class AutoClosingRoomOpenHelper implements ts1, pr
{
    public final ts1 a;
    public final y9 b;
    public final AutoClosingSupportSQLiteDatabase c;
    
    public AutoClosingRoomOpenHelper(final ts1 a, final y9 b) {
        fg0.e((Object)a, "delegate");
        fg0.e((Object)b, "autoCloser");
        this.a = a;
        (this.b = b).k(this.getDelegate());
        this.c = new AutoClosingSupportSQLiteDatabase(b);
    }
    
    @Override
    public ss1 F() {
        this.c.a();
        return this.c;
    }
    
    @Override
    public void close() {
        this.c.close();
    }
    
    @Override
    public String getDatabaseName() {
        return this.a.getDatabaseName();
    }
    
    @Override
    public ts1 getDelegate() {
        return this.a;
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.a.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
    
    public static final class AutoClosingSupportSQLiteDatabase implements ss1
    {
        public final y9 a;
        
        public AutoClosingSupportSQLiteDatabase(final y9 a) {
            fg0.e((Object)a, "autoCloser");
            this.a = a;
        }
        
        @Override
        public Cursor B(final vs1 vs1, final CancellationSignal cancellationSignal) {
            fg0.e((Object)vs1, "query");
            try {
                return (Cursor)new a(this.a.j().B(vs1, cancellationSignal), this.a);
            }
            finally {
                this.a.e();
            }
        }
        
        @Override
        public long C() {
            return ((Number)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$maximumSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$maximumSize$1.INSTANCE)).longValue();
        }
        
        @Override
        public long G(final String s, final int n, final ContentValues contentValues) {
            fg0.e((Object)s, "table");
            fg0.e((Object)contentValues, "values");
            return ((Number)this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$insert.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$insert$1(s, n, contentValues))).longValue();
        }
        
        @Override
        public Cursor L(final vs1 vs1) {
            fg0.e((Object)vs1, "query");
            try {
                return (Cursor)new a(this.a.j().L(vs1), this.a);
            }
            finally {
                this.a.e();
            }
        }
        
        @Override
        public void M(final String s) {
            fg0.e((Object)s, "sql");
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL$1(s));
        }
        
        @Override
        public boolean N() {
            return (boolean)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDatabaseIntegrityOk.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDatabaseIntegrityOk$1.INSTANCE);
        }
        
        @Override
        public void S() {
            final ss1 h = this.a.h();
            u02 a;
            if (h != null) {
                h.S();
                a = u02.a;
            }
            else {
                a = null;
            }
            if (a != null) {
                return;
            }
            throw new IllegalStateException("setTransactionSuccessful called but delegateDb is null".toString());
        }
        
        @Override
        public void T(final String s, final Object[] array) {
            fg0.e((Object)s, "sql");
            fg0.e((Object)array, "bindArgs");
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL$2(s, array));
        }
        
        @Override
        public long U(final long n) {
            return ((Number)this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaximumSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaximumSize$1(n))).longValue();
        }
        
        @Override
        public void X() {
            if (this.a.h() != null) {
                try {
                    final ss1 h = this.a.h();
                    fg0.b((Object)h);
                    h.X();
                    return;
                }
                finally {
                    this.a.e();
                }
            }
            throw new IllegalStateException("End transaction called but delegateDb is null".toString());
        }
        
        public final void a() {
            this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pokeOpen.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pokeOpen$1.INSTANCE);
        }
        
        @Override
        public void c0(final int n) {
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version$2(n));
        }
        
        @Override
        public void close() {
            this.a.d();
        }
        
        @Override
        public ws1 d0(final String s) {
            fg0.e((Object)s, "sql");
            return new AutoClosingSupportSqliteStatement(s, this.a);
        }
        
        @Override
        public int f0(final String s, final int n, final ContentValues contentValues, final String s2, final Object[] array) {
            fg0.e((Object)s, "table");
            fg0.e((Object)contentValues, "values");
            return ((Number)this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$update.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$update$1(s, n, contentValues, s2, array))).intValue();
        }
        
        @Override
        public boolean g0() {
            return (boolean)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$yieldIfContendedSafely.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$yieldIfContendedSafely$1.INSTANCE);
        }
        
        @Override
        public long getPageSize() {
            return ((Number)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize$1.INSTANCE)).longValue();
        }
        
        @Override
        public String getPath() {
            return (String)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$path.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$path$1.INSTANCE);
        }
        
        @Override
        public int getVersion() {
            return ((Number)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version$1.INSTANCE)).intValue();
        }
        
        @Override
        public Cursor h0(final String s) {
            fg0.e((Object)s, "query");
            try {
                return (Cursor)new a(this.a.j().h0(s), this.a);
            }
            finally {
                this.a.e();
            }
        }
        
        @Override
        public boolean isOpen() {
            final ss1 h = this.a.h();
            return h != null && h.isOpen();
        }
        
        @Override
        public boolean isReadOnly() {
            return (boolean)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isReadOnly.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isReadOnly$1.INSTANCE);
        }
        
        @Override
        public boolean j0() {
            return this.a.h() != null && (boolean)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$inTransaction.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$inTransaction$1.INSTANCE);
        }
        
        @Override
        public boolean k0() {
            return (boolean)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isWriteAheadLoggingEnabled.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isWriteAheadLoggingEnabled$1.INSTANCE);
        }
        
        @Override
        public int l(final String s, final String s2, final Object[] array) {
            fg0.e((Object)s, "table");
            return ((Number)this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$delete.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$delete$1(s, s2, array))).intValue();
        }
        
        @Override
        public void l0(final int n) {
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaxSqlCacheSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaxSqlCacheSize$1(n));
        }
        
        @Override
        public void m() {
            final ss1 j = this.a.j();
            try {
                j.m();
            }
            finally {
                this.a.e();
            }
        }
        
        @Override
        public List n() {
            return (List)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$attachedDbs.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$attachedDbs$1.INSTANCE);
        }
        
        @Override
        public void n0(final long n) {
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize$2(n));
        }
        
        @Override
        public void r() {
            final ss1 j = this.a.j();
            try {
                j.r();
            }
            finally {
                this.a.e();
            }
        }
        
        @Override
        public boolean s() {
            return this.a.h() != null && (boolean)this.a.g((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDbLockedByCurrentThread.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDbLockedByCurrentThread$1.INSTANCE);
        }
        
        @Override
        public void setLocale(final Locale locale) {
            fg0.e((Object)locale, "locale");
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setLocale.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setLocale$1(locale));
        }
        
        @Override
        public boolean t(final int n) {
            return (boolean)this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$needUpgrade.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$needUpgrade$1(n));
        }
        
        @Override
        public void z(final boolean b) {
            this.a.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setForeignKeyConstraintsEnabled.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setForeignKeyConstraintsEnabled$1(b));
        }
    }
    
    public static final class AutoClosingSupportSqliteStatement implements ws1
    {
        public final String a;
        public final y9 b;
        public final ArrayList c;
        
        public AutoClosingSupportSqliteStatement(final String a, final y9 b) {
            fg0.e((Object)a, "sql");
            fg0.e((Object)b, "autoCloser");
            this.a = a;
            this.b = b;
            this.c = new ArrayList();
        }
        
        @Override
        public void A(final int n, final long l) {
            this.e(n, l);
        }
        
        @Override
        public void D(final int n, final byte[] array) {
            fg0.e((Object)array, "value");
            this.e(n, array);
        }
        
        @Override
        public void I(final int n) {
            this.e(n, null);
        }
        
        @Override
        public void P(final int n, final double d) {
            this.e(n, d);
        }
        
        @Override
        public String W() {
            return (String)this.d((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForString.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForString$1.INSTANCE);
        }
        
        @Override
        public long b0() {
            return ((Number)this.d((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeInsert.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeInsert$1.INSTANCE)).longValue();
        }
        
        public final void c(final ws1 ws1) {
            final Iterator iterator = this.c.iterator();
            int index = 0;
            while (iterator.hasNext()) {
                iterator.next();
                final int n = index + 1;
                if (index < 0) {
                    nh.n();
                }
                final Object value = this.c.get(index);
                if (value == null) {
                    ws1.I(n);
                }
                else if (value instanceof Long) {
                    ws1.A(n, ((Number)value).longValue());
                }
                else if (value instanceof Double) {
                    ws1.P(n, ((Number)value).doubleValue());
                }
                else if (value instanceof String) {
                    ws1.y(n, (String)value);
                }
                else if (value instanceof byte[]) {
                    ws1.D(n, (byte[])value);
                }
                index = n;
            }
        }
        
        @Override
        public void close() {
        }
        
        public final Object d(final c90 c90) {
            return this.b.g((c90)new AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeSqliteStatementWithRefCount.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeSqliteStatementWithRefCount$1(this, c90));
        }
        
        public final void e(int size, final Object element) {
            final int index = size - 1;
            if (index >= this.c.size()) {
                size = this.c.size();
                if (size <= index) {
                    while (true) {
                        this.c.add(null);
                        if (size == index) {
                            break;
                        }
                        ++size;
                    }
                }
            }
            this.c.set(index, element);
        }
        
        @Override
        public void execute() {
            this.d((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$execute.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$execute$1.INSTANCE);
        }
        
        @Override
        public int p() {
            return ((Number)this.d((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeUpdateDelete.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeUpdateDelete$1.INSTANCE)).intValue();
        }
        
        @Override
        public long v() {
            return ((Number)this.d((c90)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForLong.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForLong$1.INSTANCE)).longValue();
        }
        
        @Override
        public void y(final int n, final String s) {
            fg0.e((Object)s, "value");
            this.e(n, s);
        }
    }
    
    public static final class a implements Cursor
    {
        public final Cursor a;
        public final y9 b;
        
        public a(final Cursor a, final y9 b) {
            fg0.e((Object)a, "delegate");
            fg0.e((Object)b, "autoCloser");
            this.a = a;
            this.b = b;
        }
        
        public void close() {
            this.a.close();
            this.b.e();
        }
        
        public void copyStringToBuffer(final int n, final CharArrayBuffer charArrayBuffer) {
            this.a.copyStringToBuffer(n, charArrayBuffer);
        }
        
        public void deactivate() {
            this.a.deactivate();
        }
        
        public byte[] getBlob(final int n) {
            return this.a.getBlob(n);
        }
        
        public int getColumnCount() {
            return this.a.getColumnCount();
        }
        
        public int getColumnIndex(final String s) {
            return this.a.getColumnIndex(s);
        }
        
        public int getColumnIndexOrThrow(final String s) {
            return this.a.getColumnIndexOrThrow(s);
        }
        
        public String getColumnName(final int n) {
            return this.a.getColumnName(n);
        }
        
        public String[] getColumnNames() {
            return this.a.getColumnNames();
        }
        
        public int getCount() {
            return this.a.getCount();
        }
        
        public double getDouble(final int n) {
            return this.a.getDouble(n);
        }
        
        public Bundle getExtras() {
            return this.a.getExtras();
        }
        
        public float getFloat(final int n) {
            return this.a.getFloat(n);
        }
        
        public int getInt(final int n) {
            return this.a.getInt(n);
        }
        
        public long getLong(final int n) {
            return this.a.getLong(n);
        }
        
        public Uri getNotificationUri() {
            return os1.a(this.a);
        }
        
        public List getNotificationUris() {
            return rs1.a(this.a);
        }
        
        public int getPosition() {
            return this.a.getPosition();
        }
        
        public short getShort(final int n) {
            return this.a.getShort(n);
        }
        
        public String getString(final int n) {
            return this.a.getString(n);
        }
        
        public int getType(final int n) {
            return this.a.getType(n);
        }
        
        public boolean getWantsAllOnMoveCalls() {
            return this.a.getWantsAllOnMoveCalls();
        }
        
        public boolean isAfterLast() {
            return this.a.isAfterLast();
        }
        
        public boolean isBeforeFirst() {
            return this.a.isBeforeFirst();
        }
        
        public boolean isClosed() {
            return this.a.isClosed();
        }
        
        public boolean isFirst() {
            return this.a.isFirst();
        }
        
        public boolean isLast() {
            return this.a.isLast();
        }
        
        public boolean isNull(final int n) {
            return this.a.isNull(n);
        }
        
        public boolean move(final int n) {
            return this.a.move(n);
        }
        
        public boolean moveToFirst() {
            return this.a.moveToFirst();
        }
        
        public boolean moveToLast() {
            return this.a.moveToLast();
        }
        
        public boolean moveToNext() {
            return this.a.moveToNext();
        }
        
        public boolean moveToPosition(final int n) {
            return this.a.moveToPosition(n);
        }
        
        public boolean moveToPrevious() {
            return this.a.moveToPrevious();
        }
        
        public void registerContentObserver(final ContentObserver contentObserver) {
            this.a.registerContentObserver(contentObserver);
        }
        
        public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
            this.a.registerDataSetObserver(dataSetObserver);
        }
        
        public boolean requery() {
            return this.a.requery();
        }
        
        public Bundle respond(final Bundle bundle) {
            return this.a.respond(bundle);
        }
        
        public void setExtras(final Bundle bundle) {
            fg0.e((Object)bundle, "extras");
            qs1.a(this.a, bundle);
        }
        
        public void setNotificationUri(final ContentResolver contentResolver, final Uri uri) {
            this.a.setNotificationUri(contentResolver, uri);
        }
        
        public void setNotificationUris(final ContentResolver contentResolver, final List list) {
            fg0.e((Object)contentResolver, "cr");
            fg0.e((Object)list, "uris");
            rs1.b(this.a, contentResolver, list);
        }
        
        public void unregisterContentObserver(final ContentObserver contentObserver) {
            this.a.unregisterContentObserver(contentObserver);
        }
        
        public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
            this.a.unregisterDataSetObserver(dataSetObserver);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

public enum FtsOptions$Order
{
    private static final FtsOptions$Order[] $VALUES;
    
    ASC, 
    DESC;
    
    private static final /* synthetic */ FtsOptions$Order[] $values() {
        return new FtsOptions$Order[] { FtsOptions$Order.ASC, FtsOptions$Order.DESC };
    }
    
    static {
        $VALUES = $values();
    }
}

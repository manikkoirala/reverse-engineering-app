// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.util.Log;
import java.util.TreeMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.io.File;
import java.util.concurrent.TimeUnit;
import android.content.Intent;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import android.app.ActivityManager;
import android.os.Looper;
import java.util.Iterator;
import java.util.BitSet;
import kotlin.collections.b;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.Callable;
import android.database.Cursor;
import android.os.CancellationSignal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.Map;
import java.util.List;
import java.util.concurrent.Executor;

public abstract class RoomDatabase
{
    public static final c o;
    public volatile ss1 a;
    public Executor b;
    public Executor c;
    public ts1 d;
    public final lg0 e;
    public boolean f;
    public boolean g;
    public List h;
    public Map i;
    public final ReentrantReadWriteLock j;
    public y9 k;
    public final ThreadLocal l;
    public final Map m;
    public final Map n;
    
    static {
        o = new c(null);
    }
    
    public RoomDatabase() {
        this.e = this.g();
        this.i = new LinkedHashMap();
        this.j = new ReentrantReadWriteLock();
        this.l = new ThreadLocal();
        final Map<Object, Object> synchronizedMap = Collections.synchronizedMap(new LinkedHashMap<Object, Object>());
        fg0.d((Object)synchronizedMap, "synchronizedMap(mutableMapOf())");
        this.m = synchronizedMap;
        this.n = new LinkedHashMap();
    }
    
    public Cursor A(final vs1 vs1, final CancellationSignal cancellationSignal) {
        fg0.e((Object)vs1, "query");
        this.c();
        this.d();
        Cursor cursor;
        if (cancellationSignal != null) {
            cursor = this.n().F().B(vs1, cancellationSignal);
        }
        else {
            cursor = this.n().F().L(vs1);
        }
        return cursor;
    }
    
    public Object C(final Callable callable) {
        fg0.e((Object)callable, "body");
        this.e();
        try {
            final Object call = callable.call();
            this.D();
            return call;
        }
        finally {
            this.i();
        }
    }
    
    public void D() {
        this.n().F().S();
    }
    
    public final Object E(final Class clazz, final ts1 ts1) {
        if (clazz.isInstance(ts1)) {
            return ts1;
        }
        Object e;
        if (ts1 instanceof pr) {
            e = this.E(clazz, ((pr)ts1).getDelegate());
        }
        else {
            e = null;
        }
        return e;
    }
    
    public void c() {
        if (this.f) {
            return;
        }
        if (this.y() ^ true) {
            return;
        }
        throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.".toString());
    }
    
    public void d() {
        if (this.t() || this.l.get() == null) {
            return;
        }
        throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.".toString());
    }
    
    public void e() {
        this.c();
        final y9 k = this.k;
        if (k == null) {
            this.v();
        }
        else {
            k.g((c90)new RoomDatabase$beginTransaction.RoomDatabase$beginTransaction$1(this));
        }
    }
    
    public ws1 f(final String s) {
        fg0.e((Object)s, "sql");
        this.c();
        this.d();
        return this.n().F().d0(s);
    }
    
    public abstract lg0 g();
    
    public abstract ts1 h(final pp p0);
    
    public void i() {
        final y9 k = this.k;
        if (k == null) {
            this.w();
        }
        else {
            k.g((c90)new RoomDatabase$endTransaction.RoomDatabase$endTransaction$1(this));
        }
    }
    
    public List j(final Map map) {
        fg0.e((Object)map, "autoMigrationSpecs");
        return nh.g();
    }
    
    public final Map k() {
        return this.m;
    }
    
    public final Lock l() {
        final ReentrantReadWriteLock.ReadLock lock = this.j.readLock();
        fg0.d((Object)lock, "readWriteLock.readLock()");
        return lock;
    }
    
    public lg0 m() {
        return this.e;
    }
    
    public ts1 n() {
        ts1 d;
        if ((d = this.d) == null) {
            fg0.t("internalOpenHelper");
            d = null;
        }
        return d;
    }
    
    public Executor o() {
        Executor b;
        if ((b = this.b) == null) {
            fg0.t("internalQueryExecutor");
            b = null;
        }
        return b;
    }
    
    public Set p() {
        return tm1.e();
    }
    
    public Map q() {
        return kotlin.collections.b.h();
    }
    
    public final ThreadLocal r() {
        return this.l;
    }
    
    public Executor s() {
        Executor c;
        if ((c = this.c) == null) {
            fg0.t("internalTransactionExecutor");
            c = null;
        }
        return c;
    }
    
    public boolean t() {
        return this.n().F().j0();
    }
    
    public void u(final pp pp) {
        fg0.e((Object)pp, "configuration");
        this.d = this.h(pp);
        final Set p = this.p();
        final BitSet set = new BitSet();
        final Iterator iterator = p.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            boolean b = true;
            final int n = -1;
            if (!hasNext) {
                int bitIndex = pp.r.size() - 1;
                if (bitIndex >= 0) {
                    while (true) {
                        final int n2 = bitIndex - 1;
                        if (!set.get(bitIndex)) {
                            throw new IllegalArgumentException("Unexpected auto migration specs found. Annotate AutoMigrationSpec implementation with @ProvidedAutoMigrationSpec annotation or remove this spec from the builder.".toString());
                        }
                        if (n2 < 0) {
                            break;
                        }
                        bitIndex = n2;
                    }
                }
                for (final kw0 kw0 : this.j(this.i)) {
                    if (!pp.d.c(kw0.a, kw0.b)) {
                        pp.d.b(kw0);
                    }
                }
                final bg1 bg1 = (bg1)this.E(bg1.class, this.n());
                if (bg1 != null) {
                    bg1.c(pp);
                }
                final AutoClosingRoomOpenHelper autoClosingRoomOpenHelper = (AutoClosingRoomOpenHelper)this.E(AutoClosingRoomOpenHelper.class, this.n());
                if (autoClosingRoomOpenHelper != null) {
                    this.k = autoClosingRoomOpenHelper.b;
                    this.m().o(autoClosingRoomOpenHelper.b);
                }
                final boolean b2 = pp.g == JournalMode.WRITE_AHEAD_LOGGING;
                this.n().setWriteAheadLoggingEnabled(b2);
                this.h = pp.e;
                this.b = pp.h;
                this.c = new ny1(pp.i);
                this.f = pp.f;
                this.g = b2;
                if (pp.j != null) {
                    if (pp.b == null) {
                        throw new IllegalArgumentException("Required value was null.".toString());
                    }
                    this.m().p(pp.a, pp.b, pp.j);
                }
                final Map q = this.q();
                final BitSet set2 = new BitSet();
                for (final Map.Entry<Class, V> entry : q.entrySet()) {
                    final Class clazz = entry.getKey();
                    for (final Class obj : (List)entry.getValue()) {
                        int bitIndex2 = pp.q.size() - 1;
                        Label_0725: {
                            if (bitIndex2 >= 0) {
                                while (true) {
                                    final int n3 = bitIndex2 - 1;
                                    if (obj.isAssignableFrom(pp.q.get(bitIndex2).getClass())) {
                                        set2.set(bitIndex2);
                                        break Label_0725;
                                    }
                                    if (n3 < 0) {
                                        break;
                                    }
                                    bitIndex2 = n3;
                                }
                            }
                            bitIndex2 = -1;
                        }
                        if (bitIndex2 < 0) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("A required type converter (");
                            sb.append(obj);
                            sb.append(") for ");
                            sb.append(clazz.getCanonicalName());
                            sb.append(" is missing in the database configuration.");
                            throw new IllegalArgumentException(sb.toString().toString());
                        }
                        this.n.put(obj, pp.q.get(bitIndex2));
                    }
                }
                int bitIndex3 = pp.q.size() - 1;
                if (bitIndex3 >= 0) {
                    while (true) {
                        final int n4 = bitIndex3 - 1;
                        if (!set2.get(bitIndex3)) {
                            final Object value = pp.q.get(bitIndex3);
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unexpected type converter ");
                            sb2.append(value);
                            sb2.append(". Annotate TypeConverter class with @ProvidedTypeConverter annotation or remove this converter from the builder.");
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        if (n4 < 0) {
                            break;
                        }
                        bitIndex3 = n4;
                    }
                }
                return;
            }
            final Class clazz2 = (Class)iterator.next();
            final int n5 = pp.r.size() - 1;
            int bitIndex4 = n;
            if (n5 >= 0) {
                bitIndex4 = n5;
                while (true) {
                    final int n6 = bitIndex4 - 1;
                    if (clazz2.isAssignableFrom(pp.r.get(bitIndex4).getClass())) {
                        set.set(bitIndex4);
                        break;
                    }
                    if (n6 < 0) {
                        bitIndex4 = n;
                        break;
                    }
                    bitIndex4 = n6;
                }
            }
            if (bitIndex4 < 0) {
                b = false;
            }
            if (!b) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("A required auto migration spec (");
                sb3.append(clazz2.getCanonicalName());
                sb3.append(") is missing in the database configuration.");
                throw new IllegalArgumentException(sb3.toString().toString());
            }
            this.i.put(clazz2, pp.r.get(bitIndex4));
        }
    }
    
    public final void v() {
        this.c();
        final ss1 f = this.n().F();
        this.m().t(f);
        if (f.k0()) {
            f.r();
        }
        else {
            f.m();
        }
    }
    
    public final void w() {
        this.n().F().X();
        if (!this.t()) {
            this.m().l();
        }
    }
    
    public void x(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        this.m().i(ss1);
    }
    
    public final boolean y() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
    
    public boolean z() {
        final y9 k = this.k;
        boolean b;
        if (k != null) {
            b = k.l();
        }
        else {
            final ss1 a = this.a;
            if (a == null) {
                final Boolean value = null;
                return fg0.a((Object)value, (Object)Boolean.TRUE);
            }
            b = a.isOpen();
        }
        final Boolean value = b;
        return fg0.a((Object)value, (Object)Boolean.TRUE);
    }
    
    public enum JournalMode
    {
        private static final JournalMode[] $VALUES;
        
        AUTOMATIC, 
        TRUNCATE, 
        WRITE_AHEAD_LOGGING;
        
        private static final /* synthetic */ JournalMode[] $values() {
            return new JournalMode[] { JournalMode.AUTOMATIC, JournalMode.TRUNCATE, JournalMode.WRITE_AHEAD_LOGGING };
        }
        
        static {
            $VALUES = $values();
        }
        
        private final boolean isLowRamDevice(final ActivityManager activityManager) {
            return os1.b(activityManager);
        }
        
        @NotNull
        public final JournalMode resolve$room_runtime_release(@NotNull final Context context) {
            fg0.e((Object)context, "context");
            if (this != JournalMode.AUTOMATIC) {
                return this;
            }
            final Object systemService = context.getSystemService("activity");
            fg0.c(systemService, "null cannot be cast to non-null type android.app.ActivityManager");
            if (!this.isLowRamDevice((ActivityManager)systemService)) {
                return JournalMode.WRITE_AHEAD_LOGGING;
            }
            return JournalMode.TRUNCATE;
        }
    }
    
    public static class a
    {
        public final Context a;
        public final Class b;
        public final String c;
        public final List d;
        public final List e;
        public List f;
        public Executor g;
        public Executor h;
        public ts1.c i;
        public boolean j;
        public JournalMode k;
        public Intent l;
        public boolean m;
        public boolean n;
        public long o;
        public TimeUnit p;
        public final d q;
        public Set r;
        public Set s;
        public String t;
        public File u;
        public Callable v;
        
        public a(final Context a, final Class b, final String c) {
            fg0.e((Object)a, "context");
            fg0.e((Object)b, "klass");
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = new ArrayList();
            this.e = new ArrayList();
            this.f = new ArrayList();
            this.k = JournalMode.AUTOMATIC;
            this.m = true;
            this.o = -1L;
            this.q = new d();
            this.r = new LinkedHashSet();
        }
        
        public a a(final b b) {
            fg0.e((Object)b, "callback");
            this.d.add(b);
            return this;
        }
        
        public a b(final kw0... original) {
            fg0.e((Object)original, "migrations");
            if (this.s == null) {
                this.s = new HashSet();
            }
            for (final kw0 kw0 : original) {
                final Set s = this.s;
                fg0.b((Object)s);
                s.add(kw0.a);
                final Set s2 = this.s;
                fg0.b((Object)s2);
                s2.add(kw0.b);
            }
            this.q.b((kw0[])Arrays.copyOf(original, original.length));
            return this;
        }
        
        public a c() {
            this.j = true;
            return this;
        }
        
        public RoomDatabase d() {
            final Executor g = this.g;
            Label_0073: {
                Executor executor;
                if (g == null && this.h == null) {
                    executor = b8.f();
                    this.h = executor;
                }
                else {
                    if (g != null && this.h == null) {
                        this.h = g;
                        break Label_0073;
                    }
                    if (g != null) {
                        break Label_0073;
                    }
                    executor = this.h;
                }
                this.g = executor;
            }
            final Set s = this.s;
            final int n = 1;
            if (s != null) {
                fg0.b((Object)s);
                final Iterator iterator = s.iterator();
                while (iterator.hasNext()) {
                    final int intValue = ((Number)iterator.next()).intValue();
                    if (this.r.contains(intValue) ^ true) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: ");
                    sb.append(intValue);
                    throw new IllegalArgumentException(sb.toString().toString());
                }
            }
            ts1.c i;
            if ((i = this.i) == null) {
                i = new r80();
            }
            ts1.c c2 = null;
            Label_0483: {
                if (i != null) {
                    ts1.c c = i;
                    if (this.o > 0L) {
                        if (this.c == null) {
                            throw new IllegalArgumentException("Cannot create auto-closing database for an in-memory database.".toString());
                        }
                        final long o = this.o;
                        final TimeUnit p = this.p;
                        if (p == null) {
                            throw new IllegalArgumentException("Required value was null.".toString());
                        }
                        final Executor g2 = this.g;
                        if (g2 == null) {
                            throw new IllegalArgumentException("Required value was null.".toString());
                        }
                        c = new z9(i, new y9(o, p, g2));
                    }
                    final String t = this.t;
                    if (t == null && this.u == null) {
                        c2 = c;
                        if (this.v == null) {
                            break Label_0483;
                        }
                    }
                    if (this.c == null) {
                        throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.".toString());
                    }
                    int n2;
                    if (t == null) {
                        n2 = 0;
                    }
                    else {
                        n2 = 1;
                    }
                    final File u = this.u;
                    int n3;
                    if (u == null) {
                        n3 = 0;
                    }
                    else {
                        n3 = 1;
                    }
                    final Callable v = this.v;
                    int n4;
                    if (v == null) {
                        n4 = 0;
                    }
                    else {
                        n4 = 1;
                    }
                    int n5;
                    if (n2 + n3 + n4 == 1) {
                        n5 = n;
                    }
                    else {
                        n5 = 0;
                    }
                    if (n5 == 0) {
                        throw new IllegalArgumentException("More than one of createFromAsset(), createFromInputStream(), and createFromFile() were called on this Builder, but the database can only be created using one of the three configurations.".toString());
                    }
                    c2 = new cg1(t, u, v, c);
                }
                else {
                    c2 = null;
                }
            }
            if (c2 == null) {
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            final Context a = this.a;
            final String c3 = this.c;
            final d q = this.q;
            final List d = this.d;
            final boolean j = this.j;
            final JournalMode resolve$room_runtime_release = this.k.resolve$room_runtime_release(a);
            final Executor g3 = this.g;
            if (g3 == null) {
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            final Executor h = this.h;
            if (h != null) {
                final pp pp = new pp(a, c3, c2, q, d, j, resolve$room_runtime_release, g3, h, this.l, this.m, this.n, this.r, this.t, this.u, this.v, null, this.e, this.f);
                final RoomDatabase roomDatabase = (RoomDatabase)pf1.b(this.b, "_Impl");
                roomDatabase.u(pp);
                return roomDatabase;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        
        public a e() {
            this.m = false;
            this.n = true;
            return this;
        }
        
        public a f(final ts1.c i) {
            this.i = i;
            return this;
        }
        
        public a g(final Executor g) {
            fg0.e((Object)g, "executor");
            this.g = g;
            return this;
        }
    }
    
    public abstract static class b
    {
        public void a(final ss1 ss1) {
            fg0.e((Object)ss1, "db");
        }
        
        public void b(final ss1 ss1) {
            fg0.e((Object)ss1, "db");
        }
        
        public void c(final ss1 ss1) {
            fg0.e((Object)ss1, "db");
        }
    }
    
    public static final class c
    {
    }
    
    public static class d
    {
        public final Map a;
        
        public d() {
            this.a = new LinkedHashMap();
        }
        
        public final void a(final kw0 obj) {
            final int a = obj.a;
            final int b = obj.b;
            final Map a2 = this.a;
            final Integer value = a;
            Object value2;
            if ((value2 = a2.get(value)) == null) {
                value2 = new TreeMap();
                a2.put(value, value2);
            }
            final TreeMap treeMap = (TreeMap)value2;
            if (treeMap.containsKey(b)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Overriding migration ");
                sb.append(treeMap.get(b));
                sb.append(" with ");
                sb.append(obj);
                Log.w("ROOM", sb.toString());
            }
            treeMap.put(b, obj);
        }
        
        public void b(final kw0... array) {
            fg0.e((Object)array, "migrations");
            for (int length = array.length, i = 0; i < length; ++i) {
                this.a(array[i]);
            }
        }
        
        public final boolean c(final int n, final int i) {
            final Map f = this.f();
            if (f.containsKey(n)) {
                Map h;
                if ((h = f.get(n)) == null) {
                    h = kotlin.collections.b.h();
                }
                return h.containsKey(i);
            }
            return false;
        }
        
        public List d(final int n, final int n2) {
            if (n == n2) {
                return nh.g();
            }
            return this.e(new ArrayList(), n2 > n, n, n2);
        }
        
        public final List e(final List list, final boolean b, int intValue, final int n) {
            int i;
        Label_0229:
            do {
                final int n2 = 1;
                if (!(b ? (intValue < n) : (intValue > n))) {
                    return list;
                }
                final TreeMap treeMap = this.a.get(intValue);
                if (treeMap == null) {
                    return null;
                }
                Set set;
                if (b) {
                    set = treeMap.descendingKeySet();
                }
                else {
                    set = treeMap.keySet();
                }
                for (final Integer key : set) {
                    boolean b2 = false;
                    Label_0185: {
                        Label_0182: {
                            if (b) {
                                fg0.d((Object)key, "targetVersion");
                                final int intValue2 = key;
                                if (intValue + 1 > intValue2 || intValue2 > n) {
                                    break Label_0182;
                                }
                            }
                            else {
                                fg0.d((Object)key, "targetVersion");
                                final int intValue3 = key;
                                if (n > intValue3 || intValue3 >= intValue) {
                                    break Label_0182;
                                }
                            }
                            b2 = true;
                            break Label_0185;
                        }
                        b2 = false;
                    }
                    if (b2) {
                        final Object value = treeMap.get(key);
                        fg0.b(value);
                        list.add(value);
                        intValue = key;
                        i = n2;
                        continue Label_0229;
                    }
                }
                i = 0;
            } while (i != 0);
            return null;
        }
        
        public Map f() {
            return this.a;
        }
    }
    
    public abstract static class e
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.a;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SharedSQLiteStatement
{
    public final RoomDatabase a;
    public final AtomicBoolean b;
    public final xi0 c;
    
    public SharedSQLiteStatement(final RoomDatabase a) {
        fg0.e((Object)a, "database");
        this.a = a;
        this.b = new AtomicBoolean(false);
        this.c = kotlin.a.a((a90)new SharedSQLiteStatement$stmt.SharedSQLiteStatement$stmt$2(this));
    }
    
    public ws1 b() {
        this.c();
        return this.g(this.b.compareAndSet(false, true));
    }
    
    public void c() {
        this.a.c();
    }
    
    public final ws1 d() {
        return this.a.f(this.e());
    }
    
    public abstract String e();
    
    public final ws1 f() {
        return (ws1)this.c.getValue();
    }
    
    public final ws1 g(final boolean b) {
        ws1 ws1;
        if (b) {
            ws1 = this.f();
        }
        else {
            ws1 = this.d();
        }
        return ws1;
    }
    
    public void h(final ws1 ws1) {
        fg0.e((Object)ws1, "statement");
        if (ws1 == this.f()) {
            this.b.set(false);
        }
    }
}

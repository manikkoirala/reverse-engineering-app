// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.os.IBinder;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import android.os.IInterface;
import java.util.LinkedHashMap;
import android.os.RemoteCallbackList;
import java.util.Map;
import android.app.Service;

public final class MultiInstanceInvalidationService extends Service
{
    public int a;
    public final Map b;
    public final RemoteCallbackList c;
    public final wd0.a d;
    
    public MultiInstanceInvalidationService() {
        this.b = new LinkedHashMap();
        this.c = new RemoteCallbackList(this) {
            public final MultiInstanceInvalidationService a;
            
            public void a(final vd0 vd0, final Object o) {
                fg0.e((Object)vd0, "callback");
                fg0.e(o, "cookie");
                this.a.b().remove(o);
            }
        };
        this.d = new wd0.a(this) {
            public final MultiInstanceInvalidationService a;
            
            public int i(final vd0 vd0, final String s) {
                fg0.e((Object)vd0, "callback");
                final int n = 0;
                if (s == null) {
                    return 0;
                }
                final RemoteCallbackList a = this.a.a();
                final MultiInstanceInvalidationService a2 = this.a;
                synchronized (a) {
                    a2.d(a2.c() + 1);
                    int c = a2.c();
                    if (a2.a().register((IInterface)vd0, (Object)c)) {
                        a2.b().put(c, s);
                    }
                    else {
                        a2.d(a2.c() - 1);
                        a2.c();
                        c = n;
                    }
                    return c;
                }
            }
            
            public void k(final int i, final String[] array) {
                fg0.e((Object)array, "tables");
                final RemoteCallbackList a = this.a.a();
                final MultiInstanceInvalidationService a2 = this.a;
                synchronized (a) {
                    final String s = a2.b().get(i);
                    if (s == null) {
                        Log.w("ROOM", "Remote invalidation client ID not registered");
                        return;
                    }
                    final int beginBroadcast = a2.a().beginBroadcast();
                    int j = 0;
                    while (j < beginBroadcast) {
                        try {
                            final Object broadcastCookie = a2.a().getBroadcastCookie(j);
                            fg0.c(broadcastCookie, "null cannot be cast to non-null type kotlin.Int");
                            final int intValue = (int)broadcastCookie;
                            final String s2 = a2.b().get(intValue);
                            if (i != intValue) {
                                if (fg0.a((Object)s, (Object)s2)) {
                                    try {
                                        ((vd0)a2.a().getBroadcastItem(j)).a(array);
                                    }
                                    catch (final RemoteException ex) {
                                        Log.w("ROOM", "Error invoking a remote callback", (Throwable)ex);
                                    }
                                }
                            }
                            ++j;
                            continue;
                        }
                        finally {
                            a2.a().finishBroadcast();
                        }
                        break;
                    }
                    a2.a().finishBroadcast();
                    final u02 a3 = u02.a;
                }
            }
            
            public void l(final vd0 vd0, final int i) {
                fg0.e((Object)vd0, "callback");
                final RemoteCallbackList a = this.a.a();
                final MultiInstanceInvalidationService a2 = this.a;
                synchronized (a) {
                    a2.a().unregister((IInterface)vd0);
                    final String s = a2.b().remove(i);
                }
            }
        };
    }
    
    public final RemoteCallbackList a() {
        return this.c;
    }
    
    public final Map b() {
        return this.b;
    }
    
    public final int c() {
        return this.a;
    }
    
    public final void d(final int a) {
        this.a = a;
    }
    
    public IBinder onBind(final Intent intent) {
        fg0.e((Object)intent, "intent");
        return (IBinder)this.d;
    }
}

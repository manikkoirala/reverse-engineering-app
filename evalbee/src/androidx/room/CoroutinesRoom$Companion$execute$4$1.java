// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n$a;
import org.jetbrains.annotations.Nullable;
import kotlinx.coroutines.n;
import android.os.CancellationSignal;
import kotlin.jvm.internal.Lambda;

final class CoroutinesRoom$Companion$execute$4$1 extends Lambda implements c90
{
    final CancellationSignal $cancellationSignal;
    final n $job;
    
    public CoroutinesRoom$Companion$execute$4$1(final CancellationSignal $cancellationSignal, final n $job) {
        this.$cancellationSignal = $cancellationSignal;
        this.$job = $job;
        super(1);
    }
    
    public final void invoke(@Nullable final Throwable t) {
        ns1.a(this.$cancellationSignal);
        n$a.a(this.$job, (CancellationException)null, 1, (Object)null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.concurrent.Callable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.room.CoroutinesRoom$Companion$execute$2", f = "CoroutinesRoom.kt", l = {}, m = "invokeSuspend")
final class CoroutinesRoom$Companion$execute$2 extends SuspendLambda implements q90
{
    final Callable<Object> $callable;
    int label;
    
    public CoroutinesRoom$Companion$execute$2(final Callable<Object> $callable, final vl vl) {
        this.$callable = $callable;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new CoroutinesRoom$Companion$execute$2(this.$callable, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((CoroutinesRoom$Companion$execute$2)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        gg0.d();
        if (this.label == 0) {
            xe1.b(o);
            return this.$callable.call();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

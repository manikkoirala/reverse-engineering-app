// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.Result$a;
import kotlin.Result;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.concurrent.Callable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.room.CoroutinesRoom$Companion$execute$4$job$1", f = "CoroutinesRoom.kt", l = {}, m = "invokeSuspend")
final class CoroutinesRoom$Companion$execute$4$job$1 extends SuspendLambda implements q90
{
    final Callable<Object> $callable;
    final df $continuation;
    int label;
    
    public CoroutinesRoom$Companion$execute$4$job$1(final Callable<Object> $callable, final df $continuation, final vl vl) {
        this.$callable = $callable;
        this.$continuation = $continuation;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new CoroutinesRoom$Companion$execute$4$job$1(this.$callable, this.$continuation, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((CoroutinesRoom$Companion$execute$4$job$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull Object call) {
        gg0.d();
        if (this.label == 0) {
            xe1.b(call);
            try {
                call = this.$callable.call();
                ((vl)this.$continuation).resumeWith(Result.constructor-impl(call));
            }
            finally {
                final df $continuation = this.$continuation;
                final Result$a companion = Result.Companion;
                final Throwable t;
                ((vl)$continuation).resumeWith(Result.constructor-impl(xe1.a(t)));
            }
            return u02.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$b;
import java.util.concurrent.RejectedExecutionException;
import kotlinx.coroutines.c;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.n;
import java.util.concurrent.Executor;

public abstract class RoomDatabaseKt
{
    public static final Object b(final Executor executor, final n n, final vl vl) {
        final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
        c.z();
        ((df)c).y((c90)new RoomDatabaseKt$acquireTransactionThread$2.RoomDatabaseKt$acquireTransactionThread$2$1(n));
        try {
            executor.execute((Runnable)new RoomDatabaseKt$acquireTransactionThread$2.RoomDatabaseKt$acquireTransactionThread$2$2((df)c, n));
        }
        catch (final RejectedExecutionException cause) {
            ((df)c).l((Throwable)new IllegalStateException("Unable to acquire a thread to perform the database transaction.", cause));
        }
        final Object v = c.v();
        if (v == gg0.d()) {
            zp.c(vl);
        }
        return v;
    }
    
    public static final Object c(RoomDatabase l$0, final vl vl) {
        Object o = null;
        Label_0046: {
            if (vl instanceof RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1) {
                final RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1 roomDatabaseKt$createTransactionContext$1 = (RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)vl;
                final int label = roomDatabaseKt$createTransactionContext$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    roomDatabaseKt$createTransactionContext$1.label = label + Integer.MIN_VALUE;
                    o = roomDatabaseKt$createTransactionContext$1;
                    break Label_0046;
                }
            }
            o = new RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1(vl);
        }
        Object result = ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).label;
        pi pi2;
        if (label2 != 0) {
            if (label2 != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            final pi pi = (pi)((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$1;
            l$0 = (RoomDatabase)((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$0;
            xe1.b(result);
            pi2 = pi;
        }
        else {
            xe1.b(result);
            final pi b = ah0.b((n)null, 1, (Object)null);
            final n n = (n)((vl)o).getContext().get((CoroutineContext$b)kotlinx.coroutines.n.r1);
            if (n != null) {
                n.q((c90)new RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$2(b));
            }
            final Executor s = l$0.s();
            ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$0 = l$0;
            ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$1 = b;
            ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).label = 1;
            final Object b2 = b(s, (n)b, (vl)o);
            if (b2 == d) {
                return d;
            }
            pi2 = b;
            result = b2;
        }
        final xl xl = (xl)result;
        return ((CoroutineContext)xl).plus((CoroutineContext)new ly1((n)pi2, xl)).plus((CoroutineContext)rv1.a(l$0.r(), (Object)pc.b(System.identityHashCode(pi2))));
    }
    
    public static final Object d(RoomDatabase l$0, c90 l$2, final vl vl) {
        Object o = null;
        Label_0050: {
            if (vl instanceof RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1) {
                final RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1 roomDatabaseKt$withTransaction$1 = (RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)vl;
                final int label = roomDatabaseKt$withTransaction$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    roomDatabaseKt$withTransaction$1.label = label + Integer.MIN_VALUE;
                    o = roomDatabaseKt$withTransaction$1;
                    break Label_0050;
                }
            }
            o = new RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1(vl);
        }
        Object o2 = ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).label;
        Object e = null;
        Label_0209: {
            if (label2 != 0) {
                if (label2 != 1) {
                    if (label2 == 2) {
                        xe1.b(o2);
                        return o2;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                else {
                    l$2 = (c90)((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).L$1;
                    l$0 = (RoomDatabase)((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).L$0;
                    xe1.b(o2);
                }
            }
            else {
                xe1.b(o2);
                final ly1 ly1 = (ly1)((vl)o).getContext().get((CoroutineContext$b)ly1.d);
                if (ly1 != null) {
                    e = ly1.e();
                    if (e != null) {
                        break Label_0209;
                    }
                }
                ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).L$0 = l$0;
                ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).L$1 = l$2;
                ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).label = 1;
                if ((o2 = c(l$0, (vl)o)) == d) {
                    return d;
                }
            }
            e = o2;
        }
        final RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$2 roomDatabaseKt$withTransaction$2 = new RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$2(l$0, l$2, (vl)null);
        ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).L$0 = null;
        ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).L$1 = null;
        ((RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)o).label = 2;
        if ((o2 = ad.g((CoroutineContext)e, (q90)roomDatabaseKt$withTransaction$2, (vl)o)) == d) {
            return d;
        }
        return o2;
    }
}

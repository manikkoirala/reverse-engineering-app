// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

public enum BuiltInTypeConverters$State
{
    private static final BuiltInTypeConverters$State[] $VALUES;
    
    DISABLED, 
    ENABLED, 
    INHERITED;
    
    private static final /* synthetic */ BuiltInTypeConverters$State[] $values() {
        return new BuiltInTypeConverters$State[] { BuiltInTypeConverters$State.ENABLED, BuiltInTypeConverters$State.DISABLED, BuiltInTypeConverters$State.INHERITED };
    }
    
    static {
        $VALUES = $values();
    }
}

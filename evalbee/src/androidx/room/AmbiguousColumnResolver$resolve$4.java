// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.jvm.internal.Lambda;

final class AmbiguousColumnResolver$resolve$4 extends Lambda implements c90
{
    final Ref$ObjectRef<f4> $bestSolution;
    
    public AmbiguousColumnResolver$resolve$4(final Ref$ObjectRef<f4> $bestSolution) {
        this.$bestSolution = $bestSolution;
        super(1);
    }
    
    public final void invoke(@NotNull final List<e4> list) {
        fg0.e((Object)list, "it");
        final f4 a = f4.d.a(list);
        if (a.a((f4)this.$bestSolution.element) < 0) {
            this.$bestSolution.element = a;
        }
    }
}

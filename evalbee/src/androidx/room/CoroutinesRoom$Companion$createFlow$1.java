// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlinx.coroutines.f;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.ChannelIterator;
import kotlin.coroutines.CoroutineContext$b;
import java.util.Set;
import kotlinx.coroutines.channels.BufferOverflow;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.concurrent.Callable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "androidx.room.CoroutinesRoom$Companion$createFlow$1", f = "CoroutinesRoom.kt", l = { 110 }, m = "invokeSuspend")
final class CoroutinesRoom$Companion$createFlow$1 extends SuspendLambda implements q90
{
    final Callable<Object> $callable;
    final RoomDatabase $db;
    final boolean $inTransaction;
    final String[] $tableNames;
    private Object L$0;
    int label;
    
    public CoroutinesRoom$Companion$createFlow$1(final boolean $inTransaction, final RoomDatabase $db, final String[] $tableNames, final Callable<Object> $callable, final vl vl) {
        this.$inTransaction = $inTransaction;
        this.$db = $db;
        this.$tableNames = $tableNames;
        this.$callable = $callable;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
        final CoroutinesRoom$Companion$createFlow$1 coroutinesRoom$Companion$createFlow$1 = new CoroutinesRoom$Companion$createFlow$1(this.$inTransaction, this.$db, this.$tableNames, this.$callable, vl);
        coroutinesRoom$Companion$createFlow$1.L$0 = l$0;
        return (vl)coroutinesRoom$Companion$createFlow$1;
    }
    
    @Nullable
    public final Object invoke(@NotNull final z40 z40, @Nullable final vl vl) {
        return ((CoroutinesRoom$Companion$createFlow$1)this.create(z40, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        final Object d = gg0.d();
        final int label = this.label;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(o);
        }
        else {
            xe1.b(o);
            final q90 q90 = (q90)new q90(this.$inTransaction, this.$db, (z40)this.L$0, this.$tableNames, this.$callable, null) {
                final z40 $$this$flow;
                final Callable<Object> $callable;
                final RoomDatabase $db;
                final boolean $inTransaction;
                final String[] $tableNames;
                private Object L$0;
                int label;
                
                @NotNull
                public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
                    final q90 q90 = (q90)new q90(this.$inTransaction, this.$db, this.$$this$flow, this.$tableNames, this.$callable, vl) {
                        final z40 $$this$flow;
                        final Callable<Object> $callable;
                        final RoomDatabase $db;
                        final boolean $inTransaction;
                        final String[] $tableNames;
                        private Object L$0;
                        int label;
                    };
                    q90.L$0 = l$0;
                    return (vl)q90;
                }
                
                @Nullable
                public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
                    return ((CoroutinesRoom$Companion$createFlow$1$1)this.create(lm, vl)).invokeSuspend(u02.a);
                }
                
                @Nullable
                public final Object invokeSuspend(@NotNull final Object o) {
                    final Object d = gg0.d();
                    final int label = this.label;
                    if (label != 0) {
                        if (label != 1) {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        xe1.b(o);
                    }
                    else {
                        xe1.b(o);
                        final lm lm = (lm)this.L$0;
                        final vf b = bg.b(-1, (BufferOverflow)null, (c90)null, 6, (Object)null);
                        final lg0.c c = new lg0.c(this.$tableNames, b) {
                            public final vf b;
                            
                            @Override
                            public void c(final Set set) {
                                fg0.e((Object)set, "tables");
                                ((zk1)this.b).m((Object)u02.a);
                            }
                        };
                        ((zk1)b).m((Object)u02.a);
                        final ly1 ly1 = (ly1)lm.C().get((CoroutineContext$b)ly1.d);
                        Object o2;
                        if (ly1 == null || (o2 = ly1.e()) == null) {
                            if (this.$inTransaction) {
                                o2 = om.b(this.$db);
                            }
                            else {
                                o2 = om.a(this.$db);
                            }
                        }
                        final vf b2 = bg.b(0, (BufferOverflow)null, (c90)null, 7, (Object)null);
                        ad.d(lm, (CoroutineContext)o2, (CoroutineStart)null, (q90)new q90(this.$db, c, b, this.$callable, b2, null) {
                            final Callable<Object> $callable;
                            final RoomDatabase $db;
                            final CoroutinesRoom$Companion$createFlow$1$1$a $observer;
                            final vf $observerChannel;
                            final vf $resultChannel;
                            Object L$0;
                            int label;
                            
                            @NotNull
                            public final vl create(@Nullable final Object o, @NotNull final vl vl) {
                                return (vl)new q90(this.$db, this.$observer, this.$observerChannel, this.$callable, this.$resultChannel, vl) {
                                    final Callable<Object> $callable;
                                    final RoomDatabase $db;
                                    final CoroutinesRoom$Companion$createFlow$1$1$a $observer;
                                    final vf $observerChannel;
                                    final vf $resultChannel;
                                    Object L$0;
                                    int label;
                                };
                            }
                            
                            @Nullable
                            public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
                                return ((CoroutinesRoom$Companion$createFlow$1$1$1)this.create(lm, vl)).invokeSuspend(u02.a);
                            }
                            
                            @Nullable
                            public final Object invokeSuspend(@NotNull Object o) {
                                final Object d = gg0.d();
                                final int label = this.label;
                                Label_0071: {
                                    if (label == 0) {
                                        break Label_0071;
                                    }
                                    Label_0050: {
                                        if (label == 1) {
                                            break Label_0050;
                                        }
                                        Label_0040: {
                                            if (label != 2) {
                                                break Label_0040;
                                            }
                                            ChannelIterator iterator = (ChannelIterator)this.L$0;
                                            try {
                                                xe1.b(o);
                                            Label_0139_Outer:
                                                while (true) {
                                                    Object o2 = this;
                                                    while (true) {
                                                        while (true) {
                                                            o = o2;
                                                            try {
                                                                ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).L$0 = iterator;
                                                                o = o2;
                                                                ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).label = 1;
                                                                o = o2;
                                                                final Object b = iterator.b((vl)o2);
                                                                if (b == d) {
                                                                    return d;
                                                                }
                                                                o = o2;
                                                                if (!(boolean)b) {
                                                                    ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).$db.m().m((lg0.c)((CoroutinesRoom$Companion$createFlow$1$1$1)o2).$observer);
                                                                    return u02.a;
                                                                }
                                                                o = o2;
                                                                iterator.next();
                                                                o = o2;
                                                                final Object call = ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).$callable.call();
                                                                o = o2;
                                                                final vf $resultChannel = ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).$resultChannel;
                                                                o = o2;
                                                                ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).L$0 = iterator;
                                                                o = o2;
                                                                ((CoroutinesRoom$Companion$createFlow$1$1$1)o2).label = 2;
                                                                o = o2;
                                                                if (((zk1)$resultChannel).z(call, (vl)o2) == d) {
                                                                    return d;
                                                                }
                                                                continue Label_0139_Outer;
                                                            }
                                                            finally {}
                                                        }
                                                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                                                        iterator = (ChannelIterator)this.L$0;
                                                        xe1.b(o);
                                                        o2 = this;
                                                        final Object b = o;
                                                        continue;
                                                    }
                                                    xe1.b(o);
                                                    this.$db.m().b((lg0.c)this.$observer);
                                                    iterator = ((ReceiveChannel)this.$observerChannel).iterator();
                                                    continue Label_0139_Outer;
                                                }
                                            }
                                            finally {}
                                        }
                                    }
                                }
                                this.$db.m().m((lg0.c)this.$observer);
                            }
                        }, 2, (Object)null);
                        final z40 $$this$flow = this.$$this$flow;
                        this.label = 1;
                        if (d50.m($$this$flow, (ReceiveChannel)b2, (vl)this) == d) {
                            return d;
                        }
                    }
                    return u02.a;
                }
            };
            this.label = 1;
            if (f.e((q90)q90, (vl)this) == d) {
                return d;
            }
        }
        return u02.a;
    }
}

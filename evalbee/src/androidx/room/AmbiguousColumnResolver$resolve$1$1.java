// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Iterator;
import java.util.ArrayList;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.jvm.internal.Lambda;

final class AmbiguousColumnResolver$resolve$1$1 extends Lambda implements s90
{
    final String[] $mapping;
    final int $mappingIndex;
    final List<List<e4>> $mappingMatches;
    
    public AmbiguousColumnResolver$resolve$1$1(final String[] $mapping, final List<? extends List<e4>> $mappingMatches, final int $mappingIndex) {
        this.$mapping = $mapping;
        this.$mappingMatches = (List<List<e4>>)$mappingMatches;
        this.$mappingIndex = $mappingIndex;
        super(3);
    }
    
    public final void invoke(final int n, final int n2, @NotNull final List<Object> list) {
        fg0.e((Object)list, "resultColumnsSublist");
        final String[] $mapping = this.$mapping;
        final ArrayList list2 = new ArrayList($mapping.length);
        if ($mapping.length <= 0) {
            this.$mappingMatches.get(this.$mappingIndex).add(new e4(new xf0(n, n2 - 1), list2));
            return;
        }
        final String s = $mapping[0];
        final Iterator iterator = list.iterator();
        if (!iterator.hasNext()) {
            zu0.a(null);
            return;
        }
        zu0.a(iterator.next());
        throw null;
    }
}

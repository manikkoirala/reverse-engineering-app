// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation.experimental;

public enum Experimental$Level
{
    private static final Experimental$Level[] $VALUES;
    
    ERROR, 
    WARNING;
    
    private static final /* synthetic */ Experimental$Level[] $values() {
        return new Experimental$Level[] { Experimental$Level.WARNING, Experimental$Level.ERROR };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

public enum RestrictTo$Scope
{
    private static final RestrictTo$Scope[] $VALUES;
    
    GROUP_ID, 
    LIBRARY, 
    LIBRARY_GROUP, 
    LIBRARY_GROUP_PREFIX, 
    SUBCLASSES, 
    TESTS;
    
    private static final /* synthetic */ RestrictTo$Scope[] $values() {
        return new RestrictTo$Scope[] { RestrictTo$Scope.LIBRARY, RestrictTo$Scope.LIBRARY_GROUP, RestrictTo$Scope.LIBRARY_GROUP_PREFIX, RestrictTo$Scope.GROUP_ID, RestrictTo$Scope.TESTS, RestrictTo$Scope.SUBCLASSES };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

public enum RequiresOptIn$Level
{
    private static final RequiresOptIn$Level[] $VALUES;
    
    ERROR, 
    WARNING;
    
    private static final /* synthetic */ RequiresOptIn$Level[] $values() {
        return new RequiresOptIn$Level[] { RequiresOptIn$Level.WARNING, RequiresOptIn$Level.ERROR };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

public enum InspectableProperty$ValueType
{
    private static final InspectableProperty$ValueType[] $VALUES;
    
    COLOR, 
    GRAVITY, 
    INFERRED, 
    INT_ENUM, 
    INT_FLAG, 
    NONE, 
    RESOURCE_ID;
    
    private static final /* synthetic */ InspectableProperty$ValueType[] $values() {
        return new InspectableProperty$ValueType[] { InspectableProperty$ValueType.NONE, InspectableProperty$ValueType.INFERRED, InspectableProperty$ValueType.INT_ENUM, InspectableProperty$ValueType.INT_FLAG, InspectableProperty$ValueType.COLOR, InspectableProperty$ValueType.GRAVITY, InspectableProperty$ValueType.RESOURCE_ID };
    }
    
    static {
        $VALUES = $values();
    }
}

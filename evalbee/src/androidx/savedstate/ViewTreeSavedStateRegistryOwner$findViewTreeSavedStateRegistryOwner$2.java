// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.jvm.internal.Lambda;

final class ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$2 extends Lambda implements c90
{
    public static final ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$2 INSTANCE;
    
    static {
        INSTANCE = new ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$2();
    }
    
    public ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$2() {
        super(1);
    }
    
    @Nullable
    public final aj1 invoke(@NotNull final View view) {
        fg0.e((Object)view, "view");
        final Object tag = view.getTag(jb1.a);
        aj1 aj1;
        if (tag instanceof aj1) {
            aj1 = (aj1)tag;
        }
        else {
            aj1 = null;
        }
        return aj1;
    }
}

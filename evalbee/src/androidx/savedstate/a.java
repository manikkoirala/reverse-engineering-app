// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import android.os.BaseBundle;
import java.util.Iterator;
import java.util.Map;
import androidx.lifecycle.Lifecycle;
import android.os.Bundle;

public final class a
{
    public static final b g;
    public final si1 a;
    public boolean b;
    public Bundle c;
    public boolean d;
    public Recreator.b e;
    public boolean f;
    
    static {
        g = new b(null);
    }
    
    public a() {
        this.a = new si1();
        this.f = true;
    }
    
    public static final void d(final a a, final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)a, "this$0");
        fg0.e((Object)qj0, "<anonymous parameter 0>");
        fg0.e((Object)event, "event");
        boolean f;
        if (event == Lifecycle.Event.ON_START) {
            f = true;
        }
        else {
            if (event != Lifecycle.Event.ON_STOP) {
                return;
            }
            f = false;
        }
        a.f = f;
    }
    
    public final Bundle b(final String s) {
        fg0.e((Object)s, "key");
        if (!this.d) {
            throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component".toString());
        }
        final Bundle c = this.c;
        if (c != null) {
            Bundle bundle;
            if (c != null) {
                bundle = c.getBundle(s);
            }
            else {
                bundle = null;
            }
            final Bundle c2 = this.c;
            if (c2 != null) {
                c2.remove(s);
            }
            final Bundle c3 = this.c;
            int n = 0;
            if (c3 != null) {
                n = n;
                if (!((BaseBundle)c3).isEmpty()) {
                    n = 1;
                }
            }
            if (n == 0) {
                this.c = null;
            }
            return bundle;
        }
        return null;
    }
    
    public final c c(final String s) {
        fg0.e((Object)s, "key");
        for (final Map.Entry<String, V> entry : this.a) {
            fg0.d((Object)entry, "components");
            final String s2 = entry.getKey();
            final c c = (c)entry.getValue();
            if (fg0.a((Object)s2, (Object)s)) {
                return c;
            }
        }
        return null;
    }
    
    public final void e(final Lifecycle lifecycle) {
        fg0.e((Object)lifecycle, "lifecycle");
        if (this.b ^ true) {
            lifecycle.a(new yi1(this));
            this.b = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already attached.".toString());
    }
    
    public final void f(Bundle bundle) {
        if (!this.b) {
            throw new IllegalStateException("You must call performAttach() before calling performRestore(Bundle).".toString());
        }
        if (this.d ^ true) {
            if (bundle != null) {
                bundle = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
            }
            else {
                bundle = null;
            }
            this.c = bundle;
            this.d = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already restored.".toString());
    }
    
    public final void g(final Bundle bundle) {
        fg0.e((Object)bundle, "outBundle");
        final Bundle bundle2 = new Bundle();
        final Bundle c = this.c;
        if (c != null) {
            bundle2.putAll(c);
        }
        final si1.d c2 = this.a.c();
        fg0.d((Object)c2, "this.components.iteratorWithAdditions()");
        while (c2.hasNext()) {
            final Map.Entry<String, V> entry = c2.next();
            bundle2.putBundle((String)entry.getKey(), ((c)entry.getValue()).a());
        }
        if (!((BaseBundle)bundle2).isEmpty()) {
            bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
        }
    }
    
    public final void h(final String s, final c c) {
        fg0.e((Object)s, "key");
        fg0.e((Object)c, "provider");
        if (this.a.l(s, c) == null) {
            return;
        }
        throw new IllegalArgumentException("SavedStateProvider with the given key is already registered".toString());
    }
    
    public final void i(Class name) {
        fg0.e((Object)name, "clazz");
        if (this.f) {
            Recreator.b e;
            if ((e = this.e) == null) {
                e = new Recreator.b(this);
            }
            this.e = e;
            try {
                ((Class)name).getDeclaredConstructor((Class[])new Class[0]);
                final Recreator.b e2 = this.e;
                if (e2 != null) {
                    name = ((Class)name).getName();
                    fg0.d((Object)name, "clazz.name");
                    e2.b(name);
                }
                return;
            }
            catch (final NoSuchMethodException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Class ");
                sb.append(((Class)name).getSimpleName());
                sb.append(" must have default constructor in order to be automatically recreated");
                throw new IllegalArgumentException(sb.toString(), cause);
            }
        }
        throw new IllegalStateException("Can not perform this action after onSaveInstanceState".toString());
    }
    
    public interface a
    {
        void a(final aj1 p0);
    }
    
    public static final class b
    {
    }
    
    public interface c
    {
        Bundle a();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.ArrayList;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.f;

public final class Recreator implements f
{
    public static final a b;
    public final aj1 a;
    
    static {
        b = new a(null);
    }
    
    public Recreator(final aj1 a) {
        fg0.e((Object)a, "owner");
        this.a = a;
    }
    
    @Override
    public void d(final qj0 qj0, final Lifecycle.Event event) {
        fg0.e((Object)qj0, "source");
        fg0.e((Object)event, "event");
        if (event != Lifecycle.Event.ON_CREATE) {
            throw new AssertionError((Object)"Next event must be ON_CREATE");
        }
        qj0.getLifecycle().c(this);
        final Bundle b = this.a.getSavedStateRegistry().b("androidx.savedstate.Restarter");
        if (b == null) {
            return;
        }
        final ArrayList stringArrayList = b.getStringArrayList("classes_to_restore");
        if (stringArrayList != null) {
            final Iterator iterator = stringArrayList.iterator();
            while (iterator.hasNext()) {
                this.f((String)iterator.next());
            }
            return;
        }
        throw new IllegalStateException("Bundle with restored state for the component \"androidx.savedstate.Restarter\" must contain list of strings by the key \"classes_to_restore\"");
    }
    
    public final void f(final String ex) {
        try {
            final Class<? extends androidx.savedstate.a.a> subclass = Class.forName((String)ex, false, Recreator.class.getClassLoader()).asSubclass(androidx.savedstate.a.a.class);
            fg0.d((Object)subclass, "{\n                Class.\u2026class.java)\n            }");
            try {
                final Constructor declaredConstructor = subclass.getDeclaredConstructor((Class<?>[])new Class[0]);
                declaredConstructor.setAccessible(true);
                try {
                    final Object instance = declaredConstructor.newInstance(new Object[0]);
                    fg0.d(instance, "{\n                constr\u2026wInstance()\n            }");
                    ((androidx.savedstate.a.a)instance).a(this.a);
                }
                catch (final Exception subclass) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to instantiate ");
                    sb.append((String)ex);
                    throw new RuntimeException(sb.toString(), (Throwable)subclass);
                }
            }
            catch (final NoSuchMethodException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Class ");
                sb2.append(subclass.getSimpleName());
                sb2.append(" must have default constructor in order to be automatically recreated");
                throw new IllegalStateException(sb2.toString(), ex);
            }
        }
        catch (final ClassNotFoundException cause) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Class ");
            sb3.append((String)ex);
            sb3.append(" wasn't found");
            throw new RuntimeException(sb3.toString(), cause);
        }
    }
    
    public static final class a
    {
    }
    
    public static final class b implements c
    {
        public final Set a;
        
        public b(final a a) {
            fg0.e((Object)a, "registry");
            this.a = new LinkedHashSet();
            a.h("androidx.savedstate.Restarter", (c)this);
        }
        
        @Override
        public Bundle a() {
            final Bundle bundle = new Bundle();
            bundle.putStringArrayList("classes_to_restore", new ArrayList(this.a));
            return bundle;
        }
        
        public final void b(final String s) {
            fg0.e((Object)s, "className");
            this.a.add(s);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.cardview.widget;

import android.view.View$MeasureSpec;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.widget.FrameLayout;

public class CardView extends FrameLayout
{
    private static final int[] COLOR_BACKGROUND_ATTR;
    private static final nf IMPL;
    private final mf mCardViewDelegate;
    private boolean mCompatPadding;
    final Rect mContentPadding;
    private boolean mPreventCornerOverlap;
    final Rect mShadowBounds;
    int mUserSetMinHeight;
    int mUserSetMinWidth;
    
    static {
        COLOR_BACKGROUND_ATTR = new int[] { 16842801 };
        (IMPL = new lf()).o();
    }
    
    public CardView(final Context context, final AttributeSet set) {
        this(context, set, oa1.a);
    }
    
    public CardView(final Context context, final AttributeSet set, int n) {
        super(context, set, n);
        final Rect mContentPadding = new Rect();
        this.mContentPadding = mContentPadding;
        this.mShadowBounds = new Rect();
        final mf mCardViewDelegate = new mf(this) {
            public Drawable a;
            public final CardView b;
            
            @Override
            public boolean a() {
                return this.b.getUseCompatPadding();
            }
            
            @Override
            public void b(final Drawable drawable) {
                this.a = drawable;
                ((View)this.b).setBackgroundDrawable(drawable);
            }
            
            @Override
            public Drawable c() {
                return this.a;
            }
            
            @Override
            public boolean d() {
                return this.b.getPreventCornerOverlap();
            }
            
            @Override
            public View e() {
                return (View)this.b;
            }
            
            @Override
            public void setShadowPadding(final int n, final int n2, final int n3, final int n4) {
                this.b.mShadowBounds.set(n, n2, n3, n4);
                final CardView b = this.b;
                final Rect mContentPadding = b.mContentPadding;
                CardView.access$001(b, n + mContentPadding.left, n2 + mContentPadding.top, n3 + mContentPadding.right, n4 + mContentPadding.bottom);
            }
        };
        this.mCardViewDelegate = mCardViewDelegate;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, ub1.a, n, rb1.a);
        n = ub1.d;
        ColorStateList list;
        if (obtainStyledAttributes.hasValue(n)) {
            list = obtainStyledAttributes.getColorStateList(n);
        }
        else {
            final TypedArray obtainStyledAttributes2 = ((View)this).getContext().obtainStyledAttributes(CardView.COLOR_BACKGROUND_ATTR);
            n = obtainStyledAttributes2.getColor(0, 0);
            obtainStyledAttributes2.recycle();
            final float[] array = new float[3];
            Color.colorToHSV(n, array);
            Resources resources;
            if (array[2] > 0.5f) {
                resources = ((View)this).getResources();
                n = va1.b;
            }
            else {
                resources = ((View)this).getResources();
                n = va1.a;
            }
            list = ColorStateList.valueOf(resources.getColor(n));
        }
        final float dimension = obtainStyledAttributes.getDimension(ub1.e, 0.0f);
        final float dimension2 = obtainStyledAttributes.getDimension(ub1.f, 0.0f);
        float dimension3 = obtainStyledAttributes.getDimension(ub1.g, 0.0f);
        this.mCompatPadding = obtainStyledAttributes.getBoolean(ub1.i, false);
        this.mPreventCornerOverlap = obtainStyledAttributes.getBoolean(ub1.h, true);
        n = obtainStyledAttributes.getDimensionPixelSize(ub1.j, 0);
        mContentPadding.left = obtainStyledAttributes.getDimensionPixelSize(ub1.l, n);
        mContentPadding.top = obtainStyledAttributes.getDimensionPixelSize(ub1.n, n);
        mContentPadding.right = obtainStyledAttributes.getDimensionPixelSize(ub1.m, n);
        mContentPadding.bottom = obtainStyledAttributes.getDimensionPixelSize(ub1.k, n);
        if (dimension2 > dimension3) {
            dimension3 = dimension2;
        }
        this.mUserSetMinWidth = obtainStyledAttributes.getDimensionPixelSize(ub1.b, 0);
        this.mUserSetMinHeight = obtainStyledAttributes.getDimensionPixelSize(ub1.c, 0);
        obtainStyledAttributes.recycle();
        CardView.IMPL.i(mCardViewDelegate, context, list, dimension, dimension2, dimension3);
    }
    
    public static /* synthetic */ void access$001(final CardView cardView, final int n, final int n2, final int n3, final int n4) {
        ((View)cardView).setPadding(n, n2, n3, n4);
    }
    
    public ColorStateList getCardBackgroundColor() {
        return CardView.IMPL.l(this.mCardViewDelegate);
    }
    
    public float getCardElevation() {
        return CardView.IMPL.b(this.mCardViewDelegate);
    }
    
    public int getContentPaddingBottom() {
        return this.mContentPadding.bottom;
    }
    
    public int getContentPaddingLeft() {
        return this.mContentPadding.left;
    }
    
    public int getContentPaddingRight() {
        return this.mContentPadding.right;
    }
    
    public int getContentPaddingTop() {
        return this.mContentPadding.top;
    }
    
    public float getMaxCardElevation() {
        return CardView.IMPL.n(this.mCardViewDelegate);
    }
    
    public boolean getPreventCornerOverlap() {
        return this.mPreventCornerOverlap;
    }
    
    public float getRadius() {
        return CardView.IMPL.k(this.mCardViewDelegate);
    }
    
    public boolean getUseCompatPadding() {
        return this.mCompatPadding;
    }
    
    public void onMeasure(int measureSpec, final int n) {
        final nf impl = CardView.IMPL;
        int n2 = measureSpec;
        int measureSpec2 = n;
        if (!(impl instanceof lf)) {
            final int mode = View$MeasureSpec.getMode(measureSpec);
            if (mode == Integer.MIN_VALUE || mode == 1073741824) {
                measureSpec = View$MeasureSpec.makeMeasureSpec(Math.max((int)Math.ceil(impl.e(this.mCardViewDelegate)), View$MeasureSpec.getSize(measureSpec)), mode);
            }
            final int mode2 = View$MeasureSpec.getMode(n);
            if (mode2 != Integer.MIN_VALUE && mode2 != 1073741824) {
                n2 = measureSpec;
                measureSpec2 = n;
            }
            else {
                measureSpec2 = View$MeasureSpec.makeMeasureSpec(Math.max((int)Math.ceil(impl.h(this.mCardViewDelegate)), View$MeasureSpec.getSize(n)), mode2);
                n2 = measureSpec;
            }
        }
        super.onMeasure(n2, measureSpec2);
    }
    
    public void setCardBackgroundColor(final int n) {
        CardView.IMPL.j(this.mCardViewDelegate, ColorStateList.valueOf(n));
    }
    
    public void setCardBackgroundColor(final ColorStateList list) {
        CardView.IMPL.j(this.mCardViewDelegate, list);
    }
    
    public void setCardElevation(final float n) {
        CardView.IMPL.m(this.mCardViewDelegate, n);
    }
    
    public void setContentPadding(final int n, final int n2, final int n3, final int n4) {
        this.mContentPadding.set(n, n2, n3, n4);
        CardView.IMPL.g(this.mCardViewDelegate);
    }
    
    public void setMaxCardElevation(final float n) {
        CardView.IMPL.c(this.mCardViewDelegate, n);
    }
    
    public void setMinimumHeight(final int mUserSetMinHeight) {
        super.setMinimumHeight(this.mUserSetMinHeight = mUserSetMinHeight);
    }
    
    public void setMinimumWidth(final int mUserSetMinWidth) {
        super.setMinimumWidth(this.mUserSetMinWidth = mUserSetMinWidth);
    }
    
    public void setPadding(final int n, final int n2, final int n3, final int n4) {
    }
    
    public void setPaddingRelative(final int n, final int n2, final int n3, final int n4) {
    }
    
    public void setPreventCornerOverlap(final boolean mPreventCornerOverlap) {
        if (mPreventCornerOverlap != this.mPreventCornerOverlap) {
            this.mPreventCornerOverlap = mPreventCornerOverlap;
            CardView.IMPL.a(this.mCardViewDelegate);
        }
    }
    
    public void setRadius(final float n) {
        CardView.IMPL.d(this.mCardViewDelegate, n);
    }
    
    public void setUseCompatPadding(final boolean mCompatPadding) {
        if (this.mCompatPadding != mCompatPadding) {
            this.mCompatPadding = mCompatPadding;
            CardView.IMPL.f(this.mCardViewDelegate);
        }
    }
}

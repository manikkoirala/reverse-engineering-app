import android.app.Dialog;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.AdError;
import android.util.Log;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import android.app.ProgressDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import android.app.Activity;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import android.content.Context;
import com.google.android.gms.ads.rewarded.RewardedAd;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class y3
{
    public static RewardedAd a;
    public static String b = "ca-app-pub-8698273815250571/8966728877";
    
    public static /* synthetic */ RewardedAd a() {
        return y3.a;
    }
    
    public static /* synthetic */ RewardedAd b(final RewardedAd a) {
        return y3.a = a;
    }
    
    public static void c(final Context context) {
        MobileAds.initialize(context, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(final InitializationStatus initializationStatus) {
            }
        });
    }
    
    public static void d(final Activity activity, final OnUserEarnedRewardListener onUserEarnedRewardListener, final c c) {
        final AdRequest build = new AdRequest.Builder().build();
        final ProgressDialog progressDialog = new ProgressDialog((Context)activity);
        progressDialog.setMessage((CharSequence)"Please wait, loading video...");
        ((Dialog)progressDialog).show();
        RewardedAd.load((Context)activity, y3.b, build, new RewardedAdLoadCallback(progressDialog, c, activity, onUserEarnedRewardListener) {
            public final ProgressDialog a;
            public final c b;
            public final Activity c;
            public final OnUserEarnedRewardListener d;
            
            public void a(final RewardedAd rewardedAd) {
                ((Dialog)this.a).dismiss();
                y3.b(rewardedAd);
                y3.a().setFullScreenContentCallback(new FullScreenContentCallback(this) {
                    public final y3$b a;
                    
                    @Override
                    public void onAdClicked() {
                        Log.d("ADMOB", "Ad was clicked.");
                    }
                    
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        Log.d("ADMOB", "Ad dismissed fullscreen content.");
                        y3.b(null);
                    }
                    
                    @Override
                    public void onAdFailedToShowFullScreenContent(final AdError adError) {
                        Log.e("ADMOB", "Ad failed to show fullscreen content.");
                        y3.b(null);
                    }
                    
                    @Override
                    public void onAdImpression() {
                        Log.d("ADMOB", "Ad recorded an impression.");
                    }
                    
                    @Override
                    public void onAdShowedFullScreenContent() {
                        Log.d("ADMOB", "Ad showed fullscreen content.");
                    }
                });
                y3.a().show(this.c, this.d);
            }
            
            @Override
            public void onAdFailedToLoad(final LoadAdError loadAdError) {
                ((Dialog)this.a).dismiss();
                Log.d("ADMOB", loadAdError.toString());
                final c b = this.b;
                if (b != null) {
                    b.a(loadAdError);
                }
                y3.b(null);
            }
        });
    }
    
    public interface c
    {
        void a(final LoadAdError p0);
    }
}

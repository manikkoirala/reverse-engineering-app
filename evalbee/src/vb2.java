import com.google.firebase.FirebaseNetworkException;
import com.google.android.gms.tasks.OnFailureListener;

// 
// Decompiled by Procyon v0.6.0
// 

public final class vb2 implements OnFailureListener
{
    public final qb2 a;
    
    public vb2(final qb2 a) {
        this.a = a;
    }
    
    public final void onFailure(final Exception ex) {
        if (ex instanceof FirebaseNetworkException) {
            sb2.a().v("Failure to refresh token; scheduling refresh after failure", new Object[0]);
            this.a.b.d();
        }
    }
}

import android.os.BaseBundle;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.Task;
import android.os.Bundle;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.Context;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public class dp
{
    public final SharedPreferences a;
    public final r10 b;
    public final Object c;
    public TaskCompletionSource d;
    public boolean e;
    public boolean f;
    public Boolean g;
    public final TaskCompletionSource h;
    
    public dp(final r10 b) {
        final Object c = new Object();
        this.c = c;
        this.d = new TaskCompletionSource();
        this.e = false;
        this.f = false;
        this.h = new TaskCompletionSource();
        final Context l = b.l();
        this.b = b;
        this.a = CommonUtils.q(l);
        Boolean g;
        if ((g = this.b()) == null) {
            g = this.a(l);
        }
        this.g = g;
        synchronized (c) {
            if (this.d()) {
                this.d.trySetResult((Object)null);
                this.e = true;
            }
        }
    }
    
    public static Boolean g(final Context context) {
        try {
            final PackageManager packageManager = context.getPackageManager();
            if (packageManager != null) {
                final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null) {
                    final Bundle metaData = applicationInfo.metaData;
                    if (metaData != null && ((BaseBundle)metaData).containsKey("firebase_crashlytics_collection_enabled")) {
                        return ((BaseBundle)applicationInfo.metaData).getBoolean("firebase_crashlytics_collection_enabled");
                    }
                }
            }
        }
        catch (final PackageManager$NameNotFoundException ex) {
            zl0.f().e("Could not read data collection permission from manifest", (Throwable)ex);
        }
        return null;
    }
    
    public final Boolean a(final Context context) {
        final Boolean g = g(context);
        if (g == null) {
            this.f = false;
            return null;
        }
        this.f = true;
        return Boolean.TRUE.equals(g);
    }
    
    public final Boolean b() {
        if (this.a.contains("firebase_crashlytics_collection_enabled")) {
            this.f = false;
            return this.a.getBoolean("firebase_crashlytics_collection_enabled", true);
        }
        return null;
    }
    
    public void c(final boolean b) {
        if (b) {
            this.h.trySetResult((Object)null);
            return;
        }
        throw new IllegalStateException("An invalid data collection token was used.");
    }
    
    public boolean d() {
        synchronized (this) {
            final Boolean g = this.g;
            boolean b;
            if (g != null) {
                b = g;
            }
            else {
                b = this.e();
            }
            this.f(b);
            return b;
        }
    }
    
    public final boolean e() {
        try {
            return this.b.v();
        }
        catch (final IllegalStateException ex) {
            return false;
        }
    }
    
    public final void f(final boolean b) {
        String s;
        if (b) {
            s = "ENABLED";
        }
        else {
            s = "DISABLED";
        }
        String s2;
        if (this.g == null) {
            s2 = "global Firebase setting";
        }
        else if (this.f) {
            s2 = "firebase_crashlytics_collection_enabled manifest flag";
        }
        else {
            s2 = "API";
        }
        zl0.f().b(String.format("Crashlytics automatic data collection %s by %s.", s, s2));
    }
    
    public Task h() {
        synchronized (this.c) {
            return this.d.getTask();
        }
    }
    
    public Task i(final Executor executor) {
        return v22.o(executor, this.h.getTask(), this.h());
    }
}

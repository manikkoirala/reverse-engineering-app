import androidx.constraintlayout.core.widgets.h;
import android.view.View;
import android.view.ViewParent;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.widget.b;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class i52 extends b
{
    public boolean j;
    public boolean k;
    
    public i52(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public void f(final ConstraintLayout constraintLayout) {
        this.e(constraintLayout);
    }
    
    @Override
    public void i(final AttributeSet set) {
        super.i(set);
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, vb1.n1);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == vb1.u1) {
                    this.j = true;
                }
                else if (index == vb1.B1) {
                    this.k = true;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.j || this.k) {
            final ViewParent parent = this.getParent();
            if (parent instanceof ConstraintLayout) {
                final ConstraintLayout constraintLayout = (ConstraintLayout)parent;
                final int visibility = this.getVisibility();
                final float elevation = this.getElevation();
                for (int i = 0; i < super.b; ++i) {
                    final View viewById = constraintLayout.getViewById(super.a[i]);
                    if (viewById != null) {
                        if (this.j) {
                            viewById.setVisibility(visibility);
                        }
                        if (this.k && elevation > 0.0f) {
                            viewById.setTranslationZ(viewById.getTranslationZ() + elevation);
                        }
                    }
                }
            }
        }
    }
    
    public abstract void p(final h p0, final int p1, final int p2);
    
    public void setElevation(final float elevation) {
        super.setElevation(elevation);
        this.d();
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        this.d();
    }
}

import java.util.concurrent.Callable;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class xm
{
    public final Executor a;
    public Task b;
    public final Object c;
    public final ThreadLocal d;
    
    public xm(final Executor a) {
        this.b = Tasks.forResult((Object)null);
        this.c = new Object();
        this.d = new ThreadLocal();
        (this.a = a).execute(new Runnable(this) {
            public final xm a;
            
            @Override
            public void run() {
                xm.a(this.a).set(Boolean.TRUE);
            }
        });
    }
    
    public static /* synthetic */ ThreadLocal a(final xm xm) {
        return xm.d;
    }
    
    public void b() {
        if (this.e()) {
            return;
        }
        throw new IllegalStateException("Not running on background worker thread as intended.");
    }
    
    public Executor c() {
        return this.a;
    }
    
    public final Task d(final Task task) {
        return task.continueWith(this.a, (Continuation)new Continuation(this) {
            public final xm a;
            
            public Void a(final Task task) {
                return null;
            }
        });
    }
    
    public final boolean e() {
        return Boolean.TRUE.equals(this.d.get());
    }
    
    public final Continuation f(final Callable callable) {
        return (Continuation)new Continuation(this, callable) {
            public final Callable a;
            public final xm b;
            
            public Object then(final Task task) {
                return this.a.call();
            }
        };
    }
    
    public Task g(final Callable callable) {
        synchronized (this.c) {
            final Task continueWith = this.b.continueWith(this.a, this.f(callable));
            this.b = this.d(continueWith);
            return continueWith;
        }
    }
    
    public Task h(final Callable callable) {
        synchronized (this.c) {
            final Task continueWithTask = this.b.continueWithTask(this.a, this.f(callable));
            this.b = this.d(continueWithTask);
            return continueWithTask;
        }
    }
}

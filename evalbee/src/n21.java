import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class n21
{
    public static final String a;
    
    static {
        a = xl0.i("PackageManagerHelper");
    }
    
    public static int a(final Context context, final String s) {
        return context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, s));
    }
    
    public static boolean b(final int n, final boolean b) {
        if (n == 0) {
            return b;
        }
        boolean b2 = true;
        if (n != 1) {
            b2 = false;
        }
        return b2;
    }
    
    public static void c(final Context context, final Class clazz, final boolean b) {
        final String s = "enabled";
        try {
            if (b == b(a(context, clazz.getName()), false)) {
                final xl0 e = xl0.e();
                final String a = n21.a;
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping component enablement for ");
                sb.append(clazz.getName());
                e.a(a, sb.toString());
                return;
            }
            final PackageManager packageManager = context.getPackageManager();
            final ComponentName componentName = new ComponentName(context, clazz.getName());
            int n;
            if (b) {
                n = 1;
            }
            else {
                n = 2;
            }
            packageManager.setComponentEnabledSetting(componentName, n, 1);
            final xl0 e2 = xl0.e();
            final String a2 = n21.a;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(clazz.getName());
            sb2.append(" ");
            String str;
            if (b) {
                str = "enabled";
            }
            else {
                str = "disabled";
            }
            sb2.append(str);
            e2.a(a2, sb2.toString());
        }
        catch (final Exception ex) {
            final xl0 e3 = xl0.e();
            final String a3 = n21.a;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(clazz.getName());
            sb3.append("could not be ");
            String str2;
            if (b) {
                str2 = s;
            }
            else {
                str2 = "disabled";
            }
            sb3.append(str2);
            e3.b(a3, sb3.toString(), ex);
        }
    }
}

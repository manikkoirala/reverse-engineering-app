import java.util.Map;
import kotlinx.coroutines.CoroutineDispatcher;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class om
{
    public static final CoroutineDispatcher a(final RoomDatabase roomDatabase) {
        fg0.e((Object)roomDatabase, "<this>");
        final Map k = roomDatabase.k();
        Object o;
        if ((o = k.get("QueryDispatcher")) == null) {
            o = xy.a(roomDatabase.o());
            k.put("QueryDispatcher", o);
        }
        fg0.c(o, "null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
        return (CoroutineDispatcher)o;
    }
    
    public static final CoroutineDispatcher b(final RoomDatabase roomDatabase) {
        fg0.e((Object)roomDatabase, "<this>");
        final Map k = roomDatabase.k();
        Object o;
        if ((o = k.get("TransactionDispatcher")) == null) {
            o = xy.a(roomDatabase.s());
            k.put("TransactionDispatcher", o);
        }
        fg0.c(o, "null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
        return (CoroutineDispatcher)o;
    }
}

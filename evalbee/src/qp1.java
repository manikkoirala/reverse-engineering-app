import androidx.work.WorkerParameters;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qp1 implements Runnable
{
    public final q81 a;
    public final op1 b;
    public final WorkerParameters.a c;
    
    public qp1(final q81 a, final op1 b, final WorkerParameters.a c) {
        fg0.e((Object)a, "processor");
        fg0.e((Object)b, "startStopToken");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public void run() {
        this.a.s(this.b, this.c);
    }
}

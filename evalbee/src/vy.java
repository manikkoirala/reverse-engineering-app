import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class vy
{
    public static void a(final String s, final ExecutorService executorService) {
        b(s, executorService, 2L, TimeUnit.SECONDS);
    }
    
    public static void b(final String str, final ExecutorService executorService, final long n, final TimeUnit timeUnit) {
        final Runtime runtime = Runtime.getRuntime();
        final ab target = new ab(str, executorService, n, timeUnit) {
            public final String a;
            public final ExecutorService b;
            public final long c;
            public final TimeUnit d;
            
            @Override
            public void a() {
                try {
                    final zl0 f = zl0.f();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Executing shutdown hook for ");
                    sb.append(this.a);
                    f.b(sb.toString());
                    this.b.shutdown();
                    if (!this.b.awaitTermination(this.c, this.d)) {
                        final zl0 f2 = zl0.f();
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(this.a);
                        sb2.append(" did not shut down in the allocated time. Requesting immediate shutdown.");
                        f2.b(sb2.toString());
                        this.b.shutdownNow();
                    }
                }
                catch (final InterruptedException ex) {
                    zl0.f().b(String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", this.a));
                    this.b.shutdownNow();
                }
            }
        };
        final StringBuilder sb = new StringBuilder();
        sb.append("Crashlytics Shutdown Hook for ");
        sb.append(str);
        runtime.addShutdownHook(new Thread(target, sb.toString()));
    }
    
    public static ExecutorService c(final String s) {
        final ExecutorService e = e(d(s), new ThreadPoolExecutor.DiscardPolicy());
        a(s, e);
        return e;
    }
    
    public static ThreadFactory d(final String s) {
        return new ThreadFactory(s, new AtomicLong(1L)) {
            public final String a;
            public final AtomicLong b;
            
            @Override
            public Thread newThread(final Runnable runnable) {
                final Thread thread = Executors.defaultThreadFactory().newThread(new ab(this, runnable) {
                    public final Runnable a;
                    public final vy$a b;
                    
                    @Override
                    public void a() {
                        this.a.run();
                    }
                });
                final StringBuilder sb = new StringBuilder();
                sb.append(this.a);
                sb.append(this.b.getAndIncrement());
                thread.setName(sb.toString());
                return thread;
            }
        };
    }
    
    public static ExecutorService e(final ThreadFactory threadFactory, final RejectedExecutionHandler handler) {
        return Executors.unconfigurableExecutorService(new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), threadFactory, handler));
    }
}

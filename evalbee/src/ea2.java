import com.google.protobuf.a0;
import com.google.protobuf.ByteString;
import com.google.firestore.v1.Write;
import com.google.protobuf.j0;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ea2 extends GeneratedMessageLite implements xv0
{
    public static final int BASE_WRITES_FIELD_NUMBER = 4;
    public static final int BATCH_ID_FIELD_NUMBER = 1;
    private static final ea2 DEFAULT_INSTANCE;
    public static final int LOCAL_WRITE_TIME_FIELD_NUMBER = 3;
    private static volatile b31 PARSER;
    public static final int WRITES_FIELD_NUMBER = 2;
    private t.e baseWrites_;
    private int batchId_;
    private j0 localWriteTime_;
    private t.e writes_;
    
    static {
        GeneratedMessageLite.V(ea2.class, DEFAULT_INSTANCE = new ea2());
    }
    
    public ea2() {
        this.writes_ = GeneratedMessageLite.A();
        this.baseWrites_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ ea2 Z() {
        return ea2.DEFAULT_INSTANCE;
    }
    
    public static b o0() {
        return (b)ea2.DEFAULT_INSTANCE.u();
    }
    
    public static ea2 p0(final ByteString byteString) {
        return (ea2)GeneratedMessageLite.P(ea2.DEFAULT_INSTANCE, byteString);
    }
    
    public static ea2 q0(final byte[] array) {
        return (ea2)GeneratedMessageLite.R(ea2.DEFAULT_INSTANCE, array);
    }
    
    public final void e0(final Write write) {
        write.getClass();
        this.g0();
        this.baseWrites_.add(write);
    }
    
    public final void f0(final Write write) {
        write.getClass();
        this.h0();
        this.writes_.add(write);
    }
    
    public final void g0() {
        final t.e baseWrites_ = this.baseWrites_;
        if (!baseWrites_.h()) {
            this.baseWrites_ = GeneratedMessageLite.L(baseWrites_);
        }
    }
    
    public final void h0() {
        final t.e writes_ = this.writes_;
        if (!writes_.h()) {
            this.writes_ = GeneratedMessageLite.L(writes_);
        }
    }
    
    public Write i0(final int n) {
        return this.baseWrites_.get(n);
    }
    
    public int j0() {
        return this.baseWrites_.size();
    }
    
    public int k0() {
        return this.batchId_;
    }
    
    public j0 l0() {
        j0 j0;
        if ((j0 = this.localWriteTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public Write m0(final int n) {
        return this.writes_.get(n);
    }
    
    public int n0() {
        return this.writes_.size();
    }
    
    public final void r0(final int batchId_) {
        this.batchId_ = batchId_;
    }
    
    public final void s0(final j0 localWriteTime_) {
        localWriteTime_.getClass();
        this.localWriteTime_ = localWriteTime_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (ea2$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = ea2.PARSER) == null) {
                    synchronized (ea2.class) {
                        if (ea2.PARSER == null) {
                            ea2.PARSER = new GeneratedMessageLite.b(ea2.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return ea2.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(ea2.DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0002\u0000\u0001\u0004\u0002\u001b\u0003\t\u0004\u001b", new Object[] { "batchId_", "writes_", Write.class, "localWriteTime_", "baseWrites_", Write.class });
            }
            case 2: {
                return new b((ea2$a)null);
            }
            case 1: {
                return new ea2();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(ea2.Z());
        }
        
        public b A(final Write write) {
            ((a)this).s();
            ((ea2)super.b).e0(write);
            return this;
        }
        
        public b B(final Write write) {
            ((a)this).s();
            ((ea2)super.b).f0(write);
            return this;
        }
        
        public b C(final int n) {
            ((a)this).s();
            ((ea2)super.b).r0(n);
            return this;
        }
        
        public b D(final j0 j0) {
            ((a)this).s();
            ((ea2)super.b).s0(j0);
            return this;
        }
    }
}

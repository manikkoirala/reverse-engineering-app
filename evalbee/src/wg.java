import android.widget.AbsListView;
import android.app.Dialog;
import com.ekodroid.omrevaluator.students.services.SyncClassListService;
import java.util.List;
import java.util.Collection;
import java.util.HashSet;
import java.sql.SQLException;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.DatabaseHelper;
import android.widget.ListAdapter;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.students.ClassDetailActivity;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.view.Menu;
import android.util.SparseBooleanArray;
import com.ekodroid.omrevaluator.database.ClassDataModel;
import android.view.MenuItem;
import android.widget.AbsListView$MultiChoiceModeListener;
import com.ekodroid.omrevaluator.students.ImportStudentsActivity;
import android.content.IntentFilter;
import android.content.Intent;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import java.util.Iterator;
import android.app.ProgressDialog;
import com.ekodroid.omrevaluator.students.clients.models.ClassDto;
import android.content.BroadcastReceiver;
import android.view.ActionMode;
import java.util.ArrayList;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.widget.ListView;
import android.widget.Button;
import android.view.View;
import android.content.Context;
import androidx.fragment.app.Fragment;

// 
// Decompiled by Procyon v0.6.0
// 

public class wg extends Fragment
{
    public Context a;
    public View b;
    public Button c;
    public Button d;
    public ListView e;
    public SwipeRefreshLayout f;
    public g3 g;
    public ArrayList h;
    public boolean i;
    public ActionMode j;
    public BroadcastReceiver k;
    
    public wg() {
        this.i = false;
    }
    
    public final void n(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(new ClassDto((String)iterator.next()));
        }
        final ProgressDialog progressDialog = new ProgressDialog(this.a);
        progressDialog.setMessage((CharSequence)this.getString(2131886481));
        ((Dialog)progressDialog).show();
        new fs(this.a, list2, new zg(this, progressDialog, list) {
            public final ProgressDialog a;
            public final ArrayList b;
            public final wg c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    this.c.o(this.b);
                }
                else {
                    final wg c = this.c;
                    a91.H(c.a, c.getString(2131886477), 2131230909, 2131231086);
                }
            }
        });
    }
    
    public final void o(final ArrayList list) {
        for (final String s : list) {
            ClassRepository.getInstance(this.a).deleteClass(s);
            ClassRepository.getInstance(this.a).deleteAllStudentForClass(s);
        }
        a91.G(this.a, 2131886170, 2131230927, 2131231085);
        this.v();
    }
    
    @Override
    public void onAttach(final Context a) {
        super.onAttach(a);
        this.a = a;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.b = layoutInflater.inflate(2131493005, viewGroup, false);
        this.t();
        this.s();
        this.r();
        this.k = new BroadcastReceiver(this) {
            public final wg a;
            
            public void onReceive(final Context context, final Intent intent) {
                this.a.v();
            }
        };
        return this.b;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        final ActionMode j = this.j;
        if (j != null) {
            j.finish();
            this.j = null;
        }
        this.a.unregisterReceiver(this.k);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        sl.registerReceiver(this.a, this.k, new IntentFilter("UPDATE_CLASS_LIST"), 4);
        this.i = a91.c(this.a);
        this.v();
    }
    
    public final void p() {
        final Context a = this.a;
        new go(a, ClassRepository.getInstance(a).getAllStudents(), new y01(this) {
            public final wg a;
            
            @Override
            public void a(final Object o) {
            }
        });
    }
    
    public final void q() {
        this.startActivity(new Intent(this.a, (Class)ImportStudentsActivity.class));
    }
    
    public final void r() {
        ((AbsListView)this.e).setChoiceMode(3);
        ((AbsListView)this.e).setMultiChoiceModeListener((AbsListView$MultiChoiceModeListener)new AbsListView$MultiChoiceModeListener(this) {
            public final wg a;
            
            public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
                final SparseBooleanArray a = this.a.g.a();
                final ArrayList list = new ArrayList();
                if (menuItem.getItemId() == 2131296708) {
                    for (int i = a.size() - 1; i >= 0; --i) {
                        if (a.valueAt(i)) {
                            list.add(((ClassDataModel)this.a.h.get(a.keyAt(i))).getClassName());
                        }
                    }
                    xs.c(this.a.a, new y01(this, list, actionMode) {
                        public final ArrayList a;
                        public final ActionMode b;
                        public final wg$j c;
                        
                        @Override
                        public void a(final Object o) {
                            if (a91.c(this.c.a.a)) {
                                this.c.a.n(this.a);
                            }
                            else {
                                this.c.a.o(this.a);
                            }
                            this.b.finish();
                        }
                    }, 2131886235, 2131886480, 2131886231, 2131886163, 0, 2131230945);
                    return true;
                }
                return false;
            }
            
            public boolean onCreateActionMode(final ActionMode j, final Menu menu) {
                this.a.getActivity().getMenuInflater().inflate(2131623937, menu);
                this.a.j = j;
                return true;
            }
            
            public void onDestroyActionMode(final ActionMode actionMode) {
                this.a.g.b();
                actionMode.finish();
                this.a.j = null;
            }
            
            public void onItemCheckedStateChanged(final ActionMode actionMode, final int n, final long n2, final boolean b) {
                final int checkedItemCount = ((AbsListView)this.a.e).getCheckedItemCount();
                final StringBuilder sb = new StringBuilder();
                sb.append(checkedItemCount);
                sb.append(" ");
                sb.append(this.a.getString(2131886791));
                actionMode.setTitle((CharSequence)sb.toString());
                this.a.g.d(n);
                this.a.g.notifyDataSetChanged();
            }
            
            public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
                return false;
            }
        });
    }
    
    public final void s() {
        this.f.setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.f.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final wg a;
            
            @Override
            public void a() {
                this.a.f.setRefreshing(true);
                this.a.v();
                final wg a = this.a;
                if (a.i) {
                    a.y(false);
                }
                this.a.f.setRefreshing(false);
            }
        });
        ((AdapterView)this.e).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final wg a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int index, final long n) {
                view.setBackgroundResource(2131230860);
                final Intent intent = new Intent(this.a.a, (Class)ClassDetailActivity.class);
                intent.putExtra("CLASS_NAME", this.a.h.get(index).getClassName());
                this.a.startActivity(intent);
            }
        });
    }
    
    public final void t() {
        this.f = (SwipeRefreshLayout)this.b.findViewById(2131297136);
        this.e = (ListView)this.b.findViewById(2131296838);
        this.c = (Button)this.b.findViewById(2131296410);
        this.d = (Button)this.b.findViewById(2131296412);
        ((View)this.c).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final wg a;
            
            public void onClick(final View view) {
                this.a.u();
            }
        });
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final wg a;
            
            public void onClick(final View view) {
                this.a.u();
            }
        });
        this.b.findViewById(2131296667).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final wg a;
            
            public void onClick(final View view) {
                xs.a(this.a.a, 2131886355, 2131886510, 2130903042, (xs.i)new xs.i(this) {
                    public final wg$e a;
                    
                    @Override
                    public void a(final int n, final String s) {
                        if (n != 0) {
                            if (n == 1) {
                                this.a.a.p();
                            }
                        }
                        else {
                            this.a.a.q();
                        }
                    }
                });
            }
        });
    }
    
    public final void u() {
        new t3(this.a, new y01(this) {
            public final wg a;
            
            @Override
            public void a(final Object o) {
                this.a.v();
            }
        });
    }
    
    public final void v() {
        final ArrayList<ClassDataModel> allClasses = ClassRepository.getInstance(this.a).getAllClasses();
        this.h = allClasses;
        if (allClasses.size() == 0) {
            this.w();
        }
        if (this.h.size() > 0) {
            this.b.findViewById(2131296746).setVisibility(8);
            ((View)this.e).setVisibility(0);
            final g3 g3 = new g3(this.a, this.h);
            this.g = g3;
            this.e.setAdapter((ListAdapter)g3);
        }
        else {
            this.b.findViewById(2131296746).setVisibility(0);
            ((View)this.e).setVisibility(8);
        }
    }
    
    public final void w() {
        final ArrayList c = new ArrayList();
        try {
            final List query = DatabaseHelper.getInstance(this.a).getTemplateJsonDAO().queryBuilder().distinct().selectColumns(new String[] { "className" }).query();
            if (query != null) {
                final Iterator iterator = query.iterator();
                while (iterator.hasNext()) {
                    c.add(((TemplateDataJsonModel)iterator.next()).getClassName());
                }
            }
            final List query2 = DatabaseHelper.getInstance(this.a).getStudentDAO().queryBuilder().distinct().selectColumns(new String[] { "className" }).query();
            if (query != null) {
                final Iterator iterator2 = query2.iterator();
                while (iterator2.hasNext()) {
                    c.add(((StudentDataModel)iterator2.next()).getClassName());
                }
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        final HashSet c2 = new HashSet(c);
        final ArrayList list = new ArrayList();
        list.addAll(c2);
        if (list.size() > 0) {
            this.x(list);
        }
    }
    
    public final void x(final ArrayList list) {
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            ClassRepository.getInstance(this.a).saveOrUpdateClass(new ClassDataModel((String)iterator.next(), null, false));
        }
    }
    
    public final void y(final boolean b) {
        if (this.i) {
            SyncClassListService.c(this.a, b);
        }
    }
}

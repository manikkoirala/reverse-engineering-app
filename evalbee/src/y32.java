import java.util.Iterator;
import java.io.IOException;
import java.io.Closeable;
import java.util.LinkedHashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class y32
{
    public final Map a;
    public final Set b;
    public volatile boolean c;
    
    public y32() {
        this.a = new HashMap();
        this.b = new LinkedHashSet();
        this.c = false;
    }
    
    public static void b(final Object o) {
        if (o instanceof Closeable) {
            try {
                ((Closeable)o).close();
            }
            catch (final IOException cause) {
                throw new RuntimeException(cause);
            }
        }
    }
    
    public final void a() {
        this.c = true;
        final Map a = this.a;
        if (a != null) {
            synchronized (a) {
                final Iterator iterator = this.a.values().iterator();
                while (iterator.hasNext()) {
                    b(iterator.next());
                }
            }
        }
        final Set b = this.b;
        if (b != null) {
            synchronized (b) {
                final Iterator iterator2 = this.b.iterator();
                while (iterator2.hasNext()) {
                    b(iterator2.next());
                }
            }
        }
        this.d();
    }
    
    public Object c(final String s) {
        final Map a = this.a;
        if (a == null) {
            return null;
        }
        synchronized (a) {
            return this.a.get(s);
        }
    }
    
    public void d() {
    }
    
    public Object e(final String s, final Object o) {
        synchronized (this.a) {
            final Object value = this.a.get(s);
            if (value == null) {
                this.a.put(s, o);
            }
            monitorexit(this.a);
            Object o2;
            if (value == null) {
                o2 = o;
            }
            else {
                o2 = value;
            }
            if (this.c) {
                b(o2);
            }
            return o2;
        }
    }
}

import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import android.graphics.Typeface;
import android.graphics.Color;
import android.graphics.Paint$Align;
import android.graphics.Paint$Style;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.Paint;
import android.graphics.Canvas;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class wd1
{
    public ArrayList a;
    public int b;
    public int c;
    public int d;
    public int[] e;
    public int f;
    public int g;
    public int h;
    public Canvas i;
    public Paint j;
    public Paint k;
    public Paint l;
    public Paint m;
    public Paint n;
    public String o;
    public String p;
    public String q;
    public String r;
    
    public wd1() {
        this.c = 1;
        this.d = 0;
        this.e = new int[2];
        this.f = 1200;
        this.g = 1800;
        this.h = 100;
        this.a = new ArrayList();
        final int f = this.f;
        final int c = this.c;
        final Bitmap bitmap = Bitmap.createBitmap(f * c, this.g * c, Bitmap$Config.ARGB_8888);
        (this.i = new Canvas(bitmap)).drawARGB(255, 255, 255, 255);
        this.a.add(bitmap);
        (this.j = new Paint()).setStyle(Paint$Style.FILL_AND_STROKE);
        this.j.setAntiAlias(true);
        this.j.setColor(-16777216);
        this.j.setStrokeWidth(1.0f);
        this.j.setTextAlign(Paint$Align.CENTER);
        final int[] e = this.e;
        e[1] = (e[0] = this.c * 100);
        (this.k = new Paint()).setStyle(Paint$Style.FILL);
        this.k.setAntiAlias(true);
        this.k.setColor(Color.rgb(200, 255, 200));
        (this.l = new Paint()).setStyle(Paint$Style.FILL);
        this.l.setAntiAlias(true);
        this.l.setColor(Color.rgb(255, 200, 200));
        (this.m = new Paint()).setStyle(Paint$Style.FILL);
        this.m.setAntiAlias(true);
        this.m.setColor(Color.rgb(255, 255, 200));
        (this.n = new Paint()).setStyle(Paint$Style.FILL);
        this.n.setAntiAlias(true);
        this.n.setColor(Color.rgb(200, 200, 255));
    }
    
    public void a(final String s, final String s2) {
        final int n = this.c * 40;
        this.j.setTextSize((float)n);
        final int n2 = this.f / 2 * this.c;
        this.j.setTypeface(Typeface.DEFAULT_BOLD);
        this.j.setTextAlign(Paint$Align.CENTER);
        final Canvas i = this.i;
        final float n3 = (float)n2;
        final int n4 = this.e[0];
        final int n5 = n / 2;
        i.drawText(" - ", n3, (float)(n4 + n5 - 2), this.j);
        this.j.setTextAlign(Paint$Align.RIGHT);
        this.i.drawText(s, (float)(n2 - 30), (float)(this.e[0] + n5 - 2), this.j);
        this.j.setTextAlign(Paint$Align.LEFT);
        this.i.drawText(s2, (float)(n2 + 30), (float)(this.e[0] + n5 - 2), this.j);
        final int[] e = this.e;
        final int n6 = e[0];
        final int c = this.c;
        e[0] = n6 + c * 80;
        e[1] += c * 80;
    }
    
    public void b(final String s) {
        final int n = this.c * 20;
        this.j.setTextSize((float)n);
        this.j.setTypeface(Typeface.DEFAULT);
        this.j.setTextAlign(Paint$Align.LEFT);
        this.i.drawText(s, (float)this.h, (float)(this.e[0] + n / 2 - 2), this.j);
        final int[] e = this.e;
        final int n2 = e[0];
        final int c = this.c;
        e[0] = n2 + c * 30;
        e[1] += c * 30;
    }
    
    public void c(final String s, final String s2, final String s3, final String s4, final String s5, final boolean b) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        final int n = this.c * 20;
        this.j.setTextSize((float)n);
        final int n2 = this.c * 200;
        this.j.setTextAlign(Paint$Align.CENTER);
        final Paint j = this.j;
        Typeface typeface;
        if (b) {
            typeface = Typeface.DEFAULT_BOLD;
        }
        else {
            typeface = Typeface.DEFAULT;
        }
        j.setTypeface(typeface);
        final Canvas i = this.i;
        final int h = this.h;
        final int n3 = n2 / 2;
        final float n4 = (float)(h + n3);
        final int n5 = this.e[0];
        final int n6 = n / 2;
        i.drawText(s, n4, (float)(n5 + n6 - 2), this.j);
        this.i(this.i, this.h + n3, this.e[0], n2, this.c * 30, paint);
        final Canvas k = this.i;
        final int h2 = this.h;
        final int n7 = n2 * 3 / 2;
        k.drawText(s2, (float)(h2 + n7), (float)(this.e[0] + n6 - 2), this.j);
        this.i(this.i, n7 + this.h, this.e[0], n2, this.c * 30, paint);
        final Canvas l = this.i;
        final int h3 = this.h;
        final int n8 = n2 * 5 / 2;
        l.drawText(s3, (float)(h3 + n8), (float)(this.e[0] + n6 - 2), this.j);
        this.i(this.i, n8 + this.h, this.e[0], n2, this.c * 30, paint);
        final Canvas m = this.i;
        final int h4 = this.h;
        final int n9 = n2 * 7 / 2;
        m.drawText(s4, (float)(h4 + n9), (float)(this.e[0] + n6 - 2), this.j);
        this.i(this.i, n9 + this.h, this.e[0], n2, this.c * 30, paint);
        final Canvas i2 = this.i;
        final int h5 = this.h;
        final int n10 = n2 * 9 / 2;
        i2.drawText(s5, (float)(h5 + n10), (float)(this.e[0] + n6 - 2), this.j);
        this.i(this.i, n10 + this.h, this.e[0], n2, this.c * 30, paint);
        final int[] e = this.e;
        final int n11 = e[0];
        final int c = this.c;
        e[0] = n11 + c * 30;
        e[1] += c * 30;
    }
    
    public final void d() {
        final int f = this.f;
        final int c = this.c;
        this.a.add(Bitmap.createBitmap(f * c, this.g * c, Bitmap$Config.ARGB_8888));
        ++this.b;
        (this.i = new Canvas((Bitmap)this.a.get(this.b))).drawARGB(255, 255, 255, 255);
        final int[] e = this.e;
        e[1] = (e[0] = this.c * 100);
        this.d = 0;
        this.h = 100;
    }
    
    public void e(final String s, final String s2, final String s3, final String s4, final boolean b, final AnswerValue.MarkedState markedState) {
        final int g = this.g;
        final int c = this.c;
        final int[] e = this.e;
        final int d = this.d;
        if (g * c - e[d] < 50) {
            if (d < 1) {
                this.d = d + 1;
                this.h += 550;
            }
            else {
                this.d();
            }
            this.e(this.o, this.p, this.q, this.r, false, AnswerValue.MarkedState.UNATTEMPTED);
        }
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        final int n = this.c * 20;
        this.j.setTextSize((float)n);
        final int n2 = this.c * 115;
        this.j.setTextAlign(Paint$Align.CENTER);
        final Paint j = this.j;
        Typeface typeface;
        if (b) {
            typeface = Typeface.DEFAULT_BOLD;
        }
        else {
            typeface = Typeface.DEFAULT;
        }
        j.setTypeface(typeface);
        final Canvas i = this.i;
        final int h = this.h;
        final int n3 = n2 / 2;
        final float n4 = (float)(h + n3);
        final int n5 = this.e[this.d];
        final int n6 = n / 2;
        i.drawText(s, n4, (float)(n5 + n6 - 2), this.j);
        this.i(this.i, this.h + n3, this.e[this.d], n2, this.c * 30, paint);
        if (markedState == AnswerValue.MarkedState.CORRECT) {
            this.i(this.i, n2 * 3 / 2 + this.h, this.e[this.d], n2, this.c * 30, this.k);
        }
        if (markedState == AnswerValue.MarkedState.INCORRECT) {
            this.i(this.i, n2 * 3 / 2 + this.h, this.e[this.d], n2, this.c * 30, this.l);
        }
        if (markedState == AnswerValue.MarkedState.PARTIAL_CORRECT) {
            this.i(this.i, n2 * 3 / 2 + this.h, this.e[this.d], n2, this.c * 30, this.m);
        }
        if (markedState == AnswerValue.MarkedState.INVALID) {
            this.i(this.i, n2 * 3 / 2 + this.h, this.e[this.d], n2, this.c * 30, this.m);
        }
        final Canvas k = this.i;
        final int h2 = this.h;
        final int n7 = n2 * 3 / 2;
        k.drawText(s2, (float)(h2 + n7), (float)(this.e[this.d] + n6 - 2), this.j);
        this.i(this.i, n7 + this.h, this.e[this.d], n2, this.c * 30, paint);
        final Canvas l = this.i;
        final int h3 = this.h;
        final int n8 = n2 * 5 / 2;
        l.drawText(s3, (float)(h3 + n8), (float)(this.e[this.d] + n6 - 2), this.j);
        this.i(this.i, n8 + this.h, this.e[this.d], n2, this.c * 30, paint);
        final Canvas m = this.i;
        final int h4 = this.h;
        final int n9 = n2 * 7 / 2;
        m.drawText(s4, (float)(h4 + n9), (float)(this.e[this.d] + n6 - 2), this.j);
        this.i(this.i, n9 + this.h, this.e[this.d], n2, this.c * 30, paint);
        final int[] e2 = this.e;
        final int d2 = this.d;
        e2[d2] += this.c * 30;
    }
    
    public void f(final String o, final String p5, final String q, final String r, final boolean b) {
        this.e(this.o = o, this.p = p5, this.q = q, this.r = r, b, AnswerValue.MarkedState.UNATTEMPTED);
    }
    
    public void g(final String s, final boolean b) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        final int n = this.c * 20;
        this.j.setTextSize((float)n);
        final int n2 = this.c * 115;
        this.j.setTextAlign(Paint$Align.CENTER);
        this.j.setTypeface(Typeface.DEFAULT);
        final Canvas i = this.i;
        final int h = this.h;
        final int n3 = n2 * 2;
        i.drawText(s, (float)(h + n3), (float)(this.e[this.d] + n / 2 - 2), this.j);
        this.i(this.i, this.h + n3, this.e[this.d], n2 * 4, this.c * 30, paint);
        final int[] e = this.e;
        final int d = this.d;
        e[d] += this.c * 30;
    }
    
    public void h(final int n) {
        final int[] e = this.e;
        final int n2 = e[0];
        final int c = this.c;
        e[0] = n2 + n * c;
        e[1] += n * c;
    }
    
    public final void i(final Canvas canvas, final int n, final int n2, int n3, int n4, final Paint paint) {
        n3 /= 2;
        final float n5 = (float)(n - n3);
        n4 /= 2;
        canvas.drawRect(n5, (float)(n2 - n4), (float)(n + n3), (float)(n2 + n4), paint);
    }
    
    public ArrayList j() {
        return this.a;
    }
}

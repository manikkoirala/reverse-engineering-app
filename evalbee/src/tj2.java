import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONException;
import com.google.android.gms.internal.firebase_auth_api.zzxw;
import android.util.Log;
import org.json.JSONObject;
import android.text.TextUtils;
import com.google.android.gms.internal.firebase-auth-api.zzafs;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase-auth-api.zzafc;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public final class tj2 extends AbstractSafeParcelable implements c22
{
    public static final Parcelable$Creator<tj2> CREATOR;
    public String a;
    public String b;
    public String c;
    public String d;
    public Uri e;
    public String f;
    public String g;
    public boolean h;
    public String i;
    
    static {
        CREATOR = (Parcelable$Creator)new kk2();
    }
    
    public tj2(final zzafc zzafc, final String b) {
        Preconditions.checkNotNull(zzafc);
        Preconditions.checkNotEmpty(b);
        this.a = Preconditions.checkNotEmpty(((com.google.android.gms.internal.firebase_auth_api.zzafc)zzafc).zzi());
        this.b = b;
        this.f = ((com.google.android.gms.internal.firebase_auth_api.zzafc)zzafc).zzh();
        this.c = ((com.google.android.gms.internal.firebase_auth_api.zzafc)zzafc).zzg();
        final Uri zzc = ((com.google.android.gms.internal.firebase_auth_api.zzafc)zzafc).zzc();
        if (zzc != null) {
            this.d = zzc.toString();
            this.e = zzc;
        }
        this.h = ((com.google.android.gms.internal.firebase_auth_api.zzafc)zzafc).zzm();
        this.i = null;
        this.g = ((com.google.android.gms.internal.firebase_auth_api.zzafc)zzafc).zzj();
    }
    
    public tj2(final zzafs zzafs) {
        Preconditions.checkNotNull(zzafs);
        this.a = ((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zzd();
        this.b = Preconditions.checkNotEmpty(((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zzf());
        this.c = ((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zzb();
        final Uri zza = ((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zza();
        if (zza != null) {
            this.d = zza.toString();
            this.e = zza;
        }
        this.f = ((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zzc();
        this.g = ((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zze();
        this.h = false;
        this.i = ((com.google.android.gms.internal.firebase_auth_api.zzafs)zzafs).zzg();
    }
    
    public tj2(final String a, final String b, final String f, final String g, final String c, final String d, final boolean h, final String i) {
        this.a = a;
        this.b = b;
        this.f = f;
        this.g = g;
        this.c = c;
        this.d = d;
        if (!TextUtils.isEmpty((CharSequence)d)) {
            this.e = Uri.parse(this.d);
        }
        this.h = h;
        this.i = i;
    }
    
    public static tj2 H(final String s) {
        try {
            final JSONObject jsonObject = new JSONObject(s);
            return new tj2(jsonObject.optString("userId"), jsonObject.optString("providerId"), jsonObject.optString("email"), jsonObject.optString("phoneNumber"), jsonObject.optString("displayName"), jsonObject.optString("photoUrl"), jsonObject.optBoolean("isEmailVerified"), jsonObject.optString("rawUserInfo"));
        }
        catch (final JSONException ex) {
            Log.d("DefaultAuthUserInfo", "Failed to unpack UserInfo from JSON");
            throw new zzxw((Throwable)ex);
        }
    }
    
    public final boolean E() {
        return this.h;
    }
    
    @Override
    public final String b() {
        return this.b;
    }
    
    public final String getDisplayName() {
        return this.c;
    }
    
    public final String getEmail() {
        return this.f;
    }
    
    public final String getPhoneNumber() {
        return this.g;
    }
    
    public final String i() {
        return this.a;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.i(), false);
        SafeParcelWriter.writeString(parcel, 2, this.b(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getDisplayName(), false);
        SafeParcelWriter.writeString(parcel, 4, this.d, false);
        SafeParcelWriter.writeString(parcel, 5, this.getEmail(), false);
        SafeParcelWriter.writeString(parcel, 6, this.getPhoneNumber(), false);
        SafeParcelWriter.writeBoolean(parcel, 7, this.E());
        SafeParcelWriter.writeString(parcel, 8, this.i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zza() {
        return this.i;
    }
    
    public final String zzb() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt("userId", (Object)this.a);
            jsonObject.putOpt("providerId", (Object)this.b);
            jsonObject.putOpt("displayName", (Object)this.c);
            jsonObject.putOpt("photoUrl", (Object)this.d);
            jsonObject.putOpt("email", (Object)this.f);
            jsonObject.putOpt("phoneNumber", (Object)this.g);
            jsonObject.putOpt("isEmailVerified", (Object)this.h);
            jsonObject.putOpt("rawUserInfo", (Object)this.i);
            return jsonObject.toString();
        }
        catch (final JSONException ex) {
            Log.d("DefaultAuthUserInfo", "Failed to jsonify this object");
            throw new zzxw((Throwable)ex);
        }
    }
}

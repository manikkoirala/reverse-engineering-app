import java.io.File;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ao
{
    public static ao a(final CrashlyticsReport crashlyticsReport, final String s, final File file) {
        return new ia(crashlyticsReport, s, file);
    }
    
    public abstract CrashlyticsReport b();
    
    public abstract File c();
    
    public abstract String d();
}

import androidx.lifecycle.o;

// 
// Decompiled by Procyon v0.6.0
// 

public final class cf0 implements b
{
    public final z32[] b;
    
    public cf0(final z32... b) {
        fg0.e((Object)b, "initializers");
        this.b = b;
    }
    
    @Override
    public y32 a(final Class clazz, final do do1) {
        fg0.e((Object)clazz, "modelClass");
        fg0.e((Object)do1, "extras");
        final z32[] b = this.b;
        final int length = b.length;
        int i = 0;
        y32 y32 = null;
        while (i < length) {
            final z32 z32 = b[i];
            if (fg0.a((Object)z32.a(), (Object)clazz)) {
                final Object invoke = z32.b().invoke((Object)do1);
                if (invoke instanceof y32) {
                    y32 = (y32)invoke;
                }
                else {
                    y32 = null;
                }
            }
            ++i;
        }
        if (y32 != null) {
            return y32;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No initializer set for given class ");
        sb.append(clazz.getName());
        throw new IllegalArgumentException(sb.toString());
    }
}

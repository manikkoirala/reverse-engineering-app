import java.sql.Timestamp;
import java.sql.Date;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class fp1
{
    public static final boolean a;
    public static final lq b;
    public static final lq c;
    public static final iz1 d;
    public static final iz1 e;
    public static final iz1 f;
    
    static {
        boolean a2;
        try {
            Class.forName("java.sql.Date");
            a2 = true;
        }
        catch (final ClassNotFoundException ex) {
            a2 = false;
        }
        a = a2;
        iz1 b2;
        if (a2) {
            b = new lq(Date.class) {};
            c = new lq(Timestamp.class) {};
            d = cp1.b;
            e = dp1.b;
            b2 = ep1.b;
        }
        else {
            b2 = null;
            b = null;
            c = null;
            d = null;
            e = null;
        }
        f = b2;
    }
}

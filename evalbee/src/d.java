// 
// Decompiled by Procyon v0.6.0
// 

public class d implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        return Math.abs(array[0]);
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "abs(x)";
    }
}

import java.io.FilterInputStream;
import javax.net.ssl.HttpsURLConnection;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.net.URL;
import java.util.HashMap;
import com.android.volley.Request;
import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.InputStream;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;

// 
// Decompiled by Procyon v0.6.0
// 

public class pd0 extends gb
{
    public final SSLSocketFactory a;
    
    public pd0() {
        this(null);
    }
    
    public pd0(final b b) {
        this(b, null);
    }
    
    public pd0(final b b, final SSLSocketFactory a) {
        this.a = a;
    }
    
    public static List e(final Map map) {
        final ArrayList list = new ArrayList(map.size());
        for (final Map.Entry<K, List> entry : map.entrySet()) {
            if (entry.getKey() != null) {
                final Iterator iterator2 = entry.getValue().iterator();
                while (iterator2.hasNext()) {
                    list.add(new sc0((String)entry.getKey(), (String)iterator2.next()));
                }
            }
        }
        return list;
    }
    
    public static boolean i(final int n, final int n2) {
        return n != 4 && (100 > n2 || n2 >= 200) && n2 != 204 && n2 != 304;
    }
    
    public static InputStream j(HttpURLConnection httpURLConnection) {
        try {
            httpURLConnection = (HttpURLConnection)httpURLConnection.getInputStream();
        }
        catch (final IOException ex) {
            httpURLConnection = (HttpURLConnection)httpURLConnection.getErrorStream();
        }
        return (InputStream)httpURLConnection;
    }
    
    @Override
    public ld0 a(final Request request, Map k) {
        final String z = request.z();
        final HashMap hashMap = new HashMap();
        hashMap.putAll((Map)k);
        hashMap.putAll(request.o());
        k = this.k(new URL(z), request);
        int n2;
        final int n = n2 = 0;
        try {
            final Iterator iterator = hashMap.keySet().iterator();
            while (true) {
                n2 = n;
                if (!iterator.hasNext()) {
                    break;
                }
                n2 = n;
                final String s = (String)iterator.next();
                n2 = n;
                k.setRequestProperty(s, (String)hashMap.get(s));
            }
            n2 = n;
            this.l(k, request);
            n2 = n;
            final int responseCode = k.getResponseCode();
            if (responseCode == -1) {
                n2 = n;
                n2 = n;
                final IOException ex = new IOException("Could not retrieve response code from HttpUrlConnection.");
                n2 = n;
                throw ex;
            }
            n2 = n;
            if (!i(request.p(), responseCode)) {
                n2 = n;
                final ld0 ld0 = new ld0(responseCode, e(k.getHeaderFields()));
                k.disconnect();
                return ld0;
            }
            n2 = 1;
            return new ld0(responseCode, e(k.getHeaderFields()), k.getContentLength(), this.g(request, k));
        }
        finally {
            if (n2 == 0) {
                k.disconnect();
            }
        }
    }
    
    public final void c(final HttpURLConnection httpURLConnection, final Request request, final byte[] b) {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey("Content-Type")) {
            httpURLConnection.setRequestProperty("Content-Type", request.l());
        }
        final DataOutputStream dataOutputStream = new DataOutputStream(this.h(request, httpURLConnection, b.length));
        dataOutputStream.write(b);
        dataOutputStream.close();
    }
    
    public final void d(final HttpURLConnection httpURLConnection, final Request request) {
        final byte[] k = request.k();
        if (k != null) {
            this.c(httpURLConnection, request, k);
        }
    }
    
    public HttpURLConnection f(final URL url) {
        final HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }
    
    public InputStream g(final Request request, final HttpURLConnection httpURLConnection) {
        return new a(httpURLConnection);
    }
    
    public OutputStream h(final Request request, final HttpURLConnection httpURLConnection, final int n) {
        return httpURLConnection.getOutputStream();
    }
    
    public final HttpURLConnection k(final URL url, final Request request) {
        final HttpURLConnection f = this.f(url);
        final int x = request.x();
        f.setConnectTimeout(x);
        f.setReadTimeout(x);
        f.setUseCaches(false);
        f.setDoInput(true);
        if ("https".equals(url.getProtocol())) {
            final SSLSocketFactory a = this.a;
            if (a != null) {
                ((HttpsURLConnection)f).setSSLSocketFactory(a);
            }
        }
        return f;
    }
    
    public void l(final HttpURLConnection httpURLConnection, final Request request) {
        String requestMethod2 = null;
        Label_0134: {
            Label_0121: {
                String requestMethod = null;
                switch (request.p()) {
                    default: {
                        throw new IllegalStateException("Unknown method type.");
                    }
                    case 7: {
                        requestMethod = "PATCH";
                        break;
                    }
                    case 6: {
                        requestMethod2 = "TRACE";
                        break Label_0134;
                    }
                    case 5: {
                        requestMethod2 = "OPTIONS";
                        break Label_0134;
                    }
                    case 4: {
                        requestMethod2 = "HEAD";
                        break Label_0134;
                    }
                    case 3: {
                        requestMethod2 = "DELETE";
                        break Label_0134;
                    }
                    case 2: {
                        requestMethod = "PUT";
                        break;
                    }
                    case 1: {
                        httpURLConnection.setRequestMethod("POST");
                        break Label_0121;
                    }
                    case 0: {
                        requestMethod2 = "GET";
                        break Label_0134;
                    }
                    case -1: {
                        final byte[] s = request.s();
                        if (s != null) {
                            httpURLConnection.setRequestMethod("POST");
                            this.c(httpURLConnection, request, s);
                        }
                        return;
                    }
                }
                httpURLConnection.setRequestMethod(requestMethod);
            }
            this.d(httpURLConnection, request);
            return;
        }
        httpURLConnection.setRequestMethod(requestMethod2);
    }
    
    public static class a extends FilterInputStream
    {
        public final HttpURLConnection a;
        
        public a(final HttpURLConnection a) {
            super(j(a));
            this.a = a;
        }
        
        @Override
        public void close() {
            super.close();
            this.a.disconnect();
        }
    }
    
    public interface b
    {
    }
}

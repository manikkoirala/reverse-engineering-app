import com.google.android.gms.common.internal.Objects;

// 
// Decompiled by Procyon v0.6.0
// 

public class cg0
{
    public String a;
    
    public cg0(final String a) {
        this.a = a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof cg0 && Objects.equal(this.a, ((cg0)o).a);
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.a);
    }
    
    @Override
    public String toString() {
        return Objects.toStringHelper(this).add("token", this.a).toString();
    }
}

import android.widget.AdapterView;
import android.widget.ListView;
import android.view.View$OnClickListener;
import android.widget.TextView;
import android.widget.ImageButton;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class j3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    
    public j3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final String text = this.b.get(index);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493023, (ViewGroup)null);
        final ImageButton imageButton = (ImageButton)inflate.findViewById(2131296656);
        ((TextView)inflate.findViewById(2131297233)).setText((CharSequence)text);
        ((View)imageButton).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, viewGroup, index) {
            public final ViewGroup a;
            public final int b;
            public final j3 c;
            
            public void onClick(final View view) {
                ((AdapterView)this.a).performItemClick(view, this.b, (long)view.getId());
            }
        });
        return inflate;
    }
}

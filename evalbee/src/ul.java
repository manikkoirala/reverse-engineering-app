import android.content.res.AssetManager;
import android.os.Build$VERSION;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.content.res.Resources$Theme;
import android.content.res.Configuration;
import android.content.ContextWrapper;

// 
// Decompiled by Procyon v0.6.0
// 

public class ul extends ContextWrapper
{
    public static Configuration f;
    public int a;
    public Resources$Theme b;
    public LayoutInflater c;
    public Configuration d;
    public Resources e;
    
    public ul(final Context context, final int a) {
        super(context);
        this.a = a;
    }
    
    public ul(final Context context, final Resources$Theme b) {
        super(context);
        this.b = b;
    }
    
    public static boolean e(final Configuration configuration) {
        if (configuration == null) {
            return true;
        }
        if (ul.f == null) {
            final Configuration f = new Configuration();
            f.fontScale = 0.0f;
            ul.f = f;
        }
        return configuration.equals(ul.f);
    }
    
    public void a(final Configuration configuration) {
        if (this.e != null) {
            throw new IllegalStateException("getResources() or getAssets() has already been called");
        }
        if (this.d == null) {
            this.d = new Configuration(configuration);
            return;
        }
        throw new IllegalStateException("Override configuration has already been set");
    }
    
    public void attachBaseContext(final Context context) {
        super.attachBaseContext(context);
    }
    
    public final Resources b() {
        if (this.e == null) {
            final Configuration d = this.d;
            Resources e;
            if (d != null && (Build$VERSION.SDK_INT < 26 || !e(d))) {
                e = ul.a.a(this, this.d).getResources();
            }
            else {
                e = super.getResources();
            }
            this.e = e;
        }
        return this.e;
    }
    
    public int c() {
        return this.a;
    }
    
    public final void d() {
        final boolean b = this.b == null;
        if (b) {
            this.b = this.getResources().newTheme();
            final Resources$Theme theme = this.getBaseContext().getTheme();
            if (theme != null) {
                this.b.setTo(theme);
            }
        }
        this.f(this.b, this.a, b);
    }
    
    public void f(final Resources$Theme resources$Theme, final int n, final boolean b) {
        resources$Theme.applyStyle(n, true);
    }
    
    public AssetManager getAssets() {
        return this.getResources().getAssets();
    }
    
    public Resources getResources() {
        return this.b();
    }
    
    public Object getSystemService(final String anObject) {
        if ("layout_inflater".equals(anObject)) {
            if (this.c == null) {
                this.c = LayoutInflater.from(this.getBaseContext()).cloneInContext((Context)this);
            }
            return this.c;
        }
        return this.getBaseContext().getSystemService(anObject);
    }
    
    public Resources$Theme getTheme() {
        final Resources$Theme b = this.b;
        if (b != null) {
            return b;
        }
        if (this.a == 0) {
            this.a = tb1.f;
        }
        this.d();
        return this.b;
    }
    
    public void setTheme(final int a) {
        if (this.a != a) {
            this.a = a;
            this.d();
        }
    }
    
    public abstract static class a
    {
        public static Context a(final ul ul, final Configuration configuration) {
            return ((Context)ul).createConfigurationContext(configuration);
        }
    }
}

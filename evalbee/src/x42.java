import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class x42 extends g52
{
    public static boolean c = true;
    
    @Override
    public void a(final View view) {
    }
    
    @Override
    public float b(final View view) {
        if (x42.c) {
            try {
                return w42.a(view);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                x42.c = false;
            }
        }
        return view.getAlpha();
    }
    
    @Override
    public void c(final View view) {
    }
    
    @Override
    public void e(final View view, final float alpha) {
        if (x42.c) {
            try {
                v42.a(view, alpha);
                return;
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                x42.c = false;
            }
        }
        view.setAlpha(alpha);
    }
}

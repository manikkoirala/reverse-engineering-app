import android.os.BaseBundle;
import com.google.android.gms.internal.play_billing.zzaf;
import java.util.List;
import com.android.billingclient.api.b;
import android.content.Intent;
import com.google.android.gms.internal.play_billing.zzhy;
import com.google.android.gms.internal.play_billing.zzej;
import com.android.billingclient.api.a;
import android.os.Bundle;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.Handler;
import android.os.Build$VERSION;
import android.content.IntentFilter;
import android.content.Context;
import android.content.BroadcastReceiver;

// 
// Decompiled by Procyon v0.6.0
// 

public final class xf2 extends BroadcastReceiver
{
    public final ca1 a = null;
    public final dd2 b = b;
    public boolean c;
    public final zf2 d;
    
    public final void c(final Context context, final IntentFilter intentFilter, final String s, final IntentFilter intentFilter2) {
        synchronized (this) {
            if (!this.c) {
                if (Build$VERSION.SDK_INT >= 33) {
                    tf2.a(context, (BroadcastReceiver)zf2.b(this.d), intentFilter, (String)null, (Handler)null, 2);
                }
                else {
                    zf2.a(this.d).getApplicationContext().getPackageName();
                    context.registerReceiver((BroadcastReceiver)zf2.b(this.d), intentFilter);
                }
                this.c = true;
            }
        }
    }
    
    public final void d(final Context context) {
        synchronized (this) {
            if (this.c) {
                context.unregisterReceiver((BroadcastReceiver)zf2.b(this.d));
                this.c = false;
                return;
            }
            zzb.zzk("BillingBroadcastManager", "Receiver is not registered.");
        }
    }
    
    public final void e(final Bundle bundle, final a a, final int n) {
        if (bundle.getByteArray("FAILURE_LOGGING_PAYLOAD") != null) {
            try {
                this.b.a(zzhy.zzx(bundle.getByteArray("FAILURE_LOGGING_PAYLOAD"), zzej.zza()));
                return;
            }
            finally {
                zzb.zzk("BillingBroadcastManager", "Failed parsing Api failure.");
                return;
            }
        }
        this.b.a(bd2.a(23, n, a));
    }
    
    public final void onReceive(final Context context, final Intent intent) {
        final Bundle extras = intent.getExtras();
        final int n = 1;
        if (extras == null) {
            zzb.zzk("BillingBroadcastManager", "Bundle is null.");
            final dd2 b = this.b;
            final a j = com.android.billingclient.api.b.j;
            b.a(bd2.a(11, 1, j));
            final ca1 a = this.a;
            if (a != null) {
                a.a(j, null);
            }
        }
        else {
            final a zze = zzb.zze(intent, "BillingBroadcastManager");
            final String action = intent.getAction();
            final String string = ((BaseBundle)extras).getString("INTENT_SOURCE");
            int n2 = 0;
            Label_0107: {
                if (string != "LAUNCH_BILLING_FLOW") {
                    n2 = n;
                    if (string == null) {
                        break Label_0107;
                    }
                    n2 = n;
                    if (!string.equals("LAUNCH_BILLING_FLOW")) {
                        break Label_0107;
                    }
                }
                n2 = 2;
            }
            if (action.equals("com.android.vending.billing.PURCHASES_UPDATED") || action.equals("com.android.vending.billing.LOCAL_BROADCAST_PURCHASES_UPDATED")) {
                final List zzi = zzb.zzi(extras);
                if (zze.b() == 0) {
                    this.b.c(bd2.b(n2));
                }
                else {
                    this.e(extras, zze, n2);
                }
                this.a.a(zze, zzi);
                return;
            }
            if (action.equals("com.android.vending.billing.ALTERNATIVE_BILLING")) {
                if (zze.b() != 0) {
                    this.e(extras, zze, n2);
                    this.a.a(zze, (List)zzaf.zzk());
                    return;
                }
                zzb.zzk("BillingBroadcastManager", "AlternativeBillingListener and UserChoiceBillingListener is null.");
                final dd2 b2 = this.b;
                final a i = com.android.billingclient.api.b.j;
                b2.a(bd2.a(77, n2, i));
                this.a.a(i, (List)zzaf.zzk());
            }
        }
    }
}

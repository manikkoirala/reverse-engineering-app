import android.content.res.Resources$Theme;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.List;
import java.util.ArrayList;
import android.content.Context;
import android.widget.ArrayAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class h3 extends ArrayAdapter
{
    public Context a;
    public ArrayList b;
    
    public h3(final Context a, final ArrayList b) {
        super(a, 0, (List)b);
        this.b = b;
        this.a = a;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        final View inflate = LayoutInflater.from(this.getContext()).inflate(2131493016, viewGroup, false);
        final TextView textView = (TextView)inflate.findViewById(2131297286);
        textView.setText((CharSequence)this.b.get(n));
        textView.setPadding(a91.d(15, this.a), a91.d(15, this.a), a91.d(15, this.a), a91.d(15, this.a));
        if (this.b.get(n).equals(this.a.getString(2131886120))) {
            textView.setTextColor(this.a.getResources().getColor(2131099724, (Resources$Theme)null));
        }
        return inflate;
    }
}

import android.os.BaseBundle;
import java.util.Locale;
import android.app.ActivityManager;
import java.io.IOException;
import java.util.Collection;
import android.os.Bundle;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.Callable;
import android.util.Base64;
import java.util.SortedSet;
import java.util.Iterator;
import android.os.Build$VERSION;
import android.os.Build;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import android.os.StatFs;
import android.os.Environment;
import com.google.firebase.crashlytics.internal.common.DeliveryMechanism;
import com.google.android.gms.tasks.Task;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.content.Context;
import java.io.FilenameFilter;

// 
// Decompiled by Procyon v0.6.0
// 

public class zm
{
    public static final FilenameFilter t;
    public final Context a;
    public final dp b;
    public final cn c;
    public final f22 d;
    public final xm e;
    public final de0 f;
    public final z00 g;
    public final s7 h;
    public final ul0 i;
    public final dn j;
    public final m4 k;
    public final wm l;
    public final nm1 m;
    public co n;
    public zm1 o;
    public final TaskCompletionSource p;
    public final TaskCompletionSource q;
    public final TaskCompletionSource r;
    public final AtomicBoolean s;
    
    static {
        t = new ym();
    }
    
    public zm(final Context a, final xm e, final de0 f, final dp b, final z00 g, final cn c, final s7 h, final f22 d, final ul0 i, final nm1 m, final dn j, final m4 k, final wm l) {
        this.o = null;
        this.p = new TaskCompletionSource();
        this.q = new TaskCompletionSource();
        this.r = new TaskCompletionSource();
        this.s = new AtomicBoolean(false);
        this.a = a;
        this.e = e;
        this.f = f;
        this.b = b;
        this.g = g;
        this.c = c;
        this.h = h;
        this.d = d;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
    }
    
    public static boolean A() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        }
        catch (final ClassNotFoundException ex) {
            return false;
        }
    }
    
    public static long C() {
        return F(System.currentTimeMillis());
    }
    
    public static List D(final gy0 gy0, final String s, final z00 z00, final byte[] array) {
        final File o = z00.o(s, "user-data");
        final File o2 = z00.o(s, "keys");
        final File o3 = z00.o(s, "rollouts-state");
        final ArrayList list = new ArrayList();
        list.add(new td("logs_file", "logs", array));
        list.add(new w00("crash_meta_file", "metadata", gy0.g()));
        list.add(new w00("session_meta_file", "session", gy0.f()));
        list.add(new w00("app_meta_file", "app", gy0.d()));
        list.add(new w00("device_meta_file", "device", gy0.a()));
        list.add(new w00("os_meta_file", "os", gy0.e()));
        list.add(P(gy0));
        list.add(new w00("user_meta_file", "user", o));
        list.add(new w00("keys_file", "keys", o2));
        list.add(new w00("rollouts_file", "rollouts", o3));
        return list;
    }
    
    public static long F(final long n) {
        return n / 1000L;
    }
    
    public static boolean O(final String s, final File file, final CrashlyticsReport.a a) {
        if (file == null || !file.exists()) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("No minidump data found for session ");
            sb.append(s);
            f.k(sb.toString());
        }
        if (a == null) {
            final zl0 f2 = zl0.f();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("No Tombstones data found for session ");
            sb2.append(s);
            f2.g(sb2.toString());
        }
        return (file == null || !file.exists()) && a == null;
    }
    
    public static ey0 P(final gy0 gy0) {
        final File c = gy0.c();
        ey0 ey0;
        if (c != null && c.exists()) {
            ey0 = new w00("minidump_file", "minidump", c);
        }
        else {
            ey0 = new td("minidump_file", "minidump", new byte[] { 0 });
        }
        return ey0;
    }
    
    public static byte[] R(final InputStream inputStream) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[1024];
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(array, 0, read);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    public static /* synthetic */ ul0 e(final zm zm) {
        return zm.i;
    }
    
    public static /* synthetic */ m4 f(final zm zm) {
        return zm.k;
    }
    
    public static /* synthetic */ cn g(final zm zm) {
        return zm.c;
    }
    
    public static /* synthetic */ nm1 h(final zm zm) {
        return zm.m;
    }
    
    public static /* synthetic */ de0 j(final zm zm) {
        return zm.f;
    }
    
    public static /* synthetic */ dp l(final zm zm) {
        return zm.b;
    }
    
    public static /* synthetic */ xm m(final zm zm) {
        return zm.e;
    }
    
    public static zp1.a o(final de0 de0, final s7 s7) {
        return zp1.a.b(de0.f(), s7.f, s7.g, de0.a().c(), DeliveryMechanism.determineFrom(s7.d).getId(), s7.h);
    }
    
    public static zp1.b p(final Context context) {
        final StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return zp1.b.c(CommonUtils.k(), Build.MODEL, Runtime.getRuntime().availableProcessors(), CommonUtils.b(context), statFs.getBlockCount() * (long)statFs.getBlockSize(), CommonUtils.w(), CommonUtils.l(), Build.MANUFACTURER, Build.PRODUCT);
    }
    
    public static zp1.c q() {
        return zp1.c.a(Build$VERSION.RELEASE, Build$VERSION.CODENAME, CommonUtils.x());
    }
    
    public static void r(final List list) {
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            ((File)iterator.next()).delete();
        }
    }
    
    public final String B() {
        final SortedSet p = this.m.p();
        String s;
        if (!p.isEmpty()) {
            s = p.first();
        }
        else {
            s = null;
        }
        return s;
    }
    
    public final InputStream E(final String name) {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        if (classLoader == null) {
            zl0.f().k("Couldn't get Class Loader");
            return null;
        }
        final InputStream resourceAsStream = classLoader.getResourceAsStream(name);
        if (resourceAsStream == null) {
            zl0.f().g("No version control information found");
            return null;
        }
        return resourceAsStream;
    }
    
    public String G() {
        final InputStream e = this.E("META-INF/version-control-info.textproto");
        if (e == null) {
            return null;
        }
        zl0.f().b("Read version control info");
        return Base64.encodeToString(R(e), 0);
    }
    
    public void H(final zm1 zm1, final Thread thread, final Throwable t) {
        this.I(zm1, thread, t, false);
    }
    
    public void I(final zm1 zm1, final Thread thread, final Throwable obj, final boolean b) {
        synchronized (this) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Handling uncaught exception \"");
            sb.append(obj);
            sb.append("\" from thread ");
            sb.append(thread.getName());
            f.b(sb.toString());
            final Task h = this.e.h(new Callable(this, System.currentTimeMillis(), obj, thread, zm1, b) {
                public final long a;
                public final Throwable b;
                public final Thread c;
                public final zm1 d;
                public final boolean e;
                public final zm f;
                
                public Task a() {
                    final long b = F(this.a);
                    final String c = this.f.B();
                    if (c == null) {
                        zl0.f().d("Tried to write a fatal exception while no session was open.");
                        return Tasks.forResult((Object)null);
                    }
                    zm.g(this.f).a();
                    zm.h(this.f).t(this.b, this.c, c, b);
                    this.f.w(this.a);
                    this.f.t(this.d);
                    this.f.v(new vd(zm.j(this.f)).toString(), this.e);
                    if (!zm.l(this.f).d()) {
                        return Tasks.forResult((Object)null);
                    }
                    final Executor c2 = zm.m(this.f).c();
                    return this.d.b().onSuccessTask(c2, (SuccessContinuation)new SuccessContinuation(this, c2, c) {
                        public final Executor a;
                        public final String b;
                        public final zm$b c;
                        
                        public Task a(final vm1 vm1) {
                            final String s = null;
                            if (vm1 == null) {
                                zl0.f().k("Received null app settings, cannot send reports at crash time.");
                                return Tasks.forResult((Object)null);
                            }
                            final Task n = this.c.f.N();
                            final nm1 h = zm.h(this.c.f);
                            final Executor a = this.a;
                            String b = s;
                            if (this.c.e) {
                                b = this.b;
                            }
                            return Tasks.whenAll(new Task[] { n, h.x(a, b) });
                        }
                    });
                }
            });
            try {
                v22.f(h);
            }
            catch (final Exception ex) {
                zl0.f().e("Error handling uncaught exception", ex);
            }
            catch (final TimeoutException ex2) {
                zl0.f().d("Cannot send reports. Timed out while fetching settings.");
            }
        }
    }
    
    public boolean J() {
        final co n = this.n;
        return n != null && n.a();
    }
    
    public List L() {
        return this.g.f(zm.t);
    }
    
    public final Task M(final long n) {
        if (A()) {
            zl0.f().k("Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
            return Tasks.forResult((Object)null);
        }
        zl0.f().b("Logging app exception event to Firebase Analytics");
        return Tasks.call((Executor)new ScheduledThreadPoolExecutor(1), (Callable)new Callable(this, n) {
            public final long a;
            public final zm b;
            
            public Void a() {
                final Bundle bundle = new Bundle();
                ((BaseBundle)bundle).putInt("fatal", 1);
                ((BaseBundle)bundle).putLong("timestamp", this.a);
                zm.f(this.b).a("_ae", bundle);
                return null;
            }
        });
    }
    
    public final Task N() {
        final ArrayList list = new ArrayList();
        for (final File file : this.L()) {
            try {
                list.add(this.M(Long.parseLong(file.getName().substring(3))));
            }
            catch (final NumberFormatException ex) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not parse app exception timestamp from file ");
                sb.append(file.getName());
                f.k(sb.toString());
            }
            file.delete();
        }
        return Tasks.whenAll((Collection)list);
    }
    
    public void Q(final String s) {
        this.e.g(new Callable(this, s) {
            public final String a;
            public final zm b;
            
            public Void a() {
                this.b.v(this.a, Boolean.FALSE);
                return null;
            }
        });
    }
    
    public void S() {
        try {
            final String g = this.G();
            if (g != null) {
                this.T("com.crashlytics.version-control-info", g);
                zl0.f().g("Saved version control info");
            }
        }
        catch (final IOException ex) {
            zl0.f().l("Unable to save version control info", ex);
        }
    }
    
    public void T(final String s, final String s2) {
        try {
            this.d.j(s, s2);
        }
        catch (final IllegalArgumentException ex) {
            final Context a = this.a;
            if (a != null && CommonUtils.u(a)) {
                throw ex;
            }
            zl0.f().d("Attempting to set custom attribute with null key, ignoring.");
        }
    }
    
    public Task U(final Task task) {
        if (!this.m.n()) {
            zl0.f().i("No crash reports are available to be sent.");
            this.p.trySetResult((Object)Boolean.FALSE);
            return Tasks.forResult((Object)null);
        }
        zl0.f().i("Crash reports are available to be sent.");
        return this.V().onSuccessTask((SuccessContinuation)new SuccessContinuation(this, task) {
            public final Task a;
            public final zm b;
            
            public Task a(final Boolean b) {
                return zm.m(this.b).h(new Callable(this, b) {
                    public final Boolean a;
                    public final zm$d b;
                    
                    public Task a() {
                        if (!this.a) {
                            zl0.f().i("Deleting cached crash reports...");
                            r(this.b.b.L());
                            zm.h(this.b.b).v();
                            this.b.b.r.trySetResult((Object)null);
                            return Tasks.forResult((Object)null);
                        }
                        zl0.f().b("Sending cached crash reports...");
                        zm.l(this.b.b).c(this.a);
                        final Executor c = zm.m(this.b.b).c();
                        return this.b.a.onSuccessTask(c, (SuccessContinuation)new SuccessContinuation(this, c) {
                            public final Executor a;
                            public final zm$d$a b;
                            
                            public Task a(final vm1 vm1) {
                                if (vm1 == null) {
                                    zl0.f().k("Received null app settings at app startup. Cannot send cached reports");
                                }
                                else {
                                    this.b.b.b.N();
                                    zm.h(this.b.b.b).w(this.a);
                                    this.b.b.b.r.trySetResult((Object)null);
                                }
                                return Tasks.forResult((Object)null);
                            }
                        });
                    }
                });
            }
        });
    }
    
    public final Task V() {
        if (this.b.d()) {
            zl0.f().b("Automatic data collection is enabled. Allowing upload.");
            this.p.trySetResult((Object)Boolean.FALSE);
            return Tasks.forResult((Object)Boolean.TRUE);
        }
        zl0.f().b("Automatic data collection is disabled.");
        zl0.f().i("Notifying that unsent reports are available.");
        this.p.trySetResult((Object)Boolean.TRUE);
        final Task onSuccessTask = this.b.h().onSuccessTask((SuccessContinuation)new SuccessContinuation(this) {
            public final zm a;
            
            public Task a(final Void void1) {
                return Tasks.forResult((Object)Boolean.TRUE);
            }
        });
        zl0.f().b("Waiting for send/deleteUnsentReports to be called.");
        return v22.n(onSuccessTask, this.q.getTask());
    }
    
    public final void W(final String str) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            final List a = l70.a((ActivityManager)this.a.getSystemService("activity"), (String)null, 0, 0);
            if (a.size() != 0) {
                this.m.u(str, a, new ul0(this.g, str), f22.h(str, this.g, this.e));
            }
            else {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("No ApplicationExitInfo available. Session: ");
                sb.append(str);
                f.i(sb.toString());
            }
        }
        else {
            final zl0 f2 = zl0.f();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("ANR feature enabled, but device is API ");
            sb2.append(sdk_INT);
            f2.i(sb2.toString());
        }
    }
    
    public void X(final long n, final String s) {
        this.e.g(new Callable(this, n, s) {
            public final long a;
            public final String b;
            public final zm c;
            
            public Void a() {
                if (!this.c.J()) {
                    zm.e(this.c).g(this.a, this.b);
                }
                return null;
            }
        });
    }
    
    public boolean s() {
        final boolean c = this.c.c();
        boolean b = true;
        if (!c) {
            final String b2 = this.B();
            if (b2 == null || !this.j.d(b2)) {
                b = false;
            }
            return b;
        }
        zl0.f().i("Found previous crash marker.");
        this.c.d();
        return true;
    }
    
    public void t(final zm1 zm1) {
        this.u(false, zm1);
    }
    
    public final void u(final boolean b, final zm1 zm1) {
        final ArrayList list = new ArrayList(this.m.p());
        if (list.size() <= (b ? 1 : 0)) {
            zl0.f().i("No open sessions to be closed.");
            return;
        }
        final String s = (String)list.get(b ? 1 : 0);
        if (zm1.a().b.b) {
            this.W(s);
        }
        else {
            zl0.f().i("ANR feature disabled.");
        }
        if (this.j.d(s)) {
            this.y(s);
        }
        String s2;
        if ((b ? 1 : 0) != 0) {
            s2 = (String)list.get(0);
        }
        else {
            this.l.e(null);
            s2 = null;
        }
        this.m.k(C(), s2);
    }
    
    public final void v(final String str, final Boolean b) {
        final long c = C();
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Opening a new session with ID ");
        sb.append(str);
        f.b(sb.toString());
        this.j.a(str, String.format(Locale.US, "Crashlytics Android SDK/%s", bn.i()), c, zp1.b(o(this.f, this.h), q(), p(this.a)));
        if (b && str != null) {
            this.d.k(str);
        }
        this.i.e(str);
        this.l.e(str);
        this.m.q(str, c);
    }
    
    public final void w(final long lng) {
        try {
            final z00 g = this.g;
            final StringBuilder sb = new StringBuilder();
            sb.append(".ae");
            sb.append(lng);
            if (!g.e(sb.toString()).createNewFile()) {
                throw new IOException("Create new file failed.");
            }
        }
        catch (final IOException ex) {
            zl0.f().l("Could not create app exception marker file.", ex);
        }
    }
    
    public void x(final String s, final Thread.UncaughtExceptionHandler uncaughtExceptionHandler, final zm1 o) {
        this.o = o;
        this.Q(s);
        Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)(this.n = new co((co.a)new co.a(this) {
            public final zm a;
            
            @Override
            public void a(final zm1 zm1, final Thread thread, final Throwable t) {
                this.a.H(zm1, thread, t);
            }
        }, o, uncaughtExceptionHandler, this.j)));
    }
    
    public final void y(final String str) {
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Finalizing native report for session ");
        sb.append(str);
        f.i(sb.toString());
        final gy0 b = this.j.b(str);
        final File c = b.c();
        final CrashlyticsReport.a b2 = b.b();
        if (O(str, c, b2)) {
            zl0.f().k("No native core present");
            return;
        }
        final long lastModified = c.lastModified();
        final ul0 ul0 = new ul0(this.g, str);
        final File i = this.g.i(str);
        if (!i.isDirectory()) {
            zl0.f().k("Couldn't create directory to store native session files, aborting.");
            return;
        }
        this.w(lastModified);
        final List d = D(b, str, this.g, ul0.b());
        fy0.b(i, d);
        zl0.f().b("CrashlyticsController#finalizePreviousNativeSession");
        this.m.j(str, d, b2);
        ul0.a();
    }
    
    public boolean z(final zm1 zm1) {
        this.e.b();
        if (this.J()) {
            zl0.f().k("Skipping session finalization because a crash has already occurred.");
            return false;
        }
        zl0.f().i("Finalizing previously open sessions.");
        try {
            this.u(true, zm1);
            zl0.f().i("Closed all previously open sessions.");
            return true;
        }
        catch (final Exception ex) {
            zl0.f().e("Unable to finalize previously open sessions.", ex);
            return false;
        }
    }
}

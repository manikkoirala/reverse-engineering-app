// 
// Decompiled by Procyon v0.6.0
// 

public final class ga implements jk
{
    public static final jk a;
    
    static {
        a = new ga();
    }
    
    @Override
    public void configure(final zw zw) {
        zw.a(xl1.class, e.a);
        zw.a(am1.class, f.a);
        zw.a(gp.class, c.a);
        zw.a(y7.class, b.a);
        zw.a(r4.class, ga.a.a);
        zw.a(f81.class, d.a);
    }
    
    public static final class a implements w01
    {
        public static final a a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        
        static {
            a = new a();
            b = n00.d("packageName");
            c = n00.d("versionName");
            d = n00.d("appBuildVersion");
            e = n00.d("deviceManufacturer");
            f = n00.d("currentProcessDetails");
            g = n00.d("appProcessDetails");
        }
        
        public void a(final r4 r4, final x01 x01) {
            x01.f(ga.a.b, r4.e());
            x01.f(ga.a.c, r4.f());
            x01.f(ga.a.d, r4.a());
            x01.f(ga.a.e, r4.d());
            x01.f(ga.a.f, r4.c());
            x01.f(ga.a.g, r4.b());
        }
    }
    
    public static final class b implements w01
    {
        public static final b a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        
        static {
            a = new b();
            b = n00.d("appId");
            c = n00.d("deviceModel");
            d = n00.d("sessionSdkVersion");
            e = n00.d("osVersion");
            f = n00.d("logEnvironment");
            g = n00.d("androidAppInfo");
        }
        
        public void a(final y7 y7, final x01 x01) {
            x01.f(ga.b.b, y7.b());
            x01.f(ga.b.c, y7.c());
            x01.f(ga.b.d, y7.f());
            x01.f(ga.b.e, y7.e());
            x01.f(ga.b.f, y7.d());
            x01.f(ga.b.g, y7.a());
        }
    }
    
    public static final class c implements w01
    {
        public static final c a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        
        static {
            a = new c();
            b = n00.d("performance");
            c = n00.d("crashlytics");
            d = n00.d("sessionSamplingRate");
        }
        
        public void a(final gp gp, final x01 x01) {
            x01.f(ga.c.b, gp.b());
            x01.f(ga.c.c, gp.a());
            x01.d(ga.c.d, gp.c());
        }
    }
    
    public static final class d implements w01
    {
        public static final d a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        
        static {
            a = new d();
            b = n00.d("processName");
            c = n00.d("pid");
            d = n00.d("importance");
            e = n00.d("defaultProcess");
        }
        
        public void a(final f81 f81, final x01 x01) {
            x01.f(ga.d.b, f81.c());
            x01.c(ga.d.c, f81.b());
            x01.c(ga.d.d, f81.a());
            x01.b(ga.d.e, f81.d());
        }
    }
    
    public static final class e implements w01
    {
        public static final e a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        
        static {
            a = new e();
            b = n00.d("eventType");
            c = n00.d("sessionData");
            d = n00.d("applicationInfo");
        }
        
        public void a(final xl1 xl1, final x01 x01) {
            x01.f(e.b, xl1.b());
            x01.f(e.c, xl1.c());
            x01.f(e.d, xl1.a());
        }
    }
    
    public static final class f implements w01
    {
        public static final f a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        
        static {
            a = new f();
            b = n00.d("sessionId");
            c = n00.d("firstSessionId");
            d = n00.d("sessionIndex");
            e = n00.d("eventTimestampUs");
            f = n00.d("dataCollectionStatus");
            g = n00.d("firebaseInstallationId");
        }
        
        public void a(final am1 am1, final x01 x01) {
            x01.f(ga.f.b, am1.e());
            x01.f(ga.f.c, am1.d());
            x01.c(ga.f.d, am1.f());
            x01.e(ga.f.e, am1.b());
            x01.f(ga.f.f, am1.a());
            x01.f(ga.f.g, am1.c());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class z70 extends a80 implements rx0
{
    @Override
    public boolean containsEntry(final Object o, final Object o2) {
        return this.delegate().containsEntry(o, o2);
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.delegate().containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.delegate().containsValue(o);
    }
    
    @Override
    public abstract rx0 delegate();
    
    @Override
    public boolean equals(final Object o) {
        return o == this || this.delegate().equals(o);
    }
    
    @Override
    public int hashCode() {
        return this.delegate().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.delegate().isEmpty();
    }
    
    @Override
    public int size() {
        return this.delegate().size();
    }
}

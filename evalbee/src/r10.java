import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.app.Application;
import com.google.android.gms.common.util.PlatformVersion;
import java.util.concurrent.atomic.AtomicReference;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.util.Base64Utils;
import java.nio.charset.Charset;
import com.google.android.gms.common.api.internal.BackgroundDetector;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.common.util.ProcessUtils;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import com.google.firebase.heartbeatinfo.a;
import com.google.firebase.concurrent.ExecutorsRegistrar;
import com.google.firebase.components.ComponentRegistrar;
import com.google.firebase.FirebaseCommonRegistrar;
import java.util.Collection;
import java.util.concurrent.Executor;
import com.google.firebase.concurrent.UiExecutor;
import com.google.firebase.components.ComponentDiscoveryService;
import com.google.firebase.provider.FirebaseInitProvider;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.Context;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class r10
{
    public static final Object k;
    public static final Map l;
    public final Context a;
    public final String b;
    public final e30 c;
    public final wj d;
    public final AtomicBoolean e;
    public final AtomicBoolean f;
    public final yi0 g;
    public final r91 h;
    public final List i;
    public final List j;
    
    static {
        k = new Object();
        l = new r8();
    }
    
    public r10(final Context context, final String s, final e30 e30) {
        this.e = new AtomicBoolean(false);
        this.f = new AtomicBoolean();
        this.i = new CopyOnWriteArrayList();
        this.j = new CopyOnWriteArrayList();
        this.a = Preconditions.checkNotNull(context);
        this.b = Preconditions.checkNotEmpty(s);
        this.c = Preconditions.checkNotNull(e30);
        final tp1 b = FirebaseInitProvider.b();
        q30.b("Firebase");
        q30.b("ComponentDiscovery");
        final List b2 = lj.c(context, ComponentDiscoveryService.class).b();
        q30.a();
        q30.b("Runtime");
        final wj.b g = wj.m(UiExecutor.INSTANCE).d(b2).c(new FirebaseCommonRegistrar()).c(new ExecutorsRegistrar()).b(zi.s(context, Context.class, new Class[0])).b(zi.s(this, r10.class, new Class[0])).b(zi.s(e30, e30.class, new Class[0])).g(new pj());
        if (d22.a(context) && FirebaseInitProvider.c()) {
            g.b(zi.s(b, tp1.class, new Class[0]));
        }
        final wj e31 = g.e();
        this.d = e31;
        q30.a();
        this.g = new yi0(new p10(this, context));
        this.h = e31.e(com.google.firebase.heartbeatinfo.a.class);
        this.g((a)new q10(this));
        q30.a();
    }
    
    public static /* synthetic */ Object c() {
        return r10.k;
    }
    
    public static /* synthetic */ AtomicBoolean e(final r10 r10) {
        return r10.e;
    }
    
    public static List k() {
        final ArrayList list = new ArrayList();
        synchronized (r10.k) {
            final Iterator iterator = r10.l.values().iterator();
            while (iterator.hasNext()) {
                list.add(((r10)iterator.next()).o());
            }
            monitorexit(r10.k);
            Collections.sort((List<Comparable>)list);
            return list;
        }
    }
    
    public static r10 m() {
        synchronized (r10.k) {
            final r10 r10 = r10.l.get("[DEFAULT]");
            if (r10 != null) {
                ((com.google.firebase.heartbeatinfo.a)r10.h.get()).l();
                return r10;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Default FirebaseApp is not initialized in this process ");
            sb.append(ProcessUtils.getMyProcessName());
            sb.append(". Make sure to call FirebaseApp.initializeApp(Context) first.");
            throw new IllegalStateException(sb.toString());
        }
    }
    
    public static r10 n(String format) {
        synchronized (r10.k) {
            final r10 r10 = r10.l.get(z(format));
            if (r10 != null) {
                ((com.google.firebase.heartbeatinfo.a)r10.h.get()).l();
                return r10;
            }
            final List k = k();
            String string;
            if (k.isEmpty()) {
                string = "";
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Available app names: ");
                sb.append(TextUtils.join((CharSequence)", ", (Iterable)k));
                string = sb.toString();
            }
            format = String.format("FirebaseApp with name %s doesn't exist. %s", format, string);
            throw new IllegalStateException(format);
        }
    }
    
    public static r10 s(final Context context) {
        synchronized (r10.k) {
            if (r10.l.containsKey("[DEFAULT]")) {
                return m();
            }
            final e30 a = e30.a(context);
            if (a == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            return t(context, a);
        }
    }
    
    public static r10 t(final Context context, final e30 e30) {
        return u(context, e30, "[DEFAULT]");
    }
    
    public static r10 u(Context applicationContext, final e30 e30, String z) {
        b(applicationContext);
        z = z(z);
        if (applicationContext.getApplicationContext() != null) {
            applicationContext = applicationContext.getApplicationContext();
        }
        synchronized (r10.k) {
            final Map l = r10.l;
            final boolean b = !l.containsKey(z);
            final StringBuilder sb = new StringBuilder();
            sb.append("FirebaseApp name ");
            sb.append(z);
            sb.append(" already exists!");
            Preconditions.checkState(b, (Object)sb.toString());
            Preconditions.checkNotNull(applicationContext, "Application context cannot be null.");
            final r10 r10 = new r10(applicationContext, z, e30);
            l.put(z, r10);
            monitorexit(r10.k);
            r10.r();
            return r10;
        }
    }
    
    public static String z(final String s) {
        return s.trim();
    }
    
    public final void A(final boolean b) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        final Iterator iterator = this.i.iterator();
        while (iterator.hasNext()) {
            ((a)iterator.next()).onBackgroundStateChanged(b);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof r10 && this.b.equals(((r10)o).o());
    }
    
    public void g(final a a) {
        this.i();
        if (this.e.get() && BackgroundDetector.getInstance().isInBackground()) {
            a.onBackgroundStateChanged(true);
        }
        this.i.add(a);
    }
    
    public void h(final y10 y10) {
        this.i();
        Preconditions.checkNotNull(y10);
        this.j.add(y10);
    }
    
    @Override
    public int hashCode() {
        return this.b.hashCode();
    }
    
    public final void i() {
        Preconditions.checkState(this.f.get() ^ true, (Object)"FirebaseApp was deleted");
    }
    
    public Object j(final Class clazz) {
        this.i();
        return this.d.a(clazz);
    }
    
    public Context l() {
        this.i();
        return this.a;
    }
    
    public String o() {
        this.i();
        return this.b;
    }
    
    public e30 p() {
        this.i();
        return this.c;
    }
    
    public String q() {
        final StringBuilder sb = new StringBuilder();
        sb.append(Base64Utils.encodeUrlSafeNoPadding(this.o().getBytes(Charset.defaultCharset())));
        sb.append("+");
        sb.append(Base64Utils.encodeUrlSafeNoPadding(this.p().c().getBytes(Charset.defaultCharset())));
        return sb.toString();
    }
    
    public final void r() {
        if (d22.a(this.a) ^ true) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Device in Direct Boot Mode: postponing initialization of Firebase APIs for app ");
            sb.append(this.o());
            Log.i("FirebaseApp", sb.toString());
            b(this.a);
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Device unlocked: initializing all Firebase APIs for app ");
            sb2.append(this.o());
            Log.i("FirebaseApp", sb2.toString());
            this.d.p(this.w());
            ((com.google.firebase.heartbeatinfo.a)this.h.get()).l();
        }
    }
    
    @Override
    public String toString() {
        return Objects.toStringHelper(this).add("name", this.b).add("options", this.c).toString();
    }
    
    public boolean v() {
        this.i();
        return ((ep)this.g.get()).b();
    }
    
    public boolean w() {
        return "[DEFAULT]".equals(this.o());
    }
    
    public interface a
    {
        void onBackgroundStateChanged(final boolean p0);
    }
    
    public static class b implements BackgroundStateChangeListener
    {
        public static AtomicReference a;
        
        static {
            b.a = new AtomicReference();
        }
        
        public static void b(final Context context) {
            if (PlatformVersion.isAtLeastIceCreamSandwich()) {
                if (context.getApplicationContext() instanceof Application) {
                    final Application application = (Application)context.getApplicationContext();
                    if (b.a.get() == null) {
                        final b b = new b();
                        if (ja2.a(r10.b.a, null, b)) {
                            BackgroundDetector.initialize(application);
                            BackgroundDetector.getInstance().addListener((BackgroundDetector.BackgroundStateChangeListener)b);
                        }
                    }
                }
            }
        }
        
        @Override
        public void onBackgroundStateChanged(final boolean b) {
            synchronized (r10.c()) {
                for (final r10 r10 : new ArrayList(r10.l.values())) {
                    if (r10.e(r10).get()) {
                        r10.A(b);
                    }
                }
            }
        }
    }
    
    public static class c extends BroadcastReceiver
    {
        public static AtomicReference b;
        public final Context a;
        
        static {
            c.b = new AtomicReference();
        }
        
        public c(final Context a) {
            this.a = a;
        }
        
        public static void b(final Context context) {
            if (c.b.get() == null) {
                final c c = new c(context);
                if (ja2.a(r10.c.b, null, c)) {
                    context.registerReceiver((BroadcastReceiver)c, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }
        
        public void c() {
            this.a.unregisterReceiver((BroadcastReceiver)this);
        }
        
        public void onReceive(final Context context, final Intent intent) {
            synchronized (r10.c()) {
                final Iterator iterator = r10.l.values().iterator();
                while (iterator.hasNext()) {
                    ((r10)iterator.next()).r();
                }
                monitorexit(r10.c());
                this.c();
            }
        }
    }
}

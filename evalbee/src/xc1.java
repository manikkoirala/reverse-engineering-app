import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import com.google.firebase.database.collection.c;

// 
// Decompiled by Procyon v0.6.0
// 

public class xc1
{
    public c a;
    public c b;
    
    public xc1() {
        this.a = new c(Collections.emptyList(), ku.c);
        this.b = new c(Collections.emptyList(), ku.d);
    }
    
    public void a(final du du, final int n) {
        final ku ku = new ku(du, n);
        this.a = this.a.c(ku);
        this.b = this.b.c(ku);
    }
    
    public void b(final c c, final int n) {
        final Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            this.a((du)iterator.next(), n);
        }
    }
    
    public boolean c(final du du) {
        final Iterator g = this.a.g(new ku(du, 0));
        return g.hasNext() && ((ku)g.next()).d().equals(du);
    }
    
    public c d(final int n) {
        final Iterator g = this.b.g(new ku(du.d(), n));
        c c = du.e();
        while (g.hasNext()) {
            final ku ku = g.next();
            if (ku.c() != n) {
                break;
            }
            c = c.c(ku.d());
        }
        return c;
    }
    
    public void e(final du du, final int n) {
        this.f(new ku(du, n));
    }
    
    public final void f(final ku ku) {
        this.a = this.a.i(ku);
        this.b = this.b.i(ku);
    }
    
    public void g(final c c, final int n) {
        final Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            this.e((du)iterator.next(), n);
        }
    }
    
    public c h(final int n) {
        final Iterator g = this.b.g(new ku(du.d(), n));
        c c = du.e();
        while (g.hasNext()) {
            final ku ku = g.next();
            if (ku.c() != n) {
                break;
            }
            c = c.c(ku.d());
            this.f(ku);
        }
        return c;
    }
}

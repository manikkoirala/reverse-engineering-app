import com.google.protobuf.t;
import com.google.protobuf.g0;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public final class k91
{
    public static final k91 c;
    public final oj1 a;
    public final ConcurrentMap b;
    
    static {
        c = new k91();
    }
    
    public k91() {
        this.b = new ConcurrentHashMap();
        this.a = new um0();
    }
    
    public static k91 a() {
        return k91.c;
    }
    
    public g0 b(final Class clazz, final g0 g0) {
        t.b(clazz, "messageType");
        t.b(g0, "schema");
        return this.b.putIfAbsent(clazz, g0);
    }
    
    public g0 c(final Class clazz) {
        t.b(clazz, "messageType");
        g0 a;
        if ((a = (g0)this.b.get(clazz)) == null) {
            a = this.a.a(clazz);
            final g0 b = this.b(clazz, a);
            if (b != null) {
                a = b;
            }
        }
        return a;
    }
    
    public g0 d(final Object o) {
        return this.c(o.getClass());
    }
}

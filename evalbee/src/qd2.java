import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.datatransport.Event;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Encoding;
import com.google.android.gms.internal.play_billing.zziv;
import com.google.android.datatransport.runtime.Destination;
import com.google.android.datatransport.cct.CCTDestination;
import com.google.android.datatransport.runtime.TransportRuntime;
import android.content.Context;
import com.google.android.datatransport.Transport;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qd2
{
    public boolean a;
    public Transport b;
    
    public qd2(final Context context) {
        try {
            TransportRuntime.initialize(context);
            this.b = TransportRuntime.getInstance().newFactory(CCTDestination.INSTANCE).getTransport("PLAY_BILLING_LIBRARY", zziv.class, Encoding.of("proto"), od2.a);
        }
        finally {
            this.a = true;
        }
    }
    
    public final void a(final zziv zziv) {
        String s;
        if (this.a) {
            s = "Skipping logging since initialization failed.";
        }
        else {
            try {
                this.b.send(Event.ofData(zziv));
                return;
            }
            finally {
                s = "logging failed.";
            }
        }
        zzb.zzk("BillingLogger", s);
    }
}

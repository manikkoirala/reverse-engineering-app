import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.firebase-auth-api.zzafn;
import com.google.firebase.auth.FirebaseAuth;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ik2 implements zb2, wf2
{
    public final FirebaseAuth a;
    
    public ik2(final FirebaseAuth a) {
        this.a = a;
    }
    
    @Override
    public final void a(final zzafn zzafn, final r30 r30) {
        this.a.w(r30, zzafn, true, true);
    }
    
    @Override
    public final void zza(final Status status) {
        final int statusCode = status.getStatusCode();
        if (statusCode == 17011 || statusCode == 17021 || statusCode == 17005) {
            this.a.n();
        }
    }
}

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ob extends uc
{
    public ob(final Context context, final hu1 hu1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        super(context, hu1);
    }
    
    @Override
    public IntentFilter j() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.os.action.CHARGING");
        intentFilter.addAction("android.os.action.DISCHARGING");
        return intentFilter;
    }
    
    @Override
    public void k(final Intent intent) {
        fg0.e((Object)intent, "intent");
        final String action = intent.getAction();
        if (action == null) {
            return;
        }
        final xl0 e = xl0.e();
        final String a = pb.a();
        final StringBuilder sb = new StringBuilder();
        sb.append("Received ");
        sb.append(action);
        e.a(a, sb.toString());
        Boolean b = null;
        Label_0166: {
            Label_0162: {
                switch (action.hashCode()) {
                    default: {
                        return;
                    }
                    case 1019184907: {
                        if (!action.equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                            return;
                        }
                        break;
                    }
                    case 948344062: {
                        if (!action.equals("android.os.action.CHARGING")) {
                            return;
                        }
                        break;
                    }
                    case -54942926: {
                        if (!action.equals("android.os.action.DISCHARGING")) {
                            return;
                        }
                        break Label_0162;
                    }
                    case -1886648615: {
                        if (!action.equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                            return;
                        }
                        break Label_0162;
                    }
                }
                b = Boolean.TRUE;
                break Label_0166;
            }
            b = Boolean.FALSE;
        }
        this.g(b);
    }
    
    public final boolean l(final Intent intent) {
        final int intExtra = intent.getIntExtra("status", -1);
        return intExtra == 2 || intExtra == 5;
    }
    
    public Boolean m() {
        final Intent registerReceiver = this.d().registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            xl0.e().c(pb.a(), "getInitialState - null intent received");
            return Boolean.FALSE;
        }
        return this.l(registerReceiver);
    }
}

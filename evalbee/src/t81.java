import java.util.Collection;
import com.google.android.gms.internal.play_billing.zzaf;
import org.json.JSONArray;
import java.util.ArrayList;
import android.text.TextUtils;
import java.util.List;
import org.json.JSONObject;

// 
// Decompiled by Procyon v0.6.0
// 

public final class t81
{
    public final String a;
    public final JSONObject b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;
    public final String k;
    public final List l;
    public final List m;
    
    public t81(String optString) {
        this.a = optString;
        final JSONObject b = new JSONObject(optString);
        this.b = b;
        final String optString2 = b.optString("productId");
        this.c = optString2;
        optString = b.optString("type");
        this.d = optString;
        if (TextUtils.isEmpty((CharSequence)optString2)) {
            throw new IllegalArgumentException("Product id cannot be empty.");
        }
        if (TextUtils.isEmpty((CharSequence)optString)) {
            throw new IllegalArgumentException("Product type cannot be empty.");
        }
        this.e = b.optString("title");
        this.f = b.optString("name");
        this.g = b.optString("description");
        this.i = b.optString("packageDisplayName");
        this.j = b.optString("iconUrl");
        this.h = b.optString("skuDetailsToken");
        this.k = b.optString("serializedDocid");
        final JSONArray optJSONArray = b.optJSONArray("subscriptionOfferDetails");
        final int n = 0;
        if (optJSONArray != null) {
            final ArrayList l = new ArrayList();
            for (int i = 0; i < optJSONArray.length(); ++i) {
                l.add(new d(optJSONArray.getJSONObject(i)));
            }
            this.l = l;
        }
        else {
            List j;
            if (!optString.equals("subs") && !optString.equals("play_pass_subs")) {
                j = null;
            }
            else {
                j = new ArrayList();
            }
            this.l = j;
        }
        final JSONObject optJSONObject = this.b.optJSONObject("oneTimePurchaseOfferDetails");
        final JSONArray optJSONArray2 = this.b.optJSONArray("oneTimePurchaseOfferDetailsList");
        final ArrayList list = new ArrayList();
        if (optJSONArray2 != null) {
            for (int k = n; k < optJSONArray2.length(); ++k) {
                list.add(new a(optJSONArray2.getJSONObject(k)));
            }
            this.m = list;
            return;
        }
        if (optJSONObject != null) {
            list.add(new a(optJSONObject));
            this.m = list;
            return;
        }
        this.m = null;
    }
    
    public a a() {
        final List m = this.m;
        if (m != null && !m.isEmpty()) {
            return this.m.get(0);
        }
        return null;
    }
    
    public String b() {
        return this.c;
    }
    
    public String c() {
        return this.d;
    }
    
    public final String d() {
        return this.b.optString("packageName");
    }
    
    public final String e() {
        return this.h;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof t81 && TextUtils.equals((CharSequence)this.a, (CharSequence)((t81)o).a));
    }
    
    public String f() {
        return this.k;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        final List l = this.l;
        final String string = this.b.toString();
        final String value = String.valueOf(l);
        final StringBuilder sb = new StringBuilder();
        sb.append("ProductDetails{jsonString='");
        sb.append(this.a);
        sb.append("', parsedJson=");
        sb.append(string);
        sb.append(", productId='");
        sb.append(this.c);
        sb.append("', productType='");
        sb.append(this.d);
        sb.append("', title='");
        sb.append(this.e);
        sb.append("', productDetailsToken='");
        sb.append(this.h);
        sb.append("', subscriptionOfferDetails=");
        sb.append(value);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class a
    {
        public final String a;
        public final long b;
        public final String c;
        public final String d;
        public final String e;
        public final zzaf f;
        public final Long g;
        public final fe2 h;
        public final oe2 i;
        public final ke2 j;
        public final me2 k;
        
        public a(JSONObject optJSONObject) {
            this.a = optJSONObject.optString("formattedPrice");
            this.b = optJSONObject.optLong("priceAmountMicros");
            this.c = optJSONObject.optString("priceCurrencyCode");
            this.d = optJSONObject.optString("offerIdToken");
            this.e = optJSONObject.optString("offerId");
            optJSONObject.optInt("offerType");
            final JSONArray optJSONArray = optJSONObject.optJSONArray("offerTags");
            final ArrayList list = new ArrayList();
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    list.add(optJSONArray.getString(i));
                }
            }
            this.f = zzaf.zzj((Collection)list);
            final boolean has = optJSONObject.has("fullPriceMicros");
            final me2 me2 = null;
            Long value;
            if (has) {
                value = optJSONObject.optLong("fullPriceMicros");
            }
            else {
                value = null;
            }
            this.g = value;
            final JSONObject optJSONObject2 = optJSONObject.optJSONObject("discountDisplayInfo");
            fe2 h;
            if (optJSONObject2 == null) {
                h = null;
            }
            else {
                h = new fe2(optJSONObject2);
            }
            this.h = h;
            final JSONObject optJSONObject3 = optJSONObject.optJSONObject("validTimeWindow");
            oe2 j;
            if (optJSONObject3 == null) {
                j = null;
            }
            else {
                j = new oe2(optJSONObject3);
            }
            this.i = j;
            final JSONObject optJSONObject4 = optJSONObject.optJSONObject("limitedQuantityInfo");
            ke2 k;
            if (optJSONObject4 == null) {
                k = null;
            }
            else {
                k = new ke2(optJSONObject4);
            }
            this.j = k;
            optJSONObject = optJSONObject.optJSONObject("preorderDetails");
            me2 l;
            if (optJSONObject == null) {
                l = me2;
            }
            else {
                l = new me2(optJSONObject);
            }
            this.k = l;
        }
        
        public String a() {
            return this.a;
        }
        
        public final String b() {
            return this.d;
        }
    }
    
    public static final class b
    {
        public final String a;
        public final long b;
        public final String c;
        public final String d;
        public final int e;
        public final int f;
        
        public b(final JSONObject jsonObject) {
            this.d = jsonObject.optString("billingPeriod");
            this.c = jsonObject.optString("priceCurrencyCode");
            this.a = jsonObject.optString("formattedPrice");
            this.b = jsonObject.optLong("priceAmountMicros");
            this.f = jsonObject.optInt("recurrenceMode");
            this.e = jsonObject.optInt("billingCycleCount");
        }
    }
    
    public static class c
    {
        public final List a;
        
        public c(final JSONArray jsonArray) {
            final ArrayList a = new ArrayList();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); ++i) {
                    final JSONObject optJSONObject = jsonArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        a.add(new b(optJSONObject));
                    }
                }
            }
            this.a = a;
        }
    }
    
    public static final class d
    {
        public final String a;
        public final String b;
        public final String c;
        public final c d;
        public final List e;
        public final de2 f;
        
        public d(final JSONObject jsonObject) {
            this.a = jsonObject.optString("basePlanId");
            String optString = jsonObject.optString("offerId");
            final boolean empty = optString.isEmpty();
            final de2 de2 = null;
            if (empty) {
                optString = null;
            }
            this.b = optString;
            this.c = jsonObject.getString("offerIdToken");
            this.d = new c(jsonObject.getJSONArray("pricingPhases"));
            final JSONObject optJSONObject = jsonObject.optJSONObject("installmentPlanDetails");
            de2 f;
            if (optJSONObject == null) {
                f = de2;
            }
            else {
                f = new de2(optJSONObject);
            }
            this.f = f;
            final ArrayList e = new ArrayList();
            final JSONArray optJSONArray = jsonObject.optJSONArray("offerTags");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    e.add(optJSONArray.getString(i));
                }
            }
            this.e = e;
        }
    }
}

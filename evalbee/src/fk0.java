import android.view.ViewGroup;
import android.widget.AdapterView;
import android.view.View;
import android.widget.ListView;

// 
// Decompiled by Procyon v0.6.0
// 

public class fk0 extends fa
{
    public final ListView w;
    
    public fk0(final ListView w) {
        super((View)w);
        this.w = w;
    }
    
    @Override
    public boolean a(final int n) {
        return false;
    }
    
    @Override
    public boolean b(final int n) {
        final ListView w = this.w;
        final int count = ((AdapterView)w).getCount();
        if (count == 0) {
            return false;
        }
        final int childCount = ((ViewGroup)w).getChildCount();
        final int firstVisiblePosition = ((AdapterView)w).getFirstVisiblePosition();
        if (n > 0) {
            if (firstVisiblePosition + childCount >= count && ((ViewGroup)w).getChildAt(childCount - 1).getBottom() <= ((View)w).getHeight()) {
                return false;
            }
        }
        else {
            if (n >= 0) {
                return false;
            }
            if (firstVisiblePosition <= 0 && ((ViewGroup)w).getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void j(final int n, final int n2) {
        gk0.b(this.w, n2);
    }
}

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.measurement.api.AppMeasurementSdk$OnEventListener;

// 
// Decompiled by Procyon v0.6.0
// 

public final class kf2 implements AppMeasurementSdk$OnEventListener
{
    public final uf2 a;
    
    public kf2(final uf2 a) {
        this.a = a;
    }
    
    public final void onEvent(final String s, final String s2, final Bundle bundle, final long n) {
        if (s != null && lc2.f(s2)) {
            final Bundle bundle2 = new Bundle();
            ((BaseBundle)bundle2).putString("name", s2);
            ((BaseBundle)bundle2).putLong("timestampInMillis", n);
            bundle2.putBundle("params", bundle);
            uf2.a(this.a).a(3, bundle2);
        }
    }
}

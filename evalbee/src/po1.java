// 
// Decompiled by Procyon v0.6.0
// 

public class po1
{
    public final boolean a;
    public final boolean b;
    
    public po1(final boolean a, final boolean b) {
        this.a = a;
        this.b = b;
    }
    
    public boolean a() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof po1)) {
            return false;
        }
        final po1 po1 = (po1)o;
        if (this.a != po1.a || this.b != po1.b) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return (this.a ? 1 : 0) * 31 + (this.b ? 1 : 0);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SnapshotMetadata{hasPendingWrites=");
        sb.append(this.a);
        sb.append(", isFromCache=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}

import android.graphics.Typeface;
import android.text.InputFilter;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import android.view.ActionMode$Callback;
import java.util.concurrent.ExecutionException;
import android.view.textclassifier.TextClassifier;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.Future;
import android.widget.TextView;

// 
// Decompiled by Procyon v0.6.0
// 

public class m7 extends TextView
{
    private final x5 mBackgroundTintHelper;
    private v6 mEmojiTextViewHelper;
    private boolean mIsSetTypefaceProcessing;
    private Future<g71> mPrecomputedTextFuture;
    private a mSuperCaller;
    private final k7 mTextClassifierHelper;
    private final l7 mTextHelper;
    
    public m7(final Context context) {
        this(context, null);
    }
    
    public m7(final Context context, final AttributeSet set) {
        this(context, set, 16842884);
    }
    
    public m7(final Context context, final AttributeSet set, final int n) {
        super(qw1.b(context), set, n);
        this.mIsSetTypefaceProcessing = false;
        this.mSuperCaller = null;
        pv1.a((View)this, ((View)this).getContext());
        (this.mBackgroundTintHelper = new x5((View)this)).e(set, n);
        final l7 mTextHelper = new l7(this);
        (this.mTextHelper = mTextHelper).m(set, n);
        mTextHelper.b();
        this.mTextClassifierHelper = new k7(this);
        this.getEmojiTextViewHelper().c(set, n);
    }
    
    public static /* synthetic */ int access$001(final m7 m7) {
        return m7.getAutoSizeMaxTextSize();
    }
    
    public static /* synthetic */ void access$1001(final m7 m7, final int firstBaselineToTopHeight) {
        m7.setFirstBaselineToTopHeight(firstBaselineToTopHeight);
    }
    
    public static /* synthetic */ int access$101(final m7 m7) {
        return m7.getAutoSizeMinTextSize();
    }
    
    public static /* synthetic */ void access$1101(final m7 m7, final int lastBaselineToBottomHeight) {
        m7.setLastBaselineToBottomHeight(lastBaselineToBottomHeight);
    }
    
    public static /* synthetic */ int access$201(final m7 m7) {
        return m7.getAutoSizeStepGranularity();
    }
    
    public static /* synthetic */ int[] access$301(final m7 m7) {
        return m7.getAutoSizeTextAvailableSizes();
    }
    
    public static /* synthetic */ int access$401(final m7 m7) {
        return m7.getAutoSizeTextType();
    }
    
    public static /* synthetic */ TextClassifier access$501(final m7 m7) {
        return m7.getTextClassifier();
    }
    
    public static /* synthetic */ void access$601(final m7 m7, final int n, final int n2, final int n3, final int n4) {
        m7.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
    }
    
    public static /* synthetic */ void access$701(final m7 m7, final int[] array, final int n) {
        m7.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
    }
    
    public static /* synthetic */ void access$801(final m7 m7, final int autoSizeTextTypeWithDefaults) {
        m7.setAutoSizeTextTypeWithDefaults(autoSizeTextTypeWithDefaults);
    }
    
    public static /* synthetic */ void access$901(final m7 m7, final TextClassifier textClassifier) {
        m7.setTextClassifier(textClassifier);
    }
    
    private v6 getEmojiTextViewHelper() {
        if (this.mEmojiTextViewHelper == null) {
            this.mEmojiTextViewHelper = new v6(this);
        }
        return this.mEmojiTextViewHelper;
    }
    
    public final void c() {
        final Future<g71> mPrecomputedTextFuture = this.mPrecomputedTextFuture;
        if (mPrecomputedTextFuture == null) {
            return;
        }
        try {
            this.mPrecomputedTextFuture = null;
            zu0.a(mPrecomputedTextFuture.get());
            nv1.n(this, null);
        }
        catch (final InterruptedException | ExecutionException ex) {}
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.b();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.b();
        }
    }
    
    public int getAutoSizeMaxTextSize() {
        if (u42.b) {
            return this.getSuperCaller().c();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.e();
        }
        return -1;
    }
    
    public int getAutoSizeMinTextSize() {
        if (u42.b) {
            return this.getSuperCaller().a();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.f();
        }
        return -1;
    }
    
    public int getAutoSizeStepGranularity() {
        if (u42.b) {
            return this.getSuperCaller().f();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.g();
        }
        return -1;
    }
    
    public int[] getAutoSizeTextAvailableSizes() {
        if (u42.b) {
            return this.getSuperCaller().k();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.h();
        }
        return new int[0];
    }
    
    public int getAutoSizeTextType() {
        final boolean b = u42.b;
        int n = 0;
        if (b) {
            if (this.getSuperCaller().j() == 1) {
                n = 1;
            }
            return n;
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.i();
        }
        return 0;
    }
    
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return nv1.q(super.getCustomSelectionActionModeCallback());
    }
    
    public int getFirstBaselineToTopHeight() {
        return nv1.b(this);
    }
    
    public int getLastBaselineToBottomHeight() {
        return nv1.c(this);
    }
    
    public a getSuperCaller() {
        if (this.mSuperCaller == null) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            a mSuperCaller;
            if (sdk_INT >= 28) {
                mSuperCaller = new c();
            }
            else {
                if (sdk_INT < 26) {
                    return this.mSuperCaller;
                }
                mSuperCaller = new b();
            }
            this.mSuperCaller = mSuperCaller;
        }
        return this.mSuperCaller;
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList c;
        if (mBackgroundTintHelper != null) {
            c = mBackgroundTintHelper.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode d;
        if (mBackgroundTintHelper != null) {
            d = mBackgroundTintHelper.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.mTextHelper.j();
    }
    
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.mTextHelper.k();
    }
    
    public CharSequence getText() {
        this.c();
        return super.getText();
    }
    
    public TextClassifier getTextClassifier() {
        if (Build$VERSION.SDK_INT < 28) {
            final k7 mTextClassifierHelper = this.mTextClassifierHelper;
            if (mTextClassifierHelper != null) {
                return mTextClassifierHelper.a();
            }
        }
        return this.getSuperCaller().b();
    }
    
    public g71.a getTextMetricsParamsCompat() {
        return nv1.g(this);
    }
    
    public boolean isEmojiCompatEnabled() {
        return this.getEmojiTextViewHelper().b();
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        this.mTextHelper.r(this, onCreateInputConnection, editorInfo);
        return w6.a(onCreateInputConnection, editorInfo, (View)this);
    }
    
    public void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.o(b, n, n2, n3, n4);
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        this.c();
        super.onMeasure(n, n2);
    }
    
    public void onTextChanged(final CharSequence charSequence, int n, final int n2, final int n3) {
        super.onTextChanged(charSequence, n, n2, n3);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null && !u42.b && mTextHelper.l()) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            this.mTextHelper.c();
        }
    }
    
    public void setAllCaps(final boolean allCaps) {
        super.setAllCaps(allCaps);
        this.getEmojiTextViewHelper().d(allCaps);
    }
    
    public void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) {
        if (u42.b) {
            this.getSuperCaller().d(n, n2, n3, n4);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.t(n, n2, n3, n4);
            }
        }
    }
    
    public void setAutoSizeTextTypeUniformWithPresetSizes(final int[] array, final int n) {
        if (u42.b) {
            this.getSuperCaller().h(array, n);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.u(array, n);
            }
        }
    }
    
    public void setAutoSizeTextTypeWithDefaults(final int n) {
        if (u42.b) {
            this.getSuperCaller().g(n);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.v(n);
            }
        }
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.g(backgroundResource);
        }
    }
    
    public void setCompoundDrawables(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCompoundDrawablesRelative(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(final int n, final int n2, final int n3, final int n4) {
        final Context context = ((View)this).getContext();
        Drawable b = null;
        Drawable b2;
        if (n != 0) {
            b2 = g7.b(context, n);
        }
        else {
            b2 = null;
        }
        Drawable b3;
        if (n2 != 0) {
            b3 = g7.b(context, n2);
        }
        else {
            b3 = null;
        }
        Drawable b4;
        if (n3 != 0) {
            b4 = g7.b(context, n3);
        }
        else {
            b4 = null;
        }
        if (n4 != 0) {
            b = g7.b(context, n4);
        }
        this.setCompoundDrawablesRelativeWithIntrinsicBounds(b2, b3, b4, b);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCompoundDrawablesWithIntrinsicBounds(final int n, final int n2, final int n3, final int n4) {
        final Context context = ((View)this).getContext();
        Drawable b = null;
        Drawable b2;
        if (n != 0) {
            b2 = g7.b(context, n);
        }
        else {
            b2 = null;
        }
        Drawable b3;
        if (n2 != 0) {
            b3 = g7.b(context, n2);
        }
        else {
            b3 = null;
        }
        Drawable b4;
        if (n3 != 0) {
            b4 = g7.b(context, n3);
        }
        else {
            b4 = null;
        }
        if (n4 != 0) {
            b = g7.b(context, n4);
        }
        this.setCompoundDrawablesWithIntrinsicBounds(b2, b3, b4, b);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCompoundDrawablesWithIntrinsicBounds(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCustomSelectionActionModeCallback(final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(nv1.r(this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean b) {
        this.getEmojiTextViewHelper().e(b);
    }
    
    public void setFilters(final InputFilter[] array) {
        super.setFilters(this.getEmojiTextViewHelper().a(array));
    }
    
    public void setFirstBaselineToTopHeight(final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.getSuperCaller().e(n);
        }
        else {
            nv1.k(this, n);
        }
    }
    
    public void setLastBaselineToBottomHeight(final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.getSuperCaller().i(n);
        }
        else {
            nv1.l(this, n);
        }
    }
    
    public void setLineHeight(final int n) {
        nv1.m(this, n);
    }
    
    public void setPrecomputedText(final g71 g71) {
        nv1.n(this, g71);
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.j(porterDuff$Mode);
        }
    }
    
    public void setSupportCompoundDrawablesTintList(final ColorStateList list) {
        this.mTextHelper.w(list);
        this.mTextHelper.b();
    }
    
    public void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode porterDuff$Mode) {
        this.mTextHelper.x(porterDuff$Mode);
        this.mTextHelper.b();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.q(context, n);
        }
    }
    
    public void setTextClassifier(final TextClassifier textClassifier) {
        if (Build$VERSION.SDK_INT < 28) {
            final k7 mTextClassifierHelper = this.mTextClassifierHelper;
            if (mTextClassifierHelper != null) {
                mTextClassifierHelper.b(textClassifier);
                return;
            }
        }
        this.getSuperCaller().l(textClassifier);
    }
    
    public void setTextFuture(final Future<g71> mPrecomputedTextFuture) {
        this.mPrecomputedTextFuture = mPrecomputedTextFuture;
        if (mPrecomputedTextFuture != null) {
            ((View)this).requestLayout();
        }
    }
    
    public void setTextMetricsParamsCompat(final g71.a a) {
        nv1.p(this, a);
    }
    
    public void setTextSize(final int n, final float n2) {
        if (u42.b) {
            super.setTextSize(n, n2);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.A(n, n2);
            }
        }
    }
    
    public void setTypeface(Typeface typeface, final int n) {
        if (this.mIsSetTypefaceProcessing) {
            return;
        }
        Typeface a;
        if (typeface != null && n > 0) {
            a = sz1.a(((View)this).getContext(), typeface, n);
        }
        else {
            a = null;
        }
        this.mIsSetTypefaceProcessing = true;
        if (a != null) {
            typeface = a;
        }
        try {
            super.setTypeface(typeface, n);
        }
        finally {
            this.mIsSetTypefaceProcessing = false;
        }
    }
    
    public interface a
    {
        int a();
        
        TextClassifier b();
        
        int c();
        
        void d(final int p0, final int p1, final int p2, final int p3);
        
        void e(final int p0);
        
        int f();
        
        void g(final int p0);
        
        void h(final int[] p0, final int p1);
        
        void i(final int p0);
        
        int j();
        
        int[] k();
        
        void l(final TextClassifier p0);
    }
    
    public class b implements a
    {
        public final m7 a;
        
        public b(final m7 a) {
            this.a = a;
        }
        
        @Override
        public int a() {
            return m7.access$101(this.a);
        }
        
        @Override
        public TextClassifier b() {
            return m7.access$501(this.a);
        }
        
        @Override
        public int c() {
            return m7.access$001(this.a);
        }
        
        @Override
        public void d(final int n, final int n2, final int n3, final int n4) {
            m7.access$601(this.a, n, n2, n3, n4);
        }
        
        @Override
        public void e(final int n) {
        }
        
        @Override
        public int f() {
            return m7.access$201(this.a);
        }
        
        @Override
        public void g(final int n) {
            m7.access$801(this.a, n);
        }
        
        @Override
        public void h(final int[] array, final int n) {
            m7.access$701(this.a, array, n);
        }
        
        @Override
        public void i(final int n) {
        }
        
        @Override
        public int j() {
            return m7.access$401(this.a);
        }
        
        @Override
        public int[] k() {
            return m7.access$301(this.a);
        }
        
        @Override
        public void l(final TextClassifier textClassifier) {
            m7.access$901(this.a, textClassifier);
        }
    }
    
    public class c extends b
    {
        public final m7 b;
        
        public c(final m7 b) {
            this.b = b.super();
        }
        
        @Override
        public void e(final int n) {
            m7.access$1001(this.b, n);
        }
        
        @Override
        public void i(final int n) {
            m7.access$1101(this.b, n);
        }
    }
}

import java.util.Iterator;
import com.google.common.collect.Range;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class y implements gc1
{
    public abstract void add(final Range p0);
    
    public void addAll(final gc1 gc1) {
        this.addAll(gc1.asRanges());
    }
    
    public void addAll(final Iterable iterable) {
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            this.add((Range)iterator.next());
        }
    }
    
    public void clear() {
        this.remove(Range.all());
    }
    
    public boolean contains(final Comparable comparable) {
        return this.rangeContaining(comparable) != null;
    }
    
    @Override
    public abstract boolean encloses(final Range p0);
    
    public boolean enclosesAll(final gc1 gc1) {
        return this.enclosesAll(gc1.asRanges());
    }
    
    public boolean enclosesAll(final Iterable iterable) {
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            if (!this.encloses((Range)iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof gc1 && this.asRanges().equals(((gc1)o).asRanges()));
    }
    
    @Override
    public final int hashCode() {
        return this.asRanges().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.asRanges().isEmpty();
    }
    
    public abstract Range rangeContaining(final Comparable p0);
    
    public abstract void remove(final Range p0);
    
    @Override
    public void removeAll(final gc1 gc1) {
        this.removeAll(gc1.asRanges());
    }
    
    public void removeAll(final Iterable iterable) {
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            this.remove((Range)iterator.next());
        }
    }
    
    @Override
    public final String toString() {
        return this.asRanges().toString();
    }
}

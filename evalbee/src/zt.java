import com.google.firestore.v1.Value;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.Comparator;

// 
// Decompiled by Procyon v0.6.0
// 

public interface zt
{
    public static final Comparator a = new yt();
    
    MutableDocument a();
    
    boolean b();
    
    boolean c();
    
    boolean d();
    
    qo1 e();
    
    boolean f();
    
    boolean g();
    
    a11 getData();
    
    du getKey();
    
    qo1 getVersion();
    
    boolean i();
    
    Value j(final s00 p0);
}

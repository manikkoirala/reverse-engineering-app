import java.util.Collection;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.core.Query;
import com.google.firebase.firestore.local.IndexManager;
import com.google.firebase.database.collection.b;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hv0 implements id1
{
    public com.google.firebase.database.collection.b a;
    public IndexManager b;
    
    public hv0() {
        this.a = au.a();
    }
    
    public static /* synthetic */ com.google.firebase.database.collection.b f(final hv0 hv0) {
        return hv0.a;
    }
    
    @Override
    public Map a(final Query query, final FieldIndex.a a, final Set set, final ga1 ga1) {
        final HashMap hashMap = new HashMap();
        final Iterator m = this.a.m(du.g((ke1)query.l().c("")));
        while (m.hasNext()) {
            final Map.Entry<K, zt> entry = m.next();
            final zt zt = entry.getValue();
            final du du = (du)entry.getKey();
            if (!query.l().k(du.m())) {
                break;
            }
            if (du.m().l() > query.l().l() + 1) {
                continue;
            }
            if (FieldIndex.a.f(zt).c(a) <= 0) {
                continue;
            }
            if (!set.contains(zt.getKey()) && !query.r(zt)) {
                continue;
            }
            hashMap.put(zt.getKey(), zt.a());
        }
        return hashMap;
    }
    
    @Override
    public Map b(final String s, final FieldIndex.a a, final int n) {
        throw new UnsupportedOperationException("getAll(String, IndexOffset, int) is not supported.");
    }
    
    @Override
    public void c(final MutableDocument mutableDocument, final qo1 qo1) {
        g9.d(this.b != null, "setIndexManager() not called", new Object[0]);
        g9.d(qo1.equals(qo1.b) ^ true, "Cannot add document to the RemoteDocumentCache with a read time of zero", new Object[0]);
        this.a = this.a.l(mutableDocument.getKey(), mutableDocument.a().v(qo1));
        this.b.a(mutableDocument.getKey().k());
    }
    
    @Override
    public MutableDocument d(final du du) {
        final zt zt = (zt)this.a.b(du);
        MutableDocument mutableDocument;
        if (zt != null) {
            mutableDocument = zt.a();
        }
        else {
            mutableDocument = MutableDocument.q(du);
        }
        return mutableDocument;
    }
    
    @Override
    public void e(final IndexManager b) {
        this.b = b;
    }
    
    public long g(final zk0 zk0) {
        final Iterator iterator = new b(null).iterator();
        long n = 0L;
        while (iterator.hasNext()) {
            n += zk0.k(iterator.next()).b();
        }
        return n;
    }
    
    @Override
    public Map getAll(final Iterable iterable) {
        final HashMap hashMap = new HashMap();
        for (final du du : iterable) {
            hashMap.put(du, this.d(du));
        }
        return hashMap;
    }
    
    public Iterable h() {
        return new b(null);
    }
    
    @Override
    public void removeAll(final Collection collection) {
        g9.d(this.b != null, "setIndexManager() not called", new Object[0]);
        final com.google.firebase.database.collection.b a = au.a();
        final Iterator iterator = collection.iterator();
        com.google.firebase.database.collection.b l = a;
        while (iterator.hasNext()) {
            final du du = (du)iterator.next();
            this.a = this.a.n(du);
            l = l.l(du, MutableDocument.r(du, qo1.b));
        }
        this.b.b(l);
    }
    
    public class b implements Iterable
    {
        public final hv0 a;
        
        public b(final hv0 a) {
            this.a = a;
        }
        
        @Override
        public Iterator iterator() {
            return new Iterator(this, hv0.f(this.a).iterator()) {
                public final Iterator a;
                public final b b;
                
                public zt b() {
                    return this.a.next().getValue();
                }
                
                @Override
                public boolean hasNext() {
                    return this.a.hasNext();
                }
            };
        }
    }
}

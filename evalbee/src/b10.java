import java.io.File;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class b10
{
    public static String a(final Context context, final String str) {
        final File externalFilesDir = context.getExternalFilesDir(ok.c);
        final StringBuilder sb = new StringBuilder();
        sb.append("Images/");
        sb.append(str);
        sb.append("_");
        sb.append(System.currentTimeMillis());
        final File file = new File(externalFilesDir, sb.toString());
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    
    public static void b(final String pathname) {
        final File file = new File(pathname);
        if (file.exists()) {
            c(file);
        }
    }
    
    public static void c(final File file) {
        if (file.isDirectory()) {
            final File[] listFiles = file.listFiles();
            for (int length = listFiles.length, i = 0; i < length; ++i) {
                c(listFiles[i]);
            }
        }
        file.delete();
    }
}

import com.google.gson.JsonIOException;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.AccessibleObject;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class cd1
{
    public static final b a;
    
    static {
        b a2;
        try {
            a2 = new d(null);
        }
        catch (final NoSuchMethodException ex) {
            a2 = new c(null);
        }
        a = a2;
    }
    
    public static void b(final AccessibleObject accessibleObject, final StringBuilder sb) {
        sb.append('(');
        Class<?>[] array;
        if (accessibleObject instanceof Method) {
            array = ((Method)accessibleObject).getParameterTypes();
        }
        else {
            array = ((Constructor)accessibleObject).getParameterTypes();
        }
        for (int i = 0; i < array.length; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(array[i].getSimpleName());
        }
        sb.append(')');
    }
    
    public static String c(final Constructor constructor) {
        final StringBuilder sb = new StringBuilder(constructor.getDeclaringClass().getName());
        b(constructor, sb);
        return sb.toString();
    }
    
    public static RuntimeException d(final ReflectiveOperationException cause) {
        throw new RuntimeException("Unexpected ReflectiveOperationException occurred (Gson 2.10.1). To support Java records, reflection is utilized to read out information about records. All these invocations happens after it is established that records exist in the JVM. This exception is unexpected behavior.", cause);
    }
    
    public static RuntimeException e(final IllegalAccessException cause) {
        throw new RuntimeException("Unexpected IllegalAccessException occurred (Gson 2.10.1). Certain ReflectionAccessFilter features require Java >= 9 to work correctly. If you are not using ReflectionAccessFilter, report this to the Gson maintainers.", cause);
    }
    
    public static String f(final Field field) {
        final StringBuilder sb = new StringBuilder();
        sb.append(field.getDeclaringClass().getName());
        sb.append("#");
        sb.append(field.getName());
        return sb.toString();
    }
    
    public static String g(final AccessibleObject accessibleObject, final boolean b) {
        String s = null;
        Label_0206: {
            StringBuilder sb2 = null;
            Label_0045: {
                String str;
                if (accessibleObject instanceof Field) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("field '");
                    str = f((Field)accessibleObject);
                    sb2 = sb;
                }
                else {
                    if (accessibleObject instanceof Method) {
                        final Method method = (Method)accessibleObject;
                        final StringBuilder sb3 = new StringBuilder(method.getName());
                        b(method, sb3);
                        final String string = sb3.toString();
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("method '");
                        sb4.append(method.getDeclaringClass().getName());
                        sb4.append("#");
                        sb4.append(string);
                        sb4.append("'");
                        s = sb4.toString();
                        break Label_0206;
                    }
                    if (!(accessibleObject instanceof Constructor)) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("<unknown AccessibleObject> ");
                        sb5.append(accessibleObject.toString());
                        sb2 = sb5;
                        break Label_0045;
                    }
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("constructor '");
                    str = c((Constructor)accessibleObject);
                    sb2 = sb6;
                }
                sb2.append(str);
                sb2.append("'");
            }
            s = sb2.toString();
        }
        String string2 = s;
        if (b) {
            string2 = s;
            if (Character.isLowerCase(s.charAt(0))) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(Character.toUpperCase(s.charAt(0)));
                sb7.append(s.substring(1));
                string2 = sb7.toString();
            }
        }
        return string2;
    }
    
    public static Method h(final Class clazz, final Field field) {
        return cd1.a.a(clazz, field);
    }
    
    public static Constructor i(final Class clazz) {
        return cd1.a.b(clazz);
    }
    
    public static String[] j(final Class clazz) {
        return cd1.a.c(clazz);
    }
    
    public static boolean k(final Class clazz) {
        return cd1.a.d(clazz);
    }
    
    public static void l(final AccessibleObject accessibleObject) {
        try {
            accessibleObject.setAccessible(true);
        }
        catch (final Exception ex) {
            final String g = g(accessibleObject, false);
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed making ");
            sb.append(g);
            sb.append(" accessible; either increase its visibility or write a custom TypeAdapter for its declaring type.");
            throw new JsonIOException(sb.toString(), ex);
        }
    }
    
    public static String m(final Constructor constructor) {
        try {
            constructor.setAccessible(true);
            return null;
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed making constructor '");
            sb.append(c(constructor));
            sb.append("' accessible; either increase its visibility or write a custom InstanceCreator or TypeAdapter for its declaring type: ");
            sb.append(ex.getMessage());
            return sb.toString();
        }
    }
    
    public abstract static class b
    {
        public abstract Method a(final Class p0, final Field p1);
        
        public abstract Constructor b(final Class p0);
        
        public abstract String[] c(final Class p0);
        
        public abstract boolean d(final Class p0);
    }
    
    public static class c extends b
    {
        public c() {
            super(null);
        }
        
        @Override
        public Method a(final Class clazz, final Field field) {
            throw new UnsupportedOperationException("Records are not supported on this JVM, this method should not be called");
        }
        
        @Override
        public Constructor b(final Class clazz) {
            throw new UnsupportedOperationException("Records are not supported on this JVM, this method should not be called");
        }
        
        @Override
        public String[] c(final Class clazz) {
            throw new UnsupportedOperationException("Records are not supported on this JVM, this method should not be called");
        }
        
        @Override
        public boolean d(final Class clazz) {
            return false;
        }
    }
    
    public static class d extends b
    {
        public final Method a;
        public final Method b;
        public final Method c;
        public final Method d;
        
        public d() {
            super(null);
            this.a = Class.class.getMethod("isRecord", (Class<?>[])new Class[0]);
            final Method method = Class.class.getMethod("getRecordComponents", (Class<?>[])new Class[0]);
            this.b = method;
            final Class<?> componentType = method.getReturnType().getComponentType();
            this.c = componentType.getMethod("getName", (Class[])new Class[0]);
            this.d = componentType.getMethod("getType", (Class[])new Class[0]);
        }
        
        @Override
        public Method a(final Class clazz, final Field field) {
            try {
                return clazz.getMethod(field.getName(), (Class[])new Class[0]);
            }
            catch (final ReflectiveOperationException ex) {
                throw d(ex);
            }
        }
        
        @Override
        public Constructor b(final Class obj) {
            try {
                final Object[] array = (Object[])this.b.invoke(obj, new Object[0]);
                final Class[] parameterTypes = new Class[array.length];
                for (int i = 0; i < array.length; ++i) {
                    parameterTypes[i] = (Class)this.d.invoke(array[i], new Object[0]);
                }
                return obj.getDeclaredConstructor((Class[])parameterTypes);
            }
            catch (final ReflectiveOperationException ex) {
                throw d(ex);
            }
        }
        
        @Override
        public String[] c(final Class obj) {
            try {
                final Object[] array = (Object[])this.b.invoke(obj, new Object[0]);
                final String[] array2 = new String[array.length];
                for (int i = 0; i < array.length; ++i) {
                    array2[i] = (String)this.c.invoke(array[i], new Object[0]);
                }
                return array2;
            }
            catch (final ReflectiveOperationException ex) {
                throw d(ex);
            }
        }
        
        @Override
        public boolean d(final Class obj) {
            try {
                return (boolean)this.a.invoke(obj, new Object[0]);
            }
            catch (final ReflectiveOperationException ex) {
                throw d(ex);
            }
        }
    }
}

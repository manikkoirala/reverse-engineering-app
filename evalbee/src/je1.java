import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import android.content.res.XmlResourceParser;
import android.content.res.Resources;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.graphics.drawable.Drawable$ConstantState;
import java.lang.ref.WeakReference;
import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuffColorFilter;
import android.content.res.ColorStateList;
import android.util.TypedValue;
import java.util.WeakHashMap;
import android.graphics.PorterDuff$Mode;

// 
// Decompiled by Procyon v0.6.0
// 

public final class je1
{
    public static final PorterDuff$Mode h;
    public static je1 i;
    public static final a j;
    public WeakHashMap a;
    public co1 b;
    public wo1 c;
    public final WeakHashMap d;
    public TypedValue e;
    public boolean f;
    public c g;
    
    static {
        h = PorterDuff$Mode.SRC_IN;
        j = new a(6);
    }
    
    public je1() {
        this.d = new WeakHashMap(0);
    }
    
    public static long d(final TypedValue typedValue) {
        return (long)typedValue.assetCookie << 32 | (long)typedValue.data;
    }
    
    public static PorterDuffColorFilter f(final ColorStateList list, final PorterDuff$Mode porterDuff$Mode, final int[] array) {
        if (list != null && porterDuff$Mode != null) {
            return k(list.getColorForState(array, 0), porterDuff$Mode);
        }
        return null;
    }
    
    public static je1 g() {
        synchronized (je1.class) {
            if (je1.i == null) {
                o(je1.i = new je1());
            }
            return je1.i;
        }
    }
    
    public static PorterDuffColorFilter k(final int n, final PorterDuff$Mode porterDuff$Mode) {
        synchronized (je1.class) {
            final a j = je1.j;
            PorterDuffColorFilter c;
            if ((c = j.c(n, porterDuff$Mode)) == null) {
                c = new PorterDuffColorFilter(n, porterDuff$Mode);
                j.d(n, porterDuff$Mode, c);
            }
            return c;
        }
    }
    
    public static void o(final je1 je1) {
    }
    
    public static boolean p(final Drawable drawable) {
        return drawable instanceof e32 || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }
    
    public static void v(final Drawable drawable, final rw1 rw1, final int[] array) {
        final int[] state = drawable.getState();
        if (fv.a(drawable) && drawable.mutate() != drawable) {
            Log.d("ResourceManagerInternal", "Mutated drawable is not the same instance as the input.");
            return;
        }
        if (drawable instanceof LayerDrawable && drawable.isStateful()) {
            drawable.setState(new int[0]);
            drawable.setState(state);
        }
        final boolean d = rw1.d;
        if (!d && !rw1.c) {
            drawable.clearColorFilter();
        }
        else {
            ColorStateList a;
            if (d) {
                a = rw1.a;
            }
            else {
                a = null;
            }
            PorterDuff$Mode porterDuff$Mode;
            if (rw1.c) {
                porterDuff$Mode = rw1.b;
            }
            else {
                porterDuff$Mode = je1.h;
            }
            drawable.setColorFilter((ColorFilter)f(a, porterDuff$Mode, array));
        }
    }
    
    public final boolean a(final Context context, final long n, final Drawable drawable) {
        synchronized (this) {
            final Drawable$ConstantState constantState = drawable.getConstantState();
            if (constantState != null) {
                hm0 value;
                if ((value = this.d.get(context)) == null) {
                    value = new hm0();
                    this.d.put(context, value);
                }
                value.j(n, new WeakReference(constantState));
                return true;
            }
            return false;
        }
    }
    
    public final void b(final Context context, final int n, final ColorStateList list) {
        if (this.a == null) {
            this.a = new WeakHashMap();
        }
        wo1 value;
        if ((value = this.a.get(context)) == null) {
            value = new wo1();
            this.a.put(context, value);
        }
        value.a(n, list);
    }
    
    public final void c(final Context context) {
        if (this.f) {
            return;
        }
        this.f = true;
        final Drawable i = this.i(context, bb1.a);
        if (i != null && p(i)) {
            return;
        }
        this.f = false;
        throw new IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
    }
    
    public final Drawable e(final Context context, final int n) {
        if (this.e == null) {
            this.e = new TypedValue();
        }
        final TypedValue e = this.e;
        context.getResources().getValue(n, e, true);
        final long d = d(e);
        final Drawable h = this.h(context, d);
        if (h != null) {
            return h;
        }
        final c g = this.g;
        Drawable a;
        if (g == null) {
            a = null;
        }
        else {
            a = g.a(this, context, n);
        }
        if (a != null) {
            a.setChangingConfigurations(e.changingConfigurations);
            this.a(context, d, a);
        }
        return a;
    }
    
    public final Drawable h(final Context key, final long n) {
        synchronized (this) {
            final hm0 hm0 = this.d.get(key);
            if (hm0 == null) {
                return null;
            }
            final WeakReference weakReference = (WeakReference)hm0.e(n);
            if (weakReference != null) {
                final Drawable$ConstantState drawable$ConstantState = (Drawable$ConstantState)weakReference.get();
                if (drawable$ConstantState != null) {
                    return drawable$ConstantState.newDrawable(key.getResources());
                }
                hm0.k(n);
            }
            return null;
        }
    }
    
    public Drawable i(final Context context, final int n) {
        synchronized (this) {
            return this.j(context, n, false);
        }
    }
    
    public Drawable j(final Context context, final int n, final boolean b) {
        synchronized (this) {
            this.c(context);
            Drawable drawable;
            if ((drawable = this.q(context, n)) == null) {
                drawable = this.e(context, n);
            }
            Drawable drawable2;
            if ((drawable2 = drawable) == null) {
                drawable2 = sl.getDrawable(context, n);
            }
            Drawable u;
            if ((u = drawable2) != null) {
                u = this.u(context, n, b, drawable2);
            }
            if (u != null) {
                fv.b(u);
            }
            return u;
        }
    }
    
    public ColorStateList l(final Context context, final int n) {
        synchronized (this) {
            ColorStateList m;
            if ((m = this.m(context, n)) == null) {
                final c g = this.g;
                ColorStateList b;
                if (g == null) {
                    b = null;
                }
                else {
                    b = g.b(context, n);
                }
                m = b;
                if (b != null) {
                    this.b(context, n, b);
                    m = b;
                }
            }
            return m;
        }
    }
    
    public final ColorStateList m(final Context key, final int n) {
        final WeakHashMap a = this.a;
        ColorStateList list = null;
        if (a != null) {
            final wo1 wo1 = a.get(key);
            list = list;
            if (wo1 != null) {
                list = (ColorStateList)wo1.f(n);
            }
        }
        return list;
    }
    
    public PorterDuff$Mode n(final int n) {
        final c g = this.g;
        PorterDuff$Mode c;
        if (g == null) {
            c = null;
        }
        else {
            c = g.c(n);
        }
        return c;
    }
    
    public final Drawable q(final Context context, final int n) {
        final co1 b = this.b;
        if (b == null || b.isEmpty()) {
            return null;
        }
        final wo1 c = this.c;
        if (c != null) {
            final String anObject = (String)c.f(n);
            if ("appcompat_skip_skip".equals(anObject) || (anObject != null && this.b.get(anObject) == null)) {
                return null;
            }
        }
        else {
            this.c = new wo1();
        }
        if (this.e == null) {
            this.e = new TypedValue();
        }
        final TypedValue e = this.e;
        final Resources resources = context.getResources();
        resources.getValue(n, e, true);
        final long d = d(e);
        final Drawable h = this.h(context, d);
        if (h != null) {
            return h;
        }
        final CharSequence string = e.string;
        Drawable drawable = h;
        if (string != null) {
            drawable = h;
            if (string.toString().endsWith(".xml")) {
                drawable = h;
                try {
                    final XmlResourceParser xml = resources.getXml(n);
                    drawable = h;
                    final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
                    int next;
                    do {
                        drawable = h;
                        next = ((XmlPullParser)xml).next();
                    } while (next != 2 && next != 1);
                    if (next != 2) {
                        drawable = h;
                        drawable = h;
                        final XmlPullParserException ex = new XmlPullParserException("No start tag found");
                        drawable = h;
                        throw ex;
                    }
                    drawable = h;
                    final String name = ((XmlPullParser)xml).getName();
                    drawable = h;
                    this.c.a(n, name);
                    drawable = h;
                    final b b2 = (b)this.b.get(name);
                    Drawable a = h;
                    if (b2 != null) {
                        drawable = h;
                        a = b2.a(context, (XmlPullParser)xml, attributeSet, context.getTheme());
                    }
                    if ((drawable = a) != null) {
                        drawable = a;
                        a.setChangingConfigurations(e.changingConfigurations);
                        drawable = a;
                        this.a(context, d, a);
                        drawable = a;
                    }
                }
                catch (final Exception ex2) {
                    Log.e("ResourceManagerInternal", "Exception while inflating drawable", (Throwable)ex2);
                }
            }
        }
        if (drawable == null) {
            this.c.a(n, "appcompat_skip_skip");
        }
        return drawable;
    }
    
    public void r(final Context key) {
        synchronized (this) {
            final hm0 hm0 = this.d.get(key);
            if (hm0 != null) {
                hm0.a();
            }
        }
    }
    
    public Drawable s(final Context context, final f32 f32, final int n) {
        synchronized (this) {
            Drawable drawable;
            if ((drawable = this.q(context, n)) == null) {
                drawable = f32.a(n);
            }
            if (drawable != null) {
                return this.u(context, n, false, drawable);
            }
            return null;
        }
    }
    
    public void t(final c g) {
        synchronized (this) {
            this.g = g;
        }
    }
    
    public final Drawable u(final Context context, final int n, final boolean b, final Drawable drawable) {
        final ColorStateList l = this.l(context, n);
        Drawable drawable2;
        if (l != null) {
            Drawable mutate = drawable;
            if (fv.a(drawable)) {
                mutate = drawable.mutate();
            }
            final Drawable r = wu.r(mutate);
            wu.o(r, l);
            final PorterDuff$Mode n2 = this.n(n);
            drawable2 = r;
            if (n2 != null) {
                wu.p(r, n2);
                drawable2 = r;
            }
        }
        else {
            final c g = this.g;
            if (g != null && g.d(context, n, drawable)) {
                drawable2 = drawable;
            }
            else {
                drawable2 = drawable;
                if (!this.w(context, n, drawable)) {
                    drawable2 = drawable;
                    if (b) {
                        drawable2 = null;
                    }
                }
            }
        }
        return drawable2;
    }
    
    public boolean w(final Context context, final int n, final Drawable drawable) {
        final c g = this.g;
        return g != null && g.e(context, n, drawable);
    }
    
    public static class a extends jm0
    {
        public a(final int n) {
            super(n);
        }
        
        public static int b(final int n, final PorterDuff$Mode porterDuff$Mode) {
            return (n + 31) * 31 + porterDuff$Mode.hashCode();
        }
        
        public PorterDuffColorFilter c(final int n, final PorterDuff$Mode porterDuff$Mode) {
            return (PorterDuffColorFilter)this.get(b(n, porterDuff$Mode));
        }
        
        public PorterDuffColorFilter d(final int n, final PorterDuff$Mode porterDuff$Mode, final PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter)this.put(b(n, porterDuff$Mode), porterDuffColorFilter);
        }
    }
    
    public interface b
    {
        Drawable a(final Context p0, final XmlPullParser p1, final AttributeSet p2, final Resources$Theme p3);
    }
    
    public interface c
    {
        Drawable a(final je1 p0, final Context p1, final int p2);
        
        ColorStateList b(final Context p0, final int p1);
        
        PorterDuff$Mode c(final int p0);
        
        boolean d(final Context p0, final int p1, final Drawable p2);
        
        boolean e(final Context p0, final int p1, final Drawable p2);
    }
}

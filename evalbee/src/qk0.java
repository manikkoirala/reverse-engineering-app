import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class qk0
{
    boolean mAbandoned;
    boolean mContentChanged;
    Context mContext;
    int mId;
    b mListener;
    a mOnLoadCanceledListener;
    boolean mProcessingChange;
    boolean mReset;
    boolean mStarted;
    
    public qk0(final Context context) {
        this.mStarted = false;
        this.mAbandoned = false;
        this.mReset = true;
        this.mContentChanged = false;
        this.mProcessingChange = false;
        this.mContext = context.getApplicationContext();
    }
    
    public void abandon() {
        this.mAbandoned = true;
        this.onAbandon();
    }
    
    public boolean cancelLoad() {
        return this.onCancelLoad();
    }
    
    public void commitContentChanged() {
        this.mProcessingChange = false;
    }
    
    public String dataToString(final Object o) {
        final StringBuilder sb = new StringBuilder(64);
        bq.a(o, sb);
        sb.append("}");
        return sb.toString();
    }
    
    public void deliverCancellation() {
    }
    
    public void deliverResult(final Object o) {
        final b mListener = this.mListener;
        if (mListener != null) {
            mListener.a(this, o);
        }
    }
    
    public void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        printWriter.print(s);
        printWriter.print("mId=");
        printWriter.print(this.mId);
        printWriter.print(" mListener=");
        printWriter.println(this.mListener);
        if (this.mStarted || this.mContentChanged || this.mProcessingChange) {
            printWriter.print(s);
            printWriter.print("mStarted=");
            printWriter.print(this.mStarted);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.mContentChanged);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.mProcessingChange);
        }
        if (this.mAbandoned || this.mReset) {
            printWriter.print(s);
            printWriter.print("mAbandoned=");
            printWriter.print(this.mAbandoned);
            printWriter.print(" mReset=");
            printWriter.println(this.mReset);
        }
    }
    
    public void forceLoad() {
        this.onForceLoad();
    }
    
    public Context getContext() {
        return this.mContext;
    }
    
    public int getId() {
        return this.mId;
    }
    
    public boolean isAbandoned() {
        return this.mAbandoned;
    }
    
    public boolean isReset() {
        return this.mReset;
    }
    
    public boolean isStarted() {
        return this.mStarted;
    }
    
    public void onAbandon() {
    }
    
    public abstract boolean onCancelLoad();
    
    public void onContentChanged() {
        if (this.mStarted) {
            this.forceLoad();
        }
        else {
            this.mContentChanged = true;
        }
    }
    
    public void onForceLoad() {
    }
    
    public void onReset() {
    }
    
    public abstract void onStartLoading();
    
    public void onStopLoading() {
    }
    
    public void registerListener(final int mId, final b mListener) {
        if (this.mListener == null) {
            this.mListener = mListener;
            this.mId = mId;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }
    
    public void registerOnLoadCanceledListener(final a a) {
    }
    
    public void reset() {
        this.onReset();
        this.mReset = true;
        this.mStarted = false;
        this.mAbandoned = false;
        this.mContentChanged = false;
        this.mProcessingChange = false;
    }
    
    public void rollbackContentChanged() {
        if (this.mProcessingChange) {
            this.onContentChanged();
        }
    }
    
    public final void startLoading() {
        this.mStarted = true;
        this.mReset = false;
        this.mAbandoned = false;
        this.onStartLoading();
    }
    
    public void stopLoading() {
        this.mStarted = false;
        this.onStopLoading();
    }
    
    public boolean takeContentChanged() {
        final boolean mContentChanged = this.mContentChanged;
        this.mContentChanged = false;
        this.mProcessingChange |= mContentChanged;
        return mContentChanged;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(64);
        bq.a(this, sb);
        sb.append(" id=");
        sb.append(this.mId);
        sb.append("}");
        return sb.toString();
    }
    
    public void unregisterListener(final b b) {
        final b mListener = this.mListener;
        if (mListener == null) {
            throw new IllegalStateException("No listener register");
        }
        if (mListener == b) {
            this.mListener = null;
            return;
        }
        throw new IllegalArgumentException("Attempting to unregister the wrong listener");
    }
    
    public void unregisterOnLoadCanceledListener(final a a) {
        throw new IllegalStateException("No listener register");
    }
    
    public interface a
    {
    }
    
    public interface b
    {
        void a(final qk0 p0, final Object p1);
    }
}

import androidx.constraintlayout.core.widgets.analyzer.b;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.c;
import androidx.constraintlayout.core.widgets.d;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;

// 
// Decompiled by Procyon v0.6.0
// 

public class zf1
{
    public static int h;
    public int a;
    public boolean b;
    public WidgetRun c;
    public WidgetRun d;
    public ArrayList e;
    public int f;
    public int g;
    
    public zf1(final WidgetRun widgetRun, final int g) {
        this.a = 0;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = new ArrayList();
        final int h = zf1.h;
        this.f = h;
        zf1.h = h + 1;
        this.c = widgetRun;
        this.d = widgetRun;
        this.g = g;
    }
    
    public void a(final WidgetRun widgetRun) {
        this.e.add(widgetRun);
        this.d = widgetRun;
    }
    
    public long b(final d d, final int n) {
        final WidgetRun c = this.c;
        final boolean b = c instanceof tf;
        long n2 = 0L;
        if (b) {
            if (((tf)c).f != n) {
                return 0L;
            }
        }
        else if (n == 0) {
            if (!(c instanceof c)) {
                return 0L;
            }
        }
        else if (!(c instanceof androidx.constraintlayout.core.widgets.analyzer.d)) {
            return 0L;
        }
        WidgetRun widgetRun;
        if (n == 0) {
            widgetRun = d.e;
        }
        else {
            widgetRun = d.f;
        }
        final DependencyNode h = widgetRun.h;
        WidgetRun widgetRun2;
        if (n == 0) {
            widgetRun2 = d.e;
        }
        else {
            widgetRun2 = d.f;
        }
        final DependencyNode i = widgetRun2.i;
        final boolean contains = c.h.l.contains(h);
        final boolean contains2 = this.c.i.l.contains(i);
        final long j = this.c.j();
        if (!contains || !contains2) {
            long d2;
            long b2;
            if (contains) {
                final DependencyNode h2 = this.c.h;
                d2 = this.d(h2, h2.f);
                b2 = this.c.h.f + j;
            }
            else {
                if (!contains2) {
                    final WidgetRun c2 = this.c;
                    final long n3 = c2.h.f + c2.j();
                    final WidgetRun widgetRun3 = this.c;
                    return n3 - widgetRun3.i.f;
                }
                final DependencyNode k = this.c.i;
                final long c3 = this.c(k, k.f);
                b2 = -this.c.i.f + j;
                d2 = -c3;
            }
            return Math.max(d2, b2);
        }
        final long d3 = this.d(this.c.h, 0L);
        final long c4 = this.c(this.c.i, 0L);
        final long n4 = d3 - j;
        final WidgetRun c5 = this.c;
        final int f = c5.i.f;
        long n5 = n4;
        if (n4 >= -f) {
            n5 = n4 + f;
        }
        final long n6 = -c4;
        final int f2 = c5.h.f;
        long n8;
        final long n7 = n8 = n6 - j - f2;
        if (n7 >= f2) {
            n8 = n7 - f2;
        }
        final float q = c5.b.q(n);
        if (q > 0.0f) {
            n2 = (long)(n8 / q + n5 / (1.0f - q));
        }
        final float n9 = (float)n2;
        final long n10 = (long)(n9 * q + 0.5f);
        final long n11 = (long)(n9 * (1.0f - q) + 0.5f);
        final WidgetRun widgetRun3 = this.c;
        final long n3 = widgetRun3.h.f + (n10 + j + n11);
        return n3 - widgetRun3.i.f;
    }
    
    public final long c(DependencyNode h, long n) {
        final WidgetRun d = h.d;
        if (d instanceof b) {
            return n;
        }
        final int size = h.k.size();
        int i = 0;
        long n2 = n;
        while (i < size) {
            final ps ps = h.k.get(i);
            long min = n2;
            if (ps instanceof DependencyNode) {
                final DependencyNode dependencyNode = (DependencyNode)ps;
                if (dependencyNode.d == d) {
                    min = n2;
                }
                else {
                    min = Math.min(n2, this.c(dependencyNode, dependencyNode.f + n));
                }
            }
            ++i;
            n2 = min;
        }
        long min2 = n2;
        if (h == d.i) {
            final long j = d.j();
            h = d.h;
            n -= j;
            min2 = Math.min(Math.min(n2, this.c(h, n)), n - d.h.f);
        }
        return min2;
    }
    
    public final long d(DependencyNode i, long n) {
        final WidgetRun d = i.d;
        if (d instanceof b) {
            return n;
        }
        final int size = i.k.size();
        int j = 0;
        long n2 = n;
        while (j < size) {
            final ps ps = i.k.get(j);
            long max = n2;
            if (ps instanceof DependencyNode) {
                final DependencyNode dependencyNode = (DependencyNode)ps;
                if (dependencyNode.d == d) {
                    max = n2;
                }
                else {
                    max = Math.max(n2, this.d(dependencyNode, dependencyNode.f + n));
                }
            }
            ++j;
            n2 = max;
        }
        long max2 = n2;
        if (i == d.h) {
            final long k = d.j();
            i = d.i;
            n += k;
            max2 = Math.max(Math.max(n2, this.d(i, n)), n - d.i.f);
        }
        return max2;
    }
}

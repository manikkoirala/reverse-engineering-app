import android.view.ViewGroup$MarginLayoutParams;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.widget.EditText;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yo
{
    public static EditText a(final Context context, final String text, final int n, final int n2) {
        final EditText editText = new EditText(context);
        ((TextView)editText).setText((CharSequence)text);
        ((TextView)editText).setInputType(1);
        ((TextView)editText).setTextAppearance(2131951925);
        ((View)editText).setBackgroundResource(2131230848);
        ((TextView)editText).setGravity(17);
        editText.setSelection(text.length());
        ((View)editText).setMinimumWidth(a91.d(n2, context));
        ((TextView)editText).setFilters(new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(n) });
        return editText;
    }
    
    public static LinearLayout b(final Context context) {
        final LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-1, a91.d(40, context));
        ((ViewGroup$MarginLayoutParams)layoutParams).setMargins(0, 1, 0, a91.d(8, context));
        ((View)linearLayout).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
        return linearLayout;
    }
    
    public static TextView c(final Context context, final String text, final int n) {
        final TextView textView = new TextView(context);
        textView.setText((CharSequence)text);
        textView.setMinWidth(a91.d(n, context));
        textView.setTextAppearance(2131952183);
        return textView;
    }
}

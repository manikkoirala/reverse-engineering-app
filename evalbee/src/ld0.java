import java.util.Collections;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ld0
{
    public final int a;
    public final List b;
    public final int c;
    public final InputStream d;
    public final byte[] e;
    
    public ld0(final int n, final List list) {
        this(n, list, -1, null);
    }
    
    public ld0(final int a, final List b, final int c, final InputStream d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = null;
    }
    
    public final InputStream a() {
        final InputStream d = this.d;
        if (d != null) {
            return d;
        }
        if (this.e != null) {
            return new ByteArrayInputStream(this.e);
        }
        return null;
    }
    
    public final int b() {
        return this.c;
    }
    
    public final List c() {
        return Collections.unmodifiableList((List<?>)this.b);
    }
    
    public final int d() {
        return this.a;
    }
}

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import android.view.View$OnClickListener;
import android.content.ContextWrapper;
import android.os.Build$VERSION;
import android.view.View;
import android.content.res.TypedArray;
import android.util.Log;
import android.util.AttributeSet;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class r7
{
    private static final String LOG_TAG = "AppCompatViewInflater";
    private static final int[] sAccessibilityHeading;
    private static final int[] sAccessibilityPaneTitle;
    private static final String[] sClassPrefixList;
    private static final co1 sConstructorMap;
    private static final Class<?>[] sConstructorSignature;
    private static final int[] sOnClickAttrs;
    private static final int[] sScreenReaderFocusable;
    private final Object[] mConstructorArgs;
    
    static {
        sConstructorSignature = new Class[] { Context.class, AttributeSet.class };
        sOnClickAttrs = new int[] { 16843375 };
        sAccessibilityHeading = new int[] { 16844160 };
        sAccessibilityPaneTitle = new int[] { 16844156 };
        sScreenReaderFocusable = new int[] { 16844148 };
        sClassPrefixList = new String[] { "android.widget.", "android.view.", "android.webkit." };
        sConstructorMap = new co1();
    }
    
    public r7() {
        this.mConstructorArgs = new Object[2];
    }
    
    public static Context e(final Context context, final AttributeSet set, final boolean b, final boolean b2) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, bc1.P3, 0, 0);
        int resourceId;
        if (b) {
            resourceId = obtainStyledAttributes.getResourceId(bc1.Q3, 0);
        }
        else {
            resourceId = 0;
        }
        int n = resourceId;
        if (b2 && (n = resourceId) == 0) {
            final int resourceId2 = obtainStyledAttributes.getResourceId(bc1.R3, 0);
            if ((n = resourceId2) != 0) {
                Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
                n = resourceId2;
            }
        }
        obtainStyledAttributes.recycle();
        Object o = context;
        if (n != 0) {
            if (context instanceof ul) {
                o = context;
                if (((ul)context).c() == n) {
                    return (Context)o;
                }
            }
            o = new ul(context, n);
        }
        return (Context)o;
    }
    
    public final void a(final Context context, final View view, final AttributeSet set) {
        if (Build$VERSION.SDK_INT > 28) {
            return;
        }
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, r7.sAccessibilityHeading);
        if (obtainStyledAttributes.hasValue(0)) {
            o32.r0(view, obtainStyledAttributes.getBoolean(0, false));
        }
        obtainStyledAttributes.recycle();
        final TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(set, r7.sAccessibilityPaneTitle);
        if (obtainStyledAttributes2.hasValue(0)) {
            o32.t0(view, obtainStyledAttributes2.getString(0));
        }
        obtainStyledAttributes2.recycle();
        final TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(set, r7.sScreenReaderFocusable);
        if (obtainStyledAttributes3.hasValue(0)) {
            o32.H0(view, obtainStyledAttributes3.getBoolean(0, false));
        }
        obtainStyledAttributes3.recycle();
    }
    
    public final void b(final View view, final AttributeSet set) {
        final Context context = view.getContext();
        if (context instanceof ContextWrapper) {
            if (o32.P(view)) {
                final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, r7.sOnClickAttrs);
                final String string = obtainStyledAttributes.getString(0);
                if (string != null) {
                    view.setOnClickListener((View$OnClickListener)new a(view, string));
                }
                obtainStyledAttributes.recycle();
            }
        }
    }
    
    public final View c(final Context context, final String str, String string) {
        final co1 sConstructorMap = r7.sConstructorMap;
        Label_0095: {
            Constructor<? extends View> constructor;
            if ((constructor = (Constructor)sConstructorMap.get(str)) != null) {
                break Label_0095;
            }
            Label_0062: {
                if (string == null) {
                    break Label_0062;
                }
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(string);
                    sb.append(str);
                    string = sb.toString();
                    while (true) {
                        constructor = Class.forName(string, false, context.getClassLoader()).asSubclass(View.class).getConstructor(r7.sConstructorSignature);
                        sConstructorMap.put(str, constructor);
                        constructor.setAccessible(true);
                        return (View)constructor.newInstance(this.mConstructorArgs);
                        string = str;
                        continue;
                    }
                }
                catch (final Exception ex) {
                    return null;
                }
            }
        }
    }
    
    public w5 createAutoCompleteTextView(final Context context, final AttributeSet set) {
        return new w5(context, set);
    }
    
    public y5 createButton(final Context context, final AttributeSet set) {
        return new y5(context, set);
    }
    
    public a6 createCheckBox(final Context context, final AttributeSet set) {
        return new a6(context, set);
    }
    
    public b6 createCheckedTextView(final Context context, final AttributeSet set) {
        return new b6(context, set);
    }
    
    public t6 createEditText(final Context context, final AttributeSet set) {
        return new t6(context, set);
    }
    
    public x6 createImageButton(final Context context, final AttributeSet set) {
        return new x6(context, set);
    }
    
    public z6 createImageView(final Context context, final AttributeSet set) {
        return new z6(context, set);
    }
    
    public a7 createMultiAutoCompleteTextView(final Context context, final AttributeSet set) {
        return new a7(context, set);
    }
    
    public d7 createRadioButton(final Context context, final AttributeSet set) {
        return new d7(context, set);
    }
    
    public e7 createRatingBar(final Context context, final AttributeSet set) {
        return new e7(context, set);
    }
    
    public h7 createSeekBar(final Context context, final AttributeSet set) {
        return new h7(context, set);
    }
    
    public j7 createSpinner(final Context context, final AttributeSet set) {
        return new j7(context, set);
    }
    
    public m7 createTextView(final Context context, final AttributeSet set) {
        return new m7(context, set);
    }
    
    public q7 createToggleButton(final Context context, final AttributeSet set) {
        return new q7(context, set);
    }
    
    public View createView(final Context context, final String s, final AttributeSet set) {
        return null;
    }
    
    public final View createView(View o, final String s, final Context context, final AttributeSet set, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        Context context2;
        if (b && o != null) {
            context2 = ((View)o).getContext();
        }
        else {
            context2 = context;
        }
        Context e = null;
        Label_0046: {
            if (!b2) {
                e = context2;
                if (!b3) {
                    break Label_0046;
                }
            }
            e = e(context2, set, b2, b3);
        }
        Context b5 = e;
        if (b4) {
            b5 = qw1.b(e);
        }
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 2001146706: {
                if (!s.equals("Button")) {
                    break;
                }
                n = 13;
                break;
            }
            case 1666676343: {
                if (!s.equals("EditText")) {
                    break;
                }
                n = 12;
                break;
            }
            case 1601505219: {
                if (!s.equals("CheckBox")) {
                    break;
                }
                n = 11;
                break;
            }
            case 1413872058: {
                if (!s.equals("AutoCompleteTextView")) {
                    break;
                }
                n = 10;
                break;
            }
            case 1125864064: {
                if (!s.equals("ImageView")) {
                    break;
                }
                n = 9;
                break;
            }
            case 799298502: {
                if (!s.equals("ToggleButton")) {
                    break;
                }
                n = 8;
                break;
            }
            case 776382189: {
                if (!s.equals("RadioButton")) {
                    break;
                }
                n = 7;
                break;
            }
            case -339785223: {
                if (!s.equals("Spinner")) {
                    break;
                }
                n = 6;
                break;
            }
            case -658531749: {
                if (!s.equals("SeekBar")) {
                    break;
                }
                n = 5;
                break;
            }
            case -937446323: {
                if (!s.equals("ImageButton")) {
                    break;
                }
                n = 4;
                break;
            }
            case -938935918: {
                if (!s.equals("TextView")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1346021293: {
                if (!s.equals("MultiAutoCompleteTextView")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1455429095: {
                if (!s.equals("CheckedTextView")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1946472170: {
                if (!s.equals("RatingBar")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        Label_0735: {
            switch (n) {
                default: {
                    o = this.createView(b5, s, set);
                    break Label_0735;
                }
                case 13: {
                    o = this.createButton(b5, set);
                    break;
                }
                case 12: {
                    o = this.createEditText(b5, set);
                    break;
                }
                case 11: {
                    o = this.createCheckBox(b5, set);
                    break;
                }
                case 10: {
                    o = this.createAutoCompleteTextView(b5, set);
                    break;
                }
                case 9: {
                    o = this.createImageView(b5, set);
                    break;
                }
                case 8: {
                    o = this.createToggleButton(b5, set);
                    break;
                }
                case 7: {
                    o = this.createRadioButton(b5, set);
                    break;
                }
                case 6: {
                    o = this.createSpinner(b5, set);
                    break;
                }
                case 5: {
                    o = this.createSeekBar(b5, set);
                    break;
                }
                case 4: {
                    o = this.createImageButton(b5, set);
                    break;
                }
                case 3: {
                    o = this.createTextView(b5, set);
                    break;
                }
                case 2: {
                    o = this.createMultiAutoCompleteTextView(b5, set);
                    break;
                }
                case 1: {
                    o = this.createCheckedTextView(b5, set);
                    break;
                }
                case 0: {
                    o = this.createRatingBar(b5, set);
                    break;
                }
            }
            this.f((View)o, s);
        }
        Object d = o;
        if (o == null) {
            d = o;
            if (context != b5) {
                d = this.d(b5, s, set);
            }
        }
        if (d != null) {
            this.b((View)d, set);
            this.a(b5, (View)d, set);
        }
        return (View)d;
    }
    
    public final View d(final Context context, final String s, final AttributeSet set) {
        String attributeValue = s;
        if (s.equals("view")) {
            attributeValue = set.getAttributeValue((String)null, "class");
        }
        try {
            final Object[] mConstructorArgs = this.mConstructorArgs;
            mConstructorArgs[0] = context;
            mConstructorArgs[1] = set;
            if (-1 != attributeValue.indexOf(46)) {
                return this.c(context, attributeValue, null);
            }
            int n = 0;
            while (true) {
                final String[] sClassPrefixList = r7.sClassPrefixList;
                if (n >= sClassPrefixList.length) {
                    return null;
                }
                final View c = this.c(context, attributeValue, sClassPrefixList[n]);
                if (c != null) {
                    return c;
                }
                ++n;
            }
        }
        catch (final Exception ex) {
            return null;
        }
        finally {
            final Object[] mConstructorArgs2 = this.mConstructorArgs;
            mConstructorArgs2[1] = (mConstructorArgs2[0] = null);
        }
    }
    
    public final void f(final View view, final String str) {
        if (view != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append(" asked to inflate view for <");
        sb.append(str);
        sb.append(">, but returned null");
        throw new IllegalStateException(sb.toString());
    }
    
    public static class a implements View$OnClickListener
    {
        public final View a;
        public final String b;
        public Method c;
        public Context d;
        
        public a(final View a, final String b) {
            this.a = a;
            this.b = b;
        }
        
        public final void a(Context context) {
        Label_0047_Outer:
            while (true) {
                Label_0070: {
                    if (context == null) {
                        break Label_0070;
                    }
                Label_0135_Outer:
                    while (true) {
                        try {
                            if (!context.isRestricted()) {
                                final Method method = context.getClass().getMethod(this.b, View.class);
                                if (method != null) {
                                    this.c = method;
                                    this.d = context;
                                    return;
                                }
                            }
                            if (context instanceof ContextWrapper) {
                                context = ((ContextWrapper)context).getBaseContext();
                                continue Label_0047_Outer;
                            }
                            context = null;
                            continue Label_0047_Outer;
                            while (true) {
                                int id;
                                while (true) {
                                    context = (Context)"";
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Could not find method ");
                                    sb.append(this.b);
                                    sb.append("(View) in a parent or ancestor Context for android:onClick attribute defined on view ");
                                    sb.append(this.a.getClass());
                                    sb.append((String)context);
                                    throw new IllegalStateException(sb.toString());
                                    id = this.a.getId();
                                    iftrue(Label_0089:)(id != -1);
                                    continue Label_0135_Outer;
                                }
                                Label_0089: {
                                    context = (Context)new StringBuilder();
                                }
                                ((StringBuilder)context).append(" with id '");
                                ((StringBuilder)context).append(this.a.getContext().getResources().getResourceEntryName(id));
                                ((StringBuilder)context).append("'");
                                context = (Context)((StringBuilder)context).toString();
                                continue;
                            }
                        }
                        catch (final NoSuchMethodException ex) {
                            continue;
                        }
                        break;
                    }
                }
            }
        }
        
        public void onClick(final View view) {
            if (this.c == null) {
                this.a(this.a.getContext());
            }
            try {
                this.c.invoke(this.d, view);
            }
            catch (final InvocationTargetException cause) {
                throw new IllegalStateException("Could not execute method for android:onClick", cause);
            }
            catch (final IllegalAccessException cause2) {
                throw new IllegalStateException("Could not execute non-public method for android:onClick", cause2);
            }
        }
    }
}

import java.util.Date;
import java.text.ParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import java.text.SimpleDateFormat;
import java.sql.Time;
import com.google.gson.reflect.TypeToken;
import java.text.DateFormat;

// 
// Decompiled by Procyon v0.6.0
// 

public final class dp1 extends hz1
{
    public static final iz1 b;
    public final DateFormat a;
    
    static {
        b = new iz1() {
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Class rawType = typeToken.getRawType();
                hz1 hz1 = null;
                if (rawType == Time.class) {
                    hz1 = new dp1(null);
                }
                return hz1;
            }
        };
    }
    
    public dp1() {
        this.a = new SimpleDateFormat("hh:mm:ss a");
    }
    
    public Time e(final rh0 rh0) {
        if (rh0.o0() == JsonToken.NULL) {
            rh0.R();
            return null;
        }
        final String i0 = rh0.i0();
        try {
            synchronized (this) {
                return new Time(this.a.parse(i0).getTime());
            }
        }
        catch (final ParseException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed parsing '");
            sb.append(i0);
            sb.append("' as SQL Time; at path ");
            sb.append(rh0.j());
            throw new JsonSyntaxException(sb.toString(), ex);
        }
    }
    
    public void f(final vh0 vh0, final Time date) {
        if (date == null) {
            vh0.u();
            return;
        }
        synchronized (this) {
            final String format = this.a.format(date);
            monitorexit(this);
            vh0.r0(format);
        }
    }
}

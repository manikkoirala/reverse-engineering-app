import java.util.List;
import java.util.AbstractCollection;
import com.google.android.gms.internal.firebase_auth_api.zzacf;
import com.google.android.gms.tasks.Tasks;
import android.os.Parcelable$Creator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.common.internal.Preconditions;
import android.content.SharedPreferences;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import android.content.SharedPreferences$Editor;
import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.common.api.Status;
import android.content.Context;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.internal.firebase-auth-api.zzap;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ed2
{
    public static final zzap d;
    public static final ed2 e;
    public Task a;
    public Task b;
    public long c;
    
    static {
        d = com.google.android.gms.internal.firebase_auth_api.zzap.zza((Object)"firebaseAppName", (Object)"firebaseUserUid", (Object)"operation", (Object)"tenantId", (Object)"verifyAssertionRequest", (Object)"statusCode", (Object)"statusMessage", (Object)"timestamp");
        e = new ed2();
    }
    
    public ed2() {
        this.c = 0L;
    }
    
    public static void b(final Context context, final Status status) {
        final SharedPreferences$Editor edit = context.getSharedPreferences("com.google.firebase.auth.internal.ProcessDeathHelper", 0).edit();
        edit.putInt("statusCode", status.getStatusCode());
        edit.putString("statusMessage", status.getStatusMessage());
        edit.putLong("timestamp", DefaultClock.getInstance().currentTimeMillis());
        edit.commit();
    }
    
    public static void c(final Context context, final zzagt zzagt, final String s, final String s2) {
        final SharedPreferences$Editor edit = context.getSharedPreferences("com.google.firebase.auth.internal.ProcessDeathHelper", 0).edit();
        edit.putString("verifyAssertionRequest", SafeParcelableSerializer.serializeToString(zzagt));
        edit.putString("operation", s);
        edit.putString("tenantId", s2);
        edit.putLong("timestamp", DefaultClock.getInstance().currentTimeMillis());
        edit.commit();
    }
    
    public static void d(final Context context, final String s, final String s2) {
        final SharedPreferences$Editor edit = context.getSharedPreferences("com.google.firebase.auth.internal.ProcessDeathHelper", 0).edit();
        edit.putString("recaptchaToken", s);
        edit.putString("operation", s2);
        edit.putLong("timestamp", DefaultClock.getInstance().currentTimeMillis());
        edit.commit();
    }
    
    public static void e(final SharedPreferences sharedPreferences) {
        final SharedPreferences$Editor edit = sharedPreferences.edit();
        final zzap d = ed2.d;
        final int size = ((AbstractCollection)d).size();
        int i = 0;
        while (i < size) {
            final String value = ((List<String>)d).get(i);
            ++i;
            edit.remove((String)value);
        }
        edit.commit();
    }
    
    public static ed2 g() {
        return ed2.e;
    }
    
    public final void a(final Context context) {
        Preconditions.checkNotNull(context);
        e(context.getSharedPreferences("com.google.firebase.auth.internal.ProcessDeathHelper", 0));
        this.a = null;
        this.c = 0L;
    }
    
    public final void f(final FirebaseAuth firebaseAuth) {
        Preconditions.checkNotNull(firebaseAuth);
        final Context l = firebaseAuth.d().l();
        int n = 0;
        final SharedPreferences sharedPreferences = l.getSharedPreferences("com.google.firebase.auth.internal.ProcessDeathHelper", 0);
        if (!firebaseAuth.d().o().equals(sharedPreferences.getString("firebaseAppName", ""))) {
            return;
        }
        if (sharedPreferences.contains("verifyAssertionRequest")) {
            final com.google.android.gms.internal.firebase_auth_api.zzagt zzagt = SafeParcelableSerializer.deserializeFromString(sharedPreferences.getString("verifyAssertionRequest", ""), (android.os.Parcelable$Creator<com.google.android.gms.internal.firebase_auth_api.zzagt>)com.google.android.gms.internal.firebase_auth_api.zzagt.CREATOR);
            final String string = sharedPreferences.getString("operation", "");
            final String string2 = sharedPreferences.getString("tenantId", (String)null);
            final String string3 = sharedPreferences.getString("firebaseUserUid", "");
            this.c = sharedPreferences.getLong("timestamp", 0L);
            if (string2 != null) {
                firebaseAuth.k(string2);
                zzagt.zzb(string2);
            }
            string.hashCode();
            Label_0250: {
                switch (string) {
                    case "com.google.firebase.auth.internal.NONGMSCORE_SIGN_IN": {
                        n = 2;
                        break Label_0250;
                    }
                    case "com.google.firebase.auth.internal.NONGMSCORE_LINK": {
                        n = 1;
                        break Label_0250;
                    }
                    case "com.google.firebase.auth.internal.NONGMSCORE_REAUTHENTICATE": {
                        break Label_0250;
                    }
                    default:
                        break;
                }
                n = -1;
            }
            Label_0366: {
                Task a = null;
                Label_0294: {
                    switch (n) {
                        case 2: {
                            a = firebaseAuth.l(lf2.O((zzagt)zzagt));
                            break Label_0294;
                        }
                        case 1: {
                            if (firebaseAuth.e().O().equals(string3)) {
                                a = firebaseAuth.q(firebaseAuth.e(), lf2.O((zzagt)zzagt));
                                break Label_0294;
                            }
                            break;
                        }
                        case 0: {
                            if (firebaseAuth.e().O().equals(string3)) {
                                a = firebaseAuth.F(firebaseAuth.e(), lf2.O((zzagt)zzagt));
                                break Label_0294;
                            }
                            break;
                        }
                    }
                    this.a = null;
                    break Label_0366;
                }
                this.a = a;
            }
            e(sharedPreferences);
            return;
        }
        if (sharedPreferences.contains("recaptchaToken")) {
            final String string4 = sharedPreferences.getString("recaptchaToken", "");
            final String string5 = sharedPreferences.getString("operation", "");
            this.c = sharedPreferences.getLong("timestamp", 0L);
            string5.hashCode();
            if (!string5.equals("com.google.firebase.auth.internal.ACTION_SHOW_RECAPTCHA")) {
                this.b = null;
            }
            else {
                this.b = Tasks.forResult((Object)string4);
            }
            e(sharedPreferences);
            return;
        }
        if (sharedPreferences.contains("statusCode")) {
            final Status status = new Status(sharedPreferences.getInt("statusCode", 17062), sharedPreferences.getString("statusMessage", ""));
            this.c = sharedPreferences.getLong("timestamp", 0L);
            e(sharedPreferences);
            this.a = Tasks.forException((Exception)zzacf.zza(status));
        }
    }
}

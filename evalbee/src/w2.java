import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.app.Fragment;
import java.util.Objects;
import androidx.fragment.app.e;
import android.app.Activity;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class w2
{
    public static mk0 c(final Activity activity, final mk0 mk0) {
        if (activity != null) {
            if (activity instanceof e) {
                final e e = (e)activity;
                Objects.requireNonNull(mk0);
                h(e, new t2(mk0));
            }
            else {
                Objects.requireNonNull(mk0);
                g(activity, new t2(mk0));
            }
        }
        return mk0;
    }
    
    public static Object d(final Class clazz, final Object obj, final String str) {
        if (obj == null) {
            return null;
        }
        try {
            return clazz.cast(obj);
        }
        catch (final ClassCastException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment with tag '");
            sb.append(str);
            sb.append("' is a ");
            sb.append(obj.getClass().getName());
            sb.append(" but should be a ");
            sb.append(clazz.getName());
            throw new IllegalStateException(sb.toString());
        }
    }
    
    public static void g(final Activity activity, final Runnable runnable) {
        g9.d(activity instanceof e ^ true, "onActivityStopCallOnce must be called with a *non*-FragmentActivity Activity.", new Object[0]);
        activity.runOnUiThread((Runnable)new u2(activity, runnable));
    }
    
    public static void h(final e e, final Runnable runnable) {
        e.runOnUiThread((Runnable)new v2(e, runnable));
    }
    
    public static class b
    {
        public final List a;
        
        public b() {
            this.a = new ArrayList();
        }
        
        public void a(final Runnable runnable) {
            synchronized (this) {
                this.a.add(runnable);
            }
        }
        
        public void b() {
            for (final Runnable runnable : this.a) {
                if (runnable != null) {
                    runnable.run();
                }
            }
        }
    }
    
    public static class c extends Fragment
    {
        public b a;
        
        public c() {
            this.a = new b(null);
        }
        
        public void onStop() {
            super.onStop();
            synchronized (this.a) {
                final b a = this.a;
                monitorexit(this.a = new b(null));
                a.b();
            }
        }
    }
    
    public static class d extends Fragment
    {
        public b a;
        
        public d() {
            this.a = new b(null);
        }
        
        @Override
        public void onStop() {
            super.onStop();
            synchronized (this.a) {
                final b a = this.a;
                monitorexit(this.a = new b(null));
                a.b();
            }
        }
    }
}

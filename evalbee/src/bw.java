import android.app.Dialog;
import android.widget.AdapterView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserProfileDto;
import android.app.ProgressDialog;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.Locale;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import android.view.View;
import android.view.View$OnClickListener;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import java.util.ArrayList;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.textfield.TextInputLayout;
import android.widget.EditText;
import android.widget.Button;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import android.content.SharedPreferences;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class bw extends u80
{
    public Context a;
    public y01 b;
    public SharedPreferences c;
    public boolean d;
    public SearchableSpinner e;
    public Button f;
    public EditText g;
    public EditText h;
    public EditText i;
    public TextInputLayout j;
    public TextInputLayout k;
    public TextInputLayout l;
    public SwipeRefreshLayout m;
    public bw n;
    
    public bw(final Context a, final boolean d, final y01 b) {
        super(a);
        this.n = this;
        this.a = a;
        this.d = d;
        this.b = b;
    }
    
    public final int e(final ArrayList list, String upperCase) {
        upperCase = upperCase.toUpperCase();
        for (int i = 0; i < list.size(); ++i) {
            if (((String)list.get(i)).toUpperCase().equals(upperCase)) {
                return i;
            }
        }
        return 0;
    }
    
    public final void f() {
        this.m.setRefreshing(true);
        new pa0(this.a, new zg(this) {
            public final bw a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                this.a.m.setRefreshing(false);
                ((View)this.a.f).setEnabled(true);
                this.a.g();
            }
        });
    }
    
    public final void g() {
        final OrgProfile instance = OrgProfile.getInstance(this.a);
        if (instance != null) {
            ((TextView)this.g).setText((CharSequence)instance.getDisplayName());
            ((TextView)this.h).setText((CharSequence)instance.getOrganization());
            if (instance.getDisplayName() == null || instance.getDisplayName().length() < 1) {
                ((TextView)this.h).setText((CharSequence)FirebaseAuth.getInstance().e().getDisplayName());
            }
        }
    }
    
    public final void h(final String s) {
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null) {
            e.Z(new g22.a().b(s).a());
        }
    }
    
    public final void i() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final bw a;
            
            public void onClick(final View view) {
                final String trim = this.a.g.getText().toString().trim();
                final OrgProfile instance = OrgProfile.getInstance(this.a.a);
                if (trim.equals("")) {
                    a91.G(this.a.a, 2131886274, 2131230909, 2131231086);
                    return;
                }
                final String trim2 = this.a.h.getText().toString().trim();
                if (instance != null && instance.getRole() == UserRole.OWNER && trim2.equals("")) {
                    a91.G(this.a.a, 2131886276, 2131230909, 2131231086);
                    return;
                }
                if (this.a.e.getSelectedItem() == null) {
                    a91.G(this.a.a, 2131886782, 2131230909, 2131231086);
                    return;
                }
                this.a.c.edit().putString("user_country", (String)this.a.e.getSelectedItem()).commit();
                this.a.c.edit().putString("user_organization", trim2).commit();
                final bw a = this.a;
                a.h(a.g.getText().toString());
                this.a.k(trim, trim2);
            }
        });
    }
    
    public final void j() {
        final Locale[] availableLocales = Locale.getAvailableLocales();
        final ArrayList list = new ArrayList();
        for (int length = availableLocales.length, i = 0; i < length; ++i) {
            final String displayCountry = availableLocales[i].getDisplayCountry();
            if (displayCountry.length() > 0 && !list.contains(displayCountry)) {
                list.add(displayCountry);
            }
        }
        Collections.sort((List<Object>)list, (Comparator<? super Object>)String.CASE_INSENSITIVE_ORDER);
        this.e.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (List)list));
        ((AdapterView)this.e).setSelection(this.e(list, this.c.getString("user_country", "India")));
        this.e.setTitle(this.a.getString(2131886782));
    }
    
    public final void k(final String s, final String s2) {
        final ProgressDialog progressDialog = new ProgressDialog(this.a);
        progressDialog.setMessage((CharSequence)"Please wait, updating profile...");
        ((Dialog)progressDialog).show();
        new s61(new UserProfileDto(s, s2), this.a, new zg(this, progressDialog, s, s2) {
            public final ProgressDialog a;
            public final String b;
            public final String c;
            public final bw d;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    final OrgProfile instance = OrgProfile.getInstance(this.d.a);
                    instance.setDisplayName(this.b);
                    instance.setOrganization(this.c);
                    instance.save(this.d.a);
                    final y01 b2 = this.d.b;
                    if (b2 != null) {
                        b2.a(null);
                    }
                    this.d.n.dismiss();
                }
                else {
                    a91.H(this.d.a, "Save failed, please try again", 2131230909, 2131231086);
                }
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131493000);
        ((Toolbar)this.findViewById(2131297334)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final bw a;
            
            public void onClick(final View view) {
                final y01 b = this.a.b;
                if (b != null) {
                    b.a(null);
                }
                this.a.dismiss();
            }
        });
        this.c = this.a.getSharedPreferences("MyPref", 0);
        this.e = (SearchableSpinner)this.findViewById(2131297093);
        this.k = (TextInputLayout)this.findViewById(2131297183);
        this.l = (TextInputLayout)this.findViewById(2131297178);
        final TextInputLayout j = (TextInputLayout)this.findViewById(2131297184);
        this.j = j;
        this.g = j.getEditText();
        this.h = this.l.getEditText();
        this.i = this.k.getEditText();
        this.f = (Button)this.findViewById(2131296466);
        this.m = (SwipeRefreshLayout)this.findViewById(2131297134);
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null) {
            ((TextView)this.g).setText((CharSequence)e.getDisplayName());
            final EditText g = this.g;
            g.setSelection(((TextView)g).length());
            ((TextView)this.i).setText((CharSequence)e.getEmail());
        }
        ((TextView)this.h).setText((CharSequence)this.c.getString("user_organization", ""));
        this.m.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final bw a;
            
            @Override
            public void a() {
                this.a.f();
            }
        });
        this.j();
        this.i();
        this.f();
        this.g();
    }
}

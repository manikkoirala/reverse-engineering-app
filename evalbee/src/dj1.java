import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Date;
import java.text.SimpleDateFormat;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class dj1
{
    public static String a;
    public static int b;
    public static int c;
    
    public static void a(final Context context, final int n) {
        dj1.b -= n;
        context.getSharedPreferences("MyPref", 0).edit().putInt("month_scan", dj1.b).commit();
    }
    
    public static String b() {
        return new SimpleDateFormat("MM-yyyy").format(new Date());
    }
    
    public static void c(final Context context) {
        ++dj1.b;
        ++dj1.c;
        final SharedPreferences sharedPreferences = context.getSharedPreferences("MyPref", 0);
        if (dj1.b == 100) {
            FirebaseAnalytics.getInstance(context).a("SCAN100", null);
        }
        if (dj1.b == 200) {
            FirebaseAnalytics.getInstance(context).a("SCAN200", null);
        }
        if (dj1.b == 500) {
            FirebaseAnalytics.getInstance(context).a("SCAN500", null);
        }
        sharedPreferences.edit().putInt("total_scan", dj1.c).putInt("month_scan", dj1.b).commit();
    }
    
    public static void d(final Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("MyPref", 0);
        dj1.a = sharedPreferences.getString("currentMonth", "");
        dj1.b = sharedPreferences.getInt("month_scan", 0);
        dj1.c = sharedPreferences.getInt("total_scan", 0);
        final String b = b();
        if (!b.equals(dj1.a)) {
            dj1.a = b;
            dj1.b = 0;
            sharedPreferences.edit().putString("currentMonth", dj1.a).putInt("month_scan", dj1.b).commit();
        }
    }
}

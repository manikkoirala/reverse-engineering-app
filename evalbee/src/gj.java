import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public interface gj
{
    default Object a(final Class clazz) {
        return this.d(da1.b(clazz));
    }
    
    r91 b(final da1 p0);
    
    default Set c(final Class clazz) {
        return this.h(da1.b(clazz));
    }
    
    default Object d(final da1 da1) {
        final r91 g = this.g(da1);
        if (g == null) {
            return null;
        }
        return g.get();
    }
    
    default r91 e(final Class clazz) {
        return this.g(da1.b(clazz));
    }
    
    jr f(final da1 p0);
    
    r91 g(final da1 p0);
    
    default Set h(final da1 da1) {
        return (Set)this.b(da1).get();
    }
    
    default jr i(final Class clazz) {
        return this.f(da1.b(clazz));
    }
}

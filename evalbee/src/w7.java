import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;
import android.content.ComponentName;
import android.os.Build$VERSION;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class w7
{
    public static void a(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ldc             ""
        //     3: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //     6: ifeq            17
        //     9: aload_0        
        //    10: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    12: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //    15: pop            
        //    16: return         
        //    17: aload_0        
        //    18: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    20: iconst_0       
        //    21: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
        //    24: astore_0       
        //    25: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
        //    28: astore_2       
        //    29: aload_2        
        //    30: aload_0        
        //    31: aconst_null    
        //    32: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
        //    37: aload_2        
        //    38: ldc             "UTF-8"
        //    40: getstatic       java/lang/Boolean.TRUE:Ljava/lang/Boolean;
        //    43: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
        //    48: aload_2        
        //    49: aconst_null    
        //    50: ldc             "locales"
        //    52: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    57: pop            
        //    58: aload_2        
        //    59: aconst_null    
        //    60: ldc             "application_locales"
        //    62: aload_1        
        //    63: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    68: pop            
        //    69: aload_2        
        //    70: aconst_null    
        //    71: ldc             "locales"
        //    73: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    78: pop            
        //    79: aload_2        
        //    80: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
        //    85: new             Ljava/lang/StringBuilder;
        //    88: astore_2       
        //    89: aload_2        
        //    90: invokespecial   java/lang/StringBuilder.<init>:()V
        //    93: aload_2        
        //    94: ldc             "Storing App Locales : app-locales: "
        //    96: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    99: pop            
        //   100: aload_2        
        //   101: aload_1        
        //   102: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   105: pop            
        //   106: aload_2        
        //   107: ldc             " persisted successfully."
        //   109: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   112: pop            
        //   113: ldc             "AppLocalesStorageHelper"
        //   115: aload_2        
        //   116: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   119: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   122: pop            
        //   123: aload_0        
        //   124: ifnull          178
        //   127: aload_0        
        //   128: invokevirtual   java/io/FileOutputStream.close:()V
        //   131: goto            178
        //   134: astore_1       
        //   135: goto            179
        //   138: astore_2       
        //   139: new             Ljava/lang/StringBuilder;
        //   142: astore_3       
        //   143: aload_3        
        //   144: invokespecial   java/lang/StringBuilder.<init>:()V
        //   147: aload_3        
        //   148: ldc             "Storing App Locales : Failed to persist app-locales: "
        //   150: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   153: pop            
        //   154: aload_3        
        //   155: aload_1        
        //   156: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   159: pop            
        //   160: ldc             "AppLocalesStorageHelper"
        //   162: aload_3        
        //   163: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   166: aload_2        
        //   167: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   170: pop            
        //   171: aload_0        
        //   172: ifnull          178
        //   175: goto            127
        //   178: return         
        //   179: aload_0        
        //   180: ifnull          187
        //   183: aload_0        
        //   184: invokevirtual   java/io/FileOutputStream.close:()V
        //   187: aload_1        
        //   188: athrow         
        //   189: astore_0       
        //   190: ldc             "AppLocalesStorageHelper"
        //   192: ldc             "Storing App Locales : FileNotFoundException: Cannot open file %s for writing "
        //   194: iconst_1       
        //   195: anewarray       Ljava/lang/Object;
        //   198: dup            
        //   199: iconst_0       
        //   200: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   202: aastore        
        //   203: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   206: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   209: pop            
        //   210: return         
        //   211: astore_0       
        //   212: goto            178
        //   215: astore_0       
        //   216: goto            187
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  17     25     189    211    Ljava/io/FileNotFoundException;
        //  29     123    138    178    Ljava/lang/Exception;
        //  29     123    134    189    Any
        //  127    131    211    215    Ljava/io/IOException;
        //  139    171    134    189    Any
        //  183    187    215    219    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 116 out of bounds for length 116
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String b(final Context p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: astore          4
        //     4: aload_0        
        //     5: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //     7: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    10: astore          6
        //    12: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    15: astore          5
        //    17: aload           5
        //    19: aload           6
        //    21: ldc             "UTF-8"
        //    23: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    28: aload           5
        //    30: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    35: istore_1       
        //    36: aload           5
        //    38: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    43: istore_2       
        //    44: aload           4
        //    46: astore_3       
        //    47: iload_2        
        //    48: iconst_1       
        //    49: if_icmpeq       110
        //    52: iload_2        
        //    53: iconst_3       
        //    54: if_icmpne       71
        //    57: aload           4
        //    59: astore_3       
        //    60: aload           5
        //    62: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    67: iload_1        
        //    68: if_icmple       110
        //    71: iload_2        
        //    72: iconst_3       
        //    73: if_icmpeq       36
        //    76: iload_2        
        //    77: iconst_4       
        //    78: if_icmpne       84
        //    81: goto            36
        //    84: aload           5
        //    86: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    91: ldc             "locales"
        //    93: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    96: ifeq            36
        //    99: aload           5
        //   101: aconst_null    
        //   102: ldc             "application_locales"
        //   104: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   109: astore_3       
        //   110: aload_3        
        //   111: astore          5
        //   113: aload           6
        //   115: ifnull          157
        //   118: aload           6
        //   120: invokevirtual   java/io/FileInputStream.close:()V
        //   123: aload_3        
        //   124: astore          5
        //   126: goto            157
        //   129: astore_0       
        //   130: goto            210
        //   133: astore_3       
        //   134: ldc             "AppLocalesStorageHelper"
        //   136: ldc             "Reading app Locales : Unable to parse through file :androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   138: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   141: pop            
        //   142: aload           4
        //   144: astore          5
        //   146: aload           6
        //   148: ifnull          157
        //   151: aload           4
        //   153: astore_3       
        //   154: goto            118
        //   157: aload           5
        //   159: invokevirtual   java/lang/String.isEmpty:()Z
        //   162: ifne            200
        //   165: new             Ljava/lang/StringBuilder;
        //   168: dup            
        //   169: invokespecial   java/lang/StringBuilder.<init>:()V
        //   172: astore_0       
        //   173: aload_0        
        //   174: ldc             "Reading app Locales : Locales read from file: androidx.appcompat.app.AppCompatDelegate.application_locales_record_file , appLocales: "
        //   176: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   179: pop            
        //   180: aload_0        
        //   181: aload           5
        //   183: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   186: pop            
        //   187: ldc             "AppLocalesStorageHelper"
        //   189: aload_0        
        //   190: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   193: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   196: pop            
        //   197: goto            207
        //   200: aload_0        
        //   201: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   203: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //   206: pop            
        //   207: aload           5
        //   209: areturn        
        //   210: aload           6
        //   212: ifnull          220
        //   215: aload           6
        //   217: invokevirtual   java/io/FileInputStream.close:()V
        //   220: aload_0        
        //   221: athrow         
        //   222: astore_0       
        //   223: ldc             "AppLocalesStorageHelper"
        //   225: ldc             "Reading app Locales : Locales record file not found: androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   227: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   230: pop            
        //   231: ldc             ""
        //   233: areturn        
        //   234: astore          4
        //   236: aload_3        
        //   237: astore          5
        //   239: goto            157
        //   242: astore_3       
        //   243: goto            220
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  4      12     222    234    Ljava/io/FileNotFoundException;
        //  12     36     133    157    Lorg/xmlpull/v1/XmlPullParserException;
        //  12     36     133    157    Ljava/io/IOException;
        //  12     36     129    222    Any
        //  36     44     133    157    Lorg/xmlpull/v1/XmlPullParserException;
        //  36     44     133    157    Ljava/io/IOException;
        //  36     44     129    222    Any
        //  60     71     133    157    Lorg/xmlpull/v1/XmlPullParserException;
        //  60     71     133    157    Ljava/io/IOException;
        //  60     71     129    222    Any
        //  84     110    133    157    Lorg/xmlpull/v1/XmlPullParserException;
        //  84     110    133    157    Ljava/io/IOException;
        //  84     110    129    222    Any
        //  118    123    234    242    Ljava/io/IOException;
        //  134    142    129    222    Any
        //  215    220    242    246    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.base/java.util.ArrayList$Itr.checkForComodification(ArrayList.java:1043)
        //     at java.base/java.util.ArrayList$Itr.next(ArrayList.java:997)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2913)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void c(final Context context) {
        if (Build$VERSION.SDK_INT >= 33) {
            final ComponentName componentName = new ComponentName(context, "androidx.appcompat.app.AppLocalesMetadataHolderService");
            if (context.getPackageManager().getComponentEnabledSetting(componentName) != 1) {
                if (f6.i().e()) {
                    final String b = b(context);
                    final Object systemService = context.getSystemService("locale");
                    if (systemService != null) {
                        f6.b.b(systemService, f6.a.a(b));
                    }
                }
                context.getPackageManager().setComponentEnabledSetting(componentName, 1, 1);
            }
        }
    }
    
    public static class a implements Executor
    {
        public final Object a;
        public final Queue b;
        public final Executor c;
        public Runnable d;
        
        public a(final Executor c) {
            this.a = new Object();
            this.b = new ArrayDeque();
            this.c = c;
        }
        
        public void c() {
            synchronized (this.a) {
                final Runnable d = this.b.poll();
                this.d = d;
                if (d != null) {
                    this.c.execute(d);
                }
            }
        }
        
        @Override
        public void execute(final Runnable runnable) {
            synchronized (this.a) {
                this.b.add(new v7(this, runnable));
                if (this.d == null) {
                    this.c();
                }
            }
        }
    }
    
    public static class b implements Executor
    {
        @Override
        public void execute(final Runnable target) {
            new Thread(target).start();
        }
    }
}

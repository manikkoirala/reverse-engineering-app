import java.util.Iterator;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;
import androidx.work.WorkInfo$State;
import androidx.work.OutOfQuotaPolicy;
import android.os.Build$VERSION;
import androidx.work.NetworkType;
import java.io.IOException;
import java.io.Closeable;
import android.net.Uri;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ByteArrayInputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.NoWhenBranchMatchedException;
import androidx.work.BackoffPolicy;

// 
// Decompiled by Procyon v0.6.0
// 

public final class z92
{
    public static final z92 a;
    
    static {
        a = new z92();
    }
    
    public static final int a(final BackoffPolicy backoffPolicy) {
        fg0.e((Object)backoffPolicy, "backoffPolicy");
        final int n = z92.a.b[backoffPolicy.ordinal()];
        int n2 = 1;
        if (n != 1) {
            if (n != 2) {
                throw new NoWhenBranchMatchedException();
            }
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    public static final Set b(byte[] array) {
        fg0.e((Object)array, "bytes");
        final LinkedHashSet set = new LinkedHashSet();
        final int length = array.length;
        final int n = 0;
        if (length == 0) {
            return set;
        }
        array = (byte[])(Object)new ByteArrayInputStream(array);
        try {
            final ObjectInputStream objectInputStream = new ObjectInputStream((InputStream)(Object)array);
            try {
                for (int int1 = objectInputStream.readInt(), i = n; i < int1; ++i) {
                    final Uri parse = Uri.parse(objectInputStream.readUTF());
                    final boolean boolean1 = objectInputStream.readBoolean();
                    fg0.d((Object)parse, "uri");
                    set.add(new zk.c(parse, boolean1));
                }
                final u02 a = u02.a;
                dh.a((Closeable)objectInputStream, (Throwable)null);
            }
            finally {
                try {}
                finally {
                    final Throwable t;
                    dh.a((Closeable)objectInputStream, t);
                }
            }
        }
        catch (final IOException ex) {}
        finally {
            try {}
            finally {
                dh.a((Closeable)(Object)array, (Throwable)set);
            }
            final u02 a2 = u02.a;
            dh.a((Closeable)(Object)array, (Throwable)null);
            return set;
        }
    }
    
    public static final BackoffPolicy c(final int i) {
        BackoffPolicy backoffPolicy;
        if (i != 0) {
            if (i != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not convert ");
                sb.append(i);
                sb.append(" to BackoffPolicy");
                throw new IllegalArgumentException(sb.toString());
            }
            backoffPolicy = BackoffPolicy.LINEAR;
        }
        else {
            backoffPolicy = BackoffPolicy.EXPONENTIAL;
        }
        return backoffPolicy;
    }
    
    public static final NetworkType d(final int i) {
        NetworkType networkType;
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            if (Build$VERSION.SDK_INT >= 30 && i == 5) {
                                return NetworkType.TEMPORARILY_UNMETERED;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Could not convert ");
                            sb.append(i);
                            sb.append(" to NetworkType");
                            throw new IllegalArgumentException(sb.toString());
                        }
                        else {
                            networkType = NetworkType.METERED;
                        }
                    }
                    else {
                        networkType = NetworkType.NOT_ROAMING;
                    }
                }
                else {
                    networkType = NetworkType.UNMETERED;
                }
            }
            else {
                networkType = NetworkType.CONNECTED;
            }
        }
        else {
            networkType = NetworkType.NOT_REQUIRED;
        }
        return networkType;
    }
    
    public static final OutOfQuotaPolicy e(final int i) {
        OutOfQuotaPolicy outOfQuotaPolicy;
        if (i != 0) {
            if (i != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not convert ");
                sb.append(i);
                sb.append(" to OutOfQuotaPolicy");
                throw new IllegalArgumentException(sb.toString());
            }
            outOfQuotaPolicy = OutOfQuotaPolicy.DROP_WORK_REQUEST;
        }
        else {
            outOfQuotaPolicy = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        }
        return outOfQuotaPolicy;
    }
    
    public static final WorkInfo$State f(final int i) {
        WorkInfo$State workInfo$State;
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            if (i != 5) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Could not convert ");
                                sb.append(i);
                                sb.append(" to State");
                                throw new IllegalArgumentException(sb.toString());
                            }
                            workInfo$State = WorkInfo$State.CANCELLED;
                        }
                        else {
                            workInfo$State = WorkInfo$State.BLOCKED;
                        }
                    }
                    else {
                        workInfo$State = WorkInfo$State.FAILED;
                    }
                }
                else {
                    workInfo$State = WorkInfo$State.SUCCEEDED;
                }
            }
            else {
                workInfo$State = WorkInfo$State.RUNNING;
            }
        }
        else {
            workInfo$State = WorkInfo$State.ENQUEUED;
        }
        return workInfo$State;
    }
    
    public static final int g(final NetworkType obj) {
        fg0.e((Object)obj, "networkType");
        final int n = z92.a.c[obj.ordinal()];
        int n2 = 1;
        if (n != 1) {
            final int n3 = 2;
            if (n != 2) {
                final int n4 = 3;
                n2 = n3;
                if (n != 3) {
                    final int n5 = 4;
                    n2 = n4;
                    if (n != 4) {
                        final int n6 = 5;
                        n2 = n5;
                        if (n != 5) {
                            if (Build$VERSION.SDK_INT < 30 || obj != NetworkType.TEMPORARILY_UNMETERED) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Could not convert ");
                                sb.append(obj);
                                sb.append(" to int");
                                throw new IllegalArgumentException(sb.toString());
                            }
                            n2 = n6;
                        }
                    }
                }
            }
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    public static final int h(final OutOfQuotaPolicy outOfQuotaPolicy) {
        fg0.e((Object)outOfQuotaPolicy, "policy");
        final int n = z92.a.d[outOfQuotaPolicy.ordinal()];
        int n2 = 1;
        if (n != 1) {
            if (n != 2) {
                throw new NoWhenBranchMatchedException();
            }
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    public static final byte[] i(final Set set) {
        fg0.e((Object)set, "triggers");
        if (set.isEmpty()) {
            return new byte[0];
        }
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
            try {
                objectOutputStream.writeInt(set.size());
                for (final zk.c c : set) {
                    objectOutputStream.writeUTF(c.a().toString());
                    objectOutputStream.writeBoolean(c.b());
                }
                final u02 a = u02.a;
                dh.a((Closeable)objectOutputStream, (Throwable)null);
                dh.a((Closeable)out, (Throwable)null);
                final byte[] byteArray = out.toByteArray();
                fg0.d((Object)byteArray, "outputStream.toByteArray()");
                return byteArray;
            }
            finally {
                try {}
                finally {
                    dh.a((Closeable)objectOutputStream, (Throwable)set);
                }
            }
        }
        finally {
            try {}
            finally {
                dh.a((Closeable)out, (Throwable)set);
            }
        }
    }
    
    public static final int j(final WorkInfo$State workInfo$State) {
        fg0.e((Object)workInfo$State, "state");
        int n = 0;
        switch (z92.a.a[workInfo$State.ordinal()]) {
            default: {
                throw new NoWhenBranchMatchedException();
            }
            case 6: {
                n = 5;
                break;
            }
            case 5: {
                n = 4;
                break;
            }
            case 4: {
                n = 3;
                break;
            }
            case 3: {
                n = 2;
                break;
            }
            case 2: {
                n = 1;
                break;
            }
            case 1: {
                n = 0;
                break;
            }
        }
        return n;
    }
}

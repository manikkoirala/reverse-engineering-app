import android.util.Log;
import com.google.android.gms.internal.firebase_auth_api.zzacf;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.recaptcha.RecaptchaAction;
import com.google.android.gms.tasks.Continuation;

// 
// Decompiled by Procyon v0.6.0
// 

public final class nd2 implements Continuation
{
    public final String a;
    public final ud2 b;
    public final RecaptchaAction c;
    public final Continuation d;
    
    public nd2(final String a, final ud2 b, final RecaptchaAction c, final Continuation d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
}

import com.google.firebase.firestore.local.IndexManager;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d51
{
    public static final String a = "d51";
    public static String b = "BUILD_OVERLAYS";
    
    public abstract gd a();
    
    public abstract eu b(final u12 p0);
    
    public abstract IndexManager c(final u12 p0);
    
    public abstract zx0 d(final u12 p0, final IndexManager p1);
    
    public abstract l21 e();
    
    public abstract wc1 f();
    
    public abstract id1 g();
    
    public abstract yt1 h();
    
    public abstract boolean i();
    
    public abstract Object j(final String p0, final hs1 p1);
    
    public abstract void k(final String p0, final Runnable p1);
    
    public abstract void l();
}

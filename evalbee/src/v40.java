import android.util.Property;
import android.util.FloatProperty;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v40
{
    final String mPropertyName;
    
    public v40(final String mPropertyName) {
        this.mPropertyName = mPropertyName;
    }
    
    public static <T> v40 createFloatPropertyCompat(final FloatProperty<T> floatProperty) {
        return new v40(((Property)floatProperty).getName(), floatProperty) {
            public final FloatProperty a;
            
            @Override
            public float getValue(final Object o) {
                return (float)((Property)this.a).get(o);
            }
            
            @Override
            public void setValue(final Object o, final float n) {
                this.a.setValue(o, n);
            }
        };
    }
    
    public abstract float getValue(final Object p0);
    
    public abstract void setValue(final Object p0, final float p1);
}

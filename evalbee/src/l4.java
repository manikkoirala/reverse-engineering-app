import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class l4
{
    public final jr a;
    public volatile m4 b;
    public volatile sc c;
    public final List d;
    
    public l4(final jr jr) {
        this(jr, new ht(), new p02());
    }
    
    public l4(final jr a, final sc c, final m4 b) {
        this.a = a;
        this.c = c;
        this.d = new ArrayList();
        this.b = b;
        this.f();
    }
    
    public static g4.a j(final g4 g4, final sm sm) {
        Object c;
        if ((c = g4.c("clx", (g4.b)sm)) == null) {
            zl0.f().b("Could not register AnalyticsConnectorListener with Crashlytics origin.");
            final g4.a c2 = g4.c("crash", (g4.b)sm);
            if ((c = c2) != null) {
                zl0.f().k("A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version.");
                c = c2;
            }
        }
        return (g4.a)c;
    }
    
    public m4 d() {
        return new j4(this);
    }
    
    public sc e() {
        return new i4(this);
    }
    
    public final void f() {
        this.a.a((jr.a)new k4(this));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class qp implements Comparable
{
    public static final qp c;
    public final String a;
    public final String b;
    
    static {
        c = c("", "");
    }
    
    public qp(final String a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    public static qp c(final String s, final String s2) {
        return new qp(s, s2);
    }
    
    public static qp d(final String s) {
        final ke1 q = ke1.q(s);
        final int l = q.l();
        boolean b2;
        final boolean b = b2 = false;
        if (l > 3) {
            b2 = b;
            if (q.h(0).equals("projects")) {
                b2 = b;
                if (q.h(2).equals("databases")) {
                    b2 = true;
                }
            }
        }
        g9.d(b2, "Tried to parse an invalid resource name: %s", q);
        return new qp(q.h(1), q.h(3));
    }
    
    public int a(final qp qp) {
        int n = this.a.compareTo(qp.a);
        if (n == 0) {
            n = this.b.compareTo(qp.b);
        }
        return n;
    }
    
    public String e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && qp.class == o.getClass()) {
            final qp qp = (qp)o;
            if (!this.a.equals(qp.a) || !this.b.equals(qp.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public String f() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DatabaseId(");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }
}

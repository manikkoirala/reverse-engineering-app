import java.util.Iterator;
import java.util.Collection;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class lh
{
    public static boolean a(final Collection collection, final Collection collection2) {
        final Iterator iterator = collection2.iterator();
        while (iterator.hasNext()) {
            if (!collection.contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public static StringBuilder b(final int n) {
        hh.b(n, "size");
        return new StringBuilder((int)Math.min(n * 8L, 1073741824L));
    }
    
    public static boolean c(final Collection collection, final Object o) {
        i71.r(collection);
        try {
            return collection.contains(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return false;
        }
    }
    
    public static boolean d(final Collection collection, final Object o) {
        i71.r(collection);
        try {
            return collection.remove(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return false;
        }
    }
    
    public static String e(final Collection collection) {
        final StringBuilder b = b(collection.size());
        b.append('[');
        final Iterator iterator = collection.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            if (n == 0) {
                b.append(", ");
            }
            if (next == collection) {
                b.append("(this Collection)");
            }
            else {
                b.append(next);
            }
            n = 0;
        }
        b.append(']');
        return b.toString();
    }
}

import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.ArrayDeque;
import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutionException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledExecutorService;
import java.util.Map;
import com.google.firebase.messaging.FirebaseMessaging;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class cy1
{
    public static final long i;
    public final Context a;
    public final cw0 b;
    public final ib0 c;
    public final FirebaseMessaging d;
    public final Map e;
    public final ScheduledExecutorService f;
    public boolean g;
    public final ay1 h;
    
    static {
        i = TimeUnit.HOURS.toSeconds(8L);
    }
    
    public cy1(final FirebaseMessaging d, final cw0 b, final ay1 h, final ib0 c, final Context a, final ScheduledExecutorService f) {
        this.e = new r8();
        this.g = false;
        this.d = d;
        this.b = b;
        this.h = h;
        this.c = c;
        this.a = a;
        this.f = f;
    }
    
    public static void b(final Task ex) {
        try {
            Tasks.await((Task)ex, 30L, TimeUnit.SECONDS);
        }
        catch (final TimeoutException ex) {
            goto Label_0017;
        }
        catch (final InterruptedException ex2) {}
        catch (final ExecutionException cause) {
            final Throwable cause2 = cause.getCause();
            if (cause2 instanceof IOException) {
                throw (IOException)cause2;
            }
            if (cause2 instanceof RuntimeException) {
                throw (RuntimeException)cause2;
            }
            throw new IOException(cause);
        }
    }
    
    public static Task e(final FirebaseMessaging firebaseMessaging, final cw0 cw0, final ib0 ib0, final Context context, final ScheduledExecutorService scheduledExecutorService) {
        return Tasks.call((Executor)scheduledExecutorService, (Callable)new by1(context, scheduledExecutorService, firebaseMessaging, cw0, ib0));
    }
    
    public static boolean g() {
        return Log.isLoggable("FirebaseMessaging", 3);
    }
    
    public final void c(final String s) {
        b(this.c.k(this.d.i(), s));
    }
    
    public final void d(final String s) {
        b(this.c.l(this.d.i(), s));
    }
    
    public boolean f() {
        return this.h.b() != null;
    }
    
    public boolean h() {
        synchronized (this) {
            return this.g;
        }
    }
    
    public final void j(final jx1 jx1) {
        synchronized (this.e) {
            final String e = jx1.e();
            if (!this.e.containsKey(e)) {
                return;
            }
            final ArrayDeque arrayDeque = this.e.get(e);
            final TaskCompletionSource taskCompletionSource = (TaskCompletionSource)arrayDeque.poll();
            if (taskCompletionSource != null) {
                taskCompletionSource.setResult((Object)null);
            }
            if (arrayDeque.isEmpty()) {
                this.e.remove(e);
            }
        }
    }
    
    public boolean k(final jx1 obj) {
        try {
            final String b = obj.b();
            final int hashCode = b.hashCode();
            int n = 0;
            Label_0061: {
                if (hashCode != 83) {
                    if (hashCode == 85) {
                        if (b.equals("U")) {
                            n = 1;
                            break Label_0061;
                        }
                    }
                }
                else if (b.equals("S")) {
                    n = 0;
                    break Label_0061;
                }
                n = -1;
            }
            String s;
            if (n != 0) {
                if (n != 1) {
                    if (!g()) {
                        return true;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown topic operation");
                    sb.append(obj);
                    sb.append(".");
                    s = sb.toString();
                }
                else {
                    this.d(obj.c());
                    if (!g()) {
                        return true;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unsubscribe from topic: ");
                    sb2.append(obj.c());
                    sb2.append(" succeeded.");
                    s = sb2.toString();
                }
            }
            else {
                this.c(obj.c());
                if (!g()) {
                    return true;
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Subscribe to topic: ");
                sb3.append(obj.c());
                sb3.append(" succeeded.");
                s = sb3.toString();
            }
            Log.d("FirebaseMessaging", s);
            return true;
        }
        catch (final IOException ex) {
            String string;
            if (!"SERVICE_NOT_AVAILABLE".equals(ex.getMessage()) && !"INTERNAL_SERVER_ERROR".equals(ex.getMessage())) {
                if (ex.getMessage() != null) {
                    throw ex;
                }
                string = "Topic operation failed without exception message. Will retry Topic operation.";
            }
            else {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Topic operation failed: ");
                sb4.append(ex.getMessage());
                sb4.append(". Will retry Topic operation.");
                string = sb4.toString();
            }
            Log.e("FirebaseMessaging", string);
            return false;
        }
    }
    
    public void l(final Runnable runnable, final long n) {
        this.f.schedule(runnable, n, TimeUnit.SECONDS);
    }
    
    public void m(final boolean g) {
        synchronized (this) {
            this.g = g;
        }
    }
    
    public final void n() {
        if (!this.h()) {
            this.q(0L);
        }
    }
    
    public void o() {
        if (this.f()) {
            this.n();
        }
    }
    
    public boolean p() {
        while (true) {
            synchronized (this) {
                final jx1 b = this.h.b();
                if (b == null) {
                    if (g()) {
                        Log.d("FirebaseMessaging", "topic sync succeeded");
                    }
                    return true;
                }
                monitorexit(this);
                if (!this.k(b)) {
                    return false;
                }
                this.h.d(b);
                this.j(b);
            }
        }
    }
    
    public void q(final long n) {
        this.l(new dy1(this, this.a, this.b, Math.min(Math.max(30L, 2L * n), cy1.i)), n);
        this.m(true);
    }
}

import android.os.Build$VERSION;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.BackoffPolicy;
import androidx.work.WorkInfo$State;
import androidx.work.b;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import androidx.work.a;
import androidx.work.impl.WorkDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gx
{
    public static final void a(final WorkDatabase workDatabase, final a a, final j82 j82) {
        fg0.e((Object)workDatabase, "workDatabase");
        fg0.e((Object)a, "configuration");
        fg0.e((Object)j82, "continuation");
        final List k = nh.k((Object[])new j82[] { j82 });
        int i = 0;
        while (k.isEmpty() ^ true) {
            final j82 j83 = (j82)sh.t(k);
            final List f = j83.f();
            fg0.d((Object)f, "current.work");
            final Iterable iterable = f;
            int n;
            if (iterable instanceof Collection && ((Collection)iterable).isEmpty()) {
                n = 0;
            }
            else {
                final Iterator iterator = iterable.iterator();
                int n2 = 0;
                while (true) {
                    n = n2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    if (!((n92)iterator.next()).d().j.e()) {
                        continue;
                    }
                    final int n3 = n2 + 1;
                    if ((n2 = n3) >= 0) {
                        continue;
                    }
                    nh.m();
                    n2 = n3;
                }
            }
            final int n4 = i + n;
            final List e = j83.e();
            i = n4;
            if (e != null) {
                k.addAll(e);
                i = n4;
            }
        }
        if (i == 0) {
            return;
        }
        final int n5 = workDatabase.K().n();
        final int b = a.b();
        if (n5 + i <= b) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Too many workers with contentUriTriggers are enqueued:\ncontentUriTrigger workers limit: ");
        sb.append(b);
        sb.append(";\nalready enqueued count: ");
        sb.append(n5);
        sb.append(";\ncurrent enqueue operation count: ");
        sb.append(i);
        sb.append(".\nTo address this issue you can: \n1. enqueue less workers or batch some of workers with content uri triggers together;\n2. increase limit via Configuration.Builder.setContentUriTriggerWorkersLimit;\nPlease beware that workers with content uri triggers immediately occupy slots in JobScheduler so no updates to content uris are missed.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static final p92 b(final p92 p) {
        fg0.e((Object)p, "workSpec");
        final zk j = p.j;
        final String c = p.c;
        p92 e = p;
        if (!fg0.a((Object)c, (Object)ConstraintTrackingWorker.class.getName())) {
            if (!j.f()) {
                e = p;
                if (!j.i()) {
                    return e;
                }
            }
            final b a = new b.a().c(p.e).e("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", c).a();
            fg0.d((Object)a, "Builder().putAll(workSpe\u2026ame)\n            .build()");
            final String name = ConstraintTrackingWorker.class.getName();
            fg0.d((Object)name, "name");
            e = p92.e(p, null, null, name, null, a, null, 0L, 0L, 0L, null, 0, null, 0L, 0L, 0L, 0L, false, null, 0, 0, 0L, 0, 0, 8388587, null);
        }
        return e;
    }
    
    public static final p92 c(final List list, final p92 p2) {
        fg0.e((Object)list, "schedulers");
        fg0.e((Object)p2, "workSpec");
        final boolean b = Build$VERSION.SDK_INT < 26;
        p92 b2 = p2;
        if (b) {
            b2 = b(p2);
        }
        return b2;
    }
}

import java.util.Arrays;
import java.util.Map;
import androidx.datastore.preferences.core.MutablePreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class t71
{
    public static final s71 a() {
        return new MutablePreferences(null, true, 1, null);
    }
    
    public static final MutablePreferences b(final s71.b... original) {
        fg0.e((Object)original, "pairs");
        final MutablePreferences mutablePreferences = new MutablePreferences(null, false, 1, null);
        mutablePreferences.h((s71.b[])Arrays.copyOf(original, original.length));
        return mutablePreferences;
    }
}

import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.GoogleApi;

// 
// Decompiled by Procyon v0.6.0
// 

public class m20 extends l20
{
    public final GoogleApi a;
    public final r91 b;
    public final r10 c;
    
    public m20(final GoogleApi a, final r10 r10, final r91 b) {
        this.a = a;
        this.c = Preconditions.checkNotNull(r10);
        this.b = b;
        if (b.get() == null) {
            Log.w("FDL", "FDL logging failed. Add a dependency for Firebase Analytics to your app to enable logging of Dynamic Link events.");
        }
    }
    
    public m20(final r10 r10, final r91 r11) {
        this(new sv(r10.l()), r10, r11);
    }
}

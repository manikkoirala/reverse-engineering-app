import android.content.pm.PackageManager$NameNotFoundException;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.Intent;
import com.google.android.gms.common.util.PlatformVersion;
import android.util.Log;
import android.content.pm.PackageInfo;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class cw0
{
    public final Context a;
    public String b;
    public String c;
    public int d;
    public int e;
    
    public cw0(final Context a) {
        this.e = 0;
        this.a = a;
    }
    
    public static String c(final r10 r10) {
        final String d = r10.p().d();
        if (d != null) {
            return d;
        }
        final String c = r10.p().c();
        if (!c.startsWith("1:")) {
            return c;
        }
        final String[] split = c.split(":");
        if (split.length < 2) {
            return null;
        }
        final String s = split[1];
        if (s.isEmpty()) {
            return null;
        }
        return s;
    }
    
    public String a() {
        synchronized (this) {
            if (this.b == null) {
                this.h();
            }
            return this.b;
        }
    }
    
    public String b() {
        synchronized (this) {
            if (this.c == null) {
                this.h();
            }
            return this.c;
        }
    }
    
    public int d() {
        synchronized (this) {
            if (this.d == 0) {
                final PackageInfo f = this.f("com.google.android.gms");
                if (f != null) {
                    this.d = f.versionCode;
                }
            }
            return this.d;
        }
    }
    
    public int e() {
        synchronized (this) {
            final int e = this.e;
            if (e != 0) {
                return e;
            }
            final PackageManager packageManager = this.a.getPackageManager();
            if (packageManager.checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1) {
                Log.e("FirebaseMessaging", "Google Play services missing or without correct permission.");
                return 0;
            }
            if (!PlatformVersion.isAtLeastO()) {
                final Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                intent.setPackage("com.google.android.gms");
                final List queryIntentServices = packageManager.queryIntentServices(intent, 0);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    return this.e = 1;
                }
            }
            final Intent intent2 = new Intent("com.google.iid.TOKEN_REQUEST");
            intent2.setPackage("com.google.android.gms");
            final List queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent2, 0);
            if (queryBroadcastReceivers != null && queryBroadcastReceivers.size() > 0) {
                return this.e = 2;
            }
            Log.w("FirebaseMessaging", "Failed to resolve IID implementation package, falling back");
            if (PlatformVersion.isAtLeastO()) {
                this.e = 2;
            }
            else {
                this.e = 1;
            }
            return this.e;
        }
    }
    
    public final PackageInfo f(final String s) {
        try {
            return this.a.getPackageManager().getPackageInfo(s, 0);
        }
        catch (final PackageManager$NameNotFoundException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to find package ");
            sb.append(obj);
            Log.w("FirebaseMessaging", sb.toString());
            return null;
        }
    }
    
    public boolean g() {
        return this.e() != 0;
    }
    
    public final void h() {
        synchronized (this) {
            final PackageInfo f = this.f(this.a.getPackageName());
            if (f != null) {
                this.b = Integer.toString(f.versionCode);
                this.c = f.versionName;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class z22
{
    public int a;
    public double[] b;
    
    public z22() {
        this.a = 0;
        this.b = new double[2];
    }
    
    public z22(final int n) {
        this.a = 0;
        this.b = null;
        this.b = new double[n];
    }
    
    public z22(final double[] b, final int a) {
        this.a = 0;
        this.b = null;
        if (b == null) {
            throw new IllegalArgumentException("value array cannot be null.");
        }
        if (a >= 0 && a <= b.length) {
            this.b = b;
            this.a = a;
            return;
        }
        throw new IllegalArgumentException("size >= 0 && size <= value.length required");
    }
    
    public void a(final double n) {
        this.d(n, this.a);
    }
    
    public void b(int i) {
        final double[] b = this.b;
        if (b.length < i) {
            final int n = b.length * 2;
            if (n >= i) {
                i = n;
            }
            final double[] b2 = new double[i];
            for (i = 0; i < this.a; ++i) {
                b2[i] = this.b[i];
            }
            this.b = b2;
        }
    }
    
    public double c(final int i) {
        if (i >= 0 && i < this.a) {
            return this.b[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("required: (index >= 0 && index < size) but: (index = ");
        sb.append(i);
        sb.append(", size = ");
        sb.append(this.a);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void d(final double n, final int i) {
        if (i >= 0) {
            final int a = this.a;
            if (i <= a) {
                this.b(a + 1);
                for (int j = this.a; j > i; --j) {
                    final double[] b = this.b;
                    b[j] = b[j - 1];
                }
                this.b[i] = n;
                ++this.a;
                return;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("required: (index >= 0 && index <= size) but: (index = ");
        sb.append(i);
        sb.append(", size = ");
        sb.append(this.a);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void e(final double n, final int i) {
        if (i >= 0 && i < this.a) {
            this.b[i] = n;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("required: (index >= 0 && index < size) but: (index = ");
        sb.append(i);
        sb.append(", size = ");
        sb.append(this.a);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public int f() {
        return this.a;
    }
}

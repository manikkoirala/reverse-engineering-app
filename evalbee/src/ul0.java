import java.io.File;

// 
// Decompiled by Procyon v0.6.0
// 

public class ul0
{
    public static final b c;
    public final z00 a;
    public y00 b;
    
    static {
        c = new b(null);
    }
    
    public ul0(final z00 a) {
        this.a = a;
        this.b = ul0.c;
    }
    
    public ul0(final z00 z00, final String s) {
        this(z00);
        this.e(s);
    }
    
    public void a() {
        this.b.b();
    }
    
    public byte[] b() {
        return this.b.a();
    }
    
    public String c() {
        return this.b.e();
    }
    
    public final File d(final String s) {
        return this.a.o(s, "userlog");
    }
    
    public final void e(final String s) {
        this.b.d();
        this.b = ul0.c;
        if (s == null) {
            return;
        }
        this.f(this.d(s), 65536);
    }
    
    public void f(final File file, final int n) {
        this.b = new ma1(file, n);
    }
    
    public void g(final long n, final String s) {
        this.b.c(n, s);
    }
    
    public static final class b implements y00
    {
        @Override
        public byte[] a() {
            return null;
        }
        
        @Override
        public void b() {
        }
        
        @Override
        public void c(final long n, final String s) {
        }
        
        @Override
        public void d() {
        }
        
        @Override
        public String e() {
            return null;
        }
    }
}

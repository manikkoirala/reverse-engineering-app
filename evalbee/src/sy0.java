import org.jetbrains.annotations.Nullable;
import android.net.Network;
import org.jetbrains.annotations.NotNull;
import android.net.ConnectivityManager;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sy0
{
    @Nullable
    public static final Network a(@NotNull final ConnectivityManager connectivityManager) {
        fg0.e((Object)connectivityManager, "<this>");
        return connectivityManager.getActiveNetwork();
    }
}

import java.util.Iterator;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ih0 extends nh0 implements Iterable
{
    public final ArrayList a;
    
    public ih0() {
        this.a = new ArrayList();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ih0 && ((ih0)o).a.equals(this.a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public Iterator iterator() {
        return this.a.iterator();
    }
    
    public void n(final nh0 nh0) {
        nh0 a = nh0;
        if (nh0 == null) {
            a = oh0.a;
        }
        this.a.add(a);
    }
}

import android.widget.Adapter;
import android.view.MotionEvent;
import android.widget.AbsListView;
import android.view.ViewParent;
import android.view.View$OnTouchListener;
import android.view.ViewGroup;
import android.view.View$MeasureSpec;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import android.widget.AbsListView$OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow$OnDismissListener;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.os.Build$VERSION;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.AdapterView$OnItemClickListener;
import android.graphics.drawable.Drawable;
import android.database.DataSetObserver;
import android.view.View;
import android.widget.ListAdapter;
import android.content.Context;
import android.widget.PopupWindow;
import android.graphics.Rect;
import android.os.Handler;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public class ek0 implements wn1
{
    public static Method O;
    public static Method P;
    public final i A;
    public final h C;
    public final g D;
    public final e F;
    public Runnable G;
    public final Handler H;
    public final Rect I;
    public Rect J;
    public boolean K;
    public PopupWindow M;
    public Context a;
    public ListAdapter b;
    public kv c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public boolean i;
    public boolean j;
    public boolean k;
    public int l;
    public boolean m;
    public boolean n;
    public int p;
    public View q;
    public int t;
    public DataSetObserver v;
    public View w;
    public Drawable x;
    public AdapterView$OnItemClickListener y;
    public AdapterView$OnItemSelectedListener z;
    
    static {
        if (Build$VERSION.SDK_INT <= 28) {
            try {
                ek0.O = PopupWindow.class.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
            }
            catch (final NoSuchMethodException ex) {
                Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
            try {
                ek0.P = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
            }
            catch (final NoSuchMethodException ex2) {
                Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
            }
        }
    }
    
    public ek0(final Context context) {
        this(context, null, sa1.D);
    }
    
    public ek0(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    public ek0(final Context a, final AttributeSet set, final int n, final int n2) {
        this.d = -2;
        this.e = -2;
        this.h = 1002;
        this.l = 0;
        this.m = false;
        this.n = false;
        this.p = Integer.MAX_VALUE;
        this.t = 0;
        this.A = new i();
        this.C = new h();
        this.D = new g();
        this.F = new e();
        this.I = new Rect();
        this.a = a;
        this.H = new Handler(a.getMainLooper());
        final TypedArray obtainStyledAttributes = a.obtainStyledAttributes(set, bc1.t1, n, n2);
        this.f = obtainStyledAttributes.getDimensionPixelOffset(bc1.u1, 0);
        final int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(bc1.v1, 0);
        this.g = dimensionPixelOffset;
        if (dimensionPixelOffset != 0) {
            this.i = true;
        }
        obtainStyledAttributes.recycle();
        (this.M = new b7(a, set, n, n2)).setInputMethodMode(1);
    }
    
    public void A(final View w) {
        this.w = w;
    }
    
    public void B(final int animationStyle) {
        this.M.setAnimationStyle(animationStyle);
    }
    
    public void C(final int n) {
        final Drawable background = this.M.getBackground();
        if (background != null) {
            background.getPadding(this.I);
            final Rect i = this.I;
            this.e = i.left + i.right + n;
        }
        else {
            this.O(n);
        }
    }
    
    public void D(final int l) {
        this.l = l;
    }
    
    public void E(Rect j) {
        if (j != null) {
            j = new Rect(j);
        }
        else {
            j = null;
        }
        this.J = j;
    }
    
    public void F(final int inputMethodMode) {
        this.M.setInputMethodMode(inputMethodMode);
    }
    
    public void G(final boolean b) {
        this.K = b;
        this.M.setFocusable(b);
    }
    
    public void H(final PopupWindow$OnDismissListener onDismissListener) {
        this.M.setOnDismissListener(onDismissListener);
    }
    
    public void I(final AdapterView$OnItemClickListener y) {
        this.y = y;
    }
    
    public void J(final AdapterView$OnItemSelectedListener z) {
        this.z = z;
    }
    
    public void K(final boolean j) {
        this.k = true;
        this.j = j;
    }
    
    public final void L(final boolean b) {
        if (Build$VERSION.SDK_INT <= 28) {
            final Method o = ek0.O;
            if (o != null) {
                try {
                    o.invoke(this.M, b);
                }
                catch (final Exception ex) {
                    Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
                }
            }
        }
        else {
            ek0.d.b(this.M, b);
        }
    }
    
    public void M(final int t) {
        this.t = t;
    }
    
    public void N(final int selection) {
        final kv c = this.c;
        if (this.a() && c != null) {
            c.setListSelectionHidden(false);
            ((AdapterView)c).setSelection(selection);
            if (((AbsListView)c).getChoiceMode() != 0) {
                ((AbsListView)c).setItemChecked(selection, true);
            }
        }
    }
    
    public void O(final int e) {
        this.e = e;
    }
    
    @Override
    public boolean a() {
        return this.M.isShowing();
    }
    
    public void c(final int g) {
        this.g = g;
        this.i = true;
    }
    
    @Override
    public void dismiss() {
        this.M.dismiss();
        this.z();
        this.M.setContentView((View)null);
        this.c = null;
        this.H.removeCallbacks((Runnable)this.A);
    }
    
    public int f() {
        if (!this.i) {
            return 0;
        }
        return this.g;
    }
    
    public Drawable getBackground() {
        return this.M.getBackground();
    }
    
    @Override
    public ListView h() {
        return this.c;
    }
    
    public int i() {
        return this.f;
    }
    
    public void j(final int f) {
        this.f = f;
    }
    
    public void m(final ListAdapter b) {
        final DataSetObserver v = this.v;
        if (v == null) {
            this.v = new f();
        }
        else {
            final ListAdapter b2 = this.b;
            if (b2 != null) {
                ((Adapter)b2).unregisterDataSetObserver(v);
            }
        }
        if ((this.b = b) != null) {
            ((Adapter)b).registerDataSetObserver(this.v);
        }
        final kv c = this.c;
        if (c != null) {
            ((AbsListView)c).setAdapter(this.b);
        }
    }
    
    public final int n() {
        final kv c = this.c;
        int n = Integer.MIN_VALUE;
        boolean b = true;
        int n3;
        if (c == null) {
            final Context a = this.a;
            this.G = new Runnable(this) {
                public final ek0 a;
                
                @Override
                public void run() {
                    final View q = this.a.q();
                    if (q != null && q.getWindowToken() != null) {
                        this.a.show();
                    }
                }
            };
            final kv p = this.p(a, this.K ^ true);
            this.c = p;
            final Drawable x = this.x;
            if (x != null) {
                p.setSelector(x);
            }
            ((AbsListView)this.c).setAdapter(this.b);
            ((AdapterView)this.c).setOnItemClickListener(this.y);
            ((View)this.c).setFocusable(true);
            ((View)this.c).setFocusableInTouchMode(true);
            ((AdapterView)this.c).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
                public final ek0 a;
                
                public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                    if (n != -1) {
                        final kv c = this.a.c;
                        if (c != null) {
                            c.setListSelectionHidden(false);
                        }
                    }
                }
                
                public void onNothingSelected(final AdapterView adapterView) {
                }
            });
            ((AbsListView)this.c).setOnScrollListener((AbsListView$OnScrollListener)this.D);
            final AdapterView$OnItemSelectedListener z = this.z;
            if (z != null) {
                ((AdapterView)this.c).setOnItemSelectedListener(z);
            }
            final kv c2 = this.c;
            final View q = this.q;
            Object contentView;
            if (q != null) {
                contentView = new LinearLayout(a);
                ((LinearLayout)contentView).setOrientation(1);
                final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-1, 0, 1.0f);
                final int t = this.t;
                if (t != 0) {
                    if (t != 1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid hint position ");
                        sb.append(this.t);
                        Log.e("ListPopupWindow", sb.toString());
                    }
                    else {
                        ((ViewGroup)contentView).addView((View)c2, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                        ((ViewGroup)contentView).addView(q);
                    }
                }
                else {
                    ((ViewGroup)contentView).addView(q);
                    ((ViewGroup)contentView).addView((View)c2, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                }
                int e = this.e;
                int n2;
                if (e >= 0) {
                    n2 = Integer.MIN_VALUE;
                }
                else {
                    e = 0;
                    n2 = 0;
                }
                q.measure(View$MeasureSpec.makeMeasureSpec(e, n2), 0);
                final LinearLayout$LayoutParams linearLayout$LayoutParams2 = (LinearLayout$LayoutParams)q.getLayoutParams();
                n3 = q.getMeasuredHeight() + linearLayout$LayoutParams2.topMargin + linearLayout$LayoutParams2.bottomMargin;
            }
            else {
                n3 = 0;
                contentView = c2;
            }
            this.M.setContentView((View)contentView);
        }
        else {
            final ViewGroup viewGroup = (ViewGroup)this.M.getContentView();
            final View q2 = this.q;
            if (q2 != null) {
                final LinearLayout$LayoutParams linearLayout$LayoutParams3 = (LinearLayout$LayoutParams)q2.getLayoutParams();
                n3 = q2.getMeasuredHeight() + linearLayout$LayoutParams3.topMargin + linearLayout$LayoutParams3.bottomMargin;
            }
            else {
                n3 = 0;
            }
        }
        final Drawable background = this.M.getBackground();
        int n5;
        if (background != null) {
            background.getPadding(this.I);
            final Rect i = this.I;
            final int top = i.top;
            final int n4 = n5 = i.bottom + top;
            if (!this.i) {
                this.g = -top;
                n5 = n4;
            }
        }
        else {
            this.I.setEmpty();
            n5 = 0;
        }
        if (this.M.getInputMethodMode() != 2) {
            b = false;
        }
        final int r = this.r(this.q(), this.g, b);
        if (!this.m && this.d != -1) {
            final int e2 = this.e;
            int n6 = 0;
            Label_0636: {
                if (e2 != -2) {
                    n = 1073741824;
                    if (e2 != -1) {
                        n6 = View$MeasureSpec.makeMeasureSpec(e2, 1073741824);
                        break Label_0636;
                    }
                }
                final int widthPixels = this.a.getResources().getDisplayMetrics().widthPixels;
                final Rect j = this.I;
                n6 = View$MeasureSpec.makeMeasureSpec(widthPixels - (j.left + j.right), n);
            }
            final int d = this.c.d(n6, 0, -1, r - n3, -1);
            int n7 = n3;
            if (d > 0) {
                n7 = n3 + (n5 + (((View)this.c).getPaddingTop() + ((View)this.c).getPaddingBottom()));
            }
            return d + n7;
        }
        return r + n5;
    }
    
    public void o() {
        final kv c = this.c;
        if (c != null) {
            c.setListSelectionHidden(true);
            ((View)c).requestLayout();
        }
    }
    
    public kv p(final Context context, final boolean b) {
        return new kv(context, b);
    }
    
    public View q() {
        return this.w;
    }
    
    public final int r(final View view, final int n, final boolean b) {
        return ek0.c.a(this.M, view, n, b);
    }
    
    public Object s() {
        if (!this.a()) {
            return null;
        }
        return ((AdapterView)this.c).getSelectedItem();
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        this.M.setBackgroundDrawable(backgroundDrawable);
    }
    
    @Override
    public void show() {
        int n = this.n();
        final boolean x = this.x();
        c61.b(this.M, this.h);
        final boolean showing = this.M.isShowing();
        boolean outsideTouchable = true;
        if (showing) {
            if (!o32.T(this.q())) {
                return;
            }
            final int e = this.e;
            int width;
            if (e == -1) {
                width = -1;
            }
            else if ((width = e) == -2) {
                width = this.q().getWidth();
            }
            final int d = this.d;
            if (d == -1) {
                if (!x) {
                    n = -1;
                }
                if (x) {
                    final PopupWindow m = this.M;
                    int width2;
                    if (this.e == -1) {
                        width2 = -1;
                    }
                    else {
                        width2 = 0;
                    }
                    m.setWidth(width2);
                    this.M.setHeight(0);
                }
                else {
                    final PopupWindow i = this.M;
                    int width3;
                    if (this.e == -1) {
                        width3 = -1;
                    }
                    else {
                        width3 = 0;
                    }
                    i.setWidth(width3);
                    this.M.setHeight(-1);
                }
            }
            else if (d != -2) {
                n = d;
            }
            final PopupWindow j = this.M;
            if (this.n || this.m) {
                outsideTouchable = false;
            }
            j.setOutsideTouchable(outsideTouchable);
            final PopupWindow k = this.M;
            final View q = this.q();
            final int f = this.f;
            final int g = this.g;
            if (width < 0) {
                width = -1;
            }
            if (n < 0) {
                n = -1;
            }
            k.update(q, f, g, width, n);
        }
        else {
            final int e2 = this.e;
            int width4;
            if (e2 == -1) {
                width4 = -1;
            }
            else if ((width4 = e2) == -2) {
                width4 = this.q().getWidth();
            }
            final int d2 = this.d;
            if (d2 == -1) {
                n = -1;
            }
            else if (d2 != -2) {
                n = d2;
            }
            this.M.setWidth(width4);
            this.M.setHeight(n);
            this.L(true);
            this.M.setOutsideTouchable(!this.n && !this.m);
            this.M.setTouchInterceptor((View$OnTouchListener)this.C);
            if (this.k) {
                c61.a(this.M, this.j);
            }
            if (Build$VERSION.SDK_INT <= 28) {
                final Method p = ek0.P;
                if (p != null) {
                    try {
                        p.invoke(this.M, this.J);
                    }
                    catch (final Exception ex) {
                        Log.e("ListPopupWindow", "Could not invoke setEpicenterBounds on PopupWindow", (Throwable)ex);
                    }
                }
            }
            else {
                ek0.d.a(this.M, this.J);
            }
            c61.c(this.M, this.q(), this.f, this.g, this.l);
            ((AdapterView)this.c).setSelection(-1);
            if (!this.K || this.c.isInTouchMode()) {
                this.o();
            }
            if (!this.K) {
                this.H.post((Runnable)this.F);
            }
        }
    }
    
    public long t() {
        if (!this.a()) {
            return Long.MIN_VALUE;
        }
        return ((AdapterView)this.c).getSelectedItemId();
    }
    
    public int u() {
        if (!this.a()) {
            return -1;
        }
        return ((AdapterView)this.c).getSelectedItemPosition();
    }
    
    public View v() {
        if (!this.a()) {
            return null;
        }
        return ((AdapterView)this.c).getSelectedView();
    }
    
    public int w() {
        return this.e;
    }
    
    public boolean x() {
        return this.M.getInputMethodMode() == 2;
    }
    
    public boolean y() {
        return this.K;
    }
    
    public final void z() {
        final View q = this.q;
        if (q != null) {
            final ViewParent parent = q.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup)parent).removeView(this.q);
            }
        }
    }
    
    public abstract static class c
    {
        public static int a(final PopupWindow popupWindow, final View view, final int n, final boolean b) {
            return popupWindow.getMaxAvailableHeight(view, n, b);
        }
    }
    
    public abstract static class d
    {
        public static void a(final PopupWindow popupWindow, final Rect epicenterBounds) {
            popupWindow.setEpicenterBounds(epicenterBounds);
        }
        
        public static void b(final PopupWindow popupWindow, final boolean isClippedToScreen) {
            popupWindow.setIsClippedToScreen(isClippedToScreen);
        }
    }
    
    public class e implements Runnable
    {
        public final ek0 a;
        
        public e(final ek0 a) {
            this.a = a;
        }
        
        @Override
        public void run() {
            this.a.o();
        }
    }
    
    public class f extends DataSetObserver
    {
        public final ek0 a;
        
        public f(final ek0 a) {
            this.a = a;
        }
        
        public void onChanged() {
            if (this.a.a()) {
                this.a.show();
            }
        }
        
        public void onInvalidated() {
            this.a.dismiss();
        }
    }
    
    public class g implements AbsListView$OnScrollListener
    {
        public final ek0 a;
        
        public g(final ek0 a) {
            this.a = a;
        }
        
        public void onScroll(final AbsListView absListView, final int n, final int n2, final int n3) {
        }
        
        public void onScrollStateChanged(final AbsListView absListView, final int n) {
            if (n == 1 && !this.a.x() && this.a.M.getContentView() != null) {
                final ek0 a = this.a;
                a.H.removeCallbacks((Runnable)a.A);
                this.a.A.run();
            }
        }
    }
    
    public class h implements View$OnTouchListener
    {
        public final ek0 a;
        
        public h(final ek0 a) {
            this.a = a;
        }
        
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            final int action = motionEvent.getAction();
            final int n = (int)motionEvent.getX();
            final int n2 = (int)motionEvent.getY();
            if (action == 0) {
                final PopupWindow m = this.a.M;
                if (m != null && m.isShowing() && n >= 0 && n < this.a.M.getWidth() && n2 >= 0 && n2 < this.a.M.getHeight()) {
                    final ek0 a = this.a;
                    a.H.postDelayed((Runnable)a.A, 250L);
                    return false;
                }
            }
            if (action == 1) {
                final ek0 a2 = this.a;
                a2.H.removeCallbacks((Runnable)a2.A);
            }
            return false;
        }
    }
    
    public class i implements Runnable
    {
        public final ek0 a;
        
        public i(final ek0 a) {
            this.a = a;
        }
        
        @Override
        public void run() {
            final kv c = this.a.c;
            if (c != null && o32.T((View)c) && ((AdapterView)this.a.c).getCount() > ((ViewGroup)this.a.c).getChildCount()) {
                final int childCount = ((ViewGroup)this.a.c).getChildCount();
                final ek0 a = this.a;
                if (childCount <= a.p) {
                    a.M.setInputMethodMode(2);
                    this.a.show();
                }
            }
        }
    }
}

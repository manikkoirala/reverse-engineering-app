import android.util.SizeF;
import org.jetbrains.annotations.Nullable;
import android.util.Size;
import org.jetbrains.annotations.NotNull;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public final class fd
{
    public static final fd a;
    
    static {
        a = new fd();
    }
    
    public static final void a(@NotNull final Bundle bundle, @NotNull final String s, @Nullable final Size size) {
        fg0.e((Object)bundle, "bundle");
        fg0.e((Object)s, "key");
        bundle.putSize(s, size);
    }
    
    public static final void b(@NotNull final Bundle bundle, @NotNull final String s, @Nullable final SizeF sizeF) {
        fg0.e((Object)bundle, "bundle");
        fg0.e((Object)s, "key");
        bundle.putSizeF(s, sizeF);
    }
}

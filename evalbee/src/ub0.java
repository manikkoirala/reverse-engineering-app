import com.graphbuilder.math.ExpressionParseException;
import com.graphbuilder.curve.ControlStringParseException;

// 
// Decompiled by Procyon v0.6.0
// 

public class ub0
{
    public String a;
    public int[] b;
    public int c;
    public int d;
    
    public ub0(final String a, final int n) {
        this.a = null;
        this.b = null;
        this.c = 0;
        this.d = 0;
        if (a != null) {
            this.b = d(a, n);
            this.a = a;
            return;
        }
        throw new IllegalArgumentException("control string cannot be null");
    }
    
    public static int[] d(final String s, final int n) {
        final int length = s.length();
        int n2 = 1;
        final int n3 = 0;
        int i = 0;
        int n4 = 0;
        while (i < length) {
            final char char1 = s.charAt(i);
            int n5;
            int n6;
            if (char1 == ',' && n4 == 0) {
                n5 = n2 + 1;
                n6 = n4;
            }
            else if (char1 == '(') {
                n6 = n4 + 1;
                n5 = n2;
            }
            else {
                n5 = n2;
                n6 = n4;
                if (char1 == ')') {
                    n6 = n4 - 1;
                    n5 = n2;
                }
            }
            ++i;
            n2 = n5;
            n4 = n6;
        }
        if (n4 == 0) {
            final int[] array = new int[n2 * 2];
            final b32 b32 = new b32();
            final y80 y80 = new y80();
            y80.b();
            int n7 = 0;
            int n8 = -1;
            int n9 = 0;
            int n10 = n4;
            int n12;
            int n13;
            for (int j = n3; j <= length; ++j, n9 = n12, n10 = n13) {
                int char2;
                if (j < length) {
                    char2 = s.charAt(j);
                }
                else {
                    char2 = 32;
                }
                if (j != length && (char2 != 44 || n10 != 0)) {
                    int n11;
                    if (char2 == 40) {
                        n11 = n10 + 1;
                    }
                    else if (char2 == 41) {
                        n11 = n10 - 1;
                    }
                    else {
                        n11 = n10;
                        if (char2 == 58) {
                            n8 = j;
                            n11 = n10;
                        }
                    }
                    n12 = n9;
                    n13 = n11;
                }
                else {
                    int n14;
                    if (n8 == -1) {
                        array[n7 + 1] = (array[n7] = (int)Math.round(f(s, b32, n, n9, j).b(b32, y80)));
                        n14 = n7 + 2;
                    }
                    else {
                        final oz f = f(s, b32, n, n9, n8);
                        final int n15 = n7 + 1;
                        array[n7] = (int)Math.round(f.b(b32, y80));
                        final oz f2 = f(s, b32, n, n8 + 1, j);
                        n14 = n15 + 1;
                        array[n15] = (int)Math.round(f2.b(b32, y80));
                    }
                    final int n16 = j + 1;
                    n8 = -1;
                    n13 = n10;
                    n7 = n14;
                    n12 = n16;
                }
            }
            return array;
        }
        throw new ControlStringParseException("round brackets do not balance");
    }
    
    public static oz f(final String s, final b32 b32, final int n, final int beginIndex, final int endIndex) {
        try {
            final oz c = pz.c(s.substring(beginIndex, endIndex));
            if (c == null) {
                throw new ControlStringParseException("control substring is empty", beginIndex, endIndex);
            }
            final String[] e = c.e();
            if (e.length <= 1) {
                if (e.length == 1) {
                    b32.b(e[0], n);
                }
                return c;
            }
            throw new ControlStringParseException("too many variables", beginIndex, endIndex);
        }
        catch (final ExpressionParseException ex) {
            throw new ControlStringParseException("error parsing expression", beginIndex, endIndex, ex);
        }
    }
    
    public int a() {
        int n = 0;
        int n2 = 0;
        while (true) {
            final int[] b = this.b;
            if (n >= b.length) {
                break;
            }
            final int n3 = b[n] - b[n + 1];
            int n4;
            if ((n4 = n3) < 0) {
                n4 = -n3;
            }
            n2 += n4 + 1;
            n += 2;
        }
        return n2;
    }
    
    public boolean b(final int n, final int n2) {
        int n3 = 0;
        while (true) {
            final int[] b = this.b;
            if (n3 >= b.length) {
                return true;
            }
            final int n4 = b[n3];
            if (n4 < n || n4 >= n2) {
                return false;
            }
            ++n3;
        }
    }
    
    public int c() {
        final int[] b = this.b;
        final int c = this.c;
        final int n = b[c];
        final int n2 = b[c + 1];
        final int d = this.d;
        int n4 = 0;
        Label_0066: {
            if (n <= n2) {
                final int n3 = n + d;
                if ((n4 = n3) < n2) {
                    break Label_0066;
                }
                n4 = n3;
            }
            else {
                final int n5 = n - d;
                if ((n4 = n5) > n2) {
                    break Label_0066;
                }
                n4 = n5;
            }
            this.d = 0;
            this.c = c + 2;
            return n4;
        }
        this.d = d + 1;
        return n4;
    }
    
    public void e(final int c, final int d) {
        if (c < 0) {
            throw new IllegalArgumentException("index_i >= 0 required");
        }
        if (c % 2 == 1) {
            throw new IllegalArgumentException("index_i must be an even number");
        }
        if (d >= 0) {
            this.c = c;
            this.d = d;
            return;
        }
        throw new IllegalArgumentException("count_j >= 0 required");
    }
}

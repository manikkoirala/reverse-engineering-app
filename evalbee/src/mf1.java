import android.view.ViewGroup;
import android.view.ViewGroup$MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.TextView;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.os.Bundle;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.widget.Spinner;
import android.widget.EditText;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class mf1 extends u80
{
    public Context a;
    public String b;
    public d c;
    public String[] d;
    public EditText[] e;
    public int f;
    public int g;
    public Spinner h;
    
    public mf1(final Context a, final String b, final d c, final String[] d, final int g, final int f) {
        super(a);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.f = f;
        this.g = g;
    }
    
    public static /* synthetic */ Spinner a(final mf1 mf1) {
        return mf1.h;
    }
    
    public final void b() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297356);
        toolbar.setTitle(this.b);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final mf1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492985);
        this.b();
        (this.h = (Spinner)this.findViewById(2131297092)).setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])new String[] { "0, 1...8, 9", "1, 2...9, 0" }));
        ((AdapterView)this.h).setSelection(this.g);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296794);
        this.e = new EditText[this.d.length];
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, -2);
        final int d = a91.d(16, this.a);
        int i = 0;
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(0, 0, d, 0);
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(-2, a91.d(40, this.a));
        while (i < this.e.length) {
            final LinearLayout b = yo.b(this.a);
            final TextView c = yo.c(this.a, this.d[i], 50);
            this.e[i] = yo.a(this.a, this.d[i], this.f, 80);
            ((ViewGroup)b).addView((View)c, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            ((ViewGroup)b).addView((View)this.e[i], (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            ((ViewGroup)linearLayout).addView((View)b);
            ++i;
        }
        this.findViewById(2131296434).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final mf1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296468).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final mf1 a;
            
            public void onClick(final View view) {
                final String[] array = new String[this.a.e.length];
                int n = 0;
                while (true) {
                    final mf1 a = this.a;
                    final EditText[] e = a.e;
                    if (n >= e.length) {
                        final d c = a.c;
                        if (c != null) {
                            c.a(((AdapterView)mf1.a(a)).getSelectedItemPosition(), array);
                        }
                        this.a.dismiss();
                        return;
                    }
                    final String string = e[n].getText().toString();
                    array[n] = string;
                    if (string.trim().length() < 1) {
                        a91.G(this.a.a, 2131886261, 2131230909, 2131231086);
                        return;
                    }
                    ++n;
                }
            }
        });
    }
    
    public interface d
    {
        void a(final int p0, final String[] p1);
    }
}

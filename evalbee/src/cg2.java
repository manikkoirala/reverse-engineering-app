import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase-auth-api.zzagt;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class cg2
{
    public static zzagt a(final v9 v9, final String s) {
        Preconditions.checkNotNull(v9);
        if (lb0.class.isAssignableFrom(v9.getClass())) {
            return lb0.J((lb0)v9, s);
        }
        if (vz.class.isAssignableFrom(v9.getClass())) {
            return vz.J((vz)v9, s);
        }
        if (gz1.class.isAssignableFrom(v9.getClass())) {
            return gz1.J((gz1)v9, s);
        }
        if (db0.class.isAssignableFrom(v9.getClass())) {
            return db0.J((db0)v9, s);
        }
        if (p51.class.isAssignableFrom(v9.getClass())) {
            return p51.J((p51)v9, s);
        }
        if (lf2.class.isAssignableFrom(v9.getClass())) {
            return lf2.K((lf2)v9, s);
        }
        throw new IllegalArgumentException("Unsupported credential type.");
    }
}

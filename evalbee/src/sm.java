import android.os.BaseBundle;
import java.util.Locale;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public class sm implements b
{
    public n4 a;
    public n4 b;
    
    public static void b(final n4 n4, final String s, final Bundle bundle) {
        if (n4 == null) {
            return;
        }
        n4.b(s, bundle);
    }
    
    @Override
    public void a(final int i, Bundle bundle) {
        zl0.f().i(String.format(Locale.US, "Analytics listener received message. ID: %d, Extras: %s", i, bundle));
        if (bundle == null) {
            return;
        }
        final String string = ((BaseBundle)bundle).getString("name");
        if (string != null) {
            if ((bundle = bundle.getBundle("params")) == null) {
                bundle = new Bundle();
            }
            this.c(string, bundle);
        }
    }
    
    public final void c(final String s, final Bundle bundle) {
        n4 n4;
        if ("clx".equals(((BaseBundle)bundle).getString("_o"))) {
            n4 = this.a;
        }
        else {
            n4 = this.b;
        }
        b(n4, s, bundle);
    }
    
    public void d(final n4 b) {
        this.b = b;
    }
    
    public void e(final n4 a) {
        this.a = a;
    }
}

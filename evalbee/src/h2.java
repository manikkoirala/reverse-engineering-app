import android.content.Context;
import android.app.SharedElementCallback$OnSharedElementsReadyListener;
import android.content.IntentSender;
import android.os.Bundle;
import android.content.Intent;
import android.app.SharedElementCallback;
import java.util.Arrays;
import android.text.TextUtils;
import java.util.HashSet;
import android.os.Handler;
import android.os.Build$VERSION;
import android.app.Activity;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class h2 extends sl
{
    public static void b(final Activity activity) {
        a.a(activity);
    }
    
    public static void c(final Activity activity) {
        b.a(activity);
    }
    
    public static void e(final Activity activity) {
        b.b(activity);
    }
    
    public static void f(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 28) {
            activity.recreate();
        }
        else {
            new Handler(((Context)activity).getMainLooper()).post((Runnable)new g2(activity));
        }
    }
    
    public static void g(final Activity activity, final String[] a, final int n) {
        final HashSet set = new HashSet();
        final int n2 = 0;
        for (int i = 0; i < a.length; ++i) {
            if (TextUtils.isEmpty((CharSequence)a[i])) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Permission request for permissions ");
                sb.append(Arrays.toString(a));
                sb.append(" must not contain null or empty values");
                throw new IllegalArgumentException(sb.toString());
            }
            if (!yc.c() && TextUtils.equals((CharSequence)a[i], (CharSequence)"android.permission.POST_NOTIFICATIONS")) {
                set.add(i);
            }
        }
        final int size = set.size();
        String[] array;
        if (size > 0) {
            array = new String[a.length - size];
        }
        else {
            array = a;
        }
        if (size > 0) {
            if (size == a.length) {
                return;
            }
            int n3 = 0;
            int n4;
            for (int j = n2; j < a.length; ++j, n3 = n4) {
                n4 = n3;
                if (!set.contains(j)) {
                    array[n3] = a[j];
                    n4 = n3 + 1;
                }
            }
        }
        if (activity instanceof d) {
            ((d)activity).validateRequestPermissionsRequestCode(n);
        }
        c.b(activity, a, n);
    }
    
    public static void h(final Activity activity, final in1 in1) {
        b.c(activity, null);
    }
    
    public static void i(final Activity activity, final in1 in1) {
        b.d(activity, null);
    }
    
    public static boolean j(final Activity activity, final String s) {
        return (yc.c() || !TextUtils.equals((CharSequence)"android.permission.POST_NOTIFICATIONS", (CharSequence)s)) && c.c(activity, s);
    }
    
    public static void k(final Activity activity, final Intent intent, final int n, final Bundle bundle) {
        a.b(activity, intent, n, bundle);
    }
    
    public static void l(final Activity activity, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) {
        a.c(activity, intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    public static void m(final Activity activity) {
        b.e(activity);
    }
    
    public abstract static class a
    {
        public static void a(final Activity activity) {
            activity.finishAffinity();
        }
        
        public static void b(final Activity activity, final Intent intent, final int n, final Bundle bundle) {
            activity.startActivityForResult(intent, n, bundle);
        }
        
        public static void c(final Activity activity, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) {
            activity.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
        }
    }
    
    public abstract static class b
    {
        public static void a(final Activity activity) {
            activity.finishAfterTransition();
        }
        
        public static void b(final Activity activity) {
            activity.postponeEnterTransition();
        }
        
        public static void c(final Activity activity, final SharedElementCallback enterSharedElementCallback) {
            activity.setEnterSharedElementCallback(enterSharedElementCallback);
        }
        
        public static void d(final Activity activity, final SharedElementCallback exitSharedElementCallback) {
            activity.setExitSharedElementCallback(exitSharedElementCallback);
        }
        
        public static void e(final Activity activity) {
            activity.startPostponedEnterTransition();
        }
    }
    
    public abstract static class c
    {
        public static void a(final Object o) {
            ((SharedElementCallback$OnSharedElementsReadyListener)o).onSharedElementsReady();
        }
        
        public static void b(final Activity activity, final String[] array, final int n) {
            activity.requestPermissions(array, n);
        }
        
        public static boolean c(final Activity activity, final String s) {
            return activity.shouldShowRequestPermissionRationale(s);
        }
    }
    
    public interface d
    {
        void validateRequestPermissionsRequestCode(final int p0);
    }
}

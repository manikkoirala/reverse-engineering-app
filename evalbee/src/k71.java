import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class k71
{
    public static void a(final boolean b, final String format, final Object... args) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.format(format, args));
    }
    
    public static Object b(final Object o) {
        o.getClass();
        return o;
    }
    
    public static Object c(final Object o, final Object obj) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(String.valueOf(obj));
    }
    
    public static Object d(final Object o, final String format, final Object... args) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(String.format(Locale.US, format, args));
    }
}

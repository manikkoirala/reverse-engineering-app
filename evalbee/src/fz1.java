import java.util.Calendar;
import android.util.Log;
import android.location.Location;
import android.location.LocationManager;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class fz1
{
    public static fz1 d;
    public final Context a;
    public final LocationManager b;
    public final a c;
    
    public fz1(final Context a, final LocationManager b) {
        this.c = new a();
        this.a = a;
        this.b = b;
    }
    
    public static fz1 a(Context applicationContext) {
        if (fz1.d == null) {
            applicationContext = applicationContext.getApplicationContext();
            fz1.d = new fz1(applicationContext, (LocationManager)applicationContext.getSystemService("location"));
        }
        return fz1.d;
    }
    
    public final Location b() {
        final int b = c51.b(this.a, "android.permission.ACCESS_COARSE_LOCATION");
        Location c = null;
        Location c2;
        if (b == 0) {
            c2 = this.c("network");
        }
        else {
            c2 = null;
        }
        if (c51.b(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            c = this.c("gps");
        }
        if (c != null && c2 != null) {
            Location location = c2;
            if (c.getTime() > c2.getTime()) {
                location = c;
            }
            return location;
        }
        if (c != null) {
            c2 = c;
        }
        return c2;
    }
    
    public final Location c(final String s) {
        try {
            if (this.b.isProviderEnabled(s)) {
                return this.b.getLastKnownLocation(s);
            }
        }
        catch (final Exception ex) {
            Log.d("TwilightManager", "Failed to get last known location", (Throwable)ex);
        }
        return null;
    }
    
    public boolean d() {
        final a c = this.c;
        if (this.e()) {
            return c.a;
        }
        final Location b = this.b();
        if (b != null) {
            this.f(b);
            return c.a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        final int value = Calendar.getInstance().get(11);
        return value < 6 || value >= 22;
    }
    
    public final boolean e() {
        return this.c.b > System.currentTimeMillis();
    }
    
    public final void f(final Location location) {
        final a c = this.c;
        final long currentTimeMillis = System.currentTimeMillis();
        final ez1 b = ez1.b();
        b.a(currentTimeMillis - 86400000L, location.getLatitude(), location.getLongitude());
        b.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        final int c2 = b.c;
        boolean a = true;
        if (c2 != 1) {
            a = false;
        }
        final long b2 = b.b;
        final long a2 = b.a;
        b.a(currentTimeMillis + 86400000L, location.getLatitude(), location.getLongitude());
        final long b3 = b.b;
        long b4;
        if (b2 != -1L && a2 != -1L) {
            long n;
            if (currentTimeMillis > a2) {
                n = b3 + 0L;
            }
            else if (currentTimeMillis > b2) {
                n = a2 + 0L;
            }
            else {
                n = b2 + 0L;
            }
            b4 = n + 60000L;
        }
        else {
            b4 = 43200000L + currentTimeMillis;
        }
        c.a = a;
        c.b = b4;
    }
    
    public static class a
    {
        public boolean a;
        public long b;
    }
}

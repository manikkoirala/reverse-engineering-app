import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import android.os.Build$VERSION;
import android.os.Looper;
import android.content.res.Configuration;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.res.Resources$NotFoundException;
import android.content.res.XmlResourceParser;
import android.util.Log;
import org.xmlpull.v1.XmlPullParser;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.os.Handler;
import android.util.TypedValue;
import android.graphics.Typeface;
import android.content.Context;
import android.util.SparseArray;
import android.content.res.Resources$Theme;
import android.content.res.ColorStateList;
import java.util.WeakHashMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class le1
{
    public static final ThreadLocal a;
    public static final WeakHashMap b;
    public static final Object c;
    
    static {
        a = new ThreadLocal();
        b = new WeakHashMap(0);
        c = new Object();
    }
    
    public static void a(final d d, final int n, final ColorStateList list, final Resources$Theme resources$Theme) {
        synchronized (le1.c) {
            final WeakHashMap b = le1.b;
            SparseArray value;
            if ((value = b.get(d)) == null) {
                value = new SparseArray();
                b.put(d, value);
            }
            value.append(n, (Object)new c(list, d.a.getConfiguration(), resources$Theme));
        }
    }
    
    public static ColorStateList b(final d key, final int n) {
        synchronized (le1.c) {
            final SparseArray sparseArray = le1.b.get(key);
            if (sparseArray != null && sparseArray.size() > 0) {
                final c c = (c)sparseArray.get(n);
                if (c != null) {
                    if (c.b.equals(key.a.getConfiguration())) {
                        final Resources$Theme b = key.b;
                        if ((b == null && c.c == 0) || (b != null && c.c == b.hashCode())) {
                            return c.a;
                        }
                    }
                    sparseArray.remove(n);
                }
            }
            return null;
        }
    }
    
    public static Typeface c(final Context context, final int n) {
        if (context.isRestricted()) {
            return null;
        }
        return m(context, n, new TypedValue(), 0, null, null, false, true);
    }
    
    public static ColorStateList d(final Resources resources, final int n, final Resources$Theme resources$Theme) {
        final d d = new d(resources, resources$Theme);
        final ColorStateList b = b(d, n);
        if (b != null) {
            return b;
        }
        final ColorStateList k = k(resources, n, resources$Theme);
        if (k != null) {
            a(d, n, k, resources$Theme);
            return k;
        }
        return le1.b.b(resources, n, resources$Theme);
    }
    
    public static Drawable e(final Resources resources, final int n, final Resources$Theme resources$Theme) {
        return le1.a.a(resources, n, resources$Theme);
    }
    
    public static Drawable f(final Resources resources, final int n, final int n2, final Resources$Theme resources$Theme) {
        return le1.a.b(resources, n, n2, resources$Theme);
    }
    
    public static Typeface g(final Context context, final int n) {
        if (context.isRestricted()) {
            return null;
        }
        return m(context, n, new TypedValue(), 0, null, null, false, false);
    }
    
    public static Typeface h(final Context context, final int n, final TypedValue typedValue, final int n2, final e e) {
        if (context.isRestricted()) {
            return null;
        }
        return m(context, n, typedValue, n2, e, null, true, false);
    }
    
    public static void i(final Context context, final int n, final e e, final Handler handler) {
        l71.g(e);
        if (context.isRestricted()) {
            e.callbackFailAsync(-4, handler);
            return;
        }
        m(context, n, new TypedValue(), 0, e, handler, false, false);
    }
    
    public static TypedValue j() {
        final ThreadLocal a = le1.a;
        TypedValue value;
        if ((value = a.get()) == null) {
            value = new TypedValue();
            a.set(value);
        }
        return value;
    }
    
    public static ColorStateList k(final Resources resources, final int n, final Resources$Theme resources$Theme) {
        if (l(resources, n)) {
            return null;
        }
        final XmlResourceParser xml = resources.getXml(n);
        try {
            return bi.a(resources, (XmlPullParser)xml, resources$Theme);
        }
        catch (final Exception ex) {
            Log.w("ResourcesCompat", "Failed to inflate ColorStateList, leaving it to the framework", (Throwable)ex);
            return null;
        }
    }
    
    public static boolean l(final Resources resources, int type) {
        final TypedValue j = j();
        boolean b = true;
        resources.getValue(type, j, true);
        type = j.type;
        if (type < 28 || type > 31) {
            b = false;
        }
        return b;
    }
    
    public static Typeface m(final Context context, final int i, final TypedValue typedValue, final int n, final e e, final Handler handler, final boolean b, final boolean b2) {
        final Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        final Typeface n2 = n(context, resources, typedValue, i, n, e, handler, b, b2);
        if (n2 == null && e == null && !b2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Font resource ID #0x");
            sb.append(Integer.toHexString(i));
            sb.append(" could not be retrieved.");
            throw new Resources$NotFoundException(sb.toString());
        }
        return n2;
    }
    
    public static Typeface n(final Context context, final Resources resources, final TypedValue obj, final int i, final int n, final e e, final Handler handler, final boolean b, final boolean b2) {
        final CharSequence string = obj.string;
        if (string == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Resource \"");
            sb.append(resources.getResourceName(i));
            sb.append("\" (");
            sb.append(Integer.toHexString(i));
            sb.append(") is not a Font: ");
            sb.append(obj);
            throw new Resources$NotFoundException(sb.toString());
        }
        final String string2 = string.toString();
        if (!string2.startsWith("res/")) {
            if (e != null) {
                e.callbackFailAsync(-3, handler);
            }
            return null;
        }
        final Typeface f = sz1.f(resources, i, string2, obj.assetCookie, n);
        if (f != null) {
            if (e != null) {
                e.callbackSuccessAsync(f, handler);
            }
            return f;
        }
        if (b2) {
            return null;
        }
        StringBuilder sb2;
        String str;
        try {
            if (!string2.toLowerCase().endsWith(".xml")) {
                final Typeface d = sz1.d(context, resources, i, string2, obj.assetCookie, n);
                if (e != null) {
                    if (d != null) {
                        e.callbackSuccessAsync(d, handler);
                    }
                    else {
                        e.callbackFailAsync(-3, handler);
                    }
                }
                return d;
            }
            final j70.b b3 = j70.b((XmlPullParser)resources.getXml(i), resources);
            if (b3 == null) {
                Log.e("ResourcesCompat", "Failed to find font-family tag");
                if (e != null) {
                    e.callbackFailAsync(-3, handler);
                }
                return null;
            }
            return sz1.c(context, b3, resources, i, string2, obj.assetCookie, n, e, handler, b);
        }
        catch (final IOException ex) {
            sb2 = new StringBuilder();
            str = "Failed to read xml resource ";
        }
        catch (final XmlPullParserException ex) {
            sb2 = new StringBuilder();
            str = "Failed to parse xml resource ";
        }
        sb2.append(str);
        sb2.append(string2);
        final IOException ex;
        Log.e("ResourcesCompat", sb2.toString(), (Throwable)ex);
        if (e != null) {
            e.callbackFailAsync(-3, handler);
        }
        return null;
    }
    
    public abstract static class a
    {
        public static Drawable a(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getDrawable(n, resources$Theme);
        }
        
        public static Drawable b(final Resources resources, final int n, final int n2, final Resources$Theme resources$Theme) {
            return resources.getDrawableForDensity(n, n2, resources$Theme);
        }
    }
    
    public abstract static class b
    {
        public static int a(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getColor(n, resources$Theme);
        }
        
        public static ColorStateList b(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getColorStateList(n, resources$Theme);
        }
    }
    
    public static class c
    {
        public final ColorStateList a;
        public final Configuration b;
        public final int c;
        
        public c(final ColorStateList a, final Configuration b, final Resources$Theme resources$Theme) {
            this.a = a;
            this.b = b;
            int hashCode;
            if (resources$Theme == null) {
                hashCode = 0;
            }
            else {
                hashCode = resources$Theme.hashCode();
            }
            this.c = hashCode;
        }
    }
    
    public static final class d
    {
        public final Resources a;
        public final Resources$Theme b;
        
        public d(final Resources a, final Resources$Theme b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && d.class == o.getClass()) {
                final d d = (d)o;
                if (!this.a.equals(d.a) || !c11.a(this.b, d.b)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return c11.b(this.a, this.b);
        }
    }
    
    public abstract static class e
    {
        public static Handler getHandler(final Handler handler) {
            Handler handler2 = handler;
            if (handler == null) {
                handler2 = new Handler(Looper.getMainLooper());
            }
            return handler2;
        }
        
        public final void callbackFailAsync(final int n, final Handler handler) {
            getHandler(handler).post((Runnable)new ne1(this, n));
        }
        
        public final void callbackSuccessAsync(final Typeface typeface, final Handler handler) {
            getHandler(handler).post((Runnable)new me1(this, typeface));
        }
        
        public abstract void onFontRetrievalFailed(final int p0);
        
        public abstract void onFontRetrieved(final Typeface p0);
    }
    
    public abstract static final class f
    {
        public static void a(final Resources$Theme resources$Theme) {
            if (Build$VERSION.SDK_INT >= 29) {
                b.a(resources$Theme);
            }
            else {
                a.a(resources$Theme);
            }
        }
        
        public abstract static class a
        {
            public static final Object a;
            public static Method b;
            public static boolean c;
            
            static {
                a = new Object();
            }
            
            public static void a(final Resources$Theme obj) {
                synchronized (f.a.a) {
                    if (!f.a.c) {
                        try {
                            (f.a.b = Resources$Theme.class.getDeclaredMethod("rebase", (Class<?>[])new Class[0])).setAccessible(true);
                        }
                        catch (final NoSuchMethodException ex) {
                            Log.i("ResourcesCompat", "Failed to retrieve rebase() method", (Throwable)ex);
                        }
                        f.a.c = true;
                    }
                    final Method b = f.a.b;
                    if (b != null) {
                        try {
                            b.invoke(obj, new Object[0]);
                            return;
                        }
                        catch (final InvocationTargetException obj) {}
                        catch (final IllegalAccessException ex2) {}
                        Log.i("ResourcesCompat", "Failed to invoke rebase() method via reflection", (Throwable)obj);
                        f.a.b = null;
                    }
                }
            }
        }
        
        public abstract static class b
        {
            public static void a(final Resources$Theme resources$Theme) {
                resources$Theme.rebase();
            }
        }
    }
}

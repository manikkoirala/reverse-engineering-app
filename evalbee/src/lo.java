import android.util.Log;
import android.os.Build$VERSION;
import java.io.Closeable;
import android.database.MatrixCursor;
import android.database.Cursor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class lo
{
    public static final Cursor a(final Cursor cursor) {
        fg0.e((Object)cursor, "c");
        try {
            final MatrixCursor matrixCursor = new MatrixCursor(cursor.getColumnNames(), cursor.getCount());
            while (cursor.moveToNext()) {
                final Object[] array = new Object[cursor.getColumnCount()];
                for (int columnCount = cursor.getColumnCount(), i = 0; i < columnCount; ++i) {
                    final int type = cursor.getType(i);
                    if (type != 0) {
                        if (type != 1) {
                            if (type != 2) {
                                if (type != 3) {
                                    if (type != 4) {
                                        throw new IllegalStateException();
                                    }
                                    array[i] = cursor.getBlob(i);
                                }
                                else {
                                    array[i] = cursor.getString(i);
                                }
                            }
                            else {
                                array[i] = cursor.getDouble(i);
                            }
                        }
                        else {
                            array[i] = cursor.getLong(i);
                        }
                    }
                    else {
                        array[i] = null;
                    }
                }
                matrixCursor.addRow(array);
            }
            dh.a((Closeable)cursor, (Throwable)null);
            return (Cursor)matrixCursor;
        }
        finally {
            try {}
            finally {
                final Throwable t;
                dh.a((Closeable)cursor, t);
            }
        }
    }
    
    public static final int b(final Cursor cursor, final String s) {
        if (Build$VERSION.SDK_INT > 25) {
            return -1;
        }
        if (s.length() == 0) {
            return -1;
        }
        final String[] columnNames = cursor.getColumnNames();
        fg0.d((Object)columnNames, "columnNames");
        return c(columnNames, s);
    }
    
    public static final int c(final String[] array, final String s) {
        fg0.e((Object)array, "columnNames");
        fg0.e((Object)s, "name");
        final StringBuilder sb = new StringBuilder();
        sb.append('.');
        sb.append(s);
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append('.');
        sb2.append(s);
        sb2.append('`');
        final String string2 = sb2.toString();
        for (int length = array.length, i = 0, n = 0; i < length; ++i, ++n) {
            final String s2 = array[i];
            if (s2.length() >= s.length() + 2) {
                if (qr1.j(s2, string, false, 2, (Object)null)) {
                    return n;
                }
                if (s2.charAt(0) == '`' && qr1.j(s2, string2, false, 2, (Object)null)) {
                    return n;
                }
            }
        }
        return -1;
    }
    
    public static final int d(final Cursor cursor, final String str) {
        fg0.e((Object)cursor, "c");
        fg0.e((Object)str, "name");
        final int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('`');
        sb.append(str);
        sb.append('`');
        int n = cursor.getColumnIndex(sb.toString());
        if (n < 0) {
            n = b(cursor, str);
        }
        return n;
    }
    
    public static final int e(final Cursor cursor, final String str) {
        fg0.e((Object)cursor, "c");
        fg0.e((Object)str, "name");
        final int d = d(cursor, str);
        if (d >= 0) {
            return d;
        }
        String i;
        try {
            final String[] columnNames = cursor.getColumnNames();
            fg0.d((Object)columnNames, "c.columnNames");
            i = a9.I((Object[])columnNames, (CharSequence)null, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (c90)null, 63, (Object)null);
        }
        catch (final Exception ex) {
            Log.d("RoomCursorUtil", "Cannot collect column names for debug purposes", (Throwable)ex);
            i = "unknown";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("column '");
        sb.append(str);
        sb.append("' does not exist. Available columns: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
}

import java.util.Comparator;
import java.util.SortedSet;

// 
// Decompiled by Procyon v0.6.0
// 

public interface to1 extends pm1
{
    SortedSet get(final Object p0);
    
    SortedSet removeAll(final Object p0);
    
    SortedSet replaceValues(final Object p0, final Iterable p1);
    
    Comparator valueComparator();
}

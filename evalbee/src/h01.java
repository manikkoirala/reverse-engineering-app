import android.os.BaseBundle;
import androidx.core.graphics.drawable.IconCompat;
import android.os.Parcelable;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class h01
{
    public static final Object a;
    public static final Object b;
    
    static {
        a = new Object();
        b = new Object();
    }
    
    public static Bundle a(final rz0.a a) {
        final Bundle bundle = new Bundle();
        final IconCompat d = a.d();
        int e;
        if (d != null) {
            e = d.e();
        }
        else {
            e = 0;
        }
        ((BaseBundle)bundle).putInt("icon", e);
        bundle.putCharSequence("title", a.h());
        bundle.putParcelable("actionIntent", (Parcelable)a.a());
        Bundle bundle2;
        if (a.c() != null) {
            bundle2 = new Bundle(a.c());
        }
        else {
            bundle2 = new Bundle();
        }
        ((BaseBundle)bundle2).putBoolean("android.support.allowGeneratedReplies", a.b());
        bundle.putBundle("extras", bundle2);
        bundle.putParcelableArray("remoteInputs", (Parcelable[])c(a.e()));
        ((BaseBundle)bundle).putBoolean("showsUserInterface", a.g());
        ((BaseBundle)bundle).putInt("semanticAction", a.f());
        return bundle;
    }
    
    public static Bundle b(final kd1 kd1) {
        new Bundle();
        throw null;
    }
    
    public static Bundle[] c(final kd1[] array) {
        if (array == null) {
            return null;
        }
        final Bundle[] array2 = new Bundle[array.length];
        for (int i = 0; i < array.length; ++i) {
            final kd1 kd1 = array[i];
            array2[i] = b(null);
        }
        return array2;
    }
}

import android.window.BackEvent;

// 
// Decompiled by Procyon v0.6.0
// 

public final class xa
{
    public static final a e;
    public final float a;
    public final float b;
    public final float c;
    public final int d;
    
    static {
        e = new a(null);
    }
    
    public xa(final float a, final float b, final float c, final int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public xa(final BackEvent backEvent) {
        fg0.e((Object)backEvent, "backEvent");
        final n5 a = n5.a;
        this(a.d(backEvent), a.e(backEvent), a.b(backEvent), a.c(backEvent));
    }
    
    public final float a() {
        return this.c;
    }
    
    public final int b() {
        return this.d;
    }
    
    public final float c() {
        return this.b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BackEventCompat{touchX=");
        sb.append(this.a);
        sb.append(", touchY=");
        sb.append(this.b);
        sb.append(", progress=");
        sb.append(this.c);
        sb.append(", swipeEdge=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
    
    public static final class a
    {
    }
}

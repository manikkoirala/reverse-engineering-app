import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class y70 extends a80 implements Entry
{
    public abstract Entry b();
    
    @Override
    public boolean equals(final Object o) {
        return this.b().equals(o);
    }
    
    @Override
    public Object getKey() {
        return this.b().getKey();
    }
    
    @Override
    public Object getValue() {
        return this.b().getValue();
    }
    
    @Override
    public int hashCode() {
        return this.b().hashCode();
    }
    
    @Override
    public Object setValue(final Object value) {
        return this.b().setValue(value);
    }
    
    public boolean standardEquals(final Object o) {
        final boolean b = o instanceof Entry;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Entry entry = (Entry)o;
            b3 = b2;
            if (b11.a(this.getKey(), entry.getKey())) {
                b3 = b2;
                if (b11.a(this.getValue(), entry.getValue())) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
}

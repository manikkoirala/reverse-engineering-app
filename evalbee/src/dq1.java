import com.google.common.base.b;
import java.util.concurrent.TimeUnit;

// 
// Decompiled by Procyon v0.6.0
// 

public final class dq1
{
    public final hw1 a;
    public boolean b;
    public long c;
    public long d;
    
    public dq1() {
        this.a = hw1.b();
    }
    
    public static String a(final TimeUnit timeUnit) {
        switch (dq1$a.a[timeUnit.ordinal()]) {
            default: {
                throw new AssertionError();
            }
            case 7: {
                return "d";
            }
            case 6: {
                return "h";
            }
            case 5: {
                return "min";
            }
            case 4: {
                return "s";
            }
            case 3: {
                return "ms";
            }
            case 2: {
                return "\u03bcs";
            }
            case 1: {
                return "ns";
            }
        }
    }
    
    public static TimeUnit b(final long n) {
        final TimeUnit days = TimeUnit.DAYS;
        final TimeUnit nanoseconds = TimeUnit.NANOSECONDS;
        if (days.convert(n, nanoseconds) > 0L) {
            return days;
        }
        final TimeUnit hours = TimeUnit.HOURS;
        if (hours.convert(n, nanoseconds) > 0L) {
            return hours;
        }
        final TimeUnit minutes = TimeUnit.MINUTES;
        if (minutes.convert(n, nanoseconds) > 0L) {
            return minutes;
        }
        final TimeUnit seconds = TimeUnit.SECONDS;
        if (seconds.convert(n, nanoseconds) > 0L) {
            return seconds;
        }
        final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
        if (milliseconds.convert(n, nanoseconds) > 0L) {
            return milliseconds;
        }
        final TimeUnit microseconds = TimeUnit.MICROSECONDS;
        if (microseconds.convert(n, nanoseconds) > 0L) {
            return microseconds;
        }
        return nanoseconds;
    }
    
    public static dq1 c() {
        return new dq1().h();
    }
    
    public static dq1 d() {
        return new dq1();
    }
    
    public long e(final TimeUnit timeUnit) {
        return timeUnit.convert(this.f(), TimeUnit.NANOSECONDS);
    }
    
    public final long f() {
        long c;
        if (this.b) {
            c = this.a.a() - this.d + this.c;
        }
        else {
            c = this.c;
        }
        return c;
    }
    
    public dq1 g() {
        this.c = 0L;
        this.b = false;
        return this;
    }
    
    public dq1 h() {
        i71.y(this.b ^ true, "This stopwatch is already running.");
        this.b = true;
        this.d = this.a.a();
        return this;
    }
    
    public dq1 i() {
        final long a = this.a.a();
        i71.y(this.b, "This stopwatch is already stopped.");
        this.b = false;
        this.c += a - this.d;
        return this;
    }
    
    @Override
    public String toString() {
        final long f = this.f();
        final TimeUnit b = b(f);
        final String c = com.google.common.base.b.c(f / (double)TimeUnit.NANOSECONDS.convert(1L, b));
        final String a = a(b);
        final StringBuilder sb = new StringBuilder(String.valueOf(c).length() + 1 + String.valueOf(a).length());
        sb.append(c);
        sb.append(" ");
        sb.append(a);
        return sb.toString();
    }
}

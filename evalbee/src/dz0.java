import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.ClientError;
import com.android.volley.AuthFailureError;
import android.os.SystemClock;
import com.android.volley.NetworkError;
import java.net.MalformedURLException;
import com.android.volley.TimeoutError;
import java.net.SocketTimeoutException;
import java.io.Serializable;
import java.io.IOException;
import com.android.volley.e;
import java.io.InputStream;
import com.android.volley.a;
import java.util.List;
import com.android.volley.VolleyError;
import com.android.volley.Request;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class dz0
{
    public static void a(final Request request, final b b) {
        final jf1 w = request.w();
        final int x = request.x();
        try {
            w.b(dz0.b.a(b));
            request.c(String.format("%s-retry [timeout=%s]", dz0.b.b(b), x));
        }
        catch (final VolleyError volleyError) {
            request.c(String.format("%s-timeout-giveup [timeout=%s]", dz0.b.b(b), x));
            throw volleyError;
        }
    }
    
    public static yy0 b(final Request request, final long n, List a) {
        final a.a m = request.m();
        if (m == null) {
            return new yy0(304, null, true, n, a);
        }
        a = id0.a(a, m);
        return new yy0(304, m.a, true, n, a);
    }
    
    public static byte[] c(final InputStream ex, int read, final ld ld) {
        final v51 v51 = new v51(ld, read);
        byte[] array;
        try {
            final byte[] a = ld.a(1024);
            try {
                while (true) {
                    read = ((InputStream)ex).read(a);
                    if (read == -1) {
                        break;
                    }
                    v51.write(a, 0, read);
                }
                final byte[] byteArray = v51.toByteArray();
                try {
                    ((InputStream)ex).close();
                }
                catch (final IOException ex) {
                    e.e("Error occurred when closing InputStream", new Object[0]);
                }
                ld.b(a);
                v51.close();
                return byteArray;
            }
            finally {}
        }
        finally {
            array = null;
        }
        if (ex != null) {
            try {
                ((InputStream)ex).close();
            }
            catch (final IOException ex2) {
                e.e("Error occurred when closing InputStream", new Object[0]);
            }
        }
        ld.b(array);
        v51.close();
    }
    
    public static void d(final long l, final Request request, final byte[] array, final int i) {
        if (e.b || l > 3000L) {
            Serializable value;
            if (array != null) {
                value = array.length;
            }
            else {
                value = "null";
            }
            e.b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", request, l, value, i, request.w().a());
        }
    }
    
    public static b e(final Request request, final IOException cause, final long n, final ld0 ld0, final byte[] array) {
        if (cause instanceof SocketTimeoutException) {
            return new b("socket", new TimeoutError(), null);
        }
        if (cause instanceof MalformedURLException) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bad URL ");
            sb.append(request.z());
            throw new RuntimeException(sb.toString(), cause);
        }
        if (ld0 != null) {
            final int d = ld0.d();
            e.c("Unexpected response code %d for %s", d, request.z());
            if (array == null) {
                return new b("network", new NetworkError(), null);
            }
            final yy0 yy0 = new yy0(d, array, false, SystemClock.elapsedRealtime() - n, ld0.c());
            if (d == 401 || d == 403) {
                return new b("auth", new AuthFailureError(yy0), null);
            }
            if (d >= 400 && d <= 499) {
                throw new ClientError(yy0);
            }
            if (d >= 500 && d <= 599 && request.P()) {
                return new b("server", new ServerError(yy0), null);
            }
            throw new ServerError(yy0);
        }
        else {
            if (request.O()) {
                return new b("connection", new NoConnectionError(), null);
            }
            throw new NoConnectionError(cause);
        }
    }
    
    public static class b
    {
        public final String a;
        public final VolleyError b;
        
        public b(final String a, final VolleyError b) {
            this.a = a;
            this.b = b;
        }
        
        public static /* synthetic */ VolleyError a(final b b) {
            return b.b;
        }
        
        public static /* synthetic */ String b(final b b) {
            return b.a;
        }
    }
}

import java.util.Iterator;
import io.grpc.Metadata$Key;
import java.util.Locale;
import java.util.HashMap;
import io.grpc.Metadata;
import java.net.ConnectException;
import java.net.UnknownHostException;
import com.google.firebase.firestore.util.Logger;
import io.grpc.Status$Code;
import com.google.firebase.firestore.remote.d;
import io.grpc.Status;
import java.util.concurrent.TimeUnit;
import com.google.firebase.firestore.util.a;
import io.grpc.ClientCall;
import com.google.firebase.firestore.remote.Stream$State;
import io.grpc.MethodDescriptor;
import com.google.firebase.firestore.util.AsyncQueue;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class g0
{
    public static final long n;
    public static final long o;
    public static final long p;
    public static final long q;
    public static final long r;
    public AsyncQueue.b a;
    public AsyncQueue.b b;
    public final w30 c;
    public final MethodDescriptor d;
    public final b e;
    public final AsyncQueue f;
    public final AsyncQueue.TimerId g;
    public final AsyncQueue.TimerId h;
    public Stream$State i;
    public long j;
    public ClientCall k;
    public final com.google.firebase.firestore.util.a l;
    public final cr1 m;
    
    static {
        final TimeUnit seconds = TimeUnit.SECONDS;
        n = seconds.toMillis(1L);
        final TimeUnit minutes = TimeUnit.MINUTES;
        o = minutes.toMillis(1L);
        p = minutes.toMillis(1L);
        q = seconds.toMillis(10L);
        r = seconds.toMillis(10L);
    }
    
    public g0(final w30 c, final MethodDescriptor d, final AsyncQueue f, final AsyncQueue.TimerId timerId, final AsyncQueue.TimerId g, final AsyncQueue.TimerId h, final cr1 m) {
        this.i = Stream$State.Initial;
        this.j = 0L;
        this.c = c;
        this.d = d;
        this.f = f;
        this.g = g;
        this.h = h;
        this.m = m;
        this.e = new b();
        this.l = new com.google.firebase.firestore.util.a(f, timerId, g0.n, 1.5, g0.o);
    }
    
    public static /* synthetic */ AsyncQueue c(final g0 g0) {
        return g0.f;
    }
    
    public static /* synthetic */ long d(final g0 g0) {
        return g0.j;
    }
    
    public final void g() {
        final AsyncQueue.b a = this.a;
        if (a != null) {
            a.c();
            this.a = null;
        }
    }
    
    public final void h() {
        final AsyncQueue.b b = this.b;
        if (b != null) {
            b.c();
            this.b = null;
        }
    }
    
    public final void i(final Stream$State i, final Status status) {
        g9.d(this.n(), "Only started streams should be closed.", new Object[0]);
        final Stream$State error = Stream$State.Error;
        g9.d(i == error || status.isOk(), "Can't provide an error when not in an error state.", new Object[0]);
        this.f.p();
        if (com.google.firebase.firestore.remote.d.e(status)) {
            o22.o(new IllegalStateException("The Cloud Firestore client failed to establish a secure connection. This is likely a problem with your app, rather than with Cloud Firestore itself. See https://bit.ly/2XFpdma for instructions on how to enable TLS on Android 4.x devices.", status.getCause()));
        }
        this.h();
        this.g();
        this.l.c();
        ++this.j;
        final Status$Code code = status.getCode();
        if (code == Status$Code.OK) {
            this.l.f();
        }
        else if (code == Status$Code.RESOURCE_EXHAUSTED) {
            Logger.a(this.getClass().getSimpleName(), "(%x) Using maximum backoff delay to prevent overloading the backend.", System.identityHashCode(this));
            this.l.g();
        }
        else if (code == Status$Code.UNAUTHENTICATED && this.i != Stream$State.Healthy) {
            this.c.d();
        }
        else if (code == Status$Code.UNAVAILABLE && (status.getCause() instanceof UnknownHostException || status.getCause() instanceof ConnectException)) {
            this.l.h(g0.r);
        }
        if (i != error) {
            Logger.a(this.getClass().getSimpleName(), "(%x) Performing stream teardown", System.identityHashCode(this));
            this.w();
        }
        if (this.k != null) {
            if (status.isOk()) {
                Logger.a(this.getClass().getSimpleName(), "(%x) Closing stream client-side", System.identityHashCode(this));
                this.k.halfClose();
            }
            this.k = null;
        }
        this.i = i;
        this.m.a(status);
    }
    
    public final void j() {
        if (this.m()) {
            this.i(Stream$State.Initial, Status.OK);
        }
    }
    
    public void k(final Status status) {
        g9.d(this.n(), "Can't handle server close on non-started stream!", new Object[0]);
        this.i(Stream$State.Error, status);
    }
    
    public void l() {
        g9.d(this.n() ^ true, "Can only inhibit backoff after in a stopped state", new Object[0]);
        this.f.p();
        this.i = Stream$State.Initial;
        this.l.f();
    }
    
    public boolean m() {
        this.f.p();
        final Stream$State i = this.i;
        return i == Stream$State.Open || i == Stream$State.Healthy;
    }
    
    public boolean n() {
        this.f.p();
        final Stream$State i = this.i;
        return i == Stream$State.Starting || i == Stream$State.Backoff || this.m();
    }
    
    public void q() {
        if (this.m() && this.b == null) {
            this.b = this.f.h(this.g, g0.p, this.e);
        }
    }
    
    public abstract void r(final Object p0);
    
    public final void s() {
        this.i = Stream$State.Open;
        this.m.b();
        if (this.a == null) {
            this.a = this.f.h(this.h, g0.q, new f0(this));
        }
    }
    
    public final void t() {
        g9.d(this.i == Stream$State.Error, "Should only perform backoff in an error state", new Object[0]);
        this.i = Stream$State.Backoff;
        this.l.b(new e0(this));
    }
    
    public void u() {
        this.f.p();
        final ClientCall k = this.k;
        final boolean b = true;
        g9.d(k == null, "Last call still set", new Object[0]);
        g9.d(this.b == null, "Idle timer still set", new Object[0]);
        final Stream$State i = this.i;
        if (i == Stream$State.Error) {
            this.t();
            return;
        }
        g9.d(i == Stream$State.Initial && b, "Already started", new Object[0]);
        this.k = this.c.g(this.d, new c(new a(this.j)));
        this.i = Stream$State.Starting;
    }
    
    public void v() {
        if (this.n()) {
            this.i(Stream$State.Initial, Status.OK);
        }
    }
    
    public void w() {
    }
    
    public void x(final Object o) {
        this.f.p();
        Logger.a(this.getClass().getSimpleName(), "(%x) Stream sending: %s", System.identityHashCode(this), o);
        this.h();
        this.k.sendMessage(o);
    }
    
    public class a
    {
        public final long a;
        public final g0 b;
        
        public a(final g0 b, final long a) {
            this.b = b;
            this.a = a;
        }
        
        public void a(final Runnable runnable) {
            g0.c(this.b).p();
            if (g0.d(this.b) == this.a) {
                runnable.run();
            }
            else {
                Logger.a(this.b.getClass().getSimpleName(), "stream callback skipped by CloseGuardedRunner.", new Object[0]);
            }
        }
    }
    
    public class b implements Runnable
    {
        public final g0 a;
        
        public b(final g0 a) {
            this.a = a;
        }
        
        @Override
        public void run() {
            this.a.j();
        }
    }
    
    public class c implements pe0
    {
        public final a a;
        public final g0 b;
        
        public c(final g0 b, final a a) {
            this.b = b;
            this.a = a;
        }
        
        @Override
        public void a(final Status status) {
            this.a.a(new i0(this, status));
        }
        
        @Override
        public void b() {
            this.a.a(new h0(this));
        }
        
        @Override
        public void c(final Metadata metadata) {
            this.a.a(new k0(this, metadata));
        }
        
        @Override
        public void onNext(final Object o) {
            this.a.a(new j0(this, o));
        }
    }
}

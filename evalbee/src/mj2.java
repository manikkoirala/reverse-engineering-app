import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.List;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public final class mj2 implements SafeParcelable
{
    public static final Parcelable$Creator<mj2> CREATOR;
    public na2 a;
    public aj2 b;
    public lf2 c;
    
    static {
        CREATOR = (Parcelable$Creator)new vj2();
    }
    
    public mj2(final na2 na2) {
        final na2 a = Preconditions.checkNotNull(na2);
        this.a = a;
        final List z0 = a.z0();
        this.b = null;
        for (int i = 0; i < z0.size(); ++i) {
            if (!TextUtils.isEmpty((CharSequence)((tj2)z0.get(i)).zza())) {
                this.b = new aj2(((tj2)z0.get(i)).b(), ((tj2)z0.get(i)).zza(), na2.A0());
            }
        }
        if (this.b == null) {
            this.b = new aj2(na2.A0());
        }
        this.c = na2.y0();
    }
    
    public mj2(final na2 a, final aj2 b, final lf2 c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public final x3 b() {
        return this.b;
    }
    
    public final r30 c() {
        return this.a;
    }
    
    public final int describeContents() {
        return 0;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.c(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.b(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.c, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class ea implements jk
{
    public static final jk a;
    
    static {
        a = new ea();
    }
    
    @Override
    public void configure(final zw zw) {
        final a a = ea.a.a;
        zw.a(nf1.class, a);
        zw.a(pa.class, a);
    }
    
    public static final class a implements w01
    {
        public static final a a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        
        static {
            a = new a();
            b = n00.d("rolloutId");
            c = n00.d("parameterKey");
            d = n00.d("parameterValue");
            e = n00.d("variantId");
            f = n00.d("templateVersion");
        }
        
        public void a(final nf1 nf1, final x01 x01) {
            x01.f(ea.a.b, nf1.e());
            x01.f(ea.a.c, nf1.c());
            x01.f(ea.a.d, nf1.d());
            x01.f(ea.a.e, nf1.g());
            x01.e(ea.a.f, nf1.f());
        }
    }
}

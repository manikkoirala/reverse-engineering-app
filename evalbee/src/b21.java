// 
// Decompiled by Procyon v0.6.0
// 

public class b21
{
    public int a;
    public boolean[] b;
    public int[] c;
    public String[] d;
    
    public b21(final int a, final boolean[] b, final String[] d) {
        this.a = a;
        this.b = b;
        this.d = d;
        this.c = new int[d.length];
    }
    
    public boolean[] a() {
        return this.b;
    }
    
    public int[] b() {
        return this.c;
    }
    
    public String[] c() {
        return this.d;
    }
    
    public int d() {
        return this.a;
    }
}

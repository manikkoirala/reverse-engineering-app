import android.content.IntentFilter;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zf2
{
    public final Context a;
    public final xf2 b;
    
    public zf2(final Context a, final ca1 ca1, final d4 d4, final dd2 dd2) {
        this.a = a;
        this.b = new xf2(this, ca1, d4, dd2, null);
    }
    
    public zf2(final Context a, final sd2 sd2, final dd2 dd2) {
        this.a = a;
        this.b = new xf2(this, null, dd2, null);
    }
    
    public final sd2 c() {
        xf2.a(this.b);
        return null;
    }
    
    public final ca1 d() {
        return xf2.b(this.b);
    }
    
    public final void e() {
        this.b.d(this.a);
    }
    
    public final void f(final boolean b) {
        final IntentFilter intentFilter = new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
        this.a.getApplicationContext().getPackageName();
        intentFilter.addAction("com.android.vending.billing.ALTERNATIVE_BILLING");
        this.b.c(this.a, intentFilter, null, null);
    }
}

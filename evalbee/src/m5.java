import org.jetbrains.annotations.NotNull;
import android.app.Application;

// 
// Decompiled by Procyon v0.6.0
// 

public final class m5
{
    public static final m5 a;
    
    static {
        a = new m5();
    }
    
    @NotNull
    public final String a() {
        final String processName = Application.getProcessName();
        fg0.d((Object)processName, "getProcessName()");
        return processName;
    }
}

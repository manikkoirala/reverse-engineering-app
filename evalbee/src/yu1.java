// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yu1 extends oz
{
    public String b;
    public boolean c;
    
    public yu1(final String s, final boolean b) {
        this.b = null;
        this.c = false;
        this.k(s);
        this.l(b);
    }
    
    public static boolean j(final String s) {
        if (s.length() == 0) {
            return false;
        }
        final char char1 = s.charAt(0);
        if ((char1 < '0' || char1 > '9') && char1 != '.' && char1 != ',' && char1 != '(' && char1 != ')' && char1 != '^' && char1 != '*' && char1 != '/' && char1 != '+' && char1 != '-' && char1 != ' ' && char1 != '\t' && char1 != '\n') {
            for (int i = 1; i < s.length(); ++i) {
                final char char2 = s.charAt(i);
                if (char2 == ',' || char2 == '(' || char2 == ')' || char2 == '^' || char2 == '*' || char2 == '/' || char2 == '+' || char2 == '-' || char2 == ' ' || char2 == '\t' || char2 == '\n') {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public String h() {
        return this.b;
    }
    
    public boolean i() {
        return this.c;
    }
    
    public void k(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("name cannot be null");
        }
        if (j(s)) {
            this.b = s;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("invalid name: ");
        sb.append(s);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void l(final boolean c) {
        this.c = c;
    }
}

import android.graphics.Matrix;
import android.graphics.Rect;
import android.view.View;
import android.os.Build$VERSION;
import android.util.Property;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class t42
{
    public static final g52 a;
    public static final Property b;
    public static final Property c;
    
    static {
        e52 a2;
        if (Build$VERSION.SDK_INT >= 29) {
            a2 = new f52();
        }
        else {
            a2 = new e52();
        }
        a = a2;
        b = new Property(Float.class, "translationAlpha") {
            public Float a(final View view) {
                return t42.c(view);
            }
            
            public void b(final View view, final Float n) {
                t42.g(view, n);
            }
        };
        c = new Property(Rect.class, "clipBounds") {
            public Rect a(final View view) {
                return o32.u(view);
            }
            
            public void b(final View view, final Rect rect) {
                o32.x0(view, rect);
            }
        };
    }
    
    public static void a(final View view) {
        t42.a.a(view);
    }
    
    public static e42 b(final View view) {
        return new d42(view);
    }
    
    public static float c(final View view) {
        return t42.a.b(view);
    }
    
    public static g62 d(final View view) {
        return new f62(view);
    }
    
    public static void e(final View view) {
        t42.a.c(view);
    }
    
    public static void f(final View view, final int n, final int n2, final int n3, final int n4) {
        t42.a.d(view, n, n2, n3, n4);
    }
    
    public static void g(final View view, final float n) {
        t42.a.e(view, n);
    }
    
    public static void h(final View view, final int n) {
        t42.a.f(view, n);
    }
    
    public static void i(final View view, final Matrix matrix) {
        t42.a.g(view, matrix);
    }
    
    public static void j(final View view, final Matrix matrix) {
        t42.a.h(view, matrix);
    }
}

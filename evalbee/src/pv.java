import android.util.AndroidRuntimeException;
import android.os.Looper;
import android.view.View;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class pv implements b
{
    public static final q m;
    public static final q n;
    public static final q o;
    public static final q p;
    public static final q q;
    public static final q r;
    public static final q s;
    public static final q t;
    public static final q u;
    public static final q v;
    public static final q w;
    public static final q x;
    public static final q y;
    public static final q z;
    public float a;
    public float b;
    public boolean c;
    public final Object d;
    public final v40 e;
    public boolean f;
    public float g;
    public float h;
    public long i;
    public float j;
    public final ArrayList k;
    public final ArrayList l;
    
    static {
        m = (q)new q("translationX") {
            public float a(final View view) {
                return view.getTranslationX();
            }
            
            public void b(final View view, final float translationX) {
                view.setTranslationX(translationX);
            }
        };
        n = (q)new q("translationY") {
            public float a(final View view) {
                return view.getTranslationY();
            }
            
            public void b(final View view, final float translationY) {
                view.setTranslationY(translationY);
            }
        };
        o = (q)new q("translationZ") {
            public float a(final View view) {
                return o32.K(view);
            }
            
            public void b(final View view, final float n) {
                o32.L0(view, n);
            }
        };
        p = (q)new q("scaleX") {
            public float a(final View view) {
                return view.getScaleX();
            }
            
            public void b(final View view, final float scaleX) {
                view.setScaleX(scaleX);
            }
        };
        q = (q)new q("scaleY") {
            public float a(final View view) {
                return view.getScaleY();
            }
            
            public void b(final View view, final float scaleY) {
                view.setScaleY(scaleY);
            }
        };
        r = (q)new q("rotation") {
            public float a(final View view) {
                return view.getRotation();
            }
            
            public void b(final View view, final float rotation) {
                view.setRotation(rotation);
            }
        };
        s = (q)new q("rotationX") {
            public float a(final View view) {
                return view.getRotationX();
            }
            
            public void b(final View view, final float rotationX) {
                view.setRotationX(rotationX);
            }
        };
        t = (q)new q("rotationY") {
            public float a(final View view) {
                return view.getRotationY();
            }
            
            public void b(final View view, final float rotationY) {
                view.setRotationY(rotationY);
            }
        };
        u = (q)new q("x") {
            public float a(final View view) {
                return view.getX();
            }
            
            public void b(final View view, final float x) {
                view.setX(x);
            }
        };
        v = (q)new q("y") {
            public float a(final View view) {
                return view.getY();
            }
            
            public void b(final View view, final float y) {
                view.setY(y);
            }
        };
        w = (q)new q("z") {
            public float a(final View view) {
                return o32.N(view);
            }
            
            public void b(final View view, final float n) {
                o32.O0(view, n);
            }
        };
        x = (q)new q("alpha") {
            public float a(final View view) {
                return view.getAlpha();
            }
            
            public void b(final View view, final float alpha) {
                view.setAlpha(alpha);
            }
        };
        y = (q)new q("scrollX") {
            public float a(final View view) {
                return (float)view.getScrollX();
            }
            
            public void b(final View view, final float n) {
                view.setScrollX((int)n);
            }
        };
        z = (q)new q("scrollY") {
            public float a(final View view) {
                return (float)view.getScrollY();
            }
            
            public void b(final View view, final float n) {
                view.setScrollY((int)n);
            }
        };
    }
    
    public pv(final Object d, final v40 e) {
        this.a = 0.0f;
        this.b = Float.MAX_VALUE;
        this.c = false;
        this.f = false;
        this.g = Float.MAX_VALUE;
        this.h = -Float.MAX_VALUE;
        this.i = 0L;
        this.k = new ArrayList();
        this.l = new ArrayList();
        this.d = d;
        this.e = e;
        float j;
        if (e != pv.r && e != pv.s && e != pv.t) {
            if (e == pv.x || e == pv.p || e == pv.q) {
                this.j = 0.00390625f;
                return;
            }
            j = 1.0f;
        }
        else {
            j = 0.1f;
        }
        this.j = j;
    }
    
    public static void h(final ArrayList list, final Object o) {
        final int index = list.indexOf(o);
        if (index >= 0) {
            list.set(index, null);
        }
    }
    
    public static void i(final ArrayList list) {
        for (int i = list.size() - 1; i >= 0; --i) {
            if (list.get(i) == null) {
                list.remove(i);
            }
        }
    }
    
    @Override
    public boolean a(final long n) {
        final long i = this.i;
        if (i == 0L) {
            this.i = n;
            this.j(this.b);
            return false;
        }
        this.i = n;
        final boolean n2 = this.n(n - i);
        final float min = Math.min(this.b, this.g);
        this.b = min;
        this.j(this.b = Math.max(min, this.h));
        if (n2) {
            this.c(false);
        }
        return n2;
    }
    
    public pv b(final p p) {
        if (!this.k.contains(p)) {
            this.k.add(p);
        }
        return this;
    }
    
    public final void c(final boolean b) {
        int i = 0;
        this.f = false;
        a5.d().g((a5.b)this);
        this.i = 0L;
        this.c = false;
        while (i < this.k.size()) {
            if (this.k.get(i) != null) {
                zu0.a(this.k.get(i));
                throw null;
            }
            ++i;
        }
        i(this.k);
    }
    
    public final float d() {
        return this.e.getValue(this.d);
    }
    
    public float e() {
        return this.j * 0.75f;
    }
    
    public boolean f() {
        return this.f;
    }
    
    public void g(final p p) {
        h(this.k, p);
    }
    
    public void j(final float n) {
        this.e.setValue(this.d, n);
        for (int i = 0; i < this.l.size(); ++i) {
            if (this.l.get(i) != null) {
                zu0.a(this.l.get(i));
                throw null;
            }
        }
        i(this.l);
    }
    
    public pv k(final float b) {
        this.b = b;
        this.c = true;
        return this;
    }
    
    public void l() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            if (!this.f) {
                this.m();
            }
            return;
        }
        throw new AndroidRuntimeException("Animations may only be started on the main thread");
    }
    
    public final void m() {
        if (!this.f) {
            this.f = true;
            if (!this.c) {
                this.b = this.d();
            }
            final float b = this.b;
            if (b > this.g || b < this.h) {
                throw new IllegalArgumentException("Starting value need to be in between min value and max value");
            }
            a5.d().a((a5.b)this, 0L);
        }
    }
    
    public abstract boolean n(final long p0);
    
    public static class o
    {
        public float a;
        public float b;
    }
    
    public interface p
    {
    }
    
    public abstract static class q extends v40
    {
        public q(final String s) {
            super(s);
        }
    }
}

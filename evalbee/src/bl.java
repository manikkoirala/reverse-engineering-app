import java.util.EnumMap;
import java.util.EnumSet;
import com.google.gson.internal.LinkedTreeMap;
import java.util.LinkedHashMap;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.ParameterizedType;
import java.util.TreeMap;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Collection;
import java.lang.reflect.Type;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Constructor;
import com.google.gson.JsonIOException;
import java.lang.reflect.AccessibleObject;
import com.google.gson.ReflectionAccessFilter$FilterResult;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class bl
{
    public final Map a;
    public final boolean b;
    public final List c;
    
    public bl(final Map a, final boolean b, final List c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static String a(final Class clazz) {
        final int modifiers = clazz.getModifiers();
        StringBuilder sb;
        String str;
        if (Modifier.isInterface(modifiers)) {
            sb = new StringBuilder();
            str = "Interfaces can't be instantiated! Register an InstanceCreator or a TypeAdapter for this type. Interface name: ";
        }
        else {
            if (!Modifier.isAbstract(modifiers)) {
                return null;
            }
            sb = new StringBuilder();
            str = "Abstract classes can't be instantiated! Register an InstanceCreator or a TypeAdapter for this type. Class name: ";
        }
        sb.append(str);
        sb.append(clazz.getName());
        return sb.toString();
    }
    
    public static u01 c(final Class obj, final ReflectionAccessFilter$FilterResult reflectionAccessFilter$FilterResult) {
        if (Modifier.isAbstract(obj.getModifiers())) {
            return null;
        }
        final boolean b = false;
        try {
            final Constructor declaredConstructor = obj.getDeclaredConstructor((Class[])new Class[0]);
            final ReflectionAccessFilter$FilterResult allow = ReflectionAccessFilter$FilterResult.ALLOW;
            int n = 0;
            Label_0068: {
                if (reflectionAccessFilter$FilterResult != allow) {
                    n = (b ? 1 : 0);
                    if (!ad1.a(declaredConstructor, null)) {
                        break Label_0068;
                    }
                    if (reflectionAccessFilter$FilterResult == ReflectionAccessFilter$FilterResult.BLOCK_ALL) {
                        n = (b ? 1 : 0);
                        if (!Modifier.isPublic(declaredConstructor.getModifiers())) {
                            break Label_0068;
                        }
                    }
                }
                n = 1;
            }
            if (n == 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to invoke no-args constructor of ");
                sb.append(obj);
                sb.append("; constructor is not accessible and ReflectionAccessFilter does not permit making it accessible. Register an InstanceCreator or a TypeAdapter for this type, change the visibility of the constructor or adjust the access filter.");
                return new u01(sb.toString()) {
                    public final String a;
                    
                    @Override
                    public Object a() {
                        throw new JsonIOException(this.a);
                    }
                };
            }
            if (reflectionAccessFilter$FilterResult == allow) {
                final String m = cd1.m(declaredConstructor);
                if (m != null) {
                    return new u01(m) {
                        public final String a;
                        
                        @Override
                        public Object a() {
                            throw new JsonIOException(this.a);
                        }
                    };
                }
            }
            return new u01(declaredConstructor) {
                public final Constructor a;
                
                @Override
                public Object a() {
                    try {
                        return this.a.newInstance(new Object[0]);
                    }
                    catch (final IllegalAccessException ex) {
                        throw cd1.e(ex);
                    }
                    catch (final InvocationTargetException ex2) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to invoke constructor '");
                        sb.append(cd1.c(this.a));
                        sb.append("' with no args");
                        throw new RuntimeException(sb.toString(), ex2.getCause());
                    }
                    catch (final InstantiationException cause) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Failed to invoke constructor '");
                        sb2.append(cd1.c(this.a));
                        sb2.append("' with no args");
                        throw new RuntimeException(sb2.toString(), cause);
                    }
                }
            };
        }
        catch (final NoSuchMethodException ex) {
            return null;
        }
    }
    
    public static u01 d(final Type type, final Class clazz) {
        if (Collection.class.isAssignableFrom(clazz)) {
            if (SortedSet.class.isAssignableFrom(clazz)) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new TreeSet();
                    }
                };
            }
            if (Set.class.isAssignableFrom(clazz)) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new LinkedHashSet();
                    }
                };
            }
            if (Queue.class.isAssignableFrom(clazz)) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new ArrayDeque();
                    }
                };
            }
            return new u01() {
                @Override
                public Object a() {
                    return new ArrayList();
                }
            };
        }
        else {
            if (!Map.class.isAssignableFrom(clazz)) {
                return null;
            }
            if (ConcurrentNavigableMap.class.isAssignableFrom(clazz)) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new ConcurrentSkipListMap();
                    }
                };
            }
            if (ConcurrentMap.class.isAssignableFrom(clazz)) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new ConcurrentHashMap();
                    }
                };
            }
            if (SortedMap.class.isAssignableFrom(clazz)) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new TreeMap();
                    }
                };
            }
            if (type instanceof ParameterizedType && !String.class.isAssignableFrom(TypeToken.get(((ParameterizedType)type).getActualTypeArguments()[0]).getRawType())) {
                return new u01() {
                    @Override
                    public Object a() {
                        return new LinkedHashMap();
                    }
                };
            }
            return new u01() {
                @Override
                public Object a() {
                    return new LinkedTreeMap();
                }
            };
        }
    }
    
    public static u01 e(final Type type, final Class clazz) {
        if (EnumSet.class.isAssignableFrom(clazz)) {
            return new u01(type) {
                public final Type a;
                
                @Override
                public Object a() {
                    final Type a = this.a;
                    if (!(a instanceof ParameterizedType)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid EnumSet type: ");
                        sb.append(this.a.toString());
                        throw new JsonIOException(sb.toString());
                    }
                    final Type type = ((ParameterizedType)a).getActualTypeArguments()[0];
                    if (type instanceof Class) {
                        return EnumSet.noneOf((Class<Enum>)type);
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid EnumSet type: ");
                    sb2.append(this.a.toString());
                    throw new JsonIOException(sb2.toString());
                }
            };
        }
        if (clazz == EnumMap.class) {
            return new u01(type) {
                public final Type a;
                
                @Override
                public Object a() {
                    final Type a = this.a;
                    if (!(a instanceof ParameterizedType)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid EnumMap type: ");
                        sb.append(this.a.toString());
                        throw new JsonIOException(sb.toString());
                    }
                    final Type type = ((ParameterizedType)a).getActualTypeArguments()[0];
                    if (type instanceof Class) {
                        return new EnumMap((Class<Enum>)type);
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid EnumMap type: ");
                    sb2.append(this.a.toString());
                    throw new JsonIOException(sb2.toString());
                }
            };
        }
        return null;
    }
    
    public u01 b(final TypeToken typeToken) {
        final Type type = typeToken.getType();
        final Class rawType = typeToken.getRawType();
        zu0.a(this.a.get(type));
        zu0.a(this.a.get(rawType));
        final u01 e = e(type, rawType);
        if (e != null) {
            return e;
        }
        final ReflectionAccessFilter$FilterResult b = ad1.b(this.c, rawType);
        final u01 c = c(rawType, b);
        if (c != null) {
            return c;
        }
        final u01 d = d(type, rawType);
        if (d != null) {
            return d;
        }
        final String a = a(rawType);
        if (a != null) {
            return new u01(this, a) {
                public final String a;
                public final bl b;
                
                @Override
                public Object a() {
                    throw new JsonIOException(this.a);
                }
            };
        }
        if (b == ReflectionAccessFilter$FilterResult.ALLOW) {
            return this.f(rawType);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to create instance of ");
        sb.append(rawType);
        sb.append("; ReflectionAccessFilter does not permit using reflection or Unsafe. Register an InstanceCreator or a TypeAdapter for this type or adjust the access filter to allow using reflection.");
        return new u01(this, sb.toString()) {
            public final String a;
            public final bl b;
            
            @Override
            public Object a() {
                throw new JsonIOException(this.a);
            }
        };
    }
    
    public final u01 f(final Class obj) {
        if (this.b) {
            return new u01(this, obj) {
                public final Class a;
                public final bl b;
                
                @Override
                public Object a() {
                    try {
                        return c12.a.d(this.a);
                    }
                    catch (final Exception cause) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unable to create instance of ");
                        sb.append(this.a);
                        sb.append(". Registering an InstanceCreator or a TypeAdapter for this type, or adding a no-args constructor may fix this problem.");
                        throw new RuntimeException(sb.toString(), cause);
                    }
                }
            };
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to create instance of ");
        sb.append(obj);
        sb.append("; usage of JDK Unsafe is disabled. Registering an InstanceCreator or a TypeAdapter for this type, adding a no-args constructor, or enabling usage of JDK Unsafe may fix this problem.");
        return new u01(this, sb.toString()) {
            public final String a;
            public final bl b;
            
            @Override
            public Object a() {
                throw new JsonIOException(this.a);
            }
        };
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
}

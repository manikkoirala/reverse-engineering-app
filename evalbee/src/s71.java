import kotlin.collections.b;
import androidx.datastore.preferences.core.MutablePreferences;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class s71
{
    public abstract Map a();
    
    public abstract Object b(final a p0);
    
    public final MutablePreferences c() {
        return new MutablePreferences(kotlin.collections.b.t(this.a()), false);
    }
    
    public final s71 d() {
        return new MutablePreferences(kotlin.collections.b.t(this.a()), true);
    }
    
    public static final class a
    {
        public final String a;
        
        public a(final String a) {
            fg0.e((Object)a, "name");
            this.a = a;
        }
        
        public final String a() {
            return this.a;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof a && fg0.a((Object)this.a, (Object)((a)o).a);
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public String toString() {
            return this.a;
        }
    }
    
    public abstract static final class b
    {
    }
}

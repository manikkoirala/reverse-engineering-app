import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class vz extends v9
{
    public static final Parcelable$Creator<vz> CREATOR;
    public final String a;
    
    static {
        CREATOR = (Parcelable$Creator)new yf2();
    }
    
    public vz(final String s) {
        this.a = Preconditions.checkNotEmpty(s);
    }
    
    public static zzagt J(final vz vz, final String s) {
        Preconditions.checkNotNull(vz);
        return (zzagt)new com.google.android.gms.internal.firebase_auth_api.zzagt((String)null, vz.a, vz.i(), (String)null, (String)null, (String)null, s, (String)null, (String)null);
    }
    
    @Override
    public String E() {
        return "facebook.com";
    }
    
    @Override
    public final v9 H() {
        return new vz(this.a);
    }
    
    @Override
    public String i() {
        return "facebook.com";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

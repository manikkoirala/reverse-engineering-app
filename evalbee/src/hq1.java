import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hq1 extends uc
{
    public hq1(final Context context, final hu1 hu1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        super(context, hu1);
    }
    
    @Override
    public IntentFilter j() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }
    
    @Override
    public void k(final Intent intent) {
        fg0.e((Object)intent, "intent");
        if (intent.getAction() == null) {
            return;
        }
        final xl0 e = xl0.e();
        final String a = iq1.a();
        final StringBuilder sb = new StringBuilder();
        sb.append("Received ");
        sb.append(intent.getAction());
        e.a(a, sb.toString());
        final String action = intent.getAction();
        if (action != null) {
            final int hashCode = action.hashCode();
            Boolean b;
            if (hashCode != -1181163412) {
                if (hashCode != -730838620) {
                    return;
                }
                if (!action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    return;
                }
                b = Boolean.TRUE;
            }
            else {
                if (!action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                    return;
                }
                b = Boolean.FALSE;
            }
            this.g(b);
        }
    }
    
    public Boolean l() {
        final Intent registerReceiver = this.d().registerReceiver((BroadcastReceiver)null, this.j());
        boolean b2;
        final boolean b = b2 = true;
        if (registerReceiver != null) {
            if (registerReceiver.getAction() == null) {
                b2 = b;
            }
            else {
                final String action = registerReceiver.getAction();
                if (action != null) {
                    final int hashCode = action.hashCode();
                    if (hashCode != -1181163412) {
                        if (hashCode == -730838620) {
                            b2 = b;
                            if (action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                                return b2;
                            }
                        }
                    }
                    else {
                        action.equals("android.intent.action.DEVICE_STORAGE_LOW");
                    }
                }
                b2 = false;
            }
        }
        return b2;
    }
}

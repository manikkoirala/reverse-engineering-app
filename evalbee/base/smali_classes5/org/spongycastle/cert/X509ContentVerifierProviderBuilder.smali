.class public interface abstract Lorg/spongycastle/cert/X509ContentVerifierProviderBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract build(Lorg/spongycastle/asn1/x509/SubjectPublicKeyInfo;)Lorg/spongycastle/operator/ContentVerifierProvider;
.end method

.method public abstract build(Lorg/spongycastle/cert/X509CertificateHolder;)Lorg/spongycastle/operator/ContentVerifierProvider;
.end method

.class public Lorg/spongycastle/cert/jcajce/JcaX509AttributeCertificateHolder;
.super Lorg/spongycastle/cert/X509AttributeCertificateHolder;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/spongycastle/x509/X509AttributeCertificate;)V
    .locals 0

    invoke-interface {p1}, Lorg/spongycastle/x509/X509AttributeCertificate;->getEncoded()[B

    move-result-object p1

    invoke-static {p1}, Lorg/spongycastle/asn1/x509/AttributeCertificate;->getInstance(Ljava/lang/Object;)Lorg/spongycastle/asn1/x509/AttributeCertificate;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/spongycastle/cert/X509AttributeCertificateHolder;-><init>(Lorg/spongycastle/asn1/x509/AttributeCertificate;)V

    return-void
.end method

.class public interface abstract Lorg/spongycastle/crypto/tls/DatagramTransport;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getReceiveLimit()I
.end method

.method public abstract getSendLimit()I
.end method

.method public abstract receive([BIII)I
.end method

.method public abstract send([BII)V
.end method

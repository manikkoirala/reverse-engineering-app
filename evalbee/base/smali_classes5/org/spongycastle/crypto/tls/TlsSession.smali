.class public interface abstract Lorg/spongycastle/crypto/tls/TlsSession;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract exportSessionParameters()Lorg/spongycastle/crypto/tls/SessionParameters;
.end method

.method public abstract getSessionID()[B
.end method

.method public abstract invalidate()V
.end method

.method public abstract isResumable()Z
.end method

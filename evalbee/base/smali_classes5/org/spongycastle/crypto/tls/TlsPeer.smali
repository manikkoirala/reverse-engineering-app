.class public interface abstract Lorg/spongycastle/crypto/tls/TlsPeer;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getCipher()Lorg/spongycastle/crypto/tls/TlsCipher;
.end method

.method public abstract getCompression()Lorg/spongycastle/crypto/tls/TlsCompression;
.end method

.method public abstract notifyAlertRaised(SSLjava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract notifyAlertReceived(SS)V
.end method

.method public abstract notifyHandshakeComplete()V
.end method

.method public abstract notifySecureRenegotiation(Z)V
.end method

.method public abstract shouldUseGMTUnixTime()Z
.end method

.class public interface abstract Lorg/spongycastle/crypto/AsymmetricBlockCipher;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getInputBlockSize()I
.end method

.method public abstract getOutputBlockSize()I
.end method

.method public abstract init(ZLorg/spongycastle/crypto/CipherParameters;)V
.end method

.method public abstract processBlock([BII)[B
.end method

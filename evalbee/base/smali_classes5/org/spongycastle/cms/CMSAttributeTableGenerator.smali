.class public interface abstract Lorg/spongycastle/cms/CMSAttributeTableGenerator;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "contentType"

.field public static final DIGEST:Ljava/lang/String; = "digest"

.field public static final DIGEST_ALGORITHM_IDENTIFIER:Ljava/lang/String; = "digestAlgID"

.field public static final SIGNATURE:Ljava/lang/String; = "encryptedDigest"


# virtual methods
.method public abstract getAttributes(Ljava/util/Map;)Lorg/spongycastle/asn1/cms/AttributeTable;
.end method

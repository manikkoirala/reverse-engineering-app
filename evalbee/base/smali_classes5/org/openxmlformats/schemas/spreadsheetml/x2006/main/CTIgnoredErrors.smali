.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredErrors;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredErrors$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredErrors;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ctignorederrorsbebctype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredErrors;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewExtLst()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTExtensionList;
.end method

.method public abstract addNewIgnoredError()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;
.end method

.method public abstract getExtLst()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTExtensionList;
.end method

.method public abstract getIgnoredErrorArray(I)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;
.end method

.method public abstract getIgnoredErrorArray()[Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;
.end method

.method public abstract getIgnoredErrorList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;",
            ">;"
        }
    .end annotation
.end method

.method public abstract insertNewIgnoredError(I)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;
.end method

.method public abstract isSetExtLst()Z
.end method

.method public abstract removeIgnoredError(I)V
.end method

.method public abstract setExtLst(Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTExtensionList;)V
.end method

.method public abstract setIgnoredErrorArray(ILorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;)V
.end method

.method public abstract setIgnoredErrorArray([Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTIgnoredError;)V
.end method

.method public abstract sizeOfIgnoredErrorArray()I
.end method

.method public abstract unsetExtLst()V
.end method

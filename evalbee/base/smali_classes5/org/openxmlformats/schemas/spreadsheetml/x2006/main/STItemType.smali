.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Factory;,
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;
    }
.end annotation


# static fields
.field public static final AVG:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final BLANK:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final COUNT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final COUNT_A:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final DATA:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final DEFAULT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final GRAND:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final INT_AVG:I = 0x5

.field public static final INT_BLANK:I = 0xf

.field public static final INT_COUNT:I = 0x9

.field public static final INT_COUNT_A:I = 0x4

.field public static final INT_DATA:I = 0x1

.field public static final INT_DEFAULT:I = 0x2

.field public static final INT_GRAND:I = 0xe

.field public static final INT_MAX:I = 0x6

.field public static final INT_MIN:I = 0x7

.field public static final INT_PRODUCT:I = 0x8

.field public static final INT_STD_DEV:I = 0xa

.field public static final INT_STD_DEV_P:I = 0xb

.field public static final INT_SUM:I = 0x3

.field public static final INT_VAR:I = 0xc

.field public static final INT_VAR_P:I = 0xd

.field public static final MAX:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final MIN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final PRODUCT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final STD_DEV:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final STD_DEV_P:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final SUM:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final VAR:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final VAR_P:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stitemtype6186type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->type:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "data"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->DATA:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "default"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->DEFAULT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "sum"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->SUM:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "countA"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->COUNT_A:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "avg"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->AVG:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "max"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->MAX:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "min"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->MIN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "product"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->PRODUCT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "count"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->COUNT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "stdDev"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->STD_DEV:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "stdDevP"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->STD_DEV_P:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "var"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->VAR:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "varP"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->VAR_P:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "grand"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->GRAND:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    const-string v0, "blank"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;->BLANK:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;

    return-void
.end method


# virtual methods
.method public abstract enumValue()Lorg/apache/xmlbeans/StringEnumAbstractBase;
.end method

.method public abstract set(Lorg/apache/xmlbeans/StringEnumAbstractBase;)V
.end method

.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTPivotTableStyle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTPivotTableStyle$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTPivotTableStyle;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ctpivottablestyle0f84type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTPivotTableStyle;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getShowColHeaders()Z
.end method

.method public abstract getShowColStripes()Z
.end method

.method public abstract getShowLastColumn()Z
.end method

.method public abstract getShowRowHeaders()Z
.end method

.method public abstract getShowRowStripes()Z
.end method

.method public abstract isSetName()Z
.end method

.method public abstract isSetShowColHeaders()Z
.end method

.method public abstract isSetShowColStripes()Z
.end method

.method public abstract isSetShowLastColumn()Z
.end method

.method public abstract isSetShowRowHeaders()Z
.end method

.method public abstract isSetShowRowStripes()Z
.end method

.method public abstract setName(Ljava/lang/String;)V
.end method

.method public abstract setShowColHeaders(Z)V
.end method

.method public abstract setShowColStripes(Z)V
.end method

.method public abstract setShowLastColumn(Z)V
.end method

.method public abstract setShowRowHeaders(Z)V
.end method

.method public abstract setShowRowStripes(Z)V
.end method

.method public abstract unsetName()V
.end method

.method public abstract unsetShowColHeaders()V
.end method

.method public abstract unsetShowColStripes()V
.end method

.method public abstract unsetShowLastColumn()V
.end method

.method public abstract unsetShowRowHeaders()V
.end method

.method public abstract unsetShowRowStripes()V
.end method

.method public abstract xgetName()Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract xgetShowColHeaders()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetShowColStripes()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetShowLastColumn()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetShowRowHeaders()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetShowRowStripes()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xsetName(Lorg/apache/xmlbeans/XmlString;)V
.end method

.method public abstract xsetShowColHeaders(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetShowColStripes(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetShowLastColumn(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetShowRowHeaders(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetShowRowStripes(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

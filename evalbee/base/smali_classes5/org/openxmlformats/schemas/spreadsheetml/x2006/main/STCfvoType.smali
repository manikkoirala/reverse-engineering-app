.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Factory;,
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;
    }
.end annotation


# static fields
.field public static final FORMULA:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

.field public static final INT_FORMULA:I = 0x5

.field public static final INT_MAX:I = 0x3

.field public static final INT_MIN:I = 0x4

.field public static final INT_NUM:I = 0x1

.field public static final INT_PERCENT:I = 0x2

.field public static final INT_PERCENTILE:I = 0x6

.field public static final MAX:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

.field public static final MIN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

.field public static final NUM:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

.field public static final PERCENT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

.field public static final PERCENTILE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stcfvotypeeb0ftype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->type:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "num"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->NUM:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    const-string v0, "percent"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->PERCENT:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    const-string v0, "max"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->MAX:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    const-string v0, "min"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->MIN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    const-string v0, "formula"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->FORMULA:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    const-string v0, "percentile"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType;->PERCENTILE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STCfvoType$Enum;

    return-void
.end method


# virtual methods
.method public abstract enumValue()Lorg/apache/xmlbeans/StringEnumAbstractBase;
.end method

.method public abstract set(Lorg/apache/xmlbeans/StringEnumAbstractBase;)V
.end method

.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Factory;,
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;
    }
.end annotation


# static fields
.field public static final AXIS_COL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

.field public static final AXIS_PAGE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

.field public static final AXIS_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

.field public static final AXIS_VALUES:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

.field public static final INT_AXIS_COL:I = 0x2

.field public static final INT_AXIS_PAGE:I = 0x3

.field public static final INT_AXIS_ROW:I = 0x1

.field public static final INT_AXIS_VALUES:I = 0x4

.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "staxis45batype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;->type:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "axisRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;->AXIS_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    const-string v0, "axisCol"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;->AXIS_COL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    const-string v0, "axisPage"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;->AXIS_PAGE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    const-string v0, "axisValues"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis;->AXIS_VALUES:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STAxis$Enum;

    return-void
.end method


# virtual methods
.method public abstract enumValue()Lorg/apache/xmlbeans/StringEnumAbstractBase;
.end method

.method public abstract set(Lorg/apache/xmlbeans/StringEnumAbstractBase;)V
.end method

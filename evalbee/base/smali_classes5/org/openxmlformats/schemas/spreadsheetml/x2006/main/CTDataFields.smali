.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataFields;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataFields$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataFields;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ctdatafields52cctype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataFields;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewDataField()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;
.end method

.method public abstract getCount()J
.end method

.method public abstract getDataFieldArray(I)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;
.end method

.method public abstract getDataFieldArray()[Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;
.end method

.method public abstract getDataFieldList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;",
            ">;"
        }
    .end annotation
.end method

.method public abstract insertNewDataField(I)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;
.end method

.method public abstract isSetCount()Z
.end method

.method public abstract removeDataField(I)V
.end method

.method public abstract setCount(J)V
.end method

.method public abstract setDataFieldArray(ILorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;)V
.end method

.method public abstract setDataFieldArray([Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTDataField;)V
.end method

.method public abstract sizeOfDataFieldArray()I
.end method

.method public abstract unsetCount()V
.end method

.method public abstract xgetCount()Lorg/apache/xmlbeans/XmlUnsignedInt;
.end method

.method public abstract xsetCount(Lorg/apache/xmlbeans/XmlUnsignedInt;)V
.end method

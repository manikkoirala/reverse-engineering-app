.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Factory;,
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;
    }
.end annotation


# static fields
.field public static final BLANK_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_COLUMN_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_COLUMN_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_HEADER_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_ROW_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_ROW_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_SUBTOTAL_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_SUBTOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final FIRST_TOTAL_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final HEADER_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final INT_BLANK_ROW:I = 0x14

.field public static final INT_FIRST_COLUMN:I = 0x4

.field public static final INT_FIRST_COLUMN_STRIPE:I = 0x8

.field public static final INT_FIRST_COLUMN_SUBHEADING:I = 0x15

.field public static final INT_FIRST_HEADER_CELL:I = 0xa

.field public static final INT_FIRST_ROW_STRIPE:I = 0x6

.field public static final INT_FIRST_ROW_SUBHEADING:I = 0x18

.field public static final INT_FIRST_SUBTOTAL_COLUMN:I = 0xe

.field public static final INT_FIRST_SUBTOTAL_ROW:I = 0x11

.field public static final INT_FIRST_TOTAL_CELL:I = 0xc

.field public static final INT_HEADER_ROW:I = 0x2

.field public static final INT_LAST_COLUMN:I = 0x5

.field public static final INT_LAST_HEADER_CELL:I = 0xb

.field public static final INT_LAST_TOTAL_CELL:I = 0xd

.field public static final INT_PAGE_FIELD_LABELS:I = 0x1b

.field public static final INT_PAGE_FIELD_VALUES:I = 0x1c

.field public static final INT_SECOND_COLUMN_STRIPE:I = 0x9

.field public static final INT_SECOND_COLUMN_SUBHEADING:I = 0x16

.field public static final INT_SECOND_ROW_STRIPE:I = 0x7

.field public static final INT_SECOND_ROW_SUBHEADING:I = 0x19

.field public static final INT_SECOND_SUBTOTAL_COLUMN:I = 0xf

.field public static final INT_SECOND_SUBTOTAL_ROW:I = 0x12

.field public static final INT_THIRD_COLUMN_SUBHEADING:I = 0x17

.field public static final INT_THIRD_ROW_SUBHEADING:I = 0x1a

.field public static final INT_THIRD_SUBTOTAL_COLUMN:I = 0x10

.field public static final INT_THIRD_SUBTOTAL_ROW:I = 0x13

.field public static final INT_TOTAL_ROW:I = 0x3

.field public static final INT_WHOLE_TABLE:I = 0x1

.field public static final LAST_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final LAST_HEADER_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final LAST_TOTAL_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final PAGE_FIELD_LABELS:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final PAGE_FIELD_VALUES:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final SECOND_COLUMN_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final SECOND_COLUMN_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final SECOND_ROW_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final SECOND_ROW_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final SECOND_SUBTOTAL_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final SECOND_SUBTOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final THIRD_COLUMN_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final THIRD_ROW_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final THIRD_SUBTOTAL_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final THIRD_SUBTOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final TOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final WHOLE_TABLE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "sttablestyletype9936type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->type:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "wholeTable"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->WHOLE_TABLE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "headerRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->HEADER_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "totalRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->TOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstColumn"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "lastColumn"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->LAST_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstRowStripe"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_ROW_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "secondRowStripe"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->SECOND_ROW_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstColumnStripe"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_COLUMN_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "secondColumnStripe"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->SECOND_COLUMN_STRIPE:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstHeaderCell"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_HEADER_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "lastHeaderCell"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->LAST_HEADER_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstTotalCell"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_TOTAL_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "lastTotalCell"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->LAST_TOTAL_CELL:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstSubtotalColumn"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_SUBTOTAL_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "secondSubtotalColumn"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->SECOND_SUBTOTAL_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "thirdSubtotalColumn"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->THIRD_SUBTOTAL_COLUMN:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstSubtotalRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_SUBTOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "secondSubtotalRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->SECOND_SUBTOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "thirdSubtotalRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->THIRD_SUBTOTAL_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "blankRow"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->BLANK_ROW:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstColumnSubheading"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_COLUMN_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "secondColumnSubheading"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->SECOND_COLUMN_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "thirdColumnSubheading"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->THIRD_COLUMN_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "firstRowSubheading"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->FIRST_ROW_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "secondRowSubheading"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->SECOND_ROW_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "thirdRowSubheading"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->THIRD_ROW_SUBHEADING:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "pageFieldLabels"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->PAGE_FIELD_LABELS:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    const-string v0, "pageFieldValues"

    invoke-static {v0}, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType;->PAGE_FIELD_VALUES:Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STTableStyleType$Enum;

    return-void
.end method


# virtual methods
.method public abstract enumValue()Lorg/apache/xmlbeans/StringEnumAbstractBase;
.end method

.method public abstract set(Lorg/apache/xmlbeans/StringEnumAbstractBase;)V
.end method

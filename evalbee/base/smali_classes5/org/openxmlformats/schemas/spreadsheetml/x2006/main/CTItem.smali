.class public interface abstract Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTItem$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ctitemc69ctype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/CTItem;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract getC()Z
.end method

.method public abstract getD()Z
.end method

.method public abstract getE()Z
.end method

.method public abstract getF()Z
.end method

.method public abstract getH()Z
.end method

.method public abstract getM()Z
.end method

.method public abstract getN()Ljava/lang/String;
.end method

.method public abstract getS()Z
.end method

.method public abstract getSd()Z
.end method

.method public abstract getT()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;
.end method

.method public abstract getX()J
.end method

.method public abstract isSetC()Z
.end method

.method public abstract isSetD()Z
.end method

.method public abstract isSetE()Z
.end method

.method public abstract isSetF()Z
.end method

.method public abstract isSetH()Z
.end method

.method public abstract isSetM()Z
.end method

.method public abstract isSetN()Z
.end method

.method public abstract isSetS()Z
.end method

.method public abstract isSetSd()Z
.end method

.method public abstract isSetT()Z
.end method

.method public abstract isSetX()Z
.end method

.method public abstract setC(Z)V
.end method

.method public abstract setD(Z)V
.end method

.method public abstract setE(Z)V
.end method

.method public abstract setF(Z)V
.end method

.method public abstract setH(Z)V
.end method

.method public abstract setM(Z)V
.end method

.method public abstract setN(Ljava/lang/String;)V
.end method

.method public abstract setS(Z)V
.end method

.method public abstract setSd(Z)V
.end method

.method public abstract setT(Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType$Enum;)V
.end method

.method public abstract setX(J)V
.end method

.method public abstract unsetC()V
.end method

.method public abstract unsetD()V
.end method

.method public abstract unsetE()V
.end method

.method public abstract unsetF()V
.end method

.method public abstract unsetH()V
.end method

.method public abstract unsetM()V
.end method

.method public abstract unsetN()V
.end method

.method public abstract unsetS()V
.end method

.method public abstract unsetSd()V
.end method

.method public abstract unsetT()V
.end method

.method public abstract unsetX()V
.end method

.method public abstract xgetC()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetD()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetE()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetF()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetH()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetM()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetN()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STXstring;
.end method

.method public abstract xgetS()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetSd()Lorg/apache/xmlbeans/XmlBoolean;
.end method

.method public abstract xgetT()Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;
.end method

.method public abstract xgetX()Lorg/apache/xmlbeans/XmlUnsignedInt;
.end method

.method public abstract xsetC(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetD(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetE(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetF(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetH(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetM(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetN(Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STXstring;)V
.end method

.method public abstract xsetS(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetSd(Lorg/apache/xmlbeans/XmlBoolean;)V
.end method

.method public abstract xsetT(Lorg/openxmlformats/schemas/spreadsheetml/x2006/main/STItemType;)V
.end method

.method public abstract xsetX(Lorg/apache/xmlbeans/XmlUnsignedInt;)V
.end method

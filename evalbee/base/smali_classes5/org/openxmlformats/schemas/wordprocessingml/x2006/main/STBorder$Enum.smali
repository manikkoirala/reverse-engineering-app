.class public final Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;
.super Lorg/apache/xmlbeans/StringEnumAbstractBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Enum"
.end annotation


# static fields
.field static final INT_APPLES:I = 0x1c

.field static final INT_ARCHED_SCALLOPS:I = 0x1d

.field static final INT_BABY_PACIFIER:I = 0x1e

.field static final INT_BABY_RATTLE:I = 0x1f

.field static final INT_BALLOONS_3_COLORS:I = 0x20

.field static final INT_BALLOONS_HOT_AIR:I = 0x21

.field static final INT_BASIC_BLACK_DASHES:I = 0x22

.field static final INT_BASIC_BLACK_DOTS:I = 0x23

.field static final INT_BASIC_BLACK_SQUARES:I = 0x24

.field static final INT_BASIC_THIN_LINES:I = 0x25

.field static final INT_BASIC_WHITE_DASHES:I = 0x26

.field static final INT_BASIC_WHITE_DOTS:I = 0x27

.field static final INT_BASIC_WHITE_SQUARES:I = 0x28

.field static final INT_BASIC_WIDE_INLINE:I = 0x29

.field static final INT_BASIC_WIDE_MIDLINE:I = 0x2a

.field static final INT_BASIC_WIDE_OUTLINE:I = 0x2b

.field static final INT_BATS:I = 0x2c

.field static final INT_BIRDS:I = 0x2d

.field static final INT_BIRDS_FLIGHT:I = 0x2e

.field static final INT_CABINS:I = 0x2f

.field static final INT_CAKE_SLICE:I = 0x30

.field static final INT_CANDY_CORN:I = 0x31

.field static final INT_CELTIC_KNOTWORK:I = 0x32

.field static final INT_CERTIFICATE_BANNER:I = 0x33

.field static final INT_CHAIN_LINK:I = 0x34

.field static final INT_CHAMPAGNE_BOTTLE:I = 0x35

.field static final INT_CHECKED_BAR_BLACK:I = 0x36

.field static final INT_CHECKED_BAR_COLOR:I = 0x37

.field static final INT_CHECKERED:I = 0x38

.field static final INT_CHRISTMAS_TREE:I = 0x39

.field static final INT_CIRCLES_LINES:I = 0x3a

.field static final INT_CIRCLES_RECTANGLES:I = 0x3b

.field static final INT_CLASSICAL_WAVE:I = 0x3c

.field static final INT_CLOCKS:I = 0x3d

.field static final INT_COMPASS:I = 0x3e

.field static final INT_CONFETTI:I = 0x3f

.field static final INT_CONFETTI_GRAYS:I = 0x40

.field static final INT_CONFETTI_OUTLINE:I = 0x41

.field static final INT_CONFETTI_STREAMERS:I = 0x42

.field static final INT_CONFETTI_WHITE:I = 0x43

.field static final INT_CORNER_TRIANGLES:I = 0x44

.field static final INT_COUPON_CUTOUT_DASHES:I = 0x45

.field static final INT_COUPON_CUTOUT_DOTS:I = 0x46

.field static final INT_CRAZY_MAZE:I = 0x47

.field static final INT_CREATURES_BUTTERFLY:I = 0x48

.field static final INT_CREATURES_FISH:I = 0x49

.field static final INT_CREATURES_INSECTS:I = 0x4a

.field static final INT_CREATURES_LADY_BUG:I = 0x4b

.field static final INT_CROSS_STITCH:I = 0x4c

.field static final INT_CUP:I = 0x4d

.field static final INT_DASHED:I = 0x7

.field static final INT_DASH_DOT_STROKED:I = 0x17

.field static final INT_DASH_SMALL_GAP:I = 0x16

.field static final INT_DECO_ARCH:I = 0x4e

.field static final INT_DECO_ARCH_COLOR:I = 0x4f

.field static final INT_DECO_BLOCKS:I = 0x50

.field static final INT_DIAMONDS_GRAY:I = 0x51

.field static final INT_DOTTED:I = 0x6

.field static final INT_DOT_DASH:I = 0x8

.field static final INT_DOT_DOT_DASH:I = 0x9

.field static final INT_DOUBLE:I = 0x5

.field static final INT_DOUBLE_D:I = 0x52

.field static final INT_DOUBLE_DIAMONDS:I = 0x53

.field static final INT_DOUBLE_WAVE:I = 0x15

.field static final INT_EARTH_1:I = 0x54

.field static final INT_EARTH_2:I = 0x55

.field static final INT_ECLIPSING_SQUARES_1:I = 0x56

.field static final INT_ECLIPSING_SQUARES_2:I = 0x57

.field static final INT_EGGS_BLACK:I = 0x58

.field static final INT_FANS:I = 0x59

.field static final INT_FILM:I = 0x5a

.field static final INT_FIRECRACKERS:I = 0x5b

.field static final INT_FLOWERS_BLOCK_PRINT:I = 0x5c

.field static final INT_FLOWERS_DAISIES:I = 0x5d

.field static final INT_FLOWERS_MODERN_1:I = 0x5e

.field static final INT_FLOWERS_MODERN_2:I = 0x5f

.field static final INT_FLOWERS_PANSY:I = 0x60

.field static final INT_FLOWERS_RED_ROSE:I = 0x61

.field static final INT_FLOWERS_ROSES:I = 0x62

.field static final INT_FLOWERS_TEACUP:I = 0x63

.field static final INT_FLOWERS_TINY:I = 0x64

.field static final INT_GEMS:I = 0x65

.field static final INT_GINGERBREAD_MAN:I = 0x66

.field static final INT_GRADIENT:I = 0x67

.field static final INT_HANDMADE_1:I = 0x68

.field static final INT_HANDMADE_2:I = 0x69

.field static final INT_HEARTS:I = 0x6c

.field static final INT_HEART_BALLOON:I = 0x6a

.field static final INT_HEART_GRAY:I = 0x6b

.field static final INT_HEEBIE_JEEBIES:I = 0x6d

.field static final INT_HOLLY:I = 0x6e

.field static final INT_HOUSE_FUNKY:I = 0x6f

.field static final INT_HYPNOTIC:I = 0x70

.field static final INT_ICE_CREAM_CONES:I = 0x71

.field static final INT_INSET:I = 0x1b

.field static final INT_LIGHTNING_1:I = 0x73

.field static final INT_LIGHTNING_2:I = 0x74

.field static final INT_LIGHT_BULB:I = 0x72

.field static final INT_MAPLE_LEAF:I = 0x76

.field static final INT_MAPLE_MUFFINS:I = 0x77

.field static final INT_MAP_PINS:I = 0x75

.field static final INT_MARQUEE:I = 0x78

.field static final INT_MARQUEE_TOOTHED:I = 0x79

.field static final INT_MOONS:I = 0x7a

.field static final INT_MOSAIC:I = 0x7b

.field static final INT_MUSIC_NOTES:I = 0x7c

.field static final INT_NIL:I = 0x1

.field static final INT_NONE:I = 0x2

.field static final INT_NORTHWEST:I = 0x7d

.field static final INT_OUTSET:I = 0x1a

.field static final INT_OVALS:I = 0x7e

.field static final INT_PACKAGES:I = 0x7f

.field static final INT_PALMS_BLACK:I = 0x80

.field static final INT_PALMS_COLOR:I = 0x81

.field static final INT_PAPER_CLIPS:I = 0x82

.field static final INT_PAPYRUS:I = 0x83

.field static final INT_PARTY_FAVOR:I = 0x84

.field static final INT_PARTY_GLASS:I = 0x85

.field static final INT_PENCILS:I = 0x86

.field static final INT_PEOPLE:I = 0x87

.field static final INT_PEOPLE_HATS:I = 0x89

.field static final INT_PEOPLE_WAVING:I = 0x88

.field static final INT_POINSETTIAS:I = 0x8a

.field static final INT_POSTAGE_STAMP:I = 0x8b

.field static final INT_PUMPKIN_1:I = 0x8c

.field static final INT_PUSH_PIN_NOTE_1:I = 0x8e

.field static final INT_PUSH_PIN_NOTE_2:I = 0x8d

.field static final INT_PYRAMIDS:I = 0x8f

.field static final INT_PYRAMIDS_ABOVE:I = 0x90

.field static final INT_QUADRANTS:I = 0x91

.field static final INT_RINGS:I = 0x92

.field static final INT_SAFARI:I = 0x93

.field static final INT_SAWTOOTH:I = 0x94

.field static final INT_SAWTOOTH_GRAY:I = 0x95

.field static final INT_SCARED_CAT:I = 0x96

.field static final INT_SEATTLE:I = 0x97

.field static final INT_SHADOWED_SQUARES:I = 0x98

.field static final INT_SHARKS_TEETH:I = 0x99

.field static final INT_SHOREBIRD_TRACKS:I = 0x9a

.field static final INT_SINGLE:I = 0x3

.field static final INT_SKYROCKET:I = 0x9b

.field static final INT_SNOWFLAKES:I = 0x9d

.field static final INT_SNOWFLAKE_FANCY:I = 0x9c

.field static final INT_SOMBRERO:I = 0x9e

.field static final INT_SOUTHWEST:I = 0x9f

.field static final INT_STARS:I = 0xa0

.field static final INT_STARS_3_D:I = 0xa2

.field static final INT_STARS_BLACK:I = 0xa3

.field static final INT_STARS_SHADOWED:I = 0xa4

.field static final INT_STARS_TOP:I = 0xa1

.field static final INT_SUN:I = 0xa5

.field static final INT_SWIRLIGIG:I = 0xa6

.field static final INT_THICK:I = 0x4

.field static final INT_THICK_THIN_LARGE_GAP:I = 0x12

.field static final INT_THICK_THIN_MEDIUM_GAP:I = 0xf

.field static final INT_THICK_THIN_SMALL_GAP:I = 0xc

.field static final INT_THIN_THICK_LARGE_GAP:I = 0x11

.field static final INT_THIN_THICK_MEDIUM_GAP:I = 0xe

.field static final INT_THIN_THICK_SMALL_GAP:I = 0xb

.field static final INT_THIN_THICK_THIN_LARGE_GAP:I = 0x13

.field static final INT_THIN_THICK_THIN_MEDIUM_GAP:I = 0x10

.field static final INT_THIN_THICK_THIN_SMALL_GAP:I = 0xd

.field static final INT_THREE_D_EMBOSS:I = 0x18

.field static final INT_THREE_D_ENGRAVE:I = 0x19

.field static final INT_TORN_PAPER:I = 0xa7

.field static final INT_TORN_PAPER_BLACK:I = 0xa8

.field static final INT_TREES:I = 0xa9

.field static final INT_TRIANGLES:I = 0xab

.field static final INT_TRIANGLE_PARTY:I = 0xaa

.field static final INT_TRIBAL_1:I = 0xac

.field static final INT_TRIBAL_2:I = 0xad

.field static final INT_TRIBAL_3:I = 0xae

.field static final INT_TRIBAL_4:I = 0xaf

.field static final INT_TRIBAL_5:I = 0xb0

.field static final INT_TRIBAL_6:I = 0xb1

.field static final INT_TRIPLE:I = 0xa

.field static final INT_TWISTED_LINES_1:I = 0xb2

.field static final INT_TWISTED_LINES_2:I = 0xb3

.field static final INT_VINE:I = 0xb4

.field static final INT_WAVE:I = 0x14

.field static final INT_WAVELINE:I = 0xb5

.field static final INT_WEAVING_ANGLES:I = 0xb6

.field static final INT_WEAVING_BRAID:I = 0xb7

.field static final INT_WEAVING_RIBBON:I = 0xb8

.field static final INT_WEAVING_STRIPS:I = 0xb9

.field static final INT_WHITE_FLOWERS:I = 0xba

.field static final INT_WOODWORK:I = 0xbb

.field static final INT_X_ILLUSIONS:I = 0xbc

.field static final INT_ZANY_TRIANGLES:I = 0xbd

.field static final INT_ZIG_ZAG:I = 0xbe

.field static final INT_ZIG_ZAG_STITCH:I = 0xbf

.field private static final serialVersionUID:J = 0x1L

.field public static final table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;


# direct methods
.method public static constructor <clinit>()V
    .locals 195

    new-instance v0, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    new-instance v2, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v1, v2

    const-string v3, "nil"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v3, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v2, v3

    const-string v4, "none"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v3, v4

    const-string v5, "single"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v5, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v4, v5

    const-string v6, "thick"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v6, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v5, v6

    const-string v7, "double"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v7, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v6, v7

    const-string v8, "dotted"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v8, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v7, v8

    const-string v9, "dashed"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v9, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v8, v9

    const-string v10, "dotDash"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v10, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v9, v10

    const-string v11, "dotDotDash"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v11, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v10, v11

    const-string v12, "triple"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v12, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v11, v12

    const-string v13, "thinThickSmallGap"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v13, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v12, v13

    const-string v14, "thickThinSmallGap"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v14, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v13, v14

    const-string v15, "thinThickThinSmallGap"

    move-object/from16 v192, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v14, v0

    const-string v15, "thinThickMediumGap"

    move-object/from16 v193, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object v15, v0

    const-string v1, "thickThinMediumGap"

    move-object/from16 v194, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v16, v0

    const-string v1, "thinThickThinMediumGap"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v17, v0

    const-string v1, "thinThickLargeGap"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v18, v0

    const-string v1, "thickThinLargeGap"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v19, v0

    const-string v1, "thinThickThinLargeGap"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v20, v0

    const-string v1, "wave"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v21, v0

    const-string v1, "doubleWave"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v22, v0

    const-string v1, "dashSmallGap"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v23, v0

    const-string v1, "dashDotStroked"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v24, v0

    const-string v1, "threeDEmboss"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v25, v0

    const-string v1, "threeDEngrave"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v26, v0

    const-string v1, "outset"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v27, v0

    const-string v1, "inset"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v28, v0

    const-string v1, "apples"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v29, v0

    const-string v1, "archedScallops"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v30, v0

    const-string v1, "babyPacifier"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v31, v0

    const-string v1, "babyRattle"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v32, v0

    const-string v1, "balloons3Colors"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v33, v0

    const-string v1, "balloonsHotAir"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v34, v0

    const-string v1, "basicBlackDashes"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v35, v0

    const-string v1, "basicBlackDots"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v36, v0

    const-string v1, "basicBlackSquares"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v37, v0

    const-string v1, "basicThinLines"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v38, v0

    const-string v1, "basicWhiteDashes"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v39, v0

    const-string v1, "basicWhiteDots"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v40, v0

    const-string v1, "basicWhiteSquares"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v41, v0

    const-string v1, "basicWideInline"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v42, v0

    const-string v1, "basicWideMidline"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v43, v0

    const-string v1, "basicWideOutline"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v44, v0

    const-string v1, "bats"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v45, v0

    const-string v1, "birds"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v46, v0

    const-string v1, "birdsFlight"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v47, v0

    const-string v1, "cabins"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v48, v0

    const-string v1, "cakeSlice"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v49, v0

    const-string v1, "candyCorn"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v50, v0

    const-string v1, "celticKnotwork"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v51, v0

    const-string v1, "certificateBanner"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v52, v0

    const-string v1, "chainLink"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v53, v0

    const-string v1, "champagneBottle"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v54, v0

    const-string v1, "checkedBarBlack"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v55, v0

    const-string v1, "checkedBarColor"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v56, v0

    const-string v1, "checkered"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v57, v0

    const-string v1, "christmasTree"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v58, v0

    const-string v1, "circlesLines"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v59, v0

    const-string v1, "circlesRectangles"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v60, v0

    const-string v1, "classicalWave"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v61, v0

    const-string v1, "clocks"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v62, v0

    const-string v1, "compass"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v63, v0

    const-string v1, "confetti"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v64, v0

    const-string v1, "confettiGrays"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v65, v0

    const-string v1, "confettiOutline"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v66, v0

    const-string v1, "confettiStreamers"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v67, v0

    const-string v1, "confettiWhite"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v68, v0

    const-string v1, "cornerTriangles"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v69, v0

    const-string v1, "couponCutoutDashes"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v70, v0

    const-string v1, "couponCutoutDots"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v71, v0

    const-string v1, "crazyMaze"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v72, v0

    const-string v1, "creaturesButterfly"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v73, v0

    const-string v1, "creaturesFish"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v74, v0

    const-string v1, "creaturesInsects"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v75, v0

    const-string v1, "creaturesLadyBug"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v76, v0

    const-string v1, "crossStitch"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v77, v0

    const-string v1, "cup"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v78, v0

    const-string v1, "decoArch"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v79, v0

    const-string v1, "decoArchColor"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v80, v0

    const-string v1, "decoBlocks"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v81, v0

    const-string v1, "diamondsGray"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v82, v0

    const-string v1, "doubleD"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v83, v0

    const-string v1, "doubleDiamonds"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v84, v0

    const-string v1, "earth1"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v85, v0

    const-string v1, "earth2"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v86, v0

    const-string v1, "eclipsingSquares1"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v87, v0

    const-string v1, "eclipsingSquares2"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v88, v0

    const-string v1, "eggsBlack"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v89, v0

    const-string v1, "fans"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v90, v0

    const-string v1, "film"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v91, v0

    const-string v1, "firecrackers"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v92, v0

    const-string v1, "flowersBlockPrint"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v93, v0

    const-string v1, "flowersDaisies"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v94, v0

    const-string v1, "flowersModern1"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v95, v0

    const-string v1, "flowersModern2"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v96, v0

    const-string v1, "flowersPansy"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v97, v0

    const-string v1, "flowersRedRose"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v98, v0

    const-string v1, "flowersRoses"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v99, v0

    const-string v1, "flowersTeacup"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v100, v0

    const-string v1, "flowersTiny"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v101, v0

    const-string v1, "gems"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v102, v0

    const-string v1, "gingerbreadMan"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v103, v0

    const-string v1, "gradient"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v104, v0

    const-string v1, "handmade1"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v105, v0

    const-string v1, "handmade2"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v106, v0

    const-string v1, "heartBalloon"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v107, v0

    const-string v1, "heartGray"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v108, v0

    const-string v1, "hearts"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v109, v0

    const-string v1, "heebieJeebies"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v110, v0

    const-string v1, "holly"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v111, v0

    const-string v1, "houseFunky"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v112, v0

    const-string v1, "hypnotic"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v113, v0

    const-string v1, "iceCreamCones"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v114, v0

    const-string v1, "lightBulb"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v115, v0

    const-string v1, "lightning1"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v116, v0

    const-string v1, "lightning2"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v117, v0

    const-string v1, "mapPins"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v118, v0

    const-string v1, "mapleLeaf"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v119, v0

    const-string v1, "mapleMuffins"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v120, v0

    const-string v1, "marquee"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v121, v0

    const-string v1, "marqueeToothed"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v122, v0

    const-string v1, "moons"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v123, v0

    const-string v1, "mosaic"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v124, v0

    const-string v1, "musicNotes"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v125, v0

    const-string v1, "northwest"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v126, v0

    const-string v1, "ovals"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v127, v0

    const-string v1, "packages"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v128, v0

    const-string v1, "palmsBlack"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v129, v0

    const-string v1, "palmsColor"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v130, v0

    const-string v1, "paperClips"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v131, v0

    const-string v1, "papyrus"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v132, v0

    const-string v1, "partyFavor"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v133, v0

    const-string v1, "partyGlass"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v134, v0

    const-string v1, "pencils"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v135, v0

    const-string v1, "people"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v136, v0

    const-string v1, "peopleWaving"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v137, v0

    const-string v1, "peopleHats"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v138, v0

    const-string v1, "poinsettias"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v139, v0

    const-string v1, "postageStamp"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v140, v0

    const-string v1, "pumpkin1"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v141, v0

    const-string v1, "pushPinNote2"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v142, v0

    const-string v1, "pushPinNote1"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v143, v0

    const-string v1, "pyramids"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v144, v0

    const-string v1, "pyramidsAbove"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v145, v0

    const-string v1, "quadrants"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v146, v0

    const-string v1, "rings"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v147, v0

    const-string v1, "safari"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v148, v0

    const-string v1, "sawtooth"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v149, v0

    const-string v1, "sawtoothGray"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v150, v0

    const-string v1, "scaredCat"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v151, v0

    const-string v1, "seattle"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v152, v0

    const-string v1, "shadowedSquares"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v153, v0

    const-string v1, "sharksTeeth"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v154, v0

    const-string v1, "shorebirdTracks"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v155, v0

    const-string v1, "skyrocket"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v156, v0

    const-string v1, "snowflakeFancy"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v157, v0

    const-string v1, "snowflakes"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v158, v0

    const-string v1, "sombrero"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v159, v0

    const-string v1, "southwest"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v160, v0

    const-string v1, "stars"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v161, v0

    const-string v1, "starsTop"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v162, v0

    const-string v1, "stars3d"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v163, v0

    const-string v1, "starsBlack"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v164, v0

    const-string v1, "starsShadowed"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v165, v0

    const-string v1, "sun"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v166, v0

    const-string v1, "swirligig"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v167, v0

    const-string v1, "tornPaper"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v168, v0

    const-string v1, "tornPaperBlack"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v169, v0

    const-string v1, "trees"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v170, v0

    const-string v1, "triangleParty"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v171, v0

    const-string v1, "triangles"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v172, v0

    const-string v1, "tribal1"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v173, v0

    const-string v1, "tribal2"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v174, v0

    const-string v1, "tribal3"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v175, v0

    const-string v1, "tribal4"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v176, v0

    const-string v1, "tribal5"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v177, v0

    const-string v1, "tribal6"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v178, v0

    const-string v1, "twistedLines1"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v179, v0

    const-string v1, "twistedLines2"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v180, v0

    const-string v1, "vine"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v181, v0

    const-string v1, "waveline"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v182, v0

    const-string v1, "weavingAngles"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v183, v0

    const-string v1, "weavingBraid"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v184, v0

    const-string v1, "weavingRibbon"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v185, v0

    const-string v1, "weavingStrips"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v186, v0

    const-string v1, "whiteFlowers"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v187, v0

    const-string v1, "woodwork"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v188, v0

    const-string v1, "xIllusions"

    const/16 v2, 0xbc

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v189, v0

    const-string v1, "zanyTriangles"

    const/16 v2, 0xbd

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v190, v0

    const-string v1, "zigZag"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-object/from16 v191, v0

    const-string v1, "zigZagStitch"

    const/16 v2, 0xbf

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;-><init>(Ljava/lang/String;I)V

    move-object/from16 v1, v193

    move-object/from16 v2, v194

    filled-new-array/range {v1 .. v191}, [Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-result-object v0

    move-object/from16 v1, v192

    invoke-direct {v1, v0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;-><init>([Lorg/apache/xmlbeans/StringEnumAbstractBase;)V

    sput-object v1, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/xmlbeans/StringEnumAbstractBase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forInt(I)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    return-object p0
.end method

.method public static forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    return-object p0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase;->intValue()I

    move-result v0

    invoke-static {v0}, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;->forInt(I)Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/STBorder$Enum;

    move-result-object v0

    return-object v0
.end method

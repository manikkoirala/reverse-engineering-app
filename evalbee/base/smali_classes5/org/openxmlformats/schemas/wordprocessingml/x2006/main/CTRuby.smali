.class public interface abstract Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRuby;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRuby$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRuby;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "ctruby9dcetype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRuby;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewRt()Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyContent;
.end method

.method public abstract addNewRubyBase()Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyContent;
.end method

.method public abstract addNewRubyPr()Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyPr;
.end method

.method public abstract getRt()Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyContent;
.end method

.method public abstract getRubyBase()Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyContent;
.end method

.method public abstract getRubyPr()Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyPr;
.end method

.method public abstract setRt(Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyContent;)V
.end method

.method public abstract setRubyBase(Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyContent;)V
.end method

.method public abstract setRubyPr(Lorg/openxmlformats/schemas/wordprocessingml/x2006/main/CTRubyPr;)V
.end method

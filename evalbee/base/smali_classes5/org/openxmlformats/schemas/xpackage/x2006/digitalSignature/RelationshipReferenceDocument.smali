.class public interface abstract Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/RelationshipReferenceDocument;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/RelationshipReferenceDocument$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/RelationshipReferenceDocument;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "relationshipreference8903doctype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/RelationshipReferenceDocument;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewRelationshipReference()Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/CTRelationshipReference;
.end method

.method public abstract getRelationshipReference()Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/CTRelationshipReference;
.end method

.method public abstract setRelationshipReference(Lorg/openxmlformats/schemas/xpackage/x2006/digitalSignature/CTRelationshipReference;)V
.end method

.class public interface abstract Lorg/w3/x2000/x09/xmldsig/TransformType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/w3/x2000/x09/xmldsig/TransformType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/w3/x2000/x09/xmldsig/TransformType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "transformtype550btype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/w3/x2000/x09/xmldsig/TransformType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewXPath()Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract addXPath(Ljava/lang/String;)V
.end method

.method public abstract getAlgorithm()Ljava/lang/String;
.end method

.method public abstract getXPathArray(I)Ljava/lang/String;
.end method

.method public abstract getXPathArray()[Ljava/lang/String;
.end method

.method public abstract getXPathList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract insertNewXPath(I)Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract insertXPath(ILjava/lang/String;)V
.end method

.method public abstract removeXPath(I)V
.end method

.method public abstract setAlgorithm(Ljava/lang/String;)V
.end method

.method public abstract setXPathArray(ILjava/lang/String;)V
.end method

.method public abstract setXPathArray([Ljava/lang/String;)V
.end method

.method public abstract sizeOfXPathArray()I
.end method

.method public abstract xgetAlgorithm()Lorg/apache/xmlbeans/XmlAnyURI;
.end method

.method public abstract xgetXPathArray(I)Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract xgetXPathArray()[Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract xgetXPathList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/xmlbeans/XmlString;",
            ">;"
        }
    .end annotation
.end method

.method public abstract xsetAlgorithm(Lorg/apache/xmlbeans/XmlAnyURI;)V
.end method

.method public abstract xsetXPathArray(ILorg/apache/xmlbeans/XmlString;)V
.end method

.method public abstract xsetXPathArray([Lorg/apache/xmlbeans/XmlString;)V
.end method

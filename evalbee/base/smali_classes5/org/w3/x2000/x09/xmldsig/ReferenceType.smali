.class public interface abstract Lorg/w3/x2000/x09/xmldsig/ReferenceType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/w3/x2000/x09/xmldsig/ReferenceType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/w3/x2000/x09/xmldsig/ReferenceType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "referencetypef44ctype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/w3/x2000/x09/xmldsig/ReferenceType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewDigestMethod()Lorg/w3/x2000/x09/xmldsig/DigestMethodType;
.end method

.method public abstract addNewTransforms()Lorg/w3/x2000/x09/xmldsig/TransformsType;
.end method

.method public abstract getDigestMethod()Lorg/w3/x2000/x09/xmldsig/DigestMethodType;
.end method

.method public abstract getDigestValue()[B
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getTransforms()Lorg/w3/x2000/x09/xmldsig/TransformsType;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract getURI()Ljava/lang/String;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetTransforms()Z
.end method

.method public abstract isSetType()Z
.end method

.method public abstract isSetURI()Z
.end method

.method public abstract setDigestMethod(Lorg/w3/x2000/x09/xmldsig/DigestMethodType;)V
.end method

.method public abstract setDigestValue([B)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setTransforms(Lorg/w3/x2000/x09/xmldsig/TransformsType;)V
.end method

.method public abstract setType(Ljava/lang/String;)V
.end method

.method public abstract setURI(Ljava/lang/String;)V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetTransforms()V
.end method

.method public abstract unsetType()V
.end method

.method public abstract unsetURI()V
.end method

.method public abstract xgetDigestValue()Lorg/w3/x2000/x09/xmldsig/DigestValueType;
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xgetType()Lorg/apache/xmlbeans/XmlAnyURI;
.end method

.method public abstract xgetURI()Lorg/apache/xmlbeans/XmlAnyURI;
.end method

.method public abstract xsetDigestValue(Lorg/w3/x2000/x09/xmldsig/DigestValueType;)V
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

.method public abstract xsetType(Lorg/apache/xmlbeans/XmlAnyURI;)V
.end method

.method public abstract xsetURI(Lorg/apache/xmlbeans/XmlAnyURI;)V
.end method

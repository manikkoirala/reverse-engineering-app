.class public interface abstract Lorg/w3/x2000/x09/xmldsig/SignedInfoType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/w3/x2000/x09/xmldsig/SignedInfoType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/w3/x2000/x09/xmldsig/SignedInfoType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "signedinfotype54dbtype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/w3/x2000/x09/xmldsig/SignedInfoType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewCanonicalizationMethod()Lorg/w3/x2000/x09/xmldsig/CanonicalizationMethodType;
.end method

.method public abstract addNewReference()Lorg/w3/x2000/x09/xmldsig/ReferenceType;
.end method

.method public abstract addNewSignatureMethod()Lorg/w3/x2000/x09/xmldsig/SignatureMethodType;
.end method

.method public abstract getCanonicalizationMethod()Lorg/w3/x2000/x09/xmldsig/CanonicalizationMethodType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getReferenceArray(I)Lorg/w3/x2000/x09/xmldsig/ReferenceType;
.end method

.method public abstract getReferenceArray()[Lorg/w3/x2000/x09/xmldsig/ReferenceType;
.end method

.method public abstract getReferenceList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/w3/x2000/x09/xmldsig/ReferenceType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSignatureMethod()Lorg/w3/x2000/x09/xmldsig/SignatureMethodType;
.end method

.method public abstract insertNewReference(I)Lorg/w3/x2000/x09/xmldsig/ReferenceType;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract removeReference(I)V
.end method

.method public abstract setCanonicalizationMethod(Lorg/w3/x2000/x09/xmldsig/CanonicalizationMethodType;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setReferenceArray(ILorg/w3/x2000/x09/xmldsig/ReferenceType;)V
.end method

.method public abstract setReferenceArray([Lorg/w3/x2000/x09/xmldsig/ReferenceType;)V
.end method

.method public abstract setSignatureMethod(Lorg/w3/x2000/x09/xmldsig/SignatureMethodType;)V
.end method

.method public abstract sizeOfReferenceArray()I
.end method

.method public abstract unsetId()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

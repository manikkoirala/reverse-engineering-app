.class public interface abstract Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "x509issuerserialtype7eb2type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract getX509IssuerName()Ljava/lang/String;
.end method

.method public abstract getX509SerialNumber()Ljava/math/BigInteger;
.end method

.method public abstract setX509IssuerName(Ljava/lang/String;)V
.end method

.method public abstract setX509SerialNumber(Ljava/math/BigInteger;)V
.end method

.method public abstract xgetX509IssuerName()Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract xgetX509SerialNumber()Lorg/apache/xmlbeans/XmlInteger;
.end method

.method public abstract xsetX509IssuerName(Lorg/apache/xmlbeans/XmlString;)V
.end method

.method public abstract xsetX509SerialNumber(Lorg/apache/xmlbeans/XmlInteger;)V
.end method

.class public interface abstract Lorg/w3/x2000/x09/xmldsig/SignatureType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/w3/x2000/x09/xmldsig/SignatureType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/w3/x2000/x09/xmldsig/SignatureType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "signaturetype0a3ftype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/w3/x2000/x09/xmldsig/SignatureType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewKeyInfo()Lorg/w3/x2000/x09/xmldsig/KeyInfoType;
.end method

.method public abstract addNewObject()Lorg/w3/x2000/x09/xmldsig/ObjectType;
.end method

.method public abstract addNewSignatureValue()Lorg/w3/x2000/x09/xmldsig/SignatureValueType;
.end method

.method public abstract addNewSignedInfo()Lorg/w3/x2000/x09/xmldsig/SignedInfoType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getKeyInfo()Lorg/w3/x2000/x09/xmldsig/KeyInfoType;
.end method

.method public abstract getObjectArray(I)Lorg/w3/x2000/x09/xmldsig/ObjectType;
.end method

.method public abstract getObjectArray()[Lorg/w3/x2000/x09/xmldsig/ObjectType;
.end method

.method public abstract getObjectList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/w3/x2000/x09/xmldsig/ObjectType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSignatureValue()Lorg/w3/x2000/x09/xmldsig/SignatureValueType;
.end method

.method public abstract getSignedInfo()Lorg/w3/x2000/x09/xmldsig/SignedInfoType;
.end method

.method public abstract insertNewObject(I)Lorg/w3/x2000/x09/xmldsig/ObjectType;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetKeyInfo()Z
.end method

.method public abstract removeObject(I)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setKeyInfo(Lorg/w3/x2000/x09/xmldsig/KeyInfoType;)V
.end method

.method public abstract setObjectArray(ILorg/w3/x2000/x09/xmldsig/ObjectType;)V
.end method

.method public abstract setObjectArray([Lorg/w3/x2000/x09/xmldsig/ObjectType;)V
.end method

.method public abstract setSignatureValue(Lorg/w3/x2000/x09/xmldsig/SignatureValueType;)V
.end method

.method public abstract setSignedInfo(Lorg/w3/x2000/x09/xmldsig/SignedInfoType;)V
.end method

.method public abstract sizeOfObjectArray()I
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetKeyInfo()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

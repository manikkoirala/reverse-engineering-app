.class public Lsk0$c;
.super Ly32;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsk0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# static fields
.field public static final f:Landroidx/lifecycle/o$b;


# instance fields
.field public d:Lwo1;

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lsk0$c$a;

    invoke-direct {v0}, Lsk0$c$a;-><init>()V

    sput-object v0, Lsk0$c;->f:Landroidx/lifecycle/o$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ly32;-><init>()V

    new-instance v0, Lwo1;

    invoke-direct {v0}, Lwo1;-><init>()V

    iput-object v0, p0, Lsk0$c;->d:Lwo1;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lsk0$c;->e:Z

    return-void
.end method

.method public static h(Lb42;)Lsk0$c;
    .locals 2

    .line 1
    new-instance v0, Landroidx/lifecycle/o;

    sget-object v1, Lsk0$c;->f:Landroidx/lifecycle/o$b;

    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/o;-><init>(Lb42;Landroidx/lifecycle/o$b;)V

    const-class p0, Lsk0$c;

    invoke-virtual {v0, p0}, Landroidx/lifecycle/o;->a(Ljava/lang/Class;)Ly32;

    move-result-object p0

    check-cast p0, Lsk0$c;

    return-object p0
.end method


# virtual methods
.method public d()V
    .locals 4

    .line 1
    invoke-super {p0}, Ly32;->d()V

    iget-object v0, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v0}, Lwo1;->l()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v2, v1}, Lwo1;->m(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsk0$a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lsk0$a;->o(Z)Lqk0;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v0}, Lwo1;->b()V

    return-void
.end method

.method public f(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v0}, Lwo1;->l()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v2}, Lwo1;->l()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v2, v1}, Lwo1;->m(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsk0$a;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v3, v1}, Lwo1;->j(I)I

    move-result v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v2}, Lsk0$a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v2, v0, p2, p3, p4}, Lsk0$a;->p(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsk0$c;->e:Z

    return-void
.end method

.method public i(I)Lsk0$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v0, p1}, Lwo1;->f(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lsk0$a;

    return-object p1
.end method

.method public j()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lsk0$c;->e:Z

    return v0
.end method

.method public k()V
    .locals 3

    .line 1
    iget-object v0, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v0}, Lwo1;->l()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v2, v1}, Lwo1;->m(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsk0$a;

    invoke-virtual {v2}, Lsk0$a;->r()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public l(ILsk0$a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lsk0$c;->d:Lwo1;

    invoke-virtual {v0, p1, p2}, Lwo1;->k(ILjava/lang/Object;)V

    return-void
.end method

.method public m()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsk0$c;->e:Z

    return-void
.end method

.class public Ldm;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public d:Lcom/google/android/material/textfield/TextInputLayout;

.field public e:Landroid/widget/AutoCompleteTextView;

.field public f:Lcom/google/android/material/textfield/TextInputLayout;

.field public g:Ljava/util/ArrayList;

.field public h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ly01;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    new-instance v0, Ldm$a;

    invoke-direct {v0, p0}, Ldm$a;-><init>(Ldm;)V

    iput-object v0, p0, Ldm;->h:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Ldm;->a:Landroid/content/Context;

    iput-object p3, p0, Ldm;->b:Ly01;

    iput-object p2, p0, Ldm;->c:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-void
.end method

.method public static synthetic a(Ldm;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Ldm;->g:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic b(Ldm;)Landroid/widget/AutoCompleteTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Ldm;->e:Landroid/widget/AutoCompleteTextView;

    return-object p0
.end method

.method public static synthetic c(Ldm;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Ldm;->j()V

    return-void
.end method

.method public static synthetic d(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Ldm;->i(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result p0

    return p0
.end method

.method public static synthetic e(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Ldm;->m(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    return-void
.end method

.method public static synthetic f(Ldm;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Ldm;->c:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic g(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Ldm;->k(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result p0

    return p0
.end method

.method public static synthetic h(Ldm;)Ly01;
    .locals 0

    .line 1
    iget-object p0, p0, Ldm;->b:Ly01;

    return-object p0
.end method


# virtual methods
.method public final i(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Ldm;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final j()V
    .locals 3

    .line 1
    iget-object v0, p0, Ldm;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getAllClassNames()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ldm;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Ldm;->a:Landroid/content/Context;

    const v2, 0x7f120028

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lh3;

    iget-object v1, p0, Ldm;->a:Landroid/content/Context;

    iget-object v2, p0, Ldm;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lh3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v1, p0, Ldm;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final k(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Ldm;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Ldm;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v5

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setName(Ljava/lang/String;)V

    iget-object p2, p0, Ldm;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lb10;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance p2, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    move-object v1, p2

    invoke-direct/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    iget-object v2, p0, Ldm;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllResultForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    if-eqz v1, :cond_0

    iget-object v2, p0, Ldm;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, p1, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllSheetImageForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Z

    :cond_0
    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public final l()V
    .locals 2

    .line 1
    const v0, 0x7f09044d

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    const v1, 0x7f12007e

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    new-instance v1, Ldm$b;

    invoke-direct {v1, p0}, Ldm$b;-><init>(Ldm;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final m(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Ldm;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ldm;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1201c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Ldm$h;

    invoke-direct {v2, p0, p1}, Ldm$h;-><init>(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    const p1, 0x7f120337

    invoke-virtual {v1, p1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    new-instance v1, Ldm$g;

    invoke-direct {v1, p0}, Ldm$g;-><init>(Ldm;)V

    const v2, 0x7f120241

    invoke-virtual {p1, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c007b

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Ldm;->l()V

    const p1, 0x7f090397

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f090394

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Ldm;->f:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f090396

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Ldm;->d:Lcom/google/android/material/textfield/TextInputLayout;

    iget-object v0, p0, Ldm;->f:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Ldm;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0}, Ldm;->j()V

    iget-object v0, p0, Ldm;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Ldm$c;

    invoke-direct {v1, p0}, Ldm$c;-><init>(Ldm;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Ldm;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Ldm$d;

    invoke-direct {v1, p0}, Ldm$d;-><init>(Ldm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Ldm;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v0, p0, Ldm;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Ldm;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Ldm;->d:Lcom/google/android/material/textfield/TextInputLayout;

    iget-object v1, p0, Ldm;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setEndIconOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900ce

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Ldm$e;

    invoke-direct {v1, p0, p1}, Ldm$e;-><init>(Ldm;Lcom/google/android/material/textfield/TextInputLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090088

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Ldm$f;

    invoke-direct {v0, p0}, Ldm$f;-><init>(Ldm;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

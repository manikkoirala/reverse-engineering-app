.class public La21;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/widget/ListView;

.field public c:Landroid/widget/Spinner;

.field public d:Ljava/util/ArrayList;

.field public e:Ll3;

.field public f:Ljava/util/ArrayList;

.field public g:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public i:I

.field public j:Lz11$c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La21;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, La21;->i:I

    new-instance v0, La21$a;

    invoke-direct {v0, p0}, La21$a;-><init>(La21;)V

    iput-object v0, p0, La21;->j:Lz11$c;

    return-void
.end method

.method public static synthetic g(La21;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, La21;->l()V

    return-void
.end method

.method public static synthetic h(La21;)I
    .locals 0

    .line 1
    iget p0, p0, La21;->i:I

    return p0
.end method

.method public static synthetic i(La21;I)I
    .locals 0

    .line 1
    iput p1, p0, La21;->i:I

    return p1
.end method

.method public static synthetic j(La21;)Lz11$c;
    .locals 0

    .line 1
    iget-object p0, p0, La21;->j:Lz11$c;

    return-object p0
.end method


# virtual methods
.method public final k(I[Ljava/lang/String;)V
    .locals 3

    .line 1
    new-array v0, p1, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    aget-object v2, p2, v1

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/widget/ArrayAdapter;

    iget-object p2, p0, La21;->a:Landroid/content/Context;

    const v1, 0x7f0c00f0

    invoke-direct {p1, p2, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object p2, p0, La21;->c:Landroid/widget/Spinner;

    invoke-virtual {p2, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p1, p0, La21;->c:Landroid/widget/Spinner;

    new-instance p2, La21$b;

    invoke-direct {p2, p0}, La21$b;-><init>(La21;)V

    invoke-virtual {p1, p2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final l()V
    .locals 3

    .line 1
    new-instance v0, Ll3;

    iget-object v1, p0, La21;->a:Landroid/content/Context;

    iget-object v2, p0, La21;->d:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Ll3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, La21;->e:Ll3;

    iget-object v1, p0, La21;->b:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, La21;->a:Landroid/content/Context;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .line 1
    const p3, 0x7f0c0090

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const p2, 0x7f09024a

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ListView;

    iput-object p2, p0, La21;->b:Landroid/widget/ListView;

    const p2, 0x7f090349

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Spinner;

    iput-object p2, p0, La21;->c:Landroid/widget/Spinner;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "EXAM_ID"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p2, p0, La21;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "EXAM_TYPE"

    invoke-virtual {p2, p3}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "ARCHIVED"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Le8;->a:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p2, p0, La21;->g:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {}, Le8;->a()Ljava/util/ArrayList;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, La21;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p2

    iget-object p3, p0, La21;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p2

    iput-object p2, p0, La21;->g:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object p2, p0, La21;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    iget-object p3, p0, La21;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object p2

    :goto_0
    iput-object p2, p0, La21;->f:Ljava/util/ArrayList;

    iget-object p2, p0, La21;->g:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_1

    iput v0, p0, La21;->i:I

    const p2, 0x7f090400

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, La21;->c:Landroid/widget/Spinner;

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object p3, p0, La21;->g:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p2, p3}, La21;->k(I[Ljava/lang/String;)V

    :goto_1
    new-instance p2, Lz11;

    iget p3, p0, La21;->i:I

    iget-object v0, p0, La21;->j:Lz11$c;

    iget-object v1, p0, La21;->g:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v2, p0, La21;->f:Ljava/util/ArrayList;

    invoke-direct {p2, p3, v0, v1, v2}, Lz11;-><init>(ILz11$c;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V

    :cond_2
    return-object p1
.end method

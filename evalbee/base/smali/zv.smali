.class public Lzv;
.super Lu80;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzv$d;
    }
.end annotation


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:Ljava/util/ArrayList;

.field public c:Landroid/content/Context;

.field public d:Lzv$d;

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lzv$d;Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lzv;->c:Landroid/content/Context;

    iput-object p2, p0, Lzv;->d:Lzv$d;

    iput-object p3, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    return-void
.end method

.method public static synthetic a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    return-object p0
.end method

.method public static synthetic b(Lzv;)Lzv$d;
    .locals 0

    .line 1
    iget-object p0, p0, Lzv;->d:Lzv$d;

    return-object p0
.end method


# virtual methods
.method public final c(Ljava/util/ArrayList;D)I
    .locals 4

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, p2

    if-nez v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final d()V
    .locals 2

    .line 1
    const v0, 0x7f09043e

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lzv$c;

    invoke-direct {v1, p0}, Lzv$c;-><init>(Lzv;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0069

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lzv;->d()V

    iget-object p1, p0, Lzv;->c:Landroid/content/Context;

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "plus_mark_json"

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lok;->I:[Ljava/lang/Double;

    invoke-static {v0, v2}, La91;->o(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lzv;->a:Ljava/util/ArrayList;

    const-string v0, "minus_mark_json"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lok;->J:[Ljava/lang/Double;

    invoke-static {p1, v0}, La91;->m(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lzv;->b:Ljava/util/ArrayList;

    const p1, 0x7f090416

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0900f3

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/CheckBox;

    const v0, 0x7f090210

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f090211

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v2

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->LABEL:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    const/16 v4, 0x8

    const-string v5, " - "

    if-ne v2, v3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lzv;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120280

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const p1, 0x7f09014f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Landroid/widget/EditText;

    const p1, 0x7f09014c

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Landroid/widget/EditText;

    const p1, 0x7f090335

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Landroid/widget/Spinner;

    const p1, 0x7f09033c

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v6, p1

    check-cast v6, Landroid/widget/Spinner;

    const p1, 0x7f090100

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v7, p1

    check-cast v7, Landroid/widget/CheckBox;

    iget-object p1, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance p1, Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lzv;->c:Landroid/content/Context;

    iget-object v1, p0, Lzv;->a:Ljava/util/ArrayList;

    const v2, 0x7f0c00f0

    invoke-direct {p1, v0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lzv;->c:Landroid/content/Context;

    iget-object v9, p0, Lzv;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v5, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v6, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p1, p0, Lzv;->a:Ljava/util/ArrayList;

    iget-object v0, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->a()D

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lzv;->c(Ljava/util/ArrayList;D)I

    move-result p1

    invoke-virtual {v5, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object p1, p0, Lzv;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->b()D

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lzv;->c(Ljava/util/ArrayList;D)I

    move-result p1

    invoke-virtual {v6, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object p1, p0, Lzv;->e:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->j()Z

    move-result p1

    invoke-virtual {v7, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const p1, 0x7f0900d0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lzv$a;

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lzv$a;-><init>(Lzv;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/Spinner;Landroid/widget/Spinner;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900ac

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lzv$b;

    invoke-direct {v0, p0}, Lzv$b;-><init>(Lzv;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

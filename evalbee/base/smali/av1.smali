.class public abstract Lav1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lav1$f;,
        Lav1$a;,
        Lav1$b;,
        Lav1$c;,
        Lav1$e;,
        Lav1$d;
    }
.end annotation


# static fields
.field public static final a:Lzu1;

.field public static final b:Lzu1;

.field public static final c:Lzu1;

.field public static final d:Lzu1;

.field public static final e:Lzu1;

.field public static final f:Lzu1;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lav1$e;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lav1$e;-><init>(Lav1$c;Z)V

    sput-object v0, Lav1;->a:Lzu1;

    new-instance v0, Lav1$e;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lav1$e;-><init>(Lav1$c;Z)V

    sput-object v0, Lav1;->b:Lzu1;

    new-instance v0, Lav1$e;

    sget-object v1, Lav1$b;->a:Lav1$b;

    invoke-direct {v0, v1, v2}, Lav1$e;-><init>(Lav1$c;Z)V

    sput-object v0, Lav1;->c:Lzu1;

    new-instance v0, Lav1$e;

    invoke-direct {v0, v1, v3}, Lav1$e;-><init>(Lav1$c;Z)V

    sput-object v0, Lav1;->d:Lzu1;

    new-instance v0, Lav1$e;

    sget-object v1, Lav1$a;->b:Lav1$a;

    invoke-direct {v0, v1, v2}, Lav1$e;-><init>(Lav1$c;Z)V

    sput-object v0, Lav1;->e:Lzu1;

    sget-object v0, Lav1$f;->b:Lav1$f;

    sput-object v0, Lav1;->f:Lzu1;

    return-void
.end method

.method public static a(I)I
    .locals 1

    .line 1
    const/4 v0, 0x1

    if-eqz p0, :cond_1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    return v0
.end method

.method public static b(I)I
    .locals 2

    .line 1
    const/4 v0, 0x1

    if-eqz p0, :cond_1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    packed-switch p0, :pswitch_data_0

    return v1

    :cond_0
    :pswitch_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :pswitch_1
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

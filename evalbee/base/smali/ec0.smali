.class public Lec0;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static h:Lhs1;


# instance fields
.field public a:Lcom/google/android/gms/tasks/Task;

.field public final b:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public c:Lio/grpc/CallOptions;

.field public d:Lcom/google/firebase/firestore/util/AsyncQueue$b;

.field public final e:Landroid/content/Context;

.field public final f:Lrp;

.field public final g:Lio/grpc/CallCredentials;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/util/AsyncQueue;Landroid/content/Context;Lrp;Lio/grpc/CallCredentials;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p2, p0, Lec0;->e:Landroid/content/Context;

    iput-object p3, p0, Lec0;->f:Lrp;

    iput-object p4, p0, Lec0;->g:Lio/grpc/CallCredentials;

    invoke-virtual {p0}, Lec0;->k()V

    return-void
.end method

.method public static synthetic a(Lec0;Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lec0;->p(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method public static synthetic b(Lec0;Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lec0;->q(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method public static synthetic c(Lec0;Lio/grpc/MethodDescriptor;Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lec0;->l(Lio/grpc/MethodDescriptor;Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Lec0;)Lio/grpc/ManagedChannel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lec0;->n()Lio/grpc/ManagedChannel;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Lec0;Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lec0;->o(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method public static synthetic f(Lec0;Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lec0;->m(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method public static synthetic g(Lec0;Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lec0;->r(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method private synthetic l(Lio/grpc/MethodDescriptor;Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lio/grpc/ManagedChannel;

    iget-object v0, p0, Lec0;->c:Lio/grpc/CallOptions;

    invoke-virtual {p2, p1, v0}, Lio/grpc/Channel;->newCall(Lio/grpc/MethodDescriptor;Lio/grpc/CallOptions;)Lio/grpc/ClientCall;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method private synthetic m(Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lec0;->s(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method private synthetic n()Lio/grpc/ManagedChannel;
    .locals 4

    .line 1
    iget-object v0, p0, Lec0;->e:Landroid/content/Context;

    iget-object v1, p0, Lec0;->f:Lrp;

    invoke-virtual {p0, v0, v1}, Lec0;->j(Landroid/content/Context;Lrp;)Lio/grpc/ManagedChannel;

    move-result-object v0

    iget-object v1, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v2, Lyb0;

    invoke-direct {v2, p0, v0}, Lyb0;-><init>(Lec0;Lio/grpc/ManagedChannel;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    invoke-static {v0}, Lh40;->c(Lio/grpc/Channel;)Lh40$b;

    move-result-object v1

    iget-object v2, p0, Lec0;->g:Lio/grpc/CallCredentials;

    invoke-virtual {v1, v2}, Lio/grpc/stub/AbstractStub;->withCallCredentials(Lio/grpc/CallCredentials;)Lio/grpc/stub/AbstractStub;

    move-result-object v1

    check-cast v1, Lh40$b;

    iget-object v2, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v2}, Lcom/google/firebase/firestore/util/AsyncQueue;->j()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/grpc/stub/AbstractStub;->withExecutor(Ljava/util/concurrent/Executor;)Lio/grpc/stub/AbstractStub;

    move-result-object v1

    check-cast v1, Lh40$b;

    invoke-virtual {v1}, Lio/grpc/stub/AbstractStub;->getCallOptions()Lio/grpc/CallOptions;

    move-result-object v1

    iput-object v1, p0, Lec0;->c:Lio/grpc/CallOptions;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "GrpcCallProvider"

    const-string v3, "Channel successfully reset."

    invoke-static {v2, v3, v1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private synthetic o(Lio/grpc/ManagedChannel;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "GrpcCallProvider"

    const-string v2, "connectivityAttemptTimer elapsed. Resetting the channel."

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lec0;->h()V

    invoke-virtual {p0, p1}, Lec0;->t(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method private synthetic p(Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lec0;->s(Lio/grpc/ManagedChannel;)V

    return-void
.end method

.method private synthetic q(Lio/grpc/ManagedChannel;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v1, Ldc0;

    invoke-direct {v1, p0, p1}, Ldc0;-><init>(Lec0;Lio/grpc/ManagedChannel;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    return-void
.end method

.method private synthetic r(Lio/grpc/ManagedChannel;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lio/grpc/ManagedChannel;->shutdownNow()Lio/grpc/ManagedChannel;

    invoke-virtual {p0}, Lec0;->k()V

    return-void
.end method


# virtual methods
.method public final h()V
    .locals 3

    .line 1
    iget-object v0, p0, Lec0;->d:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "GrpcCallProvider"

    const-string v2, "Clearing the connectivityAttemptTimer"

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lec0;->d:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue$b;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lec0;->d:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    return-void
.end method

.method public i(Lio/grpc/MethodDescriptor;)Lcom/google/android/gms/tasks/Task;
    .locals 3

    .line 1
    iget-object v0, p0, Lec0;->a:Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->j()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lbc0;

    invoke-direct {v2, p0, p1}, Lbc0;-><init>(Lec0;Lio/grpc/MethodDescriptor;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->continueWithTask(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final j(Landroid/content/Context;Lrp;)Lio/grpc/ManagedChannel;
    .locals 3

    .line 1
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/security/ProviderInstaller;->installIfNeeded(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    :goto_0
    const-string v1, "Failed to update ssl context: %s"

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v2, "GrpcCallProvider"

    invoke-static {v2, v1, v0}, Lcom/google/firebase/firestore/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    sget-object v0, Lec0;->h:Lhs1;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lhs1;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lio/grpc/ManagedChannelBuilder;

    goto :goto_2

    :cond_0
    invoke-virtual {p2}, Lrp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/grpc/ManagedChannelBuilder;->forTarget(Ljava/lang/String;)Lio/grpc/ManagedChannelBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lrp;->d()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-virtual {v0}, Lio/grpc/ManagedChannelBuilder;->usePlaintext()Lio/grpc/ManagedChannelBuilder;

    :cond_1
    move-object p2, v0

    :goto_2
    const-wide/16 v0, 0x1e

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v0, v1, v2}, Lio/grpc/ManagedChannelBuilder;->keepAliveTime(JLjava/util/concurrent/TimeUnit;)Lio/grpc/ManagedChannelBuilder;

    invoke-static {p2}, Lio/grpc/android/AndroidChannelBuilder;->usingBuilder(Lio/grpc/ManagedChannelBuilder;)Lio/grpc/android/AndroidChannelBuilder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lio/grpc/android/AndroidChannelBuilder;->context(Landroid/content/Context;)Lio/grpc/android/AndroidChannelBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lio/grpc/android/AndroidChannelBuilder;->build()Lio/grpc/ManagedChannel;

    move-result-object p1

    return-object p1
.end method

.method public final k()V
    .locals 2

    .line 1
    sget-object v0, Lwy;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lxb0;

    invoke-direct {v1, p0}, Lxb0;-><init>(Lec0;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/tasks/Tasks;->call(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    iput-object v0, p0, Lec0;->a:Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final s(Lio/grpc/ManagedChannel;)V
    .locals 6

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lio/grpc/ManagedChannel;->getState(Z)Lio/grpc/ConnectivityState;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current gRPC connectivity state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "GrpcCallProvider"

    invoke-static {v4, v1, v3}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lec0;->h()V

    sget-object v1, Lio/grpc/ConnectivityState;->CONNECTING:Lio/grpc/ConnectivityState;

    if-ne v0, v1, :cond_0

    const-string v1, "Setting the connectivityAttemptTimer"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4, v1, v2}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    sget-object v2, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->CONNECTIVITY_ATTEMPT_TIMER:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    new-instance v3, Lzb0;

    invoke-direct {v3, p0, p1}, Lzb0;-><init>(Lec0;Lio/grpc/ManagedChannel;)V

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v4, v5, v3}, Lcom/google/firebase/firestore/util/AsyncQueue;->h(Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;JLjava/lang/Runnable;)Lcom/google/firebase/firestore/util/AsyncQueue$b;

    move-result-object v1

    iput-object v1, p0, Lec0;->d:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    new-instance v1, Lac0;

    invoke-direct {v1, p0, p1}, Lac0;-><init>(Lec0;Lio/grpc/ManagedChannel;)V

    invoke-virtual {p1, v0, v1}, Lio/grpc/ManagedChannel;->notifyWhenStateChanged(Lio/grpc/ConnectivityState;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final t(Lio/grpc/ManagedChannel;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lec0;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v1, Lcc0;

    invoke-direct {v1, p0, p1}, Lcc0;-><init>(Lec0;Lio/grpc/ManagedChannel;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    return-void
.end method

.class public Le62;
.super Lt1;
.source "SourceFile"

# interfaces
.implements Landroidx/appcompat/widget/ActionBarOverlayLayout$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le62$d;
    }
.end annotation


# static fields
.field public static final D:Landroid/view/animation/Interpolator;

.field public static final E:Landroid/view/animation/Interpolator;


# instance fields
.field public final A:Lk42;

.field public final B:Lk42;

.field public final C:Lm42;

.field public a:Landroid/content/Context;

.field public b:Landroid/content/Context;

.field public c:Landroid/app/Activity;

.field public d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

.field public e:Landroidx/appcompat/widget/ActionBarContainer;

.field public f:Ldq;

.field public g:Landroidx/appcompat/widget/ActionBarContextView;

.field public h:Landroid/view/View;

.field public i:Ljava/util/ArrayList;

.field public j:I

.field public k:Z

.field public l:Le62$d;

.field public m:Lc2;

.field public n:Lc2$a;

.field public o:Z

.field public p:Ljava/util/ArrayList;

.field public q:Z

.field public r:I

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Lj42;

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Le62;->D:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Le62;->E:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lt1;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le62;->i:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Le62;->j:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le62;->p:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Le62;->r:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Le62;->s:Z

    iput-boolean v0, p0, Le62;->w:Z

    new-instance v0, Le62$a;

    invoke-direct {v0, p0}, Le62$a;-><init>(Le62;)V

    iput-object v0, p0, Le62;->A:Lk42;

    new-instance v0, Le62$b;

    invoke-direct {v0, p0}, Le62$b;-><init>(Le62;)V

    iput-object v0, p0, Le62;->B:Lk42;

    new-instance v0, Le62$c;

    invoke-direct {v0, p0}, Le62$c;-><init>(Le62;)V

    iput-object v0, p0, Le62;->C:Lm42;

    iput-object p1, p0, Le62;->c:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Le62;->E(Landroid/view/View;)V

    if-nez p2, :cond_0

    const p2, 0x1020002

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Le62;->h:Landroid/view/View;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lt1;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le62;->i:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Le62;->j:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le62;->p:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Le62;->r:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Le62;->s:Z

    iput-boolean v0, p0, Le62;->w:Z

    new-instance v0, Le62$a;

    invoke-direct {v0, p0}, Le62$a;-><init>(Le62;)V

    iput-object v0, p0, Le62;->A:Lk42;

    new-instance v0, Le62$b;

    invoke-direct {v0, p0}, Le62$b;-><init>(Le62;)V

    iput-object v0, p0, Le62;->B:Lk42;

    new-instance v0, Le62$c;

    invoke-direct {v0, p0}, Le62$c;-><init>(Le62;)V

    iput-object v0, p0, Le62;->C:Lm42;

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Le62;->E(Landroid/view/View;)V

    return-void
.end method

.method public static x(ZZZ)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    if-eqz p2, :cond_0

    return v0

    :cond_0
    if-nez p0, :cond_2

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public A(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Le62;->x:Lj42;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lj42;->a()V

    :cond_0
    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/ActionBarContainer;->setVisibility(I)V

    iget v0, p0, Le62;->r:I

    const/4 v2, 0x0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Le62;->y:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_4

    :cond_1
    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    if-eqz p1, :cond_2

    filled-new-array {v1, v1}, [I

    move-result-object p1

    iget-object v1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {v1, p1}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v1, 0x1

    aget p1, p1, v1

    int-to-float p1, p1

    sub-float/2addr v0, p1

    :cond_2
    iget-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    new-instance p1, Lj42;

    invoke-direct {p1}, Lj42;-><init>()V

    iget-object v1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-static {v1}, Lo32;->e(Landroid/view/View;)Li42;

    move-result-object v1

    invoke-virtual {v1, v2}, Li42;->m(F)Li42;

    move-result-object v1

    iget-object v3, p0, Le62;->C:Lm42;

    invoke-virtual {v1, v3}, Li42;->k(Lm42;)Li42;

    invoke-virtual {p1, v1}, Lj42;->c(Li42;)Lj42;

    iget-boolean v1, p0, Le62;->s:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Le62;->h:Landroid/view/View;

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Le62;->h:Landroid/view/View;

    invoke-static {v0}, Lo32;->e(Landroid/view/View;)Li42;

    move-result-object v0

    invoke-virtual {v0, v2}, Li42;->m(F)Li42;

    move-result-object v0

    invoke-virtual {p1, v0}, Lj42;->c(Li42;)Lj42;

    :cond_3
    sget-object v0, Le62;->E:Landroid/view/animation/Interpolator;

    invoke-virtual {p1, v0}, Lj42;->f(Landroid/view/animation/Interpolator;)Lj42;

    const-wide/16 v0, 0xfa

    invoke-virtual {p1, v0, v1}, Lj42;->e(J)Lj42;

    iget-object v0, p0, Le62;->B:Lk42;

    invoke-virtual {p1, v0}, Lj42;->g(Lk42;)Lj42;

    iput-object p1, p0, Le62;->x:Lj42;

    invoke-virtual {p1}, Lj42;->h()V

    goto :goto_0

    :cond_4
    iget-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-boolean p1, p0, Le62;->s:Z

    if-eqz p1, :cond_5

    iget-object p1, p0, Le62;->h:Landroid/view/View;

    if-eqz p1, :cond_5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    :cond_5
    iget-object p1, p0, Le62;->B:Lk42;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lk42;->b(Landroid/view/View;)V

    :goto_0
    iget-object p1, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    if-eqz p1, :cond_6

    invoke-static {p1}, Lo32;->n0(Landroid/view/View;)V

    :cond_6
    return-void
.end method

.method public final B(Landroid/view/View;)Ldq;
    .locals 3

    .line 1
    instance-of v0, p1, Ldq;

    if-eqz v0, :cond_0

    check-cast p1, Ldq;

    return-object p1

    :cond_0
    instance-of v0, p1, Landroidx/appcompat/widget/Toolbar;

    if-eqz v0, :cond_1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getWrapper()Ldq;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t make a decor toolbar out of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-string p1, "null"

    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public C()I
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0}, Ldq;->j()I

    move-result v0

    return v0
.end method

.method public final D()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Le62;->v:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Le62;->v:Z

    iget-object v1, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    :cond_0
    invoke-virtual {p0, v0}, Le62;->N(Z)V

    :cond_1
    return-void
.end method

.method public final E(Landroid/view/View;)V
    .locals 5

    .line 1
    sget v0, Ldb1;->p:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->setActionBarVisibilityCallback(Landroidx/appcompat/widget/ActionBarOverlayLayout$d;)V

    :cond_0
    sget v0, Ldb1;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Le62;->B(Landroid/view/View;)Ldq;

    move-result-object v0

    iput-object v0, p0, Le62;->f:Ldq;

    sget v0, Ldb1;->f:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/ActionBarContextView;

    iput-object v0, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    sget v0, Ldb1;->c:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/ActionBarContainer;

    iput-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    iget-object v0, p0, Le62;->f:Ldq;

    if-eqz v0, :cond_7

    iget-object v1, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    if-eqz v1, :cond_7

    if-eqz p1, :cond_7

    invoke-interface {v0}, Ldq;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Le62;->a:Landroid/content/Context;

    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1}, Ldq;->o()I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    move p1, v0

    goto :goto_0

    :cond_1
    move p1, v1

    :goto_0
    if-eqz p1, :cond_2

    iput-boolean v0, p0, Le62;->k:Z

    :cond_2
    iget-object v2, p0, Le62;->a:Landroid/content/Context;

    invoke-static {v2}, Lv1;->b(Landroid/content/Context;)Lv1;

    move-result-object v2

    invoke-virtual {v2}, Lv1;->a()Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    move p1, v1

    goto :goto_2

    :cond_4
    :goto_1
    move p1, v0

    :goto_2
    invoke-virtual {p0, p1}, Le62;->K(Z)V

    invoke-virtual {v2}, Lv1;->e()Z

    move-result p1

    invoke-virtual {p0, p1}, Le62;->I(Z)V

    iget-object p1, p0, Le62;->a:Landroid/content/Context;

    sget-object v2, Lbc1;->a:[I

    sget v3, Lsa1;->c:I

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v2, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    sget v2, Lbc1;->k:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v0}, Le62;->J(Z)V

    :cond_5
    sget v0, Lbc1;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    if-eqz v0, :cond_6

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Le62;->H(F)V

    :cond_6
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " can only be used with a compatible window decor layout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public F(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x4

    if-eqz p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Le62;->G(II)V

    return-void
.end method

.method public G(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0}, Ldq;->o()I

    move-result v0

    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Le62;->k:Z

    :cond_0
    iget-object v1, p0, Le62;->f:Ldq;

    and-int/2addr p1, p2

    not-int p2, p2

    and-int/2addr p2, v0

    or-int/2addr p1, p2

    invoke-interface {v1, p1}, Ldq;->i(I)V

    return-void
.end method

.method public H(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-static {v0, p1}, Lo32;->y0(Landroid/view/View;F)V

    return-void
.end method

.method public final I(Z)V
    .locals 4

    .line 1
    iput-boolean p1, p0, Le62;->q:Z

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1, v0}, Ldq;->v(Landroidx/appcompat/widget/c;)V

    iget-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContainer;->setTabContainer(Landroidx/appcompat/widget/c;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContainer;->setTabContainer(Landroidx/appcompat/widget/c;)V

    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1, v0}, Ldq;->v(Landroidx/appcompat/widget/c;)V

    :goto_0
    invoke-virtual {p0}, Le62;->C()I

    move-result p1

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_1

    move p1, v1

    goto :goto_1

    :cond_1
    move p1, v2

    :goto_1
    iget-object v0, p0, Le62;->f:Ldq;

    iget-boolean v3, p0, Le62;->q:Z

    if-nez v3, :cond_2

    if-eqz p1, :cond_2

    move v3, v1

    goto :goto_2

    :cond_2
    move v3, v2

    :goto_2
    invoke-interface {v0, v3}, Ldq;->l(Z)V

    iget-object v0, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    iget-boolean v3, p0, Le62;->q:Z

    if-nez v3, :cond_3

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move v1, v2

    :goto_3
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->setHasNonEmbeddedTabs(Z)V

    return-void
.end method

.method public J(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    iget-object v0, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput-boolean p1, p0, Le62;->z:Z

    iget-object v0, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    return-void
.end method

.method public K(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0, p1}, Ldq;->u(Z)V

    return-void
.end method

.method public final L()Z
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-static {v0}, Lo32;->U(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public final M()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Le62;->v:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Le62;->v:Z

    iget-object v1, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Le62;->N(Z)V

    :cond_1
    return-void
.end method

.method public final N(Z)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Le62;->t:Z

    iget-boolean v1, p0, Le62;->u:Z

    iget-boolean v2, p0, Le62;->v:Z

    invoke-static {v0, v1, v2}, Le62;->x(ZZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Le62;->w:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Le62;->w:Z

    invoke-virtual {p0, p1}, Le62;->A(Z)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Le62;->w:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Le62;->w:Z

    invoke-virtual {p0, p1}, Le62;->z(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Le62;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Le62;->u:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Le62;->N(Z)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .line 1
    return-void
.end method

.method public c()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Le62;->u:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Le62;->u:Z

    invoke-virtual {p0, v0}, Le62;->N(Z)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->x:Lj42;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lj42;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Le62;->x:Lj42;

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Le62;->s:Z

    return-void
.end method

.method public g()Z
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ldq;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0}, Ldq;->collapseActionView()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public h(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Le62;->o:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Le62;->o:Z

    iget-object p1, p0, Le62;->p:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-gtz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Le62;->p:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public i()I
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0}, Ldq;->o()I

    move-result v0

    return v0
.end method

.method public j()Landroid/content/Context;
    .locals 4

    .line 1
    iget-object v0, p0, Le62;->b:Landroid/content/Context;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Le62;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lsa1;->e:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Le62;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Le62;->b:Landroid/content/Context;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le62;->a:Landroid/content/Context;

    iput-object v0, p0, Le62;->b:Landroid/content/Context;

    :cond_1
    :goto_0
    iget-object v0, p0, Le62;->b:Landroid/content/Context;

    return-object v0
.end method

.method public l(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1
    iget-object p1, p0, Le62;->a:Landroid/content/Context;

    invoke-static {p1}, Lv1;->b(Landroid/content/Context;)Lv1;

    move-result-object p1

    invoke-virtual {p1}, Lv1;->e()Z

    move-result p1

    invoke-virtual {p0, p1}, Le62;->I(Z)V

    return-void
.end method

.method public n(ILandroid/view/KeyEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Le62;->l:Le62$d;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Le62$d;->c()Landroid/view/Menu;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_0
    invoke-static {v2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v1

    :goto_1
    invoke-interface {v0, v3}, Landroid/view/Menu;->setQwertyMode(Z)V

    invoke-interface {v0, p1, p2, v1}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result p1

    return p1

    :cond_3
    return v1
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 0

    .line 1
    iput p1, p0, Le62;->r:I

    return-void
.end method

.method public q(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Le62;->k:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Le62;->F(Z)V

    :cond_0
    return-void
.end method

.method public r(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Le62;->y:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Le62;->x:Lj42;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lj42;->a()V

    :cond_0
    return-void
.end method

.method public s(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0, p1}, Ldq;->q(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public t(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0, p1}, Ldq;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public u(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iget-object v0, p0, Le62;->f:Ldq;

    invoke-interface {v0, p1}, Ldq;->setWindowTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public v(Lc2$a;)Lc2;
    .locals 2

    .line 1
    iget-object v0, p0, Le62;->l:Le62$d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Le62$d;->a()V

    :cond_0
    iget-object v0, p0, Le62;->d:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    iget-object v0, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroidx/appcompat/widget/ActionBarContextView;->k()V

    new-instance v0, Le62$d;

    iget-object v1, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Le62$d;-><init>(Le62;Landroid/content/Context;Lc2$a;)V

    invoke-virtual {v0}, Le62$d;->r()Z

    move-result p1

    if-eqz p1, :cond_1

    iput-object v0, p0, Le62;->l:Le62$d;

    invoke-virtual {v0}, Le62$d;->i()V

    iget-object p1, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContextView;->h(Lc2;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Le62;->w(Z)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public w(Z)V
    .locals 8

    .line 1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Le62;->M()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Le62;->D()V

    :goto_0
    invoke-virtual {p0}, Le62;->L()Z

    move-result v0

    const/4 v1, 0x4

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    const-wide/16 v4, 0x64

    const-wide/16 v6, 0xc8

    if-eqz p1, :cond_1

    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1, v1, v4, v5}, Ldq;->s(IJ)Li42;

    move-result-object p1

    iget-object v0, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v0, v3, v6, v7}, Landroidx/appcompat/widget/ActionBarContextView;->f(IJ)Li42;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1, v3, v6, v7}, Ldq;->s(IJ)Li42;

    move-result-object v0

    iget-object p1, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v2, v4, v5}, Landroidx/appcompat/widget/ActionBarContextView;->f(IJ)Li42;

    move-result-object p1

    :goto_1
    new-instance v1, Lj42;

    invoke-direct {v1}, Lj42;-><init>()V

    invoke-virtual {v1, p1, v0}, Lj42;->d(Li42;Li42;)Lj42;

    invoke-virtual {v1}, Lj42;->h()V

    goto :goto_2

    :cond_2
    if-eqz p1, :cond_3

    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1, v1}, Ldq;->n(I)V

    iget-object p1, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v3}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    iget-object p1, p0, Le62;->f:Ldq;

    invoke-interface {p1, v3}, Ldq;->n(I)V

    iget-object p1, p0, Le62;->g:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v2}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public y()V
    .locals 2

    .line 1
    iget-object v0, p0, Le62;->n:Lc2$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Le62;->m:Lc2;

    invoke-interface {v0, v1}, Lc2$a;->b(Lc2;)V

    const/4 v0, 0x0

    iput-object v0, p0, Le62;->m:Lc2;

    iput-object v0, p0, Le62;->n:Lc2$a;

    :cond_0
    return-void
.end method

.method public z(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Le62;->x:Lj42;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lj42;->a()V

    :cond_0
    iget v0, p0, Le62;->r:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Le62;->y:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_4

    :cond_1
    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/ActionBarContainer;->setTransitioning(Z)V

    new-instance v0, Lj42;

    invoke-direct {v0}, Lj42;-><init>()V

    iget-object v2, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    filled-new-array {p1, p1}, [I

    move-result-object p1

    iget-object v3, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {v3, p1}, Landroid/view/View;->getLocationInWindow([I)V

    aget p1, p1, v1

    int-to-float p1, p1

    sub-float/2addr v2, p1

    :cond_2
    iget-object p1, p0, Le62;->e:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-static {p1}, Lo32;->e(Landroid/view/View;)Li42;

    move-result-object p1

    invoke-virtual {p1, v2}, Li42;->m(F)Li42;

    move-result-object p1

    iget-object v1, p0, Le62;->C:Lm42;

    invoke-virtual {p1, v1}, Li42;->k(Lm42;)Li42;

    invoke-virtual {v0, p1}, Lj42;->c(Li42;)Lj42;

    iget-boolean p1, p0, Le62;->s:Z

    if-eqz p1, :cond_3

    iget-object p1, p0, Le62;->h:Landroid/view/View;

    if-eqz p1, :cond_3

    invoke-static {p1}, Lo32;->e(Landroid/view/View;)Li42;

    move-result-object p1

    invoke-virtual {p1, v2}, Li42;->m(F)Li42;

    move-result-object p1

    invoke-virtual {v0, p1}, Lj42;->c(Li42;)Lj42;

    :cond_3
    sget-object p1, Le62;->D:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, p1}, Lj42;->f(Landroid/view/animation/Interpolator;)Lj42;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Lj42;->e(J)Lj42;

    iget-object p1, p0, Le62;->A:Lk42;

    invoke-virtual {v0, p1}, Lj42;->g(Lk42;)Lj42;

    iput-object v0, p0, Le62;->x:Lj42;

    invoke-virtual {v0}, Lj42;->h()V

    goto :goto_0

    :cond_4
    iget-object p1, p0, Le62;->A:Lk42;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lk42;->b(Landroid/view/View;)V

    :goto_0
    return-void
.end method

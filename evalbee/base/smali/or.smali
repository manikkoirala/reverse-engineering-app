.class public Lor;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final e:Ljava/lang/String;


# instance fields
.field public final a:Lij1;

.field public final b:Lag1;

.field public final c:Lch;

.field public final d:Ljava/util/Map;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "DelayedWorkTracker"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lor;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lij1;Lag1;Lch;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lor;->a:Lij1;

    iput-object p2, p0, Lor;->b:Lag1;

    iput-object p3, p0, Lor;->c:Lch;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lor;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Lp92;J)V
    .locals 3

    .line 1
    iget-object v0, p0, Lor;->d:Ljava/util/Map;

    iget-object v1, p1, Lp92;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lor;->b:Lag1;

    invoke-interface {v1, v0}, Lag1;->a(Ljava/lang/Runnable;)V

    :cond_0
    new-instance v0, Lor$a;

    invoke-direct {v0, p0, p1}, Lor$a;-><init>(Lor;Lp92;)V

    iget-object v1, p0, Lor;->d:Ljava/util/Map;

    iget-object p1, p1, Lp92;->a:Ljava/lang/String;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lor;->c:Lch;

    invoke-interface {p1}, Lch;->currentTimeMillis()J

    move-result-wide v1

    sub-long/2addr p2, v1

    iget-object p1, p0, Lor;->b:Lag1;

    invoke-interface {p1, p2, p3, v0}, Lag1;->b(JLjava/lang/Runnable;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lor;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lor;->b:Lag1;

    invoke-interface {v0, p1}, Lag1;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.class public abstract Lp9;
.super Lqk0;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lp9$a;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "AsyncTaskLoader"


# instance fields
.field volatile mCancellingTask:Lp9$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp9.a;"
        }
    .end annotation
.end field

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field mHandler:Landroid/os/Handler;

.field mLastLoadCompleteTime:J

.field volatile mTask:Lp9$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp9.a;"
        }
    .end annotation
.end field

.field mUpdateThrottle:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget-object v0, Landroidx/loader/content/ModernAsyncTask;->h:Ljava/util/concurrent/Executor;

    invoke-direct {p0, p1, v0}, Lp9;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .locals 2

    .line 2
    invoke-direct {p0, p1}, Lqk0;-><init>(Landroid/content/Context;)V

    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Lp9;->mLastLoadCompleteTime:J

    iput-object p2, p0, Lp9;->mExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public cancelLoadInBackground()V
    .locals 0

    .line 1
    return-void
.end method

.method public dispatchOnCancelled(Lp9$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp9.a;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p2}, Lp9;->onCanceled(Ljava/lang/Object;)V

    iget-object p2, p0, Lp9;->mCancellingTask:Lp9$a;

    if-ne p2, p1, :cond_0

    invoke-virtual {p0}, Lqk0;->rollbackContentChanged()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lp9;->mLastLoadCompleteTime:J

    const/4 p1, 0x0

    iput-object p1, p0, Lp9;->mCancellingTask:Lp9$a;

    invoke-virtual {p0}, Lqk0;->deliverCancellation()V

    invoke-virtual {p0}, Lp9;->executePendingTask()V

    :cond_0
    return-void
.end method

.method public dispatchOnLoadComplete(Lp9$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp9.a;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    if-eq v0, p1, :cond_0

    invoke-virtual {p0, p1, p2}, Lp9;->dispatchOnCancelled(Lp9$a;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lqk0;->isAbandoned()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p2}, Lp9;->onCanceled(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lqk0;->commitContentChanged()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lp9;->mLastLoadCompleteTime:J

    const/4 p1, 0x0

    iput-object p1, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {p0, p2}, Lqk0;->deliverResult(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lqk0;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    iget-object p2, p0, Lp9;->mTask:Lp9$a;

    const-string p4, " waiting="

    if-eqz p2, :cond_0

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string p2, "mTask="

    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object p2, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    invoke-virtual {p3, p4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object p2, p0, Lp9;->mTask:Lp9$a;

    iget-boolean p2, p2, Lp9$a;->l:Z

    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->println(Z)V

    :cond_0
    iget-object p2, p0, Lp9;->mCancellingTask:Lp9$a;

    if-eqz p2, :cond_1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string p2, "mCancellingTask="

    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object p2, p0, Lp9;->mCancellingTask:Lp9$a;

    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    invoke-virtual {p3, p4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object p2, p0, Lp9;->mCancellingTask:Lp9$a;

    iget-boolean p2, p2, Lp9$a;->l:Z

    invoke-virtual {p3, p2}, Ljava/io/PrintWriter;->println(Z)V

    :cond_1
    iget-wide v0, p0, Lp9;->mUpdateThrottle:J

    const-wide/16 v2, 0x0

    cmp-long p2, v0, v2

    if-eqz p2, :cond_2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string p1, "mUpdateThrottle="

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide p1, p0, Lp9;->mUpdateThrottle:J

    invoke-static {p1, p2, p3}, Llw1;->c(JLjava/io/PrintWriter;)V

    const-string p1, " mLastLoadCompleteTime="

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide p1, p0, Lp9;->mLastLoadCompleteTime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1, p3}, Llw1;->b(JJLjava/io/PrintWriter;)V

    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    :cond_2
    return-void
.end method

.method public executePendingTask()V
    .locals 6

    .line 1
    iget-object v0, p0, Lp9;->mCancellingTask:Lp9$a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    iget-boolean v0, v0, Lp9$a;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lp9$a;->l:Z

    iget-object v0, p0, Lp9;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    iget-wide v0, p0, Lp9;->mUpdateThrottle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lp9;->mLastLoadCompleteTime:J

    iget-wide v4, p0, Lp9;->mUpdateThrottle:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lp9$a;->l:Z

    iget-object v0, p0, Lp9;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lp9;->mTask:Lp9$a;

    iget-wide v2, p0, Lp9;->mLastLoadCompleteTime:J

    iget-wide v4, p0, Lp9;->mUpdateThrottle:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    return-void

    :cond_1
    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    iget-object v1, p0, Lp9;->mExecutor:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroidx/loader/content/ModernAsyncTask;->c(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroidx/loader/content/ModernAsyncTask;

    :cond_2
    return-void
.end method

.method public isLoadInBackgroundCanceled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lp9;->mCancellingTask:Lp9$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract loadInBackground()Ljava/lang/Object;
.end method

.method public onCancelLoad()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lqk0;->mStarted:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lqk0;->mContentChanged:Z

    :cond_0
    iget-object v0, p0, Lp9;->mCancellingTask:Lp9$a;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    iget-boolean v0, v0, Lp9$a;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    iput-boolean v1, v0, Lp9$a;->l:Z

    iget-object v0, p0, Lp9;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_1
    iput-object v2, p0, Lp9;->mTask:Lp9$a;

    return v1

    :cond_2
    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    iget-boolean v0, v0, Lp9$a;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    iput-boolean v1, v0, Lp9$a;->l:Z

    iget-object v0, p0, Lp9;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lp9;->mTask:Lp9$a;

    return v1

    :cond_3
    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {v0, v1}, Landroidx/loader/content/ModernAsyncTask;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lp9;->mTask:Lp9$a;

    iput-object v1, p0, Lp9;->mCancellingTask:Lp9$a;

    invoke-virtual {p0}, Lp9;->cancelLoadInBackground()V

    :cond_4
    iput-object v2, p0, Lp9;->mTask:Lp9$a;

    return v0

    :cond_5
    return v1
.end method

.method public onCanceled(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 1
    return-void
.end method

.method public onForceLoad()V
    .locals 1

    .line 1
    invoke-super {p0}, Lqk0;->onForceLoad()V

    invoke-virtual {p0}, Lqk0;->cancelLoad()Z

    new-instance v0, Lp9$a;

    invoke-direct {v0, p0}, Lp9$a;-><init>(Lp9;)V

    iput-object v0, p0, Lp9;->mTask:Lp9$a;

    invoke-virtual {p0}, Lp9;->executePendingTask()V

    return-void
.end method

.method public onLoadInBackground()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lp9;->loadInBackground()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setUpdateThrottle(J)V
    .locals 2

    .line 1
    iput-wide p1, p0, Lp9;->mUpdateThrottle:J

    const-wide/16 v0, 0x0

    cmp-long p1, p1, v0

    if-eqz p1, :cond_0

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lp9;->mHandler:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method public waitForLoader()V
    .locals 1

    .line 1
    iget-object v0, p0, Lp9;->mTask:Lp9$a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lp9$a;->n()V

    :cond_0
    return-void
.end method

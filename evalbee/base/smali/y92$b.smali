.class public Ly92$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly92;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:Ly92;

.field public final b:Lx82;


# direct methods
.method public constructor <init>(Ly92;Lx82;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ly92$b;->a:Ly92;

    iput-object p2, p0, Ly92$b;->b:Lx82;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1
    iget-object v0, p0, Ly92$b;->a:Ly92;

    iget-object v0, v0, Ly92;->d:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Ly92$b;->a:Ly92;

    iget-object v1, v1, Ly92;->b:Ljava/util/Map;

    iget-object v2, p0, Ly92$b;->b:Lx82;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly92$b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ly92$b;->a:Ly92;

    iget-object v1, v1, Ly92;->c:Ljava/util/Map;

    iget-object v2, p0, Ly92$b;->b:Lx82;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly92$a;

    if-eqz v1, :cond_1

    iget-object v2, p0, Ly92$b;->b:Lx82;

    invoke-interface {v1, v2}, Ly92$a;->a(Lx82;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v1

    const-string v2, "WrkTimerRunnable"

    const-string v3, "Timer with %s is already marked as complete."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Ly92$b;->b:Lx82;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

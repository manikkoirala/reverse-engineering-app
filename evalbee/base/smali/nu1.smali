.class public Lnu1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnu1$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Queue;

.field public final b:Ljava/util/HashMap;

.field public c:Lzq1;

.field public d:I

.field public e:Lnu1$a;


# direct methods
.method public constructor <init>(Lzq1;ILnu1$a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lnu1;->a:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnu1;->b:Ljava/util/HashMap;

    iput-object p1, p0, Lnu1;->c:Lzq1;

    iput p2, p0, Lnu1;->d:I

    iput-object p3, p0, Lnu1;->e:Lnu1$a;

    return-void
.end method

.method public static synthetic a(Lnu1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lnu1;->e(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic b(Lnu1;Ljava/lang/Object;Lzq1$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lnu1;->g(Ljava/lang/Object;Lzq1$a;)V

    return-void
.end method

.method public static synthetic c(Lnu1;Ljava/lang/Object;Lzq1$a;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lnu1;->f(Ljava/lang/Object;Lzq1$a;)V

    return-void
.end method

.method private synthetic e(Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lnu1;->i(Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic f(Ljava/lang/Object;Lzq1$a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lnu1;->e:Lnu1$a;

    invoke-interface {v0, p1, p2}, Lnu1$a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method private synthetic g(Ljava/lang/Object;Lzq1$a;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lnu1;->e:Lnu1$a;

    invoke-interface {v0, p1, p2}, Lnu1$a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public d(Landroid/app/Activity;Ljava/util/concurrent/Executor;Ljava/lang/Object;)V
    .locals 5

    .line 1
    invoke-static {p3}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lnu1;->c:Lzq1;

    invoke-virtual {v0}, Lzq1;->H()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lnu1;->c:Lzq1;

    invoke-virtual {v1}, Lzq1;->A()I

    move-result v1

    iget v2, p0, Lnu1;->d:I

    and-int/2addr v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    iget-object v4, p0, Lnu1;->a:Ljava/util/Queue;

    invoke-interface {v4, p3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    new-instance v4, Llo1;

    invoke-direct {v4, p2}, Llo1;-><init>(Ljava/util/concurrent/Executor;)V

    iget-object p2, p0, Lnu1;->b:Ljava/util/HashMap;

    invoke-virtual {p2, p3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result p2

    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    const-string p2, "Activity is already destroyed!"

    invoke-static {v2, p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    invoke-static {}, Li2;->a()Li2;

    move-result-object p2

    new-instance v2, Llu1;

    invoke-direct {v2, p0, p3}, Llu1;-><init>(Lnu1;Ljava/lang/Object;)V

    invoke-virtual {p2, p1, p3, v2}, Li2;->c(Landroid/app/Activity;Ljava/lang/Object;Ljava/lang/Runnable;)V

    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    iget-object p1, p0, Lnu1;->c:Lzq1;

    invoke-virtual {p1}, Lzq1;->d0()Lzq1$a;

    move-result-object p1

    new-instance p2, Lmu1;

    invoke-direct {p2, p0, p3, p1}, Lmu1;-><init>(Lnu1;Ljava/lang/Object;Lzq1$a;)V

    invoke-virtual {v4, p2}, Llo1;->a(Ljava/lang/Runnable;)V

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public h()V
    .locals 5

    .line 1
    iget-object v0, p0, Lnu1;->c:Lzq1;

    invoke-virtual {v0}, Lzq1;->A()I

    move-result v0

    iget v1, p0, Lnu1;->d:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnu1;->c:Lzq1;

    invoke-virtual {v0}, Lzq1;->d0()Lzq1$a;

    move-result-object v0

    iget-object v1, p0, Lnu1;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lnu1;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llo1;

    if-eqz v3, :cond_0

    new-instance v4, Lku1;

    invoke-direct {v4, p0, v2, v0}, Lku1;-><init>(Lnu1;Ljava/lang/Object;Lzq1$a;)V

    invoke-virtual {v3, v4}, Llo1;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public i(Ljava/lang/Object;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lnu1;->c:Lzq1;

    invoke-virtual {v0}, Lzq1;->H()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lnu1;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lnu1;->a:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    invoke-static {}, Li2;->a()Li2;

    move-result-object v1

    invoke-virtual {v1, p1}, Li2;->b(Ljava/lang/Object;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

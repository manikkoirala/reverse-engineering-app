.class public final Lhv0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lid1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhv0$b;
    }
.end annotation


# instance fields
.field public a:Lcom/google/firebase/database/collection/b;

.field public b:Lcom/google/firebase/firestore/local/IndexManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lau;->a()Lcom/google/firebase/database/collection/b;

    move-result-object v0

    iput-object v0, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    return-void
.end method

.method public static synthetic f(Lhv0;)Lcom/google/firebase/database/collection/b;
    .locals 0

    .line 1
    iget-object p0, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/core/Query;Lcom/google/firebase/firestore/model/FieldIndex$a;Ljava/util/Set;Lga1;)Ljava/util/Map;
    .locals 5

    .line 1
    new-instance p4, Ljava/util/HashMap;

    invoke-direct {p4}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/Query;->l()Lke1;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljb;->c(Ljava/lang/String;)Ljb;

    move-result-object v0

    check-cast v0, Lke1;

    invoke-static {v0}, Ldu;->g(Lke1;)Ldu;

    move-result-object v0

    iget-object v1, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    invoke-virtual {v1, v0}, Lcom/google/firebase/database/collection/b;->m(Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzt;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldu;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/Query;->l()Lke1;

    move-result-object v3

    invoke-virtual {v1}, Ldu;->m()Lke1;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljb;->k(Ljb;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ldu;->m()Lke1;

    move-result-object v1

    invoke-virtual {v1}, Ljb;->l()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/Query;->l()Lke1;

    move-result-object v3

    invoke-virtual {v3}, Ljb;->l()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-le v1, v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/google/firebase/firestore/model/FieldIndex$a;->f(Lzt;)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/firebase/firestore/model/FieldIndex$a;->c(Lcom/google/firebase/firestore/model/FieldIndex$a;)I

    move-result v1

    if-gtz v1, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Lzt;->getKey()Ldu;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1, v2}, Lcom/google/firebase/firestore/core/Query;->r(Lzt;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Lzt;->getKey()Ldu;

    move-result-object v1

    invoke-interface {v2}, Lzt;->a()Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object v2

    invoke-interface {p4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    :goto_1
    return-object p4
.end method

.method public b(Ljava/lang/String;Lcom/google/firebase/firestore/model/FieldIndex$a;I)Ljava/util/Map;
    .locals 0

    .line 1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "getAll(String, IndexOffset, int) is not supported."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c(Lcom/google/firebase/firestore/model/MutableDocument;Lqo1;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lhv0;->b:Lcom/google/firebase/firestore/local/IndexManager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "setIndexManager() not called"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lqo1;->b:Lqo1;

    invoke-virtual {p2, v0}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    const-string v1, "Cannot add document to the RemoteDocumentCache with a read time of zero"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->a()Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/firebase/firestore/model/MutableDocument;->v(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Lcom/google/firebase/database/collection/b;->l(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/firebase/database/collection/b;

    move-result-object p2

    iput-object p2, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    iget-object p2, p0, Lhv0;->b:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object p1

    invoke-virtual {p1}, Ldu;->k()Lke1;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/google/firebase/firestore/local/IndexManager;->a(Lke1;)V

    return-void
.end method

.method public d(Ldu;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    invoke-virtual {v0, p1}, Lcom/google/firebase/database/collection/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzt;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lzt;->a()Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->q(Ldu;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public e(Lcom/google/firebase/firestore/local/IndexManager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lhv0;->b:Lcom/google/firebase/firestore/local/IndexManager;

    return-void
.end method

.method public g(Lzk0;)J
    .locals 5

    .line 1
    new-instance v0, Lhv0$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhv0$b;-><init>(Lhv0;Lhv0$a;)V

    invoke-virtual {v0}, Lhv0$b;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzt;

    invoke-virtual {p1, v3}, Lzk0;->k(Lzt;)Lcom/google/firebase/firestore/proto/MaybeDocument;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protobuf/GeneratedMessageLite;->b()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public getAll(Ljava/lang/Iterable;)Ljava/util/Map;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldu;

    invoke-virtual {p0, v1}, Lhv0;->d(Ldu;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public h()Ljava/lang/Iterable;
    .locals 2

    .line 1
    new-instance v0, Lhv0$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhv0$b;-><init>(Lhv0;Lhv0$a;)V

    return-object v0
.end method

.method public removeAll(Ljava/util/Collection;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lhv0;->b:Lcom/google/firebase/firestore/local/IndexManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "setIndexManager() not called"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lau;->a()Lcom/google/firebase/database/collection/b;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldu;

    iget-object v2, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    invoke-virtual {v2, v1}, Lcom/google/firebase/database/collection/b;->n(Ljava/lang/Object;)Lcom/google/firebase/database/collection/b;

    move-result-object v2

    iput-object v2, p0, Lhv0;->a:Lcom/google/firebase/database/collection/b;

    sget-object v2, Lqo1;->b:Lqo1;

    invoke-static {v1, v2}, Lcom/google/firebase/firestore/model/MutableDocument;->r(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/database/collection/b;->l(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/firebase/database/collection/b;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lhv0;->b:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-interface {p1, v0}, Lcom/google/firebase/firestore/local/IndexManager;->b(Lcom/google/firebase/database/collection/b;)V

    return-void
.end method

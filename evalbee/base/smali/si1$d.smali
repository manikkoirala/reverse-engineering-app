.class public Lsi1$d;
.super Lsi1$f;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsi1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public a:Lsi1$c;

.field public b:Z

.field public final synthetic c:Lsi1;


# direct methods
.method public constructor <init>(Lsi1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lsi1$d;->c:Lsi1;

    invoke-direct {p0}, Lsi1$f;-><init>()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lsi1$d;->b:Z

    return-void
.end method


# virtual methods
.method public b(Lsi1$c;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lsi1$d;->a:Lsi1$c;

    if-ne p1, v0, :cond_1

    iget-object p1, v0, Lsi1$c;->d:Lsi1$c;

    iput-object p1, p0, Lsi1$d;->a:Lsi1$c;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lsi1$d;->b:Z

    :cond_1
    return-void
.end method

.method public c()Ljava/util/Map$Entry;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lsi1$d;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lsi1$d;->b:Z

    iget-object v0, p0, Lsi1$d;->c:Lsi1;

    iget-object v0, v0, Lsi1;->a:Lsi1$c;

    :goto_0
    iput-object v0, p0, Lsi1$d;->a:Lsi1$c;

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lsi1$d;->a:Lsi1$c;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lsi1$c;->c:Lsi1$c;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :goto_1
    iget-object v0, p0, Lsi1$d;->a:Lsi1$c;

    return-object v0
.end method

.method public hasNext()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lsi1$d;->b:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsi1$d;->c:Lsi1;

    iget-object v0, v0, Lsi1;->a:Lsi1$c;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lsi1$d;->a:Lsi1$c;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lsi1$c;->c:Lsi1$c;

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    return v1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lsi1$d;->c()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

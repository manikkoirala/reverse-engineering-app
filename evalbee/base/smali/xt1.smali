.class public Lxt1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt90;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([DI)D
    .locals 4

    .line 1
    const/4 p2, 0x0

    aget-wide v0, p1, p2

    const-wide/high16 p1, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, p1

    const-wide p1, 0x4005bf0a8b145769L    # Math.E

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double v2, p1, v0

    add-double/2addr p1, v0

    div-double/2addr v2, p1

    return-wide v2
.end method

.method public b(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "tanh(x)"

    return-object v0
.end method

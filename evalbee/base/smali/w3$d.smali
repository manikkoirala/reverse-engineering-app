.class public Lw3$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw3;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lw3;


# direct methods
.method public constructor <init>(Lw3;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lw3$d;->a:Lw3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 1
    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->i:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lw3$d;->a:Lw3;

    iget-object v0, v0, Lw3;->m:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lw3$d;->a:Lw3;

    iget-object v0, v0, Lw3;->j:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lw3$d;->a:Lw3;

    iget-object v0, v0, Lw3;->l:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v2, 0x7f08016e

    const v6, 0x7f0800bd

    if-eqz v0, :cond_0

    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->a:Landroid/content/Context;

    const v0, 0x7f1200bb

    invoke-static {p1, v0, v6, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->a:Landroid/content/Context;

    const v0, 0x7f1200bd

    invoke-static {p1, v0, v6, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->a:Landroid/content/Context;

    const v0, 0x7f12019a

    invoke-static {p1, v0, v6, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    const-string v0, "[0-9]+"

    invoke-virtual {v5, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-gt v0, v1, :cond_4

    :cond_3
    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->a:Landroid/content/Context;

    const v0, 0x7f12019b

    invoke-static {p1, v0, v6, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_4
    new-instance v0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v1, p0, Lw3$d;->a:Lw3;

    invoke-static {v1}, Lw3;->b(Lw3;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lw3$d;->a:Lw3;

    iget-object v1, v1, Lw3;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget-object v2, p0, Lw3$d;->a:Lw3;

    invoke-static {v2}, Lw3;->b(Lw3;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteStudent(ILjava/lang/String;)Z

    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateStudent(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)Z

    iget-object p1, p0, Lw3$d;->a:Lw3;

    iget-object p1, p1, Lw3;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "StudentAdd"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lw3$d;->a:Lw3;

    invoke-static {p1}, Lw3;->c(Lw3;)Ly01;

    move-result-object p1

    invoke-interface {p1, v1}, Ly01;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Lw3$d;->a:Lw3;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

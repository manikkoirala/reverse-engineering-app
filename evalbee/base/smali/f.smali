.class public abstract Lf;
.super Ln;
.source "SourceFile"


# instance fields
.field public final a:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ln;-><init>()V

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lf;->a:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(I)Lc81;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lf;->a(I)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public a(I)Lqc0;
    .locals 1

    .line 1
    iget-object v0, p0, Lf;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 p1, 0x4

    invoke-virtual {p0, p1}, Lf;->l(I)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(J)Lc81;
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lf;->b(J)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public b(J)Lqc0;
    .locals 1

    .line 1
    iget-object v0, p0, Lf;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lf;->l(I)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic f([B)Lc81;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf;->j([B)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public h([BII)Lqc0;
    .locals 2

    .line 1
    add-int v0, p2, p3

    array-length v1, p1

    invoke-static {p2, v0, v1}, Li71;->w(III)V

    invoke-virtual {p0, p1, p2, p3}, Lf;->p([BII)V

    return-object p0
.end method

.method public i(Ljava/nio/ByteBuffer;)Lqc0;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf;->n(Ljava/nio/ByteBuffer;)V

    return-object p0
.end method

.method public j([B)Lqc0;
    .locals 0

    .line 1
    invoke-static {p1}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lf;->o([B)V

    return-object p0
.end method

.method public k(C)Lqc0;
    .locals 1

    .line 1
    iget-object v0, p0, Lf;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Lf;->l(I)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public final l(I)Lqc0;
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lf;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lf;->p([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lf;->a:Ljava/nio/ByteBuffer;

    invoke-static {p1}, Lvg0;->a(Ljava/nio/Buffer;)V

    return-object p0

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lf;->a:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lvg0;->a(Ljava/nio/Buffer;)V

    throw p1
.end method

.method public abstract m(B)V
.end method

.method public n(Ljava/nio/ByteBuffer;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lf;->p([BII)V

    invoke-virtual {p1}, Ljava/nio/Buffer;->limit()I

    move-result v0

    invoke-static {p1, v0}, Lvg0;->c(Ljava/nio/Buffer;I)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    :goto_0
    if-lez v0, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-virtual {p0, v1}, Lf;->m(B)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public o([B)V
    .locals 2

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lf;->p([BII)V

    return-void
.end method

.method public abstract p([BII)V
.end method

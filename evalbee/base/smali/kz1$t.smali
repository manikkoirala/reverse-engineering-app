.class public Lkz1$t;
.super Lhz1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkz1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lhz1;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lrh0;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lkz1$t;->e(Lrh0;)Lnh0;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lvh0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lnh0;

    invoke-virtual {p0, p1, p2}, Lkz1$t;->h(Lvh0;Lnh0;)V

    return-void
.end method

.method public e(Lrh0;)Lnh0;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lkz1$t;->g(Lrh0;Lcom/google/gson/stream/JsonToken;)Lnh0;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lkz1$t;->f(Lrh0;Lcom/google/gson/stream/JsonToken;)Lnh0;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lrh0;->k()Z

    move-result v2

    if-eqz v2, :cond_6

    instance-of v2, v1, Lph0;

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lrh0;->K()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lkz1$t;->g(Lrh0;Lcom/google/gson/stream/JsonToken;)Lnh0;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-nez v4, :cond_4

    invoke-virtual {p0, p1, v3}, Lkz1$t;->f(Lrh0;Lcom/google/gson/stream/JsonToken;)Lnh0;

    move-result-object v4

    :cond_4
    instance-of v3, v1, Lih0;

    if-eqz v3, :cond_5

    move-object v2, v1

    check-cast v2, Lih0;

    invoke-virtual {v2, v4}, Lih0;->n(Lnh0;)V

    goto :goto_3

    :cond_5
    move-object v3, v1

    check-cast v3, Lph0;

    invoke-virtual {v3, v2, v4}, Lph0;->n(Ljava/lang/String;Lnh0;)V

    :goto_3
    if-eqz v5, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    move-object v1, v4

    goto :goto_0

    :cond_6
    instance-of v2, v1, Lih0;

    if-eqz v2, :cond_7

    invoke-virtual {p1}, Lrh0;->f()V

    goto :goto_4

    :cond_7
    invoke-virtual {p1}, Lrh0;->g()V

    :goto_4
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    return-object v1

    :cond_8
    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnh0;

    goto :goto_0
.end method

.method public final f(Lrh0;Lcom/google/gson/stream/JsonToken;)Lnh0;
    .locals 2

    .line 1
    sget-object v0, Lkz1$a0;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lrh0;->R()V

    sget-object p1, Loh0;->a:Loh0;

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Lqh0;

    invoke-virtual {p1}, Lrh0;->x()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p2, p1}, Lqh0;-><init>(Ljava/lang/Boolean;)V

    return-object p2

    :cond_2
    new-instance p2, Lqh0;

    invoke-virtual {p1}, Lrh0;->i0()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lqh0;-><init>(Ljava/lang/String;)V

    return-object p2

    :cond_3
    invoke-virtual {p1}, Lrh0;->i0()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Lqh0;

    new-instance v0, Lcom/google/gson/internal/LazilyParsedNumber;

    invoke-direct {v0, p1}, Lcom/google/gson/internal/LazilyParsedNumber;-><init>(Ljava/lang/String;)V

    invoke-direct {p2, v0}, Lqh0;-><init>(Ljava/lang/Number;)V

    return-object p2
.end method

.method public final g(Lrh0;Lcom/google/gson/stream/JsonToken;)Lnh0;
    .locals 1

    .line 1
    sget-object v0, Lkz1$a0;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x4

    if-eq p2, v0, :cond_1

    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lrh0;->b()V

    new-instance p1, Lph0;

    invoke-direct {p1}, Lph0;-><init>()V

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lrh0;->a()V

    new-instance p1, Lih0;

    invoke-direct {p1}, Lih0;-><init>()V

    return-object p1
.end method

.method public h(Lvh0;Lnh0;)V
    .locals 2

    .line 1
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lnh0;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p2}, Lnh0;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lnh0;->c()Lqh0;

    move-result-object p2

    invoke-virtual {p2}, Lqh0;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lqh0;->o()Ljava/lang/Number;

    move-result-object p2

    invoke-virtual {p1, p2}, Lvh0;->q0(Ljava/lang/Number;)Lvh0;

    goto/16 :goto_3

    :cond_1
    invoke-virtual {p2}, Lqh0;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lqh0;->n()Z

    move-result p2

    invoke-virtual {p1, p2}, Lvh0;->s0(Z)Lvh0;

    goto/16 :goto_3

    :cond_2
    invoke-virtual {p2}, Lqh0;->p()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lvh0;->r0(Ljava/lang/String;)Lvh0;

    goto/16 :goto_3

    :cond_3
    invoke-virtual {p2}, Lnh0;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lvh0;->c()Lvh0;

    invoke-virtual {p2}, Lnh0;->a()Lih0;

    move-result-object p2

    invoke-virtual {p2}, Lih0;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnh0;

    invoke-virtual {p0, p1, v0}, Lkz1$t;->h(Lvh0;Lnh0;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lvh0;->f()Lvh0;

    goto :goto_3

    :cond_5
    invoke-virtual {p2}, Lnh0;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lvh0;->d()Lvh0;

    invoke-virtual {p2}, Lnh0;->b()Lph0;

    move-result-object p2

    invoke-virtual {p2}, Lph0;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lvh0;->o(Ljava/lang/String;)Lvh0;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnh0;

    invoke-virtual {p0, p1, v0}, Lkz1$t;->h(Lvh0;Lnh0;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lvh0;->g()Lvh0;

    goto :goto_3

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t write "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_2
    invoke-virtual {p1}, Lvh0;->u()Lvh0;

    :goto_3
    return-void
.end method

.class public Ly02;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Lgj0;
.implements Ljava/util/RandomAccess;


# instance fields
.field public final a:Lgj0;


# direct methods
.method public constructor <init>(Lgj0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    iput-object p1, p0, Ly02;->a:Lgj0;

    return-void
.end method

.method public static synthetic a(Ly02;)Lgj0;
    .locals 0

    .line 1
    iget-object p0, p0, Ly02;->a:Lgj0;

    return-object p0
.end method


# virtual methods
.method public b(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Ly02;->a:Lgj0;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public d()Lgj0;
    .locals 0

    .line 1
    return-object p0
.end method

.method public f()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Ly02;->a:Lgj0;

    invoke-interface {v0}, Lgj0;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Ly02;->b(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .line 1
    new-instance v0, Ly02$b;

    invoke-direct {v0, p0}, Ly02$b;-><init>(Ly02;)V

    return-object v0
.end method

.method public k(I)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Ly02;->a:Lgj0;

    invoke-interface {v0, p1}, Lgj0;->k(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .line 1
    new-instance v0, Ly02$a;

    invoke-direct {v0, p0, p1}, Ly02$a;-><init>(Ly02;I)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .line 1
    iget-object v0, p0, Ly02;->a:Lgj0;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public x(Landroidx/datastore/preferences/protobuf/ByteString;)V
    .locals 0

    .line 1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

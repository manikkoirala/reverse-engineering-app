.class public Lgs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgs;->a:Landroid/content/Context;

    iput-object p2, p0, Lgs;->b:Ly01;

    iput-object p3, p0, Lgs;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lgs;->d()V

    return-void
.end method

.method public static synthetic a(Lgs;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lgs;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic b(Lgs;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lgs;->c:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic c(Lgs;)Ly01;
    .locals 0

    .line 1
    iget-object p0, p0, Lgs;->b:Ly01;

    return-object p0
.end method


# virtual methods
.method public final d()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lgs;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lgs;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0066

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f12009c

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0902da

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    const v3, 0x7f0902d9

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    new-instance v1, Lgs$a;

    invoke-direct {v1, p0, v2}, Lgs$a;-><init>(Lgs;Landroid/widget/RadioButton;)V

    const v2, 0x7f120097

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lgs$b;

    invoke-direct {v1, p0}, Lgs$b;-><init>(Lgs;)V

    const v2, 0x7f120053

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

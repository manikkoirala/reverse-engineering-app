.class public final Laz0$a;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Laz0;-><init>(Landroid/content/Context;Lhu1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Laz0;


# direct methods
.method public constructor <init>(Laz0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Laz0$a;->a:Laz0;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V
    .locals 3

    .line 1
    const-string v0, "network"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "capabilities"

    invoke-static {p2, p1}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    invoke-static {}, Lbz0;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Network capabilities changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Laz0$a;->a:Laz0;

    invoke-static {p1}, Laz0;->j(Laz0;)Landroid/net/ConnectivityManager;

    move-result-object p2

    invoke-static {p2}, Lbz0;->c(Landroid/net/ConnectivityManager;)Lzy0;

    move-result-object p2

    invoke-virtual {p1, p2}, Ltk;->g(Ljava/lang/Object;)V

    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 2

    .line 1
    const-string v0, "network"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    invoke-static {}, Lbz0;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Network connection lost"

    invoke-virtual {p1, v0, v1}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Laz0$a;->a:Laz0;

    invoke-static {p1}, Laz0;->j(Laz0;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-static {v0}, Lbz0;->c(Landroid/net/ConnectivityManager;)Lzy0;

    move-result-object v0

    invoke-virtual {p1, v0}, Ltk;->g(Ljava/lang/Object;)V

    return-void
.end method

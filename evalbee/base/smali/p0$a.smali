.class public final Lp0$a;
.super Landroid/view/View$AccessibilityDelegate;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lp0;


# direct methods
.method public constructor <init>(Lp0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    iput-object p1, p0, Lp0$a;->a:Lp0;

    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2}, Lp0;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1
.end method

.method public getAccessibilityNodeProvider(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1}, Lp0;->getAccessibilityNodeProvider(Landroid/view/View;)Lo1;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lo1;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/accessibility/AccessibilityNodeProvider;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2}, Lp0;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2

    .line 1
    invoke-static {p2}, Ln1;->B0(Landroid/view/accessibility/AccessibilityNodeInfo;)Ln1;

    move-result-object v0

    invoke-static {p1}, Lo32;->X(Landroid/view/View;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ln1;->s0(Z)V

    invoke-static {p1}, Lo32;->S(Landroid/view/View;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ln1;->i0(Z)V

    invoke-static {p1}, Lo32;->p(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln1;->n0(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lo32;->I(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln1;->w0(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v1, p1, v0}, Lp0;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Ln1;)V

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2, p1}, Ln1;->e(Ljava/lang/CharSequence;Landroid/view/View;)V

    invoke-static {p1}, Lp0;->getActionList(Landroid/view/View;)Ljava/util/List;

    move-result-object p1

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ln1$a;

    invoke-virtual {v0, v1}, Ln1;->b(Ln1$a;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2}, Lp0;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2, p3}, Lp0;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2, p3}, Lp0;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result p1

    return p1
.end method

.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2}, Lp0;->sendAccessibilityEvent(Landroid/view/View;I)V

    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lp0$a;->a:Lp0;

    invoke-virtual {v0, p1, p2}, Lp0;->sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

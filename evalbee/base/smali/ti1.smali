.class public Lti1;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public c:Ly01;

.field public d:Lcom/google/android/material/textfield/TextInputLayout;

.field public e:Landroid/widget/AutoCompleteTextView;

.field public f:Lcom/google/android/material/textfield/TextInputLayout;

.field public g:Ljava/util/ArrayList;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    new-instance v0, Lti1$a;

    invoke-direct {v0, p0}, Lti1$a;-><init>(Lti1;)V

    iput-object v0, p0, Lti1;->i:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lti1;->a:Landroid/content/Context;

    iput-object p2, p0, Lti1;->c:Ly01;

    iput-object p3, p0, Lti1;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p4, p0, Lti1;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-void
.end method

.method public static synthetic a(Lti1;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lti1;->g:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic b(Lti1;)Landroid/widget/AutoCompleteTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lti1;->e:Landroid/widget/AutoCompleteTextView;

    return-object p0
.end method

.method public static synthetic c(Lti1;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lti1;->h()V

    return-void
.end method

.method public static synthetic d(Lti1;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lti1;->g(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result p0

    return p0
.end method

.method public static synthetic e(Lti1;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lti1;->j(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    return-void
.end method

.method public static synthetic f(Lti1;)Ly01;
    .locals 0

    .line 1
    iget-object p0, p0, Lti1;->c:Ly01;

    return-object p0
.end method


# virtual methods
.method public final g(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lti1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final h()V
    .locals 3

    .line 1
    iget-object v0, p0, Lti1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getAllClassNames()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lti1;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lti1;->a:Landroid/content/Context;

    const v2, 0x7f120028

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lh3;

    iget-object v1, p0, Lti1;->a:Landroid/content/Context;

    iget-object v2, p0, Lti1;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lh3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v1, p0, Lti1;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final i()V
    .locals 2

    .line 1
    const v0, 0x7f09044d

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    const v1, 0x7f1202a8

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    new-instance v1, Lti1$b;

    invoke-direct {v1, p0}, Lti1$b;-><init>(Lti1;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final j(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lti1;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f120031

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lti1;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1201c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lti1$h;

    invoke-direct {v1, p0, p1}, Lti1$h;-><init>(Lti1;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    const-string p1, "Yes"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    new-instance v1, Lti1$g;

    invoke-direct {v1, p0}, Lti1$g;-><init>(Lti1;)V

    const-string v2, "No"

    invoke-virtual {p1, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c007b

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lti1;->i()V

    const p1, 0x7f090397

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f090394

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lti1;->f:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f090396

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lti1;->d:Lcom/google/android/material/textfield/TextInputLayout;

    iget-object v0, p0, Lti1;->f:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lti1;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0}, Lti1;->h()V

    const v0, 0x7f0900ce

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lti1;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lti1;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lti1;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lti1;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lti1;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lti1$c;

    invoke-direct {v2, p0}, Lti1$c;-><init>(Lti1;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lti1;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    new-instance v2, Lti1$d;

    invoke-direct {v2, p0}, Lti1$d;-><init>(Lti1;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lti1;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v1, p0, Lti1;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lti1;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lti1;->d:Lcom/google/android/material/textfield/TextInputLayout;

    iget-object v2, p0, Lti1;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/material/textfield/TextInputLayout;->setEndIconOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lti1$e;

    invoke-direct {v1, p0, p1}, Lti1$e;-><init>(Lti1;Lcom/google/android/material/textfield/TextInputLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090088

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lti1$f;

    invoke-direct {v0, p0}, Lti1$f;-><init>(Lti1;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

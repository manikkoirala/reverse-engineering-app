.class public Lbf$c;
.super Lbf;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbf;->c(Ljava/lang/String;Lc92;Z)Lbf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic b:Lc92;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Z


# direct methods
.method public constructor <init>(Lc92;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbf$c;->b:Lc92;

    iput-object p2, p0, Lbf$c;->c:Ljava/lang/String;

    iput-boolean p3, p0, Lbf$c;->d:Z

    invoke-direct {p0}, Lbf;-><init>()V

    return-void
.end method


# virtual methods
.method public h()V
    .locals 4

    .line 1
    iget-object v0, p0, Lbf$c;->b:Lc92;

    invoke-virtual {v0}, Lc92;->o()Landroidx/work/impl/WorkDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    :try_start_0
    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->K()Lq92;

    move-result-object v1

    iget-object v2, p0, Lbf$c;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Lq92;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lbf$c;->b:Lc92;

    invoke-virtual {p0, v3, v2}, Lbf;->a(Lc92;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    iget-boolean v0, p0, Lbf$c;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf$c;->b:Lc92;

    invoke-virtual {p0, v0}, Lbf;->g(Lc92;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    throw v1
.end method

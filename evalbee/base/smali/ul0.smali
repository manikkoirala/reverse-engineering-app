.class public Lul0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lul0$b;
    }
.end annotation


# static fields
.field public static final c:Lul0$b;


# instance fields
.field public final a:Lz00;

.field public b:Ly00;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lul0$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lul0$b;-><init>(Lul0$a;)V

    sput-object v0, Lul0;->c:Lul0$b;

    return-void
.end method

.method public constructor <init>(Lz00;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lul0;->a:Lz00;

    sget-object p1, Lul0;->c:Lul0$b;

    iput-object p1, p0, Lul0;->b:Ly00;

    return-void
.end method

.method public constructor <init>(Lz00;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lul0;-><init>(Lz00;)V

    invoke-virtual {p0, p2}, Lul0;->e(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 1
    iget-object v0, p0, Lul0;->b:Ly00;

    invoke-interface {v0}, Ly00;->b()V

    return-void
.end method

.method public b()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lul0;->b:Ly00;

    invoke-interface {v0}, Ly00;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lul0;->b:Ly00;

    invoke-interface {v0}, Ly00;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .line 1
    iget-object v0, p0, Lul0;->a:Lz00;

    const-string v1, "userlog"

    invoke-virtual {v0, p1, v1}, Lz00;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lul0;->b:Ly00;

    invoke-interface {v0}, Ly00;->d()V

    sget-object v0, Lul0;->c:Lul0$b;

    iput-object v0, p0, Lul0;->b:Ly00;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lul0;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    const/high16 v0, 0x10000

    invoke-virtual {p0, p1, v0}, Lul0;->f(Ljava/io/File;I)V

    return-void
.end method

.method public f(Ljava/io/File;I)V
    .locals 1

    .line 1
    new-instance v0, Lma1;

    invoke-direct {v0, p1, p2}, Lma1;-><init>(Ljava/io/File;I)V

    iput-object v0, p0, Lul0;->b:Ly00;

    return-void
.end method

.method public g(JLjava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lul0;->b:Ly00;

    invoke-interface {v0, p1, p2, p3}, Ly00;->c(JLjava/lang/String;)V

    return-void
.end method

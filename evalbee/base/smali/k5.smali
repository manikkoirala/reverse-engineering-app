.class public Lk5;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lk5$f;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

.field public c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

.field public d:Ljava/util/ArrayList;

.field public e:Lk5$f;

.field public f:[Lsf;

.field public g:[Landroid/widget/EditText;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public i:[Z

.field public j:Z

.field public k:I

.field public l:I

.field public m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lk5$f;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lk5;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lk5;->j:Z

    const/16 v1, 0x20

    iput v1, p0, Lk5;->k:I

    const/16 v1, 0x28

    iput v1, p0, Lk5;->l:I

    new-instance v1, Lk5$a;

    invoke-direct {v1, p0}, Lk5$a;-><init>(Lk5;)V

    iput-object v1, p0, Lk5;->m:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lk5;->a:Landroid/content/Context;

    iput-object p2, p0, Lk5;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iput-object p4, p0, Lk5;->e:Lk5$f;

    iput-object p3, p0, Lk5;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iput-object p5, p0, Lk5;->h:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iput-boolean v0, p0, Lk5;->j:Z

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object p4

    invoke-static {p4}, Le5;->a([Z)[Z

    move-result-object p4

    iput-object p4, p0, Lk5;->i:[Z

    iget-object v4, p0, Lk5;->m:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lk5;->d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object p1

    sget-object p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne p1, p3, :cond_0

    iget-object p1, p0, Lk5;->g:[Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lk5;->r([Landroid/widget/EditText;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lk5;->f:[Lsf;

    iget-object p2, p0, Lk5;->i:[Z

    invoke-virtual {p0, p1, p2}, Lk5;->q([Lsf;[Z)V

    :goto_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lk5$f;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 6

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lk5;->d:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lk5;->j:Z

    const/16 v0, 0x20

    iput v0, p0, Lk5;->k:I

    const/16 v0, 0x28

    iput v0, p0, Lk5;->l:I

    new-instance v0, Lk5$a;

    invoke-direct {v0, p0}, Lk5$a;-><init>(Lk5;)V

    iput-object v0, p0, Lk5;->m:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lk5;->a:Landroid/content/Context;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-direct {v0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)V

    iput-object v0, p0, Lk5;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iput-object p4, p0, Lk5;->e:Lk5$f;

    iput-object p3, p0, Lk5;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iput-object p5, p0, Lk5;->h:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 p2, 0x1

    iput-boolean p2, p0, Lk5;->j:Z

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object p2

    invoke-static {p2}, Le5;->a([Z)[Z

    move-result-object p2

    iput-object p2, p0, Lk5;->i:[Z

    iget-object v2, p0, Lk5;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iget-object v4, p0, Lk5;->m:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lk5;->d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    iget-object p1, p0, Lk5;->f:[Lsf;

    iget-object p2, p0, Lk5;->i:[Z

    invoke-virtual {p0, p1, p2}, Lk5;->q([Lsf;[Z)V

    return-void
.end method

.method public static synthetic a(Lk5;[Lsf;[Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lk5;->q([Lsf;[Z)V

    return-void
.end method

.method public static synthetic b(Lk5;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lk5;->o()V

    return-void
.end method


# virtual methods
.method public final c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 10

    .line 1
    const/16 v0, 0x8

    invoke-static {v0, p1}, La91;->d(ILandroid/content/Context;)I

    move-result v0

    iget v1, p0, Lk5;->k:I

    invoke-static {v1, p1}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v3, p0, Lk5;->l:I

    invoke-static {v3, p1}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v0, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v3, Lsf;

    invoke-direct {v3, p1}, Lsf;-><init>(Landroid/content/Context;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v5, -0x1000000

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v3, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v3, p1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v6, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v6, v1, v1}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {p4, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getOptionLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;

    move-result-object p4

    array-length v0, p4

    new-array v1, v0, [Lsf;

    move v7, v5

    :goto_0
    if-ge v7, v0, :cond_0

    new-instance v8, Lsf;

    invoke-direct {v8, p1}, Lsf;-><init>(Landroid/content/Context;)V

    aput-object v8, v1, v7

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v9

    invoke-virtual {v8, v9}, Lsf;->setQuestionNo(I)V

    aget-object v8, v1, v7

    invoke-virtual {v8, v7}, Lsf;->setSubIndex(I)V

    aget-object v8, v1, v7

    invoke-virtual {v8, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v8, v1, v7

    aget-object v9, p4, v7

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v8, v1, v7

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setGravity(I)V

    aget-object v8, v1, v7

    invoke-virtual {v3, v8, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    invoke-direct {p1, v5, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v2, v3, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v1, p0, Lk5;->f:[Lsf;

    return-void
.end method

.method public final d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 2

    .line 1
    sget-object v0, Lk5$e;->a:[I

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, p1, p2, p4, p5}, Lk5;->e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p2, p4, p5}, Lk5;->i(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1, p2, p4, p5}, Lk5;->h(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1, p2, p4, p5}, Lk5;->j(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p1, p2, p4, p5}, Lk5;->c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    :goto_0
    if-eqz p3, :cond_0

    iget-boolean p1, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {p0, p3, p2, p5}, Lk5;->n(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Landroid/widget/LinearLayout;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-boolean v2, v0, Lk5;->j:Z

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p4}, Lk5;->f(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void

    :cond_0
    const/16 v2, 0x8

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    iget v3, v0, Lk5;->k:I

    invoke-static {v3, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    iget v4, v0, Lk5;->l:I

    invoke-static {v4, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    const/4 v5, 0x2

    new-array v6, v5, [Landroid/widget/EditText;

    iput-object v6, v0, Lk5;->g:[Landroid/widget/EditText;

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v5, :cond_3

    new-instance v8, Landroid/widget/LinearLayout;

    invoke-direct {v8, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v10, Lsf;

    invoke-direct {v10, v1}, Lsf;-><init>(Landroid/content/Context;)V

    const-string v11, ""

    if-nez v7, :cond_1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :cond_1
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v11, -0x1000000

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v8, v10, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v9, 0x11

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v10, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v10, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v6}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v12, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v12, v3, v3}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v13, Landroid/widget/TextView;

    invoke-direct {v13, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v9}, Landroid/widget/TextView;->setGravity(I)V

    if-nez v7, :cond_2

    const-string v14, ">="

    goto :goto_1

    :cond_2
    const-string v14, "<="

    :goto_1
    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v10, v13, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v12, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    mul-int/lit8 v13, v3, 0x4

    const/4 v14, -0x2

    invoke-direct {v12, v13, v14}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v13, v0, Lk5;->g:[Landroid/widget/EditText;

    new-instance v15, Landroid/widget/EditText;

    invoke-direct {v15, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    aput-object v15, v13, v7

    iget-object v13, v0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v13, v13, v7

    new-instance v15, Lk5$b;

    invoke-direct {v15, v0}, Lk5$b;-><init>(Lk5;)V

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v13, v0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v13, v13, v7

    const/16 v15, 0x3002

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v13, v0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v13, v13, v7

    invoke-virtual {v13, v9}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v9, v0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v9, v9, v7

    new-array v11, v11, [Landroid/text/InputFilter;

    new-instance v13, Landroid/text/InputFilter$LengthFilter;

    const/16 v15, 0xa

    invoke-direct {v13, v15}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v13, v11, v6

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    iget-object v9, v0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v9, v9, v7

    invoke-virtual {v10, v9, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v6, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v11, 0x3f800000    # 1.0f

    iput v11, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v8, v10, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v9, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method public final f(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    const/16 v3, 0x8

    invoke-static {v3, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    iget v4, v0, Lk5;->k:I

    invoke-static {v4, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v5

    iget-boolean v6, v5, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v6, :cond_0

    const/16 v6, 0xb

    goto :goto_0

    :cond_0
    const/16 v6, 0xa

    :goto_0
    iget v8, v5, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    mul-int v9, v8, v6

    iget-boolean v10, v5, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    if-eqz v10, :cond_1

    add-int/lit8 v9, v9, 0x1

    :cond_1
    new-array v11, v9, [Lsf;

    const/high16 v14, -0x1000000

    const/4 v7, 0x1

    const/4 v12, 0x0

    if-eqz v10, :cond_2

    new-instance v10, Landroid/widget/LinearLayout;

    invoke-direct {v10, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v13, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v13, v0, Lk5;->l:I

    invoke-static {v13, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v13

    new-instance v15, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v15, v13, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v15, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v13, Lsf;

    invoke-direct {v13, v1}, Lsf;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v13, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v13, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v13, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v12}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    invoke-virtual {v13, v7}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v15, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v15, v4, v4}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v15, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v12, Lsf;

    invoke-direct {v12, v1}, Lsf;-><init>(Landroid/content/Context;)V

    const-string v7, "-"

    invoke-virtual {v12, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 v7, 0x11

    invoke-virtual {v12, v7}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v7, 0x1

    sub-int/2addr v9, v7

    invoke-virtual {v12, v9}, Lsf;->setSubIndex(I)V

    invoke-virtual {v12, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v7

    invoke-virtual {v12, v7}, Lsf;->setQuestionNo(I)V

    aput-object v12, v11, v9

    invoke-virtual {v13, v12, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v9, -0x2

    const/4 v12, 0x0

    invoke-direct {v7, v12, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v9, 0x3f800000    # 1.0f

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v10, v13, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v7, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v7

    new-array v6, v6, [Ljava/lang/String;

    iget-boolean v5, v5, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v5, :cond_4

    const-string v5, "."

    const/4 v9, 0x0

    aput-object v5, v6, v9

    const/4 v5, 0x0

    const/16 v9, 0xa

    :goto_1
    if-ge v5, v9, :cond_3

    add-int/lit8 v10, v5, 0x1

    aget-object v5, v7, v5

    aput-object v5, v6, v10

    move v5, v10

    goto :goto_1

    :cond_3
    move-object v7, v6

    :cond_4
    const/4 v12, 0x0

    :goto_2
    array-length v5, v7

    if-ge v12, v5, :cond_7

    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v6, v0, Lk5;->l:I

    invoke-static {v6, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v6

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v6, Lsf;

    invoke-direct {v6, v1}, Lsf;-><init>(Landroid/content/Context;)V

    if-nez v12, :cond_5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v13, ""

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v5, v6, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v9, 0x11

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v6, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v6, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v10, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v10, v4, v4}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v10, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/4 v13, 0x0

    :goto_3
    if-ge v13, v8, :cond_6

    mul-int v15, v12, v8

    add-int/2addr v15, v13

    new-instance v9, Lsf;

    invoke-direct {v9, v1}, Lsf;-><init>(Landroid/content/Context;)V

    aput-object v9, v11, v15

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v14

    invoke-virtual {v9, v14}, Lsf;->setQuestionNo(I)V

    aget-object v9, v11, v15

    invoke-virtual {v9, v15}, Lsf;->setSubIndex(I)V

    aget-object v9, v11, v15

    invoke-virtual {v9, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v9, v11, v15

    aget-object v14, v7, v12

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v9, v11, v15

    const/16 v14, 0x11

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setGravity(I)V

    aget-object v9, v11, v15

    invoke-virtual {v6, v9, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v13, v13, 0x1

    const/4 v9, 0x1

    const/high16 v14, -0x1000000

    goto :goto_3

    :cond_6
    const/16 v14, 0x11

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v13, 0x0

    invoke-direct {v9, v13, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v15, 0x3f800000    # 1.0f

    iput v15, v9, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v5, v6, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    const/high16 v14, -0x1000000

    goto/16 :goto_2

    :cond_7
    iput-object v11, v0, Lk5;->f:[Lsf;

    return-void
.end method

.method public final g(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v3

    const/16 v4, 0x8

    invoke-static {v4, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    iget v5, v0, Lk5;->k:I

    invoke-static {v5, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v5

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v7, v0, Lk5;->l:I

    invoke-static {v7, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v7

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v7, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    const/16 v5, 0xa

    mul-int/2addr v4, v5

    iget-boolean v7, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v7, :cond_0

    add-int/lit8 v4, v4, 0x1

    :cond_0
    new-array v9, v4, [Lsf;

    const-string v10, " "

    const/high16 v11, -0x1000000

    const/16 v12, 0x11

    const/4 v14, 0x5

    const/4 v15, 0x1

    if-eqz v7, :cond_3

    new-instance v7, Landroid/widget/LinearLayout;

    invoke-direct {v7, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v13, Lsf;

    invoke-direct {v13, v1}, Lsf;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v13, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v13, v11}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v14, :cond_2

    new-instance v5, Lsf;

    invoke-direct {v5, v1}, Lsf;-><init>(Landroid/content/Context;)V

    if-ne v13, v15, :cond_1

    const-string v14, "-"

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setGravity(I)V

    add-int/lit8 v14, v4, -0x1

    invoke-virtual {v5, v14}, Lsf;->setSubIndex(I)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v12

    invoke-virtual {v5, v12}, Lsf;->setQuestionNo(I)V

    aput-object v5, v9, v14

    :cond_1
    invoke-virtual {v7, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v13, v13, 0x1

    const/16 v5, 0xa

    const/16 v12, 0x11

    const/4 v14, 0x5

    goto :goto_0

    :cond_2
    iget-object v4, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v4

    iget v5, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    sub-int/2addr v5, v15

    const/4 v7, 0x5

    div-int/2addr v5, v7

    add-int/2addr v5, v15

    const/4 v7, 0x0

    :goto_1
    const/16 v12, 0xa

    if-ge v7, v12, :cond_8

    const/4 v13, 0x0

    :goto_2
    if-ge v13, v5, :cond_7

    new-instance v14, Landroid/widget/LinearLayout;

    invoke-direct {v14, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v15, Lsf;

    invoke-direct {v15, v1}, Lsf;-><init>(Landroid/content/Context;)V

    invoke-virtual {v15, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez v13, :cond_4

    if-nez v7, :cond_4

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, ""

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v15, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v14, v15, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/high16 v11, -0x1000000

    invoke-virtual {v15, v11}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 v12, 0x11

    invoke-virtual {v15, v12}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v12, 0x0

    :goto_3
    const/4 v15, 0x5

    if-ge v12, v15, :cond_6

    new-instance v11, Lsf;

    invoke-direct {v11, v1}, Lsf;-><init>(Landroid/content/Context;)V

    mul-int/lit8 v16, v13, 0x5

    add-int v15, v16, v12

    iget v1, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-ge v15, v1, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v1

    invoke-virtual {v11, v1}, Lsf;->setQuestionNo(I)V

    iget v1, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr v1, v7

    add-int/2addr v1, v15

    invoke-virtual {v11, v1}, Lsf;->setSubIndex(I)V

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v1, v4, v7

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr v1, v7

    add-int/2addr v1, v15

    aput-object v11, v9, v1

    :cond_5
    invoke-virtual {v14, v11, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v1, 0x11

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setGravity(I)V

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v1, p1

    const/high16 v11, -0x1000000

    goto :goto_3

    :cond_6
    const/16 v1, 0x11

    iget-object v11, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v11, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v1, p1

    const/high16 v11, -0x1000000

    const/16 v12, 0xa

    goto/16 :goto_2

    :cond_7
    const/16 v1, 0x11

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v1, p1

    const/high16 v11, -0x1000000

    goto/16 :goto_1

    :cond_8
    iput-object v9, v0, Lk5;->f:[Lsf;

    return-void
.end method

.method public final h(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/16 v2, 0x8

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    iget v3, v0, Lk5;->k:I

    invoke-static {v3, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v4

    if-eqz v4, :cond_0

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget v4, v4, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_0

    :cond_0
    const/4 v5, 0x4

    const/4 v4, 0x5

    :goto_0
    mul-int v6, v5, v4

    new-array v6, v6, [Lsf;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixPrimary()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixSecondary()[Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    move v10, v9

    :goto_1
    if-ge v10, v5, :cond_2

    new-instance v11, Landroid/widget/LinearLayout;

    invoke-direct {v11, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v12, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v12, v0, Lk5;->l:I

    invoke-static {v12, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v12

    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v13, v12, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v12, Lsf;

    invoke-direct {v12, v1}, Lsf;-><init>(Landroid/content/Context;)V

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    aget-object v15, v7, v10

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v14, -0x1000000

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v11, v12, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v13, 0x11

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v12, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v12, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12, v9}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v14, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v14, v3, v3}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v14, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    move v15, v9

    :goto_2
    if-ge v15, v4, :cond_1

    mul-int v16, v10, v4

    add-int v9, v16, v15

    new-instance v13, Lsf;

    invoke-direct {v13, v1}, Lsf;-><init>(Landroid/content/Context;)V

    aput-object v13, v6, v9

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v1

    invoke-virtual {v13, v1}, Lsf;->setQuestionNo(I)V

    aget-object v1, v6, v9

    invoke-virtual {v1, v9}, Lsf;->setSubIndex(I)V

    aget-object v1, v6, v9

    move-object/from16 v13, p3

    invoke-virtual {v1, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v1, v6, v9

    move/from16 v17, v2

    aget-object v2, v8, v15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v1, v6, v9

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    aget-object v1, v6, v9

    invoke-virtual {v12, v1, v14}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v1, p1

    move v13, v2

    move/from16 v2, v17

    const/4 v9, 0x0

    goto :goto_2

    :cond_1
    move-object/from16 v13, p3

    move/from16 v17, v2

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v9, 0x0

    invoke-direct {v1, v9, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v11, v12, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v1, p1

    move/from16 v2, v17

    goto/16 :goto_1

    :cond_2
    iput-object v6, v0, Lk5;->f:[Lsf;

    return-void
.end method

.method public final i(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    iget-boolean v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-nez v4, :cond_0

    iget v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-le v2, v3, :cond_1

    :cond_0
    invoke-virtual/range {p0 .. p4}, Lk5;->g(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void

    :cond_1
    const/16 v2, 0x8

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    iget v4, v0, Lk5;->k:I

    invoke-static {v4, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v6, v0, Lk5;->l:I

    invoke-static {v6, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v6

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/16 v2, 0xa

    new-array v2, v2, [Lsf;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move v8, v6

    :goto_0
    const/4 v9, 0x2

    if-ge v8, v9, :cond_5

    new-instance v9, Landroid/widget/LinearLayout;

    invoke-direct {v9, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v10, Lsf;

    invoke-direct {v10, v1}, Lsf;-><init>(Landroid/content/Context;)V

    if-eqz v8, :cond_3

    if-eq v8, v3, :cond_2

    goto :goto_2

    :cond_2
    const-string v11, " "

    goto :goto_1

    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_1
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v9, v10, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/high16 v11, -0x1000000

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 v11, 0x11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setGravity(I)V

    move v10, v6

    :goto_3
    const/4 v12, 0x5

    if-ge v10, v12, :cond_4

    mul-int/lit8 v12, v8, 0x5

    add-int/2addr v12, v10

    new-instance v13, Lsf;

    invoke-direct {v13, v1}, Lsf;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v14

    invoke-virtual {v13, v14}, Lsf;->setQuestionNo(I)V

    invoke-virtual {v13, v12}, Lsf;->setSubIndex(I)V

    move-object/from16 v14, p3

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    aget-object v15, v4, v12

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aput-object v13, v2, v12

    invoke-virtual {v9, v13, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v13, v11}, Landroid/widget/TextView;->setGravity(I)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_4
    move-object/from16 v14, p3

    iget-object v10, v0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_5
    iput-object v2, v0, Lk5;->f:[Lsf;

    return-void
.end method

.method public final j(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Landroid/view/View$OnClickListener;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 9

    .line 1
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-static {v1, p1}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    iget v2, p0, Lk5;->k:I

    invoke-static {v2, p1}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    const/4 v3, 0x2

    new-array v4, v3, [Lsf;

    new-instance v5, Lsf;

    invoke-direct {v5, p1}, Lsf;-><init>(Landroid/content/Context;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v7, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v7, p0, Lk5;->l:I

    invoke-static {v7, p1}, La91;->d(ILandroid/content/Context;)I

    move-result v7

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v7, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/high16 v1, 0x40000000    # 2.0f

    iput v1, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v0, v5, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v1, 0x11

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    new-instance v5, Lsf;

    invoke-direct {v5, p1}, Lsf;-><init>(Landroid/content/Context;)V

    aput-object v5, v4, v2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v7

    invoke-virtual {v5, v7}, Lsf;->setQuestionNo(I)V

    aget-object v5, v4, v2

    invoke-virtual {v5, v2}, Lsf;->setSubIndex(I)V

    aget-object v5, v4, v2

    invoke-virtual {v5, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object v5

    aget-object v7, v4, v2

    aget-object v5, v5, v2

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v5, v4, v2

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setGravity(I)V

    aget-object v5, v4, v2

    invoke-virtual {v0, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance p2, Lsf;

    invoke-direct {p2, p1}, Lsf;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lk5;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v4, p0, Lk5;->f:[Lsf;

    return-void
.end method

.method public k()[Z
    .locals 1

    .line 1
    iget-object v0, p0, Lk5;->i:[Z

    invoke-static {v0}, Le5;->a([Z)[Z

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 10

    .line 1
    iget-object v0, p0, Lk5;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return-object v2

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const v3, 0x7f1200c3

    const/4 v4, 0x1

    :try_start_0
    iget-object v5, p0, Lk5;->g:[Landroid/widget/EditText;

    array-length v6, v5

    if-ge v1, v6, :cond_2

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v5, v4, :cond_1

    iget-object v5, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v1, v5, v1

    iget-object v5, p0, Lk5;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-object v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    aget-object v1, v5, v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    iget-object v1, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    cmpg-double v1, v7, v5

    if-gez v1, :cond_3

    iget-object v1, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v1, v1, v0

    iget-object v5, p0, Lk5;->a:Landroid/content/Context;

    const v6, 0x7f12019c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-object v2

    :cond_3
    iget-object v1, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v1, v1, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    new-instance v9, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-direct {v9, v5, v6, v7, v8}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;-><init>(DD)V

    invoke-virtual {v1, v9}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    iget-object v1, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v0, v1, v0

    iget-object v1, p0, Lk5;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lk5;->g:[Landroid/widget/EditText;

    aget-object v0, v0, v4

    iget-object v1, p0, Lk5;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    return-object v2
.end method

.method public m()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lk5;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final n(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Landroid/widget/LinearLayout;
    .locals 6

    .line 1
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lk5;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const v1, 0x800005

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lk5;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lk5;->a:Landroid/content/Context;

    const v3, 0x7f120170

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lk5;->a:Landroid/content/Context;

    const v3, 0x7f120196

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v2, Lk5$c;

    invoke-direct {v2, p0, v1, p2}, Lk5$c;-><init>(Lk5;Landroid/widget/TextView;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)V

    new-instance p2, Lk5$d;

    invoke-direct {p2, p0, p1, p3, v2}, Lk5$d;-><init>(Lk5;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ly01;)V

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    invoke-direct {p1, p2, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object p2, p0, Lk5;->a:Landroid/content/Context;

    const/4 p3, 0x6

    invoke-static {p3, p2}, La91;->d(ILandroid/content/Context;)I

    move-result p2

    const/4 v2, 0x0

    iget-object v3, p0, Lk5;->a:Landroid/content/Context;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    iget-object v3, p0, Lk5;->a:Landroid/content/Context;

    invoke-static {p3, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    const/4 v4, 0x4

    iget-object v5, p0, Lk5;->a:Landroid/content/Context;

    invoke-static {v4, v5}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    invoke-virtual {p1, p2, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object p2, p0, Lk5;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v2, 0x7f06004c

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p2

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p2, p0, Lk5;->a:Landroid/content/Context;

    const v2, 0x7f08008a

    invoke-static {p2, v2}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p2, p0, Lk5;->a:Landroid/content/Context;

    const/16 v2, 0xa

    invoke-static {v2, p2}, La91;->d(ILandroid/content/Context;)I

    move-result p2

    iget-object v3, p0, Lk5;->a:Landroid/content/Context;

    invoke-static {p3, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    iget-object v4, p0, Lk5;->a:Landroid/content/Context;

    invoke-static {v2, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    iget-object v4, p0, Lk5;->a:Landroid/content/Context;

    invoke-static {p3, v4}, La91;->d(ILandroid/content/Context;)I

    move-result p3

    invoke-virtual {v1, p2, v3, v2, p3}, Landroid/widget/TextView;->setPadding(IIII)V

    const/high16 p2, 0x41600000    # 14.0f

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public final o()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lk5;->l()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v6, p0, Lk5;->e:Lk5$f;

    if-eqz v6, :cond_0

    new-instance v7, Lc21;

    iget-object v0, p0, Lk5;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v5}, Lc21;-><init>(IZ[ZLjava/lang/String;Ljava/util/ArrayList;)V

    invoke-interface {v6, v7}, Lk5$f;->a(Lc21;)V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 4

    .line 1
    iget-object v0, p0, Lk5;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lk5;->g:[Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lk5;->r([Landroid/widget/EditText;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lk5;->i:[Z

    array-length v3, v2

    if-ge v1, v3, :cond_1

    aput-boolean v0, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lk5;->f:[Lsf;

    invoke-virtual {p0, v0, v2}, Lk5;->q([Lsf;[Z)V

    return-void
.end method

.method public final q([Lsf;[Z)V
    .locals 4

    .line 1
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-boolean v1, p2, v0

    if-eqz v1, :cond_0

    aget-object v1, p1, v0

    const v2, 0x7f08016a

    invoke-virtual {v1, v2}, Lm7;->setBackgroundResource(I)V

    aget-object v1, p1, v0

    iget-object v2, p0, Lk5;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06005c

    goto :goto_1

    :cond_0
    aget-object v1, p1, v0

    const v2, 0x7f080097

    invoke-virtual {v1, v2}, Lm7;->setBackgroundResource(I)V

    aget-object v1, p1, v0

    iget-object v2, p0, Lk5;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060036

    :goto_1
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final r([Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 6

    .line 1
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, ""

    if-nez p2, :cond_0

    aget-object p2, p1, v1

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object p1, p1, v0

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    new-instance v3, Lgc0;

    invoke-direct {v3}, Lgc0;-><init>()V

    const-class v4, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-virtual {v3, p2, v4}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    aget-object v1, p1, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getLower()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object p1, p1, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getUpper()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

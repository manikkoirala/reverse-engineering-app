.class public Lq6;
.super Ljj;
.source "SourceFile"

# interfaces
.implements Lz5;


# instance fields
.field private mDelegate:Lf6;

.field private final mKeyDispatcher:Lmi0$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .line 1
    invoke-static {p1, p2}, Lq6;->getThemeResId(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Ljj;-><init>(Landroid/content/Context;I)V

    new-instance v0, Lp6;

    invoke-direct {v0, p0}, Lp6;-><init>(Lq6;)V

    iput-object v0, p0, Lq6;->mKeyDispatcher:Lmi0$a;

    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-static {p1, p2}, Lq6;->getThemeResId(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lf6;->K(I)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lf6;->v(Landroid/os/Bundle;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Ljj;-><init>(Landroid/content/Context;)V

    new-instance p1, Lp6;

    invoke-direct {p1, p0}, Lp6;-><init>(Lq6;)V

    iput-object p1, p0, Lq6;->mKeyDispatcher:Lmi0$a;

    invoke-virtual {p0, p2}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {p0, p3}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method public static getThemeResId(Landroid/content/Context;I)I
    .locals 2

    .line 1
    if-nez p1, :cond_0

    new-instance p1, Landroid/util/TypedValue;

    invoke-direct {p1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    sget v0, Lsa1;->x:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget p1, p1, Landroid/util/TypedValue;->resourceId:I

    :cond_0
    return p1
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lf6;->c(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public dismiss()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0}, Lf6;->w()V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lq6;->mKeyDispatcher:Lmi0$a;

    invoke-static {v1, v0, p0, p1}, Lmi0;->e(Lmi0$a;Landroid/view/View;Landroid/view/Window$Callback;Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public findViewById(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf6;->h(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getDelegate()Lf6;
    .locals 1

    .line 1
    iget-object v0, p0, Lq6;->mDelegate:Lf6;

    if-nez v0, :cond_0

    invoke-static {p0, p0}, Lf6;->g(Landroid/app/Dialog;Lz5;)Lf6;

    move-result-object v0

    iput-object v0, p0, Lq6;->mDelegate:Lf6;

    :cond_0
    iget-object v0, p0, Lq6;->mDelegate:Lf6;

    return-object v0
.end method

.method public getSupportActionBar()Lt1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0}, Lf6;->p()Lt1;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0}, Lf6;->r()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0}, Lf6;->q()V

    invoke-super {p0, p1}, Ljj;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf6;->v(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    .line 1
    invoke-super {p0}, Ljj;->onStop()V

    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0}, Lf6;->B()V

    return-void
.end method

.method public onSupportActionModeFinished(Lc2;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onSupportActionModeStarted(Lc2;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onWindowStartingSupportActionMode(Lc2$a;)Lc2;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public setContentView(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf6;->F(I)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .line 2
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf6;->G(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .line 3
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lf6;->H(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setTitle(I)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf6;->L(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 2
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf6;->L(Ljava/lang/CharSequence;)V

    return-void
.end method

.method superDispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public supportRequestWindowFeature(I)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lq6;->getDelegate()Lf6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf6;->E(I)Z

    move-result p1

    return p1
.end method

.class public Lcs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/ScheduledExecutorService;


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcs;->b:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method

.method public static synthetic a(Lcs;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcs;->p(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcs;->n(Ljava/lang/Runnable;Lds$b;)V

    return-void
.end method

.method public static synthetic c(Lcs;Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcs;->o(Ljava/lang/Runnable;Lds$b;)V

    return-void
.end method

.method public static synthetic d(Lcs;Ljava/util/concurrent/Callable;Lds$b;)Ljava/util/concurrent/Future;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcs;->r(Ljava/util/concurrent/Callable;Lds$b;)Ljava/util/concurrent/Future;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Lcs;Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p7}, Lcs;->v(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic f(Lcs;Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcs;->u(Ljava/lang/Runnable;Lds$b;)V

    return-void
.end method

.method public static synthetic h(Lcs;Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcs;->s(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic i(Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcs;->t(Ljava/lang/Runnable;Lds$b;)V

    return-void
.end method

.method public static synthetic j(Lcs;Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p7}, Lcs;->x(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic k(Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcs;->y(Ljava/lang/Runnable;Lds$b;)V

    return-void
.end method

.method public static synthetic l(Lcs;Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcs;->w(Ljava/lang/Runnable;Lds$b;)V

    return-void
.end method

.method public static synthetic m(Ljava/util/concurrent/Callable;Lds$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcs;->q(Ljava/util/concurrent/Callable;Lds$b;)V

    return-void
.end method

.method public static synthetic n(Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    const/4 p0, 0x0

    invoke-interface {p1, p0}, Lds$b;->set(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-interface {p1, p0}, Lds$b;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private synthetic o(Ljava/lang/Runnable;Lds$b;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lrr;

    invoke-direct {v1, p1, p2}, Lrr;-><init>(Ljava/lang/Runnable;Lds$b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private synthetic p(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .line 1
    iget-object v0, p0, Lcs;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lyr;

    invoke-direct {v1, p0, p1, p5}, Lyr;-><init>(Lcs;Ljava/lang/Runnable;Lds$b;)V

    invoke-interface {v0, v1, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic q(Ljava/util/concurrent/Callable;Lds$b;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p0

    invoke-interface {p1, p0}, Lds$b;->set(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-interface {p1, p0}, Lds$b;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private synthetic r(Ljava/util/concurrent/Callable;Lds$b;)Ljava/util/concurrent/Future;
    .locals 2

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Las;

    invoke-direct {v1, p1, p2}, Las;-><init>(Ljava/util/concurrent/Callable;Lds$b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method private synthetic s(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .line 1
    iget-object v0, p0, Lcs;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lzr;

    invoke-direct {v1, p0, p1, p5}, Lzr;-><init>(Lcs;Ljava/util/concurrent/Callable;Lds$b;)V

    invoke-interface {v0, v1, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic t(Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    invoke-interface {p1, p0}, Lds$b;->a(Ljava/lang/Throwable;)V

    throw p0
.end method

.method private synthetic u(Ljava/lang/Runnable;Lds$b;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lbs;

    invoke-direct {v1, p1, p2}, Lbs;-><init>(Ljava/lang/Runnable;Lds$b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private synthetic v(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 7

    .line 1
    iget-object v0, p0, Lcs;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lwr;

    invoke-direct {v1, p0, p1, p7}, Lwr;-><init>(Lcs;Ljava/lang/Runnable;Lds$b;)V

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    return-object p1
.end method

.method private synthetic w(Ljava/lang/Runnable;Lds$b;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lsr;

    invoke-direct {v1, p1, p2}, Lsr;-><init>(Ljava/lang/Runnable;Lds$b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private synthetic x(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;Lds$b;)Ljava/util/concurrent/ScheduledFuture;
    .locals 7

    .line 1
    iget-object v0, p0, Lcs;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lxr;

    invoke-direct {v1, p0, p1, p7}, Lxr;-><init>(Lcs;Ljava/lang/Runnable;Lds$b;)V

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic y(Ljava/lang/Runnable;Lds$b;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-interface {p1, p0}, Lds$b;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result p1

    return p1
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public invokeAll(Ljava/util/Collection;)Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    .locals 1

    .line 2
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public invokeAny(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->invokeAny(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .line 2
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ExecutorService;->invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public isShutdown()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public isTerminated()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    return v0
.end method

.method public schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 8

    .line 1
    new-instance v0, Lds;

    new-instance v7, Ltr;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Ltr;-><init>(Lcs;Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V

    invoke-direct {v0, v7}, Lds;-><init>(Lds$c;)V

    return-object v0
.end method

.method public schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 8

    .line 2
    new-instance v0, Lds;

    new-instance v7, Lqr;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lqr;-><init>(Lcs;Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)V

    invoke-direct {v0, v7}, Lds;-><init>(Lds$c;)V

    return-object v0
.end method

.method public scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 10

    .line 1
    new-instance v0, Lds;

    new-instance v9, Lvr;

    move-object v1, v9

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lvr;-><init>(Lcs;Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)V

    invoke-direct {v0, v9}, Lds;-><init>(Lds$c;)V

    return-object v0
.end method

.method public scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 10

    .line 1
    new-instance v0, Lds;

    new-instance v9, Lur;

    move-object v1, v9

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lur;-><init>(Lcs;Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)V

    invoke-direct {v0, v9}, Lds;-><init>(Lds$c;)V

    return-object v0
.end method

.method public shutdown()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shutting down is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shutting down is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1

    .line 1
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1

    .line 2
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1

    .line 3
    iget-object v0, p0, Lcs;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

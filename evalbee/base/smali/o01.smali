.class public final Lo01;
.super Lhz1;
.source "SourceFile"


# static fields
.field public static final b:Liz1;


# instance fields
.field public final a:Lvw1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/google/gson/ToNumberPolicy;->LAZILY_PARSED_NUMBER:Lcom/google/gson/ToNumberPolicy;

    invoke-static {v0}, Lo01;->f(Lvw1;)Liz1;

    move-result-object v0

    sput-object v0, Lo01;->b:Liz1;

    return-void
.end method

.method public constructor <init>(Lvw1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lhz1;-><init>()V

    iput-object p1, p0, Lo01;->a:Lvw1;

    return-void
.end method

.method public static e(Lvw1;)Liz1;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/gson/ToNumberPolicy;->LAZILY_PARSED_NUMBER:Lcom/google/gson/ToNumberPolicy;

    if-ne p0, v0, :cond_0

    sget-object p0, Lo01;->b:Liz1;

    return-object p0

    :cond_0
    invoke-static {p0}, Lo01;->f(Lvw1;)Liz1;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lvw1;)Liz1;
    .locals 1

    .line 1
    new-instance v0, Lo01;

    invoke-direct {v0, p0}, Lo01;-><init>(Lvw1;)V

    new-instance p0, Lo01$a;

    invoke-direct {p0, v0}, Lo01$a;-><init>(Lo01;)V

    return-object p0
.end method


# virtual methods
.method public bridge synthetic b(Lrh0;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lo01;->g(Lrh0;)Ljava/lang/Number;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lvh0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p0, p1, p2}, Lo01;->h(Lvh0;Ljava/lang/Number;)V

    return-void
.end method

.method public g(Lrh0;)Ljava/lang/Number;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lo01$b;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/gson/JsonSyntaxException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expecting number, got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "; at path "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lrh0;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lo01;->a:Lvw1;

    invoke-interface {v0, p1}, Lvw1;->readNumber(Lrh0;)Ljava/lang/Number;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lrh0;->R()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public h(Lvh0;Ljava/lang/Number;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Lvh0;->q0(Ljava/lang/Number;)Lvh0;

    return-void
.end method

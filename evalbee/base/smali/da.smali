.class public final Lda;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lda$a;,
        Lda$b;,
        Lda$c;
    }
.end annotation


# static fields
.field public static final a:Ljk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lda;

    invoke-direct {v0}, Lda;-><init>()V

    sput-object v0, Lda;->a:Ljk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public configure(Lzw;)V
    .locals 2

    .line 1
    const-class v0, Li91;

    sget-object v1, Lda$c;->a:Lda$c;

    invoke-interface {p1, v0, v1}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v0, Law0;

    sget-object v1, Lda$b;->a:Lda$b;

    invoke-interface {p1, v0, v1}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v0, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;

    sget-object v1, Lda$a;->a:Lda$a;

    invoke-interface {p1, v0, v1}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    return-void
.end method

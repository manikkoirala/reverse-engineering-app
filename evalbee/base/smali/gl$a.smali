.class public final Lgl$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lgl$c;


# direct methods
.method public constructor <init>(Landroid/content/ClipData;I)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1f

    if-lt v0, v1, :cond_0

    new-instance v0, Lgl$b;

    invoke-direct {v0, p1, p2}, Lgl$b;-><init>(Landroid/content/ClipData;I)V

    goto :goto_0

    :cond_0
    new-instance v0, Lgl$d;

    invoke-direct {v0, p1, p2}, Lgl$d;-><init>(Landroid/content/ClipData;I)V

    :goto_0
    iput-object v0, p0, Lgl$a;->a:Lgl$c;

    return-void
.end method


# virtual methods
.method public a()Lgl;
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$a;->a:Lgl$c;

    invoke-interface {v0}, Lgl$c;->build()Lgl;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)Lgl$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$a;->a:Lgl$c;

    invoke-interface {v0, p1}, Lgl$c;->setExtras(Landroid/os/Bundle;)V

    return-object p0
.end method

.method public c(I)Lgl$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$a;->a:Lgl$c;

    invoke-interface {v0, p1}, Lgl$c;->b(I)V

    return-object p0
.end method

.method public d(Landroid/net/Uri;)Lgl$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$a;->a:Lgl$c;

    invoke-interface {v0, p1}, Lgl$c;->a(Landroid/net/Uri;)V

    return-object p0
.end method

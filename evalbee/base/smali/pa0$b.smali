.class public Lpa0$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/d$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lpa0;->d(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lpa0;


# direct methods
.method public constructor <init>(Lpa0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lpa0$b;->a:Lpa0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lpa0$b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    const-class v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    invoke-virtual {v1, p1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    if-eqz p1, :cond_1

    iget-object v1, p0, Lpa0$b;->a:Lpa0;

    invoke-static {v1}, Lpa0;->c(Lpa0;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lpa0$b;->a:Lpa0;

    invoke-static {v1}, Lpa0;->c(Lpa0;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->reset(Landroid/content/Context;)V

    :cond_0
    iget-object v1, p0, Lpa0$b;->a:Lpa0;

    invoke-static {v1}, Lpa0;->c(Lpa0;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->save(Landroid/content/Context;)V

    iget-object v1, p0, Lpa0$b;->a:Lpa0;

    const/4 v2, 0x1

    const/16 v3, 0xc8

    invoke-static {v1, v2, v3, p1}, Lpa0;->b(Lpa0;ZILjava/lang/Object;)V

    new-instance v1, Lbb0;

    iget-object v2, p0, Lpa0$b;->a:Lpa0;

    invoke-static {v2}, Lpa0;->c(Lpa0;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p1, v2, v0}, Lbb0;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;Landroid/content/Context;Lzg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    iget-object p1, p0, Lpa0$b;->a:Lpa0;

    const/4 v1, 0x0

    const/16 v2, 0x190

    invoke-static {p1, v1, v2, v0}, Lpa0;->b(Lpa0;ZILjava/lang/Object;)V

    return-void
.end method

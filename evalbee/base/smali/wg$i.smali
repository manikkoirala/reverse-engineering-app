.class public Lwg$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lwg;->s()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lwg;


# direct methods
.method public constructor <init>(Lwg;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lwg$i;->a:Lwg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 1
    const p1, 0x7f08008c

    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance p1, Landroid/content/Intent;

    iget-object p2, p0, Lwg$i;->a:Lwg;

    iget-object p2, p2, Lwg;->a:Landroid/content/Context;

    const-class p4, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {p1, p2, p4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object p2, p0, Lwg$i;->a:Lwg;

    iget-object p2, p2, Lwg;->h:Ljava/util/ArrayList;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->getClassName()Ljava/lang/String;

    move-result-object p2

    const-string p3, "CLASS_NAME"

    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object p2, p0, Lwg$i;->a:Lwg;

    invoke-virtual {p2, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.class public Lwg;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/view/View;

.field public c:Landroid/widget/Button;

.field public d:Landroid/widget/Button;

.field public e:Landroid/widget/ListView;

.field public f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public g:Lg3;

.field public h:Ljava/util/ArrayList;

.field public i:Z

.field public j:Landroid/view/ActionMode;

.field public k:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lwg;->i:Z

    return-void
.end method

.method public static synthetic g(Lwg;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lwg;->v()V

    return-void
.end method

.method public static synthetic h(Lwg;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lwg;->u()V

    return-void
.end method

.method public static synthetic i(Lwg;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lwg;->q()V

    return-void
.end method

.method public static synthetic j(Lwg;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lwg;->p()V

    return-void
.end method

.method public static synthetic k(Lwg;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lwg;->y(Z)V

    return-void
.end method

.method public static synthetic l(Lwg;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lwg;->n(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic m(Lwg;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lwg;->o(Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public final n(Ljava/util/ArrayList;)V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Lcom/ekodroid/omrevaluator/students/clients/models/ClassDto;

    invoke-direct {v3, v2}, Lcom/ekodroid/omrevaluator/students/clients/models/ClassDto;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lwg;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120191

    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    new-instance v2, Lfs;

    iget-object v3, p0, Lwg;->a:Landroid/content/Context;

    new-instance v4, Lwg$a;

    invoke-direct {v4, p0, v1, p1}, Lwg$a;-><init>(Lwg;Landroid/app/ProgressDialog;Ljava/util/ArrayList;)V

    invoke-direct {v2, v3, v0, v4}, Lfs;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lzg;)V

    return-void
.end method

.method public final o(Ljava/util/ArrayList;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteClass(Ljava/lang/String;)Z

    iget-object v1, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteAllStudentForClass(Ljava/lang/String;)Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lwg;->a:Landroid/content/Context;

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    const v2, 0x7f12005a

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    invoke-virtual {p0}, Lwg;->v()V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, Lwg;->a:Landroid/content/Context;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 1
    const p3, 0x7f0c008d

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lwg;->b:Landroid/view/View;

    invoke-virtual {p0}, Lwg;->t()V

    invoke-virtual {p0}, Lwg;->s()V

    invoke-virtual {p0}, Lwg;->r()V

    new-instance p1, Lwg$b;

    invoke-direct {p1, p0}, Lwg$b;-><init>(Lwg;)V

    iput-object p1, p0, Lwg;->k:Landroid/content/BroadcastReceiver;

    iget-object p1, p0, Lwg;->b:Landroid/view/View;

    return-object p1
.end method

.method public onPause()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    iget-object v0, p0, Lwg;->j:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lwg;->j:Landroid/view/ActionMode;

    :cond_0
    iget-object v0, p0, Lwg;->a:Landroid/content/Context;

    iget-object v1, p0, Lwg;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    iget-object v0, p0, Lwg;->a:Landroid/content/Context;

    iget-object v1, p0, Lwg;->k:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UPDATE_CLASS_LIST"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    iget-object v0, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lwg;->i:Z

    invoke-virtual {p0}, Lwg;->v()V

    return-void
.end method

.method public final p()V
    .locals 4

    .line 1
    new-instance v0, Lgo;

    iget-object v1, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getAllStudents()Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lwg$f;

    invoke-direct {v3, p0}, Lwg$f;-><init>(Lwg;)V

    invoke-direct {v0, v1, v2, v3}, Lgo;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ly01;)V

    return-void
.end method

.method public final q()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lwg;->a:Landroid/content/Context;

    const-class v2, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final r()V
    .locals 2

    .line 1
    iget-object v0, p0, Lwg;->e:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    iget-object v0, p0, Lwg;->e:Landroid/widget/ListView;

    new-instance v1, Lwg$j;

    invoke-direct {v1, p0}, Lwg$j;-><init>(Lwg;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    return-void
.end method

.method public final s()V
    .locals 3

    .line 1
    iget-object v0, p0, Lwg;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object v0, p0, Lwg;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lwg$h;

    invoke-direct {v1, p0}, Lwg$h;-><init>(Lwg;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    iget-object v0, p0, Lwg;->e:Landroid/widget/ListView;

    new-instance v1, Lwg$i;

    invoke-direct {v1, p0}, Lwg$i;-><init>(Lwg;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public final t()V
    .locals 2

    .line 1
    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    const v1, 0x7f090370

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lwg;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    const v1, 0x7f090246

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lwg;->e:Landroid/widget/ListView;

    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    const v1, 0x7f09009a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lwg;->c:Landroid/widget/Button;

    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    const v1, 0x7f09009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lwg;->d:Landroid/widget/Button;

    iget-object v0, p0, Lwg;->c:Landroid/widget/Button;

    new-instance v1, Lwg$c;

    invoke-direct {v1, p0}, Lwg$c;-><init>(Lwg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lwg;->d:Landroid/widget/Button;

    new-instance v1, Lwg$d;

    invoke-direct {v1, p0}, Lwg$d;-><init>(Lwg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    const v1, 0x7f09019b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lwg$e;

    invoke-direct {v1, p0}, Lwg$e;-><init>(Lwg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final u()V
    .locals 3

    .line 1
    new-instance v0, Lt3;

    iget-object v1, p0, Lwg;->a:Landroid/content/Context;

    new-instance v2, Lwg$g;

    invoke-direct {v2, p0}, Lwg$g;-><init>(Lwg;)V

    invoke-direct {v0, v1, v2}, Lt3;-><init>(Landroid/content/Context;Ly01;)V

    return-void
.end method

.method public final v()V
    .locals 4

    .line 1
    iget-object v0, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getAllClasses()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lwg;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lwg;->w()V

    :cond_0
    iget-object v0, p0, Lwg;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    const v3, 0x7f0901ea

    if-lez v0, :cond_1

    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lwg;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lg3;

    iget-object v1, p0, Lwg;->a:Landroid/content/Context;

    iget-object v2, p0, Lwg;->h:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lg3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lwg;->g:Lg3;

    iget-object v1, p0, Lwg;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lwg;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lwg;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final w()V
    .locals 7

    .line 1
    const-string v0, "className"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v2, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v2

    invoke-interface {v2}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/j256/ormlite/stmt/QueryBuilder;->distinct()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v4}, Lcom/j256/ormlite/stmt/QueryBuilder;->selectColumns([Ljava/lang/String;)Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getStudentDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v4

    invoke-interface {v4}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/j256/ormlite/stmt/QueryBuilder;->distinct()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v4, v3}, Lcom/j256/ormlite/stmt/QueryBuilder;->selectColumns([Ljava/lang/String;)Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object v0

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0, v1}, Lwg;->x(Ljava/util/ArrayList;)V

    :cond_2
    return-void
.end method

.method public final x(Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/database/ClassDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateClass(Lcom/ekodroid/omrevaluator/database/ClassDataModel;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final y(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lwg;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lwg;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->c(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

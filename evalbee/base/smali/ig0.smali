.class public Lig0;
.super Lu80;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lig0$f;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:[Llg;

.field public c:Ljava/lang/String;

.field public d:Lig0$f;

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public f:[[Z

.field public g:I

.field public h:I

.field public i:Landroid/widget/Spinner;

.field public j:Landroid/widget/Spinner;

.field public k:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lig0$f;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lig0;->h:I

    new-instance v0, Lig0$a;

    invoke-direct {v0, p0}, Lig0$a;-><init>(Lig0;)V

    iput-object v0, p0, Lig0;->k:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iput-object p1, p0, Lig0;->a:Landroid/content/Context;

    iput-object p2, p0, Lig0;->d:Lig0$f;

    iput-object p3, p0, Lig0;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, p3}, Lig0;->f(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[Z

    move-result-object p1

    iput-object p1, p0, Lig0;->f:[[Z

    return-void
.end method

.method public static synthetic a(Lig0;)[[Z
    .locals 0

    .line 1
    iget-object p0, p0, Lig0;->f:[[Z

    return-object p0
.end method

.method public static synthetic b(Lig0;)Landroid/widget/Spinner;
    .locals 0

    .line 1
    iget-object p0, p0, Lig0;->j:Landroid/widget/Spinner;

    return-object p0
.end method

.method public static synthetic c(Lig0;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lig0;->g()V

    return-void
.end method

.method public static synthetic d(Lig0;)Landroid/widget/Spinner;
    .locals 0

    .line 1
    iget-object p0, p0, Lig0;->i:Landroid/widget/Spinner;

    return-object p0
.end method

.method public static synthetic e(Lig0;)Lig0$f;
    .locals 0

    .line 1
    iget-object p0, p0, Lig0;->d:Lig0$f;

    return-object p0
.end method


# virtual methods
.method public final f(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[Z
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lig0;->g:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v0

    iget v2, p0, Lig0;->g:I

    filled-new-array {v0, v2}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQuestionSets()[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    aget-object v3, p1, v2

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQueMarks()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lig0;->c:Ljava/lang/String;

    move v3, v2

    :goto_0
    array-length v4, p1

    if-ge v3, v4, :cond_1

    aget-object v4, p1, v3

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQuestions()[I

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    move v6, v2

    :goto_1
    if-ge v6, v5, :cond_0

    aget v7, v4, v6

    aget-object v8, v0, v3

    sub-int/2addr v7, v1

    aput-boolean v1, v8, v7

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final g()V
    .locals 11

    .line 1
    const v0, 0x7f090220

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget v1, p0, Lig0;->g:I

    new-array v1, v1, [Llg;

    iput-object v1, p0, Lig0;->b:[Llg;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x10

    iget-object v3, p0, Lig0;->a:Landroid/content/Context;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lig0;->a:Landroid/content/Context;

    const/16 v5, 0x28

    invoke-static {v5, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    iget-object v6, p0, Lig0;->a:Landroid/content/Context;

    invoke-static {v5, v6}, La91;->d(ILandroid/content/Context;)I

    move-result v5

    invoke-direct {v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    move v4, v3

    :goto_0
    iget-object v5, p0, Lig0;->b:[Llg;

    array-length v5, v5

    if-ge v4, v5, :cond_0

    iget-object v5, p0, Lig0;->a:Landroid/content/Context;

    invoke-static {v5}, Lyo;->b(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v5

    iget-object v6, p0, Lig0;->a:Landroid/content/Context;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, ""

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v9, 0x32

    invoke-static {v6, v7, v9}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v6

    iget-object v7, p0, Lig0;->b:[Llg;

    new-instance v9, Llg;

    iget-object v10, p0, Lig0;->a:Landroid/content/Context;

    invoke-direct {v9, v10}, Llg;-><init>(Landroid/content/Context;)V

    aput-object v9, v7, v4

    iget-object v7, p0, Lig0;->b:[Llg;

    aget-object v7, v7, v4

    invoke-virtual {v7, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v7, p0, Lig0;->b:[Llg;

    aget-object v7, v7, v4

    invoke-virtual {v7, v8}, Llg;->setQuestionNo(I)V

    iget-object v7, p0, Lig0;->b:[Llg;

    aget-object v7, v7, v4

    iget-object v9, p0, Lig0;->f:[[Z

    iget v10, p0, Lig0;->h:I

    aget-object v9, v9, v10

    aget-boolean v9, v9, v4

    invoke-virtual {v7, v9}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v7, p0, Lig0;->b:[Llg;

    aget-object v7, v7, v4

    iget-object v9, p0, Lig0;->k:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v9}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v5, v6, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, p0, Lig0;->b:[Llg;

    aget-object v4, v6, v4

    invoke-virtual {v5, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v4, v8

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    .line 1
    const v0, 0x7f090445

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lig0$e;

    invoke-direct {v1, p0}, Lig0$e;-><init>(Lig0;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c006e

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lig0;->h()V

    const p1, 0x7f09033d

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Spinner;

    iput-object p1, p0, Lig0;->i:Landroid/widget/Spinner;

    const p1, 0x7f090337

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Spinner;

    iput-object p1, p0, Lig0;->j:Landroid/widget/Spinner;

    const p1, 0x7f0901f0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    const-string v0, "None"

    const-string v1, "Max"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lig0;->a:Landroid/content/Context;

    const v4, 0x7f0c00f0

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v0, p0, Lig0;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lig0;->f:[[Z

    array-length v0, v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v0, v3, :cond_1

    iget-object v0, p0, Lig0;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lig0;->f:[[Z

    array-length v5, v5

    new-array v6, v5, [Ljava/lang/String;

    move v7, v2

    :goto_0
    if-ge v7, v5, :cond_0

    aget-object v8, v0, v7

    aput-object v8, v6, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lig0;->a:Landroid/content/Context;

    invoke-direct {v0, v5, v4, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v4, p0, Lig0;->j:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lig0;->j:Landroid/widget/Spinner;

    new-instance v4, Lig0$b;

    invoke-direct {v4, p0}, Lig0$b;-><init>(Lig0;)V

    invoke-virtual {v0, v4}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lig0;->g()V

    :goto_1
    iget-object p1, p0, Lig0;->c:Ljava/lang/String;

    if-eqz p1, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lig0;->i:Landroid/widget/Spinner;

    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->setSelection(I)V

    goto :goto_3

    :cond_3
    :goto_2
    iget-object p1, p0, Lig0;->i:Landroid/widget/Spinner;

    invoke-virtual {p1, v3}, Landroid/widget/AdapterView;->setSelection(I)V

    :goto_3
    const p1, 0x7f090086

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lig0$c;

    invoke-direct {v0, p0}, Lig0$c;-><init>(Lig0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090092

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lig0$d;

    invoke-direct {v0, p0}, Lig0$d;-><init>(Lig0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

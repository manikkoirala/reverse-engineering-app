.class public Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->R(Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->a:Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 8

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->L(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    const/4 p1, 0x0

    if-eqz p3, :cond_0

    instance-of p2, p3, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    if-eqz p2, :cond_0

    new-instance p2, Lz91;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-direct {p2, p3}, Lz91;-><init>(Landroid/content/Context;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->M(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string p3, "PURCHASE_GRANT_COMPLETED"

    invoke-virtual {p2, p3, p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f$a;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string p3, "PURCHASE_GRANT_FAILED"

    invoke-virtual {p2, p3, p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;->b:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const/4 v2, 0x0

    const v3, 0x7f1201ef

    const v4, 0x7f12029a

    const v5, 0x7f120060

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    :goto_0
    return-void
.end method

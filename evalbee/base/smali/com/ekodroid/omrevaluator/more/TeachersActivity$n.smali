.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/TeachersActivity;->Q()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;->a:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p1

    int-to-long p1, p1

    iget-object p4, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p4, p4, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {p4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ltu1;

    const-wide/32 v0, 0x7f090191    # 1.0530004593E-314

    cmp-long p1, p1, v0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->E(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;->a:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object p1

    sget-object p2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne p1, p2, :cond_1

    if-lez p3, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {p1, p4}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->F(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ltu1;)V

    :cond_1
    return-void
.end method

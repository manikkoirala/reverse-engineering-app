.class public Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lyb;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->V()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/android/billingclient/api/a;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/android/billingclient/api/a;->b()I

    move-result p1

    if-nez p1, :cond_1

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "teachers_01"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "enterprise_05"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "enterprise_10"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "enterprise_15"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "enterprise_20"

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lha1$b;->a()Lha1$b$a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lha1$b$a;->b(Ljava/lang/String;)Lha1$b$a;

    move-result-object v1

    const-string v2, "inapp"

    invoke-virtual {v1, v2}, Lha1$b$a;->c(Ljava/lang/String;)Lha1$b$a;

    move-result-object v1

    invoke-virtual {v1}, Lha1$b$a;->a()Lha1$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object p1

    invoke-static {}, Lha1;->a()Lha1$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lha1$a;->b(Ljava/util/List;)Lha1$a;

    move-result-object p1

    invoke-virtual {p1}, Lha1$a;->a()Lha1;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->E(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)Lwb;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h$a;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;)V

    invoke-virtual {v0, p1, v1}, Lwb;->e(Lha1;Lu81;)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "BILLING_CLIENT_START_FAILED"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h$b;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;)V

    const v2, 0x7f120077

    const v3, 0x7f1201e6

    const v4, 0x7f12029a

    const v5, 0x7f120060

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    :goto_1
    return-void
.end method

.method public b()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "BILLING_CLIENT_DISCONNECTED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

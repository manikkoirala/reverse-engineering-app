.class public Lcom/ekodroid/omrevaluator/more/ContactUsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

.field public d:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->c:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->C(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->D(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final C(Ljava/lang/String;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final D(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->d:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    new-instance v0, Ln52;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->c:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object v0

    const-string v2, "MyPref"

    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "user_country"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->c:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v3, "Please wait, sending email..."

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    new-instance v3, Lj61;

    new-instance v4, Lcom/ekodroid/omrevaluator/serializable/EmailData;

    invoke-direct {v4, p2, p1, p3, v1}, Lcom/ekodroid/omrevaluator/serializable/EmailData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$c;

    invoke-direct {p1, p0, v2}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Landroid/app/ProgressDialog;)V

    invoke-direct {v3, v4, v0, p1}, Lj61;-><init>(Lcom/ekodroid/omrevaluator/serializable/EmailData;Lee1;Ly01;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0024

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    const p1, 0x7f0903be

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f090143

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f090148

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/widget/EditText;

    const v1, 0x7f090334

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->c:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f030001

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0c00f0

    invoke-direct {v1, v2, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v5, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v1, 0x7f0900df

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->d:Landroid/widget/Button;

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v7

    if-eqz v7, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->d:Landroid/widget/Button;

    new-instance v8, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;

    move-object v1, v8

    move-object v2, p0

    move-object v4, v0

    move-object v6, v7

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/Spinner;Lr30;)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0901e3

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$b;

    invoke-direct {v1, p0, v7, v0}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Lr30;Landroid/widget/EditText;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->H()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->B(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->d:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v1, 0x7f08016e

    const v2, 0x7f0800bd

    const v3, 0x7f120198

    if-eqz v0, :cond_1

    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    invoke-static {p1, v3, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    const/16 v0, 0x64

    if-ge p1, v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f060044

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->C(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;I)V

    :cond_3
    return-void
.end method

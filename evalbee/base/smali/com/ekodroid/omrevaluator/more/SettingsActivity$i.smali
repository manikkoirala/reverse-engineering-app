.class public Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/SettingsActivity;->M()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->g:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "save_images"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->h:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "scan_sound"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    iget-boolean v0, v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->y:Z

    const-string v1, "scan_resolution_high"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->j:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "option_mark_with_pen"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->i:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "AUTO_SAVE_SCAN"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->p:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    const-string v1, "AUTO_SAVE_DELAY"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;->a:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->A(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    return-void
.end method

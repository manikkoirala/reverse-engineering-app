.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Landroid/widget/ListView;

.field public d:Landroid/content/Context;

.field public e:Ljava/util/ArrayList;

.field public f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public g:Landroidx/appcompat/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->M()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->P()V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->a0(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->Q()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->Z()V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ltu1;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->V(Ltu1;)V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->Y()V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->O()V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ltu1;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->X(Ltu1;)V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Lcom/ekodroid/omrevaluator/more/models/Teacher;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->S(Lcom/ekodroid/omrevaluator/more/models/Teacher;)V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->N(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final L(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {v0}, La91;->r(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getTeachersCount()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->R(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;)V

    :cond_2
    return-void
.end method

.method public final M()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    new-instance v0, Lpa0;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    invoke-direct {v0, v1, v2}, Lpa0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final N(Ljava/lang/String;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1202d3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Le61;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    sget-object v3, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->TEACHER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    invoke-direct {v2, v3, p1}, Lcom/ekodroid/omrevaluator/more/models/Teacher;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v3, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;

    invoke-direct {v3, p0, v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/app/ProgressDialog;)V

    invoke-direct {v1, v2, p1, v3}, Le61;-><init>(Lcom/ekodroid/omrevaluator/more/models/Teacher;Landroid/content/Context;Lzg;)V

    invoke-virtual {v1}, Le61;->f()V

    return-void
.end method

.method public final O()V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, "Please wait, exiting organization account..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v1

    const-string v2, "TEACHER_LEFT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v1, Lh61;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v3, Lcom/ekodroid/omrevaluator/more/TeachersActivity$q;

    invoke-direct {v3, p0, v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$q;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/app/ProgressDialog;)V

    invoke-direct {v1, v2, v3}, Lh61;-><init>(Landroid/content/Context;Lzg;)V

    invoke-virtual {v1}, Lh61;->f()V

    return-void
.end method

.method public final P()V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v2

    sget-object v3, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v2, v3, :cond_2

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {v2}, La91;->r(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getTeachersCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f12017c

    const v7, 0x7f120060

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->W()V

    goto :goto_0

    :cond_2
    iget-object v11, v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const v14, 0x7f1201cf

    const v15, 0x7f120060

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v11 .. v18}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    :goto_0
    return-void
.end method

.method public final Q()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v2}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getEmailId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    new-instance v5, Ltu1;

    const/4 v6, 0x1

    invoke-direct {v5, v6, v3}, Ltu1;-><init>(ZLcom/ekodroid/omrevaluator/more/models/Teacher;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    new-instance v6, Ltu1;

    invoke-direct {v6, v4, v3}, Ltu1;-><init>(ZLcom/ekodroid/omrevaluator/more/models/Teacher;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/TeachersActivity$m;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$m;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->sort(Ljava/util/Comparator;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->c:Landroid/widget/ListView;

    new-instance v2, Lr3;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->e:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-direct {v2, v3, v5}, Lr3;-><init>(Ljava/util/ArrayList;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->c:Landroid/widget/ListView;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$n;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v1, 0x7f090463

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    const v2, 0x7f0901b4

    if-ne v0, v1, :cond_3

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$o;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$o;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void
.end method

.method public final R(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getUserRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->TEACHER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->S(Lcom/ekodroid/omrevaluator/more/models/Teacher;)V

    :cond_1
    return-void
.end method

.method public final S(Lcom/ekodroid/omrevaluator/more/models/Teacher;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, "Please wait, removing teacher..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Lk61;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v3, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;

    invoke-direct {v3, p0, v0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/app/ProgressDialog;Lcom/ekodroid/omrevaluator/more/models/Teacher;)V

    invoke-direct {v1, p1, v2, v3}, Lk61;-><init>(Lcom/ekodroid/omrevaluator/more/models/Teacher;Landroid/content/Context;Lzg;)V

    invoke-virtual {v1}, Lk61;->f()V

    return-void
.end method

.method public final T(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_organization"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    invoke-interface {p2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p2

    if-eqz p2, :cond_0

    new-instance v0, Lg22$a;

    invoke-direct {v0}, Lg22$a;-><init>()V

    invoke-virtual {v0, p1}, Lg22$a;->b(Ljava/lang/String;)Lg22$a;

    move-result-object p1

    invoke-virtual {p1}, Lg22$a;->a()Lg22;

    move-result-object p1

    invoke-virtual {p2, p1}, Lr30;->Z(Lg22;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final U()V
    .locals 2

    .line 1
    const v0, 0x7f090451

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->g:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->g:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$i;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final V(Ltu1;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$r;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$r;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ltu1;)V

    const p1, 0x7f1202bc

    const/4 v2, 0x0

    const/high16 v3, 0x7f030000

    invoke-static {v0, p1, v2, v3, v1}, Lxs;->a(Landroid/content/Context;IIILxs$i;)V

    return-void
.end method

.method public final W()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0062

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f12002e

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f09039e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Lcom/google/android/material/textfield/TextInputLayout;)V

    const v1, 0x7f12011f

    invoke-virtual {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$d;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    const v2, 0x7f120053

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final X(Ltu1;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$a;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ltu1;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const v2, 0x7f120031

    const v3, 0x7f1201cd

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final Y()V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$p;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$p;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const v2, 0x7f120137

    const v3, 0x7f1201b7

    const v4, 0x7f120136

    const v5, 0x7f120053

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final Z()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v5

    if-nez v5, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0068

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1200ac

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090399

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/material/textfield/TextInputLayout;

    const v3, 0x7f09039a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v2}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_2

    :cond_1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v2

    invoke-virtual {v2}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x0

    if-eqz v2, :cond_3

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const-string v3, "MyPref"

    invoke-virtual {v2, v3, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "user_organization"

    const-string v8, ""

    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const v2, 0x7f090090

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/widget/Button;

    const v2, 0x7f090084

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Landroid/widget/Button;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-eq v1, v2, :cond_5

    invoke-virtual {v6, v7}, Landroid/view/View;->setActivated(Z)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setFocusable(Z)V

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    new-instance v7, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;

    move-object v1, v7

    move-object v2, p0

    move-object v3, v4

    move-object v4, v6

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/widget/EditText;Landroid/widget/EditText;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;Landroidx/appcompat/app/a;)V

    invoke-virtual {v8, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/more/TeachersActivity$g;

    invoke-direct {v1, p0, v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$g;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroidx/appcompat/app/a;)V

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final a0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->T(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, "Please wait, updating profile..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Ls61;

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserProfileDto;

    invoke-direct {v2, p1, p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserProfileDto;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance v4, Lcom/ekodroid/omrevaluator/more/TeachersActivity$h;

    invoke-direct {v4, p0, v0, p1, p2}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$h;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/app/ProgressDialog;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3, v4}, Ls61;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserProfileDto;Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0043

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->U()V

    const p1, 0x7f09024f

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->c:Landroid/widget/ListView;

    const p1, 0x7f09036e

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$j;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$j;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    const p1, 0x7f09009f

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$k;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$k;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->L(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->Q()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->M()V

    return-void
.end method

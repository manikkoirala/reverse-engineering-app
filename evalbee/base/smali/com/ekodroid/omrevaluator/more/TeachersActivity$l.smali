.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/TeachersActivity;->M()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;->a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;->a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->f:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;->a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->D(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;->a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRequest()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    move-result-object p2

    if-eqz p2, :cond_1

    new-instance p1, Lmg0;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;->a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    new-instance p3, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l$a;

    invoke-direct {p3, p0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l$a;-><init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;)V

    invoke-direct {p1, p2, p3}, Lmg0;-><init>(Landroid/content/Context;Ly01;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-nez p2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object p2

    sget-object p3, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne p2, p3, :cond_4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_4

    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$l;->a:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->E(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    :cond_4
    :goto_1
    return-void
.end method

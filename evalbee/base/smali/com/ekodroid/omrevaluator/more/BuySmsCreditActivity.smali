.class public Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Landroid/content/Context;

.field public d:Landroid/widget/EditText;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/Button;

.field public h:Landroid/widget/ProgressBar;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->K()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->D()Z

    move-result p0

    return p0
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->J(I)V

    return-void
.end method


# virtual methods
.method public final D()Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-eq v0, v1, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    const/4 v3, 0x0

    const v4, 0x7f12024a

    const v5, 0x7f1201cc

    const v6, 0x7f120060

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final E(I)Ljava/lang/String;
    .locals 4

    .line 1
    const/16 v0, 0x270f

    if-le p1, v0, :cond_0

    const/16 v0, 0x16

    goto :goto_0

    :cond_0
    const/16 v0, 0x19

    :goto_0
    mul-int/2addr v0, p1

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f12031b

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, " INR "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final F(I)Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x270f

    if-le p1, v0, :cond_0

    const/16 p1, 0x16

    goto :goto_0

    :cond_0
    const/16 p1, 0x19

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const p1, 0x7f1201c7

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final G()V
    .locals 5

    .line 1
    const v0, 0x7f09014a

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->d:Landroid/widget/EditText;

    const v1, 0x7f0903ec

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->e:Landroid/widget/TextView;

    const v1, 0x7f0903b6

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->d:Landroid/widget/EditText;

    const v0, 0x7f0900d9

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    const v0, 0x7f0902d0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->h:Landroid/widget/ProgressBar;

    const v0, 0x7f090404

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->i:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "SMS_CREDIT"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->d:Landroid/widget/EditText;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0901aa

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$b;-><init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final H()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$c;-><init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final I()V
    .locals 2

    .line 1
    const v0, 0x7f09043a

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$e;-><init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final J(I)V
    .locals 4

    .line 1
    new-instance v0, Ln52;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object v0

    new-instance v1, Lm61;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    new-instance v3, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;-><init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V

    invoke-direct {v1, v2, v0, p1, v3}, Lm61;-><init>(Landroid/content/Context;Lee1;ILy01;)V

    return-void
.end method

.method public final K()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->F(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->E(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0021

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->j:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->G()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->I()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->H()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lba1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->S()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$i;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/android/billingclient/api/a;Ljava/util/List;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/android/billingclient/api/a;->b()I

    move-result p1

    if-nez p1, :cond_0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/billingclient/api/Purchase;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$i;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "PURCHASE_OWNED_FOUND"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$i;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->T(Lcom/android/billingclient/api/Purchase;)V

    goto :goto_0

    :cond_0
    return-void
.end method

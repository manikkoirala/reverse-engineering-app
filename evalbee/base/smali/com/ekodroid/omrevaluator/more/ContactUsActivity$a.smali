.class public Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Landroid/widget/EditText;

.field public final synthetic c:Landroid/widget/Spinner;

.field public final synthetic d:Lr30;

.field public final synthetic e:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/Spinner;Lr30;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->e:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->b:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->c:Landroid/widget/Spinner;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->d:Lr30;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->a:Landroid/widget/EditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->b:Landroid/widget/EditText;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->c:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->d:Lr30;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const v3, 0x7f1200e6

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->b:Landroid/widget/EditText;

    :goto_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->e:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    :goto_2
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->b:Landroid/widget/EditText;

    goto :goto_3

    :cond_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->e:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    invoke-static {v2, v1}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->A(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->e:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    const v3, 0x7f1200bf

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->a:Landroid/widget/EditText;

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->e:Lcom/ekodroid/omrevaluator/more/ContactUsActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ContactUsActivity$a;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/ekodroid/omrevaluator/more/ContactUsActivity;->B(Lcom/ekodroid/omrevaluator/more/ContactUsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    return-void
.end method

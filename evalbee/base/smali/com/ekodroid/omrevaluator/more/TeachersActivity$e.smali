.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/TeachersActivity;->N(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->a:Landroid/app/ProgressDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 9

    .line 1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    check-cast p3, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->save(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->D(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    const v2, 0x7f1201d0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getEmailId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    const v2, 0x7f12016d

    invoke-virtual {p3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    const p3, 0x7f12001d

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const/4 v2, 0x0

    const v3, 0x7f120120

    const v5, 0x7f120060

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "ADD_TEACHER_SUCCESS"

    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "ADD_TEACHER_FAIL"

    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$e;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const p3, 0x7f1201f4

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const p3, 0x7f0800bd

    const v0, 0x7f08016e

    invoke-static {p2, p1, p3, v0}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :goto_0
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/TeachersActivity;->S(Lcom/ekodroid/omrevaluator/more/models/Teacher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/more/models/Teacher;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/app/ProgressDialog;Lcom/ekodroid/omrevaluator/more/models/Teacher;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->b:Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 4

    .line 1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object p3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getEmailId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->b:Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getEmailId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->setTeacherList(Ljava/util/ArrayList;)V

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-virtual {p1, p3}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->save(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->D(Lcom/ekodroid/omrevaluator/more/TeachersActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "TEACHER_REMOVED"

    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "TEACHER_REMOVED_FAIL"

    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$b;->c:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const p2, 0x7f0800bd

    const p3, 0x7f08016e

    const-string v0, "Update failed, please try again"

    invoke-static {p1, v0, p2, p3}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :goto_1
    return-void
.end method

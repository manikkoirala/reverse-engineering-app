.class public Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/Button;

.field public k:Landroid/widget/Button;

.field public l:Landroid/widget/Button;

.field public m:Landroid/widget/ProgressBar;

.field public n:Laa1;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public t:Lwb;

.field public v:Ljava/util/List;

.field public w:I

.field public x:Landroid/os/Handler;

.field public y:Z

.field public z:Landroidx/appcompat/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const/4 v0, -0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->w:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->y:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->Z()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->P()V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->S()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c0()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)Lwb;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->t:Lwb;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->w:I

    return p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->w:I

    return p1
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->O()Z

    move-result p0

    return p0
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->d0(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->Q()V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->V()V

    return-void
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->U()V

    return-void
.end method

.method public static synthetic M(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->X()V

    return-void
.end method

.method public static synthetic N(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->R(Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V

    return-void
.end method


# virtual methods
.method public final O()Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-eq v0, v1, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const/4 v3, 0x0

    const v4, 0x7f12024a

    const v5, 0x7f1201cc

    const v6, 0x7f120060

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final P()V
    .locals 3

    .line 1
    const v0, 0x7f0900a7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$o;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$o;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$p;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$p;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    new-instance v1, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Ln52;->b()Lee1;

    move-result-object v1

    new-instance v2, Lva0;

    invoke-direct {v2, p0, v1, v0}, Lva0;-><init>(Landroid/content/Context;Lee1;Ly01;)V

    return-void
.end method

.method public final Q()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final R(Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V
    .locals 3

    .line 1
    const v0, 0x7f1201e9

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->e0(I)V

    new-instance v0, Li32;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$f;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V

    invoke-direct {v0, p1, v1, v2}, Li32;-><init>(Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final S()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->t:Lwb;

    invoke-virtual {v0}, Lwb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->t:Lwb;

    invoke-static {}, Lia1;->a()Lia1$a;

    move-result-object v1

    const-string v2, "inapp"

    invoke-virtual {v1, v2}, Lia1$a;->b(Ljava/lang/String;)Lia1$a;

    move-result-object v1

    invoke-virtual {v1}, Lia1$a;->a()Lia1;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$i;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$i;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1, v2}, Lwb;->f(Lia1;Lba1;)V

    :cond_0
    return-void
.end method

.method public T(Lcom/android/billingclient/api/Purchase;)V
    .locals 12

    .line 1
    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->U()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const/4 v3, 0x0

    const v4, 0x7f120270

    const v5, 0x7f1201c9

    const v6, 0x7f120060

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/String;

    new-instance v1, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;

    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->q:Ljava/lang/String;

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->p:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/billingclient/api/Purchase;->e()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, La91;->n()I

    move-result v2

    int-to-long v9, v2

    iget-boolean v11, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->y:Z

    move-object v2, v1

    invoke-direct/range {v2 .. v11}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->R(Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;)V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public final U()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->n:Laa1;

    invoke-virtual {v0}, Laa1;->a()V

    return-void
.end method

.method public final V()V
    .locals 2

    .line 1
    invoke-static {p0}, Lwb;->d(Landroid/content/Context;)Lwb$a;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Lwb$a;->c(Lca1;)Lwb$a;

    move-result-object v0

    invoke-virtual {v0}, Lwb$a;->b()Lwb$a;

    move-result-object v0

    invoke-virtual {v0}, Lwb$a;->a()Lwb;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->t:Lwb;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$h;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Lwb;->g(Lyb;)V

    return-void
.end method

.method public final W()V
    .locals 2

    .line 1
    const v0, 0x7f09020f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->i:Landroid/widget/LinearLayout;

    const v0, 0x7f0902ce

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->m:Landroid/widget/ProgressBar;

    const v0, 0x7f0903eb

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->f:Landroid/widget/TextView;

    const v0, 0x7f0900a5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->k:Landroid/widget/Button;

    const v0, 0x7f0900a6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->j:Landroid/widget/Button;

    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->l:Landroid/widget/Button;

    const v0, 0x7f090412

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->g:Landroid/widget/TextView;

    const v0, 0x7f090413

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->h:Landroid/widget/TextView;

    const v0, 0x7f090466

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->d:Landroid/widget/TextView;

    const v0, 0x7f090465

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->k:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$q;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$q;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->l:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$r;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$r;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->j:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$s;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$s;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0901f8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$t;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$t;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09008f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09008e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$b;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$c;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final X()V
    .locals 2

    .line 1
    new-instance v0, Lsa0;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$k;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-direct {v0, p0, v1}, Lsa0;-><init>(Landroid/content/Context;Lzg;)V

    new-instance v0, Lta0;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$m;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$m;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-direct {v0, p0, v1}, Lta0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final Y()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "user_country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "INDIA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const v3, 0x7f0901e1

    if-nez v1, :cond_0

    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const-string v1, "SMS_LINK_EXP"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, La91;->n()I

    move-result v1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->g0()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->P()V

    :goto_0
    return-void
.end method

.method public final Z()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {v0}, La91;->r(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    move-result-object v0

    const/16 v1, 0x8

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getExpiryDate()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->i:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->f:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f120021

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getAccountType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->g:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f120166

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getTeachersCount()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v3, "FREE"

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getAccountType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->h:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f12032d

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->isRenewDue()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->y:Z

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getAccountType()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lok;->R:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const v3, 0x7f120289

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->j:Landroid/widget/Button;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getAccountType()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lok;->S:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->k:Landroid/widget/Button;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;

    move-result-object v2

    const v3, 0x7f090215

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getTeachersCount()I

    move-result v5

    if-lez v5, :cond_3

    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0903f7

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;->reportsCount:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getTeachersCount()I

    move-result v4

    mul-int/lit16 v4, v4, 0x3e8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v1, v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;->reportsCount:J

    const-wide/16 v3, 0x64

    mul-long/2addr v1, v3

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getTeachersCount()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v3, v0

    div-long/2addr v1, v3

    long-to-int v0, v1

    const v1, 0x7f0902d4

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->i:Landroid/widget/LinearLayout;

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public a0(Ljava/util/List;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->v:Ljava/util/List;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->j:Landroid/widget/Button;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->v:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt81;

    invoke-virtual {v0}, Lt81;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "teachers_01"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Lt81;->a()Lt81$a;

    move-result-object v2

    invoke-virtual {v2}, Lt81$a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v0}, Lt81;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enterprise_05"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Lt81;->a()Lt81$a;

    move-result-object v0

    invoke-virtual {v0}, Lt81$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->Z()V

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$e;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$e;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const v4, 0x7f120077

    const v5, 0x7f1201e6

    const v6, 0x7f12029a

    const v7, 0x7f120060

    const/4 v8, 0x0

    const v9, 0x7f0800d2

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    :goto_1
    return-void
.end method

.method public final b0()V
    .locals 2

    .line 1
    const v0, 0x7f090449

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->z:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->z:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$l;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final c0()V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$j;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$j;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    const/4 v2, 0x0

    const v3, 0x7f120180

    const v4, 0x7f12007a

    const v5, 0x7f120060

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final d0(Landroid/view/View;)V
    .locals 5

    .line 1
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/material/snackbar/Snackbar;->make(Landroid/view/View;Ljava/lang/CharSequence;I)Lcom/google/android/material/snackbar/Snackbar;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0c0096

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/material/snackbar/BaseTransientBottomBar;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/material/snackbar/Snackbar$SnackbarLayout;

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    const v3, 0x7f090213

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$d;

    invoke-direct {v4, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$d;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    invoke-virtual {p1}, Lcom/google/android/material/snackbar/Snackbar;->show()V

    return-void
.end method

.method public final e0(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->n:Laa1;

    if-nez v0, :cond_0

    new-instance v0, Laa1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-direct {v0, v1, p1}, Laa1;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->n:Laa1;

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Laa1;->b(I)V

    :goto_0
    return-void
.end method

.method public f0(Lt81;)V
    .locals 1

    .line 1
    const v0, 0x7f1201f7

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->e0(I)V

    invoke-static {}, Lzb$b;->a()Lzb$b$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lzb$b$a;->b(Lt81;)Lzb$b$a;

    move-result-object p1

    invoke-virtual {p1}, Lzb$b$a;->a()Lzb$b;

    move-result-object p1

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object p1

    invoke-static {}, Lzb;->a()Lzb$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lzb$a;->c(Ljava/util/List;)Lzb$a;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lzb$a;->b(Ljava/lang/String;)Lzb$a;

    move-result-object p1

    invoke-virtual {p1}, Lzb$a;->a()Lzb;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->t:Lwb;

    invoke-virtual {v0, p0, p1}, Lwb;->c(Landroid/app/Activity;Lzb;)Lcom/android/billingclient/api/a;

    return-void
.end method

.method public final g0()V
    .locals 3

    .line 1
    new-instance v0, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object v0

    new-instance v1, Lq61;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$n;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$n;-><init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    invoke-direct {v1, v0, v2}, Lq61;-><init>(Lee1;Ly01;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0036

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->b0()V

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->x:Landroid/os/Handler;

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p1

    invoke-virtual {p1}, Lr30;->O()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->p:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p1}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object p1

    const-string v0, "emailKey"

    invoke-static {p1, v0}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->q:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->W()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->V()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->t:Lwb;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lwb;->a()V

    :cond_0
    invoke-super {p0}, Lv5;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->X()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->S()V

    return-void
.end method

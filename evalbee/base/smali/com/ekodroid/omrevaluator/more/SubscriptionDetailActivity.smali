.class public Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Landroidx/appcompat/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .line 1
    const v0, 0x7f090450

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity;->c:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity;->c:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0042

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SubscriptionDetailActivity;->A()V

    const p1, 0x7f09047f

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Landroid/webkit/WebView;

    const/4 v1, 0x0

    const p1, 0x7f12030a

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

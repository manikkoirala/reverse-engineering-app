.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/TeachersActivity;->W()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/android/material/textfield/TextInputLayout;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Lcom/google/android/material/textfield/TextInputLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p2}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    const v2, 0x7f08016e

    const v3, 0x7f0800bd

    if-lt v0, v1, :cond_2

    const-string v0, "@"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    invoke-virtual {v0}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const-string p2, "Please enter different email id then yours"

    invoke-static {p1, p2, v3, v2}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {v0, p2}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->K(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$c;->b:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const-string p2, "Please enter valid email"

    invoke-static {p1, p2, v3, v2}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void
.end method

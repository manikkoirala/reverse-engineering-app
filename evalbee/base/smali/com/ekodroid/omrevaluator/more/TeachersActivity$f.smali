.class public Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/TeachersActivity;->Z()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Landroid/widget/EditText;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

.field public final synthetic d:Landroidx/appcompat/app/a;

.field public final synthetic e:Lcom/ekodroid/omrevaluator/more/TeachersActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Landroid/widget/EditText;Landroid/widget/EditText;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;Landroidx/appcompat/app/a;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->e:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->b:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->c:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->d:Landroidx/appcompat/app/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->a:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const v1, 0x7f08016e

    const v2, 0x7f0800bd

    const/4 v3, 0x1

    if-ge v0, v3, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->e:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const v0, 0x7f120197

    invoke-static {p1, v0, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->c:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v4

    sget-object v5, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v4, v5, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v3, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->e:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->d:Landroid/content/Context;

    const v0, 0x7f120199

    invoke-static {p1, v0, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->e:Lcom/ekodroid/omrevaluator/more/TeachersActivity;

    invoke-static {v1, p1, v0}, Lcom/ekodroid/omrevaluator/more/TeachersActivity;->C(Lcom/ekodroid/omrevaluator/more/TeachersActivity;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/TeachersActivity$f;->d:Landroidx/appcompat/app/a;

    invoke-virtual {p1}, Lq6;->dismiss()V

    return-void
.end method

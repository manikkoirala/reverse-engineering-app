.class public final enum Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/more/models/Teacher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MemberStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field public static final enum ACCEPTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field public static final enum EXPIRED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field public static final enum LEFT:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field public static final enum PENDING:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field public static final enum REJECTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field public static final enum REQUEST_SENT:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
    .locals 6

    sget-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->REQUEST_SENT:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    sget-object v1, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->PENDING:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    sget-object v2, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->ACCEPTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    sget-object v3, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->REJECTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    sget-object v4, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->LEFT:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    sget-object v5, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->EXPIRED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    filled-new-array/range {v0 .. v5}, [Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    const-string v1, "REQUEST_SENT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->REQUEST_SENT:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    new-instance v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    const-string v1, "PENDING"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->PENDING:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    new-instance v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    const-string v1, "ACCEPTED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->ACCEPTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    new-instance v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    const-string v1, "REJECTED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->REJECTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    new-instance v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    const-string v1, "LEFT"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->LEFT:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    new-instance v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    const-string v1, "EXPIRED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->EXPIRED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    invoke-static {}, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->$values()[Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->$VALUES:[Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->$VALUES:[Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-object v0
.end method

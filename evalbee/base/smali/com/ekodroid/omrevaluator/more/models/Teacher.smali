.class public Lcom/ekodroid/omrevaluator/more/models/Teacher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
    }
.end annotation


# instance fields
.field displayName:Ljava/lang/String;

.field emailId:Ljava/lang/String;

.field memberStatus:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field userId:Ljava/lang/String;

.field userRole:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->userRole:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->emailId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->emailId:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberStatus()Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->memberStatus:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public getUserRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->userRole:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setEmailId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->emailId:Ljava/lang/String;

    return-void
.end method

.method public setMemberStatus(Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->memberStatus:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->userId:Ljava/lang/String;

    return-void
.end method

.method public setUserRole(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/models/Teacher;->userRole:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-void
.end method

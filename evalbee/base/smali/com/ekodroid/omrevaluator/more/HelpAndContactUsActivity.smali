.class public Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/LinearLayout;

.field public g:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->g:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final B()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final C()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->e:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final D()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->f:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c002d

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    const p1, 0x7f0901f8

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->c:Landroid/widget/LinearLayout;

    const p1, 0x7f0901e2

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->d:Landroid/widget/LinearLayout;

    const p1, 0x7f09020b

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->e:Landroid/widget/LinearLayout;

    const p1, 0x7f090228

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->f:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->B()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->A()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->C()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/HelpAndContactUsActivity;->D()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

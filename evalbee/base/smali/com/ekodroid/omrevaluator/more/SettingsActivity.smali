.class public Lcom/ekodroid/omrevaluator/more/SettingsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public A:Landroid/widget/TextView;

.field public C:[Ljava/lang/Integer;

.field public D:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public F:Landroid/content/SharedPreferences;

.field public G:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;

.field public H:Landroid/widget/TextView;

.field public c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/LinearLayout;

.field public g:Landroidx/appcompat/widget/SwitchCompat;

.field public h:Landroidx/appcompat/widget/SwitchCompat;

.field public i:Landroidx/appcompat/widget/SwitchCompat;

.field public j:Landroid/widget/RadioButton;

.field public k:Landroid/widget/RadioButton;

.field public l:Landroid/widget/RadioButton;

.field public m:Landroid/widget/RadioButton;

.field public n:Landroid/widget/RadioGroup;

.field public p:Landroid/widget/Spinner;

.field public q:Landroid/widget/ArrayAdapter;

.field public t:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 6

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    const/16 v0, 0x9

    new-array v1, v0, [Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v1, v4

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v1, v5

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v3

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v4

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->C:[Ljava/lang/Integer;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->K()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;

    return-object p0
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/more/SettingsActivity;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;

    return-object p1
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->T()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->S()V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->J()V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)Landroid/content/SharedPreferences;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->R()V

    return-void
.end method


# virtual methods
.method public final I()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$c;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    new-instance v1, Lka0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-direct {v1, v2, v0}, Lka0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final J()V
    .locals 3

    .line 1
    new-instance v0, Les;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    new-instance v2, Lcom/ekodroid/omrevaluator/more/SettingsActivity$e;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$e;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-direct {v0, v1, v2}, Les;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final K()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->D:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->D:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final L()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    const-string v1, "save_images"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->t:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    const-string v1, "scan_sound"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->v:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    const-string v1, "scan_resolution_high"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->y:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    const-string v1, "option_mark_with_pen"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->w:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    const-string v1, "AUTO_SAVE_SCAN"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->x:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    const-string v1, "AUTO_SAVE_DELAY"

    const/4 v3, 0x3

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->z:I

    const v0, 0x7f0902ea

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->n:Landroid/widget/RadioGroup;

    const v0, 0x7f0903f8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->A:Landroid/widget/TextView;

    const v0, 0x7f0900fb

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->g:Landroidx/appcompat/widget/SwitchCompat;

    const v0, 0x7f0900fc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->h:Landroidx/appcompat/widget/SwitchCompat;

    const v0, 0x7f0900f4

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->i:Landroidx/appcompat/widget/SwitchCompat;

    const v0, 0x7f0902e0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->j:Landroid/widget/RadioButton;

    const v0, 0x7f0902e1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->k:Landroid/widget/RadioButton;

    const v0, 0x7f0902dd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->m:Landroid/widget/RadioButton;

    const v0, 0x7f0902dc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->l:Landroid/widget/RadioButton;

    const v0, 0x7f090332

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->p:Landroid/widget/Spinner;

    const v0, 0x7f090217

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f0c00f0

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->C:[Ljava/lang/Integer;

    invoke-direct {v0, p0, v1, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->q:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->p:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->p:Landroid/widget/Spinner;

    iget v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->z:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->l:Landroid/widget/RadioButton;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->m:Landroid/widget/RadioButton;

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->j:Landroid/widget/RadioButton;

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->w:Z

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->k:Landroid/widget/RadioButton;

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->w:Z

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->g:Landroidx/appcompat/widget/SwitchCompat;

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->t:Z

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->h:Landroidx/appcompat/widget/SwitchCompat;

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->v:Z

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->i:Landroidx/appcompat/widget/SwitchCompat;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->i:Landroidx/appcompat/widget/SwitchCompat;

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->x:Z

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    const v0, 0x7f0901f6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->d:Landroid/widget/LinearLayout;

    const v0, 0x7f090200

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->e:Landroid/widget/LinearLayout;

    const v0, 0x7f0901e7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->f:Landroid/widget/LinearLayout;

    const v0, 0x7f090460

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->H:Landroid/widget/TextView;

    return-void
.end method

.method public final M()V
    .locals 2

    .line 1
    const v0, 0x7f0900d1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$i;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final N()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->f:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$h;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->e:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$g;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Q()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->R()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->n:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$j;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$j;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public final R()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->l:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->y:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->A:Landroid/widget/TextView;

    const v1, 0x7f1201ab

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->y:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->A:Landroid/widget/TextView;

    const v1, 0x7f1201b9

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public final S()V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/more/SettingsActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$f;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->c:Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    const v2, 0x7f120098

    const v3, 0x7f12018a

    const v4, 0x7f120097

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800e1

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final T()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->G:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->H:Landroid/widget/TextView;

    const v1, 0x7f120054

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->H:Landroid/widget/TextView;

    const v1, 0x7f120098

    :goto_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c003c

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->D:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    :cond_0
    const-string p1, "MyPref"

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->F:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->Q()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->M()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->O()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->P()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->I()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity;->N()V

    const p1, 0x7f0900ad

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/more/SettingsActivity$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/more/SettingsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/more/SettingsActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

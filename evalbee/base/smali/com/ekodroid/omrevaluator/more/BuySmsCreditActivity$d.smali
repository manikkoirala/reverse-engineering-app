.class public Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->J(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 5

    .line 1
    const/4 v0, 0x1

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;

    invoke-static {}, La91;->n()I

    move-result v1

    add-int/lit8 v1, v1, 0x19

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "MyPref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "SMS_LINK_EXP"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->getShortUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    const-string v2, "No application can handle this request. Please install a webbrowser"

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f1201ce

    invoke-static {p1, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    const v1, 0x7f12029a

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity$d;->a:Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->g:Landroid/widget/Button;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/BuySmsCreditActivity;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f06005c

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lca1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->V()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/android/billingclient/api/a;Ljava/util/List;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/android/billingclient/api/a;->b()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/billingclient/api/Purchase;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->T(Lcom/android/billingclient/api/Purchase;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "PURCHASE_OK"

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/android/billingclient/api/a;->b()I

    move-result p1

    const/4 p2, 0x7

    if-ne p1, p2, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->L(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->C(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "PURCHASE_OWNED"

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->L(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->D(Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity$g;->a:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;->c:Lcom/ekodroid/omrevaluator/more/ProductAndServicesActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "PURCHASE_OTHER"

    :goto_1
    invoke-virtual {p1, p2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

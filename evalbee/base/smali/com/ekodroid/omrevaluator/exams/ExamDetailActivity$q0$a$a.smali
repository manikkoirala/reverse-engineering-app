.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;Landroid/app/ProgressDialog;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    check-cast p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->getId()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setCloudId(Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastSyncedOn(Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->getUpdatedOn()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastUpdatedOn(Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setCloudSyncOn(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSynced(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->getUserId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setUserId(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->isSyncImages()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSyncImages(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->b:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f:Landroid/widget/LinearLayout;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->E(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->F(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Landroidx/appcompat/widget/SwitchCompat;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    const/16 p1, 0x1f4

    const p3, 0x7f08016e

    const v0, 0x7f0800bd

    if-ne p2, p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v1, 0x7f1201a0

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v1, 0x7f12030c

    :goto_0
    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, v0, p3}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :goto_1
    return-void
.end method

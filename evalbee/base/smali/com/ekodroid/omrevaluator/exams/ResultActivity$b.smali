.class public Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->J(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->E(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Z)V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->L(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    return-void
.end method

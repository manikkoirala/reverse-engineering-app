.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/OnUserEarnedRewardListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUserEarnedReward(Lcom/google/android/gms/ads/rewarded/RewardItem;)V
    .locals 3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    const/16 v0, 0x32

    invoke-static {p1, v0}, Ldj1;->a(Landroid/content/Context;I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "REWARD_SCAN_GRANTED"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    const-string v2, "Extra scan rewarded"

    invoke-static {p1, v2, v0, v1}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void
.end method

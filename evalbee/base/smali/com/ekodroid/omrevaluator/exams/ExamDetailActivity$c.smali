.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/RadioButton;

.field public final synthetic b:Landroid/widget/RadioButton;

.field public final synthetic c:Landroid/widget/RadioButton;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/widget/RadioButton;Landroid/widget/RadioButton;Landroid/widget/RadioButton;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->a:Landroid/widget/RadioButton;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->b:Landroid/widget/RadioButton;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->c:Landroid/widget/RadioButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->a:Landroid/widget/RadioButton;

    invoke-virtual {p2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->K(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->b:Landroid/widget/RadioButton;

    invoke-virtual {p2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->L(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :cond_1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->c:Landroid/widget/RadioButton;

    invoke-virtual {p2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->M(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :cond_2
    return-void
.end method

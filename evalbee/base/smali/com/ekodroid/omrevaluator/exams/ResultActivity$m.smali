.class public Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/exams/ResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "m"
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/exams/ResultActivity$e;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->H(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v0}, Lve1;->l(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)D

    move-result-wide v0

    iput-wide v0, p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->t:D

    const/4 p1, 0x0

    return-object p1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->I(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->A:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->A:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

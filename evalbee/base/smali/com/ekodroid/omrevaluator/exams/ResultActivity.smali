.class public Lcom/ekodroid/omrevaluator/exams/ResultActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;
    }
.end annotation


# instance fields
.field public A:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public final c:I

.field public d:I

.field public e:Landroid/widget/ListView;

.field public f:Landroid/view/ViewGroup;

.field public g:Ln3;

.field public h:Ljava/util/ArrayList;

.field public i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/app/ProgressDialog;

.field public m:Ljava/lang/String;

.field public n:Landroid/content/BroadcastReceiver;

.field public p:Landroid/content/BroadcastReceiver;

.field public q:Lud1;

.field public t:D

.field public v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public w:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public x:I

.field public y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

.field public z:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m:Ljava/lang/String;

    const/16 v0, 0x3e7

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->x:I

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/google/android/play/core/review/ReviewManager;ILcom/google/android/play/core/tasks/Task;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->a0(Lcom/google/android/play/core/review/ReviewManager;ILcom/google/android/play/core/tasks/Task;)V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/ResultActivity;ILcom/google/android/play/core/tasks/Task;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->Z(ILcom/google/android/play/core/tasks/Task;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->R()Z

    move-result p0

    return p0
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->b0()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->l0(Z)V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/exams/ResultActivity;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->P(ILjava/lang/String;)V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->W(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->V(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->c0()V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)Lcom/ekodroid/omrevaluator/exams/ResultActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    return-object p0
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m0(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->d0()V

    return-void
.end method

.method public static synthetic M(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->U(Z)V

    return-void
.end method

.method public static synthetic N(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->k0(Z)V

    return-void
.end method

.method public static synthetic O(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Ljava/util/Set;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->T(Ljava/util/Set;Z)V

    return-void
.end method

.method private synthetic Z(ILcom/google/android/play/core/tasks/Task;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->z:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_rate_minutes"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p2}, Lcom/google/android/play/core/tasks/Task;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "IN_APP_REVIEW_SUCCESS"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private synthetic a0(Lcom/google/android/play/core/review/ReviewManager;ILcom/google/android/play/core/tasks/Task;)V
    .locals 1

    .line 1
    invoke-virtual {p3}, Lcom/google/android/play/core/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/google/android/play/core/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/android/play/core/review/ReviewInfo;

    invoke-interface {p1, p0, p3}, Lcom/google/android/play/core/review/ReviewManager;->launchReviewFlow(Landroid/app/Activity;Lcom/google/android/play/core/review/ReviewInfo;)Lcom/google/android/play/core/tasks/Task;

    move-result-object p1

    new-instance p3, Lue1;

    invoke-direct {p3, p0, p2}, Lue1;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;I)V

    invoke-virtual {p1, p3}, Lcom/google/android/play/core/tasks/Task;->addOnCompleteListener(Lcom/google/android/play/core/tasks/OnCompleteListener;)Lcom/google/android/play/core/tasks/Task;

    :cond_0
    return-void
.end method


# virtual methods
.method public final P(ILjava/lang/String;)V
    .locals 10

    .line 1
    new-instance v3, Lcom/ekodroid/omrevaluator/exams/ResultActivity$c;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    new-instance v9, Lw3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    const-string v2, "res"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v7, ""

    const-string v8, ""

    move-object v0, v9

    move-object v6, p2

    invoke-direct/range {v0 .. v8}, Lw3;-><init>(Landroid/content/Context;Ljava/lang/String;Ly01;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final Q(Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v0}, Lye1;->g()I

    move-result v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lye1;->p(Z)V

    goto :goto_0

    :cond_0
    const-string v1, " - - - "

    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final R()Z
    .locals 2

    .line 1
    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Lsl;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lh2;->g(Landroid/app/Activity;[Ljava/lang/String;I)V

    const/4 v0, 0x0

    return v0
.end method

.method public final S()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v2, 0x3e7

    :goto_0
    const-string v3, "MyPref"

    invoke-virtual {p0, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v3, v0, v2}, La91;->E(Landroid/content/SharedPreferences;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lgk;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final T(Ljava/util/Set;Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    int-to-long v3, v3

    cmp-long v3, v3, v1

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    int-to-long v3, v3

    cmp-long v3, v1, v3

    if-lez v3, :cond_3

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    int-to-long v3, p1

    cmp-long p1, v3, v1

    if-lez p1, :cond_4

    if-nez p2, :cond_4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p2

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastSyncedOn(Ljava/lang/Long;)V

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->l0(Z)V

    :cond_4
    return-void
.end method

.method public final U(Z)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastSyncedOn()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Lqa0;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    new-instance v9, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    invoke-direct {v9, p0, v0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Z)V

    invoke-direct/range {v3 .. v9}, Lqa0;-><init>(JJLandroid/content/Context;Lzg;)V

    return-void
.end method

.method public final V(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-object/from16 v4, p1

    if-eqz v3, :cond_0

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v5

    invoke-static {v5}, Lve1;->p(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v11

    invoke-static {v5}, Lve1;->q(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v12

    invoke-static {v5}, Lve1;->s(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v13

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v5

    invoke-static {v8, v9, v5}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v10

    new-instance v5, Lye1;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v7

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent()Z

    move-result v14

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced()Z

    move-result v15

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v16

    move-object v6, v5

    invoke-direct/range {v6 .. v16}, Lye1;-><init>(IDLjava/lang/String;IIIZZZ)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object/from16 v4, p1

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRankingMethod()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lve1;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->Q(Ljava/util/ArrayList;)V

    return-object v2
.end method

.method public final W(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lye1;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {v1}, Lye1;->g()I

    move-result v3

    invoke-virtual {v1}, Lye1;->f()I

    move-result v1

    invoke-direct {v2, v3, v1}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;-><init>(II)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final X()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "BLOCK_NO_SYNC"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final Y()V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$i;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$i;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->n:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$j;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$j;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->p:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public final b0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->z:Landroid/content/SharedPreferences;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->x:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m:Ljava/lang/String;

    const/16 v3, 0x1e

    invoke-static {v3, v0, v1, v2}, La91;->D(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->S()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v2, "CONNECTION_SUCCESS_VALID"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, La91;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "START_SCAN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->i0()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v2, "CONNECTION_SUCCESS_INVALID"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->X()V

    :goto_0
    return-void
.end method

.method public final c0()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e0()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    const-string v1, ""

    const/4 v2, 0x4

    const/4 v3, 0x0

    const v4, 0x7f0901eb

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Ln3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3, v4}, Ln3;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->g:Ln3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->k:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->k:Landroid/widget/TextView;

    const-string v2, "---"

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->j:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->t:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final d0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lv5;->n()Lt1;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->w:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "RA_OC_TEMPLATE_NULL"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0}, Lv5;->n()Lt1;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lt1;->t(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$m;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/exams/ResultActivity$e;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method public final e0()V
    .locals 6

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->RANK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->q:Lud1;

    const v2, 0x7f0901de

    if-eqz v1, :cond_4

    iget-boolean v3, v1, Lud1;->b:Z

    iget-object v1, v1, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    const v4, 0x7f090420

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->q:Lud1;

    iget-object v5, v5, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v5, v0, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120286

    :goto_0
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v5, v0, :cond_1

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f12014c

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v5, v0, :cond_2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120239

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f12029b

    goto :goto_0

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->q:Lud1;

    iget-boolean v0, v0, Lud1;->b:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120042

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f1200a3

    :goto_2
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x1

    :goto_3
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    new-instance v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    invoke-direct {v1}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->j(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V

    :cond_5
    return-void
.end method

.method public final f0()V
    .locals 3

    .line 1
    const v0, 0x7f09036e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->A:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->A:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public final g0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    const v1, 0x7f0900b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ResultActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    const v1, 0x7f090199

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ResultActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$h;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final h0()V
    .locals 2

    .line 1
    const v0, 0x7f0900d8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ResultActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final i0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "REWARD_SCAN_DIALOG_SHOWN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v0, Lkf1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    const v2, 0x7f1201f5

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1201fa

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ResultActivity$d;

    invoke-direct {v4, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lkf1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ly01;)V

    return-void
.end method

.method public final j0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->z:Landroid/content/SharedPreferences;

    const-string v1, "count_saveScan"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->z:Landroid/content/SharedPreferences;

    const-string v3, "count_cancelScan"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->z:Landroid/content/SharedPreferences;

    const-string v4, "last_rate_minutes"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {}, La91;->n()I

    move-result v3

    sub-int v2, v3, v2

    const v4, 0x15180

    if-lt v2, v4, :cond_1

    add-int/2addr v0, v1

    invoke-static {}, La91;->A()I

    move-result v1

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/google/android/play/core/review/ReviewManagerFactory;->create(Landroid/content/Context;)Lcom/google/android/play/core/review/ReviewManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/core/review/ReviewManager;->requestReviewFlow()Lcom/google/android/play/core/tasks/Task;

    move-result-object v1

    new-instance v2, Lte1;

    invoke-direct {v2, p0, v0, v3}, Lte1;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/google/android/play/core/review/ReviewManager;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/core/tasks/Task;->addOnCompleteListener(Lcom/google/android/play/core/tasks/OnCompleteListener;)Lcom/google/android/play/core/tasks/Task;

    :cond_1
    :goto_0
    return-void
.end method

.method public final k0(Z)V
    .locals 18

    .line 1
    move-object/from16 v7, p0

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v5

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v3

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v0

    new-instance v6, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    invoke-direct {v6}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;-><init>()V

    invoke-virtual {v6, v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->setExamId(Ljava/lang/Long;)V

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isPublished()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    :goto_0
    move-object v14, v4

    new-instance v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastUpdatedOn()Ljava/lang/Long;

    move-result-object v10

    iget-object v11, v7, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    new-instance v8, Lgc0;

    invoke-direct {v8}, Lgc0;-><init>()V

    invoke-virtual {v8, v1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateVersion()I

    move-result v13

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSyncImages()Z

    move-result v17

    move-object v8, v4

    move-object v9, v0

    invoke-direct/range {v8 .. v17}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;Lcom/ekodroid/omrevaluator/serializable/SharedType;Ljava/lang/String;Z)V

    invoke-virtual {v6, v4}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->setExam(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;)V

    :cond_1
    iget-object v1, v7, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v5, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllNotSyncedResults(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    goto :goto_2

    :cond_2
    sget-object v10, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    :goto_2
    move-object/from16 v16, v10

    new-instance v10, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v12

    const-wide/16 v13, 0x0

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItemString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object v11, v10

    invoke-direct/range {v11 .. v16}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;-><init>(ILjava/lang/Long;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;)V

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v6, v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->setStudentResults(Ljava/util/ArrayList;)V

    new-instance v8, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPendingReports;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    iget-object v12, v7, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    new-instance v13, Lcom/ekodroid/omrevaluator/exams/ResultActivity$a;

    move-object v0, v13

    move-object/from16 v1, p0

    move-object v11, v6

    move/from16 v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;Z)V

    invoke-direct/range {v8 .. v13}, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPendingReports;-><init>(JLcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final l0(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->b(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, La91;->t(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->U(Z)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/DeleteSyncReports;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    new-instance v3, Lcom/ekodroid/omrevaluator/exams/ResultActivity$k;

    invoke-direct {v3, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$k;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Z)V

    invoke-direct {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/DeleteSyncReports;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;Landroid/content/Context;Lzg;)V

    :goto_0
    return-void
.end method

.method public final m0(Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lye1;->g()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v2

    invoke-virtual {v0, v2}, Lye1;->n(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent()Z

    move-result v1

    invoke-virtual {v0, v1}, Lye1;->q(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0037

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f09044b

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$e;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lo4;->a(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->w:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->z:Landroid/content/SharedPreferences;

    new-instance p1, Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    const v2, 0x7f130131

    invoke-direct {p1, v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->l:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->w:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v0, "RA_EXAMID_NULL"

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->x:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->w:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v0, "RA_OC_TEMPLATE_NULL"

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->m:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const p1, 0x7f09024c

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    const v0, 0x7f0c0094

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    const v0, 0x7f0903e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->j:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f:Landroid/view/ViewGroup;

    const v0, 0x7f090403

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->k:Landroid/widget/TextView;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->j0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->g0()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    if-eqz p3, :cond_0

    array-length p2, p3

    if-lez p2, :cond_0

    const/4 p2, 0x0

    aget p2, p3, p2

    if-nez p2, :cond_0

    const/4 p2, 0x2

    if-ne p2, p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->b0()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 7

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    :cond_0
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v1}, La91;->t(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastSyncedOn()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/32 v5, 0x2bf20

    add-long/2addr v3, v5

    cmp-long v0, v1, v3

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllNonSyncResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->l0(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->d0()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->n:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "REPORT_SENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->y:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->p:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "UPDATE_REPORT_LIST"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    return-void
.end method

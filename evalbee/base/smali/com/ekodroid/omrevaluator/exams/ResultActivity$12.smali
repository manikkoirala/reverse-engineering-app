.class public Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ResultActivity;->f0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    const/4 p1, 0x1

    sub-int/2addr p3, p1

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p2

    int-to-long p4, p2

    const-wide/32 v0, 0x7f090099    # 1.053000337E-314

    cmp-long p2, p4, v0

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-virtual {p1}, Lye1;->g()I

    move-result p1

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p1, p3}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->F(Lcom/ekodroid/omrevaluator/exams/ResultActivity;ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object p4, p2, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->h:Ljava/util/ArrayList;

    invoke-static {p2, p4}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->G(Lcom/ekodroid/omrevaluator/exams/ResultActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p4

    if-ge p4, p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12$1;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12$1;-><init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;)V

    invoke-virtual {p1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object p1

    new-instance p4, Lgc0;

    invoke-direct {p4}, Lgc0;-><init>()V

    invoke-virtual {p4, p2, p1}, Lgc0;->t(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->J(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    new-instance p4, Lcom/ekodroid/omrevaluator/database/FileDataModel;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    const-string p5, "rollno_list_temp_file"

    invoke-direct {p4, p5, p1}, Lcom/ekodroid/omrevaluator/database/FileDataModel;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {p2, p4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateFile(Lcom/ekodroid/omrevaluator/database/FileDataModel;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->J(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    move-result-object p2

    const-class p4, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {p1, p2, p4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p2, "SELECTED_POSITION"

    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p2, "ROLLNO_LIST_FILE_NAME"

    invoke-virtual {p1, p2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const-string p3, "EXAM_ID"

    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$12;->a:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_2
    :goto_0
    return-void
.end method

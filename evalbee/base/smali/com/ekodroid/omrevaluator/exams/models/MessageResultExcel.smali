.class public Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public TYPE:I

.field public appVersion:I

.field public data:[[Ljava/lang/String;

.field public emailId:Ljava/lang/String;

.field public subjectName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I[[Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->emailId:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->TYPE:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->data:[[Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->subjectName:Ljava/lang/String;

    iput p5, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->appVersion:I

    return-void
.end method

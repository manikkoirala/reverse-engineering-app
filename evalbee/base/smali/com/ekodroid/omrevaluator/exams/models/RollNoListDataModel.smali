.class public Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private rank:I

.field private rollNo:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->rollNo:I

    iput p2, p0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->rank:I

    return-void
.end method


# virtual methods
.method public getRank()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->rank:I

    return v0
.end method

.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->rollNo:I

    return v0
.end method

.method public setRank(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->rank:I

    return-void
.end method

.method public setRollNo(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->rollNo:I

    return-void
.end method

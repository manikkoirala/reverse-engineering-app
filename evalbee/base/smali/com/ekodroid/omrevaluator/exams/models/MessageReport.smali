.class public Lcom/ekodroid/omrevaluator/exams/models/MessageReport;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public TYPE:I

.field public appVersion:I

.field public ccEmailID:Ljava/lang/String;

.field public data:[B

.field public emailId:Ljava/lang/String;

.field public mailSubject:Ljava/lang/String;

.field public studentName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I[BLjava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->emailId:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->ccEmailID:Ljava/lang/String;

    iput p3, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->TYPE:I

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->data:[B

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->mailSubject:Ljava/lang/String;

    iput p6, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->appVersion:I

    iput-object p7, p0, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->studentName:Ljava/lang/String;

    return-void
.end method

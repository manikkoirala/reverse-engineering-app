.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 12

    .line 1
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    check-cast p1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object v1

    invoke-static {v1}, La91;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    new-instance v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v4, v3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateVersion()I

    move-result v5

    new-instance v3, Lgc0;

    invoke-direct {v3}, Lgc0;-><init>()V

    invoke-virtual {v3, v2}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-wide v2, 0x757b12c00L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v8

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->getTeachers()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->isSyncImages()Z

    move-result v10

    move-object v3, v11

    invoke-direct/range {v3 .. v10}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;ILjava/lang/String;Ljava/lang/Long;Lcom/ekodroid/omrevaluator/serializable/SharedType;Ljava/util/ArrayList;Z)V

    new-instance p1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object v2

    const v3, 0x7f130131

    invoke-direct {p1, v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v3, 0x7f120275

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object v2

    new-instance v3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;

    invoke-direct {v3, p0, p1, v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a$a;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;Landroid/app/ProgressDialog;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)V

    invoke-direct {v0, v11, v2, v3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;Landroid/content/Context;Lzg;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->G(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    return-void

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->F(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Landroidx/appcompat/widget/SwitchCompat;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    :goto_0
    return-void
.end method

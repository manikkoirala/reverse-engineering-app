.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly3$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$b;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/ads/LoadAdError;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$b;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget v0, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->d:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->d:I

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    if-le v0, v1, :cond_0

    const/16 v0, 0x14

    invoke-static {p1, v0}, Ldj1;->a(Landroid/content/Context;I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$b;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "REWARD_SCAN_LOAD_FAIL_SKIP"

    invoke-virtual {p1, v0, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "REWARD_SCAN_LOAD_FAIL"

    invoke-virtual {p1, v0, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$b;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u$b;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v1, 0x7f12016e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    invoke-static {p1, v0, v1, v2}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void
.end method

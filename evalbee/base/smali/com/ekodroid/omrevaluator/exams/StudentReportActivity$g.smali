.class public Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->U(ZZLjava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->H(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Landroid/app/ProgressDialog;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    check-cast p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->D(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    iget-object v1, p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;->rollSet:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->markResultAsPublished(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->D(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;->cloudId:Ljava/lang/Long;

    invoke-virtual {p1, v0, p3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->markExamAsPublished(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const p3, 0x7f0800cf

    const v0, 0x7f08016d

    const v1, 0x7f12028d

    invoke-static {p1, v1, p3, v0}, La91;->G(Landroid/content/Context;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "WebPublishIndividualSuccess"

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const p3, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f12028c

    invoke-static {p1, v1, p3, v0}, La91;->G(Landroid/content/Context;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "WebPublishIndividualFail"

    :goto_0
    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

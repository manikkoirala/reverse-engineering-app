.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->o0(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/app/ProgressDialog;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->a:Landroid/app/ProgressDialog;

    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    if-eqz p1, :cond_0

    const/16 p3, 0xc8

    if-ne p2, p3, :cond_0

    new-instance p1, Lsu;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->b:Z

    invoke-direct {p1, p2, p3, v0}, Lsu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V

    goto :goto_1

    :cond_0
    const/4 p3, 0x0

    if-eqz p1, :cond_1

    const/16 p1, 0xda

    if-ne p2, p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->b:Z

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->Z(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "ExcelLimit"

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->b:Z

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->a0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "ExcelDownloadError"

    :goto_0
    invoke-virtual {p1, p2, p3}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_1
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->q1(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

.field public final synthetic c:Ljava/util/ArrayList;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

.field public final synthetic e:Z

.field public final synthetic f:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->f:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->b:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->c:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->d:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    iput-boolean p6, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_2

    check-cast p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getSyncedOn()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastUpdatedOn(Ljava/lang/Long;)V

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getSyncedOn()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastSyncedOn(Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSynced(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->b:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->d:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->f:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getRollSet()Ljava/util/Set;

    move-result-object p3

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->e:Z

    invoke-static {p1, p3, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->J(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Ljava/util/Set;Z)V

    new-instance p1, Landroid/content/Intent;

    const-string p3, "UPDATE_REPORT_LIST"

    invoke-direct {p1, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p3, "data_changed"

    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;->f:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->Q()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->C(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Landroid/widget/Toast;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->C(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->cancel()V

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget v0, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-ge v0, p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget v0, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->D(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->E(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->F(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;->a:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->B(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    :cond_1
    return-void
.end method

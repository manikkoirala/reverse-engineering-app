.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->F0(ZZLjava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 3

    .line 1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    check-cast p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;->rollSet:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->markResultAsPublished(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;->cloudId:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->markExamAsPublished(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f12027e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishResponse;->rollSet:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v1, 0x7f120290

    invoke-virtual {p3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    invoke-static {p1, p3, v0, v1}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "WebPublishSuccess"

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    const p3, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f1200d3

    invoke-static {p1, v1, p3, v0}, La91;->G(Landroid/content/Context;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "WebPublishFail"

    :goto_0
    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

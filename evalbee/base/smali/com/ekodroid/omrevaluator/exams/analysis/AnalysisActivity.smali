.class public Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity$a;
    }
.end annotation


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity$a;

.field public d:Lcom/google/android/material/tabs/TabLayout;

.field public e:Landroidx/viewpager/widget/ViewPager;

.field public f:La21;

.field public g:Lg5;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->f:La21;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->g:Lg5;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;)Lg5;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->g:Lg5;

    return-object p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;)La21;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->f:La21;

    return-object p0
.end method

.method public static C(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)La21;
    .locals 2

    .line 1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string p0, "EXAM_TYPE"

    invoke-virtual {v0, p0, p1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p0, La21;

    invoke-direct {p0}, La21;-><init>()V

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method

.method public static D(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Lg5;
    .locals 2

    .line 1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string p0, "EXAM_TYPE"

    invoke-virtual {v0, p0, p1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p0, Lg5;

    invoke-direct {p0}, Lg5;-><init>()V

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c001e

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity$a;

    invoke-virtual {p0}, Landroidx/fragment/app/e;->getSupportFragmentManager()Landroidx/fragment/app/k;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;Landroidx/fragment/app/k;)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->c:Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity$a;

    const p1, 0x7f090111

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/viewpager/widget/ViewPager;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->e:Landroidx/viewpager/widget/ViewPager;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->c:Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity$a;

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Lu21;)V

    const p1, 0x7f09037d

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/tabs/TabLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->d:Lcom/google/android/material/tabs/TabLayout;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->e:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, v0}, Lcom/google/android/material/tabs/TabLayout;->setupWithViewPager(Landroidx/viewpager/widget/ViewPager;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_TYPE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->C(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)La21;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->f:La21;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->i:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->D(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Lg5;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;->g:Lg5;

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

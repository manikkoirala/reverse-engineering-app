.class public Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->O()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->i:Lf3;

    invoke-virtual {v0}, Lf3;->a()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p2

    const v1, 0x7f0901c4

    const/4 v2, 0x1

    if-eq p2, v1, :cond_2

    const v1, 0x7f0901c6

    const/4 v3, 0x0

    if-eq p2, v1, :cond_0

    return v3

    :cond_0
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result p2

    if-ne p2, v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v1, p2, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc8;

    invoke-virtual {v0}, Lc8;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->J(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    return v2

    :cond_2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    sub-int/2addr v1, v2

    :goto_0
    if-ltz v1, :cond_4

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lc8;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {v0, p2}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->I(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/util/ArrayList;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->i:Lf3;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return v2
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-virtual {v0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->l:Landroid/view/ActionMode;

    iget-object p1, p2, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->g:Landroid/view/ViewGroup;

    const p2, 0x7f0901f6

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->i:Lf3;

    invoke-virtual {v0}, Lf3;->b()V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->l:Landroid/view/ActionMode;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->g:Landroid/view/ViewGroup;

    const v0, 0x7f0901f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 1

    const/4 p3, 0x1

    sub-int/2addr p2, p3

    iget-object p4, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p4, p4, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->f:Landroid/widget/ListView;

    invoke-virtual {p4}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result p4

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " Selected"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p1, p5}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object p1

    const p5, 0x7f0901c6

    invoke-interface {p1, p5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    if-ne p4, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-interface {p1, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->i:Lf3;

    invoke-virtual {p1, p2}, Lf3;->d(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->i:Lf3;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

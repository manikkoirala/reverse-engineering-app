.class public Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

.field public d:Ljava/util/ArrayList;

.field public e:Ljava/util/ArrayList;

.field public f:Landroid/widget/ListView;

.field public g:Landroid/view/ViewGroup;

.field public h:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public i:Lf3;

.field public j:Landroid/content/BroadcastReceiver;

.field public k:Z

.field public l:Landroid/view/ActionMode;

.field public m:Landroid/widget/Spinner;

.field public n:Ljava/util/ArrayList;

.field public p:Landroid/widget/ArrayAdapter;

.field public q:Ljava/lang/String;

.field public t:Lzx;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->k:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->l:Landroid/view/ActionMode;

    const-string v0, "All"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->q:Ljava/lang/String;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)Lzx;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->t:Lzx;

    return-object p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Lzx;)Lzx;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->t:Lzx;

    return-object p1
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->W()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;I)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->R(Ljava/lang/String;I)Z

    move-result p0

    return p0
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->L(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->Y()V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->S()V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->N(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->M(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->U(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->q:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public final L(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 17

    .line 1
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    new-instance v15, Lc8;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getExamDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getTemplateId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getArchivedOn()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getSizeKb()I

    move-result v8

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getVersion()I

    move-result v9

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getAttendees()I

    move-result v10

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getNoOfQue()I

    move-result v11

    const/4 v12, 0x0

    move-object/from16 v1, p0

    iget-boolean v14, v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->k:Z

    const/16 v16, 0x0

    move-object v2, v15

    move/from16 v13, p3

    move-object/from16 p2, v0

    move-object v0, v15

    move/from16 v15, v16

    invoke-direct/range {v2 .. v15}, Lc8;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIZZZI)V

    move-object/from16 v2, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p2

    goto :goto_0

    :cond_0
    move-object/from16 v1, p0

    return-void
.end method

.method public final M(Ljava/util/ArrayList;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$j;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$j;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    const v2, 0x7f12009a

    const v3, 0x7f12018c

    const v4, 0x7f120097

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800e1

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final N(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p2, "ARCHIVE_ID"

    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "CLOUD_SYNCED"

    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->f:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$i;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    return-void
.end method

.method public final P()V
    .locals 3

    .line 1
    const v0, 0x7f09036e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->h:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->h:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    const v0, 0x7f090250

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$h;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0c0093

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->g:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->g:Landroid/view/ViewGroup;

    const v1, 0x7f090333

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->m:Landroid/widget/Spinner;

    return-void
.end method

.method public final Q()V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->j:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public final R(Ljava/lang/String;I)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 p1, 0x5

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x1

    return p1

    :catch_0
    return v0
.end method

.method public final S()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->h:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->T()V

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchiveLocalDataModels()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v3}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->L(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedPendingDelete;->getPendingArchiveIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lc8;

    invoke-virtual {v4}, Lc8;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    const/4 v6, -0x1

    if-le v5, v6, :cond_1

    invoke-virtual {v4, v1}, Lc8;->k(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->h:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->W()V

    :goto_1
    return-void
.end method

.method public final T()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/FirebaseFirestore;->f()Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    const-string v2, "ArchivedExams"

    invoke-virtual {v0, v2}, Lcom/google/firebase/firestore/FirebaseFirestore;->a(Ljava/lang/String;)Lih;

    move-result-object v0

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lih;->g(Ljava/lang/String;)Lcom/google/firebase/firestore/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/a;->h()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final U(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Lbx0;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-direct {v0, v1, p1}, Lbx0;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public final V()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lc8;

    invoke-virtual {v1}, Lc8;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public final W()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const-string v2, "ARCHIVE_COUNT"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->k:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->V()V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->X()V

    new-instance v0, Lf3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-direct {v0, v1, v2}, Lf3;-><init>(Ljava/util/ArrayList;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->i:Lf3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    return-void
.end method

.method public final X()V
    .locals 6

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->DATE:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->t:Lzx;

    const v2, 0x7f0901de

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    iget-boolean v4, v1, Lzx;->b:Z

    iget-object v1, v1, Lzx;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->g:Landroid/view/ViewGroup;

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f090420

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->t:Lzx;

    iget-object v5, v5, Lzx;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    if-ne v5, v0, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120091

    :goto_0
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    if-ne v5, v0, :cond_1

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120239

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f12012e

    goto :goto_0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->t:Lzx;

    iget-boolean v0, v0, Lzx;->b:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120042

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f1200a3

    :goto_2
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    move v3, v4

    goto :goto_3

    :cond_3
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    new-instance v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;

    invoke-direct {v1}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;->d(Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;Ljava/util/ArrayList;Z)V

    return-void
.end method

.method public final Y()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->n:Ljava/util/ArrayList;

    const-string v1, "All"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc8;

    invoke-virtual {v2}, Lc8;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    const v2, 0x7f0c00f0

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->n:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->p:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->m:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->n:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x0

    :cond_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->m:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final Z()V
    .locals 2

    .line 1
    const v0, 0x7f0900b4

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090198

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a0()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->Y()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->m:Landroid/widget/Spinner;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final b0()V
    .locals 2

    .line 1
    const v0, 0x7f090438

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c001f

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->b0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->P()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->O()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->Z()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->a0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->Q()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, La91;->c(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->k:Z

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->l:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->l:Landroid/view/ActionMode;

    :cond_0
    const v0, 0x7f0901f6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->W()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->S()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->j:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UPDATE_ARCHIVE_EXAM_LIST"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    return-void
.end method

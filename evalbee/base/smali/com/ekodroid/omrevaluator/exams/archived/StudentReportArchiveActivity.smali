.class public Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/content/SharedPreferences;

.field public f:Ljava/util/ArrayList;

.field public g:I

.field public h:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public i:Landroid/widget/TextView;

.field public j:Ljava/lang/String;

.field public k:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public p:Landroid/widget/ImageButton;

.field public q:Landroid/widget/ImageButton;

.field public t:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;)Landroid/widget/Toast;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->t:Landroid/widget/Toast;

    return-object p0
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->K()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->J()V

    return-void
.end method


# virtual methods
.method public final A(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Landroid/widget/LinearLayout;)V
    .locals 7

    .line 1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->E(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/LayoutInflater;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Li5;

    const v1, 0x7f0c009a

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v2, 0x7f0903ef

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0903e0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0903c1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0903b9

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v0}, Li5;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Li5;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Li5;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Li5;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final E(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v5

    invoke-static {v2, v5}, Le5;->r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v7

    invoke-static {v7, v8}, Lve1;->o(D)D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v6

    invoke-static {p2, v6}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v6

    invoke-static {v4, v6}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    new-instance v6, Li5;

    invoke-direct {v6, v3, v5, v4, v2}, Li5;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final F()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld8;

    invoke-virtual {v0}, Ld8;->c()Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRank()I

    move-result v0

    return v0
.end method

.method public final G()Landroid/widget/TextView;
    .locals 4

    .line 1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x5a

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-static {v1, v2}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    const/16 v2, 0x8

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    const v1, 0x7f130235

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06005c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    return-object v0
.end method

.method public final H()V
    .locals 2

    .line 1
    const v0, 0x7f09019e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->p:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final I()V
    .locals 2

    .line 1
    const v0, 0x7f0901a0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->q:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final J()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->q:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->p:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    const/4 v2, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->i:Landroid/widget/TextView;

    const v1, 0x7f1200ee

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v1

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->p:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->i:Landroid/widget/TextView;

    const v1, 0x7f120128

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->i:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f12028a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public final K()V
    .locals 33

    .line 1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    const/4 v4, 0x0

    if-le v1, v2, :cond_0

    iput v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    :cond_0
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    iget v2, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld8;

    invoke-virtual {v1}, Ld8;->c()Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v2

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    iget v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ld8;

    invoke-virtual {v5}, Ld8;->a()Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v5

    if-nez v5, :cond_1

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->h:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v7, "SRA_RESULT_NULL_ARCHIVE"

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->j:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    iget v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    add-int/2addr v6, v3

    iput v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->K()V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lv5;->n()Lt1;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual/range {p0 .. p0}, Lv5;->n()Lt1;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lt1;->t(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v6

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->k:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v2

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v5, v6}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v6

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v5, v7}, Lve1;->k(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v7

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v5, v8}, Lve1;->c(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I

    move-result-object v8

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v5, v9}, Lve1;->j(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I

    move-result-object v9

    invoke-static {v8}, Lve1;->h([[I)[I

    move-result-object v10

    invoke-static {v9}, Lve1;->h([[I)[I

    move-result-object v11

    iget-object v12, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v12}, Lve1;->r(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v12

    invoke-static {v7, v12}, Lve1;->n([[D[[D)[[D

    move-result-object v13

    invoke-static {v12}, Lve1;->g([[D)[D

    move-result-object v12

    invoke-static {v6, v12}, Lve1;->m([D[D)[D

    move-result-object v14

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v3

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    mul-double v16, v16, v3

    invoke-static {v12}, Lve1;->d([D)D

    move-result-wide v18

    div-double v16, v16, v18

    iget-object v12, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v12

    if-eqz v12, :cond_3

    iget-object v12, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v12

    invoke-static {v3, v4, v12}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    :cond_3
    const/4 v12, 0x0

    :goto_0
    const v15, 0x7f0903df

    invoke-virtual {v0, v15}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-wide/from16 v20, v3

    const v3, 0x7f0903da

    invoke-virtual {v0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0903db

    invoke-virtual {v0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v22, v9

    const v9, 0x7f0903dc

    invoke-virtual {v0, v9}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    move-object/from16 v23, v8

    const v8, 0x7f0903dd

    invoke-virtual {v0, v8}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    move-object/from16 v24, v13

    const v13, 0x7f0903de

    invoke-virtual {v0, v13}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v25, v7

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamNameString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRankString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getGradeString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getClassString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNameString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f09040c

    invoke-virtual {v0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090407

    invoke-virtual {v0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v7, 0x7f090409

    invoke-virtual {v0, v7}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f09040a

    invoke-virtual {v0, v8}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v9, 0x7f09040e

    invoke-virtual {v0, v9}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    const v13, 0x7f09040b

    invoke-virtual {v0, v13}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const v15, 0x7f09037c

    invoke-virtual {v0, v15}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TableLayout;

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v26

    move-object/from16 v18, v10

    move-object/from16 v27, v11

    const/4 v11, 0x1

    add-int/lit8 v10, v26, -0x1

    invoke-virtual {v15, v11, v10}, Landroid/view/ViewGroup;->removeViews(II)V

    const v11, 0x7f0901da

    invoke-virtual {v0, v11}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    const v10, 0x7f0901db

    invoke-virtual {v0, v10}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    const v10, 0x7f0901fa

    invoke-virtual {v0, v10}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->F()I

    move-result v28

    move-object/from16 v29, v11

    const/16 v11, 0x8

    if-lez v28, :cond_4

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRank()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    const v1, 0x7f090214

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    if-eqz v12, :cond_5

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    const v1, 0x7f0901f5

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->k:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_6
    const-string v1, "---"

    :goto_3
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->k:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_8

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v3

    sub-int/2addr v3, v2

    if-gez v3, :cond_7

    const/4 v3, 0x0

    :cond_7
    aget-object v1, v1, v3

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_8
    const v1, 0x7f0901ee

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    const v1, 0x7f09038c

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectAnswerString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f09038d

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getIncorrectAnswerString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f090390

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSubjectString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f09038e

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMarksString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f09038f

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getPercentageString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    const/16 v3, 0x20

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-static {v3, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    const/4 v4, -0x2

    invoke-direct {v1, v4, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    const/4 v2, 0x1

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/4 v3, 0x0

    :goto_5
    array-length v4, v6

    const-string v7, "%"

    const-string v8, ""

    if-ge v3, v4, :cond_a

    new-instance v4, Landroid/widget/TableRow;

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-direct {v4, v9}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v9

    const/4 v12, 0x0

    invoke-virtual {v9, v12, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v13, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v13

    aget-object v13, v13, v3

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13, v12, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v30, v6, v3

    move/from16 v26, v3

    invoke-static/range {v30 .. v31}, Lve1;->o(D)D

    move-result-wide v2

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v9, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v13, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v9, 0x1

    invoke-virtual {v2, v3, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v30, v14, v26

    move-object v13, v10

    invoke-static/range {v30 .. v31}, Lve1;->o(D)D

    move-result-wide v9

    invoke-virtual {v12, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v2

    const/4 v9, 0x1

    invoke-virtual {v2, v3, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v12, v18, v26

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v3, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v10, v27, v26

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v15, v4}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    const/4 v2, 0x0

    :goto_6
    aget-object v4, v25, v26

    array-length v4, v4

    if-ge v2, v4, :cond_9

    new-instance v4, Landroid/widget/TableRow;

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-direct {v4, v9}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v9

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v10

    aget-object v10, v10, v26

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v10

    aget-object v10, v10, v2

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v10

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v28, v25, v26

    aget-wide v30, v28, v2

    move-object/from16 v32, v4

    invoke-static/range {v30 .. v31}, Lve1;->o(D)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v12, v24, v26

    aget-wide v30, v12, v2

    invoke-static/range {v30 .. v31}, Lve1;->o(D)D

    move-result-wide v11

    invoke-virtual {v4, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v4

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v12, v23, v26

    aget v12, v12, v2

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v30, v22, v26

    move-object/from16 v31, v6

    aget v6, v30, v2

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v6, v32

    invoke-virtual {v6, v9, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v10, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v11, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v15, v6}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v6, v31

    const/16 v11, 0x8

    goto/16 :goto_6

    :cond_9
    move-object/from16 v31, v6

    add-int/lit8 v3, v26, 0x1

    move-object v10, v13

    const/4 v2, 0x1

    const/16 v11, 0x8

    goto/16 :goto_5

    :cond_a
    move-object v13, v10

    new-instance v2, Landroid/widget/TableRow;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;

    invoke-direct {v2, v3}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTotalMarksString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v21}, Lve1;->o(D)D

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v10

    invoke-virtual {v10, v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object v12, v5

    invoke-static/range {v16 .. v17}, Lve1;->o(D)D

    move-result-wide v4

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v18 .. v18}, Lve1;->e([I)I

    move-result v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->G()Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v27 .. v27}, Lve1;->e([I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v9, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v10, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v7, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v15, v2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    const/16 v1, 0x8

    invoke-virtual {v13, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-object/from16 v11, v29

    invoke-virtual {v0, v12, v1, v11}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Landroid/widget/LinearLayout;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0041

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->h:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->e:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SELECTED_POSITION"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->g:I

    sget-object p1, Le8;->b:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->k:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    sget-object v0, Le8;->a:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    sget-object v0, Le8;->c:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->f:Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    const p1, 0x7f090225

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->d:Landroid/widget/LinearLayout;

    const p1, 0x7f0903f6

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->H()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->I()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->j:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->J()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/StudentReportArchiveActivity;->K()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

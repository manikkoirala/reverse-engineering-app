.class public Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->Q()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const-string p1, "file_id"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "type"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "progress"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lc8;

    invoke-virtual {v3}, Lc8;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_5

    const-string p1, "DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x1

    const/16 v1, 0x5f

    if-eqz p1, :cond_3

    if-le p2, v1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-virtual {v3, v0}, Lc8;->k(Z)V

    goto :goto_1

    :cond_3
    if-le p2, v1, :cond_4

    invoke-virtual {v3, v0}, Lc8;->m(Z)V

    goto :goto_2

    :cond_4
    invoke-virtual {v3, v2}, Lc8;->m(Z)V

    :goto_1
    invoke-virtual {v3, p2}, Lc8;->l(I)V

    :goto_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$e;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->C(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    :cond_5
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$p;
    }
.end annotation


# instance fields
.field public c:Ljava/util/ArrayList;

.field public d:Landroid/widget/ListView;

.field public e:Landroid/view/ViewGroup;

.field public f:Le3;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/widget/TextView;

.field public i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public j:Lud1;

.field public k:D

.field public l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public m:Landroid/app/ProgressDialog;

.field public n:Ljava/lang/String;

.field public p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

.field public q:Ljava/lang/String;

.field public t:Z

.field public v:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lv5;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const-string v1, ""

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->n:Ljava/lang/String;

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->q:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->t:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->b0()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    return-object p0
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->Q()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->P()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->q:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->X(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->N(Z)V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->d0(Z)V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;[[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->U([[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final J(Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/lang/String;)V
    .locals 5

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p3, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void

    :cond_1
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v1, p3, :cond_3

    new-array v1, p4, [Ljava/lang/String;

    move v2, v0

    :goto_0
    const-string v3, ""

    if-ge v2, p4, :cond_2

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    aput-object p5, v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual/range {p0 .. p5}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->J(Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/lang/String;)V

    :cond_3
    return-void
.end method

.method public final K(Ljava/util/ArrayList;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    new-array v2, p3, [Ljava/lang/String;

    move v3, v0

    :goto_1
    const-string v4, ""

    if-ge v3, p3, :cond_0

    aput-object v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    aput-object p4, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final L(Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v0}, Lye1;->g()I

    move-result v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lye1;->p(Z)V

    goto :goto_0

    :cond_0
    const-string v1, " - - - "

    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final M()Z
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f1201bd

    :goto_0
    const v7, 0x7f120060

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return v1

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x7f1201bb

    goto :goto_0
.end method

.method public final N(Z)V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120183

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Lma0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    new-instance v3, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;

    invoke-direct {v3, p0, v0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Landroid/app/ProgressDialog;Z)V

    invoke-direct {v1, v2, v3}, Lma0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final O(Ljava/lang/String;)V
    .locals 6

    .line 1
    invoke-static {}, Lo30;->f()Lo30;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lok;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    sget-object v4, Lok;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ".exm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Lo30;->n()Ljq1;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljq1;->a(Ljava/lang/String;)Ljq1;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljq1;->g(Ljava/io/File;)Lx00;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->v:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-virtual {v0, v1}, Lzq1;->p(Lcom/google/android/gms/tasks/OnFailureListener;)Lzq1;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$o;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$o;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lzq1;->u(Lcom/google/android/gms/tasks/OnSuccessListener;)Lzq1;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$n;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$n;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-virtual {p1, v0}, Lzq1;->s(Lo11;)Lzq1;

    :cond_2
    :goto_0
    return-void
.end method

.method public final P()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    new-instance v0, Ltu;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-direct {v0, v1, v2, v3}, Ltu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "DownloadKey_Archived_Csv"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f120038

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.method public final Q()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    new-instance v0, Luu;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-direct {v0, v1, v2, v3}, Luu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "DownloadKey_Archived"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f120038

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.method public final R()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->M()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e0(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final S()Ljava/util/ArrayList;
    .locals 10

    .line 1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld8;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;-><init>()V

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v7, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->a(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudents(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v0, 0x0

    move v9, v0

    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_1

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld8;

    invoke-virtual {v0}, Ld8;->b()Lye1;

    move-result-object v0

    invoke-virtual {v0}, Lye1;->g()I

    move-result v3

    const/4 v4, 0x4

    const-string v5, ""

    move-object v0, p0

    move-object v1, v6

    move-object v2, v8

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->J(Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/lang/String;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x4

    const-string v1, ""

    invoke-virtual {p0, v6, v8, v0, v1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->K(Ljava/util/ArrayList;Ljava/util/ArrayList;ILjava/lang/String;)V

    return-object v6
.end method

.method public final T(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    invoke-direct {v1}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;-><init>()V

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p2, v3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->j(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V

    invoke-virtual {v1, p1, v3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->k(Ljava/util/ArrayList;Z)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    new-instance v2, Ld8;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lye1;

    invoke-direct {v2, v3, v4}, Ld8;-><init>(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;Lye1;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final U([[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .line 1
    const/4 p4, 0x0

    invoke-virtual {p0, p4}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->h0(Lg61;)V

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[^a-zA-Z0-9_-]"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[^a-zA-Z0-9_]"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_Result"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 p4, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget p4, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    move v6, p4

    new-instance p4, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;

    const/4 v3, 0x0

    move-object v1, p4

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;-><init>(Ljava/lang/String;I[[Ljava/lang/String;Ljava/lang/String;I)V

    new-instance p1, Ln52;

    invoke-static {}, La91;->w()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1}, Ln52;->b()Lee1;

    move-result-object p1

    new-instance p2, Lg61;

    new-instance v6, Lcom/ekodroid/omrevaluator/serializable/ResultData;

    iget-object v1, p4, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->emailId:Ljava/lang/String;

    iget-object v2, p4, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->data:[[Ljava/lang/String;

    iget-object v3, p4, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->subjectName:Ljava/lang/String;

    iget v4, p4, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->appVersion:I

    move-object v0, v6

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/serializable/ResultData;-><init>(Ljava/lang/String;[[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->n:Ljava/lang/String;

    new-instance p4, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;

    invoke-direct {p4, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-direct {p2, v6, p1, p3, p4}, Lg61;-><init>(Lcom/ekodroid/omrevaluator/serializable/ResultData;Lee1;Ljava/lang/String;Lzg;)V

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->h0(Lg61;)V

    return-void
.end method

.method public final V(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 16

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-object/from16 v3, p1

    if-eqz v2, :cond_0

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v4

    invoke-static {v4}, Lve1;->p(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v10

    invoke-static {v4}, Lve1;->q(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v11

    invoke-static {v4}, Lve1;->s(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v12

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v4

    invoke-static {v7, v8, v4}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v9

    new-instance v4, Lye1;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v6

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent()Z

    move-result v13

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced()Z

    move-result v14

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v15

    move-object v5, v4

    invoke-direct/range {v5 .. v15}, Lye1;-><init>(IDLjava/lang/String;IIIZZZ)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object/from16 v3, p1

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRankingMethod()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lve1;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->L(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public final W()V
    .locals 3

    .line 1
    const v0, 0x7f090245

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->d:Landroid/widget/ListView;

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v2, 0x7f130131

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    const v0, 0x7f09036e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->v:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->v:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$k;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$l;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0c0094

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0903e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    const v1, 0x7f090403

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->h:Landroid/widget/TextView;

    return-void
.end method

.method public final X(Ljava/lang/String;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->v:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".exm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lok;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    sget-object v3, Lok;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->Y(Ljava/io/File;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->t:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->O(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$m;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$m;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v3, 0x7f1200ca

    const v4, 0x7f120176

    const v5, 0x7f120060

    const/4 v6, 0x0

    const/4 v7, 0x0

    const v8, 0x7f0800d2

    invoke-static/range {v1 .. v8}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    :goto_0
    return-void
.end method

.method public final Y(Ljava/io/File;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    const v1, 0x7f08016e

    const v2, 0x7f0800bd

    const v3, 0x7f120291

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    move-object/from16 v5, p1

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v4}, Lcom/google/android/gms/common/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v4

    invoke-static {v4}, Lyx;->a([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {v4, v3, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v5

    iput-object v5, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getResults()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v6

    iput-object v6, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;

    new-instance v15, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;->getRollno()I

    move-result v10

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v11

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;->getResultItem2()Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v7, 0x0

    move-object v8, v15

    move-object v1, v15

    move v15, v7

    invoke-direct/range {v8 .. v15}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;ZZ)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v1, 0x7f08016e

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->V(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->T(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v1}, Lve1;->l(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)D

    move-result-wide v4

    iput-wide v4, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->k:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v4, 0x7f08016e

    invoke-static {v1, v3, v2, v4}, La91;->G(Landroid/content/Context;III)V

    :goto_1
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->v:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->b0()V

    return-void
.end method

.method public final Z()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Le8;->b(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/exams/analysis/AnalysisActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "EXAM_TYPE"

    const-string v2, "ARCHIVED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final a0()V
    .locals 6

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->RANK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->j:Lud1;

    const v2, 0x7f0901de

    if-eqz v1, :cond_4

    iget-boolean v3, v1, Lud1;->b:Z

    iget-object v1, v1, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    const v4, 0x7f090420

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->j:Lud1;

    iget-object v5, v5, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v5, v0, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120286

    :goto_0
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v5, v0, :cond_1

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f12014c

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v5, v0, :cond_2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120239

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f12029b

    goto :goto_0

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->j:Lud1;

    iget-boolean v0, v0, Lud1;->b:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f120042

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f1200a3

    :goto_2
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x1

    :goto_3
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    new-instance v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    invoke-direct {v1}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->a(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V

    :cond_5
    return-void
.end method

.method public final b0()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->a0()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f1202ec

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " : 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f12031c

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->k:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    const-string v1, ""

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Le3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v2, v3}, Le3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->f:Le3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->h:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->h:Landroid/widget/TextView;

    const-string v2, "---"

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->g:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->k:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final c0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0900b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->e:Landroid/view/ViewGroup;

    const v1, 0x7f090199

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$h;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final d0(Z)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$f;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v2, 0x7f1200ca

    const v3, 0x7f12019e

    const v4, 0x7f12029a

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final e0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance p1, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v0, 0x7f130151

    invoke-direct {p1, p2, v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string p2, "layout_inflater"

    invoke-virtual {p0, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/LayoutInflater;

    const v0, 0x7f0c0081

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v0, 0x7f1200a5

    invoke-virtual {p1, v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v0, 0x7f0900ff

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/CheckBox;

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$b;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Landroid/widget/CheckBox;)V

    const p2, 0x7f12023f

    invoke-virtual {p1, p2, v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final f0()V
    .locals 6

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c008a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090241

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->S()Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f12001b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v3, Lb3;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-direct {v3, v4, v2}, Lb3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$j;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$j;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V

    const v2, 0x7f120060

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final g0()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0067

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202bf

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0902df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$i;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$i;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Landroid/widget/RadioButton;)V

    const v1, 0x7f12023f

    invoke-virtual {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final h0(Lg61;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    const v1, 0x7f1202d2

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    const v1, 0x7f120053

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$e;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Lg61;)V

    const/4 p1, -0x2

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final i0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Le8;->b(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "EXAM_TYPE"

    const-string v2, "ARCHIVED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0020

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090439

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->n:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ARCHIVE_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "CLOUD_SYNCED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->t:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->W()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->q:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->X(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->c0()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f090041

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lv5;->onDestroy()V

    invoke-static {}, Le8;->c()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    :sswitch_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->i0()V

    return v1

    :sswitch_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->Z()V

    return v1

    :sswitch_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->f0()V

    return v1

    :sswitch_3
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->g0()V

    return v1

    :sswitch_4
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->R()V

    return v1

    :sswitch_5
    new-instance p1, Lbx0;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->p:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->q:Ljava/lang/String;

    invoke-direct {p1, v0, v2}, Lbx0;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f09003d -> :sswitch_5
        0x7f090041 -> :sswitch_4
        0x7f090042 -> :sswitch_3
        0x7f090056 -> :sswitch_2
        0x7f090057 -> :sswitch_1
        0x7f090059 -> :sswitch_0
    .end sparse-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->b0()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

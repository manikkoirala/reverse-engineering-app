.class public Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->U([[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 8

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    const/4 p1, 0x0

    if-eqz p3, :cond_0

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object p2

    const p3, 0x7f0800cf

    const v0, 0x7f08016d

    const v1, 0x7f1200b4

    invoke-static {p2, v1, p3, v0}, La91;->G(Landroid/content/Context;III)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string p3, "ExcelEmailSuccessArchive"

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    const/16 p3, 0xda

    if-ne p2, p3, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d$a;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;)V

    const/4 v2, 0x0

    const v3, 0x7f1201a3

    const v4, 0x7f120050

    const v5, 0x7f120053

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object p2

    const p3, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f1200b3

    invoke-static {p2, v1, p3, v0}, La91;->G(Landroid/content/Context;III)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$d;->a:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string p3, "ExcelEmailErrorArchive"

    :goto_0
    invoke-virtual {p2, p3, p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_1
    return-void
.end method

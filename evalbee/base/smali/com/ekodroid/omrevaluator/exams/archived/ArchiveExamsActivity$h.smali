.class public Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->P()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$h;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    add-int/lit8 p3, p3, -0x1

    const p1, 0x7f08008c

    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$h;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lc8;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$h;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-virtual {p1}, Lc8;->g()Ljava/lang/String;

    move-result-object p3

    new-instance p4, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1}, Lc8;->d()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p1}, Lc8;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lc8;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p4, p5, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lc8;->j()Z

    move-result p1

    invoke-static {p2, p3, p4, p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->H(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V

    return-void
.end method

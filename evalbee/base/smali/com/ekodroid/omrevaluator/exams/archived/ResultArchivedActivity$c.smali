.class public Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->N(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Landroid/app/ProgressDialog;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->a:Landroid/app/ProgressDialog;

    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 8

    .line 1
    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    const/4 p3, 0x0

    if-eqz p1, :cond_0

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_0

    new-instance p1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$p;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-direct {p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$p;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$g;)V

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$p;->a:Z

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->b:Z

    iput-boolean p2, p1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$p;->d:Z

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_1

    const/16 p1, 0xda

    if-ne p2, p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c$a;-><init>(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;)V

    const v2, 0x7f120051

    const v3, 0x7f1201a3

    const v4, 0x7f120050

    const v5, 0x7f120053

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "ExcelLimit"

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->b:Z

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->H(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity$c;->c:Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;->B(Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;)Lcom/ekodroid/omrevaluator/exams/archived/ResultArchivedActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "ExcelDownloadError"

    :goto_0
    invoke-virtual {p1, p2, p3}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_1
    return-void
.end method

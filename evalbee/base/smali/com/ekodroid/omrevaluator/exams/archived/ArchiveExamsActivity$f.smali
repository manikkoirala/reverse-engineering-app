.class public Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->T()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 6

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/DocumentSnapshot;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/DocumentSnapshot;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/firebase/firestore/DocumentSnapshot;->d()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    new-instance v3, Lgc0;

    invoke-direct {v3}, Lgc0;-><init>()V

    new-instance v4, Lgc0;

    invoke-direct {v4}, Lgc0;-><init>()V

    invoke-virtual {v4, v2}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-class v4, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    invoke-virtual {v3, v2, v4}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getArchivedOn()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x16d

    invoke-static {v3, v4, v5}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->D(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getTemplateId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedPendingDelete;->addArchiveId(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedPendingDelete;->getPendingArchiveIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->n(Landroid/content/Context;)V

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v2, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-static {p1, v2, v0, v1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->E(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    :cond_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchiveLocalDataModels()Ljava/util/ArrayList;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->E(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->c:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedPendingDelete;->getPendingArchiveIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc8;

    invoke-virtual {v2}, Lc8;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_4

    invoke-virtual {v2, v1}, Lc8;->k(Z)V

    goto :goto_1

    :cond_5
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->h:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p1, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->F(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity$f;->a:Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;->C(Lcom/ekodroid/omrevaluator/exams/archived/ArchiveExamsActivity;)V

    return-void
.end method

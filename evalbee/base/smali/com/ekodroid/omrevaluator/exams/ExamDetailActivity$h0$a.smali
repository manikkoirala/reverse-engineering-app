.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/OnUserEarnedRewardListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->a(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUserEarnedReward(Lcom/google/android/gms/ads/rewarded/RewardItem;)V
    .locals 3

    new-instance p1, Lsu;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-boolean v1, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->a:Z

    invoke-direct {p1, v0, v2, v1}, Lsu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "REWARD_EXCEL_GRANTED"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0$a;->a:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0800cf

    const v2, 0x7f08016d

    invoke-static {p1, v0, v1, v2}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void
.end method

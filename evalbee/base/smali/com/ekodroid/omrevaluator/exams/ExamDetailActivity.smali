.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$s0;
    }
.end annotation


# instance fields
.field public final c:I

.field public d:I

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public f:Landroid/widget/LinearLayout;

.field public g:Landroid/app/ProgressDialog;

.field public h:Ljava/lang/String;

.field public i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public j:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public k:I

.field public l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

.field public m:Landroid/content/SharedPreferences;

.field public n:Landroidx/appcompat/widget/SwitchCompat;

.field public p:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public q:J

.field public t:Landroidx/appcompat/widget/Toolbar;

.field public v:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h:Ljava/lang/String;

    const/16 v0, 0x3e7

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k:I

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/google/android/play/core/review/ReviewManager;ILcom/google/android/play/core/tasks/Task;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->B0(Lcom/google/android/play/core/review/ReviewManager;ILcom/google/android/play/core/tasks/Task;)V

    return-void
.end method

.method private synthetic A0(ILcom/google/android/play/core/tasks/Task;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_rate_minutes"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p2}, Lcom/google/android/play/core/tasks/Task;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "IN_APP_REVIEW_SUCCESS"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;ILcom/google/android/play/core/tasks/Task;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->A0(ILcom/google/android/play/core/tasks/Task;)V

    return-void
.end method

.method private synthetic B0(Lcom/google/android/play/core/review/ReviewManager;ILcom/google/android/play/core/tasks/Task;)V
    .locals 1

    .line 1
    invoke-virtual {p3}, Lcom/google/android/play/core/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/google/android/play/core/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/google/android/play/core/review/ReviewInfo;

    invoke-interface {p1, p0, p3}, Lcom/google/android/play/core/review/ReviewManager;->launchReviewFlow(Landroid/app/Activity;Lcom/google/android/play/core/review/ReviewInfo;)Lcom/google/android/play/core/tasks/Task;

    move-result-object p1

    new-instance p3, Lxx;

    invoke-direct {p3, p0, p2}, Lxx;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;I)V

    invoke-virtual {p1, p3}, Lcom/google/android/play/core/tasks/Task;->addOnCompleteListener(Lcom/google/android/play/core/tasks/OnCompleteListener;)Lcom/google/android/play/core/tasks/Task;

    :cond_0
    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k0()Z

    move-result p0

    return p0
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->E0()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->G0()V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Landroidx/appcompat/widget/SwitchCompat;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->n:Landroidx/appcompat/widget/SwitchCompat;

    return-object p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->b1()V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->r0(Z)V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->q1(Z)V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Ljava/util/Set;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->n0(Ljava/util/Set;Z)V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->u0()V

    return-void
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->e1()V

    return-void
.end method

.method public static synthetic M(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->d1()V

    return-void
.end method

.method public static synthetic N(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Landroid/content/SharedPreferences;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method public static synthetic O(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->w0()V

    return-void
.end method

.method public static synthetic P(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;ZZLjava/lang/Long;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->F0(ZZLjava/lang/Long;)V

    return-void
.end method

.method public static synthetic Q(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i1()V

    return-void
.end method

.method public static synthetic R(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->t0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    return-void
.end method

.method public static synthetic S(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->x0()V

    return-void
.end method

.method public static synthetic T(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->q0()V

    return-void
.end method

.method public static synthetic U(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->j1(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;)V

    return-void
.end method

.method public static synthetic V(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->y0()V

    return-void
.end method

.method public static synthetic W(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f1(Z)V

    return-void
.end method

.method public static synthetic X(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->o0(Z)V

    return-void
.end method

.method public static synthetic Y(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m0()Z

    move-result p0

    return p0
.end method

.method public static synthetic Z(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->Z0(Z)V

    return-void
.end method

.method public static synthetic a0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->Y0(Z)V

    return-void
.end method

.method public static synthetic b0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h1()V

    return-void
.end method

.method public static synthetic c0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->D0()V

    return-void
.end method

.method public static synthetic d0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m1(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    return-void
.end method

.method public static synthetic e0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l1()V

    return-void
.end method

.method public static synthetic f0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->n1()V

    return-void
.end method

.method public static synthetic g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    return-object p0
.end method

.method public static synthetic h0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->r1(Z)V

    return-void
.end method


# virtual methods
.method public final C0()V
    .locals 6

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1201a7

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    new-instance v2, Lua0;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v5, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$x;

    invoke-direct {v5, p0, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$x;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/app/ProgressDialog;)V

    invoke-direct {v2, v3, v4, v1, v5}, Lua0;-><init>(JLandroid/content/Context;Lzg;)V

    return-void
.end method

.method public final D0()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g1()V

    return-void
.end method

.method public final E0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h:Ljava/lang/String;

    const/16 v3, 0x1e

    invoke-static {v3, v0, v1, v2}, La91;->D(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l0()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v2, "CONNECTION_SUCCESS_VALID"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, La91;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "START_SCAN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->a1()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v2, "CONNECTION_SUCCESS_INVALID"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->v0()V

    :goto_0
    return-void
.end method

.method public final F0(ZZLjava/lang/Long;)V
    .locals 8

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v1 .. v7}, Lx91;->a(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;ZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;

    move-result-object p1

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->publishMetaDataDtos:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, 0x7f120171

    const v4, 0x7f120060

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void

    :cond_0
    new-instance p2, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishExam;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-direct {p2, p1, p3, v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishExam;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;Landroid/content/Context;Lzg;)V

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->o1(Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishExam;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishExam;->f()V

    return-void
.end method

.method public final G0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->H0(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->q:J

    const v1, 0x7f090227

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->v:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->v:Z

    :goto_0
    invoke-virtual {p0}, Lv5;->invalidateOptionsMenu()V

    return-void
.end method

.method public final H0(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 20

    .line 1
    move-object/from16 v1, p0

    const v0, 0x7f0903cd

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f09041a

    invoke-virtual {v1, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0903fe

    invoke-virtual {v1, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090461

    invoke-virtual {v1, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090462

    invoke-virtual {v1, v5}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0903bc

    invoke-virtual {v1, v6}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f090422

    invoke-virtual {v1, v7}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v8, 0x7f0902cf

    invoke-virtual {v1, v8}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ProgressBar;

    iget-object v9, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v9}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v9

    iget-object v10, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getNumberOfStudentForAClass(Ljava/lang/String;)J

    move-result-wide v9

    iget-object v11, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v11}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v11

    iget-object v12, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v11, v12}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v11

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v15, 0x0

    cmp-long v17, v9, v15

    if-lez v17, :cond_0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v5

    const-string v5, "/"

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_0
    move-object/from16 v16, v5

    move-object v5, v14

    :goto_0
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    if-lez v17, :cond_1

    const-wide/16 v18, 0x64

    mul-long v18, v18, v11

    div-long v9, v18, v9

    long-to-int v5, v9

    const-wide/16 v9, 0x0

    goto :goto_1

    :cond_1
    const-wide/16 v9, 0x0

    cmp-long v5, v11, v9

    if-lez v5, :cond_2

    const/16 v5, 0x32

    goto :goto_1

    :cond_2
    move v5, v3

    :goto_1
    const v13, 0x7f0900de

    invoke-virtual {v1, v13}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v13

    cmp-long v9, v11, v9

    const/16 v10, 0x8

    if-lez v9, :cond_3

    move v11, v10

    goto :goto_2

    :cond_3
    move v11, v3

    :goto_2
    invoke-virtual {v13, v11}, Landroid/view/View;->setVisibility(I)V

    const v11, 0x7f0900e5

    invoke-virtual {v1, v11}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v11

    if-lez v9, :cond_4

    move v9, v3

    goto :goto_3

    :cond_4
    move v9, v10

    :goto_3
    invoke-virtual {v11, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v8, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v8, 0x1

    sub-int/2addr v5, v8

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v2

    if-eqz v2, :cond_5

    const v2, 0x7f120047

    goto :goto_4

    :cond_5
    const v2, 0x7f120249

    :goto_4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd"

    invoke-direct {v2, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    const-string v2, "dd"

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "MMM"

    invoke-static {v2, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object/from16 v5, v16

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_5
    const v0, 0x7f09023d

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f09023c

    invoke-virtual {v1, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    const v4, 0x7f09023e

    invoke-virtual {v1, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f09023b

    invoke-virtual {v1, v5}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v6

    if-eqz v6, :cond_9

    sget-object v6, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$l0;->a:[I

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    aget v6, v6, v7

    if-eq v6, v8, :cond_8

    const/4 v0, 0x2

    if-eq v6, v0, :cond_7

    const/4 v0, 0x3

    if-eq v6, v0, :cond_6

    goto :goto_6

    :cond_6
    invoke-virtual {v1, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_7

    :cond_7
    invoke-virtual {v1, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_7

    :cond_8
    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_7

    :cond_9
    :goto_6
    invoke-virtual {v1, v5}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_7
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final I0(Ljava/io/InputStream;Landroid/net/Uri;)V
    .locals 10

    .line 1
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-static {p1}, Lyx;->a([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    move-result-object p1

    const p2, 0x7f1200ca

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v0, 0x7f1201b3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k1(ILjava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0, v0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->p0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getResults()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;

    new-instance v9, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;->getRollno()I

    move-result v3

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;->getResultItem2()Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;ZZ)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    invoke-virtual {v0, v9}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsNotSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "ResultMergeSave"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f1201f0

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const p2, 0x7f120118

    invoke-virtual {p0, p2, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k1(ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v0, 0x7f1201e3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k1(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :goto_1
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_2
    return-void
.end method

.method public final J0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->n:Landroidx/appcompat/widget/SwitchCompat;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public final K0()V
    .locals 2

    .line 1
    const v0, 0x7f0901f2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$p;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$p;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L0()V
    .locals 2

    .line 1
    const v0, 0x7f0901d7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$m;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$m;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final M0()V
    .locals 2

    .line 1
    const v0, 0x7f090231

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$l;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final N0()V
    .locals 2

    .line 1
    const v0, 0x7f0901e8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$s;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$s;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final O0()V
    .locals 2

    .line 1
    const v0, 0x7f0901f4

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$n0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$n0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P0()V
    .locals 2

    .line 1
    const v0, 0x7f0901fc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$m0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$m0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Q0()V
    .locals 2

    .line 1
    const v0, 0x7f0901fd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$o0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$o0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final R0()V
    .locals 2

    .line 1
    const v0, 0x7f0901dd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$n;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$n;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final S0()V
    .locals 2

    .line 1
    const v0, 0x7f09020e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$g0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$g0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final T0()V
    .locals 2

    .line 1
    const v0, 0x7f09021c

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$o;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$o;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final U0()V
    .locals 2

    .line 1
    const v0, 0x7f090232

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$r;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$r;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final V0()V
    .locals 2

    .line 1
    const v0, 0x7f090233

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$q;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final W0()V
    .locals 3

    .line 1
    const v0, 0x7f09036e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->p:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->p:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$t;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$t;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    return-void
.end method

.method public final X0()V
    .locals 2

    .line 1
    const v0, 0x7f090440

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->t:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->t:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$k0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$k0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Y0(Z)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j0;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$j0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f1200ca

    const v3, 0x7f12019e

    const v4, 0x7f12029a

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final Z0(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "REWARD_EXCEL_DIALOG_SHOWN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v0, Lkf1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f1201f6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1201f9

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;

    invoke-direct {v4, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    invoke-direct {v0, v1, v2, v3, v4}, Lkf1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ly01;)V

    return-void
.end method

.method public final a1()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "REWARD_SCAN_DIALOG_SHOWN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v0, Lkf1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f1201f5

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1201fa

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;

    invoke-direct {v4, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$u;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lkf1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ly01;)V

    return-void
.end method

.method public final b1()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->n:Landroidx/appcompat/widget/SwitchCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    new-instance v3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$p0;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$p0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v4, 0x7f120061

    const v5, 0x7f12017e

    const v6, 0x7f120050

    const v7, 0x7f120053

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final c1(Z)V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f120051

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    const v2, 0x7f12016f

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    const v2, 0x7f120050

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$d0;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$d0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    const p1, 0x7f120328

    invoke-virtual {v0, p1, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final d1()V
    .locals 7

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0072

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202cf

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0903ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f090372

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/SwitchCompat;

    const v4, 0x7f0900f7

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    const-string v5, "include_rank"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v4

    new-instance v5, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$g;

    invoke-direct {v5, p0, v4, v2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lr30;Landroid/widget/TextView;)V

    invoke-virtual {v3, v5}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;

    invoke-direct {v4, p0, v2, v1, v3}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/widget/TextView;Landroid/widget/CheckBox;Landroidx/appcompat/widget/SwitchCompat;)V

    const v1, 0x7f12027d

    invoke-virtual {v0, v1, v4}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$i;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    const v2, 0x7f120053

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final e1()V
    .locals 12

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0071

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202cf

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0903ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Landroid/widget/TextView;

    const v2, 0x7f090372

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/SwitchCompat;

    const v3, 0x7f0902e2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Landroid/widget/RadioButton;

    const v3, 0x7f0902e3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Landroid/widget/RadioButton;

    const v3, 0x7f0900f7

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Landroid/widget/CheckBox;

    const v3, 0x7f0900f6

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v7, v3

    check-cast v7, Landroid/widget/CheckBox;

    const v3, 0x7f0900cb

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v11, v3

    check-cast v11, Landroid/widget/Button;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    const-string v4, "include_rank"

    const/4 v10, 0x0

    invoke-interface {v3, v4, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v6, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    const-string v4, "include_sheet_img"

    invoke-interface {v3, v4, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v7, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$d;

    invoke-direct {v4, p0, v3, v5}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lr30;Landroid/widget/TextView;)V

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e;

    move-object v3, v2

    move-object v4, p0

    move-object v10, v0

    invoke-direct/range {v3 .. v10}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/widget/TextView;Landroid/widget/CheckBox;Landroid/widget/CheckBox;Landroid/widget/RadioButton;Landroid/widget/RadioButton;Landroidx/appcompat/app/a;)V

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0900ab

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroidx/appcompat/app/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final f1(Z)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0081

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1200a5

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0900ff

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;

    invoke-direct {v2, p0, p1, v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;ZLandroid/widget/CheckBox;)V

    const p1, 0x7f12023f

    invoke-virtual {v0, p1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final g1()V
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0082

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202ce

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0902e4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    const v3, 0x7f0902db

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    const v4, 0x7f0902e9

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;

    invoke-direct {v4, p0, v2, v3, v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/widget/RadioButton;Landroid/widget/RadioButton;Landroid/widget/RadioButton;)V

    const v1, 0x7f12023f

    invoke-virtual {v0, v1, v4}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final h1()V
    .locals 11

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "[^a-zA-Z0-9_-]"

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".exm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lok;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b0;

    invoke-direct {v4, p0, v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Ljava/io/File;)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v5, 0x7f1200e8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f1200e7

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f1202e8

    const v8, 0x7f120060

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    return-void
.end method

.method public final i0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f1201bc

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    invoke-static {v0, p1, v1, v2}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void

    :cond_0
    new-instance v4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;

    invoke-direct {v4, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v5, 0x7f12003f

    const v6, 0x7f120175

    const v7, 0x7f12003e

    const v8, 0x7f120053

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final i1()V
    .locals 6

    .line 1
    new-instance v0, Lu80;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-direct {v0, v1}, Lu80;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c008a

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    const v1, 0x7f090241

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->s0()Ljava/util/ArrayList;

    move-result-object v2

    const v3, 0x7f0903b3

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f12001b

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v3, Lb3;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-direct {v3, v4, v2}, Lb3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v1, 0x7f0900b6

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$w;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$w;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->create()V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final j0()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v1}, La91;->t(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastSyncedOn()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/32 v5, 0x2bf20

    add-long/2addr v3, v5

    cmp-long v0, v1, v3

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllNonSyncResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->r1(Z)V

    :cond_1
    return-void
.end method

.method public final j1(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;)V
    .locals 3

    .line 1
    new-instance v0, Lvx;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$y;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$y;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-direct {v0, v1, p1, v2}, Lvx;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;Ly01;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final k0()Z
    .locals 2

    .line 1
    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Lsl;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lh2;->g(Landroid/app/Activity;[Ljava/lang/String;I)V

    const/4 v0, 0x0

    return v0
.end method

.method public final k1(ILjava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const/4 v1, 0x0

    const v4, 0x7f120060

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v7}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    return-void
.end method

.method public final l0()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v2, 0x3e7

    :goto_0
    const-string v3, "MyPref"

    invoke-virtual {p0, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v3, v0, v2}, La91;->E(Landroid/content/SharedPreferences;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lgk;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final l1()V
    .locals 2

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$s0;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$s0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$k;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public final m0()Z
    .locals 10

    .line 1
    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->q:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-gez v0, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x7f1201d5

    :goto_0
    const v6, 0x7f120060

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return v1

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x7f1201e7

    goto :goto_0
.end method

.method public final m1(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f120111

    const v3, 0x7f1201ad

    const v4, 0x7f120131

    const v5, 0x7f120060

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final n0(Ljava/util/Set;Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    int-to-long v3, v3

    cmp-long v3, v3, v1

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    int-to-long v3, v3

    cmp-long v3, v1, v3

    if-lez v3, :cond_3

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    int-to-long v3, p1

    cmp-long p1, v3, v1

    if-lez p1, :cond_4

    if-nez p2, :cond_4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p2

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastSyncedOn(Ljava/lang/Long;)V

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->r1(Z)V

    :cond_4
    return-void
.end method

.method public final n1()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const/4 v1, 0x0

    const v2, 0x7f1202b3

    const v3, 0x7f1201e1

    const v4, 0x7f120060

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final o0(Z)V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120183

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Lma0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;

    invoke-direct {v3, p0, v0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$f0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/app/ProgressDialog;Z)V

    invoke-direct {v1, v2, v3}, Lma0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final o1(Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishExam;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    const v1, 0x7f12027f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    const v1, 0x7f120053

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$i0;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$i0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishExam;)V

    const/4 p1, -0x2

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0xb

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    :try_start_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p2

    invoke-virtual {p0, p2, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->I0(Ljava/io/InputStream;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0029

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->X0()V

    invoke-static {p0}, Lo4;->a(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->j:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    new-instance p1, Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const v2, 0x7f130131

    invoke-direct {p1, v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->j:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v0, "RA_EXAMID_NULL"

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->k:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->j:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v0, "RA_OC_TEMPLATE_NULL"

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->h:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const p1, 0x7f090374

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/SwitchCompat;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->n:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->p1()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->J0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->W0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->z0()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e000a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0901c7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->v:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const v0, 0x7f0901c2

    if-eq p1, v0, :cond_1

    const v0, 0x7f0901c7

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->C0()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    if-eqz p3, :cond_0

    array-length p2, p3

    if-lez p2, :cond_0

    const/4 p2, 0x0

    aget p2, p3, p2

    if-nez p2, :cond_0

    const/4 p2, 0x2

    if-ne p2, p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->E0()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->j0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->G0()V

    return-void
.end method

.method public final p0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-eq v3, v4, :cond_1

    return v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method public final p1()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    const-string v1, "count_saveScan"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    const-string v3, "count_cancelScan"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m:Landroid/content/SharedPreferences;

    const-string v4, "last_rate_minutes"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {}, La91;->n()I

    move-result v3

    sub-int v2, v3, v2

    const v4, 0x15180

    if-lt v2, v4, :cond_1

    add-int/2addr v0, v1

    invoke-static {}, La91;->A()I

    move-result v1

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/google/android/play/core/review/ReviewManagerFactory;->create(Landroid/content/Context;)Lcom/google/android/play/core/review/ReviewManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/core/review/ReviewManager;->requestReviewFlow()Lcom/google/android/play/core/tasks/Task;

    move-result-object v1

    new-instance v2, Lwx;

    invoke-direct {v2, p0, v0, v3}, Lwx;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/google/android/play/core/review/ReviewManager;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/core/tasks/Task;->addOnCompleteListener(Lcom/google/android/play/core/tasks/OnCompleteListener;)Lcom/google/android/play/core/tasks/Task;

    :cond_1
    :goto_0
    return-void
.end method

.method public final q0()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->m0()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f1(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->c1(Z)V

    :goto_0
    return-void
.end method

.method public final q1(Z)V
    .locals 18

    .line 1
    move-object/from16 v7, p0

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v5

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v3

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v0

    new-instance v6, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    invoke-direct {v6}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;-><init>()V

    invoke-virtual {v6, v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->setExamId(Ljava/lang/Long;)V

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isPublished()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    :goto_0
    move-object v14, v4

    new-instance v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastUpdatedOn()Ljava/lang/Long;

    move-result-object v10

    iget-object v11, v7, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    new-instance v8, Lgc0;

    invoke-direct {v8}, Lgc0;-><init>()V

    invoke-virtual {v8, v1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateVersion()I

    move-result v13

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSyncImages()Z

    move-result v17

    move-object v8, v4

    move-object v9, v0

    invoke-direct/range {v8 .. v17}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;Lcom/ekodroid/omrevaluator/serializable/SharedType;Ljava/lang/String;Z)V

    invoke-virtual {v6, v4}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->setExam(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;)V

    :cond_1
    iget-object v1, v7, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v5, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllNotSyncedResults(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    goto :goto_2

    :cond_2
    sget-object v10, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    :goto_2
    move-object/from16 v16, v10

    new-instance v10, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v12

    const-wide/16 v13, 0x0

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItemString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object v11, v10

    invoke-direct/range {v11 .. v16}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;-><init>(ILjava/lang/Long;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;)V

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v6, v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->setStudentResults(Ljava/util/ArrayList;)V

    new-instance v8, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPendingReports;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    iget-object v12, v7, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v13, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;

    move-object v0, v13

    move-object/from16 v1, p0

    move-object v11, v6

    move/from16 v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;Z)V

    invoke-direct/range {v8 .. v13}, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPendingReports;-><init>(JLcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final r0(Z)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastSyncedOn()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Lqa0;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v9, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;

    invoke-direct {v9, p0, v0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Z)V

    invoke-direct/range {v3 .. v9}, Lqa0;-><init>(JJLandroid/content/Context;Lzg;)V

    return-void
.end method

.method public final r1(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->b(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, La91;->t(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->r0(Z)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/DeleteSyncReports;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    new-instance v3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$r0;

    invoke-direct {v3, p0, p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$r0;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    invoke-direct {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/DeleteSyncReports;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;Landroid/content/Context;Lzg;)V

    :goto_0
    return-void
.end method

.method public final s0()Ljava/util/ArrayList;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllUniqueRollNumberForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/HashSet;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudents(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x4

    new-array v5, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    move v7, v6

    :goto_1
    const-string v8, ""

    if-ge v7, v4, :cond_1

    aput-object v8, v5, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    aput-object v4, v5, v6

    const/4 v4, 0x3

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v4

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public final t0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final u0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final v0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "BLOCK_NO_SYNC"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final w0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final x0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->l:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final y0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "Choose a file"

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final z0()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->R0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->T0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->K0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->V0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->U0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->N0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->M0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->S0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->L0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->O0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->P0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->Q0()V

    const v0, 0x7f0900de

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$k;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900e5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$v;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$v;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

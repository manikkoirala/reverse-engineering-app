.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i0(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setArchiving(Z)V

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchiveId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setArchiveId(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->n(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$z;->b:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

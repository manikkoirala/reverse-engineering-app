.class public Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;->a(ZILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getStudentResults()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;->c:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->J(Lcom/ekodroid/omrevaluator/exams/ResultActivity;)Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getStudentResults()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;->c:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->getRollNo()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v3

    const/4 v4, 0x1

    const-class v5, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced()Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v6, Lgc0;

    invoke-direct {v6}, Lgc0;-><init>()V

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->getReport()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {v3, v6}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setResultItem(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    invoke-virtual {v0, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    :cond_1
    if-nez v3, :cond_0

    new-instance v3, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;->c:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->getRollNo()I

    move-result v9

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;->c:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v10

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l$a;->b:Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/ResultActivity$l;->c:Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/ResultActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v11

    new-instance v6, Lgc0;

    invoke-direct {v6}, Lgc0;-><init>()V

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->getReport()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2, v5}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v7, v3

    invoke-direct/range {v7 .. v14}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;ZZ)V

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    invoke-virtual {v0, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

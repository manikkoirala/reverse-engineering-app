.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->d1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/widget/CheckBox;

.field public final synthetic c:Landroidx/appcompat/widget/SwitchCompat;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Landroid/widget/TextView;Landroid/widget/CheckBox;Landroidx/appcompat/widget/SwitchCompat;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->b:Landroid/widget/CheckBox;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->c:Landroidx/appcompat/widget/SwitchCompat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->N(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Landroid/content/SharedPreferences;

    move-result-object p2

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "include_rank"

    invoke-interface {p2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {p2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$h;->c:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p2, v0, v1, v2}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->P(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;ZZLjava/lang/Long;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public A:Landroid/widget/Toast;

.field public C:Landroid/content/BroadcastReceiver;

.field public c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/content/SharedPreferences;

.field public f:Ljava/util/ArrayList;

.field public g:I

.field public h:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public i:Ljava/util/ArrayList;

.field public j:Ljava/util/ArrayList;

.field public k:Landroid/app/ProgressDialog;

.field public l:Ljava/lang/String;

.field public m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

.field public p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public t:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

.field public v:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public w:Z

.field public x:Landroid/widget/ImageButton;

.field public y:Landroid/widget/ImageButton;

.field public z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->w:Z

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->W()V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Landroid/widget/Toast;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->A:Landroid/widget/Toast;

    return-object p0
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->N()Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->K(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;ZZLjava/lang/Long;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->U(ZZLjava/lang/Long;)V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)Landroid/app/ProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->k:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->J()V

    return-void
.end method


# virtual methods
.method public final A(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Landroid/widget/LinearLayout;)V
    .locals 7

    .line 1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->L(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/LayoutInflater;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Li5;

    const v1, 0x7f0c009a

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const v2, 0x7f0903ef

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0903e0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0903c1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0903b9

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v0}, Li5;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Li5;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Li5;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Li5;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final J()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v3, v4, v0, v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;-><init>(Ljava/lang/Long;IZ)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v3, v1}, La91;->K(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v3, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    move-result v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3, v4, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    move-result v0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v2

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const v1, 0x7f0800cf

    const v2, 0x7f08016d

    const v3, 0x7f120298

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->W()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f120299

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.method public final K(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSyncImages()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v0, p1, p2}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->h(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V

    :cond_0
    return-void
.end method

.method public final L(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v5

    invoke-static {v2, v5}, Le5;->r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v7

    invoke-static {v7, v8}, Lve1;->o(D)D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v6

    invoke-static {p2, v6}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v6

    invoke-static {v4, v6}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    new-instance v6, Li5;

    invoke-direct {v6, v3, v5, v4, v2}, Li5;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final M()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRank()I

    move-result v0

    return v0
.end method

.method public final N()Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;
    .locals 2

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    return-object v0
.end method

.method public final O()Landroid/widget/TextView;
    .locals 4

    .line 1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x5a

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v1, v2}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const/16 v2, 0x8

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    const v1, 0x7f130235

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06005c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    return-object v0
.end method

.method public final P()V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->C:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public final Q()V
    .locals 2

    .line 1
    const v0, 0x7f09019e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->x:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final R()V
    .locals 2

    .line 1
    const v0, 0x7f0901a0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->y:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final S()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const-class v3, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3, v4, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Ljx0;->b:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    sget-object v5, Ljx0;->b:Ljava/util/ArrayList;

    invoke-static {v4}, Lbc;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->j:Ljava/util/ArrayList;

    sput-object v3, Ljx0;->a:Ljava/util/ArrayList;

    :cond_1
    const-string v3, "ROLL_NUMBER"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "RESULT_ITEM"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final T()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object v1

    const v2, 0x7f08016e

    const v3, 0x7f0800bd

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v5, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v4

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x5

    if-ge v4, v5, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x7f1201f1

    const v6, 0x7f120060

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void

    :cond_2
    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v2, v1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->Z(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const v1, 0x7f12002d

    invoke-static {v0, v1, v3, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const v1, 0x7f1201e8

    invoke-static {v0, v1, v3, v2}, La91;->G(Landroid/content/Context;III)V

    return-void
.end method

.method public final U(ZZLjava/lang/Long;)V
    .locals 9

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v8

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v1 .. v8}, Lx91;->b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;ZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;I)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;

    move-result-object p1

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->publishMetaDataDtos:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, 0x7f120172

    const v4, 0x7f120060

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void

    :cond_0
    new-instance p2, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishReport;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    invoke-direct {p2, p1, p3, v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishReport;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;Landroid/content/Context;Lzg;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->Y(Z)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/PostPublishReport;->f()V

    return-void
.end method

.method public final V()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->y:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    const/4 v2, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v1

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->z:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f12028a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final W()V
    .locals 35

    .line 1
    move-object/from16 v1, p0

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->V()V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->N()Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v9

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->t:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2, v3, v9}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v2

    const/4 v10, 0x1

    if-nez v2, :cond_0

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->h:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v4, "SRA_RESULT_NULL"

    iget-object v5, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->l:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    iget v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    add-int/2addr v3, v10

    iput v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->W()V

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lv5;->n()Lt1;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Lv5;->n()Lt1;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lt1;->t(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v3

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v3

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v11

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v2

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v11, v4}, Lve1;->k(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v4

    iget-object v5, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v11, v5}, Lve1;->c(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I

    move-result-object v5

    iget-object v6, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v11, v6}, Lve1;->j(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I

    move-result-object v6

    invoke-static {v5}, Lve1;->h([[I)[I

    move-result-object v7

    invoke-static {v6}, Lve1;->h([[I)[I

    move-result-object v8

    iget-object v12, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v12}, Lve1;->r(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v12

    invoke-static {v4, v12}, Lve1;->n([[D[[D)[[D

    move-result-object v13

    invoke-static {v12}, Lve1;->g([[D)[D

    move-result-object v12

    invoke-static {v2, v12}, Lve1;->m([D[D)[D

    move-result-object v14

    move-object v15, v5

    move-object/from16 v16, v6

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v5

    const-wide/high16 v17, 0x4059000000000000L    # 100.0

    mul-double v17, v17, v5

    invoke-static {v12}, Lve1;->d([D)D

    move-result-wide v19

    div-double v17, v17, v19

    iget-object v12, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v12

    if-eqz v12, :cond_2

    iget-object v12, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v12

    invoke-static {v5, v6, v12}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    :cond_2
    const/4 v12, 0x0

    :goto_0
    const v10, 0x7f0903df

    invoke-virtual {v1, v10}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    move/from16 v21, v9

    const v9, 0x7f0903da

    invoke-virtual {v1, v9}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    move-wide/from16 v22, v5

    const v5, 0x7f0903db

    invoke-virtual {v1, v5}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f0903dc

    invoke-virtual {v1, v6}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    move-object/from16 v24, v15

    const v15, 0x7f0903dd

    invoke-virtual {v1, v15}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v25, v13

    const v13, 0x7f0903de

    invoke-virtual {v1, v13}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v26, v4

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamNameString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRankString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getGradeString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getClassString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNameString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f09040c

    invoke-virtual {v1, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f090407

    invoke-virtual {v1, v5}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v6, 0x7f090409

    invoke-virtual {v1, v6}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v9, 0x7f09040a

    invoke-virtual {v1, v9}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    const v10, 0x7f09040e

    invoke-virtual {v1, v10}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    const v13, 0x7f09040b

    invoke-virtual {v1, v13}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const v15, 0x7f09037c

    invoke-virtual {v1, v15}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TableLayout;

    invoke-virtual {v15}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v27

    move-object/from16 v29, v7

    move-object/from16 v28, v8

    const/4 v8, 0x1

    add-int/lit8 v7, v27, -0x1

    invoke-virtual {v15, v8, v7}, Landroid/view/ViewGroup;->removeViews(II)V

    const v7, 0x7f0901da

    invoke-virtual {v1, v7}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Landroid/widget/LinearLayout;

    const v7, 0x7f0901db

    invoke-virtual {v1, v7}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    const v7, 0x7f0901fa

    invoke-virtual {v1, v7}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->M()I

    move-result v27

    move-object/from16 v30, v8

    const/16 v8, 0x8

    if-lez v27, :cond_3

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRank()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const v0, 0x7f090214

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    if-eqz v12, :cond_4

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    const v0, 0x7f0901f5

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    iget-object v0, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    const-string v0, "---"

    :goto_3
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v0

    const/4 v3, 0x1

    sub-int/2addr v0, v3

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v5

    sub-int/2addr v5, v3

    if-gez v5, :cond_6

    const/4 v5, 0x0

    :cond_6
    aget-object v0, v0, v5

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_7
    const v0, 0x7f0901ee

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    const v0, 0x7f09038c

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectAnswerString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f09038d

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getIncorrectAnswerString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090390

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSubjectString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f09038e

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMarksString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f09038f

    invoke-virtual {v1, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getPercentageString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    const/16 v3, 0x20

    iget-object v5, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v3, v5}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    const/4 v5, -0x2

    invoke-direct {v0, v5, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const/4 v5, 0x1

    invoke-static {v5, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/4 v3, 0x0

    :goto_5
    array-length v6, v2

    const-string v9, "%"

    const-string v10, ""

    if-ge v3, v6, :cond_9

    new-instance v6, Landroid/widget/TableRow;

    iget-object v12, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {v6, v12}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v4

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v13, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v31, v2, v3

    move-object/from16 v33, v9

    invoke-static/range {v31 .. v32}, Lve1;->o(D)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v12, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v8, 0x1

    invoke-virtual {v4, v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v19, v14, v3

    move-object v12, v14

    invoke-static/range {v19 .. v20}, Lve1;->o(D)D

    move-result-wide v13

    invoke-virtual {v9, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-object/from16 v13, v33

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v14, v29, v3

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v8, v28, v3

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v15, v6}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    const/4 v4, 0x0

    :goto_6
    aget-object v5, v26, v3

    array-length v5, v5

    if-ge v4, v5, :cond_8

    new-instance v5, Landroid/widget/TableRow;

    iget-object v6, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {v5, v6}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v6

    iget-object v8, v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v8

    aget-object v8, v8, v3

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v8

    aget-object v8, v8, v4

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v14, v26, v3

    aget-wide v32, v14, v4

    move-object v14, v11

    move-object/from16 v34, v12

    invoke-static/range {v32 .. v33}, Lve1;->o(D)D

    move-result-wide v11

    invoke-virtual {v9, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v9

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v12, v25, v3

    aget-wide v32, v12, v4

    move-object v12, v2

    invoke-static/range {v32 .. v33}, Lve1;->o(D)D

    move-result-wide v1

    invoke-virtual {v11, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v11, v24, v3

    aget v11, v11, v4

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v2

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v32, v16, v3

    move-object/from16 v33, v12

    aget v12, v32, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v8, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v9, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v15, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v1, p0

    move-object v11, v14

    move-object/from16 v2, v33

    move-object/from16 v12, v34

    goto/16 :goto_6

    :cond_8
    move-object/from16 v33, v2

    move-object v14, v11

    move-object/from16 v34, v12

    add-int/lit8 v3, v3, 0x1

    const/4 v5, 0x1

    const/16 v8, 0x8

    move-object/from16 v1, p0

    move-object/from16 v14, v34

    goto/16 :goto_5

    :cond_9
    move-object v13, v9

    move-object v14, v11

    new-instance v1, Landroid/widget/TableRow;

    move-object/from16 v9, p0

    iget-object v2, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {v1, v2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v5, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTotalMarksString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v22 .. v23}, Lve1;->o(D)D

    move-result-wide v11

    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v17 .. v18}, Lve1;->o(D)D

    move-result-wide v11

    invoke-virtual {v8, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v29 .. v29}, Lve1;->e([I)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->O()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v11, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v28 .. v28}, Lve1;->e([I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v5, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v8, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v11, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v15, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->t:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    iget-object v1, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move/from16 v10, v21

    invoke-virtual {v0, v1, v10}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v7}, Landroid/view/ViewGroup;->removeAllViews()V

    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_b

    iget-object v1, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_a

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_7

    :cond_b
    const/4 v1, 0x0

    iput-object v1, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_f

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v2

    array-length v2, v2

    if-ne v1, v2, :cond_f

    invoke-static {v0}, Le5;->E(Ljava/util/ArrayList;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getImagePath()Ljava/lang/String;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getImagePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const-string v4, "Image Missing"

    if-eqz v3, :cond_d

    :try_start_1
    iget-object v5, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getSheetDimension()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    move-result-object v2

    if-eqz v2, :cond_c

    iget-object v3, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_c
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v11, Lcom/ekodroid/omrevaluator/templateui/scanner/b;

    invoke-direct {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;-><init>()V

    :goto_9
    iget-object v2, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_f

    iget-object v2, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/graphics/Bitmap;

    iget-object v4, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v2, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getPageIndex()I

    move-result v8
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v11

    move-object v5, v14

    move-object v12, v7

    move v7, v10

    move-object/from16 v13, v30

    :try_start_2
    invoke-virtual/range {v2 .. v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->i(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/widget/ImageView;

    iget-object v4, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    invoke-virtual {v12, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    add-int/lit8 v1, v1, 0x1

    move-object v7, v12

    move-object/from16 v30, v13

    goto :goto_9

    :catch_0
    move-exception v0

    goto :goto_a

    :cond_f
    move-object/from16 v13, v30

    goto :goto_b

    :catch_1
    move-exception v0

    move-object/from16 v13, v30

    :goto_a
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_b
    iget-object v0, v9, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v9, v14, v0, v13}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Landroid/widget/LinearLayout;)V

    return-void
.end method

.method public final X()V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$h;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const v2, 0x7f12009e

    const v3, 0x7f1201f8

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final Y(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    new-instance p1, Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-direct {p1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->k:Landroid/app/ProgressDialog;

    const v0, 0x7f1201e5

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->k:Landroid/app/ProgressDialog;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method

.method public final Z(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 1
    new-instance p1, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    const v0, 0x7f130151

    invoke-direct {p1, p3, v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string p3, "layout_inflater"

    invoke-virtual {p0, p3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/LayoutInflater;

    const v0, 0x7f0c0084

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v0, 0x7f1202cd

    invoke-virtual {p1, v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v0, 0x7f0903bb

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090373

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/SwitchCompat;

    const v2, 0x7f0900f7

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    const v3, 0x7f090408

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->e:Landroid/content/SharedPreferences;

    const-string v4, "include_rank"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$d;

    invoke-direct {v4, p0, v3, v0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;Lr30;Landroid/widget/TextView;)V

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f1200b2

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance p2, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;

    invoke-direct {p2, p0, v2, v1}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;Landroid/widget/CheckBox;Landroidx/appcompat/widget/SwitchCompat;)V

    const p3, 0x7f12027d

    invoke-virtual {p1, p3, p2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance p2, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$f;

    invoke-direct {p2, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    const p3, 0x7f120053

    invoke-virtual {p1, p3, p2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p2, 0x5

    if-ne p1, p2, :cond_7

    if-eqz p3, :cond_7

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result p1

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "RESULT_ITEM"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    if-nez p2, :cond_1

    return-void

    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ROLL_NUMBER"

    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p3

    const-string v1, "EXAM_SET"

    invoke-virtual {p3, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    move-result p3

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2, v3, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3, v4, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-eq v0, p1, :cond_6

    invoke-virtual {v2, v0}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setRollNo(I)V

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setExamSet(I)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setAnswerValues(Ljava/util/ArrayList;)V

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setResultItem(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V

    invoke-virtual {v2, v6}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setId(Ljava/lang/Integer;)V

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsNotSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result p2

    if-eqz p2, :cond_2

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    new-instance p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p3, v1, p1, v4}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;-><init>(Ljava/lang/Long;IZ)V

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p3, p2}, La91;->K(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;

    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-lez p2, :cond_5

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getSheetImage()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p3, v0}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->setRollNo(I)V

    invoke-virtual {p3, v6}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->setId(Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateSheetImage(Lcom/ekodroid/omrevaluator/database/SheetImageModel;)Z

    goto :goto_0

    :cond_4
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetModelKeepingImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    :cond_5
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    iget p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->setRollNo(I)V

    goto :goto_1

    :cond_6
    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setExamSet(I)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setAnswerValues(Ljava/util/ArrayList;)V

    invoke-virtual {v2, p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setResultItem(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsNotSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    :goto_1
    iput-boolean v4, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->w:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "ResultEdit"

    invoke-virtual {p1, p2, v6}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->W()V

    :cond_7
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0040

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    const p1, 0x7f0903f6

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->z:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->h:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->e:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SELECTED_POSITION"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->g:I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->n:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->t:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "ROLLNO_LIST_FILE_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->t:Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getFile(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/FileDataModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/FileDataModel;->getFileData()[B

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$1;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$1;-><init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;)V

    invoke-virtual {v0}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    new-instance p1, Lgc0;

    invoke-direct {p1}, Lgc0;-><init>()V

    invoke-virtual {p1, v1, v0}, Lgc0;->k(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->f:Ljava/util/ArrayList;

    const p1, 0x7f090225

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->Q()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->R()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->l:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->P()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->N()Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/exams/models/RollNoListDataModel;->getRollNo()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->K(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e000b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const v0, 0x7f09003f

    if-eq p1, v0, :cond_2

    const v0, 0x7f090044

    if-eq p1, v0, :cond_1

    const v0, 0x7f09004e

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->T()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->S()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->X()V

    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->C:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->C:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UPDATE_SHEET_IMAGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->W()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

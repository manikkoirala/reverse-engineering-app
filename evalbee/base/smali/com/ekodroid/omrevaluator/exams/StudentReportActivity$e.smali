.class public Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->Z(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/CheckBox;

.field public final synthetic b:Landroidx/appcompat/widget/SwitchCompat;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;Landroid/widget/CheckBox;Landroidx/appcompat/widget/SwitchCompat;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->b:Landroidx/appcompat/widget/SwitchCompat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->e:Landroid/content/SharedPreferences;

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "include_rank"

    invoke-interface {p2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {p2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->b:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p2, v0, v1, v2}, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->G(Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;ZZLjava/lang/Long;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity$e;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;->c:Lcom/ekodroid/omrevaluator/exams/StudentReportActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "reportPublishIndividual"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->f1(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Landroid/widget/CheckBox;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;ZLandroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->a:Z

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->b:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->a:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    invoke-static {p2, v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->X(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Z)V

    goto :goto_0

    :cond_0
    new-instance p2, Lsu;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$e0;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    invoke-direct {p2, v0, v1, v2}, Lsu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->r0(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_2

    check-cast p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getExam()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->getExam()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    move-result-object p1

    new-instance p2, Lgc0;

    invoke-direct {p2}, Lgc0;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getTemplate()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p2, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSheetTemplate(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getPublished()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    invoke-virtual {p2, v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setPublished(Z)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getUpdatedOn()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastUpdatedOn(Ljava/lang/Long;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->c:Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;->g0(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;)Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    :cond_1
    new-instance p1, Lya;

    new-instance p2, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a$a;

    invoke-direct {p2, p0, p3}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a$a;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;)V

    new-instance p3, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a$b;

    invoke-direct {p3, p0}, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a$b;-><init>(Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity$a;)V

    invoke-direct {p1, p2, p3}, Lya;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    new-array p2, v0, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void
.end method

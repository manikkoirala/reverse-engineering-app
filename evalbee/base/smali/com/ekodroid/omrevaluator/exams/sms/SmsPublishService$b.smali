.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 8

    .line 1
    const/4 v0, 0x3

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

    if-eqz v2, :cond_2

    check-cast p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->getStatus()[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v2, v4

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->isStatus()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    iget-object v6, v6, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-static {v7}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v7

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->getRollNo()I

    move-result v5

    invoke-virtual {v6, v7, v5, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->updateSmsSendStatus(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;IZ)Z

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-static {v5, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;I)V

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    array-length v3, v3

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->d(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)I

    move-result v4

    invoke-static {v1, v0, v3, v4}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->e(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;III)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    array-length v1, v2

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->f(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->getSmsAccount()Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->g(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->h(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    iget-object v2, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    array-length v2, v2

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->d(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)I

    move-result v3

    invoke-static {p1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->e(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-static {p1, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->f(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->g(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V

    :goto_1
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->a0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 9

    .line 1
    const/16 v0, 0x8

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f1201a8

    const v5, 0x7f120257

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->h:Landroid/widget/TextView;

    const-string v1, "---"

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->i:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    check-cast p1, Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    iput-object p1, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->t:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/SmsAccount;->getCredit()I

    move-result p1

    iput p1, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->q:I

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->i:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget v2, v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->j:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

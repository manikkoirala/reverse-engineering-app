.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

.field public d:Ljava/lang/String;

.field public e:Landroid/widget/Button;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/LinearLayout;

.field public h:Landroid/widget/EditText;

.field public i:Ljava/lang/String;

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->j:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->K()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->I(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->J()V

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 6

    .line 1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    invoke-static {v1, v2}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lok;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " : Roll no"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x64

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->g:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Student name"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->h:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Exam name"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->i:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Class name"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->j:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Exam date"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->k:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Grade"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->l:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Rank"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->m:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Percentage"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->n:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Total marks"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->p:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Correct answers"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->q:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Incorrect answers"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->r:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : Unattempted questions"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->s:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "n : Marks in nth subject"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lok;->w:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "n : Percentage in nth subject"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final E()V
    .locals 1

    .line 1
    const v0, 0x7f09014e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->h:Landroid/widget/EditText;

    const v0, 0x7f09022e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f0900c1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->f:Landroid/widget/Button;

    const v0, 0x7f0900d6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->e:Landroid/widget/Button;

    return-void
.end method

.method public final F()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->f:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final G()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->e:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final H()V
    .locals 2

    .line 1
    const v0, 0x7f09044f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final I(Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$d;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    const v2, 0x7f120031

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f120177

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "?"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const v7, 0x7f0800e1

    invoke-static/range {v0 .. v7}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    return-void
.end method

.method public final J()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1201d3

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$g;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V

    const v3, 0x7f120337

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$f;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V

    const v3, 0x7f120241

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final K()V
    .locals 5

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f1200c5

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    new-instance v1, Lvi1;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v0, v3, v4}, Lvi1;-><init>(Landroid/content/Context;Ly01;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->j:Z

    :cond_0
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->J()V

    goto :goto_0

    :cond_1
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c003e

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->E()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->H()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SMS_TEMPLATE_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    const-string v0, ""

    const/16 v1, 0x8

    if-eqz p1, :cond_0

    sget-object v2, Lok;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->f:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    sget-object p1, Lok;->F:Ljava/lang/String;

    :goto_0
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v2, "Default Medium"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->f:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    sget-object p1, Lok;->H:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    if-eqz p1, :cond_2

    const-string v2, "Default Subject Wise"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->f:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    sget-object p1, Lok;->G:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getSmsTemplate(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->getSmsString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->i:Ljava/lang/String;

    :goto_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->d:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->h:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->h:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->D()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->F()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;->G()V

    return-void
.end method

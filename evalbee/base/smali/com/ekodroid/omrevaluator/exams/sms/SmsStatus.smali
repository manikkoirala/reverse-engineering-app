.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field rollNo:I

.field status:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->rollNo:I

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->status:Z

    return-void
.end method


# virtual methods
.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->rollNo:I

    return v0
.end method

.method public isStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->status:Z

    return v0
.end method

.method public setRollNo(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->rollNo:I

    return-void
.end method

.method public setStatus(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;->status:Z

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->W()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ly01;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ly01;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->a:Ly01;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->t:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    if-eqz v2, :cond_1

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x7f120193

    const v6, 0x7f120257

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void

    :cond_0
    new-instance v1, Lal1;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object v11, v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->F(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v12

    iget-object v13, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->a:Ly01;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object v14, v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    iget v15, v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->q:I

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v16

    move-object v10, v1

    invoke-direct/range {v10 .. v16}, Lal1;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ly01;Ljava/util/ArrayList;II)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->A(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    :goto_0
    return-void
.end method

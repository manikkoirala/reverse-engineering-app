.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

.field public d:Landroid/widget/Spinner;

.field public e:Landroid/widget/Button;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/ImageButton;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/ImageButton;

.field public j:Landroid/widget/ProgressBar;

.field public k:Landroid/widget/ListView;

.field public l:Ljava/util/ArrayList;

.field public m:Ljava/util/ArrayList;

.field public n:Ljava/util/ArrayList;

.field public p:Landroid/widget/LinearLayout;

.field public q:I

.field public t:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

.field public v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public w:Landroid/app/ProgressDialog;

.field public x:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->q:I

    const-string v0, ""

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->x:Ljava/lang/String;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->a0()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->M(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)Landroid/app/ProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d0(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c0(Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;)V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->O(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->T(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->R(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final J(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye1;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v0}, Lye1;->g()I

    move-result v2

    invoke-virtual {v1, v2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_0
    const-string v1, " - - - "

    :goto_1
    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final K(Ljava/util/ArrayList;)[[Ljava/lang/String;
    .locals 9

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    new-array v2, v0, [[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->L()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    move v3, v1

    :goto_0
    if-ge v3, v0, :cond_0

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    aput-object v6, v2, v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getRollNo()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    aget-object v6, v2, v3

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    aget-object v6, v2, v3

    const/4 v7, 0x2

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v7

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public final L()[Ljava/lang/String;
    .locals 3

    .line 1
    const-string v0, "Phone No"

    const-string v1, "Message"

    const-string v2, "Roll No"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final M(Ljava/lang/String;Ljava/lang/String;)V
    .locals 19

    .line 1
    move-object/from16 v1, p0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->e0(Lf61;)V

    iget-object v0, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->K(Ljava/util/ArrayList;)[[Ljava/lang/String;

    move-result-object v5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[^a-zA-Z0-9_-]"

    const-string v4, "_"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[^a-zA-Z0-9_]"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_Result"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v7, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    move v7, v2

    :goto_0
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;

    const/4 v4, 0x0

    move-object v2, v0

    move-object/from16 v3, p1

    move-object v6, v8

    invoke-direct/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;-><init>(Ljava/lang/String;I[[Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v2, Ln52;

    invoke-static {}, La91;->w()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v2}, Ln52;->b()Lee1;

    move-result-object v12

    new-instance v2, Lf61;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    new-instance v11, Lcom/ekodroid/omrevaluator/serializable/ResultData;

    iget-object v14, v0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->emailId:Ljava/lang/String;

    iget-object v15, v0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->data:[[Ljava/lang/String;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->subjectName:Ljava/lang/String;

    iget v0, v0, Lcom/ekodroid/omrevaluator/exams/models/MessageResultExcel;->appVersion:I

    move-object v13, v11

    move-object/from16 v16, v3

    move/from16 v17, v0

    move-object/from16 v18, p2

    invoke-direct/range {v13 .. v18}, Lcom/ekodroid/omrevaluator/serializable/ResultData;-><init>(Ljava/lang/String;[[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    iget-object v13, v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->x:Ljava/lang/String;

    new-instance v14, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct {v14, v1, v8, v3, v4}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v2

    invoke-direct/range {v9 .. v14}, Lf61;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/serializable/ResultData;Lee1;Ljava/lang/String;Lzg;)V

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->e0(Lf61;)V

    return-void
.end method

.method public final N(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;
    .locals 18

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v5, v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v6

    invoke-static {v6}, Lve1;->p(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v12

    invoke-static {v6}, Lve1;->q(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v13

    invoke-static {v6}, Lve1;->s(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v14

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v9

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v6

    invoke-static {v9, v10, v6}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v11

    new-instance v6, Lye1;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v8

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent()Z

    move-result v15

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced()Z

    move-result v16

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v17

    move-object v7, v6

    invoke-direct/range {v7 .. v17}, Lye1;-><init>(IDLjava/lang/String;IIIZZZ)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRankingMethod()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lve1;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->J(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    return-object v4
.end method

.method public final O(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmo1;

    invoke-virtual {p0, v1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->P(Lmo1;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final P(Lmo1;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;
    .locals 15

    .line 1
    invoke-virtual/range {p1 .. p1}, Lmo1;->f()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lok;->f:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->h()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v4, p2

    invoke-virtual {v4, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->g:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->h:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->j:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->k:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->l:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->g()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->m:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->e()D

    move-result-wide v5

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v7

    invoke-virtual/range {p1 .. p1}, Lmo1;->m()D

    move-result-wide v9

    div-double/2addr v5, v9

    invoke-static {v5, v6}, Lve1;->o(D)D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->n:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->e()D

    move-result-wide v5

    invoke-static {v5, v6}, Lve1;->o(D)D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->o:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->m()D

    move-result-wide v5

    invoke-static {v5, v6}, Lve1;->o(D)D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->p:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->q:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->d()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->r:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->n()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lmo1;->j()[Ljava/lang/String;

    move-result-object v5

    array-length v5, v5

    if-ge v4, v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lok;->v:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->j()[Ljava/lang/String;

    move-result-object v10

    aget-object v10, v10, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v5, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lok;->s:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->k()[D

    move-result-object v10

    aget-wide v11, v10, v4

    invoke-static {v11, v12}, Lve1;->o(D)D

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v5, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lok;->w:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->k()[D

    move-result-object v10

    aget-wide v11, v10, v4

    mul-double/2addr v11, v7

    invoke-virtual/range {p1 .. p1}, Lmo1;->l()[D

    move-result-object v10

    aget-wide v13, v10, v4

    div-double/2addr v11, v13

    invoke-static {v11, v12}, Lve1;->o(D)D

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    move v4, v6

    goto/16 :goto_0

    :cond_0
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lmo1;->j()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ge v2, v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->j()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->k()[D

    move-result-object v3

    aget-wide v5, v3, v2

    invoke-static {v5, v6}, Lve1;->o(D)D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lmo1;->l()[D

    move-result-object v3

    aget-wide v5, v3, v2

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    sget-object v2, Lok;->t:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->u:Ljava/lang/String;

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v3

    invoke-virtual {v3}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;

    invoke-virtual/range {p1 .. p1}, Lmo1;->h()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lmo1;->o()Z

    move-result v4

    invoke-direct {v2, v0, v3, v1, v4}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    return-object v2
.end method

.method public final Q(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 33

    .line 1
    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v3

    array-length v4, v3

    new-array v12, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_0

    aget-object v6, v3, v5

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v12, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v10, v5

    check-cast v10, Lye1;

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v5}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v5

    invoke-virtual {v10}, Lye1;->g()I

    move-result v6

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v5

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v10}, Lye1;->g()I

    move-result v7

    invoke-virtual {v3, v6, v7}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v17

    invoke-static {v2}, Lve1;->r(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v7

    invoke-static {v7}, Lve1;->g([[D)[D

    move-result-object v18

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v23

    invoke-static/range {v18 .. v18}, Lve1;->d([D)D

    move-result-wide v25

    if-nez v5, :cond_1

    new-instance v5, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v10}, Lye1;->g()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    const-string v29, ""

    const-string v30, ""

    const-string v31, ""

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v27, v5

    invoke-direct/range {v27 .. v32}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v8, v5

    new-instance v13, Lmo1;

    move-object v5, v13

    invoke-virtual {v10}, Lye1;->g()I

    move-result v6

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v10}, Lye1;->f()I

    move-result v14

    invoke-virtual {v10}, Lye1;->b()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10}, Lye1;->a()I

    move-result v19

    invoke-virtual {v10}, Lye1;->c()I

    move-result v20

    invoke-virtual {v10}, Lye1;->h()I

    move-result v21

    invoke-virtual {v10}, Lye1;->k()Z

    move-result v22

    move-wide/from16 v10, v25

    move-object/from16 v25, v12

    move-object v0, v13

    move-wide/from16 v12, v23

    move-object/from16 v16, v25

    invoke-direct/range {v5 .. v22}, Lmo1;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;DDILjava/lang/String;[Ljava/lang/String;[D[DIIIZ)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    move-object/from16 v12, v25

    goto/16 :goto_1

    :cond_2
    return-object v1
.end method

.method public final R(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsTemplateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "SMS_TEMPLATE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final S()V
    .locals 3

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    const v2, 0x7f130131

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    const v0, 0x7f0901e6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->p:Landroid/widget/LinearLayout;

    const v0, 0x7f09034b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d:Landroid/widget/Spinner;

    const v0, 0x7f090196

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->g:Landroid/widget/ImageButton;

    const v0, 0x7f0903c3

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->h:Landroid/widget/TextView;

    const v0, 0x7f0902cd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->j:Landroid/widget/ProgressBar;

    const v0, 0x7f0901a9

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->i:Landroid/widget/ImageButton;

    const v0, 0x7f09024d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->k:Landroid/widget/ListView;

    const v0, 0x7f0900da

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->e:Landroid/widget/Button;

    const v0, 0x7f0900dd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->f:Landroid/widget/Button;

    return-void
.end method

.method public final T(Ljava/util/ArrayList;)V
    .locals 2

    .line 1
    new-instance v0, Lo3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-direct {v0, v1, p1}, Lo3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->k:Landroid/widget/ListView;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final U()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getAllSmsTemplates()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lok;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "Default Medium"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "Default Subject Wise"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lok;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->getSmsTemplateName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lp3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-direct {v0, v2, v1}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d:Landroid/widget/Spinner;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->d:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final V()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->f:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$g;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final W()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->e:Landroid/widget/Button;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final X()V
    .locals 2

    .line 1
    const v0, 0x7f0900a3

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$k;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Y()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->g:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Z()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->k:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$l;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public final a0()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$m;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->i:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->j:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Ln52;->b()Lee1;

    move-result-object v1

    new-instance v2, Lva0;

    invoke-direct {v2, p0, v1, v0}, Lva0;-><init>(Landroid/content/Context;Lee1;Ly01;)V

    return-void
.end method

.method public final b0()V
    .locals 2

    .line 1
    const v0, 0x7f09044e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$e;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final c0(Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;)V
    .locals 4

    .line 1
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " Report"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "jid"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "@s.whatsapp.net"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "address"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "Share via"

    invoke-static {v0, p1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final d0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$i;

    invoke-direct {v1, p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$i;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    const v2, 0x7f1200ca

    const v3, 0x7f12019e

    const v4, 0x7f12029a

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final e0(Lf61;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    const v1, 0x7f1200aa

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    const v1, 0x7f120053

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$j;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$j;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Lf61;)V

    const/4 p1, -0x2

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final f0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->i:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$n;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$n;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final g0()V
    .locals 3

    .line 1
    new-instance v0, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object v0

    new-instance v1, Lq61;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$f;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$f;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)V

    invoke-direct {v1, v0, v2}, Lq61;-><init>(Lee1;Ly01;)V

    return-void
.end method

.method public final h0()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "user_country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "INDIA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->e:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->e:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const-string v1, "SMS_LINK_EXP"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, La91;->n()I

    move-result v1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->g0()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->a0()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c003d

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->x:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->N(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->Q(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->m:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->S()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->b0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->h0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->U()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->W()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->f0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->Z()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->X()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->V()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->h0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->U()V

    return-void
.end method

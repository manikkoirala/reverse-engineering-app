.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field isSmsSent:Z

.field message:Ljava/lang/String;

.field phoneNo:Ljava/lang/String;

.field rollNo:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->phoneNo:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->message:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->rollNo:I

    iput-boolean p4, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->isSmsSent:Z

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->phoneNo:Ljava/lang/String;

    return-object v0
.end method

.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->rollNo:I

    return v0
.end method

.method public isSmsSent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->isSmsSent:Z

    return v0
.end method

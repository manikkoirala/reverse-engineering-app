.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field sendEmailSummary:Z

.field smsAccount:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

.field smsMessages:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

.field status:[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->smsMessages:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->smsAccount:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->status:[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;

    iput-boolean p4, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->sendEmailSummary:Z

    return-void
.end method


# virtual methods
.method public getSmsAccount()Lcom/ekodroid/omrevaluator/serializable/SmsAccount;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->smsAccount:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    return-object v0
.end method

.method public getSmsMessages()[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->smsMessages:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    return-object v0
.end method

.method public getStatus()[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->status:[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;

    return-object v0
.end method

.method public isSendEmailSummary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->sendEmailSummary:Z

    return v0
.end method

.method public setSendEmailSummary(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->sendEmailSummary:Z

    return-void
.end method

.method public setSmsAccount(Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->smsAccount:Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    return-void
.end method

.method public setSmsMessages([Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->smsMessages:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    return-void
.end method

.method public setStatus([Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;->status:[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;

    return-void
.end method

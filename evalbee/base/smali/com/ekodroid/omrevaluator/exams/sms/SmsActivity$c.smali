.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->U()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    if-nez p3, :cond_0

    sget-object p1, Lok;->F:Ljava/lang/String;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p3, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->m:Ljava/util/ArrayList;

    invoke-static {p2, p3, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->G(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->H(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V

    return-void

    :cond_0
    const/4 p1, 0x1

    if-ne p3, p1, :cond_1

    sget-object p1, Lok;->H:Ljava/lang/String;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p3, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->m:Ljava/util/ArrayList;

    invoke-static {p2, p3, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->G(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->H(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V

    return-void

    :cond_1
    const/4 p1, 0x2

    if-ne p3, p1, :cond_2

    sget-object p1, Lok;->G:Ljava/lang/String;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p3, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->m:Ljava/util/ArrayList;

    invoke-static {p2, p3, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->G(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->H(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V

    return-void

    :cond_2
    const/4 p1, 0x3

    if-ne p3, p1, :cond_3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    sget-object p2, Lok;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->I(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;)V

    return-void

    :cond_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getSmsTemplate(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p3, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->m:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->getSmsString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p3, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->G(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$c;->b:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->n:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->H(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

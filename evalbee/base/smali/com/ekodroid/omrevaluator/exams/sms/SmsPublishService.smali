.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field public a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

.field public b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

.field public c:Landroid/content/SharedPreferences;

.field public d:Landroid/content/SharedPreferences$Editor;

.field public e:I

.field public f:Z

.field public g:Lp61;

.field public h:Z

.field public i:Landroid/content/BroadcastReceiver;

.field public j:Z

.field public k:Ly01;

.field public l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    const/16 v0, 0xa

    iput v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->e:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->f:Z

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->h:Z

    new-instance v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$a;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->i:Landroid/content/BroadcastReceiver;

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->j:Z

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->h:Z

    return p1
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->o(I)V

    return-void
.end method

.method public static synthetic d(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->k()I

    move-result p0

    return p0
.end method

.method public static synthetic e(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;III)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->p(III)V

    return-void
.end method

.method public static synthetic f(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->n(I)V

    return-void
.end method

.method public static synthetic g(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->r([Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V

    return-void
.end method

.method public static synthetic h(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->i()V

    return-void
.end method


# virtual methods
.method public final i()V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "REPORT_SENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public final j(ILcom/ekodroid/omrevaluator/serializable/SmsAccount;)V
    .locals 6

    .line 1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Sent Sms : "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_sentSMSCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Pending Sms : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    filled-new-array {p2, p1}, [Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - Publish Complete"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->q(I[Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " - Publish Complete, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, p1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    aget-object p1, p1, v1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    invoke-static {p2, p1, v0, v1}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-virtual {p1}, Landroid/app/Service;->stopSelf()V

    return-void
.end method

.method public final k()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_lastSMSIndexSent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final l(I[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;IZ)Lcom/ekodroid/omrevaluator/exams/sms/SmsData;
    .locals 8

    .line 1
    const-string v0, "emailKey"

    array-length v1, p2

    sub-int/2addr v1, p1

    if-le v1, p3, :cond_0

    goto :goto_0

    :cond_0
    move p3, v1

    :goto_0
    new-array v1, p3, [Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p3, :cond_1

    add-int v3, p1, v2

    aget-object v3, p2, v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :try_start_0
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p2

    invoke-virtual {p2}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v0}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-static {p3}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v2

    sget-object v3, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-eq v2, v3, :cond_3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_2
    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getUserRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v3

    sget-object v4, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v3, v4, :cond_2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getEmailId()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v0}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_3
    move-object v3, p2

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    const-string p3, "user_country"

    const-string v0, ""

    invoke-interface {p2, p3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    new-instance p2, Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    invoke-direct/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/serializable/SmsAccount;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    move-object p2, p1

    :goto_3
    new-instance p3, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

    invoke-direct {p3, v1, p2, p1, p4}, Lcom/ekodroid/omrevaluator/exams/sms/SmsData;-><init>([Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;[Lcom/ekodroid/omrevaluator/exams/sms/SmsStatus;Z)V

    return-object p3
.end method

.method public final m()[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;
    .locals 5

    .line 1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "/tempsms.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-int v0, v2

    new-array v2, v0, [B

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v1, 0x0

    invoke-virtual {v3, v2, v1, v0}, Ljava/io/BufferedInputStream;->read([BII)I

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    const-class v2, [Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    invoke-virtual {v0, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final n(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_lastSMSIndexSent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, -0x1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v0, p1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->d:Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->d:Landroid/content/SharedPreferences$Editor;

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final o(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_sentSMSCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->d:Landroid/content/SharedPreferences$Editor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    const-string v1, "count_sms"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v0, p1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "SMS_SENT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not yet implemented"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->d:Landroid/content/SharedPreferences$Editor;

    new-instance v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService$b;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->k:Ly01;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "STOP_SERVICE_PUBLISH_SMS_RESULT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->i:Landroid/content/BroadcastReceiver;

    const/4 v2, 0x2

    invoke-static {p0, v1, v0, v2}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->j:Z

    const/4 p3, 0x1

    if-eqz p2, :cond_0

    return p3

    :cond_0
    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->j:Z

    const-string p2, "EXAM_ID"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const-string p2, "SEND_SMS_SUMMARY"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->f:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->m()[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    if-eqz p1, :cond_1

    array-length p1, p1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->k()I

    move-result p2

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->p(III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->b:[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->r([Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V

    :cond_1
    return p3
.end method

.method public final p(III)V
    .locals 5

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "STOP_SERVICE_PUBLISH_SMS_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/high16 v2, 0x4000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    if-nez v1, :cond_0

    return-void

    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    const-string v4, "channel1"

    if-lt v2, v3, :cond_1

    const/4 v2, 0x4

    const-string v3, "publish result"

    invoke-static {v4, v3, v2}, Leh;->a(Ljava/lang/String;Ljava/lang/CharSequence;I)Landroid/app/NotificationChannel;

    move-result-object v2

    invoke-static {v1, v2}, Lfh;->a(Landroid/app/NotificationManager;Landroid/app/NotificationChannel;)V

    :cond_1
    new-instance v2, Lrz0$e;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-direct {v2, v3, v4}, Lrz0$e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v3, "EvalBee"

    invoke-virtual {v2, v3}, Lrz0$e;->k(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sent Sms : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrz0$e;->j(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lrz0$e;->B(J)Lrz0$e;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lrz0$e;->f(Z)Lrz0$e;

    move-result-object v2

    const v4, 0x7f0800cc

    invoke-virtual {v2, v4}, Lrz0$e;->v(I)Lrz0$e;

    move-result-object v2

    invoke-virtual {v2, p2, p3, v3}, Lrz0$e;->t(IIZ)Lrz0$e;

    move-result-object p2

    const p3, 0x7f0800ba

    const-string v2, "Cancel"

    invoke-virtual {p2, p3, v2, v0}, Lrz0$e;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2}, Lrz0$e;->b()Landroid/app/Notification;

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public final q(I[Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 1
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    const-string v3, "channel1"

    if-lt v1, v2, :cond_0

    const-string v1, "publish result"

    const/4 v2, 0x4

    invoke-static {v3, v1, v2}, Leh;->a(Ljava/lang/String;Ljava/lang/CharSequence;I)Landroid/app/NotificationChannel;

    move-result-object v1

    invoke-static {v0, v1}, Lfh;->a(Landroid/app/NotificationManager;Landroid/app/NotificationChannel;)V

    :cond_0
    new-instance v1, Lrz0$f;

    invoke-direct {v1}, Lrz0$f;-><init>()V

    array-length v2, p2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, p2, v4

    invoke-virtual {v1, v5}, Lrz0$f;->h(Ljava/lang/CharSequence;)Lrz0$f;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1, p3}, Lrz0$f;->i(Ljava/lang/CharSequence;)Lrz0$f;

    new-instance p2, Lrz0$e;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->a:Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-direct {p2, v2, v3}, Lrz0$e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "EvalBee"

    invoke-virtual {p2, v2}, Lrz0$e;->k(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2, p3}, Lrz0$e;->j(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object p2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lrz0$e;->B(J)Lrz0$e;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Lrz0$e;->f(Z)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2, v1}, Lrz0$e;->x(Lrz0$g;)Lrz0$e;

    move-result-object p2

    const p3, 0x7f0800cc

    invoke-virtual {p2, p3}, Lrz0$e;->v(I)Lrz0$e;

    move-result-object p2

    const/4 p3, 0x2

    invoke-static {p3}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object p3

    invoke-virtual {p2, p3}, Lrz0$e;->w(Landroid/net/Uri;)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2}, Lrz0$e;->b()Landroid/app/Notification;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public final r([Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;Lcom/ekodroid/omrevaluator/serializable/SmsAccount;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->k()I

    move-result v0

    array-length v1, p1

    if-ge v0, v1, :cond_1

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->h:Z

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->e:I

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->f:Z

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->l(I[Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;IZ)Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

    move-result-object p1

    new-instance p2, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, p0, v0}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p2}, Ln52;->b()Lee1;

    move-result-object p2

    new-instance v0, Lp61;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->k:Ly01;

    invoke-direct {v0, p1, p2, v1}, Lp61;-><init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsData;Lee1;Ly01;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->g:Lp61;

    return-void

    :cond_1
    :goto_0
    array-length p1, p1

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;->j(ILcom/ekodroid/omrevaluator/serializable/SmsAccount;)V

    return-void
.end method

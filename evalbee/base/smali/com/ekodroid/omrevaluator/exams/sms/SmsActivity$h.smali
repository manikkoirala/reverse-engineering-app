.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->M(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 3

    .line 1
    const/4 p1, 0x0

    if-eqz p3, :cond_0

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->C(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)Landroid/app/ProgressDialog;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    new-instance p2, Lhn1;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    check-cast p3, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".xls"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p2, v0, p3, v1}, Lhn1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string p3, "ExcelSMSDownloadSuccess"

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->C(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;)Landroid/app/ProgressDialog;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->c:Ljava/lang/String;

    invoke-static {p2, p3, v0}, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->D(Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity$h;->d:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsActivity;

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string p3, "ExcelSMSDownloadError"

    :goto_0
    invoke-virtual {p2, p3, p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

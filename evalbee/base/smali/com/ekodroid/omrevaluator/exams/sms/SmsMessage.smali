.class public Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field isSmsSent:Z

.field message:Ljava/lang/String;

.field phoneNo:Ljava/lang/String;

.field rollNo:I

.field templateId:I

.field unicode:Z


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->templateId:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->phoneNo:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getMessage()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->message:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->getRollNo()I

    move-result p2

    iput p2, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->rollNo:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsListDataModel;->isSmsSent()Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->isSmsSent:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->message:Ljava/lang/String;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->isEnglish(Ljava/lang/String;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->unicode:Z

    return-void
.end method

.method private static isEnglish(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    array-length v1, p0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-char v3, p0, v2

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    const/16 v4, 0x7f

    if-le v3, v4, :cond_1

    return v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x1

    return p0
.end method


# virtual methods
.method public getCredits()I
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->unicode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->message:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v0, v0, 0x46

    :goto_0
    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->message:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit16 v0, v0, 0xa0

    goto :goto_0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->phoneNo:Ljava/lang/String;

    return-object v0
.end method

.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->rollNo:I

    return v0
.end method

.method public isSmsSent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->isSmsSent:Z

    return v0
.end method

.method public isUnicode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->unicode:Z

    return v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->message:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->phoneNo:Ljava/lang/String;

    return-void
.end method

.method public setUnicode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/exams/sms/SmsMessage;->unicode:Z

    return-void
.end method

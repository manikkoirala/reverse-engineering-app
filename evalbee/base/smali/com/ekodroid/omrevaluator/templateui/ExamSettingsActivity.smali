.class public Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/LinearLayout;

.field public g:Landroid/widget/LinearLayout;

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Landroid/widget/TextView;

.field public p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

.field public t:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->G()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->S()V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->R()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->Q()V

    return-void
.end method


# virtual methods
.method public final G()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final H([Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;
    .locals 4

    .line 1
    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_2

    if-nez v2, :cond_1

    aget-object v3, p1, v1

    goto :goto_1

    :cond_1
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, p1, v2

    :goto_1
    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getStringLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final I(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/lang/String;
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQuestionSets()[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object p1

    if-nez v0, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_4

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getExamSet()I

    move-result v4

    if-lez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, p1, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQuestions()[I

    move-result-object v4

    if-eqz v4, :cond_3

    move v5, v2

    :goto_1
    array-length v6, v4

    if-ge v5, v6, :cond_3

    if-nez v5, :cond_2

    aget v6, v4, v2

    goto :goto_2

    :cond_2
    const-string v6, ", "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget v6, v4, v5

    :goto_2
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    const-string v4, "   "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final J()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final K()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$h;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->e:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$i;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$i;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->g:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final M()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->f:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$k;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$k;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;Lig0$f;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final N()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final O()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->h:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$b;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P()V
    .locals 1

    .line 1
    const v0, 0x7f0901e9

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->c:Landroid/widget/LinearLayout;

    const v0, 0x7f0901f7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f090201

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->d:Landroid/widget/LinearLayout;

    const v0, 0x7f0901f5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->e:Landroid/widget/LinearLayout;

    const v0, 0x7f090214

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->h:Landroid/widget/LinearLayout;

    const v0, 0x7f0901ff

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->f:Landroid/widget/LinearLayout;

    const v0, 0x7f0903c8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->i:Landroid/widget/TextView;

    const v0, 0x7f0903d2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->m:Landroid/widget/TextView;

    const v0, 0x7f0903d9

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->j:Landroid/widget/TextView;

    const v0, 0x7f0903d0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->k:Landroid/widget/TextView;

    const v0, 0x7f0903d7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->l:Landroid/widget/TextView;

    const v0, 0x7f0903f4

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->n:Landroid/widget/TextView;

    return-void
.end method

.method public final Q()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->J()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->N()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->K()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->M()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->O()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getHeaderProfileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getLabelProfileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->I(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->H([Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRankingMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final R()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$f;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    new-instance v1, Ltc0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-direct {v1, v2, v0}, Ltc0;-><init>(Landroid/content/Context;Ly01;)V

    return-void
.end method

.method public final S()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$g;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    new-instance v1, Lsi0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->q:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-direct {v1, v2, v0}, Lsi0;-><init>(Landroid/content/Context;Ly01;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c002a

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->P()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->Q()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

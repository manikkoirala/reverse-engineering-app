.class public Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lk5$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lc21;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->n:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-boolean v0, p1, Lc21;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->A(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)I

    move-result v0

    sub-int/2addr v0, v1

    aget-object v0, v2, v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p1, Lc21;->a:I

    sub-int/2addr v2, v1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iget-object p1, p1, Lc21;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->setPartialAnswerOptionKeys(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->A(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)I

    move-result v0

    sub-int/2addr v0, v1

    aget-object v0, v2, v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p1, Lc21;->a:I

    sub-int/2addr v2, v1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iget-object v2, p1, Lc21;->c:[Z

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->setMarkedValues([Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->A(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)I

    move-result v0

    sub-int/2addr v0, v1

    aget-object v0, v2, v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p1, Lc21;->a:I

    sub-int/2addr v2, v1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iget-object p1, p1, Lc21;->d:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->setMarkedPayload(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

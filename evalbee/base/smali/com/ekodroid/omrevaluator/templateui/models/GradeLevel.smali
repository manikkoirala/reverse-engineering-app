.class public Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private maxMarks:D

.field private minMarks:D

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->value:Ljava/lang/String;

    iput-wide p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->minMarks:D

    iput-wide p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->maxMarks:D

    return-void
.end method


# virtual methods
.method public getMaxMarks()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->maxMarks:D

    return-wide v0
.end method

.method public getMinMarks()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->minMarks:D

    return-wide v0
.end method

.method public getStringLabel()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getMinMarks()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getMaxMarks()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setMaxMarks(I)V
    .locals 2

    int-to-double v0, p1

    iput-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->maxMarks:D

    return-void
.end method

.method public setMinMarks(I)V
    .locals 2

    int-to-double v0, p1

    iput-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->minMarks:D

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->value:Ljava/lang/String;

    return-void
.end method

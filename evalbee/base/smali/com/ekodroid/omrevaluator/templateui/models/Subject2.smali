.class public Lcom/ekodroid/omrevaluator/templateui/models/Subject2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private sections:[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

.field private subName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->sections:[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    return-object v0
.end method

.method public getSubName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->subName:Ljava/lang/String;

    return-object v0
.end method

.method public setSections([Lcom/ekodroid/omrevaluator/templateui/models/Section2;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->sections:[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    return-void
.end method

.method public setSubName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->subName:Ljava/lang/String;

    return-void
.end method

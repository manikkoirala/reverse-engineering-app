.class public Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;
    }
.end annotation


# instance fields
.field public a:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:I

.field public h:D

.field public i:D

.field public j:Z


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;Ljava/lang/String;Ljava/lang/String;IIIDDZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->a:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->c:Ljava/lang/String;

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->e:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f:I

    iput p6, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g:I

    iput-wide p7, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h:D

    iput-wide p9, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i:D

    iput-object p12, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d:Ljava/lang/String;

    iput-boolean p11, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->j:Z

    return-void
.end method


# virtual methods
.method public a()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h:D

    return-wide v0
.end method

.method public b()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i:D

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->e:I

    return v0
.end method

.method public e()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->b:Ljava/lang/String;

    return-object v0
.end method

.method public i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->a:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->j:Z

    return v0
.end method

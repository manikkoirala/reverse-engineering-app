.class public Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private columnWidthInBubbles:[I

.field private columns:I

.field private rows:I


# direct methods
.method public constructor <init>(II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->columns:I

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->rows:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->columnWidthInBubbles:[I

    return-void
.end method


# virtual methods
.method public getColumnWidthInBubbles()[I
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->columnWidthInBubbles:[I

    return-object v0
.end method

.method public getColumns()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->columns:I

    return v0
.end method

.method public getRows()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->rows:I

    return v0
.end method

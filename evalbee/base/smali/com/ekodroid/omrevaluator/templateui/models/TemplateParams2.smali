.class public Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private examSets:I

.field private rollDigits:I

.field private subjects:[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;


# direct methods
.method public constructor <init>(II[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->rollDigits:I

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->examSets:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->subjects:[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    return-void
.end method


# virtual methods
.method public getExamSets()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->examSets:I

    return v0
.end method

.method public getRollDigits()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->rollDigits:I

    return v0
.end method

.method public getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->subjects:[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    return-object v0
.end method

.method public setExamSets(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->examSets:I

    return-void
.end method

.method public setRollDigits(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->rollDigits:I

    return-void
.end method

.method public setSubjects([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->subjects:[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    return-void
.end method

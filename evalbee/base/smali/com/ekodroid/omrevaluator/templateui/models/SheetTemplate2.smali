.class public Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private answerKeys:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

.field private answerOptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;",
            ">;"
        }
    .end annotation
.end field

.field public columnWidthInBubbles:[I

.field public columns:I

.field private examSetOption:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

.field private gradeLevels:[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

.field private headerProfile:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

.field private invalidQueMarks:Ljava/lang/String;

.field private invalidQuestionSets:[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

.field private invalidQuestions:[I

.field private labelProfile:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field private markerWidth:I

.field private name:Ljava/lang/String;

.field private pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

.field private pixelsInUnit:I

.field private rankingMethod:Ljava/lang/String;

.field public rows:I

.field private sectionsInSubject:[I

.field private sheetLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;",
            ">;"
        }
    .end annotation
.end field

.field private templateParams:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public templatePixelHeight:I

.field public templatePixelWidth:I

.field private templateVersion:I


# direct methods
.method public constructor <init>([Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;IILcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;",
            "II",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;",
            ">;",
            "Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->templateVersion:I

    const-string v0, ""

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->name:Ljava/lang/String;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pixelsInUnit:I

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->markerWidth:I

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->answerOptions:Ljava/util/ArrayList;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->sheetLabels:Ljava/util/ArrayList;

    iput-object p7, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->templateParams:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-direct {p0, p7}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSectionsArrayFromTemplateParams(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)[I

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->sectionsInSubject:[I

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->examSetOption:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    return-void
.end method

.method private getSectionsArrayFromTemplateParams(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)[I
    .locals 4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    array-length v0, v0

    new-array v1, v0, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v3

    array-length v3, v3

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private updatePagelayout()V
    .locals 5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->rows:I

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->columns:I

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->columnWidthInBubbles:[I

    invoke-direct {v1, v2, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;-><init>(II[I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    :cond_0
    return-void
.end method


# virtual methods
.method public getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->answerKeys:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    return-object v0
.end method

.method public getAnswerOptions()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->answerOptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getColumnWidthInBubbles(I)[I
    .locals 1

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->updatePagelayout()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->getColumnWidthInBubbles()[I

    move-result-object p1

    return-object p1
.end method

.method public getColumns(I)I
    .locals 1

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->updatePagelayout()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->getColumns()I

    move-result p1

    return p1
.end method

.method public getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->examSetOption:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    return-object v0
.end method

.method public getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->gradeLevels:[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    return-object v0
.end method

.method public getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->headerProfile:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    return-object v0
.end method

.method public getInvalidQueMarks()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->invalidQueMarks:Ljava/lang/String;

    return-object v0
.end method

.method public getInvalidQuestionSets()[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->invalidQuestionSets:[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    return-object v0
.end method

.method public getInvalidQuestions()[I
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->invalidQuestions:[I

    return-object v0
.end method

.method public getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->labelProfile:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->labelProfile:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->labelProfile:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    return-object v0
.end method

.method public getMarkerWidth()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->markerWidth:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;
    .locals 1

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->updatePagelayout()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    return-object v0
.end method

.method public getPixelsInUnit()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pixelsInUnit:I

    return v0
.end method

.method public getRankingMethod()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->rankingMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getRows(I)I
    .locals 1

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->updatePagelayout()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->getRows()I

    move-result p1

    return p1
.end method

.method public getSectionsInSubject()[I
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->sectionsInSubject:[I

    return-object v0
.end method

.method public getSheetLabels()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->sheetLabels:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->templateParams:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    return-object v0
.end method

.method public getTemplatePixelHeight(I)I
    .locals 1

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->updatePagelayout()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->getRows()I

    move-result p1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pixelsInUnit:I

    mul-int/2addr p1, v0

    return p1
.end method

.method public getTemplatePixelWidth(I)I
    .locals 1

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->updatePagelayout()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->getColumnWidthInBubbles()[I

    move-result-object p1

    invoke-static {p1}, Lve1;->e([I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pixelsInUnit:I

    mul-int/2addr p1, v0

    return p1
.end method

.method public getTemplateVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->templateVersion:I

    return v0
.end method

.method public setAnswerKeys([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->answerKeys:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    return-void
.end method

.method public setGradeLevels([Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->gradeLevels:[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    return-void
.end method

.method public setHeaderProfile(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->headerProfile:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    return-void
.end method

.method public setInvalidQueMarks(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->invalidQueMarks:Ljava/lang/String;

    return-void
.end method

.method public setInvalidQuestionSets([Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->invalidQuestionSets:[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    return-void
.end method

.method public setInvalidQuestions([I)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->invalidQuestions:[I

    return-void
.end method

.method public setLabelProfile(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->labelProfile:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->name:Ljava/lang/String;

    return-void
.end method

.method public setPageLayouts([Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->pageLayouts:[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    return-void
.end method

.method public setRankingMethod(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->rankingMethod:Ljava/lang/String;

    return-void
.end method

.method public setTemplateParams(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->templateParams:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    return-void
.end method

.method public setTemplateVersion(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->templateVersion:I

    return-void
.end method

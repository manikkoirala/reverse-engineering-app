.class public Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field markedPayload:Ljava/lang/String;

.field markedValues:[Z

.field partialMarks:D

.field partialType:Ljava/lang/String;

.field payload:Ljava/lang/String;

.field questionNumber:I

.field sectionId:I

.field subjectId:I

.field type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZIIDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->questionNumber:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->markedValues:[Z

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->subjectId:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->sectionId:I

    iput-wide p6, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->partialMarks:D

    iput-object p8, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->payload:Ljava/lang/String;

    iput-object p9, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->markedPayload:Ljava/lang/String;

    iput-object p10, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->partialType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getMarkedPayload()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->markedPayload:Ljava/lang/String;

    return-object v0
.end method

.method public getMarkedValues()[Z
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->markedValues:[Z

    return-object v0
.end method

.method public getPartialMarks()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->partialMarks:D

    return-wide v0
.end method

.method public getPartialType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->partialType:Ljava/lang/String;

    return-object v0
.end method

.method public getPayload()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->payload:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->questionNumber:I

    return v0
.end method

.method public getSectionId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->sectionId:I

    return v0
.end method

.method public getSubjectId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->subjectId:I

    return v0
.end method

.method public getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0
.end method

.method public setMarkedValues([Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->markedValues:[Z

    return-void
.end method

.method public setPartialMarks(D)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->partialMarks:D

    return-void
.end method

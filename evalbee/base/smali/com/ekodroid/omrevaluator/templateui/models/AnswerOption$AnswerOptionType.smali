.class public final enum Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnswerOptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum EIGHTOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum EXAMSET_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum FIVEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum ID_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum SIXOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum TENOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum THREEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

.field public static final enum TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 12

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FIVEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->THREEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v6, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->ID_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EXAMSET_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->SIXOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v10, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EIGHTOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v11, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TENOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    filled-new-array/range {v0 .. v11}, [Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "FOUROPTION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "FIVEOPTION"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FIVEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "THREEOPTION"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->THREEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "MATRIX"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "NUMARICAL"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "DECIMAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "ID_BLOCK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->ID_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "TRUEORFALSE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "EXAMSET_BLOCK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EXAMSET_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "SIXOPTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->SIXOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "EIGHTOPTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EIGHTOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const-string v1, "TENOPTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TENOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->$values()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0
.end method

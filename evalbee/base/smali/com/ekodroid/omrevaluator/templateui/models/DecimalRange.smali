.class public Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field lower:D

.field upper:D


# direct methods
.method public constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->lower:D

    iput-wide p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->upper:D

    return-void
.end method


# virtual methods
.method public getLower()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->lower:D

    return-wide v0
.end method

.method public getUpper()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->upper:D

    return-wide v0
.end method

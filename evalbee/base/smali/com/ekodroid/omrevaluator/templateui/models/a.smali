.class public Lcom/ekodroid/omrevaluator/templateui/models/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Landroid/graphics/Paint;

.field public e:Landroid/graphics/Paint;

.field public f:Landroid/graphics/Paint;

.field public g:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x64

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->b:I

    return-void
.end method


# virtual methods
.method public final A(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II[Ljava/lang/String;ILandroid/graphics/Paint;)V
    .locals 13

    .line 1
    move-object v6, p0

    move-object v7, p2

    move/from16 v8, p3

    mul-int/lit8 v0, v8, 0x3

    div-int/lit8 v9, v0, 0x5

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    mul-int/lit8 v1, v8, 0x2

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v10

    const/4 v11, 0x0

    move v12, v11

    :goto_0
    const/16 v0, 0xa

    if-ge v12, v0, :cond_0

    add-int v1, v12, p6

    rem-int/2addr v1, v0

    aget-object v2, p5, v1

    mul-int v0, v12, v8

    invoke-virtual {v10, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move v4, v9

    move-object/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_0
    mul-int/lit8 v0, v8, 0x5

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    move/from16 v1, p4

    :goto_1
    if-ge v11, v1, :cond_1

    mul-int v2, v11, v8

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual {p0, p1, v2, v8, v3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->d(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final B(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v4

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    iget v5, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    const/4 p2, 0x0

    move v1, p2

    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_1

    mul-int/lit8 v2, v4, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v3, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    add-int/lit8 v6, v1, 0x2

    mul-int/2addr v6, v4

    invoke-virtual {v2, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    move v6, p2

    :goto_1
    if-ge v6, v5, :cond_0

    mul-int v7, v6, v4

    invoke-virtual {v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v7, v0, v8}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollDigitsLabels()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollStartDigit()I

    move-result v7

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/ekodroid/omrevaluator/templateui/models/a;->A(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II[Ljava/lang/String;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final C(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 8

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v6, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    mul-int/lit8 v0, v0, 0x8

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final D(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x6

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final E(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSixOptionLabels()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v2, v0, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    const/4 v2, 0x6

    if-ge v8, v2, :cond_0

    aget-object v4, p3, v8

    mul-int v2, v8, v0

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final F(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x0

    :goto_0
    const/16 v6, 0xa

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final G(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTenOptionLabels()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v2, v0, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    const/16 v2, 0xa

    if-ge v8, v2, :cond_0

    aget-object v4, p3, v8

    mul-int v2, v8, v0

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V
    .locals 1

    .line 1
    if-nez p2, :cond_0

    return-void

    :cond_0
    int-to-float v0, p4

    invoke-virtual {p5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v0, p3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    int-to-float v0, v0

    iget p3, p3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    div-int/lit8 p4, p4, 0x2

    add-int/2addr p3, p4

    add-int/lit8 p3, p3, -0x2

    int-to-float p3, p3

    invoke-virtual {p1, p2, v0, p3, p5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final I(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 8

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v6, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p3

    mul-int/lit8 v0, v0, 0x8

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v4, p2, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->text:Ljava/lang/String;

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final J(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x3

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final K(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getThreeOptionLabels()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v2, v0, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    array-length v2, p3

    if-ge v8, v2, :cond_0

    aget-object v4, p3, v8

    mul-int v2, v8, v0

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final L(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x1

    :goto_0
    const/4 v6, 0x4

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x2

    goto :goto_0

    :cond_0
    div-int/2addr v3, v6

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final M(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v2, v0, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v2, 0x1

    move v8, v2

    :goto_0
    const/4 v2, 0x4

    if-ge v8, v2, :cond_0

    add-int/lit8 v2, v8, -0x1

    div-int/lit8 v2, v2, 0x2

    aget-object v4, p3, v2

    mul-int v2, v8, v0

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x2

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final N(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSheetLabels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->pageIndex:I

    if-ne v2, p3, :cond_0

    invoke-virtual {p0, p1, v1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->r(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final O(Ljava/lang/String;I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
    .locals 8

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->FULLWIDTH:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Page "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "   "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    const-string v3, "ExtraPage"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-object p1
.end method

.method public final P(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)I
    .locals 7

    .line 1
    const/4 v0, 0x0

    if-eqz p2, :cond_b

    iget-object v1, p2, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    goto :goto_1

    :cond_0
    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p1, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumns(I)I

    move-result p1

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move v3, p1

    move v1, v2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/a$a;->a:[I

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->size:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v4, v5, v4

    if-eq v4, v2, :cond_8

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_1

    goto :goto_0

    :cond_1
    if-eq v3, p1, :cond_2

    add-int/lit8 v1, v1, 0x1

    :cond_2
    move v3, v0

    goto :goto_0

    :cond_3
    if-le v3, v5, :cond_4

    add-int/lit8 v3, v3, -0x3

    goto :goto_0

    :cond_4
    if-eq v3, p1, :cond_5

    add-int/lit8 v1, v1, 0x1

    :cond_5
    add-int/lit8 v3, p1, -0x3

    goto :goto_0

    :cond_6
    if-le v3, v2, :cond_7

    add-int/lit8 v3, v3, -0x2

    goto :goto_0

    :cond_7
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, p1, -0x2

    goto :goto_0

    :cond_8
    if-lez v3, :cond_9

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_9
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, p1, -0x1

    goto :goto_0

    :cond_a
    return v1

    :cond_b
    :goto_1
    return v0
.end method

.method public final Q(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)I
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object v0

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->O(Ljava/lang/String;I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getEmptyHeight()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplatePixelWidth(I)I

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->V(Ljava/lang/String;I)Landroid/text/StaticLayout;

    move-result-object v1

    int-to-double v2, v2

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    int-to-double v4, v1

    const-wide v6, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    :cond_2
    int-to-double v1, v2

    invoke-virtual {p0, p1, v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;->P(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)I

    move-result v3

    int-to-double v3, v3

    const-wide/high16 v5, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v3, v5

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v5

    int-to-double v5, v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object v0

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplatePixelWidth(I)I

    move-result v2

    add-int/lit16 v2, v2, -0xfa

    int-to-double v2, v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result p1

    int-to-double v4, p1

    const-wide v6, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int p1, v2

    invoke-virtual {p0, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->R(Ljava/lang/String;I)Landroid/text/StaticLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result p1

    const/16 v0, 0x6e

    if-ge p1, v0, :cond_3

    move p1, v0

    :cond_3
    add-int/lit8 p1, p1, 0xa

    add-int/2addr v1, p1

    :cond_4
    return v1
.end method

.method public final R(Ljava/lang/String;I)Landroid/text/StaticLayout;
    .locals 4

    .line 1
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {p1, v3, v2, v0, p2}, Landroid/text/StaticLayout$Builder;->obtain(Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;

    move-result-object p1

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {p1, p2}, Landroid/text/StaticLayout$Builder;->setAlignment(Landroid/text/Layout$Alignment;)Landroid/text/StaticLayout$Builder;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/text/StaticLayout$Builder;->setIncludePad(Z)Landroid/text/StaticLayout$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/text/StaticLayout$Builder;->build()Landroid/text/StaticLayout;

    move-result-object p1

    return-object p1
.end method

.method public final S(IILcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 4

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p3, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object p3

    invoke-virtual {p0, p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->W([II)I

    move-result p2

    mul-int/2addr p2, v0

    add-int/2addr v1, p2

    iget p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    iget p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->b:I

    add-int/2addr p2, p3

    add-int/2addr p2, v2

    mul-int/lit8 p1, p1, 0x5

    mul-int/2addr p1, v0

    add-int/2addr p2, p1

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    invoke-direct {p1, v1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    return-object p1
.end method

.method public final T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 4

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object p2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;->W([II)I

    move-result p2

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    div-int/lit8 v2, v1, 0x2

    add-int v3, v0, v2

    mul-int/2addr p2, v1

    add-int/2addr v3, p2

    iget p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->b:I

    add-int/2addr v0, p2

    add-int/2addr v0, v2

    add-int/lit8 p1, p1, -0x1

    mul-int/2addr p1, v1

    add-int/2addr v0, p1

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    invoke-direct {p1, v3, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    return-object p1
.end method

.method public final U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 4

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->column:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->row:I

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object p2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;->W([II)I

    move-result p2

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    div-int/lit8 v2, v1, 0x2

    add-int v3, v0, v2

    mul-int/2addr p2, v1

    add-int/2addr v3, p2

    iget p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->b:I

    add-int/2addr v0, p2

    add-int/2addr v0, v2

    add-int/lit8 p1, p1, -0x1

    mul-int/2addr p1, v1

    add-int/2addr v0, p1

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    invoke-direct {p1, v3, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    return-object p1
.end method

.method public final V(Ljava/lang/String;I)Landroid/text/StaticLayout;
    .locals 4

    .line 1
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    const/high16 v1, 0x42200000    # 40.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {p1, v3, v2, v0, p2}, Landroid/text/StaticLayout$Builder;->obtain(Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;

    move-result-object p1

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p1, p2}, Landroid/text/StaticLayout$Builder;->setAlignment(Landroid/text/Layout$Alignment;)Landroid/text/StaticLayout$Builder;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/text/StaticLayout$Builder;->setIncludePad(Z)Landroid/text/StaticLayout$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/text/StaticLayout$Builder;->build()Landroid/text/StaticLayout;

    move-result-object p1

    return-object p1
.end method

.method public final W([II)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v0, p2, :cond_0

    aget v2, p1, v0

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;
    .locals 3

    .line 1
    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->g:Landroid/content/Context;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->d:Landroid/graphics/Paint;

    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->a(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Landroid/graphics/Bitmap;

    move-result-object p1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    invoke-virtual {p0, v0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->t(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->k(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->c(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->N(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)V

    if-nez p3, :cond_0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object p3

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->O(Ljava/lang/String;I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object p3

    :goto_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->p(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-object p1
.end method

.method public final a(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->Q(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->b:I

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplatePixelWidth(I)I

    move-result v0

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p1, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplatePixelHeight(I)I

    move-result p1

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr p1, v1

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->b:I

    add-int/2addr p1, v1

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, p1, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public final b(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/a$a;->c:[I

    iget-object v1, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    return-void

    :pswitch_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->F(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->i(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->D(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->J(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->L(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_5
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->h(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_6
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->x(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_7
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->B(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_8
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->u(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_9
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->l(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_a
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->n(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    if-ne v2, p3, :cond_0

    invoke-virtual {p0, p1, v1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->b(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final d(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V
    .locals 8

    .line 1
    div-int/lit8 p3, p3, 0x2

    add-int/lit8 p3, p3, -0x1

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    sub-int v1, v0, p3

    int-to-float v3, v1

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    sub-int v1, p2, p3

    int-to-float v4, v1

    add-int/2addr v0, p3

    int-to-float v5, v0

    add-int/2addr p2, p3

    int-to-float v6, p2

    move-object v2, p1

    move-object v7, p4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V
    .locals 1

    .line 1
    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    int-to-float v0, v0

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    int-to-float p2, p2

    int-to-float p3, p3

    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final f(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I[Ljava/lang/String;ZLandroid/graphics/Paint;)V
    .locals 8

    .line 1
    mul-int/lit8 v0, p3, 0x3

    div-int/lit8 v7, v0, 0x5

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    if-eqz p5, :cond_0

    const-string v2, "."

    neg-int p5, p3

    div-int/lit8 p5, p5, 0x2

    invoke-virtual {p2, p5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    mul-int/lit8 v4, v7, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    :cond_0
    const/4 p5, 0x0

    :goto_0
    const/16 v0, 0xa

    if-ge p5, v0, :cond_1

    aget-object v3, p4, p5

    mul-int v0, p5, p3

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move v5, v7

    move-object v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final g(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 18

    .line 1
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    iget-object v0, v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v10, v0, 0x2

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v11

    move-object/from16 v12, p3

    invoke-virtual {v6, v8, v12}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v13

    iget-object v4, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    iget v5, v9, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v13

    move v3, v11

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->y(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;I)V

    iget v0, v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    if-nez v0, :cond_0

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v4

    iget-object v5, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v13

    move v3, v11

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->w(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I[Ljava/lang/String;Landroid/graphics/Paint;)V

    :cond_0
    mul-int/lit8 v14, v11, 0x5

    div-int/lit8 v0, v14, 0x2

    invoke-virtual {v13, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    iget v3, v9, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    const/16 v5, 0xa

    if-ge v2, v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    mul-int v4, v2, v11

    invoke-virtual {v1, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    mul-int v5, v3, v11

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    iget-object v5, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v6, v7, v4, v10, v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    const/16 v5, 0xa

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v1, v9, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v1, :cond_3

    add-int/2addr v0, v11

    invoke-virtual {v13, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    iget-object v1, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v6, v7, v0, v10, v1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    const-string v2, "-"

    mul-int/lit8 v0, v11, 0x6

    div-int/lit8 v0, v0, 0x2

    int-to-double v0, v0

    int-to-double v3, v11

    const-wide v16, 0x3fd3333333333333L    # 0.3

    mul-double v3, v3, v16

    sub-double/2addr v0, v3

    double-to-int v0, v0

    invoke-virtual {v13, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    mul-int/lit8 v1, v11, -0x1

    int-to-double v3, v1

    mul-double v3, v3, v16

    double-to-int v1, v3

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    mul-int/lit8 v4, v10, 0x5

    iget-object v5, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/16 v15, 0xa

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    goto :goto_2

    :cond_3
    const/16 v15, 0xa

    :goto_2
    iget v0, v9, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    if-lez v0, :cond_4

    iget v1, v9, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    sub-int/2addr v1, v0

    mul-int/lit8 v0, v11, 0x4

    div-int/lit8 v0, v0, 0x2

    mul-int/2addr v1, v11

    add-int/2addr v0, v1

    invoke-virtual {v13, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    int-to-double v1, v11

    const-wide v3, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v15, :cond_4

    mul-int v2, v1, v11

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v6, v7, v2, v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    div-int/lit8 v14, v14, 0x4

    invoke-virtual {v13, v14}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    iget v1, v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v2

    mul-int/lit8 v4, v10, 0x2

    iget-object v5, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final h(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 17

    .line 1
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v0

    const/4 v10, 0x0

    const/4 v11, 0x2

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    invoke-direct {v0, v10, v11, v10}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;-><init>(ZIZ)V

    :cond_0
    move-object v12, v0

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v13, v0, 0x2

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v14

    move-object/from16 v15, p3

    invoke-virtual {v7, v9, v15}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    iget v1, v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/lit8 v1, v1, 0x4

    mul-int/2addr v1, v14

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v4, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    iget v5, v12, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v6

    move v3, v14

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->y(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;I)V

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, v12, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    iget-object v3, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v16, v3

    move v3, v14

    move-object v10, v6

    move-object/from16 v6, v16

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->f(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I[Ljava/lang/String;ZLandroid/graphics/Paint;)V

    mul-int/lit8 v6, v14, 0x5

    div-int/lit8 v0, v6, 0x2

    invoke-virtual {v10, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    invoke-virtual {v1, v14}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    iget-boolean v2, v12, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v2, :cond_1

    const/16 v2, 0xb

    goto :goto_0

    :cond_1
    const/16 v2, 0xa

    :goto_0
    const/4 v3, 0x0

    :goto_1
    iget v4, v12, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    if-ge v3, v4, :cond_3

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v2, :cond_2

    mul-int v5, v3, v14

    invoke-virtual {v1, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    mul-int v11, v4, v14

    invoke-virtual {v5, v11}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v11, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v7, v8, v5, v13, v11}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v4, v4, 0x1

    const/4 v11, 0x2

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    const/4 v11, 0x2

    goto :goto_1

    :cond_3
    iget-boolean v1, v12, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    if-eqz v1, :cond_4

    add-int/2addr v0, v14

    invoke-virtual {v10, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    iget-object v1, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v7, v8, v0, v13, v1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    const-string v2, "-"

    mul-int/lit8 v0, v14, 0x6

    const/4 v1, 0x2

    div-int/2addr v0, v1

    int-to-double v0, v0

    int-to-double v3, v14

    const-wide v11, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v3, v11

    sub-double/2addr v0, v3

    double-to-int v0, v0

    invoke-virtual {v10, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    mul-int/lit8 v14, v14, -0x1

    int-to-double v3, v14

    mul-double/2addr v3, v11

    double-to-int v1, v3

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    mul-int/lit8 v4, v13, 0x5

    iget-object v5, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    :cond_4
    div-int/lit8 v6, v6, 0x4

    invoke-virtual {v10, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    iget v1, v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    mul-int/lit8 v4, v13, 0x2

    iget-object v5, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final i(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x0

    :goto_0
    const/16 v6, 0x8

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final j(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getEightOptionLabels()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v2, v0, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    const/16 v2, 0x8

    if-ge v8, v2, :cond_0

    aget-object v4, p3, v8

    mul-int v2, v8, v0

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final k(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)V
    .locals 12

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    if-eq v1, p3, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object p3

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v1

    iget v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v3

    mul-int/lit8 v4, v3, 0x3

    div-int/lit8 v4, v4, 0x5

    invoke-virtual {p0, v0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    mul-int/lit8 v5, v3, 0x5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {p2, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v5, 0x0

    move v11, v5

    :goto_0
    iget v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    if-ge v11, v5, :cond_1

    rem-int v5, v11, v1

    mul-int/2addr v5, v3

    invoke-virtual {p2, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    div-int v6, v11, v1

    mul-int/2addr v6, v3

    mul-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v5, v2, v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    aget-object v7, p3, v11

    iget-object v10, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v5, p0

    move-object v6, p1

    move v9, v4

    invoke-virtual/range {v5 .. v10}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public final l(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x5

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final m(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    const/4 v2, 0x5

    div-int/2addr v1, v2

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFiveOptionLabels()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v3, v0, 0x5

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v3, 0x0

    move v9, v3

    :goto_0
    if-ge v9, v2, :cond_0

    aget-object v5, p3, v9

    mul-int v3, v9, v0

    invoke-virtual {p2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v3, p0

    move-object v4, p1

    move v7, v1

    invoke-virtual/range {v3 .. v8}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final n(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 10

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v1

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    mul-int/lit8 v3, v1, 0x5

    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x4

    if-ge v5, v6, :cond_0

    mul-int v6, v5, v1

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v6, v0, v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    div-int/2addr v3, v6

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v7

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v6

    mul-int/lit8 v8, v0, 0x2

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v4, p0

    move-object v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final o(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFourOptionLabels()[Ljava/lang/String;

    move-result-object p3

    mul-int/lit8 v2, v0, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    const/4 v2, 0x4

    if-ge v8, v2, :cond_0

    aget-object v4, p3, v8

    mul-int v2, v8, v0

    invoke-virtual {p2, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final p(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 18

    .line 1
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v6, p3

    if-nez v6, :cond_0

    return-void

    :cond_0
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const v0, -0x777778

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget v0, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    const/4 v12, 0x2

    div-int/2addr v0, v12

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getEmptyHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getTitle()Ljava/lang/String;

    move-result-object v1

    const-wide v2, 0x3fe3333333333333L    # 0.6

    if-eqz v1, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget v4, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {v9, v4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplatePixelWidth(I)I

    move-result v4

    int-to-double v4, v4

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v13

    int-to-double v13, v13

    mul-double/2addr v13, v2

    sub-double/2addr v4, v13

    double-to-int v4, v4

    invoke-virtual {v7, v1, v4}, Lcom/ekodroid/omrevaluator/templateui/models/a;->V(Ljava/lang/String;I)Landroid/text/StaticLayout;

    move-result-object v1

    iget v4, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    int-to-float v4, v4

    int-to-float v5, v0

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v1, v8}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    iget v4, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    neg-int v4, v4

    int-to-float v4, v4

    neg-int v5, v0

    int-to-float v5, v5

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    int-to-double v4, v0

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v0

    int-to-double v0, v0

    const-wide v13, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v0, v13

    add-double/2addr v4, v0

    double-to-int v0, v4

    :cond_1
    move v13, v0

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object v0

    const/4 v14, 0x0

    if-eqz v0, :cond_3

    iget-object v0, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080121

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v1, 0xc8

    const/16 v4, 0x6e

    invoke-static {v0, v1, v4, v14}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object v1

    iget v5, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {v9, v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplatePixelWidth(I)I

    move-result v5

    add-int/lit16 v5, v5, -0xfa

    int-to-double v14, v5

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v5

    int-to-double v11, v5

    mul-double/2addr v11, v2

    sub-double/2addr v14, v11

    double-to-int v2, v14

    invoke-virtual {v7, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->R(Ljava/lang/String;I)Landroid/text/StaticLayout;

    move-result-object v1

    iget v2, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    int-to-double v2, v2

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v5

    int-to-double v11, v5

    const-wide v14, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v11, v14

    add-double/2addr v2, v11

    double-to-int v2, v2

    int-to-float v3, v2

    int-to-float v5, v13

    invoke-virtual {v8, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v1, v8}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    neg-int v2, v2

    int-to-float v2, v2

    neg-int v3, v13

    int-to-float v3, v3

    invoke-virtual {v8, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    iget v3, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    sub-int/2addr v2, v3

    add-int/lit16 v2, v2, -0xfa

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v2, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v0

    if-ge v0, v4, :cond_2

    move v11, v4

    goto :goto_0

    :cond_2
    move v11, v0

    :goto_0
    iget v0, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    int-to-float v1, v0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget v2, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    sub-int/2addr v0, v2

    int-to-float v3, v0

    add-int v0, v13, v11

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    move-object/from16 v0, p1

    move v2, v5

    move-object v5, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v11, v11, 0xa

    add-int/2addr v13, v11

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v11

    invoke-virtual {v7, v9, v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->P(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)I

    move-result v12

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v12, :cond_4

    iget v1, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    const/4 v2, 0x2

    mul-int/2addr v1, v2

    div-int/2addr v1, v2

    int-to-float v1, v1

    int-to-double v2, v13

    int-to-double v4, v11

    const-wide/high16 v14, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v14

    int-to-double v14, v0

    mul-double/2addr v14, v4

    add-double/2addr v14, v2

    double-to-float v14, v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v15

    iget v8, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    sub-int/2addr v15, v8

    int-to-float v8, v15

    add-int/lit8 v15, v0, 0x1

    move/from16 v16, v11

    move/from16 v17, v12

    int-to-double v11, v15

    mul-double/2addr v4, v11

    add-double/2addr v2, v4

    double-to-float v4, v2

    move-object/from16 v0, p1

    move v2, v14

    move v3, v8

    move-object v5, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v8, p1

    move v0, v15

    move/from16 v11, v16

    move/from16 v12, v17

    goto :goto_1

    :cond_4
    iget v0, v7, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {v9, v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumns(I)I

    move-result v8

    iget-object v0, v6, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    return-void

    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v11, v8

    const/4 v12, 0x1

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/a$a;->a:[I

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->size:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    const/4 v2, 0x2

    if-eq v1, v2, :cond_b

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8

    const/4 v2, 0x4

    if-eq v1, v2, :cond_6

    :goto_3
    const/4 v14, 0x2

    :goto_4
    const/4 v15, 0x1

    goto :goto_2

    :cond_6
    if-eq v11, v8, :cond_7

    add-int/lit8 v12, v12, 0x1

    :cond_7
    const/4 v3, 0x0

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    const/4 v11, 0x0

    goto :goto_3

    :cond_8
    const/4 v14, 0x2

    if-le v11, v14, :cond_9

    sub-int v3, v8, v11

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    add-int/lit8 v11, v11, -0x3

    goto :goto_4

    :cond_9
    if-eq v11, v8, :cond_a

    add-int/lit8 v12, v12, 0x1

    :cond_a
    const/4 v3, 0x0

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    add-int/lit8 v11, v8, -0x3

    goto :goto_4

    :cond_b
    move v14, v2

    const/4 v15, 0x1

    if-le v11, v15, :cond_c

    sub-int v3, v8, v11

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    add-int/lit8 v11, v11, -0x2

    goto :goto_2

    :cond_c
    add-int/lit8 v12, v12, 0x1

    const/4 v3, 0x0

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    add-int/lit8 v11, v8, -0x2

    goto/16 :goto_2

    :cond_d
    move v15, v2

    const/4 v14, 0x2

    if-lez v11, :cond_e

    sub-int v3, v8, v11

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_2

    :cond_e
    add-int/lit8 v12, v12, 0x1

    const/4 v3, 0x0

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move v4, v13

    move-object/from16 v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    add-int/lit8 v11, v8, -0x1

    goto/16 :goto_2

    :cond_f
    return-void
.end method

.method public final q(Landroid/graphics/Canvas;IIILjava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 18

    .line 1
    move-object/from16 v6, p0

    move/from16 v7, p2

    move/from16 v8, p4

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v0, -0x1000000

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-virtual/range {p6 .. p6}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x4

    div-int/lit8 v4, v1, 0x5

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v0

    double-to-int v10, v2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    iget v3, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    move-object/from16 v5, p6

    invoke-virtual {v5, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumns(I)I

    move-result v3

    div-int/2addr v2, v3

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v5, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    mul-int v11, v2, p3

    add-int/2addr v5, v11

    int-to-double v12, v5

    const-wide v14, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v0, v14

    add-double/2addr v12, v0

    double-to-int v0, v12

    int-to-double v1, v8

    int-to-double v12, v10

    int-to-double v14, v7

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    add-double/2addr v1, v12

    double-to-int v1, v1

    invoke-direct {v3, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v5, v9

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    if-lez p3, :cond_0

    iget v0, v6, Lcom/ekodroid/omrevaluator/templateui/models/a;->a:I

    add-int v1, v0, v11

    int-to-float v1, v1

    int-to-float v2, v8

    add-int/lit8 v3, v7, -0x1

    mul-int/2addr v3, v10

    int-to-float v3, v3

    add-float/2addr v3, v2

    add-int/2addr v0, v11

    int-to-float v0, v0

    mul-int/2addr v10, v7

    int-to-float v4, v10

    add-float/2addr v2, v4

    move/from16 p2, v1

    move/from16 p3, v3

    move/from16 p4, v0

    move/from16 p5, v2

    move-object/from16 p6, v9

    invoke-virtual/range {p1 .. p6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public final r(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/a$a;->b:[I

    iget-object v1, p2, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->type:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    return-void

    :pswitch_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->G(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->j(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->E(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->I(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->C(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_5
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->z(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_6
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->K(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_7
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->m(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_8
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->M(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_9
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->o(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final s(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I)V
    .locals 8

    .line 1
    div-int/lit8 p3, p3, 0x2

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    sub-int v1, v0, p3

    int-to-float v3, v1

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    sub-int v1, p2, p3

    int-to-float v4, v1

    add-int/2addr v0, p3

    int-to-float v5, v0

    add-int/2addr p2, p3

    int-to-float v6, p2

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->d:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final t(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 7

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRows(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->c:I

    invoke-virtual {p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumns(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v0, :cond_1

    move v5, v3

    :goto_1
    if-ge v5, v1, :cond_0

    invoke-virtual {p0, v4, v5, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->S(IILcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v6

    invoke-virtual {p0, p1, v6, v2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->s(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final u(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 16

    .line 1
    move-object/from16 v8, p0

    move-object/from16 v6, p2

    iget-object v0, v6, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v0

    const/4 v7, 0x4

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    move v10, v0

    move v9, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    move v10, v0

    move v9, v7

    :goto_0
    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v11, v0, 0x2

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v12

    move-object/from16 v13, p3

    invoke-virtual {v8, v6, v13}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v14

    iget-object v4, v8, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v14

    move v3, v12

    move v5, v10

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->y(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v9, :cond_2

    mul-int/lit8 v2, v12, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v14, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    mul-int v3, v12, v1

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    move v3, v0

    :goto_2
    if-ge v3, v10, :cond_1

    mul-int v4, v3, v12

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    iget-object v5, v8, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    move-object/from16 v15, p1

    invoke-virtual {v8, v15, v4, v11, v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    move-object/from16 v15, p1

    goto :goto_1

    :cond_2
    move-object/from16 v15, p1

    mul-int/lit8 v0, v12, 0x5

    div-int/2addr v0, v7

    invoke-virtual {v14, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    iget v1, v6, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v2

    mul-int/lit8 v4, v11, 0x2

    iget-object v5, v8, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    iget-object v7, v8, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, v14

    move v3, v12

    move v4, v9

    move v5, v10

    move-object/from16 v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->v(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IIILcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Landroid/graphics/Paint;)V

    return-void
.end method

.method public final v(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IIILcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Landroid/graphics/Paint;)V
    .locals 15

    .line 1
    move-object/from16 v0, p2

    mul-int/lit8 v1, p3, 0x3

    div-int/lit8 v8, v1, 0x5

    invoke-virtual/range {p6 .. p6}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixSecondary()[Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p6 .. p6}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixPrimary()[Ljava/lang/String;

    move-result-object v10

    mul-int/lit8 v2, p3, 0x5

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v11

    const/4 v12, 0x0

    move/from16 v13, p5

    move v14, v12

    :goto_0
    if-ge v14, v13, :cond_0

    aget-object v4, v9, v14

    mul-int v2, v14, p3

    invoke-virtual {v11, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    move-object v2, p0

    move-object/from16 v3, p1

    move v6, v8

    move-object/from16 v7, p7

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_0
    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    move/from16 v1, p4

    :goto_1
    if-ge v12, v1, :cond_1

    aget-object v4, v10, v12

    add-int/lit8 v12, v12, 0x1

    mul-int v2, v12, p3

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    move-object v2, p0

    move-object/from16 v3, p1

    move v6, v8

    move-object/from16 v7, p7

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final w(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I[Ljava/lang/String;Landroid/graphics/Paint;)V
    .locals 8

    .line 1
    mul-int/lit8 v0, p3, 0x3

    div-int/lit8 v7, v0, 0x5

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    aget-object v3, p4, v0

    mul-int v1, v0, p3

    invoke-virtual {p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move v5, v7

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final x(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    iget-object v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    iget-boolean v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->g(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void

    :cond_1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getMarkerWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v7

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->T(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v8

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    const/4 v6, 0x5

    move-object v1, p0

    move-object v2, p1

    move-object v3, v8

    move v4, v7

    invoke-virtual/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->y(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;I)V

    iget v1, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    if-nez v1, :cond_2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v1, p0

    move-object v2, p1

    move-object v3, v8

    move v4, v7

    invoke-virtual/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/a;->w(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I[Ljava/lang/String;Landroid/graphics/Paint;)V

    :cond_2
    mul-int/lit8 v1, v7, 0x5

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v8, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0xa

    if-ge v3, v4, :cond_3

    iget v4, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/2addr v4, v7

    invoke-virtual {v2, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    mul-int v5, v3, v7

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->e:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v4, v0, v5}, Lcom/ekodroid/omrevaluator/templateui/models/a;->e(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget v2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/2addr v2, v7

    add-int/2addr v1, v2

    invoke-virtual {v8, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v4

    mul-int/lit8 v6, v0, 0x2

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

.method public final y(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;I)V
    .locals 7

    .line 1
    mul-int/lit8 v0, p3, 0x4

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    div-int/lit8 v0, p3, 0x2

    neg-int v0, v0

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    int-to-float v2, v0

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    int-to-float v3, p2

    mul-int/2addr p5, p3

    add-int/2addr v0, p5

    int-to-float v4, v0

    int-to-float v5, p2

    move-object v1, p1

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final z(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 8

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPixelsInUnit()I

    move-result v0

    mul-int/lit8 v1, v0, 0x3

    div-int/lit8 v6, v1, 0x5

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->U(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    mul-int/lit8 v0, v0, 0x8

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v5

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/a;->f:Landroid/graphics/Paint;

    move-object v2, p0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/a;->H(Landroid/graphics/Canvas;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ILandroid/graphics/Paint;)V

    return-void
.end method

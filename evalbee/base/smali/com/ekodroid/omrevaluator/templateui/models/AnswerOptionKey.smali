.class public Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field markedPayload:Ljava/lang/String;

.field markedValues:[Z

.field partialAnswerOptionKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;",
            ">;"
        }
    .end annotation
.end field

.field payload:Ljava/lang/String;

.field questionNumber:I

.field sectionId:I

.field subjectId:I

.field type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;",
            "I[Z",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->questionNumber:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedValues:[Z

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->subjectId:I

    iput p6, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->sectionId:I

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedPayload:Ljava/lang/String;

    iput-object p7, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->partialAnswerOptionKeys:Ljava/util/ArrayList;

    iput-object p8, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->payload:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->questionNumber:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedValues:[Z

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->subjectId:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->sectionId:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->partialAnswerOptionKeys:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->payload:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getMarkedPayload()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedPayload:Ljava/lang/String;

    return-object v0
.end method

.method public getMarkedValues()[Z
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedValues:[Z

    return-object v0
.end method

.method public getPartialAnswerOptionKeys()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->partialAnswerOptionKeys:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPayload()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->payload:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->questionNumber:I

    return v0
.end method

.method public getSectionId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->sectionId:I

    return v0
.end method

.method public getSubjectId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->subjectId:I

    return v0
.end method

.method public getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0
.end method

.method public setMarkedPayload(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedPayload:Ljava/lang/String;

    return-void
.end method

.method public setMarkedValues([Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->markedValues:[Z

    return-void
.end method

.method public setPartialAnswerOptionKeys(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->partialAnswerOptionKeys:Ljava/util/ArrayList;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    }
.end annotation


# instance fields
.field public column:I

.field public correctMarks:D

.field public incorrectMarks:D

.field public pageIndex:I

.field public partialAllowed:Z

.field public partialMarks:D

.field public payload:Ljava/lang/String;

.field public questionNumber:I

.field public row:I

.field public sectionId:I

.field public subIndex:I

.field public subjectId:I

.field public type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    iput p6, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iput p7, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    iput-wide p8, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    iput-wide p10, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    iput-boolean p12, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    iput-object p13, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    iput p14, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public primaryOptions:I

.field public secondaryOptions:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/models/Section2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field name:Ljava/lang/String;

.field negetiveMark:D

.field noOfOptionalQue:I

.field noOfQue:I

.field optionalAllowed:Z

.field optionalType:Ljava/lang/String;

.field partialAllowed:Z

.field partialMarks:D

.field payload:Ljava/lang/String;

.field positiveMark:D

.field type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNegetiveMark()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    return-wide v0
.end method

.method public getNoOfOptionalQue()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfOptionalQue:I

    return v0
.end method

.method public getNoOfQue()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfQue:I

    return v0
.end method

.method public getOptionalType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->optionalType:Ljava/lang/String;

    return-object v0
.end method

.method public getPartialMarks()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialMarks:D

    return-wide v0
.end method

.method public getPayload()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    return-object v0
.end method

.method public getPositiveMark()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    return-wide v0
.end method

.method public getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0
.end method

.method public isOptionalAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->optionalAllowed:Z

    return v0
.end method

.method public isPartialAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    return v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->name:Ljava/lang/String;

    return-void
.end method

.method public setNegetiveMark(D)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    return-void
.end method

.method public setNoOfOptionalQue(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfOptionalQue:I

    return-void
.end method

.method public setNoOfQue(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfQue:I

    return-void
.end method

.method public setOptionalAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->optionalAllowed:Z

    return-void
.end method

.method public setOptionalType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->optionalType:Ljava/lang/String;

    return-void
.end method

.method public setPartialAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    return-void
.end method

.method public setPayload(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    return-void
.end method

.method public setPositiveMark(D)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    return-void
.end method

.method public setType(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-void
.end method

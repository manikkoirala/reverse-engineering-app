.class public final enum Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LableType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_ABC:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_ABCD:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_ABCDE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_ABCDEF:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_ABCDEFGH:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_ABCDEFGHIJ:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum OPTION_TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum ROLL_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum SET_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

.field public static final enum TEXT:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
    .locals 10

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->TEXT:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCD:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABC:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->ROLL_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v6, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->SET_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v7, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEF:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v8, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGH:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    sget-object v9, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGHIJ:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    filled-new-array/range {v0 .. v9}, [Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "TEXT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->TEXT:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_ABCD"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCD:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_ABC"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABC:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_ABCDE"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_TRUEORFALSE"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "ROLL_LABEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->ROLL_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "SET_LABEL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->SET_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_ABCDEF"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEF:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_ABCDEFGH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGH:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const-string v1, "OPTION_ABCDEFGHIJ"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGHIJ:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->$values()[Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object v0
.end method

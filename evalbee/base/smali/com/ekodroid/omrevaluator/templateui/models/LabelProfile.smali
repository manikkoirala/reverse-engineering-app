.class public Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private ReportCardString:Ljava/lang/String;

.field private attemptedString:Ljava/lang/String;

.field private classString:Ljava/lang/String;

.field private correctAnswerString:Ljava/lang/String;

.field private correctString:Ljava/lang/String;

.field private dateString:Ljava/lang/String;

.field private eightOptionLabels:[Ljava/lang/String;

.field private examNameString:Ljava/lang/String;

.field private examSetLabels:[Ljava/lang/String;

.field private examSetString:Ljava/lang/String;

.field private fiveOptionLabels:[Ljava/lang/String;

.field private fourOptionLabels:[Ljava/lang/String;

.field private gradeString:Ljava/lang/String;

.field private incorrectAnswerString:Ljava/lang/String;

.field private labelProfileName:Ljava/lang/String;

.field private marksString:Ljava/lang/String;

.field private matrixPrimary:[Ljava/lang/String;

.field private matrixSecondary:[Ljava/lang/String;

.field private nameString:Ljava/lang/String;

.field private numericalOptionLabels:[Ljava/lang/String;

.field private percentageString:Ljava/lang/String;

.field private queNoString:Ljava/lang/String;

.field private questionNumberLabels:[Ljava/lang/String;

.field private rankString:Ljava/lang/String;

.field private rollDigitsLabels:[Ljava/lang/String;

.field private rollNoString:Ljava/lang/String;

.field private rollStartDigit:I

.field private sixOptionLabels:[Ljava/lang/String;

.field private subjectString:Ljava/lang/String;

.field private tenOptionLabels:[Ljava/lang/String;

.field private threeOptionLabels:[Ljava/lang/String;

.field private totalMarksString:Ljava/lang/String;

.field private trueOrFalseLabel:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 15

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Default"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->labelProfileName:Ljava/lang/String;

    const-string v0, "Roll No"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollNoString:Ljava/lang/String;

    const-string v0, "Exam Set"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examSetString:Ljava/lang/String;

    const-string v0, "Exam"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examNameString:Ljava/lang/String;

    const-string v0, "Date"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->dateString:Ljava/lang/String;

    const-string v0, "Name"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->nameString:Ljava/lang/String;

    const-string v0, "Rank"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rankString:Ljava/lang/String;

    const-string v0, "Marks"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->marksString:Ljava/lang/String;

    const-string v0, "Grade"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->gradeString:Ljava/lang/String;

    const-string v0, "Class"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->classString:Ljava/lang/String;

    const-string v0, "Percentage"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->percentageString:Ljava/lang/String;

    const-string v0, "Correct Answers"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->correctAnswerString:Ljava/lang/String;

    const-string v0, "Incorrect Answers"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->incorrectAnswerString:Ljava/lang/String;

    const-string v0, "Subject"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->subjectString:Ljava/lang/String;

    const-string v0, "Report Card"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->ReportCardString:Ljava/lang/String;

    const-string v0, "Q No"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->queNoString:Ljava/lang/String;

    const-string v0, "Attempted"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->attemptedString:Ljava/lang/String;

    const-string v0, "Correct"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->correctString:Ljava/lang/String;

    const-string v0, "Total Marks"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->totalMarksString:Ljava/lang/String;

    const-string v0, "E"

    const-string v1, "A"

    const-string v2, "B"

    const-string v3, "C"

    const-string v4, "D"

    filled-new-array {v1, v2, v3, v4, v0}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->fiveOptionLabels:[Ljava/lang/String;

    const-string v5, "A"

    const-string v6, "B"

    const-string v7, "C"

    const-string v8, "D"

    const-string v9, "E"

    const-string v10, "F"

    filled-new-array/range {v5 .. v10}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->sixOptionLabels:[Ljava/lang/String;

    const-string v5, "A"

    const-string v6, "B"

    const-string v7, "C"

    const-string v8, "D"

    const-string v9, "E"

    const-string v10, "F"

    const-string v11, "G"

    const-string v12, "H"

    filled-new-array/range {v5 .. v12}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->eightOptionLabels:[Ljava/lang/String;

    const-string v5, "A"

    const-string v6, "B"

    const-string v7, "C"

    const-string v8, "D"

    const-string v9, "E"

    const-string v10, "F"

    const-string v11, "G"

    const-string v12, "H"

    const-string v13, "I"

    const-string v14, "J"

    filled-new-array/range {v5 .. v14}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->tenOptionLabels:[Ljava/lang/String;

    filled-new-array {v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->fourOptionLabels:[Ljava/lang/String;

    filled-new-array {v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->threeOptionLabels:[Ljava/lang/String;

    const-string v0, "True"

    const-string v1, "False"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->trueOrFalseLabel:[Ljava/lang/String;

    const-string v1, "A"

    const-string v2, "B"

    const-string v3, "C"

    const-string v4, "D"

    const-string v5, "E"

    const-string v6, "F"

    const-string v7, "G"

    const-string v8, "H"

    const-string v9, "I"

    const-string v10, "J"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->matrixPrimary:[Ljava/lang/String;

    const-string v1, "P"

    const-string v2, "Q"

    const-string v3, "R"

    const-string v4, "S"

    const-string v5, "T"

    const-string v6, "U"

    const-string v7, "V"

    const-string v8, "W"

    const-string v9, "X"

    const-string v10, "Y"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->matrixSecondary:[Ljava/lang/String;

    const-string v1, "A"

    const-string v2, "B"

    const-string v3, "C"

    const-string v4, "D"

    const-string v5, "E"

    const-string v6, "F"

    const-string v7, "G"

    const-string v8, "H"

    const-string v9, "I"

    const-string v10, "J"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examSetLabels:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getDefaultquestionLabels()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->questionNumberLabels:[Ljava/lang/String;

    const-string v1, "0"

    const-string v2, "1"

    const-string v3, "2"

    const-string v4, "3"

    const-string v5, "4"

    const-string v6, "5"

    const-string v7, "6"

    const-string v8, "7"

    const-string v9, "8"

    const-string v10, "9"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->numericalOptionLabels:[Ljava/lang/String;

    const-string v1, "0"

    const-string v2, "1"

    const-string v3, "2"

    const-string v4, "3"

    const-string v5, "4"

    const-string v6, "5"

    const-string v7, "6"

    const-string v8, "7"

    const-string v9, "8"

    const-string v10, "9"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollDigitsLabels:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollStartDigit:I

    return-void
.end method

.method private getDefaultquestionLabels()[Ljava/lang/String;
    .locals 5

    const/16 v0, 0x136

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    move v2, v4

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getMatrixCombineLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;
    .locals 8

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p1

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    const/4 p1, 0x5

    :goto_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixPrimary()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixSecondary()[Ljava/lang/String;

    move-result-object v2

    mul-int/2addr v0, p1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v0, :cond_1

    div-int v5, v4, p1

    rem-int v6, v4, p1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v1, v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v2, v6

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    return-object v3
.end method

.method private getNumericalCombinationLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;
    .locals 8

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object p1

    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/lit8 v0, v0, 0xa

    iget-boolean v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    new-array v2, v0, [Ljava/lang/String;

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    const-string v3, "-"

    aput-object v3, v2, v1

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    div-int v4, v1, v3

    rem-int v3, v1, v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v6

    iget v7, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    sub-int/2addr v7, v3

    aget-object v3, v6, v7

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "x"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v2
.end method


# virtual methods
.method public getAttemptedString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->attemptedString:Ljava/lang/String;

    return-object v0
.end method

.method public getClassString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->classString:Ljava/lang/String;

    return-object v0
.end method

.method public getCorrectAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->correctAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getCorrectString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->correctString:Ljava/lang/String;

    return-object v0
.end method

.method public getDateString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->dateString:Ljava/lang/String;

    return-object v0
.end method

.method public getEightOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->eightOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getExamNameString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examNameString:Ljava/lang/String;

    return-object v0
.end method

.method public getExamSetLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examSetLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getExamSetString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examSetString:Ljava/lang/String;

    return-object v0
.end method

.method public getFiveOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->fiveOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getFourOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->fourOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getGradeString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->gradeString:Ljava/lang/String;

    return-object v0
.end method

.method public getIncorrectAnswerString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->incorrectAnswerString:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelProfileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->labelProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMarksString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->marksString:Ljava/lang/String;

    return-object v0
.end method

.method public getMatrixPrimary()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->matrixPrimary:[Ljava/lang/String;

    return-object v0
.end method

.method public getMatrixSecondary()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->matrixSecondary:[Ljava/lang/String;

    return-object v0
.end method

.method public getNameString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->nameString:Ljava/lang/String;

    return-object v0
.end method

.method public getNumericalOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->numericalOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getOptionLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile$a;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    return-object p1

    :pswitch_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTenOptionLabels()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getEightOptionLabels()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSixOptionLabels()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalCombinationLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixCombineLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_5
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_6
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getThreeOptionLabels()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_7
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFourOptionLabels()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_8
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFiveOptionLabels()[Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getPercentageString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->percentageString:Ljava/lang/String;

    return-object v0
.end method

.method public getQueNoString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->queNoString:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionNumberLabel(I)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->questionNumberLabels:[Ljava/lang/String;

    array-length v1, v0

    if-le p1, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    aget-object p1, v0, p1

    return-object p1
.end method

.method public getQuestionNumberLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->questionNumberLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getRankString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rankString:Ljava/lang/String;

    return-object v0
.end method

.method public getReportCardString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->ReportCardString:Ljava/lang/String;

    return-object v0
.end method

.method public getRollDigitsLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollDigitsLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getRollNoString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollNoString:Ljava/lang/String;

    return-object v0
.end method

.method public getRollStartDigit()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollStartDigit:I

    return v0
.end method

.method public getSixOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->sixOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getSubjectString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->subjectString:Ljava/lang/String;

    return-object v0
.end method

.method public getTenOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->tenOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getThreeOptionLabels()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->threeOptionLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getTotalMarksString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->totalMarksString:Ljava/lang/String;

    return-object v0
.end method

.method public getTrueOrFalseLabel()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->trueOrFalseLabel:[Ljava/lang/String;

    return-object v0
.end method

.method public setAttemptedString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->attemptedString:Ljava/lang/String;

    return-void
.end method

.method public setClassString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->classString:Ljava/lang/String;

    return-void
.end method

.method public setCorrectAnswerString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->correctAnswerString:Ljava/lang/String;

    return-void
.end method

.method public setCorrectString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->correctString:Ljava/lang/String;

    return-void
.end method

.method public setDateString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->dateString:Ljava/lang/String;

    return-void
.end method

.method public setEightOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->eightOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setExamNameString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examNameString:Ljava/lang/String;

    return-void
.end method

.method public setExamSetLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examSetLabels:[Ljava/lang/String;

    return-void
.end method

.method public setExamSetString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->examSetString:Ljava/lang/String;

    return-void
.end method

.method public setFiveOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->fiveOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setFourOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->fourOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setGradeString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->gradeString:Ljava/lang/String;

    return-void
.end method

.method public setIncorrectAnswerString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->incorrectAnswerString:Ljava/lang/String;

    return-void
.end method

.method public setLabelProfileName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->labelProfileName:Ljava/lang/String;

    return-void
.end method

.method public setMarksString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->marksString:Ljava/lang/String;

    return-void
.end method

.method public setMatrixPrimary([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->matrixPrimary:[Ljava/lang/String;

    return-void
.end method

.method public setMatrixSecondary([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->matrixSecondary:[Ljava/lang/String;

    return-void
.end method

.method public setNameString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->nameString:Ljava/lang/String;

    return-void
.end method

.method public setNumericalOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->numericalOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setPercentageString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->percentageString:Ljava/lang/String;

    return-void
.end method

.method public setQueNoString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->queNoString:Ljava/lang/String;

    return-void
.end method

.method public setQuestionNumberLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->questionNumberLabels:[Ljava/lang/String;

    return-void
.end method

.method public setRankString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rankString:Ljava/lang/String;

    return-void
.end method

.method public setReportCardString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->ReportCardString:Ljava/lang/String;

    return-void
.end method

.method public setRollDigitsLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollDigitsLabels:[Ljava/lang/String;

    return-void
.end method

.method public setRollNoString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollNoString:Ljava/lang/String;

    return-void
.end method

.method public setRollStartDigit(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->rollStartDigit:I

    return-void
.end method

.method public setSixOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->sixOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setSubjectString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->subjectString:Ljava/lang/String;

    return-void
.end method

.method public setTenOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->tenOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setThreeOptionLabels([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->threeOptionLabels:[Ljava/lang/String;

    return-void
.end method

.method public setTotalMarksString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->totalMarksString:Ljava/lang/String;

    return-void
.end method

.method public setTrueOrFalseLabel([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->trueOrFalseLabel:[Ljava/lang/String;

    return-void
.end method

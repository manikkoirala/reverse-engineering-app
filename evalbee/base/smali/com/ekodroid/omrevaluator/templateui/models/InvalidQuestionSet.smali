.class public Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field examSet:I

.field private invalidQueMarks:Ljava/lang/String;

.field private invalidQuestions:[I


# direct methods
.method public constructor <init>(I[ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->examSet:I

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->invalidQuestions:[I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->invalidQueMarks:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getExamSet()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->examSet:I

    return v0
.end method

.method public getInvalidQueMarks()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->invalidQueMarks:Ljava/lang/String;

    return-object v0
.end method

.method public getInvalidQuestions()[I
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->invalidQuestions:[I

    return-object v0
.end method

.method public setExamSet(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->examSet:I

    return-void
.end method

.method public setInvalidQueMarks(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->invalidQueMarks:Ljava/lang/String;

    return-void
.end method

.method public setInvalidQuestions([I)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->invalidQuestions:[I

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public decimalDigits:I

.field public digits:I

.field public hasNegetive:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ZII)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    return-void
.end method

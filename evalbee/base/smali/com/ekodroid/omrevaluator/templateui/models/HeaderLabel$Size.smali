.class public final enum Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Size"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

.field public static final enum FULLWIDTH:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

.field public static final enum LARGE:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

.field public static final enum MEDIUM:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

.field public static final enum SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;
    .locals 4

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->MEDIUM:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->LARGE:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->FULLWIDTH:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    filled-new-array {v0, v1, v2, v3}, [Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v1, "SMALL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v1, "MEDIUM"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->MEDIUM:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v1, "LARGE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->LARGE:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v1, "FULLWIDTH"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->FULLWIDTH:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->$values()[Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object v0
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public decimalAllowed:Z

.field public digits:I

.field public hasNegetive:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ZIZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    return-void
.end method

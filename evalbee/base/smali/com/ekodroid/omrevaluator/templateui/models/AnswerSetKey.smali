.class public Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private answerOptionKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;",
            ">;"
        }
    .end annotation
.end field

.field private answerValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;"
        }
    .end annotation
.end field

.field private examSet:I


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->answerOptionKeys:Ljava/util/ArrayList;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->examSet:I

    return-void
.end method


# virtual methods
.method public getAnswerOptionKeys()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->answerOptionKeys:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAnswerValue2s()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->answerValues:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExamSet()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->examSet:I

    return v0
.end method

.method public setAnswerOptionKeys(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->answerOptionKeys:Ljava/util/ArrayList;

    return-void
.end method

.method public setAnswerValue2s(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->answerValues:Ljava/util/ArrayList;

    return-void
.end method

.method public setExamSet(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->examSet:I

    return-void
.end method

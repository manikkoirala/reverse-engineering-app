.class public Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
    }
.end annotation


# instance fields
.field column:I

.field pageIndex:I

.field row:I

.field text:Ljava/lang/String;

.field type:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->text:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->row:I

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->column:I

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->type:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->pageIndex:I

    return-void
.end method


# virtual methods
.method public getColumn()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->column:I

    return v0
.end method

.method public getRow()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->row:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->type:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object v0
.end method

.method public setColumn(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->column:I

    return-void
.end method

.method public setRow(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->row:I

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->text:Ljava/lang/String;

    return-void
.end method

.method public setType(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->type:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-void
.end method

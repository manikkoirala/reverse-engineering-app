.class public Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private emptyHeight:I

.field private headerProfileName:Ljava/lang/String;

.field private instruction:Ljava/lang/String;

.field labels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->emptyHeight:I

    const-string v0, "Default"

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->headerProfileName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->instruction:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->headerProfileName:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->title:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->instruction:Ljava/lang/String;

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->emptyHeight:I

    return-void
.end method

.method public static getBlankHeaderprofile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    const-string v1, "Blank"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x3c

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v6
.end method

.method public static getDefaultHeaderprofile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->MEDIUM:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v3, "NAME"

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    const-string v3, "EXAM"

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v3, "DATE"

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    sget-object v1, Lok;->a:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v6
.end method

.method public static getDefaultMediumHeaderprofile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->MEDIUM:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v3, "NAME"

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    const-string v3, "EXAM"

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    const-string v3, "DATE"

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "Instruction for filling the sheet\n  1. This sheet should not be folded or crushed\n  2. Use only blue/black ball pen or 2HB pencil\n  3. Circle should darkened completely and properly\n  4. Erase marked circle completely for deselect"

    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    const-string v1, "Default Medium"

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v6
.end method


# virtual methods
.method public getEmptyHeight()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->emptyHeight:I

    return v0
.end method

.method public getHeaderProfileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->headerProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getInstruction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->instruction:Ljava/lang/String;

    return-object v0
.end method

.method public getLabels()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->labels:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setEmptyHeight(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->emptyHeight:I

    return-void
.end method

.method public setInstruction(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->instruction:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->title:Ljava/lang/String;

    return-void
.end method

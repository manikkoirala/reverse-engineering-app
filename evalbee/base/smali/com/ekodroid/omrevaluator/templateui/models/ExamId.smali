.class public Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private className:Ljava/lang/String;

.field private examDate:Ljava/lang/String;

.field private examName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->className:Ljava/lang/String;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->examName:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->examDate:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getExamDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->examDate:Ljava/lang/String;

    return-object v0
.end method

.method public getExamName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->examName:Ljava/lang/String;

    return-object v0
.end method

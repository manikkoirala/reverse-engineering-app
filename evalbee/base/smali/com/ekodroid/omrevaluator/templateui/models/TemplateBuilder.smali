.class public Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

.field public e:Ljava/util/ArrayList;

.field public f:Ljava/util/ArrayList;

.field public g:I

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

.field public i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public j:Ljava/util/ArrayList;

.field public k:I


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->g:I

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a:I

    new-array v1, v0, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    const/16 v3, 0x8

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j:Ljava/util/ArrayList;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->b:I

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a:I

    invoke-direct {v2, v3, v4, v1}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;-><init>(II[I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lwu1;)Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
    .locals 5

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lwu1;->a:I

    iget v2, p1, Lwu1;->c:I

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    :goto_0
    if-nez v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->m(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v0

    iget v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, p1, Lwu1;->d:I

    if-gt v2, v3, :cond_1

    iget v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v4, p1, Lwu1;->b:I

    if-le v2, v4, :cond_0

    :cond_1
    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v4, p1, Lwu1;->b:I

    invoke-direct {v2, v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method public c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
    .locals 9

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->f:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iput v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iput v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->b:I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a()V

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iput v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->g:I

    if-nez p2, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->d(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)Z

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result p1

    if-le p1, v3, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result p1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v3

    invoke-virtual {p0, v2, p1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->s(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result p1

    invoke-virtual {p0, v2, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->t(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    :goto_0
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {p0, v2, p1, v1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->x(Lcom/ekodroid/omrevaluator/templateui/models/Subject2;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j:Ljava/util/ArrayList;

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->n(Ljava/util/ArrayList;)[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v2

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->c:I

    mul-int/lit8 p2, v3, 0x2

    div-int/lit8 v4, p2, 0x3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->f:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-object v1, p1

    invoke-direct/range {v1 .. v8}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;-><init>([Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;IILcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    :try_end_0
    .catch Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public final d(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->o(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)I

    move-result v0

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iput v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->b:I

    add-int/lit8 v0, v0, -0x5

    mul-int/2addr v1, p1

    if-gt v0, v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException;

    invoke-direct {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException;-><init>()V

    throw p1
.end method

.method public final e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I
    .locals 1

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->b:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    sub-int/2addr v0, p1

    return v0
.end method

.method public final f(Lcom/ekodroid/omrevaluator/templateui/models/Section2;)I
    .locals 3

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$a;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v1, 0xc

    const/4 v2, 0x3

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    return v2

    :pswitch_1
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object p1

    iget-boolean p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz p1, :cond_0

    const/16 v1, 0xd

    :cond_0
    add-int/lit8 v1, v1, 0x1

    :pswitch_2
    return v1

    :pswitch_3
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p1

    if-eqz p1, :cond_1

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    goto :goto_0

    :cond_1
    const/4 p1, 0x4

    :goto_0
    add-int/lit8 p1, p1, 0x2

    return p1

    :pswitch_4
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final g(Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object p1

    const/4 v0, 0x0

    aget-object p1, p1, v0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->f(Lcom/ekodroid/omrevaluator/templateui/models/Section2;)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final h()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->g:I

    return v0
.end method

.method public final i(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ZZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    const/16 p2, 0xd

    goto :goto_0

    :cond_0
    const/16 p2, 0xc

    :goto_0
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v0

    if-ge v0, p2, :cond_1

    invoke-virtual {p0, p1, p3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public final j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public final k(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Lcom/ekodroid/omrevaluator/templateui/models/Section2;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 1

    .line 1
    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    invoke-static {p2}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p2

    if-eqz p2, :cond_0

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x5

    :goto_0
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v0

    if-ge v0, p2, :cond_1

    invoke-virtual {p0, p1, p3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public final l(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public final m(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 14

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    int-to-double v1, v0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v1, v3

    iget v5, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    add-int/lit8 v6, v5, 0x1

    int-to-double v6, v6

    const-wide/high16 v8, 0x4020000000000000L    # 8.0

    mul-double/2addr v6, v8

    add-double/2addr v6, v3

    div-double/2addr v1, v6

    const/4 v6, 0x5

    add-int/2addr v0, v6

    int-to-double v10, v0

    mul-double/2addr v10, v3

    int-to-double v12, v5

    mul-double/2addr v12, v8

    add-double/2addr v12, v3

    div-double/2addr v10, v12

    const-wide v3, 0x3ff6a0ea0ea0ea0fL    # 1.4142857142857144

    cmpl-double v0, v1, v3

    if-lez v0, :cond_0

    div-double/2addr v1, v3

    goto :goto_0

    :cond_0
    div-double v1, v3, v1

    :goto_0
    cmpl-double v0, v10, v3

    if-lez v0, :cond_1

    div-double/2addr v10, v3

    goto :goto_1

    :cond_1
    div-double v10, v3, v10

    :goto_1
    cmpg-double v0, v1, v10

    if-gez v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/util/ArrayList;)[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public o(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)I
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object p1

    array-length v0, p1

    const/16 v1, 0xc

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {p0, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->q(Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public final p(Lcom/ekodroid/omrevaluator/templateui/models/Section2;)I
    .locals 2

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$a;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return v1

    :pswitch_1
    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfQue:I

    sub-int/2addr p1, v1

    div-int/lit8 p1, p1, 0x5

    add-int/2addr p1, v1

    mul-int/lit8 p1, p1, 0xb

    :goto_0
    add-int/2addr p1, v1

    return p1

    :pswitch_2
    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfQue:I

    mul-int/lit8 p1, p1, 0x5

    goto :goto_0

    :pswitch_3
    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfQue:I

    add-int/lit8 v0, p1, 0x1

    div-int/lit8 p1, p1, 0x5

    add-int/2addr p1, v1

    add-int/2addr v0, p1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final q(Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {p0, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->p(Lcom/ekodroid/omrevaluator/templateui/models/Section2;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public final r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 4

    .line 1
    const/4 v0, 0x0

    iput v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a:I

    if-le v1, v3, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->a()V

    iget p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    add-int/2addr p1, v2

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    invoke-direct {p1, v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    return-object p1

    :cond_0
    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException;

    invoke-direct {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException;-><init>()V

    throw p1

    :cond_1
    return-object p1
.end method

.method public final s(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 20

    .line 1
    move-object/from16 v0, p0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->SET_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    const/4 v2, 0x0

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v3, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    const/4 v2, 0x5

    move/from16 v3, p2

    if-ge v3, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    new-instance v15, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EXAMSET_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const/4 v5, 0x0

    iget v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    add-int/lit8 v6, v3, 0x1

    iget v7, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    iget v8, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move-object v3, v15

    move/from16 v18, v8

    move/from16 v8, p3

    move-object/from16 v19, v15

    move/from16 v15, v16

    move-object/from16 v16, v17

    move/from16 v17, v18

    invoke-direct/range {v3 .. v17}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    move-object/from16 v3, v19

    iput-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    add-int/lit8 v3, p3, -0x1

    div-int/2addr v3, v2

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    return-object v1
.end method

.method public final t(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 20

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    add-int/lit8 v3, v2, 0xc

    iget v4, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->b:I

    if-gt v3, v4, :cond_0

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->f:Ljava/util/ArrayList;

    new-instance v10, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->ROLL_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    add-int/lit8 v6, v2, 0x1

    iget v7, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v8, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    const/4 v9, 0x0

    move-object v4, v10

    invoke-direct/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v15, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->ID_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const/4 v5, 0x0

    iget v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    add-int/lit8 v6, v3, 0x1

    iget v7, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    iget v8, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move-object v3, v15

    move/from16 v18, v8

    move/from16 v8, p2

    move-object/from16 v19, v15

    move/from16 v15, v16

    move-object/from16 v16, v17

    move/from16 v17, v18

    invoke-direct/range {v3 .. v17}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    move-object/from16 v3, v19

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v4, p2

    invoke-virtual {v0, v2, v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    return-object v1

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException;

    invoke-direct {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException;-><init>()V

    throw v1
.end method

.method public final u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->f:Ljava/util/ArrayList;

    new-instance v7, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    iget v1, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    const/4 v8, 0x1

    add-int/lit8 v3, v1, 0x1

    iget v4, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v5, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move-object v1, v7

    move-object v2, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v8}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p1

    return-object p1
.end method

.method public final v(Lcom/ekodroid/omrevaluator/templateui/models/Section2;ILcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IIZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 37

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p6

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$a;->a:[I

    iget-object v5, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const/16 v6, 0xa

    const/4 v7, 0x0

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x1

    const/4 v11, 0x5

    packed-switch v4, :pswitch_data_0

    return-object v2

    :pswitch_0
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_0

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_0

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_1

    :cond_0
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_1

    if-nez p2, :cond_2

    :cond_1
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGHIJ:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_2
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TENOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v9

    iget v10, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v11, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget-wide v14, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v12, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v5, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v7, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v21, v7

    move-object v7, v4

    move-wide/from16 v17, v12

    const/4 v12, 0x0

    move/from16 v13, p4

    move-wide v15, v14

    move/from16 v14, p5

    move/from16 v19, v5

    move-object/from16 v20, v1

    invoke-direct/range {v7 .. v21}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v6, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_1
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_3

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_3

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_4

    :cond_3
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_4

    if-nez p2, :cond_5

    :cond_4
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGH:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_5
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EIGHTOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v6

    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v8, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/4 v9, 0x0

    iget-wide v12, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v15, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v4, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v18, v4

    move-object v4, v14

    move-wide/from16 v16, v10

    move/from16 v10, p4

    move/from16 v11, p5

    move-object/from16 v22, v14

    move/from16 v19, v15

    move-wide/from16 v14, v16

    move/from16 v16, v19

    move-object/from16 v17, v1

    invoke-direct/range {v4 .. v18}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    move-object/from16 v1, v22

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v3, 0x8

    iget v4, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_2
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_6

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_6

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_7

    :cond_6
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_7

    if-nez p2, :cond_8

    :cond_7
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEF:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_8
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->SIXOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v8

    iget v9, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v10, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/4 v11, 0x0

    iget-wide v14, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v12, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v18, v6

    move-object v6, v4

    move-wide/from16 v16, v12

    move/from16 v12, p4

    move/from16 v13, p5

    move-object/from16 v19, v1

    move/from16 v20, v5

    invoke-direct/range {v6 .. v20}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    const/4 v4, 0x6

    invoke-virtual {v0, v1, v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_3
    iget-object v4, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    invoke-static {v4}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v4

    iget-boolean v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v5, :cond_9

    const/16 v6, 0xb

    :cond_9
    iget v7, v4, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    const/4 v8, 0x2

    if-gt v7, v8, :cond_b

    rem-int/lit8 v27, p2, 0x2

    if-nez v27, :cond_a

    goto :goto_0

    :cond_a
    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v23, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v24

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    sub-int v25, v5, v6

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v11, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move-object/from16 v22, v4

    move/from16 v26, v5

    move/from16 v28, p4

    move/from16 v29, p5

    move-wide/from16 v30, v6

    move-wide/from16 v32, v8

    move/from16 v34, v10

    move-object/from16 v35, v1

    move/from16 v36, v11

    invoke-direct/range {v22 .. v36}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    const/4 v4, 0x6

    invoke-virtual {v0, v1, v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :cond_b
    :goto_0
    invoke-virtual {v0, v2, v5, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->i(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;ZZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget v3, v4, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    if-le v3, v11, :cond_c

    move v11, v3

    :cond_c
    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v7, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v9, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v12, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    move/from16 v27, v6

    iget v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v24, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v7

    move-wide/from16 v22, v9

    move-object/from16 v25, v1

    move/from16 v26, v6

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    move/from16 v6, v27

    invoke-virtual {v2, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    return-object v1

    :pswitch_4
    iget-object v4, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    invoke-static {v4}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v4

    if-eqz v4, :cond_f

    iget-boolean v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-nez v5, :cond_d

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-le v5, v10, :cond_f

    :cond_d
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->l(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget v3, v4, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-le v3, v11, :cond_e

    move v11, v3

    :cond_e
    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v7, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v9, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v12, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v24, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v7

    move-wide/from16 v22, v9

    move-object/from16 v25, v1

    move/from16 v26, v6

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    const/16 v1, 0xa

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    return-object v1

    :cond_f
    rem-int/lit8 v17, p2, 0x5

    if-nez v17, :cond_10

    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->l(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    return-object v1

    :cond_10
    const/16 v3, 0xa

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    add-int/lit8 v15, v6, -0xa

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v5

    move/from16 v16, v3

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_5
    invoke-virtual {v0, v2, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Lcom/ekodroid/omrevaluator/templateui/models/Section2;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    invoke-static {v3}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v3

    if-eqz v3, :cond_11

    iget v9, v3, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget v11, v3, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    :cond_11
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    move/from16 v27, v9

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    move/from16 v9, v27

    invoke-virtual {v2, v9}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v1

    return-object v1

    :pswitch_6
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_12

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_12

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_13

    :cond_12
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_13

    if-nez p2, :cond_14

    :cond_13
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_14
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_7
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_15

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_15

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_16

    :cond_15
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_16

    if-nez p2, :cond_17

    :cond_16
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABC:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_17
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->THREEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_8
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_18

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_18

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_19

    :cond_18
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_19

    if-nez p2, :cond_1a

    :cond_19
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCD:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_1a
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    :pswitch_9
    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    rem-int/lit8 v3, p2, 0x5

    if-nez v3, :cond_1b

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v3

    if-le v3, v8, :cond_1b

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-gt v3, v9, :cond_1c

    :cond_1b
    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v3, :cond_1c

    if-nez p2, :cond_1d

    :cond_1c
    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {v0, v3, v2, v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    :cond_1d
    invoke-virtual {v2, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e:Ljava/util/ArrayList;

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v13, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FIVEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h()I

    move-result v14

    iget v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    const/16 v17, 0x0

    iget-wide v6, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->positiveMark:D

    iget-wide v8, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->negetiveMark:D

    iget-boolean v10, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->partialAllowed:Z

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->payload:Ljava/lang/String;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    move/from16 v26, v12

    move-object v12, v4

    move/from16 v16, v5

    move/from16 v18, p4

    move/from16 v19, p5

    move-wide/from16 v20, v6

    move-wide/from16 v22, v8

    move/from16 v24, v10

    move-object/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->k:I

    invoke-virtual {v0, v1, v11, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->y(III)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final w(Lcom/ekodroid/omrevaluator/templateui/models/Section2;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IIZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 8

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->f(Lcom/ekodroid/omrevaluator/templateui/models/Section2;)I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v1

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, p2, p5}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    :cond_0
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->TEXT:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->name:Ljava/lang/String;

    invoke-virtual {p0, v0, p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v0, 0x0

    move-object v4, p2

    :goto_0
    iget p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->noOfQue:I

    if-ge v0, p2, :cond_1

    move-object v1, p0

    move-object v2, p1

    move v3, v0

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-virtual/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->v(Lcom/ekodroid/omrevaluator/templateui/models/Section2;ILcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IIZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method public final x(Lcom/ekodroid/omrevaluator/templateui/models/Subject2;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->h:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;->SUBJECT_IN_COLUMN:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    if-ne v0, v1, :cond_0

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, p4}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->g(Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)I

    move-result v1

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, p2, p4}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->r(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    :cond_1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->TEXT:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->u(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object p2

    const/4 v0, 0x0

    move-object v3, p2

    :goto_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object p2

    array-length p2, p2

    if-ge v0, p2, :cond_2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object p2

    aget-object v2, p2, v0

    move-object v1, p0

    move v4, p3

    move v5, v0

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->w(Lcom/ekodroid/omrevaluator/templateui/models/Section2;Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;IIZ)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v3
.end method

.method public final y(III)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;->getColumnWidthInBubbles()[I

    move-result-object p3

    add-int/lit8 p1, p1, -0x1

    aget v0, p3, p1

    add-int/lit8 p2, p2, 0x3

    if-ge v0, p2, :cond_0

    aput p2, p3, p1

    :cond_0
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field x:I

.field y:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    return-void
.end method


# virtual methods
.method public addx(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 2

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    add-int/2addr v1, p1

    iget p1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    invoke-direct {v0, v1, p1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    return-object v0
.end method

.method public addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    add-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    return-object v0
.end method

.method public getX()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->x:I

    return v0
.end method

.method public getY()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->y:I

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    const/16 v2, 0x28

    const/16 v3, 0xff

    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v3, v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->n:Lz71;

    iget v4, v3, Lz71;->e:I

    int-to-float v4, v4

    iget v5, v3, Lz71;->f:I

    int-to-float v5, v5

    iget v3, v3, Lz71;->g:I

    invoke-static {v2, v0, v4, v5, v3}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->B(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Landroid/graphics/Canvas;FFI)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lem;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v5, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->n:Lz71;

    invoke-static {v4, v0, v3, v5}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Landroid/graphics/Canvas;Lem;Lz71;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_1
    sget v0, Lhn0;->d:I

    const/16 v2, 0xf

    if-gt v0, v2, :cond_2

    sget v0, Lhn0;->e:I

    if-le v0, v2, :cond_3

    :cond_2
    sget-wide v2, Lhn0;->h:D

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    sput-wide v2, Lhn0;->h:D

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->M(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xfa0

    cmp-long v0, v2, v4

    if-lez v0, :cond_7

    sget v0, Lhn0;->e:I

    const-wide v2, 0x3fd3333333333333L    # 0.3

    const/16 v4, 0x14

    const/4 v5, 0x0

    if-le v0, v4, :cond_4

    sput v1, Lhn0;->e:I

    sput-wide v2, Lhn0;->h:D

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v1, 0x7f0c007e

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->N(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "ERROR3"

    :goto_1
    invoke-virtual {v0, v1, v5}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    :cond_4
    sget v0, Lhn0;->d:I

    if-le v0, v4, :cond_6

    sput v1, Lhn0;->d:I

    sput-wide v2, Lhn0;->h:D

    sget v0, Ldj1;->c:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v1, 0x7f0c007d

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->N(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "ERROR2"

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v1, 0x7f0c007c

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->N(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "ERROR1"

    goto :goto_1

    :cond_6
    sget v0, Lhn0;->f:I

    const/16 v2, 0xa

    if-le v0, v2, :cond_7

    sput v1, Lhn0;->f:I

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v1, 0x7f0c007f

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->N(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a$a;->b:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "ERROR4"

    goto :goto_1

    :cond_7
    :goto_2
    return-void
.end method

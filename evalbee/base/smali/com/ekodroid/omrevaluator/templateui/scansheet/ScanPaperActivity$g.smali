.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "g"
.end annotation


# instance fields
.field public a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

.field public b:I

.field public c:Ld91;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    const/4 p1, 0x0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->b:I

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$a;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->c:Ld91;

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->e(I)V

    return-void
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->c()V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->O(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lwe;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->O(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lwe;

    move-result-object v0

    invoke-interface {v0}, Lwe;->a()V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->H(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)[Landroid/widget/ProgressBar;

    move-result-object v0

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->b:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->I(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const/high16 v1, 0x7f110000

    invoke-static {v0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->J(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)V

    :cond_2
    return-void
.end method

.method public d(I)Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;
    .locals 0

    .line 1
    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->b:I

    return-object p0
.end method

.method public final e(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->j:Landroid/os/Handler;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$d;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public run()V
    .locals 4

    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->C(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->e(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-boolean v1, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->D(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lcom/ekodroid/omrevaluator/templateui/scanner/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->d()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->c:Ld91;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->E(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lej1;

    move-result-object v2

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->b:I

    invoke-static {v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->F(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Ld91;Lej1;I)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->C(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->j:Landroid/os/Handler;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->j:Landroid/os/Handler;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;)V

    :goto_1
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

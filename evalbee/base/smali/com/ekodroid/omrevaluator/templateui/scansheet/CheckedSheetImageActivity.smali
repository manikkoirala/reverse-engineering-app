.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;
.super Lv5;
.source "SourceFile"


# static fields
.field public static M:I = 0x5


# instance fields
.field public A:I

.field public C:Z

.field public D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

.field public F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public G:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public H:Z

.field public I:Landroid/widget/ProgressBar;

.field public J:Lc91;

.field public K:Z

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/ImageButton;

.field public g:Landroid/widget/LinearLayout;

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/Button;

.field public k:Landroid/widget/Button;

.field public l:Landroid/widget/Button;

.field public m:Ljava/lang/String;

.field public n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

.field public p:Ljava/util/ArrayList;

.field public q:Ljava/util/ArrayList;

.field public t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public v:Landroid/content/SharedPreferences;

.field public w:Landroid/content/SharedPreferences$Editor;

.field public x:I

.field public y:Ljava/lang/String;

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->m:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A:I

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->C:Z

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->H:Z

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->L()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    return p0
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->b0()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->h0()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->d0(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;ILcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g0(ILcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->H:Z

    return p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->V()V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    return-object p0
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static P(Ljava/lang/String;II)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, "_"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, ".jpg"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final K()V
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    const-string v1, "AUTO_SAVE_DELAY"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    move v0, v1

    :cond_1
    const v1, 0x7f0902d1

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I:Landroid/widget/ProgressBar;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0903a5

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Lc91;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I:Landroid/widget/ProgressBar;

    const/4 v5, 0x0

    const/high16 v6, 0x447a0000    # 1000.0f

    invoke-direct {v3, v4, v1, v5, v6}, Lc91;-><init>(Landroid/widget/ProgressBar;Landroid/widget/TextView;FF)V

    iput-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->J:Lc91;

    mul-int/2addr v0, v2

    int-to-long v0, v0

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->J:Lc91;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->J:Lc91;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final L()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->J:Lc91;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0903a5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public final M(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z
    .locals 5

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamDate()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final N()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-wide/16 v3, 0x0

    const-string v5, "SCANC"

    invoke-interface {v0, v5, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v0, v3, v6

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v5, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public O(I)V
    .locals 3

    .line 1
    invoke-static {}, Lcj1;->a()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "START_SCAN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "PAGE_INDEX"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->H:Z

    if-eqz p1, :cond_0

    const/high16 p1, 0x2000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string p1, "SCAN_KEY"

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->H:Z

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final Q(Ljava/lang/String;)Landroid/widget/TextView;
    .locals 2

    .line 1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f060059

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-object v0
.end method

.method public final R()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    const-string v1, "count_saveScan"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    const-string v3, "count_cancelScan"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final S()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    const-string v2, "count_cancelScan"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->w:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->w:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final T()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    const-string v2, "count_saveScan"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->w:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->w:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final U(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v2

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result p1

    if-le p1, v2, :cond_1

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v2

    :cond_1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Le5;->D(Ljava/util/ArrayList;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getSectionsInSubjects()[I

    move-result-object p2

    invoke-direct {p1, v0, p2, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;-><init>(Ljava/util/ArrayList;[II)V

    return-object p1
.end method

.method public final V()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->S()V

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->C:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v2, "ScanCancelPredicted"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v2, "ScanCancel"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->O(I)V

    return-void
.end method

.method public final W()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->f:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final X()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->l:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Y()V
    .locals 2

    .line 1
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$j;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$j;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Z()V
    .locals 2

    .line 1
    const v0, 0x7f0900c3

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$i;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->k:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final b0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f1201b4

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->M(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->f0(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->d0(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final c0()V
    .locals 13

    .line 1
    new-instance v7, Lcom/ekodroid/omrevaluator/templateui/scanner/b;

    invoke-direct {v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;-><init>()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v8

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v8, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->i0(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const v1, 0x7f09030d

    const v2, 0x7f0901c9

    const/16 v9, 0x8

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-le v0, v11, :cond_0

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    move v12, v10

    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v12, v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v5

    move-object v0, v7

    move-object v3, v8

    move v6, v12

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->i(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/widget/ImageView;

    invoke-virtual {v12, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v5

    iget v6, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    move-object v0, v7

    move-object v3, v8

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->i(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v0

    const v1, 0x7f12029b

    const-string v2, " : "

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->c:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->c:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f12014c

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v3

    invoke-static {v3, v4}, Lve1;->o(D)D

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->x:I

    if-ne v0, v11, :cond_3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1, v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v1

    const/4 v3, -0x1

    const v4, 0x7f1202d4

    if-le v1, v3, :cond_4

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-int/2addr v1, v11

    aget-object v0, v0, v1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f060059

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " : None"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f060053

    :goto_2
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_3
    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v0

    array-length v1, v0

    if-le v1, v11, :cond_5

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    :goto_4
    array-length v1, v0

    if-ge v10, v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v3

    aget-object v3, v3, v10

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v3, v0, v10

    invoke-static {v3, v4}, Lve1;->o(D)D

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->Q(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    :cond_5
    return-void
.end method

.method public final d0(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V
    .locals 13

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "MyPref"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "save_images"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v2

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamDate()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamDate()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    if-eqz v1, :cond_1

    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->y:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    move v1, v0

    :goto_0
    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->y:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v4

    invoke-static {v3, v4, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->P(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v10

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x32

    invoke-virtual {v4, v5, v6, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    new-instance v12, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getExamDate()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    move-object v3, v12

    move v11, v1

    invoke-direct/range {v3 .. v11}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;-><init>(Ljava/lang/String;I[BLjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Ljava/lang/String;I)V

    invoke-virtual {v2, v12}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateSheetImage(Lcom/ekodroid/omrevaluator/database/SheetImageModel;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    invoke-virtual {v2, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsNotSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    const v1, 0x7f0800cf

    const v2, 0x7f08016d

    const v3, 0x7f1202ad

    invoke-static {p1, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->T()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getPredictedMarkers()Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getPredictedMarkers()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v1, "ScanSavePredicted"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_3
    invoke-static {}, Lox0;->a()V

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->O(I)V

    return-void
.end method

.method public final e0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->j:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$k;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final f0(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const v2, 0x7f1201d2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$c;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)V

    const p1, 0x7f120337

    invoke-virtual {v1, p1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    const v2, 0x7f120241

    invoke-virtual {p1, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final g0(ILcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ljx0;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    sget-object v2, Ljx0;->b:Ljava/util/ArrayList;

    invoke-static {v1}, Lbc;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    sput-object v0, Ljx0;->a:Ljava/util/ArrayList;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ROLL_NUMBER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "RESULT_ITEM"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "EXAM_ID"

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    sget p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->M:I

    invoke-virtual {p0, v0, p1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final h0()V
    .locals 7

    .line 1
    new-instance v6, Landroid/webkit/WebView;

    invoke-direct {v6, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    const v0, 0x7f1201e0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1201bf

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$h;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    const v3, 0x7f120060

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final i0(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p1, p2}, Lve1;->w(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    sget p2, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->M:I

    if-ne p1, p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "RESULT_ITEM"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "ROLL_NUMBER"

    invoke-virtual {p2, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p3

    const-string v0, "EXAM_SET"

    invoke-virtual {p3, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    move-result p3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setRollNo(I)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p2, v0}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object p2

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setExamSet(I)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setAnswerValues(Ljava/util/ArrayList;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setResultItem(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->c0()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "ScanEdit"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->V()V

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0022

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-static {p0}, Lo4;->a(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->G:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->w:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->m:Ljava/lang/String;

    const p1, 0x7f0901a1

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->f:Landroid/widget/ImageButton;

    const p1, 0x7f0903cc

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e:Landroid/widget/TextView;

    const p1, 0x7f0903fa

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->c:Landroid/widget/TextView;

    const p1, 0x7f0903e0

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->d:Landroid/widget/TextView;

    const p1, 0x7f090202

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g:Landroid/widget/LinearLayout;

    const p1, 0x7f090218

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->h:Landroid/widget/LinearLayout;

    const p1, 0x7f0900d5

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->j:Landroid/widget/Button;

    const p1, 0x7f0900c9

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->k:Landroid/widget/Button;

    const p1, 0x7f0900d7

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->l:Landroid/widget/Button;

    const p1, 0x7f0901fa

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->i:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$d;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0901c9

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$e;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->e0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->a0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->Z()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->W()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->X()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x1

    const/16 v2, 0x8

    if-eqz p1, :cond_8

    const-string v3, "ROLL_NUMBER"

    invoke-virtual {p1, v3}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A:I

    const-string v3, "SCAN_KEY"

    invoke-virtual {p1, v3, v1}, Landroid/os/BaseBundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->H:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v3, "EXAM_ID"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v3, "PAGE_INDEX"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v3

    iput-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->y:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->x:I

    iget p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v3

    array-length v3, v3

    sub-int/2addr v3, v0

    const v4, 0x7f0900c3

    const/4 v5, 0x0

    if-ge p1, v3, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1, v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setAnswerKeys([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->j:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->k:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->l:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->l:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->H:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1, v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setAnswerKeys([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->j:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->k:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->v:Landroid/content/SharedPreferences;

    const-string v3, "AUTO_SAVE_SCAN"

    invoke-interface {p1, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->j:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->k:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    sget p1, Lhn0;->d:I

    const/16 v3, 0xf

    if-le p1, v3, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v3, "ScanAdjusted"

    invoke-virtual {p1, v3, v5}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object p1

    array-length p1, p1

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v3, "ScanSinglePage"

    invoke-virtual {p1, v3, v5}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    sget-object p1, Lcj1;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    sget-object v4, Lcj1;->c:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    sget-object v4, Lcj1;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v9, p1

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->D:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v3, "ScanMultiPage"

    invoke-virtual {p1, v3, v5}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    if-nez p1, :cond_5

    invoke-static {}, Lox0;->a()V

    :cond_5
    sget p1, Lox0;->e:I

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A:I

    if-le p1, v3, :cond_6

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A:I

    :cond_6
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    sget-object v3, Lox0;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    sget-object v3, Lcj1;->c:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    sget-object v3, Lox0;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    sget-object v3, Lcj1;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object p1, Lcj1;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    sget-object v3, Lox0;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p0, p1, v4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->U(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object p1

    goto :goto_1

    :goto_2
    new-instance p1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A:I

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->F:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v4, p1

    invoke-direct/range {v4 .. v11}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;ZZ)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_7

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->O(I)V

    :cond_7
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->c0()V

    :cond_8
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->N()V

    iput-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->C:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_9
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getPredictedMarkers()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getPredictedMarkers()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_9

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->C:Z

    goto :goto_3

    :cond_a
    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->C:Z

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->f:Landroid/widget/ImageButton;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K:Z

    goto :goto_4

    :cond_b
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->f:Landroid/widget/ImageButton;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->R()I

    move-result p1

    rem-int/lit8 p1, p1, 0xa

    const/16 v0, 0x9

    if-ne p1, v0, :cond_c

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_c
    invoke-static {p0}, Ldj1;->c(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->K()V

    return-void
.end method

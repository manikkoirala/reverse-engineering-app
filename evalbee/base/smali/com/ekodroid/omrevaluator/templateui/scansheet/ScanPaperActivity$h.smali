.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "h"
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->O(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lwe;

    move-result-object p1

    invoke-interface {p1}, Lwe;->b()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Landroid/view/SurfaceHolder;

    move-result-object p1

    const/4 v0, -0x2

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Landroid/view/SurfaceHolder;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    const/16 v0, 0x28

    const/16 v1, 0xff

    invoke-virtual {p1, v0, v1, v1, v1}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->e:Landroid/hardware/Camera$Size;

    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p0, p1, v1, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->b(Landroid/graphics/Canvas;II)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final b(Landroid/graphics/Canvas;II)V
    .locals 23

    .line 1
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v4, v3, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-eqz v4, :cond_0

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->R(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRows(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v5, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->R(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v4

    invoke-static {v4}, Lve1;->e([I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-boolean v6, v5, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    if-eqz v6, :cond_1

    iget-object v3, v5, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v5}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->R(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v3

    invoke-static {v3}, Lve1;->e([I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v5, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->R(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRows(I)I

    move-result v4

    int-to-float v4, v4

    goto :goto_0

    :cond_0
    const/high16 v3, 0x41a80000    # 21.0f

    const/high16 v4, 0x41500000    # 13.0f

    :cond_1
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    int-to-float v7, v5

    int-to-float v8, v6

    div-float/2addr v7, v8

    int-to-float v8, v1

    int-to-float v9, v2

    div-float v10, v8, v9

    cmpl-float v7, v7, v10

    if-lez v7, :cond_2

    mul-int v5, v1, v6

    div-int/2addr v5, v2

    goto :goto_1

    :cond_2
    mul-int v6, v2, v5

    div-int/2addr v6, v1

    :goto_1
    new-instance v15, Landroid/graphics/Paint;

    invoke-direct {v15}, Landroid/graphics/Paint;-><init>()V

    const/16 v7, 0xff

    const/4 v10, 0x0

    invoke-static {v7, v10, v10, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v15, v7}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v7, 0x40000000    # 2.0f

    invoke-virtual {v15, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v10, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v15, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v10

    sub-int/2addr v10, v6

    div-int/lit8 v14, v10, 0x2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v10

    sub-int/2addr v10, v5

    div-int/lit8 v13, v10, 0x2

    int-to-float v12, v14

    int-to-float v11, v13

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v10

    sub-int/2addr v10, v14

    int-to-float v10, v10

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v16

    sub-int v7, v16, v13

    int-to-float v7, v7

    move/from16 v16, v10

    move-object/from16 v10, p1

    move/from16 v18, v11

    move v11, v12

    move/from16 v19, v12

    move/from16 v12, v18

    move/from16 v17, v13

    move/from16 v13, v16

    move/from16 v16, v14

    move v14, v7

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    if-le v5, v6, :cond_3

    int-to-float v7, v6

    goto :goto_2

    :cond_3
    int-to-float v7, v5

    :goto_2
    const/16 v10, 0x19

    int-to-float v10, v10

    mul-float/2addr v10, v7

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    float-to-int v10, v10

    sub-int v12, v5, v10

    sub-int v10, v6, v10

    int-to-float v12, v12

    int-to-float v10, v10

    div-float v13, v12, v10

    div-float v14, v3, v4

    cmpl-float v13, v13, v14

    if-lez v13, :cond_4

    mul-float/2addr v3, v10

    div-float v12, v3, v4

    goto :goto_3

    :cond_4
    mul-float/2addr v4, v12

    div-float v10, v4, v3

    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v10

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v10

    int-to-float v10, v10

    sub-float/2addr v10, v12

    div-float v15, v10, v4

    const/16 v4, 0x12

    int-to-float v4, v4

    mul-float/2addr v7, v4

    div-float/2addr v7, v11

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    float-to-int v14, v7

    move-object/from16 v10, p1

    invoke-static {v4, v10, v3, v15, v14}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->B(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Landroid/graphics/Canvas;FFI)V

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    new-instance v13, Lz71;

    float-to-int v12, v3

    float-to-int v11, v15

    move-object v10, v13

    move/from16 v20, v11

    move/from16 v11, v16

    move/from16 v16, v12

    move/from16 v12, v17

    move/from16 v21, v9

    move-object v9, v13

    move v13, v6

    move/from16 v17, v14

    move v14, v5

    move/from16 v22, v15

    move/from16 v15, v16

    move/from16 v16, v20

    invoke-direct/range {v10 .. v17}, Lz71;-><init>(IIIIIII)V

    iput-object v9, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->n:Lz71;

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-boolean v9, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    if-eqz v9, :cond_5

    new-instance v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    invoke-direct {v9}, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;-><init>()V

    iput-object v9, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iput v2, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    iput v1, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageWidth:I

    sub-float v15, v22, v18

    const/high16 v9, 0x40000000    # 2.0f

    div-float v9, v7, v9

    sub-float/2addr v15, v9

    mul-float/2addr v8, v15

    int-to-float v5, v5

    div-float/2addr v8, v5

    float-to-int v5, v8

    iput v5, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    sub-float v3, v3, v19

    sub-float/2addr v3, v9

    mul-float v9, v21, v3

    int-to-float v3, v6

    div-float/2addr v9, v3

    float-to-int v6, v9

    iput v6, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    sub-int v8, v1, v5

    iput v8, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v9, v2, v6

    iput v9, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    mul-float v7, v7, v21

    div-float/2addr v7, v3

    float-to-int v3, v7

    iput v3, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    mul-int/lit8 v3, v3, 0x64

    div-int/2addr v3, v1

    iput v3, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelengthPercent:I

    mul-int/lit8 v5, v5, 0x64

    div-int/2addr v5, v1

    iput v5, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidthPercent:I

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v8, v1

    iput v8, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidthPercent:I

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v2

    iput v6, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeightPercent:I

    mul-int/lit8 v9, v9, 0x64

    div-int/2addr v9, v2

    goto :goto_4

    :cond_5
    new-instance v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    invoke-direct {v9}, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;-><init>()V

    iput-object v9, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iput v1, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    iput v2, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageWidth:I

    sub-float v3, v3, v19

    const/high16 v9, 0x40000000    # 2.0f

    div-float v9, v7, v9

    sub-float/2addr v3, v9

    mul-float v3, v3, v21

    int-to-float v6, v6

    div-float/2addr v3, v6

    float-to-int v3, v3

    iput v3, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    sub-float v15, v22, v18

    sub-float/2addr v15, v9

    mul-float/2addr v8, v15

    int-to-float v5, v5

    div-float/2addr v8, v5

    float-to-int v5, v8

    iput v5, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    sub-int v8, v2, v3

    iput v8, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v9, v1, v5

    iput v9, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    mul-float v7, v7, v21

    div-float/2addr v7, v6

    float-to-int v6, v7

    iput v6, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v2

    iput v6, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelengthPercent:I

    mul-int/lit8 v3, v3, 0x64

    div-int/2addr v3, v2

    iput v3, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidthPercent:I

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v8, v2

    iput v8, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidthPercent:I

    mul-int/lit8 v5, v5, 0x64

    div-int/2addr v5, v1

    iput v5, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeightPercent:I

    mul-int/lit8 v9, v9, 0x64

    div-int/2addr v9, v1

    :goto_4
    iput v9, v4, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeightPercent:I

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->d(Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;)V

    return-void
.end method

.method public c(Ljava/lang/Void;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, La91;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    if-nez v0, :cond_1

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;

    invoke-direct {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->e0()V

    :goto_0
    return-void
.end method

.method public final d(Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;)V
    .locals 4

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    iget v0, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    div-int/lit8 v0, v0, 0xa

    iput v0, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    iget v3, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    sub-int/2addr v3, v0

    iput v3, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v3, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    if-ge v3, v2, :cond_1

    iget v0, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    div-int/lit8 v0, v0, 0xa

    iput v0, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageWidth:I

    sub-int/2addr v2, v0

    iput v2, p1, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    goto :goto_1

    :cond_1
    move v1, v0

    :goto_1
    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "InvalidConfig"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;->c(Ljava/lang/Void;)V

    return-void
.end method

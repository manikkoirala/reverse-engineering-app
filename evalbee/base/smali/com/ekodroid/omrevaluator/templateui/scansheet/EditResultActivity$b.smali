.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Landroid/widget/Spinner;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;Landroid/widget/EditText;Landroid/widget/Spinner;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->b:Landroid/widget/Spinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->a:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1200bb

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->a:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->g:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    const-string v2, "RESULT_ITEM"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "ROLL_NUMBER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->b:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    const-string v1, "EXAM_SET"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->A()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->B(Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;)V

    return-void
.end method

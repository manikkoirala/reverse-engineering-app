.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;
.super Lv5;
.source "SourceFile"

# interfaces
.implements Lxe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;,
        Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;
    }
.end annotation


# instance fields
.field public A:[Landroid/widget/ProgressBar;

.field public C:Landroid/widget/ProgressBar;

.field public D:[I

.field public F:Lcom/ekodroid/omrevaluator/templateui/scanner/a;

.field public G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public H:Landroid/content/SharedPreferences;

.field public I:Lfj1;

.field public J:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public K:I

.field public M:J

.field public O:Lej1;

.field public c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

.field public d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public e:Landroid/hardware/Camera$Size;

.field public f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Landroid/os/Handler;

.field public k:Landroidx/appcompat/app/a;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Lz71;

.field public p:Z

.field public q:Z

.field public t:Z

.field public v:Z

.field public w:Landroid/view/SurfaceView;

.field public x:Landroid/view/SurfaceHolder;

.field public y:Lwe;

.field public z:Lcom/ekodroid/omrevaluator/components/AutofitPreview;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->h:Z

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->p:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->y:Lwe;

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->M:J

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->O:Lej1;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Landroid/view/SurfaceHolder;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->x:Landroid/view/SurfaceHolder;

    return-object p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Landroid/graphics/Canvas;FFI)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->V(Landroid/graphics/Canvas;FFI)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->t:Z

    return p0
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lcom/ekodroid/omrevaluator/templateui/scanner/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->F:Lcom/ekodroid/omrevaluator/templateui/scanner/a;

    return-object p0
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lej1;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->O:Lej1;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Ld91;Lej1;I)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->S(Ld91;Lej1;I)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f0(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z

    move-result p0

    return p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)[Landroid/widget/ProgressBar;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    return-object p0
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->v:Z

    return p0
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->Z(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Landroid/graphics/Canvas;Lem;Lz71;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->U(Landroid/graphics/Canvas;Lem;Lz71;)V

    return-void
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->j0(II)V

    return-void
.end method

.method public static synthetic M(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->M:J

    return-wide v0
.end method

.method public static synthetic N(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g0(I)V

    return-void
.end method

.method public static synthetic O(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)Lwe;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->y:Lwe;

    return-object p0
.end method

.method public static synthetic P(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)[I
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->D:[I

    return-object p0
.end method

.method public static synthetic Q(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->h0()V

    return-void
.end method

.method public static synthetic R(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    return p0
.end method


# virtual methods
.method public final S(Ld91;Lej1;I)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    .locals 11

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->F:Lcom/ekodroid/omrevaluator/templateui/scanner/a;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->d()I

    move-result v0

    if-lez v0, :cond_0

    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->y:Lwe;

    invoke-interface {p3}, Lwe;->getImage()[B

    move-result-object p3

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->y:Lwe;

    invoke-interface {p3}, Lwe;->getFrame()[B

    move-result-object p3

    move v0, v1

    :goto_0
    const/16 v2, 0xa

    invoke-interface {p1, v2}, Ld91;->a(I)V

    const/4 v10, 0x0

    if-nez p3, :cond_1

    return-object v10

    :cond_1
    array-length v2, p3

    invoke-static {p3, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object p3

    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    if-eqz v1, :cond_2

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-gt v1, v2, :cond_4

    :cond_2
    iget-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    if-nez v1, :cond_3

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_3

    goto :goto_1

    :cond_3
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {v8, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x1

    move-object v3, p3

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V

    move-object p3, v1

    :cond_4
    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/16 v2, 0x640

    if-le v1, v2, :cond_5

    const-wide v2, 0x4097700000000000L    # 1500.0

    int-to-double v4, v1

    div-double/2addr v2, v4

    double-to-float v1, v2

    invoke-static {p3, v1}, Lbc;->e(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object p3

    :cond_5
    const/16 v1, 0x14

    invoke-interface {p1, v1}, Ld91;->a(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    invoke-virtual {p0, p3, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->T(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;)V

    new-instance v3, Lj5;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->f:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-direct {v3, p3, v1, v2}, Lj5;-><init>(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/scanner/c;

    invoke-direct {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/c;-><init>()V

    new-instance v4, Lhn0;

    invoke-direct {v4}, Lhn0;-><init>()V

    iget-boolean v8, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->h:Z

    iget v9, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    move-object v5, p1

    move-object v6, p2

    move v7, v0

    invoke-virtual/range {v2 .. v9}, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->a(Lj5;Lhn0;Ld91;Lej1;ZZI)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-result-object p1

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    if-eqz p2, :cond_6

    if-eqz v0, :cond_6

    return-object p1

    :cond_6
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->F:Lcom/ekodroid/omrevaluator/templateui/scanner/a;

    invoke-virtual {p2, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-result-object p1

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    if-nez p2, :cond_7

    return-object p1

    :cond_7
    return-object v10
.end method

.method public final T(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    iget p2, p2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageWidth:I

    if-eq p1, p2, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p2, "InvalidCamera"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public final U(Landroid/graphics/Canvas;Lem;Lz71;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    if-ne v0, v1, :cond_0

    iget v0, p3, Lz71;->d:I

    int-to-double v0, v0

    mul-double/2addr v0, v2

    iget v2, p2, Lem;->e:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p2, Lem;->b:I

    int-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-int v2, v2

    iget v3, p3, Lz71;->b:I

    add-int/2addr v2, v3

    iget v3, p2, Lem;->a:I

    goto :goto_0

    :cond_0
    iget v0, p3, Lz71;->d:I

    int-to-double v0, v0

    mul-double/2addr v0, v2

    iget v2, p2, Lem;->d:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p2, Lem;->a:I

    int-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-int v2, v2

    iget v3, p3, Lz71;->b:I

    add-int/2addr v2, v3

    iget v3, p2, Lem;->e:I

    iget v4, p2, Lem;->b:I

    sub-int/2addr v3, v4

    :goto_0
    int-to-double v3, v3

    mul-double/2addr v3, v0

    double-to-int v3, v3

    iget p3, p3, Lz71;->a:I

    add-int/2addr v3, p3

    iget p2, p2, Lem;->c:I

    int-to-double p2, p2

    mul-double/2addr v0, p2

    double-to-int p2, v0

    invoke-virtual {p0, p1, v3, v2, p2}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->X(Landroid/graphics/Canvas;III)V

    return-void
.end method

.method public final V(Landroid/graphics/Canvas;FFI)V
    .locals 4

    .line 1
    float-to-int v0, p2

    float-to-int v1, p3

    invoke-virtual {p0, p1, v0, v1, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->Y(Landroid/graphics/Canvas;III)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {p0, p1, v2, v1, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->Y(Landroid/graphics/Canvas;III)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, p3

    float-to-int v2, v2

    invoke-virtual {p0, p1, v0, v2, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->Y(Landroid/graphics/Canvas;III)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, p3

    float-to-int v3, v3

    invoke-virtual {p0, p1, v2, v3, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->Y(Landroid/graphics/Canvas;III)V

    invoke-virtual {p0, p1, v0, v1, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->W(Landroid/graphics/Canvas;III)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {p0, p1, v2, v1, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->W(Landroid/graphics/Canvas;III)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, p3

    float-to-int v1, v1

    invoke-virtual {p0, p1, v0, v1, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->W(Landroid/graphics/Canvas;III)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p2

    float-to-int p2, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p3

    float-to-int p3, v0

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->W(Landroid/graphics/Canvas;III)V

    return-void
.end method

.method public final W(Landroid/graphics/Canvas;III)V
    .locals 6

    .line 1
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/16 v0, 0xff

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v0, 0x40400000    # 3.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    div-int/lit8 p4, p4, 0x2

    sub-int v0, p2, p4

    int-to-float v1, v0

    sub-int v0, p3, p4

    int-to-float v2, v0

    add-int/2addr p2, p4

    int-to-float v3, p2

    add-int/2addr p3, p4

    int-to-float v4, p3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final X(Landroid/graphics/Canvas;III)V
    .locals 6

    .line 1
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/16 v0, 0xff

    const/4 v1, 0x0

    invoke-static {v0, v1, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v0, 0x40400000    # 3.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    div-int/lit8 p4, p4, 0x2

    sub-int v0, p2, p4

    int-to-float v1, v0

    sub-int v0, p3, p4

    int-to-float v2, v0

    add-int/2addr p2, p4

    int-to-float v3, p2

    add-int/2addr p3, p4

    int-to-float v4, p3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final Y(Landroid/graphics/Canvas;III)V
    .locals 6

    .line 1
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    const/4 v0, 0x0

    const/16 v1, 0xff

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    div-int/lit8 p4, p4, 0x2

    sub-int v0, p2, p4

    int-to-float v1, v0

    sub-int v0, p3, p4

    int-to-float v2, v0

    add-int/2addr p2, p4

    int-to-float v3, p2

    add-int/2addr p3, p4

    int-to-float v4, p3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final Z(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getMarkedAnswers()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSectionsInSubject()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getExamSet()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;-><init>(Ljava/util/ArrayList;[II)V

    sput-object v0, Lcj1;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getSheetDimension()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    move-result-object v0

    sput-object v0, Lcj1;->c:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ROLL_NUMBER"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getRollNumber()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "EXAM_ID"

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "PAGE_INDEX"

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->p:Z

    if-eqz p1, :cond_0

    const-string p1, "SCAN_KEY"

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 p1, 0x2000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public a(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->e:Landroid/hardware/Camera$Size;

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    :cond_0
    iget p2, p1, Landroid/hardware/Camera$Size;->height:I

    iget v1, p1, Landroid/hardware/Camera$Size;->width:I

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    const/16 v1, 0x384

    if-le p2, v1, :cond_1

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    :cond_1
    iget p2, p1, Landroid/hardware/Camera$Size;->height:I

    iget p1, p1, Landroid/hardware/Camera$Size;->width:I

    if-le p2, p1, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string p2, "android_id"

    invoke-static {p1, p2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->J:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "CAMERA_FRAME_ERROR"

    invoke-static {p2, v1, p1}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$a;)V

    new-array p2, v0, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public final a0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/more/InstructionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final b0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/more/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final c0(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lok;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, p3, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p4}, Lb;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p3

    invoke-virtual {p3, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 p2, 0x5

    invoke-virtual {p3, p2, p1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return v0
.end method

.method public final d0()Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRows(I)I

    move-result v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v2

    invoke-static {v2}, Lve1;->e([I)I

    move-result v2

    add-int/2addr v2, v1

    int-to-double v2, v2

    int-to-double v4, v0

    const-wide v6, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v4, v6

    cmpl-double v0, v2, v4

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i:Z

    :goto_0
    invoke-virtual {p0}, Lv5;->n()Lt1;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lv5;->n()Lt1;

    move-result-object v0

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lt1;->t(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lv5;->n()Lt1;

    move-result-object v0

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lt1;->s(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v0

    array-length v0, v0

    const v4, 0x7f0903e9

    if-le v0, v1, :cond_2

    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f120262

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_1
    return v1
.end method

.method public e0()V
    .locals 11

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "START_SCAN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const/16 v1, 0x3e7

    const-string v3, "android_id"

    const-string v4, "MyPref"

    const/4 v5, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Throwable;->printStackTrace()V

    move v6, v1

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    const/16 v8, 0x14

    invoke-virtual {p0, v8, v7, v6, v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c0(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-static {}, La91;->f()I

    move-result v0

    if-eq v6, v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-wide/16 v6, 0x0

    const-string v8, "SCAN_MODE"

    invoke-interface {v0, v8, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    cmp-long v0, v6, v9

    if-lez v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->M:J

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v6, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->M:J

    invoke-interface {v0, v8, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->t:Z

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v1, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v3, v0, v1}, La91;->E(Landroid/content/SharedPreferences;Ljava/lang/String;I)Z

    invoke-static {}, Lgk;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_3
    invoke-static {}, Lhn0;->j()V

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    invoke-virtual {v0, v5}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d(I)Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;->d(I)Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$g;

    move-result-object v1

    new-instance v2, Ljava/lang/Thread;

    invoke-direct {v2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_4
    return-void
.end method

.method public final f0(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->q:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->t:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iput-boolean v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->t:Z

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getCheckedBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getCheckedBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    sput-object p1, Lcj1;->a:Landroid/graphics/Bitmap;

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method public final g0(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->I:Lfj1;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lfj1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$f;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    new-instance v1, Lfj1;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {v1, v2, v0, p1}, Lfj1;-><init>(Landroid/content/Context;Ly01;I)V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->I:Lfj1;

    return-void
.end method

.method public final h0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->j:Landroid/os/Handler;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i0(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1202b5

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    const v1, 0x7f1200fb

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$d;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    invoke-virtual {p1, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final j0(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->D:[I

    const/4 v1, 0x1

    add-int/2addr p1, v1

    aput p2, v0, p1

    const/4 p1, 0x0

    aget p2, v0, p1

    const/4 v0, 0x2

    if-lez p2, :cond_1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    aget-object p1, p2, p1

    const/16 p2, 0x64

    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->D:[I

    aget p2, p1, v1

    aget p1, p1, v0

    if-le p2, p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    aget-object p1, p1, v1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    aget-object p2, p2, v1

    invoke-virtual {p2, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    aget-object p2, p2, v1

    invoke-virtual {p2, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->D:[I

    aget v1, p2, v1

    aget p2, p2, v0

    if-le v1, p2, :cond_2

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    aget-object p1, p2, p1

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    aget-object p1, v0, p1

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    :goto_1
    return-void
.end method

.method public final k0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/exams/ResultActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0038

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->J:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->G:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SCAN_KEY"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->p:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "PAGE_INDEX"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    const p1, 0x7f09036d

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/components/AutofitPreview;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->z:Lcom/ekodroid/omrevaluator/components/AutofitPreview;

    const/4 p1, 0x2

    new-array p1, p1, [Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->D:[I

    const v0, 0x7f090306

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    aput-object v0, p1, v1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->A:[Landroid/widget/ProgressBar;

    const v0, 0x7f090305

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    aput-object v0, p1, v2

    const p1, 0x7f090307

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->C:Landroid/widget/ProgressBar;

    const p1, 0x7f090455

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->l:Landroid/widget/TextView;

    const p1, 0x7f090454

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->m:Landroid/widget/TextView;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->j:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d0()Z

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->H:Landroid/content/SharedPreferences;

    const-string v0, "scan_sound"

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->v:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->H:Landroid/content/SharedPreferences;

    const-string v0, "scan_resolution_high"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->H:Landroid/content/SharedPreferences;

    const-string v0, "option_mark_with_pen"

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->h:Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->H:Landroid/content/SharedPreferences;

    const-string v0, "show_scan_instruction"

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string v0, "HH"

    invoke-direct {p1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x7

    if-lt p1, v0, :cond_1

    const/16 v0, 0x12

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f1201df

    goto :goto_1

    :cond_1
    :goto_0
    const p1, 0x7f1201de

    :goto_1
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->i0(Ljava/lang/String;)V

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->K:I

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object p1

    invoke-static {p1}, Lve1;->e([I)I

    move-result p1

    add-int/2addr p1, v2

    new-instance v0, Lve;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->z:Lcom/ekodroid/omrevaluator/components/AutofitPreview;

    iget-boolean v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->g:Z

    invoke-direct {v0, v3, p0, v4, p1}, Lve;-><init>(Lcom/ekodroid/omrevaluator/components/AutofitPreview;Lxe;ZI)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->y:Lwe;

    const p1, 0x7f0902ba

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/SurfaceView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->w:Landroid/view/SurfaceView;

    invoke-virtual {p1, v2}, Landroid/view/SurfaceView;->setZOrderOnTop(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->w:Landroid/view/SurfaceView;

    const/16 v0, 0xff

    invoke-static {v1, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->w:Landroid/view/SurfaceView;

    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->x:Landroid/view/SurfaceHolder;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->w:Landroid/view/SurfaceView;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/scanner/a;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$c;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V

    invoke-direct {p1, v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;-><init>(Ld91;)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->F:Lcom/ekodroid/omrevaluator/templateui/scanner/a;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f090046

    const/4 v2, 0x1

    if-eq v0, v1, :cond_2

    const v1, 0x7f090050

    if-eq v0, v1, :cond_1

    const v1, 0x7f090058

    if-eq v0, v1, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->k0()V

    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->b0()V

    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->a0()V

    return v2
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->q:Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->y:Lwe;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lwe;->a()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-static {v0}, La91;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->q:Z

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->h0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->k:Landroidx/appcompat/app/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v2, 0x7f1202b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v3, 0x7f1201e2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    const v3, 0x7f120060

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e$a;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    iput-object v0, v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->k:Landroidx/appcompat/app/a;

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity$e;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;->k:Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->a0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result p1

    if-gez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    move-result-object p1

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1201b4

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->n:Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    const-string v1, "RESULT_DATA"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-static {}, Lox0;->a()V

    invoke-static {}, Lcj1;->a()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$a;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

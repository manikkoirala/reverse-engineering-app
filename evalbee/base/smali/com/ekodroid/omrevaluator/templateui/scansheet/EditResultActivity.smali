.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;
.super Lv5;
.source "SourceFile"


# static fields
.field public static m:I = 0x5


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

.field public d:I

.field public e:I

.field public f:I

.field public g:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public j:Ljava/util/ArrayList;

.field public k:Ljava/util/ArrayList;

.field public l:Lk5$f;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->l:Lk5$f;

    return-void
.end method

.method public static synthetic A()I
    .locals 1

    .line 1
    sget v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->m:I

    return v0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->C()V

    return-void
.end method


# virtual methods
.method public final C()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    sput-object v0, Ljx0;->a:Ljava/util/ArrayList;

    sput-object v0, Ljx0;->b:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final D()V
    .locals 9

    .line 1
    const v0, 0x7f09038b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TableLayout;

    new-instance v1, Law;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->g:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->l:Lk5$f;

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->j:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->k:Ljava/util/ArrayList;

    invoke-direct/range {v1 .. v8}, Law;-><init>(Landroid/content/Context;Landroid/widget/TableLayout;Ljava/util/ArrayList;Lk5$f;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0027

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "ROLL_NUMBER"

    invoke-virtual {p1, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->d:I

    const-string v1, "RESULT_ITEM"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->g:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "EXAM_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->c:Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->e:I

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->f:I

    sget-object p1, Ljx0;->b:Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    sget-object p1, Ljx0;->a:Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    sget-object v1, Ljx0;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne p1, v1, :cond_0

    sget-object p1, Ljx0;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v1

    array-length v1, v1

    if-ne p1, v1, :cond_0

    sget-object p1, Ljx0;->a:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->j:Ljava/util/ArrayList;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->k:Ljava/util/ArrayList;

    move p1, v0

    :goto_0
    sget-object v1, Ljx0;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->k:Ljava/util/ArrayList;

    sget-object v2, Ljx0;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    sget-object v3, Ljx0;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    array-length v3, v3

    invoke-static {v2, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    const p1, 0x7f0901ef

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    const v1, 0x7f090336

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->e:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->i:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object p1

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->e:I

    new-array v4, v2, [Ljava/lang/String;

    move v5, v0

    :goto_1
    if-ge v5, v2, :cond_2

    aget-object v6, p1, v5

    aput-object v6, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    new-instance p1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0c00f0

    invoke-direct {p1, p0, v2, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->g:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-virtual {v1, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    :goto_2
    const p1, 0x7f09014b

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    new-array v2, v3, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->f:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v0

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0900cc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;

    invoke-direct {v2, p0, p1, v1}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;Landroid/widget/EditText;Landroid/widget/Spinner;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900a8

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$c;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/EditResultActivity;->D()V

    return-void
.end method

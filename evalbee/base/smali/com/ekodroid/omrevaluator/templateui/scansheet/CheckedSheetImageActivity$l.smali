.class public Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->X()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    if-nez p1, :cond_0

    invoke-static {}, Lox0;->a()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object p1

    array-length p1, p1

    sput p1, Lox0;->d:I

    :cond_0
    sget-object p1, Lox0;->c:Ljava/util/ArrayList;

    sget-object v0, Lcj1;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object p1, Lox0;->b:Ljava/util/ArrayList;

    sget-object v0, Lcj1;->c:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object p1, Lox0;->a:Ljava/util/ArrayList;

    sget-object v0, Lcj1;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->A:I

    sget v0, Lox0;->e:I

    if-le p1, v0, :cond_1

    sput p1, Lox0;->e:I

    :cond_1
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->I(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    move-result-object v0

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->J(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    const-string v1, "EXAM_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "START_SCAN"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->z:I

    add-int/lit8 v0, v0, 0x1

    const-string v1, "PAGE_INDEX"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->G(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/high16 v0, 0x2000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->G(Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;)Z

    move-result v0

    const-string v1, "SCAN_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

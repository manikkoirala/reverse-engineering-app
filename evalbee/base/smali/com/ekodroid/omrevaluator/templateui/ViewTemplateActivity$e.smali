.class public Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "e"
.end annotation


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Landroid/app/ProgressDialog;

.field public e:I

.field public final synthetic f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;)V
    .locals 5

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x64

    rem-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[^a-zA-Z0-9_-]"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOCUMENTS:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".pdf"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->c:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;I)Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->e(I)Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public varargs b([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->e:I

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->C(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;ILjava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->a:Z

    const/4 p1, 0x0

    return-object p1
.end method

.method public c(Ljava/lang/Void;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->d:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->d:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->a:Z

    if-eqz p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const-string v2, "com.ekodroid.omrevaluator.fileprovider"

    invoke-static {v1, v2, v0}, Landroidx/core/content/FileProvider;->f(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "application/pdf"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f12026b

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    :cond_2
    :goto_0
    return-void
.end method

.method public varargs d([Ljava/lang/Integer;)V
    .locals 0

    .line 1
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->b([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final e(I)Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;
    .locals 0

    .line 1
    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->e:I

    return-object p0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->c(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 3

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->d:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->f:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const v2, 0x7f120184

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->d([Ljava/lang/Integer;)V

    return-void
.end method

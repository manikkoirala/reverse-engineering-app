.class public Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->Y(I[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    add-int/lit8 p3, p3, 0x1

    invoke-static {p1, p3}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->B(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;I)I

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->A(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)I

    move-result p3

    add-int/lit8 p3, p3, -0x1

    aget-object p2, p2, p3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;->a:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->E(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

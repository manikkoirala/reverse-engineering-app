.class public Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzv$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->K(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;Z)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->LABEL:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->setSubName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->e()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setName(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->QUESTION:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    if-ne v0, v1, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->e()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->a()D

    move-result-wide v3

    iput-wide v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->b()D

    move-result-wide v3

    iput-wide v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->j()Z

    move-result v3

    iput-boolean v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->QUESTION:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    if-ne v0, v1, :cond_3

    if-nez p2, :cond_3

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d()I

    move-result v1

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->a()D

    move-result-wide v0

    iput-wide v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->b()D

    move-result-wide v0

    iput-wide v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->j()Z

    move-result p1

    iput-boolean p1, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    :cond_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->B(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)V

    return-void
.end method

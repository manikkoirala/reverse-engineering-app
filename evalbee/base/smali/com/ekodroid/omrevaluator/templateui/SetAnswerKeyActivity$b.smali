.class public Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->a0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/RadioButton;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;Landroid/widget/RadioButton;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;->b:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;->a:Landroid/widget/RadioButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;->b:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v0, p2, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    invoke-virtual {v0, p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setAnswerKeys([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;->a:Landroid/widget/RadioButton;

    invoke-virtual {p2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;->b:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->C(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;->b:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->D(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

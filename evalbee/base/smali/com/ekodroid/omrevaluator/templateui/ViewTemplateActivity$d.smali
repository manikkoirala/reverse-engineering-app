.class public Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->R(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;->b:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;->a:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;->b:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const-string v0, "com.ekodroid.omrevaluator.fileprovider"

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;->a:Ljava/io/File;

    invoke-static {p1, v0, v1}, Landroidx/core/content/FileProvider;->f(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "application/octet-stream"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x40000000    # 2.0f

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;->b:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const-string v1, "Share with"

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

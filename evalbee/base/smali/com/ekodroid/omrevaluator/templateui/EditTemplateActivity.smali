.class public Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public d:Ljava/util/ArrayList;

.field public e:Landroid/widget/ListView;

.field public f:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

.field public g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->i:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->K(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;)V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->H(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->F()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    return-object p0
.end method


# virtual methods
.method public final E(Ljava/util/ArrayList;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/util/ArrayList;
    .locals 21

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, -0x1

    const/4 v2, 0x1

    move v3, v2

    move v2, v1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    move-object/from16 v4, p1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v15, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    if-ne v1, v15, :cond_0

    iget v6, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    if-eq v2, v6, :cond_2

    :cond_0
    iget v1, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    aget-object v2, p2, v15

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->isOptionalAllowed()Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfOptionalQue()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfQue()I

    move-result v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v9, v2

    goto :goto_1

    :cond_1
    move-object v9, v6

    :goto_1
    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    sget-object v7, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->LABEL:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    aget-object v6, p2, v15

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    const-wide/16 v13, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const-string v19, ""

    move-object v6, v2

    move v11, v15

    move v12, v1

    move/from16 v20, v15

    move-wide/from16 v15, v16

    move/from16 v17, v18

    move-object/from16 v18, v19

    invoke-direct/range {v6 .. v18}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;Ljava/lang/String;Ljava/lang/String;IIIDDZLjava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    move/from16 v1, v20

    :cond_2
    new-instance v15, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    sget-object v7, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->QUESTION:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    aget-object v6, p2, v1

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v8

    aget-object v6, p2, v1

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v6

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object v9

    iget v10, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    iget v11, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v12, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    iget-wide v13, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    move/from16 v19, v1

    move/from16 v20, v2

    iget-wide v1, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    iget-boolean v5, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    move-object/from16 v6, p3

    invoke-virtual {v6, v10}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v18

    move-object v6, v15

    move-object v4, v15

    move-wide v15, v1

    move/from16 v17, v5

    invoke-direct/range {v6 .. v18}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;Ljava/lang/String;Ljava/lang/String;IIIDDZLjava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    move/from16 v1, v19

    move/from16 v2, v20

    goto/16 :goto_0

    :cond_3
    return-object v0
.end method

.method public final F()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->f:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSheetTemplate(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->f:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    const v1, 0x7f0800cf

    const v2, 0x7f08016d

    const v3, 0x7f1200ce

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final G()V
    .locals 2

    .line 1
    const v0, 0x7f0900cc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900a8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final H(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)V
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->i:Z

    :cond_0
    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    invoke-virtual {p0, p2, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->E(Ljava/util/ArrayList;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->d:Ljava/util/ArrayList;

    new-instance p1, Lm3;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->d:Ljava/util/ArrayList;

    invoke-direct {p1, p2, v0}, Lm3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->e:Landroid/widget/ListView;

    invoke-virtual {p2, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final I()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->e:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public final J()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1201d3

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$f;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V

    const v3, 0x7f120337

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$e;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V

    const v3, 0x7f120241

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final K(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;)V

    new-instance v1, Lzv;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    invoke-direct {v1, v2, v0, p1}, Lzv;-><init>(Landroid/content/Context;Lzv$d;Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->J()V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0028

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f09024b

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->e:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->g:Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->f:Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->H(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->I()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/EditTemplateActivity;->G()V

    return-void
.end method

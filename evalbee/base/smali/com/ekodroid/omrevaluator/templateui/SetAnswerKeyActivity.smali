.class public Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public final c:I

.field public final d:I

.field public e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

.field public f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public i:Z

.field public j:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public k:I

.field public l:I

.field public m:Landroid/widget/Button;

.field public n:Landroid/widget/Button;

.field public p:Lk5$f;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->c:I

    const/16 v0, 0xca

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->d:I

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->k:I

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->p:Lk5$f;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    return p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    return p1
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->M()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->L()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->K(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->X([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->U()V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->R()V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->d0([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final J()Z
    .locals 2

    .line 1
    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Lsl;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lh2;->g(Landroid/app/Activity;[Ljava/lang/String;I)V

    const/4 v0, 0x0

    return v0
.end method

.method public final K(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7

    .line 1
    new-instance v6, Lk3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->p:Lk5$f;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v5

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lk3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Lk5$f;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    const p1, 0x7f090248

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final L()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    new-instance v0, Ltu;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-direct {v0, v1, v2, v3}, Ltu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "DownloadKeyCsv"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f120038

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.method public final M()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    new-instance v0, Luu;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-direct {v0, v1, v2, v3}, Luu;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "DownloadKey"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f120038

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.method public final N(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 32

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$i;->a:[I

    iget-object v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/16 v6, 0xa

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TENOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v9, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    new-array v10, v6, [Z

    const/4 v11, 0x0

    iget v12, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v13, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/4 v14, 0x0

    iget-object v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object v7, v3

    invoke-direct/range {v7 .. v15}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_1
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v17, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EIGHTOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    const/16 v5, 0x8

    new-array v5, v5, [Z

    const/16 v20, 0x0

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/16 v23, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object/from16 v16, v3

    move/from16 v18, v4

    move-object/from16 v19, v5

    move/from16 v21, v6

    move/from16 v22, v7

    move-object/from16 v24, v2

    invoke-direct/range {v16 .. v24}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_2
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->SIXOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v10, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    const/4 v4, 0x6

    new-array v11, v4, [Z

    const/4 v12, 0x0

    iget v13, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v14, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/4 v15, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object v8, v3

    move-object/from16 v16, v2

    invoke-direct/range {v8 .. v16}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_3
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v17, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    const/16 v19, 0x0

    const/16 v20, 0x0

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/16 v23, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object/from16 v16, v3

    move/from16 v18, v4

    move/from16 v21, v5

    move/from16 v22, v6

    move-object/from16 v24, v2

    invoke-direct/range {v16 .. v24}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_4
    iget-object v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v3}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-boolean v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-nez v4, :cond_0

    iget v5, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    const/4 v7, 0x1

    if-le v5, v7, :cond_1

    :cond_0
    iget v3, v3, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr v6, v3

    if-eqz v4, :cond_1

    add-int/lit8 v6, v6, 0x1

    :cond_1
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v9, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    new-array v10, v6, [Z

    const/4 v11, 0x0

    iget v12, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v13, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/4 v14, 0x0

    iget-object v15, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object v7, v3

    invoke-direct/range {v7 .. v15}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_5
    iget-object v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v3}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v3

    if-eqz v3, :cond_2

    iget v5, v3, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    :cond_2
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v8, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    mul-int/2addr v4, v5

    new-array v9, v4, [Z

    const/4 v10, 0x0

    iget v11, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v12, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/4 v13, 0x0

    iget-object v14, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object v6, v3

    invoke-direct/range {v6 .. v14}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_6
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v16, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    const/4 v5, 0x2

    new-array v5, v5, [Z

    const/16 v19, 0x0

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/16 v22, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object v15, v3

    move/from16 v17, v4

    move-object/from16 v18, v5

    move/from16 v20, v6

    move/from16 v21, v7

    move-object/from16 v23, v2

    invoke-direct/range {v15 .. v23}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_7
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v24, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->THREEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    const/4 v5, 0x3

    new-array v5, v5, [Z

    const/16 v27, 0x0

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/16 v30, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object/from16 v23, v3

    move/from16 v25, v4

    move-object/from16 v26, v5

    move/from16 v28, v6

    move/from16 v29, v7

    move-object/from16 v31, v2

    invoke-direct/range {v23 .. v31}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_8
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FIVEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v10, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    new-array v11, v4, [Z

    const/4 v12, 0x0

    iget v13, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v14, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/4 v15, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object v8, v3

    move-object/from16 v16, v2

    invoke-direct/range {v8 .. v16}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_9
    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    sget-object v17, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    new-array v5, v5, [Z

    const/16 v20, 0x0

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    const/16 v23, 0x0

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object/from16 v16, v3

    move/from16 v18, v4

    move-object/from16 v19, v5

    move/from16 v21, v6

    move/from16 v22, v7

    move-object/from16 v24, v2

    invoke-direct/range {v16 .. v24}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final O(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v0

    new-array v2, v0, [Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    :goto_0
    if-ge v1, v0, :cond_1

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->N(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-direct {v3, v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;-><init>(Ljava/util/ArrayList;I)V

    aput-object v3, v2, v1

    move v1, v5

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public final P(Ljava/lang/String;)I
    .locals 1

    .line 1
    const/4 v0, -0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v0
.end method

.method public final Q()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scansheet/ScanPaperActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXAM_ID"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "START_SCAN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "SCAN_KEY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v1, 0xca

    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final R()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "Choose a file"

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final S()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->m:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final T()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->n:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->n:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final U()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->O(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->j:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->k:I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->W()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final V(Ljava/io/InputStream;)V
    .locals 10

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lyd;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Lyd;-><init>(Ljava/io/Reader;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object p1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v2

    :cond_0
    invoke-virtual {v1}, Lyd;->c()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v4, 0x0

    aget-object v4, v3, v4

    invoke-virtual {p0, v4}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->P(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    const/4 v6, 0x1

    :goto_0
    array-length v7, v3

    if-ge v6, v7, :cond_0

    aget-object v7, v3, v6

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    array-length v8, v7

    if-gt v6, v8, :cond_2

    iget-object v8, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v9, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v8, v9, :cond_1

    add-int/lit8 v8, v6, -0x1

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v7

    add-int/lit8 v8, v4, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    aget-object v8, v3, v6

    invoke-static {v8}, Le5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->setMarkedPayload(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    aget-object v7, v3, v6

    iget-object v9, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v7, v8, v9, v2}, Le5;->c(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object v7

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    add-int/lit8 v9, v6, -0x1

    aget-object v8, v8, v9

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v8

    add-int/lit8 v9, v4, -0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v8, v7}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->setMarkedValues([Z)V

    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v1, "ImportCsvSuccess"

    invoke-virtual {p1, v1, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v1, "ImportCsvError"

    invoke-virtual {p1, v1, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_2
    return-void
.end method

.method public final W()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->k:I

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->Y(I[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->K(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final X([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setAnswerKeys([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSheetTemplate(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    :cond_0
    return-void
.end method

.method public final Y(I[Ljava/lang/String;)V
    .locals 4

    .line 1
    const v0, 0x7f0901f1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f090347

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_0
    new-array v0, p1, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_1

    aget-object v3, p2, v2

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Landroid/widget/ArrayAdapter;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const v2, 0x7f0c00f0

    invoke-direct {p1, p2, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :goto_1
    return-void
.end method

.method public final Z()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f120031

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    const v2, 0x7f1201d3

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$h;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    const v3, 0x7f120337

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$g;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    const v3, 0x7f120241

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final a0()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0067

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202bf

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0902df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;Landroid/widget/RadioButton;)V

    const v1, 0x7f12023f

    invoke-virtual {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final b0()V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    const v2, 0x7f1201af

    const v3, 0x7f1201b0

    const v4, 0x7f120114

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final c0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;I)Z
    .locals 12

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v2

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const v4, 0x7f1200f0

    const v5, 0x7f08016e

    const v6, 0x7f0800bd

    const v7, 0x7f1202da

    const/4 v8, 0x0

    const-string v9, " "

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    if-gez p2, :cond_1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v0

    sub-int/2addr p2, v1

    aget-object p2, v0, p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-static {p1, p2, v6, v5}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return v8

    :cond_2
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object v2

    move v3, v8

    move v10, v3

    :goto_1
    array-length v11, v2

    if-ge v3, v11, :cond_5

    if-nez v10, :cond_4

    aget-boolean v10, v2, v3

    if-eqz v10, :cond_3

    goto :goto_2

    :cond_3
    move v10, v8

    goto :goto_3

    :cond_4
    :goto_2
    move v10, v1

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    if-nez v10, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    if-gez p2, :cond_6

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v0

    sub-int/2addr p2, v1

    aget-object p2, v0, p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_4
    invoke-static {p1, p2, v6, v5}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return v8

    :cond_7
    return v1
.end method

.method public final d0([Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;)Z
    .locals 4

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    aget-object p1, p1, v1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->c0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;I)Z

    move-result p1

    return p1

    :cond_0
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    add-int/lit8 v3, v0, -0x1

    aget-object v3, p1, v3

    invoke-virtual {p0, v3, v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->c0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;I)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    move v0, v1

    :cond_2
    array-length v3, p1

    if-ge v0, v3, :cond_3

    aget-object v3, p1, v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v3, v0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->c0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;I)Z

    move-result v3

    if-nez v3, :cond_2

    return v1

    :cond_3
    return v2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    const/16 v1, 0xca

    const/4 v2, -0x1

    if-ne p1, v1, :cond_2

    if-ne p2, v2, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "RESULT_DATA"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1, v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v3

    iput v3, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    if-ge v3, v0, :cond_0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    :cond_0
    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->g:[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    iget v5, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->l:I

    sub-int/2addr v5, v0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->setMarkedValues([Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->W()V

    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    if-ne p2, v2, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    :try_start_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->e:Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->V(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->W()V

    :cond_3
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->Z()V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0039

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const p1, 0x7f0900cc

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->n:Landroid/widget/Button;

    const p1, 0x7f0900b4

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->m:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->S()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->T()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->U()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f090042

    const/4 v2, 0x1

    if-eq v0, v1, :cond_3

    const v1, 0x7f090048

    if-eq v0, v1, :cond_2

    const v1, 0x7f09004f

    if-eq v0, v1, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->J()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->Q()V

    :cond_1
    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->b0()V

    return v2

    :cond_3
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->a0()V

    return v2
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    if-eqz p3, :cond_0

    array-length p2, p3

    if-lez p2, :cond_0

    const/4 p2, 0x0

    aget p2, p3, p2

    if-nez p2, :cond_0

    const/4 p2, 0x2

    if-ne p2, p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->Q()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->W()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/SetAnswerKeyActivity;->Z()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    const/4 v0, 0x1

    return v0
.end method

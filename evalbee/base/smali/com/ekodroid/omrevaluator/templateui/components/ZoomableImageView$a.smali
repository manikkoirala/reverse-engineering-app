.class public Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->w:Landroid/view/ScaleGestureDetector;

    invoke-virtual {p1, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    const/4 v0, 0x2

    aget v1, p1, v0

    const/4 v2, 0x5

    aget p1, p1, v2

    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_f

    const/4 v6, 0x0

    if-eq v4, v5, :cond_e

    if-eq v4, v0, :cond_2

    if-eq v4, v2, :cond_1

    const/4 p1, 0x6

    if-eq v4, p1, :cond_0

    goto/16 :goto_8

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput v6, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    goto/16 :goto_8

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    invoke-virtual {p1, v1, p2}, Landroid/graphics/PointF;->set(FF)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->d:Landroid/graphics/PointF;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    invoke-virtual {p2, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput v0, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    goto/16 :goto_8

    :cond_2
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget v2, p2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    if-eq v2, v0, :cond_3

    if-ne v2, v5, :cond_10

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    iget v2, p2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->e:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_10

    :cond_3
    iget v0, v3, Landroid/graphics/PointF;->x:F

    iget-object v2, p2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v4

    iget v4, v3, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v2

    iget v2, p2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->p:F

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    mul-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result p2

    int-to-float p2, p2

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->q:F

    iget v2, v2, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    mul-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget v7, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    cmpg-float p2, p2, v7

    const/4 v7, 0x0

    if-gez p2, :cond_6

    add-float p2, p1, v4

    cmpl-float v0, p2, v7

    if-lez v0, :cond_4

    :goto_0
    neg-float v4, p1

    goto :goto_1

    :cond_4
    iget v0, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->n:F

    neg-float v1, v0

    cmpg-float p2, p2, v1

    if-gez p2, :cond_5

    add-float/2addr p1, v0

    goto :goto_0

    :cond_5
    :goto_1
    move v0, v7

    goto :goto_7

    :cond_6
    iget p2, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->k:F

    cmpg-float p2, v2, p2

    if-gez p2, :cond_9

    add-float p1, v1, v0

    cmpl-float p2, p1, v7

    if-lez p2, :cond_7

    :goto_2
    neg-float v0, v1

    goto :goto_3

    :cond_7
    iget p2, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->m:F

    neg-float v2, p2

    cmpg-float p1, p1, v2

    if-gez p1, :cond_8

    add-float/2addr v1, p2

    goto :goto_2

    :cond_8
    :goto_3
    move v4, v7

    goto :goto_7

    :cond_9
    add-float p2, v1, v0

    cmpl-float v2, p2, v7

    if-lez v2, :cond_a

    :goto_4
    neg-float v0, v1

    goto :goto_5

    :cond_a
    iget v2, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->m:F

    neg-float v8, v2

    cmpg-float p2, p2, v8

    if-gez p2, :cond_b

    add-float/2addr v1, v2

    goto :goto_4

    :cond_b
    :goto_5
    add-float p2, p1, v4

    cmpl-float v1, p2, v7

    if-lez v1, :cond_c

    :goto_6
    neg-float v4, p1

    goto :goto_7

    :cond_c
    iget v1, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->n:F

    neg-float v2, v1

    cmpg-float p2, p2, v2

    if-gez p2, :cond_d

    add-float/2addr p1, v1

    goto :goto_6

    :cond_d
    :goto_7
    iget-object p1, v6, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    iget p2, v3, Landroid/graphics/PointF;->x:F

    iget v0, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, p2, v0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_8

    :cond_e
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput v6, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    iget p2, v3, Landroid/graphics/PointF;->x:F

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->d:Landroid/graphics/PointF;

    iget p1, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-int p1, p1

    iget p2, v3, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr p2, v0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    float-to-int p2, p2

    const/4 v0, 0x3

    if-ge p1, v0, :cond_10

    if-ge p2, v0, :cond_10

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    goto :goto_8

    :cond_f
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p2

    invoke-virtual {p1, v0, p2}, Landroid/graphics/PointF;->set(FF)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->d:Landroid/graphics/PointF;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    invoke-virtual {p2, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput v5, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    :cond_10
    :goto_8
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return v5
.end method

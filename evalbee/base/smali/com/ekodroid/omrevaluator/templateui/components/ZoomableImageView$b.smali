.class public Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 11

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    mul-float v3, v2, v0

    iput v3, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    iget v4, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->f:F

    cmpl-float v5, v3, v4

    if-lez v5, :cond_0

    :goto_0
    iput v4, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    div-float v0, v4, v2

    goto :goto_1

    :cond_0
    iget v4, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->e:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    iget v3, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    mul-float v4, v2, v3

    sub-float/2addr v4, v2

    iget v5, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->h:F

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    iput v4, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->m:F

    iget v4, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->k:F

    mul-float v5, v4, v3

    sub-float/2addr v5, v4

    iget v7, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->i:F

    mul-float/2addr v7, v6

    mul-float/2addr v7, v3

    sub-float/2addr v5, v7

    iput v5, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->n:F

    iget v5, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->p:F

    mul-float/2addr v5, v3

    cmpg-float v5, v5, v2

    const/4 v7, 0x5

    const/4 v8, 0x2

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    if-lez v5, :cond_6

    iget v5, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->q:F

    mul-float/2addr v5, v3

    cmpg-float v3, v5, v4

    if-gtz v3, :cond_2

    goto :goto_5

    :cond_2
    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result p1

    invoke-virtual {v1, v0, v0, v2, p1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    aget v2, v1, v8

    aget v1, v1, v7

    cmpg-float v0, v0, v9

    if-gez v0, :cond_a

    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->m:F

    neg-float v3, v0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_3

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    add-float/2addr v2, v0

    :goto_2
    neg-float v0, v2

    invoke-virtual {p1, v0, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_3

    :cond_3
    cmpl-float v0, v2, v10

    if-lez v0, :cond_4

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    goto :goto_2

    :cond_4
    :goto_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->n:F

    neg-float v2, v0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_5

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    add-float/2addr v1, v0

    :goto_4
    neg-float v0, v1

    invoke-virtual {p1, v10, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_6

    :cond_5
    cmpl-float v0, v1, v10

    if-lez v0, :cond_a

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    goto :goto_4

    :cond_6
    :goto_5
    iget-object p1, v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    div-float/2addr v2, v6

    div-float/2addr v4, v6

    invoke-virtual {p1, v0, v0, v2, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    cmpg-float p1, v0, v9

    if-gez p1, :cond_a

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    aget v2, v1, v8

    aget v1, v1, v7

    if-gez p1, :cond_a

    iget p1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->p:F

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    mul-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    int-to-float p1, p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    cmpg-float p1, p1, v3

    if-gez p1, :cond_8

    iget p1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->n:F

    neg-float v2, p1

    cmpg-float v2, v1, v2

    if-gez v2, :cond_7

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    add-float/2addr v1, p1

    neg-float p1, v1

    invoke-virtual {v0, v10, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_6

    :cond_7
    cmpl-float p1, v1, v10

    if-lez p1, :cond_a

    iget-object p1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    goto :goto_4

    :cond_8
    iget p1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->m:F

    neg-float v1, p1

    cmpg-float v1, v2, v1

    if-gez v1, :cond_9

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    add-float/2addr v2, p1

    neg-float p1, v2

    invoke-virtual {v0, p1, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_6

    :cond_9
    cmpl-float p1, v2, v10

    if-lez p1, :cond_a

    iget-object p1, v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    neg-float v0, v2

    invoke-virtual {p1, v0, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_a
    :goto_6
    const/4 p1, 0x1

    return p1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    const/4 v0, 0x2

    iput v0, p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    const/4 p1, 0x1

    return p1
.end method

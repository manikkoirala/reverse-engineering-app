.class public Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;
.super Lz6;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;
    }
.end annotation


# instance fields
.field public a:Landroid/graphics/Matrix;

.field public b:I

.field public c:Landroid/graphics/PointF;

.field public d:Landroid/graphics/PointF;

.field public e:F

.field public f:F

.field public g:[F

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public l:F

.field public m:F

.field public n:F

.field public p:F

.field public q:F

.field public t:F

.field public v:F

.field public w:Landroid/view/ScaleGestureDetector;

.field public x:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lz6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    const/4 p2, 0x0

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->b:I

    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->c:Landroid/graphics/PointF;

    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->d:Landroid/graphics/PointF;

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->e:F

    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->f:F

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/view/View;->setClickable(Z)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->x:Landroid/content/Context;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->w:Landroid/view/ScaleGestureDetector;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, p2, p2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    const/16 p1, 0x9

    new-array p1, p1, [F

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->g:[F

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    sget-object p1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;)V

    invoke-virtual {p0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method public onMeasure(II)V
    .locals 5

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->k:F

    iget p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->t:F

    div-float/2addr p2, v0

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->v:F

    div-float/2addr p1, v0

    invoke-static {p2, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p2, p1, p1}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    iget p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->k:F

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->v:F

    mul-float/2addr v0, p1

    sub-float/2addr p2, v0

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->t:F

    mul-float/2addr p1, v1

    sub-float/2addr v0, p1

    const/high16 p1, 0x40000000    # 2.0f

    div-float/2addr p2, p1

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->i:F

    div-float/2addr v0, p1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->h:F

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->j:F

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->h:F

    mul-float v1, v0, p1

    sub-float v1, p2, v1

    iput v1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->p:F

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->k:F

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->i:F

    mul-float v3, v2, p1

    sub-float v3, v1, v3

    iput v3, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->q:F

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->l:F

    mul-float v4, p2, v3

    sub-float/2addr v4, p2

    mul-float/2addr v0, p1

    mul-float/2addr v0, v3

    sub-float/2addr v4, v0

    iput v4, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->m:F

    mul-float p2, v1, v3

    sub-float/2addr p2, v1

    mul-float/2addr v2, p1

    mul-float/2addr v2, v3

    sub-float/2addr p2, v2

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->n:F

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    invoke-super {p0, p1}, Lz6;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->t:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->v:F

    return-void
.end method

.method public setMaxZoom(F)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->f:F

    return-void
.end method

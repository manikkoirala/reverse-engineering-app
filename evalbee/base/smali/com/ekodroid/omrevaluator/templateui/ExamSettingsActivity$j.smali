.class public Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lig0$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->M()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;->a:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;->a:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->E(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;->a:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->D(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setInvalidQuestionSets([Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;)V

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSheetTemplate(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;->a:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->E(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity$j;->a:Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;->F(Lcom/ekodroid/omrevaluator/templateui/ExamSettingsActivity;)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;
    }
.end annotation


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

.field public d:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/a;

.field public f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public g:I

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/ImageButton;

.field public j:Landroid/widget/ImageButton;

.field public k:Landroid/widget/TextView;

.field public l:I

.field public m:Landroid/graphics/Bitmap;

.field public n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/a;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/a;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->g:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->l:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->m:Landroid/graphics/Bitmap;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->O()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->M(I)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;ILjava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->G(ILjava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static P(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 7

    .line 1
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final D(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V
    .locals 10

    .line 1
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    const/high16 v1, 0x42700000    # 60.0f

    sub-float/2addr v0, v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v2

    sub-float/2addr v2, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    const/16 v3, 0x3e8

    if-le v1, v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v3

    :cond_0
    div-float v1, v0, v2

    float-to-double v4, v1

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-double v6, v1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v6, v8

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v1

    int-to-double v8, v1

    div-double/2addr v6, v8

    cmpl-double v1, v4, v6

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v2, v0, v1

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v2, v0

    int-to-float v0, v3

    div-float/2addr v2, v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v5, v0, v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    sub-float/2addr p2, v0

    div-float v6, p2, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result p2

    int-to-float p2, p2

    mul-float v7, p2, v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result p2

    int-to-float p2, p2

    mul-float v8, p2, v2

    move-object v3, p3

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;FFFF)V

    const-string p1, "TAG"

    const-string p2, "image converted to pdf"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public final E()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800b3

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final F()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0086

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f120089

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0902de

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    const v3, 0x7f0902e8

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$c;

    invoke-direct {v1, p0, v2}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;Landroid/widget/RadioButton;)V

    const v2, 0x7f1202d4

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final G(ILjava/lang/String;)Z
    .locals 9

    .line 1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v2

    array-length v2, v2

    move v3, v0

    :goto_0
    if-ge v3, v2, :cond_1

    new-instance v4, Lorg/apache/pdfbox/pdmodel/PDPage;

    sget-object v5, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A4:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v4, v5}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v1, v4}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, p1, v3, v6}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->H(IILcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Landroid/graphics/Bitmap;

    move-result-object v6

    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v1, v6}, Lorg/apache/pdfbox/pdmodel/graphics/image/JPEGFactory;->createFromStream(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object v5

    new-instance v6, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;

    invoke-direct {v6, v1, v4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p0, v5, v4, v6}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->D(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->close()V

    add-int/2addr v3, p1

    goto :goto_0

    :cond_1
    invoke-virtual {v1, p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return v0
.end method

.method public final H(IILcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Landroid/graphics/Bitmap;
    .locals 12

    .line 1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/a;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-virtual {p1, v0, p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1

    :cond_0
    const/high16 v1, 0x42b40000    # 90.0f

    const-wide v2, 0x3ff3333333333333L    # 1.2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/16 v7, 0xff

    if-ne p1, v6, :cond_2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v8

    array-length v8, v8

    if-ne v8, v0, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/a;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    int-to-double p2, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-double v8, v0

    mul-double/2addr v8, v2

    cmpl-double p2, p2, v8

    if-lez p2, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {p1, v1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->P(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    mul-int/2addr p3, v6

    add-int/lit16 p3, p3, 0x96

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p2

    new-instance p3, Landroid/graphics/Canvas;

    invoke-direct {p3, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p3, v7, v7, v7, v7}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    invoke-virtual {p3, p1, v4, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/lit16 v0, v0, 0x95

    int-to-float v0, v0

    invoke-virtual {p3, p1, v4, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-object p2

    :cond_2
    if-ne p1, v6, :cond_9

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object p1

    array-length p1, p1

    if-le p1, v0, :cond_9

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/a;

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-virtual {p1, v8, p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-double v8, v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-double v10, v10

    mul-double/2addr v10, v2

    cmpl-double v8, v8, v10

    if-lez v8, :cond_3

    goto :goto_1

    :cond_3
    invoke-static {p1, v1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->P(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object p1

    :goto_1
    add-int/2addr p2, v0

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v0

    array-length v0, v0

    if-ge p2, v0, :cond_5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/a;

    iget-object v8, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-virtual {v0, v8, p3, p2}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    int-to-double v8, p3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    int-to-double v10, p3

    mul-double/2addr v10, v2

    cmpl-double p3, v8, v10

    if-lez p3, :cond_4

    goto :goto_2

    :cond_4
    invoke-static {p2, v1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->P(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object p2

    goto :goto_2

    :cond_5
    move-object p2, v5

    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/2addr v0, v6

    add-int/lit16 v0, v0, 0x96

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_6

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    :cond_6
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_7

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/2addr v0, v6

    add-int/lit16 v0, v0, 0x96

    :cond_7
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p3

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    invoke-virtual {v1, p1, v4, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    if-eqz p2, :cond_8

    div-int/2addr v0, v6

    add-int/lit8 v0, v0, -0x4b

    int-to-float p1, v0

    invoke-virtual {v1, p2, v4, p1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_8
    return-object p3

    :cond_9
    return-object v5
.end method

.method public final I()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final J()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->j:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final K()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->i:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L()V
    .locals 1

    .line 1
    const v0, 0x7f0901b6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->j:Landroid/widget/ImageButton;

    const v0, 0x7f0901b8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->i:Landroid/widget/ImageButton;

    const v0, 0x7f0903fd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->k:Landroid/widget/TextView;

    const v0, 0x7f09021d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->h:Landroid/widget/LinearLayout;

    const v0, 0x7f09048f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    return-void
.end method

.method public final M(I)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;)V

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;->a(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;I)Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$e;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public final N()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v0

    array-length v0, v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->h:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->g:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->i:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->j:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v0, -0x1

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->i:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->j:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->i:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :goto_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->k:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->g:I

    add-int/2addr v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->h:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public final O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080171

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->e:Lcom/ekodroid/omrevaluator/templateui/models/a;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->g:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->m:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->N()V

    return-void
.end method

.method public final Q()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lok;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "[^a-zA-Z0-9_-]"

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->m:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {p0, v3}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->R(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_2

    :catchall_0
    move-exception v1

    move-object v2, v0

    goto :goto_3

    :catch_0
    move-exception v1

    move-object v2, v0

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v2, v0

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    :goto_0
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_2

    :catch_3
    move-exception v1

    :goto_1
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_2
    return-void

    :goto_3
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_4
    throw v1
.end method

.method public final R(Ljava/io/File;)V
    .locals 9

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "[^a-zA-Z0-9_-]"

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;Ljava/io/File;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const v3, 0x7f1200e8

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f1200e7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " : "

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lok;->c:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f1202e8

    const v6, 0x7f120060

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->I()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0048

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "EXAM_TYPE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const v0, 0x7f090436

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->E()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->J()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->K()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    const-string v0, "ARCHIVED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Le8;->a:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    :goto_0
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    goto :goto_0

    :cond_1
    :goto_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->f:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_2
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const v0, 0x7f09003e

    if-eq p1, v0, :cond_2

    const v0, 0x7f090043

    if-eq p1, v0, :cond_1

    const v0, 0x7f090045

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;

    const/4 v2, 0x0

    const v3, 0x7f1200ad

    const v4, 0x7f120194

    const v5, 0x7f120060

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->Q()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->F()V

    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->O()V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/ViewTemplateActivity;->I()V

    const/4 v0, 0x1

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private answerValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;"
        }
    .end annotation
.end field

.field private examSet:I

.field private sectionsInSubjects:[I


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;[II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;[II)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->answerValues:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->sectionsInSubjects:[I

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->examSet:I

    return-void
.end method


# virtual methods
.method public getAnswerValue2s()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->answerValues:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExamSet()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->examSet:I

    return v0
.end method

.method public getMarksForEachSubject()[D
    .locals 8

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->sectionsInSubjects:[I

    array-length v0, v0

    new-array v0, v0, [D

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->answerValues:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v3

    aget-wide v4, v0, v3

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v6

    invoke-static {v6, v7}, Lve1;->o(D)D

    move-result-wide v6

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getSectionsInSubjects()[I
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->sectionsInSubjects:[I

    return-object v0
.end method

.method public getTotalMarks()D
    .locals 5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->answerValues:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v3

    add-double/2addr v1, v3

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public setAnswerValues(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->answerValues:Ljava/util/ArrayList;

    return-void
.end method

.method public setExamSet(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->examSet:I

    return-void
.end method

.method public setSectionsInSubjects([I)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->sectionsInSubjects:[I

    return-void
.end method

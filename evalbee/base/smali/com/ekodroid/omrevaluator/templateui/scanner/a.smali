.class public Lcom/ekodroid/omrevaluator/templateui/scanner/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:Ld91;


# direct methods
.method public constructor <init>(Ld91;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->b:Ld91;

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    .locals 3

    .line 1
    monitor-enter p0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->f(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    invoke-virtual {p0, v0, v1, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->e(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_2
    :try_start_2
    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->checkedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->checkedBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->b:Ld91;

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ld91;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final b([Z[Z)Z
    .locals 5

    .line 1
    array-length v0, p1

    array-length v1, p2

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    const/4 v0, 0x1

    move v3, v0

    move v1, v2

    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_3

    if-eqz v3, :cond_1

    aget-boolean v3, p1, v1

    aget-boolean v4, p2, v1

    if-ne v3, v4, :cond_1

    move v3, v0

    goto :goto_1

    :cond_1
    move v3, v2

    :goto_1
    if-nez v3, :cond_2

    return v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return v3
.end method

.method public final c(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getMarkedAnswers()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getMarkedAnswers()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getRollNumber()I

    move-result p1

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getRollNumber()I

    move-result p2

    const/4 v2, 0x0

    if-ne p1, p2, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-eq p1, p2, :cond_0

    goto :goto_2

    :cond_0
    const/4 p1, 0x1

    move v3, p1

    move p2, v2

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge p2, v4, :cond_3

    if-eqz v3, :cond_1

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    invoke-virtual {p0, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->b([Z[Z)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, p1

    goto :goto_1

    :cond_1
    move v3, v2

    :goto_1
    if-nez v3, :cond_2

    return v2

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    return v3

    :cond_4
    :goto_2
    return v2
.end method

.method public d()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final e(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getRollNumber()I

    move-result v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getRollNumber()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getRollNumber()I

    move-result v0

    iput v0, p3, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->rollNumber:I

    :cond_0
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->getMarkedAnswers()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_3

    move v3, v1

    :goto_1
    iget-object v4, p3, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    array-length v4, v4

    if-ge v3, v4, :cond_2

    iget-object v4, p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    aget-boolean v4, v4, v3

    iget-object v5, p2, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    aget-boolean v5, v5, v3

    if-ne v4, v5, :cond_1

    iget-object v4, p3, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    iget-object v5, p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    aget-boolean v5, v5, v3

    aput-boolean v5, v4, v3

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object p3
.end method

.method public final f(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    invoke-virtual {p0, v1, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/a;->c(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

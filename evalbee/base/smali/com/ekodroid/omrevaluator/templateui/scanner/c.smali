.class public Lcom/ekodroid/omrevaluator/templateui/scanner/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->a:Z

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->DEFAULT:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    return-void
.end method


# virtual methods
.method public a(Lj5;Lhn0;Ld91;Lej1;ZZI)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    .locals 7

    .line 1
    move-object v0, p2

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p7

    invoke-virtual/range {v0 .. v5}, Lhn0;->d(Lj5;Ld91;Lej1;ZI)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    move-result-object v4

    const/4 p2, 0x0

    if-nez v4, :cond_0

    iget-object p3, p1, Lj5;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V

    iput-object p2, p1, Lj5;->a:Landroid/graphics/Bitmap;

    return-object p2

    :cond_0
    new-instance v3, Lsn1;

    invoke-direct {v3, v4}, Lsn1;-><init>(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;)V

    move-object v1, p0

    move-object v2, p1

    move v5, p6

    move v6, p7

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->d(Lj5;Lsn1;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;ZI)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-result-object p1
    :try_end_0
    .catch Lcom/ekodroid/omrevaluator/templateui/scanner/ScanningException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object p2
.end method

.method public final b(Landroid/graphics/Bitmap;Lsn1;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[IDZ)Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;
    .locals 6

    .line 1
    invoke-virtual {p2, p3, p4}, Lsn1;->b(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v0

    double-to-int v2, p5

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    iget v5, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    move-object v1, p1

    move v4, p7

    invoke-static/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->e([Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Landroid/graphics/Bitmap;ILcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;ZI)[Z

    move-result-object p4

    iget p5, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget p6, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    iget-object p2, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget p7, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    iget-object v1, p3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    move-object p1, v0

    move p3, p7

    move-object p7, v1

    invoke-direct/range {p1 .. p7}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZIILjava/lang/String;)V

    return-object v0
.end method

.method public final c(Landroid/graphics/Bitmap;Lsn1;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;ZI)I
    .locals 11

    .line 1
    move/from16 v0, p6

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v1

    iget v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    const/4 v4, -0x1

    if-eq v1, v0, :cond_1

    return v4

    :cond_1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v1

    move-object v5, p3

    invoke-virtual {p3, v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v0

    move-object v5, p2

    invoke-virtual {p2, v1, v0}, Lsn1;->b(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v5

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v0

    double-to-int v7, v0

    move-object v0, p0

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    const/4 v10, -0x1

    move-object v6, p1

    move/from16 v9, p5

    invoke-static/range {v5 .. v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->e([Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Landroid/graphics/Bitmap;ILcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;ZI)[Z

    move-result-object v1

    :goto_0
    array-length v5, v1

    if-ge v2, v5, :cond_3

    aget-boolean v5, v1, v2

    if-eqz v5, :cond_2

    add-int/2addr v2, v3

    return v2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return v4

    :cond_4
    :goto_1
    move-object v0, p0

    return v2
.end method

.method public final d(Lj5;Lsn1;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;ZI)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
    .locals 16

    .line 1
    move-object/from16 v0, p1

    move/from16 v11, p5

    iget-object v1, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v12

    iget-object v13, v0, Lj5;->a:Landroid/graphics/Bitmap;

    iget-object v4, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-object/from16 v1, p0

    move-object v2, v13

    move-object/from16 v3, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-virtual/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->c(Landroid/graphics/Bitmap;Lsn1;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;ZI)I

    move-result v14

    iget-object v1, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollStartDigit()I

    move-result v8

    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v6, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    iget-object v1, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1, v11}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v1

    iget v2, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v15, 0x1

    sub-int/2addr v2, v15

    aget v7, v1, v2

    move-object/from16 v1, p0

    move-object v2, v13

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-virtual/range {v1 .. v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->e(Landroid/graphics/Bitmap;Lsn1;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;IIIZI)I

    move-result v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_1

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v1, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    if-ne v1, v11, :cond_0

    iget-object v1, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1, v11}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v6

    move-object/from16 v1, p0

    move-object v2, v13

    move-object/from16 v3, p2

    move/from16 v8, p4

    invoke-virtual/range {v1 .. v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->b(Landroid/graphics/Bitmap;Lsn1;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[IDZ)Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_1
    new-instance v7, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;

    move-object v0, v7

    move-object v1, v13

    move v2, v9

    move v3, v14

    move-object v4, v10

    move-object/from16 v5, p3

    move/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;-><init>(Landroid/graphics/Bitmap;IILjava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;I)V

    return-object v7
.end method

.method public final e(Landroid/graphics/Bitmap;Lsn1;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;IIIZI)I
    .locals 14

    .line 1
    move-object/from16 v0, p3

    move/from16 v1, p5

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->ID_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v2, v3, :cond_5

    iget v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    const/4 v3, 0x0

    move/from16 v4, p9

    if-eq v2, v4, :cond_0

    return v3

    :cond_0
    new-array v2, v1, [I

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    move-object/from16 v5, p2

    move/from16 v6, p6

    invoke-virtual {v5, v4, v1, v6}, Lsn1;->h(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v4

    move v5, v3

    :goto_0
    const/16 v6, 0xa

    if-ge v5, v6, :cond_3

    aget-object v7, v4, v5

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v8

    double-to-int v9, v8

    move-object v13, p0

    iget-object v10, v13, Lcom/ekodroid/omrevaluator/templateui/scanner/c;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    iget v12, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    move-object v8, p1

    move/from16 v11, p8

    invoke-static/range {v7 .. v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->e([Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Landroid/graphics/Bitmap;ILcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;ZI)[Z

    move-result-object v7

    move v8, v3

    :goto_1
    aget-object v9, v4, v5

    array-length v9, v9

    if-ge v8, v9, :cond_2

    aget-boolean v9, v7, v8

    if-eqz v9, :cond_1

    add-int v9, v5, p7

    rem-int/2addr v9, v6

    aput v9, v2, v8

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    move-object v13, p0

    move v0, v3

    :goto_2
    if-ge v3, v1, :cond_4

    add-int/lit8 v4, v1, -0x1

    sub-int/2addr v4, v3

    aget v4, v2, v4

    const-wide/high16 v5, 0x4024000000000000L    # 10.0

    int-to-double v7, v3

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    double-to-int v5, v5

    mul-int/2addr v4, v5

    add-int/2addr v0, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    return v0

    :cond_5
    move-object v13, p0

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScanningException;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScanningException;-><init>()V

    throw v0
.end method

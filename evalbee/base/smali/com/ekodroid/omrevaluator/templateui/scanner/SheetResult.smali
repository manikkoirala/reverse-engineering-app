.class public Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field checkedBitmap:Landroid/graphics/Bitmap;

.field examSet:I

.field markedAnswers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;"
        }
    .end annotation
.end field

.field pageIndex:I

.field rollNumber:I

.field sheetDimension:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;IILjava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "II",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->checkedBitmap:Landroid/graphics/Bitmap;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->rollNumber:I

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->sheetDimension:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->examSet:I

    iput p6, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->pageIndex:I

    return-void
.end method


# virtual methods
.method public getCheckedBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->checkedBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getExamSet()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->examSet:I

    return v0
.end method

.method public getMarkedAnswers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->markedAnswers:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRollNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->rollNumber:I

    return v0
.end method

.method public getSheetDimension()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetResult;->sheetDimension:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    return-object v0
.end method

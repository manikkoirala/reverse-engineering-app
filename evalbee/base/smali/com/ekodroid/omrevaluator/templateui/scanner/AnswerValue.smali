.class public Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;
    }
.end annotation


# instance fields
.field markedState:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field markedValues:[Z

.field marksForAnswer:D

.field payload:Ljava/lang/String;

.field questionNumber:I

.field sectionId:I

.field subjectId:I

.field type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZIILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iput p2, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->questionNumber:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->subjectId:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->sectionId:I

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->payload:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedState:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    return-object v0
.end method

.method public getMarkedValues()[Z
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    return-object v0
.end method

.method public getMarksForAnswer()D
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->marksForAnswer:D

    return-wide v0
.end method

.method public getPayload()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->payload:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->questionNumber:I

    return v0
.end method

.method public getSectionId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->sectionId:I

    return v0
.end method

.method public getSubjectId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->subjectId:I

    return v0
.end method

.method public getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0
.end method

.method public setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedState:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    return-void
.end method

.method public setMarkedValues([Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->markedValues:[Z

    return-void
.end method

.method public setMarksForAnswer(D)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->marksForAnswer:D

    return-void
.end method

.method public setPayload(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->payload:Ljava/lang/String;

    return-void
.end method

.class public abstract Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;
    }
.end annotation


# direct methods
.method public static a(Landroid/graphics/Bitmap;IIIII)D
    .locals 18

    .line 1
    move/from16 v0, p1

    move/from16 v1, p2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    move/from16 v4, p5

    int-to-double v4, v4

    div-double/2addr v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ge v1, v5, :cond_0

    move v6, v5

    goto :goto_0

    :cond_0
    move v6, v4

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    if-le v1, v7, :cond_1

    move v7, v5

    goto :goto_1

    :cond_1
    move v7, v4

    :goto_1
    or-int/2addr v6, v7

    if-ge v0, v5, :cond_2

    move v7, v5

    goto :goto_2

    :cond_2
    move v7, v4

    :goto_2
    or-int/2addr v6, v7

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    if-le v0, v7, :cond_3

    move v7, v5

    goto :goto_3

    :cond_3
    move v7, v4

    :goto_3
    or-int/2addr v6, v7

    const-wide/16 v7, 0x0

    if-eqz v6, :cond_4

    return-wide v7

    :cond_4
    div-int/lit8 v6, p4, 0x2

    sub-int v6, v1, v6

    invoke-static {v6, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v6

    add-int/lit8 v9, p4, 0x1

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v1, v9

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-static {v1, v9}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result v1

    div-int/lit8 v9, p3, 0x2

    sub-int v9, v0, v9

    invoke-static {v9, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v9

    add-int/lit8 v10, p3, 0x1

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v0, v10

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-static {v0, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result v0

    int-to-double v9, v9

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v13, 0x3fe8000000000000L    # 0.75

    move v15, v6

    :goto_4
    int-to-double v5, v0

    cmpg-double v5, v9, v5

    move-wide/from16 p1, v11

    if-gez v5, :cond_8

    move v5, v15

    int-to-double v11, v5

    move/from16 p3, v5

    move-wide v15, v13

    move-wide/from16 v13, p1

    :goto_5
    int-to-double v5, v1

    cmpg-double v5, v11, v5

    if-gez v5, :cond_7

    double-to-int v5, v9

    double-to-int v6, v11

    move/from16 p4, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v5

    cmpl-double v17, v5, v15

    if-lez v17, :cond_5

    move-wide v15, v5

    :cond_5
    cmpg-double v17, v5, v13

    if-gez v17, :cond_6

    move-wide v13, v5

    :cond_6
    add-double/2addr v7, v5

    add-int/lit8 v4, v4, 0x1

    add-double/2addr v11, v2

    move/from16 v0, p4

    goto :goto_5

    :cond_7
    move/from16 p4, v0

    move-object/from16 v0, p0

    add-double/2addr v9, v2

    move/from16 v0, p4

    move-wide v11, v13

    move-wide v13, v15

    move/from16 v15, p3

    goto :goto_4

    :cond_8
    if-nez v4, :cond_9

    const/4 v5, 0x1

    goto :goto_6

    :cond_9
    move v5, v4

    :goto_6
    int-to-double v0, v5

    div-double/2addr v7, v0

    move-wide/from16 v11, p1

    add-double/2addr v13, v11

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    div-double/2addr v13, v0

    cmpl-double v0, v7, v13

    if-lez v0, :cond_a

    return-wide v13

    :cond_a
    return-wide v7
.end method

.method public static b(Landroid/graphics/Bitmap;IIIIID)D
    .locals 19

    .line 1
    move/from16 v0, p1

    move/from16 v1, p2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    move/from16 v4, p5

    int-to-double v4, v4

    div-double/2addr v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ge v1, v5, :cond_0

    move v6, v5

    goto :goto_0

    :cond_0
    move v6, v4

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    if-le v1, v7, :cond_1

    move v7, v5

    goto :goto_1

    :cond_1
    move v7, v4

    :goto_1
    or-int/2addr v6, v7

    if-ge v0, v5, :cond_2

    move v7, v5

    goto :goto_2

    :cond_2
    move v7, v4

    :goto_2
    or-int/2addr v6, v7

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    if-le v0, v7, :cond_3

    move v7, v5

    goto :goto_3

    :cond_3
    move v7, v4

    :goto_3
    or-int/2addr v6, v7

    const-wide/high16 v7, 0x4008000000000000L    # 3.0

    if-eqz v6, :cond_4

    return-wide v7

    :cond_4
    div-int/lit8 v6, p4, 0x2

    sub-int v6, v1, v6

    invoke-static {v6, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v6

    add-int/lit8 v9, p4, 0x1

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v1, v9

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-static {v1, v9}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result v1

    div-int/lit8 v9, p3, 0x2

    sub-int v9, v0, v9

    invoke-static {v9, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v9

    add-int/lit8 v10, p3, 0x1

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v0, v10

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    add-int/lit8 v10, v10, -0x2

    invoke-static {v0, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result v0

    int-to-double v9, v9

    const-wide/16 v13, 0x0

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    :goto_4
    int-to-double v11, v0

    cmpg-double v11, v9, v11

    if-gez v11, :cond_8

    int-to-double v11, v6

    move/from16 v17, v6

    :goto_5
    int-to-double v5, v1

    cmpg-double v5, v11, v5

    if-gez v5, :cond_7

    double-to-int v5, v9

    double-to-int v6, v11

    move/from16 p3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v5

    cmpl-double v18, v5, v13

    if-lez v18, :cond_5

    move-wide v13, v5

    :cond_5
    cmpg-double v18, v5, v15

    if-gez v18, :cond_6

    move-wide v15, v5

    :cond_6
    add-double/2addr v7, v5

    add-int/lit8 v4, v4, 0x1

    add-double/2addr v11, v2

    move/from16 v0, p3

    goto :goto_5

    :cond_7
    move/from16 p3, v0

    move-object/from16 v0, p0

    add-double/2addr v9, v2

    move/from16 v0, p3

    move/from16 v6, v17

    const/4 v5, 0x1

    goto :goto_4

    :cond_8
    if-nez v4, :cond_9

    const/4 v5, 0x1

    goto :goto_6

    :cond_9
    move v5, v4

    :goto_6
    int-to-double v0, v5

    div-double/2addr v7, v0

    sub-double/2addr v13, v7

    const-wide v0, 0x3fc999999999999aL    # 0.2

    add-double v0, p6, v0

    mul-double/2addr v13, v0

    add-double/2addr v7, v13

    const-wide v0, 0x3fd3333333333333L    # 0.3

    add-double v0, p6, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v7, v2

    if-gez v2, :cond_a

    cmpl-double v2, v7, v0

    if-lez v2, :cond_a

    return-wide v7

    :cond_a
    return-wide v0
.end method

.method public static c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 23

    .line 1
    move-object/from16 v7, p0

    move/from16 v8, p1

    move/from16 v0, p2

    move/from16 v1, p3

    move-wide/from16 v9, p5

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ge v1, v8, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v2

    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v5, v8

    if-le v1, v5, :cond_1

    move v5, v3

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    or-int/2addr v4, v5

    if-ge v0, v8, :cond_2

    move v5, v3

    goto :goto_2

    :cond_2
    move v5, v2

    :goto_2
    or-int/2addr v4, v5

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v5, v8

    if-le v0, v5, :cond_3

    move v5, v3

    goto :goto_3

    :cond_3
    move v5, v2

    :goto_3
    or-int/2addr v4, v5

    if-eqz v4, :cond_4

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    int-to-double v3, v0

    int-to-double v0, v1

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;-><init>(DD)V

    return-object v2

    :cond_4
    new-instance v11, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-direct {v11}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;-><init>()V

    move v4, v2

    move v5, v4

    :goto_4
    if-ge v4, v8, :cond_7

    add-int v6, v1, v4

    add-int/lit8 v12, v6, 0x2

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    if-le v12, v13, :cond_5

    goto :goto_5

    :cond_5
    invoke-virtual {v7, v0, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v12

    invoke-static {v12, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v12

    if-nez v12, :cond_6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v7, v0, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-static {v6, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v6

    if-nez v6, :cond_6

    goto :goto_5

    :cond_6
    add-int/lit8 v5, v4, 0x1

    move/from16 v22, v5

    move v5, v4

    move/from16 v4, v22

    goto :goto_4

    :cond_7
    :goto_5
    move v4, v2

    move v6, v4

    :goto_6
    if-ge v4, v8, :cond_a

    sub-int v12, v1, v4

    add-int/lit8 v13, v12, -0x2

    if-gez v13, :cond_8

    goto :goto_7

    :cond_8
    invoke-virtual {v7, v0, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    invoke-static {v13, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v13

    if-nez v13, :cond_9

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v7, v0, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v12

    invoke-static {v12, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v12

    if-nez v12, :cond_9

    goto :goto_7

    :cond_9
    add-int/lit8 v6, v4, 0x1

    move/from16 v22, v6

    move v6, v4

    move/from16 v4, v22

    goto :goto_6

    :cond_a
    :goto_7
    move v4, v2

    move v12, v4

    :goto_8
    if-ge v4, v8, :cond_d

    sub-int v13, v0, v4

    add-int/lit8 v14, v13, -0x2

    if-gez v14, :cond_b

    goto :goto_9

    :cond_b
    invoke-virtual {v7, v13, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v14

    invoke-static {v14, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v14

    if-nez v14, :cond_c

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v7, v13, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    invoke-static {v13, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v13

    if-nez v13, :cond_c

    goto :goto_9

    :cond_c
    add-int/lit8 v12, v4, 0x1

    move/from16 v22, v12

    move v12, v4

    move/from16 v4, v22

    goto :goto_8

    :cond_d
    :goto_9
    move v4, v2

    :goto_a
    if-ge v2, v8, :cond_10

    add-int v13, v0, v2

    add-int/lit8 v14, v13, 0x2

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    if-le v14, v15, :cond_e

    goto :goto_b

    :cond_e
    invoke-virtual {v7, v13, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v14

    invoke-static {v14, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v14

    if-nez v14, :cond_f

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v7, v13, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    invoke-static {v13, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v13

    if-nez v13, :cond_f

    goto :goto_b

    :cond_f
    add-int/lit8 v4, v2, 0x1

    move/from16 v22, v4

    move v4, v2

    move/from16 v2, v22

    goto :goto_a

    :cond_10
    :goto_b
    sub-int v2, v5, v6

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sub-int v2, v4, v12

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    add-int/2addr v5, v6

    add-int/2addr v5, v3

    add-int/2addr v4, v12

    add-int/2addr v4, v3

    invoke-static {v5, v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v12

    move v13, v0

    move v14, v1

    :goto_c
    move-object/from16 v0, p0

    move v1, v12

    move v2, v12

    move v3, v13

    move v4, v14

    move-wide/from16 v5, p5

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v15

    add-int/lit8 v17, v14, 0x1

    move/from16 v4, v17

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v18

    add-int/lit8 v20, v14, -0x1

    move/from16 v3, v20

    move v4, v13

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v0

    cmpg-double v0, v18, v0

    if-gez v0, :cond_11

    move/from16 v17, v20

    :cond_11
    add-int/lit8 v18, v13, -0x1

    move-object/from16 v0, p0

    move v1, v12

    move v2, v12

    move/from16 v3, v18

    move v4, v14

    move-wide/from16 v5, p5

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v19

    add-int/lit8 v21, v13, 0x1

    move v3, v14

    move/from16 v4, v21

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v0

    cmpl-double v0, v19, v0

    if-lez v0, :cond_12

    goto :goto_d

    :cond_12
    move/from16 v18, v21

    :goto_d
    move-object/from16 v0, p0

    move v1, v12

    move v2, v12

    move/from16 v3, v18

    move/from16 v4, v17

    move-wide/from16 v5, p5

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v0

    cmpg-double v0, v0, v15

    if-gtz v0, :cond_14

    int-to-double v0, v13

    iput-wide v0, v11, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    int-to-double v0, v14

    iput-wide v0, v11, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    if-eqz p4, :cond_13

    return-object v11

    :cond_13
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move v2, v13

    move v3, v14

    move-wide/from16 v5, p5

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v0

    return-object v0

    :cond_14
    move/from16 v14, v17

    move/from16 v13, v18

    goto :goto_c
.end method

.method public static d(Landroid/graphics/Bitmap;IIIID)D
    .locals 6

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ge p4, v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-le p4, v3, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v0

    :goto_1
    or-int/2addr v2, v3

    if-ge p3, v1, :cond_2

    move v3, v1

    goto :goto_2

    :cond_2
    move v3, v0

    :goto_2
    or-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-le p3, v3, :cond_3

    move v0, v1

    :cond_3
    or-int/2addr v0, v2

    const-wide/16 v2, 0x0

    if-eqz v0, :cond_4

    return-wide v2

    :cond_4
    div-int/lit8 v0, p1, 0x2

    sub-int v0, p4, v0

    invoke-static {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v0

    add-int/2addr p1, v1

    div-int/lit8 p1, p1, 0x2

    add-int/2addr p4, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    add-int/lit8 p1, p1, -0x2

    invoke-static {p4, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result p1

    div-int/lit8 p4, p2, 0x2

    sub-int p4, p3, p4

    invoke-static {p4, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result p4

    add-int/2addr p2, v1

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p3, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    add-int/lit8 p2, p2, -0x2

    invoke-static {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result p2

    :goto_3
    if-ge v0, p1, :cond_7

    move p3, p4

    :goto_4
    if-ge p3, p2, :cond_6

    invoke-virtual {p0, p3, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, p5, p6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    :cond_5
    add-int/lit8 p3, p3, 0x1

    goto :goto_4

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    return-wide v2
.end method

.method public static e([Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Landroid/graphics/Bitmap;ILcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;ZI)[Z
    .locals 10

    .line 1
    array-length p3, p0

    new-array p3, p3, [Z

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-object v1, p0, v0

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    move-object v1, p1

    move v4, p2

    move v7, p4

    move v8, p5

    move v9, v0

    invoke-static/range {v1 .. v9}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->m(Landroid/graphics/Bitmap;IIIDZII)Z

    move-result v1

    aput-boolean v1, p3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p3
.end method

.method public static f(I)D
    .locals 6

    .line 1
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3fd322d0e5604189L    # 0.299

    mul-double/2addr v0, v2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x3fe2c8b439581062L    # 0.587

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result p0

    int-to-double v2, p0

    const-wide v4, 0x3fbd2f1a9fbe76c9L    # 0.114

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x406fe00000000000L    # 255.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v0

    return-wide v2
.end method

.method public static g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 7

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;-><init>()V

    iget-wide v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    iget-wide v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    sub-double/2addr v3, v1

    int-to-double v5, p2

    mul-double/2addr v3, v5

    add-int/lit8 p3, p3, -0x1

    int-to-double p2, p3

    div-double/2addr v3, p2

    add-double/2addr v1, v3

    iput-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    iget-wide v1, p0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    iget-wide p0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr p0, v1

    mul-double/2addr p0, v5

    div-double/2addr p0, p2

    add-double/2addr v1, p0

    iput-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    return-object v0
.end method

.method public static h(Landroid/graphics/Bitmap;IIID)Z
    .locals 7

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ge p2, v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-le p2, v3, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v0

    :goto_1
    or-int/2addr v2, v3

    if-ge p1, v1, :cond_2

    move v3, v1

    goto :goto_2

    :cond_2
    move v3, v0

    :goto_2
    or-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-le p1, v3, :cond_3

    move v3, v1

    goto :goto_3

    :cond_3
    move v3, v0

    :goto_3
    or-int/2addr v2, v3

    if-eqz v2, :cond_4

    return v0

    :cond_4
    div-int/lit8 v2, p3, 0x2

    sub-int v3, p2, v2

    invoke-static {v3, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v3

    add-int/2addr p3, v1

    div-int/lit8 p3, p3, 0x2

    add-int/2addr p2, p3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-static {p2, v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result p2

    sub-int v2, p1, v2

    invoke-static {v2, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v2

    add-int/2addr p1, p3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    add-int/lit8 p3, p3, -0x2

    invoke-static {p1, p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result p1

    move v4, v0

    move v5, v4

    move p3, v2

    :goto_4
    if-ge p3, p1, :cond_7

    invoke-virtual {p0, p3, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-static {v6, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v6

    if-nez v6, :cond_5

    add-int/lit8 v4, v4, 0x1

    :cond_5
    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p0, p3, p2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-static {v6, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v6

    if-nez v6, :cond_6

    add-int/lit8 v4, v4, 0x1

    :cond_6
    add-int/2addr v5, v1

    add-int/lit8 p3, p3, 0x1

    goto :goto_4

    :cond_7
    :goto_5
    if-ge v3, p2, :cond_a

    invoke-virtual {p0, v2, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result p1

    invoke-static {p1, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result p1

    if-nez p1, :cond_8

    add-int/lit8 v4, v4, 0x1

    :cond_8
    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result p1

    invoke-static {p1, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result p1

    if-nez p1, :cond_9

    add-int/lit8 v4, v4, 0x1

    :cond_9
    add-int/2addr v5, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_a
    mul-int/lit8 v4, v4, 0x64

    div-int/2addr v4, v5

    const/16 p0, 0x50

    if-le v4, p0, :cond_b

    move v0, v1

    :cond_b
    return v0
.end method

.method public static i(Landroid/graphics/Bitmap;IIID)Z
    .locals 7

    .line 1
    mul-int/lit8 v0, p3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge p2, p3, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, p3

    if-le p2, v4, :cond_1

    move v4, v2

    goto :goto_1

    :cond_1
    move v4, v1

    :goto_1
    or-int/2addr v3, v4

    if-ge p1, p3, :cond_2

    move v4, v2

    goto :goto_2

    :cond_2
    move v4, v1

    :goto_2
    or-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v4, p3

    if-le p1, v4, :cond_3

    move p3, v2

    goto :goto_3

    :cond_3
    move p3, v1

    :goto_3
    or-int/2addr p3, v3

    if-eqz p3, :cond_4

    return v1

    :cond_4
    div-int/lit8 p3, v0, 0x2

    sub-int v3, p2, p3

    invoke-static {v3, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result v3

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int v4, p2, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-static {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result v4

    sub-int p3, p1, p3

    invoke-static {p3, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->o(II)I

    move-result p3

    add-int/2addr v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-static {v0, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->p(II)I

    move-result v0

    move v5, p1

    :goto_4
    if-ge v5, v0, :cond_6

    invoke-virtual {p0, v5, p2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-static {v6, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v6

    if-nez v6, :cond_5

    move v0, v2

    goto :goto_5

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_6
    move v0, v1

    :goto_5
    if-nez v0, :cond_7

    return v1

    :cond_7
    move v0, p1

    :goto_6
    if-le v0, p3, :cond_9

    invoke-virtual {p0, v0, p2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v5

    if-nez v5, :cond_8

    move p3, v2

    goto :goto_7

    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    :cond_9
    move p3, v1

    :goto_7
    if-nez p3, :cond_a

    return v1

    :cond_a
    move p3, p2

    :goto_8
    if-le p3, v3, :cond_c

    invoke-virtual {p0, p1, p3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-static {v0, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v0

    if-nez v0, :cond_b

    move p3, v2

    goto :goto_9

    :cond_b
    add-int/lit8 p3, p3, -0x1

    goto :goto_8

    :cond_c
    move p3, v1

    :goto_9
    if-nez p3, :cond_d

    return v1

    :cond_d
    :goto_a
    if-ge p2, v4, :cond_f

    invoke-virtual {p0, p1, p2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result p3

    invoke-static {p3, p4, p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result p3

    if-nez p3, :cond_e

    move v1, v2

    goto :goto_b

    :cond_e
    add-int/lit8 p2, p2, 0x1

    goto :goto_a

    :cond_f
    :goto_b
    return v1
.end method

.method public static j(Landroid/graphics/Bitmap;IIID)Z
    .locals 7

    .line 1
    mul-int/lit8 v4, p3, 0x2

    const/16 v5, 0x32

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, v4

    invoke-static/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->a(Landroid/graphics/Bitmap;IIIII)D

    move-result-wide v5

    move v1, p3

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-static/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v0

    mul-int v2, p3, p3

    int-to-double v2, v2

    mul-double/2addr v2, p4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static k(ID)Z
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v0

    cmpl-double p0, v0, p1

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z
    .locals 9

    .line 1
    iget-wide v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v5, v0

    iget-wide v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v6, v0

    move-object v2, p0

    move v3, p2

    move v4, p2

    move-wide v7, p3

    invoke-static/range {v2 .. v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->d(Landroid/graphics/Bitmap;IIIID)D

    move-result-wide v0

    mul-int v2, p2, p2

    int-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    iget-wide v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v0

    iget-wide v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v4, v0

    int-to-double p1, p2

    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    mul-double/2addr p1, v0

    double-to-int v5, p1

    move-object v2, p0

    move-wide v6, p3

    invoke-static/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->h(Landroid/graphics/Bitmap;IIID)Z

    move-result p0

    return p0
.end method

.method public static m(Landroid/graphics/Bitmap;IIIDZII)Z
    .locals 0

    .line 1
    invoke-static/range {p0 .. p8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->n(Landroid/graphics/Bitmap;IIIDZII)Z

    move-result p0

    return p0
.end method

.method public static n(Landroid/graphics/Bitmap;IIIDZII)Z
    .locals 24

    .line 1
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    if-lt v1, v3, :cond_15

    if-lt v2, v3, :cond_15

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v5, v3

    if-gt v1, v5, :cond_15

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v5, v3

    if-le v2, v5, :cond_0

    goto/16 :goto_3

    :cond_0
    div-int/lit8 v5, v3, 0x3

    mul-int/lit8 v6, v3, 0x2

    div-int/lit8 v6, v6, 0x3

    const/4 v7, 0x1

    add-int/2addr v6, v7

    const-wide/16 v8, 0x0

    move-wide v10, v8

    const/4 v12, 0x0

    :goto_0
    if-le v3, v6, :cond_1

    sub-int v13, v1, v3

    sub-int v14, v2, v3

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v15

    invoke-static {v15}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v15

    add-double/2addr v15, v8

    add-int v4, v2, v3

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    invoke-static {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    add-double v15, v15, v17

    add-int v13, v1, v3

    invoke-virtual {v0, v13, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    add-double v15, v15, v17

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v13

    add-double/2addr v15, v13

    add-double/2addr v10, v15

    add-int/lit8 v12, v12, 0x4

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    move-wide v13, v8

    move-wide v15, v13

    const/4 v3, 0x0

    :goto_1
    if-le v6, v5, :cond_6

    sub-int v4, v1, v6

    sub-int v7, v2, v6

    invoke-virtual {v0, v4, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    add-double v19, v17, v8

    cmpl-double v21, v17, v15

    if-lez v21, :cond_2

    move-wide/from16 v15, v17

    :cond_2
    add-int v8, v2, v6

    invoke-virtual {v0, v4, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    add-double v19, v19, v17

    cmpl-double v4, v17, v15

    if-lez v4, :cond_3

    move-wide/from16 v15, v17

    :cond_3
    add-int v4, v1, v6

    invoke-virtual {v0, v4, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v8

    invoke-static {v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v8

    add-double v19, v19, v8

    cmpl-double v17, v8, v15

    if-lez v17, :cond_4

    move-wide v15, v8

    :cond_4
    invoke-virtual {v0, v4, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v4

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v7

    add-double v19, v19, v7

    cmpl-double v4, v7, v15

    if-lez v4, :cond_5

    move-wide v15, v7

    :cond_5
    add-double v13, v13, v19

    add-int/lit8 v3, v3, 0x4

    add-int/lit8 v6, v6, -0x1

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    goto :goto_1

    :cond_6
    int-to-double v6, v3

    div-double v6, v13, v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    add-double/2addr v15, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v15, v8

    add-double/2addr v15, v13

    add-int/lit8 v4, v3, 0x1

    int-to-double v8, v4

    div-double/2addr v15, v8

    add-double/2addr v10, v13

    add-int/2addr v12, v3

    int-to-double v3, v12

    div-double/2addr v10, v3

    mul-int/lit8 v5, v5, 0x4

    div-int/lit8 v5, v5, 0x3

    const/4 v3, 0x1

    add-int/2addr v5, v3

    move v8, v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v12, 0x0

    :goto_2
    if-lez v8, :cond_f

    sub-int v9, v1, v8

    sub-int v14, v2, v8

    invoke-virtual {v0, v9, v14}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    const-wide/16 v19, 0x0

    add-double v21, v17, v19

    cmpl-double v23, v17, v15

    if-lez v23, :cond_7

    add-int/lit8 v3, v3, 0x1

    :cond_7
    cmpl-double v17, v17, v10

    if-lez v17, :cond_8

    add-int/lit8 v4, v4, 0x1

    :cond_8
    move-wide/from16 p6, v6

    add-int v6, v2, v8

    invoke-virtual {v0, v9, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v7

    invoke-static {v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    add-double v21, v21, v17

    cmpl-double v7, v17, v15

    if-lez v7, :cond_9

    add-int/lit8 v3, v3, 0x1

    :cond_9
    cmpl-double v7, v17, v10

    if-lez v7, :cond_a

    add-int/lit8 v4, v4, 0x1

    :cond_a
    add-int v7, v1, v8

    invoke-virtual {v0, v7, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v17

    add-double v21, v21, v17

    cmpl-double v6, v17, v15

    if-lez v6, :cond_b

    add-int/lit8 v3, v3, 0x1

    :cond_b
    cmpl-double v6, v17, v10

    if-lez v6, :cond_c

    add-int/lit8 v4, v4, 0x1

    :cond_c
    invoke-virtual {v0, v7, v14}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v6

    add-double v21, v21, v6

    cmpl-double v9, v6, v15

    if-lez v9, :cond_d

    add-int/lit8 v3, v3, 0x1

    :cond_d
    cmpl-double v6, v6, v10

    if-lez v6, :cond_e

    add-int/lit8 v4, v4, 0x1

    :cond_e
    add-double v12, v12, v21

    add-int/lit8 v5, v5, 0x4

    add-int/lit8 v8, v8, -0x1

    move-wide/from16 v6, p6

    goto :goto_2

    :cond_f
    move-wide/from16 p6, v6

    invoke-virtual/range {p0 .. p2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->f(I)D

    move-result-wide v0

    add-double/2addr v12, v0

    cmpl-double v2, v0, v15

    if-lez v2, :cond_10

    add-int/lit8 v3, v3, 0x1

    :cond_10
    cmpl-double v0, v0, v10

    if-lez v0, :cond_11

    add-int/lit8 v4, v4, 0x1

    :cond_11
    const/4 v0, 0x1

    add-int/2addr v5, v0

    int-to-double v0, v5

    div-double/2addr v12, v0

    cmpg-double v0, v12, v10

    if-gez v0, :cond_12

    move-wide/from16 v0, p6

    cmpg-double v0, v10, v0

    if-gez v0, :cond_12

    const/4 v0, 0x0

    return v0

    :cond_12
    mul-int/lit8 v0, v5, 0x2

    div-int/lit8 v0, v0, 0x3

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    cmpl-double v2, v12, v15

    if-lez v2, :cond_13

    if-le v3, v0, :cond_13

    return v1

    :cond_13
    div-int/lit8 v5, v5, 0x3

    sub-int/2addr v5, v1

    cmpl-double v2, v12, v10

    if-lez v2, :cond_14

    if-le v3, v5, :cond_14

    if-le v4, v0, :cond_14

    return v1

    :cond_14
    const/4 v0, 0x0

    return v0

    :cond_15
    :goto_3
    const/4 v0, 0x0

    return v0
.end method

.method public static o(II)I
    .locals 0

    .line 1
    if-ge p0, p1, :cond_0

    return p1

    :cond_0
    return p0
.end method

.method public static p(II)I
    .locals 0

    .line 1
    if-ge p0, p1, :cond_0

    return p0

    :cond_0
    return p1
.end method

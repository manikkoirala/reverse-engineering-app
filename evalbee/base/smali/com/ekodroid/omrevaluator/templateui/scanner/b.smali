.class public Lcom/ekodroid/omrevaluator/templateui/scanner/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xff

    const/4 v1, 0x0

    const/16 v2, 0xfa

    invoke-static {v0, v1, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    sput v3, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->a:I

    invoke-static {v0, v1, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    sput v2, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->b:I

    invoke-static {v0, v1, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    sput v2, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->c:I

    invoke-static {v0, v0, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    sput v2, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->d:I

    const/16 v2, 0xdd

    invoke-static {v0, v0, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)V
    .locals 19

    .line 1
    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move/from16 v9, p6

    new-instance v10, Lsn1;

    move-object/from16 v0, p4

    invoke-direct {v10, v0}, Lsn1;-><init>(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;)V

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-int v11, v0

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v0, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    if-eq v0, v9, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v15

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v5

    invoke-virtual {v8, v9}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v0

    invoke-virtual {v10, v14, v0}, Lsn1;->b(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v16

    const/4 v4, 0x0

    :goto_1
    array-length v0, v5

    if-ge v4, v0, :cond_3

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v0

    aget-boolean v0, v0, v4

    if-eqz v0, :cond_2

    aget-object v0, v16, v4

    iget-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v1

    iget-wide v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v0

    invoke-virtual {v7, v15}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)I

    move-result v17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v18, v4

    move v4, v11

    move-object v6, v5

    move/from16 v5, v17

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->f(Landroid/graphics/Canvas;IIII)V

    goto :goto_2

    :cond_2
    move/from16 v18, v4

    move-object v6, v5

    :goto_2
    add-int/lit8 v4, v18, 0x1

    move-object v5, v6

    goto :goto_1

    :cond_3
    move-object v6, v5

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v15, v0, :cond_4

    const/4 v13, 0x0

    :goto_3
    array-length v0, v6

    if-ge v13, v0, :cond_6

    aget-object v0, v16, v13

    iget-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v1

    iget-wide v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v0

    invoke-virtual {v7, v15}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v4, v11

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->f(Landroid/graphics/Canvas;IIII)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-eq v0, v1, :cond_7

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v0

    invoke-static {v8, v0}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    const/4 v1, 0x0

    :goto_4
    array-length v2, v6

    if-ge v1, v2, :cond_6

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object v2

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_5

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v2

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-eq v2, v3, :cond_5

    aget-object v2, v16, v1

    iget-wide v3, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    iget-wide v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v2, v4

    sget v4, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e:I

    move-object/from16 v15, p1

    invoke-virtual {v7, v15, v3, v2, v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->g(Landroid/graphics/Canvas;III)V

    goto :goto_5

    :cond_5
    move-object/from16 v15, p1

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    move-object/from16 v15, p1

    goto/16 :goto_0

    :cond_7
    move-object/from16 v15, p1

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v0

    invoke-static {v8, v0}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-eq v1, v2, :cond_8

    invoke-virtual {v10, v14}, Lsn1;->d(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v2

    invoke-static {v0, v2}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    iget-wide v0, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    mul-int/lit8 v4, v11, 0x4

    int-to-double v4, v4

    sub-double/2addr v0, v4

    double-to-int v4, v0

    mul-int/lit8 v5, v11, 0x3

    sget v6, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->b:I

    goto :goto_6

    :cond_8
    invoke-virtual {v10, v14}, Lsn1;->d(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-static {v13, v1}, Le5;->r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v2

    iget-wide v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    iget-wide v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    mul-int/lit8 v4, v11, 0x4

    int-to-double v4, v4

    sub-double/2addr v0, v4

    double-to-int v4, v0

    mul-int/lit8 v5, v11, 0x3

    sget v6, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->c:I

    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->d(Landroid/graphics/Canvas;Ljava/lang/String;IIII)V

    goto/16 :goto_0

    :cond_9
    move-object/from16 v15, p1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    if-ne v1, v9, :cond_a

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollStartDigit()I

    move-result v5

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v2, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result v3

    invoke-virtual {v8, v9}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v1

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    add-int/lit8 v0, v0, -0x1

    aget v4, v1, v0

    move-object v0, v10

    move/from16 v1, p5

    invoke-virtual/range {v0 .. v5}, Lsn1;->a(ILcom/ekodroid/omrevaluator/templateui/models/Point2Integer;III)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v12

    array-length v13, v12

    :goto_7
    if-ge v6, v13, :cond_a

    aget-object v0, v12, v6

    iget-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v1

    iget-wide v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v0

    sget v5, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v4, v11

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->f(Landroid/graphics/Canvas;IIII)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_a
    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v0

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->pageIndex:I

    if-ne v0, v9, :cond_b

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getExamSetOption()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-result-object v0

    invoke-virtual {v8, v9}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lsn1;->b(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    iget-wide v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v1

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-wide v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v0

    sget v5, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v4, v11

    invoke-virtual/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->f(Landroid/graphics/Canvas;IIII)V

    :cond_b
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    iget-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v1, v1

    iget-wide v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v2

    sget v2, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->a:I

    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->g(Landroid/graphics/Canvas;III)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Landroid/graphics/Canvas;Ljava/util/ArrayList;D)V
    .locals 9

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    iget-wide v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v5, v1

    iget-wide v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v6, v0

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    div-double v0, p3, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-int v7, v0

    sget v8, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->d:I

    move-object v3, p0

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->h(Landroid/graphics/Canvas;IIII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final d(Landroid/graphics/Canvas;Ljava/lang/String;IIII)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColor(I)V

    sget-object p6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    if-nez p2, :cond_0

    return-void

    :cond_0
    int-to-float p6, p5

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setTextSize(F)V

    int-to-float p3, p3

    div-int/lit8 p5, p5, 0x2

    add-int/2addr p4, p5

    add-int/lit8 p4, p4, -0x2

    int-to-float p4, p4

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final e(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)I
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/b$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    sget p1, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e:I

    return p1

    :cond_0
    sget p1, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->b:I

    return p1

    :cond_1
    sget p1, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->e:I

    return p1

    :cond_2
    sget p1, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->d:I

    return p1

    :cond_3
    sget p1, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->c:I

    return p1
.end method

.method public final f(Landroid/graphics/Canvas;IIII)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float p2, p2

    int-to-float p3, p3

    int-to-float p4, p4

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final g(Landroid/graphics/Canvas;III)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float p2, p2

    int-to-float p3, p3

    invoke-virtual {p1, p2, p3, v1, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final h(Landroid/graphics/Canvas;IIII)V
    .locals 6

    .line 1
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v5, p5}, Landroid/graphics/Paint;->setColor(I)V

    sub-int p5, p2, p4

    int-to-float v1, p5

    sub-int p5, p3, p4

    int-to-float v2, p5

    add-int/2addr p2, p4

    int-to-float v3, p2

    add-int/2addr p3, p4

    int-to-float v4, p3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public i(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)Landroid/graphics/Bitmap;
    .locals 7

    .line 1
    if-eqz p1, :cond_3

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p4, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    if-nez v0, :cond_1

    return-object p1

    :cond_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v0, 0x1

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getCorners()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->b(Landroid/graphics/Canvas;Ljava/util/ArrayList;)V

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getPredictedMarkers()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getPredictedMarkers()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v2

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->c(Landroid/graphics/Canvas;Ljava/util/ArrayList;D)V

    :cond_2
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->a(Landroid/graphics/Canvas;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)V

    :cond_3
    :goto_0
    return-object p1
.end method

.class public final enum Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AlgoType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

.field public static final enum DEFAULT:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

.field public static final enum ROW_AVG:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;
    .locals 2

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->DEFAULT:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->ROW_AVG:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    filled-new-array {v0, v1}, [Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    const-string v1, "DEFAULT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->DEFAULT:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    const-string v1, "ROW_AVG"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->ROW_AVG:Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->$values()[Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil$AlgoType;

    return-object v0
.end method

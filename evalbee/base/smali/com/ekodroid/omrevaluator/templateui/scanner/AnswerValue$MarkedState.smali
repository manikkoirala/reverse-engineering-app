.class public final enum Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MarkedState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field public static final enum ATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field public static final enum CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field public static final enum INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field public static final enum INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field public static final enum PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

.field public static final enum UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;
    .locals 6

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->ATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    filled-new-array/range {v0 .. v5}, [Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, "CORRECT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, "INCORRECT"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, "UNATTEMPTED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, "ATTEMPTED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->ATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, "PARTIAL_CORRECT"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, "INVALID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->$values()[Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    return-object v0
.end method

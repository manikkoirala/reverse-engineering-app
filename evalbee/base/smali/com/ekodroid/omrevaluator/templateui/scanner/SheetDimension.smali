.class public Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private bubbleSize:D

.field private height:I

.field public predictedMarkers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;",
            ">;"
        }
    .end annotation
.end field

.field public sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->predictedMarkers:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getBubbleSize()D
    .locals 5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    aget-object v1, v0, v2

    aget-object v1, v1, v2

    iget-wide v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v3, v1

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x1

    int-to-double v0, v0

    div-double/2addr v3, v0

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    div-double/2addr v3, v0

    iput-wide v3, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->bubbleSize:D

    return-wide v3
.end method

.method public getCorners()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    const/4 v2, 0x0

    aget-object v1, v1, v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v1, v1, v2

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    aget-object v1, v1, v2

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v3, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public getPredictedMarkers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->predictedMarkers:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setHeight(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->height:I

    return-void
.end method

.method public setWidth(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->width:I

    return-void
.end method

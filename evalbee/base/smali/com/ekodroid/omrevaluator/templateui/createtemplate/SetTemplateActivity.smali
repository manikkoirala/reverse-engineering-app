.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/Button;

.field public f:Landroid/widget/ImageButton;

.field public g:Landroid/widget/ImageButton;

.field public h:Landroid/widget/ImageButton;

.field public i:Landroid/widget/ImageButton;

.field public j:Landroid/widget/ImageButton;

.field public k:Landroid/widget/ImageButton;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Landroid/widget/TextView;

.field public p:Ljava/util/ArrayList;

.field public q:Landroid/widget/ArrayAdapter;

.field public t:I

.field public v:I

.field public w:I

.field public x:Landroid/content/SharedPreferences;

.field public y:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    const/4 v0, 0x5

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->t:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->v:I

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->w:I

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->M(I)V

    return-void
.end method


# virtual methods
.method public final B(I)V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v4, 0x2

    invoke-virtual {v1, v4, v2, v4, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    const/16 v7, 0xc

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v8

    const/16 v9, 0x8

    invoke-virtual {p0, v9, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v10

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v11

    invoke-virtual {p0, v9, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v9

    invoke-virtual {v6, v8, v10, v11, v9}, Landroid/view/View;->setPadding(IIII)V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f06005c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v6, 0x5

    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    const v10, 0x7f130237

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setTextAppearance(I)V

    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v11, 0x3c

    invoke-virtual {p0, v11, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v11

    invoke-direct {v10, v11, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v11, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    invoke-virtual {v11, v1, v2, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const v11, 0x7f120306

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setInputType(I)V

    const/4 v10, 0x4

    invoke-virtual {v1, v10}, Landroid/view/View;->setTextAlignment(I)V

    const v10, 0x7f130135

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setTextAppearance(I)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    const v10, 0x7f080080

    invoke-virtual {v1, v10}, Landroid/view/View;->setBackgroundResource(I)V

    new-array v10, v8, [Landroid/text/InputFilter;

    new-instance v11, Landroid/text/InputFilter$LengthFilter;

    const/16 v12, 0x32

    invoke-direct {v11, v12}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v11, v10, v2

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {v1, v10}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v10

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v7

    invoke-virtual {v1, v10, v2, v7, v2}, Landroid/view/View;->setPadding(IIII)V

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v10, 0x28

    invoke-virtual {p0, v10, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v11

    invoke-direct {v7, v5, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0x10

    invoke-virtual {p0, v5, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v5

    invoke-virtual {v7, v2, v2, v5, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iput v3, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1, v8, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    new-instance v3, Landroid/widget/Spinner;

    invoke-direct {v3, p0}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const/16 v5, 0x11

    invoke-virtual {v3, v5}, Landroid/widget/Spinner;->setGravity(I)V

    const v7, 0x7f08007d

    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v8, 0x50

    invoke-virtual {p0, v8, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v8

    invoke-virtual {p0, v10, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v10

    invoke-direct {v7, v8, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v6, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v8

    invoke-virtual {p0, v2, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v10

    invoke-virtual {p0, v6, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v6

    invoke-virtual {p0, v2, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->C(ILandroid/content/Context;)I

    move-result v2

    invoke-virtual {v7, v8, v10, v6, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iput v5, v7, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, v4, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->d:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$j;

    invoke-direct {v0, p0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$j;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    move v0, v9

    goto/16 :goto_0

    :cond_0
    return-void
.end method

.method public final C(ILandroid/content/Context;)I
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    int-to-float p1, p1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    mul-float/2addr p1, p2

    float-to-int p1, p1

    return p1
.end method

.method public final D()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->e:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final E()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->x:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->y:Landroid/content/SharedPreferences$Editor;

    const v0, 0x7f0903c7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->l:Landroid/widget/TextView;

    const v0, 0x7f090402

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->m:Landroid/widget/TextView;

    const v0, 0x7f090415

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->n:Landroid/widget/TextView;

    const v0, 0x7f09018a

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->f:Landroid/widget/ImageButton;

    const v0, 0x7f0901a5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->g:Landroid/widget/ImageButton;

    const v0, 0x7f09018c

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->h:Landroid/widget/ImageButton;

    const v0, 0x7f0901a7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->i:Landroid/widget/ImageButton;

    const v0, 0x7f09018d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->j:Landroid/widget/ImageButton;

    const v0, 0x7f0901a8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->k:Landroid/widget/ImageButton;

    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->d:Landroid/widget/LinearLayout;

    const v0, 0x7f0900c8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->e:Landroid/widget/Button;

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/String;

    :goto_0
    if-ge v2, v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    move v2, v4

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    const v3, 0x7f0c00f0

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method

.method public final F(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lt v0, p1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->d:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final G()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->f:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final H()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->g:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final I()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->i:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final J()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->h:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final K()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->k:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->j:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final M(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->B(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->F(I)V

    :goto_0
    return-void
.end method

.method public final N()V
    .locals 2

    .line 1
    const v0, 0x7f0900a1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public hideKeyboardForViewsExceptEditText(Landroid/view/View;)V
    .locals 3

    instance-of v0, p1, Landroid/widget/EditText;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->hideKeyboardForViewsExceptEditText(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c003a

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->E()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->G()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->H()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->J()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->I()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->K()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->D()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->N()V

    iget p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->w:I

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->M(I)V

    return-void
.end method

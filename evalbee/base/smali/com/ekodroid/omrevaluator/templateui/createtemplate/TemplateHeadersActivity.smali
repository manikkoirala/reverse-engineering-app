.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;

.field public d:Landroid/widget/ListView;

.field public e:Landroid/widget/Button;

.field public f:Lj3;

.field public g:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->g:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->H(Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->F()V

    return-void
.end method


# virtual methods
.method public final D()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->e:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final E()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public final F()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllHeaderProfileJson()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->g:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->getHeaderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Lj3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;

    invoke-direct {v1, v2, v0}, Lj3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->f:Lj3;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final G()V
    .locals 2

    .line 1
    const v0, 0x7f090443

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final H(Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$d;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;

    const v2, 0x7f12009d

    const v3, 0x7f12018e

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const v7, 0x7f0800e1

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0044

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->G()V

    const p1, 0x7f0900bf

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->e:Landroid/widget/Button;

    const p1, 0x7f090247

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->d:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->E()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->D()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateHeadersActivity;->F()V

    return-void
.end method

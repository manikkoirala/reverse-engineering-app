.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

.field public d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public e:Landroid/widget/Spinner;

.field public f:Landroid/widget/Spinner;

.field public g:Landroid/widget/Button;

.field public h:Landroid/widget/Button;

.field public i:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

.field public j:Landroid/widget/ImageButton;

.field public k:Landroid/widget/ImageButton;

.field public l:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public m:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->P()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->I(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->G(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->H(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->F(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-void
.end method


# virtual methods
.method public final F(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/a;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1, p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setLabelProfile(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setHeaderProfile(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->i:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {p2, p1}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final G(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "HEADER_PROFILE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final H(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 2

    .line 1
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    const-class v1, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x14000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final I(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "LABEL_PROFILE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final J()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->h:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->h:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final K()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->k:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->j:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final M()V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lok;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "Default Medium"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lok;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "Blank"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "None"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllHeaderProfileJson()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->getHeaderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Lp3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-direct {v1, v2, v0}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->f:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->f:Landroid/widget/Spinner;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$f;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->f:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final N()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllLabelProfileJson()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lok;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lok;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;->getProfileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0c00f0

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->e:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->e:Landroid/widget/Spinner;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->e:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final O()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->N()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->M()V

    return-void
.end method

.method public final P()V
    .locals 5

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$e;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;)V

    new-instance v1, Lti1;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lti1;-><init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0047

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SHEET_TEMPLATE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->d:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    :cond_0
    const p1, 0x7f09033e

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Spinner;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->e:Landroid/widget/Spinner;

    const p1, 0x7f09033a

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Spinner;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->f:Landroid/widget/Spinner;

    const p1, 0x7f090192

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->k:Landroid/widget/ImageButton;

    const p1, 0x7f090193

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->j:Landroid/widget/ImageButton;

    const p1, 0x7f09048d

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->i:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    const p1, 0x7f0900ca

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->g:Landroid/widget/Button;

    const p1, 0x7f0900cc

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->h:Landroid/widget/Button;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->g:Landroid/widget/Button;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->J()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->K()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->O()V

    return-void
.end method

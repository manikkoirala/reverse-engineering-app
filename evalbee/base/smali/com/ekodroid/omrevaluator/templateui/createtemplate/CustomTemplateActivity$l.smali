.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Y0(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Landroid/widget/EditText;II)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->a:Landroid/widget/EditText;

    iput p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->b:I

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->a:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    move-result-object p1

    const p2, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f1200c2

    invoke-static {p1, v1, p2, v0}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->TEXT:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->b:I

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->c:I

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    move-object v0, v6

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->F(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->G(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)[[Lrb0;

    move-result-object v0

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->b:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->c:I

    add-int/lit8 v1, v1, -0x1

    new-instance v2, Lrb0;

    invoke-direct {v2, p2, v6}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->b:I

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;->c:I

    invoke-static {p2, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->H(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public A:Landroid/widget/Button;

.field public C:Landroid/widget/LinearLayout;

.field public D:Landroid/widget/ImageButton;

.field public F:Landroid/widget/ImageButton;

.field public G:Landroid/widget/TextView;

.field public H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

.field public I:I

.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

.field public d:Landroid/widget/Button;

.field public e:Landroid/widget/ImageButton;

.field public f:Landroid/widget/ImageButton;

.field public g:Landroid/widget/ImageButton;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/Spinner;

.field public j:Landroid/widget/Spinner;

.field public k:Landroid/widget/Spinner;

.field public l:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

.field public m:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;

.field public p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public q:Lcom/ekodroid/omrevaluator/templateui/models/a;

.field public t:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public v:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

.field public w:Ljava/lang/String;

.field public x:Lwu1;

.field public y:Landroid/content/SharedPreferences;

.field public z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 7

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/a;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/templateui/models/a;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/a;

    new-instance v0, Lwu1;

    const/4 v2, 0x6

    const/16 v3, 0x3d

    const/4 v4, 0x1

    const/4 v5, 0x5

    const/4 v6, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lwu1;-><init>(IIIIZ)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->x:Lwu1;

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;->DEFAULT:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->I:I

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c0()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->M(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->e0(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->K(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;Lwu1;)V
    .locals 0

    .line 1
    invoke-virtual/range {p0 .. p5}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->I(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;Lwu1;)V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->Z()V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->d0()V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->L()V

    return-void
.end method


# virtual methods
.method public final I(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;Lwu1;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;

    invoke-direct {v0, p2, p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;

    :try_start_0
    iget-boolean p1, p5, Lwu1;->e:Z

    if-eqz p1, :cond_1

    const/16 p1, 0x3d

    iput p1, p5, Lwu1;->b:I

    const/4 p1, 0x6

    iput p1, p5, Lwu1;->a:I

    const/4 p1, 0x5

    iput p1, p5, Lwu1;->d:I

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    iput p1, p5, Lwu1;->c:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    iput p1, p5, Lwu1;->c:I

    goto :goto_0

    :cond_1
    iget p1, p5, Lwu1;->b:I

    iput p1, p5, Lwu1;->a:I

    iget p1, p5, Lwu1;->d:I

    iput p1, p5, Lwu1;->c:I

    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->n:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;

    invoke-virtual {p1, p5}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->b(Lwu1;)Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1, p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setLabelProfile(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p1, p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setHeaderProfile(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->I:I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->N()V
    :try_end_0
    .catch Lcom/ekodroid/omrevaluator/templateui/models/TemplateSizeTooSmallException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f080171

    invoke-static {p2, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {p3, p2}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->f0()V

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public final J([Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final K(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "HEADER_PROFILE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final L()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final M(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "LABEL_PROFILE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final N()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->h:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->A:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public final O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->d:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->F:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Q()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->D:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final R()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->f:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$j;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$j;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final S()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->g:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$i;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final T()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->e:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$h;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final U()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->z:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final V()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->A:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->A:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final W()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->y:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "TEMPLATE_DETAIL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    const v0, 0x7f0903ca

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->h:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0901b5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->F:Landroid/widget/ImageButton;

    const v0, 0x7f0901b7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->D:Landroid/widget/ImageButton;

    const v0, 0x7f0903fd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->G:Landroid/widget/TextView;

    const v0, 0x7f0900ca

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->z:Landroid/widget/Button;

    const v0, 0x7f0900cc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->A:Landroid/widget/Button;

    const v0, 0x7f090205

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->C:Landroid/widget/LinearLayout;

    const v0, 0x7f0900bb

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->d:Landroid/widget/Button;

    const v0, 0x7f09048e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    const v0, 0x7f09033e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->i:Landroid/widget/Spinner;

    const v0, 0x7f09033a

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->j:Landroid/widget/Spinner;

    const v0, 0x7f09033f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->k:Landroid/widget/Spinner;

    const v0, 0x7f090194

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->g:Landroid/widget/ImageButton;

    const v0, 0x7f090193

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->e:Landroid/widget/ImageButton;

    const v0, 0x7f090192

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->f:Landroid/widget/ImageButton;

    return-void
.end method

.method public final X()V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lok;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "Default Medium"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lok;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "Blank"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "None"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllHeaderProfileJson()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->getHeaderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Lp3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {v1, v2, v0}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->j:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->j:Landroid/widget/Spinner;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->j:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final Y()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllLabelProfileJson()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lok;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lok;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;->getProfileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lp3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {v0, v2, v1}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->i:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->i:Landroid/widget/Spinner;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$b;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->i:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final Z()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lok;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lok;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lok;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->y:Landroid/content/SharedPreferences;

    const-string v2, "LAYOUT_LIST"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, La91;->y(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Lp3;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {v1, v2, v0}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->k:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->k:Landroid/widget/Spinner;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->k:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final a0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v0

    array-length v0, v0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->C:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->I:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->D:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v0, -0x1

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->D:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->D:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :goto_1
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->G:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->I:I

    add-int/2addr v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->C:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public final b0()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->X()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->Z()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c0()V

    return-void
.end method

.method public final c0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080171

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->q:Lcom/ekodroid/omrevaluator/templateui/models/a;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->I:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {v1, v0}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->a0()V

    return-void
.end method

.method public final d0()V
    .locals 5

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    new-instance v1, Lti1;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lti1;-><init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final e0(Ljava/lang/String;)V
    .locals 16

    .line 1
    move-object/from16 v6, p0

    move-object/from16 v0, p1

    new-instance v1, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v2, v6, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    const v3, 0x7f130151

    invoke-direct {v1, v2, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v2, "layout_inflater"

    invoke-virtual {v6, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f0c0065

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v3, 0x7f120088

    invoke-virtual {v1, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v4, 0x7f090339

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    const v5, 0x7f090338

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    new-instance v7, Landroid/widget/ArrayAdapter;

    iget-object v8, v6, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    sget-object v9, Lgr1;->d:[Ljava/lang/String;

    const v10, 0x7f0c00f0

    invoke-direct {v7, v8, v10, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v7, Landroid/widget/ArrayAdapter;

    iget-object v8, v6, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    sget-object v11, Lgr1;->e:[Ljava/lang/String;

    invoke-direct {v7, v8, v10, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v5, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v7, 0x7f0902d6

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    const v8, 0x7f0902e7

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    const v10, 0x7f09008a

    invoke-virtual {v2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    const v12, 0x7f0900be

    invoke-virtual {v2, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Landroid/widget/Button;

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v13

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/view/View;->setVisibility(I)V

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v14, v2

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    aget-object v14, v2, v3

    sget-object v15, Lok;->L:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-virtual {v7, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {v8, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_0
    aget-object v2, v2, v1

    const-string v8, "X"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v8, v2

    const/4 v14, 0x2

    if-ne v8, v14, :cond_1

    aget-object v1, v2, v1

    invoke-virtual {v6, v9, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->J([Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    aget-object v1, v2, v3

    invoke-virtual {v6, v11, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->J([Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v5, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    :cond_1
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$m;

    invoke-direct {v1, v6, v0, v13}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$m;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Ljava/lang/String;Landroidx/appcompat/app/a;)V

    invoke-virtual {v10, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$n;

    move-object v0, v8

    move-object/from16 v1, p0

    move-object v2, v7

    move-object v3, v4

    move-object v4, v5

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$n;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Landroid/widget/RadioButton;Landroid/widget/Spinner;Landroid/widget/Spinner;Landroidx/appcompat/app/a;)V

    invoke-virtual {v12, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v13}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final f0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->A:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->h:Landroid/widget/TextView;

    const-string v1, "Selected layout size is too small to fit all answer options, please select suitable size"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c002c

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->W()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->V()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->U()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->O()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->T()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->R()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->S()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->P()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->Q()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->b0()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->V()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ly01;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->a:Ly01;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 24

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNameString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamNameString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getDateString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getReportCardString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getClassString()Ljava/lang/String;

    move-result-object v6

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getGradeString()Ljava/lang/String;

    move-result-object v7

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRankString()Ljava/lang/String;

    move-result-object v8

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSubjectString()Ljava/lang/String;

    move-result-object v9

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMarksString()Ljava/lang/String;

    move-result-object v10

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getPercentageString()Ljava/lang/String;

    move-result-object v11

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectAnswerString()Ljava/lang/String;

    move-result-object v12

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getIncorrectAnswerString()Ljava/lang/String;

    move-result-object v13

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTotalMarksString()Ljava/lang/String;

    move-result-object v14

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQueNoString()Ljava/lang/String;

    move-result-object v15

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getAttemptedString()Ljava/lang/String;

    move-result-object v16

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectString()Ljava/lang/String;

    move-result-object v17

    filled-new-array/range {v2 .. v17}, [Ljava/lang/String;

    move-result-object v22

    new-instance v1, Lti0;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    const v3, 0x7f1202e4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;->a:Ly01;

    const/16 v23, 0xf

    move-object/from16 v18, v1

    move-object/from16 v19, v2

    move-object/from16 v21, v3

    invoke-direct/range {v18 .. v23}, Lti0;-><init>(Landroid/content/Context;Ljava/lang/String;Ly01;[Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

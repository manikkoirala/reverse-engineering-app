.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->S()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4

    .line 1
    check-cast p1, [Ljava/lang/String;

    array-length v0, p1

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/String;

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p1, v0, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setMatrixPrimary([Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setMatrixSecondary([Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Z)V

    :cond_0
    return-void
.end method

.class final enum Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

.field public static final enum ADD:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

.field public static final enum ERASE:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

.field public static final enum OTHER:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
    .locals 3

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ADD:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ERASE:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->OTHER:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    filled-new-array {v0, v1, v2}, [Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    const-string v1, "ADD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ADD:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    const-string v1, "ERASE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ERASE:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    const-string v1, "OTHER"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->OTHER:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->$values()[Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->$VALUES:[Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    return-object v0
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->P()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    const v0, 0x7f08016e

    const v1, 0x7f0800bd

    const-string v2, ""

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->n:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v2, 0x7f1200c6

    invoke-static {p1, v2, v1, v0}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->l:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->p:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v2, 0x7f1200c0

    invoke-static {p1, v2, v1, v0}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v2, 0x7f1200c1

    invoke-static {p1, v2, v1, v0}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_2
    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v5

    invoke-static {v6, v5}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    move-result-object v5

    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v5, v4}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;Ljava/util/ArrayList;)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->X0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Ljava/util/ArrayList;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:[Landroid/widget/Spinner;

.field public final synthetic b:Ljava/util/ArrayList;

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;[Landroid/widget/Spinner;Ljava/util/ArrayList;II)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->e:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->a:[Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->b:Ljava/util/ArrayList;

    iput p4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->c:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 p2, 0x2

    new-array p2, p2, [Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->a:[Landroid/widget/Spinner;

    array-length v3, v2

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->b:Ljava/util/ArrayList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    aput-object v2, p2, v0

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->e:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->c:I

    iget v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;->d:I

    invoke-static {v0, p2, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :cond_2
    return-void
.end method

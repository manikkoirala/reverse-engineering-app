.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->F()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->f:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    aget-object v2, p1, v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v2

    move v3, v0

    :goto_1
    array-length v4, v2

    if-ge v3, v4, :cond_1

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    invoke-direct {v4}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;-><init>()V

    aput-object v4, v2, v3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    invoke-virtual {v4}, Ljk1;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1200bc

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    aget-object v5, v2, v3

    invoke-virtual {v5, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setName(Ljava/lang/String;)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->k()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setNoOfQue(I)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->r()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setPositiveMark(D)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->q()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setNegetiveMark(D)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->s()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setPartialAllowed(Z)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->p()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setType(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setPayload(Ljava/lang/String;)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setNoOfOptionalQue(I)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setOptionalType(Ljava/lang/String;)V

    aget-object v4, v2, v3

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v5, v5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v5, v5, v1

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljk1;->t()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setOptionalAllowed(Z)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_1
    aget-object v3, p1, v1

    invoke-virtual {v3, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->setSections([Lcom/ekodroid/omrevaluator/templateui/models/Section2;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_2
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->f:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    const-string v1, "TEMPLATE_DETAIL"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

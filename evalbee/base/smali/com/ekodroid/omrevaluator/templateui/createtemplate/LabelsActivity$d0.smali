.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Z(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteLabelProfileJson(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-virtual {v1}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120125

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0800cf

    const v2, 0x7f08016d

    invoke-static {p1, v0, v1, v2}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

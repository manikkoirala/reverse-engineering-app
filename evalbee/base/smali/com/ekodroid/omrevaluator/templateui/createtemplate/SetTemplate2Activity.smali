.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

.field public d:Landroid/widget/Button;

.field public e:Landroid/widget/Button;

.field public f:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public g:Landroid/widget/LinearLayout;

.field public h:[Ljava/lang/Integer;

.field public i:Landroid/content/SharedPreferences;

.field public j:[[Ljk1;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    const/16 v0, 0xe1

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->h:[Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public final A(ILandroid/content/Context;)I
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    int-to-float p1, p1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    mul-float/2addr p1, p2

    float-to-int p1, p1

    return p1
.end method

.method public final B([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->i:Landroid/content/SharedPreferences;

    const-string v3, "plus_mark_json"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lok;->I:[Ljava/lang/Double;

    invoke-static {v2, v3}, La91;->o(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->i:Landroid/content/SharedPreferences;

    const-string v5, "minus_mark_json"

    invoke-interface {v3, v5, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lok;->J:[Ljava/lang/Double;

    invoke-static {v3, v4}, La91;->m(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$c;

    invoke-direct {v4, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;)V

    const/4 v13, 0x0

    move v14, v13

    :goto_0
    array-length v5, v1

    if-ge v14, v5, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->C()Landroid/widget/LinearLayout;

    move-result-object v5

    aget-object v6, v1, v14

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->D(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    aget-object v5, v1, v14

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v15

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    array-length v6, v15

    new-array v6, v6, [Ljk1;

    aput-object v6, v5, v14

    move v12, v13

    :goto_1
    array-length v5, v15

    if-ge v12, v5, :cond_0

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v16, v5, v14

    new-instance v17, Ljk1;

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    aget-object v7, v15, v12

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f1202ba

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v11, v12, 0x1

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->h:[Ljava/lang/Integer;

    move-object/from16 v5, v17

    move-object v10, v2

    move/from16 v18, v11

    move-object v11, v3

    move/from16 v19, v12

    move-object v12, v4

    invoke-direct/range {v5 .. v12}, Ljk1;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/Section2;Ljava/lang/String;[Ljava/lang/Integer;Ljava/util/ArrayList;Ljava/util/ArrayList;Ly01;)V

    aput-object v17, v16, v19

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->g:Landroid/widget/LinearLayout;

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    aget-object v6, v6, v14

    aget-object v6, v6, v19

    invoke-virtual {v6}, Ljk1;->o()Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move/from16 v12, v18

    goto :goto_1

    :cond_0
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->hideKeyboardForViewsExceptEditText(Landroid/view/View;)V

    return-void
.end method

.method public final C()Landroid/widget/LinearLayout;
    .locals 4

    .line 1
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    const v1, 0x7f080080

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final D(Ljava/lang/String;)Landroid/widget/TextView;
    .locals 4

    .line 1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f060035

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    const/16 p1, 0x10

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    const p1, 0x7f130239

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextAppearance(I)V

    const/16 p1, 0xc

    invoke-virtual {p0, p1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->A(ILandroid/content/Context;)I

    move-result v1

    const/16 v2, 0x8

    invoke-virtual {p0, v2, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->A(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual {p0, p1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->A(ILandroid/content/Context;)I

    move-result p1

    invoke-virtual {p0, v2, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->A(ILandroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v3, p1, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    return-object v0
.end method

.method public final E()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->e:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final F()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->d:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final G(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->i:Landroid/content/SharedPreferences;

    const v0, 0x7f0901d0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f0900c8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->d:Landroid/widget/Button;

    const v0, 0x7f0900ca

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->e:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object p1

    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->h:[Ljava/lang/Integer;

    array-length v2, v0

    if-ge v1, v2, :cond_0

    add-int/lit8 v2, v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    move v1, v2

    goto :goto_0

    :cond_0
    array-length p1, p1

    new-array p1, p1, [[Ljk1;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->j:[[Ljk1;

    return-void
.end method

.method public hideKeyboardForViewsExceptEditText(Landroid/view/View;)V
    .locals 3

    instance-of v0, p1, Landroid/widget/EditText;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->hideKeyboardForViewsExceptEditText(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c003b

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "SUBJECT_DETAIL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->f:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->G(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->F()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->E()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->f:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;->B([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->Z()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->k:Landroid/widget/Spinner;

    invoke-virtual {p2}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iput-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->w:Ljava/lang/String;

    const/4 p1, 0x1

    if-nez p3, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    sget-object p4, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;->DEFAULT:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iput-object p4, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->x:Lwu1;

    iput-boolean p1, p2, Lwu1;->e:Z

    :cond_0
    if-ne p3, p1, :cond_1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    sget-object p4, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;->SUBJECT_IN_COLUMN:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iput-object p4, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->x:Lwu1;

    iput-boolean p1, p2, Lwu1;->e:Z

    :cond_1
    const/4 p2, 0x2

    const/4 p4, 0x0

    if-ne p3, p2, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->k:Landroid/widget/Spinner;

    invoke-virtual {p1, p4}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void

    :cond_2
    if-le p3, p2, :cond_5

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->w:Ljava/lang/String;

    const-string p5, "-"

    invoke-virtual {p3, p5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    array-length p5, p3

    if-ne p5, p2, :cond_5

    aget-object p5, p3, p1

    invoke-virtual {p5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p5

    sget-object v0, Lok;->K:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p5

    if-eqz p5, :cond_3

    iget-object p5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;->SUBJECT_IN_COLUMN:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    goto :goto_0

    :cond_3
    iget-object p5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;->DEFAULT:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    :goto_0
    iput-object v0, p5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    aget-object p3, p3, p4

    const-string p5, "X"

    invoke-virtual {p3, p5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    array-length p5, p3

    if-ne p5, p2, :cond_4

    aget-object p2, p3, p4

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    aget-object p3, p3, p1

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3

    goto :goto_1

    :cond_4
    move p2, p4

    move p3, p2

    :goto_1
    iget-object p5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object p5, p5, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->x:Lwu1;

    iput-boolean p4, p5, Lwu1;->e:Z

    move p4, p2

    goto :goto_2

    :cond_5
    move p3, p4

    :goto_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$l;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->x:Lwu1;

    iput p4, v5, Lwu1;->d:I

    mul-int/lit8 p3, p3, 0x5

    add-int/2addr p3, p1

    iput p3, v5, Lwu1;->b:I

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-static/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;Lwu1;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

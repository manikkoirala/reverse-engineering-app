.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public A:Landroid/widget/TextView;

.field public C:Landroid/widget/TextView;

.field public D:Landroid/widget/TextView;

.field public F:Landroid/widget/TextView;

.field public G:Landroid/widget/TextView;

.field public H:Landroid/widget/TextView;

.field public I:Landroid/widget/TextView;

.field public J:Landroid/widget/TextView;

.field public K:Landroid/widget/TextView;

.field public M:Landroid/widget/Button;

.field public O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public P:Z

.field public Q:Landroidx/appcompat/widget/Toolbar;

.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/LinearLayout;

.field public g:Landroid/widget/LinearLayout;

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/LinearLayout;

.field public k:Landroid/widget/LinearLayout;

.field public l:Landroid/widget/LinearLayout;

.field public m:Landroid/widget/LinearLayout;

.field public n:Landroid/widget/LinearLayout;

.field public p:Landroid/widget/LinearLayout;

.field public q:Landroid/widget/LinearLayout;

.field public t:Landroid/widget/LinearLayout;

.field public v:Landroid/widget/TextView;

.field public w:Landroid/widget/TextView;

.field public x:Landroid/widget/TextView;

.field public y:Landroid/widget/TextView;

.field public z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->P:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Z(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->J(Z)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;[Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->D([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final D([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .line 1
    array-length v0, p1

    array-length v1, p2

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p1, p1

    array-length v1, p2

    invoke-static {p2, v2, v0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public final E([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_1

    if-nez v2, :cond_0

    aget-object v3, p1, v1

    goto :goto_1

    :cond_0
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, p1, v2

    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final F([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p1

    const-string v4, ", "

    if-ge v2, v3, :cond_1

    if-nez v2, :cond_0

    aget-object v3, p1, v1

    goto :goto_1

    :cond_0
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, p1, v2

    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_2
    array-length p1, p2

    if-ge v1, p1, :cond_2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, p2, v1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final G()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->M:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$c0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$c0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final H()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$k;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$k;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    const v1, 0x7f0900cf

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$v;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$v;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final I()V
    .locals 1

    .line 1
    const v0, 0x7f0900c0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->M:Landroid/widget/Button;

    const v0, 0x7f0901d2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->j:Landroid/widget/LinearLayout;

    const v0, 0x7f0903ae

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->C:Landroid/widget/TextView;

    const v0, 0x7f0901d3

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->i:Landroid/widget/LinearLayout;

    const v0, 0x7f0903af

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->A:Landroid/widget/TextView;

    const v0, 0x7f090219

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->d:Landroid/widget/LinearLayout;

    const v0, 0x7f0903fc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->v:Landroid/widget/TextView;

    const v0, 0x7f0901d4

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->e:Landroid/widget/LinearLayout;

    const v0, 0x7f0903b0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->w:Landroid/widget/TextView;

    const v0, 0x7f0901d5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->f:Landroid/widget/LinearLayout;

    const v0, 0x7f0903b1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->x:Landroid/widget/TextView;

    const v0, 0x7f0901d6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f0901d1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->h:Landroid/widget/LinearLayout;

    const v0, 0x7f0903b2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->y:Landroid/widget/TextView;

    const v0, 0x7f0903ad

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->z:Landroid/widget/TextView;

    const v0, 0x7f090204

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->k:Landroid/widget/LinearLayout;

    const v0, 0x7f0903e1

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->D:Landroid/widget/TextView;

    const v0, 0x7f090207

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->l:Landroid/widget/LinearLayout;

    const v0, 0x7f0903e7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->F:Landroid/widget/TextView;

    const v0, 0x7f09022c

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->m:Landroid/widget/LinearLayout;

    const v0, 0x7f090418

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->G:Landroid/widget/TextView;

    const v0, 0x7f0901dc

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->n:Landroid/widget/LinearLayout;

    const v0, 0x7f0903b7

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->H:Landroid/widget/TextView;

    const v0, 0x7f090226

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->p:Landroid/widget/LinearLayout;

    const v0, 0x7f09040f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->I:Landroid/widget/TextView;

    const v0, 0x7f0901f3

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->q:Landroid/widget/LinearLayout;

    const v0, 0x7f0903ce

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->J:Landroid/widget/TextView;

    const v0, 0x7f090212

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->t:Landroid/widget/LinearLayout;

    const v0, 0x7f0903f2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->K:Landroid/widget/TextView;

    return-void
.end method

.method public final J(Z)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    move/from16 v1, p1

    if-eqz v1, :cond_0

    iput-boolean v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->P:Z

    :cond_0
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v2

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->H:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNameString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamNameString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getDateString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getReportCardString()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getClassString()Ljava/lang/String;

    move-result-object v6

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getGradeString()Ljava/lang/String;

    move-result-object v7

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRankString()Ljava/lang/String;

    move-result-object v8

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSubjectString()Ljava/lang/String;

    move-result-object v9

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMarksString()Ljava/lang/String;

    move-result-object v10

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getPercentageString()Ljava/lang/String;

    move-result-object v11

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectAnswerString()Ljava/lang/String;

    move-result-object v12

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getIncorrectAnswerString()Ljava/lang/String;

    move-result-object v13

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTotalMarksString()Ljava/lang/String;

    move-result-object v14

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQueNoString()Ljava/lang/String;

    move-result-object v15

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getAttemptedString()Ljava/lang/String;

    move-result-object v16

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectString()Ljava/lang/String;

    move-result-object v17

    filled-new-array/range {v2 .. v17}, [Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->I:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->v:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollDigitsLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->w:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFiveOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->x:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSixOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->y:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getEightOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->z:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTenOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->A:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFourOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->C:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getThreeOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->D:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixPrimary()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixSecondary()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->F([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->F:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->G:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->J:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->K:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->E([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final K()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$f;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->h:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$m;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$m;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->j:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$n;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$n;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final M()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$j;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$j;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->i:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$l;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$l;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final N()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->e:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$i;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$i;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final O()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->f:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$c;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->g:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$e;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Q()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$e0;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$e0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->n:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$f0;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$f0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final R()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$x;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$x;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->q:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$y;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$y;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final S()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$o;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->k:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$p;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$p;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final T()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$q;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$q;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->l:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$r;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$r;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final U()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$u;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$u;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->t:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$w;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$w;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final V()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->p:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$h0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final W()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$i0;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$i0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->d:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$a;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Lmf1$d;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final X()V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$s;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$s;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->m:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$t;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$t;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ly01;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Y()V
    .locals 2

    .line 1
    const v0, 0x7f09043c

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Q:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Q:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$b0;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$b0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Z(Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$d0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    const v2, 0x7f120031

    const v3, 0x7f12018f

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final a0()V
    .locals 9

    .line 1
    new-instance v7, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$z;

    invoke-direct {v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$z;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$a0;

    invoke-direct {v8, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$a0;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    const/4 v1, 0x0

    const v2, 0x7f1201d3

    const v3, 0x7f120337

    const v4, 0x7f120241

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v8}, Lxs;->b(Landroid/content/Context;IIIIIILy01;Ly01;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->P:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->a0()V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0031

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->I()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "LABEL_PROFILE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    if-nez p1, :cond_1

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-direct {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->M:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Q:Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f120127

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Q:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getLabelProfileName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->Q()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->N()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->W()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->M()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->S()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->T()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->X()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->P()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->K()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->V()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->R()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->U()V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->J(Z)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->H()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->G()V

    return-void
.end method

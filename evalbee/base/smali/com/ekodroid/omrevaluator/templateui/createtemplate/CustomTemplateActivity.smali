.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
    }
.end annotation


# instance fields
.field public A:Ljava/util/ArrayList;

.field public C:[[Lrb0;

.field public D:[[Lsb0;

.field public F:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

.field public G:Landroid/view/View$OnClickListener;

.field public c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public d:Landroid/widget/TableLayout;

.field public e:Landroid/widget/Button;

.field public f:Landroid/widget/ImageButton;

.field public g:Landroid/widget/LinearLayout;

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/LinearLayout;

.field public k:Landroid/widget/LinearLayout;

.field public l:Landroid/widget/Spinner;

.field public m:Landroid/widget/Spinner;

.field public n:I

.field public p:I

.field public q:I

.field public t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

.field public w:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

.field public x:Ljava/util/ArrayList;

.field public y:Ljava/util/ArrayList;

.field public z:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->OTHER:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->F:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->G:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->F:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    return-object p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->F:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    return-object p1
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->o0(II)V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;[Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->f0([Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)[[Lrb0;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    return-object p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->k0(II)V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m0(II)V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->M0()V

    return-void
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->O0()V

    return-void
.end method

.method public static synthetic M(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    return-object p0
.end method

.method public static synthetic N(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->N0()V

    return-void
.end method

.method public static synthetic O(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Z0(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    return-void
.end method

.method public static synthetic P(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z0()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic Q(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->a1(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-void
.end method

.method public static synthetic R(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->E0()V

    return-void
.end method

.method public static synthetic S(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)[[Lsb0;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    return-object p0
.end method

.method public static synthetic T(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    return-void
.end method

.method public static synthetic U(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    return-void
.end method

.method public static synthetic V(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic W(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->I0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)Z

    move-result p0

    return p0
.end method

.method public static synthetic X(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void
.end method

.method public static synthetic Y(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic Z(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p0

    return p0
.end method

.method public static synthetic a0(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;II)V

    return-void
.end method

.method public static synthetic b0(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Y0(II)V

    return-void
.end method


# virtual methods
.method public final A0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_0
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEFGH:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_1
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDEF:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_3
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABC:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_4
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCD:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_5
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->OPTION_ABCDE:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final B0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const-string p1, ""

    return-object p1

    :pswitch_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getEightOptionLabels()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x0([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSixOptionLabels()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x0([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x0([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getThreeOptionLabels()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x0([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_4
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFourOptionLabels()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x0([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_5
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFiveOptionLabels()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x0([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final C0(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const v1, 0x7f1200af

    goto :goto_1

    :pswitch_1
    const v1, 0x7f1202f5

    goto :goto_1

    :pswitch_2
    const v1, 0x7f120320

    goto :goto_1

    :pswitch_3
    const v1, 0x7f120316

    goto :goto_1

    :pswitch_4
    const v1, 0x7f1200f1

    goto :goto_1

    :pswitch_5
    const v1, 0x7f1200ef

    :goto_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final D0(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    if-nez v1, :cond_0

    const v1, 0x7f120248

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/16 v3, 0xa

    if-eq v2, v3, :cond_2

    const/16 v3, 0xb

    if-eq v2, v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Q "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method public final E0()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->L0()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f120274

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z0()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const-class v3, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "SHEET_TEMPLATE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final F0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/16 v0, 0xc

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :cond_0
    :pswitch_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final G0(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 30

    .line 1
    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EXAMSET_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    iget v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    move/from16 v18, v4

    move-object v4, v2

    invoke-direct/range {v4 .. v18}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    iput-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->w:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    :cond_0
    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x:Ljava/util/ArrayList;

    new-instance v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->ID_BLOCK:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    iget v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    move/from16 v19, v4

    move-object v4, v14

    move-object v3, v14

    move-wide v14, v15

    move/from16 v16, v17

    move-object/from16 v17, v18

    move/from16 v18, v19

    invoke-direct/range {v4 .. v18}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v2

    const/4 v14, 0x0

    const/16 v20, 0x1

    :goto_0
    array-length v4, v2

    if-ge v14, v4, :cond_4

    aget-object v4, v2, v14

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v15

    const/4 v12, 0x0

    :goto_1
    array-length v4, v15

    if-ge v12, v4, :cond_3

    aget-object v19, v15, v12

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->F0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v13, 0x0

    :goto_2
    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfQue()I

    move-result v4

    if-ge v13, v4, :cond_2

    iget-object v11, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x:Ljava/util/ArrayList;

    new-instance v10, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v5

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getPositiveMark()D

    move-result-wide v16

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNegetiveMark()D

    move-result-wide v21

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->isPartialAllowed()Z

    move-result v18

    invoke-virtual/range {v19 .. v19}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getPayload()Ljava/lang/String;

    move-result-object v23

    iget v6, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    move-object v4, v10

    move/from16 v24, v6

    move/from16 v6, v20

    move-object v3, v10

    move v10, v14

    move-object/from16 v25, v11

    move v11, v12

    move/from16 v26, v12

    move/from16 v27, v13

    move-wide/from16 v12, v16

    move/from16 v28, v14

    move-object/from16 v29, v15

    move-wide/from16 v14, v21

    move/from16 v16, v18

    move-object/from16 v17, v23

    move/from16 v18, v24

    invoke-direct/range {v4 .. v18}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;IIIIIIDDZLjava/lang/String;I)V

    move-object/from16 v4, v25

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v20, v20, 0x1

    add-int/lit8 v13, v27, 0x1

    move/from16 v12, v26

    move/from16 v14, v28

    move-object/from16 v15, v29

    goto :goto_2

    :cond_2
    move/from16 v26, v12

    move/from16 v28, v14

    move-object/from16 v29, v15

    add-int/lit8 v12, v26, 0x1

    goto :goto_1

    :cond_3
    move/from16 v28, v14

    add-int/lit8 v14, v28, 0x1

    goto/16 :goto_0

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    return-void
.end method

.method public final H0()V
    .locals 1

    .line 1
    const v0, 0x7f09019d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->f:Landroid/widget/ImageButton;

    const v0, 0x7f0900b5

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->e:Landroid/widget/Button;

    const v0, 0x7f090379

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->d:Landroid/widget/TableLayout;

    const v0, 0x7f09034d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    const v0, 0x7f09034c

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m:Landroid/widget/Spinner;

    const v0, 0x7f0901d8

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f0901ed

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h:Landroid/widget/LinearLayout;

    const v0, 0x7f090216

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j:Landroid/widget/LinearLayout;

    const v0, 0x7f090230

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->i:Landroid/widget/LinearLayout;

    const v0, 0x7f09021b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->k:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final I0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)Z
    .locals 3

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0xb

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    invoke-virtual {p0, p2, p3, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->J0(III)Z

    move-result p1

    return p1

    :pswitch_1
    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    const/4 v0, 0x5

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    invoke-virtual {p0, p2, p3, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->J0(III)Z

    move-result p1

    return p1

    :pswitch_2
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object p1

    iget-boolean p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    invoke-virtual {p0, p2, p3, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->J0(III)Z

    move-result p1

    return p1

    :pswitch_3
    invoke-virtual {p0, p2, p3, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->J0(III)Z

    move-result p1

    return p1

    :pswitch_4
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p1

    if-eqz p1, :cond_2

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    goto :goto_2

    :cond_2
    const/4 p1, 0x4

    :goto_2
    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p2, p3, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->J0(III)Z

    move-result p1

    return p1

    :pswitch_5
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p1

    return p1

    :pswitch_6
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p1

    return p1

    :pswitch_7
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p1

    return p1

    :pswitch_8
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p1

    return p1

    :pswitch_9
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p1

    return p1

    :pswitch_a
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->K0(II)Z

    move-result p1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final J0(III)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    array-length v0, v0

    sub-int/2addr v0, p1

    add-int/lit8 v1, p3, -0x1

    const v2, 0x7f08016e

    const v3, 0x7f0800bd

    const v4, 0x7f1202fc

    const/4 v5, 0x0

    if-ge v0, v1, :cond_0

    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {p1, v4, v3, v2}, La91;->G(Landroid/content/Context;III)V

    return v5

    :cond_0
    move v0, v5

    :goto_1
    const/4 v1, 0x1

    if-ge v0, p3, :cond_2

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v7, p1, -0x1

    add-int/2addr v7, v0

    aget-object v6, v6, v7

    add-int/lit8 v1, p2, -0x1

    aget-object v1, v6, v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return v1
.end method

.method public final K0(II)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    aget-object p1, v0, p1

    sub-int/2addr p2, v1

    aget-object p1, p1, p2

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const p2, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f1202fc

    invoke-static {p1, v1, p2, v0}, La91;->G(Landroid/content/Context;III)V

    const/4 p1, 0x0

    return p1

    :cond_0
    return v1
.end method

.method public final L0()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l0()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final M0()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    array-length v1, v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    array-length v0, v0

    const/4 v2, 0x1

    move v3, v2

    :goto_0
    add-int/lit8 v4, v1, 0x1

    if-ge v3, v4, :cond_1

    move v4, v2

    :goto_1
    add-int/lit8 v5, v0, 0x1

    if-ge v4, v5, :cond_0

    invoke-virtual {p0, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final N0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    const v1, 0x7f0c00f0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C0(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-direct {v2, p0, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l0()Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D0(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-direct {v2, p0, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :goto_1
    return-void
.end method

.method public final O0()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->F:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ADD:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    const v2, 0x7f06004e

    const v3, 0x7f08016b

    const/4 v4, 0x0

    const v5, 0x7f06005c

    const/4 v6, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->e:Landroid/widget/Button;

    :goto_0
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ERASE:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->e:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m:Landroid/widget/Spinner;

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->OTHER:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->e:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final P0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->e:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Q0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->g:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$q;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$q;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final R0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->h:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final S0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$p;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$p;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final T0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->k:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final U0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->i:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final V0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->f:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final W0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f0c00f0

    sget-object v2, Lgr1;->c:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$o;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$o;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final X0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Ljava/util/ArrayList;II)V
    .locals 9

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c005f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f120029

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const/4 v2, 0x2

    new-array v5, v2, [Landroid/widget/Spinner;

    const v3, 0x7f090340

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    const/4 v4, 0x0

    aput-object v3, v5, v4

    const v3, 0x7f090341

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    const/4 v3, 0x1

    aput-object v1, v5, v3

    move v1, v4

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D0(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v6, Landroid/widget/ArrayAdapter;

    const v7, 0x7f0c00f0

    invoke-direct {v6, p0, v7, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    aget-object v3, v5, v1

    invoke-virtual {v3, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    aget-object v1, v5, v4

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;

    move-object v3, p1

    move-object v4, p0

    move-object v6, p2

    move v7, p3

    move v8, p4

    invoke-direct/range {v3 .. v8}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$j;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;[Landroid/widget/Spinner;Ljava/util/ArrayList;II)V

    const p2, 0x7f120022

    invoke-virtual {v0, p2, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$k;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$k;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    const p2, 0x7f120053

    invoke-virtual {v0, p2, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final Y0(II)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0063

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f12002f

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090150

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;

    invoke-direct {v2, p0, v1, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$l;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Landroid/widget/EditText;II)V

    const p1, 0x7f120022

    invoke-virtual {v0, p1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$m;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$m;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    const p2, 0x7f120053

    invoke-virtual {v0, p2, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final Z0(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$n;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$n;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    new-instance v1, Ltb0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {v1, v2, v0, p1}, Ltb0;-><init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    return-void
.end method

.method public final a1(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 4

    .line 1
    new-instance v0, Lu80;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {v0, v1}, Lu80;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c008b

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/a;

    invoke-direct {v2}, Lcom/ekodroid/omrevaluator/templateui/models/a;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-virtual {v2, v3, p1, v1}, Lcom/ekodroid/omrevaluator/templateui/models/a;->X(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Landroid/graphics/Bitmap;

    move-result-object p1

    const v1, 0x7f09048d

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    invoke-virtual {v1, p1}, Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v2, 0x7f0900b7

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;

    invoke-direct {v3, p0, v1, p1, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;Landroid/graphics/Bitmap;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->create()V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final b1(II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 p1, p1, -0x1

    aget-object v0, v0, p1

    add-int/lit8 p2, p2, -0x1

    aget-object v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    const-string v1, " + "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    aget-object p1, v0, p1

    aget-object p1, p1, p2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f080087

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    aget-object v1, v1, p1

    aget-object v1, v1, p2

    iget-object v0, v0, Lrb0;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06005c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    aget-object p1, v0, p1

    aget-object p1, p1, p2

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f080088

    :goto_0
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final c0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NUM : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v1, 0x0

    iput v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    iget-object v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v2}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v2

    iget-boolean v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v2, :cond_0

    const/16 v2, 0xc

    goto :goto_0

    :cond_0
    const/16 v2, 0xb

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v4, v1

    aget-object v3, v3, v4

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    add-int/lit8 v4, v4, -0x1

    new-instance v5, Lrb0;

    invoke-direct {v5, v0, p1}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v5, v3, v4

    add-int v3, p2, v1

    invoke-virtual {p0, v3, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final d0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 7

    .line 1
    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->SET_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    const/4 v5, 0x0

    move-object v0, v6

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v1, p2, -0x1

    aget-object v0, v0, v1

    add-int/lit8 v2, p3, -0x1

    new-instance v3, Lrb0;

    const v4, 0x7f1200d5

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v6}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v3, v0, v2

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v0, p2, 0x1

    iput v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    const/4 v3, 0x5

    if-le v0, v3, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v0, :cond_1

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int v5, v1, v3

    add-int/lit8 v5, v5, 0x1

    aget-object v4, v4, v5

    new-instance v5, Lrb0;

    const-string v6, "E SET"

    invoke-direct {v5, v6, p1}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v5, v4, v2

    add-int v4, p2, v3

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final e0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 6

    .line 1
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QM : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v2, 0x0

    :goto_1
    add-int/lit8 v3, v0, 0x1

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v4, v2

    aget-object v3, v3, v4

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    add-int/lit8 v4, v4, -0x1

    new-instance v5, Lrb0;

    invoke-direct {v5, v1, p1}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v5, v3, v4

    add-int v3, p2, v2

    invoke-virtual {p0, v3, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final f0([Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 7

    .line 1
    const-string v0, "NQ:"

    const/4 v1, 0x0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v4, p1

    if-ge v3, v4, :cond_1

    aget-object v4, p1, v3

    if-eqz v4, :cond_0

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    if-ne v5, v6, :cond_0

    iput p2, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p3, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput v3, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    :goto_1
    const/16 p1, 0xb

    if-ge v2, p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v3, p2, -0x1

    add-int/2addr v3, v2

    aget-object p1, p1, v3

    add-int/lit8 v3, p3, -0x1

    new-instance v4, Lrb0;

    invoke-direct {v4, v0, v1}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v4, p1, v3

    add-int p1, p2, v2

    invoke-virtual {p0, p1, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->N0()V

    return-void
.end method

.method public final g0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 2

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->i0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->d0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_4
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v0

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y0(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->X0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Ljava/util/ArrayList;II)V

    return-void

    :pswitch_5
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->e0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_6
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_7
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_8
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_9
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_a
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    :pswitch_b
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final h0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;II)V
    .locals 8

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->B0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;)Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    move-result-object v1

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    move-object v0, v7

    move v2, p2

    move v3, p3

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v0, p2, -0x1

    aget-object p1, p1, v0

    add-int/lit8 v0, p3, -0x1

    new-instance v1, Lrb0;

    invoke-direct {v1, v6, v7}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v1, p1, v0

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    return-void
.end method

.method public final i0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 7

    .line 1
    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->ROLL_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    iget v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q:I

    const/4 v5, 0x0

    move-object v0, v6

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    const v0, 0x7f12029e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 p2, 0x0

    :goto_0
    const/16 p3, 0xc

    if-ge p2, p3, :cond_0

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, p2

    aget-object p3, p3, v1

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    add-int/lit8 v1, v1, -0x1

    new-instance v2, Lrb0;

    invoke-direct {v2, v0, p1}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v2, p3, v1

    iget p3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr p3, p2

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    invoke-virtual {p0, p3, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final j0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Q : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput p3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v2, p2, -0x1

    aget-object v1, v1, v2

    add-int/lit8 v2, p3, -0x1

    new-instance v3, Lrb0;

    invoke-direct {v3, v0, p1}, Lrb0;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v3, v1, v2

    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    return-void
.end method

.method public final k0(II)V
    .locals 2

    .line 1
    mul-int/lit8 p2, p2, 0x5

    add-int/lit8 p2, p2, 0x1

    filled-new-array {p2, p1}, [I

    move-result-object v0

    const-class v1, Lrb0;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lrb0;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    filled-new-array {p2, p1}, [I

    move-result-object p1

    const-class p2, Lsb0;

    invoke-static {p2, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [[Lsb0;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->G0(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    return-void
.end method

.method public final l0()Ljava/util/ArrayList;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->w:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    if-eqz v1, :cond_0

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    if-gez v2, :cond_0

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    if-gez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    if-eqz v1, :cond_1

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    if-gez v2, :cond_1

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    if-gez v2, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->z:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final m0(II)V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->d:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const/16 v2, 0x1e

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-direct {v0, v1, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    const/16 v3, 0x78

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v3, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    invoke-direct {v1, v3, v2}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    add-int/2addr p1, p1

    add-int/lit8 p1, p1, 0x1

    mul-int/lit8 p2, p2, 0x5

    add-int/lit8 p2, p2, 0x1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, p2, :cond_3

    new-instance v4, Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {v4, v5}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    move v5, v2

    :goto_1
    if-ge v5, p1, :cond_2

    rem-int/lit8 v6, v5, 0x2

    if-nez v6, :cond_1

    new-instance v6, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {v6, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    rem-int/lit8 v7, v3, 0x5

    if-nez v7, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f060036

    goto :goto_2

    :cond_0
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f06005b

    :goto_2
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v4, v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    :cond_1
    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v7, v5, 0x1

    div-int/lit8 v7, v7, 0x2

    new-instance v8, Lsb0;

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {v8, v9}, Lsb0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v7}, Lsb0;->setColumn(I)V

    invoke-virtual {v8, v6}, Lsb0;->setRow(I)V

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f060044

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v8, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v9, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->D:[[Lsb0;

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v9, v6

    add-int/lit8 v7, v7, -0x1

    aput-object v8, v6, v7

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->d:Landroid/widget/TableLayout;

    invoke-virtual {v5, v4}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final n0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->s0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->p0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->r0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_4
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->q0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_5
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_6
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_7
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_8
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_9
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_a
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final o0(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 p1, p1, -0x1

    aget-object p1, v0, p1

    add-int/lit8 p2, p2, -0x1

    aget-object p1, p1, p2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p1, Lrb0;->b:Ljava/lang/Object;

    instance-of p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    if-eqz p2, :cond_1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->n0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t0(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;)V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 8

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    const v2, 0x7f120031

    const v3, 0x7f1201a9

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const v7, 0x7f0800d2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0026

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->H0()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "LABEL_PROFILE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "TEMPLATE_PARAMS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    if-nez p1, :cond_0

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-direct {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->G0(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l0()Ljava/util/ArrayList;

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->O0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->W0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->S0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Q0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->R0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->U0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->T0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->P0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->V0()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Z0(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    return-void
.end method

.method public final p0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 6

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v2, -0x1

    iput v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    const/4 v2, 0x5

    if-le p1, v2, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v4, v0, -0x1

    add-int/2addr v4, v2

    aget-object v3, v3, v4

    add-int/lit8 v4, v1, -0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    add-int v3, v0, v2

    invoke-virtual {p0, v3, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final q0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 6

    .line 1
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    :goto_0
    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v3, -0x1

    iput v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    const/4 p1, 0x0

    :goto_1
    add-int/lit8 v3, v0, 0x1

    if-ge p1, v3, :cond_1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v4, v1, -0x1

    add-int/2addr v4, p1

    aget-object v3, v3, v4

    add-int/lit8 v4, v2, -0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    add-int v3, v1, p1

    invoke-virtual {p0, v3, v2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final r0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 6

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    if-ne v4, v1, :cond_0

    iget v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    if-ne v4, v0, :cond_0

    const/4 v4, -0x1

    iput v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iput v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    goto :goto_0

    :cond_1
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object p1

    iget-boolean p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz p1, :cond_2

    const/16 p1, 0xc

    goto :goto_1

    :cond_2
    const/16 p1, 0xb

    :goto_1
    const/4 v2, 0x0

    :goto_2
    if-ge v2, p1, :cond_3

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v4, v0, -0x1

    add-int/2addr v4, v2

    aget-object v3, v3, v4

    add-int/lit8 v4, v1, -0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    add-int v3, v0, v2

    invoke-virtual {p0, v3, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method

.method public final s0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 6

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    const/4 v2, -0x1

    iput v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    const/4 p1, 0x0

    move v2, p1

    :goto_0
    const/16 v3, 0xc

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v4, v0, -0x1

    add-int/2addr v4, v2

    aget-object v3, v3, v4

    add-int/lit8 v4, v1, -0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    add-int v3, v0, v2

    invoke-virtual {p0, v3, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->getType()Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;->ROLL_LABEL:Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel$LableType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final t0(Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->getRow()I

    move-result v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->getColumn()I

    move-result v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->getRow()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetLabel;->getColumn()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    const/4 v3, 0x0

    aput-object v3, v2, p1

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    return-void
.end method

.method public final u0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 5

    .line 1
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    add-int/lit8 v3, v1, -0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v2, -0x1

    iput v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iput v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b1(II)V

    return-void
.end method

.method public final v0()[I
    .locals 7

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->n:I

    new-array v1, v0, [I

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_2

    const/16 v4, 0x8

    aput v4, v1, v3

    move v4, v2

    :goto_1
    iget-object v5, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C:[[Lrb0;

    array-length v6, v5

    if-ge v4, v6, :cond_1

    aget-object v5, v5, v4

    aget-object v5, v5, v3

    if-eqz v5, :cond_0

    iget-object v5, v5, Lrb0;->b:Ljava/lang/Object;

    instance-of v6, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    if-eqz v6, :cond_0

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    invoke-virtual {p0, v5}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->w0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)I

    move-result v5

    aget v6, v1, v3

    if-le v5, v6, :cond_0

    aput v5, v1, v3

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public final w0(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)I
    .locals 7

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$h;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x5

    if-eq v0, v2, :cond_9

    const/16 v3, 0xb

    const/4 v4, 0x6

    if-eq v0, v4, :cond_8

    const/4 v5, 0x7

    const/16 v6, 0x8

    if-eq v0, v5, :cond_5

    if-eq v0, v1, :cond_2

    if-eq v0, v3, :cond_1

    const/16 p1, 0xc

    if-eq v0, p1, :cond_0

    return v6

    :cond_0
    const/16 p1, 0xd

    return p1

    :cond_1
    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    add-int/lit8 p1, p1, 0x3

    return p1

    :cond_2
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v0

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    if-le v0, v2, :cond_3

    move v2, v0

    :cond_3
    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    if-lez p1, :cond_4

    goto :goto_0

    :cond_4
    move v4, v2

    :goto_0
    add-int/lit8 v4, v4, 0x3

    return v4

    :cond_5
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {p1}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p1

    if-eqz p1, :cond_6

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_1

    :cond_6
    move p1, v2

    :goto_1
    if-le p1, v2, :cond_7

    add-int/lit8 p1, p1, 0x3

    return p1

    :cond_7
    return v6

    :cond_8
    return v3

    :cond_9
    return v1
.end method

.method public final x0([Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    array-length v0, p1

    const-string v1, ""

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p1, v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final y0(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v2, v3, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final z0()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
    .locals 12

    .line 1
    iget v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->n:I

    iget v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->p:I

    mul-int/lit8 v1, v1, 0x5

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->w:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    if-eqz v3, :cond_0

    iget v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    if-lez v4, :cond_0

    iget v4, v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    if-lez v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    move-object v8, v3

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->x:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    if-lez v5, :cond_1

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    if-lez v5, :cond_1

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->v0()[I

    move-result-object v3

    new-array v5, v2, [Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    invoke-direct {v2, v1, v0, v3}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;-><init>(II[I)V

    const/4 v0, 0x0

    aput-object v2, v5, v0

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    const/16 v7, 0x14

    iget-object v10, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->y:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    const/16 v6, 0x1e

    move-object v4, v0

    invoke-direct/range {v4 .. v11}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;-><init>([Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;IILcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setLabelProfile(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-object v0
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->X()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    const/4 p1, 0x2

    const/4 p2, 0x0

    if-ne p3, p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-void

    :cond_0
    const/4 p1, 0x4

    if-le p3, p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p2

    iget-object p4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHeaderProfileJsonModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object p2

    :goto_0
    iput-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    goto :goto_1

    :cond_1
    if-nez p3, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getDefaultHeaderprofile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object p2

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    if-ne p3, p1, :cond_3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getDefaultMediumHeaderprofile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object p2

    goto :goto_0

    :cond_3
    const/4 p1, 0x3

    if-ne p3, p1, :cond_4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getBlankHeaderprofile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    move-result-object p2

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    goto :goto_0

    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$a;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H:Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->v:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->x:Lwu1;

    invoke-static/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;Lwu1;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

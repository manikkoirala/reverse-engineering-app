.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

.field public d:Landroid/widget/ImageButton;

.field public e:Landroid/widget/ImageButton;

.field public f:Landroid/widget/TextView;

.field public g:I

.field public h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

.field public i:Landroid/widget/LinearLayout;

.field public j:Ljava/util/ArrayList;

.field public k:Landroidx/appcompat/widget/SwitchCompat;

.field public l:Landroidx/appcompat/widget/SwitchCompat;

.field public m:Lcom/google/android/material/textfield/TextInputLayout;

.field public n:Landroid/widget/EditText;

.field public p:Landroid/widget/EditText;

.field public q:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const/4 v0, 0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->g:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->J(I)V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->G(I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->R(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->L(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final E(I)V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v4, 0x2

    invoke-virtual {v1, v4, v2, v4, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    const/16 v7, 0xc

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v8

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v9

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v10

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v11

    invoke-virtual {v6, v8, v9, v10, v11}, Landroid/view/View;->setPadding(IIII)V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f06005c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f130237

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextAppearance(I)V

    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v11, 0x1e

    invoke-virtual {p0, v11, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v11

    invoke-direct {v10, v11, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v11, 0x10

    invoke-virtual {p0, v11, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v12

    invoke-virtual {v10, v2, v2, v12, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v12, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    invoke-virtual {v12, v1, v2, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const v12, 0x7f120124

    invoke-virtual {p0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setInputType(I)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextAppearance(I)V

    const v6, 0x7f080080

    invoke-virtual {v1, v6}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    new-array v6, v8, [Landroid/text/InputFilter;

    new-instance v10, Landroid/text/InputFilter$LengthFilter;

    const/16 v12, 0xf

    invoke-direct {v10, v12}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v10, v6, v2

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v6

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v7

    invoke-virtual {v1, v6, v2, v7, v2}, Landroid/view/View;->setPadding(IIII)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v7, 0x28

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v10

    invoke-direct {v6, v5, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v11, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v5

    invoke-virtual {v6, v2, v2, v5, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iput v3, v6, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1, v8, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/Spinner;

    invoke-direct {v1, p0}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setGravity(I)V

    const v2, 0x7f08007d

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v3, 0x82

    invoke-virtual {p0, v3, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual {p0, v7, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v5

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->i:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v0, v9

    goto/16 :goto_0

    :cond_0
    return-void
.end method

.method public final F(ILandroid/content/Context;)I
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    int-to-float p1, p1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    mul-float/2addr p1, p2

    float-to-int p1, p1

    return p1
.end method

.method public final G(I)Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;
    .locals 1

    .line 1
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object p1

    :cond_0
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->FULLWIDTH:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object p1

    :cond_1
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->LARGE:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object p1

    :cond_2
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->MEDIUM:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object p1

    :cond_3
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;->SMALL:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    return-object p1
.end method

.method public final H(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;)I
    .locals 2

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$i;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    return v1

    :cond_1
    return v0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public final I()V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    const v1, 0x7f090377

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    const v1, 0x7f090376

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->l:Landroidx/appcompat/widget/SwitchCompat;

    const v1, 0x7f090151

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->n:Landroid/widget/EditText;

    const v1, 0x7f090145

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->p:Landroid/widget/EditText;

    const v1, 0x7f0903d8

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->f:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0901a6

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->e:Landroid/widget/ImageButton;

    const v1, 0x7f09018b

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->d:Landroid/widget/ImageButton;

    const v1, 0x7f0901cf

    invoke-virtual {v0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->i:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v3, 0x7f0c00f0

    sget-object v4, Lgr1;->g:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->Q()V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->l:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->p:Landroid/widget/EditText;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getInstruction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->n:Landroid/widget/EditText;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getLabels()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/high16 v6, 0x40a00000    # 5.0f

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x1

    const/4 v9, -0x2

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v8, 0x2

    invoke-virtual {v7, v8, v3, v8, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/16 v10, 0xc

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v11

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v12

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v13

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v14

    invoke-virtual {v5, v11, v12, v13, v14}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual/range {p0 .. p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f06005c

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v11

    invoke-virtual {v5, v11}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v7, Landroid/widget/TextView;

    invoke-direct {v7, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v13, v4, 0x1

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v11, 0x7f130237

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setTextAppearance(I)V

    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v15, 0x1e

    invoke-virtual {v0, v15, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v15

    invoke-direct {v14, v15, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v15, 0x10

    invoke-virtual {v0, v15, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v8

    invoke-virtual {v14, v3, v3, v8, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v5, v7, v3, v14}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    new-instance v7, Landroid/widget/EditText;

    invoke-direct {v7, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v14, 0x7f120124

    invoke-virtual {v0, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setInputType(I)V

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setTextAppearance(I)V

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v11, 0x7f080080

    invoke-static {v8, v11}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    new-array v8, v2, [Landroid/text/InputFilter;

    new-instance v11, Landroid/text/InputFilter$LengthFilter;

    const/16 v12, 0xf

    invoke-direct {v11, v12}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v11, v8, v3

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v8

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v10

    invoke-virtual {v7, v8, v3, v10, v3}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    iget-object v8, v8, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->label:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v10, 0x28

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v11

    invoke-direct {v8, v9, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v15, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v9

    invoke-virtual {v8, v3, v3, v9, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iput v6, v8, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v5, v7, v2, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    new-instance v6, Landroid/widget/Spinner;

    invoke-direct {v6, v0}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const/16 v7, 0x11

    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setGravity(I)V

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v8, 0x7f08007d

    invoke-static {v7, v8}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;

    iget-object v4, v4, Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel;->size:Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;

    invoke-virtual {v0, v4}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->H(Lcom/ekodroid/omrevaluator/templateui/models/HeaderLabel$Size;)I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/widget/AdapterView;->setSelection(I)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v7, 0x82

    invoke-virtual {v0, v7, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v7

    invoke-virtual {v0, v10, v0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->F(ILandroid/content/Context;)I

    move-result v8

    invoke-direct {v4, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v4, v13

    goto/16 :goto_0

    :cond_2
    return-void
.end method

.method public final J(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->E(I)V

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->K(I)V

    :cond_1
    return-void
.end method

.method public final K(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lt v0, p1, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->i:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final L(Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getHeaderProfileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteHeaderProfileJson(Ljava/lang/String;)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getHeaderProfileName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;-><init>(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateHeaderProfileJson(Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final M()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->e:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$d;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final N()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->d:Landroid/widget/ImageButton;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$c;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final O()V
    .locals 2

    .line 1
    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$e;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P()V
    .locals 2

    .line 1
    const v0, 0x7f0900cd

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$f;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Q()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$a;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->l:Landroidx/appcompat/widget/SwitchCompat;

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$b;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public final R(Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c007a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202a9

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090153

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getHeaderProfileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DEFAULT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getHeaderProfileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;

    invoke-direct {v2, p0, v1, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;Landroid/widget/EditText;Ljava/util/ArrayList;)V

    const p1, 0x7f1202a7

    invoke-virtual {v0, p1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$h;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$h;-><init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;)V

    const v1, 0x7f120053

    invoke-virtual {v0, v1, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0025

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f090398

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->m:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f09043d

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, p1}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "HEADER_PROFILE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getHeaderProfileName()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->m:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->h:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;->getLabels()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->g:I

    const/4 v0, 0x1

    if-ge p1, v0, :cond_1

    iput v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->g:I

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->I()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->O()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->P()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->M()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->N()V

    iget p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->g:I

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->J(I)V

    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

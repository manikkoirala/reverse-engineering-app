.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->O()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    const-string v1, "TEMPLATE_PARAMS"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->t:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const-string v1, "LABEL_PROFILE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$k;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

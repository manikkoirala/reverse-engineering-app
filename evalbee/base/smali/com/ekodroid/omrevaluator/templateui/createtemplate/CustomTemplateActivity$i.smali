.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    check-cast p1, Lsb0;

    invoke-virtual {p1}, Lsb0;->getRow()I

    move-result v0

    invoke-virtual {p1}, Lsb0;->getColumn()I

    move-result p1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ERASE:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->C(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->N(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;->ADD:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$MODE;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->V(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->V(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v1, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->W(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v1, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->X(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;II)V

    :cond_1
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->N(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)V

    :cond_2
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Y(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Y(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->m:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Z(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v2, v1, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->a0(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;II)V

    :cond_3
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->l:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->Z(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    invoke-static {v1, v0, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->b0(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;II)V

    :cond_4
    return-void
.end method

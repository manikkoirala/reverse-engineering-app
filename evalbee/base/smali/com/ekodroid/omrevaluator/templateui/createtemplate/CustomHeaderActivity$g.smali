.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->R(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Ljava/util/ArrayList;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;Landroid/widget/EditText;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->a:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {p2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->n:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    move-object v3, p2

    goto :goto_0

    :cond_0
    move-object v3, v0

    :goto_0
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->l:Landroidx/appcompat/widget/SwitchCompat;

    invoke-virtual {p2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->p:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    move-object v4, p2

    goto :goto_1

    :cond_1
    move-object v4, v0

    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const p2, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f1200c2

    invoke-static {p1, v1, p2, v0}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_2
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->b:Ljava/util/ArrayList;

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {p2, v6}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->D(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)Z

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    const v2, 0x7f120105

    invoke-static {p2, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity$g;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomHeaderActivity;

    invoke-virtual {p2}, Landroid/app/Activity;->finish()V

    :cond_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

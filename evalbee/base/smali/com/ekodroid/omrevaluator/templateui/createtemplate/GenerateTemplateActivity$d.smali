.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->d0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Q "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->p:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EXAM_SAVE"

    invoke-static {v0, v2, p1, v1}, Lo4;->b(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;->H(Lcom/ekodroid/omrevaluator/templateui/createtemplate/GenerateTemplateActivity;)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;->a1(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

.field public final synthetic b:Landroid/graphics/Bitmap;

.field public final synthetic c:Landroid/app/Dialog;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;Landroid/graphics/Bitmap;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->d:Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->b:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->c:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->a:Lcom/ekodroid/omrevaluator/templateui/components/ZoomableImageView;

    const v0, 0x106000d

    invoke-virtual {p1, v0}, Lz6;->setImageResource(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->b:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/CustomTemplateActivity$d;->c:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

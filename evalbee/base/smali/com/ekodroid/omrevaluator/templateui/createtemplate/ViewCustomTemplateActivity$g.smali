.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->N()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    const/4 p1, 0x0

    if-nez p3, :cond_0

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object p3, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-static {p2, p1, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-void

    :cond_0
    const/4 p2, 0x1

    if-ne p3, p2, :cond_1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-static {p2, p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getLabelProfileJsonModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    move-result-object p1

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    iput-object p1, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity$g;->b:Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;

    iget-object p2, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->l:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    iget-object p3, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->m:Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-static {p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;->E(Lcom/ekodroid/omrevaluator/templateui/createtemplate/ViewCustomTemplateActivity;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

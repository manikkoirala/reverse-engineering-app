.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;->E()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p1

    int-to-long p4, p1

    const-wide/32 v0, 0x7f090190    # 1.053000459E-314

    cmp-long p1, p4, v0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;->A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;->B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;)V

    goto :goto_0

    :cond_0
    const p1, 0x7f08008c

    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance p1, Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;

    iget-object p2, p2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;

    const-class p4, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-direct {p1, p2, p4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;->A(Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p2

    const-string p3, "LABEL_PROFILE"

    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity$b;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/TemplateLabelsActivity;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

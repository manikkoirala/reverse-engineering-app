.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->D()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array v0, p1, [Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1200be

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    invoke-direct {v4}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;-><init>()V

    aput-object v4, v0, v1

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->setSubName(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aget-object v3, v0, v1

    new-array v2, v2, [Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    invoke-virtual {v3, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->setSections([Lcom/ekodroid/omrevaluator/templateui/models/Section2;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    iget v2, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->t:I

    iget v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->v:I

    invoke-direct {p1, v2, v1, v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;-><init>(II[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;->c:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplate2Activity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "SUBJECT_DETAIL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity$i;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/SetTemplateActivity;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

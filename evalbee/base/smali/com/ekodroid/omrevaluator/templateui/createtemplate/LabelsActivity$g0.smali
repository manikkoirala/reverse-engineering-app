.class public Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->V()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 3

    .line 1
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setNameString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v1, 0x1

    aget-object v2, p1, v1

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setExamNameString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v2, 0x2

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setDateString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v2, 0x3

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setReportCardString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v2, 0x4

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setClassString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v2, 0x5

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setGradeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v2, 0x6

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setRankString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/4 v2, 0x7

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setSubjectString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0x8

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setMarksString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0x9

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setPercentageString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0xa

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setCorrectAnswerString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0xb

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setIncorrectAnswerString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0xc

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setTotalMarksString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0xd

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setQueNoString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0xe

    aget-object v2, p1, v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setAttemptedString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->O:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    const/16 v2, 0xf

    aget-object p1, p1, v2

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setCorrectString(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity$g0;->a:Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-static {p1, v1}, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;->B(Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;Z)V

    return-void
.end method

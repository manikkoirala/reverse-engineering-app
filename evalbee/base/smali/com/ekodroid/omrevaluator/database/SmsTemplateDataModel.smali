.class public Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "tb_sms_template"
.end annotation


# instance fields
.field private id:Ljava/lang/Integer;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        allowGeneratedIdInsert = true
        generatedId = true
    .end annotation
.end field

.field private smsString:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private smsTemplateName:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        unique = true
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->smsString:Ljava/lang/String;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->smsTemplateName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getSmsString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->smsString:Ljava/lang/String;

    return-object v0
.end method

.method public getSmsTemplateName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;->smsTemplateName:Ljava/lang/String;

    return-object v0
.end method

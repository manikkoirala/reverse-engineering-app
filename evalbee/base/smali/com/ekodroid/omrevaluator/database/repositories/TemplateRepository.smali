.class public Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static instance:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;


# instance fields
.field private helper:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->helper:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;
    .locals 0

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->init(Landroid/content/Context;)V

    sget-object p0, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->instance:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    return-object p0
.end method

.method private static init(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->instance:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->instance:Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    :cond_0
    return-void
.end method


# virtual methods
.method public deleteAllTemplateForClass(Ljava/lang/String;)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->deleteBuilder()Lcom/j256/ormlite/stmt/DeleteBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "className"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v3, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/DeleteBuilder;->delete()I
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public deleteArchiveLocalModel(Ljava/lang/String;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getArchiveLocalDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/j256/ormlite/dao/Dao;->deleteById(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public deleteHeaderProfileJson(Ljava/lang/String;)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getHeaderProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->deleteBuilder()Lcom/j256/ormlite/stmt/DeleteBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "headerName"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v3, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/DeleteBuilder;->delete()I
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public deleteLabelProfileJson(Ljava/lang/String;)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getLabelProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->deleteBuilder()Lcom/j256/ormlite/stmt/DeleteBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "profileName"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v3, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/DeleteBuilder;->delete()I
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->deleteBuilder()Lcom/j256/ormlite/stmt/DeleteBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "name"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/Where;->and()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "className"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/Where;->and()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "examDate"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/DeleteBuilder;->delete()I
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public getAllHeaderProfileJson()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getHeaderProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryForAll()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getAllImageSyncedActivatedExams()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "isCloudSyncOn"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    invoke-virtual {v2}, Lcom/j256/ormlite/stmt/Where;->and()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "syncImages"

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getAllLabelProfileJson()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getLabelProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryForAll()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getAllSyncedExams()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "isCloudSyncOn"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getArchiveLocalDataModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getArchiveLocalDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/j256/ormlite/dao/Dao;->queryForId(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getArchiveLocalDataModels()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;",
            ">;"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getArchiveLocalDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->queryForAll()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getArchivingTemplatesJson()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "isArchiving"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getHeaderProfileJsonModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getHeaderProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "headerName"

    new-instance v4, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v4, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->helper:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    return-object v0
.end method

.method public getLabelProfileJsonModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getLabelProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "profileName"

    new-instance v4, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v4, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public getNumberOfExamForAClass(Ljava/lang/String;)J
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v0

    const-string v1, "className"

    new-instance v2, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v2, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    move-result-object p1

    invoke-virtual {p1}, Lcom/j256/ormlite/stmt/Where;->countOf()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "name"

    new-instance v4, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    invoke-virtual {v2}, Lcom/j256/ormlite/stmt/Where;->and()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "className"

    new-instance v4, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    invoke-virtual {v2}, Lcom/j256/ormlite/stmt/Where;->and()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "examDate"

    new-instance v4, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public getTemplateJsonForCloudId(Ljava/lang/Long;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "cloudId"

    invoke-virtual {v2, v3, p1}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public getTemplatesJson()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryForAll()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getTemplatesJson(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            ">;"
        }
    .end annotation

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "className"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v3, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public markExamAsPublished(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/Long;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setPublished(Z)V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public migrateScopStorage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->queryForAll()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, p1, p2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setFolderPath(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/j256/ormlite/dao/Dao;->update(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    return-void
.end method

.method public saveOrUpdateArchiveLocalDataModel(Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getArchiveLocalDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public saveOrUpdateHeaderProfileJson(Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getHeaderProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public saveOrUpdateLabelProfileJson(Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getLabelProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSynced(Z)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return v0
.end method

.method public saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z
    .locals 2

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSynced(Z)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/sql/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

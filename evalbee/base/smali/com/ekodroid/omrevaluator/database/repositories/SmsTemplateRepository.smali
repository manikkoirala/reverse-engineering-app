.class public Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static instance:Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;


# instance fields
.field private helper:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->helper:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;
    .locals 0

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->init(Landroid/content/Context;)V

    sget-object p0, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->instance:Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    return-object p0
.end method

.method private static init(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->instance:Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->instance:Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    :cond_0
    return-void
.end method


# virtual methods
.method public deleteSmsTemplate(Ljava/lang/String;)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getSmsTemplateDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0}, Lcom/j256/ormlite/dao/Dao;->deleteBuilder()Lcom/j256/ormlite/stmt/DeleteBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v1

    const-string v2, "smsTemplateName"

    new-instance v3, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v3, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v0}, Lcom/j256/ormlite/stmt/DeleteBuilder;->delete()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public getAllSmsTemplates()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getSmsTemplateDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryForAll()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-object v0
.end method

.method public getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->helper:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    return-object v0
.end method

.method public getSmsTemplate(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getSmsTemplateDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v1

    invoke-interface {v1}, Lcom/j256/ormlite/dao/Dao;->queryBuilder()Lcom/j256/ormlite/stmt/QueryBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/StatementBuilder;->where()Lcom/j256/ormlite/stmt/Where;

    move-result-object v2

    const-string v3, "smsTemplateName"

    new-instance v4, Lcom/j256/ormlite/stmt/SelectArg;

    invoke-direct {v4, p1}, Lcom/j256/ormlite/stmt/SelectArg;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Lcom/j256/ormlite/stmt/Where;->eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/j256/ormlite/stmt/Where;

    invoke-virtual {v1}, Lcom/j256/ormlite/stmt/QueryBuilder;->query()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public saveOrUpdateSmsTemplate(Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getHelper()Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getSmsTemplateDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/j256/ormlite/dao/Dao;->createOrUpdate(Ljava/lang/Object;)Lcom/j256/ormlite/dao/Dao$CreateOrUpdateStatus;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.class public Lcom/ekodroid/omrevaluator/database/SheetImageModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "sheet_image"
.end annotation


# instance fields
.field private className:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private examDate:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private examName:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private id:Ljava/lang/Integer;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        allowGeneratedIdInsert = true
        generatedId = true
    .end annotation
.end field

.field private imagePath:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isSent:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private isSmsSent:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private pageIndex:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private rollNo:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private sheetDimension:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private sheetImage:[B
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        dataType = .enum Lcom/j256/ormlite/field/DataType;->BYTE_ARRAY:Lcom/j256/ormlite/field/DataType;
    .end annotation
.end field

.field private synced:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private uploadId:J
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I[BLjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->examName:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->rollNo:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->sheetImage:[B

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->className:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->examDate:Ljava/lang/String;

    new-instance p1, Lgc0;

    invoke-direct {p1}, Lgc0;-><init>()V

    invoke-virtual {p1, p6}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->sheetDimension:Ljava/lang/String;

    iput p8, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->pageIndex:I

    iput-object p7, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->imagePath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getExamDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->examDate:Ljava/lang/String;

    return-object v0
.end method

.method public getExamName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->examName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->id:Ljava/lang/Integer;

    return-object v0
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->imagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPageIndex()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->pageIndex:I

    return v0
.end method

.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->rollNo:I

    return v0
.end method

.method public getSheetDimension()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;
    .locals 3

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->sheetDimension:Ljava/lang/String;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {v0, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    return-object v0
.end method

.method public getSheetDimensionJson()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->sheetDimension:Ljava/lang/String;

    return-object v0
.end method

.method public getSheetImage()Landroid/graphics/Bitmap;
    .locals 3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->sheetImage:[B

    if-eqz v0, :cond_0

    array-length v1, v0

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->imagePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->imagePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUploadId()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->uploadId:J

    return-wide v0
.end method

.method public isSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->synced:Z

    return v0
.end method

.method public setExamName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->examName:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->id:Ljava/lang/Integer;

    return-void
.end method

.method public setImagePath(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->imagePath:Ljava/lang/String;

    return-void
.end method

.method public setPageIndex(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->pageIndex:I

    return-void
.end method

.method public setRollNo(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->rollNo:I

    return-void
.end method

.method public setSynced(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->synced:Z

    return-void
.end method

.method public setUploadId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->uploadId:J

    return-void
.end method

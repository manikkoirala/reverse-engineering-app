.class public Lcom/ekodroid/omrevaluator/database/ClassDataModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "tb_class_v2"
.end annotation


# instance fields
.field private className:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        id = true
    .end annotation
.end field

.field private subGroups:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private synced:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->className:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->subGroups:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->synced:Z

    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getSubGroups()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->subGroups:Ljava/lang/String;

    return-object v0
.end method

.method public isSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->synced:Z

    return v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->className:Ljava/lang/String;

    return-void
.end method

.method public setSubGroups(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->subGroups:Ljava/lang/String;

    return-void
.end method

.method public setSynced(Z)V
    .locals 0

    return-void
.end method

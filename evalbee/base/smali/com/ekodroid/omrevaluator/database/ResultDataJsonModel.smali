.class public Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "result_json"
.end annotation


# instance fields
.field private className:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private examDate:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private examName:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private id:Ljava/lang/Integer;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        allowGeneratedIdInsert = true
        generatedId = true
    .end annotation
.end field

.field private isEmailSent:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isPublished:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isSmsSent:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isSynced:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private resultItem:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private rollNo:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->examName:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->rollNo:I

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->examDate:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->className:Ljava/lang/String;

    new-instance p1, Lgc0;

    invoke-direct {p1}, Lgc0;-><init>()V

    invoke-virtual {p1, p5}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->resultItem:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isEmailSent:Z

    iput-boolean p7, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent:Z

    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getExamDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->examDate:Ljava/lang/String;

    return-object v0
.end method

.method public getExamName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->examName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->id:Ljava/lang/Integer;

    return-object v0
.end method

.method public getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;
    .locals 3

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->resultItem:Ljava/lang/String;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {v0, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getSectionsInSubjects()[I

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSectionsInSubject()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->setSectionsInSubjects([I)V

    :cond_0
    invoke-static {v0, p1}, Lve1;->w(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-object v0
.end method

.method public getResultItemString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->resultItem:Ljava/lang/String;

    return-object v0
.end method

.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->rollNo:I

    return v0
.end method

.method public isPublished()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished:Z

    return v0
.end method

.method public isSmsSent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent:Z

    return v0
.end method

.method public isSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced:Z

    return v0
.end method

.method public setExamName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->examName:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->id:Ljava/lang/Integer;

    return-void
.end method

.method public setPublished(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished:Z

    return-void
.end method

.method public setResultItem(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V
    .locals 1

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->resultItem:Ljava/lang/String;

    return-void
.end method

.method public setRollNo(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->rollNo:I

    return-void
.end method

.method public setSmsSent(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent:Z

    return-void
.end method

.method public setSynced(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced:Z

    return-void
.end method

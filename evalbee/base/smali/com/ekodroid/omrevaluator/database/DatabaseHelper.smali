.class public Lcom/ekodroid/omrevaluator/database/DatabaseHelper;
.super Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;
.source "SourceFile"


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "templates_database.db"

.field private static final DATABASE_VERSION:I = 0x1c

.field private static TAG:Ljava/lang/String; = "TAG_OMR"

.field private static instance:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;


# instance fields
.field private DEFALUT_CLASS:Ljava/lang/String;

.field private archivedLocalDataModelDao:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private classJsonDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ClassDataModel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fileDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/FileDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private headerProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private labelProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resultJsonDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sheetImageDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/SheetImageModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private smsTemplateDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private studentDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/StudentDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private templateJsonDAO:Lcom/j256/ormlite/dao/Dao;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/16 v0, 0x1c

    const-string v1, "templates_database.db"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    const-string p1, "Default"

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->DEFALUT_CLASS:Ljava/lang/String;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->sheetImageDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->studentDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->fileDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->templateJsonDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->resultJsonDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->labelProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->smsTemplateDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->headerProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;

    iput-object v2, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->archivedLocalDataModelDao:Lcom/j256/ormlite/dao/Dao;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/DatabaseHelper;
    .locals 0

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->init(Landroid/content/Context;)V

    sget-object p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->instance:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    return-object p0
.end method

.method private static init(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->instance:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->instance:Lcom/ekodroid/omrevaluator/database/DatabaseHelper;

    :cond_0
    return-void
.end method


# virtual methods
.method public getArchiveLocalDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->archivedLocalDataModelDao:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->archivedLocalDataModelDao:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->archivedLocalDataModelDao:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getClassDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ClassDataModel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getClassJsonDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ClassDataModel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->classJsonDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getFileDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/FileDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->fileDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/FileDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->fileDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->fileDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getHeaderProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->headerProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->headerProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->headerProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getLabelProfileJsonDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->labelProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->labelProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->labelProfileJsonDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getResultJsonDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->resultJsonDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->resultJsonDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->resultJsonDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getSheetImageDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/SheetImageModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->sheetImageDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->sheetImageDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->sheetImageDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getSmsTemplateDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->smsTemplateDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->smsTemplateDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->smsTemplateDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getStudentDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/StudentDataModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->studentDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->studentDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->studentDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/j256/ormlite/dao/Dao<",
            "Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->templateJsonDAO:Lcom/j256/ormlite/dao/Dao;

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p0, v0}, Lcom/j256/ormlite/android/apptools/OrmLiteSqliteOpenHelper;->getDao(Ljava/lang/Class;)Lcom/j256/ormlite/dao/Dao;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->templateJsonDAO:Lcom/j256/ormlite/dao/Dao;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->templateJsonDAO:Lcom/j256/ormlite/dao/Dao;

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;Lcom/j256/ormlite/support/ConnectionSource;)V
    .locals 0

    :try_start_0
    const-class p1, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/FileDataModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    const-class p1, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    sget-object p1, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->TAG:Ljava/lang/String;

    const-string p2, "table class created"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;Lcom/j256/ormlite/support/ConnectionSource;II)V
    .locals 9

    const-class p1, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    const-class p4, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;

    const-class v0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    const-class v1, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    const-class v2, Lcom/ekodroid/omrevaluator/database/FileDataModel;

    const-class v3, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    const-class v4, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    const-class v5, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    const-class v6, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    const-class v7, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    const/4 v8, 0x0

    packed-switch p3, :pswitch_data_0

    goto/16 :goto_7

    :pswitch_0
    :try_start_0
    invoke-static {p2, v7}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'isCloudSyncOn\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'isSynced\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'isPublished\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'cloudId\' INTEGER;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'lastUpdatedOn\' INTEGER;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'lastSyncedOn\' INTEGER;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    :try_start_1
    invoke-static {p2, v4}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getResultJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'result_json\' ADD COLUMN \'isSynced\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'result_json\' ADD COLUMN \'isPublished\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    :pswitch_1
    :try_start_2
    invoke-static {p2, v7}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'sharedType\' TEXT;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'userId\' TEXT;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p3

    invoke-virtual {p3}, Lr30;->O()Ljava/lang/String;

    move-result-object p3

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "UPDATE tb_templates_v2 SET sharedType=\'PUBLIC\', userId=\'"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\' WHERE isCloudSyncOn=1;"

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_2
    :pswitch_2
    :try_start_3
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getStudentDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'tb_students_v2\' ADD COLUMN \'isSynced\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    :catch_3
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_3
    :pswitch_3
    :try_start_4
    invoke-static {p2, v7}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getTemplateJsonDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'tb_templates_v2\' ADD COLUMN \'syncImages\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_4

    :catch_4
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_4
    :try_start_5
    invoke-static {p2, v6}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getSheetImageDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'sheet_image\' ADD COLUMN \'synced\' boolean NOT NULL default 0;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_5

    :catch_5
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_5
    :pswitch_4
    :try_start_6
    invoke-static {p2, v6}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/DatabaseHelper;->getSheetImageDAO()Lcom/j256/ormlite/dao/Dao;

    move-result-object p1

    const-string p3, "ALTER TABLE \'sheet_image\' ADD COLUMN \'uploadId\' INTEGER;"

    new-array p4, v8, [Ljava/lang/String;

    invoke-interface {p1, p3, p4}, Lcom/j256/ormlite/dao/Dao;->executeRaw(Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_6

    :catch_6
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_6
    :pswitch_5
    :try_start_7
    invoke-static {p2, v5}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_7

    :catch_7
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_7

    :pswitch_6
    const/4 p3, 0x1

    :try_start_8
    invoke-static {p2, v6, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v3, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v2, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v7, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v4, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v5, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v1, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v0, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, p4, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, p1, p3}, Lcom/j256/ormlite/table/TableUtils;->dropTable(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;Z)I

    invoke-static {p2, v6}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v3}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v2}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v7}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v4}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v5}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, v0}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, p4}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I

    invoke-static {p2, p1}, Lcom/j256/ormlite/table/TableUtils;->createTableIfNotExists(Lcom/j256/ormlite/support/ConnectionSource;Ljava/lang/Class;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_7

    :catch_8
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_7
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

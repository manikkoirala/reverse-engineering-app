.class public Lcom/ekodroid/omrevaluator/database/FileDataModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "file_data"
.end annotation


# instance fields
.field private fileData:[B
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        dataType = .enum Lcom/j256/ormlite/field/DataType;->BYTE_ARRAY:Lcom/j256/ormlite/field/DataType;
    .end annotation
.end field

.field private fileName:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        id = true
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/FileDataModel;->fileName:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/FileDataModel;->fileData:[B

    return-void
.end method


# virtual methods
.method public getFileData()[B
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/FileDataModel;->fileData:[B

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/FileDataModel;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public setFileData([B)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/FileDataModel;->fileData:[B

    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/FileDataModel;->fileName:Ljava/lang/String;

    return-void
.end method

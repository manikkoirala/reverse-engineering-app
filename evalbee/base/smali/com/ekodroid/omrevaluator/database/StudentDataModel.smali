.class public Lcom/ekodroid/omrevaluator/database/StudentDataModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "tb_students_v2"
.end annotation


# instance fields
.field private className:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        uniqueCombo = true
    .end annotation
.end field

.field private emailId:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private id:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        allowGeneratedIdInsert = true
        generatedId = true
    .end annotation
.end field

.field private isSynced:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private phoneNo:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private rollNO:Ljava/lang/Integer;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        uniqueCombo = true
    .end annotation
.end field

.field private studentName:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->rollNO:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->studentName:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->emailId:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->phoneNo:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->className:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->emailId:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->phoneNo:Ljava/lang/String;

    return-object v0
.end method

.method public getRollNO()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->rollNO:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStudentName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->studentName:Ljava/lang/String;

    return-object v0
.end method

.method public isSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->isSynced:Z

    return v0
.end method

.method public setEmailId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->emailId:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->phoneNo:Ljava/lang/String;

    return-void
.end method

.method public setRollNO(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->rollNO:Ljava/lang/Integer;

    return-void
.end method

.method public setStudentName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->studentName:Ljava/lang/String;

    return-void
.end method

.method public setSynced(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->isSynced:Z

    return-void
.end method

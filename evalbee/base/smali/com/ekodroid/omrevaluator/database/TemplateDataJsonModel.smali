.class public Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "tb_templates_v2"
.end annotation


# instance fields
.field private archiveId:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private archivePayload:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private className:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        uniqueCombo = true
    .end annotation
.end field

.field private cloudId:Ljava/lang/Long;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private examDate:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        uniqueCombo = true
    .end annotation
.end field

.field private folderPath:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private id:Ljava/lang/Integer;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        allowGeneratedIdInsert = true
        generatedId = true
    .end annotation
.end field

.field private isArchiving:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isCloudSyncOn:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isPublished:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private isSynced:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private lastSyncedOn:Ljava/lang/Long;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private lastUpdatedOn:Ljava/lang/Long;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        uniqueCombo = true
    .end annotation
.end field

.field sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private sheetTemplate:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private syncImages:Z
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field

.field private userId:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->examDate:Ljava/lang/String;

    new-instance p1, Lgc0;

    invoke-direct {p1}, Lgc0;-><init>()V

    invoke-virtual {p1, p4}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->sheetTemplate:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->className:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->folderPath:Ljava/lang/String;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isArchiving:Z

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archivePayload:Ljava/lang/String;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archiveId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getArchiveId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archiveId:Ljava/lang/String;

    return-object v0
.end method

.method public getArchivePayload()Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;
    .locals 3

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archivePayload:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archivePayload:Ljava/lang/String;

    const-class v2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    invoke-virtual {v0, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;-><init>()V

    return-object v0

    :cond_2
    :goto_0
    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;-><init>()V

    return-object v0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getCloudId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->cloudId:Ljava/lang/Long;

    return-object v0
.end method

.method public getExamDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->examDate:Ljava/lang/String;

    return-object v0
.end method

.method public getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 4

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->className:Ljava/lang/String;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->examDate:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getFolderPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->folderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getLastSyncedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->lastSyncedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public getLastUpdatedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->lastUpdatedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object v0
.end method

.method public getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
    .locals 3

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->sheetTemplate:Ljava/lang/String;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public isArchiving()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isArchiving:Z

    return v0
.end method

.method public isCloudSyncOn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn:Z

    return v0
.end method

.method public isPublished()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isPublished:Z

    return v0
.end method

.method public isSyncImages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->syncImages:Z

    return v0
.end method

.method public isSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced:Z

    return v0
.end method

.method public setArchiveId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archiveId:Ljava/lang/String;

    return-void
.end method

.method public setArchivePayload(Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;)V
    .locals 1

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->archivePayload:Ljava/lang/String;

    return-void
.end method

.method public setArchiving(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isArchiving:Z

    return-void
.end method

.method public setCloudId(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->cloudId:Ljava/lang/Long;

    return-void
.end method

.method public setCloudSyncOn(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn:Z

    return-void
.end method

.method public setFolderPath(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->folderPath:Ljava/lang/String;

    return-void
.end method

.method public setLastSyncedOn(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->lastSyncedOn:Ljava/lang/Long;

    return-void
.end method

.method public setLastUpdatedOn(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->lastUpdatedOn:Ljava/lang/Long;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->name:Ljava/lang/String;

    return-void
.end method

.method public setPublished(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isPublished:Z

    return-void
.end method

.method public setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-void
.end method

.method public setSheetTemplate(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 1

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->sheetTemplate:Ljava/lang/String;

    return-void
.end method

.method public setSyncImages(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->syncImages:Z

    return-void
.end method

.method public setSynced(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced:Z

    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->userId:Ljava/lang/String;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "tb_archived_exam_v1"
.end annotation


# instance fields
.field private archivedOn:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private attendees:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private className:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private examDate:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private noOfQue:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private sizeKb:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field

.field private templateId:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
        id = true
    .end annotation
.end field

.field private version:I
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->templateId:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->className:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->examDate:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->archivedOn:Ljava/lang/String;

    iput p6, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->sizeKb:I

    iput p7, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->version:I

    iput p8, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->attendees:I

    iput p9, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->noOfQue:I

    return-void
.end method


# virtual methods
.method public getArchivedOn()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->archivedOn:Ljava/lang/String;

    return-object v0
.end method

.method public getAttendees()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->attendees:I

    return v0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getExamDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->examDate:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNoOfQue()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->noOfQue:I

    return v0
.end method

.method public getSizeKb()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->sizeKb:I

    return v0
.end method

.method public getTemplateId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->templateId:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->version:I

    return v0
.end method

.method public setArchivedOn(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->archivedOn:Ljava/lang/String;

    return-void
.end method

.method public setAttendees(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->attendees:I

    return-void
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->className:Ljava/lang/String;

    return-void
.end method

.method public setExamDate(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->examDate:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->name:Ljava/lang/String;

    return-void
.end method

.method public setNoOfQue(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->noOfQue:I

    return-void
.end method

.method public setSizeKb(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->sizeKb:I

    return-void
.end method

.method public setTemplateId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->templateId:Ljava/lang/String;

    return-void
.end method

.method public setVersion(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->version:I

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/j256/ormlite/table/DatabaseTable;
    tableName = "tb_template_header"
.end annotation


# instance fields
.field private headerName:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        id = true
    .end annotation
.end field

.field private headerProfile:Ljava/lang/String;
    .annotation runtime Lcom/j256/ormlite/field/DatabaseField;
        canBeNull = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p2}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->headerProfile:Ljava/lang/String;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->headerName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getHeaderName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->headerName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderProfile()Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;
    .locals 3

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/database/TemplateHeaderJsonDataModel;->headerProfile:Ljava/lang/String;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    invoke-virtual {v0, v1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/HeaderProfile;

    return-object v0
.end method

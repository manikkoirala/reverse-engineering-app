.class public final enum Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SortBy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

.field public static final enum CLASS:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

.field public static final enum DATE:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

.field public static final enum NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;
    .locals 3

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->DATE:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->CLASS:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    filled-new-array {v0, v1, v2}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    const-string v1, "DATE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->DATE:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    const-string v1, "NAME"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    const-string v1, "CLASS"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->CLASS:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    invoke-static {}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->$values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    return-object v0
.end method

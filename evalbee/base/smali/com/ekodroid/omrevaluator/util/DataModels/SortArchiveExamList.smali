.class public Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$c;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$c;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final b(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$a;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$a;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final c(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$b;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$b;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public d(Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$d;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;->a(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;->b(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList;->c(Ljava/util/ArrayList;Z)V

    :goto_1
    return-void
.end method

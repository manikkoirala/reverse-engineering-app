.class public final enum Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultSortBy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

.field public static final enum MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

.field public static final enum NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

.field public static final enum RANK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

.field public static final enum ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;
    .locals 4

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->RANK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v3, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    filled-new-array {v0, v1, v2, v3}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const-string v1, "MARK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const-string v1, "RANK"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->RANK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const-string v1, "ROLLNO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const-string v1, "NAME"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    invoke-static {}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->$values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    return-object v0
.end method

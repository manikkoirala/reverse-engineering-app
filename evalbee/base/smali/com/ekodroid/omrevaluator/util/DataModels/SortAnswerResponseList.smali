.class public Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$c;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$c;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final b(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$b;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$b;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final c(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$d;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$d;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final d(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$a;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$a;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public e(Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$e;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;->d(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;->b(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;->a(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;->c(Ljava/util/ArrayList;Z)V

    :goto_1
    return-void
.end method

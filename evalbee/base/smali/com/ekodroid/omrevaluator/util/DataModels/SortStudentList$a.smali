.class public Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;->b(Ljava/util/ArrayList;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$a;->b:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$a;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/ekodroid/omrevaluator/database/StudentDataModel;Lcom/ekodroid/omrevaluator/database/StudentDataModel;)I
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$a;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Integer;->compare(II)I

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p2, p1}, Ljava/lang/Integer;->compare(II)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    check-cast p2, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$a;->a(Lcom/ekodroid/omrevaluator/database/StudentDataModel;Lcom/ekodroid/omrevaluator/database/StudentDataModel;)I

    move-result p1

    return p1
.end method

.class public final enum Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SortBy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

.field public static final enum NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

.field public static final enum ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;
    .locals 2

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    filled-new-array {v0, v1}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    const-string v1, "ROLLNO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    const-string v1, "NAME"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    invoke-static {}, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->$values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    return-object v0
.end method

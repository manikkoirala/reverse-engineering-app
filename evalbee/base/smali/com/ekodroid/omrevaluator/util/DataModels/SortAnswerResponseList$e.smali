.class public abstract synthetic Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1009
    name = null
.end annotation


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$e;->a:[I

    :try_start_0
    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->QUESTION:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$e;->a:[I

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->CORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$e;->a:[I

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->INCORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$e;->a:[I

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method

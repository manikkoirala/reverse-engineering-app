.class public final enum Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnswerResponseSortBy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

.field public static final enum CORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

.field public static final enum INCORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

.field public static final enum QUESTION:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

.field public static final enum UNATTEMPTED:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;
    .locals 4

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->QUESTION:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->CORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->INCORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    sget-object v3, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    filled-new-array {v0, v1, v2, v3}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    const-string v1, "QUESTION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->QUESTION:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    const-string v1, "CORRECT"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->CORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    const-string v1, "INCORRECT"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->INCORRECT:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    const-string v1, "UNATTEMPTED"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-static {}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->$values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->$VALUES:[Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    return-object v0
.end method

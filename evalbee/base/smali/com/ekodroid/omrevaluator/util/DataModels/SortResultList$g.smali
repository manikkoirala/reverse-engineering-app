.class public Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->f(Ljava/util/ArrayList;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;->b:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lye1;Lye1;)I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lye1;->d()D

    move-result-wide v0

    invoke-virtual {p2}, Lye1;->d()D

    move-result-wide p1

    :goto_0
    invoke-static {v0, v1, p1, p2}, Ljava/lang/Double;->compare(DD)I

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p2}, Lye1;->d()D

    move-result-wide v0

    invoke-virtual {p1}, Lye1;->d()D

    move-result-wide p1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lye1;

    check-cast p2, Lye1;

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;->a(Lye1;Lye1;)I

    move-result p1

    return p1
.end method

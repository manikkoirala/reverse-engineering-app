.class public Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->d(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->b(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->e(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->c(Ljava/util/ArrayList;Z)V

    :goto_1
    return-void
.end method

.method public final b(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$c;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$c;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final c(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$e;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$e;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final d(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$b;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$b;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final e(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$d;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$d;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final f(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$g;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final g(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$j;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$j;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final h(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$i;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$i;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public final i(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$h;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$h;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public j(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->h(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->f(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->i(Ljava/util/ArrayList;Z)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p2, p3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->g(Ljava/util/ArrayList;Z)V

    :goto_1
    return-void
.end method

.method public k(Ljava/util/ArrayList;Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$f;

    invoke-direct {v0, p0, p2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$f;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;Z)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

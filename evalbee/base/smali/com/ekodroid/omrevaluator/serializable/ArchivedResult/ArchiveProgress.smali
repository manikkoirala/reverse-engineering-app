.class public Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private expExm:Z

.field private expRes:Z

.field private progress:I

.field private uploadExm:Z

.field private uploadMeta:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProgress()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->progress:I

    return v0
.end method

.method public isExpExm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->expExm:Z

    return v0
.end method

.method public isExpRes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->expRes:Z

    return v0
.end method

.method public isUploadExm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->uploadExm:Z

    return v0
.end method

.method public isUploadMeta()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->uploadMeta:Z

    return v0
.end method

.method public setExpExm(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->expExm:Z

    return-void
.end method

.method public setExpRes(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->expRes:Z

    return-void
.end method

.method public setProgress(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->progress:I

    return-void
.end method

.method public setUploadExm(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->uploadExm:Z

    return-void
.end method

.method public setUploadMeta(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->uploadMeta:Z

    return-void
.end method

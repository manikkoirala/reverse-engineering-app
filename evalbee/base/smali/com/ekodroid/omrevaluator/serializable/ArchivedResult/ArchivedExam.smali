.class public Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedExam;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private archivedVersion:I

.field private attendees:I

.field private examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field private results:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private sheetTemplate2:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/ekodroid/omrevaluator/templateui/models/ExamId;",
            "Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedResultItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedExam;->archivedVersion:I

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedExam;->examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedExam;->attendees:I

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedExam;->sheetTemplate2:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedExam;->results:Ljava/util/ArrayList;

    return-void
.end method

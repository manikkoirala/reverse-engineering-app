.class public Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public examId:J

.field public pageIndex:I

.field public rollNumber:I

.field public sheetDimensionJson:Ljava/lang/String;

.field public uploadId:J


# direct methods
.method public constructor <init>(JIIJLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->examId:J

    iput p3, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    iput p4, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    iput-wide p5, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->uploadId:J

    iput-object p7, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->sheetDimensionJson:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getExamId()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->examId:J

    return-wide v0
.end method

.method public getPageIndex()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    return v0
.end method

.method public getRollNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    return v0
.end method

.method public getSheetDimensionJson()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->sheetDimensionJson:Ljava/lang/String;

    return-object v0
.end method

.method public getUploadId()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->uploadId:J

    return-wide v0
.end method

.class public Lcom/ekodroid/omrevaluator/serializable/UserAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private androidVersion:Ljava/lang/String;

.field private appVersion:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private country:Ljava/lang/String;

.field private deviceId:Ljava/lang/String;

.field private emailSent:I

.field private fcmId:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private imageScan:I

.field private imageScanError:I

.field private name:Ljava/lang/String;

.field private organization:Ljava/lang/String;

.field private scanCancel:I

.field private scanSave:I

.field private smsSent:I

.field private syncMinutes:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->id:Ljava/lang/String;

    move v1, p2

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->scanSave:I

    move v1, p3

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->scanCancel:I

    move v1, p4

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->emailSent:I

    move v1, p5

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->smsSent:I

    move v1, p6

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->syncMinutes:I

    move v1, p7

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->imageScan:I

    move v1, p8

    iput v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->imageScanError:I

    move-object v1, p9

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->country:Ljava/lang/String;

    move-object v1, p10

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->name:Ljava/lang/String;

    move-object v1, p11

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->androidVersion:Ljava/lang/String;

    move-object v1, p12

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->appVersion:Ljava/lang/String;

    move-object v1, p13

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->fcmId:Ljava/lang/String;

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->deviceId:Ljava/lang/String;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->city:Ljava/lang/String;

    move-object/from16 v1, p16

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->organization:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAndroidVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->androidVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->appVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->country:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailSent()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->emailSent:I

    return v0
.end method

.method public getFcmId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->fcmId:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImageScan()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->imageScan:I

    return v0
.end method

.method public getImageScanError()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->imageScanError:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOrganization()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->organization:Ljava/lang/String;

    return-object v0
.end method

.method public getScanCancel()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->scanCancel:I

    return v0
.end method

.method public getScanSave()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->scanSave:I

    return v0
.end method

.method public getSmsSent()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->smsSent:I

    return v0
.end method

.method public getSyncMinutes()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->syncMinutes:I

    return v0
.end method

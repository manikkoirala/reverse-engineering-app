.class public Lcom/ekodroid/omrevaluator/serializable/UserProfile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private configData:Ljava/lang/String;

.field private data:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private userData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->data:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->userData:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->configData:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getConfigData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->configData:Ljava/lang/String;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getUserData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->userData:Ljava/lang/String;

    return-object v0
.end method

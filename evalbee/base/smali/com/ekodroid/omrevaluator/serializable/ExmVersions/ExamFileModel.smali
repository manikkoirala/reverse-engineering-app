.class public Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field jsonData:Ljava/lang/String;

.field key:Ljava/lang/String;

.field version:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->version:I

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->key:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->jsonData:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getJsonData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->jsonData:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->version:I

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field private results:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;",
            ">;"
        }
    .end annotation
.end field

.field private sheetTemplate2:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;",
            ">;",
            "Lcom/ekodroid/omrevaluator/templateui/models/ExamId;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->sheetTemplate2:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->results:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-void
.end method


# virtual methods
.method public getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object v0
.end method

.method public getResults()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->results:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->sheetTemplate2:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    return-object v0
.end method

.method public setSheetTemplate2(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->sheetTemplate2:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    return-void
.end method

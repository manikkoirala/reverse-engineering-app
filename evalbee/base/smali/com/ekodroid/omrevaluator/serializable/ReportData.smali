.class public Lcom/ekodroid/omrevaluator/serializable/ReportData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field appVersion:I

.field ccMail:Ljava/lang/String;

.field mailSubject:Ljava/lang/String;

.field studentMail:Ljava/lang/String;

.field studentName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->studentMail:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->ccMail:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->mailSubject:Ljava/lang/String;

    iput p4, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->appVersion:I

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->studentName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAppVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->appVersion:I

    return v0
.end method

.method public getCcMail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->ccMail:Ljava/lang/String;

    return-object v0
.end method

.method public getMailSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->mailSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getStudentMail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->studentMail:Ljava/lang/String;

    return-object v0
.end method

.method public getStudentName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ReportData;->studentName:Ljava/lang/String;

    return-object v0
.end method

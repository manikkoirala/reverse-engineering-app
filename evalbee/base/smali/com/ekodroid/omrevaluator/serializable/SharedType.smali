.class public final enum Lcom/ekodroid/omrevaluator/serializable/SharedType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/serializable/SharedType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field public static final enum PRIVATE:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field public static final enum PUBLIC:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field public static final enum SHARED:Lcom/ekodroid/omrevaluator/serializable/SharedType;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 3

    sget-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PRIVATE:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    sget-object v1, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PUBLIC:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    sget-object v2, Lcom/ekodroid/omrevaluator/serializable/SharedType;->SHARED:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    filled-new-array {v0, v1, v2}, [Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;

    const-string v1, "PRIVATE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/serializable/SharedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PRIVATE:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;

    const-string v1, "PUBLIC"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/serializable/SharedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PUBLIC:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;

    const-string v1, "SHARED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/serializable/SharedType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->SHARED:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-static {}, Lcom/ekodroid/omrevaluator/serializable/SharedType;->$values()[Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->$VALUES:[Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->$VALUES:[Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/serializable/SharedType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object v0
.end method

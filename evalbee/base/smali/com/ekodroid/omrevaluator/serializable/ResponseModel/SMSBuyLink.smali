.class public Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private appVersion:I

.field private emailId:Ljava/lang/String;

.field private numberOfSms:I

.field private shortUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->emailId:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->numberOfSms:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->shortUrl:Ljava/lang/String;

    iput p4, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->appVersion:I

    return-void
.end method


# virtual methods
.method public getEmailId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->emailId:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfSms()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->numberOfSms:I

    return v0
.end method

.method public getShortUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->shortUrl:Ljava/lang/String;

    return-object v0
.end method

.method public setEmailId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->emailId:Ljava/lang/String;

    return-void
.end method

.method public setNumberOfSms(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->numberOfSms:I

    return-void
.end method

.method public setShortUrl(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;->shortUrl:Ljava/lang/String;

    return-void
.end method

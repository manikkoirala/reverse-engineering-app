.class public Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountType:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private storageExpMinutes:I

.field private teachersCount:I

.field private uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->uid:Ljava/lang/String;

    iput p3, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->storageExpMinutes:I

    iput p4, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->teachersCount:I

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->accountType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->accountType:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryDate()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->storageExpMinutes:I

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "dd-MM-yyyy"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getStorageExpMinutes()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->storageExpMinutes:I

    return v0
.end method

.method public getTeachersCount()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->teachersCount:I

    return v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->uid:Ljava/lang/String;

    return-object v0
.end method

.method public isRenewDue()Z
    .locals 2

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->storageExpMinutes:I

    invoke-static {}, La91;->n()I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    const v1, 0xa8c0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setAccountType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->accountType:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->id:Ljava/lang/String;

    return-void
.end method

.method public setStorageExpMinutes(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->storageExpMinutes:I

    return-void
.end method

.method public setTeachersCount(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->teachersCount:I

    return-void
.end method

.method public setUid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->uid:Ljava/lang/String;

    return-void
.end method

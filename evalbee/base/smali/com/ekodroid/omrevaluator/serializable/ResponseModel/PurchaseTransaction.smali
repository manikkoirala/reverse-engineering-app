.class public Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private id:Ljava/lang/String;

.field private isRenew:Z

.field private orderId:Ljava/lang/String;

.field private productId:Ljava/lang/String;

.field private purchaseToken:Ljava/lang/String;

.field private uid:Ljava/lang/String;

.field private updateTime:J

.field private vendor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->orderId:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->id:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->uid:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->productId:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->purchaseToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->vendor:Ljava/lang/String;

    iput-wide p7, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->updateTime:J

    iput-boolean p9, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->isRenew:Z

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->orderId:Ljava/lang/String;

    return-object v0
.end method

.method public getProductId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->productId:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->purchaseToken:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->uid:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->updateTime:J

    return-wide v0
.end method

.method public getVendor()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->vendor:Ljava/lang/String;

    return-object v0
.end method

.method public isRenew()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->isRenew:Z

    return v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->id:Ljava/lang/String;

    return-void
.end method

.method public setOrderId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->orderId:Ljava/lang/String;

    return-void
.end method

.method public setProductId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->productId:Ljava/lang/String;

    return-void
.end method

.method public setPurchaseToken(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->purchaseToken:Ljava/lang/String;

    return-void
.end method

.method public setRenew(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->isRenew:Z

    return-void
.end method

.method public setUid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->uid:Ljava/lang/String;

    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->updateTime:J

    return-void
.end method

.method public setVendor(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseTransaction;->vendor:Ljava/lang/String;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/serializable/SerialData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private id:I

.field private stream:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I[I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/SerialData;->id:I

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/SerialData;->stream:[I

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/SerialData;->id:I

    return v0
.end method

.method public getStream()[I
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/SerialData;->stream:[I

    return-object v0
.end method

.method public setId(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/serializable/SerialData;->id:I

    return-void
.end method

.method public setStream([I)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/SerialData;->stream:[I

    return-void
.end method

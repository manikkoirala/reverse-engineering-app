.class public Lcom/ekodroid/omrevaluator/serializable/AppConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field blockLowerVersion:Z

.field blockedVersion:I

.field latestVersion:I

.field syncMinutes:I

.field syncdate:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IZII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->syncdate:Ljava/lang/String;

    iput p2, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->latestVersion:I

    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->blockLowerVersion:Z

    iput p4, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->syncMinutes:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->blockedVersion:I

    return-void
.end method


# virtual methods
.method public getBlockedVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->blockedVersion:I

    return v0
.end method

.method public getLatestVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->latestVersion:I

    return v0
.end method

.method public getSyncdate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->syncdate:Ljava/lang/String;

    return-object v0
.end method

.method public isBlockLowerVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/serializable/AppConfig;->blockLowerVersion:Z

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/serializable/ResultData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private appVersion:I

.field private data:[[Ljava/lang/String;

.field private emailId:Ljava/lang/String;

.field private subjectName:Ljava/lang/String;

.field private userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->emailId:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->data:[[Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->subjectName:Ljava/lang/String;

    iput p4, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->appVersion:I

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->userName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAppVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->appVersion:I

    return v0
.end method

.method public getData()[[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->data:[[Ljava/lang/String;

    return-object v0
.end method

.method public getEmailId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->emailId:Ljava/lang/String;

    return-object v0
.end method

.method public getSubjectName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->subjectName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/ResultData;->userName:Ljava/lang/String;

    return-object v0
.end method

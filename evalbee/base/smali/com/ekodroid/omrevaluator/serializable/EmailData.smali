.class public Lcom/ekodroid/omrevaluator/serializable/EmailData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field country:Ljava/lang/String;

.field mailBody:Ljava/lang/String;

.field mailSubject:Ljava/lang/String;

.field userMailId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->userMailId:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->mailSubject:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->mailBody:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->country:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCountry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->country:Ljava/lang/String;

    return-object v0
.end method

.method public getMailBody()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->mailBody:Ljava/lang/String;

    return-object v0
.end method

.method public getMailSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->mailSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getUserMailId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->userMailId:Ljava/lang/String;

    return-object v0
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->country:Ljava/lang/String;

    return-void
.end method

.method public setMailBody(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->mailBody:Ljava/lang/String;

    return-void
.end method

.method public setMailSubject(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->mailSubject:Ljava/lang/String;

    return-void
.end method

.method public setUserMailId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/serializable/EmailData;->userMailId:Ljava/lang/String;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->X()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 5

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p2

    const v0, 0x7f0901c4

    if-ne p2, v0, :cond_2

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->h:Lq3;

    invoke-virtual {v0}, Lq3;->a()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v0, p2}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->B(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Ljava/util/ArrayList;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    return v2

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-virtual {v0}, Lv5;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {p2, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->C(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->h:Lq3;

    invoke-virtual {v0}, Lq3;->b()V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->C(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 0

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->f:Landroid/widget/ListView;

    invoke-virtual {p3}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result p3

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " Selected"

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->h:Lq3;

    invoke-virtual {p1, p2}, Lq3;->d(I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->h:Lq3;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.class public Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->C()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->j:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->h:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->m:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->e:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->k:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->g:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->l:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->j:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const v1, 0x7f08016e

    const v2, 0x7f0800bd

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    const v0, 0x7f1200bb

    invoke-static {p1, v0, v2, v1}, La91;->G(Landroid/content/Context;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    const v0, 0x7f1200bd

    invoke-static {p1, v0, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->m:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->m:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    const/4 v0, 0x1

    if-ge p1, v0, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    const v0, 0x7f12019a

    invoke-static {p1, v0, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_2
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->l:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_4

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->l:Ljava/lang/String;

    const-string v0, "[0-9]+"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->l:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x7

    if-gt p1, v0, :cond_4

    :cond_3
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    const v0, 0x7f12019b

    invoke-static {p1, v0, v2, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_4
    new-instance p1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->k:Ljava/lang/String;

    iget-object v6, v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->m:Ljava/lang/String;

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->A(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->j:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->A(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteStudent(ILjava/lang/String;)Z

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateStudent(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "StudentAdd"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/students/AddStudentActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

.field public d:Lcom/google/android/material/textfield/TextInputLayout;

.field public e:Lcom/google/android/material/textfield/TextInputLayout;

.field public f:Lcom/google/android/material/textfield/TextInputLayout;

.field public g:Lcom/google/android/material/textfield/TextInputLayout;

.field public h:Lcom/google/android/material/textfield/TextInputLayout;

.field public i:Landroid/widget/Button;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

.field public q:Landroidx/appcompat/widget/Toolbar;

.field public t:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->c:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    new-instance v0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;-><init>(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->t:Landroid/text/TextWatcher;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->n:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final B()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->f:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->q:Landroidx/appcompat/widget/Toolbar;

    const v1, 0x7f1200ae

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->i:Landroid/widget/Button;

    const v1, 0x7f120324

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->e:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->h:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->g:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final C()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->i:Landroid/widget/Button;

    new-instance v1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$c;-><init>(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final D()V
    .locals 2

    .line 1
    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$b;-><init>(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final E()V
    .locals 2

    .line 1
    const v0, 0x7f090437

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->q:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->q:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$d;-><init>(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c001d

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->E()V

    const v0, 0x7f09039c

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->d:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f09039d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->e:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f09039b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->g:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f090395

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->h:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f090394

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->f:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f09009e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->i:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const-string v0, "CLASS_NAME"

    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->n:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "STUDENT_MODEL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->p:Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->e:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->t:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->t:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->D()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->C()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->B()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "q"
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$h;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->E(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudents(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    const/4 p1, 0x0

    return-object p1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->e:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->F(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->e:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

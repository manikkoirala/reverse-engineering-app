.class public Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Ljava/util/ArrayList;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->c:Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->g:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->h:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->f:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->f:Ljava/util/ArrayList;

    return-object p1
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->I()V

    return-void
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->H()V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->O(Z)V

    return-void
.end method


# virtual methods
.method public final H()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "Choose a file"

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final I()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->f:Ljava/util/ArrayList;

    const v1, 0x7f0900c6

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->g:Ljava/lang/String;

    const/16 v1, 0x8

    const v3, 0x7f0901fe

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->h:Ljava/lang/String;

    const v3, 0x7f0901fb

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v3}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public final J()V
    .locals 2

    .line 1
    const v0, 0x7f0900b0

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$e;-><init>(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final K()V
    .locals 2

    .line 1
    const v0, 0x7f0900c2

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$b;-><init>(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final L()V
    .locals 2

    .line 1
    const v0, 0x7f0900c6

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$d;-><init>(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final M()V
    .locals 2

    .line 1
    const v0, 0x7f09021f

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$c;-><init>(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final N()V
    .locals 2

    .line 1
    const v0, 0x7f090444

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$f;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$f;-><init>(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final O(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->c:Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->c:Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->h(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method public final P(Ljava/io/InputStream;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->h:Ljava/lang/String;

    new-instance v0, Lho;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->c:Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

    new-instance v2, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$a;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity$a;-><init>(Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;)V

    invoke-direct {v0, v1, p1, v2}, Lho;-><init>(Landroid/content/Context;Ljava/io/InputStream;Ly01;)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->P(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c002f

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->N()V

    const p1, 0x7f0903d3

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->e:Landroid/widget/TextView;

    const p1, 0x7f0903d4

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->J()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->L()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->M()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->K()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;->I()V

    return-void
.end method

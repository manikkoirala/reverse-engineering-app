.class public Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/students/AddStudentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/AddStudentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->j:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->e:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->k:Ljava/lang/String;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->j:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->i:Landroid/widget/Button;

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/AddStudentActivity$a;->a:Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;->i:Landroid/widget/Button;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

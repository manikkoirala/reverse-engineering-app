.class public Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SyncStudentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->f(Z)V

    return-void
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->e(Z)V

    return-void
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic d(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->g(Z)V

    return-void
.end method

.method public static h(Landroid/content/Context;Z)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTION_SYNC_STUDENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "force_sync"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public final e(Z)V
    .locals 3

    .line 1
    new-instance v0, Lwa0;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    new-instance v2, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;-><init>(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V

    invoke-direct {v0, v1, v2}, Lwa0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final f(Z)V
    .locals 8

    .line 1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, La91;->L(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteAllStudent()Z

    :cond_0
    invoke-static {p0}, La91;->z(Landroid/content/Context;)J

    move-result-wide v3

    new-instance v1, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    const/4 v5, 0x0

    const/16 v6, 0xc8

    new-instance v7, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$b;

    invoke-direct {v7, p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$b;-><init>(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;-><init>(Landroid/content/Context;JIILzg;)V

    return-void
.end method

.method public final g(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getAllNonSyncStudents()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->getStudentDto(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/students/PostPendingStudents;

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/students/model/BulkStudentRequest;

    invoke-direct {v2, v1}, Lcom/ekodroid/omrevaluator/clients/students/model/BulkStudentRequest;-><init>(Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->a:Landroid/content/Context;

    new-instance v3, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$a;

    invoke-direct {v3, p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$a;-><init>(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V

    invoke-direct {v0, v2, v1, v3}, Lcom/ekodroid/omrevaluator/clients/students/PostPendingStudents;-><init>(Lcom/ekodroid/omrevaluator/clients/students/model/BulkStudentRequest;Landroid/content/Context;Lzg;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->f(Z)V

    :goto_0
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-static {p0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ACTION_SYNC_STUDENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "force_sync"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->g(Z)V

    :cond_0
    return-void
.end method

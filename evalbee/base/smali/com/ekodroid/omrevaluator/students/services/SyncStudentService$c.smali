.class public Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->e(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->b:Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_3

    instance-of p1, p3, Ljava/lang/Long;

    if-eqz p1, :cond_3

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->b:Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;

    invoke-static {p3}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->c(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;)Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getAllStudents()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    int-to-long v0, p3

    cmp-long p1, v0, p1

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->a:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->b:Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;

    invoke-static {p1, p2}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->d(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;Z)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->b:Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->c(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;)Landroid/content/Context;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, La91;->L(Landroid/content/Context;J)V

    :cond_2
    :goto_1
    new-instance p1, Landroid/content/Intent;

    const-string p3, "UPDATE_STUDENT_LIST"

    invoke-direct {p1, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p3, "data_changed"

    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService$c;->b:Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->c(Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_3
    return-void
.end method

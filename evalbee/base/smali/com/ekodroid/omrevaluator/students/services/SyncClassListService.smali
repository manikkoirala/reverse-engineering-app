.class public Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SyncClassListService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->a:Landroid/content/Context;

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static c(Landroid/content/Context;Z)V
    .locals 1

    .line 1
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "ACTION_SYNC_CLASS_LIST"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .line 1
    new-instance v0, Lys1;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->a:Landroid/content/Context;

    new-instance v2, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService$a;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService$a;-><init>(Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;)V

    invoke-direct {v0, v1, v2}, Lys1;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-static {p0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ACTION_SYNC_CLASS_LIST"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->b()V

    :cond_0
    return-void
.end method

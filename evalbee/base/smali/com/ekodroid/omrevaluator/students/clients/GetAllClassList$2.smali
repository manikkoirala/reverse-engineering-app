.class public Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/d$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;->d(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;->a:Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    new-instance v2, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2$1;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2$1;-><init>(Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;)V

    invoke-virtual {v2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lgc0;->k(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;->a:Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;->c(Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteAllSyncedClass()Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/students/clients/models/ClassDto;

    new-instance v5, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/students/clients/models/ClassDto;->className:Ljava/lang/String;

    invoke-direct {v5, v3, v0, v4}, Lcom/ekodroid/omrevaluator/database/ClassDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v5}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateClass(Lcom/ekodroid/omrevaluator/database/ClassDataModel;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;->a:Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;

    const/16 v2, 0xc8

    invoke-static {v1, v4, v2, p1}, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;->b(Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;ZILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList$2;->a:Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;

    const/4 v1, 0x0

    const/16 v2, 0x190

    invoke-static {p1, v1, v2, v0}, Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;->b(Lcom/ekodroid/omrevaluator/students/clients/GetAllClassList;ZILjava/lang/Object;)V

    :goto_1
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;
.super Lv5;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;
    }
.end annotation


# instance fields
.field public c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

.field public d:Landroid/widget/TextView;

.field public e:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public f:Landroid/widget/ListView;

.field public g:Ljava/util/ArrayList;

.field public h:Lq3;

.field public i:Ljava/lang/String;

.field public j:Landroid/view/ActionMode;

.field public k:Landroidx/appcompat/widget/SwitchCompat;

.field public l:Landroid/content/BroadcastReceiver;

.field public m:Z

.field public n:Landroidx/appcompat/widget/Toolbar;

.field public p:Lvr1;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->m:Z

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g0(Z)V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->N(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->j:Landroid/view/ActionMode;

    return-object p1
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->O(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->i:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->W()V

    return-void
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->d0()V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->U()V

    return-void
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->R()V

    return-void
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->Q()V

    return-void
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->e0()V

    return-void
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/database/StudentDataModel;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->f0(Lcom/ekodroid/omrevaluator/database/StudentDataModel;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic M(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->P(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V

    return-void
.end method


# virtual methods
.method public final N(Ljava/util/ArrayList;)V
    .locals 8

    .line 1
    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const v2, 0x7f1200a1

    const v3, 0x7f12009f

    const v4, 0x7f120097

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800e1

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final O(Ljava/util/ArrayList;)V
    .locals 5

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, "Please wait, deleting students..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Lks;

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/students/model/BulkStudentRequest;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->getStudentDto(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/ekodroid/omrevaluator/clients/students/model/BulkStudentRequest;-><init>(Ljava/util/ArrayList;)V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    new-instance v4, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;

    invoke-direct {v4, p0, v0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Landroid/app/ProgressDialog;Ljava/util/ArrayList;)V

    invoke-direct {v1, v2, v3, v4}, Lks;-><init>(Lcom/ekodroid/omrevaluator/clients/students/model/BulkStudentRequest;Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final P(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CLASS_NAME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "STUDENT_MODEL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final Q()V
    .locals 4

    .line 1
    new-instance v0, Lgo;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    new-instance v3, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$f;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$f;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-direct {v0, v1, v2, v3}, Lgo;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ly01;)V

    return-void
.end method

.method public final R()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/students/ImportStudentsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "CLASS_NAME"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final S()V
    .locals 2

    .line 1
    const v0, 0x7f09019b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$n;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$n;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final T()V
    .locals 1

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$m;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$m;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->l:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public final U()V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->m:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v0}, La91;->z(Landroid/content/Context;)J

    move-result-wide v4

    const-wide/32 v6, 0x2bf20

    add-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g0(Z)V

    :cond_0
    new-instance v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$q;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$h;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public final V()V
    .locals 7

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->p:Lvr1;

    const v2, 0x7f0901de

    if-eqz v1, :cond_2

    iget-boolean v0, v1, Lvr1;->b:Z

    iget-object v1, v1, Lvr1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f090420

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->p:Lvr1;

    iget-object v4, v4, Lvr1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    sget-object v5, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f120239

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f12029b

    :goto_0
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->p:Lvr1;

    iget-boolean v4, v4, Lvr1;->b:Z

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f120042

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f1200a3

    :goto_1
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x1

    :goto_2
    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    new-instance v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;

    invoke-direct {v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v3, v1}, Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList;->c(Lcom/ekodroid/omrevaluator/util/DataModels/SortStudentList$SortBy;Ljava/util/ArrayList;Z)V

    :cond_3
    return-void
.end method

.method public final W()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->n:Landroidx/appcompat/widget/Toolbar;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f120305

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v2

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getNumberOfStudentForAClass(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->V()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    const/4 v1, 0x4

    const v2, 0x7f090371

    const/4 v3, 0x0

    const v4, 0x7f0901ec

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lq3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {v0, v1, v2}, Lq3;-><init>(Ljava/util/ArrayList;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->h:Lq3;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v4}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final X()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->f:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$c;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    return-void
.end method

.method public final Y()V
    .locals 2

    .line 1
    const v0, 0x7f0900b4

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$i;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$i;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09019a

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$j;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$j;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final Z()V
    .locals 2

    .line 1
    const v0, 0x7f09024e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public final a0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$o;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$o;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public final b0()V
    .locals 2

    .line 1
    const v0, 0x7f09009e

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$k;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$k;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09009d

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$l;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$l;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final c0()V
    .locals 2

    .line 1
    const v0, 0x7f09043b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->n:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p0, v0}, Lv5;->x(Landroidx/appcompat/widget/Toolbar;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->n:Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$g;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$g;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final d0()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/students/AddStudentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "CLASS_NAME"

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final e0()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    new-instance v3, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$p;

    invoke-direct {v3, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$p;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const v4, 0x7f120061

    const v5, 0x7f12017e

    const v6, 0x7f120050

    const v7, 0x7f120053

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final f0(Lcom/ekodroid/omrevaluator/database/StudentDataModel;Landroid/view/View;)V
    .locals 2

    .line 1
    new-instance v0, Lb61;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {v0, v1, p2}, Lb61;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const p2, 0x7f0e0009

    invoke-virtual {v0, p2}, Lb61;->b(I)V

    const/4 p2, 0x1

    invoke-virtual {v0, p2}, Lb61;->c(Z)V

    new-instance p2, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$b;

    invoke-direct {p2, p0, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$b;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V

    invoke-virtual {v0, p2}, Lb61;->d(Lb61$c;)V

    invoke-virtual {v0}, Lb61;->e()V

    return-void
.end method

.method public final g0(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v0, p1}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->h(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0023

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c0()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const v0, 0x7f090375

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->k:Landroidx/appcompat/widget/SwitchCompat;

    const v0, 0x7f09041b

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const-string v0, "CLASS_NAME"

    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->i:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->Z()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->Y()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->X()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->T()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->S()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->a0()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->b0()V

    const p1, 0x7f090371

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->e:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Lv5;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->e:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$h;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$h;-><init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->l:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UPDATE_STUDENT_LIST"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->m:Z

    if-eqz v0, :cond_0

    const v0, 0x7f090227

    invoke-virtual {p0, v0}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->U()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->Z()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p3

    int-to-long p3, p3

    const-wide/32 v0, 0x7f09019c    # 1.053000465E-314

    cmp-long p3, p3, v0

    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {p3, p1, p2}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->L(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/database/StudentDataModel;Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const p3, 0x7f08008c

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object p2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$a;->a:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {p2, p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->M(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V

    :goto_0
    return-void
.end method

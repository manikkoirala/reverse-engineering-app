.class public Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->O(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Ljava/util/ArrayList;

.field public final synthetic c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Landroid/app/ProgressDialog;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    iget-object p3, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p3, p3, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {p3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object p3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getClassName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, v0, p2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteStudent(ILjava/lang/String;)Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->H(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$e;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    const p2, 0x7f0800bd

    const p3, 0x7f08016e

    const-string v0, "Delete failed, please try again"

    invoke-static {p1, v0, p2, p3}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :goto_1
    return-void
.end method

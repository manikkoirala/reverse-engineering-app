.class public Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->N(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;->b:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;->b:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-boolean v0, p1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;->a:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->D(Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity$d;->b:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;->c:Lcom/ekodroid/omrevaluator/students/ClassDetailActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteStudent(ILjava/lang/String;)Z

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public reportsCount:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;->reportsCount:J

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "report_storage_profile"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;

    invoke-virtual {v0, p0, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;

    return-object p0
.end method


# virtual methods
.method public getReportsCount()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;->reportsCount:J

    return-wide v0
.end method

.method public save(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p0}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "report_storage_profile"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setReportsCount(J)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/ReportStorageDto;->reportsCount:J

    return-void
.end method

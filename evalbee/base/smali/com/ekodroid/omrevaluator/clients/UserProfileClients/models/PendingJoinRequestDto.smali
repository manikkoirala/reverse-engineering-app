.class public Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field memberStatus:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

.field pendingJoinRequest:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;->pendingJoinRequest:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;->memberStatus:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-void
.end method


# virtual methods
.method public getMemberStatus()Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;->memberStatus:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    return-object v0
.end method

.method public getPendingJoinRequest()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;->pendingJoinRequest:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    return-object v0
.end method

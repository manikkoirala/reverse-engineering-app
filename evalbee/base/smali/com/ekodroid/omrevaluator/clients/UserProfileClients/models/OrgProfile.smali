.class public Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field displayName:Ljava/lang/String;

.field orgId:Ljava/lang/String;

.field organization:Ljava/lang/String;

.field request:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

.field role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

.field teacherList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;",
            "Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->displayName:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->organization:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->orgId:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->request:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->teacherList:Ljava/util/ArrayList;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "org_profile"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    invoke-virtual {v0, p0, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    return-object p0
.end method

.method public static reset(Landroid/content/Context;)V
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "org_profile"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lok;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getOrgId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->orgId:Ljava/lang/String;

    return-object v0
.end method

.method public getOrganization()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->organization:Ljava/lang/String;

    return-object v0
.end method

.method public getRequest()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->request:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    return-object v0
.end method

.method public getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-object v0
.end method

.method public getTeacherList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->teacherList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public save(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p0}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "org_profile"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setOrgId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->orgId:Ljava/lang/String;

    return-void
.end method

.method public setOrganization(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->organization:Ljava/lang/String;

    return-void
.end method

.method public setRequest(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->request:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    return-void
.end method

.method public setRole(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-void
.end method

.method public setTeacherList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->teacherList:Ljava/util/ArrayList;

    return-void
.end method

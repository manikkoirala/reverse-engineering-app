.class public final enum Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

.field public static final enum ADMIN:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

.field public static final enum OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

.field public static final enum TEACHER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
    .locals 3

    sget-object v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->ADMIN:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    sget-object v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->TEACHER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    filled-new-array {v0, v1, v2}, [Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    const-string v1, "OWNER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    const-string v1, "ADMIN"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->ADMIN:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    const-string v1, "TEACHER"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->TEACHER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    invoke-static {}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->$values()[Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->$VALUES:[Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->$VALUES:[Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-object v0
.end method

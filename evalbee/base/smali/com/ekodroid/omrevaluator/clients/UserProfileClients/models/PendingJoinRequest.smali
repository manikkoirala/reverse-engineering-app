.class public Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field orgName:Ljava/lang/String;

.field role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

.field senderUserId:Ljava/lang/String;

.field sentByEmail:Ljava/lang/String;

.field sentByName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->sentByName:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->senderUserId:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->sentByEmail:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->orgName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getOrgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->orgName:Ljava/lang/String;

    return-object v0
.end method

.method public getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->role:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    return-object v0
.end method

.method public getSenderUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->senderUserId:Ljava/lang/String;

    return-object v0
.end method

.method public getSentByEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->sentByEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getSentByName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->sentByName:Ljava/lang/String;

    return-object v0
.end method

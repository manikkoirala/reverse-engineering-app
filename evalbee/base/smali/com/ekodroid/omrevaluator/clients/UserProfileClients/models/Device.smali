.class public Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field deviceId:Ljava/lang/String;

.field fcmId:Ljava/lang/String;

.field info:Ljava/lang/String;

.field lastUpdate:J

.field platform:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->deviceId:Ljava/lang/String;

    iput-wide p2, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->lastUpdate:J

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->platform:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->info:Ljava/lang/String;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->fcmId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getFcmId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->fcmId:Ljava/lang/String;

    return-object v0
.end method

.method public getInfo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->info:Ljava/lang/String;

    return-object v0
.end method

.method public getLastUpdate()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->lastUpdate:J

    return-wide v0
.end method

.method public getPlatform()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->platform:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->deviceId:Ljava/lang/String;

    return-void
.end method

.method public setFcmId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->fcmId:Ljava/lang/String;

    return-void
.end method

.method public setInfo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->info:Ljava/lang/String;

    return-void
.end method

.method public setLastUpdate(J)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->lastUpdate:J

    return-void
.end method

.method public setPlatform(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;->platform:Ljava/lang/String;

    return-void
.end method

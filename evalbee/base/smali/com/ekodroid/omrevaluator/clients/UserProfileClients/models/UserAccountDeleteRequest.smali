.class public Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field deleted:Z

.field email:Ljava/lang/String;

.field requestedOn:J

.field uid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->uid:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->email:Ljava/lang/String;

    int-to-long p1, p3

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->requestedOn:J

    iput-boolean p4, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->deleted:Z

    return-void
.end method


# virtual methods
.method public getEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestedOn()J
    .locals 2

    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->requestedOn:J

    return-wide v0
.end method

.method public getUid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->uid:Ljava/lang/String;

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->deleted:Z

    return v0
.end method

.method public setDeleted(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->deleted:Z

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->email:Ljava/lang/String;

    return-void
.end method

.method public setRequestedOn(J)V
    .locals 0

    iput-wide p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->requestedOn:J

    return-void
.end method

.method public setUid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserAccountDeleteRequest;->uid:Ljava/lang/String;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private content:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "TT;>;"
        }
    .end annotation
.end field

.field private empty:Z

.field private pageable:Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;

.field private size:I

.field private totalElements:I

.field private totalPages:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->content:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPageable()Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->pageable:Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->size:I

    return v0
.end method

.method public getTotalElements()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->totalElements:I

    return v0
.end method

.method public getTotalPages()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->totalPages:I

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->empty:Z

    return v0
.end method

.method public setContent(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->content:Ljava/util/ArrayList;

    return-void
.end method

.method public setEmpty(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->empty:Z

    return-void
.end method

.method public setPageable(Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->pageable:Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;

    return-void
.end method

.method public setSize(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->size:I

    return-void
.end method

.method public setTotalElements(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->totalElements:I

    return-void
.end method

.method public setTotalPages(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->totalPages:I

    return-void
.end method

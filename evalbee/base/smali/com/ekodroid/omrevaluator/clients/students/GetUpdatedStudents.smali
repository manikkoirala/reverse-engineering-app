.class public Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lee1;

.field public b:Lzg;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:I

.field public f:I

.field public g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;JIILzg;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->g:Landroid/content/Context;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->b:Lzg;

    iput-wide p2, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->d:J

    iput p4, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->e:I

    iput p5, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->f:I

    new-instance p6, Ln52;

    invoke-static {}, La91;->x()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p6, p1, v0}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p6}, Ln52;->b()Lee1;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->a:Lee1;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "https://"

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->x()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p6, ":"

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->q()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p6, "/api-online/students/sync"

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p1

    new-instance p6, Ljava/lang/StringBuilder;

    invoke-direct {p6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, ""

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    const-string p6, "pageNo"

    invoke-virtual {p1, p6, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    const-string p5, "pageSize"

    invoke-virtual {p1, p5, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "after"

    invoke-virtual {p1, p3, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->i()V

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->g(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->h(ZILjava/lang/Object;)V

    return-void
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->g:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic d(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->d:J

    return-wide v0
.end method

.method public static synthetic e(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->f:I

    return p0
.end method

.method public static synthetic f(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)Lzg;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->b:Lzg;

    return-object p0
.end method


# virtual methods
.method public final g(Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v7, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$c;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->c:Ljava/lang/String;

    new-instance v4, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    invoke-direct {v4, p0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;-><init>(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)V

    new-instance v5, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$b;

    invoke-direct {v5, p0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$b;-><init>(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)V

    move-object v0, v7

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$c;-><init>(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;Ljava/lang/String;)V

    new-instance p1, Lwq;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v2, 0x4e20

    invoke-direct {p1, v2, v0, v1}, Lwq;-><init>(IIF)V

    invoke-virtual {v7, p1}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->a:Lee1;

    invoke-virtual {p1}, Lee1;->d()Lcom/android/volley/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/android/volley/a;->clear()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->a:Lee1;

    invoke-virtual {p1, v7}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final h(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->b:Lzg;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lzg;->a(ZILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lr30;->i(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$a;-><init>(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

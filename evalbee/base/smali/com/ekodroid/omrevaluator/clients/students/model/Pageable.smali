.class public Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private offset:I

.field private pageNumber:I

.field private pageSize:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOffset()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;->offset:I

    return v0
.end method

.method public getPageNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;->pageNumber:I

    return v0
.end method

.method public getPageSize()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;->pageSize:I

    return v0
.end method

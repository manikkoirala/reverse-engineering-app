.class public Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->b(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$a;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$a;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$a;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->a:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->c(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$a;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->getContent()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->getRollNumber()I

    move-result v3

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->deleteStudent(ILjava/lang/String;)Z

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->toDataModel()Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateStudentAsSynced(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

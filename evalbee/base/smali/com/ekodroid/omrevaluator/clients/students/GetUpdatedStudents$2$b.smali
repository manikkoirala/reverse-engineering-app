.class public Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->b(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->getPageable()Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;->getPageNumber()I

    move-result v0

    add-int/2addr v0, v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->getTotalPages()I

    move-result v2

    if-ge v0, v2, :cond_0

    new-instance v3, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->a:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->c(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)Landroid/content/Context;

    move-result-object v4

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->a:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->d(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)J

    move-result-wide v5

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;->getPageable()Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/students/model/Pageable;->getPageNumber()I

    move-result v0

    add-int/lit8 v7, v0, 0x1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->a:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->e(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)I

    move-result v8

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->a:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->f(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;)Lzg;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;-><init>(Landroid/content/Context;JIILzg;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->b:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;

    iget-object v0, v0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2;->a:Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;

    const/16 v2, 0xc8

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents$2$b;->a:Lcom/ekodroid/omrevaluator/clients/students/model/PageResponse;

    invoke-static {v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;->b(Lcom/ekodroid/omrevaluator/clients/students/GetUpdatedStudents;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

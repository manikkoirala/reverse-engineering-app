.class public Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public className:Ljava/lang/String;

.field public emailId:Ljava/lang/String;

.field public phoneNo:Ljava/lang/String;

.field public rollNumber:I

.field public studentName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->rollNumber:I

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->studentName:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->className:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->phoneNo:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->emailId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->rollNumber:I

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->studentName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->className:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->phoneNo:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->emailId:Ljava/lang/String;

    return-void
.end method

.method public static getStudentDto(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/database/StudentDataModel;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-direct {v2, v3}, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;-><init>(Lcom/ekodroid/omrevaluator/database/StudentDataModel;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->emailId:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->phoneNo:Ljava/lang/String;

    return-object v0
.end method

.method public getRollNumber()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->rollNumber:I

    return v0
.end method

.method public getStudentName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->studentName:Ljava/lang/String;

    return-object v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->className:Ljava/lang/String;

    return-void
.end method

.method public setEmailId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->emailId:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->phoneNo:Ljava/lang/String;

    return-void
.end method

.method public setRollNumber(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->rollNumber:I

    return-void
.end method

.method public setStudentName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->studentName:Ljava/lang/String;

    return-void
.end method

.method public toDataModel()Lcom/ekodroid/omrevaluator/database/StudentDataModel;
    .locals 7

    new-instance v6, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->rollNumber:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->studentName:Ljava/lang/String;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->emailId:Ljava/lang/String;

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->phoneNo:Ljava/lang/String;

    iget-object v5, p0, Lcom/ekodroid/omrevaluator/clients/students/model/StudentDto;->className:Ljava/lang/String;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

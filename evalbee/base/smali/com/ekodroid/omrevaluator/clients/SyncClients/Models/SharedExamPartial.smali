.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field examId:Ljava/lang/Long;

.field sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field syncImages:Z

.field teachers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExamId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->examId:Ljava/lang/Long;

    return-object v0
.end method

.method public getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object v0
.end method

.method public getTeachers()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->teachers:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isSyncImages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->syncImages:Z

    return v0
.end method

.method public setExamId(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->examId:Ljava/lang/Long;

    return-void
.end method

.method public setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-void
.end method

.method public setSyncImages(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->syncImages:Z

    return-void
.end method

.method public setTeachers(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->teachers:Ljava/util/ArrayList;

    return-void
.end method

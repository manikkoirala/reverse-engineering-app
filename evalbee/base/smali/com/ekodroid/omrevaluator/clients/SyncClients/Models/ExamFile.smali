.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field expiresIn:Ljava/lang/Long;

.field sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field sheetTemplate:Ljava/lang/String;

.field syncImages:Z

.field teachers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;"
        }
    .end annotation
.end field

.field templateVersion:I


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;ILjava/lang/String;Ljava/lang/Long;Lcom/ekodroid/omrevaluator/serializable/SharedType;Ljava/util/ArrayList;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/ekodroid/omrevaluator/templateui/models/ExamId;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/ekodroid/omrevaluator/serializable/SharedType;",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/more/models/Teacher;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput p2, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->templateVersion:I

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->sheetTemplate:Ljava/lang/String;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->expiresIn:Ljava/lang/Long;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->teachers:Ljava/util/ArrayList;

    iput-boolean p7, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamFile;->syncImages:Z

    return-void
.end method

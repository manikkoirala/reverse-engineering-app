.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/d$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->e(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;

    invoke-virtual {v0, p1, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->c(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->d(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->getExamId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJsonForCloudId(Ljava/lang/Long;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->d(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    :cond_0
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;

    const/4 v1, 0x1

    const/16 v2, 0xc8

    invoke-static {v0, v1, v2, p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->b(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;ZILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;->a:Lcom/ekodroid/omrevaluator/clients/SyncClients/a;

    const/16 v0, 0x190

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->b(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

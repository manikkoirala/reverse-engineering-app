.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ResponseCreateExam"
.end annotation


# instance fields
.field id:Ljava/lang/Long;

.field sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field syncImages:Z

.field final synthetic this$0:Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam;

.field updatedOn:Ljava/lang/Long;

.field userId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->this$0:Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object v0
.end method

.method public getUpdatedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->updatedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public isSyncImages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/CreateSyncExam$ResponseCreateExam;->syncImages:Z

    return v0
.end method

.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private ccEmail:Z

.field private emailId:Ljava/lang/String;

.field private includeRank:Z

.field private publishByEmail:Ljava/lang/String;

.field private publishByName:Ljava/lang/String;

.field private rollNo:I

.field private studentName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->studentName:Ljava/lang/String;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->emailId:Ljava/lang/String;

    iput p3, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->rollNo:I

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->publishByEmail:Ljava/lang/String;

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->publishByName:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->ccEmail:Z

    iput-boolean p7, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;->includeRank:Z

    return-void
.end method

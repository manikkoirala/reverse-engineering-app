.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field id:Ljava/lang/Long;

.field published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

.field sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field syncImages:Z

.field template:Ljava/lang/String;

.field templateVersion:I

.field updatedOn:Ljava/lang/Long;

.field userId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;Lcom/ekodroid/omrevaluator/serializable/SharedType;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->id:Ljava/lang/Long;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->updatedOn:Ljava/lang/Long;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->template:Ljava/lang/String;

    iput p5, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->templateVersion:I

    iput-object p6, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    iput-object p7, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iput-object p8, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->userId:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->syncImages:Z

    return-void
.end method


# virtual methods
.method public getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->examId:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getPublished()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-object v0
.end method

.method public getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->template:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplateVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->templateVersion:I

    return v0
.end method

.method public getUpdatedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->updatedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public isSyncImages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->syncImages:Z

    return v0
.end method

.method public setId(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->id:Ljava/lang/Long;

    return-void
.end method

.method public setPublished(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-void
.end method

.method public setSyncImages(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->syncImages:Z

    return-void
.end method

.method public setTemplate(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->template:Ljava/lang/String;

    return-void
.end method

.method public setTemplateVersion(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->templateVersion:I

    return-void
.end method

.method public setUpdatedOn(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->updatedOn:Ljava/lang/Long;

    return-void
.end method

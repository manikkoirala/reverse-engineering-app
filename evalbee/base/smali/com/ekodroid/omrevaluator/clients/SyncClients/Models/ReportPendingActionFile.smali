.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field reportPendingActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;->reportPendingActions:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getReportPendingActions()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;->reportPendingActions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setReportPendingActions(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportAction;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ReportPendingActionFile;->reportPendingActions:Ljava/util/ArrayList;

    return-void
.end method

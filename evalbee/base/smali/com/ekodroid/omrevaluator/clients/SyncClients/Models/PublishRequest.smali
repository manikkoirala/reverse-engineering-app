.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public cloudId:Ljava/lang/Long;

.field public examData:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

.field public expiresIn:Ljava/lang/Long;

.field public publishMetaDataDtos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;",
            ">;"
        }
    .end annotation
.end field

.field public studentResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

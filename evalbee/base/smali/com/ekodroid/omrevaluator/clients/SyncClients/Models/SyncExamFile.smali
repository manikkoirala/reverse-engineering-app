.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field exam:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

.field examId:Ljava/lang/Long;

.field rollSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field studentResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;",
            ">;"
        }
    .end annotation
.end field

.field syncedOn:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExam()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->exam:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    return-object v0
.end method

.method public getExamId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->examId:Ljava/lang/Long;

    return-object v0
.end method

.method public getRollSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->rollSet:Ljava/util/Set;

    return-object v0
.end method

.method public getStudentResults()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->studentResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSyncedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->syncedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public setExam(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->exam:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    return-void
.end method

.method public setExamId(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->examId:Ljava/lang/Long;

    return-void
.end method

.method public setRollSet(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->rollSet:Ljava/util/Set;

    return-void
.end method

.method public setStudentResults(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->studentResults:Ljava/util/ArrayList;

    return-void
.end method

.method public setSyncedOn(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamFile;->syncedOn:Ljava/lang/Long;

    return-void
.end method

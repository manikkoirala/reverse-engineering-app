.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lee1;

.field public b:Lzg;

.field public c:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

.field public d:Landroid/content/Context;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;Landroid/content/Context;Lzg;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/api/sync/shared-exam"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->b:Lzg;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->c:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->d:Landroid/content/Context;

    new-instance p1, Ln52;

    invoke-static {}, La91;->v()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1}, Ln52;->b()Lee1;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->a:Lee1;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->g()V

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->e(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->f(ZILjava/lang/Object;)V

    return-void
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->d:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic d(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->c:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

    return-object p0
.end method


# virtual methods
.method public final e(Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v7, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$d;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->e:Ljava/lang/String;

    new-instance v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;

    invoke-direct {v4, p0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$b;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)V

    new-instance v5, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$c;

    invoke-direct {v5, p0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$c;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)V

    move-object v0, v7

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$d;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;Ljava/lang/String;)V

    new-instance p1, Lwq;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v2, 0x3a98

    invoke-direct {p1, v2, v0, v1}, Lwq;-><init>(IIF)V

    invoke-virtual {v7, p1}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->a:Lee1;

    invoke-virtual {p1, v7}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final f(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/a;->b:Lzg;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lzg;->a(ZILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lr30;->i(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/a$a;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

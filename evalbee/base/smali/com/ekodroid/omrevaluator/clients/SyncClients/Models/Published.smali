.class public final enum Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

.field public static final enum NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

.field public static final enum PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

.field public static final enum PUBLISHING:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;


# direct methods
.method private static synthetic $values()[Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;
    .locals 3

    sget-object v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHING:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    sget-object v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    filled-new-array {v0, v1, v2}, [Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    move-result-object v0

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    const-string v1, "PUBLISHED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    const-string v1, "PUBLISHING"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHING:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    new-instance v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    const-string v1, "NOT_PUBLISHED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    invoke-static {}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->$values()[Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    move-result-object v0

    sput-object v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->$VALUES:[Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;
    .locals 1

    const-class v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-object p0
.end method

.method public static values()[Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;
    .locals 1

    sget-object v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->$VALUES:[Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    invoke-virtual {v0}, [Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-object v0
.end method

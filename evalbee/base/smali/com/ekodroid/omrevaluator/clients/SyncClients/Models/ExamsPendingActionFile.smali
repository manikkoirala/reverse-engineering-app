.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field examPendingActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;->examPendingActions:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getExamPendingActions()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;->examPendingActions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setExamPendingActions(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;->examPendingActions:Ljava/util/ArrayList;

    return-void
.end method

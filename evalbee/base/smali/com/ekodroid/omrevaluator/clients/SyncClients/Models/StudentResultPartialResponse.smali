.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

.field report:Ljava/lang/String;

.field reportVersion:I

.field rollNo:I

.field updatedOn:Ljava/lang/Long;


# direct methods
.method public constructor <init>(ILjava/lang/Long;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->rollNo:I

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->updatedOn:Ljava/lang/Long;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->report:Ljava/lang/String;

    iput p4, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->reportVersion:I

    iput-object p5, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-void
.end method


# virtual methods
.method public getPublished()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-object v0
.end method

.method public getReport()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->report:Ljava/lang/String;

    return-object v0
.end method

.method public getReportVersion()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->reportVersion:I

    return v0
.end method

.method public getRollNo()I
    .locals 1

    iget v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->rollNo:I

    return v0
.end method

.method public getUpdatedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->updatedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public setPublished(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->published:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    return-void
.end method

.method public setReport(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->report:Ljava/lang/String;

    return-void
.end method

.method public setReportVersion(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->reportVersion:I

    return-void
.end method

.method public setRollNo(I)V
    .locals 0

    iput p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->rollNo:I

    return-void
.end method

.method public setUpdatedOn(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;->updatedOn:Ljava/lang/Long;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field examIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field exams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;",
            ">;"
        }
    .end annotation
.end field

.field lastSyncedOn:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExamIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->examIds:Ljava/util/Set;

    return-object v0
.end method

.method public getExams()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->exams:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLastSyncedOn()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->lastSyncedOn:Ljava/lang/Long;

    return-object v0
.end method

.method public setExamIds(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->examIds:Ljava/util/Set;

    return-void
.end method

.method public setExams(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->exams:Ljava/util/ArrayList;

    return-void
.end method

.method public setLastSyncedOn(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->lastSyncedOn:Ljava/lang/Long;

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;
.super Lv5;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/material/navigation/NavigationBarView$OnItemSelectedListener;


# instance fields
.field public c:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

.field public d:Lf2;

.field public e:Lwg;

.field public f:Lzw0;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv5;-><init>()V

    new-instance v0, Lf2;

    invoke-direct {v0}, Lf2;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->d:Lf2;

    new-instance v0, Lwg;

    invoke-direct {v0}, Lwg;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->e:Lwg;

    new-instance v0, Lzw0;

    invoke-direct {v0}, Lzw0;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->f:Lzw0;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c002e

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    const p1, 0x7f09007a

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->c:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    invoke-virtual {p1, p0}, Lcom/google/android/material/navigation/NavigationBarView;->setOnItemSelectedListener(Lcom/google/android/material/navigation/NavigationBarView$OnItemSelectedListener;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->c:Lcom/google/android/material/bottomnavigation/BottomNavigationView;

    const v0, 0x7f090291

    invoke-virtual {p1, v0}, Lcom/google/android/material/navigation/NavigationBarView;->setSelectedItemId(I)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Ly3;->c(Landroid/content/Context;)V

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const/4 v0, 0x1

    const v1, 0x7f090134

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    :pswitch_0
    invoke-virtual {p0}, Landroidx/fragment/app/e;->getSupportFragmentManager()Landroidx/fragment/app/k;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/k;->m()Landroidx/fragment/app/q;

    move-result-object p1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->e:Lwg;

    :goto_0
    invoke-virtual {p1, v1, v2}, Landroidx/fragment/app/q;->o(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/q;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/q;->g()I

    return v0

    :pswitch_1
    invoke-virtual {p0}, Landroidx/fragment/app/e;->getSupportFragmentManager()Landroidx/fragment/app/k;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/k;->m()Landroidx/fragment/app/q;

    move-result-object p1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->f:Lzw0;

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroidx/fragment/app/e;->getSupportFragmentManager()Landroidx/fragment/app/k;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/k;->m()Landroidx/fragment/app/q;

    move-result-object p1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/HomeBottomNavActivity;->d:Lf2;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090291
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

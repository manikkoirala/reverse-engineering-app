.class public Lcom/ekodroid/omrevaluator/activities/OnBoardActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/OnBoardActivity$a;->a:Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/OnBoardActivity$a;->a:Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v0, "first_time_user"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/OnBoardActivity$a;->a:Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/OnBoardActivity$a;->a:Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/OnBoardActivity$a;->a:Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

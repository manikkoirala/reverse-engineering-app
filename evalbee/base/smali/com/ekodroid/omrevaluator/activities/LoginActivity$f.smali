.class public Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/LoginActivity;->K(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->H(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->d:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    const v2, 0x7f1202f1

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->I(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->d:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1202ef

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->F(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)Landroid/widget/Button;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;->a:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->G(Lcom/ekodroid/omrevaluator/activities/LoginActivity;Z)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/activities/LoginActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Landroid/content/SharedPreferences;

.field public d:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

.field public e:Landroid/widget/EditText;

.field public f:Landroid/view/View;

.field public g:Lcom/google/firebase/auth/FirebaseAuth;

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/Button;

.field public j:Lcom/google/android/material/textfield/TextInputLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->d:Lcom/ekodroid/omrevaluator/activities/LoginActivity;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->Q()V

    return-void
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->J()V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/activities/LoginActivity;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->O(I)V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)Landroid/widget/EditText;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    return-object p0
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->h:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)Landroid/widget/Button;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->i:Landroid/widget/Button;

    return-object p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/activities/LoginActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->P(Z)V

    return-void
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)Lcom/google/firebase/auth/FirebaseAuth;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->g:Lcom/google/firebase/auth/FirebaseAuth;

    return-object p0
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->L()V

    return-void
.end method


# virtual methods
.method public final J()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    const v2, 0x7f1200e6

    :goto_0
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    move v2, v3

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->M(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    const v2, 0x7f1200bf

    goto :goto_0

    :cond_1
    move v2, v4

    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v3}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->P(Z)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->i:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "uId"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->N(Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method public final K(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->P(Z)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->i:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->getIdToken()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lmb0;->a(Ljava/lang/String;Ljava/lang/String;)Lv9;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->g:Lcom/google/firebase/auth/FirebaseAuth;

    invoke-virtual {v0, p1}, Lcom/google/firebase/auth/FirebaseAuth;->l(Lv9;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity$f;-><init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Landroid/app/Activity;Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final L()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, La91;->b(Landroid/content/Context;)V

    new-instance v0, Lbw;

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/LoginActivity$e;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity$e;-><init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, v1}, Lbw;-><init>(Landroid/content/Context;ZLy01;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final M(Ljava/lang/String;)Z
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final N(Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {}, Lz1;->R()Lz1$a;

    move-result-object v0

    const-string v1, "https://evalbee.com/finishSignUp"

    invoke-virtual {v0, v1}, Lz1$a;->e(Ljava/lang/String;)Lz1$a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lz1$a;->c(Z)Lz1$a;

    move-result-object v0

    const-string v2, "com.ekodroid.omrevaluator"

    invoke-virtual {v0, v2}, Lz1$a;->d(Ljava/lang/String;)Lz1$a;

    move-result-object v0

    const-string v3, "0"

    invoke-virtual {v0, v2, v1, v3}, Lz1$a;->b(Ljava/lang/String;ZLjava/lang/String;)Lz1$a;

    move-result-object v0

    invoke-virtual {v0}, Lz1$a;->a()Lz1;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->g:Lcom/google/firebase/auth/FirebaseAuth;

    invoke-virtual {v1, p1, v0}, Lcom/google/firebase/auth/FirebaseAuth;->j(Ljava/lang/String;Lz1;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$c;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity$c;-><init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final O(I)V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v1, 0x7f130151

    invoke-direct {v0, p0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/LoginActivity$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity$d;-><init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    const v2, 0x7f120257

    invoke-virtual {p1, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final P(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->f:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final Q()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    sget-object v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;->DEFAULT_SIGN_IN:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;-><init>(Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)V

    const v1, 0x7f120095

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->requestIdToken(Ljava/lang/String;)Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->requestEmail()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;->build()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getClient(Landroid/app/Activity;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;->getSignInIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->P(Z)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->P(Z)V

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    invoke-static {p3}, Lcom/google/android/gms/auth/api/signin/GoogleSignIn;->getSignedInAccountFromIntent(Landroid/content/Intent;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    :try_start_0
    const-class p2, Lcom/google/android/gms/common/api/ApiException;

    invoke-virtual {p1, p2}, Lcom/google/android/gms/tasks/Task;->getResult(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->K(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/api/ApiException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0033

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->g:Lcom/google/firebase/auth/FirebaseAuth;

    const p1, 0x7f090395

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->j:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->c:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->e:Landroid/widget/EditText;

    const-string v2, "uId"

    const-string v3, ""

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p1, 0x7f090223

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->h:Landroid/widget/LinearLayout;

    const p1, 0x7f0900c4

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->i:Landroid/widget/Button;

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->h:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity$a;-><init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->i:Landroid/widget/Button;

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/LoginActivity$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/activities/LoginActivity$b;-><init>(Lcom/ekodroid/omrevaluator/activities/LoginActivity;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090253

    invoke-virtual {p0, p1}, Lv5;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->f:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/activities/LoginActivity;->P(Z)V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/activities/LinkVerification$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/LinkVerification;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/activities/LinkVerification;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/LinkVerification;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LinkVerification$a;->a:Lcom/ekodroid/omrevaluator/activities/LinkVerification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/LinkVerification$a;->a:Lcom/ekodroid/omrevaluator/activities/LinkVerification;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/LinkVerification;->A(Lcom/ekodroid/omrevaluator/activities/LinkVerification;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/ekodroid/omrevaluator/activities/LinkVerification;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Error signing in with email link"

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getException()Ljava/lang/Exception;

    move-result-object p1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

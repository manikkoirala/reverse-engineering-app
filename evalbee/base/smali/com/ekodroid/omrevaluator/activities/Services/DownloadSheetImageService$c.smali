.class public Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/OnSuccessListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->e(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->d:Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p3, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iput-object p4, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lx00$a;)V
    .locals 12

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->d:Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->b(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iget v2, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    iget v1, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    invoke-virtual {p1, v0, v2, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImage(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iget v5, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v8

    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->sheetDimensionJson:Ljava/lang/String;

    const-class v3, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-virtual {v1, v2, v3}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    iget-object v10, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iget v11, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    move-object v3, v0

    invoke-direct/range {v3 .. v11}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;-><init>(Ljava/lang/String;I[BLjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iget-wide v1, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->uploadId:J

    invoke-virtual {v0, v1, v2}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->setUploadId(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->setSynced(Z)V

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateSheetImage(Lcom/ekodroid/omrevaluator/database/SheetImageModel;)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->d:Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->b:Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    iget v2, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    iget v1, v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    invoke-static {p1, v0, v2, v1}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->c(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lx00$a;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;->a(Lx00$a;)V

    return-void
.end method

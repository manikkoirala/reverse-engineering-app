.class public Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    return-void
.end method


# virtual methods
.method public r(Lcom/google/firebase/messaging/d;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/messaging/d;->i()Lcom/google/firebase/messaging/d$b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/firebase/messaging/d;->i()Lcom/google/firebase/messaging/d$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/messaging/d$b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/messaging/d;->i()Lcom/google/firebase/messaging/d$b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/messaging/d$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService;->w(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/firebase/messaging/d;->getData()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/firebase/messaging/d;->getData()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService;->x(Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public t(Ljava/lang/String;)V
    .locals 9

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lr61;

    new-instance v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v6, "Android"

    move-object v2, v1

    move-object v8, p1

    invoke-direct/range {v2 .. v8}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService$a;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService;)V

    invoke-direct {v0, v1, p0, p1}, Lr61;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;Landroid/content/Context;Lzg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const/high16 v2, 0xc000000

    const/16 v3, 0x3e9

    invoke-static {v0, v3, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    return-void

    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x1a

    const-string v6, "channel_release"

    if-lt v4, v5, :cond_1

    const/4 v4, 0x4

    const-string v5, "Information and release notes"

    invoke-static {v6, v5, v4}, Leh;->a(Ljava/lang/String;Ljava/lang/CharSequence;I)Landroid/app/NotificationChannel;

    move-result-object v4

    invoke-static {v2, v4}, Lfh;->a(Landroid/app/NotificationManager;Landroid/app/NotificationChannel;)V

    :cond_1
    new-instance v4, Lrz0$e;

    invoke-direct {v4, v0, v6}, Lrz0$e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lrz0$e;->f(Z)Lrz0$e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lrz0$e;->k(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object p1

    invoke-virtual {p1, p2}, Lrz0$e;->j(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lrz0$e;->s(I)Lrz0$e;

    move-result-object p1

    new-instance v0, Lrz0$c;

    invoke-direct {v0}, Lrz0$c;-><init>()V

    invoke-virtual {v0, p2}, Lrz0$c;->h(Ljava/lang/CharSequence;)Lrz0$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrz0$e;->x(Lrz0$g;)Lrz0$e;

    move-result-object p1

    invoke-virtual {p1, v1}, Lrz0$e;->i(Landroid/app/PendingIntent;)Lrz0$e;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lrz0$e;->B(J)Lrz0$e;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p2}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrz0$e;->w(Landroid/net/Uri;)Lrz0$e;

    move-result-object p1

    const p2, 0x7f0800c9

    invoke-virtual {p1, p2}, Lrz0$e;->v(I)Lrz0$e;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f060032

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lrz0$e;->h(I)Lrz0$e;

    invoke-virtual {p1}, Lrz0$e;->b()Landroid/app/Notification;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public final x(Ljava/util/Map;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "MESSAGETYPE"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v1, Lok;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/CloudMessageService;->y(Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public final y(Ljava/util/Map;)V
    .locals 4

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "LATEST_VERSION"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "BLOCKED_VERSION"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    sget-object v3, Lok;->C:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lok;->D:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

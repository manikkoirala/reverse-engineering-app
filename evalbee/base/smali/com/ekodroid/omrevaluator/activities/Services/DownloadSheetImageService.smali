.class public Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "DownloadImageService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->a:Landroid/content/Context;

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->f(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->d(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)V

    return-void
.end method

.method public static h(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTION_DOWNLOAD_IMAGES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "ROLL_NUMBER"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public final d(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "UPDATE_SHEET_IMAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "EXAM_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "ROLL_NUMBER"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "PAGE_INDEX"

    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public final e(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;)V
    .locals 9

    .line 1
    const-string v0, "_"

    const-string v1, "/"

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v4

    :try_start_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    :cond_0
    const-string v5, "gs://evalbee-scan-images"

    invoke-static {v5}, Lo30;->i(Ljava/lang/String;)Lo30;

    move-result-object v5

    iget v6, p2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    iget v7, p2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    invoke-static {v3, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scansheet/CheckedSheetImageActivity;->P(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lo30;->n()Ljq1;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v7, p2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->uploadId:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, p2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p2, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ".jpg"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lok;->e:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljq1;->a(Ljava/lang/String;)Ljq1;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljq1;->g(Ljava/io/File;)Lx00;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$d;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;)V

    invoke-virtual {v0, v1}, Lzq1;->p(Lcom/google/android/gms/tasks/OnFailureListener;)Lzq1;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;

    invoke-direct {v1, p0, p1, p2, v3}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$c;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lzq1;->u(Lcom/google/android/gms/tasks/OnSuccessListener;)Lzq1;

    move-result-object p1

    new-instance p2, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$b;

    invoke-direct {p2, p0}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$b;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;)V

    invoke-virtual {p1, p2}, Lzq1;->s(Lo11;)Lzq1;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public final f(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;)V
    .locals 5

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget v2, v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->rollNumber:I

    iget v3, v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->pageIndex:I

    invoke-virtual {v1, p1, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getSheetImage(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->isSynced()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getUploadId()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;->uploadId:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->e(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final g(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V
    .locals 8

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSyncImages()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v1

    array-length v1, v1

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/GetSheetImagesRequest;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v7, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$a;

    invoke-direct {v7, p0, v1, p1}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;ILcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    move-object v3, p0

    move v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/ekodroid/omrevaluator/clients/SyncClients/GetSheetImagesRequest;-><init>(Landroid/content/Context;JILzg;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    invoke-static {p0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, La91;->r(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    move-result-object v1

    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getAccountType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->S:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v0, "EXAM_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const-string v1, "ROLL_NUMBER"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v3, "ACTION_DOWNLOAD_IMAGES"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    if-le v1, v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/DownloadSheetImageService;->g(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)V

    :cond_0
    return-void
.end method

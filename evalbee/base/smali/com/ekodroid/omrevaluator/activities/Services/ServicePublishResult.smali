.class public Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field public a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Ljava/util/ArrayList;

.field public i:Landroid/content/SharedPreferences;

.field public j:Landroid/content/SharedPreferences$Editor;

.field public k:Lze1;

.field public l:Z

.field public m:Landroid/content/BroadcastReceiver;

.field public n:Z

.field public p:Ly01;

.field public q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->k:Lze1;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->l:Z

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->m:Landroid/content/BroadcastReceiver;

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n:Z

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->l:Z

    return p1
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->l()I

    move-result p0

    return p0
.end method

.method public static synthetic d(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->o()V

    return-void
.end method

.method public static synthetic e(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;III)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->p(III)V

    return-void
.end method

.method public static synthetic f(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n()V

    return-void
.end method

.method public static synthetic g(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->r(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic h(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->j()V

    return-void
.end method


# virtual methods
.method public final i(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye1;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v0}, Lye1;->g()I

    move-result v2

    invoke-virtual {v1, v2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_0
    const-string v1, " - - - "

    :goto_1
    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final j()V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "REPORT_SENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public final k(I)V
    .locals 7

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sent Reports : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_sentCount"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pending Reports : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i:Landroid/content/SharedPreferences;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    sub-int/2addr p1, v2

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    filled-new-array {v0, p1}, [Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " - Publish Complete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1, p1, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q(I[Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " - Publish Complete, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, p1, v1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const v1, 0x7f08016d

    invoke-static {v0, p1, v4, v1}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-virtual {p1}, Landroid/app/Service;->stopSelf()V

    return-void
.end method

.method public final l()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_lastIndexSent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final m(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;
    .locals 18

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v5, v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v6

    invoke-static {v6}, Lve1;->p(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v12

    invoke-static {v6}, Lve1;->q(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v13

    invoke-static {v6}, Lve1;->s(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v14

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v9

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v6

    invoke-static {v9, v10, v6}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v11

    new-instance v6, Lye1;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v8

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent()Z

    move-result v15

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced()Z

    move-result v16

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v17

    move-object v7, v6

    invoke-direct/range {v7 .. v17}, Lye1;-><init>(IDLjava/lang/String;IIIZZZ)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRankingMethod()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lve1;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    return-object v4
.end method

.method public final n()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_lastIndexSent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, -0x1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->j:Landroid/content/SharedPreferences$Editor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->j:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final o()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_sentCount"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->j:Landroid/content/SharedPreferences$Editor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->j:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not yet implemented"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->j:Landroid/content/SharedPreferences$Editor;

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->p:Ly01;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "STOP_SERVICE_PUBLISH_RESULT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->m:Landroid/content/BroadcastReceiver;

    const/4 v2, 0x2

    invoke-static {p0, v1, v0, v2}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    iget-boolean p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n:Z

    const/4 p3, 0x1

    if-eqz p2, :cond_0

    return p3

    :cond_0
    iput-boolean p3, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n:Z

    const-string p2, "SEND_PATTERN"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->b:Ljava/lang/String;

    const-string p2, "SEND_SMS"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->d:Z

    const-string p2, "CC_EMAIL"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->c:Ljava/lang/String;

    const-string p2, "SEND_EMAIL"

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->e:Z

    const-string p2, "INCLUDE_RANK"

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->f:Z

    const-string p2, "INCLUDE_SHEET_IMG"

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    iput-boolean p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->g:Z

    const-string p2, "EXAM_ID"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->m(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->l()I

    move-result p2

    invoke-virtual {p0, p3, p1, p2}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->p(III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->r(Ljava/util/ArrayList;)V

    :cond_1
    return p3
.end method

.method public final p(III)V
    .locals 5

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "STOP_SERVICE_PUBLISH_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/high16 v2, 0x4000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    if-nez v1, :cond_0

    return-void

    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    const-string v4, "channel1"

    if-lt v2, v3, :cond_1

    const/4 v2, 0x4

    const-string v3, "publish result"

    invoke-static {v4, v3, v2}, Leh;->a(Ljava/lang/String;Ljava/lang/CharSequence;I)Landroid/app/NotificationChannel;

    move-result-object v2

    invoke-static {v1, v2}, Lfh;->a(Landroid/app/NotificationManager;Landroid/app/NotificationChannel;)V

    :cond_1
    new-instance v2, Lrz0$e;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-direct {v2, v3, v4}, Lrz0$e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v3, "EvalBee"

    invoke-virtual {v2, v3}, Lrz0$e;->k(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sent Reports : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrz0$e;->j(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lrz0$e;->B(J)Lrz0$e;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lrz0$e;->f(Z)Lrz0$e;

    move-result-object v2

    const v4, 0x7f0800cc

    invoke-virtual {v2, v4}, Lrz0$e;->v(I)Lrz0$e;

    move-result-object v2

    invoke-virtual {v2, p2, p3, v3}, Lrz0$e;->t(IIZ)Lrz0$e;

    move-result-object p2

    const p3, 0x7f0800ba

    const-string v2, "Cancel"

    invoke-virtual {p2, p3, v2, v0}, Lrz0$e;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2}, Lrz0$e;->b()Landroid/app/Notification;

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public final q(I[Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 1
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    const-string v3, "channel1"

    if-lt v1, v2, :cond_1

    const/4 v1, 0x4

    const-string v2, "publish result"

    invoke-static {v3, v2, v1}, Leh;->a(Ljava/lang/String;Ljava/lang/CharSequence;I)Landroid/app/NotificationChannel;

    move-result-object v1

    invoke-static {v0, v1}, Lfh;->a(Landroid/app/NotificationManager;Landroid/app/NotificationChannel;)V

    :cond_1
    new-instance v1, Lrz0$f;

    invoke-direct {v1}, Lrz0$f;-><init>()V

    array-length v2, p2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    aget-object v5, p2, v4

    invoke-virtual {v1, v5}, Lrz0$f;->h(Ljava/lang/CharSequence;)Lrz0$f;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v1, p3}, Lrz0$f;->i(Ljava/lang/CharSequence;)Lrz0$f;

    new-instance p2, Lrz0$e;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-direct {p2, v2, v3}, Lrz0$e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "EvalBee"

    invoke-virtual {p2, v2}, Lrz0$e;->k(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2, p3}, Lrz0$e;->j(Ljava/lang/CharSequence;)Lrz0$e;

    move-result-object p2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3}, Lrz0$e;->B(J)Lrz0$e;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Lrz0$e;->f(Z)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2, v1}, Lrz0$e;->x(Lrz0$g;)Lrz0$e;

    move-result-object p2

    const p3, 0x7f0800cc

    invoke-virtual {p2, p3}, Lrz0$e;->v(I)Lrz0$e;

    move-result-object p2

    const/4 p3, 0x2

    invoke-static {p3}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object p3

    invoke-virtual {p2, p3}, Lrz0$e;->w(Landroid/net/Uri;)Lrz0$e;

    move-result-object p2

    invoke-virtual {p2}, Lrz0$e;->b()Landroid/app/Notification;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public final r(Ljava/util/ArrayList;)V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->l()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    iget-boolean v2, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->l:Z

    if-eqz v2, :cond_0

    goto/16 :goto_0

    :cond_0
    move-object/from16 v2, p1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lye1;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v3

    invoke-virtual {v1}, Lye1;->g()I

    move-result v4

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n()V

    invoke-virtual/range {p0 .. p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->r(Ljava/util/ArrayList;)V

    return-void

    :cond_1
    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-ge v4, v5, :cond_2

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x8

    if-ge v4, v5, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n()V

    invoke-virtual/range {p0 .. p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->r(Ljava/util/ArrayList;)V

    return-void

    :cond_2
    iget-object v4, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->b:Ljava/lang/String;

    const-string v5, "send_pending"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v4

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResultEmailSendStatus(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->n()V

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->o()V

    invoke-virtual/range {p0 .. p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->r(Ljava/util/ArrayList;)V

    return-void

    :cond_3
    new-instance v2, Ltd1;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v6

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v7

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->c:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getPhoneNo()Ljava/lang/String;

    move-result-object v11

    iget-boolean v12, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->e:Z

    iget-boolean v13, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->d:Z

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v1}, Lye1;->f()I

    move-result v15

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->q:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v16

    iget-boolean v1, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->g:Z

    iget-boolean v3, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->f:Z

    move-object v5, v2

    move/from16 v17, v1

    move/from16 v18, v3

    invoke-direct/range {v5 .. v18}, Ltd1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIILjava/lang/String;ZZ)V

    new-instance v1, Lze1;

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object v4, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->p:Ly01;

    invoke-direct {v1, v3, v2, v4}, Lze1;-><init>(Landroid/content/Context;Ltd1;Ly01;)V

    iput-object v1, v0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->k:Lze1;

    return-void

    :cond_4
    :goto_0
    move-object/from16 v2, p1

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->k(I)V

    return-void
.end method

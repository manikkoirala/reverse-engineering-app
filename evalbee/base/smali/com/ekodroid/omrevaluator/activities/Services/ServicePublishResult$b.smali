.class public Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    if-eqz p1, :cond_0

    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p1

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->b(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object v3, v2, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->c(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lye1;

    invoke-virtual {v2}, Lye1;->g()I

    move-result v2

    invoke-virtual {p1, v1, v2, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->updateResultSent(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;IZ)Z

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->d(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->c(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->e(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->f(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->g(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;Ljava/util/ArrayList;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->c(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->e(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;III)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->f(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult$b;->a:Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->h:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;->g(Lcom/ekodroid/omrevaluator/activities/Services/ServicePublishResult;Ljava/util/ArrayList;)V

    :goto_0
    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SyncImageService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->a:Landroid/content/Context;

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTION_SYNC_IMAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllImageSyncedActivatedExams()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->c(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)V
    .locals 19

    .line 1
    invoke-static/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v0

    invoke-static/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllNonSyncImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v1, p0

    move-object v2, v10

    move-object v5, v0

    move-wide v6, v14

    invoke-virtual/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->d(Lcom/ekodroid/omrevaluator/database/SheetImageModel;JLjava/lang/String;J)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;

    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getRollNo()I

    move-result v2

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getPageIndex()I

    move-result v3

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getSheetDimensionJson()Ljava/lang/String;

    move-result-object v18

    move-object v11, v1

    move-wide v4, v14

    move v14, v2

    move v15, v3

    move-wide/from16 v16, v4

    invoke-direct/range {v11 .. v18}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;-><init>(JIIJLjava/lang/String;)V

    move-object/from16 v2, p0

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->e(Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->setSynced(Z)V

    invoke-virtual {v10, v4, v5}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->setUploadId(J)V

    invoke-virtual {v8, v10}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateSheetImage(Lcom/ekodroid/omrevaluator/database/SheetImageModel;)Z

    goto :goto_0

    :cond_1
    move-object/from16 v2, p0

    goto :goto_0

    :cond_2
    move-object/from16 v2, p0

    return-void
.end method

.method public final d(Lcom/ekodroid/omrevaluator/database/SheetImageModel;JLjava/lang/String;J)Z
    .locals 4

    .line 1
    const-string v0, "gs://evalbee-scan-images"

    invoke-static {v0}, Lo30;->i(Ljava/lang/String;)Lo30;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p4, :cond_3

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getImagePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    return v1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p5, "_"

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getRollNo()I

    move-result p6

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getPageIndex()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ".jpg"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lo30;->n()Ljq1;

    move-result-object p5

    new-instance p6, Ljava/lang/StringBuilder;

    invoke-direct {p6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lok;->e:Ljava/lang/String;

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p5, p1}, Ljq1;->a(Ljava/lang/String;)Ljq1;

    move-result-object p1

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljq1;->l(Landroid/net/Uri;)Li12;

    move-result-object p1

    new-instance p2, Lzs1;

    invoke-direct {p2}, Lzs1;-><init>()V

    new-instance p3, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$c;

    invoke-direct {p3, p0, p2}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$c;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;Lzs1;)V

    invoke-virtual {p1, p3}, Lzq1;->p(Lcom/google/android/gms/tasks/OnFailureListener;)Lzq1;

    move-result-object p1

    new-instance p3, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$b;

    invoke-direct {p3, p0, p2}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$b;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;Lzs1;)V

    invoke-virtual {p1, p3}, Lzq1;->u(Lcom/google/android/gms/tasks/OnSuccessListener;)Lzq1;

    move-result-object p1

    new-instance p3, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$a;

    invoke-direct {p3, p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;)V

    invoke-virtual {p1, p3}, Lzq1;->s(Lo11;)Lzq1;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    iget-wide p5, p2, Lzs1;->c:J

    sub-long/2addr p3, p5

    const-wide/32 p5, 0x927c0

    cmp-long p1, p3, p5

    if-gez p1, :cond_2

    iget-boolean p1, p2, Lzs1;->b:Z

    if-nez p1, :cond_2

    const-wide/16 p3, 0x64

    :try_start_0
    invoke-static {p3, p4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-boolean p1, p2, Lzs1;->a:Z

    return p1

    :cond_3
    :goto_1
    return v1
.end method

.method public final e(Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;)Z
    .locals 5

    .line 1
    new-instance v0, Lzs1;

    invoke-direct {v0}, Lzs1;-><init>()V

    new-instance v1, Lo61;

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$d;

    invoke-direct {v2, p0, v0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService$d;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;Lzs1;)V

    invoke-direct {v1, p0, p1, v2}, Lo61;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/SheetImageDto;Lzg;)V

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lzs1;->c:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long p1, v1, v3

    if-gez p1, :cond_0

    iget-boolean p1, v0, Lzs1;->b:Z

    if-nez p1, :cond_0

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-boolean p1, v0, Lzs1;->a:Z

    return p1
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    invoke-static {p0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, La91;->r(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    move-result-object v1

    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;->getAccountType()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lok;->S:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ACTION_SYNC_IMAGE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->a()V

    :cond_0
    return-void
.end method

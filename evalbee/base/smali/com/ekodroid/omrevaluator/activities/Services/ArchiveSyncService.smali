.class public Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "ArchiveSyncService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->e(ILjava/lang/String;)V

    return-void
.end method

.method public static n(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTION_SYNC_ARCHIVE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public final b(Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getTemplateId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->o(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/16 v1, 0x3c

    const-string v2, "CLOUD_SYNC"

    invoke-virtual {p0, v1, v0, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->p(Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    const/16 v1, 0x50

    invoke-virtual {p0, v1, v0, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getTemplateId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteArchiveLocalModel(Ljava/lang/String;)Z

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getTemplateId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchiveLocalDataModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    move-result-object p1

    if-nez p1, :cond_2

    const/16 p1, 0x64

    invoke-virtual {p0, p1, v0, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "ARCHIVED_EXAM_CLOUD"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    return-void
.end method

.method public final c(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchiveId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setArchiveId(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamDate()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchivePayload()Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    move-result-object v4

    const/16 v5, 0x14

    invoke-virtual {p0, v5, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->e(ILjava/lang/String;)V

    const/4 v5, 0x1

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->isExpExm()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->f(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)Z

    move-result v4

    if-nez v4, :cond_2

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchivePayload()Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->setExpExm(Z)V

    const/16 v6, 0x46

    invoke-virtual {v4, v6}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->setProgress(I)V

    invoke-virtual {p1, v4}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setArchivePayload(Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;)V

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    invoke-virtual {p0, v6, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->e(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int/2addr v2, v5

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->g(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)Z

    move-result p1

    if-nez p1, :cond_4

    return-void

    :cond_4
    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->h(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result p1

    if-nez p1, :cond_5

    return-void

    :cond_5
    const/16 p1, 0x64

    invoke-virtual {p0, p1, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->e(ILjava/lang/String;)V

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "ARCHIVED_EXAM_LOCAL"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final d(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "UPDATE_ARCHIVE_EXAM_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "file_id"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "type"

    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public final e(ILjava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "UPDATE_EXAM_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "file_id"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public final f(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)Z
    .locals 3

    .line 1
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lok;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".exm"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 p1, 0x0

    :try_start_0
    invoke-static {p3, p4, p2}, Lyx;->d(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;

    move-result-object p2

    invoke-static {p2}, Lyx;->b(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;)[B

    move-result-object p2

    new-instance p3, Ljava/io/FileOutputStream;

    invoke-direct {p3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p3, p2}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {p3}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    goto :goto_0

    :catchall_0
    move-exception p2

    move-object p3, p1

    move-object p1, p2

    goto :goto_2

    :catch_1
    move-exception p2

    move-object p3, p1

    move-object p1, p2

    :goto_0
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-virtual {v1}, Ljava/io/File;->deleteOnExit()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p3, :cond_1

    :try_start_3
    invoke-virtual {p3}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_1
    const/4 p1, 0x0

    return p1

    :catchall_1
    move-exception p1

    :goto_2
    if-eqz p3, :cond_2

    :try_start_4
    invoke-virtual {p3}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_3

    :catch_3
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_2
    :goto_3
    throw p1
.end method

.method public final g(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;II)Z
    .locals 12

    .line 1
    move-object v10, p1

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lok;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".exm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-int v0, v0

    new-instance v11, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, La91;->g()Ljava/lang/String;

    move-result-object v5

    mul-int/lit8 v6, v0, 0x2

    const/4 v7, 0x1

    move-object v0, v11

    move-object v1, p1

    move v8, p3

    move/from16 v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateArchiveLocalDataModel(Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;)Z

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchiveLocalDataModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final h(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result p1

    return p1
.end method

.method public final i(Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchiveLocalDataModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    move-result-object v1

    const/16 v2, 0x46

    const/16 v3, 0x28

    const-string v4, "DELETE"

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->k(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v3, p1, v4}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->j(Ljava/lang/String;)Z

    invoke-virtual {p0, v2, p1, v4}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->l(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->j(Ljava/lang/String;)Z

    invoke-virtual {p0, v3, p1, v4}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteArchiveLocalModel(Ljava/lang/String;)Z

    invoke-virtual {p0, v2, p1, v4}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {p0, p1}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedPendingDelete;->deletArchiveId(Landroid/content/Context;Ljava/lang/String;)V

    const/16 v0, 0x64

    invoke-virtual {p0, v0, p1, v4}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->d(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "ARCHIVED_EXAM_DELETE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final j(Ljava/lang/String;)Z
    .locals 3

    .line 1
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lok;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".exm"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final k(Ljava/lang/String;)Z
    .locals 5

    .line 1
    invoke-static {}, Lo30;->f()Lo30;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".exm"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lo30;->n()Ljq1;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lok;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljq1;->a(Ljava/lang/String;)Ljq1;

    move-result-object p1

    invoke-virtual {p1}, Ljq1;->d()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lzs1;

    invoke-direct {v0}, Lzs1;-><init>()V

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;

    invoke-direct {v1, p0, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {p1, v1}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$c;

    invoke-direct {v1, p0, v0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$c;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {p1, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lzs1;->c:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long p1, v1, v3

    if-gez p1, :cond_1

    iget-boolean p1, v0, Lzs1;->b:Z

    if-nez p1, :cond_1

    const-wide/16 v1, 0x1f4

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-boolean p1, v0, Lzs1;->a:Z

    return p1

    :cond_2
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public final l(Ljava/lang/String;)Z
    .locals 5

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/FirebaseFirestore;->f()Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    new-instance v2, Lzs1;

    invoke-direct {v2}, Lzs1;-><init>()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lv00;->a()Lv00;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "ArchivedExams"

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/FirebaseFirestore;->a(Ljava/lang/String;)Lih;

    move-result-object p1

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lih;->g(Ljava/lang/String;)Lcom/google/firebase/firestore/a;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/google/firebase/firestore/a;->s(Ljava/util/Map;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$b;

    invoke-direct {v0, p0, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$b;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$a;

    invoke-direct {v0, p0, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v3, v2, Lzs1;->c:J

    sub-long/2addr v0, v3

    const-wide/32 v3, 0x927c0

    cmp-long p1, v0, v3

    if-gez p1, :cond_0

    iget-boolean p1, v2, Lzs1;->b:Z

    if-nez p1, :cond_0

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-boolean p1, v2, Lzs1;->a:Z

    return p1
.end method

.method public final m()V
    .locals 7

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchivingTemplatesJson()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p0, v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->c(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)V

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v2

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v4, v2, v5, v6}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;-><init>(Ljava/lang/Long;ZZ)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3}, La91;->I(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;

    goto :goto_0

    :cond_1
    invoke-static {p0}, La91;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getArchiveLocalDataModels()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->b(Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;)V

    goto :goto_1

    :cond_2
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchivedPendingDelete;->getPendingArchiveIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->i(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method public final o(Ljava/lang/String;)Z
    .locals 6

    .line 1
    invoke-static {}, Lo30;->f()Lo30;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lok;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    sget-object v5, Lok;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ".exm"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    return v2

    :cond_2
    invoke-virtual {v0}, Lo30;->n()Ljq1;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lok;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljq1;->a(Ljava/lang/String;)Ljq1;

    move-result-object v0

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljq1;->l(Landroid/net/Uri;)Li12;

    move-result-object v0

    new-instance v1, Lzs1;

    invoke-direct {v1}, Lzs1;-><init>()V

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$i;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$i;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {v0, v2}, Lzq1;->p(Lcom/google/android/gms/tasks/OnFailureListener;)Lzq1;

    move-result-object v0

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$h;

    invoke-direct {v2, p0, v1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$h;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {v0, v2}, Lzq1;->u(Lcom/google/android/gms/tasks/OnSuccessListener;)Lzq1;

    move-result-object v0

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$g;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$g;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lzq1;->s(Lo11;)Lzq1;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, v1, Lzs1;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long p1, v2, v4

    if-gez p1, :cond_3

    iget-boolean p1, v1, Lzs1;->b:Z

    if-nez p1, :cond_3

    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_3
    iget-boolean p1, v1, Lzs1;->a:Z

    return p1

    :cond_4
    :goto_1
    return v2
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ACTION_SYNC_ARCHIVE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->m()V

    :cond_0
    return-void
.end method

.method public final p(Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;)Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/FirebaseFirestore;->f()Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/ArchivedLocalDataModel;->getTemplateId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lzs1;

    invoke-direct {p1}, Lzs1;-><init>()V

    const-string v3, "ArchivedExams"

    invoke-virtual {v0, v3}, Lcom/google/firebase/firestore/FirebaseFirestore;->a(Ljava/lang/String;)Lih;

    move-result-object v0

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lih;->g(Ljava/lang/String;)Lcom/google/firebase/firestore/a;

    move-result-object v0

    invoke-static {}, Lrm1;->c()Lrm1;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/firestore/a;->q(Ljava/lang/Object;Lrm1;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$f;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$f;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$e;

    invoke-direct {v1, p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$e;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p1, Lzs1;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-boolean v0, p1, Lzs1;->b:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-boolean p1, p1, Lzs1;->a:Z

    return p1
.end method

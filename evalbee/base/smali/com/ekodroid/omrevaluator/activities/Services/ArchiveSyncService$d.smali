.class public Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/OnFailureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->k(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lzs1;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;Lzs1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;->b:Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;

    iput-object p2, p0, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;->a:Lzs1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Exception;)V
    .locals 3

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Object does not exist at location."

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;->b:Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v1, "ERROR_STORAGE_DELETE"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService$d;->a:Lzs1;

    iput-boolean v0, p1, Lzs1;->b:Z

    iput-boolean v0, p1, Lzs1;->a:Z

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "ScanSyncService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;->a:Z

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;->a:Z

    return p1
.end method


# virtual methods
.method public final b()V
    .locals 3

    .line 1
    new-instance v0, Ln52;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object v0

    new-instance v1, Ln61;

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService$a;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;)V

    invoke-direct {v1, v0, v2}, Ln61;-><init>(Lee1;Ly01;)V

    return-void
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;->b()V

    :goto_0
    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/ScanSyncService;->a:Z

    if-nez p1, :cond_0

    const-wide/16 v0, 0x7d0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    return-void
.end method

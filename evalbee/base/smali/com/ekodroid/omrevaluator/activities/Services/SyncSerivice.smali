.class public Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SyncSerivice"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->a:Z

    iput-boolean v0, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->b:Z

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->c:Landroid/content/Context;

    return-void
.end method

.method public static synthetic a(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->h()V

    return-void
.end method

.method public static synthetic b(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->g(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->c:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic d(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->a:Z

    return p1
.end method

.method public static synthetic e(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->b:Z

    return p1
.end method


# virtual methods
.method public final f()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lr30;->i(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$d;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$d;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 9

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lr61;

    new-instance v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v6, "Android"

    move-object v2, v1

    move-object v8, p1

    invoke-direct/range {v2 .. v8}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance p1, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$b;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$b;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;)V

    invoke-direct {v0, v1, p0, p1}, Lr61;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/Device;Landroid/content/Context;Lzg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public final h()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ln52;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Ln52;->b()Lee1;

    move-result-object v1

    new-instance v2, Lra0;

    new-instance v3, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$c;

    invoke-direct {v3, p0, v1, v0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$c;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;Lee1;Ljava/lang/String;)V

    invoke-direct {v2, p0, v1, v3, v0}, Lra0;-><init>(Landroid/content/Context;Lee1;Ly01;Ljava/lang/String;)V

    return-void
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {}, Lcom/google/firebase/messaging/FirebaseMessaging;->l()Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/messaging/FirebaseMessaging;->o()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice$a;-><init>(Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->f()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    :goto_0
    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->a:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->b:Z

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->c:Landroid/content/Context;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/students/services/SyncStudentService;->h(Landroid/content/Context;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->c:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->c(Landroid/content/Context;Z)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/Services/SyncSerivice;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/Services/SyncImageService;->b(Landroid/content/Context;)V

    return-void

    :cond_1
    :goto_1
    const-wide/16 v0, 0x7d0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

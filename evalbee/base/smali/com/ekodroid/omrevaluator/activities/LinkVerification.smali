.class public Lcom/ekodroid/omrevaluator/activities/LinkVerification;
.super Lv5;
.source "SourceFile"


# static fields
.field public static c:Ljava/lang/String; = "tag"


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv5;-><init>()V

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/activities/LinkVerification;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/LinkVerification;->C()V

    return-void
.end method

.method public static synthetic B()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/ekodroid/omrevaluator/activities/LinkVerification;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final C()V
    .locals 3

    .line 1
    new-instance v0, Lbw;

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/LinkVerification$b;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/LinkVerification$b;-><init>(Lcom/ekodroid/omrevaluator/activities/LinkVerification;)V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2, v1}, Lbw;-><init>(Landroid/content/Context;ZLy01;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0032

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/firebase/auth/FirebaseAuth;->i(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "MyPref"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "uId"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/google/firebase/auth/FirebaseAuth;->m(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lcom/ekodroid/omrevaluator/activities/LinkVerification$a;

    invoke-direct {v0, p0}, Lcom/ekodroid/omrevaluator/activities/LinkVerification$a;-><init>(Lcom/ekodroid/omrevaluator/activities/LinkVerification;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

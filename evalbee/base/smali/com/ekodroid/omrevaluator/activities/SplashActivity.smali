.class public Lcom/ekodroid/omrevaluator/activities/SplashActivity;
.super Lv5;
.source "SourceFile"


# instance fields
.field public c:Ljava/text/SimpleDateFormat;

.field public d:Landroid/os/Handler;

.field public e:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public f:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Landroid/content/SharedPreferences;

.field public j:Z

.field public k:Ljava/lang/Runnable;

.field public l:Ljava/lang/String;

.field public m:Ly01;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lv5;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->d:Landroid/os/Handler;

    iput-object p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->f:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    const/16 v0, 0x3e7

    iput v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->h:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->j:Z

    new-instance v1, Lcom/ekodroid/omrevaluator/activities/SplashActivity$a;

    invoke-direct {v1, p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity$a;-><init>(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V

    iput-object v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->k:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->l:Ljava/lang/String;

    return-void
.end method

.method public static synthetic A(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->R()Z

    move-result p0

    return p0
.end method

.method public static synthetic B(Lcom/ekodroid/omrevaluator/activities/SplashActivity;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->T(Z)V

    return-void
.end method

.method public static synthetic C(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->O()V

    return-void
.end method

.method public static synthetic D(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->U()V

    return-void
.end method

.method public static synthetic E(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Landroid/content/SharedPreferences;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->i:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method public static synthetic F(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->g:I

    return p0
.end method

.method public static synthetic G(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->h:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic H(Lcom/ekodroid/omrevaluator/activities/SplashActivity;ILandroid/content/SharedPreferences;ILjava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->Q(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic I(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->j:Z

    return p0
.end method

.method public static synthetic J(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->l:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic K(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Lcom/ekodroid/omrevaluator/activities/SplashActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->f:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    return-object p0
.end method

.method public static synthetic L(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->S()V

    return-void
.end method


# virtual methods
.method public final M()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->R()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->T(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->N()V

    :goto_0
    return-void
.end method

.method public final N()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->i:Landroid/content/SharedPreferences;

    iget v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->g:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->h:Ljava/lang/String;

    const/16 v3, 0xa

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->Q(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->O()V

    :goto_0
    return-void
.end method

.method public final O()V
    .locals 4

    .line 1
    new-instance v0, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object v0

    new-instance v1, Lra0;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->m:Ly01;

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v2, v3}, Lra0;-><init>(Landroid/content/Context;Lee1;Ly01;Ljava/lang/String;)V

    return-void
.end method

.method public final P()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->i:Landroid/content/SharedPreferences;

    const-string v1, "first_time_user"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->f:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    const-class v2, Lcom/ekodroid/omrevaluator/activities/OnBoardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public final Q(ILandroid/content/SharedPreferences;ILjava/lang/String;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lok;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v2, ""

    invoke-interface {p2, p3, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p4}, Lb;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p3

    invoke-virtual {p3, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 p2, 0x5

    invoke-virtual {p3, p2, p1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    return v0
.end method

.method public final R()Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->g:I

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->i:Landroid/content/SharedPreferences;

    sget-object v3, Lok;->C:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ge v1, v2, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    return v0
.end method

.method public final S()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->f:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1201b6

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    const v2, 0x7f1201b2

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/SplashActivity$c;

    invoke-direct {v2, p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity$c;-><init>(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V

    const v3, 0x7f12029a

    invoke-virtual {v1, v3, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final T(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->i:Landroid/content/SharedPreferences;

    sget-object v1, Lok;->D:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->f:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    const v3, 0x7f130151

    invoke-direct {v1, v2, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f0c0089

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v3, 0x7f12023e

    invoke-virtual {v1, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v1, p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object p1

    const v1, 0x7f0900e1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v3, Lcom/ekodroid/omrevaluator/activities/SplashActivity$d;

    invoke-direct {v3, p0, p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity$d;-><init>(Lcom/ekodroid/omrevaluator/activities/SplashActivity;Landroidx/appcompat/app/a;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0900e0

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/ekodroid/omrevaluator/activities/SplashActivity$e;

    invoke-direct {v2, p0, p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity$e;-><init>(Lcom/ekodroid/omrevaluator/activities/SplashActivity;Landroidx/appcompat/app/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->g:I

    if-le v0, v2, :cond_0

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final U()V
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "market://details?id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x48080000    # 139264.0f

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->e:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "APP_UPDATE_NOW"

    iget-object v3, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->h:Ljava/lang/String;

    const-string v4, "update_now"

    invoke-static {v0, v1, v3, v4}, Lo4;->b(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v0, Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://play.google.com/store/apps/details?id="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c003f

    invoke-virtual {p0, p1}, Lv5;->setContentView(I)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->i:Landroid/content/SharedPreferences;

    invoke-static {p0}, Ldj1;->d(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "BLOCK_NO_SYNC"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->j:Z

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->e:Lcom/google/firebase/analytics/FirebaseAnalytics;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->P()V

    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd"

    invoke-direct {p1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->h:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    iget p1, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->g:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    new-instance p1, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;

    invoke-direct {p1, p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;-><init>(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->m:Ly01;

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1c

    if-lt p1, v0, :cond_0

    :try_start_1
    const-class p1, Landroid/database/CursorWindow;

    const-string v0, "sCursorWindowSize"

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    const/high16 v0, 0x1400000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_1
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->M()V

    return-void
.end method

.class public Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/ekodroid/omrevaluator/activities/SplashActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->A(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->B(Lcom/ekodroid/omrevaluator/activities/SplashActivity;Z)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->E(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->F(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->G(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x14

    invoke-static {p1, v3, v0, v1, v2}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->H(Lcom/ekodroid/omrevaluator/activities/SplashActivity;ILandroid/content/SharedPreferences;ILjava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->I(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->K(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CONNECTION_ERROR"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->J(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->L(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->K(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CONNECTION_SUCCESS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->J(Lcom/ekodroid/omrevaluator/activities/SplashActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/ekodroid/omrevaluator/activities/SplashActivity$b;->a:Lcom/ekodroid/omrevaluator/activities/SplashActivity;

    iget-object v0, p1, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->d:Landroid/os/Handler;

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/activities/SplashActivity;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.class public Lcom/google/firebase/appcheck/FirebaseAppCheckRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Lcom/google/android/gms/common/annotation/KeepForSdk;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lda1;Lda1;Lda1;Lda1;Lgj;)Ls10;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/firebase/appcheck/FirebaseAppCheckRegistrar;->b(Lda1;Lda1;Lda1;Lda1;Lgj;)Ls10;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Lda1;Lda1;Lda1;Lda1;Lgj;)Ls10;
    .locals 8

    .line 1
    new-instance v7, Lpq;

    const-class v0, Lr10;

    invoke-interface {p4, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lr10;

    const-class v0, Lwc0;

    invoke-interface {p4, v0}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v2

    invoke-interface {p4, p0}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v3, p0

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-interface {p4, p1}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v4, p0

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-interface {p4, p2}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v5, p0

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-interface {p4, p3}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v6, p0

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lpq;-><init>(Lr10;Lr91;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V

    return-object v7
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 7

    const-class v0, Ln02;

    const-class v1, Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v0

    const-class v2, Lsj0;

    invoke-static {v2, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v2

    const-class v3, Lza;

    invoke-static {v3, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v1

    const-class v3, Lfc;

    const-class v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v3, v4}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ldg0;

    aput-object v6, v4, v5

    const-class v5, Ls10;

    invoke-static {v5, v4}, Lzi;->f(Ljava/lang/Class;[Ljava/lang/Class;)Lzi$b;

    move-result-object v4

    const-string v5, "fire-app-check"

    invoke-virtual {v4, v5}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v4

    const-class v6, Lr10;

    invoke-static {v6}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v6

    invoke-virtual {v4, v6}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v4

    invoke-static {v0}, Los;->j(Lda1;)Los;

    move-result-object v6

    invoke-virtual {v4, v6}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v4

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v6

    invoke-virtual {v4, v6}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v4

    invoke-static {v1}, Los;->j(Lda1;)Los;

    move-result-object v6

    invoke-virtual {v4, v6}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v4

    invoke-static {v3}, Los;->j(Lda1;)Los;

    move-result-object v6

    invoke-virtual {v4, v6}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v4

    const-class v6, Lwc0;

    invoke-static {v6}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v6

    invoke-virtual {v4, v6}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v4

    new-instance v6, Lt10;

    invoke-direct {v6, v0, v2, v1, v3}, Lt10;-><init>(Lda1;Lda1;Lda1;Lda1;)V

    invoke-virtual {v4, v6}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->c()Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    invoke-static {}, Lvc0;->a()Lzi;

    move-result-object v1

    const-string v2, "17.1.1"

    invoke-static {v5, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v2

    filled-new-array {v0, v1, v2}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

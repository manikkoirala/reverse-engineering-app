.class public final Lcom/google/firebase/sessions/SessionDatastoreImpl$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/sessions/SessionDatastoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final synthetic a:[Lgi0;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lgi0;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference2Impl;

    const-string v2, "dataStore"

    const-string v3, "getDataStore(Landroid/content/Context;)Landroidx/datastore/core/DataStore;"

    const-class v4, Lcom/google/firebase/sessions/SessionDatastoreImpl$a;

    const/4 v5, 0x0

    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference2Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v1}, Lyc1;->i(Lkotlin/jvm/internal/PropertyReference2;)Lji0;

    move-result-object v1

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/firebase/sessions/SessionDatastoreImpl$a;->a:[Lgi0;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lgq;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/firebase/sessions/SessionDatastoreImpl$a;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lcom/google/firebase/sessions/SessionDatastoreImpl$a;Landroid/content/Context;)Ljp;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/sessions/SessionDatastoreImpl$a;->b(Landroid/content/Context;)Ljp;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Ljp;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firebase/sessions/SessionDatastoreImpl;->f()Lpc1;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/sessions/SessionDatastoreImpl$a;->a:[Lgi0;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p1, v1}, Lpc1;->a(Ljava/lang/Object;Lgi0;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljp;

    return-object p1
.end method

.class public final Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/sessions/FirebaseSessionsRegistrar$a;
    }
.end annotation


# static fields
.field private static final Companion:Lcom/google/firebase/sessions/FirebaseSessionsRegistrar$a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-sessions"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final backgroundDispatcher:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final blockingDispatcher:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final firebaseApp:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final firebaseInstallationsApi:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final sessionFirelogPublisher:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final sessionGenerator:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final sessionsSettings:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final transportFactory:Lda1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lda1;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar$a;-><init>(Lgq;)V

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->Companion:Lcom/google/firebase/sessions/FirebaseSessionsRegistrar$a;

    const-class v0, Lr10;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    const-class v0, Lr20;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseInstallationsApi:Lda1;

    const-class v0, Lza;

    const-class v1, Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->backgroundDispatcher:Lda1;

    const-class v0, Lfc;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->blockingDispatcher:Lda1;

    const-class v0, Lcom/google/android/datatransport/TransportFactory;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->transportFactory:Lda1;

    const-class v0, Lzl1;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->sessionFirelogPublisher:Lda1;

    const-class v0, Lcom/google/firebase/sessions/SessionGenerator;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->sessionGenerator:Lda1;

    const-class v0, Lcom/google/firebase/sessions/settings/SessionsSettings;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->sessionsSettings:Lda1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lgj;)Lcm1;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->getComponents$lambda-5(Lgj;)Lcm1;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Lgj;)Lcom/google/firebase/sessions/SessionGenerator;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->getComponents$lambda-1(Lgj;)Lcom/google/firebase/sessions/SessionGenerator;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic c(Lgj;)Lvl1;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->getComponents$lambda-4(Lgj;)Lvl1;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Lgj;)Lzl1;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->getComponents$lambda-2(Lgj;)Lzl1;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Lgj;)Lcom/google/firebase/sessions/FirebaseSessions;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->getComponents$lambda-0(Lgj;)Lcom/google/firebase/sessions/FirebaseSessions;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic f(Lgj;)Lcom/google/firebase/sessions/settings/SessionsSettings;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->getComponents$lambda-3(Lgj;)Lcom/google/firebase/sessions/settings/SessionsSettings;

    move-result-object p0

    return-object p0
.end method

.method private static final getComponents$lambda-0(Lgj;)Lcom/google/firebase/sessions/FirebaseSessions;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/firebase/sessions/FirebaseSessions;

    sget-object v1, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    invoke-interface {p0, v1}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "container[firebaseApp]"

    invoke-static {v1, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lr10;

    sget-object v2, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->sessionsSettings:Lda1;

    invoke-interface {p0, v2}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "container[sessionsSettings]"

    invoke-static {v2, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/google/firebase/sessions/settings/SessionsSettings;

    sget-object v3, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->backgroundDispatcher:Lda1;

    invoke-interface {p0, v3}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    const-string v3, "container[backgroundDispatcher]"

    invoke-static {p0, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lkotlin/coroutines/CoroutineContext;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/firebase/sessions/FirebaseSessions;-><init>(Lr10;Lcom/google/firebase/sessions/settings/SessionsSettings;Lkotlin/coroutines/CoroutineContext;)V

    return-object v0
.end method

.method private static final getComponents$lambda-1(Lgj;)Lcom/google/firebase/sessions/SessionGenerator;
    .locals 3

    .line 1
    new-instance p0, Lcom/google/firebase/sessions/SessionGenerator;

    sget-object v0, Lv52;->a:Lv52;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2, v1}, Lcom/google/firebase/sessions/SessionGenerator;-><init>(Lkw1;La90;ILgq;)V

    return-object p0
.end method

.method private static final getComponents$lambda-2(Lgj;)Lzl1;
    .locals 7

    .line 1
    new-instance v6, Lcom/google/firebase/sessions/SessionFirelogPublisherImpl;

    sget-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    invoke-interface {p0, v0}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "container[firebaseApp]"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Lr10;

    sget-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseInstallationsApi:Lda1;

    invoke-interface {p0, v0}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "container[firebaseInstallationsApi]"

    invoke-static {v0, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Lr20;

    sget-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->sessionsSettings:Lda1;

    invoke-interface {p0, v0}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v0

    const-string v3, "container[sessionsSettings]"

    invoke-static {v0, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/google/firebase/sessions/settings/SessionsSettings;

    new-instance v4, Lox;

    sget-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->transportFactory:Lda1;

    invoke-interface {p0, v0}, Lgj;->g(Lda1;)Lr91;

    move-result-object v0

    const-string v5, "container.getProvider(transportFactory)"

    invoke-static {v0, v5}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v0}, Lox;-><init>(Lr91;)V

    sget-object v0, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->backgroundDispatcher:Lda1;

    invoke-interface {p0, v0}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "container[backgroundDispatcher]"

    invoke-static {p0, v0}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p0

    check-cast v5, Lkotlin/coroutines/CoroutineContext;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/sessions/SessionFirelogPublisherImpl;-><init>(Lr10;Lr20;Lcom/google/firebase/sessions/settings/SessionsSettings;Lpx;Lkotlin/coroutines/CoroutineContext;)V

    return-object v6
.end method

.method private static final getComponents$lambda-3(Lgj;)Lcom/google/firebase/sessions/settings/SessionsSettings;
    .locals 5

    .line 1
    new-instance v0, Lcom/google/firebase/sessions/settings/SessionsSettings;

    sget-object v1, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    invoke-interface {p0, v1}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "container[firebaseApp]"

    invoke-static {v1, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lr10;

    sget-object v2, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->blockingDispatcher:Lda1;

    invoke-interface {p0, v2}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "container[blockingDispatcher]"

    invoke-static {v2, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    sget-object v3, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->backgroundDispatcher:Lda1;

    invoke-interface {p0, v3}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "container[backgroundDispatcher]"

    invoke-static {v3, v4}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lkotlin/coroutines/CoroutineContext;

    sget-object v4, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseInstallationsApi:Lda1;

    invoke-interface {p0, v4}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    const-string v4, "container[firebaseInstallationsApi]"

    invoke-static {p0, v4}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lr20;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/firebase/sessions/settings/SessionsSettings;-><init>(Lr10;Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/CoroutineContext;Lr20;)V

    return-object v0
.end method

.method private static final getComponents$lambda-4(Lgj;)Lvl1;
    .locals 3

    .line 1
    new-instance v0, Lcom/google/firebase/sessions/SessionDatastoreImpl;

    sget-object v1, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    invoke-interface {p0, v1}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr10;

    invoke-virtual {v1}, Lr10;->l()Landroid/content/Context;

    move-result-object v1

    const-string v2, "container[firebaseApp].applicationContext"

    invoke-static {v1, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->backgroundDispatcher:Lda1;

    invoke-interface {p0, v2}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    const-string v2, "container[backgroundDispatcher]"

    invoke-static {p0, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lkotlin/coroutines/CoroutineContext;

    invoke-direct {v0, v1, p0}, Lcom/google/firebase/sessions/SessionDatastoreImpl;-><init>(Landroid/content/Context;Lkotlin/coroutines/CoroutineContext;)V

    return-object v0
.end method

.method private static final getComponents$lambda-5(Lgj;)Lcm1;
    .locals 2

    .line 1
    new-instance v0, Ldm1;

    sget-object v1, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    invoke-interface {p0, v1}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    const-string v1, "container[firebaseApp]"

    invoke-static {p0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lr10;

    invoke-direct {v0, p0}, Ldm1;-><init>(Lr10;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-class v0, Lcom/google/firebase/sessions/FirebaseSessions;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v1, "fire-sessions"

    invoke-virtual {v0, v1}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    sget-object v2, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseApp:Lda1;

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v3

    invoke-virtual {v0, v3}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    sget-object v3, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->sessionsSettings:Lda1;

    invoke-static {v3}, Los;->j(Lda1;)Los;

    move-result-object v4

    invoke-virtual {v0, v4}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    sget-object v4, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->backgroundDispatcher:Lda1;

    invoke-static {v4}, Los;->j(Lda1;)Los;

    move-result-object v5

    invoke-virtual {v0, v5}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v5, Li30;

    invoke-direct {v5}, Li30;-><init>()V

    invoke-virtual {v0, v5}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->e()Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v5

    const-class v0, Lcom/google/firebase/sessions/SessionGenerator;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v6, "session-generator"

    invoke-virtual {v0, v6}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    new-instance v6, Lj30;

    invoke-direct {v6}, Lj30;-><init>()V

    invoke-virtual {v0, v6}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v6

    const-class v0, Lzl1;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v7, "session-publisher"

    invoke-virtual {v0, v7}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v7

    invoke-virtual {v0, v7}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    sget-object v7, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->firebaseInstallationsApi:Lda1;

    invoke-static {v7}, Los;->j(Lda1;)Los;

    move-result-object v8

    invoke-virtual {v0, v8}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v3}, Los;->j(Lda1;)Los;

    move-result-object v3

    invoke-virtual {v0, v3}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    sget-object v3, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->transportFactory:Lda1;

    invoke-static {v3}, Los;->l(Lda1;)Los;

    move-result-object v3

    invoke-virtual {v0, v3}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v4}, Los;->j(Lda1;)Los;

    move-result-object v3

    invoke-virtual {v0, v3}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v3, Lk30;

    invoke-direct {v3}, Lk30;-><init>()V

    invoke-virtual {v0, v3}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    const-class v3, Lcom/google/firebase/sessions/settings/SessionsSettings;

    invoke-static {v3}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v3

    const-string v8, "sessions-settings"

    invoke-virtual {v3, v8}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v3

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v8

    invoke-virtual {v3, v8}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v3

    sget-object v8, Lcom/google/firebase/sessions/FirebaseSessionsRegistrar;->blockingDispatcher:Lda1;

    invoke-static {v8}, Los;->j(Lda1;)Los;

    move-result-object v8

    invoke-virtual {v3, v8}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v3

    invoke-static {v4}, Los;->j(Lda1;)Los;

    move-result-object v8

    invoke-virtual {v3, v8}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v3

    invoke-static {v7}, Los;->j(Lda1;)Los;

    move-result-object v7

    invoke-virtual {v3, v7}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v3

    new-instance v7, Ll30;

    invoke-direct {v7}, Ll30;-><init>()V

    invoke-virtual {v3, v7}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v3

    invoke-virtual {v3}, Lzi$b;->d()Lzi;

    move-result-object v8

    const-class v3, Lvl1;

    invoke-static {v3}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v3

    const-string v7, "sessions-datastore"

    invoke-virtual {v3, v7}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v3

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v7

    invoke-virtual {v3, v7}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v3

    invoke-static {v4}, Los;->j(Lda1;)Los;

    move-result-object v4

    invoke-virtual {v3, v4}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v3

    new-instance v4, Lm30;

    invoke-direct {v4}, Lm30;-><init>()V

    invoke-virtual {v3, v4}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v3

    invoke-virtual {v3}, Lzi$b;->d()Lzi;

    move-result-object v9

    const-class v3, Lcm1;

    invoke-static {v3}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v3

    const-string v4, "sessions-service-binder"

    invoke-virtual {v3, v4}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v3

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v2

    invoke-virtual {v3, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v2

    new-instance v3, Ln30;

    invoke-direct {v3}, Ln30;-><init>()V

    invoke-virtual {v2, v3}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v2

    invoke-virtual {v2}, Lzi$b;->d()Lzi;

    move-result-object v10

    const-string v2, "1.2.0"

    invoke-static {v1, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v11

    move-object v7, v0

    filled-new-array/range {v5 .. v11}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Lnh;->j([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

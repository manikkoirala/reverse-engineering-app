.class public Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/google/firebase/sessions/api/FirebaseSessionsDependencies;->a:Lcom/google/firebase/sessions/api/FirebaseSessionsDependencies;

    sget-object v1, Lcom/google/firebase/sessions/api/SessionSubscriber$Name;->CRASHLYTICS:Lcom/google/firebase/sessions/api/SessionSubscriber$Name;

    invoke-virtual {v0, v1}, Lcom/google/firebase/sessions/api/FirebaseSessionsDependencies;->a(Lcom/google/firebase/sessions/api/SessionSubscriber$Name;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;Lgj;)Lj20;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;->b(Lgj;)Lj20;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final b(Lgj;)Lj20;
    .locals 5

    .line 1
    const-class v0, Lr10;

    invoke-interface {p1, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr10;

    const-class v1, Ldn;

    invoke-interface {p1, v1}, Lgj;->i(Ljava/lang/Class;)Ljr;

    move-result-object v1

    const-class v2, Lg4;

    invoke-interface {p1, v2}, Lgj;->i(Ljava/lang/Class;)Ljr;

    move-result-object v2

    const-class v3, Lr20;

    invoke-interface {p1, v3}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lr20;

    const-class v4, Lf30;

    invoke-interface {p1, v4}, Lgj;->i(Ljava/lang/Class;)Ljr;

    move-result-object p1

    invoke-static {v0, v3, v1, v2, p1}, Lj20;->a(Lr10;Lr20;Ljr;Ljr;Ljr;)Lj20;

    move-result-object p1

    return-object p1
.end method

.method public getComponents()Ljava/util/List;
    .locals 3

    const-class v0, Lj20;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v1, "fire-cls"

    invoke-virtual {v0, v1}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    const-class v2, Lr10;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lr20;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Ldn;

    invoke-static {v2}, Los;->a(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lg4;

    invoke-static {v2}, Los;->a(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lf30;

    invoke-static {v2}, Los;->a(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v2, Lin;

    invoke-direct {v2, p0}, Lin;-><init>(Lcom/google/firebase/crashlytics/CrashlyticsRegistrar;)V

    invoke-virtual {v0, v2}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->e()Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    const-string v2, "18.6.0"

    invoke-static {v1, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v1

    filled-new-array {v0, v1}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/google/firebase/crashlytics/internal/model/a$j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/crashlytics/internal/model/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "j"
.end annotation


# static fields
.field public static final a:Lcom/google/firebase/crashlytics/internal/model/a$j;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;

.field public static final e:Ln00;

.field public static final f:Ln00;

.field public static final g:Ln00;

.field public static final h:Ln00;

.field public static final i:Ln00;

.field public static final j:Ln00;

.field public static final k:Ln00;

.field public static final l:Ln00;

.field public static final m:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/firebase/crashlytics/internal/model/a$j;

    invoke-direct {v0}, Lcom/google/firebase/crashlytics/internal/model/a$j;-><init>()V

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->a:Lcom/google/firebase/crashlytics/internal/model/a$j;

    const-string v0, "generator"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->b:Ln00;

    const-string v0, "identifier"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->c:Ln00;

    const-string v0, "appQualitySessionId"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->d:Ln00;

    const-string v0, "startedAt"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->e:Ln00;

    const-string v0, "endedAt"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->f:Ln00;

    const-string v0, "crashed"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->g:Ln00;

    const-string v0, "app"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->h:Ln00;

    const-string v0, "user"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->i:Ln00;

    const-string v0, "os"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->j:Ln00;

    const-string v0, "device"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->k:Ln00;

    const-string v0, "events"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->l:Ln00;

    const-string v0, "generatorType"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->m:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;Lx01;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->b:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->c:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->j()[B

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->d:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->e:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->l()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lx01;->e(Ln00;J)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->f:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->g:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->n()Z

    move-result v1

    invoke-interface {p2, v0, v1}, Lx01;->b(Ln00;Z)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->h:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->b()Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e$a;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->i:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->m()Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e$f;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->j:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->k()Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e$e;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->k:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->d()Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e$c;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->l:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lcom/google/firebase/crashlytics/internal/model/a$j;->m:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;->h()I

    move-result p1

    invoke-interface {p2, v0, p1}, Lx01;->c(Ln00;I)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/crashlytics/internal/model/a$j;->a(Lcom/google/firebase/crashlytics/internal/model/CrashlyticsReport$e;Lx01;)V

    return-void
.end method

.class public Lcom/google/firebase/messaging/FirebaseMessagingRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation build Lcom/google/android/gms/common/annotation/KeepForSdk;
.end annotation


# static fields
.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-fcm"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lgj;)Lcom/google/firebase/messaging/FirebaseMessaging;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/messaging/FirebaseMessagingRegistrar;->lambda$getComponents$0(Lgj;)Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getComponents$0(Lgj;)Lcom/google/firebase/messaging/FirebaseMessaging;
    .locals 9

    .line 1
    new-instance v8, Lcom/google/firebase/messaging/FirebaseMessaging;

    const-class v0, Lr10;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lr10;

    const-class v0, Lt20;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0}, Lzu0;->a(Ljava/lang/Object;)V

    const-class v0, Lv12;

    invoke-interface {p0, v0}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v3

    const-class v0, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo;

    invoke-interface {p0, v0}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v4

    const-class v0, Lr20;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lr20;

    const-class v0, Lcom/google/android/datatransport/TransportFactory;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/datatransport/TransportFactory;

    const-class v0, Lbs1;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    move-object v7, p0

    check-cast v7, Lbs1;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/google/firebase/messaging/FirebaseMessaging;-><init>(Lr10;Lt20;Lr91;Lr91;Lr20;Lcom/google/android/datatransport/TransportFactory;Lbs1;)V

    return-object v8
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 3
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/google/firebase/messaging/FirebaseMessaging;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v1, "fire-fcm"

    invoke-virtual {v0, v1}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    const-class v2, Lr10;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lt20;

    invoke-static {v2}, Los;->h(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lv12;

    invoke-static {v2}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo;

    invoke-static {v2}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lcom/google/android/datatransport/TransportFactory;

    invoke-static {v2}, Los;->h(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lr20;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lbs1;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v2, Ld30;

    invoke-direct {v2}, Ld30;-><init>()V

    invoke-virtual {v0, v2}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->c()Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    const-string v2, "23.4.0"

    invoke-static {v1, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v1

    filled-new-array {v0, v1}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public interface abstract Lcom/google/firebase/database/collection/LLRBNode;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/database/collection/LLRBNode$Color;
    }
.end annotation


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract c(Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract d(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/firebase/database/collection/LLRBNode$Color;Lcom/google/firebase/database/collection/LLRBNode;Lcom/google/firebase/database/collection/LLRBNode;)Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract getKey()Ljava/lang/Object;
.end method

.method public abstract getLeft()Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract getMax()Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract getMin()Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract getRight()Lcom/google/firebase/database/collection/LLRBNode;
.end method

.method public abstract getValue()Ljava/lang/Object;
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract size()I
.end method

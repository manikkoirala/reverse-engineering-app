.class public Lcom/google/firebase/installations/FirebaseInstallationsRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-installations"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lgj;)Lr20;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/installations/FirebaseInstallationsRegistrar;->lambda$getComponents$0(Lgj;)Lr20;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getComponents$0(Lgj;)Lr20;
    .locals 6

    .line 1
    new-instance v0, Lcom/google/firebase/installations/a;

    const-class v1, Lr10;

    invoke-interface {p0, v1}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr10;

    const-class v2, Lwc0;

    invoke-interface {p0, v2}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v2

    const-class v3, Lza;

    const-class v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v3

    invoke-interface {p0, v3}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    const-class v4, Lfc;

    const-class v5, Ljava/util/concurrent/Executor;

    invoke-static {v4, v5}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v4

    invoke-interface {p0, v4}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, Lcom/google/firebase/concurrent/FirebaseExecutors;->b(Ljava/util/concurrent/Executor;)Ljava/util/concurrent/Executor;

    move-result-object p0

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/firebase/installations/a;-><init>(Lr10;Lr91;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    const-class v0, Lr20;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v1, "fire-installations"

    invoke-virtual {v0, v1}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    const-class v2, Lr10;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lwc0;

    invoke-static {v2}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lza;

    const-class v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v2

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lfc;

    const-class v3, Ljava/util/concurrent/Executor;

    invoke-static {v2, v3}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v2

    invoke-static {v2}, Los;->j(Lda1;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v2, Ls20;

    invoke-direct {v2}, Ls20;-><init>()V

    invoke-virtual {v0, v2}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    invoke-static {}, Lvc0;->a()Lzi;

    move-result-object v2

    const-string v3, "17.2.0"

    invoke-static {v1, v3}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v1

    filled-new-array {v0, v2, v1}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lcom/google/firebase/firestore/model/FieldIndex$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/firestore/model/FieldIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# static fields
.field public static final a:Lcom/google/firebase/firestore/model/FieldIndex$a;

.field public static final b:Ljava/util/Comparator;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    sget-object v0, Lqo1;->b:Lqo1;

    invoke-static {}, Ldu;->d()Ldu;

    move-result-object v1

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/firebase/firestore/model/FieldIndex$a;->d(Lqo1;Ldu;I)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/firestore/model/FieldIndex$a;->a:Lcom/google/firebase/firestore/model/FieldIndex$a;

    new-instance v0, Lp00;

    invoke-direct {v0}, Lp00;-><init>()V

    sput-object v0, Lcom/google/firebase/firestore/model/FieldIndex$a;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/firebase/firestore/model/MutableDocument;Lcom/google/firebase/firestore/model/MutableDocument;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->k(Lcom/google/firebase/firestore/model/MutableDocument;Lcom/google/firebase/firestore/model/MutableDocument;)I

    move-result p0

    return p0
.end method

.method public static d(Lqo1;Ldu;I)Lcom/google/firebase/firestore/model/FieldIndex$a;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/model/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/firebase/firestore/model/b;-><init>(Lqo1;Ldu;I)V

    return-object v0
.end method

.method public static e(Lqo1;I)Lcom/google/firebase/firestore/model/FieldIndex$a;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lqo1;->c()Lpw1;

    move-result-object v0

    invoke-virtual {v0}, Lpw1;->e()J

    move-result-wide v0

    invoke-virtual {p0}, Lqo1;->c()Lpw1;

    move-result-object p0

    invoke-virtual {p0}, Lpw1;->d()I

    move-result p0

    add-int/lit8 p0, p0, 0x1

    new-instance v2, Lqo1;

    int-to-double v3, p0

    const-wide v5, 0x41cdcd6500000000L    # 1.0E9

    cmpl-double v3, v3, v5

    if-nez v3, :cond_0

    new-instance p0, Lpw1;

    const-wide/16 v3, 0x1

    add-long/2addr v0, v3

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3}, Lpw1;-><init>(JI)V

    goto :goto_0

    :cond_0
    new-instance v3, Lpw1;

    invoke-direct {v3, v0, v1, p0}, Lpw1;-><init>(JI)V

    move-object p0, v3

    :goto_0
    invoke-direct {v2, p0}, Lqo1;-><init>(Lpw1;)V

    invoke-static {}, Ldu;->d()Ldu;

    move-result-object p0

    invoke-static {v2, p0, p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->d(Lqo1;Ldu;I)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lzt;)Lcom/google/firebase/firestore/model/FieldIndex$a;
    .locals 2

    .line 1
    invoke-interface {p0}, Lzt;->e()Lqo1;

    move-result-object v0

    invoke-interface {p0}, Lzt;->getKey()Ldu;

    move-result-object p0

    const/4 v1, -0x1

    invoke-static {v0, p0, v1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->d(Lqo1;Ldu;I)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic k(Lcom/google/firebase/firestore/model/MutableDocument;Lcom/google/firebase/firestore/model/MutableDocument;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/model/FieldIndex$a;->f(Lzt;)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object p0

    invoke-static {p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->f(Lzt;)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->c(Lcom/google/firebase/firestore/model/FieldIndex$a;)I

    move-result p0

    return p0
.end method


# virtual methods
.method public c(Lcom/google/firebase/firestore/model/FieldIndex$a;)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/FieldIndex$a;->j()Lqo1;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->j()Lqo1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqo1;->a(Lqo1;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/FieldIndex$a;->g()Ldu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->g()Ldu;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldu;->c(Ldu;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/FieldIndex$a;->h()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->h()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Integer;->compare(II)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/google/firebase/firestore/model/FieldIndex$a;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->c(Lcom/google/firebase/firestore/model/FieldIndex$a;)I

    move-result p1

    return p1
.end method

.method public abstract g()Ldu;
.end method

.method public abstract h()I
.end method

.method public abstract j()Lqo1;
.end method

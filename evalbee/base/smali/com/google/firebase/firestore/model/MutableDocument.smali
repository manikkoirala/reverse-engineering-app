.class public final Lcom/google/firebase/firestore/model/MutableDocument;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;,
        Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;
    }
.end annotation


# instance fields
.field public final b:Ldu;

.field public c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

.field public d:Lqo1;

.field public e:Lqo1;

.field public f:La11;

.field public g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;


# direct methods
.method public constructor <init>(Ldu;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    sget-object p1, Lqo1;->b:Lqo1;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->e:Lqo1;

    return-void
.end method

.method public constructor <init>(Ldu;Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;Lqo1;Lqo1;La11;Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    iput-object p3, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    iput-object p4, p0, Lcom/google/firebase/firestore/model/MutableDocument;->e:Lqo1;

    iput-object p2, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iput-object p6, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iput-object p5, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    return-void
.end method

.method public static p(Ldu;Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-direct {v0, p0}, Lcom/google/firebase/firestore/model/MutableDocument;-><init>(Ldu;)V

    invoke-virtual {v0, p1, p2}, Lcom/google/firebase/firestore/model/MutableDocument;->l(Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p0

    return-object p0
.end method

.method public static q(Ldu;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 8

    .line 1
    new-instance v7, Lcom/google/firebase/firestore/model/MutableDocument;

    sget-object v2, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->INVALID:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    sget-object v4, Lqo1;->b:Lqo1;

    new-instance v5, La11;

    invoke-direct {v5}, La11;-><init>()V

    sget-object v6, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->SYNCED:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    move-object v0, v7

    move-object v1, p0

    move-object v3, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/firebase/firestore/model/MutableDocument;-><init>(Ldu;Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;Lqo1;Lqo1;La11;Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;)V

    return-object v7
.end method

.method public static r(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-direct {v0, p0}, Lcom/google/firebase/firestore/model/MutableDocument;-><init>(Ldu;)V

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->m(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p0

    return-object p0
.end method

.method public static s(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-direct {v0, p0}, Lcom/google/firebase/firestore/model/MutableDocument;-><init>(Ldu;)V

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->n(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 8

    .line 1
    new-instance v7, Lcom/google/firebase/firestore/model/MutableDocument;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    iget-object v2, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iget-object v3, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    iget-object v4, p0, Lcom/google/firebase/firestore/model/MutableDocument;->e:Lqo1;

    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    invoke-virtual {v0}, La11;->d()La11;

    move-result-object v5

    iget-object v6, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/firebase/firestore/model/MutableDocument;-><init>(Ldu;Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;Lqo1;Lqo1;La11;Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;)V

    return-object v7
.end method

.method public b()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public c()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    sget-object v1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->NO_DOCUMENT:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    sget-object v1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->FOUND_DOCUMENT:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public e()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->e:Lqo1;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_6

    const-class v1, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/firebase/firestore/model/MutableDocument;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    iget-object v2, p1, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    invoke-virtual {v1, v2}, Ldu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v0

    :cond_2
    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    iget-object v2, p1, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    invoke-virtual {v1, v2}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    :cond_3
    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iget-object v2, p1, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    return v0

    :cond_4
    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iget-object v2, p1, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    return v0

    :cond_5
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    iget-object p1, p1, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    invoke-virtual {v0, p1}, La11;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_6
    :goto_0
    return v0
.end method

.method public f()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    sget-object v1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->HAS_COMMITTED_MUTATIONS:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    sget-object v1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->HAS_LOCAL_MUTATIONS:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getData()La11;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    return-object v0
.end method

.method public getKey()Ldu;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    return-object v0
.end method

.method public getVersion()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    invoke-virtual {v0}, Ldu;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    sget-object v1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->UNKNOWN_DOCUMENT:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public j(Ls00;)Lcom/google/firestore/v1/Value;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object v0

    invoke-virtual {v0, p1}, La11;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1
.end method

.method public l(Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    sget-object p1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->FOUND_DOCUMENT:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iput-object p2, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    sget-object p1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->SYNCED:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    return-object p0
.end method

.method public m(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    sget-object p1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->NO_DOCUMENT:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    new-instance p1, La11;

    invoke-direct {p1}, La11;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    sget-object p1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->SYNCED:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    return-object p0
.end method

.method public n(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    sget-object p1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->UNKNOWN_DOCUMENT:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    new-instance p1, La11;

    invoke-direct {p1}, La11;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    sget-object p1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->HAS_COMMITTED_MUTATIONS:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    return-object p0
.end method

.method public o()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    sget-object v1, Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;->INVALID:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public t()Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->HAS_COMMITTED_MUTATIONS:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iput-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Document{key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->b:Ldu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", readTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->e:Lqo1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->c:Lcom/google/firebase/firestore/model/MutableDocument$DocumentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", documentState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->f:La11;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;->HAS_LOCAL_MUTATIONS:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    iput-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->g:Lcom/google/firebase/firestore/model/MutableDocument$DocumentState;

    sget-object v0, Lqo1;->b:Lqo1;

    iput-object v0, p0, Lcom/google/firebase/firestore/model/MutableDocument;->d:Lqo1;

    return-object p0
.end method

.method public v(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/model/MutableDocument;->e:Lqo1;

    return-object p0
.end method

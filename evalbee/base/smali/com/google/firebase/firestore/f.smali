.class public Lcom/google/firebase/firestore/f;
.super Lcom/google/firebase/firestore/DocumentSnapshot;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/FirebaseFirestore;Ldu;Lzt;ZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/google/firebase/firestore/DocumentSnapshot;-><init>(Lcom/google/firebase/firestore/FirebaseFirestore;Ldu;Lzt;ZZ)V

    return-void
.end method

.method public static g(Lcom/google/firebase/firestore/FirebaseFirestore;Lzt;ZZ)Lcom/google/firebase/firestore/f;
    .locals 7

    .line 1
    new-instance v6, Lcom/google/firebase/firestore/f;

    invoke-interface {p1}, Lzt;->getKey()Ldu;

    move-result-object v2

    move-object v0, v6

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/f;-><init>(Lcom/google/firebase/firestore/FirebaseFirestore;Ldu;Lzt;ZZ)V

    return-object v6
.end method


# virtual methods
.method public d()Ljava/util/Map;
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/google/firebase/firestore/DocumentSnapshot;->d()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    const-string v3, "Data in a QueryDocumentSnapshot should be non-null"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public e(Lcom/google/firebase/firestore/DocumentSnapshot$ServerTimestampBehavior;)Ljava/util/Map;
    .locals 3

    .line 1
    const-string v0, "Provided serverTimestampBehavior value must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/firebase/firestore/DocumentSnapshot;->e(Lcom/google/firebase/firestore/DocumentSnapshot$ServerTimestampBehavior;)Ljava/util/Map;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    const-string v2, "Data in a QueryDocumentSnapshot should be non-null"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-object p1
.end method

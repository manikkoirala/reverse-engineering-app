.class public Lcom/google/firebase/firestore/core/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/firestore/remote/g$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/core/p$c;,
        Lcom/google/firebase/firestore/core/p$b;
    }
.end annotation


# static fields
.field public static final o:Ljava/lang/String; = "p"


# instance fields
.field public final a:Lcom/google/firebase/firestore/local/a;

.field public final b:Lcom/google/firebase/firestore/remote/g;

.field public final c:Ljava/util/Map;

.field public final d:Ljava/util/Map;

.field public final e:I

.field public final f:Ljava/util/LinkedHashSet;

.field public final g:Ljava/util/Map;

.field public final h:Ljava/util/Map;

.field public final i:Lxc1;

.field public final j:Ljava/util/Map;

.field public final k:Ljava/util/Map;

.field public final l:Lbu1;

.field public m:Lu12;

.field public n:Lcom/google/firebase/firestore/core/p$c;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/remote/g;Lu12;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    iput-object p2, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    iput p4, p0, Lcom/google/firebase/firestore/core/p;->e:I

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->f:Ljava/util/LinkedHashSet;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    new-instance p1, Lxc1;

    invoke-direct {p1}, Lxc1;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->j:Ljava/util/Map;

    invoke-static {}, Lbu1;->a()Lbu1;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->l:Lbu1;

    iput-object p3, p0, Lcom/google/firebase/firestore/core/p;->m:Lu12;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->k:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/core/OnlineState;)V
    .locals 6

    .line 1
    const-string v0, "handleOnlineStateChange"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lka1;

    invoke-virtual {v2}, Lka1;->c()Lcom/google/firebase/firestore/core/r;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/firebase/firestore/core/r;->e(Lcom/google/firebase/firestore/core/OnlineState;)Lm32;

    move-result-object v2

    invoke-virtual {v2}, Lm32;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "OnlineState should not affect limbo documents."

    invoke-static {v3, v5, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lm32;->b()Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lm32;->b()Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    invoke-interface {v1, v0}, Lcom/google/firebase/firestore/core/p$c;->c(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    invoke-interface {v0, p1}, Lcom/google/firebase/firestore/core/p$c;->a(Lcom/google/firebase/firestore/core/OnlineState;)V

    return-void
.end method

.method public b(I)Lcom/google/firebase/database/collection/c;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/core/p$b;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/firebase/firestore/core/p$b;->a(Lcom/google/firebase/firestore/core/p$b;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ldu;->e()Lcom/google/firebase/database/collection/c;

    move-result-object p1

    invoke-static {v0}, Lcom/google/firebase/firestore/core/p$b;->c(Lcom/google/firebase/firestore/core/p$b;)Ldu;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {}, Ldu;->e()Lcom/google/firebase/database/collection/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/core/Query;

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lka1;

    invoke-virtual {v1}, Lka1;->c()Lcom/google/firebase/firestore/core/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/firestore/core/r;->k()Lcom/google/firebase/database/collection/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/database/collection/c;->l(Lcom/google/firebase/database/collection/c;)Lcom/google/firebase/database/collection/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public c(ILio/grpc/Status;)V
    .locals 7

    .line 1
    const-string v0, "handleRejectedListen"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/core/p$b;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/firebase/firestore/core/p$b;->c(Lcom/google/firebase/firestore/core/p$b;)Ldu;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object p2, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/p;->q()V

    sget-object v2, Lqo1;->b:Lqo1;

    invoke-static {v0, v2}, Lcom/google/firebase/firestore/model/MutableDocument;->r(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v6

    new-instance p1, Ljd1;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v4

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Ljd1;-><init>(Lqo1;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;)V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/p;->d(Ljd1;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->P(I)V

    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/core/p;->r(ILio/grpc/Status;)V

    :goto_1
    return-void
.end method

.method public d(Ljd1;)V
    .locals 8

    .line 1
    const-string v0, "handleRemoteEvent"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljd1;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lzt1;

    iget-object v3, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/firestore/core/p$b;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lzt1;->b()Lcom/google/firebase/database/collection/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v3

    invoke-virtual {v1}, Lzt1;->c()Lcom/google/firebase/database/collection/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v1}, Lzt1;->d()Lcom/google/firebase/database/collection/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v4

    add-int/2addr v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-gt v3, v4, :cond_1

    move v3, v4

    goto :goto_1

    :cond_1
    move v3, v5

    :goto_1
    const-string v6, "Limbo resolution for single document contains multiple changes."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Lzt1;->b()Lcom/google/firebase/database/collection/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-static {v2, v4}, Lcom/google/firebase/firestore/core/p$b;->b(Lcom/google/firebase/firestore/core/p$b;Z)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lzt1;->c()Lcom/google/firebase/database/collection/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-static {v2}, Lcom/google/firebase/firestore/core/p$b;->a(Lcom/google/firebase/firestore/core/p$b;)Z

    move-result v1

    const-string v2, "Received change for limbo target document without add."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lzt1;->d()Lcom/google/firebase/database/collection/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {v2}, Lcom/google/firebase/firestore/core/p$b;->a(Lcom/google/firebase/firestore/core/p$b;)Z

    move-result v1

    const-string v3, "Received remove for limbo target document without add."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2, v5}, Lcom/google/firebase/firestore/core/p$b;->b(Lcom/google/firebase/firestore/core/p$b;Z)Z

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->n(Ljd1;)Lcom/google/firebase/database/collection/b;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/core/p;->i(Lcom/google/firebase/database/collection/b;Ljd1;)V

    return-void
.end method

.method public e(Lyx0;)V
    .locals 2

    .line 1
    const-string v0, "handleSuccessfulWrite"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    invoke-virtual {p1}, Lyx0;->b()Lxx0;

    move-result-object v0

    invoke-virtual {v0}, Lxx0;->e()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/firebase/firestore/core/p;->p(ILio/grpc/Status;)V

    invoke-virtual {p1}, Lyx0;->b()Lxx0;

    move-result-object v0

    invoke-virtual {v0}, Lxx0;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->t(I)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->l(Lyx0;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    invoke-virtual {p0, p1, v1}, Lcom/google/firebase/firestore/core/p;->i(Lcom/google/firebase/database/collection/b;Ljd1;)V

    return-void
.end method

.method public f(ILio/grpc/Status;)V
    .locals 3

    .line 1
    const-string v0, "handleRejectedWrite"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->O(I)Lcom/google/firebase/database/collection/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/database/collection/b;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/firebase/database/collection/b;->i()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldu;

    invoke-virtual {v1}, Ldu;->m()Lke1;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Write failed at %s"

    invoke-virtual {p0, p2, v2, v1}, Lcom/google/firebase/firestore/core/p;->o(Lio/grpc/Status;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/core/p;->p(ILio/grpc/Status;)V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/p;->t(I)V

    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/core/p;->i(Lcom/google/firebase/database/collection/b;Ljd1;)V

    return-void
.end method

.method public final g(ILcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->j:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->m:Lu12;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->j:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->m:Lu12;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Trying to call %s before setting callback"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final i(Lcom/google/firebase/database/collection/b;Ljd1;)V
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lka1;

    invoke-virtual {v3}, Lka1;->c()Lcom/google/firebase/firestore/core/r;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/firebase/firestore/core/r;->h(Lcom/google/firebase/database/collection/b;)Lcom/google/firebase/firestore/core/r$b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/firebase/firestore/core/r$b;->b()Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v3}, Lka1;->a()Lcom/google/firebase/firestore/core/Query;

    move-result-object v8

    invoke-virtual {v6, v8, v7}, Lcom/google/firebase/firestore/local/a;->q(Lcom/google/firebase/firestore/core/Query;Z)Lja1;

    move-result-object v6

    invoke-virtual {v6}, Lja1;->a()Lcom/google/firebase/database/collection/b;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Lcom/google/firebase/firestore/core/r;->i(Lcom/google/firebase/database/collection/b;Lcom/google/firebase/firestore/core/r$b;)Lcom/google/firebase/firestore/core/r$b;

    move-result-object v5

    :cond_1
    if-nez p2, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljd1;->d()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v3}, Lka1;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lzt1;

    :goto_1
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljd1;->e()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v3}, Lka1;->b()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_3

    const/4 v7, 0x1

    :cond_3
    invoke-virtual {v3}, Lka1;->c()Lcom/google/firebase/firestore/core/r;

    move-result-object v6

    invoke-virtual {v6, v5, v4, v7}, Lcom/google/firebase/firestore/core/r;->d(Lcom/google/firebase/firestore/core/r$b;Lzt1;Z)Lm32;

    move-result-object v4

    invoke-virtual {v4}, Lm32;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3}, Lka1;->b()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/google/firebase/firestore/core/p;->x(Ljava/util/List;I)V

    invoke-virtual {v4}, Lm32;->b()Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lm32;->b()Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lka1;->b()I

    move-result v3

    invoke-virtual {v4}, Lm32;->b()Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v4

    invoke-static {v3, v4}, Lml0;->a(ILcom/google/firebase/firestore/core/ViewSnapshot;)Lml0;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    invoke-interface {p1, v0}, Lcom/google/firebase/firestore/core/p$c;->c(Ljava/util/List;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p1, v1}, Lcom/google/firebase/firestore/local/a;->L(Ljava/util/List;)V

    return-void
.end method

.method public final j(Lio/grpc/Status;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lio/grpc/Status;->getCode()Lio/grpc/Status$Code;

    move-result-object v0

    invoke-virtual {p1}, Lio/grpc/Status;->getDescription()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lio/grpc/Status;->getDescription()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    sget-object v1, Lio/grpc/Status$Code;->FAILED_PRECONDITION:Lio/grpc/Status$Code;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    const-string v1, "requires an index"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v2

    :cond_1
    sget-object p1, Lio/grpc/Status$Code;->PERMISSION_DENIED:Lio/grpc/Status$Code;

    if-ne v0, p1, :cond_2

    return v2

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public final k()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/tasks/TaskCompletionSource;

    new-instance v3, Lcom/google/firebase/firestore/FirebaseFirestoreException;

    const-string v4, "\'waitForPendingWrites\' task is cancelled due to User change."

    sget-object v5, Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;->CANCELLED:Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;

    invoke-direct {v3, v4, v5}, Lcom/google/firebase/firestore/FirebaseFirestoreException;-><init>(Ljava/lang/String;Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public l(Lu12;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->m:Lu12;

    invoke-virtual {v0, p1}, Lu12;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->m:Lu12;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/p;->k()V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->y(Lu12;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/firebase/firestore/core/p;->i(Lcom/google/firebase/database/collection/b;Ljd1;)V

    :cond_0
    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/g;->t()V

    return-void
.end method

.method public final m(Lcom/google/firebase/firestore/core/Query;ILcom/google/protobuf/ByteString;)Lcom/google/firebase/firestore/core/ViewSnapshot;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/firebase/firestore/local/a;->q(Lcom/google/firebase/firestore/core/Query;Z)Lja1;

    move-result-object v0

    sget-object v2, Lcom/google/firebase/firestore/core/ViewSnapshot$SyncState;->NONE:Lcom/google/firebase/firestore/core/ViewSnapshot$SyncState;

    iget-object v3, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/firestore/core/Query;

    iget-object v3, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lka1;

    invoke-virtual {v2}, Lka1;->c()Lcom/google/firebase/firestore/core/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/firestore/core/r;->j()Lcom/google/firebase/firestore/core/ViewSnapshot$SyncState;

    move-result-object v2

    :cond_0
    sget-object v3, Lcom/google/firebase/firestore/core/ViewSnapshot$SyncState;->SYNCED:Lcom/google/firebase/firestore/core/ViewSnapshot$SyncState;

    if-ne v2, v3, :cond_1

    move v4, v1

    :cond_1
    invoke-static {v4, p3}, Lzt1;->a(ZLcom/google/protobuf/ByteString;)Lzt1;

    move-result-object p3

    new-instance v2, Lcom/google/firebase/firestore/core/r;

    invoke-virtual {v0}, Lja1;->b()Lcom/google/firebase/database/collection/c;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/google/firebase/firestore/core/r;-><init>(Lcom/google/firebase/firestore/core/Query;Lcom/google/firebase/database/collection/c;)V

    invoke-virtual {v0}, Lja1;->a()Lcom/google/firebase/database/collection/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/firebase/firestore/core/r;->h(Lcom/google/firebase/database/collection/b;)Lcom/google/firebase/firestore/core/r$b;

    move-result-object v0

    invoke-virtual {v2, v0, p3}, Lcom/google/firebase/firestore/core/r;->c(Lcom/google/firebase/firestore/core/r$b;Lzt1;)Lm32;

    move-result-object p3

    invoke-virtual {p3}, Lm32;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/firebase/firestore/core/p;->x(Ljava/util/List;I)V

    new-instance v0, Lka1;

    invoke-direct {v0, p1, p2, v2}, Lka1;-><init>(Lcom/google/firebase/firestore/core/Query;ILcom/google/firebase/firestore/core/r;)V

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3}, Lm32;->b()Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object p1

    return-object p1
.end method

.method public n(Lcom/google/firebase/firestore/core/Query;)I
    .locals 3

    .line 1
    const-string v0, "listen"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "We already listen to query: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/Query;->x()Lcom/google/firebase/firestore/core/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/a;->m(Lcom/google/firebase/firestore/core/q;)Lau1;

    move-result-object v0

    invoke-virtual {v0}, Lau1;->h()I

    move-result v1

    invoke-virtual {v0}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/firebase/firestore/core/p;->m(Lcom/google/firebase/firestore/core/Query;ILcom/google/protobuf/ByteString;)Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object p1

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/google/firebase/firestore/core/p$c;->c(Ljava/util/List;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/remote/g;->E(Lau1;)V

    invoke-virtual {v0}, Lau1;->h()I

    move-result p1

    return p1
.end method

.method public final varargs o(Lio/grpc/Status;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/p;->j(Lio/grpc/Status;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "%s: %s"

    filled-new-array {p2, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "Firestore"

    invoke-static {p2, p3, p1}, Lcom/google/firebase/firestore/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final p(ILio/grpc/Status;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->j:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->m:Lu12;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    invoke-static {p2}, Lo22;->r(Lio/grpc/Status;)Lcom/google/firebase/firestore/FirebaseFirestoreException;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-virtual {v1, p2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setResult(Ljava/lang/Object;)V

    :goto_0
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public final q()V
    .locals 9

    .line 1
    :goto_0
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v1, p0, Lcom/google/firebase/firestore/core/p;->e:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldu;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->l:Lbu1;

    invoke-virtual {v0}, Lbu1;->c()I

    move-result v4

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/firebase/firestore/core/p$b;

    invoke-direct {v3, v1}, Lcom/google/firebase/firestore/core/p$b;-><init>(Ldu;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    new-instance v8, Lau1;

    invoke-virtual {v1}, Ldu;->m()Lke1;

    move-result-object v1

    invoke-static {v1}, Lcom/google/firebase/firestore/core/Query;->b(Lke1;)Lcom/google/firebase/firestore/core/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/firestore/core/Query;->x()Lcom/google/firebase/firestore/core/q;

    move-result-object v3

    const-wide/16 v5, -0x1

    sget-object v7, Lcom/google/firebase/firestore/local/QueryPurpose;->LIMBO_RESOLUTION:Lcom/google/firebase/firestore/local/QueryPurpose;

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;)V

    invoke-virtual {v0, v8}, Lcom/google/firebase/firestore/remote/g;->E(Lau1;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final r(ILio/grpc/Status;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/core/Query;

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lio/grpc/Status;->isOk()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    invoke-interface {v2, v1, p2}, Lcom/google/firebase/firestore/core/p$c;->b(Lcom/google/firebase/firestore/core/Query;Lio/grpc/Status;)V

    const-string v2, "Listen for %s failed"

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p2, v2, v1}, Lcom/google/firebase/firestore/core/p;->o(Lio/grpc/Status;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    invoke-virtual {p2, p1}, Lxc1;->d(I)Lcom/google/firebase/database/collection/c;

    move-result-object p2

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    invoke-virtual {v0, p1}, Lxc1;->h(I)Lcom/google/firebase/database/collection/c;

    invoke-virtual {p2}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ldu;

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    invoke-virtual {v0, p2}, Lxc1;->c(Ldu;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, p2}, Lcom/google/firebase/firestore/core/p;->s(Ldu;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final s(Ldu;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/g;->P(I)V

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->h:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/p;->q()V

    :cond_0
    return-void
.end method

.method public final t(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->k:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->k:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setResult(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->k:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public u(Lcom/google/firebase/firestore/core/p$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/core/p;->n:Lcom/google/firebase/firestore/core/p$c;

    return-void
.end method

.method public v(Lcom/google/firebase/firestore/core/Query;)V
    .locals 4

    .line 1
    const-string v0, "stopListening"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lka1;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    const-string v3, "Trying to stop listening to a query not found"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lka1;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->d:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/local/a;->P(I)V

    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/remote/g;->P(I)V

    sget-object p1, Lio/grpc/Status;->OK:Lio/grpc/Status;

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/core/p;->r(ILio/grpc/Status;)V

    :cond_1
    return-void
.end method

.method public final w(Lcom/google/firebase/firestore/core/LimboDocumentChange;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/LimboDocumentChange;->a()Ldu;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/firebase/firestore/core/p;->o:Ljava/lang/String;

    const-string v1, "New document in limbo: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->f:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/p;->q()V

    :cond_0
    return-void
.end method

.method public final x(Ljava/util/List;I)V
    .locals 4

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/core/LimboDocumentChange;

    sget-object v1, Lcom/google/firebase/firestore/core/p$a;->a:[I

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/LimboDocumentChange;->b()Lcom/google/firebase/firestore/core/LimboDocumentChange$Type;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/google/firebase/firestore/core/p;->o:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/LimboDocumentChange;->a()Ldu;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Document no longer in limbo: %s"

    invoke-static {v1, v3, v2}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/LimboDocumentChange;->a()Ldu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    invoke-virtual {v1, v0, p2}, Lxc1;->e(Ldu;I)V

    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    invoke-virtual {v1, v0}, Lxc1;->c(Ldu;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->s(Ldu;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/LimboDocumentChange;->b()Lcom/google/firebase/firestore/core/LimboDocumentChange$Type;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "Unknown limbo change type: %s"

    invoke-static {p2, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_2
    iget-object v1, p0, Lcom/google/firebase/firestore/core/p;->i:Lxc1;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/LimboDocumentChange;->a()Ldu;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lxc1;->a(Ldu;I)V

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->w(Lcom/google/firebase/firestore/core/LimboDocumentChange;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public y(Ljava/util/List;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 1

    .line 1
    const-string v0, "writeMutations"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/p;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/p;->a:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->V(Ljava/util/List;)Lwk0;

    move-result-object p1

    invoke-virtual {p1}, Lwk0;->b()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/google/firebase/firestore/core/p;->g(ILcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {p1}, Lwk0;->c()Lcom/google/firebase/database/collection/b;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/core/p;->i(Lcom/google/firebase/database/collection/b;Ljd1;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/core/p;->b:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/g;->s()V

    return-void
.end method

.class public Lcom/google/firebase/firestore/core/o;
.super Lcom/google/firebase/firestore/core/l;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/firebase/firestore/core/l;-><init>()V

    return-void
.end method


# virtual methods
.method public c(Lcom/google/firebase/firestore/core/d$a;)Lhj1;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->n()Ld51;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m;->x()Lcom/google/firebase/firestore/local/j;

    move-result-object v0

    invoke-interface {v0}, Lkm0;->b()Lcom/google/firebase/firestore/local/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->a()Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object p1

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->m()Lcom/google/firebase/firestore/local/a;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/firebase/firestore/local/b;->j(Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/local/a;)Lcom/google/firebase/firestore/local/b$a;

    move-result-object p1

    return-object p1
.end method

.method public d(Lcom/google/firebase/firestore/core/d$a;)Lwe0;
    .locals 3

    .line 1
    new-instance v0, Lwe0;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->n()Ld51;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->a()Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object p1

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->m()Lcom/google/firebase/firestore/local/a;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lwe0;-><init>(Ld51;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/local/a;)V

    return-object v0
.end method

.method public f(Lcom/google/firebase/firestore/core/d$a;)Ld51;
    .locals 7

    .line 1
    new-instance v4, Lzk0;

    new-instance v0, Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->c()Lrp;

    move-result-object v1

    invoke-virtual {v1}, Lrp;->a()Lqp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/firebase/firestore/remote/f;-><init>(Lqp;)V

    invoke-direct {v4, v0}, Lzk0;-><init>(Lcom/google/firebase/firestore/remote/f;)V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->g()Lcom/google/firebase/firestore/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/b;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/firebase/firestore/local/b$b;->a(J)Lcom/google/firebase/firestore/local/b$b;

    move-result-object v5

    new-instance v6, Lcom/google/firebase/firestore/local/m;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->c()Lrp;

    move-result-object v0

    invoke-virtual {v0}, Lrp;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->c()Lrp;

    move-result-object p1

    invoke-virtual {p1}, Lrp;->a()Lqp;

    move-result-object v3

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/local/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lqp;Lzk0;Lcom/google/firebase/firestore/local/b$b;)V

    return-object v6
.end method

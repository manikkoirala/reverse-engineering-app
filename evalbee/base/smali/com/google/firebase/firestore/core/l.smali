.class public Lcom/google/firebase/firestore/core/l;
.super Lcom/google/firebase/firestore/core/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/core/l$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/firebase/firestore/core/d;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/ConnectivityMonitor;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/l;->r(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/a;

    move-result-object p1

    return-object p1
.end method

.method public b(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/core/f;
    .locals 1

    .line 1
    new-instance p1, Lcom/google/firebase/firestore/core/f;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/google/firebase/firestore/core/f;-><init>(Lcom/google/firebase/firestore/core/p;)V

    return-object p1
.end method

.method public c(Lcom/google/firebase/firestore/core/d$a;)Lhj1;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public d(Lcom/google/firebase/firestore/core/d$a;)Lwe0;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public e(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/local/a;
    .locals 3

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->n()Ld51;

    move-result-object v1

    new-instance v2, Lcom/google/firebase/firestore/local/f;

    invoke-direct {v2}, Lcom/google/firebase/firestore/local/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->e()Lu12;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/google/firebase/firestore/local/a;-><init>(Ld51;Lcom/google/firebase/firestore/local/f;Lu12;)V

    return-object v0
.end method

.method public f(Lcom/google/firebase/firestore/core/d$a;)Ld51;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->g()Lcom/google/firebase/firestore/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/core/l;->s(Lcom/google/firebase/firestore/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lzk0;

    new-instance v1, Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->c()Lrp;

    move-result-object v2

    invoke-virtual {v2}, Lrp;->a()Lqp;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/firebase/firestore/remote/f;-><init>(Lqp;)V

    invoke-direct {v0, v1}, Lzk0;-><init>(Lcom/google/firebase/firestore/remote/f;)V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->g()Lcom/google/firebase/firestore/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/b;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/firebase/firestore/local/b$b;->a(J)Lcom/google/firebase/firestore/local/b$b;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/google/firebase/firestore/local/e;->n(Lcom/google/firebase/firestore/local/b$b;Lzk0;)Lcom/google/firebase/firestore/local/e;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {}, Lcom/google/firebase/firestore/local/e;->m()Lcom/google/firebase/firestore/local/e;

    move-result-object p1

    return-object p1
.end method

.method public g(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/g;
    .locals 7

    .line 1
    new-instance v6, Lcom/google/firebase/firestore/remote/g;

    new-instance v1, Lcom/google/firebase/firestore/core/l$b;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/firebase/firestore/core/l$b;-><init>(Lcom/google/firebase/firestore/core/l;Lcom/google/firebase/firestore/core/l$a;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->m()Lcom/google/firebase/firestore/local/a;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->d()Lcom/google/firebase/firestore/remote/d;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->a()Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->i()Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/remote/g;-><init>(Lcom/google/firebase/firestore/remote/g$c;Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/remote/d;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/ConnectivityMonitor;)V

    return-object v6
.end method

.method public h(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/core/p;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/core/p;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->m()Lcom/google/firebase/firestore/local/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/d;->o()Lcom/google/firebase/firestore/remote/g;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->e()Lu12;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->f()I

    move-result p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/firebase/firestore/core/p;-><init>(Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/remote/g;Lu12;I)V

    return-object v0
.end method

.method public r(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/a;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/remote/a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d$a;->b()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/google/firebase/firestore/remote/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final s(Lcom/google/firebase/firestore/b;)Z
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/b;->a()Lvk0;

    const/4 p1, 0x0

    return p1
.end method

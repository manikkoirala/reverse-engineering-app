.class public Lcom/google/firebase/firestore/core/d$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/firestore/core/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final c:Lrp;

.field public final d:Lcom/google/firebase/firestore/remote/d;

.field public final e:Lu12;

.field public final f:I

.field public final g:Lcom/google/firebase/firestore/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/firebase/firestore/util/AsyncQueue;Lrp;Lcom/google/firebase/firestore/remote/d;Lu12;ILcom/google/firebase/firestore/b;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/core/d$a;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/firebase/firestore/core/d$a;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p3, p0, Lcom/google/firebase/firestore/core/d$a;->c:Lrp;

    iput-object p4, p0, Lcom/google/firebase/firestore/core/d$a;->d:Lcom/google/firebase/firestore/remote/d;

    iput-object p5, p0, Lcom/google/firebase/firestore/core/d$a;->e:Lu12;

    iput p6, p0, Lcom/google/firebase/firestore/core/d$a;->f:I

    iput-object p7, p0, Lcom/google/firebase/firestore/core/d$a;->g:Lcom/google/firebase/firestore/b;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/firebase/firestore/util/AsyncQueue;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d$a;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    return-object v0
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method public c()Lrp;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d$a;->c:Lrp;

    return-object v0
.end method

.method public d()Lcom/google/firebase/firestore/remote/d;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d$a;->d:Lcom/google/firebase/firestore/remote/d;

    return-object v0
.end method

.method public e()Lu12;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d$a;->e:Lu12;

    return-object v0
.end method

.method public f()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/firebase/firestore/core/d$a;->f:I

    return v0
.end method

.method public g()Lcom/google/firebase/firestore/b;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d$a;->g:Lcom/google/firebase/firestore/b;

    return-object v0
.end method

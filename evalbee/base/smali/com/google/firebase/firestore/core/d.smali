.class public abstract Lcom/google/firebase/firestore/core/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/core/d$a;
    }
.end annotation


# instance fields
.field public a:Ld51;

.field public b:Lcom/google/firebase/firestore/local/a;

.field public c:Lcom/google/firebase/firestore/core/p;

.field public d:Lcom/google/firebase/firestore/remote/g;

.field public e:Lcom/google/firebase/firestore/core/f;

.field public f:Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

.field public g:Lwe0;

.field public h:Lhj1;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/ConnectivityMonitor;
.end method

.method public abstract b(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/core/f;
.end method

.method public abstract c(Lcom/google/firebase/firestore/core/d$a;)Lhj1;
.end method

.method public abstract d(Lcom/google/firebase/firestore/core/d$a;)Lwe0;
.end method

.method public abstract e(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/local/a;
.end method

.method public abstract f(Lcom/google/firebase/firestore/core/d$a;)Ld51;
.end method

.method public abstract g(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/g;
.end method

.method public abstract h(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/core/p;
.end method

.method public i()Lcom/google/firebase/firestore/remote/ConnectivityMonitor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->f:Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "connectivityMonitor not initialized yet"

    invoke-static {v0, v2, v1}, Lg9;->e(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

    return-object v0
.end method

.method public j()Lcom/google/firebase/firestore/core/f;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->e:Lcom/google/firebase/firestore/core/f;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "eventManager not initialized yet"

    invoke-static {v0, v2, v1}, Lg9;->e(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/core/f;

    return-object v0
.end method

.method public k()Lhj1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->h:Lhj1;

    return-object v0
.end method

.method public l()Lwe0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->g:Lwe0;

    return-object v0
.end method

.method public m()Lcom/google/firebase/firestore/local/a;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->b:Lcom/google/firebase/firestore/local/a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "localStore not initialized yet"

    invoke-static {v0, v2, v1}, Lg9;->e(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/local/a;

    return-object v0
.end method

.method public n()Ld51;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->a:Ld51;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "persistence not initialized yet"

    invoke-static {v0, v2, v1}, Lg9;->e(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld51;

    return-object v0
.end method

.method public o()Lcom/google/firebase/firestore/remote/g;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->d:Lcom/google/firebase/firestore/remote/g;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "remoteStore not initialized yet"

    invoke-static {v0, v2, v1}, Lg9;->e(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/remote/g;

    return-object v0
.end method

.method public p()Lcom/google/firebase/firestore/core/p;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->c:Lcom/google/firebase/firestore/core/p;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "syncEngine not initialized yet"

    invoke-static {v0, v2, v1}, Lg9;->e(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/core/p;

    return-object v0
.end method

.method public q(Lcom/google/firebase/firestore/core/d$a;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->f(Lcom/google/firebase/firestore/core/d$a;)Ld51;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->a:Ld51;

    invoke-virtual {v0}, Ld51;->l()V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->e(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/local/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->b:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->a(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->f:Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->g(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/remote/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->d:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->h(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->c:Lcom/google/firebase/firestore/core/p;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->b(Lcom/google/firebase/firestore/core/d$a;)Lcom/google/firebase/firestore/core/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->e:Lcom/google/firebase/firestore/core/f;

    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->b:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/a;->S()V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/d;->d:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/g;->M()V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->c(Lcom/google/firebase/firestore/core/d$a;)Lhj1;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/core/d;->h:Lhj1;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/core/d;->d(Lcom/google/firebase/firestore/core/d$a;)Lwe0;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/core/d;->g:Lwe0;

    return-void
.end method

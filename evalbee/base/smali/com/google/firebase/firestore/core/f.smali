.class public final Lcom/google/firebase/firestore/core/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/firestore/core/p$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/core/f$a;,
        Lcom/google/firebase/firestore/core/f$b;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/firebase/firestore/core/p;

.field public final b:Ljava/util/Map;

.field public final c:Ljava/util/Set;

.field public d:Lcom/google/firebase/firestore/core/OnlineState;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/core/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/core/f;->c:Ljava/util/Set;

    sget-object v0, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    iput-object v0, p0, Lcom/google/firebase/firestore/core/f;->d:Lcom/google/firebase/firestore/core/OnlineState;

    iput-object p1, p0, Lcom/google/firebase/firestore/core/f;->a:Lcom/google/firebase/firestore/core/p;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-virtual {p1, p0}, Lcom/google/firebase/firestore/core/p;->u(Lcom/google/firebase/firestore/core/p$c;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/core/OnlineState;)V
    .locals 4

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/core/f;->d:Lcom/google/firebase/firestore/core/OnlineState;

    iget-object v0, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/firestore/core/f$b;

    invoke-static {v2}, Lcom/google/firebase/firestore/core/f$b;->a(Lcom/google/firebase/firestore/core/f$b;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/firebase/firestore/core/n;

    invoke-virtual {v3, p1}, Lcom/google/firebase/firestore/core/n;->c(Lcom/google/firebase/firestore/core/OnlineState;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/f;->e()V

    :cond_3
    return-void
.end method

.method public b(Lcom/google/firebase/firestore/core/Query;Lio/grpc/Status;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/core/f$b;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/firebase/firestore/core/f$b;->a(Lcom/google/firebase/firestore/core/f$b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/core/n;

    invoke-static {p2}, Lo22;->r(Lio/grpc/Status;)Lcom/google/firebase/firestore/FirebaseFirestoreException;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/core/n;->b(Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 5

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/core/ViewSnapshot;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/core/ViewSnapshot;->h()Lcom/google/firebase/firestore/core/Query;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/firestore/core/f$b;

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/google/firebase/firestore/core/f$b;->a(Lcom/google/firebase/firestore/core/f$b;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/firebase/firestore/core/n;

    invoke-virtual {v4, v1}, Lcom/google/firebase/firestore/core/n;->d(Lcom/google/firebase/firestore/core/ViewSnapshot;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {v2, v1}, Lcom/google/firebase/firestore/core/f$b;->c(Lcom/google/firebase/firestore/core/f$b;Lcom/google/firebase/firestore/core/ViewSnapshot;)Lcom/google/firebase/firestore/core/ViewSnapshot;

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/f;->e()V

    :cond_4
    return-void
.end method

.method public d(Lcom/google/firebase/firestore/core/n;)I
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/n;->a()Lcom/google/firebase/firestore/core/Query;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/core/f$b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v3

    :goto_0
    if-eqz v4, :cond_1

    new-instance v1, Lcom/google/firebase/firestore/core/f$b;

    invoke-direct {v1}, Lcom/google/firebase/firestore/core/f$b;-><init>()V

    iget-object v5, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {v1}, Lcom/google/firebase/firestore/core/f$b;->a(Lcom/google/firebase/firestore/core/f$b;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/firebase/firestore/core/f;->d:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p1, v5}, Lcom/google/firebase/firestore/core/n;->c(Lcom/google/firebase/firestore/core/OnlineState;)Z

    move-result v5

    xor-int/2addr v2, v5

    const-string v5, "onOnlineStateChanged() shouldn\'t raise an event for brand-new listeners."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v5, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/google/firebase/firestore/core/f$b;->b(Lcom/google/firebase/firestore/core/f$b;)Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/google/firebase/firestore/core/f$b;->b(Lcom/google/firebase/firestore/core/f$b;)Lcom/google/firebase/firestore/core/ViewSnapshot;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/firebase/firestore/core/n;->d(Lcom/google/firebase/firestore/core/ViewSnapshot;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/f;->e()V

    :cond_2
    if-eqz v4, :cond_3

    iget-object p1, p0, Lcom/google/firebase/firestore/core/f;->a:Lcom/google/firebase/firestore/core/p;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/core/p;->n(Lcom/google/firebase/firestore/core/Query;)I

    move-result p1

    invoke-static {v1, p1}, Lcom/google/firebase/firestore/core/f$b;->e(Lcom/google/firebase/firestore/core/f$b;I)I

    :cond_3
    invoke-static {v1}, Lcom/google/firebase/firestore/core/f$b;->d(Lcom/google/firebase/firestore/core/f$b;)I

    move-result p1

    return p1
.end method

.method public final e()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/f;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v2}, Lrx;->a(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f(Lcom/google/firebase/firestore/core/n;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/n;->a()Lcom/google/firebase/firestore/core/Query;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/core/f$b;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/firebase/firestore/core/f$b;->a(Lcom/google/firebase/firestore/core/f$b;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-static {v1}, Lcom/google/firebase/firestore/core/f$b;->a(Lcom/google/firebase/firestore/core/f$b;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/firebase/firestore/core/f;->b:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/google/firebase/firestore/core/f;->a:Lcom/google/firebase/firestore/core/p;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/core/p;->v(Lcom/google/firebase/firestore/core/Query;)V

    :cond_1
    return-void
.end method

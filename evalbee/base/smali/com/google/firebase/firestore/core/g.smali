.class public final Lcom/google/firebase/firestore/core/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lrp;

.field public final b:Leo;

.field public final c:Leo;

.field public final d:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final e:Ljd;

.field public final f:Lfc0;

.field public g:Ld51;

.field public h:Lcom/google/firebase/firestore/local/a;

.field public i:Lcom/google/firebase/firestore/remote/g;

.field public j:Lcom/google/firebase/firestore/core/p;

.field public k:Lcom/google/firebase/firestore/core/f;

.field public l:Lhj1;

.field public m:Lhj1;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lrp;Lcom/google/firebase/firestore/b;Leo;Leo;Lcom/google/firebase/firestore/util/AsyncQueue;Lfc0;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->a:Lrp;

    iput-object p4, p0, Lcom/google/firebase/firestore/core/g;->b:Leo;

    iput-object p5, p0, Lcom/google/firebase/firestore/core/g;->c:Leo;

    iput-object p6, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p7, p0, Lcom/google/firebase/firestore/core/g;->f:Lfc0;

    new-instance p7, Ljd;

    new-instance v0, Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p2}, Lrp;->a()Lqp;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/google/firebase/firestore/remote/f;-><init>(Lqp;)V

    invoke-direct {p7, v0}, Ljd;-><init>(Lcom/google/firebase/firestore/remote/f;)V

    iput-object p7, p0, Lcom/google/firebase/firestore/core/g;->e:Ljd;

    new-instance p2, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p2}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance p7, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {p7, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    new-instance v0, Lz30;

    invoke-direct {v0, p0, p2, p1, p3}, Lz30;-><init>(Lcom/google/firebase/firestore/core/g;Lcom/google/android/gms/tasks/TaskCompletionSource;Landroid/content/Context;Lcom/google/firebase/firestore/b;)V

    invoke-virtual {p6, v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    new-instance p1, La40;

    invoke-direct {p1, p0, p7, p2, p6}, La40;-><init>(Lcom/google/firebase/firestore/core/g;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/util/AsyncQueue;)V

    invoke-virtual {p4, p1}, Leo;->c(Llk0;)V

    new-instance p1, Lb40;

    invoke-direct {p1}, Lb40;-><init>()V

    invoke-virtual {p5, p1}, Leo;->c(Llk0;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/tasks/Task;)Lzt;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/core/g;->m(Lcom/google/android/gms/tasks/Task;)Lzt;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Lcom/google/firebase/firestore/core/g;Lcom/google/firebase/firestore/core/n;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/core/g;->t(Lcom/google/firebase/firestore/core/n;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/firebase/firestore/core/g;Lcom/google/firebase/firestore/core/n;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/core/g;->o(Lcom/google/firebase/firestore/core/n;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/firebase/firestore/core/g;Lu12;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/core/g;->q(Lu12;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/firebase/firestore/core/g;Ljava/util/List;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/core/g;->u(Ljava/util/List;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/firebase/firestore/core/g;Lcom/google/android/gms/tasks/TaskCompletionSource;Landroid/content/Context;Lcom/google/firebase/firestore/b;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/core/g;->p(Lcom/google/android/gms/tasks/TaskCompletionSource;Landroid/content/Context;Lcom/google/firebase/firestore/b;)V

    return-void
.end method

.method public static synthetic g(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/core/g;->s(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic h(Lcom/google/firebase/firestore/core/g;Ldu;)Lzt;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/core/g;->n(Ldu;)Lzt;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic i(Lcom/google/firebase/firestore/core/g;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/util/AsyncQueue;Lu12;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/firebase/firestore/core/g;->r(Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/util/AsyncQueue;Lu12;)V

    return-void
.end method

.method public static synthetic m(Lcom/google/android/gms/tasks/Task;)Lzt;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lzt;

    invoke-interface {p0}, Lzt;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    invoke-interface {p0}, Lzt;->c()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x0

    return-object p0

    :cond_1
    new-instance p0, Lcom/google/firebase/firestore/FirebaseFirestoreException;

    const-string v0, "Failed to get document from cache. (However, this document may exist on the server. Run again without setting source to CACHE to attempt to retrieve the document from the server.)"

    sget-object v1, Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;->UNAVAILABLE:Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;

    invoke-direct {p0, v0, v1}, Lcom/google/firebase/firestore/FirebaseFirestoreException;-><init>(Ljava/lang/String;Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;)V

    throw p0
.end method

.method private synthetic n(Ldu;)Lzt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->h:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/a;->N(Ldu;)Lzt;

    move-result-object p1

    return-object p1
.end method

.method private synthetic o(Lcom/google/firebase/firestore/core/n;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->k:Lcom/google/firebase/firestore/core/f;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/f;->d(Lcom/google/firebase/firestore/core/n;)I

    return-void
.end method

.method private synthetic p(Lcom/google/android/gms/tasks/TaskCompletionSource;Landroid/content/Context;Lcom/google/firebase/firestore/b;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->getTask()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->await(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lu12;

    invoke-virtual {p0, p2, p1, p3}, Lcom/google/firebase/firestore/core/g;->k(Landroid/content/Context;Lu12;Lcom/google/firebase/firestore/b;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :goto_0
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method private synthetic q(Lu12;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->j:Lcom/google/firebase/firestore/core/p;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "SyncEngine not yet initialized"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lu12;->a()Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "FirestoreClient"

    const-string v2, "Credential changed. Current user: %s"

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->j:Lcom/google/firebase/firestore/core/p;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/p;->l(Lu12;)V

    return-void
.end method

.method private synthetic r(Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/util/AsyncQueue;Lu12;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->getTask()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isComplete()Z

    move-result p1

    xor-int/2addr p1, v1

    const-string p3, "Already fulfilled first user task"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p3, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p2, p4}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setResult(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Le40;

    invoke-direct {p1, p0, p4}, Le40;-><init>(Lcom/google/firebase/firestore/core/g;Lu12;)V

    invoke-virtual {p3, p1}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public static synthetic s(Ljava/lang/String;)V
    .locals 0

    .line 1
    return-void
.end method

.method private synthetic t(Lcom/google/firebase/firestore/core/n;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->k:Lcom/google/firebase/firestore/core/f;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/f;->f(Lcom/google/firebase/firestore/core/n;)V

    return-void
.end method

.method private synthetic u(Ljava/util/List;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->j:Lcom/google/firebase/firestore/core/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/firebase/firestore/core/p;->y(Ljava/util/List;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    return-void
.end method


# virtual methods
.method public j(Ldu;)Lcom/google/android/gms/tasks/Task;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/g;->x()V

    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v1, Lc40;

    invoke-direct {v1, p0, p1}, Lc40;-><init>(Lcom/google/firebase/firestore/core/g;Ldu;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->g(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Ld40;

    invoke-direct {v0}, Ld40;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/Task;->continueWith(Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final k(Landroid/content/Context;Lu12;Lcom/google/firebase/firestore/b;)V
    .locals 11

    .line 1
    invoke-virtual {p2}, Lu12;->a()Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "FirestoreClient"

    const-string v2, "Initializing. user=%s"

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/firebase/firestore/remote/d;

    iget-object v4, p0, Lcom/google/firebase/firestore/core/g;->a:Lrp;

    iget-object v5, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v6, p0, Lcom/google/firebase/firestore/core/g;->b:Leo;

    iget-object v7, p0, Lcom/google/firebase/firestore/core/g;->c:Leo;

    iget-object v9, p0, Lcom/google/firebase/firestore/core/g;->f:Lfc0;

    move-object v3, v0

    move-object v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/firebase/firestore/remote/d;-><init>(Lrp;Lcom/google/firebase/firestore/util/AsyncQueue;Leo;Leo;Landroid/content/Context;Lfc0;)V

    new-instance v1, Lcom/google/firebase/firestore/core/d$a;

    iget-object v5, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v6, p0, Lcom/google/firebase/firestore/core/g;->a:Lrp;

    const/16 v9, 0x64

    move-object v3, v1

    move-object v4, p1

    move-object v7, v0

    move-object v8, p2

    move-object v10, p3

    invoke-direct/range {v3 .. v10}, Lcom/google/firebase/firestore/core/d$a;-><init>(Landroid/content/Context;Lcom/google/firebase/firestore/util/AsyncQueue;Lrp;Lcom/google/firebase/firestore/remote/d;Lu12;ILcom/google/firebase/firestore/b;)V

    invoke-virtual {p3}, Lcom/google/firebase/firestore/b;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/google/firebase/firestore/core/o;

    invoke-direct {p1}, Lcom/google/firebase/firestore/core/o;-><init>()V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/google/firebase/firestore/core/l;

    invoke-direct {p1}, Lcom/google/firebase/firestore/core/l;-><init>()V

    :goto_0
    invoke-virtual {p1, v1}, Lcom/google/firebase/firestore/core/d;->q(Lcom/google/firebase/firestore/core/d$a;)V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->n()Ld51;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->g:Ld51;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->k()Lhj1;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->m:Lhj1;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->m()Lcom/google/firebase/firestore/local/a;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->h:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->o()Lcom/google/firebase/firestore/remote/g;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->i:Lcom/google/firebase/firestore/remote/g;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->j:Lcom/google/firebase/firestore/core/p;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->j()Lcom/google/firebase/firestore/core/f;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/core/g;->k:Lcom/google/firebase/firestore/core/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/d;->l()Lwe0;

    move-result-object p1

    iget-object p2, p0, Lcom/google/firebase/firestore/core/g;->m:Lhj1;

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lhj1;->start()V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lwe0;->f()Lwe0$a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/core/g;->l:Lhj1;

    invoke-interface {p1}, Lhj1;->start()V

    :cond_2
    return-void
.end method

.method public l()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->k()Z

    move-result v0

    return v0
.end method

.method public v(Lcom/google/firebase/firestore/core/Query;Lcom/google/firebase/firestore/core/f$a;Lrx;)Lcom/google/firebase/firestore/core/n;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/g;->x()V

    new-instance v0, Lcom/google/firebase/firestore/core/n;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/firebase/firestore/core/n;-><init>(Lcom/google/firebase/firestore/core/Query;Lcom/google/firebase/firestore/core/f$a;Lrx;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance p2, Lf40;

    invoke-direct {p2, p0, v0}, Lf40;-><init>(Lcom/google/firebase/firestore/core/g;Lcom/google/firebase/firestore/core/n;)V

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public w(Lcom/google/firebase/firestore/core/n;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/g;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v1, Lg40;

    invoke-direct {v1, p0, p1}, Lg40;-><init>(Lcom/google/firebase/firestore/core/g;Lcom/google/firebase/firestore/core/n;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final x()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/g;->l()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The client has already been terminated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public y(Ljava/util/List;)Lcom/google/android/gms/tasks/Task;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/core/g;->x()V

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/core/g;->d:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v2, Ly30;

    invoke-direct {v2, p0, p1, v0}, Ly30;-><init>(Lcom/google/firebase/firestore/core/g;Ljava/util/List;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->getTask()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

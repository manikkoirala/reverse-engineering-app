.class public Lcom/google/firebase/firestore/core/l$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/firestore/remote/g$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/firestore/core/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/firebase/firestore/core/l;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/core/l;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/firebase/firestore/core/l;Lcom/google/firebase/firestore/core/l$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/core/l$b;-><init>(Lcom/google/firebase/firestore/core/l;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/core/OnlineState;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/p;->a(Lcom/google/firebase/firestore/core/OnlineState;)V

    return-void
.end method

.method public b(I)Lcom/google/firebase/database/collection/c;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/p;->b(I)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    return-object p1
.end method

.method public c(ILio/grpc/Status;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/firebase/firestore/core/p;->c(ILio/grpc/Status;)V

    return-void
.end method

.method public d(Ljd1;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/p;->d(Ljd1;)V

    return-void
.end method

.method public e(Lyx0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/p;->e(Lyx0;)V

    return-void
.end method

.method public f(ILio/grpc/Status;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/core/l$b;->a:Lcom/google/firebase/firestore/core/l;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/d;->p()Lcom/google/firebase/firestore/core/p;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/firebase/firestore/core/p;->f(ILio/grpc/Status;)V

    return-void
.end method

.class public Lcom/google/firebase/firestore/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldu;

.field public final b:Lcom/google/firebase/firestore/FirebaseFirestore;


# direct methods
.method public constructor <init>(Ldu;Lcom/google/firebase/firestore/FirebaseFirestore;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ldu;

    iput-object p1, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    iput-object p2, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    return-void
.end method

.method public static synthetic a(Lcom/google/firebase/firestore/a;Lrx;Lcom/google/firebase/firestore/core/ViewSnapshot;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/a;->n(Lrx;Lcom/google/firebase/firestore/core/ViewSnapshot;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/Source;Lcom/google/firebase/firestore/DocumentSnapshot;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/firebase/firestore/a;->p(Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/Source;Lcom/google/firebase/firestore/DocumentSnapshot;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/firebase/firestore/a;Lcom/google/android/gms/tasks/Task;)Lcom/google/firebase/firestore/DocumentSnapshot;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/a;->o(Lcom/google/android/gms/tasks/Task;)Lcom/google/firebase/firestore/DocumentSnapshot;

    move-result-object p0

    return-object p0
.end method

.method public static g(Lke1;Lcom/google/firebase/firestore/FirebaseFirestore;)Lcom/google/firebase/firestore/a;
    .locals 2

    .line 1
    invoke-virtual {p0}, Ljb;->l()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/firebase/firestore/a;

    invoke-static {p0}, Ldu;->g(Lke1;)Ldu;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/google/firebase/firestore/a;-><init>(Ldu;Lcom/google/firebase/firestore/FirebaseFirestore;)V

    return-object v0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid document reference. Document references must have an even number of segments, but "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lke1;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " has "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljb;->l()I

    move-result p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static m(Lcom/google/firebase/firestore/MetadataChanges;)Lcom/google/firebase/firestore/core/f$a;
    .locals 5

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/core/f$a;

    invoke-direct {v0}, Lcom/google/firebase/firestore/core/f$a;-><init>()V

    sget-object v1, Lcom/google/firebase/firestore/MetadataChanges;->INCLUDE:Lcom/google/firebase/firestore/MetadataChanges;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p0, v1, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v3

    :goto_0
    iput-boolean v4, v0, Lcom/google/firebase/firestore/core/f$a;->a:Z

    if-ne p0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    iput-boolean v2, v0, Lcom/google/firebase/firestore/core/f$a;->b:Z

    iput-boolean v3, v0, Lcom/google/firebase/firestore/core/f$a;->c:Z

    return-object v0
.end method

.method private synthetic n(Lrx;Lcom/google/firebase/firestore/core/ViewSnapshot;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    if-eqz p3, :cond_0

    invoke-interface {p1, v0, p3}, Lrx;->a(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    return-void

    :cond_0
    const/4 p3, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    move v2, p3

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    const-string v3, "Got event without value or error set"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p2}, Lcom/google/firebase/firestore/core/ViewSnapshot;->e()Lmu;

    move-result-object v2

    invoke-virtual {v2}, Lmu;->size()I

    move-result v2

    if-gt v2, p3, :cond_2

    goto :goto_1

    :cond_2
    move p3, v1

    :goto_1
    const-string v2, "Too many documents returned on a document query"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p2}, Lcom/google/firebase/firestore/core/ViewSnapshot;->e()Lmu;

    move-result-object p3

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {p3, v1}, Lmu;->g(Ldu;)Lzt;

    move-result-object p3

    if-eqz p3, :cond_3

    invoke-virtual {p2}, Lcom/google/firebase/firestore/core/ViewSnapshot;->f()Lcom/google/firebase/database/collection/c;

    move-result-object v1

    invoke-interface {p3}, Lzt;->getKey()Ldu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/database/collection/c;->contains(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {p2}, Lcom/google/firebase/firestore/core/ViewSnapshot;->k()Z

    move-result p2

    invoke-static {v2, p3, p2, v1}, Lcom/google/firebase/firestore/DocumentSnapshot;->b(Lcom/google/firebase/firestore/FirebaseFirestore;Lzt;ZZ)Lcom/google/firebase/firestore/DocumentSnapshot;

    move-result-object p2

    goto :goto_2

    :cond_3
    iget-object p3, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {p2}, Lcom/google/firebase/firestore/core/ViewSnapshot;->k()Z

    move-result p2

    invoke-static {p3, v1, p2}, Lcom/google/firebase/firestore/DocumentSnapshot;->c(Lcom/google/firebase/firestore/FirebaseFirestore;Ldu;Z)Lcom/google/firebase/firestore/DocumentSnapshot;

    move-result-object p2

    :goto_2
    invoke-interface {p1, p2, v0}, Lrx;->a(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    return-void
.end method

.method private synthetic o(Lcom/google/android/gms/tasks/Task;)Lcom/google/firebase/firestore/DocumentSnapshot;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Lzt;

    if-eqz v3, :cond_0

    invoke-interface {v3}, Lzt;->g()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move v5, p1

    new-instance p1, Lcom/google/firebase/firestore/DocumentSnapshot;

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    iget-object v2, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    const/4 v4, 0x1

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/DocumentSnapshot;-><init>(Lcom/google/firebase/firestore/FirebaseFirestore;Ldu;Lzt;ZZ)V

    return-object p1
.end method

.method public static synthetic p(Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/Source;Lcom/google/firebase/firestore/DocumentSnapshot;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 1

    .line 1
    const-string v0, "Failed to register a listener for a single document"

    if-eqz p4, :cond_0

    invoke-virtual {p0, p4}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setException(Ljava/lang/Exception;)V

    return-void

    :cond_0
    const/4 p4, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->getTask()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->await(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmk0;

    invoke-interface {p1}, Lmk0;->remove()V

    invoke-virtual {p3}, Lcom/google/firebase/firestore/DocumentSnapshot;->a()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p3}, Lcom/google/firebase/firestore/DocumentSnapshot;->f()Lpo1;

    move-result-object p1

    invoke-virtual {p1}, Lpo1;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/google/firebase/firestore/FirebaseFirestoreException;

    const-string p2, "Failed to get document because the client is offline."

    sget-object p3, Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;->UNAVAILABLE:Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;

    invoke-direct {p1, p2, p3}, Lcom/google/firebase/firestore/FirebaseFirestoreException;-><init>(Ljava/lang/String;Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setException(Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p3}, Lcom/google/firebase/firestore/DocumentSnapshot;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p3}, Lcom/google/firebase/firestore/DocumentSnapshot;->f()Lpo1;

    move-result-object p1

    invoke-virtual {p1}, Lpo1;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/google/firebase/firestore/Source;->SERVER:Lcom/google/firebase/firestore/Source;

    if-ne p2, p1, :cond_2

    new-instance p1, Lcom/google/firebase/firestore/FirebaseFirestoreException;

    const-string p2, "Failed to get document from server. (However, this document does exist in the local cache. Run again without setting source to SERVER to retrieve the cached document.)"

    sget-object p3, Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;->UNAVAILABLE:Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;

    invoke-direct {p1, p2, p3}, Lcom/google/firebase/firestore/FirebaseFirestoreException;-><init>(Ljava/lang/String;Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p3}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setResult(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception p0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    new-array p1, p4, [Ljava/lang/Object;

    invoke-static {p0, v0, p1}, Lg9;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p0

    throw p0

    :catch_1
    move-exception p0

    new-array p1, p4, [Ljava/lang/Object;

    invoke-static {p0, v0, p1}, Lg9;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p0

    throw p0
.end method


# virtual methods
.method public d(Ljava/util/concurrent/Executor;Lcom/google/firebase/firestore/MetadataChanges;Lrx;)Lmk0;
    .locals 1

    .line 1
    const-string v0, "Provided executor must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Provided MetadataChanges value must not be null."

    invoke-static {p2, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Provided EventListener must not be null."

    invoke-static {p3, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/firebase/firestore/a;->m(Lcom/google/firebase/firestore/MetadataChanges;)Lcom/google/firebase/firestore/core/f$a;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/firebase/firestore/a;->e(Ljava/util/concurrent/Executor;Lcom/google/firebase/firestore/core/f$a;Landroid/app/Activity;Lrx;)Lmk0;

    move-result-object p1

    return-object p1
.end method

.method public final e(Ljava/util/concurrent/Executor;Lcom/google/firebase/firestore/core/f$a;Landroid/app/Activity;Lrx;)Lmk0;
    .locals 1

    .line 1
    new-instance v0, Lju;

    invoke-direct {v0, p0, p4}, Lju;-><init>(Lcom/google/firebase/firestore/a;Lrx;)V

    new-instance p4, Lj9;

    invoke-direct {p4, p1, v0}, Lj9;-><init>(Ljava/util/concurrent/Executor;Lrx;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/a;->f()Lcom/google/firebase/firestore/core/Query;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/FirebaseFirestore;->c()Lcom/google/firebase/firestore/core/g;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/firebase/firestore/core/g;->v(Lcom/google/firebase/firestore/core/Query;Lcom/google/firebase/firestore/core/f$a;Lrx;)Lcom/google/firebase/firestore/core/n;

    move-result-object p1

    new-instance p2, Lnk0;

    iget-object v0, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/FirebaseFirestore;->c()Lcom/google/firebase/firestore/core/g;

    move-result-object v0

    invoke-direct {p2, v0, p1, p4}, Lnk0;-><init>(Lcom/google/firebase/firestore/core/g;Lcom/google/firebase/firestore/core/n;Lj9;)V

    invoke-static {p3, p2}, Lw2;->c(Landroid/app/Activity;Lmk0;)Lmk0;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/google/firebase/firestore/a;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/google/firebase/firestore/a;

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    iget-object v3, p1, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {v1, v3}, Ldu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    iget-object p1, p1, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    return v0
.end method

.method public final f()Lcom/google/firebase/firestore/core/Query;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {v0}, Ldu;->m()Lke1;

    move-result-object v0

    invoke-static {v0}, Lcom/google/firebase/firestore/core/Query;->b(Lke1;)Lcom/google/firebase/firestore/core/Query;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/Source;->DEFAULT:Lcom/google/firebase/firestore/Source;

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/a;->i(Lcom/google/firebase/firestore/Source;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {v0}, Ldu;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i(Lcom/google/firebase/firestore/Source;)Lcom/google/android/gms/tasks/Task;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/Source;->CACHE:Lcom/google/firebase/firestore/Source;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/FirebaseFirestore;->c()Lcom/google/firebase/firestore/core/g;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/core/g;->j(Ldu;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    sget-object v0, Lwy;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lgu;

    invoke-direct {v1, p0}, Lgu;-><init>(Lcom/google/firebase/firestore/a;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/tasks/Task;->continueWith(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/a;->l(Lcom/google/firebase/firestore/Source;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public j()Lcom/google/firebase/firestore/FirebaseFirestore;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    invoke-virtual {v0}, Ldu;->m()Lke1;

    move-result-object v0

    invoke-virtual {v0}, Lke1;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Lcom/google/firebase/firestore/Source;)Lcom/google/android/gms/tasks/Task;
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v1}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v2, Lcom/google/firebase/firestore/core/f$a;

    invoke-direct {v2}, Lcom/google/firebase/firestore/core/f$a;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/firebase/firestore/core/f$a;->a:Z

    iput-boolean v3, v2, Lcom/google/firebase/firestore/core/f$a;->b:Z

    iput-boolean v3, v2, Lcom/google/firebase/firestore/core/f$a;->c:Z

    sget-object v3, Lwy;->b:Ljava/util/concurrent/Executor;

    new-instance v4, Liu;

    invoke-direct {v4, v0, v1, p1}, Liu;-><init>(Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/android/gms/tasks/TaskCompletionSource;Lcom/google/firebase/firestore/Source;)V

    const/4 p1, 0x0

    invoke-virtual {p0, v3, v2, p1, v4}, Lcom/google/firebase/firestore/a;->e(Ljava/util/concurrent/Executor;Lcom/google/firebase/firestore/core/f$a;Landroid/app/Activity;Lrx;)Lmk0;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setResult(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->getTask()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public q(Ljava/lang/Object;Lrm1;)Lcom/google/android/gms/tasks/Task;
    .locals 2

    .line 1
    const-string v0, "Provided data must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Provided options must not be null."

    invoke-static {p2, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lrm1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/FirebaseFirestore;->h()Lb22;

    move-result-object v0

    invoke-virtual {p2}, Lrm1;->a()Lq00;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lb22;->f(Ljava/lang/Object;Lq00;)Lz12;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {p2}, Lcom/google/firebase/firestore/FirebaseFirestore;->h()Lb22;

    move-result-object p2

    invoke-virtual {p2, p1}, Lb22;->i(Ljava/lang/Object;)Lz12;

    move-result-object p1

    :goto_0
    iget-object p2, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {p2}, Lcom/google/firebase/firestore/FirebaseFirestore;->c()Lcom/google/firebase/firestore/core/g;

    move-result-object p2

    iget-object v0, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    sget-object v1, Lh71;->c:Lh71;

    invoke-virtual {p1, v0, v1}, Lz12;->a(Ldu;Lh71;)Lwx0;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/firebase/firestore/core/g;->y(Ljava/util/List;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    sget-object p2, Lwy;->b:Ljava/util/concurrent/Executor;

    invoke-static {}, Lo22;->A()Lcom/google/android/gms/tasks/Continuation;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/google/android/gms/tasks/Task;->continueWith(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final r(La22;)Lcom/google/android/gms/tasks/Task;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/FirebaseFirestore;->c()Lcom/google/firebase/firestore/core/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/a;->a:Ldu;

    const/4 v2, 0x1

    invoke-static {v2}, Lh71;->a(Z)Lh71;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, La22;->a(Ldu;Lh71;)Lwx0;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/core/g;->y(Ljava/util/List;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    sget-object v0, Lwy;->b:Ljava/util/concurrent/Executor;

    invoke-static {}, Lo22;->A()Lcom/google/android/gms/tasks/Continuation;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/tasks/Task;->continueWith(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public s(Ljava/util/Map;)Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/a;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/FirebaseFirestore;->h()Lb22;

    move-result-object v0

    invoke-virtual {v0, p1}, Lb22;->k(Ljava/util/Map;)La22;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/a;->r(La22;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

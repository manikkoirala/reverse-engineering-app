.class public final Lcom/google/firebase/firestore/local/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzx0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/local/k$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/firebase/firestore/local/m;

.field public final b:Lzk0;

.field public final c:Lcom/google/firebase/firestore/local/IndexManager;

.field public final d:Ljava/lang/String;

.field public e:I

.field public f:Lcom/google/protobuf/ByteString;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/local/m;Lzk0;Lu12;Lcom/google/firebase/firestore/local/IndexManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    iput-object p2, p0, Lcom/google/firebase/firestore/local/k;->b:Lzk0;

    invoke-virtual {p3}, Lu12;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p3}, Lu12;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    iput-object p1, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    sget-object p1, Lcom/google/firebase/firestore/remote/j;->v:Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/firebase/firestore/local/k;->f:Lcom/google/protobuf/ByteString;

    iput-object p4, p0, Lcom/google/firebase/firestore/local/k;->c:Lcom/google/firebase/firestore/local/IndexManager;

    return-void
.end method

.method private synthetic A(Landroid/database/Cursor;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/google/firebase/firestore/local/k;->e:I

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/google/firebase/firestore/local/k;->e:I

    return-void
.end method

.method private synthetic B(ILandroid/database/Cursor;)Lxx0;
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/local/k;->t(I[B)Lxx0;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic C(Ljava/util/List;Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lxw;->b(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic D(Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p1

    invoke-static {p1}, Lcom/google/protobuf/ByteString;->copyFrom([B)Lcom/google/protobuf/ByteString;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/local/k;->f:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method public static synthetic k(Lcom/google/firebase/firestore/local/k;Landroid/database/Cursor;)Lxx0;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/k;->y(Landroid/database/Cursor;)Lxx0;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic l(Lcom/google/firebase/firestore/local/k;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/k;->A(Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic m(Lcom/google/firebase/firestore/local/k;Ljava/util/List;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/local/k;->v(Ljava/util/List;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic n(Lcom/google/firebase/firestore/local/k;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/k;->D(Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic o(Lcom/google/firebase/firestore/local/k;Ljava/util/Set;Ljava/util/List;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/k;->w(Ljava/util/Set;Ljava/util/List;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic p(Ljava/util/List;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/k;->C(Ljava/util/List;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic q(Ljava/util/List;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/k;->z(Ljava/util/List;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic r(Lxx0;Lxx0;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/k;->x(Lxx0;Lxx0;)I

    move-result p0

    return p0
.end method

.method public static synthetic s(Lcom/google/firebase/firestore/local/k;ILandroid/database/Cursor;)Lxx0;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/local/k;->B(ILandroid/database/Cursor;)Lxx0;

    move-result-object p0

    return-object p0
.end method

.method private synthetic v(Ljava/util/List;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p2

    invoke-virtual {p0, v0, p2}, Lcom/google/firebase/firestore/local/k;->t(I[B)Lxx0;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic w(Ljava/util/Set;Ljava/util/List;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    invoke-interface {p3, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/local/k;->t(I[B)Lxx0;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static synthetic x(Lxx0;Lxx0;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lxx0;->e()I

    move-result p0

    invoke-virtual {p1}, Lxx0;->e()I

    move-result p1

    invoke-static {p0, p1}, Lo22;->k(II)I

    move-result p0

    return p0
.end method

.method private synthetic y(Landroid/database/Cursor;)Lxx0;
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/local/k;->t(I[B)Lxx0;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic z(Ljava/util/List;Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final E()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT uid FROM mutation_queues"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    new-instance v2, Lzg1;

    invoke-direct {v2, v0}, Lzg1;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/firebase/firestore/local/k;->e:I

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v3, "SELECT MAX(batch_id) FROM mutations WHERE uid = ?"

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v2

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    new-instance v2, Lah1;

    invoke-direct {v2, p0}, Lah1;-><init>(Lcom/google/firebase/firestore/local/k;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/google/firebase/firestore/local/k;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/firebase/firestore/local/k;->e:I

    return-void
.end method

.method public final F()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->f:Lcom/google/protobuf/ByteString;

    invoke-virtual {v3}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v3

    filled-new-array {v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "INSERT OR REPLACE INTO mutation_queues (uid, last_acknowledged_batch_id, last_stream_token) VALUES (?, ?, ?)"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public a()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/k;->u()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT path FROM document_mutations WHERE uid = ?"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    new-instance v2, Lch1;

    invoke-direct {v2, v0}, Lch1;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const-string v2, "Document leak -- detected dangling mutation references when queue is empty. Dangling keys: %s"

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public b(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 6

    .line 1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldu;

    invoke-virtual {v0}, Ldu;->m()Lke1;

    move-result-object v0

    invoke-static {v0}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/google/firebase/firestore/local/m$b;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT DISTINCT dm.batch_id, SUBSTR(m.mutations, 1, ?) FROM document_mutations dm, mutations m WHERE dm.uid = ? AND dm.path IN ("

    const v0, 0xf4240

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    filled-new-array {v0, v3}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const-string v5, ") AND dm.uid = m.uid AND dm.batch_id = m.batch_id ORDER BY dm.batch_id"

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/local/m$b;-><init>(Lcom/google/firebase/firestore/local/m;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/local/m$b;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/local/m$b;->e()Lcom/google/firebase/firestore/local/m$d;

    move-result-object v2

    new-instance v3, Lug1;

    invoke-direct {v3, p0, v1, v0}, Lug1;-><init>(Lcom/google/firebase/firestore/local/k;Ljava/util/Set;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/local/m$b;->c()I

    move-result p1

    const/4 v1, 0x1

    if-le p1, v1, :cond_2

    new-instance p1, Lvg1;

    invoke-direct {p1}, Lvg1;-><init>()V

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    return-object v0
.end method

.method public c(Lxx0;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "DELETE FROM mutations WHERE uid = ? AND batch_id = ?"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->B(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "DELETE FROM document_mutations WHERE uid = ? AND path = ? AND batch_id = ?"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->B(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    invoke-virtual {p1}, Lxx0;->e()I

    move-result v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v4, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    filled-new-array {v4, v5}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/google/firebase/firestore/local/m;->s(Landroid/database/sqlite/SQLiteStatement;[Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lxx0;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v3, v4}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "Mutation batch (%s, %d) did not exist"

    invoke-static {v0, v4, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lxx0;->h()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwx0;

    invoke-virtual {v0}, Lwx0;->g()Ldu;

    move-result-object v0

    invoke-virtual {v0}, Ldu;->m()Lke1;

    move-result-object v3

    invoke-static {v3}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v5, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    filled-new-array {v5, v3, v6}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v1, v3}, Lcom/google/firebase/firestore/local/m;->s(Landroid/database/sqlite/SQLiteStatement;[Ljava/lang/Object;)I

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v3}, Lcom/google/firebase/firestore/local/m;->x()Lcom/google/firebase/firestore/local/j;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/firebase/firestore/local/j;->d(Ldu;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public d(Lpw1;Ljava/util/List;Ljava/util/List;)Lxx0;
    .locals 7

    .line 1
    iget v0, p0, Lcom/google/firebase/firestore/local/k;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/firebase/firestore/local/k;->e:I

    new-instance v1, Lxx0;

    invoke-direct {v1, v0, p1, p2, p3}, Lxx0;-><init>(ILpw1;Ljava/util/List;Ljava/util/List;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/k;->b:Lzk0;

    invoke-virtual {p1, v1}, Lzk0;->m(Lxx0;)Lea2;

    move-result-object p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v2, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/protobuf/a0;->toByteArray()[B

    move-result-object p1

    filled-new-array {v2, v3, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v2, "INSERT INTO mutations (uid, batch_id, mutations) VALUES (?, ?, ?)"

    invoke-virtual {p2, v2, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iget-object p2, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "INSERT INTO document_mutations (uid, path, batch_id) VALUES (?, ?, ?)"

    invoke-virtual {p2, v2}, Lcom/google/firebase/firestore/local/m;->B(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p2

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lwx0;

    invoke-virtual {v2}, Lwx0;->g()Ldu;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ldu;->m()Lke1;

    move-result-object v3

    invoke-static {v3}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v5, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    filled-new-array {v5, v3, v6}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, p2, v3}, Lcom/google/firebase/firestore/local/m;->s(Landroid/database/sqlite/SQLiteStatement;[Ljava/lang/Object;)I

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->c:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-virtual {v2}, Ldu;->k()Lke1;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/google/firebase/firestore/local/IndexManager;->a(Lke1;)V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public e(I)Lxx0;
    .locals 3

    .line 1
    add-int/lit8 p1, p1, 0x1

    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT batch_id, SUBSTR(mutations, 1, ?) FROM mutations WHERE uid = ? AND batch_id >= ? ORDER BY batch_id ASC LIMIT 1"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {v1, v2, p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    new-instance v0, Lwg1;

    invoke-direct {v0, p0}, Lwg1;-><init>(Lcom/google/firebase/firestore/local/k;)V

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/local/m$d;->d(Lr90;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lxx0;

    return-object p1
.end method

.method public f(I)Lxx0;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT SUBSTR(mutations, 1, ?) FROM mutations WHERE uid = ? AND batch_id = ?"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Lbh1;

    invoke-direct {v1, p0, p1}, Lbh1;-><init>(Lcom/google/firebase/firestore/local/k;I)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->d(Lr90;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lxx0;

    return-object p1
.end method

.method public g()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->f:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public h(Lxx0;Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    invoke-static {p2}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/firebase/firestore/local/k;->f:Lcom/google/protobuf/ByteString;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/k;->F()V

    return-void
.end method

.method public i(Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/firebase/firestore/local/k;->f:Lcom/google/protobuf/ByteString;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/k;->F()V

    return-void
.end method

.method public j()Ljava/util/List;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT batch_id, SUBSTR(mutations, 1, ?) FROM mutations WHERE uid = ? ORDER BY batch_id ASC"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    filled-new-array {v2, v3}, [Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    new-instance v2, Lxg1;

    invoke-direct {v2, p0, v0}, Lxg1;-><init>(Lcom/google/firebase/firestore/local/k;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    return-object v0
.end method

.method public start()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/k;->E()V

    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT last_stream_token FROM mutation_queues WHERE uid = ?"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Lyg1;

    invoke-direct {v1, p0}, Lyg1;-><init>(Lcom/google/firebase/firestore/local/k;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->c(Lcl;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/k;->F()V

    :cond_0
    return-void
.end method

.method public final t(I[B)Lxx0;
    .locals 6

    .line 1
    :try_start_0
    array-length v0, p2

    const v1, 0xf4240

    if-ge v0, v1, :cond_0

    iget-object p1, p0, Lcom/google/firebase/firestore/local/k;->b:Lzk0;

    invoke-static {p2}, Lea2;->q0([B)Lea2;

    move-result-object p2

    invoke-virtual {p1, p2}, Lzk0;->e(Lea2;)Lxx0;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Lcom/google/firebase/firestore/local/k$a;

    invoke-direct {v0, p2}, Lcom/google/firebase/firestore/local/k$a;-><init>([B)V

    :goto_0
    invoke-static {v0}, Lcom/google/firebase/firestore/local/k$a;->b(Lcom/google/firebase/firestore/local/k$a;)Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/k$a;->d()I

    move-result p2

    mul-int/2addr p2, v1

    const/4 v2, 0x1

    add-int/2addr p2, v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v4, "SELECT SUBSTR(mutations, ?, ?) FROM mutations WHERE uid = ? AND batch_id = ?"

    invoke-virtual {v3, v4}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v4, v2

    iget-object p2, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object p2, v4, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v2, 0x3

    aput-object p2, v4, v2

    invoke-virtual {v3, v4}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p2

    invoke-virtual {p2, v0}, Lcom/google/firebase/firestore/local/m$d;->c(Lcl;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/k$a;->e()Lcom/google/protobuf/ByteString;

    move-result-object p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/k;->b:Lzk0;

    invoke-static {p1}, Lea2;->p0(Lcom/google/protobuf/ByteString;)Lea2;

    move-result-object p1

    invoke-virtual {p2, p1}, Lzk0;->e(Lea2;)Lxx0;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string p2, "MutationBatch failed to parse: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {p2, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public u()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/k;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT batch_id FROM mutations WHERE uid = ? LIMIT 1"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/k;->d:Ljava/lang/String;

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m$d;->f()Z

    move-result v0

    return v0
.end method

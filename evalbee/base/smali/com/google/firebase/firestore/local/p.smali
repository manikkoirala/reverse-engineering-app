.class public final Lcom/google/firebase/firestore/local/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lyt1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/local/p$b;,
        Lcom/google/firebase/firestore/local/p$c;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/firebase/firestore/local/m;

.field public final b:Lzk0;

.field public c:I

.field public d:J

.field public e:Lqo1;

.field public f:J


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/local/m;Lzk0;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lqo1;->b:Lqo1;

    iput-object v0, p0, Lcom/google/firebase/firestore/local/p;->e:Lqo1;

    iput-object p1, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    iput-object p2, p0, Lcom/google/firebase/firestore/local/p;->b:Lzk0;

    return-void
.end method

.method public static synthetic j(Lcom/google/firebase/firestore/local/p$b;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/p;->t(Lcom/google/firebase/firestore/local/p$b;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic k(Lcom/google/firebase/firestore/local/p;Lcom/google/firebase/firestore/core/q;Lcom/google/firebase/firestore/local/p$c;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/p;->u(Lcom/google/firebase/firestore/core/q;Lcom/google/firebase/firestore/local/p$c;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic l(Lcom/google/firebase/firestore/local/p;Lcl;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/local/p;->s(Lcl;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic m(Lcom/google/firebase/firestore/local/p;Landroid/util/SparseArray;[ILandroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/p;->v(Landroid/util/SparseArray;[ILandroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic n(Lcom/google/firebase/firestore/local/p;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/p;->w(Landroid/database/Cursor;)V

    return-void
.end method

.method private synthetic s(Lcl;Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/google/firebase/firestore/local/p;->o([B)Lau1;

    move-result-object p2

    invoke-interface {p1, p2}, Lcl;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic t(Lcom/google/firebase/firestore/local/p$b;Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lxw;->b(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-static {p1}, Ldu;->g(Lke1;)Ldu;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/local/p$b;->a:Lcom/google/firebase/database/collection/c;

    invoke-virtual {v0, p1}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/local/p$b;->a:Lcom/google/firebase/database/collection/c;

    return-void
.end method

.method private synthetic u(Lcom/google/firebase/firestore/core/q;Lcom/google/firebase/firestore/local/p$c;Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p3

    invoke-virtual {p0, p3}, Lcom/google/firebase/firestore/local/p;->o([B)Lau1;

    move-result-object p3

    invoke-virtual {p3}, Lau1;->g()Lcom/google/firebase/firestore/core/q;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/core/q;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iput-object p3, p2, Lcom/google/firebase/firestore/local/p$c;->a:Lau1;

    :cond_0
    return-void
.end method

.method private synthetic v(Landroid/util/SparseArray;[ILandroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-virtual {p0, p3}, Lcom/google/firebase/firestore/local/p;->z(I)V

    aget p1, p2, v0

    add-int/lit8 p1, p1, 0x1

    aput p1, p2, v0

    :cond_0
    return-void
.end method

.method private synthetic w(Landroid/database/Cursor;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/firebase/firestore/local/p;->c:I

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/p;->d:J

    new-instance v0, Lqo1;

    new-instance v1, Lpw1;

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lpw1;-><init>(JI)V

    invoke-direct {v0, v1}, Lqo1;-><init>(Lpw1;)V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/p;->e:Lqo1;

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/p;->f:J

    return-void
.end method


# virtual methods
.method public final A(Lau1;)V
    .locals 10

    .line 1
    invoke-virtual {p1}, Lau1;->h()I

    move-result v0

    invoke-virtual {p1}, Lau1;->g()Lcom/google/firebase/firestore/core/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/firestore/core/q;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v1

    invoke-virtual {v1}, Lqo1;->c()Lpw1;

    move-result-object v1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/p;->b:Lzk0;

    invoke-virtual {v2, p1}, Lzk0;->o(Lau1;)Lcom/google/firebase/firestore/proto/Target;

    move-result-object v2

    iget-object v9, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1}, Lpw1;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1}, Lpw1;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p1}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->toByteArray()[B

    move-result-object v6

    invoke-virtual {p1}, Lau1;->e()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2}, Lcom/google/protobuf/a;->toByteArray()[B

    move-result-object v8

    move-object v2, v0

    filled-new-array/range {v2 .. v8}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "INSERT OR REPLACE INTO targets (target_id, canonical_id, snapshot_version_seconds, snapshot_version_nanos, resume_token, last_listen_sequence_number, target_proto) VALUES (?, ?, ?, ?, ?, ?, ?)"

    invoke-virtual {v9, v0, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public B()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT highest_target_id, highest_listen_sequence_number, last_remote_snapshot_version_seconds, last_remote_snapshot_version_nanos, target_count FROM target_globals LIMIT 1"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Lgi1;

    invoke-direct {v1, p0}, Lgi1;-><init>(Lcom/google/firebase/firestore/local/p;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->c(Lcl;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    const-string v0, "Missing target_globals entry"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final C(Lau1;)Z
    .locals 7

    .line 1
    invoke-virtual {p1}, Lau1;->h()I

    move-result v0

    iget v1, p0, Lcom/google/firebase/firestore/local/p;->c:I

    const/4 v2, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Lau1;->h()I

    move-result v0

    iput v0, p0, Lcom/google/firebase/firestore/local/p;->c:I

    move v0, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lau1;->e()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/firebase/firestore/local/p;->d:J

    cmp-long v1, v3, v5

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lau1;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/p;->d:J

    goto :goto_1

    :cond_1
    move v2, v0

    :goto_1
    return v2
.end method

.method public final D()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    iget v1, p0, Lcom/google/firebase/firestore/local/p;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/firebase/firestore/local/p;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/p;->e:Lqo1;

    invoke-virtual {v3}, Lqo1;->c()Lpw1;

    move-result-object v3

    invoke-virtual {v3}, Lpw1;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/google/firebase/firestore/local/p;->e:Lqo1;

    invoke-virtual {v4}, Lqo1;->c()Lpw1;

    move-result-object v4

    invoke-virtual {v4}, Lpw1;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/firebase/firestore/local/p;->f:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    filled-new-array {v1, v2, v3, v4, v5}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "UPDATE target_globals SET highest_target_id = ?, highest_listen_sequence_number = ?, last_remote_snapshot_version_seconds = ?, last_remote_snapshot_version_nanos = ?, target_count = ?"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lau1;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/p;->A(Lau1;)V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/p;->C(Lau1;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/p;->D()V

    :cond_0
    return-void
.end method

.method public b(Lcom/google/firebase/database/collection/c;I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "DELETE FROM target_documents WHERE target_id = ? AND path = ?"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->B(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/local/m;->x()Lcom/google/firebase/firestore/local/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    invoke-virtual {v2}, Ldu;->m()Lke1;

    move-result-object v3

    invoke-static {v3}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    filled-new-array {v5, v3}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Lcom/google/firebase/firestore/local/m;->s(Landroid/database/sqlite/SQLiteStatement;[Ljava/lang/Object;)I

    invoke-interface {v1, v2}, Lwc1;->a(Ldu;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(Lcom/google/firebase/firestore/core/q;)Lau1;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/firebase/firestore/local/p$c;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/firebase/firestore/local/p$c;-><init>(Lcom/google/firebase/firestore/local/p$a;)V

    iget-object v2, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v3, "SELECT target_proto FROM targets WHERE canonical_id = ?"

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v2

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v2, Lfi1;

    invoke-direct {v2, p0, p1, v1}, Lfi1;-><init>(Lcom/google/firebase/firestore/local/p;Lcom/google/firebase/firestore/core/q;Lcom/google/firebase/firestore/local/p$c;)V

    invoke-virtual {v0, v2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    iget-object p1, v1, Lcom/google/firebase/firestore/local/p$c;->a:Lau1;

    return-object p1
.end method

.method public d()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/firebase/firestore/local/p;->c:I

    return v0
.end method

.method public e(Lau1;)V
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/p;->A(Lau1;)V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/p;->C(Lau1;)Z

    iget-wide v0, p0, Lcom/google/firebase/firestore/local/p;->f:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/p;->f:J

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/p;->D()V

    return-void
.end method

.method public f(Lcom/google/firebase/database/collection/c;I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "INSERT OR IGNORE INTO target_documents (target_id, path) VALUES (?, ?)"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->B(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/local/m;->x()Lcom/google/firebase/firestore/local/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    invoke-virtual {v2}, Ldu;->m()Lke1;

    move-result-object v3

    invoke-static {v3}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    filled-new-array {v5, v3}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Lcom/google/firebase/firestore/local/m;->s(Landroid/database/sqlite/SQLiteStatement;[Ljava/lang/Object;)I

    invoke-interface {v1, v2}, Lwc1;->c(Ldu;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g(Lqo1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/local/p;->e:Lqo1;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/p;->D()V

    return-void
.end method

.method public h(I)Lcom/google/firebase/database/collection/c;
    .locals 3

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/p$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/firebase/firestore/local/p$b;-><init>(Lcom/google/firebase/firestore/local/p$a;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT path FROM target_documents WHERE target_id = ?"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    new-instance v1, Lhi1;

    invoke-direct {v1, v0}, Lhi1;-><init>(Lcom/google/firebase/firestore/local/p$b;)V

    invoke-virtual {p1, v1}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    iget-object p1, v0, Lcom/google/firebase/firestore/local/p$b;->a:Lcom/google/firebase/database/collection/c;

    return-object p1
.end method

.method public i()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->e:Lqo1;

    return-object v0
.end method

.method public final o([B)Lau1;
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->b:Lzk0;

    invoke-static {p1}, Lcom/google/firebase/firestore/proto/Target;->s0([B)Lcom/google/firebase/firestore/proto/Target;

    move-result-object p1

    invoke-virtual {v0, p1}, Lzk0;->g(Lcom/google/firebase/firestore/proto/Target;)Lau1;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string v0, "TargetData failed to parse: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public p(Lcl;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT target_proto FROM targets"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Lji1;

    invoke-direct {v1, p0, p1}, Lji1;-><init>(Lcom/google/firebase/firestore/local/p;Lcl;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    return-void
.end method

.method public q()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/google/firebase/firestore/local/p;->d:J

    return-wide v0
.end method

.method public r()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/google/firebase/firestore/local/p;->f:J

    return-wide v0
.end method

.method public x(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "DELETE FROM target_documents WHERE target_id = ?"

    invoke-virtual {v0, v1, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public y(JLandroid/util/SparseArray;)I
    .locals 3

    .line 1
    const/4 v0, 0x1

    new-array v0, v0, [I

    iget-object v1, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT target_id FROM targets WHERE last_listen_sequence_number <= ?"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    new-instance p2, Lii1;

    invoke-direct {p2, p0, p3, v0}, Lii1;-><init>(Lcom/google/firebase/firestore/local/p;Landroid/util/SparseArray;[I)V

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/p;->D()V

    const/4 p1, 0x0

    aget p1, v0, p1

    return p1
.end method

.method public final z(I)V
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/p;->x(I)V

    iget-object v0, p0, Lcom/google/firebase/firestore/local/p;->a:Lcom/google/firebase/firestore/local/m;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "DELETE FROM targets WHERE target_id = ?"

    invoke-virtual {v0, v1, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/google/firebase/firestore/local/p;->f:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/p;->f:J

    return-void
.end method

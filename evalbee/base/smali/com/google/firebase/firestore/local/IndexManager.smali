.class public interface abstract Lcom/google/firebase/firestore/local/IndexManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/local/IndexManager$IndexType;
    }
.end annotation


# virtual methods
.method public abstract a(Lke1;)V
.end method

.method public abstract b(Lcom/google/firebase/database/collection/b;)V
.end method

.method public abstract c(Lcom/google/firebase/firestore/core/q;)V
.end method

.method public abstract d(Lcom/google/firebase/firestore/core/q;)Lcom/google/firebase/firestore/model/FieldIndex$a;
.end method

.method public abstract e(Ljava/lang/String;)Lcom/google/firebase/firestore/model/FieldIndex$a;
.end method

.method public abstract f(Ljava/lang/String;Lcom/google/firebase/firestore/model/FieldIndex$a;)V
.end method

.method public abstract g(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public abstract i(Lcom/google/firebase/firestore/core/q;)Lcom/google/firebase/firestore/local/IndexManager$IndexType;
.end method

.method public abstract j(Lcom/google/firebase/firestore/core/q;)Ljava/util/List;
.end method

.method public abstract start()V
.end method

.class public final Lcom/google/firebase/firestore/local/e;
.super Ld51;
.source "SourceFile"


# instance fields
.field public final c:Ljava/util/Map;

.field public final d:Ljava/util/Map;

.field public final e:Lcom/google/firebase/firestore/local/c;

.field public final f:Liv0;

.field public final g:Lbv0;

.field public final h:Lhv0;

.field public i:Lwc1;

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ld51;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/e;->c:Ljava/util/Map;

    new-instance v0, Lcom/google/firebase/firestore/local/c;

    invoke-direct {v0}, Lcom/google/firebase/firestore/local/c;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/e;->e:Lcom/google/firebase/firestore/local/c;

    new-instance v0, Liv0;

    invoke-direct {v0, p0}, Liv0;-><init>(Lcom/google/firebase/firestore/local/e;)V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/e;->f:Liv0;

    new-instance v0, Lbv0;

    invoke-direct {v0}, Lbv0;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/e;->g:Lbv0;

    new-instance v0, Lhv0;

    invoke-direct {v0}, Lhv0;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/e;->h:Lhv0;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/e;->d:Ljava/util/Map;

    return-void
.end method

.method public static m()Lcom/google/firebase/firestore/local/e;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/e;

    invoke-direct {v0}, Lcom/google/firebase/firestore/local/e;-><init>()V

    new-instance v1, Ldv0;

    invoke-direct {v1, v0}, Ldv0;-><init>(Lcom/google/firebase/firestore/local/e;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/e;->s(Lwc1;)V

    return-object v0
.end method

.method public static n(Lcom/google/firebase/firestore/local/b$b;Lzk0;)Lcom/google/firebase/firestore/local/e;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/e;

    invoke-direct {v0}, Lcom/google/firebase/firestore/local/e;-><init>()V

    new-instance v1, Lcom/google/firebase/firestore/local/d;

    invoke-direct {v1, v0, p0, p1}, Lcom/google/firebase/firestore/local/d;-><init>(Lcom/google/firebase/firestore/local/e;Lcom/google/firebase/firestore/local/b$b;Lzk0;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/e;->s(Lwc1;)V

    return-object v0
.end method


# virtual methods
.method public a()Lgd;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->g:Lbv0;

    return-object v0
.end method

.method public b(Lu12;)Leu;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcv0;

    if-nez v0, :cond_0

    new-instance v0, Lcv0;

    invoke-direct {v0}, Lcv0;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/e;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic c(Lu12;)Lcom/google/firebase/firestore/local/IndexManager;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/e;->o(Lu12;)Lcom/google/firebase/firestore/local/c;

    move-result-object p1

    return-object p1
.end method

.method public d(Lu12;Lcom/google/firebase/firestore/local/IndexManager;)Lzx0;
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/google/firebase/firestore/local/e;->c:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lfv0;

    if-nez p2, :cond_0

    new-instance p2, Lfv0;

    invoke-direct {p2, p0, p1}, Lfv0;-><init>(Lcom/google/firebase/firestore/local/e;Lu12;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p2
.end method

.method public e()Ll21;
    .locals 1

    .line 1
    new-instance v0, Lgv0;

    invoke-direct {v0}, Lgv0;-><init>()V

    return-object v0
.end method

.method public f()Lwc1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    return-object v0
.end method

.method public bridge synthetic g()Lid1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/e;->q()Lhv0;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic h()Lyt1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/e;->r()Liv0;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/local/e;->j:Z

    return v0
.end method

.method public j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    invoke-interface {p1}, Lwc1;->f()V

    :try_start_0
    invoke-interface {p2}, Lhs1;->get()Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p2, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    invoke-interface {p2}, Lwc1;->e()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    invoke-interface {p2}, Lwc1;->e()V

    throw p1
.end method

.method public k(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    invoke-interface {p1}, Lwc1;->f()V

    :try_start_0
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    invoke-interface {p1}, Lwc1;->e()V

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    invoke-interface {p2}, Lwc1;->e()V

    throw p1
.end method

.method public l()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/local/e;->j:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "MemoryPersistence double-started!"

    invoke-static {v0, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/firebase/firestore/local/e;->j:Z

    return-void
.end method

.method public o(Lu12;)Lcom/google/firebase/firestore/local/c;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/google/firebase/firestore/local/e;->e:Lcom/google/firebase/firestore/local/c;

    return-object p1
.end method

.method public p()Ljava/lang/Iterable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public q()Lhv0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->h:Lhv0;

    return-object v0
.end method

.method public r()Liv0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/e;->f:Liv0;

    return-object v0
.end method

.method public final s(Lwc1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/local/e;->i:Lwc1;

    return-void
.end method

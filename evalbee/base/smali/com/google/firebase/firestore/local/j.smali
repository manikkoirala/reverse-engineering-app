.class public Lcom/google/firebase/firestore/local/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwc1;
.implements Lkm0;


# instance fields
.field public final a:Lcom/google/firebase/firestore/local/m;

.field public b:Lhk0;

.field public c:J

.field public final d:Lcom/google/firebase/firestore/local/b;

.field public e:Lxc1;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/local/m;Lcom/google/firebase/firestore/local/b$b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/j;->c:J

    iput-object p1, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    new-instance p1, Lcom/google/firebase/firestore/local/b;

    invoke-direct {p1, p0, p2}, Lcom/google/firebase/firestore/local/b;-><init>(Lkm0;Lcom/google/firebase/firestore/local/b$b;)V

    iput-object p1, p0, Lcom/google/firebase/firestore/local/j;->d:Lcom/google/firebase/firestore/local/b;

    return-void
.end method

.method public static synthetic q(Lcom/google/firebase/firestore/local/j;[ILjava/util/List;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/j;->w([ILjava/util/List;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic r(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/local/j;->v(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic s(Lcl;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/j;->u(Lcl;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic u(Lcl;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p0, p1}, Lcl;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic v(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method private synthetic w([ILjava/util/List;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lxw;->b(Ljava/lang/String;)Lke1;

    move-result-object p3

    invoke-static {p3}, Ldu;->g(Lke1;)Ldu;

    move-result-object p3

    invoke-virtual {p0, p3}, Lcom/google/firebase/firestore/local/j;->t(Ldu;)Z

    move-result v1

    if-nez v1, :cond_0

    aget v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p1, v0

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p3}, Lcom/google/firebase/firestore/local/j;->y(Ldu;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final A(Ldu;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Ldu;->m()Lke1;

    move-result-object p1

    invoke-static {p1}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/j;->l()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    filled-new-array {p1, v1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "INSERT OR REPLACE INTO target_documents (target_id, path, sequence_number) VALUES (0, ?, ?)"

    invoke-virtual {v0, v1, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ldu;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/j;->A(Ldu;)V

    return-void
.end method

.method public b()Lcom/google/firebase/firestore/local/b;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->d:Lcom/google/firebase/firestore/local/b;

    return-object v0
.end method

.method public c(Ldu;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/j;->A(Ldu;)V

    return-void
.end method

.method public d(Ldu;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/j;->A(Ldu;)V

    return-void
.end method

.method public e()V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/google/firebase/firestore/local/j;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v4, "Committing a transaction without having started one"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-wide v2, p0, Lcom/google/firebase/firestore/local/j;->c:J

    return-void
.end method

.method public f()V
    .locals 4

    .line 1
    iget-wide v0, p0, Lcom/google/firebase/firestore/local/j;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "Starting a transaction without committing the previous one"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->b:Lhk0;

    invoke-virtual {v0}, Lhk0;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/firebase/firestore/local/j;->c:J

    return-void
.end method

.method public g(Lau1;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/j;->l()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lau1;->l(J)Lau1;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m;->y()Lcom/google/firebase/firestore/local/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/p;->a(Lau1;)V

    return-void
.end method

.method public h(Ldu;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/j;->A(Ldu;)V

    return-void
.end method

.method public i(Lcl;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m;->y()Lcom/google/firebase/firestore/local/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/p;->p(Lcl;)V

    return-void
.end method

.method public j()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m;->u()J

    move-result-wide v0

    return-wide v0
.end method

.method public k(Lcl;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "select sequence_number from target_documents group by path having COUNT(*) = 1 AND target_id = 0"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Ltg1;

    invoke-direct {v1, p1}, Ltg1;-><init>(Lcl;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    return-void
.end method

.method public l()J
    .locals 4

    .line 1
    iget-wide v0, p0, Lcom/google/firebase/firestore/local/j;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "Attempting to get a sequence number outside of a transaction"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/google/firebase/firestore/local/j;->c:J

    return-wide v0
.end method

.method public m(J)I
    .locals 8

    .line 1
    const/4 v0, 0x1

    new-array v1, v0, [I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    move v3, v0

    :goto_1
    const/4 v4, 0x0

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    const-string v5, "select path from target_documents group by path having COUNT(*) = 1 AND target_id = 0 AND sequence_number <= ? LIMIT ?"

    invoke-virtual {v3, v5}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    filled-new-array {v5, v7}, [Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v3

    new-instance v5, Lsg1;

    invoke-direct {v5, p0, v1, v2}, Lsg1;-><init>(Lcom/google/firebase/firestore/local/j;[ILjava/util/List;)V

    invoke-virtual {v3, v5}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    move-result v3

    if-ne v3, v6, :cond_0

    goto :goto_0

    :cond_0
    move v3, v4

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/local/m;->g()Lid1;

    move-result-object p1

    invoke-interface {p1, v2}, Lid1;->removeAll(Ljava/util/Collection;)V

    aget p1, v1, v4

    return p1
.end method

.method public n(JLandroid/util/SparseArray;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m;->y()Lcom/google/firebase/firestore/local/p;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/firebase/firestore/local/p;->y(JLandroid/util/SparseArray;)I

    move-result p1

    return p1
.end method

.method public o(Lxc1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/firebase/firestore/local/j;->e:Lxc1;

    return-void
.end method

.method public p()J
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m;->y()Lcom/google/firebase/firestore/local/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/p;->r()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    const-string v3, "SELECT COUNT(*) FROM (SELECT sequence_number FROM target_documents GROUP BY path HAVING COUNT(*) = 1 AND target_id = 0)"

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v2

    new-instance v3, Lrg1;

    invoke-direct {v3}, Lrg1;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m$d;->d(Lr90;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final t(Ldu;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->e:Lxc1;

    invoke-virtual {v0, p1}, Lxc1;->c(Ldu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/j;->x(Ldu;)Z

    move-result p1

    return p1
.end method

.method public final x(Ldu;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT 1 FROM document_mutations WHERE path = ?"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    invoke-virtual {p1}, Ldu;->m()Lke1;

    move-result-object p1

    invoke-static {p1}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/local/m$d;->f()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final y(Ldu;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/j;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {p1}, Ldu;->m()Lke1;

    move-result-object p1

    invoke-static {p1}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "DELETE FROM target_documents WHERE path = ? AND target_id = 0"

    invoke-virtual {v0, v1, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public z(J)V
    .locals 1

    .line 1
    new-instance v0, Lhk0;

    invoke-direct {v0, p1, p2}, Lhk0;-><init>(J)V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/j;->b:Lhk0;

    return-void
.end method

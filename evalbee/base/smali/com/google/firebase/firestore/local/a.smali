.class public final Lcom/google/firebase/firestore/local/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/local/a$b;,
        Lcom/google/firebase/firestore/local/a$c;
    }
.end annotation


# static fields
.field public static final n:J


# instance fields
.field public final a:Ld51;

.field public b:Lcom/google/firebase/firestore/local/IndexManager;

.field public c:Lzx0;

.field public d:Leu;

.field public final e:Lid1;

.field public f:Lxk0;

.field public final g:Lcom/google/firebase/firestore/local/f;

.field public final h:Lxc1;

.field public final i:Lyt1;

.field public final j:Lgd;

.field public final k:Landroid/util/SparseArray;

.field public final l:Ljava/util/Map;

.field public final m:Lbu1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/firebase/firestore/local/a;->n:J

    return-void
.end method

.method public constructor <init>(Ld51;Lcom/google/firebase/firestore/local/f;Lu12;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ld51;->i()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "LocalStore was passed an unstarted persistence implementation"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    iput-object p2, p0, Lcom/google/firebase/firestore/local/a;->g:Lcom/google/firebase/firestore/local/f;

    invoke-virtual {p1}, Ld51;->h()Lyt1;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-virtual {p1}, Ld51;->a()Lgd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/local/a;->j:Lgd;

    invoke-interface {p2}, Lyt1;->d()I

    move-result p2

    invoke-static {p2}, Lbu1;->b(I)Lbu1;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/local/a;->m:Lbu1;

    invoke-virtual {p1}, Ld51;->g()Lid1;

    move-result-object p2

    iput-object p2, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    new-instance p2, Lxc1;

    invoke-direct {p2}, Lxc1;-><init>()V

    iput-object p2, p0, Lcom/google/firebase/firestore/local/a;->h:Lxc1;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/a;->l:Ljava/util/Map;

    invoke-virtual {p1}, Ld51;->f()Lwc1;

    move-result-object p1

    invoke-interface {p1, p2}, Lwc1;->o(Lxc1;)V

    invoke-virtual {p0, p3}, Lcom/google/firebase/firestore/local/a;->z(Lu12;)V

    return-void
.end method

.method private synthetic A(Lyx0;)Lcom/google/firebase/database/collection/b;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lyx0;->b()Lxx0;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-virtual {p1}, Lyx0;->f()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lzx0;->h(Lxx0;Lcom/google/protobuf/ByteString;)V

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/a;->o(Lyx0;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v1}, Lzx0;->a()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->d:Leu;

    invoke-virtual {p1}, Lyx0;->b()Lxx0;

    move-result-object v2

    invoke-virtual {v2}, Lxx0;->e()I

    move-result v2

    invoke-interface {v1, v2}, Leu;->b(I)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/a;->s(Lyx0;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v1, p1}, Lxk0;->o(Ljava/util/Set;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {v0}, Lxx0;->f()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lxk0;->d(Ljava/lang/Iterable;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    return-object p1
.end method

.method private synthetic B(Lcom/google/firebase/firestore/local/a$b;Lcom/google/firebase/firestore/core/q;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->m:Lbu1;

    invoke-virtual {v0}, Lbu1;->c()I

    move-result v3

    iput v3, p1, Lcom/google/firebase/firestore/local/a$b;->b:I

    new-instance v0, Lau1;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v1}, Ld51;->f()Lwc1;

    move-result-object v1

    invoke-interface {v1}, Lwc1;->l()J

    move-result-wide v4

    sget-object v6, Lcom/google/firebase/firestore/local/QueryPurpose;->LISTEN:Lcom/google/firebase/firestore/local/QueryPurpose;

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;)V

    iput-object v0, p1, Lcom/google/firebase/firestore/local/a$b;->a:Lau1;

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {p1, v0}, Lyt1;->e(Lau1;)V

    return-void
.end method

.method private synthetic C(Ljd1;Lqo1;)Lcom/google/firebase/database/collection/b;
    .locals 9

    .line 1
    invoke-virtual {p1}, Ljd1;->d()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v1}, Ld51;->f()Lwc1;

    move-result-object v1

    invoke-interface {v1}, Lwc1;->l()J

    move-result-wide v1

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzt1;

    iget-object v5, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lau1;

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-virtual {v3}, Lzt1;->d()Lcom/google/firebase/database/collection/c;

    move-result-object v7

    invoke-interface {v6, v7, v4}, Lyt1;->b(Lcom/google/firebase/database/collection/c;I)V

    iget-object v6, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-virtual {v3}, Lzt1;->b()Lcom/google/firebase/database/collection/c;

    move-result-object v7

    invoke-interface {v6, v7, v4}, Lyt1;->f(Lcom/google/firebase/database/collection/c;I)V

    invoke-virtual {v5, v1, v2}, Lau1;->l(J)Lau1;

    move-result-object v6

    invoke-virtual {p1}, Ljd1;->e()Ljava/util/Map;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    sget-object v8, Lqo1;->b:Lqo1;

    invoke-virtual {v6, v7, v8}, Lau1;->k(Lcom/google/protobuf/ByteString;Lqo1;)Lau1;

    move-result-object v6

    invoke-virtual {v6, v8}, Lau1;->j(Lqo1;)Lau1;

    move-result-object v6

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lzt1;->e()Lcom/google/protobuf/ByteString;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v3}, Lzt1;->e()Lcom/google/protobuf/ByteString;

    move-result-object v7

    invoke-virtual {p1}, Ljd1;->c()Lqo1;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lau1;->k(Lcom/google/protobuf/ByteString;Lqo1;)Lau1;

    move-result-object v6

    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v7, v4, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-static {v5, v6, v3}, Lcom/google/firebase/firestore/local/a;->R(Lau1;Lau1;Lzt1;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v3, v6}, Lyt1;->a(Lau1;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Ljd1;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Ljd1;->b()Ljava/util/Set;

    move-result-object p1

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v3}, Ld51;->f()Lwc1;

    move-result-object v3

    invoke-interface {v3, v2}, Lwc1;->h(Ldu;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/local/a;->M(Ljava/util/Map;)Lcom/google/firebase/firestore/local/a$c;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/firestore/local/a$c;->a(Lcom/google/firebase/firestore/local/a$c;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v1}, Lyt1;->i()Lqo1;

    move-result-object v1

    sget-object v2, Lqo1;->b:Lqo1;

    invoke-virtual {p2, v2}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {p2, v1}, Lqo1;->a(Lqo1;)I

    move-result v2

    if-ltz v2, :cond_7

    const/4 v2, 0x1

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    :goto_3
    const-string v3, "Watch stream reverted to previous snapshot?? (%s < %s)"

    filled-new-array {p2, v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v1, p2}, Lyt1;->g(Lqo1;)V

    :cond_8
    iget-object p2, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-static {p1}, Lcom/google/firebase/firestore/local/a$c;->b(Lcom/google/firebase/firestore/local/a$c;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Lxk0;->j(Ljava/util/Map;Ljava/util/Set;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    return-object p1
.end method

.method private synthetic D(Lcom/google/firebase/firestore/local/b;)Lcom/google/firebase/firestore/local/b$c;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/local/b;->f(Landroid/util/SparseArray;)Lcom/google/firebase/firestore/local/b$c;

    move-result-object p1

    return-object p1
.end method

.method private synthetic E(Ljava/util/List;)V
    .locals 6

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lml0;

    invoke-virtual {v0}, Lml0;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->h:Lxc1;

    invoke-virtual {v0}, Lml0;->b()Lcom/google/firebase/database/collection/c;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lxc1;->b(Lcom/google/firebase/database/collection/c;I)V

    invoke-virtual {v0}, Lml0;->c()Lcom/google/firebase/database/collection/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldu;

    iget-object v5, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v5}, Ld51;->f()Lwc1;

    move-result-object v5

    invoke-interface {v5, v4}, Lwc1;->a(Ldu;)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->h:Lxc1;

    invoke-virtual {v3, v2, v1}, Lxc1;->g(Lcom/google/firebase/database/collection/c;I)V

    invoke-virtual {v0}, Lml0;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lau1;

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "Can\'t set limbo-free snapshot version for unknown target: %s"

    invoke-static {v2, v4, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lau1;->f()Lqo1;

    move-result-object v2

    invoke-virtual {v0, v2}, Lau1;->j(Lqo1;)Lau1;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/firebase/firestore/local/a;->R(Lau1;Lau1;Lzt1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v0, v2}, Lyt1;->a(Lau1;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private synthetic F(I)Lcom/google/firebase/database/collection/b;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v0, p1}, Lzx0;->f(I)Lxx0;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    const-string v3, "Attempt to reject nonexistent batch!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v1, v0}, Lzx0;->c(Lxx0;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v1}, Lzx0;->a()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->d:Leu;

    invoke-interface {v1, p1}, Leu;->b(I)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {v0}, Lxx0;->f()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p1, v1}, Lxk0;->o(Ljava/util/Set;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {v0}, Lxx0;->f()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lxk0;->d(Ljava/lang/Iterable;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    return-object p1
.end method

.method private synthetic G(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lau1;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Tried to release nonexistent target: %s"

    invoke-static {v1, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->h:Lxc1;

    invoke-virtual {v1, p1}, Lxc1;->h(I)Lcom/google/firebase/database/collection/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v3}, Ld51;->f()Lwc1;

    move-result-object v3

    invoke-interface {v3, v2}, Lwc1;->a(Ldu;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v1}, Ld51;->f()Lwc1;

    move-result-object v1

    invoke-interface {v1, v0}, Lwc1;->g(Lau1;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lau1;->g()Lcom/google/firebase/firestore/core/q;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private synthetic H(Lcom/google/protobuf/ByteString;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v0, p1}, Lzx0;->i(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method private synthetic I()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->b:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-interface {v0}, Lcom/google/firebase/firestore/local/IndexManager;->start()V

    return-void
.end method

.method private synthetic J()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v0}, Lzx0;->start()V

    return-void
.end method

.method private synthetic K(Ljava/util/Set;Ljava/util/List;Lpw1;)Lwk0;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    invoke-interface {v0, p1}, Lid1;->getAll(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object p1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-virtual {v3}, Lcom/google/firebase/firestore/model/MutableDocument;->o()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {v1, p1}, Lxk0;->l(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwx0;

    invoke-virtual {v3}, Lwx0;->g()Ldu;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lm21;

    invoke-virtual {v4}, Lm21;->a()Lzt;

    move-result-object v4

    invoke-virtual {v3, v4}, Lwx0;->d(Lzt;)La11;

    move-result-object v4

    if-eqz v4, :cond_2

    new-instance v5, Le31;

    invoke-virtual {v3}, Lwx0;->g()Ldu;

    move-result-object v3

    invoke-virtual {v4}, La11;->k()Lq00;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v7}, Lh71;->a(Z)Lh71;

    move-result-object v7

    invoke-direct {v5, v3, v4, v6, v7}, Le31;-><init>(Ldu;La11;Lq00;Lh71;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v2, p3, v1, p2}, Lzx0;->d(Lpw1;Ljava/util/List;Ljava/util/List;)Lxx0;

    move-result-object p2

    invoke-virtual {p2, p1, v0}, Lxx0;->a(Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p3

    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->d:Leu;

    invoke-virtual {p2}, Lxx0;->e()I

    move-result v1

    invoke-interface {v0, v1, p3}, Leu;->e(ILjava/util/Map;)V

    invoke-virtual {p2}, Lxx0;->e()I

    move-result p2

    invoke-static {p2, p1}, Lwk0;->a(ILjava/util/Map;)Lwk0;

    move-result-object p1

    return-object p1
.end method

.method public static R(Lau1;Lau1;Lzt1;)Z
    .locals 6

    .line 1
    invoke-virtual {p0}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v0

    invoke-virtual {v0}, Lqo1;->c()Lpw1;

    move-result-object v0

    invoke-virtual {v0}, Lpw1;->e()J

    move-result-wide v2

    invoke-virtual {p0}, Lau1;->f()Lqo1;

    move-result-object v0

    invoke-virtual {v0}, Lqo1;->c()Lpw1;

    move-result-object v0

    invoke-virtual {v0}, Lpw1;->e()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/google/firebase/firestore/local/a;->n:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {p1}, Lau1;->b()Lqo1;

    move-result-object p1

    invoke-virtual {p1}, Lqo1;->c()Lpw1;

    move-result-object p1

    invoke-virtual {p1}, Lpw1;->e()J

    move-result-wide v2

    invoke-virtual {p0}, Lau1;->b()Lqo1;

    move-result-object p0

    invoke-virtual {p0}, Lqo1;->c()Lpw1;

    move-result-object p0

    invoke-virtual {p0}, Lpw1;->e()J

    move-result-wide p0

    sub-long/2addr v2, p0

    cmp-long p0, v2, v4

    if-ltz p0, :cond_2

    return v1

    :cond_2
    const/4 p0, 0x0

    if-nez p2, :cond_3

    return p0

    :cond_3
    invoke-virtual {p2}, Lzt1;->b()Lcom/google/firebase/database/collection/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/database/collection/c;->size()I

    move-result p1

    invoke-virtual {p2}, Lzt1;->c()Lcom/google/firebase/database/collection/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v0

    add-int/2addr p1, v0

    invoke-virtual {p2}, Lzt1;->d()Lcom/google/firebase/database/collection/c;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/firebase/database/collection/c;->size()I

    move-result p2

    add-int/2addr p1, p2

    if-lez p1, :cond_4

    goto :goto_0

    :cond_4
    move v1, p0

    :goto_0
    return v1
.end method

.method public static synthetic a(Lcom/google/firebase/firestore/local/a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/firebase/firestore/local/a;->J()V

    return-void
.end method

.method public static synthetic b(Lcom/google/firebase/firestore/local/a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/firebase/firestore/local/a;->I()V

    return-void
.end method

.method public static synthetic c(Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/local/b;)Lcom/google/firebase/firestore/local/b$c;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/a;->D(Lcom/google/firebase/firestore/local/b;)Lcom/google/firebase/firestore/local/b$c;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Lcom/google/firebase/firestore/local/a;Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/a;->H(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/firebase/firestore/local/a;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/a;->G(I)V

    return-void
.end method

.method public static synthetic f(Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/local/a$b;Lcom/google/firebase/firestore/core/q;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/local/a;->B(Lcom/google/firebase/firestore/local/a$b;Lcom/google/firebase/firestore/core/q;)V

    return-void
.end method

.method public static synthetic g(Lcom/google/firebase/firestore/local/a;Ljava/util/Set;Ljava/util/List;Lpw1;)Lwk0;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/a;->K(Ljava/util/Set;Ljava/util/List;Lpw1;)Lwk0;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic h(Lcom/google/firebase/firestore/local/a;Lyx0;)Lcom/google/firebase/database/collection/b;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/a;->A(Lyx0;)Lcom/google/firebase/database/collection/b;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic i(Lcom/google/firebase/firestore/local/a;Ljd1;Lqo1;)Lcom/google/firebase/database/collection/b;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/local/a;->C(Ljd1;Lqo1;)Lcom/google/firebase/database/collection/b;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic j(Lcom/google/firebase/firestore/local/a;I)Lcom/google/firebase/database/collection/b;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/a;->F(I)Lcom/google/firebase/database/collection/b;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic k(Lcom/google/firebase/firestore/local/a;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/a;->E(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public L(Ljava/util/List;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lal0;

    invoke-direct {v1, p0, p1}, Lal0;-><init>(Lcom/google/firebase/firestore/local/a;Ljava/util/List;)V

    const-string p1, "notifyLocalViewChanges"

    invoke-virtual {v0, p1, v1}, Ld51;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final M(Ljava/util/Map;)Lcom/google/firebase/firestore/local/a$c;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v3, v4}, Lid1;->getAll(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldu;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/firebase/firestore/model/MutableDocument;

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->d()Z

    move-result v7

    invoke-virtual {v6}, Lcom/google/firebase/firestore/model/MutableDocument;->d()Z

    move-result v8

    if-eq v7, v8, :cond_0

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->c()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v7

    sget-object v8, Lqo1;->b:Lqo1;

    invoke-virtual {v7, v8}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {v6}, Lcom/google/firebase/firestore/model/MutableDocument;->o()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v8

    invoke-virtual {v7, v8}, Lqo1;->a(Lqo1;)I

    move-result v7

    if-gtz v7, :cond_3

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v8

    invoke-virtual {v7, v8}, Lqo1;->a(Lqo1;)I

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v6}, Lcom/google/firebase/firestore/model/MutableDocument;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v6}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v4

    filled-new-array {v5, v6, v4}, [Ljava/lang/Object;

    move-result-object v4

    const-string v5, "LocalStore"

    const-string v6, "Ignoring outdated watch update for %s.Current version: %s  Watch version: %s"

    invoke-static {v5, v6, v4}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    :goto_2
    sget-object v6, Lqo1;->b:Lqo1;

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->e()Lqo1;

    move-result-object v7

    invoke-virtual {v6, v7}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "Cannot add a document when the remote version is zero"

    invoke-static {v6, v8, v7}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    invoke-virtual {v4}, Lcom/google/firebase/firestore/model/MutableDocument;->e()Lqo1;

    move-result-object v7

    invoke-interface {v6, v4, v7}, Lid1;->c(Lcom/google/firebase/firestore/model/MutableDocument;Lqo1;)V

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    invoke-interface {p1, v1}, Lid1;->removeAll(Ljava/util/Collection;)V

    new-instance p1, Lcom/google/firebase/firestore/local/a$c;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v2, v1}, Lcom/google/firebase/firestore/local/a$c;-><init>(Ljava/util/Map;Ljava/util/Set;Lcom/google/firebase/firestore/local/a$a;)V

    return-object p1
.end method

.method public N(Ldu;)Lzt;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {v0, p1}, Lxk0;->c(Ldu;)Lzt;

    move-result-object p1

    return-object p1
.end method

.method public O(I)Lcom/google/firebase/database/collection/b;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lfl0;

    invoke-direct {v1, p0, p1}, Lfl0;-><init>(Lcom/google/firebase/firestore/local/a;I)V

    const-string p1, "Reject batch"

    invoke-virtual {v0, p1, v1}, Ld51;->j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/database/collection/b;

    return-object p1
.end method

.method public P(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lil0;

    invoke-direct {v1, p0, p1}, Lil0;-><init>(Lcom/google/firebase/firestore/local/a;I)V

    const-string p1, "Release target"

    invoke-virtual {v0, p1, v1}, Ld51;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public Q(Lcom/google/protobuf/ByteString;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lhl0;

    invoke-direct {v1, p0, p1}, Lhl0;-><init>(Lcom/google/firebase/firestore/local/a;Lcom/google/protobuf/ByteString;)V

    const-string p1, "Set stream token"

    invoke-virtual {v0, p1, v1}, Ld51;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public S()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v0}, Ld51;->e()Ll21;

    move-result-object v0

    invoke-interface {v0}, Ll21;->run()V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/a;->T()V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/a;->U()V

    return-void
.end method

.method public final T()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lcl0;

    invoke-direct {v1, p0}, Lcl0;-><init>(Lcom/google/firebase/firestore/local/a;)V

    const-string v2, "Start IndexManager"

    invoke-virtual {v0, v2, v1}, Ld51;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final U()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Ldl0;

    invoke-direct {v1, p0}, Ldl0;-><init>(Lcom/google/firebase/firestore/local/a;)V

    const-string v2, "Start MutationQueue"

    invoke-virtual {v0, v2, v1}, Ld51;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public V(Ljava/util/List;)Lwk0;
    .locals 4

    .line 1
    invoke-static {}, Lpw1;->f()Lpw1;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwx0;

    invoke-virtual {v3}, Lwx0;->g()Ldu;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v3, Lel0;

    invoke-direct {v3, p0, v1, p1, v0}, Lel0;-><init>(Lcom/google/firebase/firestore/local/a;Ljava/util/Set;Ljava/util/List;Lpw1;)V

    const-string p1, "Locally write mutations"

    invoke-virtual {v2, p1, v3}, Ld51;->j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lwk0;

    return-object p1
.end method

.method public l(Lyx0;)Lcom/google/firebase/database/collection/b;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lkl0;

    invoke-direct {v1, p0, p1}, Lkl0;-><init>(Lcom/google/firebase/firestore/local/a;Lyx0;)V

    const-string p1, "Acknowledge batch"

    invoke-virtual {v0, p1, v1}, Ld51;->j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/database/collection/b;

    return-object p1
.end method

.method public m(Lcom/google/firebase/firestore/core/q;)Lau1;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v0, p1}, Lyt1;->c(Lcom/google/firebase/firestore/core/q;)Lau1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lau1;->h()I

    move-result v1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/firebase/firestore/local/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/firebase/firestore/local/a$b;-><init>(Lcom/google/firebase/firestore/local/a$a;)V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v2, Ljl0;

    invoke-direct {v2, p0, v0, p1}, Ljl0;-><init>(Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/local/a$b;Lcom/google/firebase/firestore/core/q;)V

    const-string v3, "Allocate target"

    invoke-virtual {v1, v3, v2}, Ld51;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    iget v1, v0, Lcom/google/firebase/firestore/local/a$b;->b:I

    iget-object v0, v0, Lcom/google/firebase/firestore/local/a$b;->a:Lau1;

    :goto_0
    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->l:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public n(Ljd1;)Lcom/google/firebase/database/collection/b;
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljd1;->c()Lqo1;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v2, Lbl0;

    invoke-direct {v2, p0, p1, v0}, Lbl0;-><init>(Lcom/google/firebase/firestore/local/a;Ljd1;Lqo1;)V

    const-string p1, "Apply remote event"

    invoke-virtual {v1, p1, v2}, Ld51;->j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/database/collection/b;

    return-object p1
.end method

.method public final o(Lyx0;)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Lyx0;->b()Lxx0;

    move-result-object v0

    invoke-virtual {v0}, Lxx0;->f()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    invoke-interface {v3, v2}, Lid1;->d(Ldu;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object v3

    invoke-virtual {p1}, Lyx0;->d()Lcom/google/firebase/database/collection/b;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/firebase/database/collection/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqo1;

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    move v5, v4

    :goto_1
    const-string v6, "docVersions should contain every doc in the write."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object v4

    invoke-virtual {v4, v2}, Lqo1;->a(Lqo1;)I

    move-result v2

    if-gez v2, :cond_0

    invoke-virtual {v0, v3, p1}, Lxx0;->c(Lcom/google/firebase/firestore/model/MutableDocument;Lyx0;)V

    invoke-virtual {v3}, Lcom/google/firebase/firestore/model/MutableDocument;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    invoke-virtual {p1}, Lyx0;->c()Lqo1;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lid1;->c(Lcom/google/firebase/firestore/model/MutableDocument;Lqo1;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {p1, v0}, Lzx0;->c(Lxx0;)V

    return-void
.end method

.method public p(Lcom/google/firebase/firestore/local/b;)Lcom/google/firebase/firestore/local/b$c;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    new-instance v1, Lgl0;

    invoke-direct {v1, p0, p1}, Lgl0;-><init>(Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/local/b;)V

    const-string p1, "Collect garbage"

    invoke-virtual {v0, p1, v1}, Ld51;->j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/local/b$c;

    return-object p1
.end method

.method public q(Lcom/google/firebase/firestore/core/Query;Z)Lja1;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/Query;->x()Lcom/google/firebase/firestore/core/q;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/local/a;->x(Lcom/google/firebase/firestore/core/q;)Lau1;

    move-result-object v0

    sget-object v1, Lqo1;->b:Lqo1;

    invoke-static {}, Ldu;->e()Lcom/google/firebase/database/collection/c;

    move-result-object v2

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lau1;->b()Lqo1;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-virtual {v0}, Lau1;->h()I

    move-result v0

    invoke-interface {v3, v0}, Lyt1;->h(I)Lcom/google/firebase/database/collection/c;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    move-object v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->g:Lcom/google/firebase/firestore/local/f;

    if-eqz p2, :cond_1

    move-object v1, v2

    :cond_1
    invoke-virtual {v3, p1, v1, v0}, Lcom/google/firebase/firestore/local/f;->e(Lcom/google/firebase/firestore/core/Query;Lqo1;Lcom/google/firebase/database/collection/c;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    new-instance p2, Lja1;

    invoke-direct {p2, p1, v0}, Lja1;-><init>(Lcom/google/firebase/database/collection/b;Lcom/google/firebase/database/collection/c;)V

    return-object p2
.end method

.method public r()Lcom/google/firebase/firestore/local/IndexManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->b:Lcom/google/firebase/firestore/local/IndexManager;

    return-object v0
.end method

.method public final s(Lyx0;)Ljava/util/Set;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lyx0;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1}, Lyx0;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lay0;

    invoke-virtual {v2}, Lay0;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lyx0;->b()Lxx0;

    move-result-object v2

    invoke-virtual {v2}, Lxx0;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lwx0;

    invoke-virtual {v2}, Lwx0;->g()Ldu;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public t()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v0}, Lyt1;->i()Lqo1;

    move-result-object v0

    return-object v0
.end method

.method public u()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v0}, Lzx0;->g()Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public v()Lxk0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    return-object v0
.end method

.method public w(I)Lxx0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v0, p1}, Lzx0;->e(I)Lxx0;

    move-result-object p1

    return-object p1
.end method

.method public x(Lcom/google/firebase/firestore/core/q;)Lau1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lau1;

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->i:Lyt1;

    invoke-interface {v0, p1}, Lyt1;->c(Lcom/google/firebase/firestore/core/q;)Lau1;

    move-result-object p1

    return-object p1
.end method

.method public y(Lu12;)Lcom/google/firebase/database/collection/b;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {v0}, Lzx0;->j()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/local/a;->z(Lu12;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/a;->T()V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/a;->U()V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    invoke-interface {p1}, Lzx0;->j()Ljava/util/List;

    move-result-object p1

    invoke-static {}, Ldu;->e()Lcom/google/firebase/database/collection/c;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/util/List;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lxx0;

    invoke-virtual {v2}, Lxx0;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwx0;

    invoke-virtual {v3}, Lwx0;->g()Ldu;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    invoke-virtual {p1, v1}, Lxk0;->d(Ljava/lang/Iterable;)Lcom/google/firebase/database/collection/b;

    move-result-object p1

    return-object p1
.end method

.method public final z(Lu12;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v0, p1}, Ld51;->c(Lu12;)Lcom/google/firebase/firestore/local/IndexManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/local/a;->b:Lcom/google/firebase/firestore/local/IndexManager;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v1, p1, v0}, Ld51;->d(Lu12;Lcom/google/firebase/firestore/local/IndexManager;)Lzx0;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->a:Ld51;

    invoke-virtual {v0, p1}, Ld51;->b(Lu12;)Leu;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/local/a;->d:Leu;

    new-instance v0, Lxk0;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    iget-object v2, p0, Lcom/google/firebase/firestore/local/a;->c:Lzx0;

    iget-object v3, p0, Lcom/google/firebase/firestore/local/a;->b:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-direct {v0, v1, v2, p1, v3}, Lxk0;-><init>(Lid1;Lzx0;Leu;Lcom/google/firebase/firestore/local/IndexManager;)V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->e:Lid1;

    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->b:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-interface {p1, v0}, Lid1;->e(Lcom/google/firebase/firestore/local/IndexManager;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/a;->g:Lcom/google/firebase/firestore/local/f;

    iget-object v0, p0, Lcom/google/firebase/firestore/local/a;->f:Lxk0;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/a;->b:Lcom/google/firebase/firestore/local/IndexManager;

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/firestore/local/f;->f(Lxk0;Lcom/google/firebase/firestore/local/IndexManager;)V

    return-void
.end method

.class public Lcom/google/firebase/firestore/local/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leu;


# instance fields
.field public final a:Lcom/google/firebase/firestore/local/m;

.field public final b:Lzk0;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/local/m;Lzk0;Lu12;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    iput-object p2, p0, Lcom/google/firebase/firestore/local/h;->b:Lzk0;

    invoke-virtual {p3}, Lu12;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p3}, Lu12;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    iput-object p1, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    return-void
.end method

.method public static synthetic g(Lcom/google/firebase/firestore/local/h;Landroid/database/Cursor;)Lk21;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/local/h;->n(Landroid/database/Cursor;)Lk21;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic h(Lcom/google/firebase/firestore/local/h;[I[Ljava/lang/String;[Ljava/lang/String;Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/google/firebase/firestore/local/h;->p([I[Ljava/lang/String;[Ljava/lang/String;Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic i(Lcom/google/firebase/firestore/local/h;Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->o(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic j(Lcom/google/firebase/firestore/local/h;[BILjava/util/Map;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->r([BILjava/util/Map;)V

    return-void
.end method

.method public static synthetic k(Lcom/google/firebase/firestore/local/h;Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->q(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic l(Lcom/google/firebase/firestore/local/h;Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->s(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method private synthetic n(Landroid/database/Cursor;)Lk21;
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/local/h;->m([BI)Lk21;

    move-result-object p1

    return-object p1
.end method

.method private synthetic o(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->t(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method private synthetic p([I[Ljava/lang/String;[Ljava/lang/String;Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    invoke-interface {p6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x0

    aput v0, p1, v1

    const/4 p1, 0x2

    invoke-interface {p6, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, p2, v1

    const/4 p1, 0x3

    invoke-interface {p6, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v1

    invoke-virtual {p0, p4, p5, p6}, Lcom/google/firebase/firestore/local/h;->t(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method private synthetic q(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->t(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method

.method private synthetic r([BILjava/util/Map;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/local/h;->m([BI)Lk21;

    move-result-object p1

    monitor-enter p3

    :try_start_0
    invoke-virtual {p1}, Lk21;->b()Ldu;

    move-result-object p2

    invoke-interface {p3, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p3

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private synthetic s(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/firebase/firestore/local/h;->t(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public a(Ldu;)Lk21;
    .locals 3

    .line 1
    invoke-virtual {p1}, Ldu;->m()Lke1;

    move-result-object v0

    invoke-virtual {v0}, Ljb;->n()Ljb;

    move-result-object v0

    check-cast v0, Lke1;

    invoke-static {v0}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ldu;->m()Lke1;

    move-result-object p1

    invoke-virtual {p1}, Ljb;->g()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_path = ? AND document_id = ?"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    filled-new-array {v2, v0, p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    new-instance v0, Ldg1;

    invoke-direct {v0, p0}, Ldg1;-><init>(Lcom/google/firebase/firestore/local/h;)V

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/local/m$d;->d(Lr90;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lk21;

    return-object p1
.end method

.method public b(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {v1, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "DELETE FROM document_overlays WHERE uid = ? AND largest_batch_id = ?"

    invoke-virtual {v0, v1, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public c(Lke1;I)Ljava/util/Map;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Lcb;

    invoke-direct {v1}, Lcb;-><init>()V

    iget-object v2, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    const-string v3, "SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_path = ? AND largest_batch_id > ?"

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    invoke-static {p1}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    filled-new-array {v3, p1, p2}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    new-instance p2, Lfg1;

    invoke-direct {p2, p0, v1, v0}, Lfg1;-><init>(Lcom/google/firebase/firestore/local/h;Lcb;Ljava/util/Map;)V

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    invoke-virtual {v1}, Lcb;->b()V

    return-object v0
.end method

.method public d(Ljava/lang/String;II)Ljava/util/Map;
    .locals 12

    .line 1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    new-array v9, v0, [Ljava/lang/String;

    new-array v10, v0, [I

    new-instance v11, Lcb;

    invoke-direct {v11}, Lcb;-><init>()V

    iget-object v0, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    const-string v1, "SELECT overlay_mutation, largest_batch_id, collection_path, document_id  FROM document_overlays WHERE uid = ? AND collection_group = ? AND largest_batch_id > ? ORDER BY largest_batch_id, collection_path, document_id LIMIT ?"

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    filled-new-array {v1, p1, p2, p3}, [Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p2

    new-instance p3, Lhg1;

    move-object v0, p3

    move-object v1, p0

    move-object v2, v10

    move-object v3, v8

    move-object v4, v9

    move-object v5, v11

    move-object v6, v7

    invoke-direct/range {v0 .. v6}, Lhg1;-><init>(Lcom/google/firebase/firestore/local/h;[I[Ljava/lang/String;[Ljava/lang/String;Lcb;Ljava/util/Map;)V

    invoke-virtual {p2, p3}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    const/4 p2, 0x0

    aget-object p3, v8, p2

    if-nez p3, :cond_0

    return-object v7

    :cond_0
    iget-object p3, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    const-string v0, "SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_group = ? AND (collection_path > ? OR (collection_path = ? AND document_id > ?)) AND largest_batch_id = ?"

    invoke-virtual {p3, v0}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p3

    iget-object v0, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    aget-object v3, v8, p2

    aget-object v4, v9, p2

    aget p2, v10, p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    move-object v2, v3

    filled-new-array/range {v0 .. v5}, [Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/google/firebase/firestore/local/m$d;->b([Ljava/lang/Object;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object p1

    new-instance p2, Lig1;

    invoke-direct {p2, p0, v11, v7}, Lig1;-><init>(Lcom/google/firebase/firestore/local/h;Lcb;Ljava/util/Map;)V

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    invoke-virtual {v11}, Lcb;->b()V

    return-object v7
.end method

.method public e(ILjava/util/Map;)V
    .locals 4

    .line 1
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldu;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwx0;

    const-string v2, "null value for key: %s"

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lk71;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwx0;

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/firebase/firestore/local/h;->v(ILdu;Lwx0;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f(Ljava/util/SortedSet;)Ljava/util/Map;
    .locals 6

    .line 1
    invoke-interface {p1}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "getOverlays() requires natural order"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Lcb;

    invoke-direct {v1}, Lcb;-><init>()V

    sget-object v2, Lke1;->b:Lke1;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldu;

    invoke-virtual {v4}, Ldu;->k()Lke1;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljb;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/firebase/firestore/local/h;->u(Ljava/util/Map;Lcb;Lke1;Ljava/util/List;)V

    invoke-virtual {v4}, Ldu;->k()Lke1;

    move-result-object v2

    invoke-interface {v3}, Ljava/util/List;->clear()V

    :cond_1
    invoke-virtual {v4}, Ldu;->l()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/firebase/firestore/local/h;->u(Ljava/util/Map;Lcb;Lke1;Ljava/util/List;)V

    invoke-virtual {v1}, Lcb;->b()V

    return-object v0
.end method

.method public final m([BI)Lk21;
    .locals 1

    .line 1
    :try_start_0
    invoke-static {p1}, Lcom/google/firestore/v1/Write;->w0([B)Lcom/google/firestore/v1/Write;

    move-result-object p1

    iget-object v0, p0, Lcom/google/firebase/firestore/local/h;->b:Lzk0;

    invoke-virtual {v0, p1}, Lzk0;->d(Lcom/google/firestore/v1/Write;)Lwx0;

    move-result-object p1

    invoke-static {p2, p1}, Lk21;->a(ILwx0;)Lk21;

    move-result-object p1
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string p2, "Overlay failed to parse: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {p2, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public final t(Lcb;Ljava/util/Map;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-interface {p3}, Landroid/database/Cursor;->isLast()Z

    move-result p3

    if-eqz p3, :cond_0

    sget-object p1, Lwy;->b:Ljava/util/concurrent/Executor;

    :cond_0
    new-instance p3, Lgg1;

    invoke-direct {p3, p0, v0, v1, p2}, Lgg1;-><init>(Lcom/google/firebase/firestore/local/h;[BILjava/util/Map;)V

    invoke-interface {p1, p3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final u(Ljava/util/Map;Lcb;Lke1;Ljava/util/List;)V
    .locals 7

    .line 1
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/google/firebase/firestore/local/m$b;

    iget-object v2, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    const-string v3, "SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_path = ? AND document_id IN ("

    iget-object v1, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    invoke-static {p3}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object p3

    filled-new-array {v1, p3}, [Ljava/lang/Object;

    move-result-object p3

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    const-string v6, ")"

    move-object v1, v0

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/firebase/firestore/local/m$b;-><init>(Lcom/google/firebase/firestore/local/m;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m$b;->d()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/m$b;->e()Lcom/google/firebase/firestore/local/m$d;

    move-result-object p3

    new-instance p4, Leg1;

    invoke-direct {p4, p0, p2, p1}, Leg1;-><init>(Lcom/google/firebase/firestore/local/h;Lcb;Ljava/util/Map;)V

    invoke-virtual {p3, p4}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final v(ILdu;Lwx0;)V
    .locals 6

    .line 1
    invoke-virtual {p2}, Ldu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ldu;->m()Lke1;

    move-result-object v0

    invoke-virtual {v0}, Ljb;->n()Ljb;

    move-result-object v0

    check-cast v0, Lke1;

    invoke-static {v0}, Lxw;->c(Ljb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Ldu;->m()Lke1;

    move-result-object p2

    invoke-virtual {p2}, Ljb;->g()Ljava/lang/String;

    move-result-object v3

    iget-object p2, p0, Lcom/google/firebase/firestore/local/h;->a:Lcom/google/firebase/firestore/local/m;

    iget-object v0, p0, Lcom/google/firebase/firestore/local/h;->c:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object p1, p0, Lcom/google/firebase/firestore/local/h;->b:Lzk0;

    invoke-virtual {p1, p3}, Lzk0;->l(Lwx0;)Lcom/google/firestore/v1/Write;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/a;->toByteArray()[B

    move-result-object v5

    filled-new-array/range {v0 .. v5}, [Ljava/lang/Object;

    move-result-object p1

    const-string p3, "INSERT OR REPLACE INTO document_overlays (uid, collection_group, collection_path, document_id, largest_batch_id, overlay_mutation) VALUES (?, ?, ?, ?, ?, ?)"

    invoke-virtual {p2, p3, p1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

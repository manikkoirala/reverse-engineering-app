.class public Lcom/google/firebase/firestore/local/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ll21;


# instance fields
.field public final a:Lcom/google/firebase/firestore/local/m;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/local/m;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    return-void
.end method

.method public static synthetic a(Ljava/util/Set;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/l;->h(Ljava/util/Set;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic b([Ljava/lang/Boolean;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/l;->i([Ljava/lang/Boolean;Landroid/database/Cursor;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/firebase/firestore/local/l;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/firebase/firestore/local/l;->g()V

    return-void
.end method

.method private synthetic g()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/l;->f()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/l;->e()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/local/m;->g()Lid1;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Lu12;

    invoke-direct {v3, v2}, Lu12;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/local/m;->c(Lu12;)Lcom/google/firebase/firestore/local/IndexManager;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/firebase/firestore/local/m;->d(Lu12;Lcom/google/firebase/firestore/local/IndexManager;)Lzx0;

    move-result-object v2

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v2}, Lzx0;->j()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lxx0;

    invoke-virtual {v6}, Lxx0;->f()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v5, v3}, Lcom/google/firebase/firestore/local/m;->b(Lu12;)Leu;

    move-result-object v5

    new-instance v6, Lxk0;

    iget-object v7, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    invoke-virtual {v7, v3}, Lcom/google/firebase/firestore/local/m;->c(Lu12;)Lcom/google/firebase/firestore/local/IndexManager;

    move-result-object v3

    invoke-direct {v6, v1, v2, v5, v3}, Lxk0;-><init>(Lid1;Lzx0;Leu;Lcom/google/firebase/firestore/local/IndexManager;)V

    invoke-virtual {v6, v4}, Lxk0;->o(Ljava/util/Set;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/l;->j()V

    return-void
.end method

.method public static synthetic h(Ljava/util/Set;Landroid/database/Cursor;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static synthetic i([Ljava/lang/Boolean;Landroid/database/Cursor;)V
    .locals 2

    .line 1
    :try_start_0
    sget-object v0, Ld51;->b:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object p1, p0, v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p0

    const-string p1, "SQLitePersistence.DataMigration failed to parse: %s"

    filled-new-array {p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-static {p1, p0}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p0

    throw p0
.end method


# virtual methods
.method public final d()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    new-instance v1, Ldh1;

    invoke-direct {v1, p0}, Ldh1;-><init>(Lcom/google/firebase/firestore/local/l;)V

    const-string v2, "build overlays"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/firestore/local/m;->k(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final e()Ljava/util/Set;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    const-string v2, "SELECT DISTINCT uid FROM mutation_queues"

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    new-instance v2, Lfh1;

    invoke-direct {v2, v0}, Lfh1;-><init>(Ljava/util/Set;)V

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    return-object v0
.end method

.method public f()Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    const-string v3, "SELECT migration_name FROM data_migrations"

    invoke-virtual {v1, v3}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v1

    new-instance v3, Leh1;

    invoke-direct {v3, v0}, Leh1;-><init>([Ljava/lang/Boolean;)V

    invoke-virtual {v1, v3}, Lcom/google/firebase/firestore/local/m$d;->e(Lcl;)I

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/l;->a:Lcom/google/firebase/firestore/local/m;

    sget-object v1, Ld51;->b:Ljava/lang/String;

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "DELETE FROM data_migrations WHERE migration_name = ?"

    invoke-virtual {v0, v2, v1}, Lcom/google/firebase/firestore/local/m;->t(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public run()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/l;->d()V

    return-void
.end method

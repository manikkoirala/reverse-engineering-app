.class public final Lcom/google/firebase/firestore/local/m;
.super Ld51;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/local/m$b;,
        Lcom/google/firebase/firestore/local/m$d;,
        Lcom/google/firebase/firestore/local/m$c;
    }
.end annotation


# instance fields
.field public final c:Lcom/google/firebase/firestore/local/m$c;

.field public final d:Lzk0;

.field public final e:Lcom/google/firebase/firestore/local/p;

.field public final f:Lcom/google/firebase/firestore/local/g;

.field public final g:Lcom/google/firebase/firestore/local/n;

.field public final h:Lcom/google/firebase/firestore/local/j;

.field public final i:Landroid/database/sqlite/SQLiteTransactionListener;

.field public j:Landroid/database/sqlite/SQLiteDatabase;

.field public k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lqp;Lzk0;Lcom/google/firebase/firestore/local/b$b;)V
    .locals 1

    .line 2
    new-instance v0, Lcom/google/firebase/firestore/local/m$c;

    invoke-static {p2, p3}, Lcom/google/firebase/firestore/local/m;->r(Ljava/lang/String;Lqp;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    invoke-direct {v0, p1, p4, p2, p3}, Lcom/google/firebase/firestore/local/m$c;-><init>(Landroid/content/Context;Lzk0;Ljava/lang/String;Lcom/google/firebase/firestore/local/m$a;)V

    invoke-direct {p0, p4, p5, v0}, Lcom/google/firebase/firestore/local/m;-><init>(Lzk0;Lcom/google/firebase/firestore/local/b$b;Lcom/google/firebase/firestore/local/m$c;)V

    return-void
.end method

.method public constructor <init>(Lzk0;Lcom/google/firebase/firestore/local/b$b;Lcom/google/firebase/firestore/local/m$c;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ld51;-><init>()V

    new-instance v0, Lcom/google/firebase/firestore/local/m$a;

    invoke-direct {v0, p0}, Lcom/google/firebase/firestore/local/m$a;-><init>(Lcom/google/firebase/firestore/local/m;)V

    iput-object v0, p0, Lcom/google/firebase/firestore/local/m;->i:Landroid/database/sqlite/SQLiteTransactionListener;

    iput-object p3, p0, Lcom/google/firebase/firestore/local/m;->c:Lcom/google/firebase/firestore/local/m$c;

    iput-object p1, p0, Lcom/google/firebase/firestore/local/m;->d:Lzk0;

    new-instance p3, Lcom/google/firebase/firestore/local/p;

    invoke-direct {p3, p0, p1}, Lcom/google/firebase/firestore/local/p;-><init>(Lcom/google/firebase/firestore/local/m;Lzk0;)V

    iput-object p3, p0, Lcom/google/firebase/firestore/local/m;->e:Lcom/google/firebase/firestore/local/p;

    new-instance p3, Lcom/google/firebase/firestore/local/g;

    invoke-direct {p3, p0, p1}, Lcom/google/firebase/firestore/local/g;-><init>(Lcom/google/firebase/firestore/local/m;Lzk0;)V

    iput-object p3, p0, Lcom/google/firebase/firestore/local/m;->f:Lcom/google/firebase/firestore/local/g;

    new-instance p3, Lcom/google/firebase/firestore/local/n;

    invoke-direct {p3, p0, p1}, Lcom/google/firebase/firestore/local/n;-><init>(Lcom/google/firebase/firestore/local/m;Lzk0;)V

    iput-object p3, p0, Lcom/google/firebase/firestore/local/m;->g:Lcom/google/firebase/firestore/local/n;

    new-instance p1, Lcom/google/firebase/firestore/local/j;

    invoke-direct {p1, p0, p2}, Lcom/google/firebase/firestore/local/j;-><init>(Lcom/google/firebase/firestore/local/m;Lcom/google/firebase/firestore/local/b$b;)V

    iput-object p1, p0, Lcom/google/firebase/firestore/local/m;->h:Lcom/google/firebase/firestore/local/j;

    return-void
.end method

.method public static synthetic A(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic m(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/local/m;->A(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic n(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/local/m;->z(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic o(Lcom/google/firebase/firestore/local/m;)Lcom/google/firebase/firestore/local/j;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/firebase/firestore/local/m;->h:Lcom/google/firebase/firestore/local/j;

    return-object p0
.end method

.method public static synthetic p(Landroid/database/sqlite/SQLiteProgram;[Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/firestore/local/m;->q(Landroid/database/sqlite/SQLiteProgram;[Ljava/lang/Object;)V

    return-void
.end method

.method public static q(Landroid/database/sqlite/SQLiteProgram;[Ljava/lang/Object;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_6

    aget-object v1, p1, v0

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteProgram;->bindNull(I)V

    goto :goto_2

    :cond_0
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    add-int/lit8 v2, v0, 0x1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v2, v1}, Landroid/database/sqlite/SQLiteProgram;->bindString(ILjava/lang/String;)V

    goto :goto_2

    :cond_1
    instance-of v2, v1, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    add-int/lit8 v2, v0, 0x1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v3, v1

    :goto_1
    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteProgram;->bindLong(IJ)V

    goto :goto_2

    :cond_2
    instance-of v2, v1, Ljava/lang/Long;

    if-eqz v2, :cond_3

    add-int/lit8 v2, v0, 0x1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    goto :goto_1

    :cond_3
    instance-of v2, v1, Ljava/lang/Double;

    if-eqz v2, :cond_4

    add-int/lit8 v2, v0, 0x1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteProgram;->bindDouble(ID)V

    goto :goto_2

    :cond_4
    instance-of v2, v1, [B

    if-eqz v2, :cond_5

    add-int/lit8 v2, v0, 0x1

    check-cast v1, [B

    invoke-virtual {p0, v2, v1}, Landroid/database/sqlite/SQLiteProgram;->bindBlob(I[B)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    filled-new-array {v1, p0}, [Ljava/lang/Object;

    move-result-object p0

    const-string p1, "Unknown argument %s of type %s"

    invoke-static {p1, p0}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p0

    throw p0

    :cond_6
    return-void
.end method

.method public static r(Ljava/lang/String;Lqp;)Ljava/lang/String;
    .locals 4

    .line 1
    const-string v0, "."

    const-string v1, "utf-8"

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "firestore."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lqp;->f()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lqp;->e()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public static synthetic z(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public B(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p1

    return-object p1
.end method

.method public C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/m$d;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1, p1}, Lcom/google/firebase/firestore/local/m$d;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Lgd;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->f:Lcom/google/firebase/firestore/local/g;

    return-object v0
.end method

.method public b(Lu12;)Leu;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/h;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/m;->d:Lzk0;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/firebase/firestore/local/h;-><init>(Lcom/google/firebase/firestore/local/m;Lzk0;Lu12;)V

    return-object v0
.end method

.method public c(Lu12;)Lcom/google/firebase/firestore/local/IndexManager;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/i;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/m;->d:Lzk0;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/firebase/firestore/local/i;-><init>(Lcom/google/firebase/firestore/local/m;Lzk0;Lu12;)V

    return-object v0
.end method

.method public d(Lu12;Lcom/google/firebase/firestore/local/IndexManager;)Lzx0;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/k;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/m;->d:Lzk0;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/firebase/firestore/local/k;-><init>(Lcom/google/firebase/firestore/local/m;Lzk0;Lu12;Lcom/google/firebase/firestore/local/IndexManager;)V

    return-object v0
.end method

.method public e()Ll21;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/local/l;

    invoke-direct {v0, p0}, Lcom/google/firebase/firestore/local/l;-><init>(Lcom/google/firebase/firestore/local/m;)V

    return-object v0
.end method

.method public bridge synthetic f()Lwc1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/m;->x()Lcom/google/firebase/firestore/local/j;

    move-result-object v0

    return-object v0
.end method

.method public g()Lid1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->g:Lcom/google/firebase/firestore/local/n;

    return-object v0
.end method

.method public bridge synthetic h()Lyt1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/m;->y()Lcom/google/firebase/firestore/local/p;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/local/m;->k:Z

    return v0
.end method

.method public j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Ld51;->a:Ljava/lang/String;

    const-string v1, "Starting transaction: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->i:Landroid/database/sqlite/SQLiteTransactionListener;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    :try_start_0
    invoke-interface {p2}, Lhs1;->get()Ljava/lang/Object;

    move-result-object p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p2, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw p1
.end method

.method public k(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    sget-object v0, Ld51;->a:Ljava/lang/String;

    const-string v1, "Starting transaction: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->i:Landroid/database/sqlite/SQLiteTransactionListener;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    :try_start_0
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    iget-object p1, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw p1
.end method

.method public l()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/local/m;->k:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "SQLitePersistence double-started!"

    invoke-static {v0, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/firebase/firestore/local/m;->k:Z

    :try_start_0
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->c:Lcom/google/firebase/firestore/local/m$c;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDatabaseLockedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->e:Lcom/google/firebase/firestore/local/p;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/p;->B()V

    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->h:Lcom/google/firebase/firestore/local/j;

    iget-object v1, p0, Lcom/google/firebase/firestore/local/m;->e:Lcom/google/firebase/firestore/local/p;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/local/p;->q()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/firestore/local/j;->z(J)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to gain exclusive lock to the Cloud Firestore client\'s offline persistence. This generally means you are using Cloud Firestore from multiple processes in your app. Keep in mind that multi-process Android apps execute the code in your Application class in all processes, so you may need to avoid initializing Cloud Firestore in your Application class. If you are intentionally using Cloud Firestore from multiple processes, you can only enable offline persistence (that is, call setPersistenceEnabled(true)) in one of them."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public varargs s(Landroid/database/sqlite/SQLiteStatement;[Ljava/lang/Object;)I
    .locals 0

    .line 1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteProgram;->clearBindings()V

    invoke-static {p1, p2}, Lcom/google/firebase/firestore/local/m;->q(Landroid/database/sqlite/SQLiteProgram;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result p1

    return p1
.end method

.method public varargs t(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->j:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public u()J
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/m;->v()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/local/m;->w()J

    move-result-wide v2

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final v()J
    .locals 2

    .line 1
    const-string v0, "PRAGMA page_count"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Lhh1;

    invoke-direct {v1}, Lhh1;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->d(Lr90;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final w()J
    .locals 2

    .line 1
    const-string v0, "PRAGMA page_size"

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/local/m;->C(Ljava/lang/String;)Lcom/google/firebase/firestore/local/m$d;

    move-result-object v0

    new-instance v1, Lgh1;

    invoke-direct {v1}, Lgh1;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/m$d;->d(Lr90;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public x()Lcom/google/firebase/firestore/local/j;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->h:Lcom/google/firebase/firestore/local/j;

    return-object v0
.end method

.method public y()Lcom/google/firebase/firestore/local/p;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/local/m;->e:Lcom/google/firebase/firestore/local/p;

    return-object v0
.end method

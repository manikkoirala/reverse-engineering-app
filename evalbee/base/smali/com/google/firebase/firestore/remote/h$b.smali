.class public abstract Lcom/google/firebase/firestore/remote/h$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/firestore/remote/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(IILjava/lang/String;Ljava/lang/String;Lcom/google/firebase/firestore/remote/h$a;)Lcom/google/firebase/firestore/remote/h$b;
    .locals 7

    .line 1
    new-instance v6, Lcom/google/firebase/firestore/remote/c;

    move-object v0, v6

    move v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/remote/c;-><init>(IILjava/lang/String;Ljava/lang/String;Lcom/google/firebase/firestore/remote/h$a;)V

    return-object v6
.end method

.method public static e(ILhz;Lqp;Lcom/google/firebase/firestore/remote/BloomFilter;Lcom/google/firebase/firestore/remote/WatchChangeAggregator$BloomFilterApplicationStatus;)Lcom/google/firebase/firestore/remote/h$b;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lhz;->a()I

    move-result v0

    invoke-virtual {p2}, Lqp;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lqp;->e()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p4, p1}, Lcom/google/firebase/firestore/remote/h$a;->e(Lcom/google/firebase/firestore/remote/BloomFilter;Lcom/google/firebase/firestore/remote/WatchChangeAggregator$BloomFilterApplicationStatus;Lhz;)Lcom/google/firebase/firestore/remote/h$a;

    move-result-object p1

    invoke-static {p0, v0, v1, p2, p1}, Lcom/google/firebase/firestore/remote/h$b;->b(IILjava/lang/String;Ljava/lang/String;Lcom/google/firebase/firestore/remote/h$a;)Lcom/google/firebase/firestore/remote/h$b;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract a()Lcom/google/firebase/firestore/remote/h$a;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()I
.end method

.method public abstract f()I
.end method

.method public abstract g()Ljava/lang/String;
.end method

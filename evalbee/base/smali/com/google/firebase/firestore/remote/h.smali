.class public final Lcom/google/firebase/firestore/remote/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/remote/h$a;,
        Lcom/google/firebase/firestore/remote/h$b;
    }
.end annotation


# static fields
.field public static final b:Lcom/google/firebase/firestore/remote/h;


# instance fields
.field public final a:Ljava/util/concurrent/CopyOnWriteArrayList;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/firebase/firestore/remote/h;

    invoke-direct {v0}, Lcom/google/firebase/firestore/remote/h;-><init>()V

    sput-object v0, Lcom/google/firebase/firestore/remote/h;->b:Lcom/google/firebase/firestore/remote/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/h;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method public static a()Lcom/google/firebase/firestore/remote/h;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/h;->b:Lcom/google/firebase/firestore/remote/h;

    return-object v0
.end method


# virtual methods
.method public b(Lcom/google/firebase/firestore/remote/h$b;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/google/firebase/firestore/remote/h;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lzu0;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

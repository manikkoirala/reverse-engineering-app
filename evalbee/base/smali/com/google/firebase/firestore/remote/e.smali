.class public Lcom/google/firebase/firestore/remote/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/remote/e$a;
    }
.end annotation


# instance fields
.field public a:Lcom/google/firebase/firestore/core/OnlineState;

.field public b:I

.field public c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

.field public d:Z

.field public final e:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final f:Lcom/google/firebase/firestore/remote/e$a;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/e$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/e;->e:Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p2, p0, Lcom/google/firebase/firestore/remote/e;->f:Lcom/google/firebase/firestore/remote/e$a;

    sget-object p1, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/e;->a:Lcom/google/firebase/firestore/core/OnlineState;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/firebase/firestore/remote/e;->d:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/firebase/firestore/remote/e;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/firebase/firestore/remote/e;->f()V

    return-void
.end method

.method private synthetic f()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/e;->c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->a:Lcom/google/firebase/firestore/core/OnlineState;

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const-string v1, "Timer should be canceled if we transitioned to a different state."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Backend didn\'t respond within %d seconds\n"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/e;->g(Ljava/lang/String;)V

    sget-object v0, Lcom/google/firebase/firestore/core/OnlineState;->OFFLINE:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/e;->h(Lcom/google/firebase/firestore/core/OnlineState;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue$b;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/e;->c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    return-void
.end method

.method public c()Lcom/google/firebase/firestore/core/OnlineState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->a:Lcom/google/firebase/firestore/core/OnlineState;

    return-object v0
.end method

.method public d(Lio/grpc/Status;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->a:Lcom/google/firebase/firestore/core/OnlineState;

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->ONLINE:Lcom/google/firebase/firestore/core/OnlineState;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_2

    sget-object p1, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/e;->h(Lcom/google/firebase/firestore/core/OnlineState;)V

    iget p1, p0, Lcom/google/firebase/firestore/remote/e;->b:I

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move p1, v2

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    const-string v1, "watchStreamFailures must be 0"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1, v1, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/e;->c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v0

    :goto_1
    const-string p1, "onlineStateTimer must be null"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, p1, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    iget v0, p0, Lcom/google/firebase/firestore/remote/e;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/firebase/firestore/remote/e;->b:I

    if-lt v0, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/e;->b()V

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "Connection failed %d times. Most recent error: %s"

    invoke-static {v0, v1, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/e;->g(Ljava/lang/String;)V

    sget-object p1, Lcom/google/firebase/firestore/core/OnlineState;->OFFLINE:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/e;->h(Lcom/google/firebase/firestore/core/OnlineState;)V

    :cond_3
    :goto_2
    return-void
.end method

.method public e()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/google/firebase/firestore/remote/e;->b:I

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/e;->h(Lcom/google/firebase/firestore/core/OnlineState;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "onlineStateTimer shouldn\'t be started yet"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->e:Lcom/google/firebase/firestore/util/AsyncQueue;

    sget-object v1, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->ONLINE_STATE_TIMEOUT:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    new-instance v2, Lu11;

    invoke-direct {v2, p0}, Lu11;-><init>(Lcom/google/firebase/firestore/remote/e;)V

    const-wide/16 v3, 0x2710

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/google/firebase/firestore/util/AsyncQueue;->h(Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;JLjava/lang/Runnable;)Lcom/google/firebase/firestore/util/AsyncQueue$b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/e;->c:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_1
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "Could not reach Cloud Firestore backend. %s\nThis typically indicates that your device does not have a healthy Internet connection at the moment. The client will operate in offline mode until it is able to successfully connect to the backend."

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/e;->d:Z

    const-string v1, "%s"

    const-string v2, "OnlineStateTracker"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    if-eqz v0, :cond_0

    invoke-static {v2, v1, p1}, Lcom/google/firebase/firestore/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/firebase/firestore/remote/e;->d:Z

    goto :goto_0

    :cond_0
    invoke-static {v2, v1, p1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final h(Lcom/google/firebase/firestore/core/OnlineState;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->a:Lcom/google/firebase/firestore/core/OnlineState;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/e;->a:Lcom/google/firebase/firestore/core/OnlineState;

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/e;->f:Lcom/google/firebase/firestore/remote/e$a;

    invoke-interface {v0, p1}, Lcom/google/firebase/firestore/remote/e$a;->a(Lcom/google/firebase/firestore/core/OnlineState;)V

    :cond_0
    return-void
.end method

.method public i(Lcom/google/firebase/firestore/core/OnlineState;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/e;->b()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/firebase/firestore/remote/e;->b:I

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->ONLINE:Lcom/google/firebase/firestore/core/OnlineState;

    if-ne p1, v1, :cond_0

    iput-boolean v0, p0, Lcom/google/firebase/firestore/remote/e;->d:Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/e;->h(Lcom/google/firebase/firestore/core/OnlineState;)V

    return-void
.end method

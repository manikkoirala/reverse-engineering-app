.class public Lcom/google/firebase/firestore/remote/i;
.super Lg0;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/remote/i$a;
    }
.end annotation


# static fields
.field public static final t:Lcom/google/protobuf/ByteString;


# instance fields
.field public final s:Lcom/google/firebase/firestore/remote/f;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    sput-object v0, Lcom/google/firebase/firestore/remote/i;->t:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method public constructor <init>(Lw30;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/f;Lcom/google/firebase/firestore/remote/i$a;)V
    .locals 8

    .line 1
    invoke-static {}, Lh40;->a()Lio/grpc/MethodDescriptor;

    move-result-object v2

    sget-object v4, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->LISTEN_STREAM_CONNECTION_BACKOFF:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    sget-object v5, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->LISTEN_STREAM_IDLE:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    sget-object v6, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->HEALTH_CHECK_TIMEOUT:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lg0;-><init>(Lw30;Lio/grpc/MethodDescriptor;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcr1;)V

    iput-object p3, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    return-void
.end method


# virtual methods
.method public A(Lau1;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/i;->m()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Watching queries requires an open stream"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/firestore/v1/ListenRequest;->h0()Lcom/google/firestore/v1/ListenRequest$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/remote/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/ListenRequest$b;->C(Ljava/lang/String;)Lcom/google/firestore/v1/ListenRequest$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->R(Lau1;)Lcom/google/firestore/v1/Target;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/ListenRequest$b;->B(Lcom/google/firestore/v1/Target;)Lcom/google/firestore/v1/ListenRequest$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->K(Lau1;)Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/ListenRequest$b;->A(Ljava/util/Map;)Lcom/google/firestore/v1/ListenRequest$b;

    :cond_0
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/ListenRequest;

    invoke-virtual {p0, p1}, Lg0;->x(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic l()V
    .locals 0

    .line 1
    invoke-super {p0}, Lg0;->l()V

    return-void
.end method

.method public bridge synthetic m()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lg0;->m()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic n()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lg0;->n()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic r(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/google/firestore/v1/ListenResponse;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/i;->y(Lcom/google/firestore/v1/ListenResponse;)V

    return-void
.end method

.method public bridge synthetic u()V
    .locals 0

    .line 1
    invoke-super {p0}, Lg0;->u()V

    return-void
.end method

.method public bridge synthetic v()V
    .locals 0

    .line 1
    invoke-super {p0}, Lg0;->v()V

    return-void
.end method

.method public y(Lcom/google/firestore/v1/ListenResponse;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/a;->f()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/f;->x(Lcom/google/firestore/v1/ListenResponse;)Lcom/google/firebase/firestore/remote/WatchChange;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->w(Lcom/google/firestore/v1/ListenResponse;)Lqo1;

    move-result-object p1

    iget-object v1, p0, Lg0;->m:Lcr1;

    check-cast v1, Lcom/google/firebase/firestore/remote/i$a;

    invoke-interface {v1, p1, v0}, Lcom/google/firebase/firestore/remote/i$a;->e(Lqo1;Lcom/google/firebase/firestore/remote/WatchChange;)V

    return-void
.end method

.method public z(I)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/i;->m()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Unwatching targets requires an open stream"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/firestore/v1/ListenRequest;->h0()Lcom/google/firestore/v1/ListenRequest$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/i;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/remote/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/ListenRequest$b;->C(Ljava/lang/String;)Lcom/google/firestore/v1/ListenRequest$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/ListenRequest$b;->D(I)Lcom/google/firestore/v1/ListenRequest$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/ListenRequest;

    invoke-virtual {p0, p1}, Lg0;->x(Ljava/lang/Object;)V

    return-void
.end method

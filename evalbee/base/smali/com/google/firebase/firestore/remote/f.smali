.class public final Lcom/google/firebase/firestore/remote/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lqp;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lqp;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/f;->a:Lqp;

    invoke-static {p1}, Lcom/google/firebase/firestore/remote/f;->V(Lqp;)Lke1;

    move-result-object p1

    invoke-virtual {p1}, Lke1;->d()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/f;->b:Ljava/lang/String;

    return-void
.end method

.method public static V(Lqp;)Lke1;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lqp;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "databases"

    invoke-virtual {p0}, Lqp;->e()Ljava/lang/String;

    move-result-object p0

    const-string v2, "projects"

    filled-new-array {v2, v0, v1, p0}, [Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Lke1;->p(Ljava/util/List;)Lke1;

    move-result-object p0

    return-object p0
.end method

.method public static W(Lke1;)Lke1;
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljb;->l()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    invoke-virtual {p0, v1}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "documents"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Tried to deserialize invalid key %s"

    filled-new-array {p0}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Ljb;->m(I)Ljb;

    move-result-object p0

    check-cast p0, Lke1;

    return-object p0
.end method

.method public static Y(Lke1;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljb;->l()I

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "projects"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljb;->h(I)Ljava/lang/String;

    move-result-object p0

    const-string v0, "databases"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method


# virtual methods
.method public A(Ldu;La11;)Lcom/google/firestore/v1/e;
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/e;->k0()Lcom/google/firestore/v1/e$b;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->I(Ldu;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/e$b;->B(Ljava/lang/String;)Lcom/google/firestore/v1/e$b;

    invoke-virtual {p2}, La11;->l()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/e$b;->A(Ljava/util/Map;)Lcom/google/firestore/v1/e$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/e;

    return-object p1
.end method

.method public final B(Lq00;)Lcom/google/firestore/v1/h;
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/h;->g0()Lcom/google/firestore/v1/h$b;

    move-result-object v0

    invoke-virtual {p1}, Lq00;->c()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls00;

    invoke-virtual {v1}, Ls00;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/h$b;->A(Ljava/lang/String;)Lcom/google/firestore/v1/h$b;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/h;

    return-object p1
.end method

.method public C(Lcom/google/firebase/firestore/core/q;)Lcom/google/firestore/v1/Target$c;
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/Target$c;->g0()Lcom/google/firestore/v1/Target$c$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->n()Lke1;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->O(Lke1;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/Target$c$a;->A(Ljava/lang/String;)Lcom/google/firestore/v1/Target$c$a;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Target$c;

    return-object p1
.end method

.method public final D(Lcom/google/firebase/firestore/core/FieldFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->i:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unknown operator %d"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :pswitch_0
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->NOT_IN:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_1
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->ARRAY_CONTAINS_ANY:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->IN:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->ARRAY_CONTAINS:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_4
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->GREATER_THAN_OR_EQUAL:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_5
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->GREATER_THAN:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_6
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->NOT_EQUAL:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_7
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->EQUAL:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_8
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->LESS_THAN_OR_EQUAL:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_9
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;->LESS_THAN:Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final E(Ls00;)Lcom/google/firestore/v1/StructuredQuery$d;
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$d;->d0()Lcom/google/firestore/v1/StructuredQuery$d$a;

    move-result-object v0

    invoke-virtual {p1}, Ls00;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/StructuredQuery$d$a;->A(Ljava/lang/String;)Lcom/google/firestore/v1/StructuredQuery$d$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/StructuredQuery$d;

    return-object p1
.end method

.method public final F(Lu00;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lu00;->b()Loy1;

    move-result-object v0

    instance-of v1, v0, Lpl1;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->l0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object v0

    invoke-virtual {p1}, Lu00;->a()Ls00;

    move-result-object p1

    invoke-virtual {p1}, Ls00;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->B(Ljava/lang/String;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    sget-object v0, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;->REQUEST_TIME:Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->E(Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;

    return-object p1

    :cond_0
    instance-of v1, v0, Lu8$b;

    if-eqz v1, :cond_1

    check-cast v0, Lu8$b;

    invoke-static {}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->l0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object v1

    invoke-virtual {p1}, Lu00;->a()Ls00;

    move-result-object p1

    invoke-virtual {p1}, Ls00;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->B(Ljava/lang/String;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    invoke-static {}, Lcom/google/firestore/v1/a;->j0()Lcom/google/firestore/v1/a$b;

    move-result-object v1

    invoke-virtual {v0}, Lu8;->f()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/firestore/v1/a$b;->A(Ljava/lang/Iterable;)Lcom/google/firestore/v1/a$b;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->A(Lcom/google/firestore/v1/a$b;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lu8$a;

    if-eqz v1, :cond_2

    check-cast v0, Lu8$a;

    invoke-static {}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->l0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object v1

    invoke-virtual {p1}, Lu00;->a()Ls00;

    move-result-object p1

    invoke-virtual {p1}, Ls00;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->B(Ljava/lang/String;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    invoke-static {}, Lcom/google/firestore/v1/a;->j0()Lcom/google/firestore/v1/a$b;

    move-result-object v1

    invoke-virtual {v0}, Lu8;->f()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/firestore/v1/a$b;->A(Ljava/lang/Iterable;)Lcom/google/firestore/v1/a$b;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->D(Lcom/google/firestore/v1/a$b;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lq01;

    if-eqz v1, :cond_3

    check-cast v0, Lq01;

    invoke-static {}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->l0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object v1

    invoke-virtual {p1}, Lu00;->a()Ls00;

    move-result-object p1

    invoke-virtual {p1}, Ls00;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->B(Ljava/lang/String;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    invoke-virtual {v0}, Lq01;->d()Lcom/google/firestore/v1/Value;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;->C(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform$a;

    move-result-object p1

    goto :goto_0

    :cond_3
    const-string p1, "Unknown transform: %s"

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public G(Li10;)Lcom/google/firestore/v1/StructuredQuery$Filter;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/google/firebase/firestore/core/FieldFilter;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/firebase/firestore/core/FieldFilter;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->T(Lcom/google/firebase/firestore/core/FieldFilter;)Lcom/google/firestore/v1/StructuredQuery$Filter;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Lcom/google/firebase/firestore/core/CompositeFilter;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/firebase/firestore/core/CompositeFilter;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->y(Lcom/google/firebase/firestore/core/CompositeFilter;)Lcom/google/firestore/v1/StructuredQuery$Filter;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Unrecognized filter type %s"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public final H(Ljava/util/List;)Lcom/google/firestore/v1/StructuredQuery$Filter;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/core/CompositeFilter;

    sget-object v1, Lcom/google/firebase/firestore/core/CompositeFilter$Operator;->AND:Lcom/google/firebase/firestore/core/CompositeFilter$Operator;

    invoke-direct {v0, p1, v1}, Lcom/google/firebase/firestore/core/CompositeFilter;-><init>(Ljava/util/List;Lcom/google/firebase/firestore/core/CompositeFilter$Operator;)V

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/f;->G(Li10;)Lcom/google/firestore/v1/StructuredQuery$Filter;

    move-result-object p1

    return-object p1
.end method

.method public I(Ldu;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/f;->a:Lqp;

    invoke-virtual {p1}, Ldu;->m()Lke1;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/remote/f;->Q(Lqp;Lke1;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final J(Lcom/google/firebase/firestore/local/QueryPurpose;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->d:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string p1, "limbo-document"

    return-object p1

    :cond_0
    const-string v0, "Unrecognized query purpose: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    const-string p1, "existence-filter-mismatch-bloom"

    return-object p1

    :cond_2
    const-string p1, "existence-filter-mismatch"

    return-object p1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public K(Lau1;)Ljava/util/Map;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lau1;->c()Lcom/google/firebase/firestore/local/QueryPurpose;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->J(Lcom/google/firebase/firestore/local/QueryPurpose;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    const-string v1, "goog-listen-tags"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public L(Lwx0;)Lcom/google/firestore/v1/Write;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/Write;->u0()Lcom/google/firestore/v1/Write$b;

    move-result-object v0

    instance-of v1, p1, Lqm1;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lwx0;->g()Ldu;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Lqm1;

    invoke-virtual {v2}, Lqm1;->o()La11;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/firebase/firestore/remote/f;->A(Ldu;La11;)Lcom/google/firestore/v1/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Write$b;->D(Lcom/google/firestore/v1/e;)Lcom/google/firestore/v1/Write$b;

    goto :goto_0

    :cond_0
    instance-of v1, p1, Le31;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lwx0;->g()Ldu;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Le31;

    invoke-virtual {v2}, Le31;->q()La11;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/firebase/firestore/remote/f;->A(Ldu;La11;)Lcom/google/firestore/v1/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Write$b;->D(Lcom/google/firestore/v1/e;)Lcom/google/firestore/v1/Write$b;

    invoke-virtual {p1}, Lwx0;->e()Lq00;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->B(Lq00;)Lcom/google/firestore/v1/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Write$b;->E(Lcom/google/firestore/v1/h;)Lcom/google/firestore/v1/Write$b;

    goto :goto_0

    :cond_1
    instance-of v1, p1, Lhs;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lwx0;->g()Ldu;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->I(Ldu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Write$b;->C(Ljava/lang/String;)Lcom/google/firestore/v1/Write$b;

    goto :goto_0

    :cond_2
    instance-of v1, p1, Lh32;

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lwx0;->g()Ldu;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->I(Ldu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Write$b;->F(Ljava/lang/String;)Lcom/google/firestore/v1/Write$b;

    :goto_0
    invoke-virtual {p1}, Lwx0;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lu00;

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->F(Lu00;)Lcom/google/firestore/v1/DocumentTransform$FieldTransform;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/firestore/v1/Write$b;->A(Lcom/google/firestore/v1/DocumentTransform$FieldTransform;)Lcom/google/firestore/v1/Write$b;

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lwx0;->h()Lh71;

    move-result-object v1

    invoke-virtual {v1}, Lh71;->d()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lwx0;->h()Lh71;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->N(Lh71;)Lcom/google/firestore/v1/Precondition;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/Write$b;->B(Lcom/google/firestore/v1/Precondition;)Lcom/google/firestore/v1/Write$b;

    :cond_4
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Write;

    return-object p1

    :cond_5
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "unknown mutation type %s"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public final M(Lcom/google/firebase/firestore/core/OrderBy;)Lcom/google/firestore/v1/StructuredQuery$e;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$e;->e0()Lcom/google/firestore/v1/StructuredQuery$e$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/OrderBy;->b()Lcom/google/firebase/firestore/core/OrderBy$Direction;

    move-result-object v1

    sget-object v2, Lcom/google/firebase/firestore/core/OrderBy$Direction;->ASCENDING:Lcom/google/firebase/firestore/core/OrderBy$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/firestore/v1/StructuredQuery$Direction;->ASCENDING:Lcom/google/firestore/v1/StructuredQuery$Direction;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/google/firestore/v1/StructuredQuery$Direction;->DESCENDING:Lcom/google/firestore/v1/StructuredQuery$Direction;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/StructuredQuery$e$a;->A(Lcom/google/firestore/v1/StructuredQuery$Direction;)Lcom/google/firestore/v1/StructuredQuery$e$a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/OrderBy;->c()Ls00;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->E(Ls00;)Lcom/google/firestore/v1/StructuredQuery$d;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/StructuredQuery$e$a;->B(Lcom/google/firestore/v1/StructuredQuery$d;)Lcom/google/firestore/v1/StructuredQuery$e$a;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/StructuredQuery$e;

    return-object p1
.end method

.method public final N(Lh71;)Lcom/google/firestore/v1/Precondition;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lh71;->d()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Can\'t serialize an empty precondition"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/firestore/v1/Precondition;->g0()Lcom/google/firestore/v1/Precondition$b;

    move-result-object v0

    invoke-virtual {p1}, Lh71;->c()Lqo1;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lh71;->c()Lqo1;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->U(Lqo1;)Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/Precondition$b;->B(Lcom/google/protobuf/j0;)Lcom/google/firestore/v1/Precondition$b;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Precondition;

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lh71;->b()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lh71;->b()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/Precondition$b;->A(Z)Lcom/google/firestore/v1/Precondition$b;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, "Unknown Precondition"

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public final O(Lke1;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/f;->a:Lqp;

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/remote/f;->Q(Lqp;Lke1;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public P(Lcom/google/firebase/firestore/core/q;)Lcom/google/firestore/v1/Target$QueryTarget;
    .locals 7

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/Target$QueryTarget;->f0()Lcom/google/firestore/v1/Target$QueryTarget$a;

    move-result-object v0

    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery;->x0()Lcom/google/firestore/v1/StructuredQuery$b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->n()Lke1;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljb;->l()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_0

    move v3, v4

    goto :goto_0

    :cond_0
    move v3, v5

    :goto_0
    const-string v6, "Collection Group queries should be within a document path or root."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v6, v5}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->O(Lke1;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/firestore/v1/Target$QueryTarget$a;->A(Ljava/lang/String;)Lcom/google/firestore/v1/Target$QueryTarget$a;

    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$c;->e0()Lcom/google/firestore/v1/StructuredQuery$c$a;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/firestore/v1/StructuredQuery$c$a;->B(Ljava/lang/String;)Lcom/google/firestore/v1/StructuredQuery$c$a;

    invoke-virtual {v2, v4}, Lcom/google/firestore/v1/StructuredQuery$c$a;->A(Z)Lcom/google/firestore/v1/StructuredQuery$c$a;

    invoke-virtual {v1, v2}, Lcom/google/firestore/v1/StructuredQuery$b;->A(Lcom/google/firestore/v1/StructuredQuery$c$a;)Lcom/google/firestore/v1/StructuredQuery$b;

    goto :goto_2

    :cond_1
    invoke-virtual {v2}, Ljb;->l()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_2

    move v3, v4

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    const-string v6, "Document queries with filters are not supported."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v6, v5}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Ljb;->n()Ljb;

    move-result-object v3

    check-cast v3, Lke1;

    invoke-virtual {p0, v3}, Lcom/google/firebase/firestore/remote/f;->O(Lke1;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/firestore/v1/Target$QueryTarget$a;->A(Ljava/lang/String;)Lcom/google/firestore/v1/Target$QueryTarget$a;

    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$c;->e0()Lcom/google/firestore/v1/StructuredQuery$c$a;

    move-result-object v3

    invoke-virtual {v2}, Ljb;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/firestore/v1/StructuredQuery$c$a;->B(Ljava/lang/String;)Lcom/google/firestore/v1/StructuredQuery$c$a;

    invoke-virtual {v1, v3}, Lcom/google/firestore/v1/StructuredQuery$b;->A(Lcom/google/firestore/v1/StructuredQuery$c$a;)Lcom/google/firestore/v1/StructuredQuery$b;

    :goto_2
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->h()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->H(Ljava/util/List;)Lcom/google/firestore/v1/StructuredQuery$Filter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firestore/v1/StructuredQuery$b;->F(Lcom/google/firestore/v1/StructuredQuery$Filter;)Lcom/google/firestore/v1/StructuredQuery$b;

    :cond_3
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->m()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/firebase/firestore/core/OrderBy;

    invoke-virtual {p0, v3}, Lcom/google/firebase/firestore/remote/f;->M(Lcom/google/firebase/firestore/core/OrderBy;)Lcom/google/firestore/v1/StructuredQuery$e;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/firestore/v1/StructuredQuery$b;->B(Lcom/google/firestore/v1/StructuredQuery$e;)Lcom/google/firestore/v1/StructuredQuery$b;

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->r()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/google/protobuf/r;->d0()Lcom/google/protobuf/r$b;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->j()J

    move-result-wide v5

    long-to-int v3, v5

    invoke-virtual {v2, v3}, Lcom/google/protobuf/r$b;->A(I)Lcom/google/protobuf/r$b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firestore/v1/StructuredQuery$b;->D(Lcom/google/protobuf/r$b;)Lcom/google/firestore/v1/StructuredQuery$b;

    :cond_5
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->p()Lcom/google/firebase/firestore/core/c;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/google/firestore/v1/d;->g0()Lcom/google/firestore/v1/d$b;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->p()Lcom/google/firebase/firestore/core/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/firestore/core/c;->b()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/firestore/v1/d$b;->A(Ljava/lang/Iterable;)Lcom/google/firestore/v1/d$b;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->p()Lcom/google/firebase/firestore/core/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/firestore/core/c;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/firestore/v1/d$b;->B(Z)Lcom/google/firestore/v1/d$b;

    invoke-virtual {v1, v2}, Lcom/google/firestore/v1/StructuredQuery$b;->E(Lcom/google/firestore/v1/d$b;)Lcom/google/firestore/v1/StructuredQuery$b;

    :cond_6
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->f()Lcom/google/firebase/firestore/core/c;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/google/firestore/v1/d;->g0()Lcom/google/firestore/v1/d$b;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->f()Lcom/google/firebase/firestore/core/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/firestore/core/c;->b()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/firestore/v1/d$b;->A(Ljava/lang/Iterable;)Lcom/google/firestore/v1/d$b;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->f()Lcom/google/firebase/firestore/core/c;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/c;->c()Z

    move-result p1

    xor-int/2addr p1, v4

    invoke-virtual {v2, p1}, Lcom/google/firestore/v1/d$b;->B(Z)Lcom/google/firestore/v1/d$b;

    invoke-virtual {v1, v2}, Lcom/google/firestore/v1/StructuredQuery$b;->C(Lcom/google/firestore/v1/d$b;)Lcom/google/firestore/v1/StructuredQuery$b;

    :cond_7
    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Target$QueryTarget$a;->B(Lcom/google/firestore/v1/StructuredQuery$b;)Lcom/google/firestore/v1/Target$QueryTarget$a;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Target$QueryTarget;

    return-object p1
.end method

.method public final Q(Lqp;Lke1;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/google/firebase/firestore/remote/f;->V(Lqp;)Lke1;

    move-result-object p1

    const-string v0, "documents"

    invoke-virtual {p1, v0}, Ljb;->c(Ljava/lang/String;)Ljb;

    move-result-object p1

    check-cast p1, Lke1;

    invoke-virtual {p1, p2}, Ljb;->a(Ljb;)Ljb;

    move-result-object p1

    check-cast p1, Lke1;

    invoke-virtual {p1}, Lke1;->d()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public R(Lau1;)Lcom/google/firestore/v1/Target;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/Target;->g0()Lcom/google/firestore/v1/Target$b;

    move-result-object v0

    invoke-virtual {p1}, Lau1;->g()Lcom/google/firebase/firestore/core/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/firestore/core/q;->s()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->C(Lcom/google/firebase/firestore/core/q;)Lcom/google/firestore/v1/Target$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Target$b;->A(Lcom/google/firestore/v1/Target$c;)Lcom/google/firestore/v1/Target$b;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->P(Lcom/google/firebase/firestore/core/q;)Lcom/google/firestore/v1/Target$QueryTarget;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Target$b;->C(Lcom/google/firestore/v1/Target$QueryTarget;)Lcom/google/firestore/v1/Target$b;

    :goto_0
    invoke-virtual {p1}, Lau1;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Target$b;->F(I)Lcom/google/firestore/v1/Target$b;

    invoke-virtual {p1}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v1

    sget-object v2, Lqo1;->b:Lqo1;

    invoke-virtual {v1, v2}, Lqo1;->a(Lqo1;)I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v1

    invoke-virtual {v1}, Lqo1;->c()Lpw1;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->S(Lpw1;)Lcom/google/protobuf/j0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Target$b;->D(Lcom/google/protobuf/j0;)Lcom/google/firestore/v1/Target$b;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Target$b;->E(Lcom/google/protobuf/ByteString;)Lcom/google/firestore/v1/Target$b;

    :goto_1
    invoke-virtual {p1}, Lau1;->a()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v1

    sget-object v2, Lqo1;->b:Lqo1;

    invoke-virtual {v1, v2}, Lqo1;->a(Lqo1;)I

    move-result v1

    if-lez v1, :cond_3

    :cond_2
    invoke-static {}, Lcom/google/protobuf/r;->d0()Lcom/google/protobuf/r$b;

    move-result-object v1

    invoke-virtual {p1}, Lau1;->a()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/r$b;->A(I)Lcom/google/protobuf/r$b;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/Target$b;->B(Lcom/google/protobuf/r$b;)Lcom/google/firestore/v1/Target$b;

    :cond_3
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Target;

    return-object p1
.end method

.method public S(Lpw1;)Lcom/google/protobuf/j0;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/protobuf/j0;->f0()Lcom/google/protobuf/j0$b;

    move-result-object v0

    invoke-virtual {p1}, Lpw1;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/j0$b;->B(J)Lcom/google/protobuf/j0$b;

    invoke-virtual {p1}, Lpw1;->d()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/j0$b;->A(I)Lcom/google/protobuf/j0$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/j0;

    return-object p1
.end method

.method public T(Lcom/google/firebase/firestore/core/FieldFilter;)Lcom/google/firestore/v1/StructuredQuery$Filter;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->g()Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->g()Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    move-result-object v0

    sget-object v2, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->NOT_EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    if-ne v0, v2, :cond_4

    :cond_0
    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;->f0()Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->f()Ls00;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->E(Ls00;)Lcom/google/firestore/v1/StructuredQuery$d;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$a;->A(Lcom/google/firestore/v1/StructuredQuery$d;)Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->h()Lcom/google/firestore/v1/Value;

    move-result-object v2

    invoke-static {v2}, La32;->y(Lcom/google/firestore/v1/Value;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->g()Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    move-result-object p1

    if-ne p1, v1, :cond_1

    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;->IS_NAN:Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;->IS_NOT_NAN:Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$a;->B(Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$a;

    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$Filter;->i0()Lcom/google/firestore/v1/StructuredQuery$Filter$a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/StructuredQuery$Filter$a;->C(Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$a;)Lcom/google/firestore/v1/StructuredQuery$Filter$a;

    move-result-object p1

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/StructuredQuery$Filter;

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->h()Lcom/google/firestore/v1/Value;

    move-result-object v2

    invoke-static {v2}, La32;->z(Lcom/google/firestore/v1/Value;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->g()Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    move-result-object p1

    if-ne p1, v1, :cond_3

    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;->IS_NULL:Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;->IS_NOT_NULL:Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter;->h0()Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->f()Ls00;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->E(Ls00;)Lcom/google/firestore/v1/StructuredQuery$d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;->A(Lcom/google/firestore/v1/StructuredQuery$d;)Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->g()Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->D(Lcom/google/firebase/firestore/core/FieldFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;->B(Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/FieldFilter;->h()Lcom/google/firestore/v1/Value;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;->C(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;

    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$Filter;->i0()Lcom/google/firestore/v1/StructuredQuery$Filter$a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/StructuredQuery$Filter$a;->B(Lcom/google/firestore/v1/StructuredQuery$FieldFilter$a;)Lcom/google/firestore/v1/StructuredQuery$Filter$a;

    move-result-object p1

    goto :goto_1
.end method

.method public U(Lqo1;)Lcom/google/protobuf/j0;
    .locals 0

    .line 1
    invoke-virtual {p1}, Lqo1;->c()Lpw1;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->S(Lpw1;)Lcom/google/protobuf/j0;

    move-result-object p1

    return-object p1
.end method

.method public final X(Lbq1;)Lio/grpc/Status;
    .locals 1

    .line 1
    invoke-virtual {p1}, Lbq1;->a0()I

    move-result v0

    invoke-static {v0}, Lio/grpc/Status;->fromCodeValue(I)Lio/grpc/Status;

    move-result-object v0

    invoke-virtual {p1}, Lbq1;->c0()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/grpc/Status;->withDescription(Ljava/lang/String;)Lio/grpc/Status;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/google/firestore/v1/StructuredQuery$CompositeFilter;)Lcom/google/firebase/firestore/core/CompositeFilter;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter;->f0()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firestore/v1/StructuredQuery$Filter;

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->i(Lcom/google/firestore/v1/StructuredQuery$Filter;)Li10;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/firebase/firestore/core/CompositeFilter;

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter;->g0()Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->c(Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;)Lcom/google/firebase/firestore/core/CompositeFilter$Operator;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lcom/google/firebase/firestore/core/CompositeFilter;-><init>(Ljava/util/List;Lcom/google/firebase/firestore/core/CompositeFilter$Operator;)V

    return-object v1
.end method

.method public c(Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;)Lcom/google/firebase/firestore/core/CompositeFilter$Operator;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->f:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/google/firebase/firestore/core/CompositeFilter$Operator;->OR:Lcom/google/firebase/firestore/core/CompositeFilter$Operator;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Only AND and OR composite filter types are supported."

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    sget-object p1, Lcom/google/firebase/firestore/core/CompositeFilter$Operator;->AND:Lcom/google/firebase/firestore/core/CompositeFilter$Operator;

    return-object p1
.end method

.method public final d(Lcom/google/firestore/v1/h;)Lq00;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/h;->f0()I

    move-result v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/firestore/v1/h;->e0(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lq00;->b(Ljava/util/Set;)Lq00;

    move-result-object p1

    return-object p1
.end method

.method public e(Lcom/google/firestore/v1/Target$c;)Lcom/google/firebase/firestore/core/q;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/Target$c;->f0()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v3, "DocumentsTarget contained other than 1 document %d"

    invoke-static {v2, v3, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Lcom/google/firestore/v1/Target$c;->e0(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->p(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/firestore/core/Query;->b(Lke1;)Lcom/google/firebase/firestore/core/Query;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/Query;->x()Lcom/google/firebase/firestore/core/q;

    move-result-object p1

    return-object p1
.end method

.method public f(Lcom/google/firestore/v1/StructuredQuery$FieldFilter;)Lcom/google/firebase/firestore/core/FieldFilter;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter;->e0()Lcom/google/firestore/v1/StructuredQuery$d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firestore/v1/StructuredQuery$d;->c0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter;->f0()Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->g(Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;)Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$FieldFilter;->g0()Lcom/google/firestore/v1/Value;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/core/FieldFilter;->e(Ls00;Lcom/google/firebase/firestore/core/FieldFilter$Operator;Lcom/google/firestore/v1/Value;)Lcom/google/firebase/firestore/core/FieldFilter;

    move-result-object p1

    return-object p1
.end method

.method public final g(Lcom/google/firestore/v1/StructuredQuery$FieldFilter$Operator;)Lcom/google/firebase/firestore/core/FieldFilter$Operator;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->j:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unhandled FieldFilter.operator %d"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :pswitch_0
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->NOT_IN:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_1
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->ARRAY_CONTAINS_ANY:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->IN:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->ARRAY_CONTAINS:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_4
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->GREATER_THAN:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_5
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->GREATER_THAN_OR_EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_6
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->NOT_EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_7
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_8
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->LESS_THAN_OR_EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_9
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->LESS_THAN:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final h(Lcom/google/firestore/v1/DocumentTransform$FieldTransform;)Lu00;
    .locals 3

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->c:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->k0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$TransformTypeCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    new-instance v0, Lu00;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->g0()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v1

    new-instance v2, Lq01;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->h0()Lcom/google/firestore/v1/Value;

    move-result-object p1

    invoke-direct {v2, p1}, Lq01;-><init>(Lcom/google/firestore/v1/Value;)V

    invoke-direct {v0, v1, v2}, Lu00;-><init>(Ls00;Loy1;)V

    return-object v0

    :cond_0
    const-string v0, "Unknown FieldTransform proto: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    new-instance v0, Lu00;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->g0()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v1

    new-instance v2, Lu8$a;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->i0()Lcom/google/firestore/v1/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/a;->h()Ljava/util/List;

    move-result-object p1

    invoke-direct {v2, p1}, Lu8$a;-><init>(Ljava/util/List;)V

    invoke-direct {v0, v1, v2}, Lu00;-><init>(Ls00;Loy1;)V

    return-object v0

    :cond_2
    new-instance v0, Lu00;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->g0()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v1

    new-instance v2, Lu8$b;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->f0()Lcom/google/firestore/v1/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/a;->h()Ljava/util/List;

    move-result-object p1

    invoke-direct {v2, p1}, Lu8$b;-><init>(Ljava/util/List;)V

    invoke-direct {v0, v1, v2}, Lu00;-><init>(Ls00;Loy1;)V

    return-object v0

    :cond_3
    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->j0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;

    move-result-object v0

    sget-object v2, Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;->REQUEST_TIME:Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;

    if-ne v0, v2, :cond_4

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->j0()Lcom/google/firestore/v1/DocumentTransform$FieldTransform$ServerValue;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v2, "Unknown transform setToServerValue: %s"

    invoke-static {v1, v2, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lu00;

    invoke-virtual {p1}, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;->g0()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object p1

    invoke-static {}, Lpl1;->d()Lpl1;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lu00;-><init>(Ls00;Loy1;)V

    return-object v0
.end method

.method public i(Lcom/google/firestore/v1/StructuredQuery$Filter;)Li10;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->g:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$Filter;->g0()Lcom/google/firestore/v1/StructuredQuery$Filter$FilterTypeCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$Filter;->h0()Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->u(Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;)Li10;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$Filter;->g0()Lcom/google/firestore/v1/StructuredQuery$Filter$FilterTypeCase;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Unrecognized Filter.filterType %d"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$Filter;->f0()Lcom/google/firestore/v1/StructuredQuery$FieldFilter;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->f(Lcom/google/firestore/v1/StructuredQuery$FieldFilter;)Lcom/google/firebase/firestore/core/FieldFilter;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$Filter;->d0()Lcom/google/firestore/v1/StructuredQuery$CompositeFilter;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->b(Lcom/google/firestore/v1/StructuredQuery$CompositeFilter;)Lcom/google/firebase/firestore/core/CompositeFilter;

    move-result-object p1

    return-object p1
.end method

.method public final j(Lcom/google/firestore/v1/StructuredQuery$Filter;)Ljava/util/List;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->i(Lcom/google/firestore/v1/StructuredQuery$Filter;)Li10;

    move-result-object p1

    instance-of v0, p1, Lcom/google/firebase/firestore/core/CompositeFilter;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/firebase/firestore/core/CompositeFilter;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/CompositeFilter;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/CompositeFilter;->b()Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public k(Ljava/lang/String;)Ldu;
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->s(Ljava/lang/String;)Lke1;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/f;->a:Lqp;

    invoke-virtual {v1}, Lqp;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "Tried to deserialize key from different project."

    invoke-static {v0, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/f;->a:Lqp;

    invoke-virtual {v2}, Lqp;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "Tried to deserialize key from different database."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/firebase/firestore/remote/f;->W(Lke1;)Lke1;

    move-result-object p1

    invoke-static {p1}, Ldu;->g(Lke1;)Ldu;

    move-result-object p1

    return-object p1
.end method

.method public l(Lcom/google/firestore/v1/Write;)Lwx0;
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->q0()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->i0()Lcom/google/firestore/v1/Precondition;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/f;->o(Lcom/google/firestore/v1/Precondition;)Lh71;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lh71;->c:Lh71;

    :goto_0
    move-object v5, v0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->o0()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->h(Lcom/google/firestore/v1/DocumentTransform$FieldTransform;)Lu00;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->a:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->k0()Lcom/google/firestore/v1/Write$OperationCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    new-instance v0, Lh32;

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->p0()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object p1

    invoke-direct {v0, p1, v5}, Lh32;-><init>(Ldu;Lh71;)V

    return-object v0

    :cond_2
    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->k0()Lcom/google/firestore/v1/Write$OperationCase;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Unknown mutation operation: %d"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_3
    new-instance v0, Lhs;

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->j0()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object p1

    invoke-direct {v0, p1, v5}, Lhs;-><init>(Ldu;Lh71;)V

    return-object v0

    :cond_4
    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->t0()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Le31;

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->m0()Lcom/google/firestore/v1/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firestore/v1/e;->g0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->m0()Lcom/google/firestore/v1/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firestore/v1/e;->e0()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, La11;->i(Ljava/util/Map;)La11;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->n0()Lcom/google/firestore/v1/h;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->d(Lcom/google/firestore/v1/h;)Lq00;

    move-result-object v4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Le31;-><init>(Ldu;La11;Lq00;Lh71;Ljava/util/List;)V

    return-object v0

    :cond_5
    new-instance v0, Lqm1;

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->m0()Lcom/google/firestore/v1/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firestore/v1/e;->g0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firestore/v1/Write;->m0()Lcom/google/firestore/v1/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/e;->e0()Ljava/util/Map;

    move-result-object p1

    invoke-static {p1}, La11;->i(Ljava/util/Map;)La11;

    move-result-object p1

    invoke-direct {v0, v1, p1, v5, v6}, Lqm1;-><init>(Ldu;La11;Lh71;Ljava/util/List;)V

    return-object v0
.end method

.method public m(Lcom/google/firestore/v1/n;Lqo1;)Lay0;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/n;->c0()Lcom/google/protobuf/j0;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object v0

    sget-object v1, Lqo1;->b:Lqo1;

    invoke-virtual {v1, v0}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move-object p2, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/firestore/v1/n;->b0()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/firestore/v1/n;->a0(I)Lcom/google/firestore/v1/Value;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance p1, Lay0;

    invoke-direct {p1, p2, v1}, Lay0;-><init>(Lqo1;Ljava/util/List;)V

    return-object p1
.end method

.method public final n(Lcom/google/firestore/v1/StructuredQuery$e;)Lcom/google/firebase/firestore/core/OrderBy;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$e;->d0()Lcom/google/firestore/v1/StructuredQuery$d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firestore/v1/StructuredQuery$d;->c0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/firestore/remote/f$a;->k:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$e;->c0()Lcom/google/firestore/v1/StructuredQuery$Direction;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    sget-object p1, Lcom/google/firebase/firestore/core/OrderBy$Direction;->DESCENDING:Lcom/google/firebase/firestore/core/OrderBy$Direction;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$e;->c0()Lcom/google/firestore/v1/StructuredQuery$Direction;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Unrecognized direction %d"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    sget-object p1, Lcom/google/firebase/firestore/core/OrderBy$Direction;->ASCENDING:Lcom/google/firebase/firestore/core/OrderBy$Direction;

    :goto_0
    invoke-static {p1, v0}, Lcom/google/firebase/firestore/core/OrderBy;->d(Lcom/google/firebase/firestore/core/OrderBy$Direction;Ls00;)Lcom/google/firebase/firestore/core/OrderBy;

    move-result-object p1

    return-object p1
.end method

.method public final o(Lcom/google/firestore/v1/Precondition;)Lh71;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->b:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/Precondition;->c0()Lcom/google/firestore/v1/Precondition$ConditionTypeCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 p1, 0x3

    if-ne v0, p1, :cond_0

    sget-object p1, Lh71;->c:Lh71;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Unknown precondition"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    invoke-virtual {p1}, Lcom/google/firestore/v1/Precondition;->e0()Z

    move-result p1

    invoke-static {p1}, Lh71;->a(Z)Lh71;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lcom/google/firestore/v1/Precondition;->f0()Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object p1

    invoke-static {p1}, Lh71;->f(Lqo1;)Lh71;

    move-result-object p1

    return-object p1
.end method

.method public final p(Ljava/lang/String;)Lke1;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->s(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-virtual {p1}, Ljb;->l()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    sget-object p1, Lke1;->b:Lke1;

    return-object p1

    :cond_0
    invoke-static {p1}, Lcom/google/firebase/firestore/remote/f;->W(Lke1;)Lke1;

    move-result-object p1

    return-object p1
.end method

.method public q(Lcom/google/firestore/v1/Target$QueryTarget;)Lcom/google/firebase/firestore/core/q;
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/Target$QueryTarget;->d0()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firestore/v1/Target$QueryTarget;->e0()Lcom/google/firestore/v1/StructuredQuery;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/google/firebase/firestore/remote/f;->r(Ljava/lang/String;Lcom/google/firestore/v1/StructuredQuery;)Lcom/google/firebase/firestore/core/q;

    move-result-object p1

    return-object p1
.end method

.method public r(Ljava/lang/String;Lcom/google/firestore/v1/StructuredQuery;)Lcom/google/firebase/firestore/core/q;
    .locals 13

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->p(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->n0()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v0, :cond_2

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v4, "StructuredQuery.from with more than one collection is not supported."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p2, v1}, Lcom/google/firestore/v1/StructuredQuery;->m0(I)Lcom/google/firestore/v1/StructuredQuery$c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firestore/v1/StructuredQuery$c;->c0()Z

    move-result v4

    invoke-virtual {v0}, Lcom/google/firestore/v1/StructuredQuery$c;->d0()Ljava/lang/String;

    move-result-object v0

    if-eqz v4, :cond_1

    move-object v5, p1

    move-object v6, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v0}, Ljb;->c(Ljava/lang/String;)Ljb;

    move-result-object p1

    check-cast p1, Lke1;

    :cond_2
    move-object v5, p1

    move-object v6, v3

    :goto_1
    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->w0()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->s0()Lcom/google/firestore/v1/StructuredQuery$Filter;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->j(Lcom/google/firestore/v1/StructuredQuery$Filter;)Ljava/util/List;

    move-result-object p1

    goto :goto_2

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_2
    move-object v7, p1

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->q0()I

    move-result p1

    if-lez p1, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_3
    if-ge v1, p1, :cond_4

    invoke-virtual {p2, v1}, Lcom/google/firestore/v1/StructuredQuery;->p0(I)Lcom/google/firestore/v1/StructuredQuery$e;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/firebase/firestore/remote/f;->n(Lcom/google/firestore/v1/StructuredQuery$e;)Lcom/google/firebase/firestore/core/OrderBy;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    move-object v8, v0

    goto :goto_4

    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    move-object v8, p1

    :goto_4
    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->u0()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->o0()Lcom/google/protobuf/r;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/r;->c0()I

    move-result p1

    int-to-long v0, p1

    goto :goto_5

    :cond_6
    const-wide/16 v0, -0x1

    :goto_5
    move-wide v9, v0

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->v0()Z

    move-result p1

    if-eqz p1, :cond_7

    new-instance p1, Lcom/google/firebase/firestore/core/c;

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->r0()Lcom/google/firestore/v1/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firestore/v1/d;->h()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->r0()Lcom/google/firestore/v1/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firestore/v1/d;->e0()Z

    move-result v1

    invoke-direct {p1, v0, v1}, Lcom/google/firebase/firestore/core/c;-><init>(Ljava/util/List;Z)V

    move-object v11, p1

    goto :goto_6

    :cond_7
    move-object v11, v3

    :goto_6
    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->t0()Z

    move-result p1

    if-eqz p1, :cond_8

    new-instance v3, Lcom/google/firebase/firestore/core/c;

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->l0()Lcom/google/firestore/v1/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/d;->h()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2}, Lcom/google/firestore/v1/StructuredQuery;->l0()Lcom/google/firestore/v1/d;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/firestore/v1/d;->e0()Z

    move-result p2

    xor-int/2addr p2, v2

    invoke-direct {v3, p1, p2}, Lcom/google/firebase/firestore/core/c;-><init>(Ljava/util/List;Z)V

    :cond_8
    move-object v12, v3

    new-instance p1, Lcom/google/firebase/firestore/core/q;

    move-object v4, p1

    invoke-direct/range {v4 .. v12}, Lcom/google/firebase/firestore/core/q;-><init>(Lke1;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JLcom/google/firebase/firestore/core/c;Lcom/google/firebase/firestore/core/c;)V

    return-object p1
.end method

.method public final s(Ljava/lang/String;)Lke1;
    .locals 3

    .line 1
    invoke-static {p1}, Lke1;->q(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-static {p1}, Lcom/google/firebase/firestore/remote/f;->Y(Lke1;)Z

    move-result v0

    const-string v1, "Tried to deserialize invalid key %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-object p1
.end method

.method public t(Lcom/google/protobuf/j0;)Lpw1;
    .locals 3

    .line 1
    new-instance v0, Lpw1;

    invoke-virtual {p1}, Lcom/google/protobuf/j0;->e0()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/protobuf/j0;->d0()I

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lpw1;-><init>(JI)V

    return-object v0
.end method

.method public final u(Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;)Li10;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;->d0()Lcom/google/firestore/v1/StructuredQuery$d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firestore/v1/StructuredQuery$d;->c0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/firestore/remote/f$a;->h:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;->e0()Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->NOT_EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    :goto_0
    sget-object v1, La32;->b:Lcom/google/firestore/v1/Value;

    :goto_1
    invoke-static {v0, p1, v1}, Lcom/google/firebase/firestore/core/FieldFilter;->e(Ls00;Lcom/google/firebase/firestore/core/FieldFilter$Operator;Lcom/google/firestore/v1/Value;)Lcom/google/firebase/firestore/core/FieldFilter;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/google/firestore/v1/StructuredQuery$UnaryFilter;->e0()Lcom/google/firestore/v1/StructuredQuery$UnaryFilter$Operator;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Unrecognized UnaryFilter.operator %d"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->NOT_EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    :goto_2
    sget-object v1, La32;->a:Lcom/google/firestore/v1/Value;

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/google/firebase/firestore/core/FieldFilter$Operator;->EQUAL:Lcom/google/firebase/firestore/core/FieldFilter$Operator;

    goto :goto_2
.end method

.method public v(Lcom/google/protobuf/j0;)Lqo1;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/protobuf/j0;->e0()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protobuf/j0;->d0()I

    move-result v0

    if-nez v0, :cond_0

    sget-object p1, Lqo1;->b:Lqo1;

    return-object p1

    :cond_0
    new-instance v0, Lqo1;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->t(Lcom/google/protobuf/j0;)Lpw1;

    move-result-object p1

    invoke-direct {v0, p1}, Lqo1;-><init>(Lpw1;)V

    return-object v0
.end method

.method public w(Lcom/google/firestore/v1/ListenResponse;)Lqo1;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->f0()Lcom/google/firestore/v1/ListenResponse$ResponseTypeCase;

    move-result-object v0

    sget-object v1, Lcom/google/firestore/v1/ListenResponse$ResponseTypeCase;->TARGET_CHANGE:Lcom/google/firestore/v1/ListenResponse$ResponseTypeCase;

    if-eq v0, v1, :cond_0

    sget-object p1, Lqo1;->b:Lqo1;

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->g0()Lcom/google/firestore/v1/TargetChange;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firestore/v1/TargetChange;->f0()I

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lqo1;->b:Lqo1;

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->g0()Lcom/google/firestore/v1/TargetChange;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/TargetChange;->c0()Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object p1

    return-object p1
.end method

.method public x(Lcom/google/firestore/v1/ListenResponse;)Lcom/google/firebase/firestore/remote/WatchChange;
    .locals 8

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->m:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->f0()Lcom/google/firestore/v1/ListenResponse$ResponseTypeCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eq v0, v6, :cond_4

    if-eq v0, v5, :cond_3

    if-eq v0, v4, :cond_2

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->e0()Lcom/google/firestore/v1/j;

    move-result-object p1

    new-instance v0, Lhz;

    invoke-virtual {p1}, Lcom/google/firestore/v1/j;->a0()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/firestore/v1/j;->d0()Lcom/google/firestore/v1/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lhz;-><init>(ILcom/google/firestore/v1/c;)V

    invoke-virtual {p1}, Lcom/google/firestore/v1/j;->c0()I

    move-result p1

    new-instance v1, Lcom/google/firebase/firestore/remote/WatchChange$c;

    invoke-direct {v1, p1, v0}, Lcom/google/firebase/firestore/remote/WatchChange$c;-><init>(ILhz;)V

    goto/16 :goto_2

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Unknown change type set"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->d0()Lcom/google/firestore/v1/i;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/i;->c0()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firestore/v1/i;->b0()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object p1

    new-instance v2, Lcom/google/firebase/firestore/remote/WatchChange$b;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3, v0, p1, v1}, Lcom/google/firebase/firestore/remote/WatchChange$b;-><init>(Ljava/util/List;Ljava/util/List;Ldu;Lcom/google/firebase/firestore/model/MutableDocument;)V

    :goto_0
    move-object v1, v2

    goto/16 :goto_2

    :cond_2
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->c0()Lcom/google/firestore/v1/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/g;->d0()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firestore/v1/g;->b0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firestore/v1/g;->c0()Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->r(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    new-instance v1, Lcom/google/firebase/firestore/remote/WatchChange$b;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3, p1}, Lcom/google/firebase/firestore/remote/WatchChange$b;-><init>(Ljava/util/List;Ljava/util/List;Ldu;Lcom/google/firebase/firestore/model/MutableDocument;)V

    goto/16 :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->b0()Lcom/google/firestore/v1/f;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/f;->d0()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firestore/v1/f;->c0()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firestore/v1/f;->b0()Lcom/google/firestore/v1/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firestore/v1/e;->g0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/firestore/v1/f;->b0()Lcom/google/firestore/v1/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firestore/v1/e;->h0()Lcom/google/protobuf/j0;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object v3

    sget-object v4, Lqo1;->b:Lqo1;

    invoke-virtual {v3, v4}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v6

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "Got a document change without an update time"

    invoke-static {v4, v6, v5}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/firestore/v1/f;->b0()Lcom/google/firestore/v1/e;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firestore/v1/e;->e0()Ljava/util/Map;

    move-result-object p1

    invoke-static {p1}, La11;->i(Ljava/util/Map;)La11;

    move-result-object p1

    invoke-static {v2, v3, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->p(Ldu;Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    new-instance v2, Lcom/google/firebase/firestore/remote/WatchChange$b;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3, p1}, Lcom/google/firebase/firestore/remote/WatchChange$b;-><init>(Ljava/util/List;Ljava/util/List;Ldu;Lcom/google/firebase/firestore/model/MutableDocument;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/firestore/v1/ListenResponse;->g0()Lcom/google/firestore/v1/TargetChange;

    move-result-object p1

    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->l:[I

    invoke-virtual {p1}, Lcom/google/firestore/v1/TargetChange;->e0()Lcom/google/firestore/v1/TargetChange$TargetChangeType;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    aget v0, v0, v7

    if-eq v0, v6, :cond_9

    if-eq v0, v5, :cond_8

    if-eq v0, v4, :cond_7

    if-eq v0, v3, :cond_6

    if-ne v0, v2, :cond_5

    sget-object v0, Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;->Reset:Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    goto :goto_1

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Unknown target change type"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    sget-object v0, Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;->Current:Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    goto :goto_1

    :cond_7
    sget-object v0, Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;->Removed:Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    invoke-virtual {p1}, Lcom/google/firestore/v1/TargetChange;->a0()Lbq1;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/f;->X(Lbq1;)Lio/grpc/Status;

    move-result-object v1

    goto :goto_1

    :cond_8
    sget-object v0, Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;->Added:Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    goto :goto_1

    :cond_9
    sget-object v0, Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;->NoChange:Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    :goto_1
    new-instance v2, Lcom/google/firebase/firestore/remote/WatchChange$d;

    invoke-virtual {p1}, Lcom/google/firestore/v1/TargetChange;->g0()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/firestore/v1/TargetChange;->d0()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-direct {v2, v0, v3, p1, v1}, Lcom/google/firebase/firestore/remote/WatchChange$d;-><init>(Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;Ljava/util/List;Lcom/google/protobuf/ByteString;Lio/grpc/Status;)V

    goto/16 :goto_0

    :goto_2
    return-object v1
.end method

.method public y(Lcom/google/firebase/firestore/core/CompositeFilter;)Lcom/google/firestore/v1/StructuredQuery$Filter;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/CompositeFilter;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/CompositeFilter;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Li10;

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/f;->G(Li10;)Lcom/google/firestore/v1/StructuredQuery$Filter;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 p1, 0x0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    :goto_1
    check-cast p1, Lcom/google/firestore/v1/StructuredQuery$Filter;

    return-object p1

    :cond_1
    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter;->h0()Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$a;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/CompositeFilter;->e()Lcom/google/firebase/firestore/core/CompositeFilter$Operator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/f;->z(Lcom/google/firebase/firestore/core/CompositeFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$a;->B(Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$a;

    invoke-virtual {v1, v0}, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$a;->A(Ljava/lang/Iterable;)Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$a;

    invoke-static {}, Lcom/google/firestore/v1/StructuredQuery$Filter;->i0()Lcom/google/firestore/v1/StructuredQuery$Filter$a;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/google/firestore/v1/StructuredQuery$Filter$a;->A(Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$a;)Lcom/google/firestore/v1/StructuredQuery$Filter$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    goto :goto_1
.end method

.method public z(Lcom/google/firebase/firestore/core/CompositeFilter$Operator;)Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/f$a;->e:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;->OR:Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Unrecognized composite filter type."

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    sget-object p1, Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;->AND:Lcom/google/firestore/v1/StructuredQuery$CompositeFilter$Operator;

    return-object p1
.end method

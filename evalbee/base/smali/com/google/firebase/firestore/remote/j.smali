.class public Lcom/google/firebase/firestore/remote/j;
.super Lg0;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/remote/j$a;
    }
.end annotation


# static fields
.field public static final v:Lcom/google/protobuf/ByteString;


# instance fields
.field public final s:Lcom/google/firebase/firestore/remote/f;

.field public t:Z

.field public u:Lcom/google/protobuf/ByteString;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    sput-object v0, Lcom/google/firebase/firestore/remote/j;->v:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method public constructor <init>(Lw30;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/f;Lcom/google/firebase/firestore/remote/j$a;)V
    .locals 8

    .line 1
    invoke-static {}, Lh40;->b()Lio/grpc/MethodDescriptor;

    move-result-object v2

    sget-object v4, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->WRITE_STREAM_CONNECTION_BACKOFF:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    sget-object v5, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->WRITE_STREAM_IDLE:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    sget-object v6, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->HEALTH_CHECK_TIMEOUT:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lg0;-><init>(Lw30;Lio/grpc/MethodDescriptor;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcr1;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    sget-object p1, Lcom/google/firebase/firestore/remote/j;->v:Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/j;->u:Lcom/google/protobuf/ByteString;

    iput-object p3, p0, Lcom/google/firebase/firestore/remote/j;->s:Lcom/google/firebase/firestore/remote/f;

    return-void
.end method


# virtual methods
.method public A(Lcom/google/firestore/v1/m;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/google/firestore/v1/m;->c0()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/j;->u:Lcom/google/protobuf/ByteString;

    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    if-nez v0, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    iget-object p1, p0, Lg0;->m:Lcr1;

    check-cast p1, Lcom/google/firebase/firestore/remote/j$a;

    invoke-interface {p1}, Lcom/google/firebase/firestore/remote/j$a;->c()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/a;->f()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/j;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firestore/v1/m;->a0()Lcom/google/protobuf/j0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firestore/v1/m;->e0()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    invoke-virtual {p1, v3}, Lcom/google/firestore/v1/m;->d0(I)Lcom/google/firestore/v1/n;

    move-result-object v4

    iget-object v5, p0, Lcom/google/firebase/firestore/remote/j;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v5, v4, v0}, Lcom/google/firebase/firestore/remote/f;->m(Lcom/google/firestore/v1/n;Lqo1;)Lay0;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lg0;->m:Lcr1;

    check-cast p1, Lcom/google/firebase/firestore/remote/j$a;

    invoke-interface {p1, v0, v2}, Lcom/google/firebase/firestore/remote/j$a;->d(Lqo1;Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public B(Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/j;->u:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method public C()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/j;->m()Z

    move-result v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "Writing handshake requires an opened stream"

    invoke-static {v0, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    xor-int/lit8 v0, v0, 0x1

    const-string v2, "Handshake already completed"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/firestore/v1/l;->g0()Lcom/google/firestore/v1/l$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/j;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/remote/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/l$b;->B(Ljava/lang/String;)Lcom/google/firestore/v1/l$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/firestore/v1/l;

    invoke-virtual {p0, v0}, Lg0;->x(Ljava/lang/Object;)V

    return-void
.end method

.method public D(Ljava/util/List;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/j;->m()Z

    move-result v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "Writing mutations requires an opened stream"

    invoke-static {v0, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    const-string v2, "Handshake must be complete before writing mutations"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/firestore/v1/l;->g0()Lcom/google/firestore/v1/l$b;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwx0;

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/j;->s:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v2, v1}, Lcom/google/firebase/firestore/remote/f;->L(Lwx0;)Lcom/google/firestore/v1/Write;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/l$b;->A(Lcom/google/firestore/v1/Write;)Lcom/google/firestore/v1/l$b;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/firebase/firestore/remote/j;->u:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/l$b;->C(Lcom/google/protobuf/ByteString;)Lcom/google/firestore/v1/l$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/l;

    invoke-virtual {p0, p1}, Lg0;->x(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic l()V
    .locals 0

    .line 1
    invoke-super {p0}, Lg0;->l()V

    return-void
.end method

.method public bridge synthetic m()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lg0;->m()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic n()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lg0;->n()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic r(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/google/firestore/v1/m;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/j;->A(Lcom/google/firestore/v1/m;)V

    return-void
.end method

.method public u()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    invoke-super {p0}, Lg0;->u()V

    return-void
.end method

.method public bridge synthetic v()V
    .locals 0

    .line 1
    invoke-super {p0}, Lg0;->v()V

    return-void
.end method

.method public w()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/j;->D(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public y()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/j;->u:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public z()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/j;->t:Z

    return v0
.end method

.class public Lcom/google/firebase/firestore/remote/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final e:Ljava/util/Set;


# instance fields
.field public final a:Lrp;

.field public final b:Lcom/google/firebase/firestore/remote/f;

.field public final c:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final d:Lw30;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashSet;

    const-string v1, "x-google-service"

    const-string v2, "x-google-gfe-request-trace"

    const-string v3, "date"

    const-string v4, "x-google-backends"

    const-string v5, "x-google-netmon-label"

    filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/firebase/firestore/remote/d;->e:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lrp;Lcom/google/firebase/firestore/util/AsyncQueue;Leo;Leo;Landroid/content/Context;Lfc0;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/d;->a:Lrp;

    iput-object p2, p0, Lcom/google/firebase/firestore/remote/d;->c:Lcom/google/firebase/firestore/util/AsyncQueue;

    new-instance v0, Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lrp;->a()Lqp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/firebase/firestore/remote/f;-><init>(Lqp;)V

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/d;->b:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual/range {p0 .. p6}, Lcom/google/firebase/firestore/remote/d;->d(Lrp;Lcom/google/firebase/firestore/util/AsyncQueue;Leo;Leo;Landroid/content/Context;Lfc0;)Lw30;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/d;->d:Lw30;

    return-void
.end method

.method public static e(Lio/grpc/Status;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lio/grpc/Status;->getCode()Lio/grpc/Status$Code;

    invoke-virtual {p0}, Lio/grpc/Status;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    instance-of v0, p0, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    const-string v0, "no ciphers available"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static f(Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;)Z
    .locals 3

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/d$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown gRPC status code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 p0, 0x1

    return p0

    :pswitch_1
    const/4 p0, 0x0

    return p0

    :pswitch_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Treated status OK as error"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static g(Lio/grpc/Status;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lio/grpc/Status;->getCode()Lio/grpc/Status$Code;

    move-result-object p0

    invoke-virtual {p0}, Lio/grpc/Status$Code;->value()I

    move-result p0

    invoke-static {p0}, Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;->fromValue(I)Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;

    move-result-object p0

    invoke-static {p0}, Lcom/google/firebase/firestore/remote/d;->f(Lcom/google/firebase/firestore/FirebaseFirestoreException$Code;)Z

    move-result p0

    return p0
.end method

.method public static h(Lio/grpc/Status;)Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/remote/d;->g(Lio/grpc/Status;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lio/grpc/Status;->getCode()Lio/grpc/Status$Code;

    move-result-object p0

    sget-object v0, Lio/grpc/Status$Code;->ABORTED:Lio/grpc/Status$Code;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/remote/i$a;)Lcom/google/firebase/firestore/remote/i;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/remote/i;

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/d;->d:Lw30;

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/d;->c:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v3, p0, Lcom/google/firebase/firestore/remote/d;->b:Lcom/google/firebase/firestore/remote/f;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/firebase/firestore/remote/i;-><init>(Lw30;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/f;Lcom/google/firebase/firestore/remote/i$a;)V

    return-object v0
.end method

.method public b(Lcom/google/firebase/firestore/remote/j$a;)Lcom/google/firebase/firestore/remote/j;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/firebase/firestore/remote/j;

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/d;->d:Lw30;

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/d;->c:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v3, p0, Lcom/google/firebase/firestore/remote/d;->b:Lcom/google/firebase/firestore/remote/f;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/firebase/firestore/remote/j;-><init>(Lw30;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/f;Lcom/google/firebase/firestore/remote/j$a;)V

    return-object v0
.end method

.method public c()Lrp;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/d;->a:Lrp;

    return-object v0
.end method

.method public d(Lrp;Lcom/google/firebase/firestore/util/AsyncQueue;Leo;Leo;Landroid/content/Context;Lfc0;)Lw30;
    .locals 8

    .line 1
    new-instance v7, Lw30;

    move-object v0, v7

    move-object v1, p2

    move-object v2, p5

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lw30;-><init>(Lcom/google/firebase/firestore/util/AsyncQueue;Landroid/content/Context;Leo;Leo;Lrp;Lfc0;)V

    return-object v7
.end method

.class public final Lcom/google/firebase/firestore/remote/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/firestore/remote/WatchChangeAggregator$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/remote/g$c;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/firebase/firestore/remote/g$c;

.field public final b:Lcom/google/firebase/firestore/local/a;

.field public final c:Lcom/google/firebase/firestore/remote/d;

.field public final d:Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

.field public final e:Ljava/util/Map;

.field public final f:Lcom/google/firebase/firestore/remote/e;

.field public g:Z

.field public final h:Lcom/google/firebase/firestore/remote/i;

.field public final i:Lcom/google/firebase/firestore/remote/j;

.field public j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

.field public final k:Ljava/util/Deque;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/remote/g$c;Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/remote/d;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/ConnectivityMonitor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/firebase/firestore/remote/g;->g:Z

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/g;->a:Lcom/google/firebase/firestore/remote/g$c;

    iput-object p2, p0, Lcom/google/firebase/firestore/remote/g;->b:Lcom/google/firebase/firestore/local/a;

    iput-object p3, p0, Lcom/google/firebase/firestore/remote/g;->c:Lcom/google/firebase/firestore/remote/d;

    iput-object p5, p0, Lcom/google/firebase/firestore/remote/g;->d:Lcom/google/firebase/firestore/remote/ConnectivityMonitor;

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    new-instance p2, Ljava/util/ArrayDeque;

    invoke-direct {p2}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p2, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    new-instance p2, Lcom/google/firebase/firestore/remote/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lmd1;

    invoke-direct {v0, p1}, Lmd1;-><init>(Lcom/google/firebase/firestore/remote/g$c;)V

    invoke-direct {p2, p4, v0}, Lcom/google/firebase/firestore/remote/e;-><init>(Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/e$a;)V

    iput-object p2, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    new-instance p1, Lcom/google/firebase/firestore/remote/g$a;

    invoke-direct {p1, p0}, Lcom/google/firebase/firestore/remote/g$a;-><init>(Lcom/google/firebase/firestore/remote/g;)V

    invoke-virtual {p3, p1}, Lcom/google/firebase/firestore/remote/d;->a(Lcom/google/firebase/firestore/remote/i$a;)Lcom/google/firebase/firestore/remote/i;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    new-instance p1, Lcom/google/firebase/firestore/remote/g$b;

    invoke-direct {p1, p0}, Lcom/google/firebase/firestore/remote/g$b;-><init>(Lcom/google/firebase/firestore/remote/g;)V

    invoke-virtual {p3, p1}, Lcom/google/firebase/firestore/remote/d;->b(Lcom/google/firebase/firestore/remote/j$a;)Lcom/google/firebase/firestore/remote/j;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    new-instance p1, Lnd1;

    invoke-direct {p1, p0, p4}, Lnd1;-><init>(Lcom/google/firebase/firestore/remote/g;Lcom/google/firebase/firestore/util/AsyncQueue;)V

    invoke-interface {p5, p1}, Lcom/google/firebase/firestore/remote/ConnectivityMonitor;->a(Lcl;)V

    return-void
.end method

.method private synthetic C(Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;->REACHABLE:Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/e;->c()Lcom/google/firebase/firestore/core/OnlineState;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->ONLINE:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;->UNREACHABLE:Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/e;->c()Lcom/google/firebase/firestore/core/OnlineState;

    move-result-object p1

    sget-object v0, Lcom/google/firebase/firestore/core/OnlineState;->OFFLINE:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "RemoteStore"

    const-string v1, "Restarting streams for network reachability change."

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->H()V

    return-void
.end method

.method private synthetic D(Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V
    .locals 1

    .line 1
    new-instance v0, Lod1;

    invoke-direct {v0, p0, p2}, Lod1;-><init>(Lcom/google/firebase/firestore/remote/g;Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->i(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/firebase/firestore/remote/g;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/firebase/firestore/remote/g;->D(Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/firebase/firestore/remote/g;Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/firebase/firestore/remote/g;->C(Lcom/google/firebase/firestore/remote/ConnectivityMonitor$NetworkStatus;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/firebase/firestore/remote/g;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->w()V

    return-void
.end method

.method public static synthetic g(Lcom/google/firebase/firestore/remote/g;Lqo1;Lcom/google/firebase/firestore/remote/WatchChange;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/remote/g;->u(Lqo1;Lcom/google/firebase/firestore/remote/WatchChange;)V

    return-void
.end method

.method public static synthetic h(Lcom/google/firebase/firestore/remote/g;Lio/grpc/Status;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->v(Lio/grpc/Status;)V

    return-void
.end method

.method public static synthetic i(Lcom/google/firebase/firestore/remote/g;)Lcom/google/firebase/firestore/remote/j;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    return-object p0
.end method

.method public static synthetic j(Lcom/google/firebase/firestore/remote/g;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->A()V

    return-void
.end method

.method public static synthetic k(Lcom/google/firebase/firestore/remote/g;Lqo1;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/firestore/remote/g;->B(Lqo1;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic l(Lcom/google/firebase/firestore/remote/g;Lio/grpc/Status;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->z(Lio/grpc/Status;)V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->b:Lcom/google/firebase/firestore/local/a;

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/remote/j;->y()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/local/a;->Q(Lcom/google/protobuf/ByteString;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxx0;

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v1}, Lxx0;->h()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/firebase/firestore/remote/j;->D(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final B(Lqo1;Ljava/util/List;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxx0;

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/remote/j;->y()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, p1, p2, v1}, Lyx0;->a(Lxx0;Lqo1;Ljava/util/List;Lcom/google/protobuf/ByteString;)Lyx0;

    move-result-object p1

    iget-object p2, p0, Lcom/google/firebase/firestore/remote/g;->a:Lcom/google/firebase/firestore/remote/g$c;

    invoke-interface {p2, p1}, Lcom/google/firebase/firestore/remote/g$c;->e(Lyx0;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->s()V

    return-void
.end method

.method public E(Lau1;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lau1;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->N()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/i;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->J(Lau1;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final F(Lcom/google/firebase/firestore/remote/WatchChange$d;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/WatchChange$d;->a()Lio/grpc/Status;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "Processing target error without a cause"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/WatchChange$d;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->q(I)V

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/g;->a:Lcom/google/firebase/firestore/remote/g$c;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/WatchChange$d;->a()Lio/grpc/Status;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/google/firebase/firestore/remote/g$c;->c(ILio/grpc/Status;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final G(Lqo1;)V
    .locals 10

    .line 1
    sget-object v0, Lqo1;->b:Lqo1;

    invoke-virtual {p1, v0}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Can\'t raise event for unknown SnapshotVersion"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->c(Lqo1;)Ljd1;

    move-result-object v0

    invoke-virtual {v0}, Ljd1;->d()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lzt1;

    invoke-virtual {v3}, Lzt1;->e()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v4, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lau1;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3}, Lzt1;->e()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-virtual {v4, v3, p1}, Lau1;->k(Lcom/google/protobuf/ByteString;Lqo1;)Lau1;

    move-result-object v3

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljd1;->e()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v2, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lau1;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v6, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    invoke-virtual {v2}, Lau1;->f()Lqo1;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lau1;->k(Lcom/google/protobuf/ByteString;Lqo1;)Lau1;

    move-result-object v6

    invoke-interface {v3, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v5}, Lcom/google/firebase/firestore/remote/g;->I(I)V

    new-instance v9, Lau1;

    invoke-virtual {v2}, Lau1;->g()Lcom/google/firebase/firestore/core/q;

    move-result-object v4

    invoke-virtual {v2}, Lau1;->e()J

    move-result-wide v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/google/firebase/firestore/local/QueryPurpose;

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;)V

    invoke-virtual {p0, v9}, Lcom/google/firebase/firestore/remote/g;->J(Lau1;)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->a:Lcom/google/firebase/firestore/remote/g$c;

    invoke-interface {p1, v0}, Lcom/google/firebase/firestore/remote/g$c;->d(Ljd1;)V

    return-void
.end method

.method public final H()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/firebase/firestore/remote/g;->g:Z

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->q()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/e;->i(Lcom/google/firebase/firestore/core/OnlineState;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->l()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/i;->l()V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->r()V

    return-void
.end method

.method public final I(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->o(I)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/i;->z(I)V

    return-void
.end method

.method public final J(Lau1;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    invoke-virtual {p1}, Lau1;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->o(I)V

    invoke-virtual {p1}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v0

    sget-object v1, Lqo1;->b:Lqo1;

    invoke-virtual {v0, v1}, Lqo1;->a(Lqo1;)I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lau1;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/g;->b(I)Lcom/google/firebase/database/collection/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/database/collection/c;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lau1;->i(Ljava/lang/Integer;)Lau1;

    move-result-object p1

    :cond_1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/i;->A(Lau1;)V

    return-void
.end method

.method public final K()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/i;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final L()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public M()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->r()V

    return-void
.end method

.method public final N()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->K()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "startWatchStream() called when shouldStartWatchStream() is false."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    invoke-direct {v0, p0}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;-><init>(Lcom/google/firebase/firestore/remote/WatchChangeAggregator$b;)V

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/i;->u()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/e;->e()V

    return-void
.end method

.method public final O()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->L()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "startWriteStream() called when shouldStartWriteStream() is false."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->u()V

    return-void
.end method

.method public P(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lau1;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "stopListening called on target no currently watched: %d"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/i;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->I(I)V

    :cond_1
    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/remote/i;->m()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {p1}, Lg0;->q()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    sget-object v0, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/remote/e;->i(Lcom/google/firebase/firestore/core/OnlineState;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public a()Lqp;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->c:Lcom/google/firebase/firestore/remote/d;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/d;->c()Lrp;

    move-result-object v0

    invoke-virtual {v0}, Lrp;->a()Lqp;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/google/firebase/database/collection/c;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->a:Lcom/google/firebase/firestore/remote/g$c;

    invoke-interface {v0, p1}, Lcom/google/firebase/firestore/remote/g$c;->b(I)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    return-object p1
.end method

.method public c(I)Lau1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lau1;

    return-object p1
.end method

.method public final m(Lxx0;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->n()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "addToWritePipeline called when pipeline is full"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {p1}, Lxx0;->h()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/j;->D(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final n()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/firebase/firestore/remote/g;->g:Z

    return v0
.end method

.method public final p()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    return-void
.end method

.method public final q()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/i;->v()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->v()V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "RemoteStore"

    const-string v2, "Stopping write stream with %d pending writes"

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->p()V

    return-void
.end method

.method public r()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/firebase/firestore/remote/g;->g:Z

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->b:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/local/a;->u()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/j;->B(Lcom/google/protobuf/ByteString;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->N()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/e;->i(Lcom/google/firebase/firestore/core/OnlineState;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->s()V

    :cond_1
    return-void
.end method

.method public s()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxx0;

    goto :goto_1

    :goto_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->b:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {v1, v0}, Lcom/google/firebase/firestore/local/a;->w(I)Lxx0;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lg0;->q()V

    goto :goto_2

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/firebase/firestore/remote/g;->m(Lxx0;)V

    :goto_1
    invoke-virtual {v0}, Lxx0;->e()I

    move-result v0

    goto :goto_0

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->L()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->O()V

    :cond_3
    return-void
.end method

.method public t()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "RemoteStore"

    const-string v2, "Restarting streams for new credential."

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->H()V

    :cond_0
    return-void
.end method

.method public final u(Lqo1;Lcom/google/firebase/firestore/remote/WatchChange;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    sget-object v1, Lcom/google/firebase/firestore/core/OnlineState;->ONLINE:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/e;->i(Lcom/google/firebase/firestore/core/OnlineState;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->h:Lcom/google/firebase/firestore/remote/i;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "WatchStream and WatchStreamAggregator should both be non-null"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    instance-of v0, p2, Lcom/google/firebase/firestore/remote/WatchChange$d;

    if-eqz v0, :cond_1

    move-object v2, p2

    check-cast v2, Lcom/google/firebase/firestore/remote/WatchChange$d;

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/firebase/firestore/remote/WatchChange$d;->b()Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    move-result-object v3

    sget-object v4, Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;->Removed:Lcom/google/firebase/firestore/remote/WatchChange$WatchTargetChangeType;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/google/firebase/firestore/remote/WatchChange$d;->a()Lio/grpc/Status;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/firebase/firestore/remote/g;->F(Lcom/google/firebase/firestore/remote/WatchChange$d;)V

    goto :goto_3

    :cond_2
    instance-of v2, p2, Lcom/google/firebase/firestore/remote/WatchChange$b;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    check-cast p2, Lcom/google/firebase/firestore/remote/WatchChange$b;

    invoke-virtual {v0, p2}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->i(Lcom/google/firebase/firestore/remote/WatchChange$b;)V

    goto :goto_2

    :cond_3
    instance-of v2, p2, Lcom/google/firebase/firestore/remote/WatchChange$c;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    check-cast p2, Lcom/google/firebase/firestore/remote/WatchChange$c;

    invoke-virtual {v0, p2}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->j(Lcom/google/firebase/firestore/remote/WatchChange$c;)V

    goto :goto_2

    :cond_4
    const-string v2, "Expected watchChange to be an instance of WatchTargetChange"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->j:Lcom/google/firebase/firestore/remote/WatchChangeAggregator;

    check-cast p2, Lcom/google/firebase/firestore/remote/WatchChange$d;

    invoke-virtual {v0, p2}, Lcom/google/firebase/firestore/remote/WatchChangeAggregator;->k(Lcom/google/firebase/firestore/remote/WatchChange$d;)V

    :goto_2
    sget-object p2, Lqo1;->b:Lqo1;

    invoke-virtual {p1, p2}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_5

    iget-object p2, p0, Lcom/google/firebase/firestore/remote/g;->b:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p2}, Lcom/google/firebase/firestore/local/a;->t()Lqo1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lqo1;->a(Lqo1;)I

    move-result p2

    if-ltz p2, :cond_5

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->G(Lqo1;)V

    :cond_5
    :goto_3
    return-void
.end method

.method public final v(Lio/grpc/Status;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lio/grpc/Status;->isOk()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->K()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Watch stream was stopped gracefully while still needed."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->p()V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/e;->d(Lio/grpc/Status;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->N()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->f:Lcom/google/firebase/firestore/remote/e;

    sget-object v0, Lcom/google/firebase/firestore/core/OnlineState;->UNKNOWN:Lcom/google/firebase/firestore/core/OnlineState;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/remote/e;->i(Lcom/google/firebase/firestore/core/OnlineState;)V

    :goto_0
    return-void
.end method

.method public final w()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lau1;

    invoke-virtual {p0, v1}, Lcom/google/firebase/firestore/remote/g;->J(Lau1;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final x(Lio/grpc/Status;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lio/grpc/Status;->isOk()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Handling write error with status OK."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/firebase/firestore/remote/d;->h(Lio/grpc/Status;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxx0;

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/remote/j;->l()V

    iget-object v1, p0, Lcom/google/firebase/firestore/remote/g;->a:Lcom/google/firebase/firestore/remote/g$c;

    invoke-virtual {v0}, Lxx0;->e()I

    move-result v0

    invoke-interface {v1, v0, p1}, Lcom/google/firebase/firestore/remote/g$c;->f(ILio/grpc/Status;)V

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->s()V

    :cond_0
    return-void
.end method

.method public final y(Lio/grpc/Status;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lio/grpc/Status;->isOk()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Handling write error with status OK."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/firebase/firestore/remote/d;->g(Lio/grpc/Status;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->y()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v0}, Lo22;->y(Lcom/google/protobuf/ByteString;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "RemoteStore error before completed handshake; resetting stream token %s: %s"

    filled-new-array {v0, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "RemoteStore"

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    sget-object v0, Lcom/google/firebase/firestore/remote/j;->v:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/remote/j;->B(Lcom/google/protobuf/ByteString;)V

    iget-object p1, p0, Lcom/google/firebase/firestore/remote/g;->b:Lcom/google/firebase/firestore/local/a;

    invoke-virtual {p1, v0}, Lcom/google/firebase/firestore/local/a;->Q(Lcom/google/protobuf/ByteString;)V

    :cond_0
    return-void
.end method

.method public final z(Lio/grpc/Status;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lio/grpc/Status;->isOk()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->L()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Write stream was stopped gracefully while still needed."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lio/grpc/Status;->isOk()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g;->i:Lcom/google/firebase/firestore/remote/j;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->x(Lio/grpc/Status;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/remote/g;->y(Lio/grpc/Status;)V

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->L()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/google/firebase/firestore/remote/g;->O()V

    :cond_3
    return-void
.end method

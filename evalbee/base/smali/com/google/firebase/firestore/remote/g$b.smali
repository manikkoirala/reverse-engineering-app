.class public Lcom/google/firebase/firestore/remote/g$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/firestore/remote/j$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/firebase/firestore/remote/g;-><init>(Lcom/google/firebase/firestore/remote/g$c;Lcom/google/firebase/firestore/local/a;Lcom/google/firebase/firestore/remote/d;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/remote/ConnectivityMonitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/firebase/firestore/remote/g;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/remote/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/firebase/firestore/remote/g$b;->a:Lcom/google/firebase/firestore/remote/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lio/grpc/Status;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g$b;->a:Lcom/google/firebase/firestore/remote/g;

    invoke-static {v0, p1}, Lcom/google/firebase/firestore/remote/g;->l(Lcom/google/firebase/firestore/remote/g;Lio/grpc/Status;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g$b;->a:Lcom/google/firebase/firestore/remote/g;

    invoke-static {v0}, Lcom/google/firebase/firestore/remote/g;->i(Lcom/google/firebase/firestore/remote/g;)Lcom/google/firebase/firestore/remote/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/remote/j;->C()V

    return-void
.end method

.method public c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g$b;->a:Lcom/google/firebase/firestore/remote/g;

    invoke-static {v0}, Lcom/google/firebase/firestore/remote/g;->j(Lcom/google/firebase/firestore/remote/g;)V

    return-void
.end method

.method public d(Lqo1;Ljava/util/List;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/remote/g$b;->a:Lcom/google/firebase/firestore/remote/g;

    invoke-static {v0, p1, p2}, Lcom/google/firebase/firestore/remote/g;->k(Lcom/google/firebase/firestore/remote/g;Lqo1;Ljava/util/List;)V

    return-void
.end method

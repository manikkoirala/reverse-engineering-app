.class public Lcom/google/firebase/firestore/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly10;
.implements Lcom/google/firebase/firestore/FirebaseFirestore$a;


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Lr10;

.field public final c:Landroid/content/Context;

.field public final d:Ljr;

.field public final e:Ljr;

.field public final f:Lfc0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lr10;Ljr;Ljr;Lfc0;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/firebase/firestore/e;->a:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/firebase/firestore/e;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/firebase/firestore/e;->b:Lr10;

    iput-object p3, p0, Lcom/google/firebase/firestore/e;->d:Ljr;

    iput-object p4, p0, Lcom/google/firebase/firestore/e;->e:Ljr;

    iput-object p5, p0, Lcom/google/firebase/firestore/e;->f:Lfc0;

    invoke-virtual {p2, p0}, Lr10;->h(Ly10;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/google/firebase/firestore/FirebaseFirestore;
    .locals 8

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/firebase/firestore/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/FirebaseFirestore;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/firebase/firestore/e;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/firebase/firestore/e;->b:Lr10;

    iget-object v3, p0, Lcom/google/firebase/firestore/e;->d:Ljr;

    iget-object v4, p0, Lcom/google/firebase/firestore/e;->e:Ljr;

    iget-object v7, p0, Lcom/google/firebase/firestore/e;->f:Lfc0;

    move-object v5, p1

    move-object v6, p0

    invoke-static/range {v1 .. v7}, Lcom/google/firebase/firestore/FirebaseFirestore;->i(Landroid/content/Context;Lr10;Ljr;Ljr;Ljava/lang/String;Lcom/google/firebase/firestore/FirebaseFirestore$a;Lfc0;)Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/firestore/e;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

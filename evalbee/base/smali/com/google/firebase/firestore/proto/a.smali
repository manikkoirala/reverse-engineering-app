.class public final Lcom/google/firebase/firestore/proto/a;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lxv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/proto/a$b;
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

.field public static final NAME_FIELD_NUMBER:I = 0x1

.field private static volatile PARSER:Lb31; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb31;"
        }
    .end annotation
.end field

.field public static final READ_TIME_FIELD_NUMBER:I = 0x2


# instance fields
.field private name_:Ljava/lang/String;

.field private readTime_:Lcom/google/protobuf/j0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/firebase/firestore/proto/a;

    invoke-direct {v0}, Lcom/google/firebase/firestore/proto/a;-><init>()V

    sput-object v0, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    const-class v1, Lcom/google/firebase/firestore/proto/a;

    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->V(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/firebase/firestore/proto/a;->name_:Ljava/lang/String;

    return-void
.end method

.method public static synthetic Z()Lcom/google/firebase/firestore/proto/a;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    return-object v0
.end method

.method public static synthetic a0(Lcom/google/firebase/firestore/proto/a;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/proto/a;->g0(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b0(Lcom/google/firebase/firestore/proto/a;Lcom/google/protobuf/j0;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/proto/a;->h0(Lcom/google/protobuf/j0;)V

    return-void
.end method

.method public static c0()Lcom/google/firebase/firestore/proto/a;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    return-object v0
.end method

.method public static f0()Lcom/google/firebase/firestore/proto/a$b;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->u()Lcom/google/protobuf/GeneratedMessageLite$a;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/proto/a$b;

    return-object v0
.end method


# virtual methods
.method public d0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/proto/a;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public e0()Lcom/google/protobuf/j0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/proto/a;->readTime_:Lcom/google/protobuf/j0;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/protobuf/j0;->c0()Lcom/google/protobuf/j0;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final g0(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lcom/google/firebase/firestore/proto/a;->name_:Ljava/lang/String;

    return-void
.end method

.method public final h0(Lcom/google/protobuf/j0;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lcom/google/firebase/firestore/proto/a;->readTime_:Lcom/google/protobuf/j0;

    return-void
.end method

.method public final y(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    sget-object p2, Lcom/google/firebase/firestore/proto/a$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/firebase/firestore/proto/a;->PARSER:Lb31;

    if-nez p1, :cond_1

    const-class p2, Lcom/google/firebase/firestore/proto/a;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lcom/google/firebase/firestore/proto/a;->PARSER:Lb31;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$b;

    sget-object p3, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$b;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    sput-object p1, Lcom/google/firebase/firestore/proto/a;->PARSER:Lb31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    return-object p1

    :pswitch_4
    const-string p1, "name_"

    const-string p2, "readTime_"

    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0208\u0002\t"

    sget-object p3, Lcom/google/firebase/firestore/proto/a;->DEFAULT_INSTANCE:Lcom/google/firebase/firestore/proto/a;

    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->N(Lcom/google/protobuf/a0;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Lcom/google/firebase/firestore/proto/a$b;

    invoke-direct {p1, p2}, Lcom/google/firebase/firestore/proto/a$b;-><init>(Lcom/google/firebase/firestore/proto/a$a;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lcom/google/firebase/firestore/proto/a;

    invoke-direct {p1}, Lcom/google/firebase/firestore/proto/a;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public Lcom/google/firebase/firestore/FirestoreRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-fst"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lgj;)Lcom/google/firebase/firestore/e;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/firestore/FirestoreRegistrar;->lambda$getComponents$0(Lgj;)Lcom/google/firebase/firestore/e;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getComponents$0(Lgj;)Lcom/google/firebase/firestore/e;
    .locals 9

    .line 1
    new-instance v6, Lcom/google/firebase/firestore/e;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    const-class v0, Lr10;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lr10;

    const-class v0, Lzf0;

    invoke-interface {p0, v0}, Lgj;->i(Ljava/lang/Class;)Ljr;

    move-result-object v3

    const-class v0, Ldg0;

    invoke-interface {p0, v0}, Lgj;->i(Ljava/lang/Class;)Ljr;

    move-result-object v4

    new-instance v5, Le20;

    const-class v0, Lv12;

    invoke-interface {p0, v0}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v0

    const-class v7, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo;

    invoke-interface {p0, v7}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v7

    const-class v8, Le30;

    invoke-interface {p0, v8}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Le30;

    invoke-direct {v5, v0, v7, p0}, Le20;-><init>(Lr91;Lr91;Le30;)V

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/firebase/firestore/e;-><init>(Landroid/content/Context;Lr10;Ljr;Ljr;Lfc0;)V

    return-object v6
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 3
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    const-class v0, Lcom/google/firebase/firestore/e;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v1, "fire-fst"

    invoke-virtual {v0, v1}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    const-class v2, Lr10;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Landroid/content/Context;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo;

    invoke-static {v2}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lv12;

    invoke-static {v2}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lzf0;

    invoke-static {v2}, Los;->a(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Ldg0;

    invoke-static {v2}, Los;->a(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Le30;

    invoke-static {v2}, Los;->h(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v2, Lr40;

    invoke-direct {v2}, Lr40;-><init>()V

    invoke-virtual {v0, v2}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    const-string v2, "24.10.0"

    invoke-static {v1, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v1

    filled-new-array {v0, v1}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/google/firebase/firestore/FirebaseFirestore;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/firestore/FirebaseFirestore$a;
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lqp;

.field public final c:Ljava/lang/String;

.field public final d:Leo;

.field public final e:Leo;

.field public final f:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final g:Lr10;

.field public final h:Lb22;

.field public final i:Lcom/google/firebase/firestore/FirebaseFirestore$a;

.field public j:Lcom/google/firebase/firestore/b;

.field public volatile k:Lcom/google/firebase/firestore/core/g;

.field public final l:Lfc0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lqp;Ljava/lang/String;Leo;Leo;Lcom/google/firebase/firestore/util/AsyncQueue;Lr10;Lcom/google/firebase/firestore/FirebaseFirestore$a;Lfc0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->a:Landroid/content/Context;

    invoke-static {p2}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lqp;

    invoke-static {p1}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lqp;

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->b:Lqp;

    new-instance p1, Lb22;

    invoke-direct {p1, p2}, Lb22;-><init>(Lqp;)V

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->h:Lb22;

    invoke-static {p3}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->c:Ljava/lang/String;

    invoke-static {p4}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Leo;

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->d:Leo;

    invoke-static {p5}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Leo;

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->e:Leo;

    invoke-static {p6}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p7, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->g:Lr10;

    iput-object p8, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->i:Lcom/google/firebase/firestore/FirebaseFirestore$a;

    iput-object p9, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->l:Lfc0;

    new-instance p1, Lcom/google/firebase/firestore/b$b;

    invoke-direct {p1}, Lcom/google/firebase/firestore/b$b;-><init>()V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/b$b;->f()Lcom/google/firebase/firestore/b;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->j:Lcom/google/firebase/firestore/b;

    return-void
.end method

.method public static e()Lr10;
    .locals 2

    .line 1
    invoke-static {}, Lr10;->m()Lr10;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You must call FirebaseApp.initializeApp first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static f()Lcom/google/firebase/firestore/FirebaseFirestore;
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/FirebaseFirestore;->e()Lr10;

    move-result-object v0

    const-string v1, "(default)"

    invoke-static {v0, v1}, Lcom/google/firebase/firestore/FirebaseFirestore;->g(Lr10;Ljava/lang/String;)Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lr10;Ljava/lang/String;)Lcom/google/firebase/firestore/FirebaseFirestore;
    .locals 1

    .line 1
    const-string v0, "Provided FirebaseApp must not be null."

    invoke-static {p0, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Provided database name must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-class v0, Lcom/google/firebase/firestore/e;

    invoke-virtual {p0, v0}, Lr10;->j(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/firebase/firestore/e;

    const-string v0, "Firestore component is not present."

    invoke-static {p0, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/firebase/firestore/e;->a(Ljava/lang/String;)Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object p0

    return-object p0
.end method

.method public static i(Landroid/content/Context;Lr10;Ljr;Ljr;Ljava/lang/String;Lcom/google/firebase/firestore/FirebaseFirestore$a;Lfc0;)Lcom/google/firebase/firestore/FirebaseFirestore;
    .locals 11

    .line 1
    invoke-virtual {p1}, Lr10;->p()Le30;

    move-result-object v0

    invoke-virtual {v0}, Le30;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p4

    invoke-static {v0, p4}, Lqp;->c(Ljava/lang/String;Ljava/lang/String;)Lqp;

    move-result-object v3

    new-instance v7, Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-direct {v7}, Lcom/google/firebase/firestore/util/AsyncQueue;-><init>()V

    new-instance v5, Lc20;

    move-object v0, p2

    invoke-direct {v5, p2}, Lc20;-><init>(Ljr;)V

    new-instance v6, Lx10;

    move-object v0, p3

    invoke-direct {v6, p3}, Lx10;-><init>(Ljr;)V

    invoke-virtual {p1}, Lr10;->o()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/firebase/firestore/FirebaseFirestore;

    move-object v1, v0

    move-object v2, p0

    move-object v8, p1

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcom/google/firebase/firestore/FirebaseFirestore;-><init>(Landroid/content/Context;Lqp;Ljava/lang/String;Leo;Leo;Lcom/google/firebase/firestore/util/AsyncQueue;Lr10;Lcom/google/firebase/firestore/FirebaseFirestore$a;Lfc0;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FirebaseOptions.getProjectId() cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static setClientLanguage(Ljava/lang/String;)V
    .locals 0
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    invoke-static {p0}, Lw30;->h(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lih;
    .locals 1

    .line 1
    const-string v0, "Provided collection path must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/FirebaseFirestore;->b()V

    new-instance v0, Lih;

    invoke-static {p1}, Lke1;->q(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-direct {v0, p1, p0}, Lih;-><init>(Lke1;Lcom/google/firebase/firestore/FirebaseFirestore;)V

    return-object v0
.end method

.method public final b()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->k:Lcom/google/firebase/firestore/core/g;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->b:Lqp;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->k:Lcom/google/firebase/firestore/core/g;

    if-eqz v1, :cond_1

    monitor-exit v0

    return-void

    :cond_1
    new-instance v4, Lrp;

    iget-object v1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->b:Lqp;

    iget-object v2, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->j:Lcom/google/firebase/firestore/b;

    invoke-virtual {v3}, Lcom/google/firebase/firestore/b;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->j:Lcom/google/firebase/firestore/b;

    invoke-virtual {v5}, Lcom/google/firebase/firestore/b;->e()Z

    move-result v5

    invoke-direct {v4, v1, v2, v3, v5}, Lrp;-><init>(Lqp;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v1, Lcom/google/firebase/firestore/core/g;

    iget-object v3, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->a:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->j:Lcom/google/firebase/firestore/b;

    iget-object v6, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->d:Leo;

    iget-object v7, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->e:Leo;

    iget-object v8, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v9, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->l:Lfc0;

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/google/firebase/firestore/core/g;-><init>(Landroid/content/Context;Lrp;Lcom/google/firebase/firestore/b;Leo;Leo;Lcom/google/firebase/firestore/util/AsyncQueue;Lfc0;)V

    iput-object v1, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->k:Lcom/google/firebase/firestore/core/g;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public c()Lcom/google/firebase/firestore/core/g;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->k:Lcom/google/firebase/firestore/core/g;

    return-object v0
.end method

.method public d()Lqp;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->b:Lqp;

    return-object v0
.end method

.method public h()Lb22;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firebase/firestore/FirebaseFirestore;->h:Lb22;

    return-object v0
.end method

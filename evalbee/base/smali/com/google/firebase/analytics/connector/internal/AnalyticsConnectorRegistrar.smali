.class public Lcom/google/firebase/analytics/connector/internal/AnalyticsConnectorRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation build Lcom/google/android/gms/common/annotation/KeepForSdk;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic lambda$getComponents$0(Lgj;)Lg4;
    .locals 3

    .line 1
    const-class v0, Lr10;

    invoke-interface {p0, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lr10;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, Lbs1;

    invoke-interface {p0, v2}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lbs1;

    invoke-static {v0, v1, p0}, Lh4;->d(Lr10;Landroid/content/Context;Lbs1;)Lg4;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 3
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation build Lcom/google/android/gms/common/annotation/KeepForSdk;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    const-class v0, Lg4;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-class v1, Lr10;

    invoke-static {v1}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v1, Landroid/content/Context;

    invoke-static {v1}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v1, Lbs1;

    invoke-static {v1}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    sget-object v1, Lge2;->a:Lge2;

    invoke-virtual {v0, v1}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->e()Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    const-string v1, "fire-analytics"

    const-string v2, "21.5.0"

    invoke-static {v1, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v1

    filled-new-array {v0, v1}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class final Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SourceFile"

# interfaces
.implements La90;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "La90;"
    }
.end annotation


# instance fields
.field final synthetic $completionListener:Lcom/google/android/gms/tasks/OnCompleteListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/OnCompleteListener<",
            "Lzq1.b;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic $pauseListener:Ln11;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln11;"
        }
    .end annotation
.end field

.field final synthetic $progressListener:Lo11;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lo11;"
        }
    .end annotation
.end field

.field final synthetic $this_taskState:Lzq1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lzq1;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lzq1;Lo11;Ln11;Lcom/google/android/gms/tasks/OnCompleteListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lzq1;",
            "Lo11;",
            "Ln11;",
            "Lcom/google/android/gms/tasks/OnCompleteListener<",
            "Lzq1.b;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$this_taskState:Lzq1;

    iput-object p2, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$progressListener:Lo11;

    iput-object p3, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$pauseListener:Ln11;

    iput-object p4, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$completionListener:Lcom/google/android/gms/tasks/OnCompleteListener;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->invoke()V

    sget-object v0, Lu02;->a:Lu02;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$this_taskState:Lzq1;

    iget-object v1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$progressListener:Lo11;

    invoke-virtual {v0, v1}, Lzq1;->a0(Lo11;)Lzq1;

    iget-object v0, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$this_taskState:Lzq1;

    iget-object v1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$pauseListener:Ln11;

    invoke-virtual {v0, v1}, Lzq1;->Z(Ln11;)Lzq1;

    iget-object v0, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$this_taskState:Lzq1;

    iget-object v1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;->$completionListener:Lcom/google/android/gms/tasks/OnCompleteListener;

    invoke-virtual {v0, v1}, Lzq1;->Y(Lcom/google/android/gms/tasks/OnCompleteListener;)Lzq1;

    return-void
.end method

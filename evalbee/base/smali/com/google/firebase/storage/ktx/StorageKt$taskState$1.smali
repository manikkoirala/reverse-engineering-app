.class final Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "SourceFile"

# interfaces
.implements Lq90;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lq90;"
    }
.end annotation

.annotation runtime Lxp;
    c = "com.google.firebase.storage.ktx.StorageKt$taskState$1"
    f = "Storage.kt"
    l = {
        0x15e
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field final synthetic $this_taskState:Lzq1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lzq1;"
        }
    .end annotation
.end field

.field private synthetic L$0:Ljava/lang/Object;

.field label:I


# direct methods
.method public constructor <init>(Lzq1;Lvl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lzq1;",
            "Lvl;",
            ")V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->$this_taskState:Lzq1;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILvl;)V

    return-void
.end method

.method public static synthetic a(Ls81;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend$lambda-4(Ls81;Lcom/google/android/gms/tasks/Task;)V

    return-void
.end method

.method public static synthetic e(Ls81;Lzq1$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend$lambda-1(Ls81;Lzq1$b;)V

    return-void
.end method

.method public static synthetic f(Ls81;Lzq1$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend$lambda-3(Ls81;Lzq1$b;)V

    return-void
.end method

.method public static synthetic h(Ls81;Lzq1$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend$lambda-1$lambda-0(Ls81;Lzq1$b;)V

    return-void
.end method

.method public static synthetic i(Ls81;Lzq1$b;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend$lambda-3$lambda-2(Ls81;Lzq1$b;)V

    return-void
.end method

.method private static final invokeSuspend$lambda-1(Ls81;Lzq1$b;)V
    .locals 2

    .line 1
    invoke-static {}, Lbr1;->a()Lbr1;

    move-result-object v0

    new-instance v1, Lcom/google/firebase/storage/ktx/e;

    invoke-direct {v1, p0, p1}, Lcom/google/firebase/storage/ktx/e;-><init>(Ls81;Lzq1$b;)V

    invoke-virtual {v0, v1}, Lbr1;->d(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final invokeSuspend$lambda-1$lambda-0(Ls81;Lzq1$b;)V
    .locals 1

    .line 1
    new-instance v0, Lqu1$a;

    invoke-direct {v0, p1}, Lqu1$a;-><init>(Ljava/lang/Object;)V

    invoke-static {p0, v0}, Leg;->w(Lzk1;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static final invokeSuspend$lambda-3(Ls81;Lzq1$b;)V
    .locals 2

    .line 1
    invoke-static {}, Lbr1;->a()Lbr1;

    move-result-object v0

    new-instance v1, Lcom/google/firebase/storage/ktx/a;

    invoke-direct {v1, p0, p1}, Lcom/google/firebase/storage/ktx/a;-><init>(Ls81;Lzq1$b;)V

    invoke-virtual {v0, v1}, Lbr1;->d(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final invokeSuspend$lambda-3$lambda-2(Ls81;Lzq1$b;)V
    .locals 1

    .line 1
    new-instance v0, Lqu1$b;

    invoke-direct {v0, p1}, Lqu1$b;-><init>(Ljava/lang/Object;)V

    invoke-static {p0, v0}, Leg;->w(Lzk1;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static final invokeSuspend$lambda-4(Ls81;Lcom/google/android/gms/tasks/Task;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, v0}, Lzk1$a;->a(Lzk1;Ljava/lang/Throwable;ILjava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->getException()Ljava/lang/Exception;

    move-result-object p1

    const-string v0, "Error getting the TaskState"

    invoke-static {p0, v0, p1}, Lkotlinx/coroutines/f;->c(Llm;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lvl;)Lvl;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lvl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lvl;",
            ")",
            "Lvl;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;

    iget-object v1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->$this_taskState:Lzq1;

    invoke-direct {v0, v1, p2}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;-><init>(Lzq1;Lvl;)V

    iput-object p1, v0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->L$0:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2
    check-cast p1, Ls81;

    check-cast p2, Lvl;

    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invoke(Ls81;Lvl;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ls81;Lvl;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ls81;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lvl;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ls81;",
            "Lvl;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->create(Ljava/lang/Object;Lvl;)Lvl;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;

    sget-object p2, Lu02;->a:Lu02;

    invoke-virtual {p1, p2}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    invoke-static {}, Lgg0;->d()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, Lxe1;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lxe1;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->L$0:Ljava/lang/Object;

    check-cast p1, Ls81;

    new-instance v1, Lcom/google/firebase/storage/ktx/b;

    invoke-direct {v1, p1}, Lcom/google/firebase/storage/ktx/b;-><init>(Ls81;)V

    new-instance v3, Lcom/google/firebase/storage/ktx/c;

    invoke-direct {v3, p1}, Lcom/google/firebase/storage/ktx/c;-><init>(Ls81;)V

    new-instance v4, Lcom/google/firebase/storage/ktx/d;

    invoke-direct {v4, p1}, Lcom/google/firebase/storage/ktx/d;-><init>(Ls81;)V

    iget-object v5, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->$this_taskState:Lzq1;

    invoke-virtual {v5, v1}, Lzq1;->s(Lo11;)Lzq1;

    iget-object v5, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->$this_taskState:Lzq1;

    invoke-virtual {v5, v3}, Lzq1;->r(Ln11;)Lzq1;

    iget-object v5, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->$this_taskState:Lzq1;

    invoke-virtual {v5, v4}, Lzq1;->m(Lcom/google/android/gms/tasks/OnCompleteListener;)Lzq1;

    new-instance v5, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;

    iget-object v6, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->$this_taskState:Lzq1;

    invoke-direct {v5, v6, v1, v3, v4}, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1$1;-><init>(Lzq1;Lo11;Ln11;Lcom/google/android/gms/tasks/OnCompleteListener;)V

    iput v2, p0, Lcom/google/firebase/storage/ktx/StorageKt$taskState$1;->label:I

    invoke-static {p1, v5, p0}, Lkotlinx/coroutines/channels/ProduceKt;->a(Ls81;La90;Lvl;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    sget-object p1, Lu02;->a:Lu02;

    return-object p1
.end method

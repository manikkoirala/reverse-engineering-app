.class public final Lcom/google/firebase/dynamiclinks/internal/FirebaseDynamicLinkRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation build Lcom/google/android/gms/common/annotation/KeepForSdk;
.end annotation


# static fields
.field private static final LIBRARY_NAME:Ljava/lang/String; = "fire-dl"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lgj;)Ll20;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/firebase/dynamiclinks/internal/FirebaseDynamicLinkRegistrar;->lambda$getComponents$0(Lgj;)Ll20;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getComponents$0(Lgj;)Ll20;
    .locals 3

    .line 1
    new-instance v0, Lm20;

    const-class v1, Lr10;

    invoke-interface {p0, v1}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lr10;

    const-class v2, Lg4;

    invoke-interface {p0, v2}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lm20;-><init>(Lr10;Lr91;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 3
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    const-class v0, Ll20;

    invoke-static {v0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-string v1, "fire-dl"

    invoke-virtual {v0, v1}, Lzi$b;->h(Ljava/lang/String;)Lzi$b;

    move-result-object v0

    const-class v2, Lr10;

    invoke-static {v2}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v2, Lg4;

    invoke-static {v2}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v2, Lk20;

    invoke-direct {v2}, Lk20;-><init>()V

    invoke-virtual {v0, v2}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    const-string v2, "21.2.0"

    invoke-static {v1, v2}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v1

    filled-new-array {v0, v1}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

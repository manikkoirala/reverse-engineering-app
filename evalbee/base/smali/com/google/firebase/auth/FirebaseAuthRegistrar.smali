.class public Lcom/google/firebase/auth/FirebaseAuthRegistrar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/firebase/components/ComponentRegistrar;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation build Lcom/google/android/gms/common/annotation/KeepForSdk;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic lambda$getComponents$0(Lda1;Lda1;Lda1;Lda1;Lda1;Lgj;)Lcom/google/firebase/auth/FirebaseAuth;
    .locals 10

    .line 1
    const-class v0, Lr10;

    invoke-interface {p5, v0}, Lgj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lr10;

    const-class v0, Ldg0;

    invoke-interface {p5, v0}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v3

    const-class v0, Lwc0;

    invoke-interface {p5, v0}, Lgj;->e(Ljava/lang/Class;)Lr91;

    move-result-object v4

    new-instance v0, Lhk2;

    invoke-interface {p5, p0}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v5, p0

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-interface {p5, p1}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v6, p0

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-interface {p5, p2}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v7, p0

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-interface {p5, p3}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v8, p0

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p5, p4}, Lgj;->d(Lda1;)Ljava/lang/Object;

    move-result-object p0

    move-object v9, p0

    check-cast v9, Ljava/util/concurrent/Executor;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lhk2;-><init>(Lr10;Lr91;Lr91;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 8
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lzi;",
            ">;"
        }
    .end annotation

    const-class v0, Lza;

    const-class v1, Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v3

    const-class v0, Lfc;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v4

    const-class v0, Lsj0;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v5

    const-class v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v2}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v6

    const-class v0, Ln02;

    invoke-static {v0, v1}, Lda1;->a(Ljava/lang/Class;Ljava/lang/Class;)Lda1;

    move-result-object v7

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lzf0;

    aput-object v2, v0, v1

    const-class v1, Lcom/google/firebase/auth/FirebaseAuth;

    invoke-static {v1, v0}, Lzi;->f(Ljava/lang/Class;[Ljava/lang/Class;)Lzi$b;

    move-result-object v0

    const-class v1, Lr10;

    invoke-static {v1}, Los;->k(Ljava/lang/Class;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v1, Lwc0;

    invoke-static {v1}, Los;->m(Ljava/lang/Class;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v3}, Los;->j(Lda1;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v4}, Los;->j(Lda1;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v5}, Los;->j(Lda1;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v6}, Los;->j(Lda1;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    invoke-static {v7}, Los;->j(Lda1;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    const-class v1, Ldg0;

    invoke-static {v1}, Los;->i(Ljava/lang/Class;)Los;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzi$b;->b(Los;)Lzi$b;

    move-result-object v0

    new-instance v1, Lya2;

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lya2;-><init>(Lda1;Lda1;Lda1;Lda1;Lda1;)V

    invoke-virtual {v0, v1}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object v0

    invoke-virtual {v0}, Lzi$b;->d()Lzi;

    move-result-object v0

    invoke-static {}, Lvc0;->a()Lzi;

    move-result-object v1

    const-string v2, "fire-auth"

    const-string v3, "22.3.0"

    invoke-static {v2, v3}, Lmj0;->b(Ljava/lang/String;Ljava/lang/String;)Lzi;

    move-result-object v2

    filled-new-array {v0, v1, v2}, [Lzi;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

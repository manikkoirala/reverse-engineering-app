.class public final Lcom/google/firestore/v1/m;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lxv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firestore/v1/m$b;
    }
.end annotation


# static fields
.field public static final COMMIT_TIME_FIELD_NUMBER:I = 0x4

.field private static final DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

.field private static volatile PARSER:Lb31; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb31;"
        }
    .end annotation
.end field

.field public static final STREAM_ID_FIELD_NUMBER:I = 0x1

.field public static final STREAM_TOKEN_FIELD_NUMBER:I = 0x2

.field public static final WRITE_RESULTS_FIELD_NUMBER:I = 0x3


# instance fields
.field private commitTime_:Lcom/google/protobuf/j0;

.field private streamId_:Ljava/lang/String;

.field private streamToken_:Lcom/google/protobuf/ByteString;

.field private writeResults_:Lcom/google/protobuf/t$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/t$e;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/firestore/v1/m;

    invoke-direct {v0}, Lcom/google/firestore/v1/m;-><init>()V

    sput-object v0, Lcom/google/firestore/v1/m;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

    const-class v1, Lcom/google/firestore/v1/m;

    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->V(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/firestore/v1/m;->streamId_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/firestore/v1/m;->streamToken_:Lcom/google/protobuf/ByteString;

    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->A()Lcom/google/protobuf/t$e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firestore/v1/m;->writeResults_:Lcom/google/protobuf/t$e;

    return-void
.end method

.method public static synthetic Z()Lcom/google/firestore/v1/m;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/m;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

    return-object v0
.end method

.method public static b0()Lcom/google/firestore/v1/m;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/m;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

    return-object v0
.end method


# virtual methods
.method public a0()Lcom/google/protobuf/j0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/m;->commitTime_:Lcom/google/protobuf/j0;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/protobuf/j0;->c0()Lcom/google/protobuf/j0;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public c0()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/m;->streamToken_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public d0(I)Lcom/google/firestore/v1/n;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/m;->writeResults_:Lcom/google/protobuf/t$e;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/n;

    return-object p1
.end method

.method public e0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/m;->writeResults_:Lcom/google/protobuf/t$e;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final y(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object p2, Lcom/google/firestore/v1/m$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/firestore/v1/m;->PARSER:Lb31;

    if-nez p1, :cond_1

    const-class p2, Lcom/google/firestore/v1/m;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lcom/google/firestore/v1/m;->PARSER:Lb31;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$b;

    sget-object p3, Lcom/google/firestore/v1/m;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$b;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    sput-object p1, Lcom/google/firestore/v1/m;->PARSER:Lb31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/firestore/v1/m;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

    return-object p1

    :pswitch_4
    const-string p1, "streamId_"

    const-string p2, "streamToken_"

    const-string p3, "writeResults_"

    const-class v0, Lcom/google/firestore/v1/n;

    const-string v1, "commitTime_"

    filled-new-array {p1, p2, p3, v0, v1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u0208\u0002\n\u0003\u001b\u0004\t"

    sget-object p3, Lcom/google/firestore/v1/m;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/m;

    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->N(Lcom/google/protobuf/a0;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Lcom/google/firestore/v1/m$b;

    invoke-direct {p1, p2}, Lcom/google/firestore/v1/m$b;-><init>(Lcom/google/firestore/v1/m$a;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lcom/google/firestore/v1/m;

    invoke-direct {p1}, Lcom/google/firestore/v1/m;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

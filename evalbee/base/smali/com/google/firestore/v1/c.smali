.class public final Lcom/google/firestore/v1/c;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lxv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firestore/v1/c$b;
    }
.end annotation


# static fields
.field public static final BITS_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

.field public static final HASH_COUNT_FIELD_NUMBER:I = 0x2

.field private static volatile PARSER:Lb31;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb31;"
        }
    .end annotation
.end field


# instance fields
.field private bits_:Lcom/google/firestore/v1/b;

.field private hashCount_:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/firestore/v1/c;

    invoke-direct {v0}, Lcom/google/firestore/v1/c;-><init>()V

    sput-object v0, Lcom/google/firestore/v1/c;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

    const-class v1, Lcom/google/firestore/v1/c;

    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->V(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    return-void
.end method

.method public static synthetic Z()Lcom/google/firestore/v1/c;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/c;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

    return-object v0
.end method

.method public static b0()Lcom/google/firestore/v1/c;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/c;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

    return-object v0
.end method


# virtual methods
.method public a0()Lcom/google/firestore/v1/b;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/c;->bits_:Lcom/google/firestore/v1/b;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/firestore/v1/b;->b0()Lcom/google/firestore/v1/b;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public c0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/firestore/v1/c;->hashCount_:I

    return v0
.end method

.method public d0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/c;->bits_:Lcom/google/firestore/v1/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final y(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    sget-object p2, Lcom/google/firestore/v1/c$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/firestore/v1/c;->PARSER:Lb31;

    if-nez p1, :cond_1

    const-class p2, Lcom/google/firestore/v1/c;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lcom/google/firestore/v1/c;->PARSER:Lb31;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$b;

    sget-object p3, Lcom/google/firestore/v1/c;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$b;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    sput-object p1, Lcom/google/firestore/v1/c;->PARSER:Lb31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/firestore/v1/c;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

    return-object p1

    :pswitch_4
    const-string p1, "bits_"

    const-string p2, "hashCount_"

    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u0004"

    sget-object p3, Lcom/google/firestore/v1/c;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/c;

    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->N(Lcom/google/protobuf/a0;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Lcom/google/firestore/v1/c$b;

    invoke-direct {p1, p2}, Lcom/google/firestore/v1/c$b;-><init>(Lcom/google/firestore/v1/c$a;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lcom/google/firestore/v1/c;

    invoke-direct {p1}, Lcom/google/firestore/v1/c;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public final Lcom/google/firestore/v1/e;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lxv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firestore/v1/e$b;,
        Lcom/google/firestore/v1/e$c;
    }
.end annotation


# static fields
.field public static final CREATE_TIME_FIELD_NUMBER:I = 0x3

.field private static final DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

.field public static final FIELDS_FIELD_NUMBER:I = 0x2

.field public static final NAME_FIELD_NUMBER:I = 0x1

.field private static volatile PARSER:Lb31; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb31;"
        }
    .end annotation
.end field

.field public static final UPDATE_TIME_FIELD_NUMBER:I = 0x4


# instance fields
.field private createTime_:Lcom/google/protobuf/j0;

.field private fields_:Lcom/google/protobuf/MapFieldLite;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/MapFieldLite<",
            "Ljava/lang/String;",
            "Lcom/google/firestore/v1/Value;",
            ">;"
        }
    .end annotation
.end field

.field private name_:Ljava/lang/String;

.field private updateTime_:Lcom/google/protobuf/j0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/firestore/v1/e;

    invoke-direct {v0}, Lcom/google/firestore/v1/e;-><init>()V

    sput-object v0, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    const-class v1, Lcom/google/firestore/v1/e;

    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->V(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Lcom/google/protobuf/MapFieldLite;->emptyMapField()Lcom/google/protobuf/MapFieldLite;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firestore/v1/e;->fields_:Lcom/google/protobuf/MapFieldLite;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/firestore/v1/e;->name_:Ljava/lang/String;

    return-void
.end method

.method public static synthetic Z()Lcom/google/firestore/v1/e;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    return-object v0
.end method

.method public static synthetic a0(Lcom/google/firestore/v1/e;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firestore/v1/e;->l0(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b0(Lcom/google/firestore/v1/e;)Ljava/util/Map;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/google/firestore/v1/e;->f0()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic c0(Lcom/google/firestore/v1/e;Lcom/google/protobuf/j0;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/firestore/v1/e;->m0(Lcom/google/protobuf/j0;)V

    return-void
.end method

.method public static d0()Lcom/google/firestore/v1/e;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    return-object v0
.end method

.method public static k0()Lcom/google/firestore/v1/e$b;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->u()Lcom/google/protobuf/GeneratedMessageLite$a;

    move-result-object v0

    check-cast v0, Lcom/google/firestore/v1/e$b;

    return-object v0
.end method


# virtual methods
.method public e0()Ljava/util/Map;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firestore/v1/e;->i0()Lcom/google/protobuf/MapFieldLite;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final f0()Ljava/util/Map;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firestore/v1/e;->j0()Lcom/google/protobuf/MapFieldLite;

    move-result-object v0

    return-object v0
.end method

.method public g0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/e;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public h0()Lcom/google/protobuf/j0;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/e;->updateTime_:Lcom/google/protobuf/j0;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/protobuf/j0;->c0()Lcom/google/protobuf/j0;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final i0()Lcom/google/protobuf/MapFieldLite;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/e;->fields_:Lcom/google/protobuf/MapFieldLite;

    return-object v0
.end method

.method public final j0()Lcom/google/protobuf/MapFieldLite;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/firestore/v1/e;->fields_:Lcom/google/protobuf/MapFieldLite;

    invoke-virtual {v0}, Lcom/google/protobuf/MapFieldLite;->isMutable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/firestore/v1/e;->fields_:Lcom/google/protobuf/MapFieldLite;

    invoke-virtual {v0}, Lcom/google/protobuf/MapFieldLite;->mutableCopy()Lcom/google/protobuf/MapFieldLite;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firestore/v1/e;->fields_:Lcom/google/protobuf/MapFieldLite;

    :cond_0
    iget-object v0, p0, Lcom/google/firestore/v1/e;->fields_:Lcom/google/protobuf/MapFieldLite;

    return-object v0
.end method

.method public final l0(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lcom/google/firestore/v1/e;->name_:Ljava/lang/String;

    return-void
.end method

.method public final m0(Lcom/google/protobuf/j0;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lcom/google/firestore/v1/e;->updateTime_:Lcom/google/protobuf/j0;

    return-void
.end method

.method public final y(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object p2, Lcom/google/firestore/v1/e$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lcom/google/firestore/v1/e;->PARSER:Lb31;

    if-nez p1, :cond_1

    const-class p2, Lcom/google/firestore/v1/e;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lcom/google/firestore/v1/e;->PARSER:Lb31;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$b;

    sget-object p3, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$b;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    sput-object p1, Lcom/google/firestore/v1/e;->PARSER:Lb31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    return-object p1

    :pswitch_4
    const-string p1, "name_"

    const-string p2, "fields_"

    sget-object p3, Lcom/google/firestore/v1/e$c;->a:Lcom/google/protobuf/x;

    const-string v0, "createTime_"

    const-string v1, "updateTime_"

    filled-new-array {p1, p2, p3, v0, v1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0001\u0000\u0000\u0001\u0208\u00022\u0003\t\u0004\t"

    sget-object p3, Lcom/google/firestore/v1/e;->DEFAULT_INSTANCE:Lcom/google/firestore/v1/e;

    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->N(Lcom/google/protobuf/a0;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Lcom/google/firestore/v1/e$b;

    invoke-direct {p1, p2}, Lcom/google/firestore/v1/e$b;-><init>(Lcom/google/firestore/v1/e$a;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lcom/google/firestore/v1/e;

    invoke-direct {p1}, Lcom/google/firestore/v1/e;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

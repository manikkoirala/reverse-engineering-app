.class public interface abstract Lcom/google/protobuf/Writer;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/Writer$FieldOrder;
    }
.end annotation


# virtual methods
.method public abstract A(ILjava/util/List;Z)V
.end method

.method public abstract B()Lcom/google/protobuf/Writer$FieldOrder;
.end method

.method public abstract C(IJ)V
.end method

.method public abstract D(ILjava/util/List;Z)V
.end method

.method public abstract E(ILjava/util/List;Z)V
.end method

.method public abstract F(IF)V
.end method

.method public abstract G(II)V
.end method

.method public abstract H(ILjava/util/List;Z)V
.end method

.method public abstract I(II)V
.end method

.method public abstract J(ILcom/google/protobuf/ByteString;)V
.end method

.method public abstract K(ILcom/google/protobuf/x$a;Ljava/util/Map;)V
.end method

.method public abstract L(ILjava/util/List;Lcom/google/protobuf/g0;)V
.end method

.method public abstract M(ILjava/lang/Object;Lcom/google/protobuf/g0;)V
.end method

.method public abstract N(ILjava/util/List;Lcom/google/protobuf/g0;)V
.end method

.method public abstract O(ILjava/lang/Object;Lcom/google/protobuf/g0;)V
.end method

.method public abstract a(ILjava/util/List;Z)V
.end method

.method public abstract b(ILjava/lang/Object;)V
.end method

.method public abstract c(II)V
.end method

.method public abstract d(ILjava/lang/String;)V
.end method

.method public abstract e(IJ)V
.end method

.method public abstract f(ILjava/util/List;Z)V
.end method

.method public abstract g(II)V
.end method

.method public abstract h(ILjava/util/List;Z)V
.end method

.method public abstract i(ILjava/util/List;Z)V
.end method

.method public abstract j(IJ)V
.end method

.method public abstract k(II)V
.end method

.method public abstract l(ILjava/util/List;Z)V
.end method

.method public abstract m(IJ)V
.end method

.method public abstract n(IZ)V
.end method

.method public abstract o(II)V
.end method

.method public abstract p(I)V
.end method

.method public abstract q(ILjava/util/List;Z)V
.end method

.method public abstract r(I)V
.end method

.method public abstract s(ILjava/util/List;Z)V
.end method

.method public abstract t(ILjava/util/List;Z)V
.end method

.method public abstract u(ILjava/util/List;)V
.end method

.method public abstract v(ILjava/util/List;)V
.end method

.method public abstract w(IJ)V
.end method

.method public abstract x(ILjava/util/List;Z)V
.end method

.method public abstract y(ILjava/util/List;Z)V
.end method

.method public abstract z(ID)V
.end method

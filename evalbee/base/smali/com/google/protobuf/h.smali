.class public final Lcom/google/protobuf/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/f0;


# instance fields
.field public final a:Lcom/google/protobuf/g;

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Lcom/google/protobuf/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    const-string v0, "input"

    invoke-static {p1, v0}, Lcom/google/protobuf/t;->b(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/g;

    iput-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    iput-object p0, p1, Lcom/google/protobuf/g;->d:Lcom/google/protobuf/h;

    return-void
.end method

.method public static N(Lcom/google/protobuf/g;)Lcom/google/protobuf/h;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/g;->d:Lcom/google/protobuf/h;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/protobuf/h;

    invoke-direct {v0, p0}, Lcom/google/protobuf/h;-><init>(Lcom/google/protobuf/g;)V

    return-object v0
.end method


# virtual methods
.method public A()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->v()I

    move-result v0

    return v0
.end method

.method public B(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/w;

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/w;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eq p1, v2, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/h;->Y(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->t()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->t()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eq v0, v2, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->Y(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->t()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public C(Ljava/util/List;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/s;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/s;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public D()I
    .locals 1

    .line 1
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->s()I

    move-result v0

    return v0
.end method

.method public E()J
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->A()J

    move-result-wide v0

    return-wide v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public G()I
    .locals 1

    .line 1
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->x()I

    move-result v0

    return v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->C()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I(Ljava/util/Map;Lcom/google/protobuf/x$a;Lcom/google/protobuf/l;)V
    .locals 7

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->E()I

    move-result v1

    iget-object v2, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v2, v1}, Lcom/google/protobuf/g;->n(I)I

    move-result v1

    iget-object v2, p2, Lcom/google/protobuf/x$a;->b:Ljava/lang/Object;

    iget-object v3, p2, Lcom/google/protobuf/x$a;->d:Ljava/lang/Object;

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/protobuf/h;->m()I

    move-result v4

    const v5, 0x7fffffff

    if-eq v4, v5, :cond_5

    iget-object v5, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v5}, Lcom/google/protobuf/g;->e()Z

    move-result v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    const/4 v5, 0x1

    const-string v6, "Unable to parse map entry."

    if-eq v4, v5, :cond_3

    if-eq v4, v0, :cond_2

    :try_start_1
    invoke-virtual {p0}, Lcom/google/protobuf/h;->p()Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-direct {v4, v6}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iget-object v4, p2, Lcom/google/protobuf/x$a;->c:Lcom/google/protobuf/WireFormat$FieldType;

    iget-object v5, p2, Lcom/google/protobuf/x$a;->d:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p0, v4, v5, p3}, Lcom/google/protobuf/h;->Q(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Class;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    :cond_3
    iget-object v4, p2, Lcom/google/protobuf/x$a;->a:Lcom/google/protobuf/WireFormat$FieldType;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v5}, Lcom/google/protobuf/h;->Q(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Class;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catch Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    invoke-virtual {p0}, Lcom/google/protobuf/h;->p()Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_0

    :cond_4
    new-instance p1, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-direct {p1, v6}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_1
    invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/g;->m(I)V

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p2, v1}, Lcom/google/protobuf/g;->m(I)V

    throw p1
.end method

.method public J(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
    .locals 1

    .line 1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/protobuf/h;->O(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V

    return-void
.end method

.method public K(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
    .locals 1

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/protobuf/h;->P(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V

    return-void
.end method

.method public L(Ljava/util/List;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/protobuf/h;->b:I

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/google/protobuf/h;->R(Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->e()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/protobuf/h;->d:I

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->D()I

    move-result v1

    if-eq v1, v0, :cond_0

    iput v1, p0, Lcom/google/protobuf/h;->d:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1
.end method

.method public M(Ljava/util/List;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/protobuf/h;->b:I

    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/google/protobuf/h;->S(Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->e()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/protobuf/h;->d:I

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->D()I

    move-result v1

    if-eq v1, v0, :cond_0

    iput v1, p0, Lcom/google/protobuf/h;->d:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1
.end method

.method public final O(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->c:I

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v1}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/protobuf/WireFormat;->c(II)I

    move-result v1

    iput v1, p0, Lcom/google/protobuf/h;->c:I

    :try_start_0
    invoke-interface {p2, p1, p0, p3}, Lcom/google/protobuf/g0;->i(Ljava/lang/Object;Lcom/google/protobuf/f0;Lcom/google/protobuf/l;)V

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    iget p2, p0, Lcom/google/protobuf/h;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, p2, :cond_0

    iput v0, p0, Lcom/google/protobuf/h;->c:I

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->parseFailure()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    iput v0, p0, Lcom/google/protobuf/h;->c:I

    throw p1
.end method

.method public final P(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    iget v2, v1, Lcom/google/protobuf/g;->a:I

    iget v3, v1, Lcom/google/protobuf/g;->b:I

    if-ge v2, v3, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/protobuf/g;->n(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    iget v2, v1, Lcom/google/protobuf/g;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/protobuf/g;->a:I

    invoke-interface {p2, p1, p0, p3}, Lcom/google/protobuf/g0;->i(Ljava/lang/Object;Lcom/google/protobuf/f0;Lcom/google/protobuf/l;)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/google/protobuf/g;->a(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    iget p2, p1, Lcom/google/protobuf/g;->a:I

    add-int/lit8 p2, p2, -0x1

    iput p2, p1, Lcom/google/protobuf/g;->a:I

    invoke-virtual {p1, v0}, Lcom/google/protobuf/g;->m(I)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->recursionLimitExceeded()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
.end method

.method public final Q(Lcom/google/protobuf/WireFormat$FieldType;Ljava/lang/Class;Lcom/google/protobuf/l;)Ljava/lang/Object;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/protobuf/h$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "unsupported field type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/protobuf/h;->i()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/protobuf/h;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/protobuf/h;->H()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/protobuf/h;->E()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_4
    invoke-virtual {p0}, Lcom/google/protobuf/h;->e()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_5
    invoke-virtual {p0}, Lcom/google/protobuf/h;->b()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_6
    invoke-virtual {p0}, Lcom/google/protobuf/h;->G()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_7
    invoke-virtual {p0, p2, p3}, Lcom/google/protobuf/h;->T(Ljava/lang/Class;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_8
    invoke-virtual {p0}, Lcom/google/protobuf/h;->s()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_9
    invoke-virtual {p0}, Lcom/google/protobuf/h;->A()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_a
    invoke-virtual {p0}, Lcom/google/protobuf/h;->readFloat()F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    return-object p1

    :pswitch_b
    invoke-virtual {p0}, Lcom/google/protobuf/h;->t()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1

    :pswitch_c
    invoke-virtual {p0}, Lcom/google/protobuf/h;->D()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_d
    invoke-virtual {p0}, Lcom/google/protobuf/h;->d()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_e
    invoke-virtual {p0}, Lcom/google/protobuf/h;->readDouble()D

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    return-object p1

    :pswitch_f
    invoke-virtual {p0}, Lcom/google/protobuf/h;->g()Lcom/google/protobuf/ByteString;

    move-result-object p1

    return-object p1

    :pswitch_10
    invoke-virtual {p0}, Lcom/google/protobuf/h;->v()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final R(Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/google/protobuf/g0;->newInstance()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/protobuf/h;->O(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V

    invoke-interface {p1, v0}, Lcom/google/protobuf/g0;->d(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final S(Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/google/protobuf/g0;->newInstance()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/protobuf/h;->P(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V

    invoke-interface {p1, v0}, Lcom/google/protobuf/g0;->d(Ljava/lang/Object;)V

    return-object v0
.end method

.method public T(Ljava/lang/Class;Lcom/google/protobuf/l;)Ljava/lang/Object;
    .locals 1

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    invoke-static {}, Lk91;->a()Lk91;

    move-result-object v0

    invoke-virtual {v0, p1}, Lk91;->c(Ljava/lang/Class;)Lcom/google/protobuf/g0;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/h;->S(Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public U(Ljava/util/List;Z)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    instance-of v0, p1, Lfj0;

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    move-object v0, p1

    check-cast v0, Lfj0;

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/h;->g()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-interface {v0, p1}, Lfj0;->q(Lcom/google/protobuf/ByteString;)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget p2, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, p2, :cond_0

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/google/protobuf/h;->H()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/protobuf/h;->F()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_2

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_5
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1
.end method

.method public final V(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->truncatedMessage()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
.end method

.method public final W(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1
.end method

.method public final X(I)V
    .locals 0

    .line 1
    and-int/lit8 p1, p1, 0x3

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->parseFailure()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
.end method

.method public final Y(I)V
    .locals 0

    .line 1
    and-int/lit8 p1, p1, 0x7

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->parseFailure()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/w;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/w;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->A()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->A()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->A()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->A()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public b()J
    .locals 2

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->r()I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->z()I

    move-result v0

    return v0
.end method

.method public f(Ljava/util/List;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/f;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/f;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->o()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/f;->b(Z)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->o()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/f;->b(Z)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public g()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->p()Lcom/google/protobuf/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public getTag()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/h;->b:I

    return v0
.end method

.method public h(Ljava/util/List;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/s;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/s;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->z()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->z()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->z()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->z()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public i()J
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->F()J

    move-result-wide v0

    return-wide v0
.end method

.method public j(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/w;

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/w;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eq p1, v2, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/h;->Y(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->y()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->y()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eq v0, v2, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->Y(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->y()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->y()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public k(Ljava/util/List;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/s;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/s;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->v()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->v()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public l(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/s;

    const/4 v1, 0x5

    const/4 v2, 0x2

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/s;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eq p1, v2, :cond_3

    if-ne p1, v1, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->s()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_0

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_2
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/h;->X(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int v3, v1, p1

    :cond_4
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->s()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v3, :cond_4

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eq v0, v2, :cond_9

    if-ne v0, v1, :cond_8

    :cond_6
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    return-void

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_6

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_8
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_9
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->X(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_a

    :goto_0
    return-void
.end method

.method public m()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->d:I

    if-eqz v0, :cond_0

    iput v0, p0, Lcom/google/protobuf/h;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iput v0, p0, Lcom/google/protobuf/h;->b:I

    :goto_0
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/google/protobuf/h;->c:I

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v0

    return v0

    :cond_2
    :goto_1
    const v0, 0x7fffffff

    return v0
.end method

.method public n(Ljava/util/List;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/h;->U(Ljava/util/List;Z)V

    return-void
.end method

.method public o(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/p;

    const/4 v1, 0x5

    const/4 v2, 0x2

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/p;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eq p1, v2, :cond_3

    if-ne p1, v1, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->u()F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/p;->b(F)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_0

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_2
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/h;->X(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int v3, v1, p1

    :cond_4
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->u()F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/p;->b(F)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v3, :cond_4

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eq v0, v2, :cond_9

    if-ne v0, v1, :cond_8

    :cond_6
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->u()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    return-void

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_6

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_8
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_9
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->X(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->u()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_a

    :goto_0
    return-void
.end method

.method public p()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/protobuf/h;->b:I

    iget v1, p0, Lcom/google/protobuf/h;->c:I

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1, v0}, Lcom/google/protobuf/g;->H(I)Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public q(Ljava/util/List;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/h;->g()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_2
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1
.end method

.method public r(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/j;

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/j;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eq p1, v2, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/h;->Y(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->q()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/j;->b(D)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->q()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/j;->b(D)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eq v0, v2, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->Y(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->q()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->q()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public readDouble()D
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->q()D

    move-result-wide v0

    return-wide v0
.end method

.method public readFloat()F
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->u()F

    move-result v0

    return v0
.end method

.method public s()J
    .locals 2

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->w()J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->t()J

    move-result-wide v0

    return-wide v0
.end method

.method public u(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/s;

    const/4 v1, 0x5

    const/4 v2, 0x2

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/s;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eq p1, v2, :cond_3

    if-ne p1, v1, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->x()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_0

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_2
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/h;->X(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int v3, v1, p1

    :cond_4
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->x()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v3, :cond_4

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eq v0, v2, :cond_9

    if-ne v0, v1, :cond_8

    :cond_6
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    return-void

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_6

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_8
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_9
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->X(I)V

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_a

    :goto_0
    return-void
.end method

.method public v()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/h;->W(I)V

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->o()Z

    move-result v0

    return v0
.end method

.method public w(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/w;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/w;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->F()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->F()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->F()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->F()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public x(Ljava/util/List;)V
    .locals 4

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/w;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/w;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->w()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->w()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/w;->b(J)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->w()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->w()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public y(Ljava/util/List;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/s;

    const/4 v1, 0x2

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/s;

    iget p1, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result p1

    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->E()I

    move-result p1

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, p1

    :cond_0
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->r()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->d()I

    move-result p1

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_2
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->r()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/protobuf/s;->g(I)V

    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {p1}, Lcom/google/protobuf/g;->D()I

    move-result p1

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq p1, v1, :cond_2

    iput p1, p0, Lcom/google/protobuf/h;->d:I

    return-void

    :cond_4
    iget v0, p0, Lcom/google/protobuf/h;->b:I

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v0

    if-eqz v0, :cond_7

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->E()I

    move-result v0

    iget-object v1, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v1}, Lcom/google/protobuf/g;->d()I

    move-result v1

    add-int/2addr v1, v0

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->r()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->d()I

    move-result v0

    if-lt v0, v1, :cond_5

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/h;->V(I)V

    return-void

    :cond_6
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;

    move-result-object p1

    throw p1

    :cond_7
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->r()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/h;->a:Lcom/google/protobuf/g;

    invoke-virtual {v0}, Lcom/google/protobuf/g;->D()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/h;->b:I

    if-eq v0, v1, :cond_7

    iput v0, p0, Lcom/google/protobuf/h;->d:I

    return-void
.end method

.method public z(Ljava/util/List;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/h;->U(Ljava/util/List;Z)V

    return-void
.end method

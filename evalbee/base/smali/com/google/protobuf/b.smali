.class public abstract Lcom/google/protobuf/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lb31;


# static fields
.field public static final a:Lcom/google/protobuf/l;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/protobuf/l;->b()Lcom/google/protobuf/l;

    move-result-object v0

    sput-object v0, Lcom/google/protobuf/b;->a:Lcom/google/protobuf/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/protobuf/g;Lcom/google/protobuf/l;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/b;->f(Lcom/google/protobuf/g;Lcom/google/protobuf/l;)Lcom/google/protobuf/a0;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic c([BLcom/google/protobuf/l;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/b;->h([BLcom/google/protobuf/l;)Lcom/google/protobuf/a0;

    move-result-object p1

    return-object p1
.end method

.method public final d(Lcom/google/protobuf/a0;)Lcom/google/protobuf/a0;
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lxv0;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/protobuf/b;->e(Lcom/google/protobuf/a0;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/a0;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object p1
.end method

.method public final e(Lcom/google/protobuf/a0;)Lcom/google/protobuf/UninitializedMessageException;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/google/protobuf/a;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/protobuf/a;

    invoke-virtual {p1}, Lcom/google/protobuf/a;->m()Lcom/google/protobuf/UninitializedMessageException;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0, p1}, Lcom/google/protobuf/UninitializedMessageException;-><init>(Lcom/google/protobuf/a0;)V

    return-object v0
.end method

.method public f(Lcom/google/protobuf/g;Lcom/google/protobuf/l;)Lcom/google/protobuf/a0;
    .locals 0

    .line 1
    invoke-interface {p0, p1, p2}, Lb31;->b(Lcom/google/protobuf/g;Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/a0;

    invoke-virtual {p0, p1}, Lcom/google/protobuf/b;->d(Lcom/google/protobuf/a0;)Lcom/google/protobuf/a0;

    move-result-object p1

    return-object p1
.end method

.method public g([BIILcom/google/protobuf/l;)Lcom/google/protobuf/a0;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/protobuf/b;->i([BIILcom/google/protobuf/l;)Lcom/google/protobuf/a0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/b;->d(Lcom/google/protobuf/a0;)Lcom/google/protobuf/a0;

    move-result-object p1

    return-object p1
.end method

.method public h([BLcom/google/protobuf/l;)Lcom/google/protobuf/a0;
    .locals 2

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, p2}, Lcom/google/protobuf/b;->g([BIILcom/google/protobuf/l;)Lcom/google/protobuf/a0;

    move-result-object p1

    return-object p1
.end method

.method public abstract i([BIILcom/google/protobuf/l;)Lcom/google/protobuf/a0;
.end method

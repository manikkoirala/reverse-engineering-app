.class public final Lcom/google/protobuf/d0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/g0;


# instance fields
.field public final a:Lcom/google/protobuf/a0;

.field public final b:Lcom/google/protobuf/k0;

.field public final c:Z

.field public final d:Lcom/google/protobuf/m;


# direct methods
.method public constructor <init>(Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/a0;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {p2, p3}, Lcom/google/protobuf/m;->e(Lcom/google/protobuf/a0;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/google/protobuf/d0;->c:Z

    iput-object p2, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    iput-object p3, p0, Lcom/google/protobuf/d0;->a:Lcom/google/protobuf/a0;

    return-void
.end method

.method public static l(Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/a0;)Lcom/google/protobuf/d0;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/protobuf/d0;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/protobuf/d0;-><init>(Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/a0;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-static {v0, p1, p2}, Lcom/google/protobuf/h0;->F(Lcom/google/protobuf/k0;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/protobuf/d0;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-static {v0, p1, p2}, Lcom/google/protobuf/h0;->D(Lcom/google/protobuf/m;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/m;->c(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/o;->k()Z

    move-result p1

    return p1
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/k0;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {v1, p2}, Lcom/google/protobuf/k0;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-boolean v0, p0, Lcom/google/protobuf/d0;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/m;->c(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object p1

    iget-object v0, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v0, p2}, Lcom/google/protobuf/m;->c(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/protobuf/o;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public d(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/k0;->j(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/m;->f(Ljava/lang/Object;)V

    return-void
.end method

.method public e(Ljava/lang/Object;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {p0, v0, p1}, Lcom/google/protobuf/d0;->j(Lcom/google/protobuf/k0;Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    iget-boolean v1, p0, Lcom/google/protobuf/d0;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v1, p1}, Lcom/google/protobuf/m;->c(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/o;->f()I

    move-result p1

    add-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public f(Ljava/lang/Object;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/k0;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-boolean v1, p0, Lcom/google/protobuf/d0;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v1, p1}, Lcom/google/protobuf/m;->c(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object p1

    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p1}, Lcom/google/protobuf/o;->hashCode()I

    move-result p1

    add-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method public g(Ljava/lang/Object;Lcom/google/protobuf/Writer;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/m;->c(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/o;->n()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/protobuf/d0;->n(Lcom/google/protobuf/k0;Ljava/lang/Object;Lcom/google/protobuf/Writer;)V

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public h(Ljava/lang/Object;[BIILcom/google/protobuf/e$a;)V
    .locals 0

    .line 1
    move-object p2, p1

    check-cast p2, Lcom/google/protobuf/GeneratedMessageLite;

    iget-object p3, p2, Lcom/google/protobuf/GeneratedMessageLite;->unknownFields:Lcom/google/protobuf/l0;

    invoke-static {}, Lcom/google/protobuf/l0;->c()Lcom/google/protobuf/l0;

    move-result-object p4

    if-ne p3, p4, :cond_0

    invoke-static {}, Lcom/google/protobuf/l0;->k()Lcom/google/protobuf/l0;

    move-result-object p3

    iput-object p3, p2, Lcom/google/protobuf/GeneratedMessageLite;->unknownFields:Lcom/google/protobuf/l0;

    :cond_0
    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public i(Ljava/lang/Object;Lcom/google/protobuf/f0;Lcom/google/protobuf/l;)V
    .locals 6

    .line 1
    iget-object v1, p0, Lcom/google/protobuf/d0;->b:Lcom/google/protobuf/k0;

    iget-object v2, p0, Lcom/google/protobuf/d0;->d:Lcom/google/protobuf/m;

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/protobuf/d0;->k(Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Ljava/lang/Object;Lcom/google/protobuf/f0;Lcom/google/protobuf/l;)V

    return-void
.end method

.method public final j(Lcom/google/protobuf/k0;Ljava/lang/Object;)I
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Lcom/google/protobuf/k0;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/protobuf/k0;->i(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public final k(Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Ljava/lang/Object;Lcom/google/protobuf/f0;Lcom/google/protobuf/l;)V
    .locals 9

    .line 1
    invoke-virtual {p1, p3}, Lcom/google/protobuf/k0;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p2, p3}, Lcom/google/protobuf/m;->d(Ljava/lang/Object;)Lcom/google/protobuf/o;

    move-result-object v8

    :goto_0
    :try_start_0
    invoke-interface {p4}, Lcom/google/protobuf/f0;->m()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, p3, v7}, Lcom/google/protobuf/k0;->o(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p4

    move-object v2, p5

    move-object v3, p2

    move-object v4, v8

    move-object v5, p1

    move-object v6, v7

    :try_start_1
    invoke-virtual/range {v0 .. v6}, Lcom/google/protobuf/d0;->m(Lcom/google/protobuf/f0;Lcom/google/protobuf/l;Lcom/google/protobuf/m;Lcom/google/protobuf/o;Lcom/google/protobuf/k0;Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p3, v7}, Lcom/google/protobuf/k0;->o(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1, p3, v7}, Lcom/google/protobuf/k0;->o(Ljava/lang/Object;Ljava/lang/Object;)V

    throw p2
.end method

.method public final m(Lcom/google/protobuf/f0;Lcom/google/protobuf/l;Lcom/google/protobuf/m;Lcom/google/protobuf/o;Lcom/google/protobuf/k0;Ljava/lang/Object;)Z
    .locals 6

    .line 1
    invoke-interface {p1}, Lcom/google/protobuf/f0;->getTag()I

    move-result v0

    sget v1, Lcom/google/protobuf/WireFormat;->a:I

    const/4 v2, 0x1

    if-eq v0, v1, :cond_2

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/protobuf/d0;->a:Lcom/google/protobuf/a0;

    invoke-static {v0}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v0

    invoke-virtual {p3, p2, v1, v0}, Lcom/google/protobuf/m;->b(Lcom/google/protobuf/l;Lcom/google/protobuf/a0;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p3, p1, v0, p2, p4}, Lcom/google/protobuf/m;->h(Lcom/google/protobuf/f0;Ljava/lang/Object;Lcom/google/protobuf/l;Lcom/google/protobuf/o;)V

    return v2

    :cond_0
    invoke-virtual {p5, p6, p1}, Lcom/google/protobuf/k0;->m(Ljava/lang/Object;Lcom/google/protobuf/f0;)Z

    move-result p1

    return p1

    :cond_1
    invoke-interface {p1}, Lcom/google/protobuf/f0;->p()Z

    move-result p1

    return p1

    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    move v3, v1

    move-object v1, v0

    :cond_3
    :goto_0
    invoke-interface {p1}, Lcom/google/protobuf/f0;->m()I

    move-result v4

    const v5, 0x7fffffff

    if-ne v4, v5, :cond_4

    goto :goto_1

    :cond_4
    invoke-interface {p1}, Lcom/google/protobuf/f0;->getTag()I

    move-result v4

    sget v5, Lcom/google/protobuf/WireFormat;->c:I

    if-ne v4, v5, :cond_5

    invoke-interface {p1}, Lcom/google/protobuf/f0;->c()I

    move-result v3

    iget-object v0, p0, Lcom/google/protobuf/d0;->a:Lcom/google/protobuf/a0;

    invoke-virtual {p3, p2, v0, v3}, Lcom/google/protobuf/m;->b(Lcom/google/protobuf/l;Lcom/google/protobuf/a0;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget v5, Lcom/google/protobuf/WireFormat;->d:I

    if-ne v4, v5, :cond_7

    if-eqz v0, :cond_6

    invoke-virtual {p3, p1, v0, p2, p4}, Lcom/google/protobuf/m;->h(Lcom/google/protobuf/f0;Ljava/lang/Object;Lcom/google/protobuf/l;Lcom/google/protobuf/o;)V

    goto :goto_0

    :cond_6
    invoke-interface {p1}, Lcom/google/protobuf/f0;->g()Lcom/google/protobuf/ByteString;

    move-result-object v1

    goto :goto_0

    :cond_7
    invoke-interface {p1}, Lcom/google/protobuf/f0;->p()Z

    move-result v4

    if-nez v4, :cond_3

    :goto_1
    invoke-interface {p1}, Lcom/google/protobuf/f0;->getTag()I

    move-result p1

    sget v4, Lcom/google/protobuf/WireFormat;->b:I

    if-ne p1, v4, :cond_a

    if-eqz v1, :cond_9

    if-eqz v0, :cond_8

    invoke-virtual {p3, v1, v0, p2, p4}, Lcom/google/protobuf/m;->i(Lcom/google/protobuf/ByteString;Ljava/lang/Object;Lcom/google/protobuf/l;Lcom/google/protobuf/o;)V

    goto :goto_2

    :cond_8
    invoke-virtual {p5, p6, v3, v1}, Lcom/google/protobuf/k0;->d(Ljava/lang/Object;ILcom/google/protobuf/ByteString;)V

    :cond_9
    :goto_2
    return v2

    :cond_a
    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidEndTag()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p1

    throw p1
.end method

.method public final n(Lcom/google/protobuf/k0;Ljava/lang/Object;Lcom/google/protobuf/Writer;)V
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Lcom/google/protobuf/k0;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2, p3}, Lcom/google/protobuf/k0;->s(Ljava/lang/Object;Lcom/google/protobuf/Writer;)V

    return-void
.end method

.method public newInstance()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/d0;->a:Lcom/google/protobuf/a0;

    instance-of v1, v0, Lcom/google/protobuf/GeneratedMessageLite;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/protobuf/GeneratedMessageLite;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->O()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/protobuf/a0;->d()Lcom/google/protobuf/a0$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/a0$a;->g()Lcom/google/protobuf/a0;

    move-result-object v0

    return-object v0
.end method

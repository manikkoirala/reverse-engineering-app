.class public final Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$NetworkConnectionInfoEncoder;,
        Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$LogEventEncoder;,
        Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$AndroidClientInfoEncoder;,
        Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$ClientInfoEncoder;,
        Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$LogRequestEncoder;,
        Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$BatchedLogRequestEncoder;
    }
.end annotation


# static fields
.field public static final CODEGEN_VERSION:I = 0x2

.field public static final CONFIG:Ljk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder;

    invoke-direct {v0}, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder;-><init>()V

    sput-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder;->CONFIG:Ljk;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public configure(Lzw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lzw;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$BatchedLogRequestEncoder;->INSTANCE:Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$BatchedLogRequestEncoder;

    const-class v1, Lcom/google/android/datatransport/cct/internal/BatchedLogRequest;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AutoValue_BatchedLogRequest;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    sget-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$LogRequestEncoder;->INSTANCE:Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$LogRequestEncoder;

    const-class v1, Lcom/google/android/datatransport/cct/internal/LogRequest;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AutoValue_LogRequest;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    sget-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$ClientInfoEncoder;->INSTANCE:Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$ClientInfoEncoder;

    const-class v1, Lcom/google/android/datatransport/cct/internal/ClientInfo;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AutoValue_ClientInfo;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    sget-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$AndroidClientInfoEncoder;->INSTANCE:Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$AndroidClientInfoEncoder;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AndroidClientInfo;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AutoValue_AndroidClientInfo;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    sget-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$LogEventEncoder;->INSTANCE:Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$LogEventEncoder;

    const-class v1, Lcom/google/android/datatransport/cct/internal/LogEvent;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AutoValue_LogEvent;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    sget-object v0, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$NetworkConnectionInfoEncoder;->INSTANCE:Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder$NetworkConnectionInfoEncoder;

    const-class v1, Lcom/google/android/datatransport/cct/internal/NetworkConnectionInfo;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    const-class v1, Lcom/google/android/datatransport/cct/internal/AutoValue_NetworkConnectionInfo;

    invoke-interface {p1, v1, v0}, Lzw;->a(Ljava/lang/Class;Lw01;)Lzw;

    return-void
.end method

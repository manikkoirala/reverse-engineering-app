.class public abstract Lcom/google/android/datatransport/cct/internal/BatchedLogRequest;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/auto/value/AutoValue;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Ljava/util/List;)Lcom/google/android/datatransport/cct/internal/BatchedLogRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/datatransport/cct/internal/LogRequest;",
            ">;)",
            "Lcom/google/android/datatransport/cct/internal/BatchedLogRequest;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/datatransport/cct/internal/AutoValue_BatchedLogRequest;

    invoke-direct {v0, p0}, Lcom/google/android/datatransport/cct/internal/AutoValue_BatchedLogRequest;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static createDataEncoder()Lhp;
    .locals 2

    .line 1
    new-instance v0, Lmh0;

    invoke-direct {v0}, Lmh0;-><init>()V

    sget-object v1, Lcom/google/android/datatransport/cct/internal/AutoBatchedLogRequestEncoder;->CONFIG:Ljk;

    invoke-virtual {v0, v1}, Lmh0;->j(Ljk;)Lmh0;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmh0;->k(Z)Lmh0;

    move-result-object v0

    invoke-virtual {v0}, Lmh0;->i()Lhp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract getLogRequests()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/datatransport/cct/internal/LogRequest;",
            ">;"
        }
    .end annotation
.end method

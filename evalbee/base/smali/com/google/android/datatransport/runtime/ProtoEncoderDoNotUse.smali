.class public abstract Lcom/google/android/datatransport/runtime/ProtoEncoderDoNotUse;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ENCODER:Lo91;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lo91;->a()Lo91$a;

    move-result-object v0

    sget-object v1, Lcom/google/android/datatransport/runtime/AutoProtoEncoderDoNotUseEncoder;->CONFIG:Ljk;

    invoke-virtual {v0, v1}, Lo91$a;->d(Ljk;)Lo91$a;

    move-result-object v0

    invoke-virtual {v0}, Lo91$a;->c()Lo91;

    move-result-object v0

    sput-object v0, Lcom/google/android/datatransport/runtime/ProtoEncoderDoNotUse;->ENCODER:Lo91;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encode(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/datatransport/runtime/ProtoEncoderDoNotUse;->ENCODER:Lo91;

    invoke-virtual {v0, p0, p1}, Lo91;->b(Ljava/lang/Object;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static encode(Ljava/lang/Object;)[B
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/datatransport/runtime/ProtoEncoderDoNotUse;->ENCODER:Lo91;

    invoke-virtual {v0, p0}, Lo91;->c(Ljava/lang/Object;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract getClientMetrics()Lcom/google/android/datatransport/runtime/firebase/transport/ClientMetrics;
.end method

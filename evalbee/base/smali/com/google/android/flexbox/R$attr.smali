.class public final Lcom/google/android/flexbox/R$attr;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/flexbox/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alignContent:I = 0x7f040030

.field public static final alignItems:I = 0x7f040031

.field public static final alpha:I = 0x7f040033

.field public static final dividerDrawable:I = 0x7f040186

.field public static final dividerDrawableHorizontal:I = 0x7f040187

.field public static final dividerDrawableVertical:I = 0x7f040188

.field public static final fastScrollEnabled:I = 0x7f0401de

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f0401df

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f0401e0

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f0401e1

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f0401e2

.field public static final flexDirection:I = 0x7f0401e4

.field public static final flexWrap:I = 0x7f0401e5

.field public static final font:I = 0x7f040208

.field public static final fontProviderAuthority:I = 0x7f04020a

.field public static final fontProviderCerts:I = 0x7f04020b

.field public static final fontProviderFetchStrategy:I = 0x7f04020c

.field public static final fontProviderFetchTimeout:I = 0x7f04020d

.field public static final fontProviderPackage:I = 0x7f04020e

.field public static final fontProviderQuery:I = 0x7f04020f

.field public static final fontStyle:I = 0x7f040211

.field public static final fontVariationSettings:I = 0x7f040212

.field public static final fontWeight:I = 0x7f040213

.field public static final justifyContent:I = 0x7f040270

.field public static final layoutManager:I = 0x7f04027e

.field public static final layout_alignSelf:I = 0x7f04027f

.field public static final layout_flexBasisPercent:I = 0x7f0402b6

.field public static final layout_flexGrow:I = 0x7f0402b7

.field public static final layout_flexShrink:I = 0x7f0402b8

.field public static final layout_maxHeight:I = 0x7f0402c3

.field public static final layout_maxWidth:I = 0x7f0402c4

.field public static final layout_minHeight:I = 0x7f0402c5

.field public static final layout_minWidth:I = 0x7f0402c6

.field public static final layout_order:I = 0x7f0402c8

.field public static final layout_wrapBefore:I = 0x7f0402cc

.field public static final maxLine:I = 0x7f040321

.field public static final recyclerViewStyle:I = 0x7f0403b2

.field public static final reverseLayout:I = 0x7f0403b8

.field public static final showDivider:I = 0x7f0403dd

.field public static final showDividerHorizontal:I = 0x7f0403de

.field public static final showDividerVertical:I = 0x7f0403df

.field public static final spanCount:I = 0x7f0403f4

.field public static final stackFromEnd:I = 0x7f0403ff

.field public static final ttcIndex:I = 0x7f0404d6


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public final Lcom/google/common/cache/LocalCache$n;
.super Lcom/google/common/cache/LocalCache$o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/common/cache/LocalCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "n"
.end annotation


# instance fields
.field public volatile e:J

.field public f:Lcom/google/common/cache/a;

.field public g:Lcom/google/common/cache/a;

.field public volatile h:J

.field public i:Lcom/google/common/cache/a;

.field public j:Lcom/google/common/cache/a;


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILcom/google/common/cache/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/cache/LocalCache$o;-><init>(Ljava/lang/Object;ILcom/google/common/cache/a;)V

    const-wide p1, 0x7fffffffffffffffL

    iput-wide p1, p0, Lcom/google/common/cache/LocalCache$n;->e:J

    invoke-static {}, Lcom/google/common/cache/LocalCache;->x()Lcom/google/common/cache/a;

    move-result-object p3

    iput-object p3, p0, Lcom/google/common/cache/LocalCache$n;->f:Lcom/google/common/cache/a;

    invoke-static {}, Lcom/google/common/cache/LocalCache;->x()Lcom/google/common/cache/a;

    move-result-object p3

    iput-object p3, p0, Lcom/google/common/cache/LocalCache$n;->g:Lcom/google/common/cache/a;

    iput-wide p1, p0, Lcom/google/common/cache/LocalCache$n;->h:J

    invoke-static {}, Lcom/google/common/cache/LocalCache;->x()Lcom/google/common/cache/a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/common/cache/LocalCache$n;->i:Lcom/google/common/cache/a;

    invoke-static {}, Lcom/google/common/cache/LocalCache;->x()Lcom/google/common/cache/a;

    move-result-object p1

    iput-object p1, p0, Lcom/google/common/cache/LocalCache$n;->j:Lcom/google/common/cache/a;

    return-void
.end method


# virtual methods
.method public getAccessTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/cache/LocalCache$n;->e:J

    return-wide v0
.end method

.method public getNextInAccessQueue()Lcom/google/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/LocalCache$n;->f:Lcom/google/common/cache/a;

    return-object v0
.end method

.method public getNextInWriteQueue()Lcom/google/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/LocalCache$n;->i:Lcom/google/common/cache/a;

    return-object v0
.end method

.method public getPreviousInAccessQueue()Lcom/google/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/LocalCache$n;->g:Lcom/google/common/cache/a;

    return-object v0
.end method

.method public getPreviousInWriteQueue()Lcom/google/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/LocalCache$n;->j:Lcom/google/common/cache/a;

    return-object v0
.end method

.method public getWriteTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/cache/LocalCache$n;->h:J

    return-wide v0
.end method

.method public setAccessTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/common/cache/LocalCache$n;->e:J

    return-void
.end method

.method public setNextInAccessQueue(Lcom/google/common/cache/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/cache/LocalCache$n;->f:Lcom/google/common/cache/a;

    return-void
.end method

.method public setNextInWriteQueue(Lcom/google/common/cache/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/cache/LocalCache$n;->i:Lcom/google/common/cache/a;

    return-void
.end method

.method public setPreviousInAccessQueue(Lcom/google/common/cache/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/cache/LocalCache$n;->g:Lcom/google/common/cache/a;

    return-void
.end method

.method public setPreviousInWriteQueue(Lcom/google/common/cache/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/cache/LocalCache$n;->j:Lcom/google/common/cache/a;

    return-void
.end method

.method public setWriteTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/common/cache/LocalCache$n;->h:J

    return-void
.end method

.class public abstract Lcom/google/common/util/concurrent/e;
.super Ljc0;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/util/concurrent/e$a;
    }
.end annotation


# direct methods
.method public static a(Lik0;Lfa0;Ljava/util/concurrent/Executor;)V
    .locals 1

    .line 1
    invoke-static {p1}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/common/util/concurrent/e$a;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/e$a;-><init>(Ljava/util/concurrent/Future;Lfa0;)V

    invoke-interface {p0, v0, p2}, Lik0;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public static b(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 2

    .line 1
    invoke-interface {p0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    const-string v1, "Future was expected to be done: %s"

    invoke-static {v0, v1, p0}, Li71;->B(ZLjava/lang/String;Ljava/lang/Object;)V

    invoke-static {p0}, Lt02;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/lang/Throwable;)Lik0;
    .locals 1

    .line 1
    invoke-static {p0}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/common/util/concurrent/f$a;

    invoke-direct {v0, p0}, Lcom/google/common/util/concurrent/f$a;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static d(Ljava/lang/Object;)Lik0;
    .locals 1

    .line 1
    if-nez p0, :cond_0

    sget-object p0, Lcom/google/common/util/concurrent/f;->b:Lik0;

    return-object p0

    :cond_0
    new-instance v0, Lcom/google/common/util/concurrent/f;

    invoke-direct {v0, p0}, Lcom/google/common/util/concurrent/f;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static e(Lik0;Lm90;Ljava/util/concurrent/Executor;)Lik0;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/google/common/util/concurrent/a;->n(Lik0;Lm90;Ljava/util/concurrent/Executor;)Lik0;

    move-result-object p0

    return-object p0
.end method

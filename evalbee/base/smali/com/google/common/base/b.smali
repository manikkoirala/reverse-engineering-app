.class public abstract Lcom/google/common/base/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/base/b$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final b:Ly41;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/common/base/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/common/base/b;->a:Ljava/util/logging/Logger;

    invoke-static {}, Lcom/google/common/base/b;->d()Ly41;

    move-result-object v0

    sput-object v0, Lcom/google/common/base/b;->b:Ly41;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lhi;
    .locals 1

    .line 1
    invoke-static {p0}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/common/base/b;->b:Ly41;

    invoke-interface {v0, p0}, Ly41;->a(Ljava/lang/String;)Lhi;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/google/common/base/b;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    :cond_0
    return-object p0
.end method

.method public static c(D)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    filled-new-array {p0}, [Ljava/lang/Object;

    move-result-object p0

    const-string p1, "%.4g"

    invoke-static {v0, p1, p0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static d()Ly41;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/common/base/b$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/common/base/b$b;-><init>(Lcom/google/common/base/b$a;)V

    return-object v0
.end method

.method public static e()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/common/base/b;->b:Ly41;

    invoke-interface {v0}, Ly41;->b()Z

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/String;)Z
    .locals 0

    .line 1
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static g()J
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

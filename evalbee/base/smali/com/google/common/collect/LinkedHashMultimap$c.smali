.class public interface abstract Lcom/google/common/collect/LinkedHashMultimap$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/common/collect/LinkedHashMultimap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "c"
.end annotation


# virtual methods
.method public abstract getPredecessorInValueSet()Lcom/google/common/collect/LinkedHashMultimap$c;
.end method

.method public abstract getSuccessorInValueSet()Lcom/google/common/collect/LinkedHashMultimap$c;
.end method

.method public abstract setPredecessorInValueSet(Lcom/google/common/collect/LinkedHashMultimap$c;)V
.end method

.method public abstract setSuccessorInValueSet(Lcom/google/common/collect/LinkedHashMultimap$c;)V
.end method

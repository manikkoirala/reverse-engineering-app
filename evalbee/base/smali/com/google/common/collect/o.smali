.class public interface abstract Lcom/google/common/collect/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/j;
.implements Lro1;


# virtual methods
.method public abstract comparator()Ljava/util/Comparator;
.end method

.method public abstract descendingMultiset()Lcom/google/common/collect/o;
.end method

.method public abstract elementSet()Ljava/util/NavigableSet;
.end method

.method public abstract entrySet()Ljava/util/Set;
.end method

.method public abstract firstEntry()Lcom/google/common/collect/j$a;
.end method

.method public abstract headMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;
.end method

.method public abstract lastEntry()Lcom/google/common/collect/j$a;
.end method

.method public abstract pollFirstEntry()Lcom/google/common/collect/j$a;
.end method

.method public abstract pollLastEntry()Lcom/google/common/collect/j$a;
.end method

.method public abstract subMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;
.end method

.method public abstract tailMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;
.end method

.class public abstract Lcom/google/common/collect/g;
.super Lcom/google/common/collect/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/o;


# instance fields
.field public transient a:Ljava/util/Comparator;

.field public transient b:Ljava/util/NavigableSet;

.field public transient c:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/h;-><init>()V

    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1

    .line 1
    new-instance v0, Lcom/google/common/collect/g$a;

    invoke-direct {v0, p0}, Lcom/google/common/collect/g$a;-><init>(Lcom/google/common/collect/g;)V

    return-object v0
.end method

.method public abstract c()Ljava/util/Iterator;
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/g;->a:Ljava/util/Comparator;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/o;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Ordering;->from(Ljava/util/Comparator;)Lcom/google/common/collect/Ordering;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/Ordering;->reverse()Lcom/google/common/collect/Ordering;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/g;->a:Ljava/util/Comparator;

    :cond_0
    return-object v0
.end method

.method public delegate()Lcom/google/common/collect/j;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic delegate()Ljava/lang/Object;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/google/common/collect/g;->delegate()Lcom/google/common/collect/j;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic delegate()Ljava/util/Collection;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/google/common/collect/g;->delegate()Lcom/google/common/collect/j;

    move-result-object v0

    return-object v0
.end method

.method public descendingMultiset()Lcom/google/common/collect/o;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    return-object v0
.end method

.method public elementSet()Ljava/util/NavigableSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/common/collect/g;->b:Ljava/util/NavigableSet;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/common/collect/p$b;

    invoke-direct {v0, p0}, Lcom/google/common/collect/p$b;-><init>(Lcom/google/common/collect/o;)V

    iput-object v0, p0, Lcom/google/common/collect/g;->b:Ljava/util/NavigableSet;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic elementSet()Ljava/util/Set;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lcom/google/common/collect/g;->elementSet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/g;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/g;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/g;->c:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public firstEntry()Lcom/google/common/collect/j$a;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/o;->lastEntry()Lcom/google/common/collect/j$a;

    move-result-object v0

    return-object v0
.end method

.method public abstract g()Lcom/google/common/collect/o;
.end method

.method public headMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/o;->tailMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/common/collect/o;->descendingMultiset()Lcom/google/common/collect/o;

    move-result-object p1

    return-object p1
.end method

.method public lastEntry()Lcom/google/common/collect/j$a;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/o;->firstEntry()Lcom/google/common/collect/j$a;

    move-result-object v0

    return-object v0
.end method

.method public pollFirstEntry()Lcom/google/common/collect/j$a;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/o;->pollLastEntry()Lcom/google/common/collect/j$a;

    move-result-object v0

    return-object v0
.end method

.method public pollLastEntry()Lcom/google/common/collect/j$a;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/o;->pollFirstEntry()Lcom/google/common/collect/j$a;

    move-result-object v0

    return-object v0
.end method

.method public subMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0, p3, p4, p1, p2}, Lcom/google/common/collect/o;->subMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/common/collect/o;->descendingMultiset()Lcom/google/common/collect/o;

    move-result-object p1

    return-object p1
.end method

.method public tailMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->g()Lcom/google/common/collect/o;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/o;->headMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/o;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/common/collect/o;->descendingMultiset()Lcom/google/common/collect/o;

    move-result-object p1

    return-object p1
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lt70;->standardToArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lt70;->standardToArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/g;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

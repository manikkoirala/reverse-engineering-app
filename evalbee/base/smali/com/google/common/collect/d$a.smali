.class public Lcom/google/common/collect/d$a;
.super Lcom/google/common/collect/g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/common/collect/d;->createDescendingMultiset()Lcom/google/common/collect/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/common/collect/d;


# direct methods
.method public constructor <init>(Lcom/google/common/collect/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/d$a;->d:Lcom/google/common/collect/d;

    invoke-direct {p0}, Lcom/google/common/collect/g;-><init>()V

    return-void
.end method


# virtual methods
.method public c()Ljava/util/Iterator;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/common/collect/d$a;->d:Lcom/google/common/collect/d;

    invoke-virtual {v0}, Lcom/google/common/collect/d;->descendingEntryIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/google/common/collect/o;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/common/collect/d$a;->d:Lcom/google/common/collect/d;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/d$a;->d:Lcom/google/common/collect/d;

    invoke-virtual {v0}, Lcom/google/common/collect/d;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lcom/google/common/primitives/SignedBytes;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/primitives/SignedBytes$LexicographicalComparator;
    }
.end annotation


# direct methods
.method public static a(J)B
    .locals 3

    .line 1
    long-to-int v0, p0

    int-to-byte v0, v0

    int-to-long v1, v0

    cmp-long v1, v1, p0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "Out of range: %s"

    invoke-static {v1, v2, p0, p1}, Li71;->j(ZLjava/lang/String;J)V

    return v0
.end method

.method public static b(BB)I
    .locals 0

    .line 1
    sub-int/2addr p0, p1

    return p0
.end method

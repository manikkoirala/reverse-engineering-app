.class public Lcom/android/volley/c;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/concurrent/BlockingQueue;

.field public final b:Lqy0;

.field public final c:Lcom/android/volley/a;

.field public final d:Lre1;

.field public volatile e:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Lqy0;Lcom/android/volley/a;Lre1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/volley/c;->e:Z

    iput-object p1, p0, Lcom/android/volley/c;->a:Ljava/util/concurrent/BlockingQueue;

    iput-object p2, p0, Lcom/android/volley/c;->b:Lqy0;

    iput-object p3, p0, Lcom/android/volley/c;->c:Lcom/android/volley/a;

    iput-object p4, p0, Lcom/android/volley/c;->d:Lre1;

    return-void
.end method

.method private c()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/android/volley/c;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/Request;

    invoke-virtual {p0, v0}, Lcom/android/volley/c;->d(Lcom/android/volley/Request;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/Request;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/android/volley/Request;->y()I

    move-result p1

    invoke-static {p1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    return-void
.end method

.method public final b(Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    .locals 1

    .line 1
    invoke-virtual {p1, p2}, Lcom/android/volley/Request;->F(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    move-result-object p2

    iget-object v0, p0, Lcom/android/volley/c;->d:Lre1;

    invoke-interface {v0, p1, p2}, Lre1;->c(Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V

    return-void
.end method

.method public d(Lcom/android/volley/Request;)V
    .locals 8

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/android/volley/Request;->H(I)V

    const/4 v2, 0x4

    :try_start_0
    const-string v3, "network-queue-take"

    invoke-virtual {p1, v3}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/volley/Request;->B()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "network-discard-cancelled"

    invoke-virtual {p1, v3}, Lcom/android/volley/Request;->j(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/volley/Request;->D()V
    :try_end_0
    .catch Lcom/android/volley/VolleyError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v2}, Lcom/android/volley/Request;->H(I)V

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/android/volley/c;->a(Lcom/android/volley/Request;)V

    iget-object v3, p0, Lcom/android/volley/c;->b:Lqy0;

    invoke-interface {v3, p1}, Lqy0;->a(Lcom/android/volley/Request;)Lyy0;

    move-result-object v3

    const-string v4, "network-http-complete"

    invoke-virtual {p1, v4}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    iget-boolean v4, v3, Lyy0;->e:Z

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/android/volley/Request;->A()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v3, "not-modified"

    invoke-virtual {p1, v3}, Lcom/android/volley/Request;->j(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/volley/Request;->D()V
    :try_end_1
    .catch Lcom/android/volley/VolleyError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p1, v2}, Lcom/android/volley/Request;->H(I)V

    return-void

    :cond_1
    :try_start_2
    invoke-virtual {p1, v3}, Lcom/android/volley/Request;->G(Lyy0;)Lcom/android/volley/d;

    move-result-object v3

    const-string v4, "network-parse-complete"

    invoke-virtual {p1, v4}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/volley/Request;->N()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v3, Lcom/android/volley/d;->b:Lcom/android/volley/a$a;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/volley/c;->c:Lcom/android/volley/a;

    invoke-virtual {p1}, Lcom/android/volley/Request;->n()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v3, Lcom/android/volley/d;->b:Lcom/android/volley/a$a;

    invoke-interface {v4, v5, v6}, Lcom/android/volley/a;->b(Ljava/lang/String;Lcom/android/volley/a$a;)V

    const-string v4, "network-cache-written"

    invoke-virtual {p1, v4}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/android/volley/Request;->C()V

    iget-object v4, p0, Lcom/android/volley/c;->d:Lre1;

    invoke-interface {v4, p1, v3}, Lre1;->a(Lcom/android/volley/Request;Lcom/android/volley/d;)V

    invoke-virtual {p1, v3}, Lcom/android/volley/Request;->E(Lcom/android/volley/d;)V
    :try_end_2
    .catch Lcom/android/volley/VolleyError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v3

    :try_start_3
    const-string v4, "Unhandled exception %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/android/volley/e;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v4, Lcom/android/volley/VolleyError;

    invoke-direct {v4, v3}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/Throwable;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Lcom/android/volley/VolleyError;->setNetworkTimeMs(J)V

    iget-object v0, p0, Lcom/android/volley/c;->d:Lre1;

    invoke-interface {v0, p1, v4}, Lre1;->c(Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V

    :goto_0
    invoke-virtual {p1}, Lcom/android/volley/Request;->D()V

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Lcom/android/volley/VolleyError;->setNetworkTimeMs(J)V

    invoke-virtual {p0, p1, v3}, Lcom/android/volley/c;->b(Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :goto_1
    invoke-virtual {p1, v2}, Lcom/android/volley/Request;->H(I)V

    return-void

    :goto_2
    invoke-virtual {p1, v2}, Lcom/android/volley/Request;->H(I)V

    throw v0
.end method

.method public e()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/volley/c;->e:Z

    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method public run()V
    .locals 2

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/android/volley/c;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-boolean v0, p0, Lcom/android/volley/c;->e:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it"

    invoke-static {v1, v0}, Lcom/android/volley/e;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

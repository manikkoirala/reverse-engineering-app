.class public final Lcom/fasterxml/aalto/dom/DOMWriterImpl;
.super Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;
.source "SourceFile"


# instance fields
.field protected _autoNsSeq:[I

.field protected _automaticNsPrefix:Ljava/lang/String;

.field protected final _config:Lcom/fasterxml/aalto/out/WriterConfig;

.field protected _currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

.field protected _openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

.field protected _suggestedDefNs:Ljava/lang/String;

.field _suggestedPrefixes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/fasterxml/aalto/out/WriterConfig;Lorg/w3c/dom/Node;)V
    .locals 2

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willRepairNamespaces()Z

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, p2, v1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;-><init>(Lorg/w3c/dom/Node;ZZ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedDefNs:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->getAutomaticNsPrefix()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_automaticNsPrefix:Ljava/lang/String;

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result p1

    if-eq p1, v1, :cond_2

    const/16 v1, 0x9

    if-eq p1, v1, :cond_1

    const/16 v1, 0xb

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljavax/xml/stream/XMLStreamException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can not create an XMLStreamWriter for a DOM node of type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    invoke-static {}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->createRoot()Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->createRoot()Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object p1

    check-cast p2, Lorg/w3c/dom/Element;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->createChild(Lorg/w3c/dom/Element;)Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    :goto_1
    return-void
.end method

.method public static createFrom(Lcom/fasterxml/aalto/out/WriterConfig;Ljavax/xml/transform/dom/DOMResult;)Lcom/fasterxml/aalto/dom/DOMWriterImpl;
    .locals 1

    invoke-virtual {p1}, Ljavax/xml/transform/dom/DOMResult;->getNode()Lorg/w3c/dom/Node;

    move-result-object p1

    new-instance v0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;-><init>(Lcom/fasterxml/aalto/out/WriterConfig;Lorg/w3c/dom/Node;)V

    return-object v0
.end method

.method private final validateElemPrefix(Ljava/lang/String;Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p3, p1, p2, v1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->isPrefixValid(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result p2

    if-ne p2, v1, :cond_1

    return-object p1

    :cond_1
    return-object v0

    :cond_2
    :goto_0
    invoke-virtual {p3}, Lcom/fasterxml/aalto/dom/OutputElementBase;->getDefaultNsUri()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    return-object v0

    :cond_4
    :goto_1
    const-string p1, ""

    return-object p1
.end method


# virtual methods
.method public appendLeaf(Lorg/w3c/dom/Node;)V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->appendNode(Lorg/w3c/dom/Node;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    return-void
.end method

.method public createStartElem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsAware:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    const-string p1, "Can not specify non-empty uri/prefix in non-namespace mode"

    invoke-static {p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->throwOutputError(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iget-object p2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {p2, p3}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->createAndAttachChild(Lorg/w3c/dom/Element;)Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object p1

    goto/16 :goto_4

    :cond_1
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsRepairing:Z

    const-string v1, ":"

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-direct {p0, p2, p1, v0}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->validateElemPrefix(Ljava/lang/String;Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p2

    if-eqz p2, :cond_a

    iget-object p2, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iget-object v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-interface {v2, p1, p3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object p1

    goto/16 :goto_3

    :cond_2
    if-nez p2, :cond_3

    const-string p2, ""

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {p0, p2, p1, v0}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->generateElemPrefix(Ljava/lang/String;Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_5
    iget-object v1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iget-object v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v2, p1, p3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object p3

    invoke-virtual {v1, p3}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->createAndAttachChild(Lorg/w3c/dom/Element;)Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object p3

    iput-object p3, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    if-eqz v0, :cond_6

    invoke-virtual {p0, p2, p1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3, p2, p1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->addPrefix(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeDefaultNamespace(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->setDefaultNsUri(Ljava/lang/String;)V

    :goto_1
    move-object p1, p3

    goto :goto_4

    :cond_7
    if-nez p2, :cond_9

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_9

    iget-object p2, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    if-nez p2, :cond_8

    const/4 p2, 0x0

    goto :goto_2

    :cond_8
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    :goto_2
    if-nez p2, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can not find prefix for namespace \""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->throwOutputError(Ljava/lang/String;)V

    :cond_9
    if-eqz p2, :cond_a

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_a
    iget-object p2, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1, p3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object p1

    :goto_3
    invoke-virtual {p2, p1}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->createAndAttachChild(Lorg/w3c/dom/Element;)Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object p1

    :goto_4
    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    if-nez p4, :cond_b

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    :cond_b
    return-void
.end method

.method public final findElemPrefix(Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {p2, p1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    invoke-virtual {p2}, Lcom/fasterxml/aalto/dom/OutputElementBase;->getDefaultNsUri()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method

.method public final findOrCreateAttrPrefix(Ljava/lang/String;Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_a

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_2

    invoke-virtual {p3, p1, p2, v1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->isPrefixValid(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    if-ne v3, v2, :cond_1

    return-object p1

    :cond_1
    if-nez v3, :cond_2

    invoke-virtual {p3, p1, p2}, Lcom/fasterxml/aalto/dom/OutputElementBase;->addPrefix(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    :cond_2
    invoke-virtual {p3, p2}, Lcom/fasterxml/aalto/dom/OutputElementBase;->getExplicitPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    return-object v3

    :cond_3
    if-eqz p1, :cond_4

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    if-eqz p1, :cond_5

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_5
    move-object p1, v3

    :goto_0
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p3, p1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    goto :goto_1

    :cond_6
    move-object v0, p1

    :cond_7
    :goto_1
    if-nez v0, :cond_9

    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    if-nez p1, :cond_8

    new-array p1, v2, [I

    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    aput v2, p1, v1

    :cond_8
    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_automaticNsPrefix:Ljava/lang/String;

    iget-object v1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    invoke-virtual {p1, v0, p2, v1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->generateMapping(Ljava/lang/String;Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v0

    :cond_9
    invoke-virtual {p3, v0, p2}, Lcom/fasterxml/aalto/dom/OutputElementBase;->addPrefix(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_2
    return-object v0
.end method

.method public final generateElemPrefix(Ljava/lang/String;Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    :cond_0
    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedDefNs:Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    move-object p1, v0

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    if-nez p1, :cond_2

    const/4 p1, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    :goto_0
    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    if-nez p1, :cond_3

    const/4 p1, 0x1

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    :cond_3
    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_automaticNsPrefix:Ljava/lang/String;

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_autoNsSeq:[I

    invoke-virtual {p3, p1, p2, v0}, Lcom/fasterxml/aalto/dom/OutputElementBase;->generateMapping(Ljava/lang/String;Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object p1

    :cond_4
    :goto_1
    return-object p1

    :cond_5
    :goto_2
    return-object v0
.end method

.method public getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 1

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsAware:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/codehaus/stax2/ri/EmptyNamespaceContext;->getInstance()Lorg/codehaus/stax2/ri/EmptyNamespaceContext;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    return-object v0
.end method

.method public getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsAware:Z

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsContext:Ljavax/xml/namespace/NamespaceContext;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljavax/xml/namespace/NamespaceContext;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/dom/OutputElementBase;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/aalto/out/WriterConfig;->getProperty(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WriterConfig;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public outputAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    if-eqz v0, :cond_4

    iget-boolean v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsAware:Z

    const-string v2, ":"

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsRepairing:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, p2, p1, v0}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->findOrCreateAttrPrefix(Ljava/lang/String;Ljava/lang/String;Lcom/fasterxml/aalto/dom/DOMOutputElement;)Ljava/lang/String;

    move-result-object p2

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_1
    iget-object p2, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {p2, p1, p3, p4}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_3
    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {p1, p3, p4}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No currently open START_ELEMENT, cannot write attribute"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDefaultNamespace(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedDefNs:Ljava/lang/String;

    return-void
.end method

.method public setPrefix(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->setDefaultNamespace(Ljava/lang/String;)V

    return-void

    :cond_0
    if-eqz p2, :cond_7

    const-string v0, "xml"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "http://www.w3.org/XML/1998/namespace"

    if-eqz v0, :cond_1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trying to redeclare prefix \'xml\' from its default URI \'http://www.w3.org/XML/1998/namespace\' to \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->throwOutputError(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v0, "xmlns"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "http://www.w3.org/2000/xmlns/"

    if-eqz v0, :cond_3

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "Trying to declare prefix \'xmlns\' (illegal as per NS 1.1 #4)"

    invoke-static {p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->throwOutputError(Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trying to bind URI \'http://www.w3.org/XML/1998/namespace to prefix \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\" (can only bind to \'xml\')"

    goto :goto_0

    :cond_4
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trying to bind URI \'http://www.w3.org/2000/xmlns/ to prefix \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\" (can not be explicitly bound)"

    goto :goto_0

    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    :cond_6
    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_suggestedPrefixes:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_7
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Can not pass null \'uri\' value"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Can not pass null \'prefix\' value"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/WriterConfig;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public writeAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, p1, p2}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->outputAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->outputAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-virtual {p0, p2, p1, p3, p4}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->outputAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iget-object p1, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    if-nez p1, :cond_0

    const-string p1, "writeDTD()"

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->reportUnsupported(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Operation only allowed to the document before adding root element"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeDefaultNamespace(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->setDefaultNamespace(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    const-string v1, "http://www.w3.org/2000/xmlns/"

    const-string v2, "xmlns"

    invoke-virtual {v0, v1, v2, p1}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->addAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No currently open START_ELEMENT, cannot write attribute"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeEmptyElement(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeEmptyElement(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeEmptyElement(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->createStartElem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public writeEmptyElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 3
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p3, p1, p2, v0}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->createStartElem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public writeEndDocument()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    return-void
.end method

.method public writeEndElement()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->isRoot()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_openElement:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/dom/DOMOutputElement;->getParent()Lcom/fasterxml/aalto/dom/DOMOutputElement;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No open start element to close"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public writeNamespace(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsAware:Z

    if-nez v0, :cond_1

    const-string v0, "Can not write namespaces with non-namespace writer."

    invoke-static {v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->throwOutputError(Ljava/lang/String;)V

    :cond_1
    const-string v0, "http://www.w3.org/2000/xmlns/"

    const-string v1, "xmlns"

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->outputAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->_currElem:Lcom/fasterxml/aalto/dom/DOMOutputElement;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/dom/OutputElementBase;->addPrefix(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    :goto_0
    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeDefaultNamespace(Ljava/lang/String;)V

    return-void
.end method

.method public writeStartElement(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->writeStartElement(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartElement(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->createStartElem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public writeStartElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, p3, p1, p2, v0}, Lcom/fasterxml/aalto/dom/DOMWriterImpl;->createStartElem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

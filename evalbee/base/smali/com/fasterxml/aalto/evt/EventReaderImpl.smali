.class public final Lcom/fasterxml/aalto/evt/EventReaderImpl;
.super Lorg/codehaus/stax2/ri/Stax2EventReaderImpl;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljavax/xml/stream/util/XMLEventAllocator;Lorg/codehaus/stax2/XMLStreamReader2;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/codehaus/stax2/ri/Stax2EventReaderImpl;-><init>(Ljavax/xml/stream/util/XMLEventAllocator;Lorg/codehaus/stax2/XMLStreamReader2;)V

    return-void
.end method


# virtual methods
.method public getErrorDesc(II)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2EventReaderImpl;->getStreamReader()Ljavax/xml/stream/XMLStreamReader;

    move-result-object v0

    check-cast v0, Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/XMLStreamReader2;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public reportProblem(Ljava/lang/String;Ljavax/xml/stream/Location;)V
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/impl/StreamExceptionBase;

    invoke-direct {v0, p1, p2}, Lcom/fasterxml/aalto/impl/StreamExceptionBase;-><init>(Ljava/lang/String;Ljavax/xml/stream/Location;)V

    throw v0
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2EventReaderImpl;->getStreamReader()Ljavax/xml/stream/XMLStreamReader;

    move-result-object v0

    check-cast v0, Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/XMLStreamReader2;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

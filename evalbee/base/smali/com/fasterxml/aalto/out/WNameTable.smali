.class public final Lcom/fasterxml/aalto/out/WNameTable;
.super Lcom/fasterxml/aalto/util/NameTable;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fasterxml/aalto/out/WNameTable$Bucket;
    }
.end annotation


# static fields
.field static final INITIAL_COLLISION_LEN:I = 0x20

.field static final LAST_VALID_BUCKET:I = 0xfe

.field static final MIN_HASH_SIZE:I = 0x10


# instance fields
.field private mCollCount:I

.field private mCollEnd:I

.field private mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

.field private mCollListShared:Z

.field private mCount:I

.field private mMainHash:[I

.field private mMainHashMask:I

.field private mMainHashShared:Z

.field private mMainNames:[Lcom/fasterxml/aalto/out/WName;

.field private mMainNamesShared:Z

.field final mNameFactory:Lcom/fasterxml/aalto/out/WNameFactory;

.field private transient mNeedRehash:Z

.field final mParent:Lcom/fasterxml/aalto/out/WNameTable;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/fasterxml/aalto/util/NameTable;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNameFactory:Lcom/fasterxml/aalto/out/WNameFactory;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mParent:Lcom/fasterxml/aalto/out/WNameTable;

    const/16 v1, 0x10

    if-ge p1, v1, :cond_1

    :cond_0
    move p1, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, p1, -0x1

    and-int/2addr v2, p1

    if-eqz v2, :cond_2

    :goto_0
    if-ge v1, p1, :cond_0

    add-int/2addr v1, v1

    goto :goto_0

    :cond_2
    :goto_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashShared:Z

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNamesShared:Z

    add-int/lit8 v2, p1, -0x1

    iput v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    new-array v2, p1, [I

    iput-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    new-array p1, p1, [Lcom/fasterxml/aalto/out/WName;

    iput-object p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollListShared:Z

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNeedRehash:Z

    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/aalto/out/WNameTable;Lcom/fasterxml/aalto/out/WNameFactory;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/fasterxml/aalto/util/NameTable;-><init>()V

    iput-object p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mParent:Lcom/fasterxml/aalto/out/WNameTable;

    iput-object p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNameFactory:Lcom/fasterxml/aalto/out/WNameFactory;

    iget p2, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    iput p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    iget p2, p1, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    iput p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    iget-object p2, p1, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    iput-object p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    iget-object p2, p1, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    iput-object p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    iget-object p2, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput-object p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iget p2, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    iput p2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    iget p1, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    iput p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNeedRehash:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashShared:Z

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNamesShared:Z

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollListShared:Z

    return-void
.end method

.method private addSymbol(Lcom/fasterxml/aalto/out/WName;)V
    .locals 6

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashShared:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->unshareMain()V

    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNeedRehash:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->rehash()V

    :cond_1
    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    and-int/2addr v1, v0

    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aget-object v3, v3, v1

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    shl-int/lit8 v0, v0, 0x8

    aput v0, v3, v1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNamesShared:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->unshareNames()V

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aput-object p1, v0, v1

    goto :goto_2

    :cond_3
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollListShared:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->unshareCollision()V

    :cond_4
    iget v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    aget v0, v0, v1

    and-int/lit16 v3, v0, 0xff

    if-nez v3, :cond_7

    iget v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    const/16 v4, 0xfe

    if-gt v3, v4, :cond_5

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    iget-object v4, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    array-length v4, v4

    if-lt v3, v4, :cond_6

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->expandCollision()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->findBestBucket()I

    move-result v3

    :cond_6
    :goto_0
    iget-object v4, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    and-int/lit16 v0, v0, -0x100

    add-int/lit8 v5, v3, 0x1

    or-int/2addr v0, v5

    aput v0, v4, v1

    goto :goto_1

    :cond_7
    add-int/lit8 v3, v3, -0x1

    :goto_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    new-instance v1, Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    aget-object v4, v0, v3

    invoke-direct {v1, p1, v4}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;-><init>(Lcom/fasterxml/aalto/out/WName;Lcom/fasterxml/aalto/out/WNameTable$Bucket;)V

    aput-object v1, v0, v3

    :goto_2
    iget-object p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    array-length p1, p1

    iget v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    shr-int/lit8 v1, p1, 0x1

    if-le v0, v1, :cond_9

    shr-int/lit8 v1, p1, 0x2

    sub-int/2addr p1, v1

    if-le v0, p1, :cond_8

    :goto_3
    iput-boolean v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNeedRehash:Z

    goto :goto_4

    :cond_8
    iget p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    if-lt p1, v1, :cond_9

    goto :goto_3

    :cond_9
    :goto_4
    return-void
.end method

.method private expandCollision()V
    .locals 4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    array-length v1, v0

    add-int v2, v1, v1

    new-array v2, v2, [Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method private findBestBucket()I
    .locals 6

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    const v2, 0x7fffffff

    const/4 v3, -0x1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_2

    aget-object v5, v0, v4

    invoke-virtual {v5}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->length()I

    move-result v5

    if-ge v5, v2, :cond_1

    const/4 v2, 0x1

    if-ne v5, v2, :cond_0

    return v4

    :cond_0
    move v3, v4

    move v2, v5

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return v3
.end method

.method private declared-synchronized mergeFromChild(Lcom/fasterxml/aalto/out/WNameTable;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v0, v1, :cond_0

    monitor-exit p0

    const/4 p1, 0x0

    return p1

    :cond_0
    :try_start_1
    iput v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    iget v0, p1, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    iput v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    iget-object v0, p1, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    iget-object v0, p1, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    iget-object v0, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iget v0, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    iput v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    iget p1, p1, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    iput p1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private rehash()V
    .locals 12

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNeedRehash:Z

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNamesShared:Z

    iget-object v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    array-length v1, v1

    add-int v2, v1, v1

    new-array v3, v2, [I

    iput-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    add-int/lit8 v3, v2, -0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    new-array v2, v2, [Lcom/fasterxml/aalto/out/WName;

    iput-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    move v2, v0

    move v4, v2

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v5}, Lcom/fasterxml/aalto/out/WName;->hashCode()I

    move-result v6

    iget v7, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    and-int/2addr v7, v6

    iget-object v8, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aput-object v5, v8, v7

    iget-object v5, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    shl-int/lit8 v6, v6, 0x8

    aput v6, v5, v7

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    if-nez v1, :cond_2

    return-void

    :cond_2
    iput v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    iput v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollListShared:Z

    iget-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    array-length v3, v2

    new-array v3, v3, [Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    :goto_1
    if-ge v0, v1, :cond_8

    aget-object v3, v2, v0

    :goto_2
    if-eqz v3, :cond_7

    add-int/lit8 v4, v4, 0x1

    iget-object v5, v3, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->mName:Lcom/fasterxml/aalto/out/WName;

    invoke-virtual {v5}, Lcom/fasterxml/aalto/out/WName;->hashCode()I

    move-result v6

    iget v7, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    and-int/2addr v7, v6

    iget-object v8, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    aget v9, v8, v7

    iget-object v10, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aget-object v11, v10, v7

    if-nez v11, :cond_3

    shl-int/lit8 v6, v6, 0x8

    aput v6, v8, v7

    aput-object v5, v10, v7

    goto :goto_5

    :cond_3
    iget v6, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    and-int/lit16 v6, v9, 0xff

    if-nez v6, :cond_6

    iget v6, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    const/16 v8, 0xfe

    if-gt v6, v8, :cond_4

    add-int/lit8 v8, v6, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    iget-object v8, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    array-length v8, v8

    if-lt v6, v8, :cond_5

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->expandCollision()V

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameTable;->findBestBucket()I

    move-result v6

    :cond_5
    :goto_3
    iget-object v8, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    and-int/lit16 v9, v9, -0x100

    add-int/lit8 v10, v6, 0x1

    or-int/2addr v9, v10

    aput v9, v8, v7

    goto :goto_4

    :cond_6
    add-int/lit8 v6, v6, -0x1

    :goto_4
    iget-object v7, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    new-instance v8, Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    aget-object v9, v7, v6

    invoke-direct {v8, v5, v9}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;-><init>(Lcom/fasterxml/aalto/out/WName;Lcom/fasterxml/aalto/out/WNameTable$Bucket;)V

    aput-object v8, v7, v6

    :goto_5
    iget-object v3, v3, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    goto :goto_2

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    if-ne v4, v0, :cond_9

    return-void

    :cond_9
    new-instance v0, Ljava/lang/Error;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Internal error: count after rehash "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "; should be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private unshareCollision()V
    .locals 4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/16 v0, 0x20

    new-array v0, v0, [Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    goto :goto_0

    :cond_0
    array-length v2, v0

    new-array v3, v2, [Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    iput-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    invoke-static {v0, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollListShared:Z

    return-void
.end method

.method private unshareMain()V
    .locals 4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    array-length v1, v0

    new-array v2, v1, [I

    iput-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-boolean v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashShared:Z

    return-void
.end method

.method private unshareNames()V
    .locals 4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    array-length v1, v0

    new-array v2, v1, [Lcom/fasterxml/aalto/out/WName;

    iput-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-boolean v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNamesShared:Z

    return-void
.end method


# virtual methods
.method public declared-synchronized createChild(Lcom/fasterxml/aalto/out/WNameFactory;)Lcom/fasterxml/aalto/out/WNameTable;
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/fasterxml/aalto/out/WNameTable;

    invoke-direct {v0, p0, p1}, Lcom/fasterxml/aalto/out/WNameTable;-><init>(Lcom/fasterxml/aalto/out/WNameTable;Lcom/fasterxml/aalto/out/WNameFactory;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    and-int/2addr v1, v0

    iget-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    aget v2, v2, v1

    shr-int/lit8 v3, v2, 0x8

    xor-int/2addr v0, v3

    shl-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WName;->hasName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    if-eqz v2, :cond_1

    and-int/lit16 v0, v2, 0xff

    if-lez v0, :cond_1

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    aget-object v0, v1, v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->find(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNameFactory:Lcom/fasterxml/aalto/out/WNameFactory;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WNameFactory;->constructName(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/WNameTable;->addSymbol(Lcom/fasterxml/aalto/out/WName;)V

    return-object p1
.end method

.method public findSymbol(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
    .locals 4

    .line 2
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashMask:I

    and-int/2addr v1, v0

    iget-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    aget v2, v2, v1

    shr-int/lit8 v3, v2, 0x8

    xor-int/2addr v0, v3

    shl-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/WName;->hasName(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    if-eqz v2, :cond_1

    and-int/lit16 v0, v2, 0xff

    if-lez v0, :cond_1

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    aget-object v0, v1, v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->find(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mNameFactory:Lcom/fasterxml/aalto/out/WNameFactory;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/WNameFactory;->constructName(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/WNameTable;->addSymbol(Lcom/fasterxml/aalto/out/WName;)V

    return-object p1
.end method

.method public markAsShared()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashShared:Z

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNamesShared:Z

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollListShared:Z

    return-void
.end method

.method public maybeDirty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHashShared:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public mergeToParent()Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mParent:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-direct {v0, p0}, Lcom/fasterxml/aalto/out/WNameTable;->mergeFromChild(Lcom/fasterxml/aalto/out/WNameTable;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/WNameTable;->markAsShared()V

    return v0
.end method

.method public nuke()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    return-void
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    return v0
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[WNameTable, size: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    array-length v3, v3

    if-ge v2, v3, :cond_1

    const-string v3, "\n#"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ": 0x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    aget v3, v3, v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " == "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainNames:[Lcom/fasterxml/aalto/out/WName;

    aget-object v3, v3, v2

    if-nez v3, :cond_0

    const-string v3, "null"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_0
    const/16 v4, 0x22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/fasterxml/aalto/out/WName;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "\nSpill("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "):"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    iget v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    aget-object v2, v2, v1

    const-string v3, "\nsp#"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->toDebugString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[WNameTable, size: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mMainHash:[I

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " coll; avg length: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    const/4 v2, 0x0

    :goto_0
    iget v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollEnd:I

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCollList:[Lcom/fasterxml/aalto/out/WNameTable$Bucket;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/fasterxml/aalto/out/WNameTable$Bucket;->length()I

    move-result v3

    const/4 v4, 0x1

    :goto_1
    if-gt v4, v3, :cond_0

    add-int/2addr v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/fasterxml/aalto/out/WNameTable;->mCount:I

    if-nez v2, :cond_2

    const-wide/16 v1, 0x0

    goto :goto_2

    :cond_2
    int-to-double v3, v1

    int-to-double v1, v2

    div-double v1, v3, v1

    :goto_2
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lcom/fasterxml/aalto/out/XmlWriter;
.super Lcom/fasterxml/aalto/out/WNameFactory;
.source "SourceFile"


# static fields
.field protected static final ATTR_MIN_ARRAYCOPY:I = 0xc

.field protected static final DEFAULT_COPYBUFFER_LEN:I = 0x200

.field protected static final MIN_ARRAYCOPY:I = 0xc

.field protected static final SURR1_FIRST:I = 0xd800

.field protected static final SURR1_LAST:I = 0xdbff

.field protected static final SURR2_FIRST:I = 0xdc00

.field protected static final SURR2_LAST:I = 0xdfff


# instance fields
.field protected final _cfgNsAware:Z

.field protected final _checkContent:Z

.field protected final _checkNames:Z

.field protected final _config:Lcom/fasterxml/aalto/out/WriterConfig;

.field protected _copyBuffer:[C

.field protected final _copyBufferLen:I

.field protected _locPastChars:I

.field protected _locRowNr:I

.field protected _locRowStartOffset:I

.field protected _xml11:Z


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/out/WriterConfig;)V
    .locals 2

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/WNameFactory;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_xml11:Z

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locPastChars:I

    const/4 v1, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowStartOffset:I

    iput-object p1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    const/16 v0, 0x200

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/out/WriterConfig;->allocMediumCBuffer(I)[C

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v0, v0

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBufferLen:I

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->isNamespaceAware()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_cfgNsAware:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckContent()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_checkContent:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckNames()Z

    move-result p1

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_checkNames:Z

    return-void
.end method

.method public static final guessEncodingBitSize(Lcom/fasterxml/aalto/out/WriterConfig;)I
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/WriterConfig;->getPreferredEncoding()Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x10

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/fasterxml/aalto/util/CharsetNames;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v1, "UTF-8"

    if-ne p0, v1, :cond_1

    return v0

    :cond_1
    const-string v1, "ISO-8859-1"

    const/16 v2, 0x8

    if-ne p0, v1, :cond_2

    return v2

    :cond_2
    const-string v1, "US-ASCII"

    if-ne p0, v1, :cond_3

    const/4 p0, 0x7

    return p0

    :cond_3
    const-string v1, "UTF-16"

    if-eq p0, v1, :cond_5

    const-string v1, "UTF-16BE"

    if-eq p0, v1, :cond_5

    const-string v1, "UTF-16LE"

    if-eq p0, v1, :cond_5

    const-string v1, "UTF-32BE"

    if-eq p0, v1, :cond_5

    const-string v1, "UTF-32LE"

    if-ne p0, v1, :cond_4

    goto :goto_0

    :cond_4
    return v2

    :cond_5
    :goto_0
    return v0
.end method


# virtual methods
.method public abstract _closeTarget(Z)V
.end method

.method public _releaseBuffers()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    iget-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/out/WriterConfig;->freeMediumCBuffer([C)V

    :cond_0
    return-void
.end method

.method public final close(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->flush()V

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->_releaseBuffers()V

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willAutoCloseOutput()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->_closeTarget(Z)V

    return-void
.end method

.method public abstract constructName(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
.end method

.method public abstract constructName(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
.end method

.method public enableXml11()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_xml11:Z

    return-void
.end method

.method public abstract flush()V
.end method

.method public getAbsOffset()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locPastChars:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->getOutputPtr()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getColumn()I
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->getOutputPtr()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowStartOffset:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public abstract getHighestEncodable()I
.end method

.method public abstract getOutputPtr()I
.end method

.method public getRow()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    return v0
.end method

.method public reportFailedEscaping(Ljava/lang/String;I)V
    .locals 1

    const v0, 0xfffe

    if-eq p2, v0, :cond_0

    const v0, 0xffff

    if-eq p2, v0, :cond_0

    const v0, 0xd800

    if-lt p2, v0, :cond_1

    const v0, 0xdfff

    if-gt p2, v0, :cond_1

    :cond_0
    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_1
    const/16 v0, 0x20

    if-ge p2, v0, :cond_3

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->isXml11()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_3
    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_NO_ESCAPING:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    return-void
.end method

.method public reportInvalidChar(I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    const-string v0, "Invalid null character in text to output"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0x20

    if-lt p1, v0, :cond_1

    const/16 v0, 0x7f

    if-lt p1, v0, :cond_3

    const/16 v0, 0x9f

    if-gt p1, v0, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid white space character (0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") in text to output"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_xml11:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " (can only be output using character entity)"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    :cond_3
    const v0, 0x10ffff

    if-le p1, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal unicode character point (0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") to output; max is 0x10FFFF as per RFC 3629"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    :cond_4
    const v0, 0xd800

    if-lt p1, v0, :cond_5

    const v0, 0xdfff

    if-gt p1, v0, :cond_5

    const-string v0, "Illegal surrogate pair -- can only be output via character entities (for current encoding), which are not allowed in this content"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid XML character "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " in text to output"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public reportInvalidEmptyName()V
    .locals 1

    const-string v0, "Empty String is not a valid name (local name, prefix or processing instruction target)"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    return-void
.end method

.method public reportNwfContent(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public reportNwfContent(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 2
    filled-new-array {p2, p3}, [Ljava/lang/Object;

    move-result-object p2

    invoke-static {p1, p2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    return-void
.end method

.method public reportNwfName(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public reportNwfName(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public throwOutputError(Ljava/lang/String;)V
    .locals 1

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    invoke-direct {v0, p1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    filled-new-array {p2}, [Ljava/lang/Object;

    move-result-object p2

    invoke-static {p1, p2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public abstract writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V
.end method

.method public abstract writeAttribute(Lcom/fasterxml/aalto/out/WName;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
.end method

.method public abstract writeAttribute(Lcom/fasterxml/aalto/out/WName;[CII)V
.end method

.method public abstract writeCData(Ljava/lang/String;)I
.end method

.method public abstract writeCData([CII)I
.end method

.method public abstract writeCharacters(Ljava/lang/String;)V
.end method

.method public abstract writeCharacters([CII)V
.end method

.method public abstract writeComment(Ljava/lang/String;)I
.end method

.method public abstract writeDTD(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeDTD(Ljava/lang/String;)V
.end method

.method public abstract writeEndTag(Lcom/fasterxml/aalto/out/WName;)V
.end method

.method public abstract writeEntityReference(Lcom/fasterxml/aalto/out/WName;)V
.end method

.method public abstract writePI(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)I
.end method

.method public abstract writeRaw(Ljava/lang/String;II)V
.end method

.method public abstract writeRaw([CII)V
.end method

.method public abstract writeSpace(Ljava/lang/String;)V
.end method

.method public abstract writeSpace([CII)V
.end method

.method public abstract writeStartTagEmptyEnd()V
.end method

.method public abstract writeStartTagEnd()V
.end method

.method public abstract writeStartTagStart(Lcom/fasterxml/aalto/out/WName;)V
.end method

.method public abstract writeTypedValue(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
.end method

.method public abstract writeXmlDeclaration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

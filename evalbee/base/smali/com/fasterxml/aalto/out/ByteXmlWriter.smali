.class public abstract Lcom/fasterxml/aalto/out/ByteXmlWriter;
.super Lcom/fasterxml/aalto/out/XmlWriter;
.source "SourceFile"


# static fields
.field static final BYTES_CDATA_END:[B

.field static final BYTES_CDATA_START:[B

.field static final BYTES_COMMENT_END:[B

.field static final BYTES_COMMENT_START:[B

.field static final BYTES_XMLDECL_ENCODING:[B

.field static final BYTES_XMLDECL_STANDALONE:[B

.field static final BYTES_XMLDECL_START:[B

.field static final BYTE_A:B = 0x61t

.field static final BYTE_AMP:B = 0x26t

.field static final BYTE_APOS:B = 0x27t

.field static final BYTE_COLON:B = 0x3at

.field static final BYTE_EQ:B = 0x3dt

.field static final BYTE_G:B = 0x67t

.field static final BYTE_GT:B = 0x3et

.field static final BYTE_HASH:B = 0x23t

.field static final BYTE_HYPHEN:B = 0x2dt

.field static final BYTE_L:B = 0x6ct

.field static final BYTE_LBRACKET:B = 0x5bt

.field static final BYTE_LT:B = 0x3ct

.field static final BYTE_M:B = 0x6dt

.field static final BYTE_O:B = 0x6ft

.field static final BYTE_P:B = 0x70t

.field static final BYTE_Q:B = 0x71t

.field static final BYTE_QMARK:B = 0x3ft

.field static final BYTE_QUOT:B = 0x22t

.field static final BYTE_RBRACKET:B = 0x5dt

.field static final BYTE_S:B = 0x73t

.field static final BYTE_SEMICOLON:B = 0x3bt

.field static final BYTE_SLASH:B = 0x2ft

.field static final BYTE_SPACE:B = 0x20t

.field static final BYTE_T:B = 0x74t

.field static final BYTE_U:B = 0x75t

.field static final BYTE_X:B = 0x78t

.field static final DEFAULT_COPY_BUFFER_SIZE:I = 0x3e8

.field static final DEFAULT_FULL_BUFFER_SIZE:I = 0xfa0

.field static final SMALL_WRITE:I = 0xfa


# instance fields
.field protected final _charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

.field protected _out:Ljava/io/OutputStream;

.field protected _outputBuffer:[B

.field protected final _outputBufferLen:I

.field protected _outputPtr:I

.field protected _surrogate:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-string v0, "<![CDATA["

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_CDATA_START:[B

    const-string v0, "]]>"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_CDATA_END:[B

    const-string v0, "<!--"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_COMMENT_START:[B

    const-string v0, "-->"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_COMMENT_END:[B

    const-string v0, "<?xml version=\'"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_XMLDECL_START:[B

    const-string v0, " encoding=\'"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_XMLDECL_ENCODING:[B

    const-string v0, " standalone=\'"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_XMLDECL_STANDALONE:[B

    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/aalto/out/WriterConfig;Ljava/io/OutputStream;Lcom/fasterxml/aalto/util/XmlCharTypes;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;-><init>(Lcom/fasterxml/aalto/out/WriterConfig;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    iput-object p2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    const/16 p2, 0xfa0

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/out/WriterConfig;->allocFullBBuffer(I)[B

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    array-length p1, p1

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iput-object p3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    return-void
.end method

.method public static final getAscii(Ljava/lang/String;[B)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static final getAscii(Ljava/lang/String;[BI)V
    .locals 4

    .line 2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    add-int v2, p2, v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, p1, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static final getAscii(Ljava/lang/String;)[B
    .locals 2

    .line 3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->getAscii(Ljava/lang/String;[BI)V

    return-object v0
.end method

.method private final longWriteCharacters(Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    move v4, v0

    :cond_0
    add-int v5, v3, v4

    invoke-virtual {p1, v3, v5, v1, v2}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v1, v2, v4}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCharacters([CII)V

    sub-int/2addr v0, v4

    if-gtz v0, :cond_1

    return-void

    :cond_1
    move v3, v5

    goto :goto_0
.end method

.method private final writeAttrNameEqQ(Lcom/fasterxml/aalto/out/WName;)V
    .locals 7

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int v2, v1, v0

    add-int/lit8 v2, v2, 0x3

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    const/16 v4, 0x22

    const/16 v5, 0x3d

    const/16 v6, 0x20

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x3

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v2, :cond_1

    invoke-virtual {p0, v6, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeName(BLcom/fasterxml/aalto/out/WName;)V

    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v2, v1, 0x1

    aput-byte v6, v0, v1

    invoke-virtual {p1, v0, v2}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v2, p1

    add-int/lit8 p1, v2, 0x1

    aput-byte v5, v0, v2

    add-int/lit8 v1, p1, 0x1

    aput-byte v4, v0, p1

    iput v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method private final writeSplitCharacters([CII)V
    .locals 5

    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_d

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    :cond_0
    aget-char v1, p1, p2

    const/16 v2, 0x800

    const/4 v3, 0x1

    if-lt v1, v2, :cond_1

    goto :goto_1

    :cond_1
    aget v4, v0, v1

    if-eqz v4, :cond_b

    const/16 v4, 0xa

    if-eq v1, v4, :cond_a

    :goto_1
    add-int/lit8 p2, p2, 0x1

    if-ge v1, v2, :cond_9

    aget v0, v0, v1

    if-eq v0, v3, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    packed-switch v0, :pswitch_data_0

    goto :goto_3

    :pswitch_0
    if-ge p2, p3, :cond_3

    aget-char v0, p1, p2

    const/16 v2, 0x3e

    if-ne v0, v2, :cond_7

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_0

    :cond_3
    :goto_2
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAsEntity(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->willEscapeCR()Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_2

    :cond_6
    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    :cond_7
    :goto_3
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v2, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_8
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    :cond_b
    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v2, v3, :cond_c

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_c
    iget-object v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    add-int/lit8 p2, p2, 0x1

    if-lt p2, p3, :cond_0

    :cond_d
    return-void

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public _closeTarget(Z)V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    :cond_0
    return-void
.end method

.method public _releaseBuffers()V
    .locals 3

    invoke-super {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->_releaseBuffers()V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v2, v0}, Lcom/fasterxml/aalto/out/WriterConfig;->freeFullBBuffer([B)V

    iput-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v2, v0}, Lcom/fasterxml/aalto/out/WriterConfig;->freeFullCBuffer([C)V

    iput-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    :cond_1
    return-void
.end method

.method public final calcSurrogate(IILjava/lang/String;)I
    .locals 3

    const v0, 0xdc00

    if-lt p2, v0, :cond_0

    const v1, 0xdfff

    if-le p2, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incomplete surrogate pair"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ": first char 0x"

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", second 0x"

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;)V

    :cond_1
    const p3, 0xd800

    sub-int/2addr p1, p3

    shl-int/lit8 p1, p1, 0xa

    const/high16 p3, 0x10000

    add-int/2addr p1, p3

    sub-int/2addr p2, v0

    add-int/2addr p1, p2

    const p2, 0x10ffff

    if-le p1, p2, :cond_2

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_2
    return p1
.end method

.method public final constructName(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->verifyNameComponent(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->doConstructName(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    return-object p1
.end method

.method public constructName(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->verifyNameComponent(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->verifyNameComponent(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->doConstructName(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    return-object p1
.end method

.method public abstract doConstructName(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
.end method

.method public abstract doConstructName(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;
.end method

.method public final fastWriteAttrValue([CII[BI)I
    .locals 4

    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_7

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->ATTR_CHARS:[I

    :cond_0
    aget-char v1, p1, p2

    const/16 v2, 0x800

    if-lt v1, v2, :cond_1

    goto :goto_1

    :cond_1
    aget v3, v0, v1

    if-eqz v3, :cond_6

    :goto_1
    iput p5, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 p2, p2, 0x1

    if-ge v1, v2, :cond_4

    aget p5, v0, v1

    const/4 v0, 0x1

    if-eq p5, v0, :cond_3

    const/4 v0, 0x5

    if-eq p5, v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAsEntity(I)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    :goto_2
    sub-int p5, p3, p2

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    sub-int/2addr v0, v1

    if-lt p5, v0, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_5
    iget p5, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    goto :goto_0

    :cond_6
    add-int/lit8 v2, p5, 0x1

    int-to-byte v1, v1

    aput-byte v1, p4, p5

    add-int/lit8 p2, p2, 0x1

    move p5, v2

    if-lt p2, p3, :cond_0

    :cond_7
    return p5
.end method

.method public final flush()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    :cond_0
    return-void
.end method

.method public final flushBuffer()V
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locPastChars:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locPastChars:I

    iget v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowStartOffset:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowStartOffset:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget-object v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-virtual {v1, v3, v2, v0}, Ljava/io/OutputStream;->write([BII)V

    :cond_0
    return-void
.end method

.method public final getOutputPtr()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return v0
.end method

.method public abstract output2ByteChar(I)V
.end method

.method public abstract outputMultiByteChar(I[CII)I
.end method

.method public abstract outputStrictMultiByteChar(I[CII)I
.end method

.method public abstract outputSurrogates(II)V
.end method

.method public final throwUnpairedSurrogate()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate(I)V

    return-void
.end method

.method public final throwUnpairedSurrogate(I)V
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flush()V

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unpaired surrogate character (0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public verifyNameComponent(Ljava/lang/String;)V
    .locals 13

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_NAME_EMPTY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const-string v2, " in name"

    const-string v3, ") not valid surrogate first character"

    const v4, 0xdc00

    const v5, 0xdfff

    const v6, 0xd800

    const/4 v7, 0x1

    if-lt v0, v6, :cond_4

    if-gt v0, v5, :cond_4

    if-lt v0, v4, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Illegal surrogate pairing in name: first character ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_2
    const/4 v8, 0x2

    if-ge v1, v8, :cond_3

    const-string v9, "Illegal surrogate pairing in name: incomplete surrogate (missing second half)"

    invoke-virtual {p0, v9}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {p0, v0, v7, v2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->calcSurrogate(IILjava/lang/String;)I

    move-result v0

    move v7, v8

    :cond_4
    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->is10NameStartChar(I)Z

    move-result v8

    const-string v9, " (name \""

    if-nez v8, :cond_5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid name start character "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\")"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/XmlWriter;->getHighestEncodable()I

    move-result v8

    const-string v10, ")"

    if-le v0, v8, :cond_6

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Illegal name start character "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\"): can not be expressed using effective encoding ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_6
    :goto_0
    if-ge v7, v1, :cond_c

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-lt v0, v6, :cond_9

    if-gt v0, v5, :cond_9

    if-lt v0, v4, :cond_7

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Illegal surrogate pairing in name: character at #"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, " ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_7
    add-int/lit8 v7, v7, 0x1

    if-lt v7, v1, :cond_8

    const-string v11, "Illegal surrogate pairing in name: name ends with incomplete surrogate pair"

    invoke-virtual {p0, v11}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-virtual {p0, v0, v11, v2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->calcSurrogate(IILjava/lang/String;)I

    move-result v0

    :cond_9
    if-le v0, v8, :cond_a

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Illegal name character "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "\", index #"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, "): can not be expressed using effective encoding ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v12}, Lcom/fasterxml/aalto/out/WriterConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_a
    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->is10NameChar(I)Z

    move-result v11

    if-nez v11, :cond_b

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid name character "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ") in name (\""

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\"), index #"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfName(Ljava/lang/String;)V

    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_c
    return-void
.end method

.method public final writeAsEntity(I)V
    .locals 10

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v2, v1, 0xa

    array-length v3, v0

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    :cond_0
    add-int/lit8 v2, v1, 0x1

    const/16 v3, 0x26

    aput-byte v3, v0, v1

    const/16 v1, 0x100

    const/16 v4, 0x78

    const/16 v5, 0x23

    const/16 v6, 0xa

    if-ge p1, v1, :cond_9

    const/16 v1, 0x70

    const/16 v7, 0x61

    if-ne p1, v3, :cond_1

    add-int/lit8 p1, v2, 0x1

    aput-byte v7, v0, v2

    add-int/lit8 v2, p1, 0x1

    const/16 v3, 0x6d

    aput-byte v3, v0, p1

    add-int/lit8 p1, v2, 0x1

    aput-byte v1, v0, v2

    goto/16 :goto_5

    :cond_1
    const/16 v3, 0x3c

    const/16 v8, 0x74

    if-ne p1, v3, :cond_2

    add-int/lit8 p1, v2, 0x1

    const/16 v1, 0x6c

    aput-byte v1, v0, v2

    add-int/lit8 v1, p1, 0x1

    aput-byte v8, v0, p1

    :goto_0
    move p1, v1

    goto/16 :goto_5

    :cond_2
    const/16 v3, 0x3e

    if-ne p1, v3, :cond_3

    add-int/lit8 p1, v2, 0x1

    const/16 v1, 0x67

    aput-byte v1, v0, v2

    add-int/lit8 v1, p1, 0x1

    aput-byte v8, v0, p1

    goto :goto_0

    :cond_3
    const/16 v3, 0x27

    const/16 v9, 0x6f

    if-ne p1, v3, :cond_4

    add-int/lit8 p1, v2, 0x1

    aput-byte v7, v0, v2

    add-int/lit8 v2, p1, 0x1

    aput-byte v1, v0, p1

    add-int/lit8 p1, v2, 0x1

    aput-byte v9, v0, v2

    add-int/lit8 v1, p1, 0x1

    const/16 v2, 0x73

    aput-byte v2, v0, p1

    goto :goto_0

    :cond_4
    const/16 v1, 0x22

    if-ne p1, v1, :cond_5

    add-int/lit8 p1, v2, 0x1

    const/16 v1, 0x71

    aput-byte v1, v0, v2

    add-int/lit8 v1, p1, 0x1

    const/16 v2, 0x75

    aput-byte v2, v0, p1

    add-int/lit8 p1, v1, 0x1

    aput-byte v9, v0, v1

    add-int/lit8 v1, p1, 0x1

    aput-byte v8, v0, p1

    goto :goto_0

    :cond_5
    add-int/lit8 v1, v2, 0x1

    aput-byte v5, v0, v2

    add-int/lit8 v2, v1, 0x1

    aput-byte v4, v0, v1

    const/16 v1, 0x10

    if-lt p1, v1, :cond_7

    shr-int/lit8 v1, p1, 0x4

    add-int/lit8 v3, v2, 0x1

    if-ge v1, v6, :cond_6

    add-int/lit8 v1, v1, 0x30

    goto :goto_1

    :cond_6
    add-int/lit8 v1, v1, 0x57

    :goto_1
    int-to-byte v1, v1

    aput-byte v1, v0, v2

    and-int/lit8 p1, p1, 0xf

    move v2, v3

    :cond_7
    add-int/lit8 v1, v2, 0x1

    if-ge p1, v6, :cond_8

    add-int/lit8 p1, p1, 0x30

    goto :goto_2

    :cond_8
    add-int/lit8 p1, p1, 0x57

    :goto_2
    int-to-byte p1, p1

    aput-byte p1, v0, v2

    goto :goto_0

    :cond_9
    add-int/lit8 v1, v2, 0x1

    aput-byte v5, v0, v2

    add-int/lit8 v2, v1, 0x1

    aput-byte v4, v0, v1

    const/16 v1, 0x14

    move v3, v2

    :cond_a
    shr-int v4, p1, v1

    and-int/lit8 v4, v4, 0xf

    if-gtz v4, :cond_b

    if-eq v3, v2, :cond_d

    :cond_b
    add-int/lit8 v5, v3, 0x1

    if-ge v4, v6, :cond_c

    add-int/lit8 v4, v4, 0x30

    goto :goto_3

    :cond_c
    add-int/lit8 v4, v4, 0x57

    :goto_3
    int-to-byte v4, v4

    aput-byte v4, v0, v3

    move v3, v5

    :cond_d
    add-int/lit8 v1, v1, -0x4

    if-gtz v1, :cond_a

    and-int/lit8 p1, p1, 0xf

    add-int/lit8 v1, v3, 0x1

    if-ge p1, v6, :cond_e

    add-int/lit8 p1, p1, 0x30

    goto :goto_4

    :cond_e
    add-int/lit8 p1, p1, 0x57

    :goto_4
    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto/16 :goto_0

    :goto_5
    add-int/lit8 v1, p1, 0x1

    const/16 v2, 0x3b

    aput-byte v2, v0, p1

    iput v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public final writeAttrValue([CII)V
    .locals 5

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    aget-char v1, p1, p2

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputSurrogates(II)V

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 p3, p3, -0x1

    :cond_0
    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->ATTR_CHARS:[I

    :cond_1
    aget-char v1, p1, p2

    const/16 v2, 0x800

    if-lt v1, v2, :cond_2

    goto :goto_1

    :cond_2
    aget v3, v0, v1

    if-eqz v3, :cond_6

    :goto_1
    add-int/lit8 p2, p2, 0x1

    if-ge v1, v2, :cond_5

    aget v0, v0, v1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAsEntity(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_4
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v2, v3, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_7
    iget-object v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    add-int/lit8 p2, p2, 0x1

    if-lt p2, p3, :cond_1

    :cond_8
    return-void
.end method

.method public final writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBufferLen:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeLongAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;I)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    const/4 v2, 0x0

    if-lez v0, :cond_1

    invoke-virtual {p2, v2, v0, v1, v2}, Ljava/lang/String;->getChars(II[CI)V

    :cond_1
    invoke-virtual {p0, p1, v1, v2, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAttribute(Lcom/fasterxml/aalto/out/WName;[CII)V

    return-void
.end method

.method public final writeAttribute(Lcom/fasterxml/aalto/out/WName;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
    .locals 2

    .line 2
    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAttrNameEqQ(Lcom/fasterxml/aalto/out/WName;)V

    iget p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    sub-int/2addr p1, v0

    invoke-virtual {p2, p1}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->bufferNeedsFlush(I)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flush()V

    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    invoke-virtual {p2, p1, v0, v1}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->encodeMore([BII)I

    move-result p1

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p2}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->isCompleted()Z

    move-result p1

    if-eqz p1, :cond_2

    iget p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget p2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt p1, p2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_1
    iget-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget p2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v0, p2, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    const/16 v0, 0x22

    aput-byte v0, p1, p2

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    goto :goto_0
.end method

.method public final writeAttribute(Lcom/fasterxml/aalto/out/WName;[CII)V
    .locals 9

    .line 3
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget-object v7, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v1

    add-int/2addr v1, v0

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    const/16 v3, 0x20

    if-lt v1, v2, :cond_1

    invoke-virtual {p0, v3, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeName(BLcom/fasterxml/aalto/out/WName;)V

    iget p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v0, 0x1

    aput-byte v3, v7, v0

    invoke-virtual {p1, v7, v1}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr p1, v1

    :goto_0
    add-int/lit8 v0, p1, 0x3

    add-int/2addr v0, p4

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    const/16 v2, 0x3d

    const/16 v8, 0x22

    if-le v0, v1, :cond_2

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v0, p1, 0x1

    aput-byte v2, v7, p1

    add-int/lit8 p1, v0, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    aput-byte v8, v7, v0

    add-int v0, p1, p4

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v0, v1, :cond_3

    invoke-virtual {p0, p2, p3, p4}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAttrValue([CII)V

    invoke-virtual {p0, v8}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    return-void

    :cond_2
    add-int/lit8 v0, p1, 0x1

    aput-byte v2, v7, p1

    add-int/lit8 p1, v0, 0x1

    aput-byte v8, v7, v0

    :cond_3
    move v6, p1

    if-lez p4, :cond_4

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, v7

    invoke-virtual/range {v1 .. v6}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->fastWriteAttrValue([CII[BI)I

    move-result v6

    :cond_4
    add-int/lit8 p1, v6, 0x1

    aput-byte v8, v7, v6

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public writeCData(Ljava/lang/String;)I
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataStart()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-lez v0, :cond_2

    iget-object v3, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v4, v3

    if-le v4, v0, :cond_0

    move v4, v0

    :cond_0
    add-int v5, v2, v4

    invoke-virtual {p1, v2, v5, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v3, v1, v4}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataContents([CII)I

    move-result v3

    if-ltz v3, :cond_1

    add-int/2addr v2, v3

    return v2

    :cond_1
    sub-int/2addr v0, v4

    move v2, v5

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataEnd()V

    const/4 p1, -0x1

    return p1
.end method

.method public writeCData([CII)I
    .locals 0

    .line 2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataStart()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataContents([CII)I

    move-result p1

    if-gez p1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataEnd()V

    :cond_0
    return p1
.end method

.method public writeCDataContents([CII)I
    .locals 5

    add-int/2addr p3, p2

    :cond_0
    :goto_0
    if-ge p2, p3, :cond_d

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->OTHER_CHARS:[I

    :cond_1
    aget-char v1, p1, p2

    const/16 v2, 0x800

    if-lt v1, v2, :cond_2

    goto :goto_1

    :cond_2
    aget v3, v0, v1

    if-eqz v3, :cond_b

    :goto_1
    add-int/lit8 p2, p2, 0x1

    if-ge v1, v2, :cond_a

    aget v0, v0, v1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v3, 0x2

    if-eq v0, v3, :cond_7

    const/4 v3, 0x3

    if-eq v0, v3, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_4

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb

    if-eq v0, v2, :cond_3

    goto :goto_2

    :cond_3
    if-ge p2, p3, :cond_8

    aget-char v0, p1, p2

    const/16 v2, 0x5d

    if-ne v0, v2, :cond_8

    add-int/lit8 v0, p2, 0x1

    if-ge v0, p3, :cond_0

    aget-char v0, p1, v0

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_0

    add-int/lit8 p2, p2, 0x2

    invoke-virtual {p0, v2, v2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(BB)V

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataEnd()V

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCDataStart()V

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    goto :goto_0

    :cond_4
    const-string v0, "CDATA"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportFailedEscaping(Ljava/lang/String;I)V

    :cond_5
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_7
    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    :cond_8
    :goto_2
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v2, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_9
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    goto :goto_0

    :cond_a
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    goto :goto_0

    :cond_b
    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v2, v3, :cond_c

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_c
    iget-object v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    add-int/lit8 p2, p2, 0x1

    if-lt p2, p3, :cond_1

    :cond_d
    const/4 p1, -0x1

    return p1
.end method

.method public final writeCDataEnd()V
    .locals 1

    sget-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_CDATA_END:[B

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    return-void
.end method

.method public final writeCDataStart()V
    .locals 1

    sget-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_CDATA_START:[B

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    return-void
.end method

.method public final writeCharacters(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBufferLen:I

    if-le v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->longWriteCharacters(Ljava/lang/String;)V

    return-void

    :cond_0
    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v1, v2}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v1, v2, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCharacters([CII)V

    :cond_1
    return-void
.end method

.method public final writeCharacters([CII)V
    .locals 5

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    aget-char v1, p1, p2

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputSurrogates(II)V

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 p3, p3, -0x1

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int v1, v0, p3

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v1, v2, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeSplitCharacters([CII)V

    return-void

    :cond_1
    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_b

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v1, v1, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    :cond_2
    aget-char v2, p1, p2

    const/16 v3, 0x800

    if-lt v2, v3, :cond_3

    goto :goto_1

    :cond_3
    aget v4, v1, v2

    if-eqz v4, :cond_a

    const/16 v4, 0xa

    if-eq v2, v4, :cond_9

    :goto_1
    add-int/lit8 p2, p2, 0x1

    if-ge v2, v3, :cond_7

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_3

    :pswitch_1
    if-ge p2, p3, :cond_4

    aget-char v1, p1, p2

    const/16 v3, 0x3e

    if-ne v1, v3, :cond_6

    goto :goto_2

    :pswitch_2
    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_5

    :cond_4
    :goto_2
    :pswitch_3
    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAsEntity(I)V

    goto :goto_5

    :pswitch_4
    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :pswitch_5
    iget-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/WriterConfig;->willEscapeCR()Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v3, v0, 0x1

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    goto :goto_4

    :cond_6
    :goto_3
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v3, v0, 0x1

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    :goto_4
    move v0, v3

    goto :goto_0

    :cond_7
    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p0, v2, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    :goto_5
    :pswitch_6
    sub-int v0, p3, p2

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    sub-int/2addr v1, v2

    if-lt v0, v1, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_8
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    goto :goto_0

    :cond_9
    iget v3, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    :cond_a
    iget-object v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v4, v0, 0x1

    int-to-byte v2, v2

    aput-byte v2, v3, v0

    add-int/lit8 p2, p2, 0x1

    move v0, v4

    if-lt p2, p3, :cond_2

    :cond_b
    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public writeComment(Ljava/lang/String;)I
    .locals 7

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCommentStart()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-lez v0, :cond_2

    iget-object v3, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v4, v3

    if-ge v0, v4, :cond_0

    move v5, v0

    goto :goto_1

    :cond_0
    move v5, v4

    :goto_1
    add-int v6, v2, v5

    invoke-virtual {p1, v2, v6, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v3, v1, v5}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCommentContents([CII)I

    move-result v3

    if-ltz v3, :cond_1

    add-int/2addr v2, v3

    return v2

    :cond_1
    add-int/2addr v2, v4

    sub-int/2addr v0, v4

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeCommentEnd()V

    const/4 p1, -0x1

    return p1
.end method

.method public writeCommentContents([CII)I
    .locals 5

    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_c

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->OTHER_CHARS:[I

    :cond_0
    aget-char v1, p1, p2

    const/16 v2, 0x800

    if-lt v1, v2, :cond_1

    goto :goto_1

    :cond_1
    aget v3, v0, v1

    if-eqz v3, :cond_a

    :goto_1
    add-int/lit8 p2, p2, 0x1

    if-ge v1, v2, :cond_9

    aget v0, v0, v1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v3, 0x2

    if-eq v0, v3, :cond_7

    const/4 v3, 0x3

    if-eq v0, v3, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_4

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd

    if-eq v0, v2, :cond_2

    goto :goto_2

    :cond_2
    const/16 v0, 0x2d

    if-ge p2, p3, :cond_3

    aget-char v2, p1, p2

    if-eq v2, v0, :cond_3

    goto :goto_2

    :cond_3
    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(BB)V

    goto :goto_0

    :cond_4
    const-string v0, "comment"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportFailedEscaping(Ljava/lang/String;I)V

    :cond_5
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_7
    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    :goto_2
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v2, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_8
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v2, v3, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_b
    iget-object v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    add-int/lit8 p2, p2, 0x1

    if-lt p2, p3, :cond_0

    :cond_c
    const/4 p1, -0x1

    return p1
.end method

.method public final writeCommentEnd()V
    .locals 1

    sget-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_COMMENT_END:[B

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    return-void
.end method

.method public final writeCommentStart()V
    .locals 1

    sget-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_COMMENT_START:[B

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    return-void
.end method

.method public writeDTD(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    return-void
.end method

.method public writeDTD(Ljava/lang/String;)V
    .locals 2

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(Ljava/lang/String;II)V

    return-void
.end method

.method public final writeEndTag(Lcom/fasterxml/aalto/out/WName;)V
    .locals 7

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v1

    add-int v2, v0, v1

    add-int/lit8 v2, v2, 0x3

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    const/16 v4, 0x3e

    const/16 v5, 0x2f

    const/16 v6, 0x3c

    if-le v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    add-int/lit8 v1, v1, 0x3

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v1, v0, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, v6}, Ljava/io/OutputStream;->write(I)V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, v5}, Ljava/io/OutputStream;->write(I)V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/out/WName;->writeBytes(Ljava/io/OutputStream;)V

    iget-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    aput-byte v4, p1, v0

    return-void

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    :cond_2
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v2, v0, 0x1

    aput-byte v6, v1, v0

    add-int/lit8 v0, v2, 0x1

    aput-byte v5, v1, v2

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v0, p1

    add-int/lit8 p1, v0, 0x1

    aput-byte v4, v1, v0

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public writeEntityReference(Lcom/fasterxml/aalto/out/WName;)V
    .locals 1

    const/16 v0, 0x26

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeName(Lcom/fasterxml/aalto/out/WName;)V

    const/16 p1, 0x3b

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    return-void
.end method

.method public final writeLongAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;I)V
    .locals 5

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int v2, v1, v0

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/out/WName;->writeBytes(Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v1, p1

    iput v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    :goto_0
    const/16 p1, 0x3d

    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(BB)V

    const/4 p1, 0x0

    move v1, p1

    :goto_1
    if-lez p3, :cond_3

    iget-object v2, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v3, v2

    if-ge p3, v3, :cond_2

    move v3, p3

    :cond_2
    add-int v4, v1, v3

    invoke-virtual {p2, v1, v4, v2, p1}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v2, p1, v3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeAttrValue([CII)V

    sub-int/2addr p3, v3

    move v1, v4

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    return-void
.end method

.method public final writeName(BLcom/fasterxml/aalto/out/WName;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    invoke-virtual {p2}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    iget-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {p2, p1}, Lcom/fasterxml/aalto/out/WName;->writeBytes(Ljava/io/OutputStream;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v2, v0, 0x1

    aput-byte p1, v1, v0

    invoke-virtual {p2, v1, v2}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v2, p1

    iput v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public final writeName(Lcom/fasterxml/aalto/out/WName;)V
    .locals 4

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v1

    add-int v2, v0, v1

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/out/WName;->writeBytes(Ljava/io/OutputStream;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public final writeName(Lcom/fasterxml/aalto/out/WName;B)V
    .locals 2

    .line 3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/out/WName;->writeBytes(Ljava/io/OutputStream;)V

    iget-object p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write(I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v0, p1

    add-int/lit8 p1, v0, 0x1

    aput-byte p2, v1, v0

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public writePI(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)I
    .locals 6

    const/16 v0, 0x3c

    const/16 v1, 0x3f

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(BB)V

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeName(Lcom/fasterxml/aalto/out/WName;)V

    if-eqz p2, :cond_2

    const/16 p1, 0x20

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-lez p1, :cond_2

    iget-object v3, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v4, v3

    if-le v4, p1, :cond_0

    move v4, p1

    :cond_0
    add-int v5, v2, v4

    invoke-virtual {p2, v2, v5, v3, v0}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v3, v0, v4}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writePIData([CII)I

    move-result v3

    if-ltz v3, :cond_1

    add-int/2addr v2, v3

    return v2

    :cond_1
    sub-int/2addr p1, v4

    move v2, v5

    goto :goto_0

    :cond_2
    const/16 p1, 0x3e

    invoke-virtual {p0, v1, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(BB)V

    const/4 p1, -0x1

    return p1
.end method

.method public writePIData([CII)I
    .locals 5

    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_c

    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->OTHER_CHARS:[I

    :cond_0
    aget-char v1, p1, p2

    const/16 v2, 0x800

    if-lt v1, v2, :cond_1

    goto :goto_1

    :cond_1
    aget v3, v0, v1

    if-eqz v3, :cond_a

    :goto_1
    add-int/lit8 p2, p2, 0x1

    if-ge v1, v2, :cond_9

    aget v0, v0, v1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    const/4 v3, 0x3

    if-eq v0, v3, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    goto :goto_2

    :cond_2
    if-ge p2, p3, :cond_7

    aget-char v0, p1, p2

    const/16 v2, 0x3e

    if-ne v0, v2, :cond_7

    return p2

    :cond_3
    const-string v0, "processing instruction"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportFailedEscaping(Ljava/lang/String;I)V

    :cond_4
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->output2ByteChar(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->reportInvalidChar(I)V

    :cond_6
    iget v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_locRowNr:I

    :cond_7
    :goto_2
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v2, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_8
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p0, v1, p1, p2, p3}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->outputMultiByteChar(I[CII)I

    move-result p2

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v2, v3, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_b
    iget-object v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    add-int/lit8 p2, p2, 0x1

    if-lt p2, p3, :cond_0

    :cond_c
    const/4 p1, -0x1

    return p1
.end method

.method public final writeRaw(B)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    aput-byte p1, v0, v1

    return-void
.end method

.method public final writeRaw(BB)V
    .locals 3

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v2, v1, 0x1

    aput-byte p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    aput-byte p2, v0, v2

    return-void
.end method

.method public final writeRaw(Ljava/lang/String;II)V
    .locals 4

    .line 3
    :goto_0
    if-lez p3, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v1, v0

    if-ge p3, v1, :cond_0

    move v1, p3

    :cond_0
    add-int v2, p2, v1

    const/4 v3, 0x0

    invoke-virtual {p1, p2, v2, v0, v3}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v0, v3, v1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([CII)V

    sub-int/2addr p3, v1

    move p2, v2

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final writeRaw([B)V
    .locals 2

    .line 4
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([BII)V

    return-void
.end method

.method public final writeRaw([BII)V
    .locals 3

    .line 5
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int v1, v0, p3

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-static {p1, p2, v1, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void

    :cond_1
    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flush()V

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    :cond_2
    const/16 v1, 0xfa

    if-ge p3, v1, :cond_3

    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    invoke-static {p1, p2, v1, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    :goto_0
    return-void
.end method

.method public abstract writeRaw([CII)V
.end method

.method public final writeSpace(Ljava/lang/String;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_copyBuffer:[C

    array-length v4, v3

    if-ge v0, v4, :cond_0

    move v4, v0

    :cond_0
    add-int v5, v2, v4

    invoke-virtual {p1, v2, v5, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    invoke-virtual {p0, v3, v1, v4}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeSpace([CII)V

    sub-int/2addr v0, v4

    move v2, v5

    goto :goto_0

    :cond_1
    return-void
.end method

.method public writeSpace([CII)V
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_out:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_SPACE_CONTENT:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    add-int/lit8 v2, p2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    add-int/2addr p3, p2

    :goto_0
    if-ge p2, p3, :cond_5

    add-int/lit8 v0, p2, 0x1

    aget-char p2, p1, p2

    const/16 v1, 0x20

    if-le p2, v1, :cond_3

    iget-object v1, p0, Lcom/fasterxml/aalto/out/XmlWriter;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/WriterConfig;->isXml11()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x85

    if-eq p2, v1, :cond_3

    const/16 v1, 0x2028

    if-eq p2, v1, :cond_3

    :cond_2
    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_SPACE_CONTENT:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/fasterxml/aalto/out/XmlWriter;->reportNwfContent(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_4
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    int-to-byte p2, p2

    aput-byte p2, v1, v2

    move p2, v0

    goto :goto_0

    :cond_5
    return-void
.end method

.method public writeStartTagEmptyEnd()V
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v1, v0, 0x2

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x2f

    aput-byte v3, v1, v0

    add-int/lit8 v0, v2, 0x1

    const/16 v3, 0x3e

    aput-byte v3, v1, v2

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public final writeStartTagEnd()V
    .locals 3

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    const/16 v2, 0x3e

    aput-byte v2, v0, v1

    return-void
.end method

.method public final writeStartTagStart(Lcom/fasterxml/aalto/out/WName;)V
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WName;->serializedLength()I

    move-result v1

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    const/16 v3, 0x3c

    if-le v1, v2, :cond_1

    invoke-virtual {p0, v3, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeName(BLcom/fasterxml/aalto/out/WName;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    add-int/lit8 v2, v0, 0x1

    aput-byte v3, v1, v0

    invoke-virtual {p1, v1, v2}, Lcom/fasterxml/aalto/out/WName;->appendBytes([BI)I

    move-result p1

    add-int/2addr v2, p1

    iput v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    return-void
.end method

.method public writeTypedValue(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
    .locals 3

    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_surrogate:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->throwUnpairedSurrogate()V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->bufferNeedsFlush(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flush()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBuffer:[B

    iget v1, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputBufferLen:I

    invoke-virtual {p1, v0, v1, v2}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->encodeMore([BII)I

    move-result v0

    iput v0, p0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->_outputPtr:I

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->flushBuffer()V

    goto :goto_0
.end method

.method public writeXmlDeclaration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_XMLDECL_START:[B

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(Ljava/lang/String;II)V

    const/16 p1, 0x27

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_XMLDECL_ENCODING:[B

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, p2, v1, v0}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(Ljava/lang/String;II)V

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    :cond_0
    if-eqz p3, :cond_1

    sget-object p2, Lcom/fasterxml/aalto/out/ByteXmlWriter;->BYTES_XMLDECL_STANDALONE:[B

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw([B)V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    invoke-virtual {p0, p3, v1, p2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(Ljava/lang/String;II)V

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(B)V

    :cond_1
    const/16 p1, 0x3f

    const/16 p2, 0x3e

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/out/ByteXmlWriter;->writeRaw(BB)V

    return-void
.end method
